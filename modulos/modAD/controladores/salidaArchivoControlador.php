<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Archivo Digital
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de la salida de documentos
class salidaArchivoControlador extends Controlador
{
	private $atSalidaModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atSalidaModelo = $this->metCargarModelo('registroArchivo');
		$this->atSalidaModelo = $this->metCargarModelo('salidaArchivo');
	}

	// Método index del controlador
	public function metIndex()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef',
			'bootstrap-datepicker/datepicker',
			'multi-select/multi-select555c',
			'wizard/wizardfa6c',
			'lightGallery-master/dist/css/lightgallery.min'
		);
		$complementosJs = array(
			'select2/select2.min',
			'bootstrap-datepicker/bootstrap-datepicker',
			'inputmask/jquery.inputmask.bundle.min',
			'multi-select/jquery.multi-select',
			'wizard/jquery.bootstrap.wizard.min',
			'lightGallery-master/dist/js/lightgallery.min',
			'lightGallery-master/dist/js/lg-thumbnail.min',
			'lightGallery-master/dist/js/lg-fullscreen.min'
		);
		$js = array(
			'Aplicacion/appFunciones',
			'materialSiace/core/demo/DemoTableDynamic',
			'materialSiace/core/demo/DemoFormWizard',
			'materialSiace/App',
			'materialSiace/core/demo/DemoFormComponents'
		);

		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$validar=array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$metodoOrdenar = 1;
        $usuario = Session::metObtener('idUsuario');
        $idAplicacion = 14;
		$this->atVista->assign('_PruebaPost', $this->atSalidaModelo->metListarSalida($metodoOrdenar, 0, $usuario, $idAplicacion));
		$this->atVista->metRenderizar('listado');
	}

	// Método que permite listar una nueva salida
	public function metNuevaSalida()
	{
		$valido = $this->metObtenerInt('valido');
		$usuario = Session::metObtener('idUsuario');
		if (isset($valido) && $valido == 1) {
			$this->metValidarToken();
			$pk_num_registro_documento = $this->metObtenerInt('pk_num_registro_documento');
			$cod_memo = $this->metObtenerTexto('cod_memo');
			$ind_motivo = $this->metObtenerTexto('ind_motivo');
			$fechaPrevia = $_POST['fecha_salida'];
			$fechaExplode = explode("/",$fechaPrevia);
			$fec_fecha_salida = $fechaExplode[2].'-'.$fechaExplode[1].'-'.$fechaExplode[0];
			$fecha_hora = date('Y-m-d H:i:s');
			$metodo = 1;
			$obtenerUsuario = $this->atSalidaModelo->metUsuario($usuario, $metodo);
			$funcionario = $obtenerUsuario['pk_num_empleado'];
			$pkNumSalida=$this->atSalidaModelo->metGuardarSalida($fec_fecha_salida,$pk_num_registro_documento,$cod_memo, $ind_motivo,  $fecha_hora ,$usuario, $funcionario);
			$verSalida = $this->atSalidaModelo->metVerSalida($pkNumSalida, $metodo);
			$num_registro = $verSalida['num_registro'];
			$ind_documento = $verSalida['ind_documento'];
			$ind_descripcion_documento = $verSalida['ind_descripcion'];
			$estatus = 'PREPARADO';
			$arrayPost = array(
				'pk_num_salida' => $pkNumSalida,
				'num_registro' => $num_registro,
				'ind_documento' => $ind_documento,
				'ind_descripcion' => $ind_descripcion_documento,
				'ind_estado' => $estatus,
				'ind_motivo' => $ind_motivo,
				'cod_memo' => $cod_memo,
			);
			echo json_encode($arrayPost);
			exit;
		}
		$form = array(
			'status' => 'nuevo',
			'pk_num_salida' => null,
			'num_registro' => null,
			'ind_descripcion' => null,
			'ind_estado' => null,
			'ind_motivo' => null,
			'cod_memo' => null,
		);
		$metodo = 2;
		$almacen=$this->atSalidaModelo->metListarAlmacen();
		$this->atVista->assign('almacen',$almacen);
		$obtenerUsuario = $this->atSalidaModelo->metUsuario($usuario, $metodo);
		$this->atVista->assign('funcionario',$obtenerUsuario);
		$this->atVista->assign('form', $form);
		$this->atVista->metRenderizar('nuevo', 'modales');
	}

	// Método que permite listar los pasillos de un almacén en específico
	public function metBuscarPasillo()
	{
		$pkNumAlmacen = $this->metObtenerInt('idAlmacen');
		$listarPasillo=$this->atSalidaModelo->metBuscarPasillo($pkNumAlmacen);

		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_pasillo" id="pk_num_pasillo" onchange="cargarEstante(this.value)"> <option value="" disabled selected>&nbsp;</option>';
		foreach($listarPasillo as $listarPasillo){
			$a .='<option value="'.$listarPasillo['pk_num_pasillo'].'">'.$listarPasillo['ind_pasillo'].'</option>';
		}
		$a .= '</select><label for="pk_num_pasillo"><i class="glyphicon glyphicon-th"></i> Pasillo</label></div>';
		echo $a;
	}

	// Método que permite listar los estantes de un pasillo
	public function metBuscarEstante()
	{
		$pkNumPasillo = $this->metObtenerInt('idPasillo');
		$listarEstante=$this->atSalidaModelo->metBuscarEstante($pkNumPasillo);

		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_estante" id="pk_num_estante" onchange="cargarDependencia(this.value)"> <option value="" disabled selected>&nbsp;</option>';
		foreach($listarEstante as $listarEstante){
			$a .='<option value="'.$listarEstante['pk_num_estante'].'">'.$listarEstante['ind_descripcion_estante'].'</option>';
		}
		$a .= '</select><label for="pk_num_estante"><i class="glyphicon glyphicon-th-list"></i> Estante</label></div>';
		echo $a;
	}

	// Método que permite listar las dependencias de la contraloría
	public function metBuscarDependencia()
	{
		$pkNumEstante = $this->metObtenerInt('idEstante');
        $usuario = Session::metObtener('idUsuario');
        $idAplicacion = 14;
        $listarDependencia=$this->atSalidaModelo->metConsultarSeguridadAlterna($usuario, $idAplicacion);
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_dependencia" id="pk_num_dependencia" onchange="cargarDocumento(this.value,'.$pkNumEstante.')"> <option value="" disabled selected>&nbsp;</option>';
		foreach($listarDependencia as $listarDependencia){
			$a .='<option value="'.$listarDependencia['pk_num_dependencia'].'">'.$listarDependencia['ind_dependencia'].'</option>';
		}
		$a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label></div>';
		echo $a;
	}

	// Método que permite listar los tipos de documentos
	public function metTipoDocumento()
	{
		$pkNumDependencia = $this->metObtenerInt('idDependencia');
		$pkNumEstante = $this->metObtenerInt('idEstante');
		$listarDocumento=$this->atSalidaModelo->metListarDocumento($pkNumDependencia);
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_documento" id="pk_num_documento" onchange="cargarCaja(this.value,'.$pkNumDependencia.','.$pkNumEstante.')"> <option value="" disabled selected>&nbsp;</option>';
		foreach($listarDocumento as $listarDocumento){
			$a .='<option value="'.$listarDocumento['pk_num_documento'].'">'.$listarDocumento['ind_descripcion_documento'].'</option>';
		}
		$a .= '</select><label for="pk_num_documento"><i class="glyphicon glyphicon-file"></i> Tipo de documento</label></div>';
		echo $a;
	}

	// Método que permite listar las cajas de los estantes
	public function metBuscarCaja()
	{
		$pkNumDocumento = $this->metObtenerInt('idDocumento');
		$pkNumDependencia = $this->metObtenerInt('idDependencia');
		$pkNumEstante = $this->metObtenerInt('idEstante');
		$listarCaja=$this->atSalidaModelo->metListarCaja($pkNumDependencia, $pkNumEstante, $pkNumDocumento);
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_caja" id="pk_num_caja" onchange="cargarRegistro(this.value)"> <option value="" disabled selected>&nbsp;</option>';
		foreach($listarCaja as $listarCaja){
			$a .='<option value="'.$listarCaja['pk_num_caja'].'">'.$listarCaja['pk_num_caja'].' - '.$listarCaja['ind_descripcion_caja'].'</option>';
		}
		$a .= '</select><label for="pk_num_caja"><i class="glyphicon glyphicon-th-large"></i> Caja</label></div>';
		echo $a;
	}

	// Método que permite buscar los registros de una caja en particular
	public function metBuscarRegistro()
	{
		$pkNumCaja = $this->metObtenerInt('idCaja');
		$listarRegistros=$this->atSalidaModelo->metListarRegistros($pkNumCaja);
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_registro_documento" id="pk_num_registro_documento" onchange="verificarEntrada(this.value)"> <option value="" disabled selected>&nbsp;</option>';
		foreach($listarRegistros as $listarRegistros){
			$a .='<option value="'.$listarRegistros['pk_num_registro_documento'].'">'.$listarRegistros['num_registro'].'-'.$listarRegistros['ind_documento'].'</option>';
		}
		$a .= '</select><label for="pk_num_registro_documento"><i class="glyphicon glyphicon-briefcase"></i> Documento</label></div>';
		echo $a;
	}

	// Método que permite eliminar una salida
	public function metEliminarSalida()
	{
		$pkNumSalida = $this->metObtenerInt('pk_num_salida');
		$this->atSalidaModelo->metEliminarSalida($pkNumSalida);
		$arrayPost = array(
			'status' => 'OK',
			'pk_num_salida' => $pkNumSalida
		);
		echo json_encode($arrayPost);
	}

	// Método que permite ver una salida de documento
	public function metVerSalida()
	{
		$pkNumSalida = $this->metObtenerInt('pk_num_salida');
		$verSalida = $this->atSalidaModelo->metVerSalida($pkNumSalida, 1);
		$verAcceso = $this->atSalidaModelo->metAcceso($pkNumSalida);
		$this->atVista->assign('acceso', $verAcceso);
		$this->atVista->assign('sal',$verSalida);
		$pkNumRegistroDocumento = $verSalida['fk_adb001_num_registro_documento'];
		$registro=$this->atSalidaModelo->metVisualizarRegistro($pkNumRegistroDocumento, 1);
		$this->atVista->assign('registro',$registro);
        $numFlagArchivo = $registro['num_flag_archivo'];
        if($numFlagArchivo==1){
            $archivo = $this->atSalidaModelo->metBuscarArchivo($pkNumRegistroDocumento);
            $this->atVista->assign('archivo', $archivo);
            $ruta = 'publico/imagenes/modAD/';
            $explodeArchivo = explode(".", $archivo['ind_nombre_archivo']);
            $extension = $explodeArchivo[1];
            $rutaArchivo = array(
                'ruta' => $ruta,
                'extension' => $extension
            );
            $this->atVista->assign('ruta', $rutaArchivo);
		}
		$this->atVista->metRenderizar('ver', 'modales');
	}

	// Método que permite editar una salida de documento
	public function metEditarSalida()
	{
		$pkNumSalida = $this->metObtenerInt('pk_num_salida');
		$usuario = Session::metObtener('idUsuario');
		$valido = $this->metObtenerInt('valido');
		if (isset($valido) && $valido == 1) {
			$cod_memo = $_POST['cod_memo'];
			$ind_motivo = $_POST['ind_motivo'];
			$fechaPrevia = $_POST['fecha_salida'];
			$fechaExplode = explode("/",$fechaPrevia);
			$fec_fecha_salida = $fechaExplode[2].'-'.$fechaExplode[1].'-'.$fechaExplode[0];
			$this->metValidarToken();
			$fecha_hora = date('Y-m-d H:i:s');
			$this->atSalidaModelo->metEditarSalida($cod_memo, $ind_motivo, $fec_fecha_salida, $fecha_hora,$usuario, $pkNumSalida);
			$metodo = 1;
			$verSalida = $this->atSalidaModelo->metVerSalida($pkNumSalida, $metodo);
			$num_registro = $verSalida['num_registro'];
			$ind_documento = $verSalida['ind_documento'];
			$ind_descripcion = $verSalida['ind_descripcion'];
			$estatoPrevio = $verSalida['ind_estado'];
			if($estatoPrevio=='PR'){
				$estado = 'PREPARADO';
			} else {
				$estado = 'APROBADO';
			}
			$arrayPost = array(
				'status' => 'modificar',
				'num_registro' => $num_registro,
				'ind_documento' => $ind_documento,
				'ind_descripcion' => $ind_descripcion,
				'ind_estado' => $estado,
				'ind_motivo' => $ind_motivo,
				'cod_memo' => $cod_memo,
				'pk_num_salida' =>  $pkNumSalida
			);
			echo json_encode($arrayPost);
			exit;
		}
		if (!empty($pkNumSalida)) {
			$metodo = 1;
			$form = $this->atSalidaModelo->metVerSalida($pkNumSalida, $metodo);
			$pkNumPasillo= $form['pk_num_pasillo'];
			$pkNumAlmacen= $form['pk_num_almacen'];
			$pkNumDependencia = $form['pk_num_dependencia'];
			$almacen=$this->atSalidaModelo->metListarAlmacen();
			$this->atVista->assign('almacen',$almacen);
			$pasillo=$this->atSalidaModelo->metBuscarPasillo($pkNumAlmacen);
			$this->atVista->assign('pasillo',$pasillo);
			$estante=$this->atSalidaModelo->metBuscarEstante($pkNumPasillo);
			$this->atVista->assign('estante',$estante);
			$dependencia = $this->atSalidaModelo->metListarDependencia();
			$this->atVista->assign('dependencia', $dependencia);
			$documento = $this->atSalidaModelo->metListarDocumento($pkNumDependencia);
			$this->atVista->assign('documento', $documento);
			$obtenerUsuario = $this->atSalidaModelo->metUsuario($usuario, $metodo);
			$this->atVista->assign('funcionario',$obtenerUsuario);
			$pkNumRegistroDocumento = $form['fk_adb001_num_registro_documento'];
			$registro = $this->atSalidaModelo->metVisualizarRegistro($pkNumRegistroDocumento, 1);
			$this->atVista->assign('registro',$registro);
			$this->atVista->assign('form', $form);
            $numFlagArchivo = $registro['num_flag_archivo'];
            if($numFlagArchivo==1){
                $archivo = $this->atSalidaModelo->metBuscarArchivo($pkNumRegistroDocumento);
                $this->atVista->assign('archivo', $archivo);
                $ruta = 'publico/imagenes/modAD/';
                $explodeArchivo = explode(".", $archivo['ind_nombre_archivo']);
                $extension = $explodeArchivo[1];
                $rutaArchivo = array(
                    'ruta' => $ruta,
                    'extension' => $extension
                );
                $this->atVista->assign('ruta', $rutaArchivo);
			}
			$this->atVista->metRenderizar('editar', 'modales');
		}
	}

	// Método que permite cargar el formulario de busqueda de salidas por rango de fecha
	public function metBuscarSalida()
	{
		$complementosCss = array(
			'bootstrap-datepicker/datepicker',
		);
		$complementosJs = array(
			'bootstrap-datepicker/bootstrap-datepicker'
		);
		$js = array(
			'materialSiace/App',
			'materialSiace/core/demo/DemoFormComponents'
		);

		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$this->atVista->metRenderizar('buscarSalida', 'modales');
	}

	// Método que permite validar la fechas
	public function metVerificarFechas()
	{
		$fechaAux = explode("/",$_POST['fechaInicio']);
		$fechaInicio = $fechaAux[2].'-'.$fechaAux[1].'-'.$fechaAux[0];
		$fechaAux = explode("/",$_POST['fechaFin']);
		$fechaFin = $fechaAux[2].'-'.$fechaAux[1].'-'.$fechaAux[0];
		if(strtotime($fechaFin) >= strtotime($fechaInicio)) {
			echo json_encode(true);
		} else {
			echo json_encode(false);
		}
	}

	// Método que permite ver el reporte de salida de los documentos en un rango de fecha
	public function metReporteSalida($fechaInicio, $fechaFin)
	{
		$this->metObtenerLibreria('cabeceraMaestros','modAD');
		$pdf= new pdfSalida('P','mm','Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true,10);
		$pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont('Arial','',8);
		$listarSalida = $this->atSalidaModelo->metBuscarSalida($fechaInicio, $fechaFin);
		$pdf->SetWidths(array(12.5, 12.5, 20, 45, 25, 20, 60));
		foreach ($listarSalida as $listarSalida) {
			$estado = $listarSalida['ind_estado'];
			if($estado=='PR'){
				$valorEstado = 'PREPARADO';
			} 
			if($estado=='AP'){
				$valorEstado = 'APROBADO';
			} 
			if($estado=='AN'){
				$valorEstado = 'ANULADO';
			} 
			$pdf->Row(array(
				$listarSalida['pk_num_salida'],
				$listarSalida['num_registro'],
				$valorEstado,
				utf8_decode($listarSalida['ind_descripcion']),
				$listarSalida['cod_memo'],
				$listarSalida['fecha_salida'],
				utf8_decode($listarSalida['ind_motivo'])
			), 5);
		}
		$pdf->Output();
	}

	// Método que permite ver el reporte de una salida en específica
	public function metSalida()
	{
		$pkNumSalida = $_GET['pk_num_salida'];
		$usuario = Session::metObtener('idUsuario');
		$obtenerUsuario= $this->atSalidaModelo->metUsuarioReporte($usuario);
		$this->metObtenerLibreria('cabeceraMaestros','modAD');
		$pdf= new pdfVerSalida('P','mm','Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetMargins(18, 25 , 30);
		$pdf->SetAutoPageBreak(true,10);
		$pdf->SetFont('Arial','',10);
		$pdf->SetX(18);
		$pdf->Cell(185, 5,utf8_decode('Elaborado por: '.$obtenerUsuario['ind_nombre1'].' '.$obtenerUsuario['ind_apellido1']), 0, 0, 'L', 0);
		$pdf->Ln();$pdf->Ln();
		$verSalida = $this->atSalidaModelo->metVerSalida($pkNumSalida, 2);
		foreach ($verSalida as $verSalida) {
			$pdf->Cell(185, 5,utf8_decode('Datos de Almacenamiento'), 0, 0, 'C', 0);
			$pdf->Ln();$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Almacén: '.$verSalida['ind_descripcion_almacen']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Pasillo: '.$verSalida['ind_pasillo']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Estante: '.$verSalida['ind_descripcion_estante']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Dependencia: '.$verSalida['ind_dependencia']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Caja: '.$verSalida['pk_num_caja'].' - '.$verSalida['ind_descripcion_caja']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Datos del documento'), 0, 0, 'C', 0);
			$pdf->Ln();$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Número de Registro del Documento: '.$verSalida['num_registro']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Documento: '.$verSalida['ind_documento']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Fecha del Documento: '.$verSalida['fecha_documento']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Descripción: '.$verSalida['ind_descripcion']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Datos de Salida'), 0, 0, 'C', 0);
			$pdf->Ln();$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('N° de salida: '.$verSalida['pk_num_salida']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('N° de Memorándum: '.$verSalida['cod_memo']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Fecha de salida: '.$verSalida['fecha_salida']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->MultiCell(185, 5, utf8_decode('Motivo: '.$verSalida['ind_motivo']),0);
		}
		$pdf->Output();
	}

	// Método que permite listar las salidas
	public function metListarSalida()
	{
		$valorSalida = $_GET['valor'];
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'bootstrap-datepicker/datepicker'
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$dateRage[] = 'moment/moment.min';
		$datepickerJs[] = 'bootstrap-datepicker/bootstrap-datepicker';

		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($datepickerJs);
		$this->atVista->metCargarJsComplemento($dateRage);
		$this->atVista->metCargarJs($js);
		$validarjs = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validarjs);
	   if($valorSalida!='Todos') {
		   $metodoOrdenar = 2;
	   } else {
		   $metodoOrdenar = 1;
	   }
        $usuario = Session::metObtener('idUsuario');
        $idAplicacion = 14;
		$this->atVista->assign('_PruebaPost', $this->atSalidaModelo->metListarSalida($metodoOrdenar, $valorSalida, $usuario, $idAplicacion));
		$this->atVista->metRenderizar('listado');
	}

	// Método que permite verificar si hay salidas para las fechas dadas
	public function metBuscarFecha()
	{
		$fecha_inicio = $_POST['fechaInicio'];
		$fechaInicioExplode = explode ("/", $fecha_inicio);
		$fechaInicio = $fechaInicioExplode[2].'-'.$fechaInicioExplode[1].'-'.$fechaInicioExplode[0];
		$fecha_fin = $_POST['fechaFin'];
		$fechaFinExplode = explode ("/", $fecha_fin);
		$fechaFin = $fechaFinExplode[2].'-'.$fechaFinExplode[1].'-'.$fechaFinExplode[0];
		$validarFechas = $this->atSalidaModelo->metBuscarFecha($fechaInicio, $fechaFin);
		$total = count($validarFechas);
		if ($total>0) {
			$this->metReporteSalida($fechaInicio, $fechaFin);
		} else {
			echo '
	        <link rel="stylesheet" type="text/css" href="'.BASE_URL.'publico'.DS.'complementos'.DS.'bootstrap'.DS.'bootstrap_v3.3.4.css">
            <br/><div class="alert alert-danger" align="center">No se encuentran resultados para las fechas dadas</div>';
		}

	}

	// Método que permite ver el estatus de una salida
	public function metEstatus() 
	{
		$pkNumSalida = $_POST['pk_num_salida'];
		$valor = $_POST['valor'];
		$usuario = Session::metObtener('idUsuario');
		$empleado = $this->atSalidaModelo->metUsuario($usuario, 1);
		$idEmpleado = $empleado['pk_num_empleado'];
		$fecha_hora = date('Y-m-d H:i:s');
		$this->atSalidaModelo->metCambiarEstatus($pkNumSalida, $valor, $idEmpleado, $fecha_hora);
		$verSalida = $this->atSalidaModelo->metVerSalida($pkNumSalida, 1);
		$num_registro = $verSalida['num_registro'];
		$ind_documento = $verSalida['ind_documento'];
		$ind_descripcion = $verSalida['ind_descripcion'];
		$estadoPrevio = $verSalida['ind_estado'];
		if($estadoPrevio=='PR'){
			$estado = 'PREPARADO';
		} 
		if ($estadoPrevio=='AP') {
			$estado = 'APROBADO';
		}
		if ($estadoPrevio=='AN') {
			$estado = 'ANULADO';
		}
		$arrayPost = array(
			'num_registro' => $num_registro,
			'ind_documento' => $ind_documento,
			'ind_descripcion' => $ind_descripcion,
			'ind_estado' => $estado,
			'pk_num_salida' =>  $pkNumSalida
		);
		echo json_encode($arrayPost);
	}

	// Método que permite verificar si un documento tiene una solicitud de salida previa
	public function metVerificarSalida()
	{
		$pk_num_registro_documento = $_POST['pk_num_registro_documento'];
		$salidaExiste = $this->atSalidaModelo->metVerificarSalida($pk_num_registro_documento);
		echo json_encode($salidaExiste);
	}

	// Método que permite ver un expediente en particular
	public function metVerExpediente()
	{
		$num_registro = $_GET['num_registro'];
		$this->metObtenerLibreria('cabeceraMaestros','modAD');
		$path = ROOT.'publico/imagenes/modAD/'.$num_registro.'/';
		$pdf= new pdfCargarRegistro('P','mm','Letter');
		$pdf->AliasNbPages();
		$pdf->SetMargins(18, 25 , 30);
		$pdf->SetAutoPageBreak(true,10);
		$pdf->Ln();
		$dir = scandir($path);
		for($i=2;$i<count($dir);$i++) {
			$pdf->AddPage();
			$pdf->Image($path.$dir[$i],1,10,200);
			$pdf->Ln();
		}
		$pdf->Output();
	}
	
	// Método que permite verificar si un archivo tiene una entrada pendiente al almacén
	public function metVerificarEntrada()
	{
		$pkNumRegistroDocumento = $_POST['pk_num_registro_documento'];
		$verificar = $this->atSalidaModelo->metVerificarEntradaRegistro($pkNumRegistroDocumento);	
		$totalVerificar = count($verificar);
		$total = array(
			'total' => $totalVerificar
		);
		echo json_encode($total);
	}
}
?>
