<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Consumo de Red
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de registros de documentos
class registroArchivoControlador extends Controlador 
{
	private $atArchivoModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		#se carga el Modelo Equipo.
		$this->atArchivoModelo = $this->metCargarModelo('registroArchivo');
	}

	// Método Index del controlador
	public function metIndex(){
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'bootstrap-datepicker/datepicker',
			'dropzone//downloads/css/dropzone'
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$datepickerJs[] = 'bootstrap-datepicker/bootstrap-datepicker';
		$dropzoneJs[]= 'dropzone/downloads/dropzone.min';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($datepickerJs);
		$this->atVista->metCargarJsComplemento($dropzoneJs);
		$this->atVista->metCargarJs($js);
		$validarjs= array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validarjs);
        $usuario = Session::metObtener('idUsuario');
        $idAplicacion = 14;
		$this->atVista->assign('_PruebaPost', $this->atArchivoModelo->metListarArchivo($usuario, $idAplicacion));
		$this->atVista->metRenderizar('listado');
	}

	public function metBuscarNumero() {

        $anio = $this->metObtenerInt('anio');
        $numRegistro = $this->atArchivoModelo->metObtenerNumRegistro($anio);
        $registroNumero = $numRegistro['maximo'];
        if($registroNumero>0) {
            $arrayPost['num_registro'] = $registroNumero + 1;
        } else {
            $arrayPost['num_registro'] = 1;
        }
        echo json_encode($arrayPost);
        exit;
	}

	// Método que permite registrar un nuevo documento
	public function metNuevoArchivo()
	{
		$js = array(
			'materialSiace/core/demo/DemoFormWizard',
			'materialSiace/core/demo/DemoFormComponents',
            'Aplicacion/appFunciones',
		);
		$complementosJs = array(
			'select2/select2.min',
			'wizard/jquery.bootstrap.wizard.min',
			'bootstrap-datepicker/bootstrap-datepicker',
		);

		$complementosCss = array(
			'wizard/wizardfa6c',
			'jquery-validation/dist/site-demo',
			'select2/select201ef',
		);

		$this->atVista->metCargarJs($js);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarCssComplemento($complementosCss);
		$valido = $this->metObtenerInt('valido');
        $pkNumRegistro = $this->metObtenerInt('pk_num_registro_documento');
        $ver = $this->metObtenerInt('ver');
		$nuevo = $this->metObtenerInt('nuevo');
        $fec_anio = date('Y');
		if (isset($valido) && $valido == 1) {
			$this->metValidarToken();
            $int = $this->metValidarFormArrayDatos('form','int');
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum');
            $formula = $this->metValidarFormArrayDatos('form','formula');
            if ($int != null && $alphaNum == null && $formula==null) {
                $validacion = $int;
            } elseif ($int == null && $alphaNum != null && $formula==null) {
                $validacion = $alphaNum;
            } elseif ($int == null && $alphaNum == null && $formula!=null) {
                $validacion = $formula;
            } elseif ($int != null && $alphaNum != null && $formula==null) {
                $validacion = array_merge($alphaNum, $int);
            } elseif ($int != null && $alphaNum == null && $formula!=null) {
                $validacion = array_merge($int, $formula);
            } elseif ($int == null && $alphaNum != null && $formula!=null) {
                $validacion = array_merge($alphaNum, $formula);
            } else {
                $validacion = array_merge($int, $alphaNum, $formula);
            }
            $validacion['fec_documento'] = 'error';
            $txt = $this->metObtenerTexto('form','txt');
            if($txt['fec_documento']!=null){
                $validacion['fec_documento'] = $txt['fec_documento'];
			}


            if(!isset($validacion['cod_memo'])) {
                $validacion['cod_memo'] = 's/n';
			}

            if(!isset($validacion['verificar'])) {
                $validacion['verificar'] = '0';
			}
            if($validacion['pk_num_caja']=='error') {
                $validacion['pk_num_caja'] = null;
			}

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            $validacion['pk_num_registro_documento'] = $pkNumRegistro;
            $fec_anio = $validacion['fec_anio'];

            if($pkNumRegistro==0) {
                $id = $this->atArchivoModelo->metGuardarArchivo($validacion);
                $validacion['status']='nuevo';
                $validacion['mensaje']='Guardado';
			} else {

                $id = $this->atArchivoModelo->metEditarArchivo($validacion);
                $validacion['status']='editar';
                $validacion['mensaje']='Modificado';
			}

            $validacion['id'] = $id;
            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }

            $ruta = ROOT.'publico'.DS.'imagenes'.DS.'modAD'.DS.''.$fec_anio.'-'.$validacion['num_registro'].DS;
            if(is_dir($ruta)){
                $archivos = array_diff(scandir($ruta), array('.','..'));
                foreach ($archivos as $archivo) {
                    $this->atArchivoModelo->metEliminarArchivoDocumento($id,$archivo);
                    $this->atArchivoModelo->metGuardarArchivoDocumento($id, $archivo);
                }
            }

            $ultimo = $this->atArchivoModelo->metObtenerNumRegistro($fec_anio);
            $ultimoId = $ultimo['maximo'];
            $ultimoId = $ultimoId + 1;
            $path = ROOT.'publico/imagenes/modAD/'.$ultimoId;
            if(is_dir($path)){
                $files = array_diff(scandir($path), array('.','..'));
                foreach ($files as $file) {
                    unlink("$path/$file");
                }
                rmdir($path);
            }

			echo json_encode($validacion);
			exit;

		}

        if (!empty($pkNumRegistro)) {
            $form = $this->atArchivoModelo->metVisualizarRegistro($pkNumRegistro, 1);

            $this->atVista->assign('form', $form);
            $numFlagArchivo = $form['num_flag_archivo'];

            if($numFlagArchivo==1){
                $archivoPdf = $this->atArchivoModelo->metBuscarArchivo($pkNumRegistro,2);
                $this->atVista->assign('archivoPdf', $archivoPdf);
			}
            $archivosImg = $this->atArchivoModelo->metBuscarArchivo($pkNumRegistro,1);
            $this->atVista->assign('archivosImg', $archivosImg);

            $this->atVista->assign('ruta', './publico/imagenes/modAD/'.$form['fec_anio'].'-'.$form['num_registro']);
            $this->atVista->assign('pk_num_registro_documento', $pkNumRegistro);
        }else{
            $this->atVista->assign('registro', 1);
		}
		$almacen=$this->atArchivoModelo->metListarAlmacen();
        $this->atVista->assign('almacen',$almacen);
        $dependencia = $this->atArchivoModelo->metListarDependencia();
        $this->atVista->assign('dependencia',$dependencia);

        $this->atVista->assign('nuevo', $nuevo);
        $this->atVista->assign('ver', $ver);
		$this->atVista->metRenderizar('nuevo', 'modales');

	}
	
	public function metSubirArchivo()
	{
        $numRegistro = $_GET['num_registro'];
        $fec_anio = $_GET['fec_anio'];
        $carpeta = ROOT.'publico'.DS.'imagenes'.DS.'modAD'.DS.$fec_anio.'-'.$numRegistro.DS;

        if(isset($_FILES['archivo']['name'])){
            foreach (glob($carpeta."*.pdf") as $nombre_archivo) {
                $borrar = unlink($nombre_archivo);
            }
            $id=$this->atArchivoModelo->metEliminarArchivoPorAnioNumRegistro($fec_anio,$numRegistro);
            $nombreArchivo = $_FILES['archivo']['name'];
            if(!is_dir($carpeta)) {
                mkdir($carpeta, 0777);
            }// si no es un directorio, entonces se crea
            move_uploaded_file($_FILES['archivo']['tmp_name'], ROOT.'publico/imagenes/modAD/'.$fec_anio.'-'.$numRegistro.'/' . $_FILES['archivo']['name']);

            chmod(ROOT.'publico/imagenes/modAD/'.$fec_anio.'-'.$numRegistro.'/' . $_FILES['archivo']['name'], 0777);
            $buscarIdentificador = $this->atArchivoModelo->metBuscarIdentificador($numRegistro,$fec_anio);
            $pkNumRegistroDocumento = $buscarIdentificador['pk_num_registro_documento'];
            $this->atArchivoModelo->metGuardarArchivoDocumento($pkNumRegistroDocumento, $nombreArchivo);
		}
		// Guardo en la tabla los datos del archivo
	}

	// Método que permite cargar un archivo en editar
	public function metSubirArchivoEditar()
	{
		$numRegistro = $_GET['num_registro'];
		$nombreArchivo = $_FILES['archivo']['name'];
		if(!empty($nombreArchivo)){
			// Guardo el nombre del nuevo archivo en la base de datos
			$buscarIdentificador = $this->atArchivoModelo->metBuscarIdentificador($numRegistro);
			$pkNumRegistroDocumento = $buscarIdentificador['pk_num_registro_documento'];
			$this->atArchivoModelo->metEditarArchivoCargar($pkNumRegistroDocumento, $nombreArchivo);
			$ruta = ROOT.'publico'.DS.'imagenes'.DS.'modAD'.DS.''.$numRegistro.DS;
			if(is_dir($ruta)){
				$archivos = array_diff(scandir($ruta), array('.','..'));
				foreach ($archivos as $archivo) {
					unlink($ruta.$archivo);
				}
			}
			move_uploaded_file($_FILES['archivo']['tmp_name'], ROOT.'publico/imagenes/modAD/'.$numRegistro.'/' . $_FILES['archivo']['name']);
			chmod(ROOT.'publico/imagenes/modAD/'.$numRegistro.'/' . $_FILES['archivo']['name'], 0777);
		}
		// Elimino el archivo de la carpeta
	}

	// Método que permite listar los pasillos de un almacén en específico
	public function metBuscarPasillo()
	{
		$pkNumAlmacen = $this->metObtenerInt('idAlmacen');
		$pkNumPasillo = $this->metObtenerInt('idPasillo');
		$listarPasillo=$this->atArchivoModelo->metBuscarPasillo($pkNumAlmacen);

		$a = '<option value="" disabled selected>&nbsp;</option>';
		foreach($listarPasillo as $listarPasillo){
			if($pkNumPasillo==$listarPasillo['pk_num_pasillo'])
				$a .='<option value="'.$listarPasillo['pk_num_pasillo'].'" selected>'.$listarPasillo['ind_pasillo'].' '.$listarPasillo['ind_descripcion_pasillo'].'</option>';
			else
                $a .='<option value="'.$listarPasillo['pk_num_pasillo'].'">'.$listarPasillo['ind_pasillo'].'</option>';
		}
		echo $a;
	}

	// Método que permite listar los estantes de un pasillo
	public function metBuscarEstante()
	{
		$pkNumPasillo = $this->metObtenerInt('idPasillo');
        $pkNumEstante = $this->metObtenerInt('idEstante');
		$listarEstante=$this->atArchivoModelo->metBuscarEstante($pkNumPasillo);

		$a = '<option value="" disabled selected>&nbsp;</option>';
		foreach($listarEstante as $listarEstante){
            if($pkNumEstante==$listarEstante['pk_num_estante'])
                $a .='<option value="'.$listarEstante['pk_num_estante'].'" selected>'.$listarEstante['ind_descripcion_estante'].'</option>';
            else
				$a .='<option value="'.$listarEstante['pk_num_estante'].'">'.$listarEstante['ind_descripcion_estante'].'</option>';
		}
		echo $a;
	}

	// Método que permite listar las dependencias de la contraloría
	public function metBuscarDependencia()
	{
		$pkNumEstante = $this->metObtenerInt('idEstante');
		$pkNumDependencia = $this->metObtenerInt('idDependencia');
        $usuario = Session::metObtener('idUsuario');
        $idAplicacion = 14;
		$listarDependencia=$this->atArchivoModelo->metConsultarSeguridadAlterna(1, $idAplicacion);
        $a = '<option value="" disabled selected>&nbsp;</option>';
		foreach($listarDependencia as $listarDependencia){
            if($pkNumDependencia==$listarDependencia['pk_num_dependencia'])
                $a .='<option value="'.$listarDependencia['pk_num_dependencia'].'" selected>'.$listarDependencia['ind_dependencia'].'</option>';
            else
				$a .='<option value="'.$listarDependencia['pk_num_dependencia'].'">'.$listarDependencia['ind_dependencia'].'</option>';
		}
		echo $a;
	}

	// Método que permite listar los tipos de documentos
	public function metTipoDocumento()
	{
		$pkNumDependencia = $_POST['idDependencia'];
		$pkNumEstante = $_POST['idEstante'];//$this->metObtenerInt('idEstante');
		$listarDocumento=$this->atArchivoModelo->metListarDocumento($pkNumDependencia);
        $a = '<option value="" disabled selected>&nbsp;</option>';
		foreach($listarDocumento as $listarDocumento){
			$a .='<option value="'.$listarDocumento['pk_num_documento'].'">'.$listarDocumento['ind_descripcion_documento'].'</option>';
		}
		echo $a;
	}

	// Método que permite listar las cajas de los estantes
	public function metBuscarCaja()
	{
		$pkNumEstante = $this->metObtenerInt('idEstante');
        $pkNumCaja = $this->metObtenerInt('idCaja');
		$listarCaja=$this->atArchivoModelo->metListarCaja($pkNumEstante);
        $a = '<option value="">&nbsp;</option>';
		foreach($listarCaja as $listarCaja){
            if($pkNumCaja==$listarCaja['pk_num_caja'])
	            $a .='<option value="'.$listarCaja['pk_num_caja'].'" selected>'.$listarCaja['pk_num_caja'].'-'.$listarCaja['ind_descripcion_caja'].'</option>';
            else
				$a .='<option value="'.$listarCaja['pk_num_caja'].'">'.$listarCaja['pk_num_caja'].'-'.$listarCaja['ind_descripcion_caja'].'</option>';
		}
		echo $a;
	}
	// Método que permite listar las cajas de los estantes
	public function metBuscarCarpeta()
	{
		$pkNumEstante = $this->metObtenerInt('idEstante');
        $pkNumCarpeta = $this->metObtenerInt('idCarpeta');
		$listarCarpeta=$this->atArchivoModelo->metListarCarpeta($pkNumEstante);
        $a = '<option value="" disabled selected>&nbsp;</option>';
		foreach($listarCarpeta as $listarCaja){
            if($pkNumCarpeta==$listarCaja['pk_num_carpeta'])
                $a .='<option value="'.$listarCaja['pk_num_carpeta'].'" selected>'.$listarCaja['pk_num_carpeta'].'-'.$listarCaja['ind_descripcion_carpeta'].'</option>';
            else
				$a .='<option value="'.$listarCaja['pk_num_carpeta'].'">'.$listarCaja['pk_num_carpeta'].'-'.$listarCaja['ind_descripcion_carpeta'].'</option>';
		}
		echo $a;
	}

	// Método que permite listar los registros de una caja
	public function metBuscarRegistro()
	{
		$pkNumCaja = $this->metObtenerInt('idCaja');
		$listarRegistros=$this->atSalidaModelo->metListarRegistros($pkNumCaja);
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_registro_documento" id="pk_num_registro_documento"> <option value="" disabled selected>&nbsp;</option>';
		foreach($listarRegistros as $listarRegistros){
			$a .='<option value="'.$listarRegistros['pk_num_registro_documento'].'">'.$listarRegistros['num_registro'].'-'.$listarRegistros['ind_documento'].'</option>';
		}
		$a .= '</select><label for="pk_num_registro_documento"><i class="glyphicon glyphicon-briefcase"></i> Documento</label></div>';
		echo $a;
	}

	// Método que permite mostrar el archivo digitalizado
	public function metExpediente($idRegistro,$fec_anio,$verificar = 0,$nombre = false)
	{
		$this->metObtenerLibreria('cabeceraMaestros','modAD');
		if($idRegistro!="'a'") {
            $form = $this->atArchivoModelo->metVisualizarRegistro($idRegistro, 1);
			$ultimoMaximo = $form['fec_anio'].'-'.$form['num_registro'];
		} else {
			$ultimo = $this->atArchivoModelo->metObtenerNumRegistro($fec_anio);
			$ultimoId = $ultimo['maximo'];
			$ultimoMaximo = $fec_anio.'-'.($ultimoId + 1);
		}

		$path = ROOT.'publico'.DS.'imagenes'.DS.'modAD'.DS.$ultimoMaximo.DS;
        $pdf= new pdfCargarRegistro('P','mm','Letter');
		if(is_readable($path)){

			if($verificar==0) {
                $pdf->AliasNbPages();
                $pdf->SetMargins(18, 25 , 30);
                $pdf->SetAutoPageBreak(true,10);
                $pdf->Ln();
                $dir = scandir($path);
                for($i=2;$i<count($dir);$i++) {
                    $trozos = explode(".", $dir[$i]);
                    $extension = end($trozos);
                    if($extension!='pdf') {
                        $pdf->AddPage();
                        $pdf->Image($path.$dir[$i],1,10,200);
                        $pdf->Ln();
					}
                }
                $pdf->Output();
			} else {
                $mi_pdf = $path.$nombre;
                header('Content-type: application/pdf');
                header('Content-Disposition: inline; filename="'.$nombre.'"');
                readfile($mi_pdf);
			}
        }
	}

	// Método que permite ver un expediente en particular
	public function metVerExpediente()
	{

		$num_registro = $_GET['num_registro'];
		$this->metObtenerLibreria('cabeceraMaestros','modAD');
		$path = ROOT.'publico/imagenes/modAD/'.$num_registro.'/';
		$pdf= new pdfCargarRegistro('P','mm','Letter');
		$pdf->AliasNbPages();
		$pdf->SetMargins(18, 25 , 30);
		$pdf->SetAutoPageBreak(true,10);
		$pdf->Ln();
		$dir = scandir($path);
		for($i=2;$i<count($dir);$i++) {
			$pdf->AddPage();
			$pdf->Image($path.$dir[$i],1,10,200);
			$pdf->Ln();
		}
		$pdf->Output();
	}

	// Método que permite cargar las imágenes del archivo digitalizado
	public function metCargarImagen($pkNumRegistro = false,$anio)
	{
		//se procesa la ruta hacia la carpeta del archivo digital
		if($pkNumRegistro!='a') {
            $form = $this->atArchivoModelo->metVisualizarRegistro($pkNumRegistro, 1);
            $ultimoId = $form['fec_anio'].'-'.$form['num_registro'];
		} else {
            $ultimo = $this->atArchivoModelo->metObtenerNumRegistro($anio);
            $ultimoId = $ultimo['maximo'];
            $ultimoId += 1;
            $ultimoId = $anio.'-'.$ultimoId;
		}
		$path = ROOT.'publico'.DS.'imagenes'.DS.'modAD'.DS.$ultimoId.DS;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if (isset($_POST["cargarImagenes"]) && $_POST["cargarImagenes"] == 1) {
				$id = $_POST['idDocumento'];
				$ruta = ROOT.'publico'.DS.'imagenes'.DS.'modAD'.DS.$id.DS;
				$result  = array();
				$files = scandir($ruta);                 //1
				if (false!==$files ) {
					foreach ( $files as $file ) {
						if ( '.'!=$file && '..'!=$file) {       //2
							$obj['name'] = $file;
							$obj['size'] = filesize($ruta.$file);
							$result[] = $obj;
						}
					}
				}
				header('Content-type: text/json');              //3
				header('Content-type: application/json');
				echo json_encode($result);
			} else if (isset($_POST["delete"]) && $_POST["delete"] == true) {
					$name = $_POST["filename"];
					$path = './publico'.DS.'imagenes'.DS.'modAD'.DS.$ultimoId.DS;

					if (file_exists($path.$name)) {
						unlink($path.$name);
						echo json_encode(array("res" => true));
                        $this->atArchivoModelo->metEliminarArchivoDocumento($pkNumRegistro,$name);
					} else {
						echo json_encode(array("res" => false));
					}
				} else	{ // para guardar las imagenes
					//se obtienen los datos de la imagen
					$file = $_FILES["file"]["name"];
					if(!is_dir($path)) // si no es un directorio, entonces se crea
						mkdir($path, 0777);
					//se hace el renombrado de la imagen
					$archivos = scandir($path);

					$cuenta = 1; // contador de imagenes del documento
					for ($i=2;$i<count($archivos);$i++) {
						$ext = explode(".",$archivos[$i]);
						if($ext[1] == 'jpg' || $ext[1] == 'png' || $ext[1] == 'gif' || $ext[1] == 'jpeg')
							$cuenta += 1;
					}
					$path_parts = pathinfo("/$file");
					$extension = NULL;
					$extension = $path_parts['extension'];
					$nom_imagen = NULL;
					$nom_imagen = $file;

					if ($file && move_uploaded_file($_FILES["file"]["tmp_name"], $path.$file)) {
						var_dump($pkNumRegistro);
						var_dump($nom_imagen);
                        $this->atArchivoModelo->metGuardarArchivoDocumento($pkNumRegistro, $nom_imagen);
					}

				}
		}

	}

	// Método que permite cambiar la imagen cuando se edita
	public function metCambiarImagen()
	{
		$numRegistro = $_GET['num_registro'];
		$path = ROOT.'publico'.DS.'imagenes'.DS.'modAD'.DS.$numRegistro.DS;
		if(is_dir($path)){
			$archivos = array_diff(scandir($path), array('.','..'));
			$proximo = 1;
			foreach ($archivos as $archivo) {
				$x = explode(".",$archivo);
				if($x[0] != $proximo){
					break;
				}
				else{
					$proximo ++;
				}
			}
			$file = $_FILES["file"]["name"];
			$path_parts = pathinfo("/$file");
			$extension = NULL;
			$extension = $path_parts['extension'];
			$nom_imagen = NULL;
			$nom_imagen = $proximo.'.'.$extension;
			if($file && move_uploaded_file($_FILES["file"]["tmp_name"], $path.$nom_imagen)) {

			}
		}
	}

	// Método que permite eliminar los archivos registrados
	public function metEliminarArchivo()
	{
		$pkNumRegistro = $this->metObtenerInt('pk_num_registro_documento');
		$registro =$this->atArchivoModelo->metVisualizarRegistro($pkNumRegistro, 1);
		$numRegistro = $registro['num_registro'];
		$this->atArchivoModelo->metEliminarArchivo($pkNumRegistro);
		$arrayPost = array(
			'status' => 'OK',
			'pk_num_registro_documento' => $pkNumRegistro
		);

		$ruta = ROOT.'publico'.DS.'imagenes'.DS.'modAD'.DS.''.$numRegistro.DS;
		if(is_dir($ruta)){
			$archivos = array_diff(scandir($ruta), array('.','..'));
			foreach ($archivos as $archivo) {
				unlink($ruta.$archivo);
			}
			rmdir($ruta);
		}
		echo json_encode($arrayPost);
	}

	// Método que permite ver los archivos
	public function metVerArchivo()
	{
		$pkNumRegistro = $this->metObtenerInt('pk_num_registro_documento');
		$registro = $this->atArchivoModelo->metVisualizarRegistro($pkNumRegistro, 1);
		$this->atVista->assign('reg',$registro);
		$numFlagArchivo = $registro['num_flag_archivo'];

        if($numFlagArchivo==1){
            $archivo = $this->atArchivoModelo->metBuscarArchivo($pkNumRegistro);
            $this->atVista->assign('archivo', $archivo);
            $ruta = 'publico/imagenes/modAD/';
            $explodeArchivo = explode(".", $archivo['ind_nombre_archivo']);
            $extension = $explodeArchivo[1];
            $rutaArchivo = array(
                'ruta' => $ruta,
                'extension' => $extension
            );
            $this->atVista->assign('ruta', $rutaArchivo);
        }

	}

	// Método que permite editar los archivos
	public function metEditarArchivo()
	{
		$js = array(
			'materialSiace/core/demo/DemoFormWizard',
			'materialSiace/core/demo/DemoFormComponents'
		);
		$complementosJs = array(
			'wizard/jquery.bootstrap.wizard.min',
			'bootstrap-datepicker/bootstrap-datepicker',
		);
		$complementosCss = array(
			'wizard/wizardfa6c',
			'jquery-validation/dist/site-demo',
		);
        $usuario = Session::metObtener('idUsuario');
		$this->atVista->metCargarJs($js);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarCssComplemento($complementosCss);
		$pkNumRegistro = $this->metObtenerInt('pk_num_registro_documento');
		$valido = $this->metObtenerInt('valido');
		if (isset($valido) && $valido == 1) {
			$cod_memo = $_POST['cod_memo'];
			$ind_documento = $this->metObtenerAlphaNumerico('ind_documento');
			$fechaDocumento = $_POST['fec_documento'];
			$ind_descripcion = $this->metObtenerTexto('ind_descripcion');
			$ind_resumen = $this->metObtenerTexto('ind_resumen');
			$pk_num_caja = $this->metObtenerInt('pk_num_caja');
			$pk_num_empleado = $this->metObtenerInt('pk_num_empleado');
			$usuario = Session::metObtener('idUsuario');
			$fecha_hora = date('Y-m-d H:i:s');
			// Formato de fecha
			$fechaExplodeDocumento = explode("/", $fechaDocumento);
			$fec_documento = $fechaExplodeDocumento[2] . '-' . $fechaExplodeDocumento[1] . '-' . $fechaExplodeDocumento[0];
			$fecha_registro = date('d/m/Y');
			// Fin formato
			$this->metValidarToken();

			$this->atArchivoModelo->metEditarArchivo($cod_memo, $ind_documento, $ind_descripcion, $fec_documento, $ind_resumen, $pk_num_empleado, $pk_num_caja, $fecha_hora, $usuario, $pkNumRegistro);
			$verRegistro = $this->atArchivoModelo->metVisualizarRegistro($pkNumRegistro, 1);
			$num_registro = $verRegistro['num_registro'];
			$arrayPost = array(
				'num_registro' => $num_registro,
				'cod_memo' => $cod_memo,
				'fecha_registro' => $fecha_registro,
				'fecha_documento' => $fechaDocumento,
				'ind_descripcion' => $ind_descripcion,
				'pk_num_registro_documento' => $pkNumRegistro
			);
			echo json_encode($arrayPost);
			exit;
		}
		if (!empty($pkNumRegistro)) {
			$metodo=1;
			$form = $this->atArchivoModelo->metVisualizarRegistro($pkNumRegistro, $metodo);
			$form = array(
				'status' => 'modificar',
				'num_registro' => $form['num_registro'],
				'cod_memo' => $form['cod_memo'],
				'pk_num_almacen' => $form['pk_num_almacen'],
				'ind_descripcion_almacen' => $form['ind_descripcion_almacen'],
				'pk_num_pasillo' => $form['pk_num_pasillo'],
				'ind_pasillo' => $form['ind_pasillo'],
				'pk_num_estante' => $form['pk_num_estante'],
				'ind_descripcion_estante' => $form['ind_descripcion_estante'],
				'pk_num_dependencia'=> $form['pk_num_dependencia'],
				'ind_dependencia' => $form['ind_dependencia'],
				'pk_num_documento' => $form['pk_num_documento'],
				'ind_descripcion_documento' => $form['ind_descripcion_documento'],
				'pk_num_caja' => $form['pk_num_caja'],
				'ind_descripcion_caja' => $form['ind_descripcion_caja'],
				'num_registro' => $form['num_registro'],
				'fecha_documento' => $form['fecha_documento'],
				'fecha_registro' => $form['fecha_registro'],
				'pk_num_empleado' => $form['pk_num_empleado'],
				'ind_nombre1' => $form['ind_nombre1'],
                'ind_nombre2' => $form['ind_nombre2'],
				'ind_apellido1' => $form['ind_apellido1'],
                'ind_apellido2' => $form['ind_apellido2'],
				'ind_descripcion' => $form['ind_descripcion'],
				'ind_resumen' => $form['ind_resumen'],
				'pk_num_registro_documento' => $pkNumRegistro,
				'num_flag_archivo' => $form['num_flag_archivo']
			);
			$almacen = $this->atArchivoModelo->metListarAlmacen();
			$this->atVista->assign('almacen',$almacen);
			$pasillo = $this->atArchivoModelo->metBuscarPasillo($form['pk_num_almacen']);
			$this->atVista->assign('pasillo',$pasillo);
			$estante = $this->atArchivoModelo->metBuscarEstante($form['pk_num_pasillo']);
			$this->atVista->assign('estante',$estante);
            $idAplicacion = 14;
            $dependencia=$this->atArchivoModelo->metConsultarSeguridadAlterna(1, $idAplicacion);
			$this->atVista->assign('dependencia',$dependencia);
			$documento = $this->atArchivoModelo->metListarDocumento($form['pk_num_dependencia']);
			$this->atVista->assign('documento', $documento);
			$caja = $this->atArchivoModelo->metListarCaja($form['pk_num_estante']);
			$this->atVista->assign('caja',$caja);
			$empleado = $this->atArchivoModelo->metListarFuncionario();
			$this->atVista->assign('empleado',$empleado);
			$this->atVista->assign('form', $form);
            $numFlagArchivo = $form['num_flag_archivo'];

            if($numFlagArchivo==1){
                $archivo = $this->atArchivoModelo->metBuscarArchivo($pkNumRegistro);
                $this->atVista->assign('archivo', $archivo);
                $ruta = 'publico/imagenes/modAD/';
                $explodeArchivo = explode(".", $archivo['ind_nombre_archivo']);
                $extension = $explodeArchivo[1];
                $rutaArchivo = array(
                    'ruta' => $ruta,
                    'extension' => $extension
                );
                $this->atVista->assign('ruta', $rutaArchivo);
			}
			$this->atVista->metRenderizar('editar', 'modales');
		}
	}

	// Método que permite mostrar el reporte de un registro en particular
	public function metRegistro()
	{
		$pkNumRegistroDocumento = $_GET['pk_num_registro_documento'];
		$usuario = Session::metObtener('idUsuario');
		$obtenerUsuario= $this->atArchivoModelo->metUsuarioReporte($usuario);
		$this->metObtenerLibreria('cabeceraMaestros','modAD');
		$pdf= new pdfRegistroDocumento('P','mm','Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetMargins(18, 25 , 30);
		$pdf->SetAutoPageBreak(true,10);
		$pdf->SetFont('Arial','',10);
		$pdf->SetX(18);
		$pdf->Cell(185, 5,utf8_decode('Elaborado por: '.$obtenerUsuario['ind_nombre1'].' '.$obtenerUsuario['ind_apellido1']), 0, 0, 'L', 0);
		$pdf->Ln();$pdf->Ln();
		$verRegistro = $this->atArchivoModelo->metVisualizarRegistro($pkNumRegistroDocumento, 2);
		foreach ($verRegistro as $verRegistro)
		{
			$pdf->Cell(185, 5,utf8_decode('Datos de Almacenamiento'), 0, 0, 'C', 0);
			$pdf->Line(20,66,200,66);
			$pdf->Ln();$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Almacén: '.$verRegistro['ind_descripcion_almacen']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Pasillo: '.$verRegistro['ind_pasillo']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Estante: '.$verRegistro['ind_descripcion_estante']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Dependencia: '.$verRegistro['ind_dependencia']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Caja: '.$verRegistro['pk_num_caja'].' - '.$verRegistro['ind_descripcion_caja']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Datos de Registro'), 0, 0, 'C', 0);
			$pdf->Line(20,106,200,106);
			$pdf->Ln();$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Número de Registro del Documento: '.$verRegistro['num_registro']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Documento: '.$verRegistro['ind_documento']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Fecha del Documento: '.$verRegistro['fec_documento']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Remitente: '.$verRegistro['ind_nombre1'].' '.$verRegistro['ind_apellido1']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->Cell(185, 5,utf8_decode('Descripción: '.$verRegistro['ind_descripcion']), 0, 0, 'L', 0);$pdf->Ln();
			$pdf->MultiCell(185, 5, utf8_decode('Resumen: '.$verRegistro['ind_resumen']),0);
		}
		$pdf->Output();
	}

	// Método que permite ver el reporte de los archivos digitalizados
	public function metReporteRegistro()
	{
		$this->metObtenerLibreria('cabeceraMaestros','modAD');
		$pdf= new pdfRegistro('L','mm','Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true,10);
		$pdf->SetFont('Arial','',10);
        $usuario = Session::metObtener('idUsuario');
        $idAplicacion = 14;
		$listarArchivo= $this->atArchivoModelo->metListarArchivo($usuario, $idAplicacion);
		$pdf->SetWidths(array(32, 35, 108, 46, 41));
		foreach($listarArchivo as $listarArchivo) {
			$pdf->Row(array(
				utf8_decode($listarArchivo['num_registro']),
				utf8_decode($listarArchivo['ind_documento']),
				utf8_decode($listarArchivo['ind_descripcion']),
				$listarArchivo['fecha_documento'],
				$listarArchivo['fecha_registro']

			), 5);
			
		}
		$pdf->Output();
	}

    // Método que permite buscar un empleado
    public function metConsultarEmpleado()
    {
        $idAplicacion = 14; // id de la aplicacion de Archivo Digital
    	$usuario = Session::metObtener('idUsuario');
    	$listarEmpleado = $this->atArchivoModelo->metListarEmpleado($usuario, $idAplicacion);
        $this->atVista->assign('listarEmpleado', $listarEmpleado);
        $this->atVista->metRenderizar('buscarEmpleado', 'modales');
    }

    //Metodo que permite cargar un empleado a la vista de registro de empleado en la parte del remitente
	public function metCargarEmpleado()
	{
		$pkNumEmpleado = $_POST['pk_num_empleado'];
		$consultarEmpleado = $this->atArchivoModelo->metConsultarEmpleado($pkNumEmpleado);
		$datoEmpleado = array(
			'remitente' => $consultarEmpleado['ind_nombre1'].' '.$consultarEmpleado['ind_nombre2'].' '.$consultarEmpleado['ind_apellido1'].' '.$consultarEmpleado['ind_apellido2']
		);
		echo json_encode($datoEmpleado);
	}
}
?>
