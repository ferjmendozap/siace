<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Archivo Digital
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de estantes 
class estanteControlador extends Controlador
{
	private $atEstanteModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atEstanteModelo = $this->metCargarModelo('estante');
	}

	//Método Index del controlador
	public function metIndex()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJs($js);
		$validar=array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$this->atVista->assign('_PruebaPost', $this->atEstanteModelo->metListarEstante());
		$this->atVista->metRenderizar('listado');
	}

	// Método que permite registrar un estante
	public function metNuevoEstante()
	{
		$valido = $this->metObtenerInt('valido');
		if (isset($valido) && $valido == 1) {
			$this->metValidarToken();
			$pk_num_almacen = $this->metObtenerInt('pk_num_almacen');
			$pk_num_pasillo = $this->metObtenerInt('pk_num_pasillo');
			$ind_descripcion_estante = $this->metObtenerTexto('ind_descripcion_estante');
			$usuario = Session::metObtener('idUsuario');
			$fecha_hora = date('Y-m-d H:i:s');
			$pkNumEstante=$this->atEstanteModelo->metGuardarEstante($pk_num_pasillo, $ind_descripcion_estante, $fecha_hora ,$usuario);
			$ind_descripcion_almacen= $this->atEstanteModelo->metVerAlmacen($pk_num_almacen);
			$ind_pasillo= $this->atEstanteModelo->metVerPasillo($pk_num_pasillo);
			$arrayPost = array(
				'pk_num_estante' => $pkNumEstante,
				'ind_descripcion_estante'=> $ind_descripcion_estante,
				'ind_pasillo' => $ind_pasillo,
				'ind_descripcion_almacen' => $ind_descripcion_almacen
			);
			echo json_encode($arrayPost);
			exit;
		}
		$form = array(
			'status' => 'nuevo',
			'pk_num_estante' => null,
			'ind_descripcion_almacen' => null,
			'ind_pasillo' => null,
			'ind_descripcion_estante'=> null
		);
		$listarAlmacen=$this->atEstanteModelo->metListarAlmacen();
		$this->atVista->assign('almacen',$listarAlmacen);
		$this->atVista->assign('form', $form);
		$this->atVista->metRenderizar('nuevo', 'modales');
	}

	// Método que permite listar los pasillos de un almacén en particular
	public function metBuscarPasillo()
	{
		$pkNumAlmacen = $this->metObtenerInt('idAlmacen');
		$listarPasillo=$this->atEstanteModelo->metBuscarPasillo($pkNumAlmacen);

		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_pasillo" id="pk_num_pasillo"> <option value="" disabled selected>&nbsp;</option>';
		foreach($listarPasillo as $listarPasillo){
			$a .='<option value="'.$listarPasillo['pk_num_pasillo'].'">'.$listarPasillo['ind_pasillo'].'</option>';
		}
		$a .= '</select><label for="pk_num_pasillo"><i class="glyphicon glyphicon-th"></i> Pasillo</label></div>';
		echo $a;
	}

	// Método que permite visualizar el estante
	public function metVerEstante()
	{
		$pkNumEstante = $this->metObtenerInt('pk_num_estante');
		$metodo=2;
		$estante=$this->atEstanteModelo->metVerEstante($pkNumEstante, $metodo);
		$this->atVista->assign('estante',$estante);
		$this->atVista->metRenderizar('ver', 'modales');
	}

	// Método que permite eliminar el estante
	public function metEliminarEstante()
	{
		$pkNumEstante = $this->metObtenerInt('pk_num_estante');
		$this->atEstanteModelo->metEliminarEstante($pkNumEstante);
		$arrayPost = array(
			'status' => 'OK',
			'pk_num_estante' => $pkNumEstante
		);
		echo json_encode($arrayPost);
	}

	// Método que permite editar el estante
	public function metEditarEstante()
	{
		$pkNumEstante = $this->metObtenerInt('pk_num_estante');
		$valido = $this->metObtenerInt('valido');
		if (isset($valido) && $valido == 1) {
			$pk_num_almacen = $this->metObtenerInt('pk_num_almacen');
			$pk_num_pasillo = $this->metObtenerInt('pk_num_pasillo');
			$ind_descripcion_estante = $this->metObtenerTexto('ind_descripcion_estante');
			$this->metValidarToken();
			$usuario = Session::metObtener('idUsuario');
			$fecha_hora = date('Y-m-d H:i:s');
			$this->atEstanteModelo->metEditarEstante($ind_descripcion_estante, $pk_num_pasillo, $fecha_hora,$usuario, $pkNumEstante);
			$ind_descripcion_almacen= $this->atEstanteModelo->metVerAlmacen($pk_num_almacen);
			$ind_pasillo= $this->atEstanteModelo->metVerPasillo($pk_num_pasillo);
			$arrayPost = array(
				'pk_num_estante' => $pkNumEstante,
				'ind_pasillo' => $ind_pasillo,
				'ind_descripcion_almacen' => $ind_descripcion_almacen,
				'ind_descripcion_estante'=> $ind_descripcion_estante
			);
			echo json_encode($arrayPost);
			exit;
		}
		if (!empty($pkNumEstante)) {
			$metodo=1;
			$form = $this->atEstanteModelo->metVerEstante($pkNumEstante, $metodo);
			$almacen= $this->atEstanteModelo->metBuscarAlmacen($pkNumEstante);
			$pkNumAlmacen=$almacen['pk_num_almacen'];
			$nombreCompleto= $form['ind_nombre1'].' '.$form['ind_apellido1'];
			$form = array(
				'status' => 'modificar',
				'pk_num_almacen' => $form['pk_num_almacen'],
				'pk_num_pasillo' => $form['pk_num_pasillo'],
				'ind_descripcion_estante' => $form['ind_descripcion_estante'],
				'fecha' => $form['fecha'],
				'nombreCompleto' => $nombreCompleto,
				'pk_num_estante' => $pkNumEstante
			);
			$listarAlmacen=$this->atEstanteModelo->metListarAlmacen();
			$this->atVista->assign('almacen',$listarAlmacen);
			$listarPasillo=$this->atEstanteModelo->metListarPasillo($pkNumAlmacen);
			$this->atVista->assign('pasillo',$listarPasillo);
			$this->atVista->assign('form', $form);
			$this->atVista->metRenderizar('editar', 'modales');
		}
	}

	// Método que permite visualizar el reporte de estante
	public function metReporteEstante()
	{
		$this->metObtenerLibreria('cabeceraMaestros','modAD');
		$pdf= new pdfEstante('P','mm','Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetMargins(18, 25 , 30);
		$pdf->SetAutoPageBreak(true,10);
		$pdf->SetFont('Arial','',10);
		$pdf->SetX(18);
		$pdf->Ln();
		$estante= $this->atEstanteModelo->metListarReporte();
		$pdf->SetWidths(array(30, 45, 40, 70));
		foreach($estante as $estante) {
			$pdf->Row(array(
				$estante['pk_num_estante'],
				utf8_decode($estante['ind_descripcion_estante']),
				utf8_decode($estante['ind_pasillo']),
				utf8_decode($estante['ind_descripcion_almacen'])
			), 5);
		}
		$pdf->Output();
	}

	// Método que permite verificar la existencia de un estante
	public function metVerificarEstante()
	{
		$ind_descripcion_estante = $this->metObtenerTexto('ind_descripcion_estante');
		$pk_num_almacen = $this->metObtenerInt("pk_num_almacen");
		$pk_num_pasillo = $this->metObtenerInt("pk_num_pasillo");
		$pk_num_estante = $this->metObtenerInt("pk_num_estante");
		$EstanteExiste = $this->atEstanteModelo->metBuscarEstante($ind_descripcion_estante, $pk_num_almacen, $pk_num_pasillo, $pk_num_estante);
		echo json_encode(!$EstanteExiste);
	}
}
?>
