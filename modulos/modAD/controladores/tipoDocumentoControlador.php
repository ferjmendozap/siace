<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Archivo Digital
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de tipos de documento
class tipoDocumentoControlador extends Controlador 
{
	private $atDocumentoModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		#se carga el Modelo Equipo.
		$this->atDocumentoModelo = $this->metCargarModelo('tipoDocumento');
	}

	// Método index del controlador
	public function metIndex(){
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJs($js);
		$validar=array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$this->atVista->assign('_PruebaPost', $this->atDocumentoModelo->metListarDocumento());
		$this->atVista->metRenderizar('listado');
	}

	//Metodo que permite registrar un nuevo tipo de documento
	public function metNuevoDocumento()
	{
		$valido = $this->metObtenerInt('valido');
		if (isset($valido) && $valido == 1) {
			$this->metValidarToken();
			$ind_descripcion_documento = $this->metObtenerTexto('ind_descripcion_documento');
			$usuario = Session::metObtener('idUsuario');
			$fecha_hora = date('Y-m-d H:i:s');
			$pkNumDocumento=$this->atDocumentoModelo->metGuardarDocumento($ind_descripcion_documento, $fecha_hora ,$usuario);
			$arrayPost = array(
				'pk_num_documento' => $pkNumDocumento,
				'ind_descripcion_documento' => $ind_descripcion_documento,
				'pk_num_documento' => $pkNumDocumento
			);
			echo json_encode($arrayPost);
			exit;
		}
		$form = array(
			'status' => 'nuevo',
			'pk_num_documento' => null,
			'ind_descripcion_documento' => null
		);
		$this->atVista->assign('form', $form);
		$this->atVista->metRenderizar('nuevo', 'modales');
	}

	// Método que permite ver un tipo de documento
	public function metVerDocumento()
	{
		$pkNumDocumento = $this->metObtenerInt('pk_num_documento');
		$metodo= 2;
		$documento=$this->atDocumentoModelo->metVerDocumento($pkNumDocumento, $metodo);
		$this->atVista->assign('documento',$documento);
		$this->atVista->metRenderizar('ver', 'modales');
	}

	// Método que permite eliminar un tipo de documento
	public function metEliminarDocumento()
	{
		$pkNumDocumento = $this->metObtenerInt('pk_num_documento');
		$this->atDocumentoModelo->metEliminarDocumento($pkNumDocumento);
		$arrayPost = array(
			'status' => 'OK',
			'pk_num_documento' => $pkNumDocumento
		);
		echo json_encode($arrayPost);
	}

	//Método que permite editar un tipo de documento
	public function metEditarDocumento()
	{
		$pkNumDocumento = $this->metObtenerInt('pk_num_documento');
		$valido = $this->metObtenerInt('valido');
		if (isset($valido) && $valido == 1) {
			$ind_descripcion_documento = $this->metObtenerTexto('ind_descripcion_documento');
			$this->metValidarToken();
			$usuario = Session::metObtener('idUsuario');
			$fecha_hora = date('Y-m-d H:i:s');
			$this->atDocumentoModelo->metEditarDocumento($ind_descripcion_documento, $fecha_hora,$usuario, $pkNumDocumento);
			$arrayPost = array(
				'status' => 'modificar',
				'ind_descripcion_documento' => $ind_descripcion_documento,
				'pk_num_documento' =>  $pkNumDocumento
			);
			echo json_encode($arrayPost);
			exit;
		}
		if (!empty($pkNumDocumento)) {
			$metodo= 1;
			$form = $this->atDocumentoModelo->metVerDocumento($pkNumDocumento, $metodo);
			$nombreCompleto= $form['ind_nombre1'].' '.$form['ind_apellido1'];
			$form = array(
				'status' => 'modificar',
				'pk_num_documento' => $form['pk_num_documento'],
				'ind_descripcion_documento' => $form['ind_descripcion_documento'],
				'fecha' => $form['fecha'],
				'nombreCompleto' => $nombreCompleto,
				'pk_num_documento' => $pkNumDocumento
			);
			$this->atVista->assign('form', $form);
			$this->atVista->metRenderizar('editar', 'modales');
		}
	}

	// Método que permite ver el reporte de los tipos de documentos registrados
	public function metReporteDocumento()
	{
		$this->metObtenerLibreria('cabeceraMaestros','modAD');
		$pdf= new pdfDocumento('P','mm','Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetMargins(18, 25 , 30);
		$pdf->SetAutoPageBreak(true,10);
		$pdf->SetFont('Arial','',10);
		$pdf->SetX(18);
		$documento= $this->atDocumentoModelo->metListarDocumento();
		$pdf->SetWidths(array(40, 145));
		foreach ($documento as $documento) {
			$pdf->Row(array(
				utf8_decode($documento['pk_num_documento']),
				utf8_decode($documento['ind_descripcion_documento'])
			), 5);
			$pdf->Ln();
		}
		$pdf->Output();
	}

	// Método que verifica la existencia de un tipo de documento
	public function metVerificarDescripcionDocumento()
	{
		$ind_descripcion_documento = $this->metObtenerAlphaNumerico("ind_descripcion_documento");
		$pk_num_documento = $this->metObtenerInt("pk_num_documento");
		$DescripcionExiste = $this->atDocumentoModelo->metBuscarDescripcionDocumento($ind_descripcion_documento, $pk_num_documento);
		echo json_encode(!$DescripcionExiste);
	}
}
?>
