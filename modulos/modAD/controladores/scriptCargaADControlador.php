<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Archivo Digital. Menú
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Menu
require_once 'dataMenuAD.php';
class scriptCargaADControlador extends Controlador
{
    use dataMenuAD;

    private $atScriptCarga;

    public function __construct()
    {
        parent::__construct();
        $this->atScriptCarga = $this->metCargarModelo('scriptCarga');
    }

    public function metIndex()
    {
        echo "INICIANDO<br>";

        echo "MENU ACHIVO DIGITAL<br>";
        $this->atScriptCarga->metCargarMenu($this->metDataMenu());

        echo "TERMINADO";
    }
}
