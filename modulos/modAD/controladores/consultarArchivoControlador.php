<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Visitas
 * PROCESO: Registro de Visistas
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Sergio Zabaleta                            |zsergio01@gmail.com                 |0414-3638131                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |08-10-2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class consultarArchivoControlador extends Controlador
{

    private $atConsultaModelo;
    private $atFPDF;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atConsultaModelo = $this->metCargarModelo('consultarArchivo');
        $this->atArchivoModelo = $this->metCargarModelo('registroArchivo');

    }

    public function metIndex()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);


        foreach ($this->atConsultaModelo->metListarEstante() AS $i){
            $estante[] = array(
                'id'=>$i['pk_num_estante'],
                'nombre'=>$i['ind_descripcion_estante']
            );
        }

        $tipoDocumento=$this->atConsultaModelo->atMiscelaneoModelo->metMostrarSelect('TDAD');
        $this->atVista->assign('documento',$tipoDocumento);

        $numTramo=$this->atConsultaModelo->atMiscelaneoModelo->metMostrarSelect('TRAD');
        $this->atVista->assign('tramo',$numTramo);

        $this->atVista->assign('selectEstante', $estante);

        $this->atVista->metRenderizar('listado');

    }


    public function metArchivos()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $documento = $this->metObtenerInt('documento');     $descripcion = $this->metObtenerAlphaNumerico('descripcion');
        $estante = $this->metObtenerInt('estante');     $tramo = $this->metObtenerInt('tramo');
        $desde = $_POST['desde'];                           $hasta = $_POST['hasta'];
        $resumen = $this->metObtenerAlphaNumerico('resumen');

        $this->atVista->assign('listado',$this->atConsultaModelo->metBuscarArchivo($documento,$descripcion,$tramo,$estante,$resumen,$desde,$hasta));

        $this->atVista->metRenderizar('listadoConsulta');
    }

    public function metImprimirReporte()
    {
        $this->metObtenerLibreria('cabeceraMaestros','modAD');
        $pdf = new pdfRegistroFiltro('L','mm','Letter');

        $documento = $_GET['documento'];        $tramo = $_GET['tramo'];
        $descripcion = $_GET['descripcion'];    $estante = $_GET['estante'];
        $hasta = $_GET['hasta'];                $desde = $_GET['desde'];
        $resumen = $_GET['resumen'];
        //
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true,10);

        $pdf->Ln(2);
        $pdf->SetFont('Arial', 'B', 10);

        $pdf->Ln(4);

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 6);
        $pdf->Cell(10, 5,utf8_decode('NRO'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5,utf8_decode('NRO DE DOCUMENTO'), 1, 0, 'C', 1);
        $pdf->Cell(40, 5,utf8_decode('DESCRIPCION'), 1, 0, 'C', 1);
        $pdf->Cell(40, 5,utf8_decode('TIPO DE DOCUMENTO'), 1, 0, 'C', 1);
        $pdf->Cell(40, 5,utf8_decode('ESTANTE'), 1, 0, 'C', 1);
        $pdf->Cell(40, 5,utf8_decode('TRAMO'), 1, 0, 'C', 1);

        $pdf->Ln();
        //
        $datos = $this->atConsultaModelo->metBuscarArchivo($documento,$descripcion,$tramo,$estante,$resumen,$desde,$hasta);
        $num=0;
        foreach ($datos as $i){
            $num++;
            $pdf->SetFillColor(255, 255, 255);
            $pdf->SetFont('Arial', '', 7);
            $pdf->SetWidths(array(10,25,40,40,40,40));
            $pdf->SetAligns(array('C','C','C','C','C','C'));

            $pdf->Row(array(
                $num,
                $i['ind_documento'],
                $i['ind_descripcion'],
                $i['documento'],
                $i['ind_descripcion_estante'],
                $i['tramo']
            ));
        }
        $pdf->ln();
        $pdf->ln();
        $pdf->ln();
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetFont('Arial', '', 7);
        $pdf->SetWidths(array(30,10));
        $pdf->SetAligns(array('C','C'));
        $pdf->Row(array('TOTAL DE ARCHIVOS:',$num));

        //	Muestro el contenido del pdf.
        $pdf->Output();

    }


}
