<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Archivo Digital
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/  
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de cajas de archivos
class carpetaControlador extends Controlador
{
	private $atCarpetaModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atCarpetaModelo = $this->metCargarModelo('carpeta');
	}

	//Método index del controlador
	public function metIndex(){
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef'
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJs($js);
		$validar=array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$complementosJs[] = 'select2/select2.min';
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJsComplemento($validar);
		$this->atVista->assign('listarCarpeta', $this->atCarpetaModelo->metListarCarpeta());
		$this->atVista->metRenderizar('listado');
	}

	//Método que permite registrar una caja de archivos
	public function metNuevaCarpeta()
	{
		$valido = $this->metObtenerInt('valido');
		if (isset($valido) && $valido == 1) {
			$this->metValidarToken();
			$pk_num_estante = $_POST['pk_num_estante'];
			$fec_anio = $_POST['fec_anio'];
            $ind_descripcion_carpeta = $_POST['ind_descripcion_carpeta'];
			$pk_num_dependencia = $_POST['pk_num_dependencia'];
			$pk_num_caja = $_POST['pk_num_caja'];
			$pk_num_documento = $_POST['fk_a006_miscelaneo_documento'];
			$pk_num_tramo = $_POST['fk_a006_miscelaneo_tramo'];
			$pkNumCarpeta = $this->atCarpetaModelo->metGuardarCarpeta($pk_num_estante, $fec_anio, $ind_descripcion_carpeta, $pk_num_dependencia, $pk_num_documento, $pk_num_tramo, $pk_num_caja);
			$obtenerDependencia=$this->atCarpetaModelo->metVerDependencia($pkNumCarpeta);
			$ind_dependencia= $obtenerDependencia['ind_dependencia'];
            $obtenerTipoDocumento = $this->atCarpetaModelo->metVisualizarMiscelaneo($pk_num_documento);
            $ind_descripcion_documento = $obtenerTipoDocumento['ind_nombre_detalle'];
            $obtenerNumeroTramo = $this->atCarpetaModelo->metVisualizarMiscelaneo($pk_num_tramo);
            $ind_descripcion_tramo = $obtenerNumeroTramo['ind_nombre_detalle'];
			$arrayPost = array(
				'ind_descripcion_carpeta' => $ind_descripcion_carpeta,
				'ind_nombre_detalle' => $ind_descripcion_documento,
				'ind_dependencia' => $ind_dependencia,
				'fec_anio' => $fec_anio,
				'pk_num_carpeta' => $pkNumCarpeta
			);
			echo json_encode($arrayPost);
			exit;
		}
		$form = array(
			'status' => 'nuevo',
			'ind_descripcion_carpeta' => null,
			'ind_descripcion_documento' => null,
			'ind_dependencia' => null,
			'fec_anio' => null,
			'pk_num_dependencia' => null,
			'pk_num_documento' => null,
			'pk_num_carpeta'=> null
		);
		$almacen=$this->atCarpetaModelo->metListarAlmacen();
		$this->atVista->assign('almacen',$almacen);
		$dependencia=$this->atCarpetaModelo->metListarDependencia();
		$this->atVista->assign('dependencia',$dependencia);
		$tipoDocumento=$this->atCarpetaModelo->atMiscelaneoModelo->metMostrarSelect('TDAD');
		$this->atVista->assign('documento',$tipoDocumento);
        $numeroTramo=$this->atCarpetaModelo->atMiscelaneoModelo->metMostrarSelect('TRAD');
		$this->atVista->assign('tramo',$numeroTramo);
		$this->atVista->assign('form', $form);
		$anio = date("Y");
		$this->atVista->assign('desde',$anio);
		$this->atVista->metRenderizar('nuevo', 'modales');
	}

	//Método que permite listar el selector de pasillo
	public function metBuscarPasillo()
	{
		$pkNumAlmacen = $this->metObtenerInt('idAlmacen');
		$listarPasillo=$this->atCarpetaModelo->metBuscarPasillo($pkNumAlmacen);

		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_pasillo" id="pk_num_pasillo" onchange="cargarEstante(this.value)"> <option value="" disabled selected>&nbsp;</option>';
		foreach($listarPasillo as $listarPasillo){
			$a .='<option value="'.$listarPasillo['pk_num_pasillo'].'">'.$listarPasillo['ind_pasillo'].'</option>';
		}
		$a .= '</select><label for="pk_num_pasillo"><i class="glyphicon glyphicon-menu-hamburger"></i> Pasillo</label></div>';
		echo $a;
	}

	//Método que permite listar el selector de pasillo
	public function metBuscarCajas()
	{
		$pkNumDependencia = $this->metObtenerInt('idDependencia');
		$listarCajas=$this->atCarpetaModelo->metBuscarDependenciaCaja($pkNumDependencia);

		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_caja" id="pk_num_caja"> <option value="" disabled selected>&nbsp;</option>';
		foreach($listarCajas as $listarCaja){
			$a .='<option value="'.$listarCaja['pk_num_caja'].'">'.$listarCaja['ind_descripcion_caja'].'</option>';
		}
		$a .= '</select><label for="pk_num_caja"><i class="glyphicon glyphicon-briefcase"></i> Caja</label></div>';
		echo $a;
	}

	//Método que permite listar el selector de estante
	public function metBuscarEstante()
	{
		$pkNumPasillo = $this->metObtenerInt('idPasillo');
		$listarEstante=$this->atCarpetaModelo->metBuscarEstante($pkNumPasillo);

		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_estante" id="pk_num_estante"> <option value="" disabled selected>&nbsp;</option>';
		foreach($listarEstante as $listarEstante){
			$a .='<option value="'.$listarEstante['pk_num_estante'].'">'.$listarEstante['ind_descripcion_estante'].'</option>';
		}
		$a .= '</select><label for="pk_num_estante"><i class="glyphicon glyphicon-th-large"></i> Estante</label></div>';
		echo $a;
	}

	//Método que permite visualizar una caja en específica
	public function metVerCarpeta()
	{
		$metodo=2;
		$pkNumCarpeta = $this->metObtenerInt('pk_num_carpeta');
		$carpeta=$this->atCarpetaModelo->metVerCarpeta($pkNumCarpeta, $metodo);
		$this->atVista->assign('carpeta',$carpeta);
		$this->atVista->metRenderizar('ver', 'modales');
	}

	//Método que permite eliminar una caja
	public function metEliminarCarpeta()
	{
		$pkNumCarpeta = $this->metObtenerInt('pk_num_carpeta');
		$this->atCarpetaModelo->metEliminarCarpeta($pkNumCarpeta);
		$arrayPost = array(
			'status' => 'OK',
			'pk_num_carpeta' => $pkNumCarpeta
		);
		echo json_encode($arrayPost);
	}

	//Método que permite ver el listado de cajas en un reporte
	public function metReporteCarpeta()
	{
		$this->metObtenerLibreria('cabeceraMaestros','modAD');
		$pdf= new pdfCaja('L','mm','Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 5);
		$pdf->SetFont('Arial','',10);
		$pdf->SetWidths(array(75, 40, 45, 30, 20, 51));
		$pdf->SetAligns(array('C', 'L', 'C', 'C', 'C','C'));
		$carpeta= $this->atCarpetaModelo->metListarCarpeta();
		foreach($carpeta as $carpeta) {
			$pdf->Row(array(
				utf8_decode($carpeta['pk_num_carpeta'].'-'.$carpeta['ind_dependencia'].'-'.$carpeta['fec_anio']),
				utf8_decode($carpeta['ind_nombre_detalle']),
				utf8_decode($carpeta['ind_descripcion_carpeta']),
				utf8_decode($carpeta['ind_descripcion_estante']),
				utf8_decode($carpeta['ind_pasillo']),
				utf8_decode($carpeta['ind_descripcion_almacen'])
			), 5);
		}
		$pdf->Output();
	}

	//Método que permite editar la caja
	public function metEditarCarpeta()
	{
		$pkNumCarpeta = $this->metObtenerInt('pk_num_carpeta');
		$valido = $this->metObtenerInt('valido');

        $tipoDocumento=$this->atCarpetaModelo->atMiscelaneoModelo->metMostrarSelect('TDAD');
        $this->atVista->assign('documento',$tipoDocumento);
        $numeroTramo=$this->atCarpetaModelo->atMiscelaneoModelo->metMostrarSelect('TRAD');
        $this->atVista->assign('tramo',$numeroTramo);

		if (isset($valido) && $valido == 1) {

			$ind_descripcion_carpeta = $this->metObtenerTexto('ind_descripcion_carpeta');
			$pk_num_almacen = $this->metObtenerInt('pk_num_almacen');
			$pk_num_pasillo = $this->metObtenerInt('pk_num_pasillo');
			$pk_num_estante = $this->metObtenerInt('pk_num_estante');
			$pk_num_dependencia = $this->metObtenerInt('pk_num_dependencia');

			if(isset($_POST['pk_num_caja']))
 	           $pk_num_caja = $_POST['pk_num_caja'];
			else
                $pk_num_caja = NULL;

			$pk_num_documento = $this->metObtenerInt('fk_a006_miscelaneo_documento');;
            $pk_num_tramo = $this->metObtenerInt('fk_a006_miscelaneo_tramo');;
			$fec_anio = $this->metObtenerInt('fec_anio');
			$this->metValidarToken();
			$id = $this->atCarpetaModelo->metEditarCarpeta($pk_num_estante,$fec_anio, $ind_descripcion_carpeta, $pk_num_dependencia, $pk_num_documento, $pk_num_tramo, $pk_num_caja, $pkNumCarpeta);
			$obtenerAlmacen=$this->atCarpetaModelo->metBuscarAlmacen($pk_num_almacen);
			$ind_descripcion_almacen = $obtenerAlmacen['ind_descripcion_almacen'];
			$obtenerPasillo=$this->atCarpetaModelo->metVerPasillo($pk_num_pasillo);
			$ind_pasillo= $obtenerPasillo['ind_pasillo'];
			$obtenerEstante=$this->atCarpetaModelo->metVerEstante($pk_num_estante);
			$ind_descripcion_estante = $obtenerEstante['ind_descripcion_estante'];
			$obtenerDependencia = $this->atCarpetaModelo->metVisualizarDependencia($pk_num_dependencia);
			$ind_dependencia = $obtenerDependencia['ind_dependencia'];
            $obtenerTipoDocumento = $this->atCarpetaModelo->metVisualizarMiscelaneo($pk_num_documento);
            $ind_descripcion_documento = $obtenerTipoDocumento['ind_nombre_detalle'];
            $obtenerNumeroTramo = $this->atCarpetaModelo->metVisualizarMiscelaneo($pk_num_tramo);
            $ind_descripcion_tramo = $obtenerNumeroTramo['ind_nombre_detalle'];

			$arrayPost = array(
				'status' => 'modificar',
				'pk_num_almacen' => $pk_num_almacen,
				'ind_descripcion_almacen' => $ind_descripcion_almacen,
				'ind_descripcion_carpeta' => $ind_descripcion_carpeta,
				'ind_pasillo' => $ind_pasillo,
				'pk_num_pasillo' => $pk_num_pasillo,
				'fec_anio' => $fec_anio,
				'pk_num_estante' => $pk_num_estante,
				'ind_descripcion_estante' => $ind_descripcion_estante,
				'pk_num_dependencia' => $pk_num_dependencia,
				'ind_dependencia' => $ind_dependencia,
				'pk_num_miscelaneo_detalle' => $pk_num_documento,
				'ind_nombre_detalle' => $ind_descripcion_documento,
				'pk_num_carpeta' =>  $pkNumCarpeta,
				'pk_num_caja' =>  $pk_num_caja,
				'id' =>  $id,
			);

			echo json_encode($arrayPost);
			exit;
		}

		if (!empty($pkNumCarpeta)) {
			$metodo=1;
			$form = $this->atCarpetaModelo->metVerCarpeta($pkNumCarpeta, $metodo);
			$pkNumPasillo= $form['pk_num_pasillo'];
			$pkNumAlmacen= $form['pk_num_almacen'];
            $pk_num_documento = $this->metObtenerInt('fk_a006_miscelaneo_documento');
            $pk_num_tramo = $this->metObtenerInt('fk_a006_miscelaneo_tramo');
			$pkNumDependencia = $form['pk_num_dependencia'];
			$almacen=$this->atCarpetaModelo->metListarAlmacen();
			$this->atVista->assign('almacen',$almacen);
			$pasillo=$this->atCarpetaModelo->metBuscarPasillo($pkNumAlmacen);
			$this->atVista->assign('pasillo',$pasillo);
			$estante=$this->atCarpetaModelo->metBuscarEstante($pkNumPasillo);
			$this->atVista->assign('estante',$estante);
			$dependencia = $this->atCarpetaModelo->metListarDependencia($pkNumDependencia);
			$this->atVista->assign('dependencia', $dependencia);
			$caja = $this->atCarpetaModelo->metListarCaja($pkNumDependencia);
			$this->atVista->assign('caja', $caja);
            $obtenerTipoDocumento = $this->atCarpetaModelo->metVisualizarMiscelaneo($pk_num_documento);
            $obtenerNumeroTramo = $this->atCarpetaModelo->metVisualizarMiscelaneo($pk_num_tramo);
            $ind_descripcion_documento = $obtenerTipoDocumento['ind_nombre_detalle'];
            $ind_descripcion_tramo = $obtenerNumeroTramo['ind_nombre_detalle'];
    		$this->atVista->assign('form', $form);
			$anio = date("Y");
			$this->atVista->assign('desde',$anio);

			$this->atVista->metRenderizar('editar', 'modales');
		}
	}
}
?>
