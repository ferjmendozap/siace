<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Archivo Digital
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/  
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de cajas de archivos
class cajaControlador extends Controlador
{
	private $atCajaModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atCajaModelo = $this->metCargarModelo('caja');
	}

	//Método index del controlador
	public function metIndex(){
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef'
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJs($js);
		$validar=array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$complementosJs[] = 'select2/select2.min';
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJsComplemento($validar);
		$this->atVista->assign('listarCaja', $this->atCajaModelo->metListarCaja());
		$this->atVista->metRenderizar('listado');
	}

	//Método que permite registrar una caja de archivos
	public function metNuevaCaja()
	{
		$valido = $this->metObtenerInt('valido');
		if (isset($valido) && $valido == 1) {
			$this->metValidarToken();
			$pk_num_estante = $_POST['pk_num_estante'];
			$fec_anio = $_POST['fec_anio'];
			$ind_descripcion_caja = $_POST['ind_descripcion_caja'];
			$pk_num_dependencia = $_POST['pk_num_dependencia'];
			//$pk_num_documento = $_POST['pk_num_documento'];
			$usuario = Session::metObtener('idUsuario');
			$fecha_hora = date('Y-m-d H:i:s');
			$pkNumCaja = $this->atCajaModelo->metGuardarCaja($pk_num_estante, $fec_anio, $ind_descripcion_caja, $pk_num_dependencia, $fecha_hora, $usuario);
			$obtenerDependencia=$this->atCajaModelo->metVerDependencia($pkNumCaja);
			$ind_dependencia= $obtenerDependencia['ind_dependencia'];
		//	$obtenerDocumento= $this->atCajaModelo->metVerDocumento($pkNumCaja);
		//	$ind_descripcion_documento= $obtenerDocumento['ind_descripcion_documento'];
			$arrayPost = array(
				'ind_descripcion_caja' => $ind_descripcion_caja,
		//		'ind_descripcion_documento' => $ind_descripcion_documento,
				'ind_dependencia' => $ind_dependencia,
				'fec_anio' => $fec_anio,
				'pk_num_caja' => $pkNumCaja
			);
			echo json_encode($arrayPost);
			exit;
		}
		$form = array(
			'status' => 'nuevo',
			'ind_descripcion_caja' => null,
			'ind_descripcion_documento' => null,
			'ind_dependencia' => null,
			'fec_anio' => null,
			'pk_num_dependencia' => null,
		//	'pk_num_documento' => null,
			'pk_num_caja'=> null
		);
		$almacen=$this->atCajaModelo->metListarAlmacen();
		$this->atVista->assign('almacen',$almacen);
		$dependencia=$this->atCajaModelo->metListarDependencia();
		$this->atVista->assign('dependencia',$dependencia);
		//$tipoDocumento=$this->atCajaModelo->metListarDocumento();
		//$this->atVista->assign('documento',$tipoDocumento);
		$this->atVista->assign('form', $form);
		$anio = date("Y");
		$this->atVista->assign('desde',$anio);
		$this->atVista->metRenderizar('nuevo', 'modales');
	}

	//Método que permite listar el selector de pasillo
	public function metBuscarPasillo()
	{
		$pkNumAlmacen = $this->metObtenerInt('idAlmacen');
		$listarPasillo=$this->atCajaModelo->metBuscarPasillo($pkNumAlmacen);

		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_pasillo" id="pk_num_pasillo" onchange="cargarEstante(this.value)"> <option value="" disabled selected>&nbsp;</option>';
		foreach($listarPasillo as $listarPasillo){
			$a .='<option value="'.$listarPasillo['pk_num_pasillo'].'">'.$listarPasillo['ind_pasillo'].'</option>';
		}
		$a .= '</select><label for="pk_num_pasillo"><i class="glyphicon glyphicon-menu-hamburger"></i> Pasillo</label></div>';
		echo $a;
	}

	//Método que permite listar el selector de estante
	public function metBuscarEstante()
	{
		$pkNumPasillo = $this->metObtenerInt('idPasillo');
		$listarEstante=$this->atCajaModelo->metBuscarEstante($pkNumPasillo);

		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_estante" id="pk_num_estante"> <option value="" disabled selected>&nbsp;</option>';
		foreach($listarEstante as $listarEstante){
			$a .='<option value="'.$listarEstante['pk_num_estante'].'">'.$listarEstante['ind_descripcion_estante'].'</option>';
		}
		$a .= '</select><label for="pk_num_estante"><i class="glyphicon glyphicon-th-large"></i> Estante</label></div>';
		echo $a;
	}

	//Método que permite visualizar una caja en específica
	public function metVerCaja()
	{
		$metodo=2;
		$pkNumCaja = $this->metObtenerInt('pk_num_caja');
		$caja=$this->atCajaModelo->metVerCaja($pkNumCaja, $metodo);
		$this->atVista->assign('caja',$caja);
		$this->atVista->metRenderizar('ver', 'modales');
	}

	//Método que permite eliminar una caja
	public function metEliminarCaja()
	{
		$pkNumCaja = $this->metObtenerInt('pk_num_caja');
		$this->atCajaModelo->metEliminarCaja($pkNumCaja);
		$arrayPost = array(
			'status' => 'OK',
			'pk_num_caja' => $pkNumCaja
		);
		echo json_encode($arrayPost);
	}

	//Método que permite ver el listado de cajas en un reporte
	public function metReporteCaja()
	{
		$this->metObtenerLibreria('cabeceraMaestros','modAD');
		$pdf= new pdfCaja('L','mm','Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 5);
		$pdf->SetFont('Arial','',10);
		$pdf->SetWidths(array(75, 40, 45, 30, 20, 51));
		$pdf->SetAligns(array('C', 'L', 'C', 'C', 'C','C'));
		$caja= $this->atCajaModelo->metListarCaja();
		foreach($caja as $caja) {
			$pdf->Row(array(
				utf8_decode($caja['pk_num_caja'].'-'.$caja['ind_dependencia'].'-'.$caja['fec_anio']),
				utf8_decode($caja['ind_descripcion_documento']),
				utf8_decode($caja['ind_descripcion_caja']),
				utf8_decode($caja['ind_descripcion_estante']),
				utf8_decode($caja['ind_pasillo']),
				utf8_decode($caja['ind_descripcion_almacen'])
			), 5);
		}
		$pdf->Output();
	}

	//Método que permite editar la caja
	public function metEditarCaja()
	{
		$pkNumCaja = $this->metObtenerInt('pk_num_caja');
		$valido = $this->metObtenerInt('valido');
		if (isset($valido) && $valido == 1) {
			$ind_descripcion_caja = $this->metObtenerTexto('ind_descripcion_caja');
			$pk_num_almacen = $this->metObtenerInt('pk_num_almacen');
			$pk_num_pasillo = $this->metObtenerInt('pk_num_pasillo');
			$pk_num_estante = $this->metObtenerInt('pk_num_estante');
			$pk_num_dependencia = $this->metObtenerInt('pk_num_dependencia');
			//$pk_num_documento = $this->metObtenerInt('pk_num_documento');
			$fec_anio = $this->metObtenerInt('fec_anio');
			$this->metValidarToken();
			$usuario = Session::metObtener('idUsuario');
			$fecha_hora = date('Y-m-d H:i:s');
			$this->atCajaModelo->metEditarCaja($pk_num_estante,$fec_anio, $ind_descripcion_caja, $pk_num_dependencia, $pkNumCaja);
			$obtenerAlmacen=$this->atCajaModelo->metBuscarAlmacen($pk_num_almacen);
			$ind_descripcion_almacen = $obtenerAlmacen['ind_descripcion_almacen'];
			$obtenerPasillo=$this->atCajaModelo->metVerPasillo($pk_num_pasillo);
			$ind_pasillo= $obtenerPasillo['ind_pasillo'];
			$obtenerEstante=$this->atCajaModelo->metVerEstante($pk_num_estante);
			$ind_descripcion_estante = $obtenerEstante['ind_descripcion_estante'];
			$obtenerDependencia = $this->atCajaModelo->metVisualizarDependencia($pk_num_dependencia);
			$ind_dependencia = $obtenerDependencia['ind_dependencia'];
		//	$obtenerDocumento = $this->atCajaModelo->metVisualizarDocumento($pk_num_documento);
		//	$ind_descripcion_documento = $obtenerDocumento['ind_descripcion_documento'];
			$arrayPost = array(
				'status' => 'modificar',
				'pk_num_almacen' => $pk_num_almacen,
				'ind_descripcion_almacen' => $ind_descripcion_almacen,
				'ind_descripcion_caja' => $ind_descripcion_caja,
				'ind_pasillo' => $ind_pasillo,
				'pk_num_pasillo' => $pk_num_pasillo,
				'fec_anio' => $fec_anio,
				'pk_num_pasillo' => $pk_num_almacen,
				'pk_num_estante' => $pk_num_estante,
				'ind_descripcion_estante' => $ind_descripcion_estante,
				'pk_num_dependencia' => $pk_num_dependencia,
				'ind_dependencia' => $ind_dependencia,
			//	'pk_num_documento' => $pk_num_documento,
			//	'ind_descripcion_documento' => $ind_descripcion_documento,
				'pk_num_caja' =>  $pkNumCaja
			);
			echo json_encode($arrayPost);
			exit;
		}
		if (!empty($pkNumCaja)) {
			$metodo=1;
			$form = $this->atCajaModelo->metVerCaja($pkNumCaja, $metodo);
			$pkNumPasillo= $form['pk_num_pasillo'];
			$pkNumAlmacen= $form['pk_num_almacen'];
			$pkNumDependencia = $form['pk_num_dependencia'];
			$almacen=$this->atCajaModelo->metListarAlmacen();
			$this->atVista->assign('almacen',$almacen);
			$pasillo=$this->atCajaModelo->metBuscarPasillo($pkNumAlmacen);
			$this->atVista->assign('pasillo',$pasillo);
			$estante=$this->atCajaModelo->metBuscarEstante($pkNumPasillo);
			$this->atVista->assign('estante',$estante);
			$dependencia = $this->atCajaModelo->metListarDependencia($pkNumDependencia);
			$this->atVista->assign('dependencia', $dependencia);
		//	$documento = $this->atCajaModelo->metListarDocumento();
		//	$this->atVista->assign('documento', $documento);
			$this->atVista->assign('form', $form);
			$anio = date("Y");
			$this->atVista->assign('desde',$anio);
			$this->atVista->metRenderizar('editar', 'modales');
		}
	}
}
?>
