<form id="formAjax" action="{$_Parametros.url}modAD/salidaArchivoCONTROL/NuevaSalidaMET" class="form floating-label" novalidate>
<input type="hidden" name="valido" value="1" />
<div class="modal-body">
	<div class="col-md-6 col-sm-6">
		<div class="card">
			<div class="card-body">
				<div class="form-group floating-label">
					<select class="form-control"  name="pk_num_almacen" onchange="cargarPasillo(this.value)">
						<option value="">&nbsp;</option>
						{foreach item=us from=$almacen}
							<option value="{$us.pk_num_almacen}">{$us.ind_descripcion_almacen}</option>
						{/foreach}
					</select>
					<label for="pk_num_almacen"><i class="glyphicon glyphicon-home"></i> Almacén</label>
				</div>
				<div id="pasillo">
					<div class="form-group floating-label">
						<select class="form-control">
							<option value="">&nbsp;</option>
						</select>
						<label><i class="glyphicon glyphicon-th"></i> Pasillo</label>
					</div>
				</div>
				<div id="estante">
					<div class="form-group floating-label">
						<select class="form-control">
							<option value="">&nbsp;</option>
						</select>
						<label><i class="glyphicon glyphicon-th-list"></i> Estante</label>
					</div>
				</div>
				<div id="dependencia">
					<div class="form-group floating-label">
						<select class="form-control">
							<option value="">&nbsp;</option>
						</select>
						<label><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
					</div>
				</div>
				<div id="documento">
					<div class="form-group floating-label">
						<select class="form-control">
							<option value="">&nbsp;</option>
						</select>
						<label><i class="glyphicon glyphicon-file"></i> Tipo de documento</label>
					</div>
				</div>
				<div id="caja">
					<div class="form-group floating-label">
						<select class="form-control">
							<option value="">&nbsp;</option>
						</select>
						<label><i class="glyphicon glyphicon-inbox"></i> Caja</label>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6">
		<div class="card">
			<div class="card-body">
				<div id="registro">
					<div class="form-group floating-label">
						<select class="form-control">
							<option value="">&nbsp;</option>
						</select>
						<label><i class="glyphicon glyphicon-file"></i> Documento</label>
					</div>
				</div>
				<div class="form-group">
					<input type="text" name="cod_memo" id="cod_memo" class="form-control">
					<label class="control-label"><i class="glyphicon glyphicon-th-large"></i> Número de memorandum</label>
				</div>
				<div class="form-group">
					<input type="text"  name="ind_motivo" id="ind_motivo" class="form-control">
					<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Motivo</label>
				</div>
				<div class="form-group control-width-normal">
					<div class="input-group date" id="demo-date">
						<div class="input-group-content">
							<input type="text" class="form-control" id="fecha" name="fecha_salida">
							<label>Fecha de salida del documento</label>
						</div>
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div><!--end .form-group -->
				<div class="form-group floating-label">
					{foreach item=fun from=$funcionario}
					<input type="text" class="form-control" id="regular2" value="{$fun.ind_nombre1} {$fun.ind_apellido1}" disabled="disabled">
					<label for="regular2">Preparado por:</label>
					{/foreach}
				</div>
			</div>
		</div>       
	</div>
    <div align="right">
         <div id="btnGuardar">
                <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado el registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
         &nbsp;&nbsp;&nbsp;
                <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
          </div>
     </div>
  </div>
</form>
<script type="text/javascript">
	function cargarPasillo(idAlmacen) {
		$("#pasillo").html("");
		$.post("{$_Parametros.url}modAD/salidaArchivoCONTROL/BuscarPasilloMET",{ idAlmacen:""+idAlmacen }, function (dato) {
			$("#pasillo").html(dato);
		});
	}
	function cargarEstante(idPasillo) {
		$("#estante").html("");
		$.post("{$_Parametros.url}modAD/salidaArchivoCONTROL/BuscarEstanteMET",{ idPasillo:""+idPasillo }, function (dato) {
			$("#estante").html(dato);
		});
	}
	function cargarDependencia(idEstante) {
		$("#dependencia").html("");
		$.post("{$_Parametros.url}modAD/salidaArchivoCONTROL/BuscarDependenciaMET",{ idEstante:""+idEstante }, function (dato) {
			$("#dependencia").html(dato);
		});
	}
	function cargarCaja(idDocumento,idDependencia, idEstante) {
		$("#caja").html("");
		$.post("{$_Parametros.url}modAD/salidaArchivoCONTROL/BuscarCajaMET",{ idDocumento:""+idDocumento,idDependencia:""+idDependencia, idEstante:""+idEstante, }, function (dato) {
			$("#caja").html(dato);
		});
	}

	function cargarRegistro(idCaja) {
		$("#registro").html("");
		$.post("{$_Parametros.url}modAD/salidaArchivoCONTROL/BuscarRegistroMET",{ idCaja:""+idCaja}, function (dato) {
			$("#registro").html(dato);
		});
	}

	function cargarDocumento(idDependencia, idEstante) {
		$("#documento").html("");
		$.post("{$_Parametros.url}modAD/salidaArchivoCONTROL/TipoDocumentoMET",{ idDependencia:""+idDependencia, idEstante:""+idEstante}, function (dato) {
			$("#documento").html(dato);
		});
	}
	$(document).ready(function () {
		//validation rules
		$("#formAjax").validate({
			rules:{
				pk_num_almacen:{
					required:true
				},
				pk_num_pasillo:{
					required:true
				},
				pk_num_estante: {
					required: true
				},
				pk_num_caja: {
					required: true
				},
				pk_num_dependencia: {
					required: true
				},
				cod_memo: {
					required: true
				},
				ind_motivo: {
					required: true
				},
				fecha_salida: {
					required: true
				},
				pk_num_registro_documento: {
					required:true,
					remote:{
						url:"{$_Parametros.url}modAD/salidaArchivoCONTROL/VerificarSalidaMET",
						type:"post"
					}
				},
				pk_num_documento: {
					required: true
				}
			},
			messages:{
				pk_num_almacen:{
					required: "Seleccione el almacén"
				},
				pk_num_pasillo:{
					required: "Seleccione el pasillo"
				},
				pk_num_estante: {
					required: "Seleccione el estante"
				},
				pk_num_caja: {
					required: "Seleccione la caja"
				},
				pk_num_dependencia: {
					required: "Seleccione la dependencia"
				},
				cod_memo: {
					required: "Ingrese el código del memo"
				},
				ind_motivo: {
					required: "Indique el motivo de la salida del documento"
				},
				fecha_salida: {
					required: "Indique la fecha de salida"
				},
				pk_num_registro_documento: {
					required: "Seleccione el documento",
					remote: jQuery.validator.format("El documento ya posee una solicitud de salida vigente")
				},
				pk_num_documento: {
					required: "Seleccione el tipo de documento"
				}
			},
			submitHandler: function(form) {
				$.post($(form).attr('action'), $(form).serialize(),function(dato){
					$(document.getElementById('datatable1')).append('<tr id="pk_num_salida'+dato['pk_num_salida']+'"><td>'+dato['pk_num_salida']+'</td><td>'+dato['num_registro']+'</td><td>'+dato['ind_documento']+'</td><td>'+dato['ind_descripcion']+'</td><td>PREPARADO</td>' +
							'{if in_array('AD-01-01-02-09-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha visualizado la salida" titulo="Visualizar salida de documento" data-toggle="modal" data-target="#formModal" pk_num_salida="'+dato['pk_num_salida']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-01-02-02-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha modificado la salida del documento" titulo="Modificar Salida" data-toggle="modal" data-target="#formModal" pk_num_salida="'+dato['pk_num_salida']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-01-02-05-R',$_Parametros.perfil)}<td align="center"><button class="verSalida logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha visualizado el reporte de salida" titulo="Reporte salida de documento" data-toggle="modal" data-target="#formModal" pk_num_salida="'+dato['pk_num_salida']+'"><i class="md md-attach-file" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-01-02-03-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado una salida de documento" titulo="¿Estás Seguro?" mensaje="¿Estás seguro de eliminar la salida del documento?" boton="si, Eliminar" pk_num_salida="'+dato['pk_num_salida']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td>{/if}</tr>');
					swal("Salida Guardado", "Salida guardada satisfactoriamente", "success");
					$(document.getElementById('cerrarModal')).click();
					$(document.getElementById('ContenidoModal')).html('');
				},'json');
			}
		});
	});
	$("#fecha").datepicker({
		format: 'dd/mm/yyyy',
		language: 'es',
		autoclose: true
	});

	function verificarEntrada(pkNumRegistroDocumento)
	{
		var $url = "{$_Parametros.url}modAD/salidaArchivoCONTROL/VerificarEntradaMET";
		$.post($url, { pk_num_registro_documento: pkNumRegistroDocumento},function(respuesta_post){
			  if(respuesta_post['total']>0) {
				  $("#btnGuardar").html("");
				  swal("Entrada Pendiente", "El documento se encuentra fuera del Archivo en calidad de préstamo");
				  $("#btnGuardar").html(' <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado el registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>');
			  } else {
				  $("#btnGuardar").html(' <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado el registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>');
			  }
        },'json');	
	}

    function verificarEntrada(pkNumRegistroDocumento)
    {
        var $url = "{$_Parametros.url}modAD/salidaArchivoCONTROL/VerificarEntradaMET";
        $.post($url, { pk_num_registro_documento: pkNumRegistroDocumento},function(respuesta_post){
            if(respuesta_post['total']>0) {
                $("#btnGuardar").html("");
                swal("Entrada Pendiente", "El documento se encuentra fuera del Archivo en calidad de préstamo");
                $("#btnGuardar").html(' <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado el registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>');
            } else {
                $("#btnGuardar").html(' <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado el registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>');
            }
        },'json');
    }
</script>

