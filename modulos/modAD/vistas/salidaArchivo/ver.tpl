<div class="form floating-label">
	<div class="modal-body" >
			<div class="col-md-6 col-sm-6">
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$sal.ind_descripcion_almacen}" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-home"></i> Almacén</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$sal.ind_pasillo}" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-th"></i> Pasillo</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$sal.ind_descripcion_estante}" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-th-list"></i> Estante</label>
				</div>
				<div class="form-group floating-label">
					<textarea class="form-control" rows="2">{$sal.ind_dependencia}</textarea>
					<label class="control-label"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$sal.ind_descripcion_documento}" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-file"></i> Tipo de documento</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$sal.pk_num_caja}-{$sal.ind_descripcion_caja}" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-inbox"></i> Caja</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$sal.num_registro}-{$sal.ind_documento}" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-file"></i> Documento</label>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$sal.cod_memo}" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-th-large"></i> Número de memorándum</label>
				</div>
				<div class="form-group floating-label">
					<textarea class="form-control" rows="2">{$sal.ind_motivo}</textarea>
					<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Motivo</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$sal.fecha_salida}" disabled="disabled">
					<label for="regular2">Fecha de salida del documento</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$sal.ind_nombre1} {$sal.ind_apellido1}" disabled="disabled">
					<label for="regular2">{if $sal.ind_estado=='PR'}Preparado{/if}{if $sal.ind_estado=='AP'}Aprobado{/if}{if $sal.ind_estado=='AN'}Anulado{/if} por:</label>
				</div>
				{foreach item=ac from=$acceso}
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$ac.fecha_modificacion}" disabled="disabled">
					<label for="regular2">Última modificación:</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$ac.ind_nombre1} {$ac.ind_apellido1}" disabled="disabled">
					<label for="regular2">Último usuario:</label>
				</div>
				{/foreach}
			</div>
			<div class="modal-body" >
				<div class="col-md-12 col-sm-12">
					<div class="card">
						<div class="card-body style-default-light height-1">DOCUMENTO DIGITALIZADO: {$registro.ind_descripcion}</div>
						{if $sal.num_flag_archivo==1}
                        	{if $ruta.extension=='pdf'}
								<iframe name="miIframe" frameborder="0" width="100%" height="700px" src="{$ruta.ruta}{$archivo.num_registro}/{$archivo.ind_nombre_archivo}"></iframe>
							{else}
								<div class="col-md-12 col-sm-12" align="center">
									<a type="button" class="btn ink-reaction btn-raised btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Descargando..." href="{$ruta.ruta}{$archivo.num_registro}/{$archivo.ind_nombre_archivo}" download="{$archivo.ind_nombre_archivo}">Descargar Archivo</a>
								</div>
                        	{/if}
						{else}
						<br/>
						<div class="card-body small-padding text-center">
							<iframe name="miIframe" frameborder="0" width="100%" height="700px" src="{$_Parametros.url}modAD/salidaArchivoCONTROL/VerExpedienteMET?num_registro={$registro.num_registro}"></iframe>
						</div>
						{/if}
						<br/><br/>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12">
				<div align="right">
						{if $sal.ind_estado=='PR'}
							{if in_array('AD-01-01-02-07-AP',$_Parametros.perfil)}<button type="button" class="btn btn-primary ink-reaction btn-raised"  id="accion1" pk_num_salida="{$sal.pk_num_salida}"><span class="md md-done"></span> Aprobar</button>{/if}
							&nbsp;
							{if in_array('AD-01-01-02-08-AN',$_Parametros.perfil)}<button type="button" class="btn btn-primary ink-reaction btn-danger" id="accion2" pk_num_salida="{$sal.pk_num_salida}"><span class="md md-block"></span> Anular</button>{/if}
						{/if}
						&nbsp;
						<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cerrar</button>
					</div>
				</div>
			</div>
</div>
<script type="text/javascript">
	var $url='{$_Parametros.url}modAD/salidaArchivoCONTROL/estatusMET';
	$("#accion1").click(function() {
		var $valor = 'AP';
		$.post($url,{ pk_num_salida: $(this).attr('pk_num_salida'), valor: $valor},function(dato){
			$('#pk_num_salida'+dato['pk_num_salida']).remove();
			$(document.getElementById('datatable1')).append('<tr id="pk_num_salida'+dato['pk_num_salida']+'"><td>'+dato['pk_num_salida']+'</td><td>'+dato['num_registro']+'</td><td>'+dato['ind_documento']+'</td><td>'+dato['ind_descripcion']+'</td><td>'+dato['ind_estado']+'</td><td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha visualizado la salida del documento" titulo="Visualizar salida de documento" data-toggle="modal" data-target="#formModal" pk_num_salida="'+dato['pk_num_salida']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td><td align="center"></td><td align="center"><button id="reporteSalida" class="verSalida logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha visualizado el reporte de salida" titulo="Reporte salida de documento" data-toggle="modal" data-target="#formModal" pk_num_salida="'+dato['pk_num_salida']+'"><i class="md md-attach-file" style="color: #ffffff;"></i></button></td><td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado un documento" titulo="¿Estás Seguro?" mensaje="¿Estás seguro de eliminar la salida del documento?" boton="si, Eliminar" pk_num_salida="'+dato['pk_num_salida']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td></tr>');
			swal("Aprobado", "Salida aprobada exitosamente", "success");
			$(document.getElementById('cerrarModal')).click();
			$(document.getElementById('ContenidoModal')).html('');
		}, 'json');
	});

	var $url2='{$_Parametros.url}modAD/salidaArchivoCONTROL/estatusMET';
	$("#accion2").click(function() {
		var $valor = 'AN';
		$.post($url2,{ pk_num_salida: $(this).attr('pk_num_salida'), valor: $valor},function(dato){
			$('#pk_num_salida'+dato['pk_num_salida']).remove();
			$(document.getElementById('datatable1')).append('<tr id="pk_num_salida'+dato['pk_num_salida']+'"><td>'+dato['pk_num_salida']+'</td><td>'+dato['num_registro']+'</td><td>'+dato['ind_documento']+'</td><td>'+dato['ind_descripcion']+'</td><td>'+dato['ind_estado']+'</td><td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha visualizado la salida del documento" titulo="Visualizar salida de documento" data-toggle="modal" data-target="#formModal" pk_num_salida="'+dato['pk_num_salida']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td><td align="center"></td><td align="center"><button id="reporteSalida" class="reporte logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha visualizado el reporte de salida" titulo="Reporte salida de documento" data-toggle="modal" data-target="#formModal" pk_num_salida="'+dato['pk_num_salida']+'"><i class="md md-attach-file" style="color: #ffffff;"></i></button></td><td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado un documento" titulo="¿Estás Seguro?" mensaje="¿Estás seguro de eliminar la salida del documento?" boton="si, Eliminar" pk_num_salida="'+dato['pk_num_salida']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td></tr>');
			swal("Aprobado", "Registro aprobado exitosamente", "success");
			$(document.getElementById('cerrarModal')).click();
			$(document.getElementById('ContenidoModal')).html('');
		}, 'json');
	});
</script>
