<br/>
<section class="style-default-bright">
    <div class="col-lg-12">
		<div class="card style-default-bright">
			<div class="card-head"><h2 class="text-primary">&nbsp;Salida de Documento</h2></div>
		</div>
	</div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                        <tr align="center">
                            <th><i class="glyphicon glyphicon-th-large"></i> N° de Salida</th>
                            <th><i class="glyphicon glyphicon-th-large"></i> N° de registro</th>
                            <th><i class="glyphicon glyphicon-th-large"></i> N° de Documento</th>
                            <th><i class="md-create"></i> Descripcion</th>
                            <th><i class="glyphicon glyphicon-list-alt"></i> Estado</th>
                            {if in_array('AD-01-01-02-09-V',$_Parametros.perfil)}<th width="40">Ver</th>{/if}
                            {if in_array('AD-01-01-02-02-M',$_Parametros.perfil)}<th width="50">Editar</th>{/if}
                            {if in_array('AD-01-01-02-05-R',$_Parametros.perfil)}<th width="40">Reporte</th>{/if}
                            {if in_array('AD-01-01-02-03-E',$_Parametros.perfil)}<th width="70">Eliminar</th>{/if}
                        </tr>
                        </thead>
                        <tbody>
                            {foreach item=post from=$_PruebaPost}
                            <tr id="pk_num_salida{$post.pk_num_salida}">
                            <td>{$post.pk_num_salida}</td>
                            <td>{$post.num_registro}</td>
                            <td>{$post.ind_documento}</td>
                            <td>{$post.ind_descripcion}</td>
                            <td>
                                {if $post.ind_estado=='PR'}PREPARADO{/if}
                                {if $post.ind_estado=='AP'}APROBADO{/if}
                                {if $post.ind_estado=='AN'}ANULADO{/if}
                            </td>
                            {if in_array('AD-01-01-02-09-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_salida="{$post.pk_num_salida}" title="Ver documento" ><i class="glyphicon glyphicon-search"></i></button></td>{/if}
                            {if in_array('AD-01-01-02-02-M',$_Parametros.perfil)}<td align="center">{if $post.ind_estado=='PR'}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"  pk_num_salida="{$post.pk_num_salida}" descipcion="El Usuario ha modificado una salida de documento" title="Modificar salida de documento" ><i class="fa fa-edit"></i></button>{/if}</td>{/if}
                            {if in_array('AD-01-01-02-05-R',$_Parametros.perfil)}<td align="center"><button class="verSalida logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"  pk_num_salida="{$post.pk_num_salida}" descipcion="El Usuario ha visualizado el reporte de salida" title="Reporte de salida de documento"><i class="md md-attach-file"></i></button></td>{/if}
                            {if in_array('AD-01-01-02-03-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_salida="{$post.pk_num_salida}"  boton="si, Eliminar" descipcion="El usuario ha eliminado la salida" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar la salida del documento?" title="Eliminar salida de documento"><i class="md md-delete"></i></button></td>{/if}
                        </tr>
                            {/foreach}
                        </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="9">
                            {if in_array('AD-01-01-02-01-N',$_Parametros.perfil)}<button class="logsUsuario btn ink-reaction btn-raised btn-info" descripcion="El usuario ha creado una salida" data-toggle="modal" data-target="#formModal" titulo="Registrar nueva salida" id="nuevo"><i class="md md-create"></i> Nueva Salida</button>{/if}
                            &nbsp;
                            <button  class="logsUsuario btn ink-reaction btn-raised btn-primary" id="reporteSalida" descripcion="Generar Reporte de salida: preparados" data-toggle="modal" data-target="#formModal"  titulo="Listado de salidas">Reporte de salidas</button><br/>
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
            $(document).ready(function() {

                var $url='{$_Parametros.url}modAD/salidaArchivoCONTROL/NuevaSalidaMET';

                $('#nuevo').click(function(){
					$('#modalAncho').css( "width", "75%" );
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $('#ContenidoModal').html("");
                    $.post($url,'',function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });
                var $url_modificar='{$_Parametros.url}modAD/salidaArchivoCONTROL/EditarSalidaMET';
                $('#datatable1 tbody').on( 'click', '.modificar', function () {
		    $('#modalAncho').css( "width", "75%" );
                    $('#formModalLabel').html($(this).attr('title'));
                    $('#ContenidoModal').html("");
                    $.post($url_modificar,{ pk_num_salida: $(this).attr('pk_num_salida')},function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });
                $('#datatable1 tbody').on( 'click', '.eliminar', function () {
                    var pk_num_salida=$(this).attr('pk_num_salida');
                    swal({
                        title: $(this).attr('titulo'),
                        text: $(this).attr('mensaje'),
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: $(this).attr('boton'),
                        closeOnConfirm: false
                    }, function(){
                        var $url='{$_Parametros.url}modAD/salidaArchivoCONTROL/EliminarSalidaMET';
                        $.post($url, { pk_num_salida: pk_num_salida},function($dato){
                            if($dato['status']=='OK'){
                                $(document.getElementById('pk_num_salida'+$dato['pk_num_salida'])).html('');
                                swal("Eliminado!", "La salida fue eliminada", "success");
                                $('#cerrar').click();
                            }
                        },'json');
                    });
                });

            var $urlVer='{$_Parametros.url}modAD/salidaArchivoCONTROL/VerSalidaMET';
            $('#datatable1 tbody').on( 'click', '.ver', function () {
                $('#modalAncho').css( "width", "75%" );
                $('#formModalLabel').html($(this).attr('title'));
                $('#ContenidoModal').html("");
                $.post($urlVer,{ pk_num_salida: $(this).attr('pk_num_salida')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });

            var $urlBuscar='{$_Parametros.url}modAD/salidaArchivoCONTROL/BuscarSalidaMET';
            $('#reporteSalida').click(function(){
                $('#modalAncho').css( "width", "75%" );
                $('#formModalLabel').html($(this).attr('titulo'));
                $('#ContenidoModal').html("");
                $.post($urlBuscar,'',function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });

                $('#datatable1 tbody').on( 'click', '.verSalida', function () {
                var $pk_num_salida=$(this).attr('pk_num_salida');
                var $urlVerSalida =  '{$_Parametros.url}modAD/salidaArchivoCONTROL/SalidaMET/?pk_num_salida='+$pk_num_salida;
                $('#modalAncho').css( "width", "75%" );
                $('#formModalLabel').html($(this).attr('title'));
                $('#ContenidoModal').html("");
                $('#ContenidoModal').html('<iframe frameborder="0" src="'+$urlVerSalida+'"  width="100%" height="440px"></iframe>');
            });
            });

</script>
