<form action="{$_Parametros.url}modAD/salidaArchivoCONTROL/EditarSalidaMET" id="formAjax" method="post" class="form" role="form">
	<div class="form floating-label">
		<input type="hidden" value="1" name="valido" />
		{if isset($form.pk_num_salida)}
			<input type="hidden" value="{$form.pk_num_salida}" name="pk_num_salida" id="pk_num_salida" />
		{/if}
		<div class="modal-body" >
			<div class="col-md-6 col-sm-6">
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$form.ind_descripcion_almacen}" disabled>
					<label for="regular2"><i class="glyphicon glyphicon-home"></i> Almacén</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$form.ind_pasillo}" disabled>
					<label for="regular2"><i class="glyphicon glyphicon-th"></i> Pasillo</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$form.ind_descripcion_estante}" disabled>
					<label for="regular2"><i class="glyphicon glyphicon-th-list"></i> Estante</label>
				</div>
				<div class="form-group floating-label">
					<textarea class="form-control" rows="2" disabled>{$form.ind_dependencia}</textarea>
					<label class="control-label"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$form.ind_descripcion_documento}" disabled>
					<label for="regular2"><i class="glyphicon glyphicon-file"></i> Tipo de documento</label>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$form.pk_num_caja} {$form.ind_descripcion_caja}" disabled>
					<label for="regular2"><i class="glyphicon glyphicon-inbox"></i> Caja</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$form.ind_documento}" disabled>
					<label for="regular2"><i class="glyphicon glyphicon-file"></i> Documento</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$form.cod_memo}" name="cod_memo">
					<label for="regular2"><i class="glyphicon glyphicon-th-large"></i> Número de memorándum</label>
				</div>
				<div class="form-group floating-label">
					<textarea class="form-control" rows="2" name="ind_motivo">{$form.ind_motivo}</textarea>
					<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Motivo</label>
				</div>
				<div class="form-group control-width-normal">
					<div class="input-group date" id="demo-date">
						<div class="input-group-content">
							<input type="text" class="form-control" id="fecha" name="fecha_salida" value="{$form.fecha_salida}">
								<label>Fecha de salida del documento</label>
							</div>
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
					</div><!--end .form-group -->
				</div>
		    </div>
			<div class="modal-body" >
				<div class="col-md-12 col-sm-12">
					<div class="card">
						<div class="card-body style-default-light height-1">DOCUMENTO DIGITALIZADO: {$registro.ind_descripcion}</div>
						{if $form.num_flag_archivo==1}
                        	{if $ruta.extension=='pdf'}
								<iframe name="miIframe" frameborder="0" width="100%" height="700px" src="{$ruta.ruta}{$archivo.num_registro}/{$archivo.ind_nombre_archivo}"></iframe>
							{else}
								<br/>
								<div class="col-md-12 col-sm-12" align="center">
									<a type="button" class="btn ink-reaction btn-raised btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Descargando..." href="{$ruta.ruta}{$archivo.num_registro}/{$archivo.ind_nombre_archivo}" download="{$archivo.ind_nombre_archivo}">Descargar Archivo</a>
								</div>
							{/if}
						{else}
							<div class="card-body small-padding text-center">
								<iframe name="miIframe" frameborder="0" width="100%" height="700px" src="{$_Parametros.url}modAD/registroArchivoCONTROL/VerExpedienteMET?num_registro={$registro.num_registro}"></iframe>
							</div>
						{/if}
						<br/><br/>
					</div>
				</div>
			</div>
			<div align="right">
				<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado el registro de salida del documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
				<button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	$("#fecha").datepicker({
		format: 'dd/mm/yyyy',
		language: 'es',
		autoclose: true
	});
	$(document).ready(function() {
		$("#formAjax").validate({
			rules:{
				cod_memo:{
					required:true
				}
			},
			messages:{
				cod_memo:{
					required: "Este campo es requerido"
				}
			},
			submitHandler: function(form){
				$.post($(form).attr("action"), $(form).serialize(),function(dato){
					$('#pk_num_salida'+dato['pk_num_salida']).remove();
					$(document.getElementById('datatable1')).append('<tr id="pk_num_salida'+dato['pk_num_salida']+'"><td>'+dato['pk_num_salida']+'</td><td>'+dato['num_registro']+'</td><td>'+dato['ind_documento']+'</td><td>'+dato['ind_descripcion']+'</td><td>'+dato['ind_estado']+'</td>' +
							'{if in_array('AD-01-01-02-09-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha visualizado la salida del documento" titulo="Visualizar Salida de Documento" data-toggle="modal" data-target="#formModal" pk_num_salida="'+dato['pk_num_salida']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-01-02-02-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha modificado una salida" titulo="Modificar salida" data-toggle="modal" data-target="#formModal" pk_num_salida="'+dato['pk_num_salida']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-01-02-05-R',$_Parametros.perfil)}<td align="center"><button id="reporteSalida" class="reporte logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha visualizado el reporte de salida" titulo="Reporte salida de documento" data-toggle="modal" data-target="#formModal" pk_num_salida="'+dato['pk_num_salida']+'"><i class="md md-attach-file" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-01-02-03-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado un documento" titulo="¿Estás Seguro?" mensaje="¿Estás seguro de eliminar el documento?" boton="si, Eliminar" pk_num_salida="'+dato['pk_num_salida']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td>{/if}</tr>');
					swal("Documento modificado", "Salida modificada exitosamente", "success");
					$(document.getElementById('cerrarModal')).click();
					$(document.getElementById('ContenidoModal')).html('');
				},'json');
			}
		});
	});
</script>


