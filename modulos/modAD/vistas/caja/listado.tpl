<br/>
<section class="style-default-bright">
    <div class="col-lg-12">
		<div class="card style-default-bright">
			<div class="card-head"><h2 class="text-primary">&nbsp;Caja</h2></div>
		</div>
	</div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                        <tr  align="center">
                            <th><i class="glyphicon glyphicon-inbox"></i> Id caja</th>
                            <th><i class="md md-folder-open"></i> Número-Dependencia-Año</th>
                            <th><i class="md md-attach-file"></i> Tipo de Documento</th>
                            <th><i class="glyphicon glyphicon-pencil"></i> Descripción</th>
                            {if in_array('AD-01-02-04-03-V',$_Parametros.perfil)}<th width="40"> Ver</th>{/if}
                            {if in_array('AD-01-02-04-04-M',$_Parametros.perfil)}<th width="50">Editar</th>{/if}
                            {if in_array('AD-01-02-04-05-E',$_Parametros.perfil)}<th width="70">Eliminar</th>{/if}
                        </tr>
                    </thead>
                        <tbody>
                            {foreach item=caja from=$listarCaja}
                            <tr id="pk_num_caja{$caja.pk_num_caja}">
                            <td>{$caja.pk_num_caja}</td>
                            <td>{$caja.pk_num_caja}-{$caja.ind_dependencia}-{$caja.fec_anio}</td>
                            <td>{$caja.ind_descripcion_documento}</td>
                            <td>{$caja.ind_descripcion_caja}</td>
                            {if in_array('AD-01-02-04-03-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_caja="{$caja.pk_num_caja}" titulo="Ver caja" title="Ver caja" id="ver"><i class="glyphicon glyphicon-search"></i></button></td>{/if}
                            {if in_array('AD-01-02-04-04-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_caja="{$caja.pk_num_caja}" descipcion="El usuario ha modificado una caja" title="Modificar caja" titulo="Modificar caja" ><i class="fa fa-edit"></i></button></td>{/if}
                            {if in_array('AD-01-02-04-05-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_caja="{$caja.pk_num_caja}"  boton="si, Eliminar" descipcion="El usuario ha eliminado una caja" titulo="¿Estás Seguro?" title="Eliminar caja" mensaje="¿Desea eliminar el almacén?" title="Eliminar almacén"><i class="md md-delete"></i></button></td>{/if}
                        </tr>
                            {/foreach}
                        </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="6">
                            {if in_array('AD-01-02-04-02-N',$_Parametros.perfil)}<button  class="logsUsuario btn ink-reaction btn-raised btn-info" descripcion="El usuario ha registrado una caja" data-toggle="modal" data-target="#formModal" titulo="Registrar nueva caja" id="nuevo"><i class="md md-create"></i>Nueva caja</button>{/if}
                            &nbsp;
                            {if in_array('AD-01-02-04-06-R',$_Parametros.perfil)}<button  class="logsUsuario btn ink-reaction btn-raised btn-primary" id="reporteCaja" descripcion="Generar Reporte de Caja" data-toggle="modal" data-target="#formModal" titulo="Listado de Cajas">Reporte de Caja</button>{/if}
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
            $(document).ready(function() {

                var $url='{$_Parametros.url}modAD/cajaCONTROL/NuevaCajaMET';
                var $url_modificar='{$_Parametros.url}modAD/cajaCONTROL/EditarCajaMET';
                $('#nuevo').click(function(){
					$('#modalAncho').css( "width", "75%" );
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $('#ContenidoModal').html("");
                    $.post($url,'',function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });



                $('#datatable1 tbody').on( 'click', '.modificar', function () {
		    $('#modalAncho').css( "width", "75%" );
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $('#ContenidoModal').html("");
                    $.post($url_modificar,{ pk_num_caja: $(this).attr('pk_num_caja')},function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });
                $('#datatable1 tbody').on( 'click', '.eliminar', function () {

                    var pk_num_caja=$(this).attr('pk_num_caja');
                    swal({
                        title: $(this).attr('titulo'),
                        text: $(this).attr('mensaje'),
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: $(this).attr('boton'),
                        closeOnConfirm: false
                    }, function(){
                        var $url='{$_Parametros.url}modAD/cajaCONTROL/EliminarCajaMET';
                        $.post($url, { pk_num_caja: pk_num_caja},function($dato){
                            if($dato['status']=='OK'){
                                $(document.getElementById('pk_num_caja'+$dato['pk_num_caja'])).html('');
                                swal("Eliminado!", "La caja fue eliminada.", "success");
                                $('#cerrar').click();
                            }
                        },'json');
                    });
                });
            });
    $(document).ready(function() {
        $('#reporteCaja').click(function () {
            $('#modalAncho').css( 'width', '95%' );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $('#ContenidoModal').html("<iframe src='{$_Parametros.url}modAD/cajaCONTROL/ReporteCajaMET' border='1' width='100%' height='440px'></iframe>");
        });
    });
            var $urlVer='{$_Parametros.url}modAD/cajaCONTROL/VerCajaMET';
            $('#datatable1 tbody').on( 'click', '.ver', function () {
                $('#modalAncho').css( "width", "75%" );
                $('#formModalLabel').html($(this).attr('title'));
                $('#ContenidoModal').html("");
                $.post($urlVer,{ pk_num_caja: $(this).attr('pk_num_caja')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });
</script>
