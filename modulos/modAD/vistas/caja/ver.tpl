<div class="form floating-label">
	<div class="modal-body" >
			{foreach item=ca from=$caja}
				<div class="col-md-6 col-sm-6">
					<div class="form-group floating-label">
						<input type="text" class="form-control" id="regular2" value="{$ca.ind_descripcion_almacen}" disabled="disabled">
						<label for="regular2"><i class="glyphicon glyphicon-home"></i> Almacén</label>
					</div>
					<div class="form-group floating-label">
						<input type="text" class="form-control" id="regular2" value="{$ca.ind_pasillo}" disabled="disabled">
						<label for="regular2"><i class="glyphicon glyphicon-th"></i> Pasillo</label>
					</div>
					<div class="form-group floating-label">
						<input type="text" class="form-control" id="regular2" value="{$ca.ind_descripcion_estante}" disabled="disabled">
						<label for="regular2"><i class="glyphicon glyphicon-th-list"></i> Estante</label>
					</div>
					<div class="form-group floating-label">
						<input type="text" class="form-control" id="regular2" value="{$ca.ind_descripcion_caja}" disabled="disabled">
						<label for="regular2"><i class="glyphicon glyphicon-pencil"></i> Descripción</label>
					</div>
					<div class="form-group floating-label">
						<input type="text" class="form-control" id="regular2" value="{$ca.fec_anio}" disabled="disabled">
						<label for="regular2"><i class="glyphicon glyphicon-calendar"></i> Año</label>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="form-group floating-label">
						<input type="text" class="form-control" id="regular2" value="{$ca.ind_dependencia}" disabled="disabled">
						<label for="regular2"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
					</div>
					<div class="form-group floating-label">
						<input type="text" class="form-control" id="regular2" value="{$ca.ind_descripcion_documento}" disabled="disabled">
						<label for="regular2"><i class="glyphicon glyphicon-file"></i> Tipo de Documento</label>
					</div>
					<div class="form-group floating-label">
						<input type="text" class="form-control" id="regular2" value="{$ca.fecha}" disabled="disabled">
						<label for="regular2">Última modificación</label>
					</div>
					<div class="form-group floating-label">
						<input type="text" class="form-control" id="regular2" value="{$ca.ind_nombre1} {$ca.ind_apellido1}" disabled="disabled">
						<label for="regular2">Último usuario</label>
					</div>
				</div>
			{/foreach}

			<div align="right">
				<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El usuario ha regresado al listado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cerrar</button>
			</div>

</div>
