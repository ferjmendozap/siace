<form id="formAjax" action="{$_Parametros.url}modAD/entradaArchivoCONTROL/NuevaEntradaMET" class="form floating-label" novalidate>
	<input type="hidden" name="valido" value="1" />
	    <input type="hidden" name="pk_num_salida" id="pk_num_salida" value="{$sal.pk_num_salida}" />
		<input type="hidden" name="ind_documento" value="{$sal.ind_documento}" />
	<div class="modal-body">
		<div class="col-md-6 col-sm-6">
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$sal.ind_descripcion_almacen}" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-home"></i> Almacén</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$sal.ind_pasillo}" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-th"></i> Pasillo</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$sal.ind_descripcion_estante}" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-th-list"></i> Estante</label>
			</div>
			<div class="form-group floating-label">
				<textarea class="form-control" rows="2">{$sal.ind_dependencia}</textarea>
				<label class="control-label"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$sal.ind_descripcion_documento}" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-file"></i> Tipo de documento</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$sal.pk_num_caja}-{$sal.ind_descripcion_caja}" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-inbox"></i> Caja</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$sal.num_registro}-{$sal.ind_documento}" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-file"></i> Documento</label>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$sal.cod_memo}" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-th-large"></i> Número de memorándum</label>
			</div>
			<div class="form-group floating-label">
				<textarea class="form-control" rows="2">{$sal.ind_motivo}</textarea>
				<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Motivo</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$sal.fecha_salida}" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-calendar"></i> Fecha de salida del documento</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$sal.ind_nombre1} {$sal.ind_apellido1}" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-list"></i> {$sal.ind_estado} por:</label>
			</div>
			{foreach item=ac from=$acceso}
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$ac.fecha_modificacion}" disabled="disabled">
					<label for="regular2">última modificación:</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$ac.ind_nombre1} {$ac.ind_apellido1}" disabled="disabled">
					<label for="regular2">último usuario:</label>
				</div>
			{/foreach}
			</div>
	</div>
	<div class="modal-body">
		<div class="col-md-12 col-sm-12">
			<div class="card">
				<div class="card-body style-default-light height-1">Registro de Entrada de Documento</div>
				<div class="card-body small-padding text-left">
					<div class="col-md-6 col-sm-6">
						<div class="form-group floating-label">
							<input type="text" class="form-control"  name="cod_memo" id="cod_memo">
							<label for="cod_memo"><i class="md md-create"></i> Memorándum </label>
						</div>
						<div class="form-group control-width-normal">
							<div class="input-group date" id="demo-date">
								<div class="input-group-content">
									<input type="text" class="form-control" id="fecha_entrada" name="fecha_entrada" onChange="fechas(this.value)">
									<label>Fecha de Entrada</label>
								</div>
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
						</div><!--end .form-group -->
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="form-group">
							<textarea class="form-control" rows="4" name="ind_observacion" id="ind_observacion"></textarea>
							<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Observación</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-body" >
		<div class="col-md-12 col-sm-12">
			<div class="card">
				<div class="card-body style-default-light height-1">DOCUMENTO DIGITALIZADO: {$registro.ind_descripcion}</div>
				{if $sal.num_flag_archivo==1}
                	{if $ruta.extension=='pdf'}
						<iframe name="miIframe" frameborder="0" width="100%" height="700px" src="{$ruta.ruta}{$archivo.num_registro}/{$archivo.ind_nombre_archivo}"></iframe>
					{else}
						<br/>
						<div class="col-md-12 col-sm-12" align="center">
							<a type="button" class="btn ink-reaction btn-raised btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Descargando..." href="{$ruta.ruta}{$archivo.num_registro}/{$archivo.ind_nombre_archivo}" download="{$archivo.ind_nombre_archivo}">Descargar Archivo</a>
						</div>
					{/if}
				{else}
					<div class="card-body small-padding text-center">
						<iframe name="miIframe" frameborder="0" width="100%" height="700px" src="{$_Parametros.url}modAD/entradaArchivoCONTROL/VerExpedienteMET?num_registro={$registro.num_registro}"></iframe>
					</div>
				{/if}
				<br/><br/>
			</div>
		</div>
		<div align="right">
			<div class="col-md-12 col-sm-12">
				<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
				<button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	$("#fecha_entrada").datepicker({
		format: 'dd/mm/yyyy',
		language: 'es',
		autoclose: true
	});


	function fechas(fechaEntrada)
	{
		var $url = "{$_Parametros.url}modAD/entradaArchivoCONTROL/VerificarFechaMET";
		var pk_num_salida = $("#pk_num_salida").val();
		$.post($url, { pkNumSalida: pk_num_salida, fechaEntrada: fechaEntrada},function(respuesta_post){
			  if(respuesta_post['fecha']==true) {
				  swal("Rango de fecha incorrecto", "La fecha de entrada no puede ser menor que la fecha de salida del documento");
			  }
        },'json');
	}

	$(document).ready(function () {
		//validation rules
		$("#formAjax").validate({
			rules:{
				cod_memo:{
					required:true
				},
				ind_observacion:{
					required:true
				},
				fecha_entrada:{
					required:true,
				}
			},
			messages:{
				cod_memo:{
					required: "Este campo es requerido"
				},
				ind_observacion:{
					required: "Este campo es requerido"
				},
				fecha_entrada: {
					required: "Este campo es requerido"				}
			},
			submitHandler: function(form) {
				$.post($(form).attr('action'), $(form).serialize(),function(dato){
					var botonVer = '<button class="visualizar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" title="Ver entrada de documento" pk_num_entrada="'+dato['pk_num_entrada']+'"><i class="glyphicon glyphicon-search"></i></button>';
					var botonEditar = '<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_entrada="'+dato['pk_num_entrada']+'" descripcion="El Usuario ha modificado una entrada" title="Modificar entrada" ><i class="fa fa-edit"></i></button>';
					var botonEliminar = '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_entrada="'+dato['pk_num_entrada']+'"  boton="si, Eliminar" descripcion="El usuario ha eliminado una entrada de documento" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar la entrada de documento?" title="Eliminar entrada"><i class="md md-delete"></i></button>';
					 $(document.getElementById('datatable1')).append('<tr id="pk_num_entrada'+dato['pk_num_entrada']+'"><td>'+dato['pk_num_entrada']+'</td><td>'+dato['cod_memo']+'</td><td>'+dato['ind_documento']+'</td><td>'+dato['fecha_entrada']+'</td><td>PREPARADO</td><td align="center">' + botonVer + '</td><td align="center">' + botonEditar+'</td><td align="center">' + botonEliminar +'</td></tr>');
					swal("Registro Guardado", "Registro guardado satisfactoriamente", "success");
					$(document.getElementById('cerrarModal')).click();
					$(document.getElementById('ContenidoModal')).html('');
				},'json');
			}
		});
	});
</script>
