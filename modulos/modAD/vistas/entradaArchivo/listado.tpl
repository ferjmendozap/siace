<br/>
<section class="style-default-bright">
    <div class="col-lg-12">
		<div class="card style-default-bright">
			<div class="card-head"><h2 class="text-primary">&nbsp;Entrada de Documentos</h2></div>
		</div>
	</div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div align="right">
                    <div class="btn-group "><a data-toggle="offcanvas" title="Filtro" class="btn ink-reaction btn-floating-action btn-sm btn-primary" href="#offcanvas-filtro"><i class="fa fa-filter"></i></a></div>&nbsp;
                </div>
                <br/>
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                        <tr align="center">
                            <th><i class="glyphicon glyphicon-th-large"></i> N° de Entrada</th>
                            <th><i class="glyphicon glyphicon-th-large"></i> Memo</th>
                            <th><i class="glyphicon glyphicon-file"></i> Documento</th>
                            <th><i class="glyphicon glyphicon-calendar"></i> Fecha de Entrada</th>
                            <th><i class="glyphicon glyphicon-list"></i> Estatus</th>
                            <th width="40">Ver</th>
                            <th width="50">Editar</th>
                            <th width="70">Eliminar</th>
                        </tr>
                        </thead>
                        <tbody>
                            {foreach item=post from=$_PruebaPost}
                            <tr id="pk_num_entrada{$post.pk_num_entrada}">
                            <td>{$post.pk_num_entrada}</td>
                            <td>{$post.cod_memo}</td>
                            <td>{$post.ind_documento}</td>
                            <td>{$post.fecha_entrada}</td>
                            <td>{if $post.ind_estado=='PR'}PREPARADO{/if}
                                {if $post.ind_estado=='AP'}APROBADO{/if}
                                {if $post.ind_estado=='AN'}ANULADO{/if}
                            </td>
                            <td align="center"><button class="visualizar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_entrada="{$post.pk_num_entrada}" title="Ver entrada de documento"><i class="glyphicon glyphicon-search"></i></button></td>
                            <td align="center">{if $post.ind_estado=='PR'}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_entrada="{$post.pk_num_entrada}" descripcion="El Usuario ha modificado una entrada" title="Modificar entrada" ><i class="fa fa-edit"></i></button>{/if}</td>
                            <td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_entrada="{$post.pk_num_entrada}"  boton="si, Eliminar" descripcion="El usuario ha eliminado una entrada de documento" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar la entrada de documento?" title="Eliminar entrada"><i class="md md-delete"></i></button>
                            </td>
                        </tr>
                            {/foreach}
                        </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="8">
                            <button  class="logsUsuario btn ink-reaction btn-raised btn-primary" id="verSalida" descripcion="Muestra el listado de salida de documentos" titulo="Listado de Salidas" data-toggle="modal" data-target="#formModal">Listado de Salidas</button>
                            &nbsp;
                            <button class="logsUsuario btn ink-reaction btn-raised btn-primary" id="reporteEntrada" descripcion="Generar Reporte de Almacén" data-toggle="modal" data-target="#formModal" titulo="Listado de Entradas">Reporte de Entrada</button>
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <!-- filtro -->
    <div class="offcanvas">
        <form id="formulario" class="form floating-label" role="form" method="post" novalidate>
            <div id="offcanvas-filtro" class="offcanvas-pane width-12">
                <div class="offcanvas-head">
                    <header>Filtro de Búsqueda</header>
                    <div class="offcanvas-tools">
                        <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                            <i class="md md-close"></i>
                        </a>
                    </div>
                </div>
                <div class="offcanvas-body">
                    <div class="form-group floating-label">
                        <select id="pk_num_organismo" name="pk_num_organismo" class="form-control dirty" onchange="cargarDependencia(this.value)">
                            {foreach item=org from=$listadoOrganismo}
                                {if $org.pk_num_organismo == $emp.pk_num_organismo}
                                    <option selected value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                                {else}
                                    <option value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
                    </div>
                    <div id="dependencia">
                        <div class="form-group floating-label">
                            <select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control dirty">
                                <option value="">&nbsp;</option>
                                {foreach item=dep from=$listadoDependencia}
                                    <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {/foreach}
                            </select>
                            <label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
                        </div>
                    </div>
                    <div class="form-group" style="width:100%">
                        <div class="input-daterange input-group" id="demo-date-range">
                            <div class="input-group-content">
                                <input type="text" class="fecha form-control dirty" name="fechaInicio" id="fecha_inicio">
                                <label><i class="glyphicon glyphicon-calendar"></i> Fecha de Entrada</label>
                            </div>
                            <span class="input-group-addon">a</span>
                            <div class="input-group-content">
                                <input type="text" class="fecha form-control dirty" name="fechaFin" id="fecha_fin">
                                <div class="form-control-line"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group floating-label">
                        <select id="ind_estado" name="ind_estado" class="form-control dirty">
                            <option value=""></option>
                            <option value="PR">Preparado</option>
                            <option value="AP">Aprobado</option>
                            <option value="AN">Anulado</option>
                        </select>
                        <label for="ind_estado"><i class="glyphicon glyphicon-share"></i> Estado</label>
                    </div>
                    <br/>
                    <div class="col-sm-12 col-sm-12" align="center">
                        <button type="submit" id="formu" class="btn btn-primary ink-reaction btn-raised"> Buscar</button>
                    </div>
                </div>
        </form>
    </div>
</section>
<script type="text/javascript">
    $(".fecha").datepicker({
        format: 'dd/mm/yyyy',
        language: 'es',
        autoclose: true
    });
    $(document).ready(function() {

        var $url='{$_Parametros.url}modAD/entradaArchivoCONTROL/NuevaEntradaMET';
        $('#nuevo').click(function(){
            $('#modalAncho').css( "width", "45%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        var $url_modificar='{$_Parametros.url}modAD/entradaArchivoCONTROL/EditarEntradaMET';
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
        $('#modalAncho').css( "width", "75%" );
            $('#formModalLabel').html($(this).attr('title'));
            $('#ContenidoModal').html("");
            $.post($url_modificar,{ pk_num_entrada: $(this).attr('pk_num_entrada')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var pk_num_entrada=$(this).attr('pk_num_entrada');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modAD/entradaArchivoCONTROL/EliminarEntradaMET';
                $.post($url, { pk_num_entrada: pk_num_entrada},function(respuesta_post){
                        var tabla_listado = $('#datatable1').DataTable();
                        tabla_listado.clear().draw();
                        if(respuesta_post != -1) {
                            for(var i=0; i<respuesta_post.length; i++) {
                                var botonVer = '<button class="visualizar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_entrada="'+respuesta_post[i].pk_num_entrada+'" descripcion="El Usuario ha visualizado una entrada" title="Ver Entrada" ><i class="glyphicon glyphicon-search"></i></button>';
                                var botonEditar = '<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_entrada="'+respuesta_post[i].pk_num_entrada+'" descripcion="El Usuario ha modificado una entrada" title="Modificar entrada" ><i class="fa fa-edit"></i></button>';
                                var botonEliminar = '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_entrada="'+respuesta_post[i].pk_num_entrada+'"  boton="si, Eliminar" descripcion="El usuario ha eliminado una entrada de documento" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar la entrada de documento?" title="Eliminar entrada"><i class="md md-delete"></i></button>';
                               var fila = tabla_listado.row.add([
                                    respuesta_post[i].pk_num_entrada,
                                    respuesta_post[i].cod_memo,
                                    respuesta_post[i].ind_documento,
                                    respuesta_post[i].fecha_entrada,
                                    respuesta_post[i].ind_estado,
                                    botonVer,
                                    botonEditar,
                                    botonEliminar

                                ]).draw()
                                        .nodes()
                                        .to$()
                                $(fila).attr('id','pk_num_entrada'+respuesta_post[i].pk_num_entrada+'');
                            }
                        swal("Eliminado!", "La entrada fue eliminada.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });
    var $urlBuscar='{$_Parametros.url}modAD/entradaArchivoCONTROL/BuscarEntradaMET';
    $('#reporteEntrada').click(function(){
        $('#modalAncho').css( "width", "75%" );
        $('#formModalLabel').html($(this).attr('titulo'));
        $('#ContenidoModal').html("");
        $.post($urlBuscar,'',function($dato){
            $('#ContenidoModal').html($dato);
        });
    });


    var $urlSalida='{$_Parametros.url}modAD/entradaArchivoCONTROL/ListarSalidaMET';
    $('#verSalida').click(function(){
        $('#modalAncho').css( "width", "85%" );
        $('#formModalLabel').html($(this).attr('titulo'));
        $('#ContenidoModal').html("");
        $.post($urlSalida,'',function($dato){
            $('#ContenidoModal').html($dato);
        });
    });

    function cargarDependencia(pk_num_organismo) {
        $("#dependencia").html("");
        $.post("{$_Parametros.url}modAD/entradaArchivoCONTROL/BuscarDependenciaMET",{ pk_num_organismo:""+pk_num_organismo }, function (dato) {
            $("#dependencia").html(dato);
        });
    }

    var $urlVisualizar='{$_Parametros.url}modAD/entradaArchivoCONTROL/VerEntradaMET';
    $('#datatable1 tbody').on( 'click', '.visualizar', function () {
        $('#modalAncho').css( "width", "75%" );
        $('#formModalLabel').html($(this).attr('title'));
        $('#ContenidoModal').html("");
        $.post($urlVisualizar,{ pk_num_entrada: $(this).attr('pk_num_entrada')},function($dato){
            $('#ContenidoModal').html($dato);
        });
    });

    $("#formulario").validate({
        submitHandler: function(form) {
            var fecha_inicio = $("#fecha_inicio").val();
            var fecha_fin = $("#fecha_fin").val();
            var pk_num_organismo = $("#pk_num_organismo").val();
            var pk_num_dependencia = $("#pk_num_dependencia").val();
            var ind_estado = $("#ind_estado").val();
            var url_listar='{$_Parametros.url}modAD/entradaArchivoCONTROL/ConsultarEntradaMET';
            $.post(url_listar,{ fecha_inicio: fecha_inicio, fecha_fin: fecha_fin, pk_num_organismo: pk_num_organismo, pk_num_dependencia: pk_num_dependencia, ind_estado: ind_estado},function(respuesta_post) {
                var tabla_listado = $('#datatable1').DataTable();
                tabla_listado.clear().draw();
                if(respuesta_post != -1) {
                    for(var i=0; i<respuesta_post.length; i++) {
                        if(respuesta_post[i].ind_estado=='PR'){
                            var estado = 'PREPARADO';
                        }
                        if(respuesta_post[i].ind_estado=='AP'){
                            var estado = 'APROBADO';
                        }
                        if(respuesta_post[i].ind_estado=='AN'){
                            var estado = 'ANULADO';
                        }
                        var botonVer = '<button class="visualizar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_entrada="'+respuesta_post[i].pk_num_entrada+'" descripcion="El Usuario ha visualizado una entrada" title="Ver Entrada" ><i class="glyphicon glyphicon-search"></i></button>';
                        if(respuesta_post[i].ind_estado=='PR'){
                            var botonEditar = '<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_entrada="'+respuesta_post[i].pk_num_entrada+'" descripcion="El Usuario ha modificado una entrada" title="Modificar entrada" ><i class="fa fa-edit"></i></button>';
                        } else {
                            var botonEditar = '';
                        }
                        var botonEliminar = '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_entrada="'+respuesta_post[i].pk_num_entrada+'"  boton="si, Eliminar" descripcion="El usuario ha eliminado una entrada de documento" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar la entrada de documento?" title="Eliminar entrada"><i class="md md-delete"></i></button>';
                        tabla_listado.row.add([
                            respuesta_post[i].pk_num_entrada,
                            respuesta_post[i].cod_memo,
                            respuesta_post[i].ind_documento,
                            respuesta_post[i].fecha_entrada,
                            estado,
                            botonVer,
                            botonEditar,
                            botonEliminar

                        ]).draw()
                                .nodes()
                                .to$()
                    }
                }
            },'json');
        }
    });
</script>
