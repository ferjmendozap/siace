<div class="form floating-label">
	<div class="modal-body" >
			<div class="col-md-6 col-sm-6">
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$ent.pk_num_entrada}" size="45%" disabled="disabled" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-th-large"></i> N° de Entrada </label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$ent.ind_documento}" size="45%" disabled="disabled" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-file"></i> Documento </label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$ent.fecha_salida}" size="45%" disabled="disabled" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-calendar"></i> Fecha de Salida </label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$ent.fecha_entrada}" size="45%" disabled="disabled" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-calendar"></i> Fecha de Entrada </label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$ent.cod_memo}" size="45%" disabled="disabled" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-pencil"></i> Memorándum </label>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="form-group floating-label">
					<textarea class="form-control" rows="2" disabled="disabled">{$ent.ind_observacion}</textarea>
					<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Observación</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$ent.ind_nombre1} {$ent.ind_apellido1}" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-list"></i> {$ent.ind_estado} por:</label>
				</div>
			{foreach item=ac from=$acceso}
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$ac.fecha_modificacion}" disabled="disabled">
					<label for="regular2">última modificación:</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$ac.ind_nombre1} {$ac.ind_apellido1}" disabled="disabled">
					<label for="regular2">último usuario:</label>
				</div>
			{/foreach}
			</div>
		</div>
		<div class="modal-body">
			<div class="col-md-12 col-sm-12">
				<div class="card">
					<div class="card-body style-default-light height-1">DOCUMENTO DIGITALIZADO: {$registro.ind_descripcion}</div>
					{if $ent.num_flag_archivo==1}
                    	{if $ruta.extension=='pdf'}
							<iframe name="miIframe" frameborder="0" width="100%" height="700px" src="{$ruta.ruta}{$archivo.num_registro}/{$archivo.ind_nombre_archivo}"></iframe>
						{else}
							<br/>
							<div class="col-md-12 col-sm-12" align="center">
								<a type="button" class="btn ink-reaction btn-raised btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Descargando..." href="{$ruta.ruta}{$archivo.num_registro}/{$archivo.ind_nombre_archivo}" download="{$archivo.ind_nombre_archivo}">Descargar Archivo</a>
							</div>
						{/if}
					{else}
						<div class="card-body small-padding text-center">
							<iframe name="miIframe" frameborder="0" width="100%" height="700px" src="{$_Parametros.url}modAD/entradaArchivoCONTROL/VerExpedienteMET?num_registro={$registro.num_registro}"></iframe>
						</div>
					{/if}
					<br/><br/>
				</div>
			</div>
			<div class="col-md-12 col-sm-12">
				<div align="right">
					{if $ent.ind_estado=='PR'}
						<button type="button" class="btn btn-primary ink-reaction btn-raised"  id="accion1" pk_num_entrada="{$ent.pk_num_entrada}"><span class="md md-done"></span> Aprobar</button>
						&nbsp;
						<button type="button" class="btn btn-primary ink-reaction btn-danger" id="accion2" pk_num_entrada="{$ent.pk_num_entrada}"><span class="md md-block"></span> Anular</button>
					{/if}
					&nbsp;
					<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">

	var $url='{$_Parametros.url}modAD/entradaArchivoCONTROL/estatusMET';
	$("#accion1").click(function() {
		var $valor = 'AP';
		$.post($url, {  pk_num_entrada: $(this).attr('pk_num_entrada'), valor: $valor},function(respuesta_post){
			var tabla_listado = $('#datatable1').DataTable();
			tabla_listado.clear().draw();
			if(respuesta_post != -1) {
				for(var i=0; i<respuesta_post.length; i++) {
					var botonVer = '<button class="visualizar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_entrada="'+respuesta_post[i].pk_num_entrada+'" descripcion="El Usuario ha visualizado una entrada" title="Ver Entrada" ><i class="glyphicon glyphicon-search"></i></button>';
					var botonEliminar = '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_entrada="'+respuesta_post[i].pk_num_entrada+'"  boton="si, Eliminar" descripcion="El usuario ha eliminado una entrada de documento" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar la entrada de documento?" title="Eliminar entrada"><i class="md md-delete"></i></button>';
					if(respuesta_post[i].ind_estado=='PR'){
						var estado = 'PREPARADO';
					}
					if(respuesta_post[i].ind_estado=='AP'){
						var estado = 'APROBADO';
					}
					if(respuesta_post[i].ind_estado=='AN'){
						var estado = 'ANULADO';
					}
					tabla_listado.row.add([
						respuesta_post[i].pk_num_entrada,
						respuesta_post[i].cod_memo,
						respuesta_post[i].ind_documento,
						respuesta_post[i].fecha_entrada,
						estado,
						botonVer,
						'',
						botonEliminar

					]).draw()
							.nodes()
							.to$()
				}
				swal("Aprobado", "Entrada aprobada exitosamente", "success");
				$(document.getElementById('cerrarModal')).click();
				$(document.getElementById('ContenidoModal')).html('');
			}
		}, 'json');
	});

	$("#accion2").click(function() {
		var $valor2 = 'AN';
		$.post($url, {  pk_num_entrada: $(this).attr('pk_num_entrada'), valor: $valor2},function(respuesta_post){
			var tabla_listado = $('#datatable1').DataTable();
			tabla_listado.clear().draw();
			if(respuesta_post != -1) {
				for(var i=0; i<respuesta_post.length; i++) {
					var botonVer = '<button class="visualizar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_entrada="'+respuesta_post[i].pk_num_entrada+'" descripcion="El Usuario ha visualizado una entrada" title="Ver Entrada" ><i class="glyphicon glyphicon-search"></i></button>';
					var botonEliminar = '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_entrada="'+respuesta_post[i].pk_num_entrada+'"  boton="si, Eliminar" descripcion="El usuario ha eliminado una entrada de documento" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar la entrada de documento?" title="Eliminar entrada"><i class="md md-delete"></i></button>';
					tabla_listado.row.add([
						respuesta_post[i].pk_num_entrada,
						respuesta_post[i].cod_memo,
						respuesta_post[i].ind_documento,
						respuesta_post[i].fecha_entrada,
						respuesta_post[i].ind_estado,
						botonVer,
						'',
						botonEliminar

					]).draw()
							.nodes()
							.to$()
				}
				swal("Anulado", "Entrada anulada exitosamente", "success");
				$(document.getElementById('cerrarModal')).click();
				$(document.getElementById('ContenidoModal')).html('');
			}
		}, 'json');
	});
</script>