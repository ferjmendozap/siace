<section class="style-default-bright">
    <div class="section-body contain-lg">
        <div class="col-lg-12 contain-lg">
            <br/>
            <table id="datatable2" class="table table-striped table-hover">
                <thead>
                <tr  align="center">
                    <th><i class="glyphicon glyphicon-th-large"></i> N° de Salida</th>
                    <th><i class="glyphicon glyphicon-calendar"></i> Fecha de Salida</th>
                    <th><i class="glyphicon glyphicon-th-large"></i> Memo</th>
                    <th><i class="glyphicon glyphicon-file"></i> Documento</th>
                    <th align="center"><i class="glyphicon glyphicon-repeat"></i> Registrar Entrada</th>
                </tr>
                </thead>
                <tbody>
                {foreach item=post from=$salida}
                    <tr id="pk_num_salida{$post.pk_num_salida}">
                        <td>{$post.pk_num_salida}</td>
                        <td>{$post.fecha_salida}</td>
                        <td>{$post.cod_memo}</td>
                        <td>{$post.ind_documento}</td>
                        <td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal2" data-target="#formModal" data-keyboard="false" data-backdrop="static"  pk_num_salida="{$post.pk_num_salida}"  title="Registrar Entrada de Documentos" id="ver"><i class="glyphicon glyphicon-import"></i></button></td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
    </div>
</section>
<script type="text/javascript">

    var $urlVer='{$_Parametros.url}modAD/entradaArchivoCONTROL/NuevaEntradaMET';
    $('#datatable2 tbody').on( 'click', '.ver', function () {
        $('#modalAncho').css( "width", "75%" );
        $('#formModalLabel').html($(this).attr('title'));
        $('#ContenidoModal').html("");
        $.post($urlVer,{ pk_num_salida: $(this).attr('pk_num_salida')},function($dato){
            $('#ContenidoModal').html($dato);
        });
    });

    $(document).ready(function() {
        $('#datatable2').DataTable( {
            "language": {
                "lengthMenu": "Mostrar _MENU_ ",
                "sSearch":        "Buscar:",
                "zeroRecords": "No hay resultados",
                "info": "Mostrar _PAGE_ de _PAGES_",
                "infoEmpty": "No hay resultados",
                "infoFiltered": "(filtered from _MAX_ total records)",
                "paginate": {
                    "previous": "<",
                    "next": ">"
                }
            }
        } );
    } );

    if ( $.fn.dataTable.isDataTable( '#datatable2' ) ) {
        table = $('#datatable2').DataTable();
    }
    else {
        table = $('#datatable2').DataTable( {
            paging: true
        } );
    }

   
</script>
