<form action="{$_Parametros.url}modAD/entradaArchivoCONTROL/EditarEntradaMET" id="formAjax" method="post" class="form" role="form">
<div class="modal-body">
	<input type="hidden" value="1" name="valido" />
    <input type="hidden" value="{$sal.pk_num_salida}" name="pk_num_salida" id="pk_num_salida"/>
	{if isset($ent.pk_num_entrada)}
		<input type="hidden" value="{$ent.pk_num_entrada}" name="pk_num_entrada" id="pk_num_entrada" />
	{/if}
	<div class="col-md-6 col-sm-6">
		<div class="form-group floating-label">
			<input type="text" class="form-control" id="regular2" value="{$ent.pk_num_entrada}" size="45%" disabled="disabled" disabled="disabled">
			<label for="regular2"><i class="glyphicon glyphicon-th-large"></i> N° de Entrada </label>
		</div>
		<div class="form-group floating-label">
			<input type="text" class="form-control" id="regular2" value="{$ent.ind_documento}" size="45%" disabled="disabled" disabled="disabled">
			<label for="regular2"><i class="glyphicon glyphicon-file"></i> Documento </label>
		</div>
		<div class="form-group floating-label">
			<input type="text" class="form-control" id="regular2" value="{$ent.fecha_salida}" size="45%" disabled="disabled" disabled="disabled">
			<label for="regular2"><i class="glyphicon glyphicon-calendar"></i> Fecha de Salida </label>
		</div>
		<div class="form-group control-width-normal">
			<div class="input-group date" id="demo-date">
				<div class="input-group-content">
					<input type="text" class="form-control" id="fecha_entrada" name="fecha_entrada" value="{$ent.fecha_entrada}" onChange="fechas(this.value)">
					<label><i class="glyphicon glyphicon-calendar"></i> Fecha de Entrada</label>
				</div>
				<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			</div>
		</div><!--end .form-group -->
		<div class="form-group floating-label">
			<input type="text" class="form-control" id="regular2" value="{$ent.cod_memo}" name="cod_memo" id="cod_memo" size="45%">
			<label for="regular2"><i class="glyphicon glyphicon-pencil"></i> Memorándum </label>
		</div>
	</div>
	<div class="col-md-6 col-sm-6">
		<div class="form-group floating-label">
			<textarea class="form-control" rows="2" name="ind_observacion" id="ind_observacion">{$ent.ind_observacion}</textarea>
			<label class="control-label"> <i class="glyphicon glyphicon-pencil"></i>Observación</label>
		</div>
		<div class="form-group floating-label">
			<input type="text" class="form-control" id="regular2" value="{$ent.ind_nombre1} {$ent.ind_apellido1}" size="45%" disabled="disabled" disabled="disabled">
			<label for="regular2"><i class="glyphicon glyphicon-list"></i>{if $ent.ind_estado==PR} Preparado {/if}{if $ent.ind_estado==AP} Aprobado {/if}{if $ent.ind_estado==AN} Anulado {/if}  por</label>
		</div>
		{foreach item=ac from=$acceso}
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$ac.fecha_modificacion}" disabled="disabled">
				<label for="regular2">Última modificación:</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$ac.ind_nombre1} {$ac.ind_apellido1}" disabled="disabled">
				<label for="regular2">Último usuario:</label>
			</div>
		{/foreach}
	</div>
	<div class="col-md-12 col-sm-12">
		<div class="card">
			<div class="card-body style-default-light height-1">DOCUMENTO DIGITALIZADO: {$registro.ind_descripcion}</div>
			{if $ent.num_flag_archivo==1}
            	{if $ruta.extension=='pdf'}
					<iframe name="miIframe" frameborder="0" width="100%" height="700px" src="{$ruta.ruta}{$archivo.num_registro}/{$archivo.ind_nombre_archivo}"></iframe>
				{else}
					<br/>
					<div class="col-md-12 col-sm-12" align="center">
						<a type="button" class="btn ink-reaction btn-raised btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Descargando..." href="{$ruta.ruta}{$archivo.num_registro}/{$archivo.ind_nombre_archivo}" download="{$archivo.ind_nombre_archivo}">Descargar Archivo</a>
					</div>
				{/if}
			{else}
				<div class="card-body small-padding text-center">
					<iframe name="miIframe" frameborder="0" width="100%" height="700px" src="{$_Parametros.url}modAD/entradaArchivoCONTROL/VerExpedienteMET?num_registro={$registro.num_registro}"></iframe>
				</div>
			{/if}
			<br/><br/>
		</div>
	</div>
	<div class="col-md-12 col-sm-12">
		<div align="right">
			<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
			<button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
	$("#fecha_entrada").datepicker({
		format: 'dd/mm/yyyy',
		language: 'es',
		autoclose: true
	});
	
	function fechas(fechaEntrada)
	{
		var $url = "{$_Parametros.url}modAD/entradaArchivoCONTROL/VerificarFechaMET";
		var pk_num_salida = $("#pk_num_salida").val();
		$.post($url, { pkNumSalida: pk_num_salida, fechaEntrada: fechaEntrada},function(respuesta_post){
			  if(respuesta_post['fecha']==true) {
				  swal("Rango de fecha incorrecto", "La fecha de entrada no puede ser menor que la fecha de salida del documento");
			  }
        },'json');
	}
	
    $(document).ready(function() {
        $("#formAjax").validate({
			rules:{
				cod_memo:{
					required:true
				},
				ind_observacion:{
					required:true
				},
				fecha_entrada:{
					required:true
				}
			},
			messages:{
				cod_memo:{
					required: "Este campo es requerido"
				},
				ind_observacion:{
					required: "Este campo es requerido"
				},
				fecha_entrada: {
					required: "Este campo es requerido"
				}
			},
			submitHandler: function(form){
				$.post($(form).attr("action"), $(form).serialize(),function(dato){
				$('#pk_num_entrada'+dato['pk_num_entrada']).remove();
				$(document.getElementById('datatable1')).append('<tr id="pk_num_entrada'+dato['pk_num_entrada']+'">' +
						'<td>'+dato['pk_num_entrada']+'</td><td>'+dato['cod_memo']+'</td><td>'+dato['ind_documento']+'</td><td>'+dato['fecha_entrada']+'</td><td>'+dato['ind_estado']+'</td><td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha visualizado una entrada" ' +
						'titulo="Visualizar entrada de documento" data-toggle="modal" data-target="#formModal" pk_num_entrada="'+dato['pk_num_entrada']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>' +
						'<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El Usuario ha modificado una entrada" ' +
				'titulo="Modificar entrada de documento" data-toggle="modal" data-target="#formModal" pk_num_entrada="'+dato['pk_num_entrada']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>' +
						'<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado una entrada de documento" titulo="¿Estás Seguro?" mensaje="¿Estás seguro de eliminar la entrada?" boton="si, Eliminar" pk_num_entrada="'+dato['pk_num_entrada']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td></tr>');
				swal("Entrada Modificada", "Entrada modificada exitosamente", "success");
				$(document.getElementById('cerrarModal')).click();
				$(document.getElementById('ContenidoModal')).html('');
            },'json');
        }
    });    
});
</script>


