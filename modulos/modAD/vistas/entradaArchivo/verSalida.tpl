<div class="form floating-label">
	<div class="modal-body" >
		{foreach item=sal from=$salida}
			<div class="col-md-6 col-sm-6">
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$sal.ind_descripcion_almacen}" disabled="disabled">
					<label for="regular2">Almacén</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$sal.ind_pasillo}" disabled="disabled">
					<label for="regular2">Pasillo</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$sal.ind_descripcion_estante}" disabled="disabled">
					<label for="regular2">Estante</label>
				</div>
				<div class="form-group floating-label">
					<textarea class="form-control" rows="2">{$sal.ind_dependencia}</textarea>
					<label class="control-label">Dependencia</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$sal.ind_descripcion_documento}" disabled="disabled">
					<label for="regular2">Tipo de documento</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$sal.pk_num_caja}-{$sal.ind_descripcion_caja}" disabled="disabled">
					<label for="regular2">Caja</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$sal.num_registro}-{$sal.ind_documento}" disabled="disabled">
					<label for="regular2">Documento</label>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$sal.cod_memo}" disabled="disabled">
					<label for="regular2">Número de memorándum</label>
				</div>
				<div class="form-group floating-label">
					<textarea class="form-control" rows="2">{$sal.ind_motivo}</textarea>
					<label class="control-label">Motivo</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$sal.fecha_salida}" disabled="disabled">
					<label for="regular2">Fecha de salida del documento</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$sal.ind_nombre1} {$sal.ind_apellido1}" disabled="disabled">
					<label for="regular2">Preparado por:</label>
				</div>
				{foreach item=ac from=$acceso}
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$ac.fecha_modificacion}" disabled="disabled">
					<label for="regular2">última modificación:</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$ac.ind_nombre1} {$ac.ind_apellido1}" disabled="disabled">
					<label for="regular2">último usuario:</label>
				</div>
				{/foreach}
				<div align="right">
					<button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="md md-done"></span> Aprobar</button>
					&nbsp;&nbsp;
					<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado la visualización de la entrada" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
				</div>
			</div>
		{/foreach}
	</div>
	<div class="modal-body" >
		<div class="col-md-12 col-sm-12">
			<div class="card">
				<div class="card-body style-default-light height-1">Registro de Entrada de Documento</div>
				<div class="card-body small-padding text-center">

				</div>
			</div>
		</div>
	</div>
	<div class="modal-body" >
		<div class="col-md-12 col-sm-12">
			<div align="right">
				<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado el registro de la entrada" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>
