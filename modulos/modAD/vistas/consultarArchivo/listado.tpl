<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="section-header">
                        <h2 class="text-primary">CONSULTA DE ARCHIVOS </h2>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="col-sm-1">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkDocumento">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="documento"
                                               class="control-label" style="margin-top: 10px;"> Tipo de Documento:</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <select class="form-control select2"
                                                disabled="disabled"
                                                id="doc"
                                                data-placeholder="Seleccione Organismo">
                                            <option value="">Seleccione...</option>
                                            {foreach item=doc from=$documento}
                                                    <option value="{$doc.pk_num_miscelaneo_detalle}"> {$doc.ind_nombre_detalle} </option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="col-sm-1 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkDescripcion">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="descripcion"
                                               class="control-label" style="margin-top: 10px;"> Descripcion:</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control"
                                                   disabled="disabled"
                                                   id="des"
                                                   value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="col-sm-1">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkEstante">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="esta"
                                               class="control-label" style="margin-top: 10px;"> Estante:</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <select class="form-control select2"
                                                disabled="disabled"
                                                id="est"
                                                data-placeholder="Seleccione Dependencia">
                                            <option value="">Seleccione...</option>}
                                            {foreach item=i from=$selectEstante}
                                                <option value="{$i.id}">{$i.nombre}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="col-sm-1 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkResumen">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="resumen"
                                               class="control-label" style="margin-top: 10px;"> Resumen:</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control"
                                                   disabled="disabled"
                                                   id="res"
                                                   value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="col-sm-1">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkTramo">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="tramo"
                                               class="control-label" style="margin-top: 10px;"> Tramo:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <select class="form-control select2"
                                                disabled="disabled"
                                                id="trm"
                                                data-placeholder="Seleccione Tramo">
                                            <option value="">Seleccione...</option>
                                            {foreach item=trm from=$tramo}
                                                <option value="{$trm.pk_num_miscelaneo_detalle}"> {$trm.ind_nombre_detalle} </option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="col-sm-2">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkFecha">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="pago"
                                               class="control-label" style="margin-top: 10px;"> Fecha:</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="desde"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Desde"
                                               disabled="disabled"
                                               readonly>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="hasta"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Hasta"
                                               disabled="disabled"
                                               readonly>
                                    </div>
                                </div>

                            </div>

                            <div align="center">
                                <button class="buscar logsUsuario btn ink-reaction btn-raised btn-primary">
                                    BUSCAR
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="section-body contain-lg" id="listadoConsulta">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="datatable1" class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>Foto.</th>
                                            <th>Visitante</th>
                                            <th>Organo/Ente Externo</th>
                                            <th>Destino</th>
                                            <th>Entradas</th>
                                            <th>Salida</th>
                                            <th>Accion</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        /// Complementos
        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});

        $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

        //busqueda segun el filtro
        $('.buscar').click(function(){
            var url='{$_Parametros.url}modAD/consultarArchivoCONTROL/ArchivosMET';
            $.post(url,{ documento: $('#doc').val(), estante: $('#est').val(), tramo: $('#trm').val(), descripcion: $('#des').val(), resumen: $('#res').val(), desde: $('#desde').val(), hasta: $('#hasta').val() }, function(dato){
                $('#listadoConsulta').html('');
                $('#listadoConsulta').html(dato);
            });
        });


        //habilitar y deshabilitar campos del filtro
        $('#checkDocumento').click(function () {

            if(this.checked){
                $('#doc').attr('disabled', false);
            }else{
                $('#doc').attr('disabled', true);
                $(document.getElementById('doc')).val("");
            }
        });$('#checkTramo').click(function () {

            if(this.checked){
                $('#trm').attr('disabled', false);
            }else{
                $('#trm').attr('disabled', true);
                $(document.getElementById('trm')).val("");
            }
        });
        $('#checkEstante').click(function () {
            if(this.checked){
                $('#est').attr('disabled', false);
            }else{
                $('#est').attr('disabled', true);
                $(document.getElementById('est')).val("");
            }
        });
        $('#checkDescripcion').click(function () {
            if(this.checked){
                $('#des').attr('disabled', false);
            }else{
                $('#des').attr('disabled', true);
                $(document.getElementById('des')).val("");

            }
        });

        $('#checkResumen').click(function () {
            if(this.checked){
                $('#res').attr('disabled', false);
            }else{
                $('#res').attr('disabled', true);
                $(document.getElementById('res')).val("");

            }
        });


        $('#checkFecha').click(function () {
            if(this.checked){
                $('#desde').attr('disabled', false);
                $('#hasta').attr('disabled', false);
            }else{
                $('#desde').attr('disabled', true);
                $(document.getElementById('desde')).val("");
                $('#hasta').attr('disabled', true);
                $(document.getElementById('hasta')).val("");

            }
        });
    });

</script>