<div class="section-body contain-lg">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>N° de Documento</th>
                        <th>Descripcion</th>
                        <th>Resumen</th>
                        <th>Tipo de Documento</th>
                        <th>Estante</th>
                        <th>Tramo</th>
                        <th>Accion</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=i from=$listado}
                        <tr id="id">
                            <td><label></label></td>
                            <td><label>{$i.ind_documento}</label></td>
                            <td><label>{$i.ind_descripcion}</label></td>
                            <td><label>{$i.ind_resumen}</label></td>
                            <td><label>{$i.documento}</label></td>
                            <td><label>{$i.ind_descripcion_estante}</label></td>
                            <td><label>{$i.tramo}</label></td>
                            <td align="center">
                             <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" titulo="Documento Escaneado" pk_num_registro_documento="{$i.pk_num_registro_documento}" fecArchivo="{$i.fec_anio}" num_flag_archivo="{$i.num_flag_archivo}" ind_nombre_archivo="{$i.ind_nombre_archivo}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" idVisita="{$i.pk_num_registro_visita}" title="Ver Documento"
                                      titulo="<i class='icm icm-calculate2'></i>">
                                        <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                             </button>

                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="3">
                            <div align="rigth">
                                <button class="pdf logsUsuario btn ink-reaction btn-raised btn-primary" data-toggle="modal" data-target="#formModal"
                                        data-keyboard="false" data-backdrop="static" titulo="<i class='md md-print'></i> Reporte de Visitas">
                                    EXPORTAR PDF
                                </button>
                            </div>
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function() {
                //exportar PDF
                $('.pdf').click(function(){
                    $('#modalAncho').css("width", "80%");
                    var documento = $('#doc').val();       var descripcion = $('#des').val();
                    var estante = $('#est').val();         var tramo = $('#trm').val();
                    var hasta = $('#hasta').val();         var desde = $('#desde').val();
                    var resumen = $('#res').val();

                    var url='{$_Parametros.url}modAD/consultarArchivoCONTROL/imprimirReporteMET/?documento='+documento+'&'+'descripcion='+descripcion+'&'+'tramo='+tramo+'&'+'estante='+estante+'&'+'resumen='+resumen+'&'+'desde='+desde+'&'+'hasta='+hasta;
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $.post( url,{ },function(dato){
                        $('#ContenidoModal').html('<iframe frameborder="0" src="'+url+'" width="100%" height="540px"></iframe>');
                    });
                });

                $('.ver').click(function(){
                    var fecArchivo=$(this).attr('fecArchivo');
                    var pk_num_registro_documento=$(this).attr('pk_num_registro_documento');
                    var num_flag_archivo=$(this).attr('num_flag_archivo');
                    var ind_nombre_archivo=$(this).attr('ind_nombre_archivo');

                   var url = "{$_Parametros.url}modAD/registroArchivoCONTROL/ExpedienteMET/"+pk_num_registro_documento+"/"+fecArchivo+"/"+num_flag_archivo+"/"+ind_nombre_archivo;
                    $('#formModalLabel').html($(this).attr('titulo'));

                    $('#ContenidoModal').html('<iframe frameborder="0" src="'+url+'" width="100%" height="540px"></iframe>');
                });



            });
        </script>

