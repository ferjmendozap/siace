<form id="formAjax" action="{$_Parametros.url}modAD/pasilloCONTROL/NuevoPasilloMET" class="form floating-label" novalidate="novalidate">
<input type="hidden" name="valido" value="1" />
<div class="modal-body">
	<div class="form-group floating-label">
		<select id="pk_num_almacen" name="pk_num_almacen" class="form-control">
			<option value="">&nbsp;</option>
			{foreach item=us from=$almacen}
				<option value="{$us.pk_num_almacen}">{$us.ind_descripcion_almacen}</option>
			{/foreach}
		</select>
		<label for="pk_num_almacen"><i class="glyphicon glyphicon-home"></i> Almacén</label>
	</div>

	<div class="form-group floating-label">
		<input type="text" class="form-control"  name="ind_pasillo" id="ind_pasillo">
		<label for="ind_pasillo"><i class="glyphicon glyphicon-th"></i> Pasillo </label>
	</div>

	<div class="form-group floating-label">
		<input type="text" class="form-control"  name="ind_descripcion_pasillo" id="ind_descripcion_pasillo">
		<label for="ind_descripcion_pasillo"><i class="glyphicon glyphicon-pencil"></i> Descripción </label>
	</div>

	<div align="right">
		<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado el registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
		<button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
	</div>
</form>
<script type="text/javascript">
	$(document).ready(function () {
		//validation rules
		$("#formAjax").validate({
			rules:{
				pk_num_almacen: {
					required: true
				},
				ind_pasillo:{
					required:true,
					remote:{
						url:"{$_Parametros.url}modAD/pasilloCONTROL/VerificarPasilloMET",
						type:"post",
						data: {
							pk_num_almacen: function(){
								return $( "#pk_num_almacen" ).val();
							}
						}
					}
				},
				ind_descripcion_pasillo:{
					required:true
				}
			},
			messages:{
				ind_pasillo:{
					required: "Ingrese el pasillo",
					remote: jQuery.validator.format("Ya se encuentra registrado en el sistema")
				},
				ind_descripcion_pasillo: {
					required: "Ingrese descripcion del pasillo"
				},
				pk_num_almacen: {
					required: "Seleccione el almacén"
				}
			},
			submitHandler: function(form) {
				$.post($(form).attr('action'), $(form).serialize(),function(dato){
					$(document.getElementById('datatable1')).append('<tr id="pk_num_pasillo'+dato['pk_num_pasillo']+'"><td>'+dato['ind_pasillo']+'</td><td>'+dato['ind_descripcion_pasillo']+'</td><td>'+dato['ind_descripcion_almacen']+'</td><td>'+dato['ind_ubicacion_almacen']+'</td>' +
							'{if in_array('AD-01-02-02-03-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha visualizado el pasillo" titulo="Visualizar Pasillo" title="Visualizar Pasillo" data-toggle="modal" data-target="#formModal" pk_num_pasillo="'+dato['pk_num_pasillo']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-02-02-04-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha modificado el pasillo" titulo="Modificar Pasillo" title="Modificar Pasillo" data-toggle="modal" data-target="#formModal" pk_num_pasillo="'+dato['pk_num_pasillo']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-02-02-05-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado un pasillo" titulo="¿Estás Seguro?" title="Eliminar" mensaje="¿Estás seguro de eliminar el pasillo?" boton="si, Eliminar" pk_num_pasillo="'+dato['pk_num_pasillo']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td>{/if}</tr>');
					swal("Pasillo Guardado", "Pasillo guardado satisfactoriamente", "success");
					$(document.getElementById('cerrarModal')).click();
					$(document.getElementById('ContenidoModal')).html('');
				},'json');
			}
		});
	});
</script>
