<form action="{$_Parametros.url}modAD/pasilloCONTROL/EditarPasilloMET" id="formAjax" method="post" class="form" role="form">
	  <div class="modal-body">
		<input type="hidden" value="1" name="valido" />
		{if isset($form.pk_num_pasillo)}
			<input type="hidden" value="{$form.pk_num_pasillo}" name="pk_num_pasillo" id="pk_num_pasillo" />
		{/if}
		  <div class="form-group floating-label">
			  <select id="pk_num_almacen" name="pk_num_almacen" class="form-control">
				  <option value="">&nbsp;</option>
				  {foreach item=per from=$almacen}
					  {if $per.pk_num_almacen == $form.pk_num_almacen}
						  <option selected value="{$per.pk_num_almacen}">{$per.ind_descripcion_almacen}</option>
					  {else}
						  <option value="{$per.pk_num_almacen}">{$per.ind_descripcion_almacen}</option>
					  {/if}
				  {/foreach}
			  </select>
			  <label for="select2"><i class="glyphicon glyphicon-home"></i> Almacén</label>
		  </div>
		<div class="form-group floating-label">
			<input type="text" class="form-control" id="regular2" value="{$form.ind_pasillo}" name="ind_pasillo" id="ind_pasillo">
			<label for="regular2"><i class="glyphicon glyphicon-th"></i> Pasillo</label>
		</div>
		<div class="form-group floating-label">
			<input type="text" class="form-control" id="regular2" value="{$form.ind_descripcion_pasillo}" name="ind_descripcion_pasillo" id="ind_descripcion_pasillo">
			<label for="regular2"><i class="glyphicon glyphicon-pencil"></i> Descripción</label>
		</div>
	<div class="form-group floating-label">
		<input type="text" class="form-control" id="regular2" value="{$form.fecha}" disabled="disabled">
		<label for="regular2">última fecha de modificación</label>
	</div>
	<div class="form-group floating-label">
		<input type="text" class="form-control" id="regular2" value="{$form.nombreCompleto}" disabled="disabled">
		<label for="regular2">último usuario</label>
	</div>
		  <div align="right">
			  <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado el registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
			  <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
		  </div>
</form>
<script type="text/javascript">
	$(document).ready(function() {
		$("#formAjax").validate({
			rules:{
				pk_num_almacen:{
					required:true
				},
				ind_pasillo:{
					required:true,
					remote:{
						url:"{$_Parametros.url}modAD/pasilloCONTROL/VerificarPasilloMET",
						type:"post",
						data: {
							pk_num_almacen: function(){
								return $( "#pk_num_almacen" ).val();
							},
							pk_num_pasillo: function(){
								return $( "#pk_num_pasillo" ).val();
							}
						}
					}
				},
				ind_descripcion_pasillo:{
					required:true
				}
			},
			messages:{
				pk_num_almacen:{
					required: "Este campo es requerido"
				},
				ind_pasillo:{
					required: "Este campo es requerido",
					remote: jQuery.validator.format("Ya se encuentra registrado en el sistema")
				},
				ind_descripcion_pasillo:{
					required: "Este campo es requerido"
				}
			},
			submitHandler: function(form){
				$.post($(form).attr("action"), $(form).serialize(),function(dato){
					$('#pk_num_pasillo'+dato['pk_num_pasillo']).remove();
					$(document.getElementById('datatable1')).append('<tr id="pk_num_pasillo'+dato['pk_num_pasillo']+'"><td>'+dato['ind_pasillo']+'</td><td>'+dato['ind_descripcion_pasillo']+'</td>' + '<td>'+dato['ind_descripcion_almacen']+'</td><td>'+dato['ind_ubicacion_almacen']+'</td>' +
							'{if in_array('AD-01-02-02-03-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha visualizado un pasillo" ' +
							'titulo="Visualizar pasillo" title="Visualizar pasillo" data-toggle="modal" data-target="#formModal" pk_num_pasillo="'+dato['pk_num_pasillo']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-02-02-04-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha modificado un pasillo" ' +
							'titulo="Modificar pasillo" title="Modificar pasillo" data-toggle="modal" data-target="#formModal" pk_num_pasillo="'+dato['pk_num_pasillo']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-02-02-05-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado un pasillo" titulo="¿Estás Seguro?" title="Eliminar" mensaje="¿Estás seguro de eliminar el pasillo?" boton="si, Eliminar" pk_num_almacen="'+dato['pk_num_pasillo']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td>{/if}</tr>');
					swal("Pasillo Modificado", "Pasillo modificado exitosamente", "success");
					$(document.getElementById('cerrarModal')).click();
					$(document.getElementById('ContenidoModal')).html('');
				},'json');
			}
		});
	});
</script>


