<br/>
<section class="style-default-bright">
    <div class="col-lg-12">
		<div class="card style-default-bright">
			<div class="card-head"><h2 class="text-primary">&nbsp;Pasillo</h2></div>
		</div>
	</div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th><i class="glyphicon glyphicon-th-large"></i> N° de Pasillo</th>
                            <th><i class="glyphicon glyphicon-th-large"></i> Pasillo</th>
                            <th><i class="glyphicon glyphicon-pencil"></i> Descripción</th>
                            <th><i class="glyphicon glyphicon-home"></i> Almacén</th>
                            <th><i class="md md-location-searching"> Ubicación</th>
                            {if in_array('AD-01-02-02-03-V',$_Parametros.perfil)}<th width="40">Ver</th>{/if}
                            {if in_array('AD-01-02-02-04-M',$_Parametros.perfil)}<th width="50">Editar</th>{/if}
                            {if in_array('AD-01-02-02-05-E',$_Parametros.perfil)}<th width="70">Eliminar</th>{/if}
                        </tr>
                    </thead>
                        <tbody>
                            {foreach item=post from=$_PruebaPost}
                            <tr id="pk_num_pasillo{$post.pk_num_pasillo}">
                            <td>{$post.pk_num_pasillo}</td>
                            <td>{$post.ind_pasillo}</td>
                            <td>{$post.ind_descripcion_pasillo}</td>
                            <td>{$post.ind_descripcion_almacen}</td>
                            <td>{$post.ind_ubicacion_almacen}</td>
                                {if in_array('AD-01-02-02-03-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_pasillo="{$post.pk_num_pasillo}" title="Ver pasillo" titulo="Ver pasillo" id="ver"><i class="glyphicon glyphicon-search"></i></button></td>{/if}
                                {if in_array('AD-01-02-02-04-M',$_Parametros.perfil)}<td align="center"> <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_pasillo="{$post.pk_num_pasillo}" descipcion="El usuario ha modificado un pasillo" titulo="Modificar pasillo" title="Modificar pasillo" ><i class="fa fa-edit"></i></button></td>{/if}
                                {if in_array('AD-01-02-02-05-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_pasillo="{$post.pk_num_pasillo}"  boton="si, Eliminar" descipcion="El usuario ha eliminado un pasillo" titulo="¿Estás Seguro?" title="Eliminar" mensaje="¿Desea eliminar el pasillo?" title="Eliminar pasillo"><i class="md md-delete"></i></button></td>{/if}
                            </tr>
                            {/foreach}
                        </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="7">
                            {if in_array('AD-01-02-02-02-N',$_Parametros.perfil)}<button  class="logsUsuario btn ink-reaction btn-raised btn-info" descripcion="El usuario ha creado un pasillo" data-toggle="modal" data-target="#formModal" titulo="Registrar nuevo pasillo" id="nuevo"><i class="md md-create"></i> Nuevo pasillo</button>{/if}
                            &nbsp;
                            {if in_array('AD-01-02-02-06-R',$_Parametros.perfil)}<button class="logsUsuario btn ink-reaction btn-raised btn-primary" id="reportePasillo" descripcion="Generar Reporte de Pasillos" data-toggle="modal" data-target="#formModal" titulo="Listado de Pasillos">Reporte de Pasillos</button>{/if}
                        </th>
                    </tr>
                    </tfoot>
                    </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
            $(document).ready(function() {

                var $url='{$_Parametros.url}modAD/pasilloCONTROL/NuevoPasilloMET';
                var $url_modificar='{$_Parametros.url}modAD/pasilloCONTROL/EditarPasilloMET';
                $('#nuevo').click(function(){
					$('#modalAncho').css( "width", "45%" );
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $('#ContenidoModal').html("");
                    $.post($url,'',function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });

                $('#datatable1 tbody').on( 'click', '.modificar', function () {
		    $('#modalAncho').css( "width", "45%" );
                    $('#formModalLabel').html($(this).attr('title'));
                    $('#ContenidoModal').html("");
                    $.post($url_modificar,{ pk_num_pasillo: $(this).attr('pk_num_pasillo')},function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });
                $('#datatable1 tbody').on( 'click', '.eliminar', function () {

                    var pk_num_pasillo=$(this).attr('pk_num_pasillo');
                    swal({
                        title: $(this).attr('titulo'),
                        text: $(this).attr('mensaje'),
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: $(this).attr('boton'),
                        closeOnConfirm: false
                    }, function(){
                        var $url='{$_Parametros.url}modAD/pasilloCONTROL/EliminarPasilloMET';
                        $.post($url, { pk_num_pasillo: pk_num_pasillo},function($dato){
                            if($dato['status']=='OK'){
                                $(document.getElementById('pk_num_pasillo'+$dato['pk_num_pasillo'])).html('');
                                swal("Eliminado!", "El pasillo fue eliminado.", "success");
                                $('#cerrar').click();
                            }
                        },'json');
                    });
                });
            });
            $(document).ready(function() {
                $('#reportePasillo').click(function () {
                    $('#modalAncho').css( 'width', '95%' );
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $('#ContenidoModal').html("");
                    $('#ContenidoModal').html("<iframe src='{$_Parametros.url}modAD/pasilloCONTROL/ReportePasilloMET' border='1' width='100%' height='440px'></iframe>");
                });
            });
            var $urlVer='{$_Parametros.url}modAD/pasilloCONTROL/VerPasilloMET';
            $('#datatable1 tbody').on( 'click', '.ver', function () {
                $('#modalAncho').css( "width", "45%" );
                $('#formModalLabel').html($(this).attr('title'));
                $('#ContenidoModal').html("");
                $.post($urlVer,{ pk_num_pasillo: $(this).attr('pk_num_pasillo')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });
</script>
