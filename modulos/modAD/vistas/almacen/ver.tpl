<div class="form floating-label">
	<div class="modal-body" >
		{foreach item=alm from=$almacen}
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$alm.pk_num_almacen}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-th-large"></i> N° de Almacén </label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$alm.ind_descripcion_almacen}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-pencil"></i> Descripción</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$alm.ind_ubicacion_almacen}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2"><i class="md-location-searching"></i> Ubicación</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$alm.ind_nombre1} {$alm.ind_apellido1}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2"><i class="md md-person"></i> Encargado</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$alm.fecha}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2">Última modificación</label>
			</div>
		{/foreach}
		{foreach item=us from=$usuario}
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$us.ind_nombre1} {$us.ind_apellido1}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2">Último usuario</label>
		{/foreach}
</div>
<div align="right">
				<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El usuario ha regresado al listado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cerrar</button>
			</div>
</div>