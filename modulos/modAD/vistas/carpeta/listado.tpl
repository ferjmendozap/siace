<br/>
<section class="style-default-bright">
    <div class="col-lg-12">
		<div class="card style-default-bright">
			<div class="card-head"><h2 class="text-primary">&nbsp;Carpeta</h2></div>
		</div>
	</div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                        <tr  align="center">
                            <th><i class="glyphicon glyphicon-inbox"></i> Id carpeta</th>
                            <th><i class="md md-folder-open"></i> Número-Dependencia-Año</th>
                            <th><i class="md md-attach-file"></i> Tipo de Documento</th>
                            <th><i class="glyphicon glyphicon-pencil"></i> Descripción</th>
                            {if in_array('AD-01-02-05-03-V',$_Parametros.perfil)}<th width="40"> Ver</th>{/if}
                            {if in_array('AD-01-02-05-04-M',$_Parametros.perfil)}<th width="50">Editar</th>{/if}
                            {if in_array('AD-01-02-05-05-E',$_Parametros.perfil)}<th width="70">Eliminar</th>{/if}
                        </tr>
                    </thead>
                        <tbody>
                            {foreach item=carpeta from=$listarCarpeta}
                            <tr id="pk_num_carpeta{$carpeta.pk_num_carpeta}">
                            <td>{$carpeta.pk_num_carpeta}</td>
                            <td>{$carpeta.pk_num_carpeta}-{$carpeta.ind_dependencia}-{$carpeta.fec_anio}</td>
                            <td>{$carpeta.ind_nombre_detalle}</td>
                            <td>{$carpeta.ind_descripcion_carpeta}</td>
                            {if in_array('AD-01-02-05-03-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_carpeta="{$carpeta.pk_num_carpeta}" titulo="Ver carpeta" title="Ver carpeta" id="ver"><i class="glyphicon glyphicon-search"></i></button></td>{/if}
                            {if in_array('AD-01-02-05-04-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_carpeta="{$carpeta.pk_num_carpeta}" descipcion="El usuario ha modificado una carpeta" title="Modificar carpeta" titulo="Modificar carpeta" ><i class="fa fa-edit"></i></button></td>{/if}
                            {if in_array('AD-01-02-05-05-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_carpeta="{$carpeta.pk_num_carpeta}"  boton="si, Eliminar" descipcion="El usuario ha eliminado una carpeta" titulo="¿Estás Seguro?" title="Eliminar carpeta" mensaje="¿Desea eliminar el almacén?" title="Eliminar almacén"><i class="md md-delete"></i></button></td>{/if}
                        </tr>
                            {/foreach}
                        </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="6">
                            {if in_array('AD-01-02-05-02-N',$_Parametros.perfil)}<button  class="logsUsuario btn ink-reaction btn-raised btn-info" descripcion="El usuario ha registrado una caja" data-toggle="modal" data-target="#formModal" titulo="Registrar nueva carpeta" id="nuevo"><i class="md md-create"></i>Nueva Carpeta</button>{/if}
                            &nbsp;
                            {if in_array('AD-01-02-05-06-R',$_Parametros.perfil)}<button  class="logsUsuario btn ink-reaction btn-raised btn-primary" id="reporteCarpeta" descripcion="Generar Reporte de Carpeta" data-toggle="modal" data-target="#formModal" titulo="Listado de Carpetas">Reporte de Carpeta</button>{/if}
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
            $(document).ready(function() {

                var $url='{$_Parametros.url}modAD/carpetaCONTROL/NuevaCarpetaMET';
                var $url_modificar='{$_Parametros.url}modAD/carpetaCONTROL/EditarCarpetaMET';
                $('#nuevo').click(function(){
					$('#modalAncho').css( "width", "75%" );
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $('#ContenidoModal').html("");
                    $.post($url,'',function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });



                $('#datatable1 tbody').on( 'click', '.modificar', function () {
		    $('#modalAncho').css( "width", "75%" );
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $('#ContenidoModal').html("");
                    $.post($url_modificar,{ pk_num_carpeta: $(this).attr('pk_num_carpeta')},function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });
                $('#datatable1 tbody').on( 'click', '.eliminar', function () {

                    var pk_num_carpeta=$(this).attr('pk_num_carpeta');
                    swal({
                        title: $(this).attr('titulo'),
                        text: $(this).attr('mensaje'),
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: $(this).attr('boton'),
                        closeOnConfirm: false
                    }, function(){
                        var $url='{$_Parametros.url}modAD/carpetaCONTROL/EliminarCarpetaMET';
                        $.post($url, { pk_num_carpeta: pk_num_carpeta},function($dato){
                            if($dato['status']=='OK'){
                                $(document.getElementById('pk_num_carpeta'+$dato['pk_num_carpeta'])).html('');
                                swal("Eliminado!", "La carpeta fue eliminada.", "success");
                                $('#cerrar').click();
                            }
                        },'json');
                    });
                });
            });
    $(document).ready(function() {
        $('#reporteCarpeta').click(function () {
            $('#modalAncho').css( 'width', '95%' );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $('#ContenidoModal').html("<iframe src='{$_Parametros.url}modAD/carpetaCONTROL/ReporteCarpetaMET' border='1' width='100%' height='440px'></iframe>");
        });
    });
            var $urlVer='{$_Parametros.url}modAD/carpetaCONTROL/VerCarpetaMET';
            $('#datatable1 tbody').on( 'click', '.ver', function () {
                $('#modalAncho').css( "width", "75%" );
                $('#formModalLabel').html($(this).attr('title'));
                $('#ContenidoModal').html("");
                $.post($urlVer,{ pk_num_carpeta: $(this).attr('pk_num_carpeta')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });
</script>
