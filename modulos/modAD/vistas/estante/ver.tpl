<div class="form floating-label">
	<div class="modal-body" >
		{foreach item=est from=$estante}
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$est.pk_num_estante}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-th-list"></i> N° de Estante</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$est.ind_descripcion_estante}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-pencil"></i> Descripción</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$est.ind_pasillo}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-th"></i> Pasillo</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$est.ind_descripcion_almacen}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-home"></i> Almacén</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$est.fecha}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2">Última modificación</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$est.ind_nombre1} {$est.ind_apellido1}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2">Último usuario</label>
			</div>
		{/foreach}
	<div align="right">
		<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El usuario ha regresado al listado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cerrar</button>
	</div>
</div>