<form id="formAjax" action="{$_Parametros.url}modAD/estanteCONTROL/NuevoEstanteMET" class="form floating-label" novalidate="novalidate">
	<input type="hidden" name="valido" value="1" />
	<div class="modal-body">
		<div class="form-group floating-label">
			<select id="pk_num_almacen" name="pk_num_almacen" class="form-control" onchange="cargarPasillo(this.value)">
				<option value="">&nbsp;</option>
				{foreach item=us from=$almacen}
					<option value="{$us.pk_num_almacen}">{$us.ind_descripcion_almacen}</option>
				{/foreach}
			</select>
			<label for="pk_num_almacen"><i class="glyphicon glyphicon-home"></i> Almacén</label>
		</div>
		<div id="pasillo">
			<div class="form-group floating-label">
			<select class="form-control">
				<option value="">&nbsp;</option>
			</select>
			<label for="pk_num_pasillo"><i class="glyphicon glyphicon-th"></i> Pasillo</label>
			</div>
		</div>
		<div class="form-group floating-label">
			<input type="text" class="form-control"  name="ind_descripcion_estante" id="ind_descripcion_estante">
			<label for="ind_descripcion_estante"><i class="md md-create"></i> Descripción </label>
		</div>
		<div align="right">
			<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado el registro de un nuevo estante" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
			<button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
		</div>
</form>
<script type="text/javascript">
	$(document).ready(function () {
		//validation rules
		$("#formAjax").validate({
			rules:{
				pk_num_almacen:{
					required:true
				},
				pk_num_pasillo:{
					required:true
				},
				ind_descripcion_estante:{
					required:true,
					remote:{
						url:"{$_Parametros.url}modAD/estanteCONTROL/VerificarEstanteMET",
						type:"post",
						data: {
							pk_num_almacen: function(){
								return $( "#pk_num_almacen" ).val();
							},
							pk_num_pasillo: function(){
								return $( "#pk_num_pasillo" ).val();
							},
						}
					}
				}
			},
			messages:{
				pk_num_almacen:{
					required: "Este campo es requerido"
				},
				pk_num_pasillo:{
					required: "Este campo es requerido"
				},
				ind_descripcion_estante: {
					required: "Este campo es requerido",
					remote: jQuery.validator.format("Ya se encuentra registrado en el sistema")
				}
			},
			submitHandler: function(form) {
				$.post($(form).attr('action'), $(form).serialize(),function(dato){
					$(document.getElementById('datatable1')).append('<tr id="pk_num_estante'+dato['pk_num_estante']+'"><td>'+dato['pk_num_estante']+'</td><td>'+dato['ind_descripcion_estante']+'</td><td>'+dato['ind_pasillo']+'</td><td>'+dato['ind_descripcion_almacen']+'</td>' +
							'{if in_array('AD-01-02-03-03-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha visualizado un estante" titulo="Ver Estante" title="Ver Estante" data-toggle="modal" data-target="#formModal" pk_num_estante="'+dato['pk_num_estante']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-02-03-04-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha modificado el estante" titulo="Modificar Estante" title="Modificar Estante" data-toggle="modal" data-target="#formModal" pk_num_estante="'+dato['pk_num_estante']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-02-03-05-E',$_Parametros.perfil)}<td align="center"> <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado un estante" title="Eliminar" titulo="¿Estás Seguro?" mensaje="¿Estas seguro de eliminar el estante?" boton="si, Eliminar" pk_num_estante="'+dato['pk_num_estante']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td>{/if}</tr>');
					swal("Estante Guardado", "Estante guardado satisfactoriamente", "success");
					$(document.getElementById('cerrarModal')).click();
					$(document.getElementById('ContenidoModal')).html('');
				},'json');
			}
		});
	});
	function cargarPasillo(idAlmacen) {
		$("#pasillo").html("");
		$.post("{$_Parametros.url}modAD/estanteCONTROL/BuscarPasilloMET",{ idAlmacen:""+idAlmacen }, function (dato) {
			$("#pasillo").html(dato);
		});
	}
</script>
