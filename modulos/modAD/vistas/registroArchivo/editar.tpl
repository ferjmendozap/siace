<form action="{$_Parametros.url}modAD/registroArchivoCONTROL/EditarArchivoMET" id="formAjax" method="post" class="form" role="form">
	<input type="hidden" value="1" name="valido" />
	{if isset($form.pk_num_registro_documento)}
		<input type="hidden" value="{$form.pk_num_registro_documento}" name="pk_num_registro_documento" id="pk_num_registro_documento" />
		<input type="hidden" value="{$form.num_flag_archivo}" id="valorFormulario" />

	{/if}
	<div class="modal-body">
		<div id="rootwizard1" class="form-wizard form-wizard-horizontal">
			<form class="form floating-label">
				<div class="form-wizard-nav">
					<div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
					<ul class="nav nav-justified">
						<li class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">Datos de Registro</span></a></li>
						{if $ver !=1 }
						<li><a href="#tab2"></a></li>
						{/if}
						<li><a href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">Imagen</span></a></li>
						<li><a href="#tab3" data-toggle="tab"><span class="step">3</span> <span class="title">Expediente</span></a></li>
					</ul>
				</div><!--end .form-wizard-nav -->
				<div class="tab-content clearfix">
					<div class="tab-pane active" id="tab1">
						<br/>
						<h4 class="text-primary text-center">Datos de Registro</h4>
						<br/>
						<div class="col-md-6 col-sm-6">
							<div class="card">
								<div class="card-body">
									<div class="form-group">
										<input type="text" name="num_registro" id="num_registro" class="form-control" value="{$form.num_registro}">
										<label class="control-label"><i class="glyphicon glyphicon-th-large"></i> Número de Registro</label>
									</div>
									<div class="form-group">
										<input type="text" name="cod_memo" id="cod_memo" class="form-control" value="{$form.cod_memo}" >
										<label class="control-label"><i class="glyphicon glyphicon-list-alt"></i> Número de Memorandum</label>
									</div>
									<div class="form-group floating-label">
										<select id="pk_num_almacen" name="pk_num_almacen" class="form-control" onchange="cargarPasillo(this.value)">
											<option value="">&nbsp;</option>
											{foreach item=alm from=$almacen}
												{if $alm.pk_num_almacen == $form.pk_num_almacen}
													<option selected value="{$alm.pk_num_almacen}">{$alm.ind_descripcion_almacen}</option>
												{else}
													<option value="{$alm.pk_num_almacen}">{$alm.ind_descripcion_almacen}</option>
												{/if}
											{/foreach}
										</select>
										<label for="select2"><i class="glyphicon glyphicon-home"></i> Almacén</label>
									</div>
									<div id="pasillo">
									<div class="form-group floating-label" id="pasillo">
										<select id="pk_num_pasillo" name="pk_num_pasillo" class="form-control" >
											<option value="">&nbsp;</option>
											{foreach item=pas from=$pasillo}
												{if $pas.pk_num_pasillo == $form.pk_num_pasillo}
													<option selected value="{$pas.pk_num_pasillo}">{$pas.ind_pasillo}</option>
												{else}
													<option value="{$pas.pk_num_pasillo}">{$pas.ind_pasillo}</option>
												{/if}
											{/foreach}
										</select>
										<label for="select2"><i class="glyphicon glyphicon-th"></i> Pasillo</label>
									</div>
									</div>
									<div id="estante">
										<div class="form-group floating-label" id="estante">
											<select id="pk_num_estante" name="pk_num_estante" class="form-control" >
												<option value="">&nbsp;</option>
												{foreach item=est from=$estante}
													{if $est.pk_num_estante == $form.pk_num_estante}
														<option selected value="{$est.pk_num_estante}">{$est.ind_descripcion_estante}</option>
													{else}
														<option value="{$est.pk_num_estante}">{$est.ind_descripcion_estante}</option>
													{/if}
												{/foreach}
											</select>
											<label for="select2"><i class="glyphicon glyphicon-th-list"></i> Estante</label>
										</div>
									</div>
									<div id="dependencia">
										<div class="form-group floating-label">
											<select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control">
												<option value="">&nbsp;</option>
												{foreach item=dep from=$dependencia}
													{if $dep.pk_num_dependencia == $form.pk_num_dependencia}
														<option selected value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
													{else}
														<option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
													{/if}
												{/foreach}
											</select>
											<label for="select2"><i class="glyphicon glyphicon-briefcase"></i>Dependencia</label>
										</div>
									</div>
									<div id="documento">
										<div class="form-group floating-label">

											<select id="pk_num_documento" name="pk_num_documento" class="form-control">
												<option value="">&nbsp;</option>
												{foreach item=doc from=$documento}
													{if $doc.pk_num_documento == $form.pk_num_documento}
														<option selected value="{$doc.pk_num_documento}">{$doc.ind_descripcion_documento}</option>
													{else}
														<option value="{$doc.pk_num_documento}">{$doc.ind_descripcion_documento}</option>
													{/if}
												{/foreach}
											</select>
											<label for="select2"><i class="glyphicon glyphicon-file"></i> Documento</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="card">
								<div class="card-body">
									<div id="caja">
										<div class="form-group floating-label">
											<select id="pk_num_caja" name="pk_num_caja" class="form-control">
												<option value="">&nbsp;</option>
												{foreach item=ca from=$caja}
													{if $ca.pk_num_caja == $form.pk_num_caja}
														<option selected value="{$ca.pk_num_caja}">{$ca.pk_num_caja} {$ca.ind_descripcion_caja}</option>
													{else}
														<option value="{$ca.pk_num_caja}">{$ca.pk_num_caja} - {$doc.ind_descripcion_caja}</option>
													{/if}
												{/foreach}
											</select>
											<label for="select2"><i class="glyphicon glyphicon-inbox"></i> Caja</label>
										</div>
									</div>
									<div class="form-group">
										<input type="text" name="ind_documento" id="ind_documento" class="form-control" value="{$form.num_registro}">
										<label class="control-label"><i class="glyphicon glyphicon-th-large"></i> Número de documento</label>
									</div>
									<div class="form-group control-width-normal">
										<div class="input-group date" id="demo-date">
											<div class="input-group-content">
												<input type="text" class="form-control" id="fecha" name="fec_documento" value="{$form.fecha_documento}">
												<label>Fecha del documento</label>
											</div>
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div><!--end .form-group -->
									<div class="form-group floating-label">
										<div class="form-group floating-label">
											<div class="input-group">
												<div class="input-group-content">
													<input type="text" class="form-control dirty" id="remitente" name="remitente" value="{$form.ind_nombre1} {$form.ind_nombre2} {$form.ind_apellido1} {$form.ind_apellido2}">
													<input type="hidden" class="form-control" id="pk_num_empleado" name="pk_num_empleado" value="{$form.pk_num_empleado}">
													<label for="groupbutton10">Remitente</label>
												</div>
												<div class="input-group-btn">
													<button class="btn btn-default" type="button" onclick="buscarFuncionario()" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Funcionarios"><i class="glyphicon glyphicon-search"></i></button>
												</div>
											</div>
										</div><!--end .form-group -->
									</div>
									<div class="form-group">
										<input type="text" name="ind_descripcion" id="ind_descripcion" value="{$form.ind_descripcion}" class="form-control">
										<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Descripción</label>
									</div>
									<div class="form-group">
										<textarea class="form-control" rows="4" name="ind_resumen" id="ind_resumen">{$form.ind_resumen}</textarea>
										<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Resumen</label>
									</div>
								</div>
							</div>
						</div>
					</div><!--end #tab1 -->
					<div class="tab-pane" id="tab2">
						{if $form.num_flag_archivo==1}
							<div class="col-md-12 col-sm-12">
								<div class="col-md-6 col-sm-6">
									<br/>
									<div class="card">
										<div class="card-body">
											Archivo Cargado:
											<div>{$archivo.ind_nombre_archivo}</div>
											<br/>
												<center><a type="button" class="btn ink-reaction btn-raised btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Descargando..." href="{$ruta.ruta}{$archivo.num_registro}/{$archivo.ind_nombre_archivo}" download="{$archivo.ind_nombre_archivo}">Descargar Archivo</a></center>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<br/>
									<div class="card">
										<div class="card-body">
											Editar Archivo
											<div id="carga_archivo">
												<input type="file" name="archivo" id="archivo" onchange="validarExtension()">
											</div>
											<br/>
											<center><button type="button" class="btn btn-primary ink-reaction btn-raised" onclick="resetear()">Resetear</button></center>
										</div>
									</div>
								</div>
								<div>
                                    {if $ruta.extension=='pdf'}
										<h3>Archivo Cargado Actualmente</h3>
										<iframe name="miIframe" frameborder="0" width="100%" height="700px" src="{$ruta.ruta}{$archivo.num_registro}/{$archivo.ind_nombre_archivo}"></iframe>
                                    {/if}
								</div>
							</div>
						{else}
							<div id="my-dropzone" class="dropzone"></div>
						{/if}
					</div><!--end #tab2 -->
					{/if}
					<div class="tab-pane" id="tab3">
						<div class="form-group">
							{if $form.num_flag_archivo==1}
								<div class="alert alert-info">La visualización del PDF ha sido desactivada</div>
							{else}
								<center><a type="button" class="btn ink-reaction btn-raised btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> cargando..." href="{$_Parametros.url}modAD/registroArchivoCONTROL/ExpedienteMET?num_registro={$form.num_registro}" target="miIframe">Ver Pdf</a></center>
								<br/>
								<iframe name="miIframe" frameborder="0" width="100%" height="700px" src=""></iframe>
							{/if}
						</div>
						<div align="center">
							<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;&nbsp;&nbsp;
							<button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
						</div>
					</div><!--end #tab3 -->
				</div>
				<ul class="pager wizard">
					<li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
					<li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
					<li class="next last"><a class="btn-raised" href="javascript:void(0);">Último</a></li>
					<li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
				</ul>
			</form>
		</div>
	</div>
</form>
<script type="text/javascript">

	$(document).ready(function() {
		
		$("#formAjax").validate({
			rules:{
				num_registro:{
					required:true
				}
			},
			messages:{
				num_registro:{
					required: "Este campo es requerido"
				}
			},
			submitHandler: function(form){
				$.post($(form).attr("action"), $(form).serialize(),function(dato){
					$('#pk_num_registro_documento'+dato['pk_num_registro_documento']).remove();
					$(document.getElementById('datatable1')).append('<tr id="pk_num_registro_documento'+dato['pk_num_registro_documento']+'">' +
							'<td>'+dato['num_registro']+'</td>' +
							'<td>'+dato['cod_memo']+'</td>' +
							'<td>'+dato['ind_descripcion']+'</td>' +
							'<td>'+dato['fecha_documento']+'</td>' +
							'<td>'+dato['fecha_registro']+'</td>' +
							'{if in_array('AD-01-01-01-04-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha visualizado un registro" titulo="Visualizar Registro de Documento" data-toggle="modal" data-target="#formModal" pk_num_registro_documento="'+dato['pk_num_registro_documento']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-01-01-02-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha modificado un registro de documento" titulo="Modificar Registro de Documento" data-toggle="modal" data-target="#formModal" pk_num_registro_documento="'+dato['pk_num_registro_documento']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-01-01-05-VR',$_Parametros.perfil)}<td align="center"><button class="verRegistro logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha visualizado un registro de documento" titulo="Visualizar Registro de Documento" data-toggle="modal" data-target="#formModal" pk_num_registro_documento="'+dato['pk_num_registro_documento']+'"><i class="md md-attach-file" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-01-01-03-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado un registro de documento" titulo="¿Estás Seguro?" mensaje="¿Estás seguro de eliminar el registro del documento?" boton="si, Eliminar" pk_num_registro_documento="'+dato['pk_num_registro_documento']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td>{/if}</tr>');
					swal("Registro Modificado", "Registro de documento modificado exitosamente", "success");
					$(document.getElementById('cerrarModal')).click();
					$(document.getElementById('ContenidoModal')).html('');
				},'json');
                var valorFormulario = $("#valorFormulario").val();
                if(valorFormulario==1){
                    var inputFileArchivo = document.getElementById('archivo');
                    var num_registro = $("#num_registro").val();
                    var file = inputFileArchivo.files[0];
                    var data = new FormData();
                    data.append('archivo',file);
                    var url = "{$_Parametros.url}modAD/registroArchivoCONTROL/SubirArchivoEditarMET?num_registro="+num_registro;
                    $.ajax({
                        url:url,
                        type: 'POST',
                        contentType:false,
                        data:data,
                        processData:false,
                        cache:false});
                }
			}
		});
	});
	$("#fecha").datepicker({
		format: 'dd/mm/yyyy',
		language: 'es',
		autoclose: true
	});

	function resetear()
	{
		$("#carga_archivo").html('<input type="file" name="archivo" id="archivo">');
	}
	
	var idDocumento = $("#num_registro").val();
	
	Dropzone.autoDiscover = false;
	Dropzone.options.myDropzone = {
		init: function() {
			thisDropzone = this;
			<!-- 4 -->
			
			$.post("{$_Parametros.url}modAD/registroArchivoCONTROL/CargarImagenMET",{ cargarImagenes:1,idDocumento:""+idDocumento }, function(data) {
					//alert("dropsone inicializando");
				<!-- 5 -->
				$.each(data, function(key,value){
					 
					var mockFile = { name: value.name, size: value.size };
					
					thisDropzone.options.addedfile.call(thisDropzone, mockFile);
	 
					thisDropzone.options.thumbnail.call(thisDropzone, mockFile, "{$_Parametros.url}publico/imagenes/modAD/"+idDocumento+"/"+value.name);
					 
				});
				 
			});
		}
	};
	$("#my-dropzone").dropzone({
		url: "{$_Parametros.url}modAD/registroArchivoCONTROL/CambiarImagenMET?num_registro="+idDocumento,
		addRemoveLinks: true,
		dictRemoveFile: "Eliminar",
		dictCancelUpload: "Eliminar",
		maxFileSize: 1000,
		dictResponseError: "Ha ocurrido un error en el server",
		acceptedFiles: '.jpeg,.jpg,.png,.gif',
		autoProcessQueue: true,
		complete: function(file)
		{
			if(file.status == "success")
			{

				//swal("Carga exitosa", "Archivo cargado satisfactoriamente", "success");
			}
		},
		error: function(file)
		{
			swal("Error", "Error subiendo el archivo", "success");
		},
		removedfile: function(file, serverFileName)
		{
			var name = file.name;
			$.ajax({
				type: "POST",
				url: "{$_Parametros.url}modAD/registroArchivoCONTROL/CargarImagenMET",
				data: { filename:""+name,delete:"true",num_registro:""+idDocumento },
				success: function(data)
				{
					var json = JSON.parse(data);
					if(json.res == true)
					{
						var element;
						(element = file.previewElement) != null ?
								element.parentNode.removeChild(file.previewElement) :
								false;
						swal("Elemento eliminado", "Elemento eliminado satisfactoriamente", "success");
					}
				}
			});
		}
	});
	$("#formAjax").validate({
		rules:{
		},
		messages:{
		},
		submitHandler: function(form) {
			$.post($(form).attr('action'), $(form).serialize(),function(dato){

			},'json');
		}
	});
	$(".btn-loading-state").on("click",function(){

	});
	function cargarPasillo(idAlmacen) {
		$("#pasillo").html("");
		$.post("{$_Parametros.url}modAD/registroArchivoCONTROL/BuscarPasilloMET",{ idAlmacen:""+idAlmacen }, function (dato) {
			$("#pasillo").html(dato);
		});
	}
	function cargarEstante(idPasillo) {
		$("#estante").html("");
		$.post("{$_Parametros.url}modAD/registroArchivoCONTROL/BuscarEstanteMET",{ idPasillo:""+idPasillo }, function (dato) {
			$("#estante").html(dato);
		});
	}
	function cargarDependencia(idEstante) {
		$("#dependencia").html("");
		$.post("{$_Parametros.url}modAD/registroArchivoCONTROL/BuscarDependenciaMET",{ idEstante:""+idEstante }, function (dato) {
			$("#dependencia").html(dato);
		});
	}
	function cargarCaja(idDocumento,idDependencia, idEstante) {
		$("#caja").html("");
		$.post("{$_Parametros.url}modAD/registroArchivoCONTROL/BuscarCajaMET",{ idDocumento:""+idDocumento,idDependencia:""+idDependencia, idEstante:""+idEstante, }, function (dato) {
			$("#caja").html(dato);
		});
	}

	function cargarRegistro(idCaja) {
		$("#registro").html("");
		$.post("{$_Parametros.url}modAD/registroArchivoCONTROL/BuscarRegistroMET",{ idCaja:""+idCaja}, function (dato) {
			$("#registro").html(dato);
		});
	}

	function cargarDocumento(idDependencia, idEstante) {
		$("#documento").html("");
		$.post("{$_Parametros.url}modAD/registroArchivoCONTROL/TipoDocumentoMET",{ idDependencia:""+idDependencia, idEstante:""+idEstante}, function (dato) {
			$("#documento").html(dato);
		});
	}

    function buscarFuncionario(){
        var $urlFuncionario = '{$_Parametros.url}modAD/registroArchivoCONTROL/ConsultarEmpleadoMET';
        $('#ContenidoModal2').html("");
        $('#formModalLabel2').html('Listado de Empleados');
        $.post($urlFuncionario,"",function($dato){
            $('#ContenidoModal2').html($dato);
        });
    }

    function validarExtension()
    {
        var archivo = $("#archivo").val();
        var extensiones = archivo.substring(archivo.lastIndexOf("."));
        if((extensiones != ".pdf") && (extensiones != ".PDF"))
        {
            swal("La extension del archivo no es válida, debe ser .pdf");
            $("#carga_archivo").html("");$("#carga_archivo").html('<input type="file" name="archivo" id="archivo" onchange="validarExtension()">');
        }
    }

</script>


