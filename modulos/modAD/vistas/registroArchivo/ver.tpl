<div class="form floating-label">
	<div class="modal-body" >

			<div class="col-md-6 col-sm-6">
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$reg.num_registro}" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-th-large"></i> Número de registro</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$reg.cod_memo}" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-list-alt"></i> Número de memorándum</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$reg.ind_descripcion_almacen}" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-home"></i> Almacén</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$reg.ind_pasillo}" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-th"></i> Pasillo</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$reg.ind_descripcion_estante}" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-th-list"></i> Estante</label>
				</div>
				<div class="form-group floating-label">
					<textarea class="form-control" rows="2" disabled="disabled">{$reg.ind_dependencia}</textarea>
					<label class="control-label"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$reg.pk_num_documento}-{$reg.ind_descripcion_documento}" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-file"></i> Tipo de documento</label>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" disabled="disabled" value="{$reg.pk_num_caja}-{$reg.ind_descripcion_caja}">
					<label for="regular2"><i class="glyphicon glyphicon-inbox"></i> Caja</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$reg.ind_documento}" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-th-large"></i> Número de documento</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$reg.fecha_documento}" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-calendar"></i> Fecha del documento</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$reg.ind_nombre1} {$reg.ind_apellido1}" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-user"></i> Remitente</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$reg.ind_descripcion}" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-pencil"></i> Descripción </label>
				</div>
				<div class="form-group floating-label">
					<textarea class="form-control" rows="2" disabled="disabled">{$reg.ind_resumen}</textarea>
					<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Resumen</label>
				</div>
			</div>
	</div>
	<div class="modal-body" >
		<div class="col-md-12 col-sm-12">
			<div class="card">
				<div class="card-body style-default-light height-1">DOCUMENTO DIGITALIZADO: {$reg.ind_descripcion}</div>
				{if $reg.num_flag_archivo==1}

					{if $ruta.extension=='pdf'}
						<iframe name="miIframe" frameborder="0" width="100%" height="700px" src="{$ruta.ruta}{$archivo.num_registro}/{$archivo.ind_nombre_archivo}"></iframe>
					{else}
						<div class="col-md-12 col-sm-12" align="center">
							<a type="button" class="btn ink-reaction btn-raised btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Descargando..." href="{$ruta.ruta}{$archivo.num_registro}/{$archivo.ind_nombre_archivo}" download="{$archivo.ind_nombre_archivo}">Descargar Archivo</a>
						</div>
					{/if}
				{else}
					<br/>
					<div class="card-body small-padding text-center">
						<iframe name="miIframe" frameborder="0" width="100%" height="700px" {$_Parametros.url}modAD/registroArchivoCONTROL/ExpedienteMET?num_registro={$form.num_registro}></iframe>
					</div>
				{/if}
				<br/><br/>
			</div>
		</div>
	</div>
	<div class="modal-body" >
		<div class="col-md-12 col-sm-12">
	<div align="right">
		<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado la visualización del documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
	</div>
			</div></div>
</div>