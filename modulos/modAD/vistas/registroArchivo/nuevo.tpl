<div class="row">
	<div class="col-lg-12">
		<form id="formAjax" action="{$_Parametros.url}modAD/registroArchivoCONTROL/NuevoArchivoMET" class="form floating-label form-validation" role="form" novalidate>
			<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
				<input type="hidden" name="valido" value="1" />
				<input type="hidden" name="ver" value="{$ver}" />
				<input type="hidden" name="pk_num_registro_documento" value="{$pk_num_registro_documento}" />
				<div class="form-wizard-nav">
					<div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
					<ul class="nav nav-justified">
						<li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span><span class="title">Datos de Registro</span></a></li>
						{if $ver==1}
							<li><a href="#step3" data-toggle="tab"><span class="step">2</span> <span class="title"> Ver Archivo</span></a></li>
                        {else}
							<li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">Archivo</span></a></li>
							{if $registro != 1}<li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title">Expediente</span></a></li>{/if}
						{/if}
					</ul>
				</div><!--end .form-wizard-nav -->
				<div class="tab-content clearfix">
					<div class="tab-pane active" id="step1">
						<div class="col-md-6 col-sm-6">
							<div class="card">
								<div class="card-body">
									<div class="col-md-12 floating-label">
										<div class="col-md-6 form-group" id="num_registroError">
											<label for="num_registro"><i class="glyphicon glyphicon-th-large" ></i> Número de Registro</label>
											<input type="text" name="form[alphaNum][num_registro]" id="num_registro" readonly class="form-control" value="{if isset($form.num_registro)}{$form.num_registro}{/if}">
										</div>
										<div class="col-md-6 form-group" id="fec_anioError">
											<select id="fec_anio" name="form[int][fec_anio]" class="form-control" {if isset($ver) and $ver==1} disabled {/if}>
												<option value="">&nbsp;</option>
												{for $i=2007; $i<2018; $i++}
													{if isset($form.fec_anio) AND $form.fec_anio==$i}
														<option selected>{$i}</option>
													{else}
														<option>{$i}</option>
													{/if}

												{/for}
											</select>
											<label for="fec_anio">Año</label>
										</div>
									</div>
									<div class="col-md-12 floating-label">
										<div class="col-md-4 form-group">
											<label class="checkbox-styled" style="margin-top: 20px;">
												<input type="checkbox" id="checkMemo" class="form-control" {if isset($ver) and $ver==1} disabled {/if} {if isset($form.cod_memo) AND $form.cod_memo!='s/n'} checked {/if}>
												<span>Memorandum</span>
											</label>
										</div>
										<div class="col-md-8 form-group" id="cod_memoError">
											<input type="text" name="form[alphaNum][cod_memo]" id="cod_memo" class="form-control" {if isset($ver) and $ver==1} disabled {/if} {if isset($form.cod_memo) AND $form.cod_memo!='s/n'} value="{$form.cod_memo}" {else} disabled {/if}>
											<label class="control-label"><i class="glyphicon glyphicon-th-large"></i> Número de Memorandum</label>
										</div>
									</div>
									<div class="col-md-12 form-group" id="ind_documentoError">
										<input type="text" name="form[alphaNum][ind_documento]" id="ind_documento" class="form-control" {if isset($ver) and $ver==1} disabled {/if} value="{if isset($form.ind_documento)}{$form.ind_documento}{/if}">
										<label for="ind_documento"><i class="glyphicon glyphicon-th-large"></i> Número de documento</label>
									</div>
									<div class="form-group col-md-12 floating-label">
										<select id="pk_num_almacen" class="form-control" onchange="cargarPasillo(this.value,'0')" {if isset($ver) and $ver==1} disabled {/if}>
											<option value="">&nbsp;</option>
                                            {foreach item=us from=$almacen}
                                                {if isset($form.pk_num_almacen) AND $form.pk_num_almacen==$us.pk_num_almacen}
													<option value="{$us.pk_num_almacen}" selected>{$us.ind_descripcion_almacen}</option>
                                                {else}
													<option value="{$us.pk_num_almacen}">{$us.ind_descripcion_almacen}</option>
                                                {/if}
                                            {/foreach}
										</select>
										<label for="pk_num_almacen"><i class="glyphicon glyphicon-home"></i> Almacén</label>
									</div>
									<div class="form-group col-md-12 floating-label">
										<label><i class="glyphicon glyphicon-menu-hamburger"></i> Pasillo</label>
										<select class="form-control" id="pasillo" onchange="cargarEstante(this.value,'0')" {if isset($ver) and $ver==1} disabled {/if}>
											<option value="">&nbsp;</option>
										</select>

									</div>
									<div class="form-group col-md-12 floating-label">
										<select class="form-control" id="dependencia" {if isset($ver) and $ver==1} disabled {/if}">
											<option value="">&nbsp;</option>
                                            {foreach item=de from=$dependencia}
                                                {if isset($form.pk_num_dependencia) AND $form.pk_num_dependencia==$de.pk_num_dependencia}
													<option value="{$de.pk_num_dependencia}" selected>{$de.ind_dependencia}</option>
                                                {else}
													<option value="{$de.pk_num_dependencia}">{$de.ind_dependencia}</option>
                                                {/if}
                                            {/foreach}
										</select>
										<label><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
									</div>
									<div class="form-group col-md-12 floating-label">
										<label><i class="glyphicon glyphicon-th-large"></i> Estante</label>
										<select class="form-control" id="estante" onchange="cargarCarpetaCaja(this.value,'0')" {if isset($ver) and $ver==1} disabled {/if}>
											<option value="">&nbsp;</option>
                                            {foreach item=de from=$dependencia}
                                                {if isset($form.pk_num_dependencia) AND $form.pk_num_dependencia==$de.pk_num_dependencia}
													<option value="{$de.pk_num_dependencia}" selected>{$de.ind_dependencia}</option>
                                                {else}
													<option value="{$de.pk_num_dependencia}">{$de.ind_dependencia}</option>
                                                {/if}
                                            {/foreach}
										</select>

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="card">
								<div class="card-body">
									<div class="form-group col-md-12 floating-label" id="pk_num_cajaError">
										<label><i class="glyphicon glyphicon-th-large"></i> Caja</label>
										<select class="form-control" id="caja" name="form[int][pk_num_caja]" {if isset($ver) and $ver==1} disabled {/if}>
											<option value="">&nbsp;</option>
										</select>

									</div>

										<div class="form-group col-md-12 floating-label" id="pk_num_carpetaError">
											<label><i class="glyphicon md md-folder"></i> Carpeta</label>
											<select class="form-control" id="carpeta" name="form[int][pk_num_carpeta]" {if isset($ver) and $ver==1} disabled {/if}>
												<option value="">&nbsp;</option>
											</select>

										</div>


									<div class="form-group col-md-12 floating-label" id="fec_documentoError">
										<div class="input-group date" id="demo-date">
											<div class="input-group-content">
												<input type="text" class="form-control" id="fecha" {if isset($ver) and $ver==1} disabled {/if} name="form[txt][fec_documento]" value="{if isset($form.fec_documento)}{$form.fec_documento}{/if}">
												<label>Fecha del documento</label>
											</div>
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
									<div class="form-group col-md-12 floating-label" id="remitenteError">
											<div class="input-group-content">
												<input type="text" class="form-control dirty" id="remitente" name="form[int][remitente]" {if isset($ver) and $ver==1} disabled {/if} value="{if isset($form.ind_nombre1)}{$form.ind_nombre1} {$form.ind_nombre2} {$form.ind_apellido1} {$form.ind_apellido2}{/if}">
												<input type="hidden" class="form-control" id="pk_num_empleado" name="form[int][pk_num_empleado]" value="{if isset($form.pk_num_empleado)}{$form.pk_num_empleado}{/if}">
												<label for="groupbutton10">Remitente</label>
											</div>
											<div class="input-group-btn">
												<button class="btn btn-default" type="button" onclick="buscarFuncionario()" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Funcionarios"><i class="glyphicon glyphicon-search" {if isset($ver) and $ver==1} disabled {/if}></i></button>
											</div>
									</div>

									<div class="form-group col-md-12 floating-label" id="ind_descripcionError">
										<input type="text" name="form[formula][ind_descripcion]" id="ind_descripcion" class="form-control" value="{if isset($form.ind_descripcion)}{$form.ind_descripcion}{/if}" {if isset($ver) and $ver==1} disabled {/if}>
										<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Descripción</label>
									</div>
									<div class="form-group col-md-12 floating-label" id="ind_resumenError">
										<textarea class="form-control" rows="4" name="form[formula][ind_resumen]" {if isset($ver) and $ver==1} disabled {/if} id="ind_resumen">{if isset($form.ind_resumen)}{$form.ind_resumen}{/if}</textarea>
										<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Resumen</label>
									</div>

								</div>
							</div>
						</div>
					</div><!--end #step1 -->
					<div class="tab-pane" id="step2">
						<br/><br/>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-2 col-sm-2" id="boton">
								<div class="input-group-addon">
									<div class="checkbox checkbox-inline checkbox-styled" >
										Cargar archivo
										<label>
											&nbsp;&nbsp;<input type="checkbox" name="form[int][verificar]" id="verificar" onclick="cargar_archivo()" {if isset($form.num_flag_archivo) AND $form.num_flag_archivo==1}checked valor="1" {else} valor="0" {/if}  value="1">
										</label>
									</div>
								</div>
							</div>
							<div class="col-md-2 col-sm-2">
								<div id="carga_archivo">

								</div>
							</div>
						</div>
						<br/><br/><br/>

						<div id="cargar_dropzone">
							<div id="dropzone" class="dropzone">
								<input type="hidden" id="cantImg" value="{count($archivosImg)}">
                                {foreach item=img from=$archivosImg}
									{$nombre = $img.ind_nombre_archivo}
									{$r = "$ruta/$nombre"}
									{if is_readable($r)}
										<!--div id="{$nombre}">
											<img src="{$r}" width="100px" height="100px"><br>
											<button type="button" class="dz-remove eliminarArchivo" nombre="{$nombre}">Eliminar</button>
										</div-->

										<div class="dz-preview dz-processing dz-success dz-image-preview" id="imagen{$img.pk_num_archivo}">
											<div class="dz-details">
												<div class="dz-filename">
													<span data-dz-name="">{$nombre}</span>
												</div>
												<div class="dz-size" data-dz-size="">
													<strong>0</strong> KiB
												</div>
												<img data-dz-thumbnail="" alt="{$nombre}" src="{$r}">
											</div>
											<div class="dz-progress">
												<span class="dz-upload" data-dz-uploadprogress="" style="width: 100%;"></span>
											</div>
											<div class="dz-success-mark"><span>✔</span></div>
											<div class="dz-error-mark"><span>✘</span></div>
											<div class="dz-error-message"><span data-dz-errormessage=""></span></div>
											<a class="dz-remove eliminarArchivo" nombre="{$nombre}" id="imagen{$img.pk_num_archivo}" href="javascript:undefined;" data-dz-remove="">Eliminar</a>
										</div>
									{/if}
                                {/foreach}

							</div>
						</div>
					</div><!--end #step2 -->
					<div class="tab-pane" id="step3">
						<br/><br/>
						<div class="form-group">
							<div class="col-md-12 col-sm-12" id="ver_pdf">
								<center><a type="button" class="btn ink-reaction btn-raised btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> cargando..." href="" target="miIframe" id="botonPdf">Ver Pdf</a></center>
							</div>
							<div id="ver_mensaje" class="col-md-12 col-sm-12"></div>
							<br/>
							<div id="verIframe"><iframe name="miIframe" frameborder="0" width="100%" height="700px" src=""></iframe></div>
						</div>
					</div><!--end #step3 -->
				</div><!--end .tab-content -->
				<ul class="pager wizard">
					<li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
					<li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
					<li class="next last"><a class="btn-raised" href="javascript:void(0);">Último</a></li>
					<li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
				</ul>
			</div><!--end #rootwizard -->
			<div class="modal-footer">

				<div align="center">
					<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;&nbsp;&nbsp;
                    {if $pk_num_registro_documento!=0}
						{if isset($ver) and $ver!=1}
						<button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Modificar</button>
                        {/if}
						{else}
						<button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                    {/if}

				</div>
			</div>

		</form>
	</div><!--end .col -->
</div><!--end .row -->
<!-- END VALIDATION FORM WIZARD -->
<script type="text/javascript">

    function cargarPasillo(idAlmacen, idPasillo) {
        $("#pasillo").html("");
        $.post("{$_Parametros.url}modAD/registroArchivoCONTROL/BuscarPasilloMET",{ idAlmacen:idAlmacen, idPasillo: idPasillo }, function (dato) {
            $("#pasillo").html(dato);
        });
    }
    function cargarEstante(idPasillo, idEstante) {
        $("#estante").html("");
        $.post("{$_Parametros.url}modAD/registroArchivoCONTROL/BuscarEstanteMET",{ idPasillo:idPasillo, idEstante:idEstante }, function (dato) {
            $("#estante").html(dato);
        });
    }
    function cargarDependencia(idEstante,idDependencia) {
        $("#dependencia").html("");
        $.post("{$_Parametros.url}modAD/registroArchivoCONTROL/BuscarDependenciaMET",{ idEstante:idEstante,idDependencia:idDependencia }, function (dato) {
            $("#dependencia").html(dato);
        });
    }
    function cargarCarpetaCaja(idEstante,idCaja,idCarpeta) {
        $("#caja").html("");
        $.post("{$_Parametros.url}modAD/registroArchivoCONTROL/BuscarCajaMET",{ idEstante:idEstante,idCaja:idCaja }, function (dato) {
            $("#caja").html(dato);
        });
        $("#carpeta").html("");
        $.post("{$_Parametros.url}modAD/registroArchivoCONTROL/BuscarCarpetaMET",{ idEstante:idEstante,idCarpeta:idCarpeta }, function (dato) {
            $("#carpeta").html(dato);
        });
    }

    function cargar_archivo() {
        valor = $("#verificar").attr("valor");

        if(valor==1){
            $("#verificar").attr("valor","2");
            $("#cargar_dropzone").css("display", "none");
            var opciones ="";
            {foreach item=pdf from=$archivoPdf}
            	//opciones += "<a href='{$ruta}/{$pdf.ind_nombre_archivo}' target='_blank'>{$pdf.ind_nombre_archivo}</a><br>";
            	opciones += "{$pdf.ind_nombre_archivo}<br>";
            {/foreach}
            $("#carga_archivo").html('<input type="file" name="archivo" id="archivo" onchange="validarExtension()" value="">Archivos Subidos<br>'+
                opciones
			);
//            $("#ver_pdf").css("display", "none");
//            $("#ver_mensaje").html('<div class="alert alert-info">La visualización del PDF ha sido desactivada</div>');
//            $("#verIframe").html('');
            $("#botones").html('<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;&nbsp;&nbsp; <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>');
        } else {
            $("#verificar").attr("valor","1");
            $("#cargar_dropzone").css("display", "block");
            $("#carga_archivo").html("");
            $("#ver_mensaje").html("");
            $("#verIframe").html('<iframe name="miIframe" frameborder="0" width="100%" height="700px" src=""></iframe>');
            $("#ver_pdf").css("display", "block");
            $("#botones").html('<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;&nbsp;&nbsp; <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>');
        }

    }

    function validarExtension()
    {
        var archivo = $("#archivo").val();
        var extensiones = archivo.substring(archivo.lastIndexOf("."));
        if((extensiones != ".pdf") && (extensiones != ".PDF"))
        {
            swal("La extension del archivo no es válida, debe ser .pdf");
            $("#carga_archivo").html("");$("#carga_archivo").html('<input type="file" name="archivo" id="archivo" onchange="validarExtension()">');
        }
    }

    function eliminarArchivo(file) {

        var name = file.name;
        {if isset($pk_num_registro_documento)}
        var url = "{$_Parametros.url}modAD/registroArchivoCONTROL/CargarImagenMET/{$pk_num_registro_documento}/"+$('#fec_anio').val();
        {else}
        url = "{$_Parametros.url}modAD/registroArchivoCONTROL/CargarImagenMET/a/"+$('#fec_anio').val();
        {/if}

        $.ajax({
            type: "POST",
            url: url,
            data: { filename:name, delete:"true" },
            success: function(data)
            {
                var json = JSON.parse(data);
                if(json.res == true)
                {
                    var element;
                    (element = file.previewElement) != null ?
                        element.parentNode.removeChild(file.previewElement) :
                        false;
                    swal("Elemento eliminado", "Elemento eliminado satisfactoriamente", "success");
                }
            }
        });
    }

    $(document).ready(function(){

        Dropzone.autoDiscover = false;
        cargar_archivo();

        $("#dropzone").dropzone({
            url: "{$_Parametros.url}modAD/registroArchivoCONTROL/CargarImagenMET/{$pk_num_registro_documento}/"+$('#fec_anio').val(),
            addRemoveLinks: true,
            dictRemoveFile: 1,
            dictCancelUpload: "Eliminar",
            maxFileSize: 1000,
            dictResponseError: "Ha ocurrido un error en el server",
            acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF',
            autoProcessQueue: true,
            init: function() {
                this.on("processing", function(file) {
                    {if isset($pk_num_registro_documento)}
                    this.options.url = "{$_Parametros.url}modAD/registroArchivoCONTROL/CargarImagenMET/{$pk_num_registro_documento}/"+$('#fec_anio').val();
                    {else}
					this.options.url = "{$_Parametros.url}modAD/registroArchivoCONTROL/CargarImagenMET/a/"+$('#fec_anio').val();
                    {/if}

                });
            },
            complete: function(file)
            {
                if(file.status == "success")
                {

                    //swal("Carga exitosa", "Archivo cargado satisfactoriamente", "success");
                }
            },
            error: function(file)
            {
                swal("Error", "Error subiendo el archivo", "success");
            },
            removedfile: function(file, serverFileName)
            {
                eliminarArchivo(file);
            }
        });

        {if $pk_num_registro_documento}
			if($('#cantImg').val()!=0) {
                $('.dz-message').remove();
            }
        {/if}


       $(".eliminarArchivo").on('click', function(file){ //enables click event
           file.name = $(this).attr('nombre');
           $('#'+$(this).attr('id')).remove();
           eliminarArchivo(file);
           swal("Elemento eliminado", "Elemento eliminado satisfactoriamente", "success");
        });

        $("#fecha").datepicker({
            format: 'yyyy-mm-dd',
            language: 'es',
            autoclose: true
        });

        var app = new  AppFunciones();
        var date="{date('d/m/Y')}";

        $("#formAjax").validate({

            submitHandler: function(form) {
                $.post($(form).attr('action'), $(form).serialize(),function(dato) {

                    if(dato['status']=='error') {
                        app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                    } else if(dato['status']=='errorSql') {
                        app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son UNICOS');
                    } else {
                        swal("Registro "+dato['mensaje'], "Documento "+dato['mensaje']+" satisfactoriamente", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                        if(dato['mensaje']=='Guardado') {
                            $(document.getElementById('datatable1')).append('<tr id="pk_num_registro_documento'+dato['pk_num_registro_documento']+'">' +
                                '<td>'+dato['num_registro']+'</td>' +
                                '<td>'+dato['ind_documento']+'</td>' +
                                '<td>'+dato['ind_descripcion']+'</td>' +
                                '<td>'+dato['fec_documento']+'</td>' +
                                '<td>'+date+'</td>' +
                                '{if in_array('AD-01-01-01-04-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha visualizado el documento" titulo="Visualizar Registro de Documento" data-toggle="modal" data-target="#formModal" pk_num_registro_documento="'+dato['pk_num_registro_documento']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>{/if}' +
                                '{if in_array('AD-01-01-01-02-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha modificado el registro del documento" titulo="Modificar Registro del Documento" data-toggle="modal" data-target="#formModal" pk_num_registro_documento="'+dato['pk_num_registro_documento']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}' +
                                '{if in_array('AD-01-01-01-05-VR',$_Parametros.perfil)}<td align="center"><button class="verRegistro logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha visualizado un registro de documento" titulo="Ver Reporte de Documento" data-toggle="modal" data-target="#formModal" pk_num_registro_documento="'+dato['pk_num_registro_documento']+'"><i class="md md-attach-file" style="color: #ffffff;"></i></button></td>{/if}' +
                                '{if in_array('AD-01-01-01-03-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado un registro de documento" titulo="¿Estás Seguro?" mensaje="¿Estás seguro de eliminar el registro de documento?" boton="si, Eliminar" pk_num_registro_documento="'+dato['pk_num_registro_documento']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td>{/if}</tr>');
                        } else {
                            $(document.getElementById('pk_num_registro_documento'+dato['pk_num_registro_documento'])).html(
                                '<td>'+dato['num_registro']+'</td>' +
                                '<td>'+dato['ind_documento']+'</td>' +
                                '<td>'+dato['ind_descripcion']+'</td>' +
                                '<td>'+dato['fec_documento']+'</td>' +
                                '<td>'+dato['fec_registro']+'</td>' +
                                '{if in_array('AD-01-01-01-04-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha visualizado el documento" titulo="Visualizar Registro de Documento" data-toggle="modal" data-target="#formModal" pk_num_registro_documento="'+dato['pk_num_registro_documento']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>{/if}' +
                                '{if in_array('AD-01-01-01-02-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha modificado el registro del documento" titulo="Modificar Registro del Documento" data-toggle="modal" data-target="#formModal" pk_num_registro_documento="'+dato['pk_num_registro_documento']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}' +
                                '{if in_array('AD-01-01-01-05-VR',$_Parametros.perfil)}<td align="center"><button class="verRegistro logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha visualizado un registro de documento" titulo="Ver Reporte de Documento" data-toggle="modal" data-target="#formModal" pk_num_registro_documento="'+dato['pk_num_registro_documento']+'"><i class="md md-attach-file" style="color: #ffffff;"></i></button></td>{/if}' +
                                '{if in_array('AD-01-01-01-03-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado un registro de documento" titulo="¿Estás Seguro?" mensaje="¿Estás seguro de eliminar el registro de documento?" boton="si, Eliminar" pk_num_registro_documento="'+dato['pk_num_registro_documento']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td>{/if}</tr>');
                        }
                    }
                } ,'json');
                var valor = $("#verificar").attr("valor");
                if(valor==2){
                    var inputFileArchivo = document.getElementById('archivo');
                    var num_registro = $("#num_registro").val();
                    var fec_anio = $("#fec_anio").val();
                    var file = inputFileArchivo.files[0];
                    var data = new FormData();
                    data.append('archivo',file);
                    var url = "{$_Parametros.url}modAD/registroArchivoCONTROL/SubirArchivoMET?num_registro="+num_registro+"&fec_anio="+fec_anio;
                    $.ajax({
                        url:url,
                        type: 'POST',
                        contentType:false,
                        data:data,
                        processData:false,
                        cache:false});
                }

            }
        });
        $('#fec_anio').on('change',function () {
            $.post("{$_Parametros.url}modAD/registroArchivoCONTROL/BuscarNumeroMET",{ anio:$(this).val() }, function (dato) {
                $("#num_registro").attr('value',dato['num_registro']);
                {$pdf = ""}
                {if isset($archivoPdf)}
					{$pdf = $archivoPdf[0].ind_nombre_archivo}
                {/if}
                {if $pk_num_registro_documento!=0}
					var url = "{$_Parametros.url}modAD/registroArchivoCONTROL/ExpedienteMET/{$pk_num_registro_documento}/{$form.fec_anio}/{$form.num_flag_archivo}/{$pdf}";
                {else}
					if($('#verificar').attr('checked')=='checked') {
					    var verificar = '1';
                    } else {
                        verificar = '0';
                    }
					url = "{$_Parametros.url}modAD/registroArchivoCONTROL/ExpedienteMET/'a'/"+$('#fec_anio').val()+"/"+verificar+"/{$pdf}";
				{/if}
				$('#botonPdf').attr('href',url);
            },'json');
        });

        {$pdf = ""}
        {if isset($archivoPdf)}
        	{$pdf = $archivoPdf[0].ind_nombre_archivo}
        {/if}
        {if $pk_num_registro_documento!=0}
	        var url = "{$_Parametros.url}modAD/registroArchivoCONTROL/ExpedienteMET/{$pk_num_registro_documento}/{$form.fec_anio}/{$form.num_flag_archivo}/{$pdf}";
        {else}
			if($('#verificar').attr('checked')=='checked') {
				var verificar = '1';
			} else {
				verificar = '0';
			}
    	    url = "{$_Parametros.url}modAD/registroArchivoCONTROL/ExpedienteMET/'a'/"+$('#fec_anio').val()+"/"+verificar+"/{$pdf}";
        {/if}
        $('#botonPdf').attr('href',url);

        $('#pk_num_almacen').ready(function () {
            {if isset($form.pk_num_almacen) AND isset($form.pk_num_pasillo)}
				cargarPasillo({$form.pk_num_almacen},{$form.pk_num_pasillo});
				cargarEstante({$form.pk_num_pasillo},{$form.pk_num_estante});
				cargarDependencia({$form.pk_num_estante},{$form.pk_num_dependencia});
				var idCaja = '0';
				{if isset($form.pk_num_caja)}
            		idCaja = {$form.pk_num_caja};
				{/if}
				cargarCarpetaCaja({$form.pk_num_estante},idCaja,{$form.pk_num_carpeta});
            {/if}
        });
        $('#checkMemo').on('click',function () {
			if($(this).attr('checked')=='checked') {
			    $('#cod_memo').attr('disabled',false);
            } else {
                $('#cod_memo').attr('disabled',true);
                $('#cod_memo').attr('value','');
            }
        });
    });

    function buscarFuncionario(){
        var $urlFuncionario = '{$_Parametros.url}modAD/registroArchivoCONTROL/ConsultarEmpleadoMET';
        $('#ContenidoModal2').html("");
        $('#formModalLabel2').html('Listado de Empleados');
        $.post($urlFuncionario,"",function($dato){
            $('#ContenidoModal2').html($dato);
        });
    }
</script>