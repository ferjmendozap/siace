<input type="hidden" name="posicion" id="posicion" value="{$emp.valorBoton}"/>
<input type="hidden" name="valor" id="valor" value="{$emp.valor}"/>
<table id="datatable2" class="table table-striped table-hover">
	<thead>
	<tr align="center">
		<th><i class="md-person"></i> Nº</th>
		<th><i class="md-person"></i> Nombre Completo</th>
		<th><i class="glyphicon glyphicon-triangle-right"></i> Nro. Documento</th>
		<th><i class="glyphicon glyphicon-triangle-right"></i> Dependencia</th>
		<th> Seleccionar</th>
	</tr>
	</thead>
	<tbody>
	{assign var="numero" value="1"}
	{foreach item=emp from=$listarEmpleado}
		<tr>
			<td>{$emp.pk_num_empleado}</td>
			<td>{$emp.ind_nombre1} {$emp.ind_nombre2} {$emp.ind_apellido1} {$emp.ind_apellido2}</td>
			<td>{$emp.ind_cedula_documento}</td>
			<td>{$emp.ind_dependencia}</td>
			<td><button type="button" pk_num_empleado="{$emp.pk_num_empleado}" class="cargar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha buscado un empleado" title="Cargar empleado"><i class="glyphicon glyphicon-download"></i></button></td>
		</tr>
	{/foreach}
	</tbody>
</table>
</section>
<script type="text/javascript">

	$(document).ready(function() {
		$('#datatable2').DataTable( {
			"language": {
				"lengthMenu": "Mostrar _MENU_ ",
				"sSearch":        "Buscar:",
				"zeroRecords": "No hay resultados",
				"info": "Mostrar _PAGE_ de _PAGES_",
				"infoEmpty": "No hay resultados",
				"infoFiltered": "(filtered from _MAX_ total records)",
				"paginate": {
					"previous": "<",
					"next": ">"
				}
			}
		} );
	} );

	if ( $.fn.dataTable.isDataTable( '#datatable2' ) ) {
		table = $('#datatable2').DataTable();
	}
	else {
		table = $('#datatable2').DataTable( {
			paging: true
		} );
	}

    $('#datatable2 tbody').on( 'click', '.cargar', function (respuesta_post) {
        var $urlCargar='{$_Parametros.url}modAD/registroArchivoCONTROL/CargarEmpleadoMET';
        var pk_num_empleado = $(this).attr('pk_num_empleado');
		$.post($urlCargar, { pk_num_empleado: pk_num_empleado}, function (dato) {
			$("#remitente").val(dato['remitente']);
			$("#pk_num_empleado").val(pk_num_empleado);
		}, 'json');
		$(document.getElementById('cerrarModal2')).click();
		$(document.getElementById('ContenidoModal2')).html('');
    });
</script>