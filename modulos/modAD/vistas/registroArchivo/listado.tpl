<br/>
<section class="style-default-bright">
    <div class="col-lg-12">
		<div class="card style-default-bright">
			<div class="card-head"><h2 class="text-primary">&nbsp;Registro de Archivos</h2></div>
		</div>
	</div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                        <tr  align="center">
                            <th><i class="glyphicon glyphicon-th-large"></i> N° de Registro</th>
                            <th><i class="glyphicon glyphicon-file"></i> N° de Documento</th>
                            <th><i class="md-create"></i> Descripcion</th>
                            <th><i class="glyphicon glyphicon-calendar"></i> Fecha Documento</th>
                            <th><i class="glyphicon glyphicon-calendar"></i> Fecha Registro</th>
                            <th width="40">Ver</th>
                            <th width="50">Editar</th>
                            <th width="40">Reporte</th>
                            <th width="70">Eliminar</th>
                        </tr>
                        </thead>
                        <tbody>
                            {foreach item=post from=$_PruebaPost}
                            <tr id="pk_num_registro_documento{$post.pk_num_registro_documento}">
                            <td>{$post.num_registro}</td>
                            <td>{$post.ind_documento}</td>
                            <td>{$post.ind_descripcion}</td>
                            <td>{$post.fecha_documento}</td>
                            <td>{$post.fecha_registro}</td>
                                <td  align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_registro_documento="{$post.pk_num_registro_documento}" title="Ver registro de documento"><i class="glyphicon glyphicon-search" id="ver"></i></button></td>
                                <td  align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_registro_documento="{$post.pk_num_registro_documento}" descipcion="El Usuario ha modificado un archivo" title="Modificar documento" titulo="Modificar documento"><i class="fa fa-edit"></i></button></td>
                                <td  align="center"><button class="verRegistro logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"  pk_num_registro_documento="{$post.pk_num_registro_documento}" descipcion="El Usuario ha visualizado el reporte de documento" title="Reporte de documento" titulo="Reporte de documento"><i class="md md-attach-file"></i></button></td>
                                <td  align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                pk_num_registro_documento="{$post.pk_num_registro_documento}"  boton="si, Eliminar" descipcion="El usuario ha eliminado un archivo" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar el archivo?" title="Eliminar Archivo" pk_num_registro_documento="{$post.pk_num_registro_documento}"><i class="md md-delete"></i></button>
                            </td>
                        </tr>
                            {/foreach}
                        </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="9">
                            <button  class="logsUsuario btn ink-reaction btn-raised btn-info" descripcion="El usuario ha rgistrado un archivo" data-toggle="modal" data-target="#formModal" titulo="Registrar Archivo" id="nuevo"><i class="md md-create"></i> Nuevo Registro</button>
                            &nbsp;
                            <button class="logsUsuario btn ink-reaction btn-raised btn-primary" id="reporteRegistro" descripcion="Generar Reporte de Documentos Digitalizados" data-toggle="modal" data-target="#formModal" titulo="Listado de Documentos"  title="Listado de Documentos">Reporte de Documentos Digitalizados</button>
                        </th>
                    </tr>
                    </tfoot>
                  </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
            $(document).ready(function() {

                var $url='{$_Parametros.url}modAD/registroArchivoCONTROL/NuevoArchivoMET';

                $('#nuevo').click(function(){
					$('#modalAncho').css( "width", "80%" );
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $('#ContenidoModal').html("");
                    $.post($url,{ },function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });
                var $url_modificar='{$_Parametros.url}modAD/registroArchivoCONTROL/NuevoArchivoMET';
                $('#datatable1 tbody').on( 'click', '.modificar', function () {
		            $('#modalAncho').css( "width", "80%" );
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $('#ContenidoModal').html("");
                    $.post($url_modificar,{ pk_num_registro_documento: $(this).attr('pk_num_registro_documento')},function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });
                $('#datatable1 tbody').on( 'click', '.eliminar', function () {

                    var pk_num_registro_documento=$(this).attr('pk_num_registro_documento');
                    swal({
                        title: $(this).attr('titulo'),
                        text: $(this).attr('mensaje'),
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: $(this).attr('boton'),
                        closeOnConfirm: false
                    }, function(){
                        var $url='{$_Parametros.url}modAD/registroArchivoCONTROL/EliminarArchivoMET';
                        $.post($url, { pk_num_registro_documento: pk_num_registro_documento},function($dato){
                            if($dato['status']=='OK'){
                                $(document.getElementById('pk_num_registro_documento'+$dato['pk_num_registro_documento'])).html('');
                                swal("Eliminado!", "El Archivo fue eliminado.", "success");
                                $('#cerrar').click();
                            }
                        },'json');
                    });
                });
            });
            var $urlVer='{$_Parametros.url}modAD/registroArchivoCONTROL/NuevoArchivoMET';
            $('#datatable1 tbody').on( 'click', '.ver', function () {
                $('#modalAncho').css( "width", "80%" );
                $('#formModalLabel').html($(this).attr('title'));
                $('#ContenidoModal').html("");

                $.post($urlVer,{ pk_num_registro_documento: $(this).attr('pk_num_registro_documento'), ver: 1 },function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });

            $('#datatable1 tbody').on( 'click', '.verRegistro', function () {
                var $pk_num_registro_documento = $(this).attr('pk_num_registro_documento');
                var $urlVerRegistro =  '{$_Parametros.url}modAD/registroArchivoCONTROL/RegistroMET/?pk_num_registro_documento='+$pk_num_registro_documento;
                $('#modalAncho').css( "width", "75%" );
                $('#formModalLabel').html($(this).attr('titulo'));
                $('#ContenidoModal').html("");
                $('#ContenidoModal').html('<iframe frameborder="0" src="'+$urlVerRegistro+'"  width="100%" height="440px"></iframe>');
            });

            $('#reporteRegistro').click(function(){
                $('#modalAncho').css( "width", "75%" );
                $('#formModalLabel').html($(this).attr('titulo'));
                $('#ContenidoModal').html("");
                $('#ContenidoModal').html('<iframe frameborder="0" src="{$_Parametros.url}modAD/registroArchivoCONTROL/ReporteRegistroMET"  width="100%" height="440px"></iframe>');
            });
</script>
