<div class="form floating-label">
	<div class="modal-body" >
		{foreach item=doc from=$documento}
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$doc.pk_num_documento}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-th-large"></i> N° de documento</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$doc.ind_descripcion_documento}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-pencil"></i> Descripción</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$doc.fecha}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2">Última modificación</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" id="regular2" value="{$doc.ind_nombre1} {$doc.ind_apellido1}" size="45%" disabled="disabled" disabled="disabled">
				<label for="regular2">Último usuario</label>
			</div>
		{/foreach}
	<div align="right">
		<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha regresado al listado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cerrar</button>
	</div>
</div>