<form id="formAjax" action="{$_Parametros.url}modAD/tipoDocumentoCONTROL/NuevoDocumentoMET" class="form floating-label" novalidate="novalidate">
<input type="hidden" name="valido" value="1" />
<div class="modal-body">
	<div class="form-group floating-label">
		<input type="text" class="form-control"  name="ind_descripcion_documento" id="ind_descripcion_documento">
		<label for="ind_descripcion_documento"><i class="md md-insert-drive-file"></i> Descripción</label>
	</div>
	<div align="right">
		<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
		<button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
	</div>
</form>
<script type="text/javascript">
	$(document).ready(function () {
		//validation rules
		$("#formAjax").validate({
			rules:{
				num_documento:{
					required:true,
					remote:{
						url:"{$_Parametros.url}modAD/tipoDocumentoCONTROL/VerificarDocumentoMET",
						type:"post"
					}
				},
				ind_descripcion_documento:{
					required: true,
					remote:{
						url:"{$_Parametros.url}modAD/tipoDocumentoCONTROL/VerificarDescripcionDocumentoMET",
						type:"post",
						data: {
							pk_num_documento: function(){
								return $( "#pk_num_documento" ).val();
							}
						}
					}
				}
			},
			messages:{
				num_documento:{
					required: "Este campo es requerido",
					remote: jQuery.validator.format("Ya se encuentra registrado en el sistema")
				},
				ind_descripcion_documento:{
					required: "Este campo es requerido",
					remote: jQuery.validator.format("Ya se encuentra registrado en el sistema")
				}
			},
			submitHandler: function(form) {
				$.post($(form).attr('action'), $(form).serialize(),function(dato){
					$(document.getElementById('datatable1')).append('<tr id="pk_num_documento'+dato['pk_num_documento']+'"><td>'+dato['pk_num_documento']+'</td><td>'+dato['ind_descripcion_documento']+'</td>' +
							'{if in_array('AD-01-02-05-03-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha visualizado el tipo de documento" titulo="Visualizar tipo de documento" title="Visualizar tipo de documento" data-toggle="modal" data-target="#formModal" pk_num_documento="'+dato['pk_num_documento']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-02-05-04-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha modificado el tipo de documento" titulo="Modificar Tipo de Documento" title="Modificar Tipo de Documento" data-toggle="modal" data-target="#formModal" pk_num_documento="'+dato['pk_num_documento']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-02-05-05-E',$_Parametros.perfil)}<td align="center"> <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado un tipo de documento" title="Eliminar Tipo de Documento" titulo="¿Estás Seguro?" mensaje="¿Estás seguro de eliminar el tipo de documento?" boton="si, Eliminar" pk_num_documento="'+dato['pk_num_documento']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td>{/if}</tr>');
					swal("Tipo de Documento Guardado", "Tipo de documento guardado satisfactoriamente", "success");
					$(document.getElementById('cerrarModal')).click();
					$(document.getElementById('ContenidoModal')).html('');
				},'json');
			}
		});
	});
</script>
