<form action="{$_Parametros.url}modAD/tipoDocumentoCONTROL/EditarDocumentoMET" id="formAjax" method="post" class="form" role="form">
	<div class="modal-body">
		<input type="hidden" value="1" name="valido" />
		<input type="hidden" value="{$form.pk_num_documento}" name="pk_num_documento" id="pk_num_documento"/>
		<div class="form-group floating-label">
			<input type="text" class="form-control" id="regular2" value="{$form.pk_num_documento}" disabled>
			<label for="regular2"><i class="glyphicon glyphicon-th-large"></i> N° de documento</label>
		</div>
		<div class="form-group floating-label">
			<input type="text" class="form-control" id="regular2" value="{$form.ind_descripcion_documento}" name="ind_descripcion_documento" id="ind_descripcion_documento">
			<label for="regular2"><i class="glyphicon glyphicon-pencil"></i> Descripción</label>
		</div>
		<div class="form-group floating-label">
			<input type="text" class="form-control" id="regular2" value="{$form.fecha}" disabled="disabled">
			<label for="regular2">última fecha de modificación</label>
		</div>
		<div class="form-group floating-label">
			<input type="text" class="form-control" id="regular2" value="{$form.nombreCompleto}" disabled="disabled">
			<label for="regular2">último usuario</label>
		</div>
		<div align="right">
			<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado el registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
			<button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
		</div>
</form>
<script type="text/javascript">
	$(document).ready(function() {
		$("#formAjax").validate({
			rules:{
				ind_descripcion_documento: {
					required: true,
					remote:{
						url:"{$_Parametros.url}modAD/tipoDocumentoCONTROL/VerificarDescripcionDocumentoMET",
						type:"post",
						data: {
							pk_num_documento: function(){
								return $( "#pk_num_documento" ).val();
							}
						}
					}
				}
			},
			messages:{
				ind_descripcion_documento:{
					required: "Este campo es requerido",
					remote: jQuery.validator.format("Ya se encuentra registrado en el sistema")
				}
			},
			submitHandler: function(form){
				$.post($(form).attr("action"), $(form).serialize(),function(dato){
					$('#pk_num_documento'+dato['pk_num_documento']).remove();
					$(document.getElementById('datatable1')).append('<tr id="pk_num_documento'+dato['pk_num_documento']+'">' +
							'<td>'+dato['pk_num_documento']+'</td>' +
							'<td>'+dato['ind_descripcion_documento']+'</td>' +
							'{if in_array('AD-01-02-05-03-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha visualizado un documento" ' +
							'titulo="Visualizar tipo de documento" title="Visualizar tipo de documento" data-toggle="modal" data-target="#formModal" pk_num_documento="'+dato['pk_num_documento']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-02-05-04-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha modificado un documento" ' +
							'titulo="Modificar documento" title="Modificar documento" data-toggle="modal" data-target="#formModal" pk_num_documento="'+dato['pk_num_documento']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-02-05-05-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado un documento" title="Eliminar tipo de documento" titulo="¿Estás Seguro?" mensaje="¿Estás seguro de eliminar el documento?" boton="si, Eliminar" pk_num_documento="'+dato['pk_num_documento']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td>{/if}</tr>');
					swal("Documento modificado", "Documento modificado exitosamente", "success");
					$(document.getElementById('cerrarModal')).click();
					$(document.getElementById('ContenidoModal')).html('');
				},'json');
			}
		});
	});
</script>


