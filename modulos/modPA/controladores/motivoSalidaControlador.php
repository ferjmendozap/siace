<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/


class motivoSalidaControlador extends Controlador
{
    private $atMotivoSalidaModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atMotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
    }
	


    #Metodo Index del controlador Motivo Salida.
    public function metIndex()
    {

    }

    #Metodo parac cargar el listado de los motivos de salida
    public function metListarMotivoSalida()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
            'multi-select/multi-select555c',

        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'multi-select/jquery.multi-select',);



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('_MotivoSalidaPost', $this->atMotivoSalidaModelo->metGetMotivoSalida());
        $this->atVista->metRenderizar('ListarMotivoSalida');
    }
    public function metRegistrarMotivoSalida()
    {

        $idMotivoSalida= $this->metObtenerInt('idMotivoSalida');
        $valido=$this->metObtenerInt('valido');
        $maestro            = $this->metObtenerInt('maestro');




        $complementosJs = array(

            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(

            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {

            $this->metValidarToken();

            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formInt = $this->metObtenerInt('form', 'int');

            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }


            foreach ($formInt as $tituloInt => $valorInt) {
                if (!empty($formInt[$tituloInt])) {
                    $validacion[$tituloInt] = $valorInt;
                } else {
                    $validacion[$tituloInt] = '';
                }
            }

            if ($idMotivoSalida === 0) {



                $id = $this->atMotivoSalidaModelo->metRegistrarMotivoSalida(
                    $validacion['cod_detalle'],
                    $validacion['motivoSalida'],
                    $validacion['idMaestro']
                );
                if(is_array($id)){

                    $validacion=[];
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;


                }else{

                    $validacion['status'] = 'creacion';
                    $validacion['idMotivoSalida'] = $id;
                    echo json_encode($validacion);
                    exit;

                }





            } else {


                $validacion['status'] = 'modificacion';

                $this->atMotivoSalidaModelo->metModificarMotivoSalida($idMotivoSalida,
                    $validacion['cod_detalle'],
                    $validacion['motivoSalida']
                );
                $validacion['idMotivoSalida'] = $idMotivoSalida;
                echo json_encode($validacion);
                exit;


            }
        }

        if($idMotivoSalida!=0){
            $db = $this->atMotivoSalidaModelo->metMostrarMotivoSalida($idMotivoSalida);
            $this->atVista->assign('formDB',$db);
        }

        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->assign('idMaestro',$maestro);
        $this->atVista->assign('idMotivoSalida',$idMotivoSalida);

        $this->atVista->metRenderizar('RegistrarMotivoSalida',"modales");




    }


    public function metEliminarMotivoSalida()
    {
        $idMotivoSalida = $this->metObtenerInt('idMotivoSalida');
        $this->atMotivoSalidaModelo->metEliminarMotivoSalida($idMotivoSalida);
        $array = array(
            'status' => 'OK',
            'idMotivoSalida' => $idMotivoSalida
        );

        echo json_encode($array);
    }




}