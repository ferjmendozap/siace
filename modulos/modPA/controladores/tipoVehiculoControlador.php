<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/


class tipoVehiculoControlador extends Controlador
{
    private $atTipoVehiculoModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atTipoVehiculoModelo = $this->metCargarModelo('tipoVehiculo');
    }
	


    #Metodo Index del controlador Prueba.
    public function metIndex()
    {
    }

    #Metodo para cargar el listado de los diferentes tipos de   vehículos
    public function metListarTipoVehiculo()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
            'multi-select/multi-select555c',

        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'multi-select/jquery.multi-select',);



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('_TipoVehiculoPost', $this->atTipoVehiculoModelo->metGetTipoVehiculo());
        $this->atVista->metRenderizar('ListarTipoVehiculo');
    }
	

    #Metodo para cargar el form registro  de tipo de  vehiculo
    public function metRegistrarTipoVehiculo()
    {


        $valido=$this->metObtenerInt('valido');


        $Valores = array("ind_descripcion" => " ", );
        $Accion=array( "accion" => "registrar",);

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );


        if($valido==1) {

            $TipoVehiculo=$this->metObtenerAlphaNumerico('TipoVehiculo');
            $ClaseVehiculo=$this->metObtenerInt('ClaseVehiculo');
            $id = $this->atTipoVehiculoModelo->metSetTipoVehiculo($TipoVehiculo,$ClaseVehiculo);
            $datos=[];
            $datos['idTipo']=$id;
            $datos['tipo']=$TipoVehiculo;
            echo json_encode($datos);
            exit;


        }
        # Llamado al modelo claseVehiculo para la carga de select en el formulario.
        $this->_ClaseModelo = $this->metCargarModelo('claseVehiculo');
        # Asignamos el resultado del llamado a la variable _ClaseVehiculoPost.
        $this->atVista->assign('_ClaseVehiculoPost', $this->_ClaseModelo->metGetClaseVehiculo());


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        
        $this->atVista->assign('_TipoVehiculoPost',$Valores);
        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarTipoVehiculo',"modales");




    }

    #Metodo para cargar el form para modificar una tipo de vehiculo
    public function metModificarTipoVehiculo()
    {


        $valido=$this->metObtenerInt('valido');
        $id_valor=$this->metObtenerInt('idPost');

        $Accion=array( "accion" => "modificar");


        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {
            $TipoVehiculo=$this->metObtenerAlphaNumerico('TipoVehiculo');
            $ClaseVehiculo=$this->metObtenerInt('ClaseVehiculo');
            $this->atTipoVehiculoModelo->metUpdateTipoVehiculo($TipoVehiculo,$ClaseVehiculo,$id_valor);
            $datos=[];
            $datos['idTipo']=$id_valor;
            $datos['tipo']=$TipoVehiculo;
            echo json_encode($datos);
            exit;

        }

        # Llamado al modelo claseVehiculo para la carga de select en el formulario.
        $this->_ClaseModelo = $this->metCargarModelo('claseVehiculo');
        # Asignamos el resultado del llamado a la variable _ClaseVehiculoPost.
        $this->atVista->assign('_ClaseVehiculoPost', $this->_ClaseModelo->metGetClaseVehiculo());


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        
        $this->atVista->assign('_TipoVehiculoPost',$this->atTipoVehiculoModelo->metMostrar($id_valor));
        $this->atVista->assign('_Acciones', $Accion);

        $this->atVista->metRenderizar('RegistrarTipoVehiculo',"modales");



    }

    #Metodo para eliminar  un  registro de tipo vehiculo
    public function metEliminarTipoVehiculo()
    {

        $id_valor=$this->metObtenerInt('idPost');

        $this->atTipoVehiculoModelo->metDeleteTipoVehiculo($id_valor);




    }




}