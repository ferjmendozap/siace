<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/


class piezaVehiculoControlador extends Controlador
{
    private $atPiezaVehiculoModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atPiezaVehiculoModelo = $this->metCargarModelo('piezaVehiculo');
    }


    #Metodo Index del controlador Prueba.
    public function metIndex()
    {
    }

    #Metodo para cargar el listado de los diferentes tipos de   vehículos
    public function metListarPiezaVehiculo()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
            'multi-select/multi-select555c',

        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'multi-select/jquery.multi-select',);



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('_PiezaVehiculoPost', $this->atPiezaVehiculoModelo->metGetPiezaVehiculo());
        $this->atVista->metRenderizar('ListarPiezaVehiculo');
    }


    #Metodo para cargar el form registro  de pieza de  vehiculo
    public function metRegistrarPiezaVehiculo()
    {


        $valido = $this->metObtenerInt('valido');


        $Valores = array("ind_descripcion" => " ",);
        $Accion = array("accion" => "registrar",);

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if ($valido == 1) {

            $PiezaVehiculo = $this->metObtenerAlphaNumerico('PiezaVehiculo');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }


            $id = $this->atPiezaVehiculoModelo->metSetPiezaVehiculo($PiezaVehiculo, $validacion['UbicacionPiezaA'], $validacion['UbicacionPiezaB'], $validacion['UbicacionPiezaC']);
            $datos=[];
            $datos['idPieza']=$id;
            $datos['pieza']=$PiezaVehiculo;
            echo json_encode($datos);
            exit;

        }


        $this->atVista->assign('_UbicacionPiezaA',$this->atPiezaVehiculoModelo->metMostrarSelect('UBIA'));
        $this->atVista->assign('_UbicacionPiezaB',$this->atPiezaVehiculoModelo->metMostrarSelect('UBIB'));
        $this->atVista->assign('_UbicacionPiezaC',$this->atPiezaVehiculoModelo->metMostrarSelect('UBIC'));

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);

        $this->atVista->assign('_PiezaVehiculoPost', $Valores);
        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarPiezaVehiculo', "modales");


    }

    #Metodo para cargar el form para modificar una pieza de vehiculo
    public function metModificarPiezaVehiculo()
    {


        $valido = $this->metObtenerInt('valido');
        $id_valor = $this->metObtenerInt('idPost');

        $Accion = array("accion" => "modificar");


        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if ($valido == 1) {
            $PiezaVehiculo = $this->metObtenerAlphaNumerico('PiezaVehiculo');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            $this->atPiezaVehiculoModelo->metUpdatePiezaVehiculo($PiezaVehiculo,  $validacion['UbicacionPiezaA'], $validacion['UbicacionPiezaB'], $validacion['UbicacionPiezaC'], $id_valor);
            $datos=[];
            $datos['idPieza']=$id_valor;
            $datos['pieza']=$PiezaVehiculo;
            echo json_encode($datos);
            exit;

        }


        $this->atVista->assign('_UbicacionPiezaA',$this->atPiezaVehiculoModelo->metMostrarSelect('UBIA'));
        $this->atVista->assign('_UbicacionPiezaB',$this->atPiezaVehiculoModelo->metMostrarSelect('UBIB'));
        $this->atVista->assign('_UbicacionPiezaC',$this->atPiezaVehiculoModelo->metMostrarSelect('UBIC'));

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);

        $this->atVista->assign('_PiezaVehiculoPost', $this->atPiezaVehiculoModelo->metMostrar($id_valor));
        $this->atVista->assign('_Acciones', $Accion);

        $this->atVista->metRenderizar('RegistrarPiezaVehiculo', "modales");


    }

    #Metodo para eliminar  un  registro de marca vehiculo
    public function metEliminarPiezaVehiculo()
    {
        $id_valor = $this->metObtenerInt('idPost');

        $this->atPiezaVehiculoModelo->metDeletePiezaVehiculo($id_valor);
    }

    public function metPersona($persona, $idCampo = false)
    {
        $this->atVista->assign('lista', $this->atChoferModelo->metGetPersonas());
        $this->atVista->assign('listaPersona', $persona);
        $this->atVista->assign('idCampo', $idCampo);
        $this->atVista->metRenderizar('listadoPersona', 'modales');
    }



}