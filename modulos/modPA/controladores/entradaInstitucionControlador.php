<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/



class entradaInstitucionControlador extends Controlador
{
    private $atEntradaInstitucionModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEntradaInstitucionModelo = $this->metCargarModelo('entradaInstitucion');
    }
	


    #Metodo Index del controlador Prueba.
    public function metIndex()
    {


    }

    #Metodo para cargar el listado de los diferentes tipos de   vehículos
    public function metListarEntradaInstitucion()
    {



        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
            'multi-select/multi-select555c',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'multi-select/jquery.multi-select',
            );

        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('_EntradaInstitucionPost', $this->atEntradaInstitucionModelo->metGetEntradaInstitucion());


        $this->atVista->metRenderizar('ListadoEntradaInstitucion');
    }
	

    #Metodo para cargar el form registro  de tipo de  vehiculo
    public function metRegistrarEntradaInstitucion()
    {



        $valido=$this->metObtenerInt('valido');

        $Accion=array( "accion" => "registrar",);

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );


        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }




            $hasta=$this->metFormatoFecha($_POST['entrada']);

            $hora_hasta=$validacion['hora_hasta'];
            $hora_hasta = strtotime($hora_hasta);
            $hora_hasta = date("H:i", $hora_hasta);
            $hasta=trim( $hasta." ".$hora_hasta.":00");

            $this->_SalidaIntitucionModelo = $this->metCargarModelo('salidaInstitucion');
            $consulta=$this->_SalidaIntitucionModelo->metGetFkSolicitud($validacion['Salida']);
            $id=$this->atEntradaInstitucionModelo->metSetEntradaInstitucion($hasta,$validacion['kilometraje'],$validacion['ObservacionEntrada'] ,$validacion['Salida'],$consulta[0]['fk_pad001_num_solicitud_vehiculo']);
            $respuesta=$this->atEntradaInstitucionModelo->metMostrar($id);

            $datos=[];
            $datos['idEntrada']=$id;
            $datos['modelo']=$respuesta['ind_modelo']." - ".$respuesta['ind_placa'];
            $datos['entrada']=$respuesta['fec_entrada'];
            $datos['observacion']=$validacion['ObservacionEntrada'];
            echo json_encode($datos);
            exit;
        }


        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_SalidaIntitucionModelo = $this->metCargarModelo('salidaInstitucion');
        $this->atVista->assign('_SalidaIntitucionPost',$this->_SalidaIntitucionModelo->metGetSalidaInstitucion());

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        

        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarEntradaInstitucion',"modales");


    }

    #Metodo para cargar el form para modificar una tipo de vehiculo
    public function metModificarEntradaInstitucion()
    {


        $valido=$this->metObtenerInt('valido');



        $Accion=array( "accion" => "modificar",);

        $id_valor=$this->metObtenerInt('idPost');

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );



        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }




            $hasta=$this->metFormatoFecha($_POST['entrada']);

            $hora_hasta=$validacion['hora_hasta'];
            $hora_hasta = strtotime($hora_hasta);
            $hora_hasta = date("H:i", $hora_hasta);
            $hasta=trim( $hasta." ".$hora_hasta.":00");






            $this->atEntradaInstitucionModelo->metUpdateEntradaInstitucion($hasta,$validacion['kilometraje'],$validacion['ObservacionEntrada'] ,$validacion['idEntrada']);

            $respuesta=$this->atEntradaInstitucionModelo->metMostrar($validacion['idEntrada']);

            $datos=[];
            $datos['idEntrada']=$validacion['idEntrada'];
            $datos['modelo']=$respuesta['ind_modelo']." - ".$respuesta['ind_placa'];
            $datos['entrada']=$respuesta['fec_entrada'];
            $datos['observacion']=$validacion['ObservacionEntrada'];
            echo json_encode($datos);
            exit;

        }

        $this->atVista->assign('_EntradaIntitucionPost',$this->atEntradaInstitucionModelo->metMostrar($id_valor));

       // # Llamado al modelo  Vehiculo para la carga de select en el formulario.
       // $this->_SalidaIntitucionModelo = $this->metCargarModelo('salidaInstitucion');
        $this->atVista->assign('_SalidaIntitucionPost',$this->atEntradaInstitucionModelo->metGetEntradaInstitucion());


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        

        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarEntradaInstitucion',"modales");


    }

    #Metodo para eliminar  un  registro de marca vehiculo
    public function metAnularEntradaInstitucion()
    {

        $id_valor=$this->metObtenerInt('idPost');
        $consulta=$this->atEntradaInstitucionModelo->metMostrar($id_valor);

        $this->atEntradaInstitucionModelo->metDeleteEntradaInstitucion($id_valor,$consulta['fk_pad001_num_solicitud_vehiculo']);




    }


    protected function metFormatoFecha($fecha)
    {
        $resultado=explode("/",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }
}