<?php
require_once 'datosCarga/dataBasicaInicialMiscelaneos.php';
require_once 'datosCarga/dataBasicaInicialMenu.php';
require_once 'datosCarga/dataParqueAutomotor.php';

class scriptCargaControlador extends Controlador
{

    use dataBasicaInicialMiscelaneos;
    use dataBasicaInicialMenu;
    use dataParqueAutomotor;
    private $atScriptCarga;

    public function __construct()
    {
        parent::__construct();
        $this->atScriptCarga = $this->metCargarModelo('scriptCarga');
    }



    public function metIndex()
    {
        echo "INICIANDO <br>";
        echo "Cargando Menu de Parque Automotor <br>";
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>MENU<br>';
        $this->atScriptCarga->metCargarMenu($this->metDataMenu());
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>MISCELANEOS<br>';
        $this->atScriptCarga->metCargarMiscelaneos($this->metDataMiscelaneos());
        ### Parque Automotor ###
 
        $this->metParqueAutomotor();

        echo "TERMINADO";
    }


    ### Función ParqueAutomotor
    public function metParqueAutomotor()
    {
        echo 'INICIO CARGA DE PARQUE AUTOMOTOR<br><br>';
        echo '-----Carga Tipo Vehiculo----<br><br>';
        $this->atScriptCarga->metTipoVehiculo($this->metDataTipoVehiculo());

    }




}
