<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/


class tipoMantenimientoControlador extends Controlador
{
    private $atTipoMantenimientoModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atTipoMantenimientoModelo = $this->metCargarModelo('tipoMantenimiento');
    }
	


    #Metodo Index del controlador Prueba.
    public function metIndex()
    {
    }

    #Metodo para cargar el listado de las diferentes ubicaciones de las piezas en los   vehículos
    public function metListarTipoMantenimiento()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
            'multi-select/multi-select555c',

        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'multi-select/jquery.multi-select',);



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);



        $this->atVista->assign('_TipoMantenimientoPost', $this->atTipoMantenimientoModelo->metGetTipoMantenimiento());
        $this->atVista->metRenderizar('ListarTipoMantenimiento');
    }

    public function metRegistrarTipoMantenimiento()
    {

        $idTipoMantenimiento= $this->metObtenerInt('idTipoMantenimiento');
        $valido=$this->metObtenerInt('valido');
        $maestro            = $this->metObtenerInt('maestro');




        $complementosJs = array(

            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(

            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {

            $this->metValidarToken();

            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formInt = $this->metObtenerInt('form', 'int');

            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }


            foreach ($formInt as $tituloInt => $valorInt) {
                if (!empty($formInt[$tituloInt])) {
                    $validacion[$tituloInt] = $valorInt;
                } else {
                    $validacion[$tituloInt] = '';
                }
            }

            if ($idTipoMantenimiento === 0) {



                $id = $this->atTipoMantenimientoModelo->metRegistrarTipoMantenimiento(
                    $validacion['cod_detalle'],
                    $validacion['tipoMantenimiento'],
                    $validacion['idMaestro']
                );
                if(is_array($id)){

                    $validacion=[];
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;

                }else{
                    $validacion['status'] = 'creacion';
                    $validacion['idTipoMantenimiento'] = $id;
                    echo json_encode($validacion);
                    exit;

                }



            } else {


                $validacion['status'] = 'modificacion';

                $this->atTipoMantenimientoModelo->metModificarTipoMantenimiento($idTipoMantenimiento,
                    $validacion['cod_detalle'],
                    $validacion['tipoMantenimiento']
                );
                $validacion['idTipoMantenimiento'] = $idTipoMantenimiento;
                echo json_encode($validacion);
                exit;


            }
        }

        if($idTipoMantenimiento!=0){
            $db = $this->atTipoMantenimientoModelo->metMostrarTipoMantenimiento($idTipoMantenimiento);
            $this->atVista->assign('formDB',$db);
        }

        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->assign('idMaestro',$maestro);
        $this->atVista->assign('idTipoMantenimiento',$idTipoMantenimiento);

        $this->atVista->metRenderizar('RegistrarTipoMantenimiento',"modales");




    }


    public function metEliminarTipoMantenimiento()
    {
        $idTipoMantenimiento = $this->metObtenerInt('idTipoMantenimiento');
        $this->atTipoMantenimientoModelo->metEliminarTipoMantenimiento($idTipoMantenimiento);
        $array = array(
            'status' => 'OK',
            'idTipoMantenimiento' => $idTipoMantenimiento
        );

        echo json_encode($array);
    }




}