<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/


class polizaControlador extends Controlador
{
    private $atPolizaModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atPolizaModelo = $this->metCargarModelo('poliza');

    }
	



    public function metIndex()
    {
    }


    public function metListarPoliza()
    {


        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
            'multi-select/multi-select555c',

        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'multi-select/jquery.multi-select',);



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('_PolizaPost', $this->atPolizaModelo->metGetPoliza());
        $this->atVista->metRenderizar('ListarPoliza');
    }



    public function metModificarPoliza()
    {


        $valido=$this->metObtenerInt('valido');
        $id_valor=$this->metObtenerInt('idPost');

        $Accion=array( "accion" => "modificar");




        $js = array(
            'modPA/jquery-validation/dist/jquery.validate.min',
            'modPA/jquery-validation/dist/additional-methods.min',
            'modPA/wizard/jquery.bootstrap.wizard.min',
            'modPA/core/source/App',
            'modPA/core/source/AppForm',
            'modPA/inputmask/jquery.inputmask.bundle.min',

            'modPA/core/demo/DemoFormWizard',
            'modPA/core/demo/DemoFormComponents',


        );

        if($valido==1) {

            #Recibiendo las variables del formulario que utiliza la clase póliza
            $Aseguradora=$this->metObtenerInt('Aseguradora');

            $NumeroPoliza=$this->metObtenerAlphaNumerico('NumeroPoliza');

            $NumeroSeguro=$this->metObtenerAlphaNumerico('NumeroSeguro');

            $desde=$this->metFormatoFecha($this->metObtenerTexto('desde'));

            $hasta=$this->metFormatoFecha($this->metObtenerTexto('hasta'));

            $MontoPagado=$this->metObtenerMonto('MontoPagado');
            $MontoPagado=str_replace(",",".",$MontoPagado);
            $MontoCobertura=$this->metObtenerMonto('MontoCobertura');
            $MontoCobertura=str_replace(",",".",$MontoCobertura);
            $Vehiculo=$this->metObtenerInt('Vehiculo');



             $this->atPolizaModelo->metUpdatePoliza($NumeroPoliza,$NumeroSeguro,$desde,$hasta,"1",$MontoPagado,$MontoCobertura,$Vehiculo,"1",$id_valor);



        }
        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo->metGetVehiculo());

        $this->atVista->metCargarJs($js);
        
        $this->atVista->assign('_PolizaPost',$this->atPolizaModelo->metMostrar($id_valor));
        $this->atVista->assign('_Acciones', $Accion);

        $this->atVista->metRenderizar('RegistrarPoliza',"modales");



    }


    public function metEliminarPoliza()
    {

        $id_valor=$this->metObtenerInt('idPost');

        $this->atPolizaModelo->metDeletePoliza($id_valor);




    }

    protected function metFormatoFecha($fecha)
    {
        $resultado=explode("/",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }

}