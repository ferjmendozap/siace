<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        13-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
trait dataBasicaInicialMiscelaneos
{


    public function metMiscelaneosPA()
    {
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--->>Modulo PA<br>';
        $a005_miscelaneo_maestro = array( array('ind_nombre_maestro' => 'MARCA DE VEHICULOS', 'ind_descripcion' => 'MARCA DE VEHICULOS',  'fk_a015_num_seguridad_aplicacion' => '12', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'MARVEH',
            'Det' => array(
                array('cod_detalle' => '01', 'ind_nombre_detalle' => 'CHEVROLET'),
                array('cod_detalle' => '02', 'ind_nombre_detalle' => 'CHRYSLER'),
                array('cod_detalle' => '03', 'ind_nombre_detalle' => 'TOYOTA'),
                array('cod_detalle' => '04', 'ind_nombre_detalle' => 'FIAT'),
                array('cod_detalle' => '05', 'ind_nombre_detalle' => 'FORD'),
            )
        ),
            array('ind_nombre_maestro' => 'Motivo de Salida', 'ind_descripcion' => 'Motivo de salida de un vehículo',  'fk_a015_num_seguridad_aplicacion' => '12', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'MOTSAL',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Diligencia CE'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Compras'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Jornada OAC'),
                )
            ),
            array('ind_nombre_maestro' => 'Clase de Vehículo', 'ind_descripcion' => 'Clase de Vehículo',  'fk_a015_num_seguridad_aplicacion' => '12', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'CLASEVEH',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Carro'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Moto'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Camioneta'),
                )
            ),
            array('ind_nombre_maestro' => 'Tipo de Mantenimiento', 'ind_descripcion' => 'Tipo de Mantenimiento',  'fk_a015_num_seguridad_aplicacion' => '12', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOMANT',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Revisión '),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Cambio de Aceite'),
                )
            ),
            array('ind_nombre_maestro' => 'Tipo de Salida', 'ind_descripcion' => 'Tipo de Salida',  'fk_a015_num_seguridad_aplicacion' => '12', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOSAL',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Regional'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Nacional'),
                    array('cod_detalle' => '3 ', 'ind_nombre_detalle' => 'Internacional'),
                )
            ),
            array('ind_nombre_maestro' => 'Estado de la Solicitud', 'ind_descripcion' => 'Estado de la Solicitud',  'fk_a015_num_seguridad_aplicacion' => '12', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'ESTADOSOL',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Preparado'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Verificado'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Conformado'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'Aprobado'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'Asignado'),
                    array('cod_detalle' => '6', 'ind_nombre_detalle' => 'Salida'),
                    array('cod_detalle' => '7 ', 'ind_nombre_detalle' => 'Completado'),
                    array('cod_detalle' => '8', 'ind_nombre_detalle' => 'Anulado'),
                )
            ),
            array('ind_nombre_maestro' => 'Ubicacion A', 'ind_descripcion' => 'Ubicacion A',  'fk_a015_num_seguridad_aplicacion' => '12', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'UBIA',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Superior'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Inferior'),

                )
            ),
            array('ind_nombre_maestro' => 'Ubicacion B', 'ind_descripcion' => 'Ubicacion B',  'fk_a015_num_seguridad_aplicacion' => '12', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'UBIB',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Delantero'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Trasero'),

                )
            ),
            array('ind_nombre_maestro' => 'Ubicacion C', 'ind_descripcion' => 'Ubicacion C',  'fk_a015_num_seguridad_aplicacion' => '12', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'UBIC',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Derecha'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Izquierda'),

                )
            )
        );
        return $a005_miscelaneo_maestro;
    }



    public function metDataMiscelaneos()
    {
        $a005_miscelaneo_maestro = array_merge(

            $this->metMiscelaneosPA()
        );
        return $a005_miscelaneo_maestro;
    }
}
