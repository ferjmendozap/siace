<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/


class tipoSalidaControlador extends Controlador
{
    private $atTipoSalidaModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atTipoSalidaModelo = $this->metCargarModelo('tipoSalida');
    }
	


    #Metodo Index del controlador Prueba.
    public function metIndex()
    {
    }

    #Metodo para cargar el listado de los diferentes tipos de   vehículos
    public function metListarTipoSalida()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
            'multi-select/multi-select555c',

        );
        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'multi-select/jquery.multi-select',);



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('_TipoSalidaPost', $this->atTipoSalidaModelo->metGetTipoSalida());
        $this->atVista->metRenderizar('ListarTipoSalida');
    }

    public function metRegistrarTipoSalida()
    {

        $idTipoSalida= $this->metObtenerInt('idTipoSalida');
        $valido=$this->metObtenerInt('valido');
        $maestro            = $this->metObtenerInt('maestro');




        $complementosJs = array(

            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(

            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {

            $this->metValidarToken();

            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formInt = $this->metObtenerInt('form', 'int');

            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }


            foreach ($formInt as $tituloInt => $valorInt) {
                if (!empty($formInt[$tituloInt])) {
                    $validacion[$tituloInt] = $valorInt;
                } else {
                    $validacion[$tituloInt] = '';
                }
            }

            if ($idTipoSalida === 0) {



                $id = $this->atTipoSalidaModelo->metRegistrarTipoSalida(
                    $validacion['cod_detalle'],
                    $validacion['tipoSalida'],
                    $validacion['idMaestro']
                );

                if(is_array($id)){

                    $validacion=[];
                    $validacion['status'] = 'error';
                    $validacion['estado'] = 'creacion';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;

                }else{
                    $validacion['estado'] = 'creacion';
                    $validacion['idTipoSalida'] = $id;
                    echo json_encode($validacion);
                    exit;


                }


            } else {


       
                $validacion['estado'] = 'creacion';
                $this->atTipoSalidaModelo->metModificarTipoSalida($idTipoSalida,
                    $validacion['cod_detalle'],
                    $validacion['tipoSalida']
                );
                $validacion['idTipoSalida'] = $idTipoSalida;
                echo json_encode($validacion);
                exit;


            }
        }

        if($idTipoSalida!=0){
            $db = $this->atTipoSalidaModelo->metMostrarTipoSalida($idTipoSalida);
            $this->atVista->assign('formDB',$db);
        }

        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->assign('idMaestro',$maestro);
        $this->atVista->assign('idTipoSalida',$idTipoSalida);

        $this->atVista->metRenderizar('RegistrarTipoSalida',"modales");




    }


    public function metEliminarTipoSalida()
    {
        $idTipoSalida = $this->metObtenerInt('idTipoSalida');
        $this->atTipoSalidaModelo->metEliminarTipoSalida($idTipoSalida);
        $array = array(
            'status' => 'OK',
            'idTipoSalida' => $idTipoSalida
        );

        echo json_encode($array);
    }


}