<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/

class accesorioControlador extends Controlador
{
    private $atAccesorioModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atAccesorioModelo = $this->metCargarModelo('accesorio');
    }
	


    #Metodo Index del controlador Accesorio.
    public function metIndex()
    {
    }

    #Metodo parac cargar el listado de los Accesorio
    public function metListarAccesorio()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'ckeditor/ckeditor',
            'ckeditor/adapters/jquery',

        );


        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);


        $this->atVista->assign('_AccesorioPost', $this->atAccesorioModelo->metGetAccesorio());
        $this->atVista->metRenderizar('ListarAccesorio');
    }
	

    #Metodo para cargar el form registro Accesorio vehiculo
    public function metRegistrarAccesorio()
    {


        $valido=$this->metObtenerInt('valido');


        $Valores = array("ind_descripcion_accesorio" => " " );
        $Accion=array( "accion" => "registrar",);

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {
            $Accesorio=$this->metObtenerAlphaNumerico('AccesorioVehiculo');
            $id = $this->atAccesorioModelo->metSetAccesorio($Accesorio);

            if(is_array($id)){

                $validacion=[];
                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $id;
                echo json_encode($validacion);
                exit;

            }else{

                $datos=[];
                $datos['status'] = 'registro';
                $datos['idAccesorio']=$id;
                $datos['accesorio']=$Accesorio;
                echo json_encode($datos);
                exit;

            }



        }

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        
        $this->atVista->assign('_AccesorioPost',$Valores);
        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarAccesorio',"modales");




    }

    #Metodo para cargar el form registro de un  Accesorio
    public function metModificarAccesorio()
    {


        $valido=$this->metObtenerInt('valido');
        $id_valor=$this->metObtenerInt('idPost');

        $Accion=array( "accion" => "modificar");



        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {
            $Accesorio=$this->metObtenerAlphaNumerico('AccesorioVehiculo');
            $this->atAccesorioModelo->metUpdateAccesorio($Accesorio,$id_valor);
            $datos=[];
            $datos['idAccesorio']=$id_valor;
            $datos['accesorio']=$Accesorio;
            echo json_encode($datos);
            exit;

        }

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        
        $this->atVista->assign('_AccesorioPost',$this->atAccesorioModelo->metMostrar($id_valor));
        $this->atVista->assign('_Acciones', $Accion);

        $this->atVista->metRenderizar('RegistrarAccesorio',"modales");



    }


    public function metEliminarAccesorio()
    {

        $id_valor=$this->metObtenerInt('idPost');

        $this->atAccesorioModelo->metDeleteAccesorio($id_valor);




    }




}