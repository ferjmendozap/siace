<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/


class dannioControlador extends Controlador
{
    private $atDannioModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atDannioModelo = $this->metCargarModelo('dannio');
    }
	


    #Metodo Index del controlador Prueba.
    public function metIndex()
    {


    }


    public function metListarDannio()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
            'multi-select/multi-select555c',

        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'multi-select/jquery.multi-select',);



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('_DannioPost', $this->atDannioModelo->metGetDannio());
        $this->atVista->metRenderizar('ListadoDannio');
    }


    public function metListarVehiculoDannio()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
            'multi-select/multi-select555c',

        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'multi-select/jquery.multi-select',);



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('_DannioPost', $this->atDannioModelo->metGetVehiculosConDannio());
        $this->atVista->metRenderizar('ListadoVehiculoDannio');
    }

    public function metVerVehiculoDannio()
    {

        $id_valor=$this->metObtenerInt('idPost');

        $valido=$this->metObtenerInt('valido');

        $Accion=array( "accion" => "ver",);



        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        $this->atVista->assign('_VehiculoPost',$this->atDannioModelo->metGetDannioVehiculo($id_valor));

        $this->atVista->metCargarJs($js);

        $this->atVista->metCargarJsComplemento($complementosJs);

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->assign('_Acciones', $Accion);

        $this->atVista->metRenderizar('VerVehiculoDannio',"modales");





    }

    public function metRegistrarDannio()
    {


        $valido=$this->metObtenerInt('valido');



        $Accion=array( "accion" => "registrar",);


        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }


            $desde=$this->metFormatoFecha($_POST['entrada']);

            $desde=trim($desde." 00:00:00");




            $id = $this->atDannioModelo->metSetDannio($validacion['ObservacionDannio'],$desde,$validacion['Vehiculo'],$validacion['menu']);
            $respuesta=$this->atDannioModelo->metMostrar($id);

            $datos=[];
            $datos['idDanio']=$id;
            $datos['modelo']=$respuesta['ind_modelo']." - ".$respuesta['ind_placa'];
            $datos['fecha']=$respuesta['fec_registro'];
            $datos['pieza']=$respuesta['pieza'] ;

            echo json_encode($datos);
            exit;

        }



        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo->metGetVehiculo());




        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_PiezaModelo = $this->metCargarModelo('piezaVehiculo');
        $this->atVista->assign('_PiezaPost',$this->_PiezaModelo->metGetPiezaVehiculo());

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        

        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarDannio',"modales");


    }

    #Metodo para cargar el form para modificar una tipo de vehiculo
    public function metModificarDannio()
    {


        $valido=$this->metObtenerInt('valido');



        $Accion=array( "accion" => "modificar",);

        $id_valor=$this->metObtenerInt('idPost');


        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );


        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }


            $desde=$this->metFormatoFecha($_POST['entrada']);


            $desde=trim($desde." 00:00:00");

            $this->atDannioModelo->metUpdateDannio($validacion['ObservacionDannio'],$desde,$validacion['Vehiculo'],$validacion['menu'],$validacion['bd_foto'],$validacion['idDannio']);
            $respuesta=$this->atDannioModelo->metMostrar($validacion['idDannio']);

            $datos=[];
            $datos['idDanio']=$validacion['idDannio'];
            $datos['modelo']=$respuesta['ind_modelo']." - ".$respuesta['ind_placa'];
            $datos['fecha']=$respuesta['fec_registro'];
            $datos['pieza']=$respuesta['pieza'] ;

            echo json_encode($datos);
            exit;

        }


        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');

        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo->metGetVehiculo());

        $this->atVista->assign('_DannioPost',$this->atDannioModelo->metMostrar($id_valor));


        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_PiezaModelo = $this->metCargarModelo('piezaVehiculo');

        $this->atVista->assign('_PiezaPost',$this->_PiezaModelo->metGetPiezaVehiculo());
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        

        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarDannio',"modales");


    }

    #Metodo para eliminar  un  registro de marca vehiculo
    public function metAnularDannio()
    {
        $id_valor=$this->metObtenerInt('idPost');

        $this->atDannioModelo->metDeleteDannio($id_valor);
    }

 

    protected function metFormatoFecha($fecha)
    {
        $resultado=explode("/",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }
}