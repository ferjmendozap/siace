<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/


class reporteControlador extends Controlador
{
    private $atReporteModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atReporteModelo = $this->metCargarModelo('reporte');
    }


    #Metodo Index del controlador Prueba.
    public function metIndex()
    {

    }
    #Metodo para cargar el listado de los diferentes tipos de   vehículos
    public function metvehiculosReportePdf($placa,$modelo)
    {
        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        //$obtenerUsuario= $this->atBusquedaEquipo->metUsuarioReporte($usuario);
        $this->metObtenerLibreria('cabeceraEquipo','modPA');
        $pdf = new pdfReporteVehiculo('L','mm','Letter');
        $filtro="WHERE 1 ";
        if($placa!='no'){
            $filtro.="AND (pa_b001_vehiculo.ind_placa LIKE '%$placa%' OR pa_b001_vehiculo.pk_num_vehiculo LIKE '%$placa%') ";
        }
        if($modelo!='no'){
            $filtro.="AND pa_b001_vehiculo.ind_modelo LIKE '%$modelo%' ";
        }
        //$filtro.="ORDER BY $modelo";

        # Llamado al modelo Vehiculo para la carga de los valores a asignar en los select
        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');

        $consulta = $this->_VehiculoModelo->metGetVehiculo('1',$filtro);

        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(5, 5);
        $pdf->Cell(5, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, utf8_decode('PLACA'), 1, 0, 'C', 1);
        $pdf->Cell(40, 5, 'SERIAL DEL MOTOR', 1, 0, 'C', 1);
        $pdf->Cell(60, 5, utf8_decode('SERIAL DE LA CARROCERÍA'), 1, 0, 'C', 1);
        $pdf->Cell(30, 5, utf8_decode('MARCA'), 1, 0, 'C', 1);
        $pdf->Cell(40, 5, utf8_decode('MODELO'), 1, 0, 'C', 1);
        $pdf->Cell(20, 5, utf8_decode('CLASE'), 1, 0, 'C', 1);
        $pdf->Cell(30, 5, utf8_decode('TIPO VEHÍCULO'), 1, 1, 'C', 1);
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', '', 6);
        $pdf->SetDisplayMode('real');

        for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {$pdf->Cell(5, 5);
            $pdf->Cell(5, 5,utf8_decode(($i+1)), 1, 0, 'C', 1);
            $pdf->Cell(25, 5, utf8_decode($consulta[$i]['ind_placa']), 1, 0, 'C', 1);
            $pdf->Cell(40, 5, utf8_decode($consulta[$i]['ind_serial_motor']), 1, 0, 'C', 1);
            $pdf->Cell(60, 5, utf8_decode($consulta[$i]['ind_serial_carroceria']), 1, 0, 'C', 1);
            $pdf->Cell(30, 5, utf8_decode($consulta[$i]['ind_marca']), 1, 0, 'C', 1);
            $pdf->Cell(40, 5, utf8_decode($consulta[$i]['ind_modelo']), 1, 0, 'C', 1);
            $pdf->Cell(20, 5, utf8_decode($consulta[$i]['ind_descripcion']), 1, 0, 'C', 1);
            $pdf->Cell(30, 5, utf8_decode($consulta[$i]['ind_tipo']), 1, 1, 'C', 1);
        }
        $pdf->Output();
    }   

    #Metodo para cargar el listado de los diferentes tipos de   vehículos
    public function metvehiculoReporte()
    {
        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',
            'materialSiace/core/demo/DemoFormComponents',
        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );






        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);



        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo-> metGetVehiculo());

        $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
        $this->atVista->assign('_MotivoSalidaPost',$this->_MotivoSalidaModelo-> metGetMotivoSalida());


        $this->atVista->metRenderizar('ListadoVehiculos' );

    }


    #Metodo para cargar el listado de los diferentes tipos de   vehículos
    public function metinstitucionReporte()
    {

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',
            'materialSiace/core/demo/DemoFormComponents',
        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );






        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);



        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo-> metGetVehiculo());

        $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
        $this->atVista->assign('_MotivoSalidaPost',$this->_MotivoSalidaModelo-> metGetMotivoSalida());


        $this->atVista->metRenderizar('ListarSalidaEntradaInstitucion' );


    }

    public function metinstitucionReportePdf()
    {

        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
        $formTxt=$this->metObtenerTexto('form','txt');
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formAlphaNum)) {
            foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                    $validacion[$tituloAlphaNum] = $valorAlphaNum;
                } else {
                    $validacion[$tituloAlphaNum] = 'error';
                }
            }
        }

       if($_POST['desde']!=""){

        $desde=$this->metFormatoFecha($_POST['desde']);
        $fechadesde=trim($desde);
		}else{
		$fechadesde="error";
		}
 	    if($_POST['hasta']!=""){
        $hasta=$this->metFormatoFecha($_POST['hasta']);
        $fechahasta=trim($hasta);
		}else{
		$fechahasta="error";
		}



        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        //$obtenerUsuario= $this->atBusquedaEquipo->metUsuarioReporte($usuario);
        $this->metObtenerLibreria('cabeceraEquipo','modPA');
        $pdf=new pdfReporteInstitucion('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(5, 5);
        $pdf->Cell(5, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, utf8_decode('VEHÍCULO'), 1, 0, 'C', 1);
        $pdf->Cell(55, 5, 'CHOFER', 1, 0, 'C', 1);

        $pdf->Cell(20, 5, 'MOTIVO', 1, 0, 'C', 1);
        $pdf->Cell(18, 5, 'TIPO SALIDA', 1, 0, 'C', 1);
        $pdf->Cell(18, 5, 'F-H SALIDA', 1, 0, 'C', 1);
        $pdf->Cell(18, 5, 'F-H ENTRADA', 1, 0, 'C', 1);
        $pdf->Cell(18, 5, 'KM SALIDA', 1, 0, 'C', 1);
        $pdf->Cell(18, 5, 'KM ENTRADA', 1, 0, 'C', 1);
        $pdf->Cell(60, 5, utf8_decode('OBSERVACIÓN'), 1, 1, 'C', 1);
        $pdf->SetFont('Arial', '', 7);

        $this->_ReporteModelo = $this->metCargarModelo('reporte');
        $consulta= $this->_ReporteModelo-> metGetReporteInstitucion($validacion['vehiculo'],$fechadesde,$fechahasta,$validacion['motivo']);

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);

        for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {

            $pdf->SetWidths(array(5,25,55,20,18,18,18,18,18,60));
            $pdf->SetAligns(array('C','C','C','C','C','C','C','C','C','C'));
            $pdf->Cell(5,5); //
            $pdf->Row(array($consulta[$i]['pk_num_entrada'],
                utf8_decode($consulta[$i]['ind_modelo']." - ".$consulta[$i]['ind_placa']),
                 utf8_decode($consulta[$i]['ind_nombre1'])." ".utf8_decode($consulta[$i]['ind_apellido1']),

                utf8_decode($consulta[$i]['ind_motivo']),
                utf8_decode($consulta[$i]['ind_tipo_salida']),
                utf8_decode($consulta[$i]['fec_hora_salida'])." ".$consulta[$i]['hora_salida'],
                utf8_decode($consulta[$i]['fec_hora_entrada'])." ".$consulta[$i]['hora_entrada'],
                utf8_decode($consulta[$i]['ind_kilometraje']),
                utf8_decode($consulta[$i]['ind_kilometraje_entrada']),
                utf8_decode($consulta[$i]['ind_observacion_entrada'])
            ));
        }
        $pdf->Output();

    }


    public function metentradaTallerReporte()
    {

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);



        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo-> metGetVehiculo());

        $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
        $this->atVista->assign('_MotivoSalidaPost',$this->_MotivoSalidaModelo-> metGetMotivoSalida());


        $this->atVista->metRenderizar('ListadoEntradaTaller' );


    }


    public function metentradaTallerReportePdf()
    {

        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
        $formTxt=$this->metObtenerTexto('form','txt');
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formAlphaNum)) {
            foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                    $validacion[$tituloAlphaNum] = $valorAlphaNum;
                } else {
                    $validacion[$tituloAlphaNum] = 'error';
                }
            }
        }

        if($_POST['desde']!=""){
            $desde=$this->metFormatoFecha($_POST['desde']);
            $fechadesde=trim($desde);
        }else{
            $fechadesde="error";
        }
        if($_POST['hasta']!=""){
            $hasta=$this->metFormatoFecha($_POST['hasta']);
            $fechahasta=trim($hasta);
        }else{
            $fechahasta="error";
        }

        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        //$obtenerUsuario= $this->atBusquedaEquipo->metUsuarioReporte($usuario);
        $this->metObtenerLibreria('cabeceraEquipo','modPA');
        $pdf=new pdfReporteEntradaTaller('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(5, 5);
        $pdf->Cell(5, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, utf8_decode('VEHÍCULO'), 1, 0, 'C', 1);
        $pdf->Cell(50, 5, 'CHOFER', 1, 0, 'C', 1);
        $pdf->Cell(30, 5, 'TALLER', 1, 0, 'C', 1);
        $pdf->Cell(30, 5, 'SEGURO POR', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'F-H ENTRADA', 1, 0, 'C', 1);
        $pdf->Cell(18, 5, 'KM ENTRADA', 1, 0, 'C', 1);
        $pdf->Cell(65, 5, utf8_decode('OBSERVACIÓN'), 1, 1, 'C', 1);
        $pdf->SetFont('Arial', '', 7);

        $this->_ReporteModelo = $this->metCargarModelo('reporte');
        $consulta= $this->_ReporteModelo-> metGetReporteEntradaTaller($validacion['vehiculo'],$fechadesde,$fechahasta);

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);

        for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {
            if($consulta[$i]['ind_cobertura']==1){
                $PagadoPor="Contraloría";
            }else{
                $PagadoPor="Aseguradora";
            }

            $pdf->SetWidths(array(5,25,50,30,30,25,18,65));
            $pdf->SetAligns(array('C','L','L','L','L','L','L','L'));
            $pdf->Cell(5,5); //


            $pdf->Row(array($i+1,
                utf8_decode($consulta[$i]['ind_modelo']." - ".$consulta[$i]['ind_placa']),
                utf8_decode($consulta[$i]['ind_nombre1']." ".$consulta[$i]['ind_apellido1']),
                utf8_decode($consulta[$i]['nombreTaller']),
                utf8_decode($PagadoPor),
                utf8_decode($consulta[$i]['fec_entrada']),
                utf8_decode($consulta[$i]['ind_kilometraje']),
                utf8_decode($consulta[$i]['ind_observacion']),


            ));


        }
        $pdf->Output();

    }

    public function metsalidaTallerReporte()
    {

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);



        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo-> metGetVehiculo());

        $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
        $this->atVista->assign('_MotivoSalidaPost',$this->_MotivoSalidaModelo-> metGetMotivoSalida());


        $this->atVista->metRenderizar('ListadoSalidaTaller' );


    }

    public function metsalidaTallerReportePdf()
    {

        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
        $formTxt=$this->metObtenerTexto('form','txt');
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formAlphaNum)) {
            foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                    $validacion[$tituloAlphaNum] = $valorAlphaNum;
                } else {
                    $validacion[$tituloAlphaNum] = 'error';
                }
            }
        }

        if($_POST['desde']!=""){
            $desde=$this->metFormatoFecha($_POST['desde']);
            $fechadesde=trim($desde);
        }else{
            $fechadesde="error";
        }
        if($_POST['hasta']!=""){
            $hasta=$this->metFormatoFecha($_POST['hasta']);
            $fechahasta=trim($hasta);
        }else{
            $fechahasta="error";
        }



        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        //$obtenerUsuario= $this->atBusquedaEquipo->metUsuarioReporte($usuario);
        $this->metObtenerLibreria('cabeceraEquipo','modPA');
        $pdf=new pdfReporteSalidaTaller('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(5, 5);
        $pdf->Cell(5, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, utf8_decode('VEHÍCULO'), 1, 0, 'C', 1);
        $pdf->Cell(75, 5, utf8_decode('MOTIVO'), 1, 0, 'C', 1);
        $pdf->Cell(35, 5, utf8_decode('CONDICIONES'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'F-H SALIDA', 1, 0, 'C', 1);
        $pdf->Cell(18, 5, 'KM SALIDA', 1, 0, 'C', 1);
        $pdf->Cell(55, 5, utf8_decode('OBSERVACIÓN'), 1, 0, 'C', 1);
        $pdf->Cell(15, 5, utf8_decode('MONTO'), 1, 1, 'C', 1);
        $pdf->SetFont('Arial', '', 7);

        $this->_ReporteModelo = $this->metCargarModelo('reporte');
        $consulta= $this->_ReporteModelo-> metGetReporteSalidaTaller($validacion['vehiculo'],$fechadesde,$fechahasta);

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);

        for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {


            $pdf->SetWidths(array(5,25,75,35,25,18,55,15));
            $pdf->SetAligns(array('C','L','L','L','L','L','L','R'));
            $pdf->Cell(5,5); //



            $pdf->Row(array($i+1,
                utf8_decode($consulta[$i]['ind_modelo']." - ".$consulta[$i]['ind_placa']),
                utf8_decode($consulta[$i]['ind_motivo']),
                utf8_decode($consulta[$i]['ind_condiciones']),
                utf8_decode($consulta[$i]['fec_salida']),
                utf8_decode($consulta[$i]['ind_kilometraje']),
                utf8_decode($consulta[$i]['ind_observacion']),
                utf8_decode($consulta[$i]['num_monto']),


            ));


        }
        $pdf->Output();

    }



    public function metchoferReportePdf()
    {

        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        //$obtenerUsuario= $this->atBusquedaEquipo->metUsuarioReporte($usuario);
        $this->metObtenerLibreria('cabeceraEquipo','modPA');
        $pdf=new pdfReporteChofer('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(5, 5);
        $pdf->Cell(5, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(120, 5, "NOMBRE Y APELLIDO", 1, 0, 'C', 1);
        $pdf->Cell(120, 5, utf8_decode('OBSERVACIÓN'), 1, 1, 'C', 1);
        $pdf->SetFont('Arial', '', 6);



        $this->_ChoferModelo = $this->metCargarModelo('chofer');
        $consulta=$this->_ChoferModelo-> metGetChofer();


        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);

        for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {
            $pdf->SetWidths(array(5,120,120));
            $pdf->SetAligns(array('C','C','C'));
            $pdf->Cell(5,5);
            $pdf->Row(array($i + 1,
                utf8_decode($consulta[$i]['ind_nombre1'] . " " . $consulta[$i]['ind_apellido1']),
                utf8_decode($consulta[$i]['ind_observacion'])


            ));
        }
        $pdf->Output();
    }

    public function metchoferReporte()
    {
        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',
            'materialSiace/core/demo/DemoFormComponents',
        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );






        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);



        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo-> metGetVehiculo());

        $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
        $this->atVista->assign('_MotivoSalidaPost',$this->_MotivoSalidaModelo-> metGetMotivoSalida());


        $this->atVista->metRenderizar('ListadoChoferes' );

    }


    
    public function metmantenimientoReporte()
    {

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);



        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo-> metGetVehiculo());

        $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
        $this->atVista->assign('_MotivoSalidaPost',$this->_MotivoSalidaModelo-> metGetMotivoSalida());


        $this->atVista->metRenderizar('ListadoMantenimiento' );


    }

    public function metmantenimientoReportePdf()
    {

        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
        $formTxt=$this->metObtenerTexto('form','txt');
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formAlphaNum)) {
            foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                    $validacion[$tituloAlphaNum] = $valorAlphaNum;
                } else {
                    $validacion[$tituloAlphaNum] = 'error';
                }
            }
        }

        if($_POST['desde']!=""){
            $desde=$this->metFormatoFecha($_POST['desde']);
            $fechadesde=trim($desde);
        }else{
            $fechadesde="error";
        }
        if($_POST['hasta']!=""){
            $hasta=$this->metFormatoFecha($_POST['hasta']);
            $fechahasta=trim($hasta);
        }else{
            $fechahasta="error";
        }



        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        //$obtenerUsuario= $this->atBusquedaEquipo->metUsuarioReporte($usuario);
        $this->metObtenerLibreria('cabeceraEquipo','modPA');
        $pdf=new pdfReporteMantenimiento('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(5, 5);
        $pdf->Cell(5, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(50, 5, utf8_decode('VEHÍCULO'), 1, 0, 'C', 1);
        $pdf->Cell(45, 5, utf8_decode('TIPO MANT.'), 1, 0, 'C', 1);
        $pdf->Cell(55, 5, utf8_decode('EN OCACIÓN.'), 1, 0, 'C', 1);
        $pdf->Cell(35, 5, utf8_decode('FECHA DEL MANT.'), 1, 0, 'C', 1);
        $pdf->Cell(35, 5, utf8_decode('FECHA PROX. MANT.'), 1, 0, 'C', 1);
        $pdf->Cell(30, 5, utf8_decode('SIG. KM.'), 1, 1, 'C', 1);
        $pdf->SetFont('Arial', '', 7);

        $this->_ReporteModelo = $this->metCargarModelo('reporte');

        $consulta= $this->_ReporteModelo-> metGetReporteMantenimiento($validacion['vehiculo'],$fechadesde,$fechahasta);

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);

        for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {


            $pdf->SetWidths(array(5,50,45,55,35,35,30));
            $pdf->SetAligns(array('C','L','L','L','L','L','L'));
            $pdf->Cell(5,5); //



            $pdf->Row(array($i+1,
                utf8_decode($consulta[$i]['ind_modelo']." - ".$consulta[$i]['ind_placa']),
                utf8_decode($consulta[$i]['tipoMantenimiento']),
                utf8_decode($consulta[$i]['ind_en_ocacion']),
                utf8_decode($consulta[$i]['fec_mant']),
                utf8_decode($consulta[$i]['fec_proximo_mantenimiento']),
                utf8_decode($consulta[$i]['ind_num_siguiente_kilometraje']),
                  ));


        }
        $pdf->Output();

    }


    public function metdannioReporte()
    {

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);



        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo-> metGetVehiculo());

        $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
        $this->atVista->assign('_MotivoSalidaPost',$this->_MotivoSalidaModelo-> metGetMotivoSalida());


        $this->atVista->metRenderizar('ListadoDannio' );


    }

    public function metdannioReportePdf()
    {

        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
        $formTxt=$this->metObtenerTexto('form','txt');
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formAlphaNum)) {
            foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                    $validacion[$tituloAlphaNum] = $valorAlphaNum;
                } else {
                    $validacion[$tituloAlphaNum] = 'error';
                }
            }
        }
        if($_POST['desde']!=""){
            $desde=$this->metFormatoFecha($_POST['desde']);
            $fechadesde=trim($desde);
        }else{
            $fechadesde="error";
        }
        if($_POST['hasta']!=""){
            $hasta=$this->metFormatoFecha($_POST['hasta']);
            $fechahasta=trim($hasta);
        }else{
            $fechahasta="error";
        }



        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        //$obtenerUsuario= $this->atBusquedaEquipo->metUsuarioReporte($usuario);
        $this->metObtenerLibreria('cabeceraEquipo','modPA');
        $pdf=new pdfReporteDannio('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(5, 5);
        $pdf->Cell(5, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(35, 5, utf8_decode('VEHÍCULO'), 1, 0, 'C', 1);
        $pdf->Cell(18, 5, utf8_decode('FECHA REG.'), 1, 0, 'C', 1);
        $pdf->Cell(135, 5, utf8_decode('PIEZA'), 1, 0, 'C', 1);
        $pdf->Cell(18, 5, utf8_decode('ESTADO'), 1, 0, 'C', 1);
        $pdf->Cell(18, 5, utf8_decode('FECHA SOL.'), 1, 0, 'C', 1);
        $pdf->Cell(20, 5, utf8_decode('MONTO'), 1, 1, 'C', 1);
        $pdf->SetFont('Arial', '', 7);

        $this->_ReporteModelo = $this->metCargarModelo('reporte');

        $consulta= $this->_ReporteModelo-> metGetReporteDannio($validacion['vehiculo'],$fechadesde,$fechahasta);

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);

        for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {

            if(is_null($consulta[$i]['fec_solucion'])){
                $consulta[$i]['fec_solucion']="--";
                $consulta[$i]['estado']="Dañado";
                $consulta[$i]['ind_monto']="--";
            }else{
                $consulta[$i]['estado']="Solucionado";
                $consulta[$i]['num_monto'].=" Bs.";

            }

            $pdf->SetWidths(array(5,35,18,135,18,18,20));
            $pdf->SetAligns(array('C','C','C','C','C','C','C'));
            $pdf->Cell(5,5); //



            $pdf->Row(array($i+1,
                utf8_decode($consulta[$i]['ind_modelo']." - ".$consulta[$i]['ind_placa']),
                utf8_decode($consulta[$i]['fec_registro']),
                utf8_decode($consulta[$i]['pieza']),
                utf8_decode($consulta[$i]['estado']),
                utf8_decode($consulta[$i]['fec_solucion']),
                utf8_decode($consulta[$i]['num_monto']),
            ));


        }
        $pdf->Output();

    }

    public function metasignacionesReporte()
    {

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',

        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);



        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo-> metGetVehiculo());

        $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
        $this->atVista->assign('_MotivoSalidaPost',$this->_MotivoSalidaModelo-> metGetMotivoSalida());


        $this->atVista->metRenderizar('ListadoAsignaciones');


    }

    public function metasignacionesReportePdf()
    {

        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
        $formTxt=$this->metObtenerTexto('form','txt');
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formAlphaNum)) {
            foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                    $validacion[$tituloAlphaNum] = $valorAlphaNum;
                } else {
                    $validacion[$tituloAlphaNum] = 'error';
                }
            }
        }

        if($_POST['desde']!=""){
            $desde=$this->metFormatoFecha($_POST['desde']);
            $fechadesde=trim($desde);
        }else{
            $fechadesde="error";
        }
        if($_POST['hasta']!=""){
            $hasta=$this->metFormatoFecha($_POST['hasta']);
            $fechahasta=trim($hasta);
        }else{
            $fechahasta="error";
        }



        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        //$obtenerUsuario= $this->atBusquedaEquipo->metUsuarioReporte($usuario);
        $this->metObtenerLibreria('cabeceraEquipo','modPA');
        $pdf=new pdfReporteAsignaciones('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(5, 5);
        $pdf->Cell(5, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(55, 5, utf8_decode('VEHÍCULO'), 1, 0, 'C', 1);
        $pdf->Cell(155, 5, utf8_decode('CHOFER.'), 1, 0, 'C', 1);
        $pdf->Cell(35, 5, utf8_decode('FECHA DE SALIDA'), 1, 1, 'C', 1);


        $pdf->SetFont('Arial', '', 7);

        $this->_ReporteModelo = $this->metCargarModelo('reporte');

        $consulta= $this->_ReporteModelo-> metGetReporteAsignaciones($validacion['vehiculo'],$fechadesde,$fechahasta);

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);

        for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {


            $pdf->SetWidths(array(5,55,155,35));
            $pdf->SetAligns(array('C','C','C','C'));
            $pdf->Cell(5,5); //



            $pdf->Row(array($i+1,
                utf8_decode($consulta[$i]['ind_modelo']." - ".$consulta[$i]['ind_placa']),
                utf8_decode($consulta[$i]['ind_nombre1']." ".$consulta[$i]['ind_apellido1']),
                utf8_decode($consulta[$i]['fec_requerida']),


            ));


        }
        $pdf->Output();

    }


    public function metsolicitudReporte()
    {

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',

        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);



        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo-> metGetVehiculo());

        $this->_EstatusModelo = $this->metCargarModelo('estatus');
        $this->atVista->assign('_EstatusPost',$this->_EstatusModelo-> metGetEstatus());


        $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
        $this->atVista->assign('_MotivoSalidaPost',$this->_MotivoSalidaModelo-> metGetMotivoSalida());


        $this->atVista->metRenderizar('ListadoSolicitud');


    }

    public function metsolicitudReportePdf()
    {

        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
        $formTxt=$this->metObtenerTexto('form','txt');
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formAlphaNum)) {
            foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                    $validacion[$tituloAlphaNum] = $valorAlphaNum;
                } else {
                    $validacion[$tituloAlphaNum] = 'error';
                }
            }
        }

        if($_POST['desde']!=""){
            $desde=$this->metFormatoFecha($_POST['desde']);
            $fechadesde=trim($desde);
        }else{
            $fechadesde="error";
        }
        if($_POST['hasta']!=""){
            $hasta=$this->metFormatoFecha($_POST['hasta']);
            $fechahasta=trim($hasta);
        }else{
            $fechahasta="error";
        }



        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        //$obtenerUsuario= $this->atBusquedaEquipo->metUsuarioReporte($usuario);
        $this->metObtenerLibreria('cabeceraEquipo','modPA');
        $pdf=new pdfReporteSolicitud('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(5, 5);
        $pdf->Cell(5, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(55, 5, utf8_decode('PERSONA SOLICITANTE'), 1, 0, 'C', 1);
        $pdf->Cell(34, 5, utf8_decode('FECHA PREPARACIÓN'), 1, 0, 'C', 1);

        $pdf->Cell(24, 5, utf8_decode('ESTATUS'), 1, 0, 'C', 1);
        $pdf->Cell(35, 5, utf8_decode('MOTIVO'), 1, 0, 'C', 1);
        $pdf->Cell(100, 5, utf8_decode('OBSERVACIÓN'), 1, 1, 'C', 1);


        $pdf->SetFont('Arial', '', 7);

        $this->_ReporteModelo = $this->metCargarModelo('reporte');

        $consulta= $this->_ReporteModelo-> metGetReporteSolicitud($validacion['vehiculo'],$fechadesde,$fechahasta,$validacion['estatus'],$validacion['motivo']);

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);

        for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {

            $pdf->SetWidths(array(5,55,34,24,35,100));
            $pdf->SetAligns(array('C','C','C','C','C','C','C'));
            $pdf->Cell(5,5); //



            $pdf->Row(array($i+1,
                utf8_decode($consulta[$i]['ind_nombre1']." ".$consulta[$i]['ind_apellido1']),
                utf8_decode($consulta[$i]['fec_preparacion']),

                utf8_decode($consulta[$i]['estatus']),
                utf8_decode($consulta[$i]['ind_motivo']),
                utf8_decode($consulta[$i]['ind_observacion']),
            ));


        }
        $pdf->Output();

    }


    public function metdisponibleReporte()
    {

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',

        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);



        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost',$this->_VehiculoModelo-> metGetVehiculo());

        $this->_EstatusModelo = $this->metCargarModelo('estatus');
        $this->atVista->assign('_EstatusPost',$this->_EstatusModelo-> metGetEstatus());


        $this->_MotivoSalidaModelo = $this->metCargarModelo('motivoSalida');
        $this->atVista->assign('_MotivoSalidaPost',$this->_MotivoSalidaModelo-> metGetMotivoSalida());


        $this->atVista->metRenderizar('ListadoDisponibilidad');


    }

    public function metdisponibleReportePdf()
    {


        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        //$obtenerUsuario= $this->atBusquedaEquipo->metUsuarioReporte($usuario);
        $this->metObtenerLibreria('cabeceraEquipo','modPA');
        $pdf=new pdfReporteDisponibilidad('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(5, 5);
        $pdf->Cell(5, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(55, 5, utf8_decode('VEHÍCULO'), 1, 0, 'C', 1);
        $pdf->Cell(34, 5, utf8_decode('MARCA'), 1, 0, 'C', 1);

        $pdf->Cell(24, 5, utf8_decode('CLASE'), 1, 0, 'C', 1);
        $pdf->Cell(35, 5, utf8_decode('TIPO'), 1, 0, 'C', 1);
        $pdf->Cell(100, 5, utf8_decode('ESTADO'), 1, 1, 'C', 1);


        $pdf->SetFont('Arial', '', 7);

        $this->_ReporteModelo = $this->metCargarModelo('reporte');

        $consulta= $this->_ReporteModelo-> metGetReporteDisponibilidad();

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);

        for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {

            if($consulta[$i]['num_estatus']=='1'){
                $consulta[$i]['num_estatus']="Disponible";
            }else{
                $consulta[$i]['num_estatus']=" En Taller";
            }

            $pdf->SetWidths(array(5,55,34,24,35,100));
            $pdf->SetAligns(array('C','C','C','C','C','C','C'));
            $pdf->Cell(5,5); //



            $pdf->Row(array($i+1,
                utf8_decode($consulta[$i]['ind_modelo']." ".$consulta[$i]['ind_placa']),
                utf8_decode($consulta[$i]['ind_marca']),
                utf8_decode($consulta[$i]['ind_descripcion']),
                utf8_decode($consulta[$i]['ind_tipo']) ,
                utf8_decode($consulta[$i]['num_estatus']),
            ));


        }
        $pdf->Output();

    }
    protected function metFormatoFecha($fecha)
    {
        $resultado=explode("/",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }



}