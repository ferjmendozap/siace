<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/


class salidaTallerControlador extends Controlador
{
    private $atSalidaTallerModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atSalidaTallerModelo = $this->metCargarModelo('salidaTaller');
    }
	



    public function metIndex()
    {

    }


    public function metListarSalidaTaller()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
            'multi-select/multi-select555c',

        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'multi-select/jquery.multi-select',);



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('_SalidaTallerPost', $this->atSalidaTallerModelo->metGetSalidaTaller());
        $this->atVista->metRenderizar('ListadoSalidaTaller');
    }
	


    public function metRegistrarSalidaTaller()
    {


        $valido=$this->metObtenerInt('valido');



        $Accion=array( "accion" => "registrar",);

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );


        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            $formMonto=$this->metObtenerMonto('form','monto');

            if(!empty($formMonto)){
                foreach ($formMonto as $tituloInt => $valorInt) {
                    if(!empty($formMonto[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }


            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }




            $hasta=$this->metFormatoFecha($_POST['salida']);

            $hora_hasta=$validacion['hora'];
            $hora_hasta = strtotime($hora_hasta);
            $hora_hasta = date("H:i", $hora_hasta);
            $fecha=trim( $hasta." ".$hora_hasta.":00");





            $validacion['MontoPagado']=str_replace(",", ".", $validacion['MontoPagado']);
            $this->_EntradaTallerModelo = $this->metCargarModelo('entradaTaller');
            $EntradaTallerPost=$this->_EntradaTallerModelo->metMostrar($validacion['EntradaTaller']);


            $id = $this->atSalidaTallerModelo->metSetSalidaTaller($fecha,$validacion['Condiciones'],$validacion['Kilometraje'],$validacion['MontoPagado'],$validacion['ObservacionSalida'],$validacion['EntradaTaller'],$EntradaTallerPost[0]['fk_pab001_num_vehiculo']);
            $respuesta=$this->atSalidaTallerModelo->metMostrarModificar($id  );

            $datos=[];
            $datos['idSalida']=$id  ;
            $datos['modelo']=$respuesta[0]['ind_modelo']." - ".$respuesta[0]['ind_placa'];
            $datos['monto']=number_format($respuesta[0]['num_monto'] , 2, ',', '.');
            $datos['salida']=$respuesta[0]['fec_entrada'];
            $datos['observacion']=$validacion['ObservacionSalida'];
            echo json_encode($datos);
            exit;
        }



        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_EntradaTallerModelo = $this->metCargarModelo('entradaTaller');
        $this->atVista->assign('_EntradaTallerPost',$this->_EntradaTallerModelo->metGetEntradaTaller());





        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_ChoferModelo = $this->metCargarModelo('chofer');
        $this->atVista->assign('_ChoferPost',$this->_ChoferModelo->metGetChofer());

        # Llamado al modelo  Vehiculo para la carga de select en el formulario.
        $this->_TallerModelo = $this->metCargarModelo('taller');
        $this->atVista->assign('_TallerPost',$this->_TallerModelo->metGetTaller());


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        

        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarSalidaTaller',"modales");


    }


    public function metModificarSalidaTaller()
    {


        $valido=$this->metObtenerInt('valido');



        $Accion=array( "accion" => "modificar",);

        $id_valor=$this->metObtenerInt('idPost');

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );




        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            $formMonto=$this->metObtenerMonto('form','monto');

            if(!empty($formMonto)){
                foreach ($formMonto as $tituloInt => $valorInt) {
                    if(!empty($formMonto[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }


            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }




            $hasta=$this->metFormatoFecha($_POST['salida']);

            $hora_hasta=$validacion['hora'];
            $hora_hasta = strtotime($hora_hasta);
            $hora_hasta = date("H:i", $hora_hasta);
            $fecha=trim( $hasta." ".$hora_hasta.":00");








            $validacion['MontoPagado']=str_replace(",", ".", $validacion['MontoPagado']);

            $this->atSalidaTallerModelo->metUpdateSalidaTaller($fecha,$validacion['Condiciones'],$validacion['Kilometraje'],$validacion['ObservacionSalida'],$validacion['MontoPagado'], $validacion['idPost'] );
            $respuesta=$this->atSalidaTallerModelo->metMostrarModificar($validacion['idPost'] );

            $datos=[];
            $datos['idSalida']=$validacion['idPost'] ;
            $datos['modelo']=$respuesta[0]['ind_modelo']." - ".$respuesta[0]['ind_placa'];
            $datos['monto']=number_format($respuesta[0]['num_monto'] , 2, ',', '.');
            $datos['salida']=$respuesta[0]['fec_entrada'];
            $datos['observacion']=$validacion['ObservacionSalida'];
            echo json_encode($datos);
            exit;
        }

        $this->atVista->assign('_SalidaTallerPost',$this->atSalidaTallerModelo->metMostrarModificar($id_valor));

        $this->atVista->metCargarJs($js);

        $this->atVista->metCargarJsComplemento($complementosJs);

        $this->atVista->metCargarCssComplemento($complementosCss);

        $this->atVista->metCargarJs($js);
        
        $this->atVista->assign('_Acciones', $Accion);

        $this->atVista->metRenderizar('ModificarSalidaTaller',"modales");

    }


    public function metAnularSalidaTaller()
    {

        $id_valor=$this->metObtenerInt('idPost');

        $this->atSalidaTallerModelo->metDeleteSalidaTaller($id_valor);




    }
    protected function metObtenerMonto($clave,$posicion=false)
    {
        if((isset($_POST[$clave]) && !empty($_POST[$clave]))){
            if(is_array($_POST[$clave])){
                if($posicion) {
                    if (isset($_POST[$clave][$posicion]) && !empty($_POST[$clave][$posicion])) {
                        foreach ($_POST[$clave][$posicion] as $titulo => $valor) {
                            if(is_array($_POST[$clave][$posicion][$titulo])){
                                foreach($_POST[$clave][$posicion][$titulo] as $titulo1 => $valor1){
                                    $_POST[$clave][$posicion][$titulo][$titulo1] = (string)htmlspecialchars(preg_replace('/[^0-9 ,]/i', '', $_POST[$clave][$posicion][$titulo][$titulo1]));
                                }
                            }else{
                                if(isset($_POST[$clave][$posicion][$titulo]) && !empty($_POST[$clave][$posicion][$titulo])) {
                                    $_POST[$clave][$posicion][$titulo] = (string)htmlspecialchars(preg_replace('/[^0-9 ,]/i', '', $_POST[$clave][$posicion][$titulo]));
                                }
                            }
                        }
                        return $_POST[$clave][$posicion];
                    }
                }else{
                    if (isset($_POST[$clave])) {
                        foreach ($_POST[$clave] as $titulo => $valor) {
                            $_POST[$clave][$titulo] = (string)htmlspecialchars(preg_replace('/[^0-9 ,]/i', '', $_POST[$clave][$titulo]));
                        }
                        return $_POST[$clave];
                    }
                }
            }else{
                $_POST[$clave] = (string)htmlspecialchars(preg_replace('/[^0-9 ,]/i', '', $_POST[$clave]));
                return $_POST[$clave];
            }
        }
    }

    protected function metFormatoFecha($fecha)
    {
        $resultado=explode("/",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }
}