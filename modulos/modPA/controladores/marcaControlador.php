<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/


class marcaControlador extends Controlador
{
    private $atMarcaModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atMarcaModelo = $this->metCargarModelo('marca');
    }
	


    #Metodo Index del controlador Marca.
    public function metIndex()
    {

    }

    #Metodo parac cargar el listado de las marcas de los vehículos
    public function metListarMarcas()
    {


        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
            'multi-select/multi-select555c',

        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'multi-select/jquery.multi-select',);



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
         
        $this->atVista->assign('_MarcaPost', $this->atMarcaModelo->metGetMarcas());
        $this->atVista->metRenderizar('ListarMarcas');
    }
	

    #Metodo para cargar el form registro marca vehiculo
    public function metRegistrarMarcaVehiculo()
    {

        $idMarca= $this->metObtenerInt('idMarca');
        $valido=$this->metObtenerInt('valido');
        $maestro            = $this->metObtenerInt('maestro');




        $complementosJs = array(

            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(

            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {

            $this->metValidarToken();

            $formTxt = $this->metObtenerTexto('form', 'txt');
            $formInt = $this->metObtenerInt('form', 'int');

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt])) {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = '';

                }
            }

            foreach ($formInt as $tituloInt => $valorInt) {
                if (!empty($formInt[$tituloInt])) {
                    $validacion[$tituloInt] = $valorInt;
                } else {
                    $validacion[$tituloInt] = '';
                }
            }

            if ($idMarca === 0) {


                $validacion['status'] = 'creacion';
                $id = $this->atMarcaModelo->metRegistrarMarca(
                    $validacion['cod_detalle'],
                    $validacion['marca'],
                    $validacion['idMaestro']
                );

                $validacion['idMarca'] = $id;
                echo json_encode($validacion);
                exit;


            } else {


                $validacion['status'] = 'modificacion';

                $this->atMarcaModelo->metModificarMarca($idMarca,
                    $validacion['cod_detalle'],
                    $validacion['marca']
                );
                $validacion['idMarca'] = $idMarca;
                echo json_encode($validacion);
                exit;


            }
        }

        if($idMarca!=0){
            $db = $this->atMarcaModelo->metMostrarMarca($idMarca);
            $this->atVista->assign('formDB',$db);
        }

        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->assign('idMaestro',$maestro);
        $this->atVista->assign('idMarca',$idMarca);

        $this->atVista->metRenderizar('RegistrarMarcaVehiculo',"modales");




    }


    public function metEliminarMarca()
    {
        $idMarca = $this->metObtenerInt('idMarca');
        $this->atMarcaModelo->metEliminarMarca($idMarca);
        $array = array(
            'status' => 'OK',
            'idMarca' => $idMarca
        );

        echo json_encode($array);
    }




}