<link type="text/css"   rel="stylesheet" href="{$_Parametros.ruta_Complementos}theme-default/libs/wizard/wizard.css?1425466601" />





<!-- BEGIN VALIDATION FORM WIZARD -->


<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
    <form action="" id="formAjax"class="form   form-validation" role="form"  >



        <input type="hidden" value="1" name="valido"  id="valido" />

        <div class="form-wizard-nav">
            <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
            <ul class="nav nav-justified">
                                <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">GENERAL</span></a></li>

                                <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title"> DAÑOS A PAGAR DEL VEHÍCULO</span></a></li>
                                <li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title"> INFORMACIÓN DEL PAGO</span></a></li>



                            </ul>
                        </div><!--end .form-wizard-nav -->
                        <div class="tab-content clearfix">
                            <div class="tab-pane active" id="step1">





                            <input type="hidden" value="{if isset($_Solucion.pk_num_solucion)}{$_Solucion.pk_num_solucion}{/if}"  name="form[int][idSolucion]"      id="idSolucion" />




                            
                                <input type="hidden" value="{if isset($_Acciones.accion)}{$_Acciones.accion}{/if}"    id="accion" />
                          

                            <div class="col-lg-6">


                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <select {if  ($_Acciones.accion=="modificar")} disabled {/if}  required="" class="form-control" name="form[int][Vehiculo]" id="Vehiculo" aria-required="true">
                                            <option value="">&nbsp;</option>

 
                                            {foreach item=tipo from=$_VehiculoPost}

                                                {if isset($_Vehiculo)}

                                                    {if $tipo.pk_num_vehiculo==$_Vehiculo['vehiculo']}
                                                        <option selected value="{$tipo.pk_num_vehiculo}">{$tipo.ind_modelo} - {$tipo.ind_placa} </option>
                                                    {else}
                                                        <option value="{$tipo.pk_num_vehiculo}">{$tipo.ind_modelo} - {$tipo.ind_placa}</option>
                                                    {/if}
                                                {else}
                                                    <option value="{$tipo.pk_num_vehiculo}">{$tipo.ind_modelo} - {$tipo.ind_placa}</option>
                                                {/if}
                                            {/foreach}




                                        </select>
                                        <label for="rangelength2"> Vehículo </label>
                                    </div>
                                </div>


                            </div>






                        </div><!--end #step1 -->

                        <div class="tab-pane" id="step2">



                            <div class="card-body">
                                <form class="form">
                                    <div class="col-lg-6">Piezas Dañadas </div> <div class="col-lg-6"> &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  Daños Solucionados</div>
                                    <select  required="" multiple="multiple"   id="optgroup"  name="form[int][menu][]" style="position: absolute; left: -9999px;">




                                        {$i=0}
                                        {foreach item=menu from=$_VehiculoPieza}

                                            {if ($_VehiculoPost[$i]['ind_estado']==0)}
                                                <option value="{$menu.pk_num_averia_vehiculo}">{$menu.pieza} </option>
                                            {else}
                                                <option selected value="{$menu.pk_num_averia_vehiculo}">{$menu.pieza} </option>
                                            {/if}
                                            {$i++}
                                        {/foreach}




                                    </select>




                                </form>
                            </div>

                        </div><!--end #step2 -->
                            <div class="tab-pane" id="step3">
                                <div class="col-lg-6">

                                    <div class="form-group      ">
                                        <input  required=""  type="text" name="entrada" class="form-control fechas2" value="{if isset($_Solucion.fec_solucion)}{$_Solucion.fec_solucion}{/if}">
                                        <label>Fecha de Registro</label>
                                    </div>



                                    <div class="form-group">
                                        <input  required="" type="text"    value="{if isset($_Solucion.num_monto)}Bs. {$_Solucion.num_monto|number_format:2:",":"."}{/if}" name="form[monto][MontoPagado]" id="MontoPagado" class="form-control  " aria-required="true">
                                        <label for="digits2">Monto Pagado</label>
                                        <p class="help-block">Solo Números</p>
                                    </div>



                                    <div class="form-group">
                                           <textarea  required="" placeholder="" rows="3" class="form-control" id="ObservacionPago" name="form[alphaNum][ObservacionPago]">{if isset($_Solucion.ind_observacion )}{$_Solucion.ind_observacion}{/if}</textarea>
                                           <label for="textarea1">Observación del Pago</label>
                                    </div>


                                </div>
                                <div class="row">
                                    &nbsp;
                                </div>
                                <div class="row">
                                    &nbsp;
                                </div>

                                <div class="row">
                                    &nbsp;
                                </div>

                                <div class="row">
                                    &nbsp;
                                </div>

                                <div class="row">
                                    &nbsp;
                                </div>
                                <div class="row">
                                    &nbsp;
                                </div>
                                <span class="clearfix"></span>

                                <div class="modal-footer">

                                    <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>

                                    <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>


                                </div>
                            </div>       </div>

        <ul class="pager wizard">

            <li class="previous"><a class="btn-raised"       href="javascript:void(0);">Previo</a></li>

            <li class="next"><a class="btn-raised"           href="javascript:void(0);">Siguiente</a></li>
        </ul>




    </form>
</div>



<script type="text/javascript">


    $.validator.setDefaults({
        submitHandler: function() {


            var datos = $("#formAjax" ).serialize();



            var accion = $("#accion").val();
            if(accion=="registrar") {
                $.post("{$_Parametros.url}modPA/solucionDannioCONTROL/RegistrarSolucionDannioMET",datos, function (dato) {
                    $(document.getElementById('datatable1')).append('<td>'+dato['idSolucionDanio']+'</td>' +
                            '<td> '+dato['fecha']+'</td>' +
                            '<td>'+dato['modelo']+' </td>' +
                            '<td>'+dato['monto']+' </td>' +

                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idSolucionDanio="'+dato['idSolucionDanio']+'"' +
                            'descipcion="El Usuario a Modificado un registro de  Solución " titulo="Modificar Solución del Daño">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td   class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idSolucionDanio="'+dato['idSolucionDanio']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un registro de  Solución " titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el registro de Solución !!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');

                }, 'json');
                swal("Registro Agregado!", "La Solución al daño  ha sido agregado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }else {
                $.post("{$_Parametros.url}modPA/solucionDannioCONTROL/ModificarSolucionDannioMET", datos, function (dato) {
                    $(document.getElementById('idSolucionDanio'+dato['idSolucionDanio'])).html('<td>'+dato['idSolucionDanio']+'</td>' +
                            '<td> '+dato['fecha']+'</td>' +
                            '<td>'+dato['modelo']+' </td>' +
                            '<td>'+dato['monto']+' </td>' +


                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idSolucionDanio="'+dato['idSolucionDanio']+'"' +
                            'descipcion="El Usuario a Modificado el registro de Solución " titulo="Modificar Solución ">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td  class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idSolucionDanio="'+dato['idSolucionDanio']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado el registro de Solución " titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el registro de Solución !!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');

                }, 'json');
                swal("Registro Modifcado!", "La marca del vehículo ha sido modificado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }




        }
    });



    $(document).ready(function() {

        $("#formAjax").validate();
        $('#fechas').datepicker ({  format: 'dd/mm/yyyy', language:'es', startDate:'0d'});
        $('.fechas2').datepicker({ format: 'dd/mm/yyyy', language:'es', startDate:'0d' });
        $('#optgroup').multiSelect({  });
		$('#MontoPagado').inputmask(' 999.999.999,99 BS', { numericInput: true });




        $("#Vehiculo").change(function () {

            $("#Vehiculo option:selected").each(function () {

                $.post("{$_Parametros.url}modPA/solucionDannioCONTROL/ActualizarDannioMET", { Vehiculo: $('#Vehiculo').val() ,accion: $('#accion').val() }, function(dato){

                    $('#ContenidoModal').html(dato);

                });
            });
        });


    });
</script>