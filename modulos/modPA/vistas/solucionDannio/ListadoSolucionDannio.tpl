<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div id="body1">
    <div class="card">
        <div class="card-head">
            <header> <h2 class="text-primary">LISTAR DAÑOS SOLUCIONADOS EN VEHÍCULOS</h2></header>
        </div>
        <div class="col-md-12">
            <h5>Listado de los daños solucionados en los vehículos registrados en la institución.</h5>
        </div><!--end .col -->

    </div><!--end .card -->


    <div class="form-group   col-lg-12 ">

        <div class="col-lg-11 "></div>	<a class="btn btn-flat btn-accent ink-reaction span8" href="javascript:void(0);">filtrar</a>
    </div>

</div><!--end .card -->

<section class="style-default-bright">




    <!-- Listado -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-actionbar">
                <div class="card-actionbar-row">


                    
                </div>
            </div>
        </div><!--end .col -->
    </div><!--end .row -->
    <div class="row">

        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="sort-alpha col-sm-1">N° </th>
                        <th class="sort-alpha col-sm-2">Fecha Registro</th>
                        <th class="sort-alpha col-sm-2">Vehículo</th>




                           <th class="sort-alpha col-sm-1">Monto Pagado</th>
                        {if in_array('PA-01-03-02-02-M',$_Parametros.perfil)}<th class="sort-alpha col-sm-1">Modificar Solución</th>{/if}
                        {if in_array('PA-01-03-02-03-A',$_Parametros.perfil)}<th class="sort-alpha col-sm-1">Anular  Solución</th>{/if}






                    </tr>
                    </thead>
                    <tbody>

                    {foreach item=post from=$_SolucionDannioPost}
                        <tr id="idSolucionDanio{$post.pk_num_solucion}" class="gradeA">
                            <td>{$post.pk_num_solucion}</td>
                            <td>{$post.fec_solucion}</td>
                            <td>{$post.vehiculo} - {$post.ind_placa}</td>


                            <td>Bs. {$post.num_monto|number_format:2:",":"."}   </td>


                            {if in_array('PA-01-03-02-02-M',$_Parametros.perfil)}
                            <td>
                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Modificar Solución" descipcion="--" idSolucionDanio="{$post.pk_num_solucion}" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                </button>
                            </td>
                            {/if}
                            {if in_array('PA-01-03-02-03-A',$_Parametros.perfil)}
                            <td>
                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idSolucionDanio="{$post.pk_num_solucion}" mensaje="Estas seguro que desea eliminar el registro !!" titulo="Estas Seguro?" descipcion="El usuario a eliminado un registro" boton="si, Eliminar" idmenu="7">
                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                </button>
                            </td>
                            {/if}

                        </tr>








                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="6">
                            {if in_array('PA-01-03-01-01-N',$_Parametros.perfil)}
                                <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                        descripcion="Registro de solución de daño a vehículo de la institución"  titulo="Registrar Solución de Daño" id="nuevo" >
                                     <i class="md md-create"></i> Registrar Solución de Daño &nbsp;&nbsp;
                                </button>
                            {/if}
                        </th>

                    </tr>
                    </tfoot>
                </table>
            </div><!--end .table-responsive -->
        </div><!--end .col -->
    </div><!--end .row -->
    <!-- fin listado -->




    </div><!--end .row -->




    <script type="text/javascript">
        $(document).ready(function() {
            var $url='{$_Parametros.url}modPA/solucionDannioCONTROL/RegistrarSolucionDannioMET';
            $('#nuevo').click(function(){
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idMenu:0 },function($dato){
                    $('#ContenidoModal').html($dato);
                });

            });
            $('#modalAncho').css( "width", "85%" );




            $('#datatable1 tbody').on( 'click', '.modificar', function () {
                var $url='{$_Parametros.url}modPA/solucionDannioCONTROL/ModificarSolucionDannioMET';

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idSolucionDanio')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });

            $('#datatable1 tbody').on( 'click', '.eliminar', function () {


                var $url='{$_Parametros.url}modPA/solucionDannioCONTROL/AnularSolucionDannioMET';
                var idPost=$(this).attr('idSolucionDanio');
                var idMenu=$(this).attr('idMenu');
                swal({
                    title:$(this).attr('titulo'),
                    text: $(this).attr('mensaje'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false
                }, function(){

                    var $url='{$_Parametros.url}modPA/solucionDannioCONTROL/AnularSolucionDannioMET';
                    $(document.getElementById('idSalida'+idPost)).html('');


                    $.post($url,{ idPost: idPost},function($dato){
                        swal("Eliminado!", "el menu fue eliminado satisfactoriamente.", "success");
                    });


                });
            });


        });
    </script>
    </div>




