<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Proveedores</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Nombre y Apellido</th>
                            <th>Nro. Documento</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$lista}
                            <tr class="persona">
                                <input type="hidden"
                                       cedula="{$i.ind_documento_fiscal}"
                                       nombre="{$i.nomProveedor}"
                                       idProveedor="{$i.fk_a003_num_persona_proveedor}">
                                <td>{$i.nomProveedor}</td>
                                <td>{$i.ind_documento_fiscal}</td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatable2').on('click', 'tbody tr', function () {

            var input = $(this).find('input');

            $(document.getElementById('nombreProveedor')).val(input.attr('nombre'));
            $(document.getElementById('codProveedor')).val(input.attr('idProveedor'));

            $('#cerrarModal2').click();
        });

    });
</script>