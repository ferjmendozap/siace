<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div class="card">
    <div class="card-head">
        <header> <h2 class="text-primary">LISTAR VEHÍCULOS </h2></header>
    </div>
    <div class="col-md-12">
        <h5>Listado de vehículos registrados en la Contraloria.</h5>
    </div><!--end .col -->

</div><!--end .card -->





<section class="style-default-bright">


    <div class="row">

        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="sort-alpha col-sm-2"> N° </th>
                        <th class="sort-alpha col-sm-2">Modelo</th>
                        <th class="sort-alpha" width="458px">Placa</th>

                        <th class="sort-alpha col-sm-2">Año</th>
                        <th class="sort-alpha col-sm-1"> Editar</th>
                        <th class="sort-alpha col-sm-1"> Eliminar</th>

                    </tr>
                    </thead>
                    <tbody>

                    {foreach item=post from=$_VehiculoPost}
                        <tr id="idVehiculo{$post.pk_num_vehiculo}" class="gradeA">
                            <td>{$post.pk_num_vehiculo}</td>

                            <td>{$post.ind_modelo}</td>
                            <td>{$post.ind_placa}</td>
                            <td>{$post.fec_annio}</td>
                            <td>{if in_array('PA-01-06-01-01-02-M',$_Parametros.perfil)}

                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Modificar Vehículo" descipcion="El Usuario a Modificado un post" idVehiculo="{$post.pk_num_vehiculo}" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                </button>  {/if} </td>
                            <td>{if in_array('PA-01-06-01-01-03-E',$_Parametros.perfil)}
                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idVehiculo="{$post.pk_num_vehiculo}" mensaje="Estas seguro que desea eliminar la el post!!" titulo="Estas Seguro?" descipcion="El usuario a eliminado un post" boton="si, Eliminar" idmenu="7">
                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                </button>{/if}
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="6">

                            {if in_array('PA-01-06-01-01-01-N',$_Parametros.perfil)}
                                <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                        descripcion="el Usuario a registrado vehículo"  titulo="Registrar Vehículo" id="nuevo" >
                                    <i class="md md-create"></i> Registrar Vehículo &nbsp;&nbsp;&nbsp;
                                </button>
                            {/if}

                        </th>

                    </tr>
                    </tfoot>
                </table>
            </div><!--end .table-responsive -->
        </div><!--end .col -->
    </div><!--end .row -->
    <!-- fin listado -->




    </div><!--end .row -->


    <script type="text/javascript">

        $(document).ready(function() {
            $('#modalAncho').css( "width", "85%" );
            $('#nuevo').click(function(){

                $('#formModalLabel').html($(this).attr('titulo'));
                 var $url='{$_Parametros.url}modPA/vehiculoCONTROL/RegistrarVehiculoMET';
                 $.post($url,{ idMenu:0 },function($dato){
                    $('#ContenidoModal').html($dato);


                 });
        });




        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {

            var $url='{$_Parametros.url}modPA/vehiculoCONTROL/ModificarVehiculoMET';

            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idPost: $(this).attr('idVehiculo')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.eliminar', function () {


            var $url='{$_Parametros.url}modPA/vehiculoCONTROL/EliminarVehiculoMET';
            var idPost=$(this).attr('idVehiculo');
            var idMenu=$(this).attr('idMenu');
            swal({
                title:$(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){

                var $url='{$_Parametros.url}modPA/vehiculoCONTROL/EliminarVehiculoMET';


                $.post($url,{ idPost: idPost},function($dato){
                    $(document.getElementById('idVehiculo'+idPost)).html('');
                    swal("Eliminado!", "el menu fue eliminado satisfactoriamente.", "success");
                });

                ;
            });
        });

    </script>


