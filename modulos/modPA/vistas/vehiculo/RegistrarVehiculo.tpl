<!-- BEGIN VALIDATION FORM WIZARD -->

<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
    <form action="" id="formAjax"class="form   form-validation" role="form"  >
                        <input type="hidden" value="1" name="valido"  id="valido" />
                        <input type="hidden" value="{if isset($_VehiculoBD[0]['ind_color'])}{$_VehiculoBD[0]['ind_color']}{/if}" name="form[int][ColorBD]"  id="ColorBD" />
                        <input type="hidden" value="{if isset($_VehiculoBD[0]['pk_num_vehiculo'])}{$_VehiculoBD[0]['pk_num_vehiculo']}{/if}" name="form[int][Vehiculo]"  id="Vehiculo" />
                        <input type="hidden" value="{if isset($_PolizaPost[0]['pk_num_poliza'])}{$_PolizaPost[0]['pk_num_poliza']}{/if}" name="form[int][Poliza]"  id="Poliza" />

                         
                            <input type="hidden" value="{$_Acciones.accion}"    id="accion" />
                     

                        <div class="form-wizard-nav">
                            <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
                            <ul class="nav nav-justified">

                                <li class="active"><a href="#step1"   data-toggle="tab"><span class="step">1</span> <span class="title">GENERAL</span></a></li>
                                <li disabled ><a href="#step2" data-toggle="tab"   ><span  class="step">2</span> <span class="title">IMAGEN</span></a></li>
                                <li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title">SEGURO</span></a></li>

                            </ul>
                        </div><!--end .form-wizard-nav -->
                        <div class="tab-content clearfix">
                            <div class="tab-pane active" id="step1">


                                <div class="col-lg-6">


                                        <div class="form-group">
                                            <select required="" class="form-control" name="form[int][MarcaVehiculo]" id="MarcaVehiculo" aria-required="true">
                                                <option value="">&nbsp;</option>



                                                {foreach item=tipo from=$_MarcaVehiculoPost}

                                                    {if isset($_VehiculoBD[0]['fk_a006_num_marca_vehiculo'])}

                                                        {if $tipo.cod_detalle==$_VehiculoBD[0]['fk_a006_num_marca_vehiculo']}
                                                            <option selected value="{$tipo.cod_detalle}"> {$tipo.ind_nombre_detalle} </option>
                                                        {else}
                                                            <option value="{$tipo.cod_detalle}"> {$tipo.ind_nombre_detalle}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$tipo.cod_detalle}"> {$tipo.ind_nombre_detalle}</option>
                                                    {/if}
                                                {/foreach}





                                            </select>
                                            <label for="rangelength2">Marca del Vehículo</label>
                                        </div>


                                        <div class="form-group">
                                            <select required=""  class="form-control" name="form[int][ClaseVehiculo]" id="ClaseVehiculo" aria-required="true">
                                                <option value="">&nbsp;</option>



                                                {foreach item=tipo from=$_ClaseVehiculoPost}

                                                    {if isset($_VehiculoBD[0]['fk_a006_num_clase_vehiculo'])}

                                                        {if $tipo.cod_detalle==$_VehiculoBD[0]['fk_a006_num_clase_vehiculo']}
                                                            <option selected value="{$tipo.cod_detalle}"> {$tipo.ind_nombre_detalle}</option>
                                                        {else}
                                                            <option value="{$tipo.cod_detalle}"> {$tipo.ind_nombre_detalle}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$tipo.cod_detalle}"> {$tipo.ind_nombre_detalle}</option>
                                                    {/if}
                                                {/foreach}





                                            </select>
                                            <label for="rangelength2">Clase de Vehículo</label>
                                        </div>
                                        <div class="form-group">
                                            <select required="" class="form-control" name="form[int][TipoVehiculo]" id="TipoVehiculo" aria-required="true">
                                                <option value="">&nbsp;</option>


                                                {foreach item=tipo from=$_TipoVehiculoPost}

                                                    {if isset($_VehiculoBD[0]['fk_pab009_num_tipo_vehiculo'])}

                                                        {if $tipo.pk_num_tipo_vehiculo==$_VehiculoBD[0]['fk_pab009_num_tipo_vehiculo']}
                                                            <option selected value="{$tipo.pk_num_tipo_vehiculo}"> {$tipo.ind_descripcion}</option>
                                                        {else}
                                                            <option value="{$tipo.pk_num_tipo_vehiculo}"> {$tipo.ind_descripcion}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$tipo.pk_num_tipo_vehiculo}"> {$tipo.ind_descripcion}</option>
                                                    {/if}
                                                {/foreach}




                                            </select>
                                            <label for="rangelength2">Tipo de Vehículo</label>
                                        </div>

                                        <div class="form-group">
                                            <input type="text" value="{if isset($_VehiculoBD[0]['ind_modelo'])}{$_VehiculoBD[0]['ind_modelo']}{/if}" class="form-control" id="Modelo" name="form[alphaNum][Modelo]" data-rule-rangelength="[2, 65]" required>
                                            <label for="rangevalue2">Modelo del Vehículo</label>

                                        </div>
                                        <div class="form-group">
                                            <select required="" class="form-control" name="form[int][Color]" id="Color" aria-required="true">
                                                <option value="">&nbsp;</option>

                                                <option value="1">Azul</option>
                                                <option value="2">Amarillo</option>
                                                <option value="3">Beige</option>
                                                <option value="4">Blanco</option>
                                                <option value="5">Dorado</option>
                                                <option value="6">Gris</option>
                                                <option value="7">Marrón</option>
                                                <option value="8">Naranja</option>
                                                <option value="9">Negro</option>
                                                <option value="10">Plateado</option>
                                                <option value="11">Rojo</option>
                                                <option value="12">Verde</option>
                                                <option value="13">Violeta</option>



                                            </select>
                                            <label for="rangelength2">Color </label>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="Annio" name="form[int][Annio]"  value="{if isset($_VehiculoBD[0]['fec_annio'])}{$_VehiculoBD[0]['fec_annio']}{/if}" data-rule-rangelength="[4, 4]" required>
                                            <label for="rangelength2">Año</label>
                                            <p class="help-block">4 Dígitos</p>
                                        </div>	 </div><div class="col-lg-6 ">

                                    <div class="form-group">
                                        <input type="text" class="form-control" id="Placa" name="form[txt][Placa]"      value="{if isset($_VehiculoBD[0]['ind_placa'])}{$_VehiculoBD[0]['ind_placa']}{/if}" data-rule-rangelength="[6, 7]" required>
                                        <label for="rangelength2">Placa</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="SerialMotor" name="form[alphaNum][SerialMotor]" data-rule-rangelength="[7, 30]"  value="{if isset($_VehiculoBD[0]['ind_serial_motor'])}{$_VehiculoBD[0]['ind_serial_motor']}{/if}"required>
                                        <label for="rangelength2">Serial Motor</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="SerialCarroceria" name="form[alphaNum][SerialCarroceria]" data-rule-rangelength="[7, 30]"  value="{if isset($_VehiculoBD[0]['ind_serial_carroceria'])}{$_VehiculoBD[0]['ind_serial_carroceria']}{/if}" required>
                                        <label for="rangelength2">Serial Carroceria</label>
                                    </div>
                                </div>

                            </div><!--end #step1 -->
                            <div class="tab-pane" id="step2">


                     <div class="col-lg-12 ">
                                    <div class="col-sm-1 ">  </div>

                                    <div class="col-lg-3 ">

	 
                                        <output id="output_list2" style="width: auto">
                                            {if    isset($_VehiculoBD[0]['ind_placa'])}
                                        {if  ({$_VehiculoBD[0]['ind_url_foto_lateral_derecha']}!="faceman_Suburban_Assault_Vehicle_(Side).svg")}

                                            <img src="{$_Parametros['ruta_Img']}modPA/{$_VehiculoBD[0]['ind_placa']}/{$_VehiculoBD[0]['ind_url_foto_lateral_derecha']}" class="height-4 width-4 img-rounded " style="cursor: pointer" onclick="document.getElementById('files2').click()" />

                                        {else}
                                            <img src="{$_Parametros['ruta_Img']}modPA/faceman_Suburban_Assault_Vehicle_(Side).svg" class="height-4 width-4 img-rounded " style="cursor: pointer" onclick="document.getElementById('files2').click()" />
                                        {/if}
                                            {else}
                                                <img src="{$_Parametros['ruta_Img']}modPA/faceman_Suburban_Assault_Vehicle_(Side).svg" class="height-4 width-4 img-rounded " style="cursor: pointer" onclick="document.getElementById('files2').click()" />
                                            {/if}
                                        
                                        </output>
                                        <input style="display:none; width: 200px"  type="file" id="files2" name="files2" accept="image/jpeg" />
										<input type="hidden" name="form[txt][VLD]"  value="{if    isset($_VehiculoBD[0]['ind_url_foto_lateral_derecha'])}{($_VehiculoBD[0]['ind_url_foto_lateral_derecha'])} {else}faceman_Suburban_Assault_Vehicle_(Side).svg{/if}"    id="VLD" />
                                        <label for="rangelength2"> Vista Lateral Derecho



                                        </label>


                                    </div>
                                    <div class="col-lg-1 ">  </div>
                                    <div class="col-lg-3">

                                        <output id="output_list3" style="width: auto">

                                            {if    isset($_VehiculoBD[0]['ind_placa'])}
                                                {if  file_exists("{$_RutaModuloImagen['ruta']}{$_VehiculoBD[0]['ind_placa']}/{$_VehiculoBD[0]['ind_url_foto_lateral_izquierda']}")}

                                                    <img src="{$_Parametros['ruta_Img']}modPA/{$_VehiculoBD[0]['ind_placa']}/{$_VehiculoBD[0]['ind_url_foto_lateral_izquierda']}" class="height-4 width-4 img-rounded " style="cursor: pointer" onclick="document.getElementById('files3').click()" />

                                                {else}
                                                    <img src="{$_Parametros['ruta_Img']}modPA/faceman_Suburban_Assault_Vehicle_(Side)2.jpg" class="height-4 width-4 img-rounded " style="cursor: pointer" onclick="document.getElementById('files3').click()" />
                                                {/if}
                                            {else}
                                                <img src="{$_Parametros['ruta_Img']}modPA/faceman_Suburban_Assault_Vehicle_(Side)2.jpg" class="height-4 width-4 img-rounded " style="cursor: pointer" onclick="document.getElementById('files3').click()" />
                                            {/if}

                                        </output>
                                        <input style="display:none; width: 200px" class="ui-corner-all"  type="file" id="files3" name="files3"  accept="image/jpeg" />
                                        <input type="hidden" name="form[txt][VLI]"  value="{if    isset($_VehiculoBD[0]['ind_url_foto_lateral_izquierda'])}{($_VehiculoBD[0]['ind_url_foto_lateral_izquierda'])} {else}faceman_Suburban_Assault_Vehicle_(Side)2.jpg{/if}"    id="VLI" />
                                        <label for="rangelength2"> Vista Lateral Izquierdo </label>

                                        </div>


                               <div class="col-lg-1 ">  </div>
                                    <div class="col-lg-3 ">


                                        <output id="output_list4" style="width: auto">

                                            {if    isset($_VehiculoBD[0]['ind_placa'])}
                                                {if  file_exists("{$_RutaModuloImagen['ruta']}{$_VehiculoBD[0]['ind_placa']}/{$_VehiculoBD[0]['ind_url_foto_frontal']}")}

                                                    <img src="{$_Parametros['ruta_Img']}modPA/{$_VehiculoBD[0]['ind_placa']}/{$_VehiculoBD[0]['ind_url_foto_frontal']}" class="height-4 width-4 img-rounded " style="cursor: pointer" onclick="document.getElementById('files4').click()" />

                                                {else}
                                                    <img src="{$_Parametros['ruta_Img']}modPA/faceman_Suburban_Assault_Vehicle_(Front).svg" class="height-4 width-4 img-rounded " style="cursor: pointer" onclick="document.getElementById('files4').click()" />
                                                {/if}
                                            {else}
                                                <img src="{$_Parametros['ruta_Img']}modPA/faceman_Suburban_Assault_Vehicle_(Front).svg" class="height-4 width-4 img-rounded " style="cursor: pointer" onclick="document.getElementById('files4').click()" />
                                            {/if}

                                        </output>
                                        <input style="display:none; width: 200px" class="ui-corner-all"  type="file" id="files4" name="files4" accept="image/jpeg"  />
                                        <input type="hidden" name="form[txt][VF]"  value="{if    isset($_VehiculoBD[0]['ind_url_foto_frontal'])}{($_VehiculoBD[0]['ind_url_foto_frontal'])}{else}faceman_Suburban_Assault_Vehicle_(Front).svg{/if}"    id="VF" />
                                        <label for="rangelength2"> Vista Frontal   </label>


                                    </div>

                         <div class="col-sm-1 ">  </div>

                         <div class="col-lg-3 ">

                             <output id="output_list5" style="width: auto">
                                 {if    isset($_VehiculoBD[0]['ind_placa'])}
                                     {if  file_exists("{$_RutaModuloImagen['ruta']}{$_VehiculoBD[0]['ind_placa']}/{$_VehiculoBD[0]['ind_url_foto_trasera']}")}

                                         <img src="{$_Parametros['ruta_Img']}modPA/{$_VehiculoBD[0]['ind_placa']}/{$_VehiculoBD[0]['ind_url_foto_trasera']}" class="height-4 width-4 img-rounded " style="cursor: pointer" onclick="document.getElementById('files5').click()" />

                                     {else}
                                         <img src="{$_Parametros['ruta_Img']}modPA/Vehiculos-vista_trasera.gif" class="height-4 width-4 img-rounded " style="cursor: pointer" onclick="document.getElementById('files5').click()" />
                                     {/if}
                                 {else}
                                     <img src="{$_Parametros['ruta_Img']}modPA/Vehiculos-vista_trasera.gif" class="height-4 width-4 img-rounded " style="cursor: pointer" onclick="document.getElementById('files5').click()" />
                                 {/if}
                             </output>
                             <input style="display:none; width: 200px"  type="file" id="files5" name="files5" accept="image/jpeg"  />
                             <input type="hidden" name="form[txt][VT]"  value="{if    isset($_VehiculoBD[0]['ind_url_foto_trasera'])}{($_VehiculoBD[0]['ind_url_foto_trasera'])}{else}Vehiculos-vista_trasera.gif{/if}"    id="VT" />
                             <label for="rangelength2"> Vista Trasera</label>


                         </div>
                         <div class="col-lg-1 ">  </div>
                         <div class="col-lg-3">

                             <output id="output_list6" style="width: auto">
                                 {if    isset($_VehiculoBD[0]['ind_placa'])}

                                     {if  file_exists("{$_RutaModuloImagen['ruta']}{$_VehiculoBD[0]['ind_placa']}/{$_VehiculoBD[0]['ind_url_foto_superior']}")}

                                         <img src="{$_Parametros['ruta_Img']}modPA/{$_VehiculoBD[0]['ind_placa']}/{$_VehiculoBD[0]['ind_url_foto_superior']}" class="height-4 width-4 img-rounded " style="cursor: pointer" onclick="document.getElementById('files6').click()" />

                                     {else}
                                         <img src="{$_Parametros['ruta_Img']}modPA/Vehiculos-vista_aerea.png" class="height-4 width-4 img-rounded " style="cursor: pointer" onclick="document.getElementById('files6').click()" />
                                     {/if}
                                 {else}
                                     <img src="{$_Parametros['ruta_Img']}modPA/Vehiculos-vista_aerea.png" class="height-4 width-4 img-rounded " style="cursor: pointer" onclick="document.getElementById('files6').click()" />
                                 {/if}
                             </output>
                             <input style="display:none; width: 200px" class="ui-corner-all"  type="file" id="files6" name="files6" accept="image/jpeg"  />

                             <input type="hidden" name="form[txt][VS]" value="{if    isset($_VehiculoBD[0]['ind_url_foto_superior'])}{($_VehiculoBD[0]['ind_url_foto_superior'])}{else}Vehiculos-vista_aerea.png{/if}"    id="VS" />
                             <label for="rangelength2"> Vista Superior </label>

                         </div>


                         <div class="col-lg-1 ">  </div>
                         <div class="col-lg-3 ">

 
                             <output id="output_list7" style="width: auto">
                                 {if    isset($_VehiculoBD[0]['ind_placa'])}
                                     {if  file_exists("{$_RutaModuloImagen['ruta']}{$_VehiculoBD[0]['ind_placa']}/{$_VehiculoBD[0]['ind_url_foto_inferior']}")}

                                         <img src="{$_Parametros['ruta_Img']}modPA/{$_VehiculoBD[0]['ind_placa']}/{$_VehiculoBD[0]['ind_url_foto_inferior']}" class="height-4 width-4 img-rounded " style="cursor: pointer" onclick="document.getElementById('files7').click()" />

                                     {else}
                                         <img src="{$_Parametros['ruta_Img']}modPA/vista_inferior_vehiculo.png" class="height-4 width-4 img-rounded " style="cursor: pointer" onclick="document.getElementById('files7').click()" />
                                     {/if}
                                 {else}
                                     <img src="{$_Parametros['ruta_Img']}modPA/vista_inferior_vehiculo.png" class="height-4 width-4 img-rounded " style="cursor: pointer" onclick="document.getElementById('files7').click()" />
                                 {/if}
                             </output>
                             <input style="display:none; width: 200px" class="ui-corner-all"  type="file" id="files7" name="files7" accept="image/jpeg"  />
                             <input type="hidden" name="form[txt][VI]" value="{if    isset($_VehiculoBD[0]['ind_url_foto_inferior'])}{($_VehiculoBD[0]['ind_url_foto_inferior'])}{else}vista_inferior_vehiculo.png{/if}"    id="VI" />
                             <label for="rangelength2"> Vista Inferior</label>


                         </div>

                     </div>




                            </div><!--end #step2 -->
                            <div class="tab-pane" id="step3">




                                <div class="col-lg-6">


                                    <div class="col-sm-12">
                                        <div class="col-sm-10 form-group ">

                                            <input type="text" class="form-control"
                                                   id="nombreProveedor"
                                                   value="{if isset($_PolizaPost[0]['ind_nombre1'])}{$_PolizaPost[0]['ind_nombre1']}
                                               {else}
                                               {if isset($_PolizaPost[0]['ind_nombre2'])}{$_PolizaPost[0]['ind_nombre2']}  {$_PolizaPost[0]['ind_nombre3']}{/if}
                                               {/if}
                                                "
                                                   readonly >

                                            <input type="hidden" class="form-control"
                                                   id="codProveedor" name="form[int][Aseguradora]"
                                                   value="{if isset($_PolizaPost[0]['fk_lgb022_num_proveedor'])}{$_PolizaPost[0]['fk_lgb022_num_proveedor']}{/if}"
                                            >

                                            <label for="rangelength2">Aseguradora</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group floating-label">
                                                <button
                                                        class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                        type="button"
                                                        data-toggle="modal"
                                                        data-target="#formModal2"
                                                        titulo="Listado de Personas"
                                                        {if isset($ver) and $ver==1}disabled{/if}
                                                        url="{$_Parametros.url}modPA/vehiculoCONTROL/proveedorMET/proveedor/">
                                                    <i class="md md-search"></i>
                                                </button>
                                            </div>
                                        </div>

                                    </div>



                                </div>

                                <div class="col-lg-6">
                                <div class="form-group">

                                    <input type="text"  data-rule-digits="true" required="" value="{if isset($_PolizaPost[0]['ind_poliza'])}{$_PolizaPost[0]['ind_poliza']}{/if}" class="form-control" id="NumeroPoliza" name="form[int][NumeroPoliza]" data-rule-rangelength="[2, 65]" >
                                    <label for="rangelength2">Número de Póliza</label>

                                </div>

                                <div class="form-group">
                                    <input type="text" data-rule-digits="true" required=""   value="{if isset($_PolizaPost[0]['ind_recibo'])}{$_PolizaPost[0]['ind_recibo']}{/if}" class="form-control" id="NumeroSeguro" name="form[int][NumeroSeguro]" data-rule-rangelength="[2, 65]"  >
                                    <label for="rangelength2">Número de Seguro</label>

                                </div>


                                <div class="form-group">

                                    <div id="fechas" class="input-daterange input-group">
                                        <div class="input-group-content">
                                            <input  required=""  class="form-control" type="text" id="desde" name="desde" value="{if isset($_PolizaPost[0]['fec_desde'])}{$_PolizaPost[0]['fec_desde']}{/if}">
                                            <label>Fecha de Cobertura</label>
                                        </div>

                                        <span class="input-group-addon">Hasta</span>

                                        <div class="input-group-content">
                                            <input required=""  class="form-control" type="text"  id="hasta"  name="hasta" value="{if isset($_PolizaPost[0]['fec_hasta'])}{$_PolizaPost[0]['fec_hasta']}{/if}">
                                            <div class="form-control-line"></div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <input type="text" required=""    value="{if isset($_PolizaPost[0]['num_monto_pagado'])}{$_PolizaPost[0]['num_monto_pagado']|number_format:2:",":"."}{/if}" name="form[monto][MontoPagado]" id="MontoPagado" class="form-control MontoPagado" aria-required="true">
                                        <label for="digits2">Monto Pagado</label>
                                        <p class="help-block">Solo Números</p>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" required=""  value="{if isset($_PolizaPost[0]['num_monto_cobertura'])}{$_PolizaPost[0]['num_monto_cobertura']|number_format:2:",":"."}{/if}" name="form[monto][MontoCobertura]" id="MontoCobertura" class="form-control MontoPagado" aria-required="true">
                                        <label for="digits2">Monto Cobertura</label>
                                        <p class="help-block">Solo Números</p>
                                    </div>  </div>

                                </div>

                            <div class="row">
                                &nbsp;
                            </div>
                            <div class="row">
                                &nbsp;
                            </div>

                            <div class="row">
                                &nbsp;
                            </div>

                            <div class="row">
                                &nbsp;
                            </div>

                            <div class="row">
                                &nbsp;
                            </div>
                            <div class="row">
                                &nbsp;
                            </div>
                            <span class="clearfix"></span>

                            <div class="modal-footer">

                                <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>

                                <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>


                            </div>
                        </div>       </div>

<ul class="pager wizard">

    <li class="previous"><a class="btn-raised"       href="javascript:void(0);">Previo</a></li>

    <li class="next"><a class="btn-raised"           href="javascript:void(0);">Siguiente</a></li>
</ul>




</form>
</div>


<script type="text/javascript">




var contenido = new Array(); 
    $.validator.setDefaults({
        submitHandler: function() {


            var datos = $("#formAjax" ).serialize();



            var accion = $("#accion").val();
            if(accion=="registrar") {



                $.post("{$_Parametros.url}modPA/vehiculoCONTROL/RegistrarVehiculoMET",datos, function (dato) {


                    $(document.getElementById('datatable1')).append('<tr  id="idVehiculo'+dato['idVehiculo']+'">' +
                            '<td>'+dato['idVehiculo']+'</td>' +
                            '<td> '+dato['modelo']+'</td>' +
                            '<td> '+dato['placa']+'</td>' +
                            '<td> '+dato['anio']+'</td>' +
                            '<td   class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idVehiculo="'+dato['idVehiculo']+'"' +
                            'descipcion="El Usuario a Modificado un Vehiculo" titulo="Modificar Vehiculo">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td   class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idVehiculo="'+dato['idVehiculo']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Vehiculo" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Vehiculo!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');


                }, 'json');
				
                swal("Vehículo Registrado!", "Vehículo ha sido agregado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }else {
 
				

                $.post("{$_Parametros.url}modPA/vehiculoCONTROL/ModificarVehiculoMET", datos, function (dato) {


                    $(document.getElementById('idVehiculo'+dato['idVehiculo'])).html('<td>'+dato['idVehiculo']+'</td>' +
                            '<td> '+dato['modelo']+'</td>' +
                            '<td> '+dato['placa']+'</td>' +
                            '<td> '+dato['anio']+'</td>' +
                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idVehiculo="'+dato['idVehiculo']+'"' +
                            'descipcion="El Usuario a Modificado un Vehiculo" titulo="Modificar Vehiculo">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td  class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idVehiculo="'+dato['idVehiculo']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Vehiculo" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Vehiculo!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');



                    
                }, 'json');
              
                       
				
				
                swal("Vehículo Modificado!", "Vehículo  ha sido modificado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
						 
            }



        }
    });


    $(document).ready(function() {



        $("#Placa").change(function () {

            $.post("{$_Parametros.url}modPA/vehiculoCONTROL/VerificarPlacaMET", { Placa: $('#Placa').val() }, function(data){
                
                if(data==1){

                    alert(" placa ya se encuentra registrada");
                    $("#Placa").val(" ");
                }


                var json = data,
                        obj = JSON.parse(json);
                $("#idAplicable").html(obj);


            });

        });


        $("#formAjax").validate();
        $('#fechas').datepicker({ format: 'dd/mm/yyyy', language:'es', startDate:'0d' });
		$('.MontoPagado').inputmask(' 999.999.999,99 BS', { numericInput: true });

        $("#Color").val($("#ColorBD").val());

            $("#ClaseVehiculo").change(function () {

                $("#ClaseVehiculo option:selected").each(function () {

                    $.post("{$_Parametros.url}modPA/vehiculoCONTROL/ActualizarClaseMET", { ClaseVehiculo: $('#ClaseVehiculo').val() }, function(data){
                        var json = data,
                        obj = JSON.parse(json);
                        $("#TipoVehiculo").html(obj);
                    });
                });
            })

             });



    function archivo(evt) {
        var Placa= $("#Placa").val();
        var files = evt.target.files; // FileList object

        // Obtenemos la imagen del campo "file".
        for (var i = 0, f; f = files[i]; i++) {
            //Solo admitimos imágenes.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            reader.onload = (function(theFile) {
                return function(e) {
                    // Insertamos la imagen
                    document.getElementById("list").innerHTML = ['<img class="height-4 width-4 img-rounded" onclick="document.getElementById(\'files\').click()" width="335" height="190"  src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                    var data = new FormData();
                    jQuery.each($('#files')[0].files, function(i, file) {
                        data.append('files-'+i, file);
 

                    });

                    data.append('Carpeta', Placa);
                    data.append('Vista', 'Perfil');

                    $.ajax({
                        url: '{$_Parametros.url}modPA/vehiculoCONTROL/SubirFotoMET',
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST',
                        success: function(data){

                        }
                    });

                };
            })(f);

            reader.readAsDataURL(f);
        }
    }

//    document.getElementById('files').addEventListener('change', archivo, false);

  function archivo2(evt) {

        var Placa= $("#Placa").val();
        var files = evt.target.files; // FileList object

        // Obtenemos la imagen del campo "file".
        for (var i = 0, f; f = files[i]; i++) {
            //Solo admitimos imágenes.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            reader.onload = (function(theFile) {
                return function(e) {
                    // Insertamos la imagen
                    document.getElementById("output_list2").innerHTML = ['<img class="height-4 width-4 img-rounded " onclick="document.getElementById(\'files2\').click()" width="295px" height="250px"  src="', e.target.result,'" title="', escape(theFile.tmp_name), '"/>'].join('');

                    var data = new FormData();
                    jQuery.each($('#files2')[0].files, function(i, file) {
                        data.append('files-'+i, file);
						$("#VLD").val(theFile.name);
						 
                    });

                    data.append('Carpeta', Placa);
                    data.append('Vista', 'VLD');

                    $.ajax({
                        url: '{$_Parametros.url}modPA/vehiculoCONTROL/SubirFotoMET',
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST',
                        success: function(data){

                        }
                    });
                };

            })(f);
            reader.readAsDataURL(f);
        }
    }
    document.getElementById('files2').addEventListener('change', archivo2 ,false);
    function archivo3(evt) {
        var Placa= $("#Placa").val();
        var files = evt.target.files; // FileList object
        // Obtenemos la imagen del campo "file".
        for (var i = 0, f; f = files[i]; i++) {
            //Solo admitimos imágenes.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            reader.onload = (function(theFile) {
                return function(e) {
                    // Insertamos la imagen
                    document.getElementById("output_list3").innerHTML = ['<img class="height-4 width-4 img-rounded " onclick="document.getElementById(\'files3\').click()" width="295px" height="250px"  src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                    var data = new FormData();
                    jQuery.each($('#files3')[0].files, function(i, file) {
                        data.append('files-'+i, file);
						$("#VLI").val(theFile.name);
                    });

                    data.append('Carpeta', Placa);
                    data.append('Vista', 'VLI');

                    $.ajax({
                        url: '{$_Parametros.url}modPA/vehiculoCONTROL/SubirFotoMET',
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST',
                        success: function(data){

                        }
                    });
                };
            })(f);

            reader.readAsDataURL(f);
        }
    }
    document.getElementById('files3').addEventListener('change', archivo3, false);
    function archivo4(evt) {
        var Placa= $("#Placa").val();
        var files = evt.target.files; // FileList object
        // Obtenemos la imagen del campo "file".
        for (var i = 0, f; f = files[i]; i++) {
            //Solo admitimos imágenes.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            reader.onload = (function(theFile) {
                return function(e) {
                    // Insertamos la imagen
                    document.getElementById("output_list4").innerHTML = ['<img class="height-4 width-4 img-rounded " onclick="document.getElementById(\'files4\').click()" width="200px" height="250px" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                    var data = new FormData();
                    jQuery.each($('#files4')[0].files, function(i, file) {
                        data.append('files-'+i, file);
                        $("#VF").val(theFile.name);
                    });

                    data.append('Carpeta', Placa);
                    data.append('Vista', 'VF');

                    $.ajax({
                        url: '{$_Parametros.url}modPA/vehiculoCONTROL/SubirFotoMET',
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST',
                        success: function(data){

                        }
                    });
                };
            })(f);

            reader.readAsDataURL(f);
        }
    }
    document.getElementById('files4').addEventListener('change', archivo4, false);
    function archivo5(evt) {
        var Placa= $("#Placa").val();
        var files = evt.target.files; // FileList object
        // Obtenemos la imagen del campo "file".
        for (var i = 0, f; f = files[i]; i++) {
            //Solo admitimos imágenes.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            reader.onload = (function(theFile) {
                return function(e) {
                    // Insertamos la imagen
                    document.getElementById("output_list5").innerHTML = ['<img class="height-4 width-4 img-rounded " onclick="document.getElementById(\'files5\').click()" width="200px" height="190px" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                    var data = new FormData();
                    jQuery.each($('#files5')[0].files, function(i, file) {
                        data.append('files-'+i, file);
                        $("#VT").val(theFile.name);
                    });

                    data.append('Carpeta', Placa);
                    data.append('Vista', 'VT');

                    $.ajax({
                        url: '{$_Parametros.url}modPA/vehiculoCONTROL/SubirFotoMET',
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST',
                        success: function(data){

                        }
                    });

                };
            })(f);

            reader.readAsDataURL(f);
        }
    }
    document.getElementById('files5').addEventListener('change', archivo5, false);
    function archivo6(evt) {
        var Placa= $("#Placa").val();
        var files = evt.target.files; // FileList object
        // Obtenemos la imagen del campo "file".
        for (var i = 0, f; f = files[i]; i++) {
            //Solo admitimos imágenes.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            reader.onload = (function(theFile) {
                return function(e) {
                    // Insertamos la imagen
                    document.getElementById("output_list6").innerHTML = ['<img class="height-4 width-4 img-rounded "onclick="document.getElementById(\'files6\').click()" width="180px" height="250px" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');

                    var data = new FormData();
                    jQuery.each($('#files6')[0].files, function(i, file) {
                        data.append('files-'+i, file);
                         $("#VS").val(theFile.name);
                    });

                    data.append('Carpeta', Placa);
                    data.append('Vista', 'VS');

                    $.ajax({
                        url: '{$_Parametros.url}modPA/vehiculoCONTROL/SubirFotoMET',
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST',
                        success: function(data){

                        }
                    });

                };
            })(f);

            reader.readAsDataURL(f);
        }
    }
    document.getElementById('files6').addEventListener('change', archivo6, false);
    function archivo7(evt) {
        var Placa= $("#Placa").val();
        var files = evt.target.files; // FileList object
        // Obtenemos la imagen del campo "file".
        for (var i = 0, f; f = files[i]; i++) {
            //Solo admitimos imágenes.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            reader.onload = (function(theFile) {
                return function(e) {
                    // Insertamos la imagen
                    document.getElementById("output_list7").innerHTML = ['<img class="height-4 width-4 img-rounded " onclick="document.getElementById(\'files7\').click()" width="180px" height="250px" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                    var data = new FormData();
                    jQuery.each($('#files7')[0].files, function(i, file) {
                        data.append('files-'+i, file);
                         $("#VI").val(theFile.name);
                    });

                    data.append('Carpeta', Placa);
                    data.append('Vista', 'VI');

                    $.ajax({
                        url: '{$_Parametros.url}modPA/vehiculoCONTROL/SubirFotoMET',
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST',
                        success: function(data){

                        }
                    });

                };
            })(f);
            reader.readAsDataURL(f);
        }
    }
    document.getElementById('files7').addEventListener('change', archivo7, false);







$('.accionModal').click(function () {

    accionModal(this,'url')
});

function accionModal(id,attr){

    $('#formModalLabel2').html($(id).attr('titulo'));
    $.post($(id).attr(attr), {
        cargar: 0,

        tr: $("#"+$(id).attr('idTabla')+" > tbody > tr").length + 1
    }, function ($dato) {
        $('#ContenidoModal2').html($dato);
    });
}














</script>