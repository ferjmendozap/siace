<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div id="body1">
<div class="card">
    <div class="card-head">
        <header> <h2 class="text-primary">LISTAR PÓLIZAS DE VEHÍCULOS </h2></header>
    </div>
    <div class="col-md-12">
        <h5>Listado de las pólizas de vehículos.</h5>
    </div><!--end .col -->

</div><!--end .card -->


<div class="form-group   col-lg-12 ">

    <div class="col-lg-11 "></div>	<a class="btn btn-flat btn-accent ink-reaction span8" href="javascript:void(0);">filtrar</a>
</div>

</div><!--end .card -->

<section class="style-default-bright">




    <!-- Listado -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-actionbar">
                <div class="card-actionbar-row">


                </div>
            </div>
        </div><!--end .col -->
    </div><!--end .row -->
    <div class="row">

        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="sort-alpha col-sm-2">N° </th>
                        <th class="sort-alpha col-sm-6">Póliza de Vehículo</th>
                        <th class="sort-alpha col-sm-1"> Editar</th>
                        <th class="sort-alpha col-sm-1"> Eliminar</th>


                    </tr>
                    </thead>
                    <tbody>

                    {foreach item=post from=$_PolizaPost}
                        <tr id="idPost{$post.pk_num_poliza}" class="gradeA">
                            <td>{$post.pk_num_poliza}</td>
                            <td>{$post.ind_poliza}</td>
                            <td>
                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Modificar Post" descipcion="El Usuario a Modificado un post" idModificar="{$post.pk_num_poliza}" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                </button>   </td>
                            <td>
                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idEliminar="{$post.pk_num_poliza}" mensaje="Estas seguro que desea eliminar la el post!!" titulo="Estas Seguro?" descipcion="El usuario a eliminado un post" boton="si, Eliminar" idmenu="7">
                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                </button>
                            </td>
                        </tr>








                    {/foreach}
                    </tbody>
                </table>
            </div><!--end .table-responsive -->
        </div><!--end .col -->
    </div><!--end .row -->
    <!-- fin listado -->




    </div><!--end .row -->


    <script type="text/javascript">
        $(document).ready(function() {
            $('#modalAncho').css( "width", "85%" );


            $('#datatable1 tbody').on( 'click', '.modificar', function () {
                var $url='{$_Parametros.url}modPA/polizaCONTROL/ModificarPolizaMET';

                $.post($url,{ idPost: $(this).attr('idModificar')},function($dato){
                    $('#ContenidoModal').html($dato);
                });

            });

            $('#datatable1 tbody').on( 'click', '.eliminar', function () {


                var $url='{$_Parametros.url}modPA/polizaCONTROL/EliminarPolizaMET';
                var idPost=$(this).attr('idEliminar');
                var idMenu=$(this).attr('idMenu');
                swal({
                    title:$(this).attr('titulo'),
                    text: $(this).attr('mensaje'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false
                }, function(){

                    var $url='{$_Parametros.url}modPA/polizaCONTROL/EliminarPolizaMET';


                    $.post($url,{ idPost: idPost},function($dato){
                        swal("Eliminado!", "el menu fue eliminado satisfactoriamente.", "success");
                    });

                    ;
                });
            });





        });
    </script>
    </div>