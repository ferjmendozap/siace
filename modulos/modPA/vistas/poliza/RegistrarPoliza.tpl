



<!-- BEGIN VALIDATION FORM WIZARD -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body ">
                <div id="rootwizard2" class="form-wizard form-wizard-horizontal">
                    <form  id="formulario_registrar_poliza"class="form   form-validation" role="form" novalidate="novalidate">
                        <input type="hidden" value="1" name="valido"  id="valido" />
                        <div class="form-wizard-nav">
                            <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
                            <ul class="nav nav-justified">
                                <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">GENERAL</span></a></li>

                                <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">GUARDAR</span></a></li>

                            </ul>
                        </div><!--end .form-wizard-nav -->
                        <div class="tab-content clearfix">
                            <div class="tab-pane active" id="step1">
                                {if isset($_PolizaPost)}
                                {foreach item=post from=$_PolizaPost}
                                    <input type="hidden" value="{$post.pk_num_poliza}"  name="idPost"    id="idPost" />
                                    <input type="hidden" value="{$post.fk_lgb000_proveedor}" name="AseguradoraBD"  id="AseguradoraBD" />
                                    <input type="hidden" value="{$post.fk_pab001_num_vehiculo}" name="VehiculoBD"  id="VehiculoBD" />
                                <div class="col-lg-6">
                                    <form class="form form-validate " novalidate="novalidate">

                                        <div class="form-group">
                                            <select  class="form-control" name="Aseguradora" id="Aseguradora" >
                                                <option value="">&nbsp;</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                                <option value="60">60</option>
                                                <option value="70">70</option>
                                            </select>
                                            <label for="rangelength2">Aseguradora</label>
                                        </div>

                                        <div class="form-group">
                                            <input type="text"  data-rule-digits="true" value="{$post.ind_poliza}" class="form-control" id="NumeroPoliza" name="NumeroPoliza" data-rule-rangelength="[2, 65]" required>
                                            <label for="rangelength2">Número de Póliza</label>

                                        </div>

                                        <div class="form-group">
                                            <input type="text" data-rule-digits="true" value="{$post.ind_recibo}" class="form-control" id="NumeroSeguro" name="NumeroSeguro" data-rule-rangelength="[2, 65]" required>
                                            <label for="rangelength2">Número de Seguro</label>

                                        </div>


                                        <div class="form-group">

                                            <div id="demo-date-range" class="input-daterange input-group">
                                                <div class="input-group-content">
                                                    <input class="form-control" type="text" name="desde" value="{$post.fec_desde}">
                                                    <label>Vigencia</label>
                                                </div>
                                                <span class="input-group-addon">Hasta</span>
                                                <div class="input-group-content">
                                                    <input class="form-control" type="text" name="hasta" value="{$post.fec_hasta}">
                                                    <div class="form-control-line"></div>
                                                </div>


                                            </div>
                                            <div class="form-group">
                                                <input type="text"    value="{$post.num_monto_pagado}" name="MontoPagado" id="MontoPagado"  class="form-control MontoPagado" aria-required="true">
                                                <label for="digits2">Monto Pagado</label>
                                                <p class="help-block">Solo Números</p>
                                            </div>
                                            <div class="form-group">
                                                <input type="text"   value="{$post.num_monto_cobertura}"  name="MontoCobertura" id="MontoCobertura" class="form-control MontoPagado" aria-required="true">
                                                <label for="digits2">Monto Cobertura</label>
                                                <p class="help-block">Solo Números</p>
                                            </div>  </div>

                                </div>

                                {/foreach}
                                {/if}
                                <div class="col-lg-6">
                                <div class="form-group">
                                    <select required="" class="form-control" name="Vehiculo" id="Vehiculo" aria-required="true">
                                        <option value="">&nbsp;</option>

                                        {foreach item=post from=$_VehiculoPost}
                                            <option value="{$post.pk_num_vehiculo}">  {$post.ind_modelo}- {$post.ind_placa}</option>

                                        {/foreach}

                                    </select>
                                    <label for="rangelength2"> Vehículo</label>
                                </div>
                                </div>
                            </div><!--end #step1 -->

                            <div class="tab-pane"   id="step2">
                            <button id="boton" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" type="button">Guardar</button>
                            </div><!--end #step2 -->

                        </div><!--end .tab-content -->
                        <ul class="pager wizard">
                            <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                            <li class="previous"><a class="btn-raised"       href="javascript:void(0);">Previo</a></li>
                            <li class="next last"><a class="btn-raised"      href="javascript:void(0);">Último</a></li>
                            <li class="next"><a class="btn-raised"           href="javascript:void(0);">Siguiente</a></li>
                        </ul>
                    </form>
                </div><!--end #rootwizard -->
            </div><!--end .card-body -->
        </div><!--end .card -->
        <em class="text-caption">Form wizard with validation</em>
    </div><!--end .col -->
</div><!--end .row -->
<!-- END VALIDATION FORM WIZARD -->


<script type="text/javascript">
    $(document).ready(function() {

       // $('#demo-date-range').datepicker({ format: 'dd/mm/yyyy', language:'es', startDate:'0d' });
        $('#demo-date-range').datepicker({
            format: 'dd/mm/yyyy', language:'es',

        })
		$('.MontoPagado').inputmask(' 999.999.999,99 BS', { numericInput: true });
		
        $("#formulario_registrar_poliza").submit(function(){


            retruefalse;
        });

        // Asignamos el valor que trae el fk en el select
        $("#Aseguradora").val($("#AseguradoraBD").val());
        $("#Vehiculo").val($("#VehiculoBD").val());

        //var dt_to = $.datepicker.formatDate('yy-mm-dd', new Date());

        $('#boton').click(function(){

            var accion = $("#accion").val();
            if(accion=="r") {
                $.post("{$_Parametros.url}modPA/polizaCONTROL/RegistrarPolizaMET", $("#formulario_registrar_poliza").serialize(), function (dato) {


                }, 'json');
                swal("Registro Agregado!", "La marca del vehículo ha sido agregado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
              //
            }else {
                $.post("{$_Parametros.url}modPA/polizaCONTROL/ModificarPolizaMET", $("#formulario_registrar_poliza").serialize(), function (dato) {


                }, 'json');
                swal("Registro Modifcado!", "La marca del vehículo ha sido modificado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
             //
            }









        });

    });
</script>