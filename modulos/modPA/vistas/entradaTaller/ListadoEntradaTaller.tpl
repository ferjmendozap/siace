<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div id="body1">
    <div class="card">
        <div class="card-head">
            <header> <h2 class="text-primary"> LISTAR ENTRADAS DE VEHÍCULOS AL TALLER </h2></header>
        </div>
        <div class="col-md-12">
            <h5>Listado de las entradas de vehículos al taller.</h5>
        </div><!--end .col -->

    </div><!--end .card -->


    <div class="form-group   col-lg-12 ">

        <div class="col-lg-11 "></div>	<a class="btn btn-flat btn-accent ink-reaction span8" href="javascript:void(0);">filtrar</a>
    </div>

</div><!--end .card -->

<section class="style-default-bright">





    <div class="row">

        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="sort-alpha col-sm-1">Fecha Entrada </th>
                        <th class="sort-alpha col-sm-2">Vehículo</th>
                        <th class="sort-alpha col-sm-2">Modelo-Placa</th>
                        <th class="sort-alpha col-sm-2">Taller</th>

                        <th class="sort-alpha col-sm-3">Motivo</th>

                        {if in_array('PA-01-04-01-02-M',$_Parametros.perfil)}   <th class="  col-sm-1"> Modificar Entrada</th>{/if}
                        {if in_array('PA-01-04-01-03-A',$_Parametros.perfil)}   <th class="  col-sm-1"> Anular Entrada</th>{/if}

                    </tr>
                    </thead>
                    <tbody>

                    {foreach item=post from=$_EntradaTallerPost}
                        <tr id="idEntrada{$post.pk_num_entrada_taller}" class="gradeA">
                            <td>{$post.fec_entrada }</td>
                            <td>
                                {if !is_null($post.ind_url_foto_frontal)&&trim($post.ind_url_foto_frontal)!=""&&trim($post.ind_url_foto_frontal)!="faceman_Suburban_Assault_Vehicle_(Front).svg"}

                                    <img src="{$_Parametros['ruta_Img']}modPA/{$post.ind_placa}/{$post.ind_url_foto_frontal}" class="height-1 width-1 img-circle "  />
                                {else}
                                    <img src="{$_Parametros['ruta_Img']}modPA/faceman_Suburban_Assault_Vehicle_(Front).svg" class="height-1 width-1 img-circle "  />

                                {/if}
                            </td>
                            <td> {$post.ind_modelo} - {$post.ind_placa}</td>
                            <td>{if isset($post['ind_nombre1'])}{$post['ind_nombre1']}
                                {else}
                                    {if isset($post['ind_nombre2'])}{$post['ind_nombre2']}  {$post['ind_nombre3']}{/if}
                                {/if}</td>

                            <td> {$post.ind_motivo}  </td>




                            {if in_array('PA-01-04-01-02-M',$_Parametros.perfil)} <td>
                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Modificar Entrada Taller" descipcion="--" idEntrada="{$post.pk_num_entrada_taller}" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                    </button>
                            </td>
                            {/if}
                            {if in_array('PA-01-04-01-03-A',$_Parametros.perfil)}
                                <td>
                                    <button class="anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idEntrada="{$post.pk_num_entrada_taller}" mensaje="Estas seguro que desea anular la entrada del vehículo!!" titulo="Estas Seguro?" descipcion="El usuario a eliminado un post" boton="si, Eliminar" >
                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                    </button>
                                </td>
                            {/if}





                        </tr>








                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="7">
                            {if in_array('PA-01-04-01-01-N',$_Parametros.perfil)}
                                <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                        descripcion="Registro de entrada de un vehículo al taller"  titulo="Registrar Entrada Taller" id="nuevo" >
                                     <i class="md md-create"></i> Registrar Entrada Taller &nbsp;&nbsp;&nbsp;
                                </button>
                            {/if}


                        </th>

                    </tr>
                    </tfoot>
                </table>
            </div><!--end .table-responsive -->
        </div><!--end .col -->
    </div><!--end .row -->
    <!-- fin listado -->




    </div><!--end .row -->




    <script type="text/javascript">
        $(document).ready(function() {
            var $url='{$_Parametros.url}modPA/entradaTallerCONTROL/RegistrarEntradaTallerMET';
            $('#nuevo').click(function(){
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idMenu:0 },function($dato){
                    $('#ContenidoModal').html($dato);
                });

            });
            $('#modalAncho').css( "width", "85%" );




            $('#datatable1 tbody').on( 'click', '.modificar', function () {
                var $url='{$_Parametros.url}modPA/entradaTallerCONTROL/ModificarEntradaTallerMET';

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idEntrada')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });

            $('#datatable1 tbody').on( 'click', '.anular', function () {


                var $url='{$_Parametros.url}modPA/entradaTallerCONTROL/AnularEntradaTallerMET';
                var idPost=$(this).attr('idEntrada');
                var idMenu=$(this).attr('idMenu');
                swal({
                    title:$(this).attr('titulo'),
                    text: $(this).attr('mensaje'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false
                }, function(){

                    var $url='{$_Parametros.url}modPA/entradaTallerCONTROL/AnularEntradaTallerMET';

                    $(document.getElementById('idEntrada'+idPost)).html('');
                    $.post($url,{ idPost: idPost},function($dato){
                        swal("Eliminado!", "el registro fue eliminado satisfactoriamente.", "success");
                    });


                });
            });


        });
    </script>
    </div>




