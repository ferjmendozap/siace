<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Personas</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Persona</th>
                            <th>Descripcion</th>
                            <th>Nro. Documento</th>
                            <th>Dependecia</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=persona from=$lista}
                            <tr id="idPersona{$persona.pk_num_persona}">
                                <input type="hidden" value="{$persona.pk_num_persona}" class="persona"
                                       proveedor="{$persona.pk_num_persona}"
                                       nombre="{$persona.ind_nombre1} {$persona.ind_nombre2} {$persona.ind_apellido1} {$persona.ind_apellido2}"
                                       documentoProv="{$persona.ind_documento_fiscal}"
                                        >
                                
                                <td><label>{$persona.pk_num_persona}</label></td>
                                <td><label>{$persona.ind_nombre1} {$persona.ind_nombre2} {$persona.ind_apellido1} {$persona.ind_apellido2}</label></td>
                                <td><label>{$persona.ind_cedula_documento}</label></td>
                                <td><label>{$persona.ind_dependencia}</label></td>

                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

</div>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatable2').on('click', 'tbody tr', function () {

            var input = $(this).find('input');

            $(document.getElementById('nombreProveedor')).val(input.attr('nombre'));
            $(document.getElementById('codProveedor')).val(input.attr('proveedor'));

           $('#cerrarModal2').click();
        });


    });
</script>
