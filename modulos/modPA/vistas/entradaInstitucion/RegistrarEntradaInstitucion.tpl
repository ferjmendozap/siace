<link type="text/css"   rel="stylesheet" href="{$_Parametros.ruta_Complementos}theme-default/libs/wizard/wizard.css?1425466601" />





<!-- BEGIN VALIDATION FORM WIZARD -->


<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
    <form action="" id="formAjax"class="form   form-validation" role="form"  >



        <input type="hidden" value="1" name="valido"  id="valido" />
        
        <div class="form-wizard-nav">
            <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
            <ul class="nav nav-justified">

                                <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">GENERAL</span></a></li>

                                <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title"> INFORMACIÓN DE ENTRADA DE VEHÍCULO</span></a></li>



                            </ul>
                        </div><!--end .form-wizard-nav -->
                        <div class="tab-content clearfix">
                            <div class="tab-pane active" id="step1">





                            <input type="hidden" value="{if isset($_EntradaIntitucionPost.pk_num_entrada)}{$_EntradaIntitucionPost.pk_num_entrada}{/if}"  name="form[int][idEntrada]"      id="idEntrada" />



 
                                <input type="hidden" value="{if isset($_Acciones.accion)}{$_Acciones.accion}{/if}"    id="accion" />
                 

                            <div class="col-lg-6">


                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <select  {if isset($_EntradaIntitucionPost.pk_num_entrada)} disabled {/if}required="" class="form-control" name="form[int][Salida]" id="Salida" aria-required="true">
                                            <option value="">&nbsp;</option>



                                            {foreach item=tipo from=$_SalidaIntitucionPost}

                                                {if isset($_EntradaIntitucionPost.pk_num_entrada)}

                                                    {if $tipo.pk_num_salida_vehiculo==$_EntradaIntitucionPost.pk_num_entrada}
                                                        <option selected value="{$tipo.pk_num_salida_vehiculo}">{$tipo.ind_modelo} - {$tipo.ind_placa} </option>
                                                    {else}
                                                        <option value="{$tipo.pk_num_salida_vehiculo}">{$tipo.ind_modelo} - {$tipo.ind_placa}</option>
                                                    {/if}
                                                {else}
                                                    <option value="{$tipo.pk_num_salida_vehiculo}">{$tipo.ind_modelo} - {$tipo.ind_placa}</option>
                                                {/if}
                                            {/foreach}




                                        </select>
                                        <label for="rangelength2"> Salida de la Institución </label>
                                    </div>
                                </div>


                            </div>






                        </div><!--end #step1 -->

                        <div class="tab-pane" id="step2">



                            <div class="col-lg-6">
                                <div class="col-lg-12">


                                    <div class="form-group   col-lg-6  ">
                                        <input type="text" required="" readonly name="entrada" class="form-control fechas2" value="{if isset($_EntradaIntitucionPost.fec_entrada)}{$_EntradaIntitucionPost.fec_entrada}{/if}">
                                        <label>Fecha de Entrada</label>
                                    </div>

                                    <div class="form-group col-lg-6 ">
                                        <input type="text" required="" name="form[txt][hora_hasta]" id="hora_hasta" class="form-control time12-mask" value="{if isset($_EntradaIntitucionPost.hora_hasta)}{$_EntradaIntitucionPost.hora_hasta}{/if}">
                                        <label>Hora de Llegada</label>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">


                                    <input type="text" required="" value="{if isset($_EntradaIntitucionPost.ind_kilometraje_entrada)}{$_EntradaIntitucionPost.ind_kilometraje_entrada}{/if}"name="form[int][kilometraje]" data-rule-rangelength="[0, 9]" id="kilometraje" class="form-control"  >



                                    <label for="rangelength2">Kilometraje</label>
                                    </div>
                                </div>


                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <textarea required="" placeholder="" rows="3" class="form-control" id="ObservacionEntrada" name="form[alphaNum][ObservacionEntrada]">{if isset($_EntradaIntitucionPost.ind_observacion_entrada )}{$_EntradaIntitucionPost.ind_observacion_entrada }{/if}</textarea>
                                    <label for="textarea1">Observación de la Entrada del Vehículo</label>
                                </div>

                            </div>

                            <div class="row">
                                &nbsp;
                            </div>
                            <div class="row">
                                &nbsp;
                            </div>

                            <div class="row">
                                &nbsp;
                            </div>

                            <div class="row">
                                &nbsp;
                            </div>

                            <div class="row">
                                &nbsp;
                            </div>
                            <div class="row">
                                &nbsp;
                            </div>
                            <span class="clearfix"></span>

                            <div class="modal-footer">

                                <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>

                                <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>


                            </div>
                        </div><!--end #step2 -->
                        </div>

        <ul class="pager wizard">

            <li class="previous"><a class="btn-raised"       href="javascript:void(0);">Previo</a></li>

            <li class="next"><a class="btn-raised"           href="javascript:void(0);">Siguiente</a></li>
        </ul>




    </form>
</div>


<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {


            var datos = $("#formAjax" ).serialize();

            var accion = $("#accion").val();
		 
            if(accion=="registrar") {
                $.post("{$_Parametros.url}modPA/entradaInstitucionCONTROL/RegistrarEntradaInstitucionMET", datos, function (dato) {
                    $(document.getElementById('datatable1')).append('<td>'+dato['idEntrada']+'</td>' +
                            '<td>   </td>' +
                            '<td>'+dato['modelo']+' </td>' +
                            '<td>'+dato['entrada']+' </td>' +
                            '<td>'+dato['observacion']+' </td>' +
                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idEntrada="'+dato['idEntrada']+'"' +
                            'descipcion="El Usuario a Modificado Entrada" titulo="Modificar Entrada">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td   class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idEntrada="'+dato['idEntrada']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado Entrada" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Entrada!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>')

                }, 'json');
                swal("Entrada Registrada!", "La entrada del vehículo ha sido agregado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }else {
                $.post("{$_Parametros.url}modPA/entradaInstitucionCONTROL/ModificarEntradaInstitucionMET", datos, function (dato) {

                    $(document.getElementById('idEntrada'+dato['idEntrada'])).html('<td>'+dato['idEntrada']+'</td>' +
                            '<td>   </td>' +
                            '<td>'+dato['modelo']+' </td>' +
                            '<td>'+dato['entrada']+' </td>' +
                            '<td>'+dato['observacion']+' </td>' +
                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idEntrada="'+dato['idEntrada']+'"' +
                            'descipcion="El Usuario a Modificado  Entrada" titulo="Modificar Entrada">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td  class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idEntrada="'+dato['idEntrada']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado  Entrada" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar  Entrada!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');

                }, 'json');
                swal("Entrada Modificada!", "La entrada del vehículo ha sido modificado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }



        }
    });


    $(document).ready(function() {

        $("#formAjax").validate();
        $('#fechas').datepicker({ format: 'dd/mm/yyyy', language:'es', startDate:'0d' });
        $('.fechas2').datepicker({ format: 'dd/mm/yyyy', language:'es'});
        $('#optgroup').multiSelect({ selectableOptgroup: true });
        $("#kilometraje").inputmask( "999999",{ numericInput: true});


    });
</script>