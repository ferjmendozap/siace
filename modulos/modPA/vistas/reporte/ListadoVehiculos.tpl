<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS         |               CORREO                |          TELEFONO              |
* | 1 |      Erick Carvajal R                |dt.ait.soporte.tecnico1@cgesucre.gob.ve|       04248335896            |
* |   |                                      |                                     |                                |
* |___|______________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        08-12-2017       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->

<div class="card">
    <div class="card-head">
        <header><h2 class="text-primary">Listado Vehículos</h2></header>
    </div>
    <div class="col-md-12">
        <h5>Reporte en pdf del listado de vehículos de la institución.</h5>
    </div>
</div>

<div class="card">
    <form action="{$_Parametros.url}modPA/reporteCONTROL/vehiculosReportePdfMET" id="formulario" method="post">

        <div class="contain-lg col-lg-1" align="right">
            <span>Placa:</span>
        </div>
        <div class="form-group col-lg-2 contain-lg">
            <div class="form-group col-lg-3 contain-lg">
                <label class="checkbox-styled">
                    <input type="checkbox" id="checkPlaca" class="form-control">
                    <span></span>
                </label>
            </div>
            <div class="form-group col-lg-9 contain-lg">
                <div class="form-group" id="buscarError">
                    <input type="text" disabled name="placa" id="placa" class="form-control">
                </div>
            </div>
        </div>

        {*<div class="contain-lg col-lg-1" align="right">*}
        {*<span>Marca:</span>*}
        {*</div>*}
        {*<div class="col-lg-2 contain-lg">*}
        {*<div class="col-lg-3 contain-lg">*}
        {*<label class="checkbox-styled">*}
        {*<input type="checkbox" id="checkMarca" class="form-control">*}
        {*<span></span>*}
        {*</label>*}
        {*</div>*}
        {*<div class="col-lg-9 contain-lg">*}
        {*<div class="form-group" id="buscarError">*}
        {*<input type="text" disabled name="marca" id="marca" class="form-control">*}
        {*</div>*}
        {*</div>*}
        {*</div>*}

        <div class="form-group contain-lg col-lg-1" align="right">
            <span>Modelo:</span>
        </div>
        <div class="form-group col-lg-2 contain-lg">
            <div class="form-group col-lg-3 contain-lg">
                <label class="checkbox-styled">
                    <input type="checkbox" id="checkModelo" class="form-control">
                    <span></span>
                </label>
            </div>
            <div class="form-group col-lg-9 contain-lg">
                <div class="form-group" id="buscarError">
                    <input type="text" disabled name="modelo" id="modelo" class="form-control">
                </div>
            </div>
        </div>

        <div class="form-group col-lg-12" align="center">
            <button id="accion" class="btn btn-info ink-reaction btn-raised">
                Buscar
            </button>
        </div>
    </form>

    <div id="respuestaPdf">

    </div>

</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#formulario').submit(function () {
            return false;
        });

        //Placa
        $('#checkPlaca').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#placa').attr('disabled',false);
            } else {
                $('#placa').attr('disabled','disabled');
                $('#placa').val('');
            }
        });

//        //Marca
//        $('#checkMarca').click(function () {
//            if($(this).attr('checked')=="checked"){
//                $('#marca').attr('disabled',false);
//            } else {
//                $('#marca').attr('disabled','disabled');
//                $('#marca').val('');
//            }
//        });

        //Modelo
        $('#checkModelo').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#modelo').attr('disabled',false);
            } else {
                $('#modelo').attr('disabled','disabled');
                $('#modelo').val('');
            }
        });

        //Submit
        $('#accion').click(function () {
            if($('#checkPlaca').attr('checked')=="checked" && $('#placa').val().length > 0){
                var placa = $('#placa').val();
            } else {
                placa = 'no';
            }

            if($('#checkModelo').attr('checked')=="checked" && $('#modelo').val().length > 0){
                var modelo = $('#modelo').val();
            } else {
                modelo = 'no';
            }

            var url = $('#formulario').attr('action');
            $('#respuestaPdf').html('<iframe frameborder="0" src="' + url +'/'+placa+'/'+modelo+'" width="100%" height="540px"></iframe>');

        });
    });
</script>