<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div class="card">
    <div class="card-head">
        <header> <h2 class="text-primary"> VEHÍCULOS DISPONIBLES </h2></header>
    </div>
    <div class="col-md-12">
        <h5>Reporte en pdf de los vehículos disponibles en la institución.</h5>
    </div><!--end .col -->

</div><!--end .card -->

<div class="card   ">
    <div class="card-head">
        <header>Búsqueda</header>
    </div>

        <form   class="form" action="{$_Parametros.url}modPA/reporteCONTROL/disponibleReportePdfMET"   method="post" target="_blank">
        <div class="col-lg-12">



 








</div>


    <div class="form-group   col-lg-12   " align="center">

        <button type="submit" class="btn btn-info ink-reaction btn-raised"  >Buscar</button>

        </form>
</div>

</div><!--end .card -->

<section class="style-default-bright">




    <script type="text/javascript">

        $(document).ready(function() {
            $('#modalAncho').css("width", "85%");
            $('#fechas').datepicker({ format: 'dd/mm/yyyy', language:'es', startDate:'0d' });

        });
    </script>


