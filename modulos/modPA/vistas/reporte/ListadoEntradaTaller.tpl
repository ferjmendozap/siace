<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div class="card">
    <div class="card-head">
        <header> <h2 class="text-primary">ENTRADA TALLER </h2></header>
    </div>
    <div class="col-md-12">
        <h5>Reporte en pdf de la entrada de vehículo al taller.</h5>
    </div><!--end .col -->

</div><!--end .card -->

<div class="card   ">
    <div class="card-head">
        <header>Búsqueda</header>
    </div>


        <form   class="form" action="{$_Parametros.url}modPA/reporteCONTROL/entradaTallerReportePdfMET"   method="post" target="_blank">
        <div class="col-lg-12">
            <div class="form-group   col-lg-2">
                <select id="vehiculo" class="form-control" name="form[int][vehiculo]">
                    <option value=""> </option>
                    {foreach item=tipo from=$_VehiculoPost}

                            <option value="{$tipo.pk_num_vehiculo}"> {$tipo.ind_modelo} - {$tipo.ind_placa}</option>

                    {/foreach}
                </select>
                <label for="select1">Vehículo</label>
            </div>



                <div class="form-group col-lg-4">

                    <div id="fechas" class="input-daterange input-group">
                        <div class="input-group-content">
                            <input class="form-control" type="text" id="desde" name="desde" >
                            <label>Fecha a Solicitar</label>
                        </div>

                        <span class="input-group-addon">Hasta</span>
                        <div class="input-group-content">
                            <input class="form-control" type="text"  id="hasta"  name="hasta">
                            <div class="form-control-line"></div>
                        </div>
                    </div>
                </div>








</div>


    <div class="form-group   col-lg-12   " align="center">

        <button type="submit" class="btn btn-info ink-reaction btn-raised"  >Buscar</button>

        </form>
</div>

</div><!--end .card -->

<section class="style-default-bright">




    <script type="text/javascript">

        $(document).ready(function() {
            $('#modalAncho').css("width", "85%");
            $('#fechas').datepicker({ format: 'dd/mm/yyyy', language:'es', startDate:'0d' });



        });
    </script>


