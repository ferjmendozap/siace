<form action="" id="formAjax" class="form" role="form" method="post" AUTOCOMPLETE="off">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idMotivoSalida}" name="idMotivoSalida"/>
        <input type="hidden" value="{if isset($idMaestro)}{$idMaestro}{/if}" name="form[int][idMaestro]"/>

        <div class="col-xs-6">

            <div class="form-group floating-label">

                <input type="text" class="form-control input-sm" maxlength="4" value="{if isset($formDB.cod_detalle)}{$formDB.cod_detalle}{/if}" name="form[alphaNum][cod_detalle]" id="cod_detalle" required data-msg-required="Introduzca Codigo Detalle"  >
                <label for="cod_detalle">Codigo Detalle</label>
                <p class="help-block"><span class="text-xs">* El campo es Obligatorio</span></p>
            </div>

        </div>

        <div class="col-xs-12">

            <div class="form-group floating-label">

                <input type="text" class="form-control input-sm" maxlength="100" value="{if isset($formDB.ind_nombre_detalle)}{$formDB.ind_nombre_detalle}{/if}" name="form[alphaNum][motivoSalida]" id="motivoSalida" required data-msg-required="Introduzca la Descripcion del Grupo Ocupacional" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_nombre_detalle"> Motivo de Salida </label>
                <p class="help-block"><span class="text-xs">* El campo es Obligatorio</span></p>
            </div>

        </div>


        <span class="clearfix"></span>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"> <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"> <span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>



<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {


            var datos = $("#formAjax" ).serialize();



            var accion = $("#accion").val();


                $.post("{$_Parametros.url}modPA/motivoSalidaCONTROL/RegistrarMotivoSalidaMET", datos, function (dato) {
                    
                    if(dato['status']=='modificacion'){

                        swal({
                            title: "¡Por favor espere!",
                            text: "Se esta procesando su solicitud, puede demorar un poco.",
                            timer: 50000000,
                            showConfirmButton: false
                        });


                        $(document.getElementById('idMotivoSalida'+dato['idMotivoSalida'])).html('<td>'+dato['cod_detalle']+'</td>' +
                                '<td>'+dato['motivoSalida']+'</td>' +
                                '<td  class="sort-alpha col-sm-1" >' +
                                '{if in_array('PA-01-06-10-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                                'data-keyboard="false" data-backdrop="static" idMotivoSalida="'+dato['idMotivoSalida']+'"' +
                                'descipcion="El Usuario a Modificado un Motivo de Salida" titulo="Modificar Motivo de Salida">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                                '{if in_array('PA-01-06-10-01-01-N',$_Parametros.perfil)}<td  class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idMotivoSalida="'+dato['idMotivoSalida']+'"  boton="si, Eliminar"' +
                                'descipcion="El usuario a eliminado un Motivo de Salida" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Motivo de Salida!!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>');
                        swal("Registro Modificado!", "El motivo de salida fue modificado satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                    }else if(dato['status']=='creacion'){
                        swal({
                            title: "¡Por favor espere!",
                            text: "Se esta procesando su solicitud, puede demorar un poco.",
                            timer: 50000000,
                            showConfirmButton: false
                        });


                        if(dato['status']=='error') {

                            swal("Error", "No se pudo realizar operacion. Verifique que los valores sean los correctos o Consulte con el Administrador del Sistema.", "error");

                        }else{

                            $(document.getElementById('datatable1')).append('<tr  id="idMotivoSalida'+dato['idMotivoSalida']+'">' +
                                    '<td>'+dato['cod_detalle']+'</td>' +
                                    '<td>'+dato['motivoSalida']+'</td>' +
                                    '<td   class="sort-alpha col-sm-1" >' +
                                    '{if in_array('PA-01-06-10-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                                    'data-keyboard="false" data-backdrop="static" idMotivoSalida="'+dato['idMotivoSalida']+'"' +
                                    'descipcion="El Usuario a Modificado un Motivo de Salida" titulo="Modificar Motivo de Salida">' +
                                    '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                                    '{if in_array('PA-01-06-10-01-01-N',$_Parametros.perfil)}<td   class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idMotivoSalida="'+dato['idMotivoSalida']+'"  boton="si, Eliminar"' +
                                    'descipcion="El usuario a eliminado un Motivo de Salida" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Motivo de Salida!!">' +
                                    '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                    '</td>' +
                                    '</tr>');
                            swal("Registro Guardado!", "El motivo de salida fue guardado satisfactoriamente.", "success");
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');

                        }

                    }

                }, 'json');
                swal("Registro Agregado!", "El motivo de salida ha sido agregado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //




        }
    });

    $(document).ready(function() {


        $("#formAjax").validate();


    });




    $(document).ready(function() {

        $("#formAjax").validate();

    });



    $('.accionModal').click(function () {

        accionModal(this,'url')
    });

    function accionModal(id,attr){

        $('#formModalLabel2').html($(id).attr('titulo'));
        $.post($(id).attr(attr), {
            cargar: 0,

            tr: $("#"+$(id).attr('idTabla')+" > tbody > tr").length + 1
        }, function ($dato) {
            $('#ContenidoModal2').html($dato);
        });
    }





</script>