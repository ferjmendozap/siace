<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido"  id="valido" />




                            <input type="hidden" value="{if isset($_MantenimientoPost[0]['pk_num_mantenimiento'])}{$_MantenimientoPost[0]['pk_num_mantenimiento']}{/if}"  name="form[int][idPost]"      id="idPost" />

                            <input type="hidden" value="{$_Acciones.accion}"    id="accion" />


                            <div class="col-lg-6">




                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <select   required="" class="form-control" name="form[int][Vehiculo]" id="Vehiculo" aria-required="true">
                                            <option value="">&nbsp;</option>

                                            {foreach item=tipo from=$_VehiculoPost}

                                                {if isset($_MantenimientoPost[0]['fk_pab001_num_vehiculo'])}

                                                    {if $tipo.pk_num_vehiculo==$_MantenimientoPost[0]['fk_pab001_num_vehiculo']}
                                                        <option selected value="{$tipo.pk_num_vehiculo}">{$tipo.ind_modelo} - {$tipo.ind_placa} </option>
                                                    {else}
                                                        <option value="{$tipo.pk_num_vehiculo}">{$tipo.ind_modelo} - {$tipo.ind_placa}</option>
                                                    {/if}
                                                {else}
                                                    <option value="{$tipo.pk_num_vehiculo}">{$tipo.ind_modelo} - {$tipo.ind_placa}</option>
                                                {/if}
                                            {/foreach}




                                        </select>
                                        <label for="rangelength2"> Vehículo </label>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <select   required="" class="form-control" name="form[int][TipoMantenimiento]" id="TipoMantenimiento" aria-required="true">
                                            <option value="">&nbsp;</option>




                                            {foreach item=tipo from=$_tipoMantenimientoPost}

                                                {if isset($_MantenimientoPost[0]['fk_a006_num_tipo_mantenimiento'])}

                                                    {if $tipo.pk_num_miscelaneo_detalle==$_MantenimientoPost[0]['fk_a006_num_tipo_mantenimiento']}
                                                        <option selected value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                                    {else}
                                                        <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
                                                    {/if}
                                                {else}
                                                    <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
                                                {/if}
                                            {/foreach}




                                        </select>
                                        <label for="rangelength2"> Tipo de Mantenimiento </label>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">


                                        <input type="text" required="" value="{if isset($_MantenimientoPost[0]['ind_en_ocacion'])}{$_MantenimientoPost[0]['ind_en_ocacion']}{/if}"name="form[alphaNum][ocasion]" data-rule-rangelength="[2,100]"  class="form-control"  >



                                        <label for="rangelength2"> Ocasión </label>
                                    </div>
                                </div>

                                <div class="col-lg-12">


                                    <div class="form-group   col-lg-6  ">
                                        <input required="" type="text" readonly  name="mantenimiento" class="form-control fechas2" value="{if isset($_MantenimientoPost[0]['fec_mant'])}{$_MantenimientoPost[0]['fec_mant']}{/if}">
                                        <label>Fecha de Mantenimiento</label>
                                    </div>

                                    <div class="form-group   col-lg-6  ">
                                        <input required="" type="text" readonly name="prox_mantenimiento" class="form-control fechas2" value="{if isset($_MantenimientoPost[0]['fec_proximo_mantenimiento'])}{$_MantenimientoPost[0]['fec_proximo_mantenimiento']}{/if}">
                                        <label>Fecha del Próximo Mantenimiento</label>
                                    </div>



                                </div>



                            </div>


                            <div class="col-lg-6">

                                <div class="col-lg-12">

                                    <div class="form-group">
                                        <textarea placeholder="" rows="3" class="form-control" id="Motivo" name="form[alphaNum][observacion]">{if isset($_MantenimientoPost[0]['ind_observacion'])}{$_MantenimientoPost[0]['ind_observacion']}{/if}</textarea>
                                        <label for="textarea1">Observación</label>
                                    </div>

                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group col-lg-6">
                                        <input  type="text" required="" value="{if isset($_MantenimientoPost[0]['num_monto'])}{$_MantenimientoPost[0]['num_monto']|number_format:2:",":"."}{/if}" name="form[monto][MontoPagado]" id="MontoPagado" class="form-control " aria-required="true">
                                        <label for="digits2">Monto Pagado</label>
                                        <p class="help-block">Solo Números</p>
                                    </div>
                                    <div class="form-group col-lg-6 ">
                                        <input required="" type="text"   value="{if isset($_MantenimientoPost[0]['ind_num_siguiente_kilometraje'])}{$_MantenimientoPost[0]['ind_num_siguiente_kilometraje']}{/if}" name="form[int][kilometraje]" data-rule-rangelength="[0, 9]" id="kilometraje" class="form-control"  >
                                        <label for="rangelength2">Kilometraje</label>
                                    </div>
                                </div>




                            </div>










                            </div>

                            <div class="row">
                                &nbsp;
                            </div>
                            <div class="row">
                                &nbsp;
                            </div>
                            <span class="clearfix"></span>




                            <div class="modal-footer">

                                <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                                {if  ($_Acciones.accion=="modificar")}
                                    <button type="submit" id="boton" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" type="button">Modificar</button>
                                {else}
                                    <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
                                {/if}

                            </div>


</form>



<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $("#formAjax" ).serialize();


            var accion = $("#accion").val();
            if(accion=="registrar") {
                $.post("{$_Parametros.url}modPA/mantenimientoCONTROL/RegistrarMantenimientoMET", datos, function (dato) {
                    $(document.getElementById('datatable1')).append('<td>'+dato['fecha']+'</td>' +
                            '<td>     </td>' +
                            '<td>'+dato['modelo']+' </td>' +
                            '<td>'+dato['ocasion']+' </td>' +
                            '<td>'+dato['observacion']+' </td>' +
                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idMantenimiento="'+dato['idMantenimiento']+'"' +
                            'descipcion="El Usuario a Modificado un Salida" titulo="Modificar Salida">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td   class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idMantenimiento="'+dato['idMantenimiento']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Salida" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Salida!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');

                }, 'json');
                swal("Mantenimiento Agregado!", "El mantenimiento del vehículo ha sido agregado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }else {
                $.post("{$_Parametros.url}modPA/mantenimientoCONTROL/ModificarMantenimientoMET",datos, function (dato) {
                    $(document.getElementById('idMantenimiento'+dato['idMantenimiento'])).html('<td>'+dato['fecha']+'</td>' +
                            '<td>   </td>' +
                            '<td>'+dato['modelo']+' </td>' +
                            '<td>'+dato['ocasion']+' </td>' +
                            '<td>'+dato['observacion']+' </td>' +
                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idMantenimiento="'+dato['idMantenimiento']+'"' +
                            'descipcion="El Usuario a Modificado un Mantenimiento" titulo="Modificar Mantenimiento">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td  class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idMantenimiento="'+dato['idMantenimiento']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Mantenimiento" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Mantenimiento!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');

                }, 'json');
                swal("Mantenimiento Modifcado!", "El mantenimiento del vehículo ha sido modificado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }




        }
    });



    $(document).ready(function() {
        $("#formAjax").validate();
        $('#fechas').datepicker({ format: 'dd/mm/yyyy', language:'es'});
        $('.fechas2').datepicker({ format: 'dd/mm/yyyy', language:'es'});
        $("#kilometraje").inputmask( "999999",{ numericInput: true});
        $('#MontoPagado').inputmask(' 999.999.999,99 BS', { numericInput: true });

    });
</script>
