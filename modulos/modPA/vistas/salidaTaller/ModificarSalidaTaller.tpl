

<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido"  id="valido" />


                            <input type="hidden" value="{if isset($_SalidaTallerPost[0]['pk_num_salida_taller'])}{$_SalidaTallerPost[0]['pk_num_salida_taller']}{/if}"  name="form[int][idPost]"      id="idPost" />




                            {foreach item=post from=$_Acciones}
                                <input type="hidden" value="{$post.accion}"    id="accion" />
                            {/foreach}

                            <div class="col-lg-6">

                                <div class="col-lg-12">

                                    <div class="form-group   col-lg-6  ">
                                        <input required="" type="text" readonly name="salida" class="form-control fechas2" value="{if isset($_SalidaTallerPost[0]['fec_entrada'])}{$_SalidaTallerPost[0]['fec_entrada']}{/if}">
                                        <label>Fecha de Salida</label>
                                    </div>

                                    <div required="" class="form-group col-lg-6 ">
                                        <input required="" type="text"name="form[txt][hora]" id="hora_hasta" class="form-control time12-mask" value="{if isset($_SalidaTallerPost[0]['hora'])}{$_SalidaTallerPost[0]['hora']}{/if}">
                                        <label>Hora de Salida</label>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">


                                    <input required="" type="text" required="" value="{if isset($_SalidaTallerPost[0]['ind_kilometraje'])}{$_SalidaTallerPost[0]['ind_kilometraje']}{/if}" name="form[int][Kilometraje]"  data-rule-rangelength="[0, 9]" id="kilometraje" class="form-control"  >



                                    <label for="rangelength2">Kilometraje</label>
                                        <p class="help-block">Solo Números</p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input required="" type="text"     value="{if isset($_SalidaTallerPost[0]['num_monto'])} {$_SalidaTallerPost[0]['num_monto']|number_format:2:",":"."}{/if}" name="form[monto][MontoPagado]" id="MontoPagado" class="form-control " aria-required="true">
                                        <label for="digits2">Monto Pagado</label>
                                        <p class="help-block">Solo Números</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <textarea required="" placeholder="" rows="3" class="form-control" id="Condiciones" name="form[txt][Condiciones]">{if isset($_SalidaTallerPost[0]['ind_condiciones'])}{$_SalidaTallerPost[0]['ind_condiciones']}{/if}</textarea>
                                    <label for="textarea1"> Condiciones </label>
                                </div>

                                <div class="form-group">
                                    <textarea required="" placeholder="" rows="3" class="form-control" id="ObservacionSalida" name="form[txt][ObservacionSalida]">{if isset($_SalidaTallerPost[0]['ind_observacion'])}{$_SalidaTallerPost[0]['ind_observacion']}{/if}</textarea>
                                    <label for="textarea1">Observación de la Salida del Vehículo</label>
                                </div>



                            </div>


                            <div class="row">
                                &nbsp;
                            </div>
                            <div class="row">
                                &nbsp;
                            </div>
                            <span class="clearfix"></span>




                            <div class="modal-footer">

                                <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                                {if  ($_Acciones.accion=="modificar")}
                                    <button type="submit" id="boton" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" type="button">Modificar</button>
                                {else}
                                    <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
                                {/if}

                            </div>


</form>

<!--end .card-body -->


<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {


            var datos = $("#formAjax" ).serialize();



                $.post("{$_Parametros.url}modPA/salidaTallerCONTROL/ModificarSalidaTallerMET", datos, function (dato) {

                    $(document.getElementById('idSalida'+dato['idSalida'])).html('<td>'+dato['idSalida']+'</td>' +
                            '<td> Registro Modificado </td>' +
                            '<td>'+dato['salida']+' </td>' +
                            '<td>'+dato['modelo']+' </td>' +
                            '<td>'+dato['monto']+' </td>' +
                            '<td>'+dato['observacion']+' </td>' +
                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idSalida="'+dato['idSalida']+'"' +
                            'descipcion="El Usuario a Modificado un Salida" titulo="Modificar Salida">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td  class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idSalida="'+dato['idSalida']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Salida" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Salida!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');

                }, 'json');
                swal("Salida Taller Modificada!", "La salida del vehículo del taller ha sido modificado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //



        }
    });



    $(document).ready(function() {
        $("#formAjax").validate();
        $('#fechas').datepicker({ format: 'dd/mm/yyyy', language:'es', startDate:'0d' });
        $('.fechas2').datepicker({ format: 'dd/mm/yyyy', language:'es', startDate:'0d' });
        $("#kilometraje").inputmask( "999999",{ numericInput: true});
        $('#MontoPagado').inputmask(' 999.999.999,99 BS', { numericInput: true });



    });
</script>