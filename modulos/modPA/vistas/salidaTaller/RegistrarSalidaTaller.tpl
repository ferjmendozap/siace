<link type="text/css"   rel="stylesheet" href="{$_Parametros.ruta_Complementos}theme-default/libs/wizard/wizard.css?1425466601" />





<!-- BEGIN VALIDATION FORM WIZARD -->


<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
    <form action="" id="formAjax"class="form   form-validation" role="form"  >



        <input type="hidden" value="1" name="valido"  id="valido" />
        <div class="form-wizard-nav">
            <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
            <ul class="nav nav-justified">



                                {if  ($_Acciones.accion=="registrar")}
                                <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">GENERAL</span></a></li>
                                {/if}
                                <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title"> INFORMACIÓN DE SALIDA DEL VEHÍCULO</span></a></li>



                            </ul>
                        </div><!--end .form-wizard-nav -->
                        <div class="tab-content clearfix">
                            <div class="tab-pane active" id="step1">





                            <input type="hidden" value="{if isset($_SalidaTallerPost[0]['pk_num_salida_taller'])}{$_SalidaTallerPost[0]['pk_num_salida_taller']}{/if}"  name="form[int][idPost]"      id="idPost" />





                            <input type="hidden" value="{$_Acciones.accion}"    id="accion" />


                            <div class="col-lg-6">


                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <select  required="" class="form-control" name="form[int][EntradaTaller]" id="EntradaTaller" aria-required="true">
                                            <option value="">&nbsp;</option>



                                            {foreach item=tipo from=$_EntradaTallerPost}

                                                {if isset($_EntradaIntitucionPost.fk_pae001_num_salida_vehiculo)}

                                                    {if $tipo.pk_num_salida_vehiculo==$_EntradaIntitucionPost.fk_pae001_num_salida_vehiculo}
                                                        <option selected value="{$tipo.pk_num_entrada_taller}">{$tipo.ind_modelo} - {$tipo.ind_placa} </option>
                                                    {else}
                                                        <option value="{$tipo.pk_num_entrada_taller}">{$tipo.ind_modelo} - {$tipo.ind_placa}</option>
                                                    {/if}
                                                {else}
                                                    <option value="{$tipo.pk_num_entrada_taller}">{$tipo.ind_modelo} - {$tipo.ind_placa}</option>
                                                {/if}
                                            {/foreach}




                                        </select>
                                        <label for="rangelength2"> Vehículos en Taller </label>
                                    </div>
                                </div>


                            </div>






                        </div><!--end #step1 -->

                        <div class="tab-pane" id="step2">

                            <div class="col-lg-6">

                                <div class="col-lg-12">

                                    <div class="form-group   col-lg-6  ">
                                        <input required="" type="text" name="salida"  readonly class="form-control fechas2" value="{if isset($_SalidaTallerPost[0]['fec_entrada'])}{$_SalidaTallerPost[0]['fec_entrada']}{/if}">
                                        <label>Fecha de Salida</label>
                                    </div>

                                    <div required="" class="form-group col-lg-6 ">
                                        <input required="" type="text"name="form[alphaNum][hora]" id="hora_hasta" class="form-control time12-mask" value="{if isset($_SalidaTallerPost[0]['hora'])}{$_SalidaTallerPost[0]['hora']}{/if}">
                                        <label>Hora de Salida</label>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">


                                    <input required="" type="text" required="" value="{if isset($_SalidaTallerPost[0]['ind_kilometraje'])}{$_SalidaTallerPost[0]['ind_kilometraje']}{/if}" name="form[int][Kilometraje]"  data-rule-rangelength="[0, 9]" id="kilometraje" class="form-control"  >



                                    <label for="rangelength2">Kilometraje</label>
                                        <p class="help-block">Solo Números</p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input required="" type="text"     value="{if isset($_SalidaTallerPost[0]['num_monto'])} {$_SalidaTallerPost[0]['num_monto']|number_format:2:",":"."}{/if}" name="form[monto][MontoPagado]" id="MontoPagado" class="form-control " aria-required="true">
                                        <label for="digits2">Monto Pagado</label>
                                        <p class="help-block">Solo Números</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <textarea required="" placeholder="" rows="3" class="form-control" id="Condiciones" name="form[alphaNum][Condiciones]">{if isset($_SalidaTallerPost[0]['ind_condiciones'])}{$_SalidaTallerPost[0]['ind_condiciones']}{/if}</textarea>
                                    <label for="textarea1"> Condiciones </label>
                                </div>

                                <div class="form-group">
                                    <textarea required="" placeholder="" rows="3" class="form-control" id="ObservacionSalida" name="form[alphaNum][ObservacionSalida]">{if isset($_SalidaTallerPost[0]['ind_observacion'])}{$_SalidaTallerPost[0]['ind_observacion']}{/if}</textarea>
                                    <label for="textarea1">Observación de la Salida del Vehículo</label>
                                </div>


                            </div>
                            <div class="row">
                                &nbsp;
                            </div>
                            <div class="row">
                                &nbsp;
                            </div>

                            <div class="row">
                                &nbsp;
                            </div>

                            <div class="row">
                                &nbsp;
                            </div>

                            <div class="row">
                                &nbsp;
                            </div>
                            <div class="row">
                                &nbsp;
                            </div>
                            <span class="clearfix"></span>

                            <div class="modal-footer">

                                <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>

                                <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>


                            </div>
                        </div><!--end #step2 -->
                        </div>

        <ul class="pager wizard">

            <li class="previous"><a class="btn-raised"       href="javascript:void(0);">Previo</a></li>

            <li class="next"><a class="btn-raised"           href="javascript:void(0);">Siguiente</a></li>
        </ul>




    </form>
</div>


<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {


            var datos = $("#formAjax" ).serialize();




            var accion = $("#accion" ).val();


                $.post("{$_Parametros.url}modPA/salidaTallerCONTROL/RegistrarSalidaTallerMET",datos, function (dato) {
                    $(document.getElementById('datatable1')).append('<td>'+dato['idSalida']+'</td>' +
                            '<td> Registro Modificado </td>' +
                            '<td>'+dato['salida']+' </td>' +
                            '<td>'+dato['modelo']+' </td>' +
                            '<td>'+dato['monto']+' </td>' +
                            '<td>'+dato['observacion']+' </td>' +
                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idSalida="'+dato['idSalida']+'"' +
                            'descipcion="El Usuario a Modificado un Salida" titulo="Modificar Salida">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td   class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idSalida="'+dato['idSalida']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Salida" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Salida!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');
                }, 'json');
                swal("Registro Agregado!", "La marca del vehículo ha sido agregado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //



        }
    });



    $(document).ready(function() {
        $("#formAjax").validate();
        $('#fechas').datepicker({ format: 'dd/mm/yyyy', language:'es'});
        $('.fechas2').datepicker({ format: 'dd/mm/yyyy', language:'es'});
        $("#kilometraje").inputmask( "999999",{ numericInput: true});
        $('#MontoPagado').inputmask(' 999.999.999,99 BS', { numericInput: true });



    });
</script>