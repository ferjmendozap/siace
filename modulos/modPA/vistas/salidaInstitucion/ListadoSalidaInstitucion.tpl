<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************-->
<div id="body1">
    <div class="card">
        <div class="card-head">
            <header> <h2 class="text-primary">LISTAR SALIDAS DE LA INSTITUCIÓN </h2></header>
        </div>
        <div class="col-md-12">
            <h5>Listado de las salidas registradas de la institución.</h5>
        </div><!--end .col -->

    </div><!--end .card -->



</div><!--end .card -->

<section class="style-default-bright">
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th class="sort-alpha col-sm-1">N° </th>
                            <th class="sort-alpha col-sm-1">Vehículo</th>
                            <th class="sort-alpha col-sm-2">Modelo-Placa</th>
                            <th class="sort-alpha col-sm-2">Fecha Salida</th>
                            <th class="sort-alpha col-sm-2">Hora Salida</th>
                            <th class="sort-alpha col-sm-3">Observacion</th>
                            <th class="  col-sm-1"> Modificar Salida</th>
                            <th class="  col-sm-1"> Anular Salida</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=post from=$_SalidaInstitucionPost}
                            <tr id="idSalida{$post.pk_num_salida_vehiculo}" class="gradeA">
                                <td>{$post.pk_num_salida_vehiculo}</td>
                                <td>
                                    {if !is_null($post.ind_url_foto_frontal)&&trim($post.ind_url_foto_frontal)!=""&&trim($post.ind_url_foto_frontal)!="faceman_Suburban_Assault_Vehicle_(Front).svg"}
                                        <img src="{$_Parametros['ruta_Img']}modPA/{$post.ind_placa}/{$post.ind_url_foto_frontal}" class="height-1 width-1 img-circle "  />
                                    {else}
                                        <img src="{$_Parametros['ruta_Img']}modPA/faceman_Suburban_Assault_Vehicle_(Front).svg" class="height-1 width-1 img-circle "  />
                                    {/if}
                                </td>
                                <td>{$post.ind_modelo}-{$post.ind_placa}</td>
                                <td>{$post.fec_hora_salida}</td>
                                <td>{$post.hora_salida}</td>
                                <td>{$post.ind_observacion}</td>
                                <td>
                                    {if in_array('PA-01-02-01-02-M',$_Parametros.perfil)}
                                        <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Modificar Salida" descipcion="--" idSalida="{$post.pk_num_salida_vehiculo}" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                                        </button>
                                    {/if}
                                </td>
                                <td>
                                    {if in_array('PA-01-02-01-03-A',$_Parametros.perfil)}
                                        <button class="anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idSalida="{$post.pk_num_salida_vehiculo}" mensaje="Estas seguro que desea anular la salida del vehículo!!" titulo="Estas Seguro?" descipcion="El usuario a eliminado un post" boton="si, Eliminar" idmenu="7">
                                            <i class="md md-delete" style="color: #ffffff;"></i>
                                        </button>
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="7">
                                {if in_array('PA-01-02-01-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info " data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                        descripcion="Registro de salida de un vehículo de la institución"  titulo="Registrar Salida" id="nuevo" >
                                        <i class="md md-create"></i> Registrar Salida &nbsp;&nbsp;
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div><!--end .table-responsive -->
            </div><!--end .col -->
        </div><!--end .row -->
    </div><!--end .row -->
    <!-- fin listado -->
</section>




    <script type="text/javascript">
        $(document).ready(function() {
            var $url='{$_Parametros.url}modPA/salidaInstitucionCONTROL/RegistrarSalidaInstitucionMET';
            $('#nuevo').click(function(){
                $('#modalAncho').css( "width", "85%" );
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idMenu:0 },function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });




            $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#modalAncho').css( "width", "85%" );
                var $url='{$_Parametros.url}modPA/salidaInstitucionCONTROL/ModificarSalidaInstitucionMET';

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idSalida')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });
			
		 
			
			 $('#datatable1 tbody').on( 'click', '.anular', function () {

                var idPost=$(this).attr('idSalida');
                var idMenu=$(this).attr('idMenu');
                swal({
                    title:$(this).attr('titulo'),
                    text: $(this).attr('mensaje'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false
                }, function(){

                    var $url='{$_Parametros.url}modPA/salidaInstitucionCONTROL/AnularSalidaInstitucionMET';
                    $(document.getElementById('idSalida'+idPost)).html('');

                    $.post($url,{ idPost: idPost},function($dato){
                        swal("Eliminado!", "el registro fue eliminado satisfactoriamente.", "success");
                    });


                });
            });
			

        });
    </script>




