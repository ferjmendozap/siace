<form action="" id="formAjax" class="form" role="form" method="post" AUTOCOMPLETE="off">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idMarca}" name="idMarca"/>
        <input type="hidden" value="{if isset($idMaestro)}{$idMaestro}{/if}" name="form[int][idMaestro]"/>

        <div class="col-xs-6">

            <div class="form-group floating-label">

                <input type="text" class="form-control input-sm" maxlength="4" value="{if isset($formDB.cod_detalle)}{$formDB.cod_detalle}{/if}" name="form[txt][cod_detalle]" id="cod_detalle" required data-msg-required="Introduzca Codigo Detalle"  >
                <label for="cod_detalle">Codigo Detalle</label>
                <p class="help-block"><span class="text-xs">* El campo es Obligatorio</span></p>
            </div>

        </div>

        <div class="col-xs-12">

            <div class="form-group floating-label">

                <input type="text" class="form-control input-sm" maxlength="100" value="{if isset($formDB.ind_nombre_detalle)}{$formDB.ind_nombre_detalle}{/if}" name="form[txt][marca]" id="marca" required data-msg-required="Introduzca la Descripcion del Grupo Ocupacional" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_nombre_detalle"> Marca del Vehículo</label>
                <p class="help-block"><span class="text-xs">* El campo es Obligatorio</span></p>
            </div>

        </div>


        <span class="clearfix"></span>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"> <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"> <span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>



<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {


            var datos = $("#formAjax" ).serialize();



            var accion = $("#accion").val();


                $.post("{$_Parametros.url}modPA/marcaCONTROL/RegistrarMarcaVehiculoMET", datos, function (dato) {
                    
                    if(dato['status']=='modificacion'){
                        $(document.getElementById('idMarca'+dato['idMarca'])).html('<td>'+dato['cod_detalle']+'</td>' +
                                '<td>'+dato['marca']+'</td>' +
                                '<td  class="sort-alpha col-sm-1" >' +
                                '{if in_array('RH-01-04-01-11-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                                'data-keyboard="false" data-backdrop="static" idMarca="'+dato['idMarca']+'"' +
                                'descipcion="El Usuario a Modificado una Marca" titulo="Modificar Marca">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                                '{if in_array('RH-01-04-01-12-E',$_Parametros.perfil)}<td  class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idMarca="'+dato['idMarca']+'"  boton="si, Eliminar"' +
                                'descipcion="El usuario a eliminado una Marca" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Marca!!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>');
                        swal("Registro Modificado!", "Marca  fue modificada satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                    }else if(dato['status']=='creacion'){
                        $(document.getElementById('datatable1')).append('<tr  id="idMarca'+dato['idMarca']+'">' +
                                '<td>'+dato['cod_detalle']+'</td>' +
                                '<td>'+dato['marca']+'</td>' +
                                '<td   class="sort-alpha col-sm-1" >' +
                                '{if in_array('RH-01-04-01-11-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                                'data-keyboard="false" data-backdrop="static" idMarca="'+dato['idMarca']+'"' +
                                'descipcion="El Usuario a Modificado un Marca" titulo="Modificar Marca">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                                '{if in_array('RH-01-04-01-12-E',$_Parametros.perfil)}<td   class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idMarca="'+dato['idMarca']+'"  boton="si, Eliminar"' +
                                'descipcion="El usuario a eliminado una Marca" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Marca!!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>' +
                                '</tr>');
                        swal("Registro Guardado!", "La Marca fue guardado satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                    }

                }, 'json');






        }
    });

    $(document).ready(function() {


        $("#formAjax").validate();


    });




</script>