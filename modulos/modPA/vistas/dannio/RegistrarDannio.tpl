<link type="text/css"   rel="stylesheet" href="{$_Parametros.ruta_Complementos}theme-default/libs/wizard/wizard.css?1425466601" />





<!-- BEGIN VALIDATION FORM WIZARD -->


<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
    <form action="" id="formAjax"class="form   form-validation" role="form"  >

        <input type="hidden" value="1" name="valido"  id="valido" />

        <div class="form-wizard-nav">
            <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
            <ul class="nav nav-justified">
                                <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">GENERAL</span></a></li>
                                {if  ($_Acciones.accion=="modificars")}
                                <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title"> PIEZA DAÑADA</span></a></li>
                                <li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title"> VER DAÑO</span></a></li>

                                {else}
                                <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title"> PIEZAS DAÑADAS</span></a></li>
                                {/if}
                            </ul>
                        </div><!--end .form-wizard-nav -->
                        <div class="tab-content clearfix">
                            <div class="tab-pane active" id="step1">

                            <input type="hidden" value="{if isset($_DannioPost.pk_num_averia_vehiculo)}{$_DannioPost.pk_num_averia_vehiculo}{/if}"  name="form[int][idDannio]"      id="idDannio" />



                                <input type="hidden" value="{if isset($_Acciones.accion)}{$_Acciones.accion}{/if}"    id="accion" />

                                <div class="col-lg-6">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <select required="" class="form-control" name="form[int][Vehiculo]" id="Vehiculo" aria-required="true">
                                            <option value="">&nbsp;</option>



                                            {foreach item=tipo from=$_VehiculoPost}

                                                {if isset($_DannioPost.fk_pab001_num_vehiculo)}

                                                    {if $tipo.pk_num_vehiculo==$_DannioPost.fk_pab001_num_vehiculo}
                                                        <option selected value="{$tipo.pk_num_vehiculo}">{$tipo.ind_modelo}- {$tipo.ind_placa} </option>
                                                    {else}
                                                        <option value="{$tipo.pk_num_vehiculo}">{$tipo.ind_modelo}- {$tipo.ind_placa}</option>
                                                    {/if}
                                                {else}
                                                    <option value="{$tipo.pk_num_vehiculo}">{$tipo.ind_modelo}- {$tipo.ind_placa} </option>
                                                {/if}
                                            {/foreach}




                                        </select>
                                        <label for="rangelength2"> Vehículo</label>
                                    </div>

                                </div>

                                <div class="col-lg-12">


                                    <div class="form-group">
                                        <input type="text" required="" readonly name="entrada" class="form-control fechas2" value="{if isset($_DannioPost.fec_registro)}{$_DannioPost.fec_registro}{/if}">
                                        <label>Fecha de Registro</label>
                                    </div>
                                </div>
                            </div>

                                <div class="col-lg-6">

                                    <div class="form-group">
                                        <textarea required="" placeholder="" rows="3" class="form-control" id="ObservacionDannio" name="form[alphaNum][ObservacionDannio]">{if isset($_DannioPost.ind_observacion_averia)}{$_DannioPost.ind_observacion_averia}{/if}</textarea>
                                        <label for="textarea1">Observación de los daños</label>
                                    </div>

                                </div>

                                <div class="row">
                                    &nbsp;
                                </div>
                                <div class="row">
                                    &nbsp;
                                </div>

                        </div><!--end #step1 -->

                        <div class="tab-pane" id="step2">

                            <div class="col-lg-6"> &nbsp;&nbsp; Piezas </div> <div class="col-lg-6"> &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  Piezas Dañadas</div>

                            <div class="card-body">
                                <form class="form">

                                    <select   required=""  multiple="multiple"   id="optgroup"  name="form[int][menu][]" style="position: absolute; left: -9999px;">
                                      {$_DannioPost.fk_pab007_num_piezas}
                                      {foreach item=menu from=$_PiezaPost}

                                            {if  ($menu.pk_num_piezas==$_DannioPost.fk_pab007_num_piezas)}
                                                <option selected="selected" value="{$menu.pk_num_piezas}">{$menu.ind_descripcion} ({$menu.nombre_ubia} - {$menu.nombre_ubib} - {$menu.nombre_ubic})   </option>
                                            {else}
                                                <option value="{$menu.pk_num_piezas}">{$menu.ind_descripcion}  ({$menu.nombre_ubia} - {$menu.nombre_ubib} - {$menu.nombre_ubic})   </option>
                                            {/if}
                                        {/foreach}

                                     </select>



                            </div>
                            {if  ($_Acciones.accion!="modificars")}
                            <div class="modal-footer">

                                <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>

                                <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>


                            </div>
                            {/if}

                        </div><!--end #step2 -->

                            <div class="tab-pane nav-tabs tabs-center " id="step3">

                                <output id="output_list3">

                                        {if !is_null($_DannioPost.ind_url)&&trim($_DannioPost.ind_url)!=""}

                                            <img src="{$_Parametros['ruta_Img']}modPA/dannio/{$_DannioPost.ind_placa}/{$_DannioPost.ind_url}" class="height-4 width-4 img-rounded " style="cursor: pointer" onclick="document.getElementById('foto').click()" />

                                        {else}
                                            <img src="{$_Parametros['ruta_Img']}modPA/llave-herramienta.png" class="height-4 width-4 img-rounded " style="cursor: pointer" onclick="document.getElementById('foto').click()" />
                                        {/if}


                                </output>
                                <input type="hidden" id="Ruta" name="Ruta"  value="dannio/{$_DannioPost.ind_placa}"/>
                                <input style="display:none; width: 300px"  type="file" id="foto" name="foto" accept="image/jpeg" />
                                <input type="hidden" name="form[txt][bd_foto]"  value="{if    isset($_DannioPost.ind_url)}{($_DannioPost.ind_url)}{/if}"    id="bd_foto" />
                                <label for="rangelength2"> Foto del Daño </label>


                                <div class="modal-footer">

                                    <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>

                                    <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>


                                </div>
                            </div><!--end #step3 -->

                        </div>

                            <ul class="pager wizard">

                                <li class="previous"><a class="btn-raised"       href="javascript:void(0);">Previo</a></li>

                                <li class="next"><a class="btn-raised"           href="javascript:void(0);">Siguiente</a></li>
                            </ul>




    </form>
</div>


<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {


            var datos = $("#formAjax" ).serialize();



            var accion = $("#accion").val();
            if(accion=="registrar") {
                $.post("{$_Parametros.url}modPA/dannioCONTROL/RegistrarDannioMET",datos, function (dato) {
                    $(document.getElementById('datatable1')).append('<td>'+dato['idDanio']+'</td>' +
                            '<td>    </td>' +
                            '<td>'+dato['fecha']+' </td>' +
                            '<td>'+dato['modelo']+' </td>' +
                            '<td>'+dato['pieza']+' </td>' +

                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idDaño="'+dato['idDanio']+'"' +
                            'descipcion="El Usuario a Modificado  Daño" titulo="Modificar Daño">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td   class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idDaño="'+dato['idDanio']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado  Daño" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Daño!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');

                }, 'json');
                swal("Registro Agregado!", "Daño ha sido agregado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }else {
                $.post("{$_Parametros.url}modPA/dannioCONTROL/ModificarDannioMET", datos, function (dato) {
                    $(document.getElementById('idDanio'+dato['idDanio'])).html('<td>'+dato['idDanio']+'</td>' +
                            '<td>   </td>' +
                            '<td>'+dato['fecha']+' </td>' +
                            '<td>'+dato['modelo']+' </td>' +
                            '<td>'+dato['pieza']+' </td>' +

                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idDanio="'+dato['idDanio']+'"' +
                            'descipcion="El Usuario a Modificado  Daño" titulo="Modificar Daño">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td  class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idDanio="'+dato['idDanio']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado  Daño" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Daño!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');

                }, 'json');
                swal("Registro Modificado!", "Daño vehículo ha sido modificado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }



        }
    });



    $(document).ready(function() {

        $("#formAjax").validate();
        $('#fechas').datepicker({ format: 'dd/mm/yyyy', language:'es', startDate:'0d' });
        $('.fechas2').datepicker({ format: 'dd/mm/yyyy', language:'es', startDate:'0d' });
        $('#optgroup').multiSelect({ selectableOptgroup: true });


        // Asignamos el valor que trae el fk en el select
        $("#ClaseVehiculo").val($("#idClase").val());




    });

    function archivo2(evt) {

        var Placa= $("#Ruta").val();
        var files = evt.target.files; // FileList object

        // Obtenemos la imagen del campo "file".
        for (var i = 0, f; f = files[i]; i++) {
            //Solo admitimos imágenes.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            reader.onload = (function(theFile) {
                return function(e) {
                    // Insertamos la imagen
                    document.getElementById("output_list3").innerHTML = ['<img class="height-4 width-4 img-rounded " onclick="document.getElementById(\'foto\').click()" width="295px" height="250px"  src="', e.target.result,'" title="', escape(theFile.tmp_name), '"/>'].join('');

                    var data = new FormData();
                    jQuery.each($('#foto')[0].files, function(i, file) {
                        data.append('files-'+i, file);
                        $("#bd_foto").val(theFile.name);

                    });

                    data.append('Carpeta', Placa);
                    data.append('Vista', 'VLD');

                    $.ajax({
                        url: '{$_Parametros.url}modPA/vehiculoCONTROL/SubirFotoMET',
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST',
                        success: function(data){

                        }
                    });
                };

            })(f);
            reader.readAsDataURL(f);
        }
    }
    document.getElementById("foto").addEventListener('change', archivo2 ,false);
</script>