<link type="text/css"   rel="stylesheet" href="{$_Parametros.ruta_Complementos}theme-default/libs/wizard/wizard.css?1425466601" />





<!-- BEGIN VALIDATION FORM WIZARD -->


<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
    <form action="" id="formAjax"class="form   form-validation" role="form"  >



        <input type="hidden" value="1" name="valido"  id="valido" />

                            <div class="col-lg-12">

                            <div class="card-body">
                                <form class="form">
                                    <div class="col-lg-6">Piezas Dañadas </div> <div class="col-lg-6"> &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  Daños Solucionados</div>
                                    <select  required="" multiple="multiple"   id="optgroup"  name="form[int][menu][]" style="position: absolute; left: -9999px;">

                                        {$i=0}
                                        {foreach item=menu from=$_VehiculoPost}

                                            {if ($_VehiculoPost[$i]['ind_estado']==0)}
                                                <option value="{$menu.pk_num_averia_vehiculo}">{$menu.pieza} </option>
                                            {else}
                                                <option selected value="{$menu.pk_num_averia_vehiculo}">{$menu.pieza} </option>
                                            {/if}
                                            {$i++}
                                        {/foreach}

                                   </select>

                                </form>
                            </div>


                                </div>
                                <div class="row">
                                    &nbsp;
                                </div>
                                <div class="row">
                                    &nbsp;
                                </div>

                                <div class="row">
                                    &nbsp;
                                </div>

                                <div class="row">
                                    &nbsp;
                                </div>

                                <div class="row">
                                    &nbsp;
                                </div>
                                <div class="row">
                                    &nbsp;
                                </div>
                                <span class="clearfix"></span>

                                <div class="modal-footer">

                                    <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>


                                </div>




    </form>
</div>



<script type="text/javascript">


    $.validator.setDefaults({
        submitHandler: function() {


            var datos = $("#formAjax" ).serialize();



            var accion = $("#accion").val();
            if(accion=="registrar") {
                $.post("{$_Parametros.url}modPA/solucionDannioCONTROL/RegistrarSolucionDannioMET",datos, function (dato) {


                }, 'json');
                swal("Registro Agregado!", "La marca del vehículo ha sido agregado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }else {
                $.post("{$_Parametros.url}modPA/solucionDannioCONTROL/ModificarSolucionDannioMET", datos, function (dato) {


                }, 'json');
                swal("Registro Modifcado!", "La marca del vehículo ha sido modificado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }




        }
    });



    $(document).ready(function() {

        $("#formAjax").validate();
        $('#fechas').datepicker ({  format: 'dd/mm/yyyy', language:'es', startDate:'0d'});
        $('.fechas2').datepicker({ format: 'dd/mm/yyyy', language:'es', startDate:'0d' });
        $('#optgroup').multiSelect({  });
		$('#MontoPagado').inputmask(' 999.999.999,99 BS', { numericInput: true });




        $("#Vehiculo").change(function () {

            $("#Vehiculo option:selected").each(function () {

                $.post("{$_Parametros.url}modPA/solucionDannioCONTROL/ActualizarDannioMET", { Vehiculo: $('#Vehiculo').val() ,accion: $('#accion').val() }, function(dato){

                    $('#ContenidoModal').html(dato);

                });
            });
        });


    });
</script>