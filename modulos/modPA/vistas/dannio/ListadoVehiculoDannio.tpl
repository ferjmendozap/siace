<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div id="body1">
    <div class="card">
        <div class="card-head">
            <header> <h2 class="text-primary">LISTAR VEHÍCULOS CON DAÑOS</h2></header>
        </div>
        <div class="col-md-12">
            <h5>Listado de los vehículos registrados en la institución que presentan daños actualmente.</h5>
        </div><!--end .col -->

    </div><!--end .card -->


    <div class="form-group   col-lg-12 ">

        <div class="col-lg-11 "></div>	<a class="btn btn-flat btn-accent ink-reaction span8" href="javascript:void(0);">filtrar</a>
    </div>

</div><!--end .card -->

<section class="style-default-bright">




    <!-- Listado -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-actionbar">
                <div class="card-actionbar-row">

                </div>
            </div>
        </div><!--end .col -->
    </div><!--end .row -->
    <div class="row">

        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="sort-alpha col-sm-1">N° </th>
                        <th class="sort-alpha col-sm-1">Vehículo</th>
                        <th class="sort-alpha col-sm-1">Fecha Registro</th>
                        <th class="sort-alpha col-sm-3"> Modelo-Placa</th>
                        <th class="sort-alpha col-sm-1">Ver Daños Actuales</th>

                    </tr>
                    </thead>
                    <tbody>

                    {foreach item=post from=$_DannioPost}
                        <tr id="idPost{$post.pk_num_averia_vehiculo}" class="gradeA">
                            <td>{$post.pk_num_averia_vehiculo}</td>
                            <td>
                                {if !is_null($post.ind_url_foto_frontal)&&trim($post.ind_url_foto_frontal)!=""&&trim($post.ind_url_foto_frontal)!="faceman_Suburban_Assault_Vehicle_(Front).svg"}

                                    <img src="{$_Parametros['ruta_Img']}modPA/{$post.ind_placa}/{$post.ind_url_foto_frontal}" class="height-1 width-1 img-circle "  />
                                {else}
                                    <img src="{$_Parametros['ruta_Img']}modPA/faceman_Suburban_Assault_Vehicle_(Front).svg" class="height-1 width-1 img-circle "  />

                                {/if}
                            </td>
                            <td>{$post.fec_registro}</td>
                            <td>{$post.ind_modelo} - {$post.ind_placa}</td>





 
                            <td>{if in_array('PA-01-03-01-02-M',$_Parametros.perfil)}
                                <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Ver Daños" descipcion="--" idModificar="{$post.fk_pab001_num_vehiculo}" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                    <i class="fa fa-eye" style="color: #ffffff;"></i>
                                </button>
                                {/if}
                            </td>




                        </tr>








                    {/foreach}
                    </tbody>
                </table>
            </div><!--end .table-responsive -->
        </div><!--end .col -->
    </div><!--end .row -->
    <!-- fin listado -->




    </div><!--end .row -->




    <script type="text/javascript">
        $(document).ready(function() {




            $('#datatable1 tbody').on( 'click', '.ver', function () {
                var $url='{$_Parametros.url}modPA/dannioCONTROL/VerVehiculoDannioMET';

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idModificar')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });




        });
    </script>
    </div>




