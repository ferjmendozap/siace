<!-- BEGIN VALIDATION FORM WIZARD -->

<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido"  id="valido" />


                                <div class="col-lg-6">
                                    <form class="form form-validate " novalidate="novalidate">
                                        <div class="form-group">
                                        {foreach item=post from=$_TipoVehiculoPost}


                                            <input value="{if isset($post.ind_descripcion)}{$post.ind_descripcion}{/if}" type="text" class="form-control" id="TipoVehiculo" name="TipoVehiculo" data-rule-rangelength="[2, 30]" required>
                                            <input type="hidden" value="{$post.pk_num_tipo_vehiculo}"  name="idPost"    id="idPost" />
                                            <input type="hidden" value="{$post.fk_a006_num_clase_vehiculo}"  name="idClase"    id="idClase" />
                                        {/foreach}

                                        <input type="hidden" value="{$_Acciones.accion}"    id="accion" />


                                        <label for="rangelength2">Tipo de Vehículo</label>
                                        </div>


                                        <div class="form-group">
                                            <select required="" class="form-control" name="ClaseVehiculo" id="ClaseVehiculo" aria-required="true">
                                                <option value="">&nbsp;</option>

                                                {foreach item=post from=$_ClaseVehiculoPost}
                                                    <option value="{$post.pk_num_miscelaneo_detalle}">{$post.ind_nombre_detalle}</option>

                                                {/foreach}

                                            </select>
                                            <label for="rangelength2">Clase de Vehículo</label>
                                        </div>
                                </div>


        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

        <div class="modal-footer">

            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>

            <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>


        </div>
    </div>       </div>






</form>
</div>


<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {


            var datos = $("#formAjax" ).serialize();



            var accion = $("#accion").val();
            if(accion=="registrar") {
                $.post("{$_Parametros.url}modPA/tipoVehiculoCONTROL/RegistrarTipoVehiculoMET", datos, function (dato) {

                    $(document.getElementById('datatable1')).append('<tr  id="idTipo'+dato['idTipo']+'">' +
                            '<td>'+dato['idTipo']+'</td>' +
                            '<td> '+dato['tipo']+'</td>' +
                            '<td   class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idTipo="'+dato['idTipo']+'"' +
                            'descipcion="El Usuario a Modificado un Tipo Vehículo" titulo="Modificar Tipo Vehículo">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td   class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idTipo="'+dato['idTipo']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Tipo Vehículo" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Tipo Vehículo!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');

                }, 'json');
                swal("Registro Agregado!", "Tipo Vehículo ha sido agregado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }else {
                $.post("{$_Parametros.url}modPA/tipoVehiculoCONTROL/ModificarTipoVehiculoMET", datos, function (dato) {

                    $(document.getElementById('idTipo'+dato['idTipo'])).html('<td>'+dato['idTipo']+'</td>' +
                            '<td>'+dato['tipo']+' </td>' +
                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idTipo="'+dato['idTipo']+'"' +
                            'descipcion="El Usuario a Modificado un Tipo Vehículo" titulo="Modificar Tipo Vehículo">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td  class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idTipo="'+dato['idTipo']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Tipo Vehículo" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Tipo Vehículo!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    
                    
                }, 'json');

                swal("Registro Modificado!", "Tipo Vehículo ha sido modificado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
            }



        }
    });


    $(document).ready(function() {
        $("#formAjax").validate();
        $("#ClaseVehiculo").val($("#idClase").val());

    });

</script>