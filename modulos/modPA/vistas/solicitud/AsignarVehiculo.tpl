<link type="text/css"   rel="stylesheet" href="{$_Parametros.ruta_Complementos}theme-default/libs/wizard/wizard.css?1425466601" />





<!-- BEGIN VALIDATION FORM WIZARD -->


<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
    <form action="" id="formAjax"class="form   form-validation" role="form"  >



        <input type="hidden" value="1" name="valido"  id="valido" />
        <div class="form-wizard-nav">
            <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
            <ul class="nav nav-justified">
                <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">GENERAL</span></a></li>

                <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">ASIGNAR VEHÍCULO</span></a></li>


            </ul>
        </div><!--end .form-wizard-nav -->

        <div class="tab-content clearfix">
            <div class="tab-pane active" id="step1">


                                <input type="hidden" value="{if isset($_SolicitudPost[0]['pk_num_solicitud'])}{$_SolicitudPost[0]['pk_num_solicitud']}{/if}"  name="form[int][idPost]"      id="idPost" />



                <input type="hidden" value="{if isset($_Acciones.accion)}{$_Acciones.accion}{/if}"    id="accion" />

                                <div class="col-lg-6">
                                    <div class="col-lg-12">
                                        <div class="form-group">

                                            <div id="fechas" class="input-daterange input-group">
                                                <div class="input-group-content">
                                                    <input class="form-control" type="text" readonly id="desde" name="desde" value="{if isset($_SolicitudPost[0]['fec_requerida'])}{$_SolicitudPost[0]['fec_requerida']}{/if}">
                                                    <label>Fecha a Solicitar</label>
                                                </div>

                                                <span class="input-group-addon">Hasta</span>
                                                <div class="input-group-content">
                                                    <input class="form-control" type="text"  readonly id="hasta"  name="hasta" value="{if isset($_SolicitudPost[0]['fec_hasta'])}{$_SolicitudPost[0]['fec_hasta']}{/if}">
                                                    <div class="form-control-line"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group col-lg-6">
                                            <input type="text" name="form[txt][hora_desde]" id="hora_desde" class="form-control time12-mask" value="{if isset($_SolicitudPost[0]['hora_requerida'])}{$_SolicitudPost[0]['hora_requerida']}{/if}">
                                            <label>Hora de Salida</label>

                                        </div>

                                        <div class="form-group col-lg-6">
                                            <input type="text"name="form[txt][hora_hasta]" id="hora_hasta" class="form-control time12-mask" value="{if isset($_SolicitudPost[0]['hora_hasta'])}{$_SolicitudPost[0]['hora_hasta']}{/if}">
                                            <label>Hora de Llegada</label>
                                        </div>
                                    </div>



                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <select required="" class="form-control" name="form[int][TipoSalida]" id="TipoSalida" aria-required="true">
                                                <option value="">&nbsp;</option>


                                                {foreach item=tipo from=$_TipoSalidaPost}

                                                    {if isset($_SolicitudPost[0]['fk_a006_num_tipo_salida'])}

                                                        {if $tipo.pk_num_miscelaneo_detalle==$_SolicitudPost[0]['fk_a006_num_tipo_salida']}
                                                            <option selected value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                                        {else}
                                                            <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                                    {/if}
                                                {/foreach}




                                            </select>
                                            <label for="rangelength2"> Tipo de Salida </label>
                                        </div>
                                    </div>


                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <select required="" class="form-control" name="form[int][MotivoSalida]" id="MotivoSalida" aria-required="true">
                                                <option value="">&nbsp;</option>

                                                {foreach item=tipo from=$_MotivoSalidaPost}

                                                    {if isset($_SolicitudPost[0]['fk_a006_num_motivo_salida'])}

                                                        {if $tipo.pk_num_miscelaneo_detalle==$_SolicitudPost[0]['fk_a006_num_motivo_salida']}
                                                            <option selected value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                                        {else}
                                                            <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                                    {/if}
                                                {/foreach}



                                            </select>
                                            <label for="rangelength2"> Motivo de Salida </label>
                                        </div>
                                    </div>
                                </div>



                                <div class="col-lg-6">

                                    <div class="form-group">
                                        <textarea required="" placeholder="" rows="3" class="form-control" id="Observacion" name="form[alphaNum][Observacion]">{if isset($_SolicitudPost[0]['ind_observacion'])}{$_SolicitudPost[0]['ind_observacion']}{/if}</textarea>
                                        <label for="textarea1">Observación</label>
                                    </div>

                                </div>




                <div class="col-lg-6">

                    <div class="form-group">
                        <input disabled class="form-control" type="text"  value="{if isset($_SolicitudPost[0]['ind_nombre1'])}{$_SolicitudPost[0]['ind_nombre1']} {$_SolicitudPost[0]['ind_apellido1']}{/if}"/>
                        <label for="textarea1"> Funcionario Solicitante</label>
                    </div>

                </div>

                            </div><!--end #step1 -->

                            <div class="tab-pane" id="step2">

                                <div class="col-lg-6">

                                    <div class="form-group">
                                        <select required="" class="form-control" name="form[int][Vehiculo]" id="Vehiculo"  >
                                            <option value="">&nbsp;</option>


                                            {foreach item=tipo from=$_VehiculoPost}

                                                {if isset($_SolicitudPost[0]['fk_pab001_num_vehiculo_solicitud'])}

                                                    {if $tipo.pk_num_vehiculo==$_SolicitudPost[0]['fk_pab001_num_vehiculo_solicitud']}
                                                        <option selected value="{$tipo.pk_num_vehiculo}">{$tipo.ind_modelo}- {$tipo.ind_placa} </option>
                                                    {else}
                                                        <option value="{$tipo.pk_num_vehiculo}">{$tipo.ind_modelo}- {$tipo.ind_placa}</option>
                                                    {/if}
                                                {else}
                                                    <option value="{$tipo.pk_num_vehiculo}">{$tipo.ind_modelo}- {$tipo.ind_placa} </option>
                                                {/if}
                                            {/foreach}

                                        </select>

                                        <label for="rangelength2"> Vehículo</label>

                                    </div>

                                </div>

                                <div class="col-lg-6">

                                    <div class="form-group">


                                        <input type="text" required="" data-rule-rangelength="[2, 30]" name="Disponibilidad" id="Disponibilidad" class="form-control"  >


                                        <label for="rangelength2">Disponibilidad del Vehículo</label>
                                    </div>
                                </div>

                                <div class="row">
                                    &nbsp;
                                </div>
                                <div class="row">
                                    &nbsp;
                                </div>

                                <div class="row">
                                    &nbsp;
                                </div>

                                <div class="row">
                                    &nbsp;
                                </div>

                                <div class="row">
                                    &nbsp;
                                </div>
                                <div class="row">
                                    &nbsp;
                                </div>
                                <span class="clearfix"></span>

                                <div class="modal-footer">

                                    <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                                    {if ($_Acciones.accion=="asignar")}
                                    <button type="submit" id="boton" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" type="button">Asignar</button>
                                    {else}
                                    <button type="submit" id="boton" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" type="button">Modificar</button>
                                    {/if}

                                </div>
                            </div><!--end #step2 -->
        </div>

        <ul class="pager wizard">

            <li class="previous"><a class="btn-raised"       href="javascript:void(0);">Previo</a></li>

            <li class="next"><a class="btn-raised"           href="javascript:void(0);">Siguiente</a></li>
        </ul>




    </form>
</div>
<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {


            var datos = $("#formAjax" ).serialize();



            var accion = $("#accion").val();
            if(accion=="asignar") {


                $.post("{$_Parametros.url}modPA/solicitudCONTROL/AsignarVehiculoMET",datos, function (dato) {
                     
					 $(document.getElementById('idSolicitud'+dato['idSolicitud'])).html('<td>'+dato['idSolicitud']+'</td>' +
                            '<td>'+dato['Solicitante']+' </td>' +
                            '<td>'+dato['Fechas']+' </td>' +
                            '<td>'+dato['Estatus']+' </td>' +
                            '<td>'+dato['Observacion']+' </td>' +
                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array("PA-01-06-12-01-01-N",$_Parametros.perfil)}<button class="asignar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idAsignar="'+dato['idSolicitud']+'"' +
                            'descipcion="El Usuario ha asignado un vehículo " titulo="Modificar Asignación">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '</td>');


                            $(document.getElementById('idSolicitud'+ dato['idSolicitud'])).html('');

                }, 'json');

                swal("Vehículo Asignado!", "La solicitud del vehículo ha sido aprobada.", "success");

                $(document.getElementById('cerrarModal')).click();

                $(document.getElementById('ContenidoModal')).html('');


            }


        }
    });



    $(document).ready(function() {

        $("#formAjax").validate();
        $('#fechas').datepicker({ format: 'dd/mm/yyyy', language:'es', startDate:'0d' });
        if($('#Vehiculo').val()!="") {
            $.post("{$_Parametros.url}modPA/solicitudCONTROL/DisponibilidadVehiculoMET", {
                Vehiculo: $('#Vehiculo').val(),
                desde: $('#desde').val(),
                hasta: $('#hasta').val(),
                hora_desde: $('#hora_desde').val(),
                hora_hasta: $('#hora_hasta').val(),
                idPost: $('#idPost').val()
            }, function (data) {
                var json = data,
                        obj = JSON.parse(json);

                $("#Disponibilidad").val(obj);
            });
        }else{
            $("#Disponibilidad").val(" ");
        }





        $("#Vehiculo").change(function () {

            $("#Vehiculo option:selected").each(function () {

                $.post("{$_Parametros.url}modPA/solicitudCONTROL/DisponibilidadVehiculoMET", { Vehiculo: $('#Vehiculo').val(),desde: $('#desde').val(),hasta: $('#hasta').val(),hora_desde: $('#hora_desde').val(),hora_hasta: $('#hora_hasta').val(),idPost: $('#idPost').val() }, function(data){
                    var json = data,
                         obj = JSON.parse(json);

                    $("#Disponibilidad").val(obj);

                    if(obj=="No Disponible"){
                        $("#Vehiculo").val(" ");
                    }
                    if($("#Vehiculo").val()==""){
                        $("#Disponibilidad").val("No Disponible");
                    }

                });
            });
        });



    });
</script>