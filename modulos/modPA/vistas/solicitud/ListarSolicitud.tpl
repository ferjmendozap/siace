<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div id="body1">
    <div class="card">
        <div class="card-head">
            <header> <h2 class="text-primary">LISTAR SOLICITUDES DE SALIDA DE VEHÍCULOS </h2></header>
        </div>
        <div class="col-md-12">
            <h5>Listado de las solicitudes de salida de vehículos.</h5>
        </div><!--end .col -->

    </div><!--end .card -->

<section class="style-default-bright">

    <div class="row">

        <div class="col-lg-12">

            <div class="table-responsive">

                <div class="btn-group pull-right ">
                <a data-toggle="offcanvas" title="Filtro" class="  btn ink-reaction btn-floating-action btn-sm btn-primary" href="#offcanvas-filtro"> <i class="fa fa-filter"></i> </a>
                </div><br>
                <br/>
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="sort-alpha col-sm-1">N° </th>
                        <th class="sort-alpha col-sm-2">Solicitante</th>
                        <th class=" col-sm-3">Fechas Solicitadas</th>
                        <th class="sort-alpha col-sm-1">Estatus</th>
                        <th class="sort-alpha col-sm-3">Observacion</th>
                        {if in_array('PA-01-01-01-07-V',$_Parametros.perfil)}

                        <th class=" col-sm-1">  Ver </th>
                        {/if}
                        {if in_array('PA-01-01-01-01-M',$_Parametros.perfil)} <th class=" col-sm-1">  Editar </th> {/if}
                        {if in_array('PA-01-01-01-03-R',$_Parametros.perfil)} <th class=" col-sm-1">  Rechazar </th> {/if}
                        {if in_array('PA-01-01-01-04-E',$_Parametros.perfil)} <th class="  col-sm-1"> Eliminar</th> {/if}
                    </tr>
                    </thead>
                    <tbody>

                    {foreach item=post from=$_SolicitudPost}
                        <tr id="idSolicitud{$post.pk_num_solicitud}" class="gradeA">

                            <td>{$post.pk_num_solicitud}</td>
                            <td>{$post.ind_nombre1} {$post.ind_apellido1}</td>
                            <td>{$post.fec_requerida} {$post.hora_requerida} - {$post.fec_hasta} {$post.hora_hasta}</td>
                            <td>{$post.ind_nombre_detalle}</td>
                            <td>{$post.ind_observacion}</td>

                                {if in_array('PA-01-01-01-07-V',$_Parametros.perfil)}
                                <td>
                                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Ver Solicitud" descipcion="--" idSolicitud="{$post.pk_num_solicitud}" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                            <i class="fa fa-eye" style="color: #ffffff;"></i>
                                        </button>
                                </td>
                                {/if}



                                {if in_array('PA-01-01-01-01-M',$_Parametros.perfil)}
                                <td>
                                {if  ($post.fk_a006_num_estado_solicitud=='1')}
                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Modificar Solicitud" descipcion="--" idSolicitud="{$post.pk_num_solicitud}" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                </button>
                                {/if}
                                </td>
                                {/if}

                                {if in_array('PA-01-01-01-03-R',$_Parametros.perfil)}
                                <td>
                                {if  ($post.fk_a006_num_estado_solicitud<'4')}
                                <button class="rechazar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Rechazar Solicitud" descipcion="--" idSolicitud="{$post.pk_num_solicitud}" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                    <i class="md md-clear" style="color: #ffffff;"></i>
                                </button>
                                {/if}
                                </td>
                                {/if}


                                {if in_array('PA-01-01-01-04-E',$_Parametros.perfil)}
                                <td>
                                {if  ($post.fk_a006_num_estado_solicitud=='1') || ($post.fk_a006_num_estado_solicitud=='8') }
                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idSolicitud="{$post.pk_num_solicitud}" mensaje="Estas seguro que desea eliminar la el post!!" titulo="Estas Seguro?" descipcion="El usuario a eliminado un post" boton="si, Eliminar" idmenu="7">
                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                </button>
                                {/if}
                                </td>
                                {/if}

                        </tr>








                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="8">
                            {if in_array('PA-01-01-01-06-N',$_Parametros.perfil)}
                            <button class="logsUsuario btn ink-reaction btn-raised  btn-info " data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                    descripcion="Registro Solicitud de Salida"  titulo="Registrar Solicitud de Salida" id="nuevo" >
                                <i class="md md-create"></i> Nueva Solicitud de Salida &nbsp;&nbsp;
                            </button>
                            {/if}
                        </th>

                    </tr>
                    </tfoot>
                </table>

            </div><!--end .table-responsive -->
        </div><!--end .col -->
    </div><!--end .row -->
    <!-- fin listado -->




    </div><!--end .row -->

    <!-- filtro -->
    <div class="offcanvas">
        <div id="offcanvas-filtro" class="offcanvas-pane width-9">
            <div class="offcanvas-head">
                <header>Filtro</header>
                <div class="offcanvas-tools">
                    <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                        <i class="md md-close"></i>
                    </a>
                </div>
            </div>

            <div class="offcanvas-body">

                    <input type="hidden" name="filtro" value="1">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group ">
                                <select id="filtro_estatus" name="form[int][Estatus]" class="form-control">
                                    <option value="">&nbsp;</option>

                                    {foreach item=app from=$_EstatusPost}

                                        {if isset($_EstatusSelect)}

                                            {if $app.cod_detalle==$_EstatusSelect}
                                                <option selected value="{$app.cod_detalle}">{$app.ind_nombre_detalle} </option>
                                            {else}
                                                <option value="{$app.cod_detalle}">{$app.ind_nombre_detalle} </option>
                                            {/if}

                                        {else}
                                            <option value="{$app.cod_detalle}">{$app.ind_nombre_detalle} </option>
                                        {/if}
                                    {/foreach}

                                </select>
                                <label for="rangelength2">  Estatus de la Solicitud</label>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <select  class="form-control" name="form[int][MotivoSalida]" id="filtro_motivo" aria-required="true">
                                    <option value="">&nbsp;</option>

                                    {foreach item=tipo from=$_MotivoSalidaPost}

                                        {if isset($_MotivoSalidaSelect)}

                                            {if $tipo.pk_num_miscelaneo_detalle==$_MotivoSalidaSelect}
                                                <option selected value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                            {else}
                                                <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                            {/if}

                                        {else}
                                            <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                        {/if}
                                    {/foreach}



                                </select>
                                <label for="rangelength2"> Motivo de Salida </label>
                            </div>
                        </div>


                        <div class="col-lg-12">
                            <div class="form-group">
                                <select  class="form-control" name="form[int][TipoSalida]" id="filtro_tipo" aria-required="true">
                                    <option value="">&nbsp;</option>


                                    {foreach item=tipo from=$_TipoSalidaPost}

                                        {if isset($_TipoSalidaSelect)}

                                            {if $tipo.pk_num_miscelaneo_detalle==$_TipoSalidaSelect}
                                                <option selected value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                            {else}
                                                <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                            {/if}
                                        {else}
                                            <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                        {/if}
                                    {/foreach}




                                </select>
                                <label for="rangelength2"> Tipo de Salida </label>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <button type="button"  onclick="Filtro()" class="btn btn-xs ink-reaction btn-raised btn-info" id="botonFiltro">
                                Filtrar
                            </button>
                        </div>
                    </div>

            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(document).ready(function() {
            var $url='{$_Parametros.url}modPA/solicitudCONTROL/RegistrarSolicitudMET';
            $('#nuevo').click(function(){
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idMenu:0 },function($dato){
                    $('#ContenidoModal').html($dato);
                });

            });


            $('#modalAncho').css( "width", "85%" );

            $('#datatable1 tbody').on( 'click', '.ver', function () {

                var $url='{$_Parametros.url}modPA/solicitudCONTROL/VerSolicitudMET';

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idSolicitud')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });

            $('#datatable1 tbody').on( 'click', '.modificar', function () {
                var $url='{$_Parametros.url}modPA/solicitudCONTROL/ModificarSolicitudMET';

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idSolicitud')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });

            $('#datatable1 tbody').on( 'click', '.verificar', function () {
                var $url='{$_Parametros.url}modPA/solicitudCONTROL/DisponibilidadSolicitudMET';

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idSolicitud')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });

            $('#datatable1 tbody').on( 'click', '.rechazar', function () {
                var $url='{$_Parametros.url}modPA/solicitudCONTROL/RechazarSolicitudMET';

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idSolicitud')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });


            $('#datatable1 tbody').on( 'click', '.eliminar', function () {


                var $url='{$_Parametros.url}modPA/solicitudCONTROL/EliminarSolicitudMET';
                var idPost=$(this).attr('idSolicitud');
                var idMenu=$(this).attr('idMenu');
                swal({
                    title:$(this).attr('titulo'),
                    text: $(this).attr('mensaje'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false
                }, function(){

                    var $url='{$_Parametros.url}modPA/solicitudCONTROL/EliminarSolicitudMET';


                    $.post($url,{ idPost: idPost},function($dato){
                        $(document.getElementById('idSolicitud'+idPost)).html('');
                        swal("Eliminado!", "el menu fue eliminado satisfactoriamente.", "success");
                    });

                    ;
                });
            });







        });


        // Buscador
        function Filtro()
        {

            var filtro_motivo = $("#filtro_motivo").val();
            var filtro_tipo = $("#filtro_tipo").val();
            var filtro_estatus = $("#filtro_estatus").val();
            var url_listar='{$_Parametros.url}modPA/solicitudCONTROL/FiltroProyectoMET';
            $.post(url_listar,{ filtro_motivo: filtro_motivo, filtro_tipo: filtro_tipo, filtro_estatus: filtro_estatus  },function(respuesta_post) {


                var tabla_listado = $('#datatable1').DataTable();
                tabla_listado.clear().draw();

                if(respuesta_post != -1) {
                    for(var i=0; i<respuesta_post.length; i++) {



                        if(respuesta_post[i].fk_a006_num_estado_solicitud==1){

                            var botonModificar ='<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Modificar Solicitud" descipcion="--" idSolicitud="'+respuesta_post[i].pk_num_solicitud+'" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal"><i class="fa fa-edit" style="color: #ffffff;"></i>  </button>';

                        }else{

                            var botonModificar ='';
                        }

                        if(respuesta_post[i].fk_a006_num_estado_solicitud<4){

                            var botonRechazar ='<button class="rechazar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Rechazar Solicitud" descipcion="--" idSolicitud="'+respuesta_post[i].pk_num_solicitud+'" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal" ><i class="md md-delete" style="color: #ffffff;"></i></button>';

                        }else{

                            var botonRechazar ='';
                        }

                        if(respuesta_post[i].fk_a006_num_estado_solicitud==1 ||respuesta_post[i].fk_a006_num_estado_solicitud==8 ){

                            var botonAnular ='<button class="anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idAnular="'+respuesta_post[i].pk_num_solicitud+'" mensaje="¿Estás seguro que desea anular el proyecto de contrato y las obligaciones relacionadas al proyecto?" title="¿Estás Seguro que desea anular?" titulo="¿Estás Seguro que desea anular?" descipcion="El usuario anuló un proyecto de contrato" boton="si, Eliminar" ><i class="md md-delete" style="color: #ffffff;"></i></button>';

                        }else{

                            var botonAnular ='';
                        }

                        var botonVer ='<button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"  title="Ver Proyecto de Contrato" titulo="Ver Proyecto de Contrato"   idVer="'+respuesta_post[i].pk_num_solicitud+'"  data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal"><i class="md md-remove-red-eye" style="color: #ffffff;"></i></button>';
                        

                        tabla_listado.row.add([
                            respuesta_post[i].pk_num_solicitud,
                            respuesta_post[i].ind_nombre1,
                            respuesta_post[i].fec_requerida+" "+respuesta_post[i].fec_hasta,
                            respuesta_post[i].ind_nombre_detalle,
                            respuesta_post[i].ind_observacion,
                            botonVer,
                            botonModificar,
                            botonRechazar,
                            botonAnular
                        ]).draw()
                            .nodes()
                            .to$()
                    }
                }


            },'json');

        }



    </script>
    </div>


    </div><!--end .row -->


    </div>