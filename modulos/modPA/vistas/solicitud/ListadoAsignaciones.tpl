<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div id="body1">
    <div class="card">
        <div class="card-head">
            <header> <h2 class="text-primary">LISTAR   SOLICITUDES ASIGNADAS </h2></header>
        </div>
        <div class="col-md-12">
            <h5>Listado de las solicitudes con vehículo ya asignado, con el registro de salida por realizar.</h5>
        </div><!--end .col -->

    </div><!--end .card -->


    <div class="form-group   col-lg-12 ">
        <div class="col-lg-11 ">

         </div>	<a class="btn btn-flat btn-accent ink-reaction span8" href="javascript:void(0);">filtrar</a>

    </div>

</div><!--end .card -->

<section class="style-default-bright">




    <!-- Listado -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-actionbar">
                <div class="card-actionbar-row">

                </div>
            </div>
        </div><!--end .col -->
    </div><!--end .row -->
    <div class="row">

        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="sort-alpha col-sm-1">N° </th>
                        <th class="sort-alpha col-sm-2">Solicitante</th>
                        <th class="sort-alpha col-sm-3">Fechas Solicitadas</th>
                        <th class="sort-alpha col-sm-1">Estatus</th>
                        <th class="sort-alpha col-sm-3">Observacion</th>

                        <th class="  col-sm-1"> Modificar Asignación </th>





                    </tr>
                    </thead>
                    <tbody>

                    {foreach item=post from=$_SolicitudPost}
                        <tr id="idSolicitud{$post.pk_num_solicitud}" class="gradeA">
                            <td>{$post.pk_num_solicitud}</td>
                            <td>{$post.ind_nombre1} {$post.ind_apellido1}</td>
                            <td>{$post.fec_requerida} {$post.hora_requerida}- {$post.fec_hasta} {$post.hora_hasta}</td>

                            <td>{$post.ind_nombre_detalle}</td>
                            <td>{$post.ind_observacion}</td>



                            <td>{if in_array('PA-01-01-06-01-M',$_Parametros.perfil)}
                                {if  ($post.fk_a006_num_estado_solicitud=='5'||$post.fk_a006_num_estado_solicitud=='6')}
                                    <button class="asignar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Modificar Asignación" descipcion="--" idAsignar="{$post.pk_num_solicitud}" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                        <i class="md md-directions-car" style="color: #ffffff;"></i>
                                    </button>
                                {/if}
                                {/if}
                            </td>




                        </tr>








                    {/foreach}
                    </tbody>
                </table>
            </div><!--end .table-responsive -->
        </div><!--end .col -->
    </div><!--end .row -->
    <!-- fin listado -->




    </div><!--end .row -->




    <script type="text/javascript">
        $(document).ready(function() {

            $('#modalAncho').css( "width", "85%" );




            $('#datatable1 tbody').on( 'click', '.asignar', function () {
                var $url='{$_Parametros.url}modPA/solicitudCONTROL/AsignarVehiculoMET';

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idSolicitud: $(this).attr('idAsignar')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });

        });
    </script>
    </div>




