<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div id="body1">
    <div class="card">
        <div class="card-head">
            <header> <h2 class="text-primary">LISTAR SOLICITUDES  POR APROBAR</h2></header>
        </div>
        <div class="col-md-12">
            <h5>Listado de las solicitudes de salida de vehículos por aprobar.</h5>
        </div><!--end .col -->

    </div><!--end .card -->


    <div class="form-group   col-lg-12 ">

        <div class="col-lg-11 "></div>	<a class="btn btn-flat btn-accent ink-reaction span8" href="javascript:void(0);">filtrar</a>
    </div>

</div><!--end .card -->

<section class="style-default-bright">




    <!-- Listado -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-actionbar">
                <div class="card-actionbar-row">

                </div>
            </div>
        </div><!--end .col -->
    </div><!--end .row -->
    <div class="row">

        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="sort-alpha col-sm-1">N° </th>
                        <th class="sort-alpha col-sm-2">Solicitante</th>
                        <th class="sort-alpha col-sm-3">Fechas Solicitadas</th>
                        <th class="sort-alpha col-sm-1">Estatus</th>
                        <th class="sort-alpha col-sm-3">Observacion</th>
                        <th class="sort-alpha col-sm-3">Conformado por</th>

                        {if in_array('PA-01-01-04-01-A',$_Parametros.perfil)}  <th class="  col-sm-1"> Aprobar</th> {/if}
                        <th class=" col-sm-1">  Rechazar </th>




                    </tr>
                    </thead>
                    <tbody>

                    {foreach item=post from=$_SolicitudPost}
                        <tr id="idSolicitud{$post.pk_num_solicitud}" class="gradeA">
                            <td> {$post.pk_num_solicitud}</td>
                            <td> {$post.ind_nombre1} {$post.ind_apellido1}</td>
                            <td>{$post.fec_requerida} {$post.hora_requerida}- {$post.fec_hasta} {$post.hora_hasta}</td>
                            <td>{$post.ind_nombre_detalle}</td>
                            <td>{$post.ind_observacion}</td>
                            <td> {$post.nombre_conformado} {$post.apellido_conformado} </td>



                                {if in_array('PA-01-01-04-01-A',$_Parametros.perfil)}
                                <td>
                                {if  ($post.fk_a006_num_estado_solicitud=='3')}
                                    <button class="aprobar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Aprobar Solicitud" descipcion="--" idAprobar="{$post.pk_num_solicitud}" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                        <i class="md md-done" style="color: #ffffff;"></i>
                                    </button>
                                {/if}
                                </td>
                                {/if}


                            <td>{if in_array('PA-01-01-01-03-R',$_Parametros.perfil)}
                                {if  ($post.fk_a006_num_estado_solicitud<'4')}
                                <button class="rechazar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Rechazar Solicitud" descipcion="--" idRechazar="{$post.pk_num_solicitud}" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                    <i class="md md-clear" style="color: #ffffff;"></i>
                                </button>
                                {/if}
                                {/if}
                            </td>



                        </tr>

                    {/foreach}
                    </tbody>
                </table>
            </div><!--end .table-responsive -->
        </div><!--end .col -->
    </div><!--end .row -->
    <!-- fin listado -->




    </div><!--end .row -->




    <script type="text/javascript">
        $(document).ready(function() {

            $('#modalAncho').css( "width", "85%" );




            $('#datatable1 tbody').on( 'click', '.aprobar', function () {
                var $url='{$_Parametros.url}modPA/solicitudCONTROL/AprobarSolicitudMET';

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idAprobar')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });

            $('#datatable1 tbody').on( 'click', '.rechazar', function () {
                var $url='{$_Parametros.url}modPA/solicitudCONTROL/RechazarSolicitudMET';

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idRechazar')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });









        });
    </script>
    </div>



