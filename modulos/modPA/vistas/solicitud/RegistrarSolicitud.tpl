<form action="" id="formAjax" class="form" role="form" method="post">
                        <div class="modal-body">
                        <input type="hidden" value="1" name="valido"  id="valido" />

                                 <input type="hidden" value="{if isset($_SolicitudPost[0]['pk_num_solicitud'])}{$_SolicitudPost[0]['pk_num_solicitud']}{/if}"  name="form[int][idPost]"      id="idPost" />


                                    <input type="hidden" value="{if isset($_Acciones.accion)}{$_Acciones.accion}{/if}"    id="accion" />


                                <div class="col-lg-6">
                                    <div class="col-lg-12">
                                        <div class="form-group">

                                        <div id="fechas" class="input-daterange input-group" >
                                            <div class="input-group-content">
                                                <input required="" class="form-control" readonly type="text" id="desde" name="desde" value="{if isset($_SolicitudPost[0]['fec_requerida'])}{$_SolicitudPost[0]['fec_requerida']}{/if}">
                                                <label>Fecha a Solicitar</label>
                                            </div>

                                            <span class="input-group-addon">Hasta</span>
                                            <div class="input-group-content">
                                                <input required="" class="form-control" type="text" readonly id="hasta"  name="hasta" value="{if isset($_SolicitudPost[0]['fec_hasta'])}{$_SolicitudPost[0]['fec_hasta']}{/if}">
                                                <div class="form-control-line"></div>
                                            </div>
                                        </div>
                                        </div>
                                     </div>

                                    <div class="col-lg-12">
                                        <div class="form-group col-lg-6">
                                            <input required="" type="text" name="form[txt][hora_desde]" class="form-control time12-mask" value="{if isset($_SolicitudPost[0]['hora_requerida'])}{$_SolicitudPost[0]['hora_requerida']}{/if}">
                                            <label>Hora de Salida</label>

                                        </div>

                                        <div class="form-group col-lg-6">
                                            <input required=""  type="text"name="form[txt][hora_hasta]"  class="form-control time12-mask" value="{if isset($_SolicitudPost[0]['hora_hasta'])}{$_SolicitudPost[0]['hora_hasta']}{/if}">
                                            <label>Hora de Llegada</label>
                                        </div>
                                    </div>



                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <select required="" class="form-control" name="form[int][TipoSalida]" id="TipoSalida" aria-required="true">
                                                <option value="">&nbsp;</option>


                                                {foreach item=tipo from=$_TipoSalidaPost}

                                                    {if isset($_SolicitudPost[0]['fk_a006_num_tipo_salida'])}

                                                        {if $tipo.pk_num_miscelaneo_detalle==$_SolicitudPost[0]['fk_a006_num_tipo_salida']}
                                                            <option selected value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                                        {else}
                                                            <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                                    {/if}
                                                {/foreach}




                                            </select>
                                            <label for="rangelength2"> Tipo de Salida </label>
                                        </div>
                                    </div>


                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <select required="" class="form-control" name="form[int][MotivoSalida]" id="MotivoSalida" aria-required="true">
                                                <option value="">&nbsp;</option>

                                                {foreach item=tipo from=$_MotivoSalidaPost}

                                                    {if isset($_SolicitudPost[0]['fk_a006_num_motivo_salida'])}

                                                        {if $tipo.pk_num_miscelaneo_detalle==$_SolicitudPost[0]['fk_a006_num_motivo_salida']}
                                                            <option selected value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                                        {else}
                                                            <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle} </option>
                                                    {/if}
                                                {/foreach}



                                            </select>
                                            <label for="rangelength2"> Motivo de Salida </label>
                                        </div>
                                    </div>
                                </div>



                                    <div class="col-lg-6">

                                        <div class="form-group">
                                            <textarea required="" placeholder="" rows="3" class="form-control" id="Observacion" name="form[txt][Observacion]">{if isset($_SolicitudPost[0]['ind_observacion'])}{$_SolicitudPost[0]['ind_observacion']}{/if}</textarea>
                                            <label for="textarea1">Observación</label>
                                        </div>

                                    </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <input disabled class="form-control" type="text"  value="{if isset($_SolicitudPost[0]['ind_nombre1'])}{$_SolicitudPost[0]['ind_nombre1']} {$_SolicitudPost[0]['ind_apellido1']}{/if}"/>
                                    <label for="textarea1"> Funcionario Solicitante</label>
                                </div>

                            </div>

                            {if isset($_SolicitudPost[0]['fk_a006_num_estado_solicitud']) && $_SolicitudPost[0]['fk_a006_num_estado_solicitud']>='3'}
                                <div class="col-lg-6">

                                    <div class="form-group">
                                        <input disabled class="form-control" type="text"  value="{if isset($_SolicitudPost[0]['ind_nombre_conformado'])}{$_SolicitudPost[0]['ind_nombre_conformado']} {$_SolicitudPost[0]['ind_apellido_conformado']}{/if}"/>
                                        <label for="textarea1"> Conformado Por</label>
                                    </div>

                                </div>

                            {/if}

                            {if isset($_SolicitudPost[0]['fk_a006_num_estado_solicitud']) && $_SolicitudPost[0]['fk_a006_num_estado_solicitud']>='4'}
                                <div class="col-lg-6">

                                    <div class="form-group">
                                        <input disabled class="form-control" type="text"  value="{if isset($_SolicitudPost[0]['ind_nombre_aprobado'])}{$_SolicitudPost[0]['ind_nombre_aprobado']} {$_SolicitudPost[0]['ind_apellido_aprobado']}{/if}"/>
                                        <label for="textarea1"> Aprobado Por</label>
                                    </div>

                                </div>

                            {/if}

                            {if isset($_SolicitudPost[0]['fk_a006_num_estado_solicitud']) && $_SolicitudPost[0]['fk_a006_num_estado_solicitud']=='8'}
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <textarea disabled placeholder="" rows="3" class="form-control" id="Observacion" name="form[txt][Observacion]">{$_SolicitudPost[0]['ind_motivo_rechazo']}</textarea>
                                    <label for="textarea1"> Motivo del Rechazo</label>
                                </div>

                            </div>

                            {/if}







    </div>

    <div class="row">
        &nbsp;
    </div>
    <div class="row">
        &nbsp;
    </div>
    <span class="clearfix"></span>




    <div class="modal-footer">

        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        {if  ($_Acciones.accion=="modificar")}
            <button type="submit" id="boton" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" type="button">Modificar</button>
        {/if}
        {if ($_Acciones.accion=="registrar")}

            <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
        {/if}

    </div>


                    </form>






<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $("#formAjax" ).serialize();


            var accion = $("#accion").val();
            if(accion=="registrar") {
                $.post("{$_Parametros.url}modPA/solicitudCONTROL/RegistrarSolicitudMET", $("#formAjax").serialize(), function (dato) {

                    $(document.getElementById('datatable1')).append('<tr  id="idSolicitud'+dato['idSolicitud']+'">' +
                            '<td>'+dato['idSolicitud']+'</td>' +
                            '<td>  '+dato['nombre']+' </td>' +
                            '<td> '+dato['fechas']+'</td>' +
                            '<td> '+dato['estatus']+'</td>' +
                            '<td> '+dato['observacion']+'</td>' +
                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="ver  btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idSolicitud="'+dato['idSolicitud']+'"' +
                            ' titulo="Ver Solicitud">' +
                            '<i class="fa fa-eye" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idSolicitud="'+dato['idSolicitud']+'"' +
                            'descipcion="El Usuario a Modificado un Solicitud" titulo="Modificar Solicitud">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="rechazar   btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idSolicitud="'+dato['idSolicitud']+'"' +
                            ' titulo="Ver Solicitud">' +
                            '<i class="md md-clear" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td  class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idSolicitud="'+dato['idSolicitud']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Solicitud" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Solicitud!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');

                }, 'json');
                swal("Registro Agregado!", "La Solicitud ha sido agregado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }else if (accion=="modificar"){
                $.post("{$_Parametros.url}modPA/solicitudCONTROL/ModificarSolicitudMET", $("#formAjax").serialize(), function (dato) {

                    $(document.getElementById('idSolicitud'+dato['idSolicitud'])).html('<td>'+dato['idSolicitud']+'</td>' +

                            '<td>  '+dato['nombre']+' </td>' +
                            '<td> '+dato['fechas']+'</td>' +
                            '<td> '+dato['estatus']+'</td>' +
                            '<td> '+dato['observacion']+'</td>' +
                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="ver  btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idSolicitud="'+dato['idSolicitud']+'"' +
                            ' titulo="Ver Solicitud">' +
                            '<i class="fa fa-eye" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idSolicitud="'+dato['idSolicitud']+'"' +
                            'descipcion="El Usuario a Modificado un Solicitud" titulo="Modificar Solicitud">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '<td  class="sort-alpha col-sm-1" >' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<button class="rechazar   btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idSolicitud="'+dato['idSolicitud']+'"' +
                            ' titulo="Ver Solicitud">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('PA-01-06-12-01-01-N',$_Parametros.perfil)}<td  class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idSolicitud="'+dato['idSolicitud']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Solicitud" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Solicitud!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');



                }, 'json');
                swal("Registro Modificado!", "La Solicitud ha sido modificado satisfactoriamente.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //
            }else {
                $.post("{$_Parametros.url}modPA/solicitudCONTROL/VerSolicitudMET", $("#formAjax").serialize(), function (dato) {


                }, 'json');

            }


        }
    });


    $(document).ready(function() {

        $("#formAjax").validate();
        $('#fechas').datepicker({ format: 'dd/mm/yyyy', language:'es'});

    });





</script>