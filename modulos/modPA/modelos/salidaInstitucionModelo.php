<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class salidaInstitucionModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener("idUsuario");
    }

    #metodo para obtener todos los registros guardados de los post
    public function metGetSalidaInstitucion()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT pa_e003_entrada_vehiculo.*,
             pa_e001_salida_vehiculo.ind_observacion,

             pa_e001_salida_vehiculo.fk_pad001_num_solicitud_vehiculo as pk_num_solicitud,

             pk_num_salida_vehiculo,
             pa_b001_vehiculo.ind_url_foto_frontal,
             pa_e001_salida_vehiculo.fk_pab001_num_vehiculo,
             pa_b001_vehiculo.ind_modelo ,
             pa_b001_vehiculo.ind_placa,
             DATE_FORMAT(pa_e001_salida_vehiculo.fec_hora_salida,'%d/%m/%Y') as fec_hora_salida,
             DATE_FORMAT(pa_e001_salida_vehiculo.fec_hora_salida,'%h:%i %p') as hora_salida


             FROM pa_e003_entrada_vehiculo



             RIGHT JOIN pa_e001_salida_vehiculo on (pa_e001_salida_vehiculo.pk_num_salida_vehiculo= pa_e003_entrada_vehiculo.fk_pae001_num_salida_vehiculo )
             left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e001_salida_vehiculo.fk_pab001_num_vehiculo)
             WHERE  pa_e003_entrada_vehiculo.pk_num_entrada IS null
            "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }


    public function metGetFkSolicitud($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT pa_e001_salida_vehiculo.fk_pad001_num_solicitud_vehiculo



             FROM pa_e001_salida_vehiculo


             WHERE  pa_e001_salida_vehiculo.pk_num_salida_vehiculo='".$id."'
            "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }
    #metodo para guardar el registro del post
    public function metSetSalidaInstitucion($hora_salida,$hora_entrada,$kilometraje,$observacion,$solicitud,$chofer,$vehiculo,$estatus,$accesorios)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoPost=$this->_db->prepare(
            "insert into  pa_e001_salida_vehiculo (fec_hora_salida, fec_hora_hasta,ind_kilometraje,ind_observacion,fk_a018_num_seguridad_usuario,fec_ultima_modificacion,fk_pad001_num_solicitud_vehiculo,fk_pab012_num_chofer,fk_pab001_num_vehiculo) values
                                                  (:fec_hora_salida, :fec_hora_hasta,:ind_kilometraje,:ind_observacion,:fk_a018_num_seguridad_usuario,:fec_ultima_modificacion,:fk_pad001_num_solicitud_vehiculo,:fk_pab012_num_chofer,:fk_pab001_num_vehiculo)"
        );

        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(
            ':fec_hora_salida' => $hora_salida,
            ':fec_hora_hasta' => $hora_entrada,
            ':ind_kilometraje' => $kilometraje,
            ':ind_observacion' => $observacion,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
            ':fk_pad001_num_solicitud_vehiculo' =>$solicitud,
            ':fk_pab012_num_chofer' => $chofer,
            ':fk_pab001_num_vehiculo' => $vehiculo,
        ));
        $idPost= $this->_db->lastInsertId();


        if($accesorios) {
            $Accesorios = $this->_db->prepare(
                "insert into pa_c002_accesorio_salida  (fk_pab011_num_accesorio, fk_pae001_num_salida_vehiculo)values (:fk_pab011_num_accesorio, :fk_pae001_num_salida_vehiculo)"
            );
            for ($i = 0; $i < count($accesorios); $i++) {

                $Accesorios->execute(array(
                    ':fk_pab011_num_accesorio' => $accesorios[$i],
                    ':fk_pae001_num_salida_vehiculo' => $idPost
                ));
            }
        }





        #commit — Consigna una transacción
        $this->_db->commit();


        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "update `pa_d001_solicitud_vehiculo` set fk_a006_num_estado_solicitud = '$estatus' " .
            "where  pk_num_solicitud = '$solicitud'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();



        return $idPost;
    }

    #metodo para obtener el pk_num_salida de su fk_solicitud
    public function metGetPkSalida($idSolicitud)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT
          pa_d001_solicitud_vehiculo.pk_num_solicitud,
          DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_requerida,'%d/%m/%Y') as fec_requerida,
          DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_hasta,'%d/%m/%Y') as fec_hasta,

          DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_requerida,'%h:%i %p') as hora_requerida,
          DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_hasta,'%h:%i %p') as hora_hasta,

          DATE_FORMAT(pa_e001_salida_vehiculo.fec_hora_salida,'%d/%m/%Y') as fec_hora_salida,
          DATE_FORMAT(pa_e001_salida_vehiculo.fec_hora_hasta,'%d/%m/%Y') as fec_hora_hasta,

          DATE_FORMAT(pa_e001_salida_vehiculo.fec_hora_salida,'%h:%i %p') as hora_salida,
          DATE_FORMAT(pa_e001_salida_vehiculo.fec_hora_hasta,'%h:%i %p') as hora_hasta2,

          pa_d001_solicitud_vehiculo.fk_rhb001_empleado_solicitante ,
          pa_d001_solicitud_vehiculo.fk_rhb001_empleado_conformado,
          pa_d001_solicitud_vehiculo.fec_aprobado,
          pa_d001_solicitud_vehiculo.fk_a006_num_estado_solicitud,
          pa_d001_solicitud_vehiculo.ind_observacion,
          pa_d001_solicitud_vehiculo.ind_motivo_rechazo,
          pa_d001_solicitud_vehiculo.fk_a018_num_seguridad_usuario,
          pa_d001_solicitud_vehiculo.fec_ultima_modificacion,
          pa_d001_solicitud_vehiculo.fk_a006_num_motivo_salida,
          pa_d001_solicitud_vehiculo.fk_a006_num_tipo_salida,
          pa_b001_vehiculo.ind_url_foto_frontal,

          pa_b001_vehiculo.ind_modelo ,
             pa_b001_vehiculo.ind_placa,
             pa_e001_salida_vehiculo.fk_pab001_num_vehiculo,
          pa_e001_salida_vehiculo.ind_kilometraje,
          pa_e001_salida_vehiculo.fk_pab001_num_vehiculo,
          pa_e001_salida_vehiculo.fk_pab012_num_chofer,
          pa_e001_salida_vehiculo.pk_num_salida_vehiculo,
          pa_e001_salida_vehiculo.ind_observacion as observacionSalida
            FROM `pa_e001_salida_vehiculo`

             left join pa_d001_solicitud_vehiculo on (pa_d001_solicitud_vehiculo.pk_num_solicitud=pa_e001_salida_vehiculo.fk_pad001_num_solicitud_vehiculo)
             left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e001_salida_vehiculo.fk_pab001_num_vehiculo)
            where pa_e001_salida_vehiculo.fk_pad001_num_solicitud_vehiculo='".$idSolicitud."'
     "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para modificar el registro del post
    public function metUpdateSalidaInstitucion($hora_salida,$hora_entrada,$kilometraje,$observacion,$solicitud,$chofer,$vehiculo,$estatus,$accesorios,$idSalida)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia

        $modificar=$this->_db->prepare(
            "UPDATE pa_e001_salida_vehiculo
              SET
              fec_hora_salida=:fec_hora_salida,
              fec_hora_hasta =:fec_hora_hasta,
              ind_kilometraje=:ind_kilometraje,
              ind_observacion=:ind_observacion,
              fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
              fec_ultima_modificacion=:fec_ultima_modificacion,
              fk_pad001_num_solicitud_vehiculo=:fk_pad001_num_solicitud_vehiculo,
              fk_pab012_num_chofer=:fk_pab012_num_chofer,
              fk_pab001_num_vehiculo=:fk_pab001_num_vehiculo


              WHERE pk_num_salida_vehiculo='".$idSalida."'"
        );
        $modificar->execute(array(
            ':fec_hora_salida' => $hora_salida,
            ':fec_hora_hasta' => $hora_entrada,
            ':ind_kilometraje' => $kilometraje,
            ':ind_observacion' => $observacion,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
            ':fk_pad001_num_solicitud_vehiculo' =>$solicitud,
            ':fk_pab012_num_chofer' => $chofer,
            ':fk_pab001_num_vehiculo' => $vehiculo,
        ));




        $eliminarAccesorios=$this->_db->prepare(
            "DELETE FROM pa_c002_accesorio_salida  WHERE  fk_pae001_num_salida_vehiculo=:fk_pae001_num_salida_vehiculo"
        );
        $eliminarAccesorios->execute(array(':fk_pae001_num_salida_vehiculo'=>$idSalida));


        if($accesorios) {
            $Accesorios = $this->_db->prepare(
                "insert into pa_c002_accesorio_salida  (fk_pab011_num_accesorio, fk_pae001_num_salida_vehiculo)values (:fk_pab011_num_accesorio, :fk_pae001_num_salida_vehiculo)"
            );
            for ($i = 0; $i < count($accesorios); $i++) {

                $Accesorios->execute(array(
                    ':fk_pab011_num_accesorio' => $accesorios[$i],
                    ':fk_pae001_num_salida_vehiculo' => $idSalida
                ));
            }
        }





        #commit — Consigna una transacción
        $this->_db->commit();


        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "update `pa_d001_solicitud_vehiculo` set fk_a006_num_estado_solicitud = '$estatus' " .
            "where  pk_num_solicitud = '$solicitud'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();



        return $idSalida;
    }



    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrar($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
        "    SELECT
                pa_d001_solicitud_vehiculo.pk_num_solicitud,
                DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_requerida,'%d/%m/%Y') as fec_requerida,
                DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_hasta,'%d/%m/%Y') as fec_hasta,
        
                DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_requerida,'%h:%i %p') as hora_requerida,
                DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_hasta,'%h:%i %p') as hora_hasta,
        
                DATE_FORMAT(pa_e001_salida_vehiculo.fec_hora_salida,'%d/%m/%Y') as fec_hora_salida,
                DATE_FORMAT(pa_e001_salida_vehiculo.fec_hora_hasta,'%d/%m/%Y') as fec_hora_hasta,
        
                DATE_FORMAT(pa_e001_salida_vehiculo.fec_hora_salida,'%h:%i %p') as hora_salida,
                DATE_FORMAT(pa_e001_salida_vehiculo.fec_hora_hasta,'%h:%i %p') as hora_hasta2,
        
                pa_d001_solicitud_vehiculo.fk_rhb001_empleado_solicitante ,
                pa_d001_solicitud_vehiculo.fk_rhb001_empleado_conformado,
                pa_d001_solicitud_vehiculo.fec_aprobado,
                pa_d001_solicitud_vehiculo.fk_a006_num_estado_solicitud,
                pa_d001_solicitud_vehiculo.ind_observacion,
                pa_d001_solicitud_vehiculo.ind_motivo_rechazo,
                pa_d001_solicitud_vehiculo.fk_a018_num_seguridad_usuario,
                pa_d001_solicitud_vehiculo.fec_ultima_modificacion,
                pa_d001_solicitud_vehiculo.fk_a006_num_motivo_salida,
                pa_d001_solicitud_vehiculo.fk_a006_num_tipo_salida,
                pa_b001_vehiculo.ind_url_foto_frontal,
                pa_b001_vehiculo.ind_placa,
                pa_b001_vehiculo.ind_modelo ,
                pa_e001_salida_vehiculo.ind_kilometraje,
                pa_e001_salida_vehiculo.fk_pab001_num_vehiculo,
                pa_e001_salida_vehiculo.fk_pab012_num_chofer,
                pa_e001_salida_vehiculo.pk_num_salida_vehiculo,
                pa_e001_salida_vehiculo.ind_observacion as observacionSalida,
                rh_b001_empleado.fk_a003_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_apellido1,  
                a003_persona.ind_nombre1 as chofer1,
                a003_persona.ind_apellido1 as chofer2
                
            FROM `pa_e001_salida_vehiculo`

            left join pa_d001_solicitud_vehiculo on (pa_d001_solicitud_vehiculo.pk_num_solicitud=pa_e001_salida_vehiculo.fk_pad001_num_solicitud_vehiculo)
            left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e001_salida_vehiculo.fk_pab001_num_vehiculo)
            left join  rh_b001_empleado on (rh_b001_empleado.fk_a003_num_persona=pa_e001_salida_vehiculo.fk_pab012_num_chofer)
            left join  a003_persona     on (a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona)

            where  pa_e001_salida_vehiculo.pk_num_salida_vehiculo=".$id.""
        );


        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }






    public function metDeleteSalidaInstitucion($id,$idsolicitud)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto

        $this->_db->query(
            "delete from `pa_c002_accesorio_salida` where fk_pae001_num_salida_vehiculo='$id'"
        );


        $this->_db->query(
            "delete from `pa_e001_salida_vehiculo`  where pk_num_salida_vehiculo='$id'"
        );



        $modificar=$this->_db->prepare(
            "UPDATE `pa_d001_solicitud_vehiculo`
              SET fk_a006_num_estado_solicitud=:fk_a006_num_estado_solicitud  WHERE 	pk_num_solicitud='".$idsolicitud."'"
        );
 ;
        $modificar->execute(array(
            ':fk_a006_num_estado_solicitud' => '5',
        ));


        #commit — Consigna una transacción
        $this->_db->commit();
    }




}
