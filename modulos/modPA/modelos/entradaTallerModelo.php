<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class entradaTallerModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener("idUsuario");
    }

    #metodo para obtener todos los registros guardados de los post
    public function metGetEntradaTaller()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT
             DATE_FORMAT(pa_e006_entrada_taller.fec_hora  ,'%d/%m/%Y') as fec_entrada,
             DATE_FORMAT(pa_e006_entrada_taller.fec_hora  ,'%h:%i %p') as hora,
             pa_e006_entrada_taller.fk_pab001_num_vehiculo,
             pa_e006_entrada_taller.fk_pab012_num_chofer,
             pa_e006_entrada_taller.pk_num_entrada_taller,
             pa_e006_entrada_taller.ind_motivo,
             pa_b001_vehiculo.ind_modelo,
             pa_b001_vehiculo.ind_placa,
             pa_b001_vehiculo.ind_url_foto_frontal,
             pa_b012_chofer.fk_rhb001_num_empleado,
             lg_b022_proveedor.fk_a003_num_persona_proveedor,
             a003_persona.ind_nombre1,
             pa_e007_salida_taller.fk_pae006_num_entrada_taller,
             p_natural.ind_nombre1 as ind_nombre2,
             p_natural.ind_apellido1 as ind_nombre3
             
             FROM `pa_e006_entrada_taller`

             inner join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e006_entrada_taller.fk_pab001_num_vehiculo AND pa_b001_vehiculo.num_estatus='0')
             left join  pa_b012_chofer on    (pa_b012_chofer.pk_num_chofer=pa_e006_entrada_taller.fk_pab012_num_chofer)
             left join  lg_b022_proveedor on (lg_b022_proveedor.fk_a003_num_persona_proveedor=pa_e006_entrada_taller.fk_lgb022_num_proveedor)
             left join  a003_persona on      (a003_persona.pk_num_persona=lg_b022_proveedor.fk_a003_num_persona_proveedor)
             left  join pa_e007_salida_taller on (pa_e007_salida_taller.fk_pae006_num_entrada_taller =pa_e006_entrada_taller.pk_num_entrada_taller)
             left join a003_persona as p_natural on     (p_natural.pk_num_persona=pa_e006_entrada_taller.fk_lgb022_num_proveedor)
             WHERE   pa_e007_salida_taller.fk_pae006_num_entrada_taller IS NULL; "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para guardar el registro del post
    public function metSetEntradaTaller($hora,$motivo,$kilometraje,$observacion,$cobertura,$vehiculo,$chofer,$taller)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoPost=$this->_db->prepare(
            "insert into  `pa_e006_entrada_taller` (fec_hora,ind_motivo,ind_kilometraje,ind_cobertura,ind_observacion,fk_a018_num_seguridad_usuario,fec_ultima_modificacion,fk_pab001_num_vehiculo,fk_pab012_num_chofer,fk_lgb022_num_proveedor) values
                                                   ( :fec_hora,:ind_motivo,:ind_kilometraje,:ind_cobertura,:ind_observacion,:fk_a018_num_seguridad_usuario,:fec_ultima_modificacion,:fk_pab001_num_vehiculo,:fk_pab012_num_chofer,:fk_lgb022_num_proveedor)"
        );

        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(
            ':fec_hora' => $hora,
            ':ind_motivo' => $motivo,
            ':ind_kilometraje' =>$kilometraje,
            ':ind_cobertura' => $cobertura,
            ':ind_observacion' => $observacion,
            ':fk_a018_num_seguridad_usuario' =>$this->atIdUsuario,
            ':fec_ultima_modificacion' =>date('Y-m-d H:i:s'),
            ':fk_pab001_num_vehiculo'=>$vehiculo,
            ':fk_pab012_num_chofer'=>$chofer,
            ':fk_lgb022_num_proveedor'=>$taller,



        ));
       $idPost= $this->_db->lastInsertId();

        $this->_db->query(
            "update `pa_b001_vehiculo` set num_estatus='0' where pk_num_vehiculo='$vehiculo'"
        );


        $this->_db->commit();





        return $idPost;
    }



    #metodo para obtener  el fk vehiculo
    public function metGetFkVehiculo($valor)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT
             fk_pab001_num_vehiculo

             FROM `pa_e006_entrada_taller`

             WHERE pk_num_entrada_taller='".$valor."'
               "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para modificar el registro del post
    public function metUpdateEntradaTaller($hora,$motivo,$kilometraje,$observacion,$cobertura,$chofer,$taller,$id,$vehiculo,$vehiculoBD)

    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        #Si vehiculo de formulario es igual al vehiculo que esta en la bd no se modifica y no hay necesidad de actualizar su estado de disponibilidad
        $modificar = $this->_db->prepare(
            "UPDATE pa_e006_entrada_taller
              SET
             fec_hora=:fec_hora,
             ind_motivo=:ind_motivo,
             ind_kilometraje=:ind_kilometraje,
             ind_cobertura=:ind_cobertura,
             ind_observacion=:ind_observacion,
             fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
             fec_ultima_modificacion=:fec_ultima_modificacion,

             fk_pab012_num_chofer=:fk_pab012_num_chofer,
             fk_pab001_num_vehiculo=:fk_pab001_num_vehiculo,
             fk_lgb022_num_proveedor=:fk_lgb022_num_proveedor
             WHERE pk_num_entrada_taller='".$id."'"
        );
        $modificar->execute(array(
            ':fec_hora' => $hora,
            ':ind_motivo' => $motivo,
            ':ind_kilometraje' => $kilometraje,
            ':ind_cobertura' => $cobertura,
            ':ind_observacion' => $observacion,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
            ':fk_pab012_num_chofer' => $chofer,
            ':fk_lgb022_num_proveedor' => $taller,
            ':fk_pab001_num_vehiculo' => $vehiculo,

        ));

        if ($vehiculo!=$vehiculoBD) {
            $this->_db->query(
                "update `pa_b001_vehiculo` set num_estatus='0' where pk_num_vehiculo='$vehiculo'"
            );

            $this->_db->query(
                "update `pa_b001_vehiculo` set num_estatus='1' where pk_num_vehiculo='$vehiculoBD'"
            );
    }



        #commit — Consigna una transacción
        $this->_db->commit();


        return $id;
    }



    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrar($id)
    {

        #ejecuto la consulta a la base de datos
            $pruebaPost =  $this->_db->query(
            "SELECT
             DATE_FORMAT(pa_e006_entrada_taller.fec_hora  ,'%d/%m/%Y') as fec_entrada,
             DATE_FORMAT(pa_e006_entrada_taller.fec_hora  ,'%h:%i %p') as hora,
             pa_e006_entrada_taller.fk_pab001_num_vehiculo,
             pa_e006_entrada_taller.pk_num_entrada_taller,
             pa_e006_entrada_taller.ind_kilometraje,
             pa_e006_entrada_taller.fk_pab012_num_chofer,
             pa_e006_entrada_taller.ind_motivo,
             pa_e006_entrada_taller.ind_cobertura,
             pa_b001_vehiculo.ind_modelo,
             pa_b001_vehiculo.ind_placa,
             pa_b012_chofer.fk_rhb001_num_empleado,
             pa_e006_entrada_taller.fk_lgb022_num_proveedor,
             lg_b022_proveedor.fk_a003_num_persona_proveedor,
             pa_e006_entrada_taller.ind_observacion,
             a003_persona.ind_nombre1,
             
             p_natural.ind_nombre1 as ind_nombre2,
             p_natural.ind_apellido1 as ind_nombre3
             FROM `pa_e006_entrada_taller`

             left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e006_entrada_taller.fk_pab001_num_vehiculo)
             left join pa_b012_chofer on   (pa_b012_chofer.pk_num_chofer=pa_e006_entrada_taller.fk_pab012_num_chofer)
             
             left join lg_b022_proveedor on (lg_b022_proveedor.fk_a003_num_persona_proveedor=pa_e006_entrada_taller.fk_lgb022_num_proveedor) 
             left join a003_persona on     (a003_persona.pk_num_persona=lg_b022_proveedor.fk_a003_num_persona_proveedor)
             left join a003_persona as p_natural on     (p_natural.pk_num_persona=pa_e006_entrada_taller.fk_lgb022_num_proveedor)
             where   pk_num_entrada_taller='".$id."'"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }





    public function metDeleteEntradaTaller($id,$idvehiculo)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "delete from `pa_e006_entrada_taller` " .
            "where pk_num_entrada_taller= '$id'"
        );


        $modificar=$this->_db->prepare(
            "UPDATE `pa_b001_vehiculo`
              SET num_estatus=:num_estatus  WHERE pk_num_vehiculo='".$idvehiculo."'"
        );


        $modificar->execute(array(
            ':num_estatus' => '1',
        ));


        #commit — Consigna una transacción
        $this->_db->commit();
    }






}
