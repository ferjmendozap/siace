<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class estatusModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener("idUsuario");
    }

    #metodo para obtener todos los registros guardados de los post
    public function metGetEstatus()
    {   #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "select
            a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,
            a006_miscelaneo_detalle.cod_detalle,
            a006_miscelaneo_detalle.ind_nombre_detalle

            from a005_miscelaneo_maestro

            inner join a006_miscelaneo_detalle on  (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro  )

            where  (a005_miscelaneo_maestro.cod_maestro='ESTADOSOL' )

            "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }



}
