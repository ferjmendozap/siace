<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class dannioModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener("idUsuario");
    }

    #metodo para obtener todos los registros guardados de los post
    public function metGetDannio()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT
             pa_e004_averia_vehiculo.pk_num_averia_vehiculo,
             pa_e004_averia_vehiculo.ind_observacion_averia,
             DATE_FORMAT(pa_e004_averia_vehiculo.fec_registro ,'%d/%m/%Y') as fec_registro,
             pa_e004_averia_vehiculo.fk_pab007_num_piezas,
             pa_b007_pieza_vehiculo.ind_descripcion  as pieza,
             pa_b001_vehiculo.ind_modelo as vehiculo,
             pa_b001_vehiculo.ind_placa,
             pa_b001_vehiculo.ind_url_foto_frontal
             FROM `pa_e004_averia_vehiculo`

             left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e004_averia_vehiculo.fk_pab001_num_vehiculo)
             left join pa_b007_pieza_vehiculo on (pa_b007_pieza_vehiculo.pk_num_piezas=pa_e004_averia_vehiculo.fk_pab007_num_piezas)
             where pa_e004_averia_vehiculo.ind_estado='0'
            "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para obtener todos los vehículos con daños actuales
    public function metGetVehiculosConDannio()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT
             pa_e004_averia_vehiculo.pk_num_averia_vehiculo,
             pa_e004_averia_vehiculo.fk_pab001_num_vehiculo,
             pa_b001_vehiculo.ind_modelo,
             pa_b001_vehiculo.ind_placa,
             pa_b001_vehiculo.pk_num_vehiculo,
             DATE_FORMAT(pa_e004_averia_vehiculo.fec_registro ,'%d/%m/%Y') as fec_registro,
             pa_e004_averia_vehiculo.ind_observacion_averia,
             pa_b001_vehiculo.ind_url_foto_frontal,
             pa_b007_pieza_vehiculo.ind_descripcion  as pieza
             FROM `pa_e004_averia_vehiculo`

             left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e004_averia_vehiculo.fk_pab001_num_vehiculo)
             left join pa_b007_pieza_vehiculo on (pa_b007_pieza_vehiculo.pk_num_piezas=pa_e004_averia_vehiculo.fk_pab007_num_piezas)
             where pa_e004_averia_vehiculo.ind_estado='0'  group by pa_b001_vehiculo.pk_num_vehiculo"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para obtener todos los vehículos con daños actuales de un vehículo
    public function metGetDannioVehiculo($vehiculo)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT
             pa_e004_averia_vehiculo.pk_num_averia_vehiculo,
             pa_b001_vehiculo.ind_modelo,
             pa_b001_vehiculo.ind_placa,
             pa_b001_vehiculo.pk_num_vehiculo,
             DATE_FORMAT(pa_e004_averia_vehiculo.fec_registro ,'%d/%m/%Y') as fec_registro,
             pa_e004_averia_vehiculo.ind_observacion_averia,
             pa_b007_pieza_vehiculo.ind_descripcion as pieza
             FROM `pa_e004_averia_vehiculo`

             left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e004_averia_vehiculo.fk_pab001_num_vehiculo)
             left join pa_b007_pieza_vehiculo on (pa_b007_pieza_vehiculo.pk_num_piezas=pa_e004_averia_vehiculo.fk_pab007_num_piezas)
             where pa_e004_averia_vehiculo.ind_estado='0' AND  pa_b001_vehiculo.pk_num_vehiculo='".$vehiculo."' 	 group by pa_e004_averia_vehiculo.pk_num_averia_vehiculo"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }



    #metodo para guardar el registro del post
    public function metSetDannio($observacion,$fecha,$vehiculo,$dannio)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        if($dannio) {
            $Dannios = $this->_db->prepare(
                "insert into pa_e004_averia_vehiculo  (ind_observacion_averia, fec_registro,fk_a018_num_seguridad_usuario,fec_ultima_modificacion,fk_pab001_num_vehiculo,fk_pab007_num_piezas,ind_estado)values (:ind_observacion_averia, :fec_registro,:fk_a018_num_seguridad_usuario,:fec_ultima_modificacion,:fk_pab001_num_vehiculo,:fk_pab007_num_piezas,:ind_estado)"
            );
            for ($i = 0; $i < count($dannio); $i++) {

                $Dannios->execute(array(
                    ':ind_observacion_averia' => $observacion,
                    ':fec_registro' => $fecha,
                    ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
                    ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
                    ':fk_pab001_num_vehiculo' => $vehiculo,
                    ':fk_pab007_num_piezas'=> $dannio[$i],
                    ':ind_estado'=> "0"
                ));
            }
        }



        $idPost= $this->_db->lastInsertId();

        #commit — Consigna una transacción
        $this->_db->commit();




        return $idPost;
    }


    #metodo para modificar el registro del post
    public function metUpdateDannio($observacion,$fecha,$vehiculo,$dannio,$bd_foto,$idDannio)
    {
 
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia

        $modificar=$this->_db->prepare(
            "UPDATE pa_e004_averia_vehiculo
              SET ind_observacion_averia=:ind_observacion_averia, fec_registro=:fec_registro,fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,fec_ultima_modificacion=:fec_ultima_modificacion,fk_pab001_num_vehiculo=:fk_pab001_num_vehiculo,fk_pab007_num_piezas=:fk_pab007_num_piezas,ind_url=:ind_url WHERE pk_num_averia_vehiculo='".$idDannio."'"
        );
        $modificar->execute(array(
            ':ind_observacion_averia' =>  $observacion,
            ':fec_registro' => $fecha,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
            ':fk_pab001_num_vehiculo'=> $vehiculo,
            ':fk_pab007_num_piezas'=> $dannio[0],
            ':ind_url'=> $bd_foto
        ));


        $this->_db->commit();



        return $idDannio;
    }



    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrar($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
        "SELECT
        DATE_FORMAT(pa_e004_averia_vehiculo.fec_registro,'%d/%m/%Y') as fec_registro,
        pa_e004_averia_vehiculo.ind_observacion_averia,
        pa_e004_averia_vehiculo.fk_pab001_num_vehiculo,
        pa_e004_averia_vehiculo.fk_pab007_num_piezas,
        pa_e004_averia_vehiculo.pk_num_averia_vehiculo,
         pa_b007_pieza_vehiculo.ind_descripcion  as pieza,
        pa_e004_averia_vehiculo.ind_url,
        pa_b001_vehiculo.ind_modelo,
        pa_b001_vehiculo.ind_placa


        FROM `pa_e004_averia_vehiculo`
        left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e004_averia_vehiculo.fk_pab001_num_vehiculo)
        left join pa_b007_pieza_vehiculo on (pa_b007_pieza_vehiculo.pk_num_piezas=pa_e004_averia_vehiculo.fk_pab007_num_piezas)
        where  pk_num_averia_vehiculo='".$id."'"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetch();
    }





    public function metDeleteDannio($id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "delete from `pa_e004_averia_vehiculo` " .
            "where  pk_num_averia_vehiculo= '$id'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }

    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrarSelect($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT pa_e004_averia_vehiculo.pk_num_averia_vehiculo,pa_b007_pieza_vehiculo.ind_descripcion   FROM `pa_e004_averia_vehiculo`
 left Join pa_b007_pieza_vehiculo on (pa_b007_pieza_vehiculo.pk_num_piezas=pa_e004_averia_vehiculo.fk_pab007_num_piezas)
 where  fk_pab001_num_vehiculo='".$id."'"
        );
        //echo "SQL ".$pruebaPost;
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }




}
