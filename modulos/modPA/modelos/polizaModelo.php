<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class polizaModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener("idUsuario");
    }

    #metodo para obtener todos los registros guardados de los post
    public function metGetPoliza()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query("SELECT * FROM pa_c001_poliza"  );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para guardar el registro del post
    public function metSetPoliza($poliza,$recibo,$desde,$hasta,$estatus,$monto_pagado,$monto_cobertura,$fk_vehiculo,$fk_proveedor)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoPost=$this->_db->prepare(
            "insert into pa_c001_poliza (ind_poliza,ind_recibo,fec_desde,fec_hasta,num_estatus,num_monto_pagado,num_monto_cobertura,fk_pab001_num_vehiculo,fk_lgb022_proveedor,fk_a018_num_seguridad_usuario,fec_ultima_modificacion)
                                values ( :ind_poliza,:ind_recibo,:fec_desde,:fec_hasta,:num_estatus,:num_monto_pagado,:num_monto_cobertura,:fk_pab001_num_vehiculo,:fk_lgb022_proveedor,:fk_a018_num_seguridad_usuario,:fec_ultima_modificacion )"
        );

        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(
            ':ind_poliza' => $poliza,
            ':ind_recibo' => $recibo,
            ':fec_desde' => $desde,
            ':fec_hasta' => $hasta,
            ':num_estatus' => $estatus,
            ':num_monto_pagado' => $monto_pagado,
            ':num_monto_cobertura' => $monto_cobertura,
            ':fk_pab001_num_vehiculo' => $fk_vehiculo,
            ':fk_lgb022_proveedor' => $fk_proveedor,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
            ':fec_ultima_modificacion' =>date('Y-m-d H:i:s'),

        ));





        $idPost= $this->_db->lastInsertId();
        #commit — Consigna una transacción
        $this->_db->commit();
        return $idPost;
    }

    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrar($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT pk_num_poliza,ind_poliza,ind_recibo,num_estatus,   num_monto_pagado,  num_monto_cobertura,	fk_pab001_num_vehiculo,fk_lgb022_proveedor,DATE_FORMAT(fec_desde, '%d/%m/%Y') as fec_desde,DATE_FORMAT(fec_hasta, '%d/%m/%Y') as fec_hasta  FROM `pa_c001_poliza` where  pk_num_poliza='".$id."'"
        );
        //echo "SQL ".$pruebaPost;
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }
    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrarVinculacion($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT 
             pa_c001_poliza.pk_num_poliza,
             pa_c001_poliza.ind_poliza,
             pa_c001_poliza.ind_recibo,
             pa_c001_poliza.num_estatus,
             pa_c001_poliza.num_monto_pagado ,  
             pa_c001_poliza.num_monto_cobertura,	
             pa_c001_poliza.fk_pab001_num_vehiculo,
             pa_c001_poliza.fk_lgb022_proveedor,
             DATE_FORMAT(pa_c001_poliza.fec_desde, '%d/%m/%Y') as fec_desde,
             DATE_FORMAT(pa_c001_poliza.fec_hasta, '%d/%m/%Y') as fec_hasta,  
             lg_b022_proveedor.fk_a003_num_persona_proveedor,
             a003_persona.ind_nombre1
             
             FROM `pa_c001_poliza`  
             
             left join  lg_b022_proveedor on (lg_b022_proveedor.fk_a003_num_persona_proveedor=pa_c001_poliza.fk_lgb022_proveedor)
             left join  a003_persona      on (a003_persona.pk_num_persona=lg_b022_proveedor.fk_a003_num_persona_proveedor)
            
             where  pa_c001_poliza.fk_pab001_num_vehiculo='".$id."' AND  pa_c001_poliza.fec_hasta>='".date('Y-m-d')."'  order by pa_c001_poliza.pk_num_poliza desc limit 0,1 
             "
        );


        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    public function metUpdatePoliza($poliza,$recibo,$desde,$hasta,$estatus,$monto_pagado,$monto_cobertura,$fk_vehiculo,$fk_proveedor,$id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(

            "update `pa_c001_poliza` set ind_poliza = '".$poliza."' , ind_recibo = '".$recibo."',num_estatus = '$estatus',num_monto_pagado= '$monto_pagado',num_monto_cobertura = '$monto_cobertura',fk_lgb022_proveedor = '$fk_proveedor',fec_desde = '".$desde."',fec_hasta = '".$hasta."',fk_pab001_num_vehiculo = '$fk_vehiculo', fk_a018_num_seguridad_usuario= '".$this->atIdUsuario."',fec_ultima_modificacion='".date('Y-m-d H:i:s')."' " .
            "where pk_num_poliza= '$id'"
        );


        #commit — Consigna una transacción
        $this->_db->commit();
    }

    public function metDeletePoliza($id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "delete from `pa_c001_poliza` " .
            "where pk_num_poliza= '$id'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }




}
