<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class vehiculoModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
		$this->atIdUsuario = Session::metObtener("idUsuario");
    }

    #metodo para obtener todos los registros guardados de los post
    public function metGetVehiculo($id=false,$filtro=false)
    {
        $where = 'ORDER BY pa_b001_vehiculo.pk_num_vehiculo';
        if($filtro) {
            $id=false;
            $where = $filtro;
        } elseif($id) {
            $where = "WHERE pa_b001_vehiculo.pk_num_vehiculo = '".$id."' ORDER BY pa_b001_vehiculo.pk_num_vehiculo";
        }
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query("
            SELECT  
              pa_b001_vehiculo.ind_placa,
              pa_b001_vehiculo.ind_serial_motor,
              pa_b001_vehiculo.fec_annio,
              pa_b001_vehiculo.pk_num_vehiculo,
              pa_b001_vehiculo.ind_serial_carroceria,
              pa_b001_vehiculo.ind_modelo,
              pa_b001_vehiculo.fk_pab009_num_tipo_vehiculo,

              a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,
              claseMaestro.pk_num_miscelaneo_maestro,
              claseDetalle.ind_nombre_detalle as ind_descripcion,
              a006_miscelaneo_detalle.ind_nombre_detalle as ind_marca,

              pa_b009_tipo_vehiculo.ind_descripcion as ind_tipo

             FROM `pa_b001_vehiculo`

             left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='MARVEH')
             left join a006_miscelaneo_detalle on (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND pa_b001_vehiculo.fk_a006_num_marca_vehiculo=a006_miscelaneo_detalle.cod_detalle)
             left join a005_miscelaneo_maestro as claseMaestro on (claseMaestro.cod_maestro='CLASEVEH')
             left join a006_miscelaneo_detalle as claseDetalle on (claseDetalle.fk_a005_num_miscelaneo_maestro=claseMaestro.pk_num_miscelaneo_maestro AND pa_b001_vehiculo.fk_a006_num_clase_vehiculo=claseDetalle.cod_detalle)
             left join pa_b009_tipo_vehiculo on (pa_b009_tipo_vehiculo.pk_num_tipo_vehiculo=pa_b001_vehiculo.fk_pab009_num_tipo_vehiculo)
             $where
        ");

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        if($id){
            return $pruebaPost->fetch();
        } else {
            return $pruebaPost->fetchAll();
        }
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        //return $pruebaPost->fetchAll();
    }
    #metodo para obtener todos los registros guardados de los post
    public function metGetVehiculoActivo()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query("SELECT * FROM  pa_b001_vehiculo WHERE num_estatus=1"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }
    #metodo para guardar el registro del post
    public function metSetVehiculo($annio,$placa,$serial_motor,$serial_carroceria,$color,$modelo,$tipo,$marca,$clase,$vld,$vli,$vf,$vt,$vs,$vi)
    {


          #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoPost=$this->_db->prepare(
            "insert into  pa_b001_vehiculo (fec_annio,ind_placa,ind_serial_motor,ind_serial_carroceria,ind_color,ind_modelo,fk_a006_num_marca_vehiculo,num_estatus,fk_a006_num_clase_vehiculo,fk_pab009_num_tipo_vehiculo,fk_a018_num_seguridad_usuario,fec_ultima_modificacion,ind_url_foto_lateral_derecha,ind_url_foto_lateral_izquierda,ind_url_foto_frontal,ind_url_foto_trasera,ind_url_foto_superior,ind_url_foto_inferior)
                                  values ( :fec_annio,:ind_placa,:ind_serial_motor,:ind_serial_carroceria,:ind_color,:ind_modelo,:fk_a006_num_marca_vehiculo,:num_estatus,:fk_a006_num_clase_vehiculo,:fk_pab009_num_tipo_vehiculo,:fk_a018_num_seguridad_usuario,:fec_ultima_modificacion
								  ,:ind_url_foto_lateral_derecha
								  ,:ind_url_foto_lateral_izquierda
								  ,:ind_url_foto_frontal
								  ,:ind_url_foto_trasera
								  ,:ind_url_foto_superior
								  ,:ind_url_foto_inferior )"
        );

        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(
            ':fec_annio' => $annio,
            ':ind_placa' => $placa,
            ':ind_serial_motor' => $serial_motor,
            ':ind_serial_carroceria' => $serial_carroceria,
            ':ind_color' => $color,
            ':ind_modelo' => $modelo,
            ':fk_pab009_num_tipo_vehiculo' => $tipo,
            ':fk_a018_num_seguridad_usuario'=> $this->atIdUsuario,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
            ':fk_a006_num_marca_vehiculo' => $marca,
            ':num_estatus' => 1,
            ':fk_a006_num_clase_vehiculo' => $clase,
			':ind_url_foto_lateral_derecha'=> $vld,
			':ind_url_foto_lateral_izquierda'=> $vli,
			':ind_url_foto_frontal'=> $vf,
			':ind_url_foto_trasera'=> $vt,
			':ind_url_foto_superior'=> $vs,
			':ind_url_foto_inferior'=> $vi,
           


 

        ));
        $idPost= $this->_db->lastInsertId();
        #commit — Consigna una transacción
        $this->_db->commit();
        return $idPost;
    }

    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrar($id)
    {

        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT * FROM `pa_b001_vehiculo` where  pk_num_vehiculo='".$id."
            '
  "
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrarModificacion($id)
    {

        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT * FROM `pa_b001_vehiculo` where  pk_num_vehiculo='".$id."' and num_estatus=1
  "
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }



    public function metUpdateVehiculo($annio,$placa,$serial_motor,$serial_carroceria,$color,$modelo,$tipo,$marca,$clase,$id,$vld,$vli,$vf,$vt,$vs,$vi)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(



            "update `pa_b001_vehiculo` set  fec_annio = '$annio' ,  ind_placa = '$placa' , ind_serial_motor = '$serial_motor' , ind_serial_carroceria = '$serial_carroceria' , ind_color = '$color', ind_modelo = '$modelo'
   ,  fk_a006_num_marca_vehiculo = '$marca',fk_a006_num_clase_vehiculo = '$clase', fk_pab009_num_tipo_vehiculo = '$tipo',fk_a018_num_seguridad_usuario='".$this->atIdUsuario."',fec_ultima_modificacion='".date('Y-m-d H:i:s')."' 
   , ind_url_foto_lateral_derecha='".$vld."' 
   , ind_url_foto_lateral_izquierda='".$vli."' 
   , ind_url_foto_frontal='".$vf."' 
   , ind_url_foto_trasera='".$vt."' 
   , ind_url_foto_superior='".$vs."' 
   , ind_url_foto_inferior='".$vi."' 
   
   where pk_num_vehiculo= '$id'"
        );
 

        #commit — Consigna una transacción
        $this->_db->commit();
    }

    public function metDeleteVehiculo($id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "delete from `pa_b001_vehiculo` " .
            "where pk_num_vehiculo= '$id'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }



    public function metBuscarPlaca($placa)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query("SELECT pk_num_vehiculo FROM  pa_b001_vehiculo WHERE ind_placa='".$placa."' limit 0,1"
        );

        return $pruebaPost->rowCount();
    }


    public function metListarProveedor()
    {
        $proveedorPost = $this->_db->query("
          SELECT
              proveedor.*,
              concat_ws(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nomProveedor,
              persona.ind_documento_fiscal,
              persona.num_estatus
          FROM
            lg_b022_proveedor AS proveedor
          INNER JOIN a003_persona AS persona ON persona.pk_num_persona =  proveedor.fk_a003_num_persona_proveedor
              ");
        $proveedorPost->SETFetchMode(PDO::FETCH_ASSOC);
        return $proveedorPost->fetchAll();
    }
}
