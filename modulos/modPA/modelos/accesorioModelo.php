<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/


class accesorioModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener("idUsuario");
    }

    #metodo para obtener todos los registros guardados de los post
    public function metGetAccesorio()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query( "SELECT * FROM pa_b011_accesorio"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para guardar el registro del post
    public function metSetAccesorio($titulo)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoPost=$this->_db->prepare(
            "insert into pa_b011_accesorio (ind_descripcion_accesorio,fk_a018_num_seguridad_usuario,fec_ultima_modificacion) values ( :ind_descripcion_accesorio,:fk_a018_num_seguridad_usuario,:fec_ultima_modificacion )"
        );

        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(
            ':ind_descripcion_accesorio' => $titulo,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
            ':fec_ultima_modificacion' =>date('Y-m-d H:i:s'),

        ));
        
        $idPost= $this->_db->lastInsertId();

        $error = $NuevoPost->errorInfo();


        if(!empty($error[1]) && !empty($error[2])){

            $this->_db->rollBack();
            return $error;
        }else{

            $this->_db->commit();

            return $idPost;

        }


    }

    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrar($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT pk_num_accesorio ,ind_descripcion_accesorio FROM `pa_b011_accesorio` where  pk_num_accesorio='".$id."'"
        );
        //echo "SQL ".$pruebaPost;
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para mostrar todos los registros de accesorios relacionados con una salida
    public function metMostrarSalida($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT fk_pab011_num_accesorio    FROM `pa_c002_accesorio_salida` where  fk_pae001_num_salida_vehiculo ='".$id."'"
        );
        //echo "SQL ".$pruebaPost;
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }


    public function metUpdateAccesorio($nombre,$id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "update `pa_b011_accesorio` set ind_descripcion_accesorio = '$nombre' , fk_a018_num_seguridad_usuario= '".$this->atIdUsuario."',fec_ultima_modificacion='".date('Y-m-d H:i:s')."'"  .
            "where pk_num_accesorio= '$id'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }

    public function metDeleteAccesorio($id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "delete from `pa_b011_accesorio` " .
            "where pk_num_accesorio= '$id'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }




}
