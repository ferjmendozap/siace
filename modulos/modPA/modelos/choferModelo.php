<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/


class choferModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener("idUsuario");
    }

    #metodo para obtener todos los registros guardados de los post
    public function metGetChofer()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query("
        SELECT
        pa_b012_chofer.*,rh_b001_empleado.fk_a003_num_persona,rh_b001_empleado.pk_num_empleado,a003_persona.ind_nombre1,a003_persona.ind_apellido1,
        rh_b001_empleado.fk_a003_num_persona,
        rh_b001_empleado.pk_num_empleado,
        a003_persona.ind_nombre1,
        a003_persona.ind_apellido1,
        a003_persona.pk_num_persona,
        a003_persona.ind_cedula_documento,
        a004_dependencia.ind_dependencia,
        rh_c076_empleado_organizacion.fk_a004_num_dependencia
        
        
        FROM rh_b001_empleado
        
      
        
        INNER join  pa_b012_chofer on (rh_b001_empleado.fk_a003_num_persona=pa_b012_chofer.fk_rhb001_num_empleado)
        INNER join  a003_persona     on (a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona)
           INNER  JOIN  rh_c076_empleado_organizacion on (rh_c076_empleado_organizacion.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado)
        INNER JOIN   a004_dependencia on (a004_dependencia.pk_num_dependencia=rh_c076_empleado_organizacion.fk_a004_num_dependencia) 

        "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }



    #metodo para guardar el registro del post
    public function metSetChofer( $observacion,$fk_empleado)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoPost=$this->_db->prepare(
            "insert into pa_b012_chofer (ind_observacion,fk_rhb001_num_empleado,fk_a018_num_seguridad_usuario,fec_ultima_modificacion) values ( :ind_observacion,:fk_rhb001_num_empleado,:fk_a018_num_seguridad_usuario,:fec_ultima_modificacion )"
        );


        "insert into pa_b012_chofer (ind_observacion,fk_rhb001_num_empleado,fk_a018_num_seguridad_usuario,fec_ultima_modificacion) values ( '".$observacion."', '".$fk_empleado."', '".$this->atIdUsuario."','".date('Y-m-d H:i:s')."' )";
        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(
            ':ind_observacion' => $observacion,
            ':fk_rhb001_num_empleado' => $fk_empleado ,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
            ':fec_ultima_modificacion' =>date('Y-m-d H:i:s'),
        ));

        $idPost= $this->_db->lastInsertId();

        $error = $NuevoPost->errorInfo();


        if(!empty($error[1]) && !empty($error[2])){

            $this->_db->rollBack();
            return $error;
        }else{

            $this->_db->commit();

            return $idPost;

        }
    }

    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrar($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT
        pa_b012_chofer.*,rh_b001_empleado.fk_a003_num_persona,rh_b001_empleado.pk_num_empleado,a003_persona.ind_nombre1,a003_persona.ind_apellido1
        FROM pa_b012_chofer
        left join  rh_b001_empleado on (rh_b001_empleado.fk_a003_num_persona=pa_b012_chofer.fk_rhb001_num_empleado)
        left join  a003_persona     on (a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona) where  pk_num_chofer='".$id."'"
        );
        //echo "SQL ".$pruebaPost;
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }


    public function metUpdateChofer($observacion,$empleado,  $id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "update `pa_b012_chofer` set  fk_rhb001_num_empleado = '$empleado'  , ind_observacion = '$observacion'  , fk_a018_num_seguridad_usuario= '".$this->atIdUsuario."',fec_ultima_modificacion='".date('Y-m-d H:i:s')."'"  .
            "where pk_num_chofer= '$id'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }

    public function metDeleteChofer($id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "delete from `pa_b012_chofer` " .
            "where pk_num_chofer= '$id'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }

    public function metObtenerFuncionario($id)
    {


        $listarSolicitud =  $this->_db->query(
            "
             SELECT   rh_b001_empleado.fk_a003_num_persona,
             a003_persona.ind_nombre1,a003_persona.ind_apellido1
            
             FROM rh_b001_empleado
            
            
 
             left join a003_persona on (a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona)
            
             WHERE  rh_b001_empleado.fk_a003_num_persona='$id'


        "
        );
        $listarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $listarSolicitud->fetchAll();



    }
    public function metGetPersonas()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query("SELECT 
            rh_b001_empleado.fk_a003_num_persona, 
            rh_b001_empleado.pk_num_empleado, 
            a003_persona.ind_nombre1, 
            a003_persona.ind_apellido1, 
            a003_persona.pk_num_persona, 
            a003_persona.ind_cedula_documento, 
            a004_dependencia.ind_dependencia, 
            rh_c076_empleado_organizacion.fk_a004_num_dependencia 
            FROM rh_b001_empleado 
            inner join a003_persona on ( a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona AND ind_estado_aprobacion='AP') 
            INNER JOIN rh_c076_empleado_organizacion on (rh_c076_empleado_organizacion.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado) 
            INNER JOIN a004_dependencia on (a004_dependencia.pk_num_dependencia=rh_c076_empleado_organizacion.fk_a004_num_dependencia) 
            where rh_b001_empleado.num_estatus='1'"

        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

}
