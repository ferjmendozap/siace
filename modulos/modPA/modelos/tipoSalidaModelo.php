<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class tipoSalidaModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
		$this->atIdUsuario = Session::metObtener("idUsuario");
    }

    #metodo para obtener todos los registros guardados de los post
    public function metGetTipoSalida()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(            "select
            a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,
            a006_miscelaneo_detalle.pk_num_miscelaneo_detalle,
            a006_miscelaneo_detalle.cod_detalle,
            a006_miscelaneo_detalle.ind_nombre_detalle

            from a005_miscelaneo_maestro

            inner join a006_miscelaneo_detalle on  (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro  )

            where  (a005_miscelaneo_maestro.cod_maestro='TIPOSAL' )"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }


    #metodo para mostrar un registro con referencia a un pk
    public function metMostrar($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "select
            a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,
            a006_miscelaneo_detalle.cod_detalle,
            a006_miscelaneo_detalle.ind_nombre_detalle

            from a005_miscelaneo_maestro

            inner join a006_miscelaneo_detalle on  (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro  )

            where  (a005_miscelaneo_maestro.cod_maestro='TIPOSAL' )"
        );
        //echo "SQL ".$pruebaPost;
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    public function metRegistrarTipoSalida ($cod_detalle,$descripcion,$idMaestro)
    { 
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                a006_miscelaneo_detalle
                (cod_detalle,ind_nombre_detalle,num_estatus,fk_a005_num_miscelaneo_maestro)
                VALUES
                (:cod_detalle,:ind_nombre_grupo,:ind_estatus,:fk_a005_num_miscelaneo_maestro)
              ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':cod_detalle'      => $cod_detalle,
            ':ind_nombre_grupo' => $descripcion,
            ':ind_estatus'      => '1',
            ':fk_a005_num_miscelaneo_maestro' => $idMaestro,
        ));



        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarTipoSalida($idTipoSalida,$cod_detalle,$descripcion)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
              a006_miscelaneo_detalle
            SET
              cod_detalle=:cod_detalle,
              ind_nombre_detalle=:ind_nombre_detalle
            WHERE
              pk_num_miscelaneo_detalle='$idTipoSalida'
            "
        );

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':cod_detalle'      => $cod_detalle,
            ':ind_nombre_detalle' => $descripcion,
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
        }
    }
    public function metMostrarTipoSalida($idTipoSalida)
    {
        #ejecuto la consulta a la base de datos
        $con = $this->_db->query(
            "SELECT
              a006.pk_num_miscelaneo_detalle,
              a006.ind_nombre_detalle,
              a006.cod_detalle,
              a006.num_estatus,
              a006.fk_a005_num_miscelaneo_maestro
            FROM
              a006_miscelaneo_detalle AS a006
            INNER JOIN
              a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
            WHERE a005.cod_maestro='TIPOSAL' AND pk_num_miscelaneo_detalle='$idTipoSalida'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }


    public function metEliminarTipoSalida($idTipoSalida)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "delete from a006_miscelaneo_detalle where pk_num_miscelaneo_detalle ='$idTipoSalida'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }





}
