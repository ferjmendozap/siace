<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        17-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class scriptCargaModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
    }

    public function metBusquedaSimple($tabla, $innerJoin, $where)
    {
        $busquedaSimple = $this->_db->query("
            SELECT
              *
            FROM
              $tabla
              $innerJoin
            WHERE
              $where
        ");
        $busquedaSimple->setFetchMode(PDO::FETCH_ASSOC);
        return $busquedaSimple->fetch();
    }

    public function metMostrarSelectImpuesto($codImpuesto)
    {
        $impuesto = $this->_db->query("
            SELECT
           cp_b015_impuesto.pk_num_impuesto
            FROM
              cp_b015_impuesto
            WHERE
              cp_b015_impuesto.cod_impuesto='$codImpuesto'
        ");
        $impuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $impuesto->fetch();
    }

    public function metMostrarCuenta($codCuenta)
    {
        $impuesto = $this->_db->query("
            SELECT
           cb_b004_plan_cuenta.pk_num_cuenta
            FROM
              cb_b004_plan_cuenta
            WHERE
              cb_b004_plan_cuenta.cod_cuenta='$codCuenta'
        ");
        $impuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $impuesto->fetch();
    }

    public function metMostrarPartida($codPartida)
    {
        $impuesto = $this->_db->query("
            SELECT
           pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria
            FROM
              pr_b002_partida_presupuestaria
            WHERE
              pr_b002_partida_presupuestaria.cod_partida='$codPartida'
        ");
        $impuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $impuesto->fetch();
    }

    public function metCargarPais($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a008_pais
              SET
                fk_a006_num_estado_solicitud=1,ind_pais=:ind_pais, pk_num_pais=:pk_num_pais
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a008_pais', false, "pk_num_pais = '" . $array['idpaises'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_pais' => $array['idpaises'],
                    ':ind_pais' => $array['nombre']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarEstado($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a009_estado
              SET
                fk_a006_num_estado_solicitud=1,fk_a008_num_pais=:fk_a008_num_pais, ind_estado=:ind_estado, pk_num_estado=:pk_num_estado
            ");

        foreach ($arrays as $array) {
            if ($array['idPais'] == '233') {
                $busqueda = $this->metBusquedaSimple('a009_estado', false, "pk_num_estado = '" . $array['idestados'] . "'");
                if (!$busqueda) {
                    $registro->execute(array(
                        ':pk_num_estado' => $array['idestados'],
                        ':ind_estado' => $array['estados'],
                        ':fk_a008_num_pais' => $array['idPais']
                    ));
                }
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarCiudad($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a010_ciudad
              SET
                fk_a006_num_estado_solicitud=1, ind_ciudad=:ind_ciudad,
                pk_num_ciudad=:pk_num_ciudad, ind_cod_postal=0, fk_a011_num_municipio=:fk_a011_num_municipio
            ");

        foreach ($arrays as $array) {
            if ($array['idmunicipio'] == 210) {
                $busqueda = $this->metBusquedaSimple('a010_ciudad', false, "pk_num_ciudad = '1'");
                if (!$busqueda) {
                    $registro->execute(array(
                        ':pk_num_ciudad' => 1,
                        ':ind_ciudad' => $array['municipio'],
                        ':fk_a011_num_municipio' => $array['idmunicipio']
                    ));
                }
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarMunicipio($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a011_municipio
              SET
                fk_a006_num_estado_solicitud=1,fk_a009_num_estado=:fk_a009_num_estado, ind_municipio=:ind_municipio,
                pk_num_municipio=:pk_num_municipio
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a011_municipio', false, "pk_num_municipio = '" . $array['idmunicipio'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_municipio' => $array['idmunicipio'],
                    ':ind_municipio' => $array['municipio'],
                    ':fk_a009_num_estado' => $array['estadoestados']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarParroquia($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a012_parroquia
              SET
                fk_a006_num_estado_solicitud=1, fk_a011_num_municipio=:fk_a011_num_municipio, ind_parroquia=:ind_parroquia,
                pk_num_parroquia=:pk_num_parroquia
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a012_parroquia', false, "pk_num_parroquia = '" . $array['idparroquia'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_parroquia' => $array['idparroquia'],
                    ':ind_parroquia' => $array['parroquia'],
                    ':fk_a011_num_municipio' => $array['municipiomunicipio']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarPersona($arrays)
    {
        $this->_db->query(
            "SET FOREIGN_KEY_CHECKS=0;"
        );
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a003_persona
              SET
                fk_a010_num_ciudad=:fk_a010_num_ciudad, ind_cedula_documento=:ind_cedula_documento,
                ind_documento_fiscal=:ind_documento_fiscal, fk_a006_num_estado_solicitud=1, ind_nombre1=:ind_nombre1, ind_tipo_persona=:ind_tipo_persona,
                pk_num_persona=:pk_num_persona, ind_apellido1=:ind_apellido1
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a003_persona', false, "pk_num_persona = '" . $array['pk_num_persona'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_persona' => $array['pk_num_persona'],
                    ':fk_a010_num_ciudad' => $array['fk_a010_num_ciudad'],
                    ':ind_cedula_documento' => $array['ind_cedula_documento'],
                    ':ind_documento_fiscal' => $array['ind_documento_fiscal'],
                    ':ind_nombre1' => $array['ind_nombre1'],
                    ':ind_apellido1' => $array['ind_apellido1'],
                    ':ind_tipo_persona' => $array['ind_tipo_persona']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarEmpleado($arrays)
    {
        $this->_db->beginTransaction();
        $registroEmpleado = $this->_db->prepare(
            "INSERT INTO
                rh_b001_empleado
              SET
                fec_ultima_modificacion=NOW(), fk_a003_num_persona=:fk_a003_num_persona, fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                ind_estado_aprobacion='ap', fk_a006_num_estado_solicitud=1, pk_num_empleado=:pk_num_empleado
            ");

        $registroUsuario = $this->_db->prepare(
            "INSERT INTO
                a018_seguridad_usuario
              SET
                fec_ultima_modificacion=NOW(), fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fk_a006_num_estado_solicitud=1, fk_rhb001_num_empleado=:fk_rhb001_num_empleado, ind_password=:ind_password,
                ind_template=:ind_template,pk_num_seguridad_usuario=:pk_num_seguridad_usuario,ind_usuario=:ind_usuario
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('rh_b001_empleado', false, "pk_num_empleado = '" . $array['pk_num_empleado'] . "'");
            if (!$busqueda) {
                $registroEmpleado->execute(array(
                    ':pk_num_empleado' => $array['pk_num_empleado'],
                    ':fk_a003_num_persona' => $array['fk_a003_num_persona'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                ));
                $registroUsuario->execute(array(
                    ':pk_num_seguridad_usuario' => $array['pk_num_seguridad_usuario'],
                    ':fk_rhb001_num_empleado' => $array['fk_rhb001_num_empleado'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':ind_password' => $array['ind_password'],
                    ':ind_usuario' => $array['ind_usuario'],
                    ':ind_template' => $array['ind_template']
                ));
            }
        }
        $error = $registroUsuario->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            var_dump($error);
            echo '<br><br>';
            $this->_db->rollBack();
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
        }
        $this->_db->query(
            "SET FOREIGN_KEY_CHECKS=1;"
        );

    }

    public function metCargarAplicacion($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a015_seguridad_aplicacion
              SET
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario, fec_creacion=NOW(), cod_aplicacion=:cod_aplicacion,
                ind_descripcion=:ind_descripcion, ind_nombre_modulo=:ind_nombre_modulo, num_estatus=1,
                pk_num_seguridad_aplicacion=:pk_num_seguridad_aplicacion

            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a015_seguridad_aplicacion', false, "pk_num_seguridad_aplicacion = '" . $array['pk_num_seguridad_aplicacion'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_seguridad_aplicacion' => $array['pk_num_seguridad_aplicacion'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':cod_aplicacion' => $array['cod_aplicacion'],
                    ':ind_descripcion' => mb_strtoupper($array['ind_descripcion'], 'utf-8'),
                    ':ind_nombre_modulo' => mb_strtoupper($array['ind_nombre_modulo'], 'utf-8')
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCargarPerfil($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a017_seguridad_perfil
              SET
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario, ind_descripcion=:ind_descripcion,
                fec_ultima_modificacion=NOW(), fk_a006_num_estado_solicitud=1, ind_nombre_perfil=:ind_nombre_perfil,
                pk_num_seguridad_perfil=:pk_num_seguridad_perfil
        ");

        $registroPerfil = $this->_db->prepare(
            "INSERT INTO
                a020_seguridad_usuarioperfil
              SET
                fk_a018_num_seguridad_usuario=1, fk_a017_num_seguridad_perfil=:fk_a017_num_seguridad_perfil
        ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a017_seguridad_perfil', false, "pk_num_seguridad_perfil = '" . $array['pk_num_seguridad_perfil'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_seguridad_perfil' => $array['pk_num_seguridad_perfil'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':ind_nombre_perfil' => $array['ind_nombre_perfil']
                ));
                $registroPerfil->execute(array(
                    ':fk_a017_num_seguridad_perfil' => $array['pk_num_seguridad_perfil']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCargarMenu($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a027_seguridad_menu
              SET
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario, ind_descripcion=:ind_descripcion,
                fec_ultima_modificacion=NOW(),   cod_interno=:cod_interno, cod_padre=:cod_padre,
                fk_a015_num_seguridad_aplicacion=:fk_a015_num_seguridad_aplicacion, ind_icono=:ind_icono, ind_nombre=:ind_nombre,
                ind_rol=:ind_rol, ind_ruta=:ind_ruta, num_nivel=:num_nivel
            ");
        $registroMenuPerfil = $this->_db->prepare(
            "INSERT INTO
                a028_seguridad_menupermiso
              SET
                fk_a017_num_seguridad_perfil=1, fk_a027_num_seguridad_menu=:fk_a027_num_seguridad_menu
            ");
        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a027_seguridad_menu', false, "cod_interno = '" . $array['cod_interno'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':num_nivel' => $array['num_nivel'],
                    ':ind_ruta' => $array['ind_ruta'],
                    ':ind_rol' => $array['ind_rol'],
                    ':ind_nombre' => mb_strtoupper($array['ind_nombre'], 'utf-8'),
                    ':ind_icono' => $array['ind_icono'],
                    ':fk_a015_num_seguridad_aplicacion' => $array['fk_a015_num_seguridad_aplicacion'],
                    ':cod_padre' => mb_strtoupper($array['cod_padre'], 'utf-8'),
                    ':cod_interno' => mb_strtoupper($array['cod_interno'], 'utf-8'),
                    ':ind_descripcion' => mb_strtoupper($array['ind_descripcion'], 'utf-8'),
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                ));
                $idMenu = $this->_db->lastInsertId();
                $registroMenuPerfil->execute(array(
                    ':fk_a027_num_seguridad_menu' => $idMenu
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCargarMiscelaneos($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a005_miscelaneo_maestro
              SET
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario, ind_descripcion=:ind_descripcion,
                fec_ultima_modificacion=NOW(),  num_estatus=1,cod_maestro=:cod_maestro,
                fk_a015_num_seguridad_aplicacion=:fk_a015_num_seguridad_aplicacion, ind_nombre_maestro=:ind_nombre_maestro
            ");

        $registroDetalle = $this->_db->prepare(
            "INSERT INTO
                a006_miscelaneo_detalle
              SET
                cod_detalle=:cod_detalle, fk_a005_num_miscelaneo_maestro=:fk_a005_num_miscelaneo_maestro ,
                ind_nombre_detalle=:ind_nombre_detalle
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a005_miscelaneo_maestro', false, "cod_maestro = '" . $array['cod_maestro'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':cod_maestro' => $array['cod_maestro'],
                    ':fk_a015_num_seguridad_aplicacion' => $array['fk_a015_num_seguridad_aplicacion'],
                    ':ind_nombre_maestro' => $array['ind_nombre_maestro']
                ));
                $idMiscelanneo = $this->_db->lastInsertId();
                foreach ($array['Det'] as $arrayDet) {
                    $registroDetalle->execute(array(
                        ':fk_a005_num_miscelaneo_maestro' => $idMiscelanneo,
                        ':cod_detalle' => $arrayDet['cod_detalle'],


                        ':ind_nombre_detalle' => $arrayDet['ind_nombre_detalle']
                    ));
                }
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metMostrarSelect($codMaestro, $codDetalle)
    {
        $miscelaneoDetalle = $this->_db->query("
            SELECT
              a006.pk_num_miscelaneo_detalle
            FROM
              a006_miscelaneo_detalle AS a006
            INNER JOIN
              a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
            WHERE
              a005.cod_maestro='$codMaestro' AND
              a006.cod_detalle='$codDetalle'
        ");
        $miscelaneoDetalle->setFetchMode(PDO::FETCH_ASSOC);
        return $miscelaneoDetalle->fetch();
    }




    #### PARQUE AUTOMOTOR
    public function metTipoVehiculo($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                pa_b009_tipo_vehiculo
              SET
                ind_descripcion=:ind_descripcion,
                fk_a006_num_clase_vehiculo=:fk_a006_num_clase_vehiculo,
                fec_ultima_modificacion=:fec_ultima_modificacion,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('pa_b009_tipo_vehiculo', false, "ind_descripcion = '" . $array['ind_descripcion'] . "'");
            if (!$busqueda) {

                $registro->execute(array(
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':fk_a006_num_clase_vehiculo' => $array['fk_a006_num_clase_vehiculo'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':fec_ultima_modificacion' => date('Y-m-d')

                ));
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }



}

