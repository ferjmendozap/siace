<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class solicitudModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener("idUsuario");
        $this->atIdEmpleado = Session::metObtener("idEmpleado");
    }

    #metodo para obtener todos los registros guardados de los post
    public function metGetSolicitud()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(" SELECT
        pa_d001_solicitud_vehiculo.pk_num_solicitud,
        DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_requerida,'%d/%m/%Y') as fec_requerida,
        DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_hasta,'%d/%m/%Y') as fec_hasta,
        DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_requerida,'%h:%i %p') as hora_requerida,
        DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_hasta,'%h:%i %p') as hora_hasta,

        pa_d001_solicitud_vehiculo.fk_rhb001_empleado_solicitante ,
        pa_d001_solicitud_vehiculo.fk_rhb001_empleado_conformado,
        pa_d001_solicitud_vehiculo.fec_aprobado,
        pa_d001_solicitud_vehiculo.fk_a006_num_estado_solicitud,
        pa_d001_solicitud_vehiculo.ind_observacion,
        pa_d001_solicitud_vehiculo.ind_motivo_rechazo,
        pa_d001_solicitud_vehiculo.fk_a018_num_seguridad_usuario,
        pa_d001_solicitud_vehiculo.fec_ultima_modificacion,
        pa_d001_solicitud_vehiculo.fk_a006_num_motivo_salida,
        pa_d001_solicitud_vehiculo.fk_a006_num_tipo_salida,
        pa_d001_solicitud_vehiculo.fk_pab001_num_vehiculo,
         a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,

         a006_miscelaneo_detalle.ind_nombre_detalle,

        rh_b001_empleado.fk_a003_num_persona,
        a003_persona.ind_nombre1,
        a003_persona.ind_apellido1

     FROM  `pa_d001_solicitud_vehiculo`

      left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='ESTADOSOL')
        left join a006_miscelaneo_detalle on (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND pa_d001_solicitud_vehiculo.fk_a006_num_estado_solicitud=a006_miscelaneo_detalle.cod_detalle )

     left join rh_b001_empleado on (rh_b001_empleado.pk_num_empleado=pa_d001_solicitud_vehiculo.fk_rhb001_empleado_solicitante)
     left join a003_persona on (a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona)

"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }


    public function metGetSolicitudFiltros($estatus,$motivo,$tipo)
    {

        if($estatus!=""){
            $complemento1=" AND pa_d001_solicitud_vehiculo.fk_a006_num_estado_solicitud='".$estatus."'";
        }else{
            $complemento1=" ";
        }

        if($motivo!=""){
            $complemento2=" AND pa_d001_solicitud_vehiculo.fk_a006_num_motivo_salida='".$motivo."'";
        }else{
            $complemento2=" ";
        }

        if($tipo!=""){
            $complemento3=" AND pa_d001_solicitud_vehiculo.fk_a006_num_tipo_salida='".$tipo."'";
        }else{
            $complemento3=" ";
        }

        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query("SELECT
        pa_d001_solicitud_vehiculo.pk_num_solicitud,
        DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_requerida,'%d/%m/%Y') as fec_requerida,
        DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_hasta,'%d/%m/%Y') as fec_hasta,
        DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_requerida,'%h:%i %p') as hora_requerida,
        DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_hasta,'%h:%i %p') as hora_hasta,

        pa_d001_solicitud_vehiculo.fk_rhb001_empleado_solicitante ,
        pa_d001_solicitud_vehiculo.fk_rhb001_empleado_conformado,
        pa_d001_solicitud_vehiculo.fec_aprobado,
        pa_d001_solicitud_vehiculo.fk_a006_num_estado_solicitud,
        pa_d001_solicitud_vehiculo.ind_observacion,
        pa_d001_solicitud_vehiculo.ind_motivo_rechazo,
        pa_d001_solicitud_vehiculo.fk_a018_num_seguridad_usuario,
        pa_d001_solicitud_vehiculo.fec_ultima_modificacion,
        pa_d001_solicitud_vehiculo.fk_a006_num_motivo_salida,
        pa_d001_solicitud_vehiculo.fk_a006_num_tipo_salida,
        pa_d001_solicitud_vehiculo.fk_pab001_num_vehiculo,
        a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,

        a006_miscelaneo_detalle.ind_nombre_detalle,

        rh_b001_empleado.fk_a003_num_persona,
        a003_persona.ind_nombre1,
        a003_persona.ind_apellido1

     FROM  `pa_d001_solicitud_vehiculo`

     left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='ESTADOSOL')
     left join a006_miscelaneo_detalle on (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND pa_d001_solicitud_vehiculo.fk_a006_num_estado_solicitud=a006_miscelaneo_detalle.cod_detalle )


     left join rh_b001_empleado on (rh_b001_empleado.pk_num_empleado=pa_d001_solicitud_vehiculo.fk_rhb001_empleado_solicitante)
     left join a003_persona on (a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona)
     WHERE 1 $complemento1 $complemento2 $complemento3
"
        );



        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }


	#metodo para obtener todos los registros guardados de los post
    public function metGetSolicitudes($estatus)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT
        pa_d001_solicitud_vehiculo.pk_num_solicitud,
         DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_requerida,'%d/%m/%Y') as fec_requerida,
         DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_hasta,'%d/%m/%Y') as fec_hasta,

         DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_requerida,'%h:%i %p') as hora_requerida,
         DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_hasta,'%h:%i %p') as hora_hasta,

        pa_d001_solicitud_vehiculo.fk_rhb001_empleado_solicitante ,
        pa_d001_solicitud_vehiculo.fk_rhb001_empleado_conformado,
        pa_d001_solicitud_vehiculo.fec_aprobado,
        pa_d001_solicitud_vehiculo.fk_a006_num_estado_solicitud,
        pa_d001_solicitud_vehiculo.ind_observacion,
        pa_d001_solicitud_vehiculo.ind_motivo_rechazo,
        pa_d001_solicitud_vehiculo.fk_a018_num_seguridad_usuario,
        pa_d001_solicitud_vehiculo.fec_ultima_modificacion,
        pa_d001_solicitud_vehiculo.fk_a006_num_motivo_salida,
        pa_d001_solicitud_vehiculo.fk_a006_num_tipo_salida,
        pa_d001_solicitud_vehiculo.fk_pab001_num_vehiculo,
        pa_d001_solicitud_vehiculo.fk_pab001_num_vehiculo as fk_pab001_num_vehiculo_solicitud ,
        pa_b001_vehiculo.ind_modelo,
        pa_b001_vehiculo.ind_placa,
                a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,
        a006_miscelaneo_detalle.ind_nombre_detalle,
        rh_b001_empleado.fk_a003_num_persona,
        a003_persona.ind_nombre1,
        a003_persona.ind_apellido1,

        conforma.fk_a003_num_persona,
        persona_conforma.ind_nombre1 as nombre_conformado,
        persona_conforma.ind_apellido1 as apellido_conformado

        FROM `pa_d001_solicitud_vehiculo`

        left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='ESTADOSOL')
        left join a006_miscelaneo_detalle on (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND pa_d001_solicitud_vehiculo.fk_a006_num_estado_solicitud=a006_miscelaneo_detalle.cod_detalle )

        left join pa_b001_vehiculo ON (pa_b001_vehiculo.pk_num_vehiculo=pa_d001_solicitud_vehiculo.fk_pab001_num_vehiculo)
        left join rh_b001_empleado on (rh_b001_empleado.pk_num_empleado=pa_d001_solicitud_vehiculo.fk_rhb001_empleado_solicitante)
        left join a003_persona on (a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona)

        left join rh_b001_empleado as conforma on (conforma.pk_num_empleado=pa_d001_solicitud_vehiculo.fk_rhb001_empleado_conformado)
        left join a003_persona as persona_conforma on (persona_conforma.pk_num_persona=conforma.fk_a003_num_persona)


        where  pa_d001_solicitud_vehiculo.fk_a006_num_estado_solicitud='".$estatus."'      "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }


    #metodo para guardar el registro del post
    public function metSetSolicitud($desde,$hasta,$estatus,$observacion,$motivo,$salida)
    {



          #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoPost=$this->_db->prepare(
            "insert into  
                        pa_d001_solicitud_vehiculo 
                      (fec_requerida,
                      fec_hasta,
                      fk_rhb001_empleado_solicitante,
                      fec_preparacion,
                      fk_a006_num_estado_solicitud,
                      ind_observacion,
                      fk_a006_num_motivo_salida,
                      fk_a006_num_tipo_salida,
                      fk_a018_num_seguridad_usuario,
                      fec_ultima_modificacion)
                      values ( :fec_requerida,:fec_hasta,:fk_rhb001_empleado_solicitante,:fec_preparacion,:fk_a006_num_estado_solicitud,:ind_observacion,:fk_a006_num_motivo_salida,:fk_a006_num_tipo_salida,:fk_a018_num_seguridad_usuario,:fec_ultima_modificacion )"
        );


        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(
            ':fec_requerida' => $desde,
            ':fec_hasta' => $hasta,
            ':fec_preparacion' => date('Y-m-d H:i:s'),
            ':fk_a006_num_estado_solicitud' => $estatus,
            ':ind_observacion' => $observacion,
            ':fk_a006_num_motivo_salida' => $motivo,
            ':fk_a006_num_tipo_salida' => $salida,
            ':fk_rhb001_empleado_solicitante' =>$this->atIdEmpleado,
            ':fk_a018_num_seguridad_usuario' =>$this->atIdUsuario,
            ':fec_ultima_modificacion' =>date('Y-m-d H:i:s'),

        ));
        $idPost= $this->_db->lastInsertId();
        #commit — Consigna una transacción
        $this->_db->commit();
        return $idPost;
    }
	
	public function metMostrarEstatus($id)
    {


        #ejecuto la consulta a la base de datos

        $pruebaPost =  $this->_db->query(
        "SELECT
 
        pa_d001_solicitud_vehiculo.fk_a006_num_estado_solicitud
 

        FROM `pa_d001_solicitud_vehiculo`

        where  pk_num_solicitud='".$id."'"
        );

     
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }
	

    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrar($id)
    {


        #ejecuto la consulta a la base de datos

        $pruebaPost =  $this->_db->query(
        "SELECT
        pa_d001_solicitud_vehiculo.pk_num_solicitud,
        DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_requerida,'%d/%m/%Y') as fec_requerida,
        DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_hasta,'%d/%m/%Y') as fec_hasta,

        DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_requerida,'%h:%i %p') as hora_requerida,
        DATE_FORMAT(pa_d001_solicitud_vehiculo.fec_hasta,'%h:%i %p') as hora_hasta,

        DATE_FORMAT(pa_e001_salida_vehiculo.fec_hora_salida,'%d/%m/%Y') as fec_hora_salida,
        DATE_FORMAT(pa_e001_salida_vehiculo.fec_hora_hasta,'%d/%m/%Y') as fec_hora_hasta,

        DATE_FORMAT(pa_e001_salida_vehiculo.fec_hora_salida,'%h:%i %p') as hora_salida,
        DATE_FORMAT(pa_e001_salida_vehiculo.fec_hora_hasta,'%h:%i %p') as hora_hasta2,

        pa_d001_solicitud_vehiculo.fk_rhb001_empleado_solicitante ,
        pa_d001_solicitud_vehiculo.fk_rhb001_empleado_conformado,
        pa_d001_solicitud_vehiculo.fec_aprobado,
        pa_d001_solicitud_vehiculo.fk_a006_num_estado_solicitud,
        pa_d001_solicitud_vehiculo.ind_observacion,
        pa_d001_solicitud_vehiculo.ind_motivo_rechazo,
        pa_d001_solicitud_vehiculo.fk_a018_num_seguridad_usuario,
        pa_d001_solicitud_vehiculo.fec_ultima_modificacion,
        pa_d001_solicitud_vehiculo.fk_a006_num_motivo_salida,
        pa_d001_solicitud_vehiculo.fk_a006_num_tipo_salida,
        pa_d001_solicitud_vehiculo.fk_rhb001_empleado_conformado,
        pa_d001_solicitud_vehiculo.fk_pab001_num_vehiculo as fk_pab001_num_vehiculo_solicitud,
        pa_b001_vehiculo.ind_placa,
        pa_b001_vehiculo.ind_url_foto_frontal,
        pa_e001_salida_vehiculo.ind_kilometraje,
        pa_e001_salida_vehiculo.fk_pab001_num_vehiculo,
        pa_e001_salida_vehiculo.fk_pab012_num_chofer,
        pa_e001_salida_vehiculo.pk_num_salida_vehiculo,
        pa_e001_salida_vehiculo.ind_observacion as observacionSalida,
        rh_b001_empleado.fk_a003_num_persona,
        empleado_conformado.fk_a003_num_persona,
        empleado_aprobado.fk_a003_num_persona,
        persona_aprobado.ind_nombre1 as ind_nombre_aprobado,
        persona_aprobado.ind_apellido1 as ind_apellido_aprobado,
        persona_conformado.ind_nombre1 as ind_nombre_conformado,
        persona_conformado.ind_apellido1 as ind_apellido_conformado,
        a003_persona.ind_nombre1,
        a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,
        a006_miscelaneo_detalle.ind_nombre_detalle,
        a003_persona.ind_apellido1

        FROM `pa_d001_solicitud_vehiculo`

        left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='ESTADOSOL')
        left join a006_miscelaneo_detalle on (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND pa_d001_solicitud_vehiculo.fk_a006_num_estado_solicitud=a006_miscelaneo_detalle.cod_detalle )

        left join pa_e001_salida_vehiculo on (pa_e001_salida_vehiculo.fk_pad001_num_solicitud_vehiculo=pa_d001_solicitud_vehiculo.pk_num_solicitud)
        left join rh_b001_empleado on (rh_b001_empleado.pk_num_empleado=pa_d001_solicitud_vehiculo.fk_rhb001_empleado_solicitante)
        left join a003_persona on (a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona)
        left join rh_b001_empleado as empleado_conformado on (empleado_conformado.pk_num_empleado=pa_d001_solicitud_vehiculo.fk_rhb001_empleado_conformado)
        left join a003_persona  as persona_conformado on (persona_conformado.pk_num_persona=empleado_conformado.fk_a003_num_persona)

        left join rh_b001_empleado as empleado_aprobado on (empleado_aprobado.pk_num_empleado=pa_d001_solicitud_vehiculo.fk_rhb001_empleado_aprobado)
        left join a003_persona  as persona_aprobado on (persona_aprobado.pk_num_persona=empleado_aprobado.fk_a003_num_persona)
        left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_d001_solicitud_vehiculo.fk_pab001_num_vehiculo)
        where  pk_num_solicitud='".$id."'"
        );

     
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }


    public function metUpdateSolicitud($fec_requerida,$fec_hasta ,$estatus,$observacion,$motivo,$tipo ,$id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto

        $modificar=$this->_db->prepare(
            "UPDATE pa_d001_solicitud_vehiculo
              SET
             fec_requerida=:fec_requerida,
             fec_hasta=:fec_hasta,
             fk_a006_num_estado_solicitud=:fk_a006_num_estado_solicitud,
             ind_observacion=:ind_observacion,
             fk_a006_num_motivo_salida=:fk_a006_num_motivo_salida,
             fk_a006_num_tipo_salida=:fk_a006_num_tipo_salida,

             fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
             fec_ultima_modificacion=:fec_ultima_modificacion


             WHERE pk_num_solicitud='".$id."'"
        );
        $modificar->execute(array(
            ':fec_requerida' => $fec_requerida,
            ':fec_hasta' => $fec_hasta,
            ':fk_a006_num_estado_solicitud' =>$estatus,
            ':ind_observacion' => $observacion,
            ':fk_a006_num_motivo_salida' => $motivo,
            ':fk_a006_num_tipo_salida' =>$tipo,

            ':fk_a018_num_seguridad_usuario' =>$this->atIdUsuario,
            ':fec_ultima_modificacion' =>date('Y-m-d H:i:s'),


        ));



        #commit — Consigna una transacción
        $this->_db->commit();
        return $id;
    }

    public function metEstatusSolicitud($motivo,$estatus  ,$id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "update `pa_d001_solicitud_vehiculo` set   fk_a006_num_estado_solicitud = '$estatus', ind_motivo_rechazo = '$motivo',fk_a018_num_seguridad_usuario='".$this->atIdUsuario."', fec_ultima_modificacion=NOW()  " .
            "where pk_num_solicitud= '$id'"
        );


        #commit — Consigna una transacción
        $this->_db->commit();
    }


    #metodo para mostrar las disponibilidad de un vehiculo por rango de fechas
    public function metDisponibilidad($desde,$hasta,$id,$idsolicitud)
    {

        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "
            SELECT * FROM `pa_d001_solicitud_vehiculo`
                WHERE  fk_pab001_num_vehiculo='".$id."' AND ((fec_requerida BETWEEN '".$desde."' AND '".$hasta."')
                OR
                             (fec_hasta BETWEEN '".$desde."' AND '".$hasta."')
                OR
                             (fec_requerida <= '".$desde."' AND fec_hasta >= '".$hasta."') ) AND (pa_d001_solicitud_vehiculo.pk_num_solicitud !='".$idsolicitud."' AND fk_a006_num_estado_solicitud>1 AND fk_a006_num_estado_solicitud<7 )
            "
        );


        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }
    #metodo para asignar un vehiculo momentaneamente y subir el estatus a verificado
    public function metGuardarDisponibilidad($vehiculo,$estatus  ,$id,$fec_requerida,$fec_hasta,$observacion)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto

        $modificar=$this->_db->prepare(
            "UPDATE pa_d001_solicitud_vehiculo
              SET
             fec_requerida=:fec_requerida,
             fec_hasta=:fec_hasta,
             fk_a006_num_estado_solicitud=:fk_a006_num_estado_solicitud,
             ind_observacion=:ind_observacion,
             fk_pab001_num_vehiculo=:fk_pab001_num_vehiculo,
             fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
             fec_ultima_modificacion=:fec_ultima_modificacion


             WHERE pk_num_solicitud='".$id."'"
        );
        $modificar->execute(array(
            ':fec_requerida' => $fec_requerida,
            ':fec_hasta' => $fec_hasta,
            ':fk_a006_num_estado_solicitud' =>$estatus,
            ':ind_observacion' => $observacion,
            ':fk_pab001_num_vehiculo' =>$vehiculo,
            ':fk_a018_num_seguridad_usuario' =>$this->atIdUsuario,
            ':fec_ultima_modificacion' =>date('Y-m-d H:i:s'),
        ));

        #commit — Consigna una transacción
        $this->_db->commit();
    }

    #metodo para asignar un vehiculo momentaneamente y subir el estatus a verificado
    public function metConformar( $estatus  ,$id,$fec_requerida,$fec_hasta,$observacion)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto

        $modificar=$this->_db->prepare(
            "UPDATE pa_d001_solicitud_vehiculo
              SET
             fec_requerida=:fec_requerida,
             fec_hasta=:fec_hasta,
             fk_a006_num_estado_solicitud=:fk_a006_num_estado_solicitud,
             ind_observacion=:ind_observacion,
    
             fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
             fec_ultima_modificacion=:fec_ultima_modificacion,
             fec_conformado=:fec_conformado,
             fk_rhb001_empleado_conformado=:fk_rhb001_empleado_conformado


             WHERE pk_num_solicitud='".$id."'"
        );
        $modificar->execute(array(
            ':fec_requerida' => $fec_requerida,
            ':fec_hasta' => $fec_hasta,
            ':fk_a006_num_estado_solicitud' =>$estatus,
            ':ind_observacion' => $observacion,

            ':fk_a018_num_seguridad_usuario' =>$this->atIdUsuario,
            ':fec_ultima_modificacion' =>date('Y-m-d H:i:s'),
            ':fk_rhb001_empleado_conformado' =>$this->atIdEmpleado,
            ':fec_conformado' =>date('Y-m-d H:i:s'),
        ));

        #commit — Consigna una transacción
        $this->_db->commit();
    }

    public function metAprobar( $estatus  ,$id,$fec_requerida,$fec_hasta,$observacion)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto

        $modificar=$this->_db->prepare(
            "UPDATE pa_d001_solicitud_vehiculo
              SET
             fec_requerida=:fec_requerida,
             fec_hasta=:fec_hasta,
             fk_a006_num_estado_solicitud=:fk_a006_num_estado_solicitud,
             ind_observacion=:ind_observacion,
       
             fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
             fec_ultima_modificacion=:fec_ultima_modificacion,
             fec_aprobado=:fec_aprobado,
             fk_rhb001_empleado_aprobado=:fk_rhb001_empleado_aprobado


             WHERE pk_num_solicitud='".$id."'"
        );
        $modificar->execute(array(
            ':fec_requerida' => $fec_requerida,
            ':fec_hasta' => $fec_hasta,
            ':fk_a006_num_estado_solicitud' =>$estatus,
            ':ind_observacion' => $observacion,

            ':fk_a018_num_seguridad_usuario' =>$this->atIdUsuario,
            ':fec_ultima_modificacion' =>date('Y-m-d H:i:s'),
            ':fk_rhb001_empleado_aprobado'=>$this->atIdEmpleado,
            ':fec_aprobado' =>date('Y-m-d H:i:s'),
        ));

        #commit — Consigna una transacción
        $this->_db->commit();
    }


    #metodo para mostrar todos las dependencias
    public function metGetDependencias()
    {


        #ejecuto la consulta a la base de datos

        $pruebaPost =  $this->_db->query(
        "SELECT pk_num_dependencia,ind_dependencia

        FROM `a004_dependencia`"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    public function metRevisarMantenimiento($id)
    {

        $pruebaPost =  $this->_db->query(
            "SELECT
                          pa_d001_solicitud_vehiculo.fk_pab001_num_vehiculo,
                          pa_e002_mantenimiento.pk_num_mantenimiento,
                          pa_e002_mantenimiento.fec_proximo_mantenimiento
                        FROM
                          pa_d001_solicitud_vehiculo
                        LEFT JOIN
                          pa_e002_mantenimiento ON(
                            pa_e002_mantenimiento.fk_pab001_num_vehiculo = pa_d001_solicitud_vehiculo.fk_pab001_num_vehiculo
                          )
                        LEFT JOIN 
                          a006_miscelaneo_detalle ON(
                            pa_e002_mantenimiento.fk_a006_num_tipo_mantenimiento = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
                          )
                        LEFT JOIN 
                          a005_miscelaneo_maestro ON(
                            a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro = a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND 
                            a005_miscelaneo_maestro.cod_maestro = 'TIPOMANT'
                          )
                        WHERE
                          pa_d001_solicitud_vehiculo.pk_num_solicitud = '".$id."'
                        ORDER BY
                          pk_num_mantenimiento DESC
                        LIMIT 0, 1"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();



    }

    public function metDeleteSolicitud($id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "delete from `pa_d001_solicitud_vehiculo` " .
            "where pk_num_solicitud= '$id'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }



}
