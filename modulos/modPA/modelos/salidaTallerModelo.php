<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class salidaTallerModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener("idUsuario");
    }

    #metodo para obtener todos los registros guardados de los post
    public function metGetSalidaTaller()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT
             DATE_FORMAT(pa_e007_salida_taller.fec_hora  ,'%d/%m/%Y') as fec_salida,
             DATE_FORMAT(pa_e007_salida_taller.fec_hora  ,'%h:%i %p') as hora,
             pa_e007_salida_taller.pk_num_salida_taller,
             pa_e006_entrada_taller.fk_pab001_num_vehiculo,
             pa_b001_vehiculo.ind_modelo,
             pa_b001_vehiculo.ind_placa,
             pa_b001_vehiculo.ind_url_foto_frontal,
              pa_e007_salida_taller.num_monto,

             pa_e007_salida_taller.ind_observacion
             FROM `pa_e007_salida_taller`


             left join pa_e006_entrada_taller on (pa_e006_entrada_taller.pk_num_entrada_taller=pa_e007_salida_taller.fk_pae006_num_entrada_taller)
             left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e006_entrada_taller.fk_pab001_num_vehiculo  )




             "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para guardar el registro del post
    public function metSetSalidaTaller($hora,$condiciones,$kilometraje,$monto,$observacion,$entrada,$vehiculo)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoPost=$this->_db->prepare(
            "insert into  `pa_e007_salida_taller`    (fec_hora,ind_kilometraje,num_monto,ind_condiciones,ind_observacion,fk_a018_num_seguridad_usuario,fec_ultima_modificacion,fk_pae006_num_entrada_taller) values
                                                     ( :fec_hora,:ind_kilometraje,:num_monto,:ind_condiciones,:ind_observacion,:fk_a018_num_seguridad_usuario,:fec_ultima_modificacion,:fk_pae006_num_entrada_taller)"
        );

        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(
            ':fec_hora' => $hora,
            ':num_monto' =>$monto,
            ':ind_kilometraje' =>$kilometraje,
            ':ind_condiciones' => $condiciones,
            ':ind_observacion' => $observacion,
            ':fk_a018_num_seguridad_usuario' =>$this->atIdUsuario,
            ':fec_ultima_modificacion' =>date('Y-m-d H:i:s'),
            ':fk_pae006_num_entrada_taller'=>$entrada,




        ));

       $idPost= $this->_db->lastInsertId();

        $this->_db->query(
            "update `pa_b001_vehiculo` set num_estatus='1' where pk_num_vehiculo='$vehiculo'"
        );

        $this->_db->commit();





        return $idPost;
    }


    #metodo para modificar el registro del post
    public function metUpdateSalidaTaller($hora,$condiciones,$kilometraje,$observacion,$monto,$id)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia

        $modificar=$this->_db->prepare(
            "UPDATE pa_e007_salida_taller
              SET
             fec_hora=:fec_hora,
             ind_condiciones=:ind_condiciones,
             ind_kilometraje=:ind_kilometraje,
             num_monto=:num_monto,
             ind_observacion=:ind_observacion,
             fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
             fec_ultima_modificacion=:fec_ultima_modificacion


             WHERE pk_num_salida_taller='".$id."'"
        );
        $modificar->execute(array(
            ':fec_hora' => $hora,
            ':ind_condiciones' => $condiciones,
            ':ind_kilometraje' =>$kilometraje,
            ':num_monto' => $monto,
            ':ind_observacion' => $observacion,
            ':fk_a018_num_seguridad_usuario' =>$this->atIdUsuario,
            ':fec_ultima_modificacion' =>date('Y-m-d H:i:s'),

        ));

 


        #commit — Consigna una transacción
        $this->_db->commit();


        return $id;
    }



    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrar($id)
    {

        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT
             DATE_FORMAT(pa_e006_entrada_taller.fec_hora  ,'%d/%m/%Y') as fec_entrada,
             DATE_FORMAT(pa_e006_entrada_taller.fec_hora  ,'%h:%i %p') as hora,
             pa_e006_entrada_taller.fk_pab001_vehiculo,
             pa_e006_entrada_taller.pk_num_entrada_taller,
             pa_e006_entrada_taller.ind_kilometraje,
             pa_e006_entrada_taller.fk_pab012_chofer,
             pa_e006_entrada_taller.ind_motivo,
             pa_b001_vehiculo.ind_modelo,
             pa_b001_vehiculo.ind_placa,
             pa_b012_chofer.fk_rhb001_num_empleado,
             pa_e006_entrada_taller.fk_a003_persona,
             pa_e006_entrada_taller.ind_observacion,
             a003_persona.ind_nombre1
             FROM `pa_e006_entrada_taller`

left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e006_entrada_taller.fk_pab001_vehiculo)
left join pa_b012_chofer on   (pa_b012_chofer.pk_num_chofer=pa_e006_entrada_taller.fk_pab012_chofer)
left join a003_persona on     (a003_persona.pk_num_persona=pa_e006_entrada_taller.fk_a003_persona) where   pk_num_entrada_taller='".$id."'"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrarModificar($id)
    {

        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT
             pa_e007_salida_taller.pk_num_salida_taller,
             DATE_FORMAT(pa_e007_salida_taller.fec_hora,'%d/%m/%Y') as fec_entrada,
             DATE_FORMAT(pa_e007_salida_taller.fec_hora,'%h:%i %p') as hora,
             pa_e007_salida_taller.ind_kilometraje,
             pa_e007_salida_taller.num_monto,
             pa_e007_salida_taller.ind_condiciones,
             pa_e007_salida_taller.ind_observacion,
             pa_e006_entrada_taller.fk_pab001_num_vehiculo,
             pa_b001_vehiculo.ind_modelo,
             pa_b001_vehiculo.ind_placa,
             pa_e007_salida_taller.fk_pae006_num_entrada_taller


            FROM pa_e007_salida_taller
            left join pa_e006_entrada_taller on (pa_e006_entrada_taller.pk_num_entrada_taller= pa_e007_salida_taller.fk_pae006_num_entrada_taller)
            left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e006_entrada_taller.fk_pab001_num_vehiculo)
            
            where pa_e007_salida_taller.pk_num_salida_taller='".trim($id)."'" );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }



    public function metDeleteSalidaTaller($id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "delete from `pa_e007_salida_taller` " .
            "where pk_num_salida_taller= '$id'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }






}
