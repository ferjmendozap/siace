<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class solucionDannioModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener("idUsuario");
    }

    #metodo para obtener todos los registros guardados de los post
    public function metGetSolucionDannio()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT

             pa_e008_solucion_averia.ind_observacion,
             DATE_FORMAT(pa_e008_solucion_averia.fec_solucion,'%d/%m/%Y') as fec_solucion,
             pa_e008_solucion_averia.pk_num_solucion,

             pa_e008_solucion_averia.num_monto,
             pa_e004_averia_vehiculo.fk_pab001_num_vehiculo,
             pa_b001_vehiculo.ind_modelo as vehiculo,
             pa_b001_vehiculo.ind_placa,
             pa_e008_solucion_averia.fk_pae004_num_averia_vehiculo

             FROM `pa_e008_solucion_averia`
             left join  pa_e004_averia_vehiculo on (pa_e004_averia_vehiculo.pk_num_averia_vehiculo=pa_e008_solucion_averia.fk_pae004_num_averia_vehiculo)
             left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e004_averia_vehiculo.fk_pab001_num_vehiculo)"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }



    #metodo para guardar el registro del post
    public function metSetSolucionDannio( $fecha,$monto ,$dannio,$observacion)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        if($dannio) {

            $Dannios = $this->_db->prepare(
                "insert into  pa_e008_solucion_averia  (fk_pae004_num_averia_vehiculo, num_monto,fec_solucion,fk_a018_num_seguridad_usuario,fec_ultima_modificacion,ind_observacion)values (:fk_pae004_num_averia_vehiculo, :num_monto,:fec_solucion,:fk_a018_num_seguridad_usuario,:fec_ultima_modificacion,:ind_observacion)"
            );
            
            for ($i = 0; $i < count($dannio); $i++) {

                $Dannios->execute(array(
                    ':fk_pae004_num_averia_vehiculo' => $dannio[$i],
                    ':num_monto' => $monto,
                    ':fec_solucion' => $fecha,
                    ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario ,
                    ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
                    ':ind_observacion' => $observacion,
                ));

                $ActualizarDannio = $this->_db->prepare(
                    "UPDATE pa_e004_averia_vehiculo
                     SET   ind_estado=:ind_estado WHERE pk_num_averia_vehiculo='".$dannio[$i]."'"
                );
                $ActualizarDannio->execute(array(
                    ':ind_estado' => "1",

                ));

            }
        }

        $idPost= $this->_db->lastInsertId();


        #commit — Consigna una transacción
        $this->_db->commit();




        return $idPost;
    }


    #metodo para modificar el registro del post
    public function metUpdateSolucionDannio($observacion,$fecha ,$monto,$dannio,$idSolucion)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia

        $modificar=$this->_db->prepare(
            "UPDATE pa_e008_solucion_averia
               SET
               num_monto=:num_monto,
               fec_solucion=:fec_solucion,
               fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
               fec_ultima_modificacion=:fec_ultima_modificacion,
               ind_observacion=:ind_observacion,
               fk_pae004_num_averia_vehiculo=:fk_pae004_num_averia_vehiculo
               WHERE pk_num_solucion='".$idSolucion."'"
        );
        $modificar->execute(array(
            ':num_monto' =>  $monto,
            ':fec_solucion' => $fecha,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
            ':fk_pae004_num_averia_vehiculo'=> $dannio[0],
            ':ind_observacion'=> $observacion
        ));



        $this->_db->commit();


        return 1;
    }



    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrar($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT
             pa_e008_solucion_averia.ind_observacion,

             DATE_FORMAT( pa_e008_solucion_averia.fec_solucion,'%d/%m/%Y') as fec_solucion,
             pa_e008_solucion_averia.pk_num_solucion,

             pa_e008_solucion_averia.num_monto ,
             pa_e004_averia_vehiculo.fk_pab001_num_vehiculo,
             pa_b001_vehiculo.ind_modelo , 
             pa_b001_vehiculo.ind_placa,

             pa_e008_solucion_averia.fk_pae004_num_averia_vehiculo
             FROM `pa_e008_solucion_averia`
             left join  pa_e004_averia_vehiculo on (pa_e004_averia_vehiculo.pk_num_averia_vehiculo=pa_e008_solucion_averia.fk_pae004_num_averia_vehiculo)
             left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e004_averia_vehiculo.fk_pab001_num_vehiculo)
             WHERE pk_num_solucion='".$id."'"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetch();
    }


    public function metGetSolucionDannioModificar($vehiculo, $id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT
             pa_e004_averia_vehiculo.pk_num_averia_vehiculo,
             pa_e008_solucion_averia.pk_num_solucion,
             pa_b001_vehiculo.ind_modelo,
             pa_b001_vehiculo.ind_placa,
             pa_b001_vehiculo.pk_num_vehiculo,
             DATE_FORMAT(pa_e004_averia_vehiculo.fec_registro ,'%d/%m/%Y') as fec_registro,
             pa_e004_averia_vehiculo.ind_observacion_averia,
             pa_e004_averia_vehiculo.ind_estado,
             pa_b007_pieza_vehiculo.ind_descripcion  as pieza
             FROM  pa_e008_solucion_averia
             left join  pa_e004_averia_vehiculo on       ((pa_e008_solucion_averia.fk_pae004_num_averia_vehiculo=pa_e004_averia_vehiculo.pk_num_averia_vehiculo) or pa_e004_averia_vehiculo.fk_pab001_num_vehiculo='".$vehiculo."' and pa_e004_averia_vehiculo.ind_estado='0')
             left join pa_b001_vehiculo on (pa_b001_vehiculo.pk_num_vehiculo=pa_e004_averia_vehiculo.fk_pab001_num_vehiculo)
             left join pa_b007_pieza_vehiculo on (pa_b007_pieza_vehiculo.pk_num_piezas=pa_e004_averia_vehiculo.fk_pab007_num_piezas)

             where     pa_e008_solucion_averia.pk_num_solucion='".$id."' 	"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para obtener  el fk vehiculo
    public function metGetFkDannio($valor)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT
             fk_pae004_num_averia_vehiculo

             FROM `pa_e008_solucion_averia`

             WHERE pk_num_solucion='".$valor."'
             "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    public function metDeleteSolucionDannio($id,$dannio)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "delete from `pa_e008_solucion_averia` " .
            "where pk_num_solucion='$id'"
        );


        for ($i = 0; $i < count($dannio); $i++) {
            $ActualizarDannio = $this->_db->prepare(
             "UPDATE pa_e004_averia_vehiculo
              SET   ind_estado=:ind_estado WHERE pk_num_averia_vehiculo='".$dannio[$i]."'"
            );
            $ActualizarDannio->execute(array(
                ':ind_estado' => "0",

            ));
        }

        #commit — Consigna una transacción
        $this->_db->commit();
    }







}
