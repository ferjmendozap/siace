<table id="datatable4" class="table table-striped table-hover">
	<thead>
	<tr align="center">
		<th><i class="md-person"></i> Nº</th>
		<th><i class="md-person"></i> Lugar</th>
		<th><i class="md-person"></i> Estado</th>
		<th><i class="md-person"></i> Municipio</th>
		<th><i class="md-person"></i> Ciudad</th>
		<th> Seleccionar</th>
	</tr>
	</thead>
	<tbody>
	{assign var="numero" value="1"}
	{foreach item=lugar from=$listarLugar}
		<tr>
			<td>{$lugar.pk_num_lugar_evento}</td>
			<td>{$lugar.ind_lugar_evento}</td>
			<td>{$lugar.ind_estado}</td>
			<td>{$lugar.ind_municipio}</td>
			<td>{$lugar.ind_ciudad}</td>
			<td><button type="button" pk_num_lugar_evento="{$lugar.pk_num_lugar_evento}" class="cargar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha buscado un tema title="Cargar Tema"><i class="glyphicon glyphicon-download"></i></button></td>
		</tr>
	{/foreach}
	</tbody>
</table>
</section>
<script type="text/javascript">

	$(document).ready(function() {
		$('#datatable4').DataTable( {
			"language": {
				"lengthMenu": "Mostrar _MENU_ ",
				"sSearch":        "Buscar:",
				"zeroRecords": "No hay resultados",
				"info": "Mostrar _PAGE_ de _PAGES_",
				"infoEmpty": "No hay resultados",
				"infoFiltered": "(filtered from _MAX_ total records)",
				"paginate": {
					"previous": "<",
					"next": ">"
				}
			}
		} );
	} );

	if ( $.fn.dataTable.isDataTable( '#datatable4' ) ) {
		table = $('#datatable4').DataTable();
	}
	else {
		table = $('#datatable4').DataTable( {
			paging: true
		} );
	}

	// Buscador

	// Seleccion de empleados
	$('#datatable4 tbody').on( 'click', '.cargar', function () {
		// **********************************************PASO 1*************************************************
		var $urlCargar='{$_Parametros.url}modEV/gestionEventoCONTROL/CargarLugarMET';
		$.post($urlCargar, { pk_num_lugar_evento: $(this).attr('pk_num_lugar_evento')}, function (dato) {
		$("#ind_lugar_evento").val(dato['ind_lugar_evento']);
		$("#pk_num_lugar_evento").val(dato['pk_num_lugar_evento']);
		}, 'json');
		$(document.getElementById('cerrarModal2')).click();
		$(document.getElementById('ContenidoModal2')).html('');
	});
</script>