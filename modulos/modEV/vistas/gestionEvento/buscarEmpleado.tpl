<input type="hidden" name="posicion" id="posicion" value="{$emp.valorBoton}"/>
<input type="hidden" name="valor" id="valor" value="{$emp.valor}"/>
<div class="form floating-label">
	<section class="style-default-bright">
		<div class="well clearfix">
			<div class="col-md-6 col-sm-6">
				<div class="form-group floating-label">
					<select id="pk_num_organismo" name="pk_num_organismo" class="form-control dirty" onchange="cargarDependencia(this.value)">
						{foreach item=org from=$listadoOrganismo}
							{if $org.pk_num_organismo == $emp.pk_num_organismo}
								<option selected value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
							{else}
								<option value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
							{/if}
						{/foreach}
					</select>
					<label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
				</div>
				<div id="dependencia">
					<div class="form-group floating-label">
						<select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control">
							<option value="">&nbsp;</option>
							{foreach item=dep from=$listadoDependencia}
								<option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
							{/foreach}
						</select>
						<label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="form-group">
					<input type="text" class="form-control dirty" id="ind_cedula_documento" name="ind_cedula_documento">
					<label class="control-label"><i class="glyphicon glyphicon-calendar"></i> Cédula</label>
				</div>
				<div class="form-group floating-label">
					<select id="opcionBuscar" name="opcionBuscar" class="form-control">
						<option value="1">Empleado</option>
						<option value="2">Persona</option>
					</select>
					<label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Buscar por</label>
				</div>
			</div>
		</div>
		<div class="col-sm-12 col-sm-12" align="center">
			<button type="submit" onclick="buscarTrabajador()" class="btn btn-primary ink-reaction btn-raised"><span class="glyphicon glyphicon-floppy-disk" ></span> Buscar</button>
		</div>
</div>
<br/><br/>
<table id="datatable2" class="table table-striped table-hover">
	<thead>
	<tr align="center">
		<th><i class="md-person"></i> Nº</th>
		<th><i class="md-person"></i> Código</th>
		<th><i class="md-person"></i> Nombre Completo</th>
		<th><i class="glyphicon glyphicon-triangle-right"></i> Nro. Documento</th>
		<th> Seleccionar</th>
	</tr>
	</thead>
	<tbody>
	{assign var="numero" value="1"}
	{foreach item=datos from=$datosTrabajador}
		<tr>
			<td>{$numero++}</td>
			<td>{$datos.pk_num_persona}</td>
			<td>{$datos.ind_nombre1} {$datos.ind_nombre2} {$datos.ind_apellido1} {$datos.ind_apellido2} </td>
			<td>{$datos.ind_cedula_documento}</td>
			<td><button type="button" pk_num_persona="{$datos.pk_num_persona}" class="cargar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha buscado un empleado" title="Cargar empleado"><i class="glyphicon glyphicon-download"></i></button></td>
		</tr>
	{/foreach}
	</tbody>
</table>
</section>
<script type="text/javascript">
	// Inicializador de DataTables
	$(".fecha").datepicker({
		format: 'dd/mm/yyyy',
	});

	$(document).ready(function() {
		$('#datatable2').DataTable( {
			"language": {
				"lengthMenu": "Mostrar _MENU_ ",
				"sSearch":        "Buscar:",
				"zeroRecords": "No hay resultados",
				"info": "Mostrar _PAGE_ de _PAGES_",
				"infoEmpty": "No hay resultados",
				"infoFiltered": "(filtered from _MAX_ total records)",
				"paginate": {
					"previous": "<",
					"next": ">"
				}
			}
		} );
	} );

	if ( $.fn.dataTable.isDataTable( '#datatable2' ) ) {
		table = $('#datatable2').DataTable();
	}
	else {
		table = $('#datatable2').DataTable( {
			paging: true
		} );
	}

	function cargarDependencia(pk_num_organismo) {
		$("#dependencia").html("");
		$.post("{$_Parametros.url}modEV/gestionEventoCONTROL/BuscarDependenciaMET",{ pk_num_organismo:""+pk_num_organismo }, function (dato) {
			$("#dependencia").html(dato);
		});
	}

	// Buscador
	function buscarTrabajador()
	{
		var pk_num_organismo = $("#pk_num_organismo").val();
		var pk_num_dependencia = $("#pk_num_dependencia").val();
		var ind_cedula_documento = $("#ind_cedula_documento").val();
		var opcionBuscar = $("#opcionBuscar").val();
		var url_listar='{$_Parametros.url}modEV/gestionEventoCONTROL/BuscarTrabajadorMET';
		$.post(url_listar,{ pk_num_organismo: pk_num_organismo, pk_num_dependencia: pk_num_dependencia, ind_cedula_documento: ind_cedula_documento, opcionBuscar: opcionBuscar},function(respuesta_post) {
			var tabla_listado = $('#datatable2').DataTable();
			tabla_listado.clear().draw();
			if(respuesta_post != -1) {
				for(var i=0; i<respuesta_post.length; i++) {
					var botonCargar = '<button type="button" pk_num_persona = "' + respuesta_post[i].pk_num_persona + '" class="cargar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha buscado un empleado" title="Cargar empleado"><i class="glyphicon glyphicon-download"></i></button>';
					tabla_listado.row.add([
						i+1,
						respuesta_post[i].pk_num_persona,
						respuesta_post[i].ind_nombre1+ ' ' + respuesta_post[i].ind_nombre2+' '+ respuesta_post[i].ind_apellido1+' '+ respuesta_post[i].ind_apellido2,
						respuesta_post[i].ind_cedula_documento,
						botonCargar
					]).draw()
							.nodes()
							.to$()
				}
			}

		},'json');
	}

	function verificarPersona(pkNumPersona){
		var encontrar = 0;
		for(var i = 0; i < arrayPersona.length; i++) { //busca si pkNumPersona esta en el arrayPersona si está es porque ya esta en lista
			if (arrayPersona[i] == pkNumPersona) {
				encontrar = 1;
				break;
			}
		}
		if(encontrar==0){
			if(i>0){
				var valor = i+1;
			}
			arrayPersona[i] = pkNumPersona;
		}
		return encontrar;
	}

	BAND_FORM = 2;
	// Seleccion de empleados

	$('#datatable2 tbody').on( 'click', '.cargar', function (respuesta_post) {
		// **********************************************PASO 1*************************************************
		var $urlCargar='{$_Parametros.url}modEV/gestionEventoCONTROL/CargarEmpleadoMET';
		var valorBoton = $("#posicion").val();
		var valorPersona = $("#valor").val();
		$verificar = verificarPersona($(this).attr('pk_num_persona'));
		if($verificar==0) {
			$.post($urlCargar, { pk_num_persona: $(this).attr('pk_num_persona')}, function (dato) {
				if (valorPersona == 1) { //ponentes

					$("#ind_nombre" + valorBoton).val(dato['nombre']);
					$("#ind_cedula_documento" + valorBoton).val(dato['ind_cedula_documento']);

					//*************************************************
					$("#buscar_empl"+valorBoton).prop("disabled", true);
					$("#check_firma"+valorBoton).prop("disabled", false);
					$("#check_firma"+valorBoton).val(valorBoton);
					$("#buscar_empl"+valorBoton).prop("id", "");
					$('select[id="tipo_firma'+valorBoton+'"]').attr('pk_persona_firma', dato['pk_num_persona']);
					//*************************************************

				} else { //participantes

					//*************************************************
					$("#buscar_participante"+valorBoton).prop("disabled", true);
					$("#buscar_participante"+valorBoton).prop("id", "");
					//$("#check_firma"+valorBoton).prop("disabled", false);
					//$("#check_firma"+valorBoton).val(valorBoton);
					//$('select[id="tipo_firma'+valorBoton+'"]').attr('pk_persona_firma', dato['pk_num_persona']);

					//*************************************************

					$("#ind_nombre_participante" + valorBoton).val(dato['nombre']);
					$("#ind_cedula_participante" + valorBoton).val(dato['ind_cedula_documento']);
				}
			}, 'json');
			$(document.getElementById('cerrarModal2')).click();
			$(document.getElementById('ContenidoModal2')).html('');
			$(document.getElementById('ContenidoModal3')).html('');
		} else {
			swal("Disculpe", "Esta persona ya se encuentra agregada en el listado");
		}

	});
</script>
