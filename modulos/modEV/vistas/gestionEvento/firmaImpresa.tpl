<section class="style-default-light">&nbsp;

    <!--CUERPO DEL FORMULARIO-->
    <form id="formAjax2" class="form form-validation" role="form" novalidate="novalidate" >
        <div class="row">
            <div class="col-sm-12 ">
                <div class="row"><div class="col-sm-12 ">{if $band == 1} FIRMA: {else} CARGAR FIRMA DE {/if}{$nombre}</div></div>
                <div class="row"><div class="col-sm-12 ">DOC. IDENTIFICACIÓN.: {$cedula}</div></div>
            </div>
            <div class="col-sm-12 ">&nbsp;</div>
            <div class="col-sm-12" align="center">
                {if $band == 1}
                     <img class="img-thumbnail border-gray border-lg img-responsive WIDTH-100" src="{$_Parametros.ruta_Img}modEV/firmas/{$imagenFirma}" alt="Firma Impresa" title="Firma Impresa" id="imagenModificada" />
                {else}
                    <img align="center" class="img-thumbnail border-gray border-lg img-responsive auto-width" src="{$_Parametros.ruta_Img}modEV/firmas/no_firma.png" alt="Cargar Firma" title="Cargar imagen de firma" id="cargarImagen" style="cursor: pointer; "/>
                    <p class="help-block">Haga click para cargar la imagen</p>
                {/if}
                <input type="file" name="ind_ruta_img" id="ind_ruta_img" style="display: none" />
            </div>
            {if $band == 1}
            <div align="left" class="col-sm-12 ">
                <button id="btn_mod_firma" class="btn btn-xs btn-primary ink-reaction btn-raised logsUsuarioModal" type="button"><i class="fa fa-edit"></i> Modificar</button>
            </div>
            {/if}
            <div class="col-sm-12">
                &nbsp;
            </div>
            <div class="col-sm-12">
                &nbsp;
            </div>
            <div class="row">
                <div align="right" class="col-sm-12">
                    <button id="boton_cerrar_firma" class="btn btn-sm btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal" type="button"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                    {if $band == 1}
                        <button id="btn_aceptar_firma" class="btn btn-sm btn-primary ink-reaction btn-raised logsUsuarioModal" type="button"><span class="glyphicon glyphicon-floppy-disk"></span>Aceptar</button>
                    {else}
                        <button id="btn_guardar_firma" class="btn btn-sm btn-primary ink-reaction btn-raised logsUsuarioModal" type="button"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
                    {/if}
                </div>
            </div>
        </div>
    </form>
    <input type="hidden" id="band_opcion" value="" />
    <!--FIN DEL TITULO DEL FORMULARIO-->
</section>
<script>

    BAND_FORM = 1;

    $("#cargarImagen").click(function() {
        $("#band_opcion").val(1);
        $("#ind_ruta_img").click();//input file
    });

    $("#ind_ruta_img").change(function(e) {

        var files = e.target.files;
        var file = $("#ind_ruta_img")[0].files[0];
        var fileName = file.name;

        var aux_n = fileName.split('.');
        var aux_n2 = 0;
        if ( aux_n.length > 2 || fileName.indexOf(',') > -1 || fileName.indexOf('"') > -1 || fileName.indexOf("'") > -1 || fileName.indexOf('(') > -1 || fileName.indexOf(')') > -1 || fileName.indexOf('}') > -1 )
        {
            aux_n2 = 1;
        }

        if(aux_n2 == 0)
        {

            swImagen = 1;
            fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
            if (fileExtension == 'png' || fileExtension == 'PNG')  {

                for (var i = 0, f; f = files[i]; i++) {

                    if (!f.type.match('image.*')) {

                        continue;
                    }

                    var reader = new FileReader();
                    reader.onload = (function (theFile) {

                        return function (e) {

                           if($("#band_opcion").val() == 1)//opcion cargar imagen
                           {
                               $("#cargarImagen").attr("src", e.target.result);
                               $("#cargarImagen").css({
                                   'width': '400px',
                                   'height': '140px'
                               });
                           }
                           else//opcion modificar imagen
                           {
                               modificarFirmaImagen(e);
                           }

                        };

                    })(f);
                    reader.readAsDataURL(f);
                }


            }
            else
            {
                swal("¡Extensión de archivo no válido!", "Asegurese de subir un archivo de tipo imagen: png", "warning");
                $("#ind_ruta_img").val('');
                $("#ind_ruta_img_f").val('');

                if($("#band_opcion").val() == 1)//opcion cargar imagen
                {
                    $("#cargarImagen").attr("src",'{$_Parametros.ruta_Img}modEV/firmas/no_firma.png');
                }
                else//opcion modificar imagen
                {
                    $("#cargarImagen").attr("src",'{$_Parametros.ruta_Img}modEV/firmas/{$imagenFirma}');
                }
                $("#cargarImagen").css({ 'width':'400px', 'height':'140px' });

            }
        }
        else
        {
            swal("¡Nombre de archivo no válido!", "Renombre el archivo, sin caracteres especiales", "warning");
            $("#ind_ruta_img").val('');
            $("#ind_ruta_img_f").val('');

            if($("#band_opcion").val() == 1)//opcion cargar imagen
            {
                $("#cargarImagen").attr("src",'{$_Parametros.ruta_Img}modEV/firmas/no_firma.png');
            }
            else//opcion modificar imagen
            {
                $("#cargarImagen").attr("src",'{$_Parametros.ruta_Img}modEV/firmas/{$imagenFirma}');
            }

            $("#cargarImagen").css({ 'width':'400px', 'height':'140px' });
        }
    });

    $("#btn_guardar_firma").click( function(){

        var file = $("#ind_ruta_img").val();
        if(file == '')
        {
            swal("¡La imagen de la firma es obligatoria!", "Debe cargar la imagen en formato PNG transparente.", "warning");
        }
        else
        {

            var data = new FormData($("#formAjax2")[0]);
            data.append('pk_persona',{$pk_persona});
            $.ajax({
                url: "{$_Parametros.url}modEV/gestionEventoCONTROL/GuardarFirmaPngMET",
                data: data,
                type: 'POST',
                contentType: false,
                processData: false,
                success: function (dato){

                    if(dato)
                    {
                        if(dato == 1)
                        {
                            swal({
                                title: 'Firma registrada',
                                text: 'La imagen de la firma se ha registrado satisfactoriamente',
                                type: "success",
                                closeOnConfirm: true
                            }, function () {

                                $(document.getElementById('cerrarModal3')).click();
                                $("#tipo_firma" + {$valor_indice}).val(2);
                            });
                        }
                        else
                        {
                            if(dato == 0)
                            {
                                swal("¡El nombre de la imagen de la firma ya existe!", "Debe cargar la imagen con un nombre de archivo diferente.", "warning");
                            }
                            else
                            {
                                swal("¡No se realizó la operación!", "Error tipo: "+dato, "error");
                            }
                        }
                    }
                    else
                    {
                        alert('Error al registrar imagen: '+ $dato);
                    }
                }
            });

        }
    });

    function accionesCerrar(){

        $("#ind_ruta_img").val('');
        $("#cargarImagen").attr("src",'{$_Parametros.ruta_Img}modEV/firmas/no_firma.png');
        $("#cargarImagen").css({ 'width':'400px', 'height':'140px' });
        $("#cargarImagen")

        $("#tipo_firma" + {$valor_indice}).val(0);

    }

    $(".close").click(function (){

        if( BAND_FORM == 1)// invoca el form firmaImpresa.tpl
        {
            accionesCerrar();
        }
    });

    $("#boton_cerrar_firma").click( function(){ //cancelar
        accionesCerrar();
    });

    $("#btn_aceptar_firma").click(function (){
        $(document.getElementById('cerrarModal3')).click();
        $("#tipo_firma" + {$valor_indice}).val(2);
    });

    $("#btn_mod_firma").click(function(){
        $("#band_opcion").val(2);
        $("#ind_ruta_img").click();
    });

    function modificarFirmaImagen(e)
    {
        swal({
            title: '¿Desea cambiar la firma?',
            text: 'La imagen de la firma será modificada y afectará los registros.',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: 'Sí, Cambiar',
            cancelButtonText: "Cancelar",
            closeOnConfirm: false
        }, function () {

            var data = new FormData($("#formAjax2")[0]);
            data.append('pk_persona',{$pk_persona});
            $.ajax({
                url: "{$_Parametros.url}modEV/gestionEventoCONTROL/GuardarFirmaPngMET",
                data: data,
                type: 'POST',
                contentType: false,
                processData: false,
                success: function (dato){

                    if(dato)
                    {
                        if(dato == 1)
                        {
                            $("#imagenModificada").attr("src", e.target.result);
                            $("#imagenModificada").css({
                                'width': '400px',
                                'height': '140px'
                            });

                            swal({
                                title: 'Firma Actualizada',
                                text: 'La imagen de la firma se ha registrado satisfactoriamente',
                                type: "success",
                                closeOnConfirm: true
                            }, function () { });

                        }
                        else
                        {
                            if(dato == 0)
                            {
                                swal("¡El nombre de la imagen de la firma ya existe!", "Debe cargar la imagen con un nombre de archivo diferente.", "warning");
                            }
                            else
                            {
                               swal("¡No se realizó la operación!", "Error tipo: "+dato, "error");
                            }
                        }
                    }
                    else
                    {
                       alert('Error al registrar imagen: '+ $dato);
                    }
                }
            });

        });

    }




</script>