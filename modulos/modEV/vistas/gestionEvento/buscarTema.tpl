<input type="hidden" name="posicion" id="posicion" value="{$temaBoton.valorBoton}"/>
<table id="datatable3" class="table table-striped table-hover">
	<thead>
	<tr align="center">
		<th><i class="md-person"></i> Nº</th>
		<th><i class="md-person"></i> Tema</th>
		<th> Seleccionar</th>
	</tr>
	</thead>
	<tbody>
	{assign var="numero" value="1"}
	{foreach item=tema from=$listarTema}
		<tr>
			<td>{$tema.pk_num_tema_evento}</td>
			<td>{$tema.ind_tema}</td>
			<td><button type="button" pk_num_tema_evento="{$tema.pk_num_tema_evento}" class="cargar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha buscado un tema title="Cargar Tema"><i class="glyphicon glyphicon-download"></i></button></td>
		</tr>
	{/foreach}
	</tbody>
</table>
</section>
<script type="text/javascript">

	$(document).ready(function() {
		$('#datatable3').DataTable( {
			"language": {
				"lengthMenu": "Mostrar _MENU_ ",
				"sSearch":        "Buscar:",
				"zeroRecords": "No hay resultados",
				"info": "Mostrar _PAGE_ de _PAGES_",
				"infoEmpty": "No hay resultados",
				"infoFiltered": "(filtered from _MAX_ total records)",
				"paginate": {
					"previous": "<",
					"next": ">"
				}
			}
		} );
	} );

	if ( $.fn.dataTable.isDataTable( '#datatable3' ) ) {
		table = $('#datatable3').DataTable();
	}
	else {
		table = $('#datatable3').DataTable( {
			paging: true
		} );
	}

    function verificarTema(pk_num_tema_evento){
        var encontrar = 0;
        for(var i = 0; i < arrayTema.length; i++) {
            if (arrayTema[i] == pk_num_tema_evento) {

                encontrar = 1;
                break;
            }
        }
        if(encontrar==0){
            if(i>0){
                var valor = i+1;
            }
            arrayTema[i] = pk_num_tema_evento;
        }
        return encontrar;
    }

	// Seleccion de empleados
	$('#datatable3 tbody').on( 'click', '.cargar', function () {
		// **********************************************PASO 1*************************************************
		var $urlCargar='{$_Parametros.url}modEV/gestionEventoCONTROL/CargarTemaMET';
		var valorBoton = $("#posicion").val();
        var pk_num_tema_evento = $(this).attr('pk_num_tema_evento');
        var verificar = verificarTema(pk_num_tema_evento);
        if(verificar==0) {
            $.post($urlCargar, { pk_num_tema_evento: pk_num_tema_evento}, function (dato) {
                $("#ind_tema" + valorBoton).val(dato['ind_tema']);
                $("#pk_num_tema_evento" + valorBoton).val(dato['pk_num_tema_evento']);
                $("#buscar_tem"+valorBoton).prop("disabled", true);
            }, 'json');
            $(document.getElementById('cerrarModal2')).click();
            $(document.getElementById('ContenidoModal2')).html('');
        } else {
            swal("Disculpe", "Este tema ya se encuentra agregado en el listado");
        }
	});
</script>