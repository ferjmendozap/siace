<form action="{$_Parametros.url}modEV/lugarCONTROL/EditarLugarMET" id="formAjax" method="post" class="form" role="form">
<div class="modal-body">
	<input type="hidden" value="1" name="valido" />
	{if isset($evento.pk_num_lugar_evento)}
		<input type="hidden" value="{$evento.pk_num_lugar_evento}" name="pk_num_lugar_evento" id="pk_num_lugar_evento" />
	{/if}
	<div class="form-group floating-label">
		<input type="text" class="form-control" id="regular2" value="{$evento.pk_num_lugar_evento}" disabled="disabled">
		<label for="regular2">N°</label>
	</div>
	<div class="form-group floating-label">
		<input type="text" class="form-control" name="ind_lugar_evento" id="ind_lugar_evento" value="{$evento.ind_lugar_evento}">
		<label for="regular2">Lugar de Evento</label>
	</div>
	<div class="form-group floating-label">
		<select id="pk_num_pais" name="pk_num_pais" class="form-control" onchange="cargarEstado(this.value)">
			{foreach item=pa from=$pais}
				{if $pa.pk_num_pais==$ubicacion.pk_num_pais}
					<option value="{$pa.pk_num_pais}" selected>{$pa.ind_pais}</option>
				{else}
					<option value="{$pa.pk_num_pais}">{$pa.ind_pais}</option>
				{/if}
			{/foreach}
		</select>
		<label for="pk_num_pais"><i class="glyphicon glyphicon-triangle-right"></i> Pais</label>
	</div>
	<div id="estado">
		<div class="form-group floating-label">
			<select class="form-control" name="pk_num_estado" id="pk_num_estado" onchange="cargarCiudad(this.value)">
				{foreach item=es from=$estado}
					{if $es.pk_num_estado==$ubicacion.pk_num_estado}
						<option value="{$es.pk_num_estado}" selected>{$es.ind_estado}</option>
					{else}
						<option value="{$es.pk_num_estado}">{$es.ind_estado}</option>
					{/if}
				{/foreach}
			</select>
			<label><i class="glyphicon glyphicon-triangle-right"></i> Estado</label>
		</div>
	</div>
	<div id="ciudad">
		<div class="form-group floating-label" name="pk_num_ciudad" id="pk_num_ciudad">
			<select class="form-control">
				{foreach item=ci from=$ciudad}
					{if $ci.pk_num_ciudad==$ubicacion.pk_num_ciudad}
						<option value="{$ci.pk_num_ciudad}" selected>{$ci.ind_ciudad}</option>
					{else}
						<option value="{$ci.pk_num_ciudad}">{$ci.ind_ciudad}</option>
					{/if}
				{/foreach}
			</select>
			<label><i class="glyphicon glyphicon-triangle-right"></i> Ciudad</label>
		</div>
	</div>
	<div id="municipio">
		<div class="form-group floating-label">
			<select class="form-control" name="pk_num_municipio" id="pk_num_municipio">
				{foreach item=mun from=$municipio}
					{if $mun.pk_num_municipio==$ubicacion.pk_num_municipio}
						<option value="{$mun.pk_num_municipio}" selected>{$mun.ind_municipio}</option>
					{else}
						<option value="{$mun.pk_num_municipio}">{$mun.ind_municipio}</option>
					{/if}
				{/foreach}
			</select>
			<label><i class="glyphicon glyphicon-triangle-right"></i> Municipio</label>
		</div>
	</div>
	<div id="parroquia">
		<div class="form-group floating-label">
			<select class="form-control" name="pk_num_parroquia" id="pk_num_parroquia">
				{foreach item=pa from=$parroquia}
					{if $pa.pk_num_parroquia==$ubicacion.pk_num_parroquia}
						<option value="{$pa.pk_num_parroquia}" selected>{$pa.ind_parroquia}</option>
					{else}
						<option value="{$pa.pk_num_parroquia}">{$pa.ind_parroquia}</option>
					{/if}
				{/foreach}
			</select>
			<label><i class="glyphicon glyphicon-triangle-right"></i> Parroquia</label>
		</div>
	</div>
	<div id="sector">
		<div class="form-group floating-label">
			<select class="form-control" name="pk_num_sector" id="pk_num_sector">
				{foreach item=sec from=$sector}
					{if $sec.pk_num_sector==$ubicacion.pk_num_sector}
						<option value="{$sec.pk_num_sector}" selected>{$sec.ind_sector}</option>
					{else}
						<option value="{$sec.pk_num_sector}">{$sec.ind_sector}</option>
					{/if}
				{/foreach}
			</select>
			<label><i class="glyphicon glyphicon-triangle-right"></i> Sector</label>
		</div>
	</div>
	<div class="form-group floating-label">
		<input type="text" class="form-control" id="regular2" value="{$evento.fec_ultima_modificacion}" disabled="disabled">
		<label for="regular2">Última modificación</label>
	</div>
	<div class="form-group floating-label">
		<input type="text" class="form-control" id="regular2" value="{$evento.ind_nombre1} {$evento.ind_nombre2} {$evento.ind_apellido1} {$evento.ind_apellido2}" disabled="disabled">
		<label for="regular2">Último usuario</label>
	</div>
	<div align="right">
		<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
		<button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
	</div>
</div>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        $("#formAjax").validate({
			rules:{
				ind_lugar_evento:{
					required:true
				}
			},
			messages:{
				ind_lugar_evento:{
					required: "Este campo es requerido"
				}
			},
			submitHandler: function(form){
				$.post($(form).attr('action'), $(form).serialize(),function(dato){
					$('#pk_num_lugar_evento'+dato['pk_num_lugar_evento']).remove();
					if(dato['num_estatus']==1){
						var botonEstatus =  '<button class="estatus logsUsuario btn ink-reaction btn-raised btn-xs btn-success"  data-keyboard="false" data-backdrop="static" pk_num_lugar_evento="'+dato['pk_num_lugar_evento']+'" descripcion="El Usuario ha deshabilitado un lugar" title="Deshabilitar Lugar"><i class="md-done"></i></button>'
					} else {
						var botonEstatus = '<button class="estatus logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  data-keyboard="false" data-backdrop="static" pk_num_lugar_evento="'+dato['pk_num_lugar_evento']+'" descipcion="El Usuario ha deshabilitado un lugar" title="Deshabilitar Lugar"><i class="md md-not-interested"></i></button>';
					}
                    if(dato['total']==0){
                        var botonEliminar = '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_lugar_evento="'+dato['pk_num_lugar_evento']+'"  boton="si, Eliminar" descipcion="El usuario ha eliminado un lugar evento" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar el lugar del evento?" title="Eliminar lugar de evento"><i class="md md-delete"></i></button>';
                    } else if(dato['total']>0){
                        var botonEliminar = '';
                    }
					$(document.getElementById('datatable1')).append('<tr id="pk_num_lugar_evento'+dato['pk_num_lugar_evento']+'"><td>'+dato['pk_num_lugar_evento']+'</td>' +
							'<td>'+dato['ind_lugar_evento']+'</td>' +
							'<td align="center">'+botonEstatus+'</td>' +
					'<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha modificado un lugar" titulo="Modificar Lugar" title="Modificar Lugar" data-toggle="modal" data-target="#formModal" pk_num_lugar_evento="'+dato['pk_num_lugar_evento']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td><td align="center">'+ botonEliminar +'</td></tr>');
					swal("Lugar modificado", "Lugar modificado exitosamente", "success");
					$(document.getElementById('cerrarModal')).click();
					$(document.getElementById('ContenidoModal')).html('');
            },'json');
        }
    });
});
	function cargarEstado(pk_num_pais) {
		$("#estado").html("");
		$("#ciudad").html('<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_ciudad" id="pk_num_ciudad"><option value="">&nbsp;</option></select><label><i class="glyphicon glyphicon-triangle-right"></i> Ciudad</label></div>');
		$("#municipio").html('<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_municipio" id="pk_num_municipio"><option value="">&nbsp;</option></select><label><i class="glyphicon glyphicon-triangle-right"></i> Municipio</label></div>');
		$("#parroquia").html('<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_parroquia" id="pk_num_parroquia"><option value="">&nbsp;</option></select><label><i class="glyphicon glyphicon-triangle-right"></i> Parroquia</label></div>');
		$("#sector").html('<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_sector" id="pk_num_sector"><option value="">&nbsp;</option></select><label><i class="glyphicon glyphicon-triangle-right"></i> Sector</label></div>');
		$.post("{$_Parametros.url}modEV/lugarCONTROL/CargarSelectMET",{ pk_num_pais:""+pk_num_pais, valor: 1 }, function (dato) {
			$("#estado").html(dato);
		});
	}

	function cargarCiudad(pk_num_estado) {
		$("#ciudad").html("");
		$("#municipio").html("");
		$("#parroquia").html('<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_parroquia" id="pk_num_parroquia"><option value="">&nbsp;</option></select><label><i class="glyphicon glyphicon-triangle-right"></i> Parroquia</label></div>');
		$("#sector").html('<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_sector" id="pk_num_sector"><option value="">&nbsp;</option></select><label><i class="glyphicon glyphicon-triangle-right"></i> Sector</label></div>');
		$.post("{$_Parametros.url}modEV/lugarCONTROL/CargarSelectMET",{ pk_num_estado:""+pk_num_estado, valor: 2 }, function (dato) {
			$("#ciudad").html(dato);
		});
		$.post("{$_Parametros.url}modEV/lugarCONTROL/CargarSelectMET",{ pk_num_estado:""+pk_num_estado, valor: 3 }, function (dato) {
			$("#municipio").html(dato);
		});
	}

	function cargarParroquia(pk_num_municipio) {
		$("#parroquia").html("");
		$("#sector").html('<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_sector" id="pk_num_sector"><option value="">&nbsp;</option></select><label><i class="glyphicon glyphicon-triangle-right"></i> Sector</label></div>');
		$.post("{$_Parametros.url}modEV/lugarCONTROL/CargarSelectMET",{ pk_num_municipio:""+pk_num_municipio, valor: 4 }, function (dato) {
			$("#parroquia").html(dato);
		});
	}

	function cargarSector(pk_num_parroquia) {
		$("#sector").html("");
		$.post("{$_Parametros.url}modEV/lugarCONTROL/CargarSelectMET",{ pk_num_parroquia:""+pk_num_parroquia, valor: 5 }, function (dato) {
			$("#sector").html(dato);
		});
	}
</script>


