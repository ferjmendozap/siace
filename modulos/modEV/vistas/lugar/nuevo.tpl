<form id="formAjax" action="{$_Parametros.url}modEV/lugarCONTROL/NuevoLugarMET" class="form floating-label" novalidate="novalidate">
<input type="hidden" name="valido" value="1" />
<div class="modal-body">
	<div class="form-group floating-label">
		<input type="text" class="form-control"  name="ind_lugar_evento" id="ind_lugar_evento">
		<label for="ind_lugar_evento"><i class="md md-create"></i> Lugar de Evento </label>
	</div>
	<div class="form-group floating-label">
		<select id="pk_num_pais" name="pk_num_pais" class="form-control" onchange="cargarEstado(this.value)">
				{foreach item=pa from=$pais}
					{if $pa.pk_num_pais==$paisActual.pk_num_pais}
						<option value="{$pa.pk_num_pais}" selected>{$pa.ind_pais}</option>
					{else}
						<option value="{$pa.pk_num_pais}">{$pa.ind_pais}</option>
					{/if}
				{/foreach}
		</select>
		<label for="pk_num_pais"><i class="glyphicon glyphicon-triangle-right"></i> Pais</label>
	</div>
	<div id="estado">
		<div class="form-group floating-label">
			<select class="form-control" name="pk_num_estado" id="pk_num_estado" onchange="cargarCiudad(this.value)">
				<option value="">&nbsp;</option>
				{foreach item=es from=$estado}
					<option value="{$es.pk_num_estado}">{$es.ind_estado}</option>
				{/foreach}
			</select>
			<label><i class="glyphicon glyphicon-triangle-right"></i> Estado</label>
		</div>
	</div>
	<div id="ciudad">
		<div class="form-group floating-label" name="pk_num_ciudad" id="pk_num_ciudad">
			<select class="form-control">
				<option value="">&nbsp;</option>
			</select>
			<label><i class="glyphicon glyphicon-triangle-right"></i> Ciudad</label>
		</div>
	</div>
	<div id="municipio">
		<div class="form-group floating-label">
			<select class="form-control" name="pk_num_municipio" id="pk_num_municipio">
				<option value="">&nbsp;</option>
			</select>
			<label><i class="glyphicon glyphicon-triangle-right"></i> Municipio</label>
		</div>
	</div>
	<div id="parroquia">
		<div class="form-group floating-label">
			<select class="form-control" name="pk_num_parroquia" id="pk_num_parroquia">
				<option value="">&nbsp;</option>
			</select>
			<label><i class="glyphicon glyphicon-triangle-right"></i> Parroquia</label>
		</div>
	</div>
	<div id="sector">
		<div class="form-group floating-label">
			<select class="form-control" name="pk_num_sector" id="pk_num_sector">
				<option value="">&nbsp;</option>
			</select>
			<label><i class="glyphicon glyphicon-triangle-right"></i> Sector</label>
		</div>
	</div>
	<div align="right">
		<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
		<button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
	</div>
</form>
<script type="text/javascript">
	$(document).ready(function () {
		//validation rules
		$("#formAjax").validate({
			rules:{
				ind_lugar_evento:{
					required:true,
					remote:{
						url:"{$_Parametros.url}modEV/lugarCONTROL/VerificarLugarMET",
						type:"post"
					}
				}
			},
			messages:{
				ind_lugar_evento:{
					required: "Este campo es requerido",
					remote: jQuery.validator.format("Ya se encuentra registrado en el sistema")
				}
			},
			submitHandler: function(form) {
				$.post($(form).attr('action'), $(form).serialize(),function(dato){
					if(dato['num_estatus']==1){
						var botonEstatus =  '<button class="estatus logsUsuario btn ink-reaction btn-raised btn-xs btn-success"  data-keyboard="false" data-backdrop="static" pk_num_lugar_evento="'+dato['pk_num_lugar_evento']+'" descripcion="El Usuario ha deshabilitado un lugar" title="Deshabilitar Lugar"><i class="md-done"></i></button>'
					} else {
						var botonEstatus = '<button class="estatus logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  data-keyboard="false" data-backdrop="static" pk_num_lugar_evento="'+dato['pk_num_lugar_evento']+'" descipcion="El Usuario ha deshabilitado un lugar" title="Deshabilitar Lugar"><i class="md md-not-interested"></i></button>';
					}
					if(dato['total']==0){
					    var botonEliminar = '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_lugar_evento="'+dato['pk_num_lugar_evento']+'"  boton="si, Eliminar" descipcion="El usuario ha eliminado un lugar evento" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar el lugar del evento?" title="Eliminar lugar de evento"><i class="md md-delete"></i></button>';
                    } else if(dato['total']>0){
                        var botonEliminar = '';
                    }
					$(document.getElementById('datatable1')).append('<tr id="pk_num_lugar_evento'+dato['pk_num_lugar_evento']+'"><td>'+dato['pk_num_lugar_evento']+'</td><td>'+dato['ind_lugar_evento']+'</td><td align="center">'+botonEstatus+'</td><td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha modificado un lugar" titulo="Modificar Lugar" title="Modificar Lugar" data-toggle="modal" data-target="#formModal" pk_num_lugar_evento="'+dato['pk_num_lugar_evento']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td><td align="center">'+botonEliminar+'</td></tr>');
					swal("Lugar Guardado", "Lugar guardado satisfactoriamente", "success");
					$(document.getElementById('cerrarModal')).click();
					$(document.getElementById('ContenidoModal')).html('');
				},'json');
			}
		});
	});

	function cargarEstado(pk_num_pais) {
		$("#estado").html("");
		$("#ciudad").html('<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_ciudad" id="pk_num_ciudad"><option value="">&nbsp;</option></select><label><i class="glyphicon glyphicon-triangle-right"></i> Ciudad</label></div>');
		$("#municipio").html('<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_municipio" id="pk_num_municipio"><option value="">&nbsp;</option></select><label><i class="glyphicon glyphicon-triangle-right"></i> Municipio</label></div>');
		$("#parroquia").html('<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_parroquia" id="pk_num_parroquia"><option value="">&nbsp;</option></select><label><i class="glyphicon glyphicon-triangle-right"></i> Parroquia</label></div>');
		$("#sector").html('<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_sector" id="pk_num_sector"><option value="">&nbsp;</option></select><label><i class="glyphicon glyphicon-triangle-right"></i> Sector</label></div>');
		$.post("{$_Parametros.url}modEV/lugarCONTROL/CargarSelectMET",{ pk_num_pais:""+pk_num_pais, valor: 1 }, function (dato) {
			$("#estado").html(dato);
		});
	}

	function cargarCiudad(pk_num_estado) {
		$("#ciudad").html("");
		$("#municipio").html("");
		$("#parroquia").html('<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_parroquia" id="pk_num_parroquia"><option value="">&nbsp;</option></select><label><i class="glyphicon glyphicon-triangle-right"></i> Parroquia</label></div>');
		$("#sector").html('<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_sector" id="pk_num_sector"><option value="">&nbsp;</option></select><label><i class="glyphicon glyphicon-triangle-right"></i> Sector</label></div>');
		$.post("{$_Parametros.url}modEV/lugarCONTROL/CargarSelectMET",{ pk_num_estado:""+pk_num_estado, valor: 2 }, function (dato) {
			$("#ciudad").html(dato);
		});
		$.post("{$_Parametros.url}modEV/lugarCONTROL/CargarSelectMET",{ pk_num_estado:""+pk_num_estado, valor: 3 }, function (dato) {
			$("#municipio").html(dato);
		});
	}

	function cargarParroquia(pk_num_municipio) {
		$("#parroquia").html("");
		$("#sector").html('<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_sector" id="pk_num_sector"><option value="">&nbsp;</option></select><label><i class="glyphicon glyphicon-triangle-right"></i> Sector</label></div>');
		$.post("{$_Parametros.url}modEV/lugarCONTROL/CargarSelectMET",{ pk_num_municipio:""+pk_num_municipio, valor: 4 }, function (dato) {
			$("#parroquia").html(dato);
		});
	}

	function cargarSector(pk_num_parroquia) {
		$("#sector").html("");
		$.post("{$_Parametros.url}modEV/lugarCONTROL/CargarSelectMET",{ pk_num_parroquia:""+pk_num_parroquia, valor: 5 }, function (dato) {
			$("#sector").html(dato);
		});
	}
</script>
