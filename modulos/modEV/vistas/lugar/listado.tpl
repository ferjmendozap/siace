<br/>
<section class="style-default-bright">
    <h2 class="text-primary">&nbsp;Lista de Lugares</h2>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <br/>
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr align="center">
                        <th width="60"> N°</th>
                        <th> Lugar</th>
                        <th width="60" align="center"> Estatus</th>
                        <th width="60" align="center">Editar</th>
                        <th width="60" align="center">Eliminar</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=lugar from=$listadoLugar}
                        <tr id="pk_num_lugar_evento{$lugar.pk_num_lugar_evento}">
                            <td>{$lugar.pk_num_lugar_evento}</td>
                            <td>{$lugar.ind_lugar_evento}</td>
                            <td align="center">
                                {if $lugar.num_estatus==1}
                                    <button class="estatus logsUsuario btn ink-reaction btn-raised btn-xs btn-success"  data-keyboard="false" data-backdrop="static" pk_num_lugar_evento="{$lugar.pk_num_lugar_evento}" descipcion="El Usuario ha deshabilitado un lugar" title="Deshabilitar Lugar"><i class="md-done"></i></button>
                                {else}
                                    <button class="estatus logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  data-keyboard="false" data-backdrop="static" pk_num_lugar_evento="{$lugar.pk_num_lugar_evento}" descipcion="El Usuario ha deshabilitado un lugar" title="Deshabilitar Lugar"><i class="md md-not-interested"></i></button>
                                {/if}
                            </td>
                            <td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_lugar_evento="{$lugar.pk_num_lugar_evento}" descipcion="El Usuario ha Modificado un lugar" title="Modificar Lugar"  titulo="Modificar Lugar"><i class="fa fa-edit"></i></button></td>
                            <td align="center">{if $lugar.total==0}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_lugar_evento="{$lugar.pk_num_lugar_evento}"  boton="si, Eliminar" descipcion="El usuario ha eliminado un lugar evento" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar el lugar del evento?" title="Eliminar lugar de evento"><i class="md md-delete"></i></button>{/if}</td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="5">
                                <button  class="logsUsuario btn ink-reaction btn-raised btn-info" descripcion="El usuario ha creado un nuevo lugar" data-toggle="modal" data-target="#formModal" titulo="Registrar Nuevo Lugar" id="nuevo"><i class="md md-create"></i> Nuevo Lugar</button>
                            </th>
                        </tr>
                    </tfoot>
                </table>
            </div>

        </div>
    </div>
</section>
<script type="text/javascript">
            $(document).ready(function() {
                var $url='{$_Parametros.url}modEV/lugarCONTROL/NuevoLugarMET';
                $('#nuevo').click(function(){
					$('#modalAncho').css( "width", "45%" );
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $('#ContenidoModal').html("");
                    $.post($url,'',function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });

                var $url_modificar='{$_Parametros.url}modEV/lugarCONTROL/EditarLugarMET';
                $('#datatable1 tbody').on( 'click', '.modificar', function () {
		        $('#modalAncho').css( "width", "45%" );
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $('#ContenidoModal').html("");
                    $.post($url_modificar,{ pk_num_lugar_evento: $(this).attr('pk_num_lugar_evento')},function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });

            });

            $('#datatable1 tbody').on( 'click', '.eliminar', function () {
                var pk_num_lugar_evento=$(this).attr('pk_num_lugar_evento');
                swal({
                    title: $(this).attr('titulo'),
                    text: $(this).attr('mensaje'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false
                }, function(){
                    var $url='{$_Parametros.url}modEV/lugarCONTROL/EliminarLugarMET';
                    $.post($url, { pk_num_lugar_evento: pk_num_lugar_evento},function($dato){
                        if($dato['status']=='OK'){
                            $(document.getElementById('pk_num_lugar_evento'+$dato['pk_num_lugar_evento'])).html('');
                            swal("Eliminado!", "El lugar fue eliminado.", "success");
                            $('#cerrar').click();
                        }
                    },'json');
                });
            });

            var $urlEstatus = '{$_Parametros.url}modEV/lugarCONTROL/CambiarEstatusMET';
            $('#datatable1 tbody').on( 'click', '.estatus', function () {
               var pk_num_lugar_evento =  $(this).attr('pk_num_lugar_evento');
                $.post($urlEstatus,{ pk_num_lugar_evento: pk_num_lugar_evento},function(dato){
                    $('#pk_num_lugar_evento'+dato['pk_num_lugar_evento']).remove();
                    if(dato['num_estatus']==1){
                        var botonEstatus =  '<button class="estatus logsUsuario btn ink-reaction btn-raised btn-xs btn-success"  data-keyboard="false" data-backdrop="static" pk_num_lugar_evento="'+dato['pk_num_lugar_evento']+'" descripcion="El Usuario ha deshabilitado un lugar" title="Deshabilitar Lugar"><i class="md-done"></i></button>'
                    } else {
                        var botonEstatus = '<button class="estatus logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  data-keyboard="false" data-backdrop="static" pk_num_lugar_evento="'+dato['pk_num_lugar_evento']+'" descipcion="El Usuario ha deshabilitado un lugar" title="Deshabilitar Lugar"><i class="md md-not-interested"></i></button>';
                    }
                    if(dato['total']==0){
                        var botonEliminar = '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_lugar_evento="'+dato['pk_num_lugar_evento']+'"  boton="si, Eliminar" descipcion="El usuario ha eliminado un lugar evento" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar el lugar del evento?" title="Eliminar lugar de evento"><i class="md md-delete"></i></button>';
                    } else {
                        var botonEliminar = '';
                    }
                    $(document.getElementById('datatable1')).append('<tr id="pk_num_lugar_evento'+dato['pk_num_lugar_evento']+'"><td>'+dato['pk_num_lugar_evento']+'</td>' +
                        '<td>'+dato['ind_lugar_evento']+'</td>' +
                        '<td align="center">'+botonEstatus+'</td>' +
                        '<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha modificado un lugar" titulo="Modificar Lugar" title="Modificar Lugar" data-toggle="modal" data-target="#formModal" pk_num_lugar_evento="'+dato['pk_num_lugar_evento']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>' +
                        '<td align="center">'+botonEliminar+'</td></tr>');
                },'json');
            });


</script>
