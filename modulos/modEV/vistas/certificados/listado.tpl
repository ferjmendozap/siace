<br/>
<section class="style-default-bright">
    <h2 class="text-primary">&nbsp;Certificados</h2>
    <div class="section-body contain-lg">
        <div class="row">
            <br/>
            <div class="col-lg-12 contain-lg" align="center">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr align="center">
                        <th> N°</th>
                        <th> Imagen</th>
                        <th align="center" >Ver</th>
                        <th>Estatus</th>
                        <th>Usuario</th>
                        <th>Última modificación</th>
                    </thead>
                    <tbody>
                    {foreach item=certificado from=$listarCertificado}
                        <tr id="pk_num_certificado{$certificado.pk_num_certificado}">
                            <td>{$certificado.pk_num_certificado}</td>
                            <td>{$certificado.ind_imagen}</td>
                            <td>
                                <div align="center">
                                    <a href="{$_Parametros.url}publico/imagenes/modEV/fondo/{$certificado.ind_imagen}"  target="_blank">
                                        <img width="180" height="140" src="{$_Parametros.url}publico/imagenes/modEV/fondo/{$certificado.ind_imagen}"/>
                                    </a>
                                </div>
                            </td>
                            <td align="center">
                                {if $certificado.num_estatus==1}
                                    <button class="estatus logsUsuario btn ink-reaction btn-raised btn-xs btn-success"  data-keyboard="false" data-backdrop="static" pk_num_certificado="{$certificado.pk_num_certificado}" descipcion="El Usuario ha deshabilitado un tema" title="Deshabilitar tema"><i class="md-done"></i></button>
                                {else}
                                    <button class="estatus logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  data-keyboard="false" data-backdrop="static" pk_num_certificado="{$certificado.pk_num_certificado}" descipcion="El Usuario ha deshabilitado un tema" title="Deshabilitar tema"><i class="md md-not-interested"></i></button>
                                {/if}
                            </td>
                            <td>{$certificado.ind_nombre1} {$certificado.ind_nombre1} {$certificado.ind_apellido1} {$certificado.ind_apellido2}</td>
                            <td>{$certificado.fecha_modificacion}</td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="6">
                            {if in_array('EV-01-02-01-N',$_Parametros.perfil)}<button  class="logsUsuario btn ink-reaction btn-raised btn-info" descripcion="El usuario ha creado gestionado un certificado" data-toggle="modal" data-target="#formModal" titulo="Gestionar Certificado" id="nuevo"> Gestión de Certificados</button>{/if}
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modEV/certificadosCONTROL/GestionCertificadoMET';
        $('#nuevo').click(function(){
            $('#modalAncho').css( "width", "80%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
    });

    $('.aniimated-thumbnials').lightGallery({
        thumbnail:true
		
    });

    var $urlEstatus = '{$_Parametros.url}modEV/certificadosCONTROL/CambiarEstatusMET';
    $('#datatable1 tbody').on( 'click', '.estatus', function () {
        var pk_num_certificado =  $(this).attr('pk_num_certificado');
        $.post($urlEstatus,{ pk_num_certificado: pk_num_certificado},function(dato){
            $('#pk_num_certificado'+dato['pk_num_certificado']).remove();
            if(dato['num_estatus']==1){
                var botonEstatus =  '<button class="estatus logsUsuario btn ink-reaction btn-raised btn-xs btn-success"  data-keyboard="false" data-backdrop="static" pk_num_certificado="'+dato['pk_num_certificado']+'" descripcion="El Usuario ha deshabilitado un certificado" title="Deshabilitar Certificado"><i class="md-done"></i></button>'
            } else {
                var botonEstatus = '<button class="estatus logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  data-keyboard="false" data-backdrop="static" pk_num_certificado="'+dato['pk_num_certificado']+'" descipcion="El Usuario ha deshabilitado un certificado" title="Deshabilitar Certificado"><i class="md md-not-interested"></i></button>';
            }
            var botonCargar = ' <div align="center"><a href="{$_Parametros.url}publico/imagenes/modEV/fondo/'+dato['ind_imagen']+'" target=""> <img width="180" height="140" src="{$_Parametros.url}publico/imagenes/modEV/fondo/'+dato['ind_imagen']+'"/> </a> </div>';
            var nombre = dato['ind_nombre1']+' '+dato['ind_nombre2']+' '+dato['ind_apellido1']+' '+dato['ind_apellido2'];
            $(document.getElementById('datatable1')).append('<tr id="pk_num_certificado'+dato['pk_num_certificado']+'">' +
                    '<td>'+dato['pk_num_certificado']+'</td>' +
                    '<td>'+dato['ind_imagen']+'</td>' +
                    '<td>'+botonCargar+'</td>' +
                    '<td align="center">'+botonEstatus+'</td>' +
                    '<td>'+nombre+'</td>' +
                    '<td>'+dato['fecha_modificacion']+'</td>' +
                    '</tr>');
        },'json');
    });
</script>
