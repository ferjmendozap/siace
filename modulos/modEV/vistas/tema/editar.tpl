<form action="{$_Parametros.url}modEV/temaCONTROL/EditarTemaMET" id="formAjax" method="post" class="form" role="form">
<div class="modal-body">
	<input type="hidden" value="1" name="valido" />
	{if isset($evento.pk_num_tema_evento)}
		<input type="hidden" value="{$evento.pk_num_tema_evento}" name="pk_num_tema_evento" id="pk_num_tema_evento" />
	{/if}
	<div class="form-group floating-label">
		<input type="text" class="form-control" id="regular2" value="{$evento.pk_num_tema_evento}" disabled="disabled">
		<label for="regular2">N°</label>
	</div>
	<div class="form-group floating-label">
		<input type="text" class="form-control" name="ind_tema" id="ind_tema" value="{$evento.ind_tema}">
		<label for="regular2">Tema de Evento</label>
	</div>
	<div class="form-group floating-label">
		<input type="text" class="form-control" id="regular2" value="{$evento.fec_ultima_modificacion}" disabled="disabled">
		<label for="regular2">Última modificación</label>
	</div>
	<div class="form-group floating-label">
		<input type="text" class="form-control" id="regular2" value="{$evento.ind_nombre1} {$evento.ind_nombre2} {$evento.ind_apellido1} {$evento.ind_apellido2}" disabled="disabled">
		<label for="regular2">Último usuario</label>
	</div>
	<div align="right">
		<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
		<button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
	</div>
</div>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        $("#formAjax").validate({
			rules:{
				ind_tema:{
					required:true,
					remote:{
						url:"{$_Parametros.url}modEV/temaCONTROL/VerificarTemaMET",
						type:"post"
					}
				}
			},
			messages:{
				ind_tema:{
					required: "Este campo es requerido",
					remote: jQuery.validator.format("Ya se encuentra registrado en el sistema")
				}
			},
			submitHandler: function(form){
				$.post($(form).attr('action'), $(form).serialize(),function(dato){
					$('#pk_num_tema_evento'+dato['pk_num_tema_evento']).remove();
					if(dato['num_estatus']==1){
						var botonEstatus =  '<button class="estatus logsUsuario btn ink-reaction btn-raised btn-xs btn-success"  data-keyboard="false" data-backdrop="static" pk_num_tema_evento="'+dato['pk_num_tema_evento']+'" descripcion="El Usuario ha deshabilitado un tema" title="Deshabilitar Tema"><i class="md-done"></i></button>'
					} else {
						var botonEstatus = '<button class="estatus logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  data-keyboard="false" data-backdrop="static" pk_num_tema_evento="'+dato['pk_num_tema_evento']+'" descipcion="El Usuario ha deshabilitado un tema" title="Deshabilitar Tema"><i class="md md-not-interested"></i></button>';
					}
                    if(dato['total']==0){
                        var botonEliminar = '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_lugar_evento="'+dato['pk_num_lugar_evento']+'"  boton="si, Eliminar" descipcion="El usuario ha eliminado un lugar evento" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar el lugar del evento?" title="Eliminar lugar de evento"><i class="md md-delete"></i></button>';
                    } else if(dato['total']>0){
                        var botonEliminar = '';
                    }
					$(document.getElementById('datatable1')).append('<tr id="pk_num_tema_evento'+dato['pk_num_tema_evento']+'"><td>'+dato['pk_num_tema_evento']+'</td>' +
							'<td>'+dato['ind_tema']+'</td>' +
							'<td align="center">'+botonEstatus+'</td>' +
					'<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha modificado un tema" titulo="Modificar Tema" title="Modificar Tema" data-toggle="modal" data-target="#formModal" pk_num_tema_evento="'+dato['pk_num_tema_evento']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td><td align="center">'+ botonEliminar +'</td></tr>');
					swal("Tema modificado", "Tema modificado exitosamente", "success");
					$(document.getElementById('cerrarModal')).click();
					$(document.getElementById('ContenidoModal')).html('');
            },'json');
        }
    });    
});
</script>


