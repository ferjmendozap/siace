<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO SUCRE.
 * MODULO: Control de Eventos.
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.sistemas.control@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// // Esta clase se encarga de gestionar los lugares donde se realizan los eventos

class lugarModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método que permite listar los lugares añadidos al sistema
    public function metListarLugar()
    {
        $listarLugar = $this->_db->query(
            "select a.pk_num_lugar_evento, a.num_estatus, a.ind_lugar_evento, count(a.pk_num_lugar_evento) as total from ev_c003_lugar as a left join ev_b001_evento as b on a.pk_num_lugar_evento=b.fk_evc003_num_lugar group by a.pk_num_lugar_evento"
        );
        $listarLugar->setFetchMode(PDO::FETCH_ASSOC);
        return $listarLugar->fetchAll();
    }

    // Método que permite guardar un lugar en el sistema
    public function metGuardarLugar($indLugarEvento, $fechaHora , $usuario,  $pkNumSector)
    {
        $this->_db->beginTransaction();
        $NuevoLugar = $this->_db->prepare(
            "insert into ev_c003_lugar values (null, :ind_lugar_evento, :num_estatus, :fk_a013_num_sector, :fk_a018_num_seguridad_usuario, :fec_ultima_modificacion)"
        );
        $NuevoLugar->execute(array(
        ':ind_lugar_evento' => $indLugarEvento,
        ':num_estatus' => 1,
        ':fk_a013_num_sector' => $pkNumSector,
        ':fk_a018_num_seguridad_usuario' => $usuario,
        ':fec_ultima_modificacion' => $fechaHora
    ));
        $pkNumLugarEvento = $this->_db->lastInsertId();
        $this->_db->commit();
        return $pkNumLugarEvento;
    }

    // Método que permite verificar la existencia en el sistema del lugar que se quiere registrar
    public function metBuscarLugar($indLugarEvento)
    {
        $buscarLugar = $this->_db->query(
            "select pk_num_lugar_evento from ev_c003_lugar where ind_lugar_evento='$indLugarEvento'"
        );
        $buscarLugar->setFetchMode(PDO::FETCH_ASSOC);
        $arreglo = $buscarLugar->fetchAll();
        if (count($arreglo) > 0) {
            return true;
        } else {
            return false;
        }
    }

    // Método que permite buscar un lugar en especifico
    public function metVerLugar($pkNumLugarEvento)
    {
        $verLugar = $this->_db->query(
            "select a.pk_num_lugar_evento, a.num_estatus, a.fk_a013_num_sector, a.ind_lugar_evento, a.fec_ultima_modificacion, c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, (select count(fk_evc003_num_lugar) as total from ev_b001_evento where fk_evc003_num_lugar=$pkNumLugarEvento) as total from ev_c003_lugar as a, a018_seguridad_usuario as b, vl_rh_persona_empleado as c where a.pk_num_lugar_evento=$pkNumLugarEvento and a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado"
        );
        $verLugar->setFetchMode(PDO::FETCH_ASSOC);
        return $verLugar->fetch();
    }

    // Método que permite cambiar el estatus de un lugar
    public function metCambiarEstatus($pkNumLugarEvento, $valor)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update ev_c003_lugar set num_estatus=$valor where pk_num_lugar_evento=$pkNumLugarEvento"
        );
        $this->_db->commit();
    }

    // Método que permite editar un lugar
    public function metEditarLugar($ind_lugar_evento, $fecha_hora, $usuario, $pkNumLugarEvento, $pkNumSector)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update ev_c003_lugar set ind_lugar_evento='$ind_lugar_evento', fec_ultima_modificacion='$fecha_hora', fk_a018_num_seguridad_usuario=$usuario, fk_a013_num_sector=$pkNumSector where pk_num_lugar_evento=$pkNumLugarEvento"
        );
        $this->_db->commit();
    }

    // Método que permite obtener el pais donde reside un usuario
    public function metVerUsuario($usuario)
    {
        $verUsuario =  $this->_db->query(
            "select g.pk_num_pais from a018_seguridad_usuario as a, vl_rh_persona_empleado as b, a013_sector as c, a012_parroquia as d, a011_municipio as e, a009_estado as f, a008_pais as g where a.pk_num_seguridad_usuario=$usuario and a.fk_rhb001_num_empleado=b.pk_num_empleado and b.fk_a013_num_sector=c.pk_num_sector and c.fk_a012_num_parroquia=d.pk_num_parroquia and d.fk_a011_num_municipio=e.pk_num_municipio and e.fk_a009_num_estado=f.pk_num_estado and f.fk_a008_num_pais=g.pk_num_pais"
        );
        $verUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $verUsuario->fetch();
    }

    // Método que permite consultar la ubicación del lugar
    public function metConsultarUbicacion($pkNumSector)
    {
        $verUbicacion =  $this->_db->query(
            "select b.pk_num_sector, c.pk_num_parroquia, d.pk_num_municipio, e.pk_num_estado, f.pk_num_pais, h.pk_num_ciudad from ev_c003_lugar as a, a013_sector as b, a012_parroquia as c, a011_municipio as d, a009_estado as e, a008_pais as f,
 a014_ciudad_municipio as g, a010_ciudad as h where a.fk_a013_num_sector=b.pk_num_sector and b.fk_a012_num_parroquia=c.pk_num_parroquia and c.fk_a011_num_municipio=d.pk_num_municipio and d.fk_a009_num_estado=e.pk_num_estado and e.fk_a008_num_pais=f.pk_num_pais and d.pk_num_municipio=g.fk_a011_num_municipio and g.fk_a010_num_ciudad=h.pk_num_ciudad and b.pk_num_sector=$pkNumSector"
        );
        $verUbicacion->setFetchMode(PDO::FETCH_ASSOC);
        return $verUbicacion->fetch();
    }

    public function metConsultarLugar($pkNumLugarEvento)
    {
        $listarLugar = $this->_db->query(
            "select count(fk_evc003_num_lugar) as total from ev_b001_evento where fk_evc003_num_lugar=$pkNumLugarEvento"
        );
        $listarLugar->setFetchMode(PDO::FETCH_ASSOC);
        return $listarLugar->fetch();
    }

    public function metEliminarLugar($pkNumLugarEvento)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from ev_c003_lugar where pk_num_lugar_evento = $pkNumLugarEvento"
        );
        $this->_db->commit();
    }
}// fin de la clase
?>
