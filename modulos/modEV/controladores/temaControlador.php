<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO SUCRE.
 * MODULO: Control de Eventos.
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.sistemas.control@cgesucre.gob.ve | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Esta clase se encarga de gestionar los temas de los eventos
class temaControlador extends Controlador
{
	private $atTemaModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atTemaModelo = $this->metCargarModelo('tema');
	}

	// Método index del controlador
	public function metIndex(){
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJs($js);
		$validar=array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$this->atVista->assign('listadoTema', $this->atTemaModelo->metListarTema());
		$this->atVista->metRenderizar('listado');
	}

	//Método que permite registrar un nuevo tema
	public function metNuevoTema()
	{
		$valido = $this->metObtenerInt('valido');
		if (isset($valido) && $valido == 1) {
			$this->metValidarToken();
			$indTemaEvento = $_POST['ind_tema'];
			$usuario = Session::metObtener('idUsuario');
			$fechaHora = date('Y-m-d H:i:s');
			$pkNumTemaEvento = $this->atTemaModelo->metGuardarTema($indTemaEvento, $fechaHora , $usuario);
			$consultarTema = $this->atTemaModelo->metConsultarTema($pkNumTemaEvento);
			$arrayPost = array(
				'pk_num_tema_evento' => $pkNumTemaEvento,
				'ind_tema' => $indTemaEvento,
				'num_estatus' => 1,
				'total' => $consultarTema['total']
			);
			echo json_encode($arrayPost);
			exit;
		}
		$this->atVista->metRenderizar('nuevo', 'modales');
	}

	//Método que permite editar un tema
	public function metEditarTema()
	{
		$pkNumTemaEvento = $this->metObtenerInt('pk_num_tema_evento');
		$valido = $this->metObtenerInt('valido');
		if (isset($valido) && $valido == 1) {
			$this->metValidarToken();
			$indTema = $_POST['ind_tema'];
			$usuario = Session::metObtener('idUsuario');
			$fecha_hora = date('Y-m-d H:i:s');
			$this->atTemaModelo->metEditarTema($indTema, $fecha_hora, $usuario, $pkNumTemaEvento);
			$temaEvento = $this->atTemaModelo->metVerTema($pkNumTemaEvento);
            $consultarTema = $this->atTemaModelo->metConsultarTema($pkNumTemaEvento);
			$arrayPost = array(
				'status' => 'modificar',
				'ind_tema' => $temaEvento['ind_tema'],
				'pk_num_tema_evento' => $pkNumTemaEvento,
				'num_estatus' => $temaEvento['num_estatus'],
				'total' => $consultarTema['total']
			);
			echo json_encode($arrayPost);
			exit;
		}
		if (!empty($pkNumTemaEvento)) {
			$listarEvento = $this->atTemaModelo->metVerTema($pkNumTemaEvento);
			$this->atVista->assign('evento', $listarEvento);
			$this->atVista->metRenderizar('editar', 'modales');
		}
	}

	// Método que permite verificar la existencia de un tema en el sistema
	public function metVerificarTema() {
		$indTemaEvento = $_POST['ind_tema'];
		$temaExiste = $this->atTemaModelo->metBuscarTema($indTemaEvento);
		echo json_encode(!$temaExiste);
	}

	// Método que permite cambiar el estatus de un tema
	public function metCambiarEstatus()
	{
		$pkNumTemaEvento = $this->metObtenerInt('pk_num_tema_evento');
		$verTema = $this->atTemaModelo->metVerTema($pkNumTemaEvento);
		$estatus = $verTema['num_estatus'];
		if($estatus==1) {
			$valor = 0;
		} else {
			$valor = 1;
		}
		$this->atTemaModelo->metCambiarEstatus($pkNumTemaEvento, $valor);
		$consultarTema = $this->atTemaModelo->metVerTema($pkNumTemaEvento);
		echo json_encode($consultarTema);
	}

    // Eliminar lugar
    public function metEliminarTema()
    {
        $pkNumTemaEvento = $this->metObtenerInt('pk_num_tema_evento');
        $this->atTemaModelo->metEliminarTema( $pkNumTemaEvento);
        $arrayPost = array(
            'status' => 'OK',
            'pk_num_tema_evento' => $pkNumTemaEvento
        );
        echo json_encode($arrayPost);
    }
}
?>
