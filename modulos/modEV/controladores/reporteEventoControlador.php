<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Eventos
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.sistemas.control@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase encargada de la gestión de vacaciones solicitadas por funcionarios
class reporteEventoControlador extends Controlador
{
    private $atReporteModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atReporteModelo = $this->metCargarModelo('reporteEvento');

        Session::metAcceso();
        if(!Session::metObtener('INDORGANISMO'))
        {
            $this->atDefaulOrg = Session::metObtener('DEFAULTORG');
            $detalleEncabezado = $this->atReporteModelo->metDetallesCabeceraOrganismo($this->atDefaulOrg);
            Session::metCrear("INDORGANISMO", $detalleEncabezado['ind_descripcion_empresa']);
            Session::metCrear("INDLOGO", $detalleEncabezado['ind_logo']);
            Session::metCrear("INDLOGOSECUNDARIO", $detalleEncabezado['ind_logo_secundario']);
        }
    }

    // Index del controlador
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker',
            'multi-select/multi-select555c'
        );
        $complementosJs = array(
            'select2/select2.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'multi-select/jquery.multi-select'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $validar = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $this->atVista->metCargarJsComplemento($validar);
        $anioEvento = $this->atReporteModelo->metConsultarAnio();
        $listarEvento = $this->atReporteModelo->metListadoEvento();
        $tipoEvento = $this->atReporteModelo->metMostrarSelect('TIPEV');
        $this->atVista->assign('tipoEvento', $tipoEvento);
        $this->atVista->assign('anioEvento', $anioEvento);
        $this->atVista->assign('listarEvento', $listarEvento);
        $listarEnte = $this->atReporteModelo->metListarEnte();
        $this->atVista->assign('listaEnte', $listarEnte);
        $this->atVista->metRenderizar('reporteEvento');
    }

    // Método que permite transformar las horas de horario militar a horario estándar
    public function metHorarioEstandar($horaEntrada)
    {
        // Hora de Entrada
        $horaExplodeInicio = explode(":", $horaEntrada);
        $horaInicio = $horaExplodeInicio[0];
        $minutoInicio = $horaExplodeInicio[1];
        // Convirtiendo a horario estándar
        if($horaInicio>12) {
            $horaInicioPrevio = $horaInicio - 12;
            $hora_inicio = str_pad($horaInicioPrevio, 2, "0", STR_PAD_LEFT);
        }
        if($horaInicio<=12) {
            $hora_inicio = str_pad($horaInicio, 2, "0", STR_PAD_LEFT);
        }
        // Horario
        if($horaInicio>=12) {
            $horarioInicio = 'PM';
        }
        if($horaInicio<12) {
            $horarioInicio = 'AM';
        }

        // Los agrupo en un array
        $datoHorario = array($hora_inicio,$minutoInicio, $horarioInicio);
        return $datoHorario;
    }

    // Método que permite buscar los meses
    public function metBuscarMes($mesFecha)
    {
        switch($mesFecha){
            case 1:
                $mes="ENERO";
                break;
            case 2:
                $mes="FEBRERO";
                break;
            case 3:
                $mes="MARZO";
                break;
            case 4:
                $mes="ABRIL";
                break;
            case 5:
                $mes="MAYO";
                break;
            case 6:
                $mes="JUNIO";
                break;
            case 7:
                $mes="JULIO";
                break;
            case 8:
                $mes="AGOSTO";
                break;
            case 9:
                $mes="SEPTIEMBRE";
                break;
            case 10:
                $mes="OCTUBRE";
                break;
            case 11:
                $mes="NOVIEMBRE";
                break;
            case 12:
                $mes="DICIEMBRE";
                break;
        }
        return $mes;
    }

    // Método que permite generar el reporte de eventos correspondiente
    public function metGenerarReporteEvento()
    {
        $busqueda = $_GET['busqueda'];
        switch($busqueda){
            case 1:
                $mesEvento = $_GET['mes_evento'];
                $anioEvento = $_GET['anio_evento'];
                $this->metGenerarReporte($mesEvento, $anioEvento, 1);
                break;
            case 2:
                $trimestre = $_GET['trimestre'];
                $anioTrimestre = $_GET['anio_trimestre'];
                $this->metGenerarReporte($trimestre, $anioTrimestre, 2);
                break;
            case 3:
                $semestre = $_GET['semestre'];
                $anioSemestre = $_GET['anio_semestre'];
                $this->metGenerarReporte($semestre, $anioSemestre, 3);
                break;
            case 4:
                $pkNumEvento = $_GET['pk_num_evento'];
                $this->metGenerarReporteParticipante($pkNumEvento);
                break;
            case 5:
                $tipoEvento = $_GET['tipo_evento'];
                $anioTipoEvento = $_GET['anio_tipo_evento'];
                $this->metGenerarEventoAnual($tipoEvento, $anioTipoEvento);
                break;
            case 6:
                $pkNumLugarEvento = $_GET['pk_num_lugar_evento'];
                $anioTipoEvento = $_GET['anio_tipo'];
                $mes = $_GET['mes'];
                $this->metGenerarReporteLugar($pkNumLugarEvento, $anioTipoEvento, $mes);
                break;
            case 7:
                $pkNumEnte = $_GET['pk_num_ente'];
                $anioEnte = $_GET['anio_ente'];
                $mesEnte = $_GET['mes_ente'];
                $this->metGenerarReporteEnte($pkNumEnte, $anioEnte, $mesEnte);
                break;
        }
    }

    // Método que permite generar el reporte de eventos
    public function metGenerarReporte($mesEvento, $anioEvento, $tipo)
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $this->metObtenerLibreria('cabeceraEvento', 'modEV');
        $pdf = new pdfMensual('L', 'mm', 'Letter');
        $pdf->AliasNbPages();
                
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 5);
        
        if($tipo==1){
            // REPORTE GENERAL DE EVENTOS
            $mes = $this->metBuscarMes($mesEvento);
            $pdf->Encabezado($mes, $anioEvento, 1, '');
            
        }
        if($tipo==2){
            // REPORTE TRIMESTRAL
            if($mesEvento==1){
                $valor = '(ENERO - MARZO)';
            } else if($mesEvento==2){
                $valor = '(ABRIL - JUNIO)';
            } else if($mesEvento==3){
                $valor = '(JULIO - SEPTIEMBRE)';
            } else {
                $valor = '(OCTUBE - DICIEMBRE)';
            }
            $pdf->Encabezado($mesEvento, $anioEvento, 2, $valor);
        }
        if($tipo==3){
            if($mesEvento==1){
                $valor = '(ENERO - JUNIO)';
            } else {
                $valor = '(JULIO - DICIEMBRE)';
            }
            $pdf->Encabezado($mesEvento, $anioEvento, 3, $valor);
        }
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetWidths(array(20, 55, 47, 18, 18, 55, 15, 30));
        $pdf->SetAligns(array('C', 'L', 'L', 'C', 'C', 'L', 'C', 'C'));
        $listarEvento = $this->atReporteModelo->metListarEvento($mesEvento, $anioEvento, $tipo);
        $pdf->SetFont('Arial', '', 8);
        $total = 0;
        foreach($listarEvento as $evento) {
            $pkNumEvento = $evento['pk_num_evento'];
            $obtenerHora = $this->metHorarioEstandar($evento['fec_hora_entrada']);
            $tipoPersonaPrevio = $this->atReporteModelo->metConsultarTipoPersona(2);
            $tipoPersona = $tipoPersonaPrevio['pk_num_miscelaneo_detalle'];
            $cantidadParticipante = $this->atReporteModelo->metObtenerParticipante($pkNumEvento, $tipoPersona);
            $tipoPersonaPonente = $this->atReporteModelo->metConsultarTipoPersona(1);
            $obtenerPonente = $this->atReporteModelo->metConsultarPonentes($pkNumEvento, $tipoPersonaPonente['pk_num_miscelaneo_detalle']);
            $i = 0;
            $ponenteNombre = array();
            foreach($obtenerPonente as $ponente){
                $ponenteNombre[$i] = $ponente['ind_nombre1'].' '. $ponente['ind_nombre2'].' '.$ponente['ind_apellido1'].' '.$ponente['ind_apellido2'];
                $i++;
            }
            $pdf->Row(array(
                $pkNumEvento,
                utf8_decode($evento['ind_nombre_evento']),
                utf8_decode($evento['ind_lugar_evento']),
                $evento['fecha_inicio'],
                $evento['fecha_fin'],
                utf8_decode($ponenteNombre[0].' '.$ponenteNombre[1]),
                $obtenerHora[0].':'.$obtenerHora[1].' '.$obtenerHora[2],
                $cantidadParticipante['total']
            ), 5);
            $total = $total + $cantidadParticipante['total'];
        }
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(229, 229, 229); $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(228,5,'TOTAL DE PARTICIPANTES: ',0,0,'R',0);
        $pdf->Cell(30,5,$total,0,0,'C',1);
        $pdf->Output();
    }

    // Método que permite generar el reporte de eventos por participante
    public function metGenerarReporteParticipante($pkNumEvento)
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $this->metObtenerLibreria('cabeceraEvento', 'modEV');
        $pdf = new pdfParticipantes('L', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 5);
        $pdf->SetFont('Arial', 'B', 8);
        $listarEvento = $this->atReporteModelo->metVerEvento($pkNumEvento);
        $pdf->Cell(255,5,'FECHA: '.$listarEvento['fecha_inicio'].' - '.$listarEvento['fecha_fin'],0,1,'L',0);
        $pdf->MultiCell(255,5,'EVENTO: '.utf8_decode($listarEvento['ind_nombre_evento']), '', 'L', 0);
        $pdf->MultiCell(255,5,'LUGAR: '.utf8_decode($listarEvento['ind_lugar_evento']), '', 'L', 0);
        $tipoPersona = $this->atReporteModelo->metConsultarTipoPersona(1);
        $ponenteEvento = $this->atReporteModelo->metConsultarPonentes($pkNumEvento, $tipoPersona['pk_num_miscelaneo_detalle']);
        $pdf->MultiCell(255,5,'PONENTE: ', '', 'L', 0);
        foreach($ponenteEvento as $ponente){
            $pdf->MultiCell(255,5,utf8_decode('        - '.$ponente['ind_nombre1'].' '.$ponente['ind_nombre2'].' '.$ponente['ind_apellido1'].' '.$ponente['ind_apellido2']), '', 'L', 0);
        }
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(229, 229, 229); $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(30,5,utf8_decode('N° EVENTO'),1,0,'C',1);
        $pdf->Cell(30,5,utf8_decode('CÉDULA'),1,0,'C',1);
        $pdf->Cell(140,5,utf8_decode('PARTICIPANTES'),1,0,'C',1);
        $pdf->Cell(30,5,utf8_decode('CERTIFICADO'),1,0,'C',1);
        $pdf->Cell(30,5,utf8_decode('N° DE CERTIFICADO'),1,0,'C',1);
        $pdf->Ln();
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetWidths(array(30, 30, 140, 30, 30));
        $pdf->SetAligns(array('C', 'C', 'L', 'C', 'C'));
        $tipoPersonaParticipante = $this->atReporteModelo->metConsultarTipoPersona(2);
        $participantesEvento = $this->atReporteModelo->metVerPersona($tipoPersonaParticipante['pk_num_miscelaneo_detalle'], $pkNumEvento, 1);
        $i = 1;
        $pdf->SetFont('Arial', '', 8);
        foreach($participantesEvento as $part){
            // Verificar si recibió el certificado
            $recibioCertificado = $part['num_flag_culmino_evento'];
            if($recibioCertificado>0){
                $recibio = 'SI';
            } else {
                $recibio = 'NO';
            }
            $pkNumPersona = $part['fk_a003_num_persona'];
            $obtenerCertificado = $this->atReporteModelo->metObtenerCertificado($pkNumEvento, $pkNumPersona, 2);
            $pdf->Row(array(
                $i,
                $part['ind_cedula_documento'],
                utf8_decode($part['ind_nombre1'].' '.$part['ind_nombre2'].' '.$part['ind_apellido1'].' '.$part['ind_apellido2']),
                $recibio,
                $obtenerCertificado['num_certificado']
            ), 5);
            $i++;
        }
        $pdf->Output();
    }

    // Método que permite generar el reporte de eventos por participante
    public function metGenerarEventoAnual($tipoEvento, $anioTipoEvento)
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $this->metObtenerLibreria('cabeceraEvento', 'modEV');
        $pdf = new pdfParticipantes('L', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 5);
        // Tipo de persona
        $tipoPersona =  $this->atReporteModelo->metConsultarTipoPersona(2);
        if($tipoEvento==0) {
            $pdf->Ln();
            $tipoEvento = $this->atReporteModelo->metMostrarSelect('TIPEV');
            foreach ($tipoEvento as $tipo) {
                $tipoEventoPrevio = $tipo['pk_num_miscelaneo_detalle'];
                $consultarEvento = $this->atReporteModelo->metConsultarEvento($tipoEventoPrevio, $anioTipoEvento);
                // Verifico si el tipo de evento tiene eventos registrados para ese año
                $cantidadEvento = count($consultarEvento);
                if ($cantidadEvento > 0) {
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->SetDrawColor(0, 0, 0);
                    $pdf->SetFillColor(229, 229, 229);
                    $pdf->SetTextColor(0, 0, 0);
                    $pdf->Cell(260, 5, utf8_decode($tipo['ind_nombre_detalle']), 1, 1, 'L', 1);
                    $pdf->SetDrawColor(0, 0, 0);
                    $pdf->SetFillColor(229, 229, 235);
                    $pdf->SetTextColor(0, 0, 0);
                    $pdf->Cell(20, 5, utf8_decode('N° EVENTO'), 1, 0, 'C', 1);
                    $pdf->Cell(90, 5, utf8_decode('EVENTO'), 1, 0, 'C', 1);
                    $pdf->Cell(60, 5, utf8_decode('LUGAR'), 1, 0, 'C', 1);
                    $pdf->Cell(30, 5, utf8_decode('FECHA INICIO'), 1, 0, 'C', 1);
                    $pdf->Cell(30, 5, utf8_decode('FECHA FIN'), 1, 0, 'C', 1);
                    $pdf->Cell(30, 5, utf8_decode('PARTICIPANTES'), 1, 1, 'C', 1);
                    $pdf->SetWidths(array(20, 90, 60, 30, 30, 30));
                    $pdf->SetAligns(array('C', 'L', 'L', 'C', 'C', 'C'));
                    $pdf->SetFont('Arial', '', 8);
                    foreach ($consultarEvento as $evento) {
                        $pkNumEvento = $evento['pk_num_evento'];
                        // Verifico la cantidad de participantes asistentes al evento
                        $listarParticipantes = $this->atReporteModelo->metVerPersona($tipoPersona['pk_num_miscelaneo_detalle'], $pkNumEvento, 1);
                        $cantidadParticipante = count($listarParticipantes);
                        $pdf->Row(array(
                            $pkNumEvento,
                            utf8_decode($evento['ind_nombre_evento']),
                            utf8_decode($evento['ind_lugar_evento']),
                            $evento['fecha_inicio'],
                            $evento['fecha_fin'],
                            $cantidadParticipante
                        ), 5);
                    }
                }
            }

        } else {
            $nombreTipoEvento = $this->atReporteModelo->metConsultarTipoEvento($tipoEvento);
            $consultarEvento = $this->atReporteModelo->metConsultarEvento($tipoEvento, $anioTipoEvento);
            // Verifico si el tipo de evento tiene eventos registrados para ese año
            $cantidadEvento = count($consultarEvento);
            if ($cantidadEvento > 0) {
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->SetDrawColor(0, 0, 0);
                $pdf->SetFillColor(229, 229, 229);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->Cell(260, 5, utf8_decode($nombreTipoEvento['ind_nombre_detalle']), 1, 1, 'L', 1);
                $pdf->SetDrawColor(0, 0, 0);
                $pdf->SetFillColor(229, 229, 235);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->Cell(20, 5, utf8_decode('N° EVENTO'), 1, 0, 'C', 1);
                $pdf->Cell(90, 5, utf8_decode('EVENTO'), 1, 0, 'C', 1);
                $pdf->Cell(60, 5, utf8_decode('LUGAR'), 1, 0, 'C', 1);
                $pdf->Cell(30, 5, utf8_decode('FECHA INICIO'), 1, 0, 'C', 1);
                $pdf->Cell(30, 5, utf8_decode('FECHA FIN'), 1, 0, 'C', 1);
                $pdf->Cell(30, 5, utf8_decode('PARTICIPANTES'), 1, 1, 'C', 1);
                $pdf->SetWidths(array(20, 90, 60, 30, 30, 30));
                $pdf->SetAligns(array('C', 'L', 'L', 'C', 'C', 'C'));
                $pdf->SetFont('Arial', '', 8);
                foreach ($consultarEvento as $evento) {
                    $pkNumEvento = $evento['pk_num_evento'];
                    // Verifico la cantidad de participantes asistentes al evento
                    $listarParticipantes = $this->atReporteModelo->metVerPersona($tipoPersona['pk_num_miscelaneo_detalle'], $pkNumEvento, 1);
                    $cantidadParticipante = count($listarParticipantes);
                    $pdf->Row(array(
                        $pkNumEvento,
                        utf8_decode($evento['ind_nombre_evento']),
                        utf8_decode($evento['ind_lugar_evento']),
                        $evento['fecha_inicio'],
                        $evento['fecha_fin'],
                        $cantidadParticipante
                    ), 5);
                }
            }
        }
        $pdf->Output();
    }

    // Permite crear un reporte de eventos basado en los lugares donde se dicto
    public function metGenerarReporteLugar($pkNumLugarEvento, $anioTipoEvento, $mes)
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $this->metObtenerLibreria('cabeceraEvento', 'modEV');
        $pdf = new pdfLugares('L', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 5);
        $listarEvento = $this->atReporteModelo->metBuscarEventos($pkNumLugarEvento, $anioTipoEvento, $mes);
        $pdf->Ln();
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetWidths(array(20, 65, 65, 18, 18, 30, 40));
        $pdf->SetAligns(array('C', 'L', 'L', 'C', 'C', 'C', 'L'));
        $pdf->SetFont('Arial', '', 8);
        foreach ($listarEvento as $evento) {
            $pdf->Row(array(
                $evento['pk_num_evento'],
                utf8_decode($evento['ind_nombre_evento']),
                utf8_decode($evento['ind_lugar_evento']),
                $evento['fecha_inicio'],
                $evento['fecha_fin'],
                $evento['fec_horas_total'],
                utf8_decode($evento['ind_nombre_detalle'])
            ), 5);
        }
        $pdf->Output();
    }

    // Permite crear un reporte de eventos basado en los tipos de ente a la cual pertenecen los participantes
    public function metGenerarReporteEnte($pkNumEnte, $anioEnte, $mesEnte)
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $this->metObtenerLibreria('cabeceraEvento', 'modEV');
        $pdf = new pdfEnte('L', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 5);
        $listarEvento = $this->atReporteModelo->metBuscarEventosEnte($pkNumEnte, $anioEnte, $mesEnte);
        $pdf->Ln();
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetWidths(array(20, 75, 18, 18, 20, 50, 50));
        $pdf->SetAligns(array('C', 'L', 'C', 'C', 'C', 'L', 'L'));
        $pdf->SetFont('Arial', '', 8);
        foreach ($listarEvento as $evento) {
            $pdf->Row(array(
                $evento['pk_num_evento'],
                utf8_decode($evento['ind_nombre_evento']),
                $evento['fecha_inicio'],
                $evento['fecha_fin'],
                $evento['fec_horas_total'],
                utf8_decode($evento['ind_nombre_detalle']),
                utf8_decode($evento['ind_nombre_ente']),
            ), 5);
        }
        $pdf->Output();
    }

    // Metodo que permite listar los meses de un evento dependiendo del año
    public function metConsultarMesEvento()
    {
        $anioEvento = $_POST['anio_evento'];
        $tipo = $_POST['tipo'];
        $listadoMes = $this->atReporteModelo->metListarMes($anioEvento);
        if($tipo==1){
            $a = '<div class="form-group floating-label"><select class="form-control dirty" name="mes" id="mes"><option value="0">&nbsp;</option>';
        } else {
            $a = '<div class="form-group floating-label"><select class="form-control dirty" name="mes_ente" id="mes_ente"><option value="0">&nbsp;</option>';
        }

        foreach($listadoMes as $mes){
            $a .='<option value="'.$mes['mes'].'">'.$mes['mes'].'</option>';
        }
        $a .= '</select><label for="mes"><i class="glyphicon glyphicon-briefcase"></i> Mes</label></div>';
        echo $a;
    }
}