<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: PLANIFICACIÓN FISCAL
 * PROCESO: INGRESO Y MANTENIMIENTO DE FASES DE PROCESOS FISCAL
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 3 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |         0426-5144382           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #3                      |        01-08-2016       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once 'funcionesGenerales.php';
class fasesFiscalModelo extends Modelo
{
    private $atFuncionesGnrles;
    public function __construct(){
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atFuncionesGrles = new funcionesGenerales();
    }
    /**
     * Consultas para listar y validar
     * @param $case
     * @param null $params
     * @return array
     */
    function metConsultaFase($case, $params=NULL){
        $tabla="pf_b003_fase";$campos="*";$sql_criterio="";$Union="";
        switch($case){
            case "listado_fases": //Lista todas las fases ó según un tipo de proceso para cargar la grilla principal.
                $campos="pf_b003_fase.*, pf_b002_proceso.txt_descripcion_proceso";
                $Union=" INNER JOIN pf_b002_proceso ON pf_b003_fase.fk_pfb002_num_proceso = pf_b002_proceso.pk_num_proceso";
                if($params){
                    $sql_criterio=" WHERE fk_pfb002_num_proceso=".$params;
                }
                $sql_criterio.=" ORDER BY cod_proceso,cod_fase";
            break;
            case "valida_ingreso": //Valida el ingreso de un registro.
                $sql_criterio=" WHERE fk_pfb002_num_proceso=".$params['pk_num_proceso']." AND txt_descripcion_fase='".$params['txt_descripcion_fase']."'";
            break;
            case "valida_modificar": //valida la actualización de un registro.
                $sql_criterio=" WHERE pk_num_fase != ".$params['idFase']." AND fk_pfb002_num_proceso=".$params['pk_num_proceso']." AND txt_descripcion_fase='".$params['txt_descripcion_fase']."'";
            break;
            case "por_tipo_proceso": //Lista los procesos para llenar los combobox de proceso
                $tabla="pf_b002_proceso";
                $campos="pk_num_proceso,txt_descripcion_proceso";
                $sql_criterio=" WHERE num_estatus='1'";
            break;
            case "por_fase": //Para mostrar los datos de fases para actualizar
                $tabla.=" AS pf_b003";
                $campos="pf_b003.*, a018.ind_usuario";
                $Union=" INNER JOIN a018_seguridad_usuario a018 ON pf_b003.fk_a018_num_seguridad_usuario = a018.pk_num_seguridad_usuario";
                $sql_criterio=" WHERE pk_num_fase=".$params;
            break;
        }
        $sql_query="SELECT $campos FROM $tabla$Union".$sql_criterio;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Ingresa un nuevo registro
     * @param array $params
     * @return array
     */
    public function metIngresarFases($params){
        $valores = array(
            'cod_fase' => $params['cod_fase'],
            'txt_descripcion_fase' => $params['txt_descripcion_fase'],
            'fk_pfb002_num_proceso' => $params['pk_num_proceso'],
            'num_estatus' => $params['num_estatus']
        );
        $this->_db->beginTransaction();
        $sql_query = $this->_db->prepare("
              INSERT INTO pf_b003_fase SET
              cod_fase=:cod_fase,
              txt_descripcion_fase=:txt_descripcion_fase,
              fk_pfb002_num_proceso=:fk_pfb002_num_proceso,
              num_estatus=:num_estatus,
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            ");
        $sql_query->execute($valores);
        $idRegistro = $this->_db->lastInsertId();
        $error = $sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$idRegistro=0;
        }else{
            $this->_db->commit();
        }
        return $idRegistro;
    }
    /**
     * Modifica un registro
     * @param array $params
     * @return bool|int
     */
    public function metModificaFases($params){
        $valores = array(
            'txt_descripcion_fase' => $params["txt_descripcion_fase"],
            'num_estatus' => $params["num_estatus"]
        );
        $this->_db->beginTransaction();
        $sql_query = $this->_db->prepare("
            UPDATE pf_b003_fase SET
                txt_descripcion_fase=:txt_descripcion_fase,
                num_estatus=:num_estatus,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=$this->atIdUsuario
              WHERE
                pk_num_fase='".$params["idFase"]."'
        ");
        $sql_query->execute($valores);
        $error = $sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$reg_afectado=0;
        }else{
            $reg_afectado=$this->_db->commit();
        }
        return $reg_afectado;
    }
    /**
     * Elimina un registro siempre y cuando no esté relacionado
     * @param $idFase
     * @return array|bool
     */
    public function metEliminaFase($idFase){
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare("DELETE FROM pf_b003_fase WHERE pk_num_fase=:pk_num_fase");
        $sql_query->execute(array('pk_num_fase'=>$idFase));
        $error=$sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack(); $reg_afectado=$error;
        }else{
            $reg_afectado=$this->_db->commit();
        }
        return $reg_afectado;
    }
}
?>