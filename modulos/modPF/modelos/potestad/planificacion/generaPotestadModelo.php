<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Planificación Fiscal
 * PROCESO: Potestad Investigativa
 * DESCRIPCIÓN: Lista las Valoraciones Jurídicas y se genera la planificación de Potestad Investigativa
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Alexis Ontiveros     | ontiveros.alexis@cmldc.gob.ve        | 0426-514.43.82
 * |_____________________________________________________________________________________
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        04-12-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/
class generaPotestadModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }
    /**
     * Busca las planificaciones de Valoraciones Jurídicas de acuerdo a los criterios de búsqueda
     * para llenar la grilla ppal. ó montarla el en form modal de generación de potestad.
     */
    public function metBuscarPlanificaciones($params){
        $sql_criterio="";$criterio=false;$Union="";
        $tabla="pf_b001_planificacion_fiscal tb_valoracion";
        $campos="tb_valoracion.*,fk_a039_num_ente";
        $Union=" INNER JOIN a041_persona_ente ON tb_valoracion.fk_a041_num_persona_ente = a041_persona_ente.pk_num_persona_ente";
        if(isset($params['idTipoProceso'])){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="tb_valoracion.fk_b002_num_proceso=".$params['idTipoProceso'];
        }
        if(isset($params['idValoracion']) AND $params['idValoracion']>0){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="tb_valoracion.pk_num_planificacion=".$params['idValoracion'];
        }else{
            if(isset($params['cbox_dependencia']) AND $params['cbox_dependencia']>0){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_valoracion.fk_a004_num_dependencia=".$params['cbox_dependencia'];
            }
            if(isset($params['cbox_centro_costo']) AND $params['cbox_centro_costo']>0){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_valoracion.fk_a023_num_centro_costo=".$params['cbox_centro_costo'];
            }
            if(isset($params['opcion_buscar']) AND $params['opcion_buscar']=='valoracionJ') {//Entra cuando se trata de buscar planificaciones de valoración jurídicas
                $sql_criterio .= ($criterio) ? " AND " : " WHERE ";$criterio = true; // OR tb_valoracion.ind_estado_planificacion='TE'
                $sql_criterio .= "(tb_valoracion.ind_estado_planificacion='AC')";
            }
            if(isset($params['txt_objetivo_plan']) AND $params['txt_objetivo_plan']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";
                $sql_criterio.="tb_valoracion.ind_objetivo LIKE('%".$params['txt_objetivo_plan']."%')";$criterio=true;
            }
            if(isset($params['cbox_ente']) AND $params['cbox_ente']>0){
                $Union.=" INNER JOIN a039_ente ON a041_persona_ente.fk_a039_num_ente = a039_ente.pk_num_ente";
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.=" pk_num_ente=".$params['cbox_ente'];
            }
            if(isset($params['txt_fechareg1']) AND $params['txt_fechareg1'] OR isset($params['txt_fechareg2']) AND $params['txt_fechareg2']){
                if(!$params['txt_fechareg1']){
                    $params['txt_fechareg1']=$params['txt_fechareg2'];
                }elseif(!$params['txt_fechareg2']){
                    $params['txt_fechareg2']=$params['txt_fechareg1'];
                }
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.=" CAST(tb_valoracion.fec_registro_planificacion AS DATE) BETWEEN '".$params['txt_fechareg1']."' AND '".$params['txt_fechareg2']."'";
            }else{
                if(isset($params['cbox_yearplan']) AND $params['cbox_yearplan']>0){
                    $sql_criterio.=($criterio)? " AND ": " WHERE ";
                    $sql_criterio.="YEAR(tb_valoracion.fec_registro_planificacion)='".$params['cbox_yearplan']."'";$criterio=true;
                }
            }
            $sql_criterio .= ($criterio) ? " AND " : " WHERE ";
            $sql_criterio .= "tb_valoracion.pk_num_planificacion NOT IN (SELECT tb_potestad.num_planificacion_padre FROM pf_b001_planificacion_fiscal tb_potestad )";
        }
        $sql_criterio.=" ORDER BY cod_planificacion ASC";
        $sql_query="SELECT $campos FROM $tabla$Union".$sql_criterio;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Busca auditores asignados a la planificación.
     * @param $params
     * @return array
     */
    public function metAuditoresDesignados($params)
    {
        $tabla="pf_c002_auditor_planificacion"; $campos="*";$sql_criterio="";$criterio=false;
        $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="fk_pfb001_num_planificacion=".$params['idPlanificacion']; $criterio=true;
        if(isset($params['pk_num_empleado'])){
            $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="fk_rhb001_num_empleado = ".$params['pk_num_empleado'];$criterio=true;
        }
        if(isset($params['flag_coordinador'])){
            $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="num_flag_coordinador = ".$params['flag_coordinador'];
        }
        $sql_query="SELECT $campos FROM $tabla".$sql_criterio;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }

    /**
     * Permite la búsqueda de actividades relacionas a la planificación.
     * @param $params
     * @return array
     */
    public function metActividadesAsignadas($params){
        $tabla="pf_c001_actividad_planific"; $campos="*";$sql_criterio="";$criterio=false;
        if(isset($params['idPlanificacion'])) {
            $sql_criterio = ($criterio) ? " AND " : " WHERE ";$criterio = true;
            $sql_criterio .= "fk_pfb001_num_planificacion=" . $params['idPlanificacion'];
        }
        if(isset($params['ind_estado'])) {
            $sql_criterio .= ($criterio) ? " AND " : " WHERE ";
            $sql_criterio .= "ind_estado_actividad='".$params['ind_estado']."'";
        }
        if($params['limite']) {
            $sql_criterio .= " LIMIT " . $params['limite'];
        }
        if(isset($params['completa'])) {
            $campos = "pf_c001_actividad_planific.*,num_dias_duracion_actividad AS num_duracion_actividad,pk_num_actividad, txt_descripcion_actividad,ind_afecto_plan,pf_b003_fase.cod_fase,pf_b003_fase.txt_descripcion_fase";
            $Union = " INNER JOIN pf_b004_actividad ON pf_c001_actividad_planific.fk_pfb004_num_actividad=pf_b004_actividad.pk_num_actividad ";
            $Union .= " INNER JOIN pf_b003_fase ON pf_b004_actividad.fk_pfb003_num_fase=pf_b003_fase.pk_num_fase ";
            $sql_criterio.=" ORDER BY cod_fase, cod_actividad";
        }
        $sql_query="SELECT $campos FROM $tabla$Union".$sql_criterio;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Lista las actividades restantes disponibles para agregar a una actuación.
     */
    public function metListaActividadesDisp($params){
        $tabla="pf_c005_actividad_tipoactuac";$campos="*";$sql_criterio="";$criterio=false;
        $campos="pf_b004_actividad.*,pf_b003_fase.cod_fase,pf_b003_fase.txt_descripcion_fase";
        $Union=" INNER JOIN pf_b004_actividad ON pf_c005_actividad_tipoactuac.fk_pfb004_num_actividad=pf_b004_actividad.pk_num_actividad ";
        $Union.=" INNER JOIN pf_b003_fase ON pf_b004_actividad.fk_pfb003_num_fase=pf_b003_fase.pk_num_fase ";
        if(isset($params['idTipoProceso'])) {
            $sql_criterio = ($criterio) ? " AND " : " WHERE ";$criterio = true;
            $sql_criterio .= "fk_pfb002_num_proceso_tipo=" . $params['idTipoProceso'];
        }
        if(isset($params['idTipoActuacion'])) {
            $sql_criterio .= ($criterio) ? " AND " : " WHERE ";
            $sql_criterio .= "fk_a006_num_miscelaneodet=".$params['idTipoActuacion'];
        }
        $sql_criterio .= " AND pf_b004_actividad.num_estatus=1 ORDER BY cod_fase, cod_actividad";
        $sql_query="SELECT $campos FROM $tabla$Union".$sql_criterio;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Ingresa los datos principales de la Planificación.
     */
    public function metIngresaPlanificacion($params){
        $fecha_actual = date('Y-m-d H:i:s');
        $arrValores=array(
            'num_planificacion_padre'=>$params['idValoracion'],
            'fk_b002_num_proceso'=>$params['idTipoProceso'],
            'fk_a001_num_organismo'=>$params['id_contraloria'],
            'fk_a004_num_dependencia'=>$params['id_dependencia'],
            'fk_a041_num_persona_ente'=>$params['fk_a041_num_persona_ente'],
            'fk_a006_num_miscdet_tipoactuacion'=>$params['idtipo_actuacion'],
            'fk_a006_num_miscdet_origenactuacion'=>$params['idorigen_actuacion'],
            'fk_a023_num_centro_costo'=>$params['pk_num_centro_costo'],
            'ind_objetivo'=>$params['ind_objetivo'],
            'ind_alcance'=>$params['ind_alcance'],
            'ind_observacion'=>$params['ind_observacion']?$params['ind_observacion']:null,
            'fk_rhb001_num_empleado_preparado'=>$params['idEmpleadoUser'],
            'fec_preparacion'=>$fecha_actual,
            'fec_inicio'=>$params['fec_inicio'],
            'fk_a018_num_seg_usuario_reg'=>$this->atIdUsuario,
            'fec_registro_planificacion'=>$fecha_actual,
            'fk_a018_num_seg_usuario_mod'=>$this->atIdUsuario,
            'fec_ultima_modific_planificacion' =>$fecha_actual
        );
        $this->_db->beginTransaction();
        $sql_query = $this->_db->prepare("
          INSERT INTO pf_b001_planificacion_fiscal SET
          num_planificacion_padre=:num_planificacion_padre,
          fk_b002_num_proceso=:fk_b002_num_proceso,
          fk_a001_num_organismo=:fk_a001_num_organismo,
          fk_a004_num_dependencia=:fk_a004_num_dependencia,
          fk_a041_num_persona_ente=:fk_a041_num_persona_ente,
          fk_a006_num_miscdet_tipoactuacion=:fk_a006_num_miscdet_tipoactuacion,
          fk_a006_num_miscdet_origenactuacion=:fk_a006_num_miscdet_origenactuacion,
          fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
          ind_objetivo=:ind_objetivo,
          ind_alcance=:ind_alcance,
          ind_observacion=:ind_observacion,
          fk_rhb001_num_empleado_preparado=:fk_rhb001_num_empleado_preparado,
          fec_preparacion=:fec_preparacion,
          fec_inicio=:fec_inicio,
          fk_a018_num_seg_usuario_reg=:fk_a018_num_seg_usuario_reg,
          fec_registro_planificacion=:fec_registro_planificacion,
          fk_a018_num_seg_usuario_mod=:fk_a018_num_seg_usuario_mod,
          fec_ultima_modific_planificacion=:fec_ultima_modific_planificacion");
        $sql_query->execute($arrValores);
        $idPlanificacion = $this->_db->lastInsertId();
        $error = $sql_query->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack(); $retornar = false;
        } else {
            $this->_db->commit();$retornar = $idPlanificacion;
        }
        return $retornar;
    }
    /***
     * Actualiza los datos principales de la Actuación actual.
     */
    public function metActualizaPlanificacion($params)
    {
        $fecha_actual = date('Y-m-d H:i:s');
        $arrValores = array(
            'fk_a041_num_persona_ente'=>$params['fk_a041_num_persona_ente'],
            'fk_a006_num_miscdet_origenactuacion' => $params['idorigen_actuacion'],
            'fk_a023_num_centro_costo' => $params['pk_num_centro_costo'],
            'ind_objetivo' => $params['ind_objetivo'],
            'ind_alcance' => $params['ind_alcance'],
            'ind_observacion' => $params['ind_observacion'] ? $params['ind_observacion'] : null,
            'fk_rhb001_num_empleado_preparado' => $params['idEmpleadoUser'],
            'fec_preparacion' => $fecha_actual,
            'fec_inicio' => $params['fec_inicio'],
            'fk_a018_num_seg_usuario_mod' => $this->atIdUsuario,
            'fec_ultima_modific_planificacion' => $fecha_actual,
            'pk_num_planificacion' => $params['idPlanificacion'],
        );
        $this->_db->beginTransaction();
        $sql_query = $this->_db->prepare("
              UPDATE pf_b001_planificacion_fiscal SET
              fk_a041_num_persona_ente=:fk_a041_num_persona_ente,
              fk_a006_num_miscdet_origenactuacion=:fk_a006_num_miscdet_origenactuacion,
              fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
              ind_objetivo=:ind_objetivo,
              ind_alcance=:ind_alcance,
              ind_observacion=:ind_observacion,
              fk_rhb001_num_empleado_preparado=:fk_rhb001_num_empleado_preparado,
              fec_preparacion=:fec_preparacion,
              fec_inicio=:fec_inicio,
              fk_a018_num_seg_usuario_mod=:fk_a018_num_seg_usuario_mod,
              fec_ultima_modific_planificacion=:fec_ultima_modific_planificacion
              WHERE pk_num_planificacion=:pk_num_planificacion");
        $sql_query->execute($arrValores);
        $error = $sql_query->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            $retornar = array("reg_afectado" => "", "Error" => "errorSql");
        } else {
            $this->_db->commit();
            $retornar = array("reg_afectado" => true, "Error" => "");
        }
        return $retornar;
    }
    /***
     * Actualiza los datos principales de la Planificación y actualiza las actividades relacionadas cuando existe una cambio de fecha de planificación.
     */
    public function metActualizaPlanYActividades($params){
        $fecha_actual = date('Y-m-d H:i:s');
        $arrValores = array(
            'fk_a041_num_persona_ente'=>$params['fk_a041_num_persona_ente'],
            'fk_a006_num_miscdet_origenactuacion' => $params['idorigen_actuacion'],
            'fk_a023_num_centro_costo' => $params['pk_num_centro_costo'],
            'ind_objetivo' => $params['ind_objetivo'],
            'ind_alcance' => $params['ind_alcance'],
            'ind_observacion' => $params['ind_observacion'] ? $params['ind_observacion'] : null,
            'fk_rhb001_num_empleado_preparado' => $params['idEmpleadoUser'],
            'fec_preparacion' => $fecha_actual,
            'fec_inicio' => $params['fec_inicio'],
            'fk_a018_num_seg_usuario_mod' => $this->atIdUsuario,
            'fec_ultima_modific_planificacion' => $fecha_actual,
            'pk_num_planificacion' => $params['idPlanificacion'],
        );
        $this->_db->beginTransaction();
        $sql_query1 = $this->_db->prepare("
              UPDATE pf_b001_planificacion_fiscal SET
              fk_a041_num_persona_ente=:fk_a041_num_persona_ente,
              fk_a006_num_miscdet_origenactuacion=:fk_a006_num_miscdet_origenactuacion,
              fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
              ind_objetivo=:ind_objetivo,
              ind_alcance=:ind_alcance,
              ind_observacion=:ind_observacion,
              fk_rhb001_num_empleado_preparado=:fk_rhb001_num_empleado_preparado,
              fec_preparacion=:fec_preparacion,
              fec_inicio=:fec_inicio,
              fk_a018_num_seg_usuario_mod=:fk_a018_num_seg_usuario_mod,
              fec_ultima_modific_planificacion=:fec_ultima_modific_planificacion
              WHERE pk_num_planificacion=:pk_num_planificacion");
        $sql_query1->execute($arrValores);
        $error = $sql_query1->errorInfo();
        //Se actualiza las actividades del plan
        $secuencia=1;
        $sql_query2 = $this->_db->prepare("
            UPDATE pf_c001_actividad_planific SET
            num_secuencia_actividad=:num_secuencia_actividad,
            fec_inicio_actividad=:fec_inicio_actividad,
            fec_inicio_real_actividad=:fec_inicio_real_actividad,
            fec_culmina_actividad=:fec_culmina_actividad,
            fec_culmina_real_actividad=:fec_culmina_real_actividad,
            num_dias_duracion_actividad=:num_dias_duracion_actividad,
            fk_a018_num_seg_usuaro_map=:fk_a018_num_seg_usuaro_map,
            fec_modifica_ap=:fec_modifica_ap
            WHERE fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion AND fk_pfb004_num_actividad=:fk_pfb004_num_actividad");
        foreach($params['hdd_dataActividad'] as $cad_actividad) {
            $arrLapsos = explode('N', $cad_actividad);
            $id_actividad=$arrLapsos[0];
            $arrValores=array(
                'num_secuencia_actividad'=>$secuencia,
                'fec_inicio_actividad'=>$arrLapsos[2],
                'fec_inicio_real_actividad'=>$arrLapsos[4],
                'fec_culmina_actividad'=>$arrLapsos[3],
                'fec_culmina_real_actividad'=>$arrLapsos[5],
                'num_dias_duracion_actividad'=>$arrLapsos[1],
                'fk_a018_num_seg_usuaro_map'=>$this->atIdUsuario,
                'fec_modifica_ap'=>$fecha_actual,
                'fk_pfb001_num_planificacion'=>$params['idPlanificacion'],
                'fk_pfb004_num_actividad'=>$id_actividad
            );
            $sql_query2->execute($arrValores);
            $error2 = $sql_query2->errorInfo();
            $secuencia++;
        }
        if (!empty($error[1]) && !empty($error[2]) OR !empty($error2[1]) && !empty($error2[2])) {
            $this->_db->rollBack(); $retornar = false;
        } else {
            $this->_db->commit(); $retornar = true;
        }
        return $retornar;
    }
    /**
     * Asigna un auditor de la planificación.
     * @param $params
     * @return array
     */
    public function metIngresaAuditor($params){
        $fecha_actual=date('Y-m-d H:i:s');
        $arrValores=array(
            'fk_pfb001_num_planificacion'=>$params['idPlanificacion'],
            'fk_rhb001_num_empleado'=>$params['pk_num_empleado'],
            'num_flag_coordinador'=>$params['flagCoord'],
            'fec_coordinador'=>$params['flagCoord']==1?date('Y-m-d'):NULL,
            'fk_a018_num_seg_user_regauditor'=>$this->atIdUsuario,
            'fec_registro_auditor'=>$fecha_actual,
            'fec_estatus'=>date('Y-m-d')
        );
        $this->_db->beginTransaction();
        $sql_query = $this->_db->prepare("
              INSERT INTO pf_c002_auditor_planificacion SET
              fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion,
              fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
              num_flag_coordinador=:num_flag_coordinador,
              fec_coordinador=:fec_coordinador,
              fk_a018_num_seg_user_regauditor=:fk_a018_num_seg_user_regauditor,
              fec_registro_auditor=:fec_registro_auditor,
              fec_estatus=:fec_estatus");
        $sql_query->execute($arrValores);
        $id_x = $this->_db->lastInsertId();
        $error = $sql_query->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            $retornar = array("idDesignado" =>"", "Error"=>"errorSql");
        } else {
            $this->_db->commit();
            $retornar = array("idDesignado"=>$id_x,"Error"=>"");
        }
        return $retornar;
    }

    /**
     * Quita un responsable de la planificación actual.
     * @param $IdResponsable
     * @return bool
     */
    public function metQuitarResponsable($IdResponsable,$idPlanificacion){
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare(
            "DELETE FROM pf_c002_auditor_planificacion WHERE fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion AND fk_rhb001_num_empleado=:fk_rhb001_num_empleado"
        );
        $sql_query->execute(array('fk_pfb001_num_planificacion'=>$idPlanificacion,'fk_rhb001_num_empleado'=>$IdResponsable));
        $error=$sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$retornar=false;
        }else{
            $this->_db->commit();$retornar=true;
        }
        return $retornar;
    }

    /**
     * Activa ó Inactiva un responsable de la planificación actual siempre y cuando la mísma esté aprobada.
     * @param $IdResponsable
     * @return bool
     */
    public function metEstatusResponsable($IdResponsable,$idPlanificacion,$estatus) {
        $this->_db->beginTransaction();
        $arrValores=array(
            'fk_a018_num_seg_user_regauditor'=>$this->atIdUsuario,
            'num_estatus'=>$estatus,
            'fec_estatus'=>date('Y-m-d'),
            'fk_pfb001_num_planificacion'=>$idPlanificacion,
            'fk_rhb001_num_empleado'=>$IdResponsable
        );
        $sql_query=$this->_db->prepare("UPDATE pf_c002_auditor_planificacion SET
              fk_a018_num_seg_user_regauditor=:fk_a018_num_seg_user_regauditor,
              num_estatus=:num_estatus,
              fec_estatus=:fec_estatus
              WHERE fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion AND fk_rhb001_num_empleado=:fk_rhb001_num_empleado");
        $sql_query->execute($arrValores);
        $error=$sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$retornar=false;
        }else{
            $this->_db->commit();$retornar=true;
        }
        return $retornar;
    }
    /**
     * Activa a un responsable como coordinador y inactiva el anterior
     * @param $IdResponsable
     * @param $idPlanificacion
     * @return bool
     */
    public function metSeleccionaCoordinador($IdResponsable,$idPlanificacion) {
        $this->_db->beginTransaction();
        $arrValores=array(
            'num_flag_coordinador'=>0,
            'fk_pfb001_num_planificacion'=>$idPlanificacion
        );
        $sql_query1=$this->_db->prepare("UPDATE pf_c002_auditor_planificacion SET
              num_flag_coordinador=:num_flag_coordinador
              WHERE fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion");
        $sql_query1->execute($arrValores);
        $error1=$sql_query1->errorInfo();

        $arrValores=array(
            'num_flag_coordinador'=>1,
            'fec_coordinador'=>date('Y-m-d'),
            'fk_pfb001_num_planificacion'=>$idPlanificacion,
            'fk_rhb001_num_empleado'=>$IdResponsable
        );
        $sql_query2=$this->_db->prepare("UPDATE pf_c002_auditor_planificacion SET
              num_flag_coordinador=:num_flag_coordinador,
              fec_coordinador=:fec_coordinador
              WHERE fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion AND fk_rhb001_num_empleado=:fk_rhb001_num_empleado");
        $sql_query2->execute($arrValores);
        $error2=$sql_query2->errorInfo();
        if(!empty($error1[1]) && !empty($error1[2]) OR !empty($error2[1]) && !empty($error2[2]) ){
            $this->_db->rollBack();$retornar=false;
        }else{
            $this->_db->commit();$retornar=true;
        }
        return $retornar;
    }
    /**
     * Ingresa una o un set de actividades a la planificación actual.
     * @param $params
     * @return array
     */
    public function metIngresaActividades($params){
        $fecha_actual=date('Y-m-d H:i:s');$secuencia=1;
        $this->_db->beginTransaction();
        $sql_query = $this->_db->prepare("
            INSERT INTO pf_c001_actividad_planific SET
            fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion,
            fk_pfb004_num_actividad=:fk_pfb004_num_actividad,
            num_secuencia_actividad=:num_secuencia_actividad,
            fec_inicio_actividad=:fec_inicio_actividad,
            fec_inicio_real_actividad=:fec_inicio_real_actividad,
            fec_culmina_actividad=:fec_culmina_actividad,
            fec_culmina_real_actividad=:fec_culmina_real_actividad,
            num_dias_duracion_actividad=:num_dias_duracion_actividad,
            num_dias_prorroga_actividad=:num_dias_prorroga_actividad,
            fk_a018_num_seg_usuario_rap=:fk_a018_num_seg_usuario_rap,
            fec_registro_ap=:fec_registro_ap,
            fk_a018_num_seg_usuaro_map=:fk_a018_num_seg_usuaro_map,
            fec_modifica_ap=:fec_modifica_ap");
        foreach($params['hdd_dataActividad'] AS $cad_lapsos){
            $arrLapsos = explode('N', $cad_lapsos);
            $id_actividad=$arrLapsos[0];
            $arrValores=array(
                'fk_pfb001_num_planificacion'=>$params['idPlanificacion'],
                'fk_pfb004_num_actividad'=>$id_actividad,
                'num_secuencia_actividad'=>$secuencia,
                'fec_inicio_actividad'=>$arrLapsos[2],
                'fec_inicio_real_actividad'=>$arrLapsos[4],
                'fec_culmina_actividad'=>$arrLapsos[3],
                'fec_culmina_real_actividad'=>$arrLapsos[5],
                'num_dias_duracion_actividad'=>$arrLapsos[1],
                'num_dias_prorroga_actividad'=>0,
                'fk_a018_num_seg_usuario_rap'=>$this->atIdUsuario,
                'fec_registro_ap'=>$fecha_actual,
                'fk_a018_num_seg_usuaro_map'=>$this->atIdUsuario,
                'fec_modifica_ap'=>$fecha_actual,
            );
            $sql_query->execute($arrValores);
            $idActividad = $this->_db->lastInsertId();
            $secuencia++;
        }
        $error = $sql_query->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            $retornar = array("result"=>"");
        } else {
            $this->_db->commit();
            $retornar = array("result"=>true,"idActividad_planific"=>$idActividad);
        }
        return $retornar;
    }
    /**
     * Actualiza un set de actividades de la planificación actual.
     * @param $params
     * @return bool
     */
    public function metActualizaActividades($params){
        $fecha_actual=date('Y-m-d H:i:s'); $secuencia=1;
        $this->_db->beginTransaction();
        $sql_query = $this->_db->prepare("
            UPDATE pf_c001_actividad_planific SET
            num_secuencia_actividad=:num_secuencia_actividad,
            fec_inicio_actividad=:fec_inicio_actividad,
            fec_inicio_real_actividad=:fec_inicio_real_actividad,
            fec_culmina_actividad=:fec_culmina_actividad,
            fec_culmina_real_actividad=:fec_culmina_real_actividad,
            num_dias_duracion_actividad=:num_dias_duracion_actividad,
            fk_a018_num_seg_usuaro_map=:fk_a018_num_seg_usuaro_map,
            fec_modifica_ap=:fec_modifica_ap
            WHERE fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion AND fk_pfb004_num_actividad=:fk_pfb004_num_actividad");
        foreach($params['hdd_dataActividad'] as $cad_lapsos) {
            $arrLapsos = explode('N', $cad_lapsos);
            $id_actividad=$arrLapsos[0];
            $arrValores=array(
                'num_secuencia_actividad'=>$secuencia,
                'fec_inicio_actividad'=>$arrLapsos[2],
                'fec_inicio_real_actividad'=>$arrLapsos[4],
                'fec_culmina_actividad'=>$arrLapsos[3],
                'fec_culmina_real_actividad'=>$arrLapsos[5],
                'num_dias_duracion_actividad'=>$arrLapsos[1],
                'fk_a018_num_seg_usuaro_map'=>$this->atIdUsuario,
                'fec_modifica_ap'=>$fecha_actual,
                'fk_pfb001_num_planificacion'=>$params['idPlanificacion'],
                'fk_pfb004_num_actividad'=>$id_actividad
            );
            $sql_query->execute($arrValores);
            $secuencia++;
        }
        $error = $sql_query->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();$retornar = false;
        } else {
            $this->_db->commit();$retornar = true;
        }
        return $retornar;
    }
    /**
     * Elimina el registro de asignación de una actividad de la planificación actual por id de la actividad.
     */
    public function metQuitaActividad($params)
    {
        $reg_afectado=0;
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare(
            "DELETE FROM pf_c001_actividad_planific
             WHERE fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion AND fk_pfb004_num_actividad=:fk_pfb004_num_actividad"
        );
        $sql_query->execute(array(
                'fk_pfb001_num_planificacion'=>$params['idPlanificacion_act'],
                'fk_pfb004_num_actividad'=>$params['idActividad'])
        );
        $error=$sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$retornar=false;
        }else{
            $this->_db->commit(); $retornar=true;
        }
        return $retornar;
    }
}