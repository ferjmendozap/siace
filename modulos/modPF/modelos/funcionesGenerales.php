<?php
/*****************************************************************************************************************************************
 * DEV          :CONTRALORIA DE ESTADOS
 * PROYECTO     :SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO       :PLANIFICACIÓN FISCAL
 * Descripción  :Funciones de propósito general para el módulo de Planificación Fiscal
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 3 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |         0426-514.43.82         |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #3                      |        01-08-2016       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class funcionesGenerales extends Modelo{
    /**
     * CREADO POR: Alexis Ontiveros
     * DESCRIPCIÓN: Genera el corelativo para crear un nuevo código para los maestros Proceso, Fase y Actividades
     * Retorna: Correlativo en texto
     * Parámetros:
     *  $tabla, nombre de la tabla.
     *  $campo, campo que correspondencia al código.
     *  $digitos, Cantidad de dígitos.
     *  $campo2, Campo por el cual filtar.
     *  $valor2, Valor de filtro.
     */
    public function metGeneraCodigo($tabla, $campo, $digitos, $campo2=null, $valor2=null){
        $sqlCriterio = "";$codigo="";
        $campos="SUBSTRING($campo, -2) $campo";
        if($campo2 and $valor2){
            $sqlCriterio = " WHERE $campo2 = $valor2";
        }
        $sqlCriterio.=" ORDER BY $campo DESC LIMIT 1";
        $sql_query = "SELECT $campos FROM $tabla".$sqlCriterio;
        $result = $this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $rcset = $result->fetch();
        if(is_array($rcset)){
            $codigo=(int) ($rcset[$campo]+1);
            $codigo=(string) str_repeat("0", $digitos-strlen($codigo)).$codigo;
        }else{
            $codigo=1;
            $codigo=(string) str_repeat("0", $digitos-strlen($codigo)).$codigo;
        }
        $rcset=null; return ($codigo);
    }
    public function metObtenerIdAplicacion(){
        $sql_query = $this->_db->query("SELECT pk_num_seguridad_aplicacion  FROM a015_seguridad_aplicacion WHERE cod_aplicacion='PF'");
        $sql_query->setFetchMode(PDO::FETCH_ASSOC);
        $result=$sql_query->fetch();
        $idAplicacion=$result["pk_num_seguridad_aplicacion"];$result=null;
        return $idAplicacion;
    }
    /**
     * CREADO POR: Alexis Ontiveros
     * DESCRIPCIÓN: Busca uno mas registros en una tabla.
     * Retorna: Array.
     * Parámetros:
     *   $tabla, nombre de la tabla.
     *   $campo_filtro, Nombre campo por el cual filtrar.
     *   $valor_filtro, Valor de filtro.
     */
    public function metBuscaRegistro($tabla, $campo_filtro=null, $valor_filtro=null){
        $sqlCriterio = "";
        if($campo_filtro and $valor_filtro){
            $sqlCriterio = " WHERE $campo_filtro='".$valor_filtro."'";
        }
        $result = $this->_db->query("SELECT * FROM $tabla$sqlCriterio");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * CREADO POR: Alexis Ontiveros, 14 Sep 16
     * DESCRIPCIÓN: Lista un organismo en particular ó todos.
     * Retorna: Array.
     * Parámetros.
     *   $campos            : Los campos que se desean extraer.
     *   $id_organismo      : Identificador del organismo.
     *   $estatus_organismo : Estatus (activo:1, inactivo:S0).
     *   $tipo_organismo    : El tipo de organismo (interno:IN, externo:E).
     */
    public function metBuscaOrganismos($campos,$id_organismo=NULL,$estatus_organismo=NULL,$tipo_organismo=NULL){
        $criterio=false; $sql_criterio='';$fecha_registro='';
        if($id_organismo){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="pk_num_organismo = ".$id_organismo; $criterio=true;
        }
        if($estatus_organismo){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="num_estatus = ".$estatus_organismo; $criterio=true;
        }
        if($tipo_organismo){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="ind_tipo_organismo = '".$tipo_organismo."'";
        }
        $sql_criterio.=" ORDER BY ind_descripcion_empresa";
        $arr_campos=explode(',',$campos);
        if(in_array('fec_ultima_modificacion',$arr_campos)){
            $fecha_registro=",date_format(fec_ultima_modificacion,'%d/%m/%Y') AS fec_ultima_modificacion";
        }
        $result = $this->_db->query("SELECT ".$campos."$fecha_registro FROM a001_organismo".$sql_criterio);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * CREADO POR: Alexis Ontiveros, 15 Sep 16
     * DESCRIPCIÓN: Lista dependencias internas de acuerdo a los parámetros pasados.
     * Retorna: Array.
     * Parámetros.
     *   $campos:           : Los campos que se desean extraer.
     *   $flag_controlfiscal: Dependencia de control fiscal (Si:1, No:0)
     *   $estatus_dep       : Estatus (activo:1, inactivo:S0).
     *   $id_organismo      : Identificador del organismo.
     */
    public function metBuscaDependenciaInterna($campos,$id_dependencia=NULL,$flag_controlfiscal=NULL,$estatus_dep=NULL,$id_organismo=NULL){
        $criterio=false; $sql_criterio='';$fecha_registro='';
        if($id_dependencia){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="pk_num_dependencia = ".$id_dependencia; $criterio=true;
        }
        if($flag_controlfiscal){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="num_flag_controlfiscal = ".$flag_controlfiscal; $criterio=true;
        }
        if($estatus_dep){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="num_estatus = ".$estatus_dep; $criterio=true;
        }
        if($id_organismo){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="fk_a001_num_organismo = ".$id_organismo; $criterio=true;
        }
        $sql_criterio.=" ORDER BY ind_dependencia";
        $arr_campos=explode(',',$campos);
        if(in_array('fec_ultima_modificacion',$arr_campos)){
            $fecha_registro=",date_format(fec_ultima_modificacion,'%d/%m/%Y') AS fec_ultima_modificacion";
        }
        $result = $this->_db->query("SELECT ".$campos."$fecha_registro FROM a004_dependencia".$sql_criterio);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * CREADO POR: Alexis Ontiveros, 16 Sep 16
     * DESCRIPCIÓN: Lista dependencias de centro de costos relacionadas a una dependencia interna.
     * Retorna: Array.
     * Parámetros.
     *   $campos:               : Los campos que se desean extraer.
     *   $iddep_centrocosto     : Id de centro costo por cual filtrar
     *   $id_dependencia        : Id de la dependencia por cual filtrar
     *   $estatus_centrocosto   : Estatus (activo:1, inactivo:0) OJO no a sido renombrado
     */
    public function metBuscaDepCentroCostos($campos,$iddep_centrocosto=NULL,$id_dependencia=NULL,$estatus_centrocosto=NULL){
        $criterio=false; $sql_criterio='';
        if($iddep_centrocosto){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="pk_num_centro_costo = ".$iddep_centrocosto; $criterio=true;
        }
        if($id_dependencia){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="fk_a004_num_dependencia = ".$id_dependencia; $criterio=true;
        }
        if($estatus_centrocosto){//Nota: este campo no a sido renombrado a num_estatus
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="ind_estado = ".$estatus_centrocosto; $criterio=true;
        }
        $sql_criterio.=" ORDER BY ind_descripcion_centro_costo";
        $result = $this->_db->query("SELECT ".$campos." FROM a023_centro_costo".$sql_criterio);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * CREADO POR: Alexis Ontiveros, 18 Sep 16
     * DESCRIPCIÓN: Lista detalle miscelaneo de acuerdo al código maestro miscelaneo.
     * Retorna: Array.
     * Parámetros.
     *   $campos:               : Los campos que se desean extraer.
     *   $cod_miscelanio_maestro: Id de centro costo por cual filtrar.
     *   $coddet_escluir        : Código detalle a excluír en la consulta.
     *   $id_detalle_miscelaneo : Id miscelaneo por cual filtrar.
     */
    public function metBuscaMiscelanio($campos,$cod_miscelanio_maestro,$coddet_excluir=NULL,$cod_detalle=NULL,$id_detalle_miscelaneo=NULL){
        $criterio=false; $sql_criterio='';
        if($cod_miscelanio_maestro){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="cod_maestro = '".$cod_miscelanio_maestro."'"; $criterio=true;
        }
        if($id_detalle_miscelaneo){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="pk_num_miscelaneo_detalle = ".$id_detalle_miscelaneo; $criterio=true;
        }
        if($coddet_excluir){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="a006_miscelaneo_detalle.cod_detalle != '".$coddet_excluir."'";
        }
        if($cod_detalle){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="a006_miscelaneo_detalle.cod_detalle = '".$cod_detalle."'";
        }
        $sql_criterio.=($criterio)? " AND ": " WHERE ";
        $sql_criterio.="a006_miscelaneo_detalle.num_estatus=1";
        $sql_criterio.=" ORDER BY ind_nombre_detalle";
        $result = $this->_db->query(
            "SELECT ".$campos." FROM a005_miscelaneo_maestro
            INNER JOIN a006_miscelaneo_detalle ON a005_miscelaneo_maestro.pk_num_miscelaneo_maestro = a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro".$sql_criterio);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * CREADO POR       : Alexis Ontiveros, 21 Sep 16
     * DESCRIPCIÓN      : Lista el organísmo ó la dependencia de ubicación organizacional de un usuario de sistema.
     * Retorna: Array.
     * Parámetros.
     *   $campos:       : Los campos que se desean extraer.
     *   $id_usuario    : Id de la persona a la cual buscar la ubicación organizacional.
     */
    public function metBuscaUbicacionOrgUsuario($campos,$id_usuario=NULL){
        $criterio=false; $sql_criterio='';
        if($id_usuario){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="pk_num_seguridad_usuario = ".$id_usuario; $criterio=true;
        }
        $result = $this->_db->query("SELECT $campos FROM a018_seguridad_usuario
                  INNER JOIN rh_b001_empleado ON a018_seguridad_usuario.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                  INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                  INNER JOIN rh_c076_empleado_organizacion ON rh_b001_empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
                  INNER JOIN a001_organismo ON rh_c076_empleado_organizacion.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                  INNER JOIN a004_dependencia ON rh_c076_empleado_organizacion.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia".$sql_criterio);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * CREADO POR: Alexis Ontiveros, 21 Sep 16
     * DESCRIPCIÓN: Busca la dependencia padre de control al cual pertenece un usuario.
     * Retorna: Array.
     * Parámetros.
     */
    public function metBuscaDepControlUsuario($id_dependencia){
        $retornar=array();
        if($id_dependencia){ $ciclar=true;$i=0;
            while($ciclar){$i++;
                $result = $this->_db->query("SELECT pk_num_dependencia AS id_dependencia,ind_dependencia AS nombre_dependencia,num_flag_controlfiscal,ind_codpadre FROM a004_dependencia WHERE pk_num_dependencia=".$id_dependencia);
                $result->setFetchMode(PDO::FETCH_ASSOC);
                $fila = $result->fetch();
                if(is_array($fila)) {
                    if ($fila['ind_codpadre']==0 OR $fila['num_flag_controlfiscal'] == 1) {
                        $ciclar = false;
                    } else {
                        $id_dependencia = $fila['ind_codpadre'];
                    }
                }else{
                    $fila=null; $ciclar = false;
                }
            }
            if(is_array($fila) AND $i<10){
                $retornar=$fila;
            }else{
                $retornar=array("id_dependencia"=>"","nombre_dependencia"=>"No hay dependencia de control fiscal");
            }
            $result=null;
        }
        return $retornar;
    }
    /**
     * CREADO POR       :Alexis Ontiveros, 22 Sep 16
     * DESCRIPCIÓN      :Busca la o las dependencias internas de forma recursiva ascendente de una dependencia determinada.
     * Retorna          :Array con una cadena de dependencias y el id de la dependencia padre si fuera el caso.
     * Parámetros.
     *  $id_dependencia: Id de la dependencia.
     *  $saltolinea     :Salto de linea (\n). Opcional. Por defecto '<br>'.
     */
    public function metBuscaRecursivoDependencia($id_dependencia, $saltolinea=NULL){
        if($id_dependencia){ $ciclar=true;$i=0; $dependencias='';
            while($ciclar){$i++;
                $result = $this->_db->query("SELECT * FROM a004_dependencia WHERE pk_num_dependencia='".$id_dependencia."'");
                $result->setFetchMode(PDO::FETCH_ASSOC);
                $fila = $result->fetch();
                if($fila['ind_codpadre']==0){
                    $ciclar=false; $depArray[$i]=$fila['ind_dependencia']; $id_padre=$id_dependencia;
                }else{
                    $depArray[$i]=$fila['ind_dependencia'];
                    $id_dependencia=$fila['ind_codpadre'];
                }
            }
            /*se recorre el array en forma decreciente para asignar las dependencias encontradas en una cadena en la variable $dependencias
              separadas por un guión y con salto de linea.*/
            if($saltolinea=='\n'){$Subtraer=2;}else{$saltolinea="<br>"; $Subtraer=4;}
            if(count($depArray)>1){
                for($i=count($depArray); $i>=1; $i--){
                    $dependencias .= '-'.$depArray[$i].'.'.$saltolinea;
                }
            }else{
                $dependencias='-'.$depArray[$i].'.'.$saltolinea;
            }
        }
        $dependencias=substr($dependencias, 0, -$Subtraer);//se suprime el último <br> ó \n
        $fila=null;
        return array('dependencias'=>$dependencias,'id_padre'=>$id_padre);
    }
    /**
     * CREADO POR       :Alexis Ontiveros, 21 Sep 16
     * DESCRIPCIÓN      :Busca una dependencia o todas.
     * Retorna          :Array con los datos de dependencias.
     * Parámetros.
     *   $id_dependencia:Id de la dependencia.
     */
    public function metBuscaDependencia($id_dependencia=NULL){
        $fila='';
        if($id_dependencia){
            $sql_criterio=" WHERE pk_num_dependencia=".$id_dependencia;
        }
        $sql_criterio.=" ORDER BY ind_dependencia";
        $result = $this->_db->query("SELECT * FROM a004_dependencia".$sql_criterio);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $rcrset=$result->fetch();
        if(COUNT($rcrset)>0){
            $fila=$rcrset; $rcrset=null;
        }
        return $fila;
    }
    /**
     * CREADO POR       :Alexis Ontiveros, 23 Sep 16
     * DESCRIPCIÓN      :Busca una o mas dependencia para llenar combobox de dependencias internas en forma recursiva.
     * Retorna          :Array con la dependencia padre, sus dependencias y el id padre.
     * Parámetros.
     *   $id_organismo  :Id del organísmo que ejecuta.
     *   $id_dependencia:Id de la dependencia seleccionada.
     *   $flag_controlfiscal: 1 permite extraer las dependencias de control fiscal ó NULL extrae todas que dependenden del organísmo.
     */
    public function metCargaCboxRecursivo($id_organismo,$id_dependencia,$flag_controlfiscal=NULL){
        if($flag_controlfiscal){ //ind_codpadre=".$id_dependencia." AND
            $sql_criterio=" WHERE num_flag_controlfiscal=".$flag_controlfiscal." AND num_estatus=1 AND fk_a001_num_organismo=".$id_organismo;
        }else{
            $sql_criterio=" WHERE ind_codpadre=".$id_dependencia." AND num_estatus=1 AND fk_a001_num_organismo=".$id_organismo;
        }
        $sql_criterio.=" ORDER BY ind_dependencia";
        $result = $this->_db->query("SELECT pk_num_dependencia AS id_dep, ind_dependencia AS nombre_dep, ind_codpadre AS iddep_padre, num_flag_controlfiscal AS flag_controlfiscal, txt_abreviacion AS dep_abreviatura FROM a004_dependencia".$sql_criterio);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $rcset = $result->fetchAll();
        $nombre_org="";$data=array();
        if($id_dependencia){
            $array_x=$this->metBuscaRecursivoDependencia($id_dependencia,'');
            $cad_tultick=$array_x['dependencias'];
            $nombre_org=str_replace('&nbsp;', '', $cad_tultick);
            $nombre_org=str_replace('-', '', $nombre_org);
            $nombre_org=str_replace('.<br>', '/', $nombre_org);
            $nombre_org=str_replace('*', '', $nombre_org);
            $fila = $this->metBuscaDependencia($id_dependencia);
            if(is_array($fila)){
                $fila['toltick']="";$cadena="";
                if($fila['ind_codpadre']){
                    $toltick=explode('/', $nombre_org);
                    $leng=count($toltick);
                    if($leng>1){
                        $toltickx=array_slice($toltick, 0,$leng-1);
                        foreach ($toltickx as $key => $value) {
                            $cadena.=$value.'/';
                        }
                    }
                    $fila['toltick']=$cadena;
                }
                $data[]=array("id_dep"=>$fila['pk_num_dependencia'], "nombre_dep"=>$fila['ind_dependencia'], "iddep_padre"=>$fila['ind_codpadre'],"toltick"=>$fila['toltick']);
                $fila='';
            }
        }
        if(COUNT($rcset)>0){
            foreach($rcset as $fila){
                $fila['toltick']=$nombre_org;
                $data[] = $fila;
            }
            $retorno = $data;

        }else{
            if(is_array($data)){
                $retorno = $data;
            }else{
                $retorno = false;
            }
        }
        $rcset=null;return $retorno;
    }
    /**
     * CREADO POR       :Alexis Ontiveros, 06 Otc 16
     * DESCRIPCIÓN      :Busca uno o mas entes en forma recursiva ascendente de un determinado ente.
     * Retorna          :Una cadena con los entes separados por un salto de linea si fuese el caso y el id padre.
     * Parámetros.
     *   $id_ente:      :Id del ente.
     *   $saltolinea    :Salto de linea (\n). opcional, por defecto '<br>'.
     */
    public function metBuscaRecursivoEntes($id_ente, $saltolinea=NULL, $tipoSangria=NULL){
        if($id_ente){ $ciclar=true;$i=0; $cadEntes='';
            while($ciclar){$i++;
                $result = $this->_db->query("SELECT * FROM a039_ente WHERE pk_num_ente='".$id_ente."'");
                $result->setFetchMode(PDO::FETCH_ASSOC);
                $fila = $result->fetch();
                if($fila['num_ente_padre']==0){
                    $ciclar=false; $depArray[$i]=$fila['ind_nombre_ente']; $id_padre=$id_ente;
                }else{
                    $depArray[$i]=$fila['ind_nombre_ente'];
                    $id_ente=$fila['num_ente_padre'];
                }
            }
            /*se recorre el array en forma decreciente para asignar los ente dependientes encontrados en una cadena
             separados por un guión y con salto de linea.*/
            if($saltolinea=='\n'){
                $Subtraer=2; $saltolinea="\n";
            }else{
                $saltolinea="<br>"; $Subtraer=4;
            }
            if(count($depArray)>1){
                if($tipoSangria=='PDF'){
                    $espaciado=" ";
                }else{
                    $espaciado="&nbsp;";
                }
                $k=0;  $sangria="";
                for($i=count($depArray); $i>=1; $i--){
                    if($k>0){$sangria.=$espaciado;}
                    $cadEntes .= $sangria.'-'.$depArray[$i].'.'.$saltolinea;$k++;
                }
            }else{
                $cadEntes='-'.$depArray[$i].'.'.$saltolinea;
            }
        }
        $cadEntes=substr($cadEntes, 0, -$Subtraer);/*se suprime el último <br> ó \n*/
        $depArray=null;$fila=null;
        return array('cadEntes'=>$cadEntes,'id_padre'=>$id_padre);
    }
    /**
     * CREADO POR       :Alexis Ontiveros, 06 Otc 16
     * DESCRIPCIÓN      :Busca un ente en particular.
     * Retorna          :Un array con los registros encontrado.
     * Parámetros.
     *  $id_ente        :Id del ente.
     */
    public function metBuscaEnte($id_ente=null){
        $fila="";
        if($id_ente){
            $sql_criterio=" WHERE pk_num_ente=".$id_ente;
        }
        $sql_criterio.=" ORDER BY ind_nombre_ente";
        $result = $this->_db->query("SELECT * FROM a039_ente".$sql_criterio);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $rcrset=$result->fetch();
        if(COUNT($rcrset)>0){
            $fila=$rcrset;
        }
        $rcrset=null;return $fila;
    }
    /**
     * CREADO POR       :Alexis Ontiveros, 06 Otc 16
     * DESCRIPCIÓN      :Busca entes para llenar un combobox en forma recursiva
     * Retorna          :Un array con el ente padre, sus entes dependientes y el id padre.
     * Parámetros.
     *  $id_ente_padre  :Id del ente seleccionado ó cero(0).
     *  $saltolinea     :Salto de linea (\n). opcional, por defecto '<br>'.
     *  $num_objetocontrol: para filtrar entes que son objeto a control fiscal. Opcional
     */
    public function metCargaCboxRecursivoEnte($id_ente_padre,$num_objetocontrol=NULL){
        if($num_objetocontrol===0 OR $num_objetocontrol===1){
            $sql_criterio=" WHERE num_ente_padre=".$id_ente_padre." AND fk_a037_num_tipo_ente!=1 AND num_sujeto_control=".$num_objetocontrol;
        }else{
            $sql_criterio=" WHERE num_ente_padre=".$id_ente_padre." AND a006_miscelaneo_detalle.cod_detalle!='FPPA'";
        }
        $sql_criterio.=" ORDER BY ind_nombre_ente";
        $result = $this->_db->query("
          SELECT pk_num_ente AS id_ente, ind_nombre_ente AS nombre_ente, num_ente_padre AS idente_padre
          FROM a039_ente
            INNER JOIN a038_categoria_ente ON a039_ente.fk_a038_num_categoria_ente = a038_categoria_ente.pk_num_categoria_ente
            INNER JOIN a006_miscelaneo_detalle ON a038_categoria_ente.fk_a006_miscdet_tipoente = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
          ".$sql_criterio);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $rcset = $result->fetchAll();
        $nombre_entes=""; $data="";
        if($id_ente_padre){
            /*Se busca el padre o los padres para crear el toltick*/
            $array_x=$this->metBuscaRecursivoEntes($id_ente_padre,'');
            $cad_tultick=$array_x['cadEntes'];
            /*Se limpia la cadena*/
            $nombre_entes=str_replace('&nbsp;', '', $cad_tultick);
            $nombre_entes=str_replace('-', '', $nombre_entes);
            $nombre_entes=str_replace('.<br>', '/', $nombre_entes);
            $nombre_entes=str_replace('*', '', $nombre_entes);
            /*Se busca al ente padre*/
            $fila = $this->metBuscaEnte($id_ente_padre);
            if(is_array($fila)){
                $fila['toltick']="";$cadena="";
                if($fila['num_ente_padre']){
                    $toltick=explode('/', $nombre_entes);
                    $leng=count($toltick);
                    if($leng>1){
                        $toltickx=array_slice($toltick, 0,$leng-1);
                        foreach ($toltickx as $key => $value) {
                            $cadena.=$value.'/';
                        }
                    }
                    $fila['toltick']=$cadena;
                }
                $data[]=array("id_ente"=>$fila["pk_num_ente"],"nombre_ente"=>$fila["ind_nombre_ente"],"idente_padre"=>$fila["num_ente_padre"]);
                $fila="";
            }
        }
        if(COUNT($rcset)>0){
            foreach($rcset as $fila){
                $fila['toltick']=$nombre_entes;
                $data[] = $fila;
            }
            $retorno = $data;
        }else{
            if(is_array($data)){
                $retorno = $data;
            }else{
                $retorno = false;
            }
        }
        $rcset=null;return $retorno;
    }
    /**
     * CREADO POR       :Alexis Ontiveros, 03 Otc 16
     * DESCRIPCIÓN      :Busca los empleados de una dependencia determinada.
     * Retorna          :Un array con los datos encontrados
     * Parámetros.
     *  $campos         :Nombres de campos a extraer.
     *  $id_organismo   :Id del organismo que ejecuta
     *  $id_dependencia :Id de la dependencia.
     */
    public function metBuscaEmpleadosDependencia($campos,$id_organismo,$id_dependencia=NULL){
        $criterio=false; $sql_criterio='';
        $Union="INNER JOIN rh_b001_empleado ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado ";
        $Union.="INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona ";
        $Union.="INNER JOIN rh_c005_empleado_laboral ON rh_b001_empleado.pk_num_empleado = rh_c005_empleado_laboral.fk_rhb001_num_empleado ";
        $Union.="INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos ";
        $Union.="INNER JOIN rh_c006_tipo_cargo ON rh_c063_puestos.fk_rhc006_num_cargo = rh_c006_tipo_cargo.pk_num_cargo";
        $sql_criterio.=($criterio)? " AND ": " WHERE ";
        $sql_criterio.="rh_b001_empleado.num_estatus='1' AND ind_nombre1!='Administrador' AND fk_a001_num_organismo = ".$id_organismo; $criterio=true;
        if($id_dependencia){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="fk_a004_num_dependencia = ".$id_dependencia;
        }
        $sql_criterio.=" ORDER BY ind_nombre1";
        $result = $this->_db->query("SELECT $campos FROM rh_c076_empleado_organizacion $Union".$sql_criterio);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * CREADO POR       :Alexis Ontiveros, 30 Sep 16
     * DESCRIPCIÓN      :Formatea la fecha para la base de datos
     * Retorna          :en formato "año-mes-dia".
     * Parámetros.
     *   $campos:       : La fecha en formato "yyyy-mm-dd".
     */
    public function metFormateaFechaMsql($fecha){
        if(!$fecha OR $fecha=='null' OR $fecha=='NULL'){
            $fechaMsql="0000-00-00";
        }else{
            $arrfecha=explode('-',$fecha);
            $fechaMsql=$arrfecha[2].'-'.$arrfecha[1].'-'.$arrfecha[0];
        }
        return $fechaMsql;
    }
    /**
     * CREADO POR      :Alexis Ontiveros, 30 Sep 16
     * DESCRIPCIÓN     :Busca el código interno ó siglas de una dependencia de control para conformar el código de una determinada planificación.
     * Retorna         :El código interno ó siglas.
     * $id_dependencia :Id de la dependencia.
     */
    public function metBuscaSiglas($id_dependencia){
        $retornar='';
        $result = $this->_db->query("SELECT * FROM a035_parametros a035 INNER JOIN a015_seguridad_aplicacion a015 ON a035.fk_a015_num_seguridad_aplicacion = a015.pk_num_seguridad_aplicacion WHERE a035.ind_parametro_clave='PF_PREF_CODIGO' AND a015.cod_aplicacion = 'PF'");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $fila = $result->fetch();
        if($fila['ind_valor_parametro']==1){
            $result = $this->_db->query("SELECT * FROM a004_dependencia WHERE pk_num_dependencia=".$id_dependencia);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $fila = $result->fetch();
            $retornar=$fila['txt_abreviacion'];
        } else if ($fila['ind_valor_parametro']==2) {
            $result = $this->_db->query("SELECT * FROM a004_dependencia WHERE pk_num_dependencia=".$id_dependencia);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $fila = $result->fetch();
            $retornar=$fila['ind_codinterno'];
        }
        $result=null;$fila=null;return $retornar;
    }
    /**
     * CREADO POR      :Alexis Ontiveros, 30 Sep 16
     * DESCRIPCIÓN     :Establece si se incluye ó nó el indicativo del tipo de procedimiento administrativo en el número de control de
     *                  valoración jurídica y determinación de responsabilidad.
     * $tipoProcedimiento: Tipo de procedimiento administrativo "PA" ó "IM"
     * Retorna         :False o el indicativo según planificación.
     * $id_dependencia :Id de la dependencia.
     */
    public function metIndicativoTipoProcAdmin($tipoProcedimiento){
        $retornar=false;
        $result = $this->_db->query("
          SELECT * FROM a035_parametros
            INNER JOIN a015_seguridad_aplicacion ON a035_parametros.fk_a015_num_seguridad_aplicacion = a015_seguridad_aplicacion.pk_num_seguridad_aplicacion
          WHERE a035_parametros.ind_parametro_clave='PF_TPA_CODIGO' AND a015_seguridad_aplicacion.cod_aplicacion = 'PF'");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $fila = $result->fetch();
        if(is_array($fila)){
            if($fila['ind_valor_parametro']==1){
                if($tipoProcedimiento=="PA"){
                    $retornar="PDRA";//Procedimiento de determinación de responsabilidad administrativa.
                }elseif($tipoProcedimiento=="IM"){
                    $retornar="PDIM";//Procedimiento de determinación de imposición de multa.
                }
            }
            $result=null;$fila=null;return $retornar;
        }
    }

    /**
     * Valida si las planificaciones en Determinación de Responsabilidad incluye el indicativo del tipo de procedimiento o nó.
     * @return bool
     */
    public function metValidaTipoProcAdmin(){
        $retornar=false;
        $result = $this->_db->query("
          SELECT * FROM a035_parametros
            INNER JOIN a015_seguridad_aplicacion ON a035_parametros.fk_a015_num_seguridad_aplicacion = a015_seguridad_aplicacion.pk_num_seguridad_aplicacion
          WHERE a035_parametros.ind_parametro_clave='PF_TPA_CODIGO' AND a015_seguridad_aplicacion.cod_aplicacion = 'PF'");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $fila = $result->fetch();
        if(is_array($fila)){
            if($fila['ind_valor_parametro']==1){
                $retornar=true;
            }
            $result=null;$fila=null;return $retornar;
        }
    }
    /**
     * CREADO POR: Alexis Ontiveros, 25 Feb 17
     * @param $tipoProceso, código del proceso
     * @return string Retorna el subfijo para conformar el código de cada planificación
     */
    public function metSubFijoProceso($codProceso){
        $retornar='';
        $arrresult=$this->metBuscaMiscelanio('ind_nombre_detalle','PFINDIPROC','',$codProceso);
        if(is_array($arrresult)){
            $retornar=$arrresult[0]['ind_nombre_detalle'];
        }
        $arrresult=null; return $retornar;
    }
    /**
     * CREADO POR               :Alexis Ontiveros, 30 Sep 16
     * DESCRIPCIÓN              :Crea el número de control de una planificación de: Actuación, Valoración Preliminar, Potestad, Valoración Jurídica o Determinación.
     * Retorna                  :El código (número de control) de la planificación
     * Parámetros.
     *  $id_dependencia         :Id de la dependencias que ejecuta la planificación.
     *  $tipo_proceso           :Id del tipo de proceso: Actuación, Valoración Preliminar, Potestad, Valoración Jurídica o Determinación.
     *  $id_planificacion_hasta :El id de la planificación.
     *  $year_actuacion         :Año de la planificación según su fecha de inicio.
     *  $codTipoProceso         :Código del tipo de proceso
     *  $tipoProcedimiento      :Tipo de procedimiento administrativo.
     *
     */
    public function metCorrelativoPlanificacion($id_dependencia,$tipo_proceso,$year_planificacion,$codTipoProceso,$tipoProcedimiento=NULL){
        //pk_num_planificacion BETWEEN 1 AND $id_planificacion_hasta
        $sqlQuery = $this->_db->query("SELECT COUNT(*) AS total_reg FROM pf_b001_planificacion_fiscal WHERE cod_planificacion IS NOT NULL AND fk_b002_num_proceso="
            .$tipo_proceso." AND num_subcodigo=0 AND fk_a004_num_dependencia=".$id_dependencia." AND YEAR(fec_inicio)='".$year_planificacion."'");
        $sqlQuery->setFetchMode(PDO::FETCH_ASSOC);
        $rcset = $sqlQuery->fetch();
        $correlativo=(int) ($rcset['total_reg']+1);
        $correlativo=(string) str_repeat("0", 4-strlen($correlativo)).$correlativo;
        $cadSiglas=$this->metBuscaSiglas($id_dependencia);
        $indicativoProceso=$this->metSubFijoProceso($codTipoProceso);//Se busca el indicativo de proceso fiscal.
        if($codTipoProceso=='04' OR $codTipoProceso=='05'){//Construye el número de control de las planificaciones de determinación de responsabilidades
            if($tipoProcedimiento){//Entra cuando se incluye el indicativo del tipo de procedimiento administrativo
                if($indicativoProceso=="VJ"){ //Entra cuando es una Valoración Jurídica.
                    $nroControl=$cadSiglas.'-'.$indicativoProceso.'-'.$tipoProcedimiento.'-'.$correlativo.'-'.$year_planificacion;
                }else{ //Entra cuando es un Procedimiento Administrativo.
                    $nroControl=$cadSiglas.'-'.$tipoProcedimiento.'-'.$correlativo.'-'.$year_planificacion;
                }
            }else{//Entra cuando NO se incluye el indicativo del tipo de procedimiento administrativo
                if($indicativoProceso=="VJ"){
                    $nroControl=$cadSiglas.'-'.$indicativoProceso.'-'.$correlativo.'-'.$year_planificacion;
                }else{
                    $nroControl=$cadSiglas.'-'.$indicativoProceso.'-'.$correlativo.'-'.$year_planificacion;
                }
            }
        }else{//Construye el número de control de una Actuación de control fiscal, Valoración Preliminar o Potestad investigativa
            $nroControl=$cadSiglas.'-'.$indicativoProceso.'-'.$correlativo.'-'.$year_planificacion;
        }
        $rcset=null;return $nroControl;
    }
    /**
     * CREADO POR        :Alexis Ontiveros, 30 Sep 16
     * DESCRIPCIÓN       :Crea el número de control de una Sub Planificación de Valoración ó Potestad que se genera a partír de las planificaciones devueltas.
     * Retorna           :El cádigo
     * Parámetros.
     *  $tipo_proceso    :Id del tipo de proceso: Valoración ó Potestad
     *  $nroPlanDevuelta :Nro. de control de la planificación(padre) devuelta.
     *  $num_subcodigo   :sub número de la planificación devuelta.
     *  $idDependencia   :Id de la dependencia que procesa la planificación.
     */
    public function metNroControlSubPlanific($tipo_proceso,$nroPlanDevuelta,$num_subcodigo,$idDependencia){
        if($num_subcodigo>0){//Se determina el nro. de control original
            $quitar=strlen($num_subcodigo)+1;
            $nroPlanDevuelta=substr($nroPlanDevuelta, 0, -$quitar);
        }
        $sqlQuery = $this->_db->query("SELECT COUNT(*)+1 AS subNumero FROM pf_b001_planificacion_fiscal WHERE fk_b002_num_proceso=".$tipo_proceso." AND cod_planificacion LIKE '".$nroPlanDevuelta."%' AND num_subcodigo>0 AND fk_a004_num_dependencia=".$idDependencia);
        $sqlQuery->setFetchMode(PDO::FETCH_ASSOC);
        $rcset = $sqlQuery->fetch();
        $nroControlPlan=$nroPlanDevuelta.'-'.$rcset['subNumero'];
        $retornar=array("subCorrelativo"=>$rcset['subNumero'],"cod_planificacion"=>$nroControlPlan);
        $rcset=null;return $retornar;
    }
    /**
     * CREADO POR       :Alexis Ontiveros, 30 Sep 16
     * DESCRIPCIÓN      :Actualiza un campo de una determinada tabla.
     * Retorna          :True o false
     * Parámetros.
     *  $tabla          :Nombre de la tabla.
     *  $campo_filtro   :Nombre del campo filtro.
     *  $valor_filtro   :Valor del filtro.
     *  $campo_actualizar:Nombre del campo a ser atualizado.
     *  $nuevo_valor    :Nuevo Valor.
     */
    public function metActualizaCampo($tabla,$campo_filtro,$valor_filtro,$campo_actualizar,$nuevo_valor){
        $this->_db->beginTransaction();
        $sqlQuery=$this->_db->prepare("UPDATE $tabla SET $campo_actualizar=:$campo_actualizar WHERE $campo_filtro=$valor_filtro");
        $sqlQuery->execute(array($campo_actualizar=>$nuevo_valor));
        $error = $sqlQuery->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack(); $retornar=false;
        }else{
            if($this->_db->commit()){
                $retornar=true;
            }
        }
        return $retornar;
    }
    public function metLimpiaEspacios($cadena){
        $cadena = str_replace(' ', '', $cadena);
        return $cadena;
    }
    /**
     * CREADO POR       :Alexis Ontiveros, 03 Otc 16
     * DESCRIPCIÓN      :Busca los datos de un empleado.
     * Retorna          :Un array con los datos encontrados
     * Parámetros.
     *  $campos         :Nombres de campos a extraer.
     *  $id_empleado    :Id de empleado de la persona.
     */
    public function metBuscaEmpledos($campos,$id_empleado){
        $criterio=false; $sql_criterio='';
        $Union="INNER JOIN rh_b001_empleado ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado ";
        $Union.="INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona ";
        $Union.="INNER JOIN rh_c005_empleado_laboral ON rh_b001_empleado.pk_num_empleado = rh_c005_empleado_laboral.fk_rhb001_num_empleado ";
        $Union.="INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos ";
        $Union.="INNER JOIN rh_c006_tipo_cargo ON rh_c063_puestos.fk_rhc006_num_cargo = rh_c006_tipo_cargo.pk_num_cargo";
        if($id_empleado){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="pk_num_empleado = ".$id_empleado;
        }
        $sql_criterio.=" ORDER BY ind_nombre1";
        $result = $this->_db->query("SELECT $campos FROM rh_c076_empleado_organizacion $Union".$sql_criterio);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * CREADO POR           :Alexis Ontiveros, 19 Otc 16
     * DESCRIPCIÓN          :Crear un array multidimensional a partír de un array clave - valor.
     * Retorna              :Un array agrupado.
     * Parámetros.
     *  $array              :Array a tratar.
     *  $clave_agrupar      :Nombre del elemento contenido en $array el cual su valor a ser el nombre del grupo.
     *  $clave_prefijo      :Nombre del elemento contenido en el array, cuyo valor será el prefijo del nombre del grupo. Nota: OPCIONAL.
     *  $separador_prefijo  :Caracter de separador entre el prefijo y la clave por el cual se agrupa. Nota: OPCIONAL.
     */
    public function metAgrupaArray(&$array, $clave_agrupar, $clave_prefijo=NULL,$separador_prefijo=NULL) {
        $array_filtrado = array();$prefijo="";
        foreach($array as $index=>$array_value) {//se obtiene en $array_value el set de arrays para asignarlo cada valor de $clave_agrupar.
            if($clave_prefijo){//Se determina el prefijo.
                if($separador_prefijo){
                    $prefijo=$array_value[$clave_prefijo].$separador_prefijo;
                }else{
                    $prefijo=$array_value[$clave_prefijo].' ';
                }
            }
            $value = $array_value[$clave_agrupar]; //Se obtiene el valor de la clave.
            unset($array_value[$clave_agrupar]);//se elimina la clave.
            $value=$prefijo.$value;//Se concatena el valor de la clave al prefijo.
            $array_filtrado[$value][] = $array_value;//Se asigna el set de arrays al elemento grupo.
        }
        return $array_filtrado; // array multidimencional con los rows agrupados.
    }
    /**
     * Buscar días feríados
     * Creado por: Trino Cordero.
     */
    public function metObtenerNroDiasFeriados ($fdesde, $fhasta)
    {
        list($anioDesde, $mesDesde, $diaDesde) = explode('-', $fdesde);
        list($anioHasta, $mesHasta, $diaHasta) = explode('-', $fhasta);
        $diamesDesde = "$mesDesde-$diaDesde";
        $diamesHasta = "$mesHasta-$diaHasta";
        $sql = "select *
			    from rh_c069_feriados
			    where (num_flag_variable = 1
			          and (fec_anio = '$anioDesde' or fec_anio = '$anioHasta')
			          and (fec_dia >= '$diamesDesde' and fec_dia <= '$diamesHasta'))
			       or (num_flag_variable = 0 and fec_dia >= '$diamesDesde' and fec_dia <= '$diamesHasta')";
        $result = $this->_db->query($sql);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $diasFeriados = $result->fetchAll();
        $nroDiasFeriados = 0;
        foreach ($diasFeriados as $diaFeriado) {
            list($mes, $dia) = explode('-', $diaFeriado['fec_dia']);
            if ($diaFeriado['fec_anio'] == "") {
                $anio = $anioDesde;
            } else {
                $anio = $diaFeriado['fec_anio'];
            }
            $fecha = "$anio-$mes-$dia";
            $diaSemana = $this->metObtenerDiaSemana($fecha);
            if ($diaSemana >= 1 && $diaSemana <= 5) {
                $nroDiasFeriados++;
            }
            if ($anioDesde != $anioHasta) {
                if ($diaFeriado['fec_anio'] == "") {
                    $anio = $anioHasta;
                } else {
                    $anio = $diaFeriado['fec_anio'];
                }
                $fecha = "$anio-$mes-$dia";
                $diaSemana = $this->metObtenerDiaSemana($fecha);

                if ($diaSemana >= 1 && $diaSemana <= 5) {
                    $nroDiasFeriados++;
                }
            }
        }
        $diasFeriados=null;return $nroDiasFeriados;
    }
    /**
     * Obtener un arreglo de días feriados
     * Creado por: Trino Cordero.
     */
    public function metObtenerListaDiasFeriados ($fdesde, $fhasta)
    {
        list($anioDesde, $mesDesde, $diaDesde) = explode('-', $fdesde);
        list($anioHasta, $mesHasta, $diaHasta) = explode('-', $fhasta);
        $diamesDesde = "$mesDesde-$diaDesde";
        $diamesHasta = "$mesHasta-$diaHasta";
        $sql = "select *
			    from rh_c069_feriados
			    where (num_flag_variable = 1
			          and (fec_anio = '$anioDesde' or fec_anio = '$anioHasta')
			          and (fec_dia >= '$diamesDesde' and fec_dia <= '$diamesHasta'))
			       or (num_flag_variable = 0 and fec_dia >= '$diamesDesde' and fec_dia <= '$diamesHasta')";
        $result = $this->_db->query($sql);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $diasFeriados = $result->fetchAll();
        $listaDiasFeriados = array();
        foreach ($diasFeriados as $diaFeriado) {
            //Falta el caso cuando se cambia a un nuevo año.
            $anio = $anioDesde;
            $fecha = "$anio-{$diaFeriado['fec_dia']}";
            $listaDiasFeriados[] = $fecha;
        }
        $diasFeriados = null;return $listaDiasFeriados;
    }
    /**
     * Buscar el día de la semana
     * Creado por: Trino Cordero.
     */
    function metObtenerDiaSemana($fecha) {
        // primero creo un array para saber los días de la semana
        $dias = array(0, 1, 2, 3, 4, 5, 6);
        $dia = substr($fecha, 8, 2);
        $mes = substr($fecha, 5, 2);
        $anio = substr($fecha, 0, 4);
        // en la siguiente instrucción $pru toma el día de la semana, lunes, martes,
        $pru = $dias[date("w", mktime(0, 0, 0, $mes, $dia, $anio))];
        return $pru;
    }
    /**
     * Buscar días hábiles
     * Creado por: Trino Cordero.
     */
    function metObtenerDiasHabiles ($fdesde, $fhasta) {
        //Diferencia entre días
        $objFechaDesde = new DateTime($fdesde);
        $objFechaHasta = new DateTime($fhasta);
        $intervalo = $objFechaDesde->diff($objFechaHasta);
        $diasCompletos = (int) $intervalo->format('%a');
        $diasFeriados = $this->metObtenerNroDiasFeriados($fdesde, $fhasta);
        $diaSemana = $this->metObtenerDiaSemana($fdesde);
        $diasHabiles = 0;
        for ($i = 0; $i <= $diasCompletos; $i++) {
            if ($diaSemana >= 1 && $diaSemana <= 5) {
                $diasHabiles++;
            }
            $diaSemana++;

            if ($diaSemana == 7) {
                $diaSemana = 0;
            }
        }
        $diasHabiles -= $diasFeriados;
        $diasFeriados=null;return $diasHabiles;
    }
    /**
     * Sumar días hábiles
     * Creado por: Trino Cordero.
     */
    function metSumarDiasHabiles($fechaInicio, $dias)
    {
        $fecha = new DateTime($fechaInicio);
        $anio = $fecha->format('Y');
        //Lista de dias feriados en el año
        $listaDiasFeriados = $this->metObtenerListaDiasFeriados("$anio-01-01", "$anio-12-31");
        //Contar días hábiles
        for ($i = 2; $i <= $dias; $i++) {
            //Sumar un día y verificar
            $fecha->add(new DateInterval('P01D'));
            //Obtener día de la semana
            $diaSemana = $this->metObtenerDiaSemana($fecha->format('Y-m-d'));
            if ($diaSemana == 6) {
                //Si el día de la semana es sábado, sumarle dos días para que sea lunes
                $fecha->add(new DateInterval('P02D'));
            }elseif ($diaSemana == 0 ) {
                //Si el día de la semana es sábado, sumarle dos días para que sea lunes
                $fecha;
            } else {
                $iterar = true;
                while ($iterar) {
                    //VERIFICAR SI ES FERIADO
                    $fechaString = $fecha->format('Y-m-d');
                    //Si el día es feriado y no es sábado o domingo, sumarle un día
                    if (in_array($fechaString, $listaDiasFeriados) && !in_array($this->metObtenerDiaSemana($fechaString), array(0, 6))) {
                        $fecha->add(new DateInterval('P01D'));
                        if ($this->metObtenerDiaSemana($fecha->format('Y-m-d')) == 6) {
                            $fecha->add(new DateInterval('P02D'));
                        }
                    } else {
                        $iterar = false;
                    }
                }
            }
        }
        $listaDiasFeriados=null;return $fecha->format('Y-m-d');
    }
    /**
     * Extrae los datos de una persona relacionada a un ente ó dependencia ente
     * @param null $id_ente
     * @param null $id_persona
     * @return mixed
     */
    public function metBuscaPersonaEnte($id_ente=NULL, $id_persona=NULL){
        if($id_ente){
            $criterio=" WHERE fk_a039_num_ente=".$id_ente;
        }else{
            $criterio=" WHERE pk_num_persona_ente=".$id_persona;
        }
        $result = $this->_db->query("
          SELECT pk_num_persona_ente,fk_a003_num_persona_titular,fk_a039_num_ente,fk_a006_num_miscdet_cargo_pers,fk_a006_num_miscdetalle_situacion,
          num_estatus_titular, ind_nombre1, ind_nombre2, ind_apellido1, ind_apellido2,tb_cargos.ind_nombre_detalle AS cargo_persona,tb_situacion.ind_nombre_detalle AS situacion_persona
          FROM a041_persona_ente
          INNER JOIN a003_persona ON a041_persona_ente.fk_a003_num_persona_titular = a003_persona.pk_num_persona
          INNER JOIN a006_miscelaneo_detalle AS tb_cargos ON a041_persona_ente.fk_a006_num_miscdet_cargo_pers = tb_cargos.pk_num_miscelaneo_detalle
          INNER JOIN a006_miscelaneo_detalle AS tb_situacion ON a041_persona_ente.fk_a006_num_miscdetalle_situacion = tb_situacion.pk_num_miscelaneo_detalle".$criterio);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetch();
    }
    /**
     * Verifica posibles relaciones de registros en una o varias tablas.
     * Parámetro    : $arrDatos; array de arrays con los nombres de las tablas, campos y valores.
     *              : Retorna true ó false.
     * Creado por   : Alexis Ontiveros.
     */
    public function metVerificaRelacion($arrDatos){
        $retornar = false; //El registro no esxiste
        foreach ($arrDatos as $array_d){//se recorre el array $tbdatos para obtener los nombres y valores.
            $cfiltro2='';
            if(isset($array_d['valorc2']) AND $array_d['valorc2']){
                if(is_numeric($array_d['valorc2'])){
                    $cfiltro2=" AND ".$array_d['campo2']."=".$array_d['valorc2'];
                }else{
                    $cfiltro2=" AND ".$array_d['campo2']."='".$array_d['valorc2']."'";
                }
            }
            $result = $this->_db->query("SELECT * FROM ".$array_d['tabla']." WHERE ".$array_d['campo1']."=".$array_d['valorc1'].$cfiltro2." LIMIT 1");
            $result->setFetchMode(PDO::FETCH_ASSOC);
            if($result->fetch()) {
                $retornar=true; break; //El registro si existe
            }
        }
        $result=null;return $retornar;
    }
    /**
     * CREADO POR       :Alexis Ontiveros, 14 Nov 16
     * DESCRIPCIÓN      :Crea un array de años a partír de la planificación registrada con el año mas bajo para llenar un combobox de años.
     * Retorna          :Un array.
     */
    function metComboBoxYears(){
        $arrYear=array(); $yearActual = date('Y');
        $result = $this->_db->query("SELECT MIN(fec_registro_planificacion) AS fecha FROM pf_b001_planificacion_fiscal LIMIT 1");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $rcset = $result->fetch();
        if($rcset['fecha']){
            $arrfecha=explode('-',$rcset['fecha']);
            $yearInicial=$arrfecha[0];
        }else{
            $yearInicial=date('Y');
        }
        for($year = $yearInicial; $year<=$yearActual; $year++){
            $arrYear[]=$year;
        }
        if(COUNT($arrYear)==0){//Entra cuando la fecha del server es menor a la fecha actual.
            $arrYear[]='Error: la fecha('.$yearActual.') del server no es válida';
        }
        $rcset=null; return $arrYear;
    }
    /**
     * CREADO POR   :Alexis Ontiveros, 15 Nov 16
     * DESCRIPCIÓN  :Formatea fecha del tipo ####-##-## a ##-##-####.
     * Retorna      :la fecha formateada.
     * Parámetros   :$fecha, la fecha ####-##-##.  $fechaLarga: true, implica mostrar con hora si la fecha es larga
     */
    public function metFormateaFecha($fecha, $fechaLarga=NULL){
        $hora='';
        if(!$fecha OR $fecha=='null' OR $fecha=='NULL'){
            $fechaNormal="00-00-0000";
        }else{
            $arrfecha=explode('-',$fecha);
            if(strlen($arrfecha[2])>2){
                $arrDia=explode(' ',$arrfecha[2]);
                $dia=$arrDia[0];
                if($fechaLarga){$hora=' '.$arrDia[1];}
            }else{
                $dia=$arrfecha[2];
            }
            $fechaNormal=$dia.'-'.$arrfecha[1].'-'.$arrfecha[0].$hora;
        }
        return $fechaNormal;
    }
    /**
     * CREADO POR   :Alexis Ontiveros, 15 Nov 16
     * DESCRIPCIÓN  :Busca las fechas fin y fin real de una planificación.
     * Retorna      :Un array
     * Parámetros:  :$id_planificacion, id de la planificación en cuestión.
     */
    public function metBuscaFechaFinPlan($id_planificacion){
        $retornar=array('fecha_finplan'=>'','fecha_finrealplan'=>'');
        $result = $this->_db->query("SELECT fec_culmina_actividad AS fecha_finplan,fec_culmina_real_actividad AS fecha_finrealplan
        FROM pf_c001_actividad_planific WHERE fk_pfb001_num_planificacion=".$id_planificacion." ORDER BY num_secuencia_actividad DESC limit 1");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $rcset = $result->fetch();
        if($rcset){
            $rcset['fecha_finplan']=$this->metFormateaFecha($rcset['fecha_finplan']);
            $rcset['fecha_finrealplan']=$this->metFormateaFecha($rcset['fecha_finrealplan']);
            $retornar=$rcset;
        }
        $rcset=null;return $retornar;
    }
    /**
     * CREADO POR   :Alexis Ontiveros, 27 dic 16
     * DESCRIPCIÓN  :Crea los totales días que afectan y los no afectan, total prórroga, total días planificación y se extrae la fecha fin
     *               de una planificación de acuerdo a las actividades asignadas a la misma.
     * @param       $id_planificacion
     * @return      array
     */
    public function metBuscaTotalesPlan($id_planificacion){
        //echo 'Lego:'.$id_planificacion;
        $totalDias_Afecta=0;
        $totalDias_NoAfecta=0;
        $totalDias_Prorroga=0;
        $totalDias_Plan=0;
        $fechaReal_finPlan="";
        $result = $this->_db->query("SELECT num_dias_duracion_actividad,num_dias_prorroga_actividad,num_dias_adelanto_actividad,fec_culmina_actividad,fec_culmina_real_actividad,ind_afecto_plan
        FROM pf_c001_actividad_planific
        INNER JOIN pf_b004_actividad ON pf_c001_actividad_planific.fk_pfb004_num_actividad = pf_b004_actividad.pk_num_actividad
        where fk_pfb001_num_planificacion=".$id_planificacion." ORDER BY num_secuencia_actividad ASC");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $rcset = $result->fetchAll();
        if($rcset){
            foreach($rcset as $fila){
                if($fila['ind_afecto_plan']=='S'){
                    $totalDias_Afecta=$totalDias_Afecta+$fila['num_dias_duracion_actividad'];
                }else{
                    $totalDias_NoAfecta=$totalDias_NoAfecta+$fila['num_dias_duracion_actividad'];
                }
                $totalDias_Prorroga=$totalDias_Prorroga+$fila['num_dias_prorroga_actividad'];
                $fechaReal_finPlan=$fila['fec_culmina_real_actividad'];
            }
            $totalDias_Plan=$totalDias_Plan+$totalDias_Afecta+$totalDias_NoAfecta+$totalDias_Prorroga;
            $fechaReal_finPlan=$this->metFormateaFecha($fechaReal_finPlan);
        }
        $retornar=array('fecha_fin_plan'=>$fechaReal_finPlan,'cant_dias_afecta'=>$totalDias_Afecta,'cant_dias_no_afecta'=>$totalDias_NoAfecta,
            'cant_dias_prorroga'=>$totalDias_Prorroga,'totalDias_Plan'=>$totalDias_Plan);
        $rcset=null;return $retornar;
    }
    /**
     * CREADO POR   :Alexis Ontiveros, 17 Nov 16
     * DESCRIPCIÓN  :Busca un el nombre de un usuario de sistema.
     * Retorna      :El nombre del usuario
     * Parámetros:  :$id_user, id del usuario.
     */
    public function metBuscaUsuario($id_user=NULL,$id_empleado=NULL){
        $retornar="";
        $criterio=false; $sql_criterio='';
        if(isset($id_user)){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="pk_num_seguridad_usuario=".$id_user; $criterio=true;
        }
        if(isset($id_empleado)){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="pk_num_empleado=".$id_empleado;
        }
        $result = $this->_db->query("
          SELECT CONCAT_WS(' ',ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2) AS nombre_usuario FROM a018_seguridad_usuario
          INNER JOIN rh_b001_empleado ON a018_seguridad_usuario.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
          INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona".$sql_criterio);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $rcset = $result->fetch();
        if($rcset){
            $retornar=$rcset["nombre_usuario"];
        }
        $rcset=null; return $retornar;
    }
    /**
     * Creado por: Alexis Ontiveros.
     * Reformula la fecha de inicio y fin de ejecución de las actividades de una planificación actual.
     */
    public function metRecalculaActividadesPlan($arrParams){
        $arrDataActividades = $arrParams['hdd_dataActividad'];
        $fecha_inicial=$this->metFormateaFechaMsql($arrParams['fecha_inicio_actuacion']);
        foreach($arrDataActividades as $cadena){
            $arrDatos=explode('N',$cadena);
            $dias_duracion=$arrDatos[1];
            $duracion_actividad=$dias_duracion;
            $fecha_fin_actividad=$this->metSumarDiasHabiles($fecha_inicial, (int) $duracion_actividad);
            $fechaIncioActividad=$this->metFormateaFecha($fecha_inicial);
            $fechafin=$this->metFormateaFecha($fecha_fin_actividad);
            $fila=$arrDatos[0].'N'.$dias_duracion.'N'.$fechaIncioActividad.'N'.$fechafin.'N'.$fechaIncioActividad.'N'.$fechafin;
            $duracion_actividad++;
            $fecha_inicial=$this->metSumarDiasHabiles($fecha_inicial, (int) $duracion_actividad);
            $arrData[]=$fila;
        }
        return $arrData;
    }

    /**
     * Creado por: Alexis Ontiveros.
     * Permite ordenar un array multidimensional por un elemento. Ascendente ó descendente.
     * @param $ArrayOrdenar, array a ordenar.
     * @param $elemento. elemento por el cual ordenar.
     * @param $inverso, true: descendente, false: ascendente.
     * @return array, array ordenado.
     */
    public function metOrderMultiDimensionalArray ($ArrayOrdenar, $elemento, $inverso = false) {
        $position = array();
        $newRow = array();
        foreach ($ArrayOrdenar as $key => $row) {
            $position[$key]  = $row[$elemento];
            $newRow[$key] = $row;
        }
        if ($inverso) {
            arsort($position);
        }
        else {
            asort($position);
        }
        $returnArray = array();
        foreach ($position as $key => $pos) {
            $returnArray[] = $newRow[$key];
        }
        $ArrayOrdenar=null;return $returnArray;
    }

    /**
     * Creado por: Alexis Ontiveros.
     * Comvierte las fecha a mysql de las actividades.
     * @param $arrayActividades, array de cadenas de datos de las actividades.
     * @return array,
     */
    public function metFechaMsqlActividades($arrayActividades){
        foreach($arrayActividades as $cadena){
            $newCadena="";
            $arrActividad=explode('N', $cadena);
            $arrActividad[2]=$this->metFormateaFechaMsql($arrActividad[2]);
            $arrActividad[3]=$this->metFormateaFechaMsql($arrActividad[3]);
            $arrActividad[4]=$this->metFormateaFechaMsql($arrActividad[4]);
            $arrActividad[5]=$this->metFormateaFechaMsql($arrActividad[5]);
            $newCadena=$arrActividad[0].'N'.$arrActividad[1].'N'.$arrActividad[2].'N'.$arrActividad[3].'N'.$arrActividad[4].'N'.$arrActividad[5];
            $arrData[]=$newCadena;
        }
        $arrayActividades=null;return $arrData;
    }

    /**
     * Creado por   :Alexis Ontiveros.
     * Descripción  :Para obtener cualquier dato del registro de una planificación.
     * @param       :$idPlanificacion.
     * @return      :Array
     */
    public function metObtenerDatosPlanificacion($idPlanificacion){
        $result = $this->_db->query("SELECT * FROM pf_b001_planificacion_fiscal WHERE pk_num_planificacion=".$idPlanificacion);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetch();
    }
    /**
     * Creado por   :Alexis Ontiveros.
     * Descripción  :Extrae actividades según criterio de búsqueda.
     * @param       :Array de parámetros.
     * @return      :Array
     */
    public function metObtenerActividad($campos,$params){
        $criterio=false; $sql_criterio='';
        if(isset($params["idPlanificacion"])){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="fk_pfb001_num_planificacion = ".$params["idPlanificacion"]; $criterio=true;
        }
        if(isset($params["num_secuencia"])){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="num_secuencia_actividad = ".$params["num_secuencia"]; $criterio=true;
        }
        if(isset($params["estado_actividad"])){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="ind_estado_actividad = '".$params["estado_actividad"]."'";
        }
        $sql_criterio.=" ORDER BY num_secuencia_actividad ASC";
        $result = $this->_db->query("SELECT $campos FROM pf_c001_actividad_planific".$sql_criterio);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $rcrset=$result->fetchAll();
        $result=null; return $rcrset;
    }

    /**
     * Creado por Alexis Ontiveros.
     * Extrae el padre o padres de una dependencia de ente
     * @param $id_ente
     * @param null $saltolinea
     * @return string
     */
    public function metBuscaEntesPadre($id_ente, $saltolinea=NULL){
        $result = $this->_db->query("SELECT pk_num_ente,num_ente_padre,ind_nombre_ente, num_estatus FROM a039_ente WHERE pk_num_ente=".$id_ente);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $fila = $result->fetch(); $id_ente=""; $cadEntes='';
        if($fila){
            if($fila["num_ente_padre"]){
                $id_ente=$fila["num_ente_padre"];
            }
        }
        if($id_ente){
            $ciclar=true; $arrDep=array();
            while($ciclar){
                $result = $this->_db->query("SELECT pk_num_ente,num_ente_padre,ind_nombre_ente, num_estatus FROM a039_ente WHERE pk_num_ente=".$id_ente);
                $result->setFetchMode(PDO::FETCH_ASSOC);
                $fila = $result->fetch();
                if($fila['num_ente_padre']==0){
                    $arrDep[]=$fila['ind_nombre_ente']; $ciclar=false;
                }else{
                    $arrDep[]=$fila['ind_nombre_ente']; $id_ente=$fila['num_ente_padre'];
                }
            }
            //se recorre el array en forma decreciente para asignar los entes dependientes encontrados en una cadena separados por un guión y con salto de linea.
            if($saltolinea=='\n'){$Subtraer=2;}else{$saltolinea="<br>"; $Subtraer=4;}
            if(COUNT($arrDep)>1){ $k=0; $espacio="&nbsp;"; $sangria="";
                for($i=COUNT($arrDep)-1; $i>=0; $i--){
                    if($k>0){$sangria.=$espacio;}
                    $cadEntes .= $sangria.'-'.$arrDep[$i].'.'.$saltolinea;$k++;
                }
                $cadEntes=substr($cadEntes, 0, -$Subtraer);/*se suprime el último <br> ó \n*/
            }else{
                $cadEntes=$arrDep[0];
            }
        }
        $arrDep=null;$fila=null; return $cadEntes;
    }
    /**
     * Creado por Alexis Ontiveros.
     * Concatena los nombre y apellidos de una persona
     * @param array $arrData
     * @return string
     */
    public function metConsolidaNombrePersEnte($arrData){
        if($arrData["ind_nombre1"]){$titular=$arrData["ind_nombre1"];}
        if($arrData["ind_nombre2"]){$titular.=' '.$arrData["ind_nombre2"];}
        if($arrData["ind_apellido1"]){$titular.=' '.$arrData["ind_apellido1"];}
        if($arrData["ind_apellido2"]){$titular.=' '.$arrData["ind_apellido2"];}
        return $titular;
    }
    /**
     * Creado por Alexis Ontiveros.
     * Busca el tipo de proceso fiscal
     * @param null int $id_tipoproceso
     * @param null string $cod_proceso
     * @return mixed
     */
    public function metTipoProcesoFiscal($id_tipoproceso=NULL,$cod_proceso=NULL){
        $criterio=false; $sql_criterio='';
        if(isset($id_tipoproceso)){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="pk_num_proceso = ".$id_tipoproceso; $criterio=true;
        }
        if(isset($cod_proceso)){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="cod_proceso = '".$cod_proceso."'";
        }
        $sql_criterio.=" ORDER BY cod_proceso ASC";
        $result = $this->_db->query("SELECT * FROM pf_b002_proceso".$sql_criterio);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $rcset = $result->fetchAll();
        $result=null;return $rcset;
    }

    /**
     * Estados de actuación fiscal
     * @return array
     */
    public function metEstadosAF(){
        $arrEstado=array(
            array("PR"=>"En Preparación"),
            array("RV"=>"Revisada"),
            array("AP"=>"Aprobada"),
            array("TE"=>"Terminada"),
            array("CO"=>"Completada"),
            array("AN"=>"Anulada"),
            array("CE"=>"Cerrada"),
            array("EV"=>"Enviado CGR")
        );
        return $arrEstado;
    }
    /**
     * Estados de valoración preliminar de P.I.
     * @return array
     */
    public function metEstadosVP(){
        $arrEstado=array(
            array("PR"=>"En Preparación"),
            array("RV"=>"Revisada"),
            array("AP"=>"Aprobada"),
            array("AC"=>"Auto de Proceder"),
            array("AA"=>"Auto de Archivo"),
            array("AN"=>"Anulada"),
            array("CE"=>"Cerrada"),
            array("EV"=>"Enviado CGR"),
            array("VJ"=>"Enviado VJDR"),
            array("DV"=>"Devuelta")
        );
        return $arrEstado;
    }
    /**
     * Estados de valoración potestad investigativa.
     * @return array
     */
    public function metEstadosPI(){
        $arrEstado=array(
            array("PR"=>"En Preparación"),
            array("RV"=>"Revisada"),
            array("AP"=>"Aprobada"),
            array("TE"=>"Terminada"),
            array("CO"=>"Completada"),
            array("AN"=>"Anulada"),
            array("CE"=>"Cerrada"),
            array("EV"=>"Enviado CGR"),
            array("DV"=>"Devuelta")
        );
        return $arrEstado;
    }
    /**
     * Estados de valoración jurídica de determinación de responsabilidad.
     * @return array
     */
    public function metEstadosVJ(){
        $arrEstado=array(
            array("PR"=>"En Preparación"),
            array("RV"=>"Revisada"),
            array("AP"=>"Aprobada"),
            array("AI"=>"Auto de Inicio"),
            array("AA"=>"Auto de Archivo"),
            array("AN"=>"Anulada"),
            array("CE"=>"Cerrada"),
            array("EV"=>"Enviado CGR"),
            array("DV"=>"Devuelta")
        );
        return $arrEstado;
    }
    /**
     * Estados de determinación de responsabilidad administrativa.
     * @return array
     */
    public function metEstadosDRA(){
        $arrEstado=array(
            array("PR"=>"En Preparación"),
            array("RV"=>"Revisada"),
            array("AP"=>"Aprobada"),
            array("TE"=>"Terminada"),
            array("CO"=>"Completada"),
            array("AN"=>"Anulada"),
            array("CE"=>"Cerrada"),
            array("EV"=>"Enviado CGR")
        );
        return $arrEstado;
    }
    /**
     * Prepara el array de los estados de una determinada planificación para imprimirlos
     * @param $nombreFuncEstados, nombre de la función que contiene los estados del proceso a extraer
     * @return array
     */
    public function metListaEstadosPlanific($nombreFuncEstados){
        $nombFuncion="met".$nombreFuncEstados;
        $arrEstado=$this->$nombFuncion();
        foreach($arrEstado as $fila){
            $clave=array_keys($fila);
            $arrData[]=array("estado"=>$clave[0],"desc_estado"=>$fila[$clave[0]]);
        }
        return $arrData;
    }
    /**
     * Estados de las diferentes planificaciones.
     * @param $valor
     * @return string
     */
    public function metEstadoPlanificacion($valor){
        $retornar = "";
        switch ($valor) {
            case "PR"://Actuación fiscal, Potestad, Valoración Preliminar, Valoración Jurídica, Determinación de Responsabilidad.
                $retornar = "En Preparación";
                break;
            case "RV"://Actuación fiscal, Potestad, Valoración Preliminar, Valoración Jurídica, Determinación de Responsabilidad.
                $retornar = "Revisada";
                break;
            case "AP"://Actuación fiscal, Potestad, Valoración Preliminar, Valoración Jurídica, Determinación de Responsabilidad.
                $retornar = "Aprobada";
                break;
            case "TE"://Actuación fiscal, Potestad, Determinación de Responsabilidad.
                $retornar = "Terminada";
                break;
            case "CO"://Actuación fiscal, Potestad, Determinación de Responsabilidad.
                $retornar = "Completada";
                break;
            case "CE"://Actuación fiscal, Potestad, Valoración Preliminar, Valoración Jurídica, Determinación de Responsabilidad.
                $retornar = "Cerrada";
                break;
            case "AN"://Actuación fiscal, Potestad, Valoración Preliminar, Valoración Jurídica, Determinación de Responsabilidad.
                $retornar = "Anulada";
                break;
            case "AC"://Valoración Preliminar
                $retornar = "Auto de proceder";
                break;
            case "AA"://Valoración Preliminar, Valoración Jurídica.
                $retornar = "Auto de Archivo";
                break;
            case "VJ"://Valoración Preliminar
                $retornar = "Enviado VJDR";
                break;
            case "EV"://Actuación fiscal, Potestad, Valoración Preliminar, Valoración Jurídica, Determinación de Responsabilidad.
                $retornar = "Enviado CGR";
                break;
            case "DV":// Potestad, Valoración Preliminar, Valoración Jurídica.
                $retornar = "Devuelta";
                break;
            case "AI"://Valoración Jurídica,
                $retornar = "Auto de Inicio";
                break;
        }
        return $retornar;
    }
    /**
     * Tipos de Estado de una prórroga
     * @param $valor
     * @return string
     */
    public function metEstadoProrroga($valor){
        switch($valor){
            case "PR":
                $retornar="En Preparación";
                break;
            case "RV":
                $retornar="Revisada";
                break;
            case "AP":
                $retornar="Aprobada";
                break;
            case "AN":
                $retornar="Anulada";
                break;
        }
        return $retornar;
    }
    public function metEstadosProg(){
        $arrEstado=array(
            array("PR"=>"En Preparación"),
            array("RV"=>"Revisada"),
            array("AP"=>"Aprobada"),
            array("AN"=>"Anulada")
        );
        return $arrEstado;
    }
    /**
     * Busca el número de control de una planificación del PROCESO PADRE de una valoración júridica ó de potestad generadas por su correspondiente planifcación padre Devuelta.
     * @param $idPlanificPadre  : id de la planificación padre
     * @param $idProceso        : id del tipo de proceso padre
     * @return string           :
     */
    public function metBuscaProcesoPadre($idPlanificPadre,$idProceso){
        $ciclar=true;$nroControl="";
        while($ciclar){
            $result = $this->_db->query("SELECT num_planificacion_padre,fk_b002_num_proceso,cod_planificacion FROM pf_b001_planificacion_fiscal WHERE pk_num_planificacion=".$idPlanificPadre);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $fila = $result->fetch();
            if($fila){
                if($fila['fk_b002_num_proceso']==$idProceso){
                    $nroControl=$fila['cod_planificacion']; $ciclar=false;
                }else{
                    $idPlanificPadre=$fila['num_planificacion_padre'];
                }
            }else{
                $ciclar=false;
            }
            $fila=NULL;
        }
        return $nroControl;
    }
    public function metEstatus($valor){
        if($valor==1){
            return 'Activo';
        }else{
            return 'Inactivo';
        }
    }
    /**
     * Permite establecer las opciones de quitar, activar ó inactivar personal responsable asignados a una planificación
     * @param $estadoPlanificacion. Estado de la planificación
     * @param $estatusResponsable. Estatus de la persona asignada a la planificación
     * @return string
     */
    public function metOpcionAuditor($estadoPlanificacion,$estatusResponsable){
        $opcion_auditor='';
        if($estadoPlanificacion=='PR' OR $estadoPlanificacion=='RV' OR $estadoPlanificacion=='AP'){
            $opcion_auditor='Quitar';
            if($estadoPlanificacion=='AP' AND $estatusResponsable==0){
                $opcion_auditor='Activar';
            }
        }
        return $opcion_auditor;
    }
    /**
     * Permite establecer la habilitación ó no de los objetos de formulario en la asiganción de responsables a la planificación
     * @param $estadoPlanificacion
     * @return bool
     */
    public function metEstadoObjetosAuditor($estadoPlanificacion){
        $retornar=true;
        if($estadoPlanificacion=='CO' OR $estadoPlanificacion=='AN' OR $estadoPlanificacion=='CE' OR $estadoPlanificacion=='EV' OR $estadoPlanificacion=='AI' OR $estadoPlanificacion=='AA' OR $estadoPlanificacion=='DV' OR $estadoPlanificacion=='AC' OR $estadoPlanificacion=='VJ'){
            $retornar=false;
        }
        return $retornar;
    }

    /**
     * Tipos de Procedimientos administrativos
     * @return array
     */
    public function metTiposProcedimientosAdmin(){
        $arrTipos=array(
            array("valor"=>"PA","descripcion"=>"Procedimiento de responsabilidad administrativa"),
            array("valor"=>"IM","descripcion"=>"Procedimiento de imputación de multa"),
        );
        return $arrTipos;
    }

    /**
     * Busca los tipos de procesos fiscal asignados a una dependencia de control en particular
     * @param $idDependencia
     * @return array
     */
    public function metBuscaProcesosFiscal($idDependencia,$limit=NULL){
        $crit_limite="";
        if($limit){$crit_limite=" LIMIT 1";}
        $query = $this->_db->query("
          SELECT pk_num_proceso AS idProceso,txt_descripcion_proceso AS descProceso FROM pf_c009_procesodependencia
            INNER JOIN pf_b002_proceso ON pf_c009_procesodependencia.fk_pfb002_num_proceso = pf_b002_proceso.pk_num_proceso
          WHERE fk_a004_num_dependencia=".$idDependencia.$crit_limite
        );
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $result = $query->fetchAll();
        $retornar = $result; $result=NULL; return $retornar;
    }
    /**
     * Permite extraer los números de control de planificaciones de acuerdo a una dependencia de control y el proceso respectivo ó de una planificación en específico
     * @param $arrParams. Array de parámetros
     * @return array
     */
    public function metBuscaCodPlanificacion($arrParams){
        $sql_criterio="";$criterio=false;
        if(isset($arrParams["idProsesoFiscal"])){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="fk_b002_num_proceso = ".$arrParams["idProsesoFiscal"];
        }
        $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
        $sql_criterio.="cod_planificacion IS NOT NULL";
        if(isset($arrParams["idDependencia"])){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="fk_a004_num_dependencia = ".$arrParams["idDependencia"]; $criterio=true;
        }
        if(isset($arrParams["idPlanificacion"])){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="pk_num_planificacion = ".$arrParams["idPlanificacion"];
        }
        $query = $this->_db->query("SELECT pk_num_planificacion AS idPlanifOrigen, fk_b002_num_proceso AS idTipoProcFiscal, cod_planificacion AS codPlanificacion FROM pf_b001_planificacion_fiscal".$sql_criterio);
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $result = $query->fetchAll();
        $retornar = $result; $result=NULL; return $retornar;
    }

    /**
     * Busca las decisiones, multas y responsables de una determinada planificación de determinación de responsabilidades (Procedimiento Administrativo)
     * @param $idPlanificacion
     * @return array
     */
    public function metBuscaDesicionesPlan($idPlanificacion){
        $arrData=array();
        $query = $this->_db->query("
          SELECT ind_cedula_documento AS cedula_pers,CONCAT_WS(' ', ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2) AS nombre_persona,ind_tipodecision AS desc_decision,monto AS monto_multa,monto_reparo
          FROM pf_c006_decision
          INNER JOIN a003_persona ON pf_c006_decision.fk_a003_num_persona = a003_persona.pk_num_persona
          INNER JOIN pf_c007_tipo_decision ON pf_c006_decision.fk_pfc007_num_tipodecision = pf_c007_tipo_decision.pk_num_tipodecision
          WHERE fk_pfb001_num_planificacion=".$idPlanificacion." ORDER BY ind_nombre1"
        );
        $query->setFetchMode(PDO::FETCH_ASSOC);
        return $query->fetchAll();
    }
    /**
     * Determina si la persona que ejecuta es un usuario de alguna de las dependencias de control fiscal,
     * está como invitado ó es un usuario no válido para el módulo
     * @return : Un array con la ubicación organizacional y tipo de usuario de la persona que ejecuta
     */
    public function metValidaUsuario(){
        $userValido=false; $tipoUser=""; $retornar=array("userValido"=>false);
        $idUsuario=Session::metObtener('idUsuario');
        //Se extrae la ubicación organizacional del usuario.
        $campos="pk_num_empleado,pk_num_dependencia,ind_codinterno,num_flag_controlfiscal,pk_num_organismo";
        $arr_ubicacion=$this->metBuscaUbicacionOrgUsuario($campos,$idUsuario);
        if(COUNT($arr_ubicacion)>0){
            $arr_ubicacion=$arr_ubicacion[0];
            if($arr_ubicacion["num_flag_controlfiscal"]==1){//Si entra, es un Usuario de dependencia de control fiscal
                $tipoUser="userCtrolFcal"; $userValido=true;
            }elseif($arr_ubicacion["num_flag_controlfiscal"]==0){//De lo contrario, se sigue determinando el tipo de usuario
                $id_dependencia=$arr_ubicacion["pk_num_dependencia"]; $ciclar=true;
                //Se consulta de manera recursiva hasta encontrar la dependencia padre.
                while($ciclar){
                    $sqlQuery = $this->_db->query("SELECT pk_num_dependencia AS id_dependencia,ind_codpadre,num_flag_controlfiscal FROM a004_dependencia WHERE pk_num_dependencia=".$id_dependencia);
                    $sqlQuery->setFetchMode(PDO::FETCH_ASSOC);
                    $fila = $sqlQuery->fetch();
                    if($fila){
                        if ($fila['ind_codpadre'] == 0 OR $fila['num_flag_controlfiscal'] == 1) {//Es la dependencia padre.
                            if ($fila['num_flag_controlfiscal'] == 1) {//Es un Usuario de dependencia de control fiscal
                                $tipoUser="userCtrolFcal"; $userValido=true;
                                $arr_ubicacion["pk_num_dependencia"]=$fila['id_dependencia'];// asigna el id de la Dep. padre
                            } else {//De lo contrario, es un Usuario del tipo Invitado.
                                //Se verifica si el usuario está incluido por seguridad alterna (asignado a alguna dependencia de control fiscal)
                                $query = $this->_db->query("
                                    SELECT a019_seguridad_dependencia.fk_a018_num_seguridad_usuario FROM a019_seguridad_dependencia
                                        INNER JOIN a018_seguridad_usuario ON a019_seguridad_dependencia.fk_a018_num_seguridad_usuario = a018_seguridad_usuario.pk_num_seguridad_usuario
                                        INNER JOIN a015_seguridad_aplicacion ON a019_seguridad_dependencia.fk_a015_num_seguridad_aplicacion = a015_seguridad_aplicacion.pk_num_seguridad_aplicacion
                                    WHERE a019_seguridad_dependencia.fk_a018_num_seguridad_usuario=".$idUsuario." AND a018_seguridad_usuario.num_estatus=1 AND cod_aplicacion='PF' LIMIT 1
                                ");
                                $query->setFetchMode(PDO::FETCH_ASSOC);
                                $result=$query->fetch();
                                if($result){
                                    $tipoUser="userInvitado"; $userValido=true;
                                }
                            }
                            $ciclar = false;
                        }else{//Sigue el ciclo.
                            $id_dependencia = $fila['ind_codpadre'];
                        }
                    }else{
                        $ciclar = false;
                    }
                }
                $result=null; $fila=null;
            }
            $retornar=array_merge(array("userValido"=>$userValido,"tipoUser"=>$tipoUser), $arr_ubicacion);
        }
        return $retornar;
    }
    /**
     * Se busca las dependencias al cual el usuario invitado tiene acceso y de acuerdo al id del proceso fiscal
     * @param $idContraloria
     * @return Un array con las dependencias
     */
    public function metBuscaDepControlInvitado($idContraloria, $idTipoProceso=NULL){
        $retornar=array(); $idUsuario=Session::metObtener('idUsuario');
        if($idTipoProceso){
            $criterio=" AND fk_pfb002_num_proceso=".$idTipoProceso;
        }else{
            $criterio=" AND (cod_proceso !='04' AND cod_proceso !='05')"; //extrae las dependencias de control menos la de Determinación de Responsabilidad
        }
        $query = $this->_db->query("
            SELECT DISTINCT pf_c009_procesodependencia.fk_a004_num_dependencia AS id_dependencia, ind_dependencia AS nombre_dependencia
            FROM a019_seguridad_dependencia
            INNER JOIN a004_dependencia ON a019_seguridad_dependencia.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
            INNER JOIN a018_seguridad_usuario ON a019_seguridad_dependencia.fk_a018_num_seguridad_usuario = a018_seguridad_usuario.pk_num_seguridad_usuario
            INNER JOIN a015_seguridad_aplicacion ON a019_seguridad_dependencia.fk_a015_num_seguridad_aplicacion = a015_seguridad_aplicacion.pk_num_seguridad_aplicacion
            INNER JOIN pf_c009_procesodependencia ON pf_c009_procesodependencia.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
            INNER JOIN pf_b002_proceso ON pf_c009_procesodependencia.fk_pfb002_num_proceso = pf_b002_proceso.pk_num_proceso
            WHERE a019_seguridad_dependencia.fk_a018_num_seguridad_usuario=".$idUsuario." AND a018_seguridad_usuario.num_estatus=1 AND num_flag_controlfiscal=1
            AND fk_a001_num_organismo=".$idContraloria." AND cod_aplicacion='PF'".$criterio);
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $result=$query->fetchAll();
        if($result){
            $retornar=$result;
        }
        return $retornar;
    }
    /**
     * Busca sólo las dependendencias de control menos de Determinación de Responsabilidad.
     * @param $idContraloria
     * @return Un
     */
    public function metBuscaDepControlFiscal($idContraloria){
        return $this->metBuscaDepControlInvitado($idContraloria);
    }
    /*******************************Ubicaciones Geográficas**************************************/
    public function metListarPais(){
        $pais= $this->_db->query("SELECT * FROM a008_pais WHERE num_estatus='1'");
        $pais->setFetchMode(PDO::FETCH_ASSOC);
        return $pais->fetchAll();
    }
    public function metBuscaEntidad($idpais=NULL){
        if($idpais){
            $criterio=" fk_a008_num_pais=".$idpais." AND ";
        }
        $query="SELECT * FROM a009_estado WHERE ".$criterio."num_estatus='1'";
        $result= $this->_db->query($query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    public function metBuscaCiudad($idEstado=NULL){
        if($idEstado){
            $criterio=" fk_a009_num_estado=".$idEstado." AND ";
        }
        $query="SELECT * FROM a010_ciudad WHERE ".$criterio."num_estatus='1' ORDER BY ind_ciudad ASC";
        $result=$this->_db->query($query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    public function metBuscaMunicipio($idEstado=NULL){
        if($idEstado){
            $criterio=" fk_a009_num_estado=".$idEstado." AND ";
        }
        $query="SELECT * FROM a011_municipio WHERE ".$criterio."num_estatus='1' ORDER BY ind_municipio ASC";
        $result= $this->_db->query($query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    public function metBuscaParroquia($idMunicipio=NULL){
        if($idMunicipio){
            $criterio=" fk_a011_num_municipio=".$idMunicipio." AND ";
        }
        $query="SELECT * FROM a012_parroquia WHERE ".$criterio."num_estatus='1' ORDER BY ind_parroquia ASC";
        $result= $this->_db->query($query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }

    /**
     * Convierte un monto del tipo #.####,## al tipo #####.## para ser ingresado a la BD.
     * @param $monto
     * @return mixed
     */
    public function metConsolidaMontoBd($monto){
        if($monto>0){
            $monto=preg_replace('/[.]/', '', $monto);
            $monto=preg_replace('/[,]/', '.', $monto);
        }
        return $monto;
    }

    /**
     * Verifica si una carpeta existe, si no existe la crea.
     * @param $rutaCarpeta
     * @return bool
     */
    public function metValidaCarpeta($rutaCarpeta){
        if(file_exists($rutaCarpeta)){
            return true;
        }else{
            if(@mkdir($rutaCarpeta)){
                return true;
            }else{
                return false;
            }
        }
    }

    /**
     * Retorna la sigla de un proceso fiscal en particular.
     * @param $idProceso
     * @return string
     */
    public function metSiglaTipoProceso($idProceso){
        switch($idProceso){
            case 1:
                return 'AF'; //Actuación Fiscal
                break;
            case 2:
                return 'VP'; //Valoración Preliminar
                break;
            case 3:
                return 'PI'; //Potestad Investigativa
                break;
            case 4:
                return 'VJ'; //Valoración Jurídica
                break;
            case 5:
                return 'PA'; //Procedimientos Administrativos
                break;
        }
    }

    /**
     * Compara dos fechas en formato 'dd-mm-aaaa'
     * @param $fecha1
     * @param $fecha2
     * @param null $opcion
     * @return int
     */
    public function metComparaFechas($fecha1,$fecha2,$opcion=NULL){
        $f1 = explode("-",$fecha1); $f2 = explode("-",$fecha2);
        $fecha1=GregorianToJd($f1[1],$f1[0],$f1[2]); $fecha2=GregorianToJd($f2[1],$f2[0],$f2[2]);
        if($opcion=='esMenor'){
            if($fecha1 < $fecha2){return 1;}else{return 0;}
        }elseif($opcion=='esMayor'){
            if($fecha1 > $fecha2){return 1;}else{return 0;}
        }else{
            if($fecha1 == $fecha2){return 1;}else{return 0;}
        }
    }
    public function metBuscaCodsPlanificacion($arrParams){
        $result= $this->_db->query("
          SELECT cod_planificacion AS nro_planificacion FROM pf_b001_planificacion_fiscal
          WHERE fk_b002_num_proceso=".$arrParams['idTipoProceso']." AND fk_a001_num_organismo=".$arrParams['idContraloria']." AND fk_a004_num_dependencia=".$arrParams['idDependencia']." AND YEAR(fec_inicio)='".$arrParams['yearPlanificacion']."'"
        );
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }

    /**
     * Busca los datos de emcabezado de reportes según el id de la dependencia
     * @param $idDependencia
     * @return mixed
     */
    public function metBuscaDatosEncabezado($idDependencia){
        $query = $this->_db->query("SELECT ind_descripcion_empresa AS nombre_contraloria, ind_logo AS logo_reporte, ind_dependencia AS nombre_dependencia
                    FROM a001_organismo
                      INNER JOIN a004_dependencia ON a001_organismo.pk_num_organismo = a004_dependencia.fk_a001_num_organismo
                    WHERE pk_num_dependencia=$idDependencia
        ");
        $query->setFetchMode(PDO::FETCH_ASSOC);
        return $query->fetch();
    }
    /**
     * Permite verificar si una planificación tiene prórrogas.
     * @param $idPlanificacion
     * @return mixed
     */
    public function metVerificaProrroga($idPlanificacion){
        $query = $this->_db->query("SELECT pk_num_prorroga_actividad FROM pf_c003_prorroga_actividad
            INNER JOIN pf_c001_actividad_planific ON pf_c003_prorroga_actividad.fk_pfc001_num_actividad_planific = pf_c001_actividad_planific.pk_num_actividad_planific
            WHERE fk_pfb001_num_planificacion=".$idPlanificacion
        );
        $query->setFetchMode(PDO::FETCH_ASSOC);
        return $query->fetch();
    }
}
?>