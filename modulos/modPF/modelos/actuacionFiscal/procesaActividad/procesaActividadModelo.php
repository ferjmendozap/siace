<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Planificación Fiscal
 * PROCESO: Procesa las actividades de cada proceso fiscal
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          José Díaz                        |        diaz.jose@cmldc.gob.ve      |         0416-2124302           |
 * | 2 |          Alexis Ontiveros                 |    ontiveros.alexis@cmldc.gob.ve   |         0426-514.43.82         |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        20-01-2017       |         1.0        |
 * |               #2                      |        26-06-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class procesaActividadModelo extends Modelo{
    public $atFuncionesGnrles;
    public function __construct(){
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }
    public function metListarActividaPlanificacion($idTipoProceso,$idDepCtrolFiscUser){
        $actividadPlanificacion = $this->_db->query(
            "SELECT pf_b001.pk_num_planificacion,
                   pf_b001.cod_planificacion AS codigo ,
                   pf_b003.cod_fase AS codFase,
                   pf_b003.txt_descripcion_fase AS fase,
                   pf_b004.txt_descripcion_actividad AS actividad,
                   a041.fk_a039_num_ente,
                   pf_c001.num_dias_duracion_actividad AS dias,
                   pf_c001.fec_inicio_real_actividad AS fec_ini,
                   pf_c001.fec_culmina_real_actividad AS fec_fin
                        FROM pf_c001_actividad_planific pf_c001
                        INNER JOIN pf_b004_actividad pf_b004
                        ON pf_b004.pk_num_actividad = pf_c001.fk_pfb004_num_actividad
                        INNER JOIN pf_b001_planificacion_fiscal pf_b001
                        ON pf_b001.pk_num_planificacion = pf_c001.fk_pfb001_num_planificacion
                        INNER JOIN a041_persona_ente a041
                        ON pf_b001.fk_a041_num_persona_ente = a041.pk_num_persona_ente
                        INNER JOIN pf_b003_fase pf_b003 ON pf_b003.pk_num_fase = pf_b004.fk_pfb003_num_fase
                        WHERE fk_b002_num_proceso=".$idTipoProceso." AND pf_c001.ind_estado_actividad='EJ' AND pf_b001.fk_a004_num_dependencia=".$idDepCtrolFiscUser."
                        ORDER BY codFase, codigo"
        );
        $actividadPlanificacion->setFetchMode(PDO::FETCH_ASSOC);
        return $actividadPlanificacion->fetchAll();
    }
    /**
     * Extrae la actividad en ejecución de acuerdo al id de la planificación para montarla en el form Procesa Actividad.
     * @param $idPlanificacion
     * @return array
     */
    public function metMostrarActividaPlanificacion($idPlanificacion){
        $actividadPlanificacion = $this->_db->query(
            "SELECT a039.pk_num_ente, pf_b001.pk_num_planificacion,pf_b001.num_planificacion_padre AS idPlanifPadre, pf_c001.pk_num_actividad_planific,
                    pf_b001.cod_planificacion AS codigo, pf_b003.txt_descripcion_fase AS fase,pf_b004.txt_descripcion_actividad AS actividad,
                    ind_afecto_plan AS actividad_afectaPlan, a039.ind_nombre_ente AS ente,pf_c001.num_dias_duracion_actividad AS lapso_actividad,
                    pf_c001.num_dias_prorroga_actividad AS diasProrroga,pf_c001.fec_inicio_real_actividad AS fec_ini, pf_c001.fec_culmina_real_actividad AS fec_fin,
                    pf_b001.ind_objetivo AS objetivo,pf_c001.num_secuencia_actividad AS nro_secuenciActividad,ind_observacion_actividad AS observ_actividad
                FROM pf_c001_actividad_planific pf_c001
                INNER JOIN pf_b004_actividad pf_b004 ON pf_b004.pk_num_actividad = pf_c001.fk_pfb004_num_actividad
                INNER JOIN pf_b001_planificacion_fiscal pf_b001 ON pf_b001.pk_num_planificacion = pf_c001.fk_pfb001_num_planificacion
                INNER JOIN a041_persona_ente a041 ON pf_b001.fk_a041_num_persona_ente = a041.pk_num_persona_ente
                INNER JOIN a039_ente a039 ON a039.pk_num_ente = a041.fk_a039_num_ente
                INNER JOIN pf_b003_fase pf_b003 ON pf_b003.pk_num_fase = pf_b004.fk_pfb003_num_fase
                WHERE pk_num_planificacion=".$idPlanificacion." AND pf_c001.ind_estado_actividad = 'EJ' ORDER BY pk_num_planificacion"
        );
        $actividadPlanificacion->setFetchMode(PDO::FETCH_ASSOC);
        return $actividadPlanificacion->fetchAll();
    }
    /**
     * Verifica si la siguiente actividad afecta la planificación
     * @param $idPlanificacion
     * @param null $numScuenciaActividad
     * @return mixed
     */
    public function metActividadesAfecta($idPlanificacion,$numScuenciaActividad=NULL){
        if($numScuenciaActividad){
            $criterioSecuencia=" AND num_secuencia_actividad > ".$numScuenciaActividad." AND";
        }
        $sqlQuery = $this->_db->query(" SELECT pk_num_actividad_planific FROM pf_c001_actividad_planific pf_c001
		  INNER JOIN pf_b004_actividad pf_b004 ON pf_b004.pk_num_actividad=pf_c001.fk_pfb004_num_actividad
          WHERE fk_pfb001_num_planificacion = ".$idPlanificacion."$criterioSecuencia ind_estado_actividad!='TE' AND pf_b004.ind_afecto_plan='S'
          ORDER BY num_secuencia_actividad ASC LIMIT 1");
        $sqlQuery->setFetchMode(PDO::FETCH_ASSOC);
        return $sqlQuery->fetch();
    }
    /**
     * Se verifica si la actividad tiene alguna prórroga sin aprobar
     * @param $idPlanificacion
     * @return mixed
     */
    public function metObtenerProrroga($pk_num_actividad_planific){
        $prorroga = $this->_db->query(
            "SELECT * FROM pf_c003_prorroga_actividad WHERE fk_pfc001_num_actividad_planific=".$pk_num_actividad_planific." AND (ind_estado = 'PR' OR ind_estado = 'RV');"
        );
        $prorroga->setFetchMode(PDO::FETCH_ASSOC);
        return $prorroga->fetch();
    }
/*************************************************TERMINAR ACTIVIDADES*************************************************/
    /**
     * Termina una actividad en cuestión y coloca la siguiente como en 'En Ejecución'.
     * @param $params
     * @return bool
     */
    public function metTerminaActividad($params){
        $params["numSecuenciaActividad"]=$params["numSecuenciaActividad"]+1;
        $this->_db->beginTransaction();
        //Termina la actividad en proceso
        $sqlQuery_1=$this->_db->prepare("
            UPDATE pf_c001_actividad_planific SET
              num_dias_adelanto_actividad=:num_dias_adelanto_actividad,
              fec_termino_cierre_actividad=:fec_termino_cierre_actividad,
              fec_registro_cierre_actividad=:fec_registro_cierre_actividad,
              num_dias_cierre_actividad=:num_dias_cierre_actividad,
              ind_estado_actividad=:ind_estado_actividad,
              ind_observacion_actividad=:ind_observacion_actividad,
              fk_a018_num_seg_usuaro_map=:fk_a018_num_seg_usuaro_map,
              fec_modifica_ap=:fec_modifica_ap
            WHERE pk_num_actividad_planific=:pk_num_actividad_planific
        ");
        $sqlQuery_1->execute(array(
            "num_dias_adelanto_actividad"=>$params["cant_diasAdelanto"],
            "fec_termino_cierre_actividad"=>$params["fec_terminado"],
            "fec_registro_cierre_actividad"=>$params["fec_reg_cierre"],
            "num_dias_cierre_actividad"=>$params["num_CantDiasCulmina"],
            "ind_estado_actividad"=>$params["estadoActvidad"],
            "ind_observacion_actividad"=>$params["ind_observacion"]?$params["ind_observacion"]:NULL,
            "fk_a018_num_seg_usuaro_map"=>$this->atIdUsuario,
            "fec_modifica_ap"=>date("Y-m-d H:i:s"),
            "pk_num_actividad_planific"=>$params["idActividadPlanif"]
        ));
        $error1 = $sqlQuery_1->errorInfo();
        //Coloca la siguiente actividad en 'Ejecución'
        $sqlQuery_2=$this->_db->prepare("
            UPDATE pf_c001_actividad_planific SET
              ind_estado_actividad=:ind_estado_actividad,
              fk_a018_num_seg_usuaro_map=:fk_a018_num_seg_usuaro_map,
              fec_modifica_ap=:fec_modifica_ap
            WHERE fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion AND num_secuencia_actividad=:num_secuencia_actividad
        ");
        $sqlQuery_2->execute(array(
            "ind_estado_actividad"=>"EJ",
            "fk_a018_num_seg_usuaro_map"=>$this->atIdUsuario,
            "fec_modifica_ap"=>date("Y-m-d H:i:s"),
            "fk_pfb001_num_planificacion"=>$params["idPlanificacion"],
            "num_secuencia_actividad"=>$params["numSecuenciaActividad"]
        ));
        $error2 = $sqlQuery_2->errorInfo();
        if((!empty($error1[1]) && !empty($error1[2])) OR (!empty($error2[1]) && !empty($error2[2]))){
            $this->_db->rollBack(); return false;
        }else{
            $this->_db->commit(); return true;
        }
    }
/*******************************************CULMINAR PLANIFICACIONES***************************************************/
    /**
     * Culmina una planificación al tipo 'TERMINADA' y su últmina actividad a terminada.
     * @param $params, array de parámetros
     * @return bool
     */
    public function metCulminaPlanificacionYactividad($params){
        $this->_db->beginTransaction();
        //Culmina la planificación como 'Terminada'. Nota: No completada.
        $sqlQuery_1=$this->_db->prepare(" UPDATE pf_b001_planificacion_fiscal SET
                ind_estado_planificacion=:ind_estado_planificacion,
                fk_a018_num_seg_usuario_mod=:fk_a018_num_seg_usuario_mod,
                fec_ultima_modific_planificacion=:fec_ultima_modific_planificacion
            WHERE pk_num_planificacion=:pk_num_planificacion");
        $sqlQuery_1->execute(array(
            'ind_estado_planificacion'=>$params["estadoPlanificacion"],
            'fk_a018_num_seg_usuario_mod'=>$this->atIdUsuario,
            'fec_ultima_modific_planificacion'=>date("Y-m-d H:i:s"),
            'pk_num_planificacion'=>$params["idPlanificacion"]
        ));
        $error1 = $sqlQuery_1->errorInfo();
        //Termina la actividad en cuestión
        $sqlQuery_2=$this->_db->prepare("
            UPDATE pf_c001_actividad_planific SET
              num_dias_adelanto_actividad=:num_dias_adelanto_actividad,
              fec_termino_cierre_actividad=:fec_termino_cierre_actividad,
              fec_registro_cierre_actividad=:fec_registro_cierre_actividad,
              num_dias_cierre_actividad=:num_dias_cierre_actividad,
              ind_estado_actividad=:ind_estado_actividad,
              ind_observacion_actividad=:ind_observacion_actividad,
              fk_a018_num_seg_usuaro_map=:fk_a018_num_seg_usuaro_map,
              fec_modifica_ap=:fec_modifica_ap
            WHERE pk_num_actividad_planific=:pk_num_actividad_planific
        ");
        $sqlQuery_2->execute(array(
            "num_dias_adelanto_actividad"=>$params["cant_diasAdelanto"],
            "fec_termino_cierre_actividad"=>$params["fec_terminado"],
            "fec_registro_cierre_actividad"=>$params["fec_reg_cierre"],
            "num_dias_cierre_actividad"=>$params["num_CantDiasCulmina"],
            "ind_estado_actividad"=>$params["estadoActvidad"],
            "ind_observacion_actividad"=>$params["ind_observacion"]?$params["ind_observacion"]:NULL,
            "fk_a018_num_seg_usuaro_map"=>$this->atIdUsuario,
            "fec_modifica_ap"=>date("Y-m-d H:i:s"),
            "pk_num_actividad_planific"=>$params["idActividadPlanif"]
        ));
        $error2 = $sqlQuery_2->errorInfo();
        //Coloca la actividad siguiente en estado de 'En Ejecución'
        $params["numSecuenciaActividad"]=$params["numSecuenciaActividad"]+1;
        $sqlQuery_3=$this->_db->prepare("
            UPDATE pf_c001_actividad_planific SET
              ind_estado_actividad=:ind_estado_actividad,
              fk_a018_num_seg_usuaro_map=:fk_a018_num_seg_usuaro_map,
              fec_modifica_ap=:fec_modifica_ap
            WHERE fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion AND num_secuencia_actividad=:num_secuencia_actividad
        ");
        $sqlQuery_3->execute(array(
            "ind_estado_actividad"=>"EJ",
            "fk_a018_num_seg_usuaro_map"=>$this->atIdUsuario,
            "fec_modifica_ap"=>date("Y-m-d H:i:s"),
            "fk_pfb001_num_planificacion"=>$params["idPlanificacion"],
            "num_secuencia_actividad"=>$params["numSecuenciaActividad"]
        ));
        $error3 = $sqlQuery_3->errorInfo();
        if((!empty($error1[1]) && !empty($error1[2])) OR (!empty($error2[1]) && !empty($error2[2])) OR (!empty($error3[1]) && !empty($error3[2]))){
            $this->_db->rollBack(); return false;
        }else{
            $this->_db->commit(); return true;
        }
    }
    /**
     * Culmina una planificación como 'COMPLETADA' y termina su última activida,
     * @param $idPlanificacion
     * @param $estadoPlanificacion
     * @return bool
     */
    public function metCompletarPlanificacion($params){
        $this->_db->beginTransaction();
        //Culmina la planificación como 'Completada'.
        $sqlQuery_1=$this->_db->prepare(" UPDATE pf_b001_planificacion_fiscal SET
                ind_estado_planificacion=:ind_estado_planificacion,
                fk_a018_num_seg_usuario_mod=:fk_a018_num_seg_usuario_mod,
                fec_ultima_modific_planificacion=:fec_ultima_modific_planificacion
            WHERE pk_num_planificacion=:pk_num_planificacion");
        $sqlQuery_1->execute(array(
            'ind_estado_planificacion'=>$params["estadoPlanificacion"],
            'fk_a018_num_seg_usuario_mod'=>$this->atIdUsuario,
            'fec_ultima_modific_planificacion'=>date("Y-m-d H:i:s"),
            'pk_num_planificacion'=>$params["idPlanificacion"]
        ));
        $error1 = $sqlQuery_1->errorInfo();
        //Termina la actividad en cuestión
        $sqlQuery_2=$this->_db->prepare("
            UPDATE pf_c001_actividad_planific SET
              num_dias_adelanto_actividad=:num_dias_adelanto_actividad,
              fec_termino_cierre_actividad=:fec_termino_cierre_actividad,
              fec_registro_cierre_actividad=:fec_registro_cierre_actividad,
              num_dias_cierre_actividad=:num_dias_cierre_actividad,
              ind_estado_actividad=:ind_estado_actividad,
              ind_observacion_actividad=:ind_observacion_actividad,
              fk_a018_num_seg_usuaro_map=:fk_a018_num_seg_usuaro_map,
              fec_modifica_ap=:fec_modifica_ap
            WHERE pk_num_actividad_planific=:pk_num_actividad_planific
        ");
        $sqlQuery_2->execute(array(
            "num_dias_adelanto_actividad"=>$params["cant_diasAdelanto"],
            "fec_termino_cierre_actividad"=>$params["fec_terminado"],
            "fec_registro_cierre_actividad"=>$params["fec_reg_cierre"],
            "num_dias_cierre_actividad"=>$params["num_CantDiasCulmina"],
            "ind_estado_actividad"=>$params["estadoActvidad"],
            "ind_observacion_actividad"=>$params["ind_observacion"]?$params["ind_observacion"]:NULL,
            "fk_a018_num_seg_usuaro_map"=>$this->atIdUsuario,
            "fec_modifica_ap"=>date("Y-m-d H:i:s"),
            "pk_num_actividad_planific"=>$params["idActividadPlanif"]
        ));
        $error2 = $sqlQuery_2->errorInfo();
        if((!empty($error1[1]) && !empty($error1[2])) OR (!empty($error2[1]) && !empty($error2[2]))){
            $this->_db->rollBack(); return false;
        }else{
            $this->_db->commit(); return true;
        }
    }
    /***********************************************PROCESO DE DEVOLUCIÓN**********************************************/
    public function metProcesaDevolucion($params){
        $this->_db->beginTransaction();
        //Actualiza la planificación padre como 'DEVUELTA'.
        $sqlQuery_1=$this->_db->prepare(" UPDATE pf_b001_planificacion_fiscal SET
                ind_estado_planificacion=:ind_estado_planificacion,
                ind_flag_situacion=:ind_flag_situacion,
                ind_nota_justificacion=:ind_nota_justificacion,
                fec_nota_justificacion=:fec_nota_justificacion,
                fk_rhb001_num_empleado_notajust=:fk_rhb001_num_empleado_notajust
            WHERE pk_num_planificacion=:pk_num_planificacion");
        $sqlQuery_1->execute(array(
            'ind_estado_planificacion'=>$params["estadoPlanificacion"],
            'ind_flag_situacion'=>$params["flagSituacion"],
            'ind_nota_justificacion'=>$params["ind_nota_justificacion"],
            'fec_nota_justificacion'=>date("Y-m-d"),
            'fk_rhb001_num_empleado_notajust'=>$params["IdEmpleadoUser"],
            'pk_num_planificacion'=>$params["idPlanifPadre"]
        ));
        $error1 = $sqlQuery_1->errorInfo();
        //Culmina la planificación actual como 'DEVUELTA'.
        $sqlQuery_2=$this->_db->prepare(" UPDATE pf_b001_planificacion_fiscal SET
                ind_estado_planificacion=:ind_estado_planificacion,
                ind_flag_situacion=:ind_flag_situacion,
                ind_nota_justificacion=:ind_nota_justificacion,
                fec_nota_justificacion=:fec_nota_justificacion,
                fk_rhb001_num_empleado_notajust=:fk_rhb001_num_empleado_notajust,
                fk_a018_num_seg_usuario_mod=:fk_a018_num_seg_usuario_mod,
                fec_ultima_modific_planificacion=:fec_ultima_modific_planificacion
            WHERE pk_num_planificacion=:pk_num_planificacion");
        $sqlQuery_2->execute(array(
            'ind_estado_planificacion'=>$params["estadoPlanificacion"],
            'ind_flag_situacion'=>$params["flagSituacion"],
            'ind_nota_justificacion'=>$params["ind_nota_justificacion"],
            'fec_nota_justificacion'=>date("Y-m-d"),
            'fk_rhb001_num_empleado_notajust'=>$params["IdEmpleadoUser"],
            'fk_a018_num_seg_usuario_mod'=>$this->atIdUsuario,
            'fec_ultima_modific_planificacion'=>date("Y-m-d H:i:s"),
            'pk_num_planificacion'=>$params["idPlanificacion"]
        ));
        $error2 = $sqlQuery_1->errorInfo();
        //Termina la actividad en cuestión de la planificación actual
        $sqlQuery_3=$this->_db->prepare("
            UPDATE pf_c001_actividad_planific SET
              num_dias_adelanto_actividad=:num_dias_adelanto_actividad,
              fec_termino_cierre_actividad=:fec_termino_cierre_actividad,
              fec_registro_cierre_actividad=:fec_registro_cierre_actividad,
              num_dias_cierre_actividad=:num_dias_cierre_actividad,
              ind_estado_actividad=:ind_estado_actividad,
              ind_observacion_actividad=:ind_observacion_actividad,
              fk_a018_num_seg_usuaro_map=:fk_a018_num_seg_usuaro_map,
              fec_modifica_ap=:fec_modifica_ap
            WHERE pk_num_actividad_planific=:pk_num_actividad_planific
        ");
        $sqlQuery_3->execute(array(
            "num_dias_adelanto_actividad"=>$params["cant_diasAdelanto"],
            "fec_termino_cierre_actividad"=>$params["fec_terminado"],
            "fec_registro_cierre_actividad"=>$params["fec_reg_cierre"],
            "num_dias_cierre_actividad"=>$params["num_CantDiasCulmina"],
            "ind_estado_actividad"=>$params["estadoActvidad"],
            "ind_observacion_actividad"=>$params["ind_observacion"]?$params["ind_observacion"]:NULL,
            "fk_a018_num_seg_usuaro_map"=>$this->atIdUsuario,
            "fec_modifica_ap"=>date("Y-m-d H:i:s"),
            "pk_num_actividad_planific"=>$params["idActividadPlanif"]
        ));
        $error3 = $sqlQuery_2->errorInfo();
        if((!empty($error1[1]) && !empty($error1[2])) OR (!empty($error2[1]) && !empty($error2[2]))  OR (!empty($error3[1]) && !empty($error3[2]))){
            $this->_db->rollBack(); return false;
        }else{
            $this->_db->commit(); return true;
        }
    }
    /**
     * Busca las planificaciones de acuerdo a los parámetros pasados.
     * @param $case
     * @param $params
     * @return array
     */
    public function metBuscarPlanificaciones($case,$params){
        $sql_criterio="";$criterio=false;
        switch($case){
            case 'por_listado': //Extrae Planificaciones que no esten en: Preparación, Revisadas ó Cerradas.
                if($params["IdTipoProceso"]){
                    $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="fk_b002_num_proceso = ".$params["IdTipoProceso"]; $criterio=true;
                }
                if($params["IdDependenciaCtrol"]){
                    $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="fk_a004_num_dependencia = ".$params["IdDependenciaCtrol"]; $criterio=true;
                }
                $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="(ind_estado_planificacion!='PR' AND ind_estado_planificacion!='RV' AND ind_estado_planificacion!='CE')";
                $sql_criterio.=" ORDER BY cod_planificacion";
            break;
            case 'por_idPlanificacion':
                if($params["idPlanificacion"]){
                    $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="pk_num_planificacion = ".$params["idPlanificacion"];
                }
            break;
        }
        $actuaciones = $this->_db->query("SELECT *,fk_a039_num_ente FROM pf_b001_planificacion_fiscal
          INNER JOIN a041_persona_ente ON pf_b001_planificacion_fiscal.fk_a041_num_persona_ente = a041_persona_ente.pk_num_persona_ente".$sql_criterio);
        $actuaciones->setFetchMode(PDO::FETCH_ASSOC);
        return $actuaciones->fetchAll();
    }
    /**
     * Busca auditores asignados a la planificación.
     * @param $params
     * @return array
     */
    public function metAuditoresDesignados($params){
        $tabla="pf_c002_auditor_planificacion"; $campos="*";$sql_criterio="";$criterio=false;
        $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="fk_pfb001_num_planificacion=".$params['idPlanificacion']; $criterio=true;
        if(isset($params['pk_num_empleado'])){
            $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="fk_rhb001_num_empleado = ".$params['pk_num_empleado'];$criterio=true;
        }
        if(isset($params['flag_coordinador'])){
            $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="num_flag_coordinador = ".$params['flag_coordinador'];
        }
        $sql_query="SELECT $campos FROM $tabla".$sql_criterio;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    public function metActividadesAsignadas($idPlanificacion){
        $sql_query="SELECT pf_c001_actividad_planific.*,num_dias_duracion_actividad AS num_duracion_actividad,pk_num_actividad, txt_descripcion_actividad, ind_afecto_plan, pf_b003_fase.cod_fase,pf_b003_fase.txt_descripcion_fase
        FROM pf_c001_actividad_planific
          INNER JOIN pf_b004_actividad
            ON pf_c001_actividad_planific.fk_pfb004_num_actividad=pf_b004_actividad.pk_num_actividad
        INNER JOIN pf_b003_fase
            ON pf_b004_actividad.fk_pfb003_num_fase=pf_b003_fase.pk_num_fase
        WHERE fk_pfb001_num_planificacion=$idPlanificacion
            ORDER BY cod_fase, cod_actividad";
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Se extrae la secuencia de la última actividad de la planificación en cuestión
     * @param $idPlanificacion
     * @return mixed
     */
    public function metSecueciaUltimaActividad($idPlanificacion){
        $sql="SELECT MAX(num_secuencia_actividad) AS nro_secuenciaUltimaActividad FROM pf_c001_actividad_planific
              WHERE fk_pfb001_num_planificacion=".$idPlanificacion." ORDER BY num_secuencia_actividad DESC LIMIT 1";
        $resultado=$this->_db->query($sql);
        $resultado->setFetchMode(PDO::FETCH_ASSOC);
        return $resultado->fetch();
    }
    public function metEsPadre($idPf){
        $idPadre = $this->_db->query("SELECT num_planificacion_padre FROM pf_b001_planificacion_fiscal
          WHERE num_planificacion_padre=$idPf
        ");
        $idPadre->setFetchMode(PDO::FETCH_ASSOC);
        return $idPadre->fetchAll();
    }
    /***********************************PROCESO DE REVERSAR PLANIFICACIONES CULMINADAS*********************************/
    /**
     * Se extrae una actividad de la planificación actual de acuerdo a los parámetros pasados
     * @param $case
     * @param $params
     * @return mixed
     */
    public function metBuscaActividadPlanificacion($case,$params){
        $sql_criterio="";$criterio=false;
        switch($case){
            case 'ulmina_actividad': //Extrae los datos de la última actividad.
                if($params["idPlanificacion"]){
                    $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="fk_pfb001_num_planificacion = ".$params["idPlanificacion"];
                    $sql_criterio.=" ORDER BY num_secuencia_actividad DESC LIMIT 1";
                }
            break;
            case 'por_actividadAfectas': //Se extrae una activida afecta
                $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="fk_pfb001_num_planificacion = ".$params["idPlanificacion"]; $criterio=true;
                $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="ind_afecto_plan='S'";
                $sql_criterio.=" ORDER BY num_secuencia_actividad ASC LIMIT 1";
            break;
        }
        $sqlQuery=$this->_db->query("
          SELECT pk_num_actividad_planific AS id_ActividadPlanif, num_secuencia_actividad AS secuencia_actividad, ind_afecto_plan AS flag_afectaPlan
          FROM pf_c001_actividad_planific
          INNER JOIN pf_b004_actividad ON pf_c001_actividad_planific.fk_pfb004_num_actividad = pf_b004_actividad.pk_num_actividad".$sql_criterio
        );
        $sqlQuery->setFetchMode(PDO::FETCH_ASSOC);
        return $sqlQuery->fetch();
    }
    /**
     * Reversa una planificación y su última actividad al estado anterior
     * @param $params
     * @return bool
     */
    public function metReversaPlanificacion($params,$opcion=NULL){
       // print_r($params); echo 'H:'.$opcion;exit;
        $this->_db->beginTransaction();
        if($opcion=='DV') {
            //Se actualiza la planificación padre a su estado anterior
            $sqlQuery_1 = $this->_db->prepare("UPDATE pf_b001_planificacion_fiscal SET
                ind_estado_planificacion=:ind_estado_planificacion,
                ind_flag_situacion=:ind_flag_situacion,
                ind_nota_justificacion=:ind_nota_justificacion,
                fec_nota_justificacion=:fec_nota_justificacion,
                fk_rhb001_num_empleado_notajust=:fk_rhb001_num_empleado_notajust
                WHERE pk_num_planificacion=:pk_num_planificacion
            ");
            $sqlQuery_1->execute(array(
                'ind_estado_planificacion' => $params["estadoPlaficPadre"],
                'ind_flag_situacion' => $params["flag_situacionPadre"],
                'ind_nota_justificacion' => null,
                'fec_nota_justificacion' => null,
                'fk_rhb001_num_empleado_notajust' => null,
                'pk_num_planificacion' => $params["idPlanificacionPadre"]
            ));
            $error1 = $sqlQuery_1->errorInfo();
        }
        //Se actualiza la planificación actual a su estado anterior
        $sqlQuery_2=$this->_db->prepare("UPDATE pf_b001_planificacion_fiscal SET
                ind_estado_planificacion=:ind_estado_planificacion,
                ind_flag_situacion=:ind_flag_situacion,
                ind_nota_justificacion=:ind_nota_justificacion,
                fec_nota_justificacion=:fec_nota_justificacion,
                fk_rhb001_num_empleado_notajust=:fk_rhb001_num_empleado_notajust,
                fk_a018_num_seg_usuario_mod=:fk_a018_num_seg_usuario_mod,
                fec_ultima_modific_planificacion=:fec_ultima_modific_planificacion
                WHERE pk_num_planificacion=:pk_num_planificacion
            ");
        $sqlQuery_2->execute(array(
            'ind_estado_planificacion'=>$params["estadoPlanif"],
            'ind_flag_situacion'=>$params["flag_situacion"],
            'ind_nota_justificacion'=>null,
            'fec_nota_justificacion'=>null,
            'fk_rhb001_num_empleado_notajust'=>null,
            'fk_a018_num_seg_usuario_mod'=>$this->atIdUsuario,
            'fec_ultima_modific_planificacion'=>date("Y-m-d H:i:s"),
            'pk_num_planificacion'=>$params["idPlanificacion"]
        ));
        $error2 = $sqlQuery_2->errorInfo();
        //Se actualiza la última actividad de la planificación a su estado anterior
        $sqlQuery_3=$this->_db->prepare("UPDATE pf_c001_actividad_planific SET
              num_dias_adelanto_actividad=:num_dias_adelanto_actividad,
              fec_termino_cierre_actividad=:fec_termino_cierre_actividad,
              fec_registro_cierre_actividad=:fec_registro_cierre_actividad,
              num_dias_cierre_actividad=:num_dias_cierre_actividad,
              ind_estado_actividad=:ind_estado_actividad,
              fk_a018_num_seg_usuaro_map=:fk_a018_num_seg_usuaro_map,
              fec_modifica_ap=:fec_modifica_ap
              WHERE pk_num_actividad_planific=:pk_num_actividad_planific
          ");
        $sqlQuery_3->execute(array(
            'num_dias_adelanto_actividad'=>null,
            'fec_termino_cierre_actividad'=>null,
            'fec_registro_cierre_actividad'=>null,
            'num_dias_cierre_actividad'=>null,
            'ind_estado_actividad'=>$params["estadoActividad"],
            'fk_a018_num_seg_usuaro_map'=>$this->atIdUsuario,
            'fec_modifica_ap'=>date("Y-m-d H:i:s"),
            'pk_num_actividad_planific'=>$params["idActividadPlanific"]
        ));
        $error3 = $sqlQuery_3->errorInfo();
        if($opcion=='DV') {
            if((!empty($error1[1]) && !empty($error1[2])) OR (!empty($error2[1]) && !empty($error2[2])) OR (!empty($error3[1]) && !empty($error3[2]))){
                $this->_db->rollBack(); return false;
            }
        }else{
            if((!empty($error2[1]) && !empty($error2[2])) OR (!empty($error3[1]) && !empty($error3[2]))){
                $this->_db->rollBack(); return false;
            }
        }
        $this->_db->commit(); return true;
    }
    /*************************************PROCESO DE ARCHIVOS ADJUNTOS*******************************************/
    public function metGuardaDatosArchivo($params){
        //print_r($params);exit;
        $this->_db->beginTransaction();
        $sqlQuery=$this->_db->prepare("
            INSERT INTO pf_c004_documento_actividad SET
                fk_pfc001_num_actividad_planific=:fk_pfc001_num_actividad_planific,
                fec_documento=:fec_documento,
                ind_nro_documento=:ind_nro_documento,
                ind_desc_documento=:ind_desc_documento,
                ind_ruta_documento=:ind_ruta_documento,
                fk_a018_num_seg_user_regdoc=:fk_a018_num_seg_user_regdoc,
                fec_registro_documento=:fec_registro_documento
            ");
        $sqlQuery->execute(array(
            'fk_pfc001_num_actividad_planific'=>$params["idActividadPlanif"],
            'fec_documento'=>$params["txt_fechaDoc"],
            'ind_nro_documento'=>$params["txt_nroDoc"]?$params["txt_nroDoc"]:null,
            'ind_desc_documento'=>$params["txt_nombreDoc"],
            'ind_ruta_documento'=>$params["rutaDoc"],
            'fk_a018_num_seg_user_regdoc'=>$this->atIdUsuario,
            'fec_registro_documento'=>date("Y-m-d H:i:s")
        ));
        $error = $sqlQuery->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack(); return false;
        }else{
            $this->_db->commit(); return true;
        }
    }
    /**
     * Extrae los registros de documentos adjuntos a una actividad de acuerdo al id actividad-planificación ó por el id del adjunto.
     * @param $case
     * @param $id_x
     * @return array
     */
    public function metBuscaArchivosAdjuntos($case,$id_x){
        $criterio="";
        switch($case){
            case 'por_idActividadPlanif':
                $criterio="WHERE fk_pfc001_num_actividad_planific=".$id_x." ORDER BY ind_desc_documento";
                break;
            case 'por_idDocumento':
                $criterio="WHERE pk_num_documento_actividad=".$id_x;
                break;
        }
        $sqlQuery = $this->_db->query(
            "SELECT pk_num_documento_actividad AS id_doc, fec_documento AS fecha_doc, ind_nro_documento AS nro_doc,
                  ind_desc_documento AS nombre_doc,ind_ruta_documento AS ruta_doc
              FROM pf_c004_documento_actividad $criterio"
        );
        $sqlQuery->setFetchMode(PDO::FETCH_ASSOC);
        return $sqlQuery->fetchAll();
    }
    /**
     * Elimina un archivo adjuntado a una actividad.
     * @param $idDecision
     * @return bool
     */
    public function metEliminaArchivosAdjuntos($idDocumento){
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare("DELETE FROM pf_c004_documento_actividad WHERE pk_num_documento_actividad=:pk_num_documento_actividad");
        $sql_query->execute(array('pk_num_documento_actividad'=>$idDocumento));
        $error=$sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack(); return false;
        }else{
            $this->_db->commit(); return true;
        }
    }
    /**********************************FALLOS SANCIONES DE PROCEDIMIENTOS ADMINISTRATIVOS**************************************/
    /**
     * Extrae los registros del personal a objeto de sanción
     * @param bool|false $filtrar
     * @return array
     */
    public function metListarResponsables($filtrar=false){
        $filtro='';
        if($filtrar!=false){
            $filtro="WHERE a003_persona.pk_num_persona not in($filtrar) ";
        }
        $sql="SELECT pk_num_persona AS idResponsable, ind_cedula_documento AS cedula, ind_documento_fiscal AS rif,
              CONCAT_WS(' ', ind_nombre1,ind_nombre2) AS nombres, CONCAT_WS(' ',ind_apellido1, ind_apellido2) AS apellidos,num_estatus AS estatus
              FROM a003_persona
              $filtro";

        $responsable = $this->_db->query($sql);
        $responsable->setFetchMode(PDO::FETCH_ASSOC);
        return $responsable->fetchAll();
    }
    /**
     * Se determina si la Determinación tiene ó nó decisiones registradas.
     * @param $idPlanificacion
     * @return mixed
     */
    public function metDecisionDeterminacion($idPlanificacion){
        $sql="SELECT * FROM pf_c006_decision WHERE fk_pfb001_num_planificacion=$idPlanificacion";
        $decision = $this->_db->query($sql);
        $decision->setFetchMode(PDO::FETCH_ASSOC);
        return $decision->fetch();
    }
    /**
     * Se extraen los tipos de decisiones de Procedimientos administrativos
     * @return array
     */
    public function metTipoDecisiones(){
        $result=$this->_db->query("
          SELECT pk_num_tipodecision AS idTipo_decision,ind_tipodecision AS nombre_decision,flag_num_campo AS cant_montos
          FROM pf_c007_tipo_decision ORDER BY ind_tipodecision");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Busca una sanción de una determinada persona en una Determinación en particular
     * @param $arrParams
     * @return mixed
     */
    public function metValidaSancion($arrParams){
        $sqlQuery=$this->_db->query("
            SELECT ind_tipodecision AS nombre_decision FROM pf_c006_decision
            INNER JOIN pf_c007_tipo_decision ON pf_c006_decision.fk_pfc007_num_tipodecision = pf_c007_tipo_decision.pk_num_tipodecision
            WHERE fk_pfb001_num_planificacion=".$arrParams["idPlanificacion"]." AND fk_a003_num_persona=".$arrParams["idPersona"]." AND fk_pfc007_num_tipodecision=".$arrParams["idTipo_decision"]
        );
        $sqlQuery->setFetchMode(PDO::FETCH_ASSOC);
        return $sqlQuery->fetch();
    }
    public function metIngresaSancion($arrParams){
        $fechaActual=date('Y-m-d H:i:s');
        $this->_db->beginTransaction();
        $sqlQuery=$this->_db->prepare("
            INSERT INTO pf_c006_decision SET
                fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion,
                fk_a003_num_persona=:fk_a003_num_persona,
                fk_pfc007_num_tipodecision=:fk_pfc007_num_tipodecision,
                monto=:monto,
                monto_reparo=:monto_reparo,
                fk_a018_num_seg_usuario_mod=:fk_a018_num_seg_usuario_mod,
                fec_ultima_modific_decision=:fec_ultima_modific_decision
         ");
        foreach($arrParams as $params){
            $sqlQuery->execute(array(
                'fk_pfb001_num_planificacion'=>$params["idPlanificacion"],
                'fk_a003_num_persona'=>$params["idPersona"],
                'fk_pfc007_num_tipodecision'=>$params["idTipo_decision"],
                'monto'=>$params["monto"],
                'monto_reparo'=>$params["montoReparo"],
                'fk_a018_num_seg_usuario_mod'=>$this->atIdUsuario,
                'fec_ultima_modific_decision'=>$fechaActual
            ));
        }
        $error = $sqlQuery->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack(); return false;
        }else{
            $this->_db->commit(); return true;
        }
    }
    /** md md-visibility
     * Elimina una sanción de una persona en una determinación de responsabilidad.
     * @param $idDecision
     * @return bool
     */
    public function metEliminaSancion($idDecision){
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare("DELETE FROM pf_c006_decision WHERE pk_num_decision=:pk_num_decision");
        $sql_query->execute(array('pk_num_decision'=>$idDecision));
        $error=$sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack(); return false;
        }else{
            $this->_db->commit(); return true;
        }
    }
    /**
     * Extrae las personas sancionadas de una Determinación de Responsabilidad.
     * @param $idPlanificacion
     * @return array
     */
    public function metPersonasSanciondas($idPlanificacion){
        $result=$this->_db->query("
            SELECT pk_num_decision AS id_decision, ind_tipodecision AS nombre_decision, ind_cedula_documento AS cedula_pers,CONCAT_WS(' ', ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2) AS nombre_persona,
              monto, monto_reparo, pf_c006_decision.fec_ultima_modific_decision AS fecha_decision,flag_num_campo AS cantmontos_decision
            FROM pf_c006_decision
              INNER JOIN pf_c007_tipo_decision ON pf_c006_decision.fk_pfc007_num_tipodecision = pf_c007_tipo_decision.pk_num_tipodecision
              INNER JOIN a003_persona ON pf_c006_decision.fk_a003_num_persona = a003_persona.pk_num_persona
            WHERE fk_pfb001_num_planificacion=".$idPlanificacion." ORDER BY ind_nombre1 ASC
        ");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
}
?>