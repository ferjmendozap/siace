<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Planificación Fiscal
 * PROCESO: Actuación Fiscal.
 * DESCRIPCIÓN: Lista, crea y actualiza las planificaciones.
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Alexis Ontiveros     | ontiveros.alexis@cmldc.gob.ve        | 0426-514.43.82
 * |_____________________________________________________________________________________
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        07-12-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/
class actuacionFiscalModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct(){
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }
    /**
     * Busca las planificaciones de acuerdo a los criterios de búsqueda
     * para llenar la grilla ppal. ó montarla el en form modal.
     * @param $params
     * @return array
     */
    public function metBuscarActuaciones($params){
        //print_r($params);exit;
        $sql_criterio="";$criterio=false;$Union="";
        $tabla="pf_b001_planificacion_fiscal tb_actuacion";
        $campos="tb_actuacion.*,fk_a039_num_ente";
        $Union=" INNER JOIN a041_persona_ente ON tb_actuacion.fk_a041_num_persona_ente = a041_persona_ente.pk_num_persona_ente";
        if(isset($params['idTipoProceso'])){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="tb_actuacion.fk_b002_num_proceso=".$params['idTipoProceso'];
        }
        if(isset($params['idPlanificacion']) AND $params['idPlanificacion']>0){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="tb_actuacion.pk_num_planificacion=".$params['idPlanificacion'];
        }else{
            if(isset($params['cod_planificacion']) AND $params['cod_planificacion']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_actuacion.cod_planificacion='".$params['cod_planificacion']."'";
            }
            if(isset($params['cbox_dependencia']) AND $params['cbox_dependencia']>0){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_actuacion.fk_a004_num_dependencia=".$params['cbox_dependencia'];
            }
            if(isset($params['cbox_centro_costo']) AND $params['cbox_centro_costo']>0){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_actuacion.fk_a023_num_centro_costo=".$params['cbox_centro_costo'];
            }
            if(isset($params['cbox_estadoplan']) AND $params['cbox_estadoplan']) {
                $sql_criterio .= ($criterio) ? " AND " : " WHERE ";$criterio = true;
                $sql_criterio .= "tb_actuacion.ind_estado_planificacion='" . $params['cbox_estadoplan'] . "'";
            }
            if(isset($params['txt_objetivo_plan']) AND $params['txt_objetivo_plan']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";
                $sql_criterio.="tb_actuacion.ind_objetivo LIKE('%".$params['txt_objetivo_plan']."%')";$criterio=true;
            }
            if(isset($params['cbox_ente']) AND $params['cbox_ente']>0){
                $Union.=" INNER JOIN a039_ente ON a041_persona_ente.fk_a039_num_ente = a039_ente.pk_num_ente";
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.=" pk_num_ente=".$params['cbox_ente'];
            }
            if(isset($params['txt_fechareg1']) AND $params['txt_fechareg1'] OR isset($params['txt_fechareg2']) AND $params['txt_fechareg2']){
                if(!$params['txt_fechareg1']){
                    $params['txt_fechareg1']=$params['txt_fechareg2'];
                }elseif(!$params['txt_fechareg2']){
                    $params['txt_fechareg2']=$params['txt_fechareg1'];
                }
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.=" CAST(tb_actuacion.fec_registro_planificacion AS DATE) BETWEEN '".$params['txt_fechareg1']."' AND '".$params['txt_fechareg2']."'";
            }else{
                if(isset($params['cbox_yearplan']) AND $params['cbox_yearplan']>0){
                    $sql_criterio.=($criterio)? " AND ": " WHERE ";
                    $sql_criterio.="YEAR(tb_actuacion.fec_registro_planificacion)='".$params['cbox_yearplan']."'";$criterio=true;
                }
            }
        }
        $sql_criterio.=" ORDER BY cod_planificacion ASC";
        $sql_query="SELECT $campos FROM $tabla$Union".$sql_criterio;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Busca los entes activos cuyo representantes esten activos para llenar la grilla de entes a objeto de control fiscal
     * @return array
     */
    public function metBuscaEntesGrilla(){
        $tabla="a041_persona_ente";
        $campos="pk_num_persona_ente,fk_a039_num_ente AS id_ente,num_ente_padre,ind_nombre_ente AS nombre_ente,num_estatus_titular AS estatus_titular,
        cod_detalle AS cod_situacion_titular ";
        $Union=" INNER JOIN a039_ente ON a041_persona_ente.fk_a039_num_ente = a039_ente.pk_num_ente
        INNER JOIN a006_miscelaneo_detalle ON a041_persona_ente.fk_a006_num_miscdetalle_situacion = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle";
        $sql_criterio=" WHERE ind_nombre_ente!='Particular' AND a039_ente.num_estatus=1 AND num_estatus_titular=1 ORDER BY num_ente_padre,pk_num_ente";
        $sql_query="SELECT $campos FROM $tabla$Union".$sql_criterio;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Busca auditores asignados a la planificación.
     * @param $params
     * @return array
     */
    public function metAuditoresDesignados($params){
        $tabla="pf_c002_auditor_planificacion"; $campos="*";$sql_criterio="";$criterio=false;
        $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="fk_pfb001_num_planificacion=".$params['idPlanificacion']; $criterio=true;
        if(isset($params['pk_num_empleado'])){
            $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="fk_rhb001_num_empleado = ".$params['pk_num_empleado'];$criterio=true;
        }
        if(isset($params['flag_coordinador'])){
            $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="num_flag_coordinador = ".$params['flag_coordinador']." AND num_estatus=1";
        }
        $sql_query="SELECT $campos FROM $tabla".$sql_criterio;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Permite la búsqueda de actividades relacionas a la planificación.
     * @param $params
     * @return array
     */
    public function metActividadesAsignadas($params){
        $tabla="pf_c001_actividad_planific"; $campos="*";$sql_criterio="";$criterio=false;
        if(isset($params['idPlanificacion'])) {
            $sql_criterio = ($criterio) ? " AND " : " WHERE ";$criterio = true;
            $sql_criterio .= "fk_pfb001_num_planificacion=" . $params['idPlanificacion'];
        }
        if(isset($params['ind_estado'])) {
            $sql_criterio .= ($criterio) ? " AND " : " WHERE ";
            $sql_criterio .= "ind_estado_actividad='".$params['ind_estado']."'";
        }
        if(isset($params['completa'])) {
            $campos = "pf_c001_actividad_planific.*,num_dias_duracion_actividad AS num_duracion_actividad,pk_num_actividad, txt_descripcion_actividad, ind_afecto_plan, pf_b003_fase.cod_fase,pf_b003_fase.txt_descripcion_fase";
            $Union = " INNER JOIN pf_b004_actividad ON pf_c001_actividad_planific.fk_pfb004_num_actividad=pf_b004_actividad.pk_num_actividad ";
            $Union .= " INNER JOIN pf_b003_fase ON pf_b004_actividad.fk_pfb003_num_fase=pf_b003_fase.pk_num_fase ";
            $sql_criterio.=" ORDER BY cod_fase, cod_actividad";
        }
        if($params['limite']) {
            $sql_criterio .= " LIMIT " . $params['limite'];
        }
        $sql_query="SELECT $campos FROM $tabla$Union".$sql_criterio;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**

     * Lista las actividades restantes disponibles para agregar a una planificación.
     */
    public function metListaActividadesDisp($params){
        //print_r($params); //$idDependencia
        $tabla="pf_c005_actividad_tipoactuac";$campos="*";$sql_criterio="";$criterio=false;
        $campos="pf_b004_actividad.*,pf_b003_fase.cod_fase,pf_b003_fase.txt_descripcion_fase";
        $Union=" INNER JOIN pf_b004_actividad ON pf_c005_actividad_tipoactuac.fk_pfb004_num_actividad=pf_b004_actividad.pk_num_actividad ";
        $Union.=" INNER JOIN pf_b003_fase ON pf_b004_actividad.fk_pfb003_num_fase=pf_b003_fase.pk_num_fase ";
        if(isset($params['idTipoProceso'])) {
            $sql_criterio = ($criterio) ? " AND " : " WHERE ";$criterio = true;
            $sql_criterio .= "fk_pfb002_num_proceso_tipo=" . $params['idTipoProceso'];
        }
        if(isset($params['idTipoActuacion'])) {
            $sql_criterio .= ($criterio) ? " AND " : " WHERE ";
            $sql_criterio .= "fk_a006_num_miscelaneodet=".$params['idTipoActuacion'];
        }
        $sql_criterio .= " AND pf_b004_actividad.num_estatus=1 ORDER BY cod_fase, cod_actividad";
        $sql_query="SELECT $campos FROM $tabla$Union".$sql_criterio; //echo $sql_query;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Ingresa los datos para el registro principal de la Planificación.
     */
    public function metIngresaPlanificacion($params){
        $fecha_actual = date('Y-m-d H:i:s');
        $arrValores=array(
            'fk_b002_num_proceso'=>$params['idTipoProceso'],
            'fk_a001_num_organismo'=>$params['id_contraloria'],
            'fk_a004_num_dependencia'=>$params['id_dependencia'],
            'fk_a041_num_persona_ente'=>$params['idPersonaEnte'],
            'fk_a006_num_miscdet_tipoactuacion'=>$params['idtipo_actuacion'],
            'fk_a006_num_miscdet_origenactuacion'=>$params['idorigen_actuacion'],
            'fk_a023_num_centro_costo'=>$params['pk_num_centro_costo'],
            'ind_objetivo'=>$params['ind_objetivo'],
            'ind_alcance'=>$params['ind_alcance'],
            'ind_observacion'=>$params['ind_observacion']?$params['ind_observacion']:null,
            'fk_rhb001_num_empleado_preparado'=>$params['idEmpleadoUser'],
            'fec_preparacion'=>$fecha_actual,
            'fec_inicio'=>$params['fec_inicio'],
            'fk_a018_num_seg_usuario_reg'=>$this->atIdUsuario,
            'fec_registro_planificacion'=>$fecha_actual,
            'fk_a018_num_seg_usuario_mod'=>$this->atIdUsuario,
            'fec_ultima_modific_planificacion' =>$fecha_actual
        );
        $this->_db->beginTransaction();
        $sql_query = $this->_db->prepare("
          INSERT INTO pf_b001_planificacion_fiscal SET
          fk_b002_num_proceso=:fk_b002_num_proceso,
          fk_a001_num_organismo=:fk_a001_num_organismo,
          fk_a004_num_dependencia=:fk_a004_num_dependencia,
          fk_a041_num_persona_ente=:fk_a041_num_persona_ente,
          fk_a006_num_miscdet_tipoactuacion=:fk_a006_num_miscdet_tipoactuacion,
          fk_a006_num_miscdet_origenactuacion=:fk_a006_num_miscdet_origenactuacion,
          fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
          ind_objetivo=:ind_objetivo,
          ind_alcance=:ind_alcance,
          ind_observacion=:ind_observacion,
          fk_rhb001_num_empleado_preparado=:fk_rhb001_num_empleado_preparado,
          fec_preparacion=:fec_preparacion,
          fec_inicio=:fec_inicio,
          fk_a018_num_seg_usuario_reg=:fk_a018_num_seg_usuario_reg,
          fec_registro_planificacion=:fec_registro_planificacion,
          fk_a018_num_seg_usuario_mod=:fk_a018_num_seg_usuario_mod,
          fec_ultima_modific_planificacion=:fec_ultima_modific_planificacion");
        $sql_query->execute($arrValores);
        $idPlanificacion = $this->_db->lastInsertId();
        $error = $sql_query->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack(); $retornar = false;
        } else {
            $this->_db->commit();$retornar = $idPlanificacion;
        }
        return $retornar;
    }
    /***
     * Actualiza sólo los datos principales de la Planificación.
     */
    public function metActualizaPlanificacion($params){
        $fecha_actual = date('Y-m-d H:i:s');
        $arrValores = array(
            'fk_a041_num_persona_ente'=>$params['idPersonaEnte'],
            'fk_a006_num_miscdet_tipoactuacion' => $params['idtipo_actuacion'],
            'fk_a006_num_miscdet_origenactuacion' => $params['idorigen_actuacion'],
            'fk_a023_num_centro_costo' => $params['pk_num_centro_costo'],
            'ind_objetivo' => $params['ind_objetivo'],
            'ind_alcance' => $params['ind_alcance'],
            'ind_observacion' => $params['ind_observacion'] ? $params['ind_observacion'] : null,
            'fk_rhb001_num_empleado_preparado' => $params['idEmpleadoUser'],
            'fec_preparacion' => $fecha_actual,
            'fec_inicio' => $params['fec_inicio'],
            'fk_a018_num_seg_usuario_mod' => $this->atIdUsuario,
            'fec_ultima_modific_planificacion' => $fecha_actual,
            'pk_num_planificacion' => $params['idPlanificacion'],
        );
        $this->_db->beginTransaction();
        $sql_query = $this->_db->prepare("
              UPDATE pf_b001_planificacion_fiscal SET
              fk_a041_num_persona_ente=:fk_a041_num_persona_ente,
              fk_a006_num_miscdet_tipoactuacion=:fk_a006_num_miscdet_tipoactuacion,
              fk_a006_num_miscdet_origenactuacion=:fk_a006_num_miscdet_origenactuacion,
              fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
              ind_objetivo=:ind_objetivo,
              ind_alcance=:ind_alcance,
              ind_observacion=:ind_observacion,
              fk_rhb001_num_empleado_preparado=:fk_rhb001_num_empleado_preparado,
              fec_preparacion=:fec_preparacion,
              fec_inicio=:fec_inicio,
              fk_a018_num_seg_usuario_mod=:fk_a018_num_seg_usuario_mod,
              fec_ultima_modific_planificacion=:fec_ultima_modific_planificacion
              WHERE pk_num_planificacion=:pk_num_planificacion");
        $sql_query->execute($arrValores);
        $error = $sql_query->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            $retornar = false;
        } else {
            $this->_db->commit();
            $retornar = true;
        }
        return $retornar;
    }
    /***
     * Actualiza los datos principales de la Planificación y elimina las actividades relacionadas cuando existe un cambio de tipo de actuación.
     */
    public function metActualizaPlanElimActividades($params){
        $fecha_actual = date('Y-m-d H:i:s');
        $arrValores = array(
            'fk_a041_num_persona_ente'=>$params['idPersonaEnte'],
            'fk_a006_num_miscdet_tipoactuacion' => $params['idtipo_actuacion'],
            'fk_a006_num_miscdet_origenactuacion' => $params['idorigen_actuacion'],
            'fk_a023_num_centro_costo' => $params['pk_num_centro_costo'],
            'ind_objetivo' => $params['ind_objetivo'],
            'ind_alcance' => $params['ind_alcance'],
            'ind_observacion' => $params['ind_observacion'] ? $params['ind_observacion'] : null,
            'fec_preparacion' => $fecha_actual,
            'fec_inicio' => $params['fec_inicio'],
            'fk_a018_num_seg_usuario_mod' => $this->atIdUsuario,
            'fec_ultima_modific_planificacion' => $fecha_actual,
            'pk_num_planificacion' => $params['idPlanificacion'],
        );
        $this->_db->beginTransaction();
        $sql_query1 = $this->_db->prepare("
              UPDATE pf_b001_planificacion_fiscal SET
              fk_a041_num_persona_ente=:fk_a041_num_persona_ente,
              fk_a006_num_miscdet_tipoactuacion=:fk_a006_num_miscdet_tipoactuacion,
              fk_a006_num_miscdet_origenactuacion=:fk_a006_num_miscdet_origenactuacion,
              fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
              ind_objetivo=:ind_objetivo,
              ind_alcance=:ind_alcance,
              ind_observacion=:ind_observacion,
              fec_preparacion=:fec_preparacion,
              fec_inicio=:fec_inicio,
              fk_a018_num_seg_usuario_mod=:fk_a018_num_seg_usuario_mod,
              fec_ultima_modific_planificacion=:fec_ultima_modific_planificacion
              WHERE pk_num_planificacion=:pk_num_planificacion
        ");
        $sql_query1->execute($arrValores);
        //Se elimina las actividades
        $sql_query2=$this->_db->prepare(
            "DELETE FROM pf_c001_actividad_planific WHERE fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion"
        );
        $sql_query2->execute(array('fk_pfb001_num_planificacion'=>$params['idPlanificacion']));
        $error = $sql_query1->errorInfo();
        $error2 = $sql_query2->errorInfo();
        if (!empty($error[1]) && !empty($error[2]) OR !empty($error2[1]) && !empty($error2[2])) {
            $this->_db->rollBack(); $retornar = false;
        } else {
            $this->_db->commit(); $retornar = true;
        }
        return $retornar;
    }
    /***
     * Actualiza los datos principales de la Planificación y actualiza las actividades relacionadas cuando existe una cambio de fecha de planificación.
     */
    public function metActualizaPlanYActividades($params){
        $fecha_actual = date('Y-m-d H:i:s');
        $arrValores = array(
            'fk_a041_num_persona_ente'=>$params['idPersonaEnte'],
            'fk_a006_num_miscdet_tipoactuacion' => $params['idtipo_actuacion'],
            'fk_a006_num_miscdet_origenactuacion' => $params['idorigen_actuacion'],
            'fk_a023_num_centro_costo' => $params['pk_num_centro_costo'],
            'ind_objetivo' => $params['ind_objetivo'],
            'ind_alcance' => $params['ind_alcance'],
            'ind_observacion' => $params['ind_observacion'] ? $params['ind_observacion'] : null,
            'fk_rhb001_num_empleado_preparado' => $params['idEmpleadoUser'],
            'fec_preparacion' => $fecha_actual,
            'fec_inicio' => $params['fec_inicio'],
            'fk_a018_num_seg_usuario_mod' => $this->atIdUsuario,
            'fec_ultima_modific_planificacion' => $fecha_actual,
            'pk_num_planificacion' => $params['idPlanificacion'],
        );
        $this->_db->beginTransaction();
        $sql_query1 = $this->_db->prepare("
              UPDATE pf_b001_planificacion_fiscal SET
              fk_a041_num_persona_ente=:fk_a041_num_persona_ente,
              fk_a006_num_miscdet_tipoactuacion=:fk_a006_num_miscdet_tipoactuacion,
              fk_a006_num_miscdet_origenactuacion=:fk_a006_num_miscdet_origenactuacion,
              fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
              ind_objetivo=:ind_objetivo,
              ind_alcance=:ind_alcance,
              ind_observacion=:ind_observacion,
              fk_rhb001_num_empleado_preparado=:fk_rhb001_num_empleado_preparado,
              fec_preparacion=:fec_preparacion,
              fec_inicio=:fec_inicio,
              fk_a018_num_seg_usuario_mod=:fk_a018_num_seg_usuario_mod,
              fec_ultima_modific_planificacion=:fec_ultima_modific_planificacion
              WHERE pk_num_planificacion=:pk_num_planificacion");
        $sql_query1->execute($arrValores);
        $error = $sql_query1->errorInfo();
        //Se actualiza las actividades del plan
        $secuencia=1;
        $sql_query2 = $this->_db->prepare("
            UPDATE pf_c001_actividad_planific SET
            num_secuencia_actividad=:num_secuencia_actividad,
            fec_inicio_actividad=:fec_inicio_actividad,
            fec_inicio_real_actividad=:fec_inicio_real_actividad,
            fec_culmina_actividad=:fec_culmina_actividad,
            fec_culmina_real_actividad=:fec_culmina_real_actividad,
            num_dias_duracion_actividad=:num_dias_duracion_actividad,
            fk_a018_num_seg_usuaro_map=:fk_a018_num_seg_usuaro_map,
            fec_modifica_ap=:fec_modifica_ap
            WHERE fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion AND fk_pfb004_num_actividad=:fk_pfb004_num_actividad");
        foreach($params['hdd_dataActividad'] as $cad_actividad) {
            $arrLapsos = explode('N', $cad_actividad);
            $id_actividad=$arrLapsos[0];
            $arrValores=array(
                'num_secuencia_actividad'=>$secuencia,
                'fec_inicio_actividad'=>$arrLapsos[2],
                'fec_inicio_real_actividad'=>$arrLapsos[4],
                'fec_culmina_actividad'=>$arrLapsos[3],
                'fec_culmina_real_actividad'=>$arrLapsos[5],
                'num_dias_duracion_actividad'=>$arrLapsos[1],
                'fk_a018_num_seg_usuaro_map'=>$this->atIdUsuario,
                'fec_modifica_ap'=>$fecha_actual,
                'fk_pfb001_num_planificacion'=>$params['idPlanificacion'],
                'fk_pfb004_num_actividad'=>$id_actividad
            );
            $sql_query2->execute($arrValores);
            $secuencia++;
        }
        $error2 = $sql_query2->errorInfo();
        if (!empty($error[1]) && !empty($error[2]) OR !empty($error2[1]) && !empty($error2[2])) {
            $this->_db->rollBack(); $retornar = false;
        } else {
            $this->_db->commit(); $retornar = true;
        }
        return $retornar;
    }
    /**
     * Asigna un auditor de la planificación.
     * @param $params
     * @return array
     */
    public function metIngresaAuditor($params){
        $fecha_actual=date('Y-m-d H:i:s');
        $arrValores=array(
            'fk_pfb001_num_planificacion'=>$params['idPlanificacion'],
            'fk_rhb001_num_empleado'=>$params['pk_num_empleado'],
            'num_flag_coordinador'=>$params['flagCoord'],
            'fec_coordinador'=>$params['flagCoord']==1?date('Y-m-d'):NULL,
            'fk_a018_num_seg_user_regauditor'=>$this->atIdUsuario,
            'fec_registro_auditor'=>$fecha_actual,
            'fec_estatus'=>date('Y-m-d')
        );
        $this->_db->beginTransaction();
        $sql_query = $this->_db->prepare("
              INSERT INTO pf_c002_auditor_planificacion SET
              fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion,
              fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
              num_flag_coordinador=:num_flag_coordinador,
              fec_coordinador=:fec_coordinador,
              fk_a018_num_seg_user_regauditor=:fk_a018_num_seg_user_regauditor,
              fec_registro_auditor=:fec_registro_auditor,
              fec_estatus=:fec_estatus");
        $sql_query->execute($arrValores);
        $id_x = $this->_db->lastInsertId();
        $error = $sql_query->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            $retornar = array("idDesignado" =>"", "Error"=>"errorSql");
        } else {
            $this->_db->commit();
            $retornar = array("idDesignado"=>$id_x,"Error"=>"");
        }
        return $retornar;
    }

    /**
     * Quita un responsable de la planificación actual.
     * @param $IdResponsable
     * @return bool
     */
    public function metQuitarResponsable($IdResponsable,$idPlanificacion){
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare(
            "DELETE FROM pf_c002_auditor_planificacion WHERE fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion AND fk_rhb001_num_empleado=:fk_rhb001_num_empleado"
        );
        $sql_query->execute(array('fk_pfb001_num_planificacion'=>$idPlanificacion,'fk_rhb001_num_empleado'=>$IdResponsable));
        $error=$sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$retornar=false;
        }else{
            $this->_db->commit();$retornar=true;
        }
        return $retornar;
    }

    /**
     * Activa ó Inactiva un responsable de la planificación actual siempre y cuando la mísma esté aprobada.
     * @param $IdResponsable
     * @return bool
     */
    public function metEstatusResponsable($IdResponsable,$idPlanificacion,$estatus) {
        $this->_db->beginTransaction();
        $arrValores=array(
            'fk_a018_num_seg_user_regauditor'=>$this->atIdUsuario,
            'num_estatus'=>$estatus,
            'fec_estatus'=>date('Y-m-d'),
            'fk_pfb001_num_planificacion'=>$idPlanificacion,
            'fk_rhb001_num_empleado'=>$IdResponsable
        );
        $sql_query=$this->_db->prepare("UPDATE pf_c002_auditor_planificacion SET
              fk_a018_num_seg_user_regauditor=:fk_a018_num_seg_user_regauditor,
              num_estatus=:num_estatus,
              fec_estatus=:fec_estatus
              WHERE fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion AND fk_rhb001_num_empleado=:fk_rhb001_num_empleado");
        $sql_query->execute($arrValores);
        $error=$sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$retornar=false;
        }else{
            $this->_db->commit();$retornar=true;
        }
        return $retornar;
    }
    /**
     * Activa a un responsable como coordinador y inactiva el anterior
     * @param $IdResponsable
     * @param $idPlanificacion
     * @return bool
     */
    public function metSeleccionaCoordinador($IdResponsable,$idPlanificacion) {
        $this->_db->beginTransaction();
        $arrValores=array(
            'num_flag_coordinador'=>0,
            'fk_pfb001_num_planificacion'=>$idPlanificacion
        );
        $sql_query1=$this->_db->prepare("UPDATE pf_c002_auditor_planificacion SET
              num_flag_coordinador=:num_flag_coordinador
              WHERE fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion");
        $sql_query1->execute($arrValores);
        $error1=$sql_query1->errorInfo();

        $arrValores=array(
            'num_flag_coordinador'=>1,
            'fec_coordinador'=>date('Y-m-d'),
            'fk_pfb001_num_planificacion'=>$idPlanificacion,
            'fk_rhb001_num_empleado'=>$IdResponsable
        );
        $sql_query2=$this->_db->prepare("UPDATE pf_c002_auditor_planificacion SET
              num_flag_coordinador=:num_flag_coordinador,
              fec_coordinador=:fec_coordinador
              WHERE fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion AND fk_rhb001_num_empleado=:fk_rhb001_num_empleado");
        $sql_query2->execute($arrValores);
        $error2=$sql_query2->errorInfo();
        if(!empty($error1[1]) && !empty($error1[2]) OR !empty($error2[1]) && !empty($error2[2]) ){
            $this->_db->rollBack();$retornar=false;
        }else{
            $this->_db->commit();$retornar=true;
        }
        return $retornar;
    }

    /**
     * Ingresa una o un set de actividades a la planificación actual.
     * @param $params
     * @return array
     */
    public function metIngresaActividades($params){
        $fecha_actual=date('Y-m-d H:i:s');$secuencia=1;
        $this->_db->beginTransaction();
        $sql_query = $this->_db->prepare("
            INSERT INTO pf_c001_actividad_planific SET
            fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion,
            fk_pfb004_num_actividad=:fk_pfb004_num_actividad,
            num_secuencia_actividad=:num_secuencia_actividad,
            fec_inicio_actividad=:fec_inicio_actividad,
            fec_inicio_real_actividad=:fec_inicio_real_actividad,
            fec_culmina_actividad=:fec_culmina_actividad,
            fec_culmina_real_actividad=:fec_culmina_real_actividad,
            num_dias_duracion_actividad=:num_dias_duracion_actividad,
            num_dias_prorroga_actividad=:num_dias_prorroga_actividad,
            fk_a018_num_seg_usuario_rap=:fk_a018_num_seg_usuario_rap,
            fec_registro_ap=:fec_registro_ap,
            fk_a018_num_seg_usuaro_map=:fk_a018_num_seg_usuaro_map,
            fec_modifica_ap=:fec_modifica_ap");
        foreach($params['hdd_dataActividad'] AS $cad_actividad){
            $arrLapsos = explode('N', $cad_actividad);
            $id_actividad=$arrLapsos[0];
            $arrValores=array(
                'fk_pfb001_num_planificacion'=>$params['idPlanificacion'],
                'fk_pfb004_num_actividad'=>$id_actividad,
                'num_secuencia_actividad'=>$secuencia,
                'fec_inicio_actividad'=>$arrLapsos[2],
                'fec_inicio_real_actividad'=>$arrLapsos[4],
                'fec_culmina_actividad'=>$arrLapsos[3],
                'fec_culmina_real_actividad'=>$arrLapsos[5],
                'num_dias_duracion_actividad'=>$arrLapsos[1],
                'num_dias_prorroga_actividad'=>0,
                'fk_a018_num_seg_usuario_rap'=>$this->atIdUsuario,
                'fec_registro_ap'=>$fecha_actual,
                'fk_a018_num_seg_usuaro_map'=>$this->atIdUsuario,
                'fec_modifica_ap'=>$fecha_actual,
            );
            $sql_query->execute($arrValores);
            $idActividad = $this->_db->lastInsertId();
            $secuencia++;
        }
        $error = $sql_query->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            $retornar = array("result"=>"");
        } else {
            $this->_db->commit();
            $retornar = array("result"=>true,"idActividad_planific"=>$idActividad);
        }
        return $retornar;
    }
    /**
     * Actualiza un set de actividades de la planificación actual.
     * @param $params
     * @return bool
     */
    public function metActualizaActividades($params){
        $fecha_actual=date('Y-m-d H:i:s'); $secuencia=1;
        $this->_db->beginTransaction();
        $sql_query = $this->_db->prepare("
            UPDATE pf_c001_actividad_planific SET
            num_secuencia_actividad=:num_secuencia_actividad,
            fec_inicio_actividad=:fec_inicio_actividad,
            fec_inicio_real_actividad=:fec_inicio_real_actividad,
            fec_culmina_actividad=:fec_culmina_actividad,
            fec_culmina_real_actividad=:fec_culmina_real_actividad,
            num_dias_duracion_actividad=:num_dias_duracion_actividad,
            fk_a018_num_seg_usuaro_map=:fk_a018_num_seg_usuaro_map,
            fec_modifica_ap=:fec_modifica_ap
            WHERE fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion AND fk_pfb004_num_actividad=:fk_pfb004_num_actividad");
        foreach($params['hdd_dataActividad'] as $cad_actividad) {
            $arrLapsos = explode('N', $cad_actividad);
            $id_actividad=$arrLapsos[0];
            $arrValores=array(
                'num_secuencia_actividad'=>$secuencia,
                'fec_inicio_actividad'=>$arrLapsos[2],
                'fec_inicio_real_actividad'=>$arrLapsos[4],
                'fec_culmina_actividad'=>$arrLapsos[3],
                'fec_culmina_real_actividad'=>$arrLapsos[5],
                'num_dias_duracion_actividad'=>$arrLapsos[1],
                'fk_a018_num_seg_usuaro_map'=>$this->atIdUsuario,
                'fec_modifica_ap'=>$fecha_actual,
                'fk_pfb001_num_planificacion'=>$params['idPlanificacion'],
                'fk_pfb004_num_actividad'=>$id_actividad
            );
            $sql_query->execute($arrValores);
            $secuencia++;
        }
        $error = $sql_query->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();$retornar = false;
        } else {
            $retornar=$this->_db->commit(); //$retornar = true;
        }
        return $retornar;
    }
    /**
     * Elimina el registro de asignación de una actividad de la planificación actual por id de la actividad.
     */
    public function metQuitaActividad($params)
    {
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare(
            "DELETE FROM pf_c001_actividad_planific
             WHERE fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion AND fk_pfb004_num_actividad=:fk_pfb004_num_actividad"
        );
        $sql_query->execute(array(
                'fk_pfb001_num_planificacion'=>$params['idPlanificacion'],
                'fk_pfb004_num_actividad'=>$params['idActividad'])
        );
        $error=$sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$retornar=false;
        }else{
            $this->_db->commit(); $retornar=true;
        }
        return $retornar;
    }
    /**
     * Anula una Planificación.
     */
    public function metAnularPlanificacion($params){
        $fecha_actual = date('Y-m-d H:i:s'); $fechaNota=date('Y-m-d');
        $arrValores = array(
            'ind_estado_planificacion' => 'AN',
            'ind_nota_justificacion' => $params['hd_notanulacion'],
            'fec_nota_justificacion' => $fechaNota,
            'fk_rhb001_num_empleado_notajust' => $params['idEmpleadoUser'],
            'fk_a018_num_seg_usuario_mod' => $this->atIdUsuario,
            'fec_ultima_modific_planificacion' => $fecha_actual,
            'pk_num_planificacion' => $params['idPlanificacion'],
        );
        $this->_db->beginTransaction();
        $sql_query = $this->_db->prepare("
            UPDATE pf_b001_planificacion_fiscal SET
            ind_estado_planificacion=:ind_estado_planificacion,
            ind_nota_justificacion=:ind_nota_justificacion,
            fec_nota_justificacion=:fec_nota_justificacion,
            fk_rhb001_num_empleado_notajust=:fk_rhb001_num_empleado_notajust,
            fk_a018_num_seg_usuario_mod=:fk_a018_num_seg_usuario_mod,
            fec_ultima_modific_planificacion=:fec_ultima_modific_planificacion
            WHERE pk_num_planificacion=:pk_num_planificacion");
        $sql_query->execute($arrValores);
        $error = $sql_query->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack(); $retornar = false;
        } else {
            $this->_db->commit(); $retornar = true;
        }
        return $retornar;
    }
    /**
     * Cierra una Planificación.
     */
    public function metCerrarPlanificacion($params){
        $fecha_actual = date('Y-m-d H:i:s'); $fechaNota=date('Y-m-d');
        $arrValores = array(
            'ind_estado_planificacion' => 'CE',
            'ind_nota_justificacion' => $params['hd_notanulacion'],
            'fec_nota_justificacion' => $fechaNota,
            'fk_rhb001_num_empleado_notajust' => $params['idEmpleadoUser'],
            'fk_a018_num_seg_usuario_mod' => $this->atIdUsuario,
            'fec_ultima_modific_planificacion' => $fecha_actual,
            'pk_num_planificacion' => $params['idPlanificacion'],
        );
        $this->_db->beginTransaction();
        $sql_query = $this->_db->prepare("
            UPDATE pf_b001_planificacion_fiscal SET
            ind_estado_planificacion=:ind_estado_planificacion,
            ind_nota_justificacion=:ind_nota_justificacion,
            fec_nota_justificacion=:fec_nota_justificacion,
            fk_rhb001_num_empleado_notajust=:fk_rhb001_num_empleado_notajust,
            fk_a018_num_seg_usuario_mod=:fk_a018_num_seg_usuario_mod,
            fec_ultima_modific_planificacion=:fec_ultima_modific_planificacion
            WHERE pk_num_planificacion=:pk_num_planificacion");
        $sql_query->execute($arrValores);
        $error = $sql_query->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack(); $retornar = false;
        } else {
            $this->_db->commit(); $retornar = true;
        }
        return $retornar;
    }
}