<?php
/***********************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Planificación Fiscal
 * PROCESO: Reportes de Planficaciones.
 * DESCRIPCIÓN: Ejecuta la creación, modificación, revisión, aprobación y anulación de prórrogas de la actividad en
 *              ejecución de planificaciones aprobadas en los diferentes proceso fiscales: Actuación fiscal,
 *              Valoración preliminar, Potestad investigativa, Valoración jurídica y Procedimientos administrativos
 * PROGRAMADORES.______________________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Alexis Ontiveros     | ontiveros.alexis@cmldc.gob.ve        | 0426-514.43.82
 * |___________________________________________________________________________________________________________________
 *
 * VERSION
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        10-09-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 ***********************************************************************************************************************/
class procesaProrrogaModelo extends Modelo{
    private $atIdUsuario;
    public function __construct(){
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }
    /**
     * Consulta de planificaciones con o sin prórrogas
     * @param $params
     * @return array
     */
    public function metConsultaPlanificacion($params){
        $tabla="pf_b001_planificacion_fiscal tb_planificacion";$sql_criterio="";$criterio=false;
        $Union=" INNER JOIN a041_persona_ente ON tb_planificacion.fk_a041_num_persona_ente = a041_persona_ente.pk_num_persona_ente";
        $Union.=" INNER JOIN a006_miscelaneo_detalle tb_tipoactuacion ON tb_planificacion.fk_a006_num_miscdet_tipoactuacion = tb_tipoactuacion.pk_num_miscelaneo_detalle";
        $Union.=" INNER JOIN a006_miscelaneo_detalle tb_origenactuacion ON tb_planificacion.fk_a006_num_miscdet_origenactuacion = tb_origenactuacion.pk_num_miscelaneo_detalle";
        if($params['idTipoProceso']){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="tb_planificacion.fk_b002_num_proceso=".$params['idTipoProceso'];
        }
        if($params['txt_codPlanificacion']){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="tb_planificacion.cod_planificacion LIKE '%".$params['txt_codPlanificacion']."%'";
        }
        if($params['id_contraloria']){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="tb_planificacion.fk_a001_num_organismo='".$params['id_contraloria']."'";
        }
        if($params['idDependencia']){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="tb_planificacion.fk_a004_num_dependencia=".$params['idDependencia'];
        }
        if($params['cbox_tipoActuacion']){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="tb_planificacion.fk_a006_num_miscdet_tipoactuacion=".$params['cbox_tipoActuacion'];
        }
        if($params['cbox_origenActuacion']){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="tb_planificacion.fk_a006_num_miscdet_origenactuacion=".$params['cbox_origenActuacion'];
        }
        if($params['cbox_ente']){
            $Union.=" INNER JOIN a039_ente ON a041_persona_ente.fk_a039_num_ente = a039_ente.pk_num_ente";
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="pk_num_ente=".$params['cbox_ente'];
        }
        if($params['cbox_buscarPor']=="PL") {//Búsqueda por planificaciones
            $Union.=" INNER JOIN pf_c001_actividad_planific ON tb_planificacion.pk_num_planificacion = pf_c001_actividad_planific.fk_pfb001_num_planificacion";
            $sql_criterio .= ($criterio) ? " AND " : " WHERE ";$criterio = true;
            if($params['cbox_estadosplan']){
                $sql_criterio .= "tb_planificacion.ind_estado_planificacion='".$params['cbox_estadosplan']."'";
            }else{
                $sql_criterio .= "(tb_planificacion.ind_estado_planificacion='AP' OR tb_planificacion.ind_estado_planificacion='TE')";
            }
            if($params['txt_fechainic1'] AND $params['txt_fechainic2']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";
                $sql_criterio.=" CAST(tb_planificacion.fec_inicio AS DATE) BETWEEN '".$params['txt_fechainic1']."' AND '".$params['txt_fechainic2']."'";
            }else{
                if($params['cbox_yearplan']){
                    $sql_criterio.=($criterio)? " AND ": " WHERE ";
                    $sql_criterio.="YEAR(tb_planificacion.fec_inicio)='".$params['cbox_yearplan']."'";
                }
            }
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="ind_estado_actividad='EJ' ";
            $sql_criterio.="AND (NOT EXISTS(SELECT 1 FROM pf_c003_prorroga_actividad WHERE fk_pfc001_num_actividad_planific=pk_num_actividad_planific ";
            $sql_criterio.="AND pf_c003_prorroga_actividad.ind_estado!='AN'))";

            $campos="DISTINCT(tb_planificacion.pk_num_planificacion), tb_planificacion.cod_planificacion, tb_planificacion.fk_a004_num_dependencia,
            tb_planificacion.fk_a041_num_persona_ente,tb_planificacion.ind_objetivo,tb_planificacion.ind_alcance,tb_planificacion.ind_estado_planificacion,
            tb_planificacion.fec_inicio, fk_a039_num_ente, tb_tipoactuacion.ind_nombre_detalle AS nombre_tipoactuacion,
            tb_origenactuacion.ind_nombre_detalle AS nombre_origenactuacion";
            $sql_criterio.=" ORDER BY cod_planificacion ASC";

        }else{//Búsqueda de planificaciones con prórrogas

            $Union.=" INNER JOIN pf_c001_actividad_planific ON tb_planificacion.pk_num_planificacion = pf_c001_actividad_planific.fk_pfb001_num_planificacion";
            $Union.=" INNER JOIN pf_b004_actividad ON pf_c001_actividad_planific.fk_pfb004_num_actividad = pf_b004_actividad.pk_num_actividad";
            $Union.=" INNER JOIN pf_c003_prorroga_actividad ON pf_c001_actividad_planific.pk_num_actividad_planific = pf_c003_prorroga_actividad.fk_pfc001_num_actividad_planific";
            $sql_criterio .= ($criterio) ? " AND " : " WHERE ";$criterio = true;
            $sql_criterio .= "tb_planificacion.ind_estado_planificacion!='PR'";
            if($params['cbox_estadosProrroga']){
                $sql_criterio .= ($criterio) ? " AND " : " WHERE ";$criterio = true;
                if($params['opcionMenu']=='PR') {
                    if ($params['cbox_estadosProrroga'] == 'RV') {
                        $sql_criterio .= "(pf_c003_prorroga_actividad.ind_estado='" . $params['cbox_estadosProrroga'] . "' OR fk_rhb001_num_empleado_revispor IS NOT NULL)";
                    }else{
                        $sql_criterio .= "pf_c003_prorroga_actividad.ind_estado='" . $params['cbox_estadosProrroga'] . "'";
                    }
                }elseif($params['opcionMenu']=='RV') {
                    if ($params['cbox_estadosProrroga'] == 'RV') {
                        $sql_criterio .= "(pf_c003_prorroga_actividad.ind_estado='" . $params['cbox_estadosProrroga'] . "' OR fk_rhb001_num_empleado_revispor IS NOT NULL)";
                    } else {
                        $sql_criterio .= "pf_c003_prorroga_actividad.ind_estado='" . $params['cbox_estadosProrroga'] . "'";
                    }
                }elseif($params['opcionMenu']=='AP') {
                    $sql_criterio .= "pf_c003_prorroga_actividad.ind_estado='" . $params['cbox_estadosProrroga'] . "'";
                }
            }else{
                if($params['opcionMenu']=='RV') {
                    if ($params['cbox_estadosProrroga'] == 'RV') {
                        $sql_criterio .= ($criterio) ? " AND " : " WHERE ";$criterio = true;
                        $sql_criterio .= "(pf_c003_prorroga_actividad.ind_estado='" . $params['cbox_estadosProrroga'] . "' OR fk_rhb001_num_empleado_revispor IS NOT NULL)";
                    }
                }elseif($params['opcionMenu']=='AP') {
                    $sql_criterio .= ($criterio) ? " AND " : " WHERE ";$criterio = true;
                    $sql_criterio .= "pf_c003_prorroga_actividad.ind_estado='RV' OR tb_planificacion.fk_b002_num_proceso=".$params['idTipoProceso']."
                    AND (pf_c003_prorroga_actividad.ind_estado='AP' OR fk_rhb001_num_empleado_revispor IS NOT NULL
                    OR pf_c003_prorroga_actividad.ind_estado='AN')";
                }
            }
            if($params['txt_fechainic1'] AND $params['txt_fechainic2']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";
                $sql_criterio.=" CAST(fec_actualiza_prorroga AS DATE) BETWEEN '".$params['txt_fechainic1']."' AND '".$params['txt_fechainic2']."'";
            }else{
                if($params['cbox_yearplan']){
                    $sql_criterio.=($criterio)? " AND ": " WHERE ";
                    $sql_criterio.="YEAR(fec_actualiza_prorroga)='".$params['cbox_yearplan']."'";
                }
            }
            $campos="DISTINCT(tb_planificacion.pk_num_planificacion), tb_planificacion.cod_planificacion, tb_planificacion.ind_estado_planificacion,
            pk_num_prorroga_actividad,cod_prorroga,txt_descripcion_actividad,ind_motivo,pf_c003_prorroga_actividad.num_dias_prorroga,fk_rhb001_num_empleado_revispor,
            fec_registro_prorroga,ind_estado";
            $sql_criterio.=" ORDER BY cod_prorroga ASC";
        }
        $sql_query="SELECT $campos FROM $tabla$Union".$sql_criterio; //echo $sql_query; exit;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Extrae la última actividad en ejecución ó terminada de la planificación.
     * @param $idPlanificacion
     * @return array
     */
    public function metActividadEjTe($idPlanificacion){
        $result=$this->_db->query("
            SELECT pk_num_actividad_planific AS id_actividaPlanific,fk_pfb004_num_actividad AS id_actividad,
              ind_estado_actividad AS estado_actividad,num_secuencia_actividad AS secuencia_actividad
            FROM pf_c001_actividad_planific
            WHERE fk_pfb001_num_planificacion=".$idPlanificacion." AND ind_estado_actividad!='PE' ORDER BY num_secuencia_actividad DESC LIMIT 1");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetch();
    }
    /**
     * Extrae una prórroga de la actividad de acuerdo a su id
     * @param $idProrroga
     * @return array
     */
    public function metProrrogaActividad($idProrroga){
        $result=$this->_db->query("SELECT * FROM pf_c003_prorroga_actividad WHERE pk_num_prorroga_actividad=".$idProrroga);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetch();
    }
    /**
     * Permite la búsqueda de actividades relacionas a la planificación.
     * @param $params
     * @return array
     */
    public function metActividadesAsignadas($params){
        $tabla="pf_c001_actividad_planific"; $campos="*";$sql_criterio="";$criterio=false;
        if(isset($params['idPlanificacion'])) {
            $sql_criterio = ($criterio) ? " AND " : " WHERE ";$criterio = true;
            $sql_criterio .= "fk_pfb001_num_planificacion=" . $params['idPlanificacion'];
        }
        $sql_criterio .= ($criterio) ? " AND " : " WHERE ";$criterio = true;
        $sql_criterio .= "pk_num_actividad_planific IN (SELECT tb_activplan.pk_num_actividad_planific
                          FROM pf_c001_actividad_planific tb_activplan
                          LEFT JOIN pf_c003_prorroga_actividad tb_prorrogas ON tb_activplan.pk_num_actividad_planific=tb_prorrogas.fk_pfc001_num_actividad_planific
                          ORDER BY tb_prorrogas.pk_num_prorroga_actividad)";
        if(isset($params['ind_estado'])) {
            $sql_criterio .= ($criterio) ? " AND " : " WHERE ";
            $sql_criterio .= "ind_estado_actividad='".$params['ind_estado']."'";
        }
        if($params['completa']) {
            $campos = "pf_c001_actividad_planific.*, txt_descripcion_actividad,ind_afecto_plan, pf_b003_fase.cod_fase,pf_b003_fase.txt_descripcion_fase";
        }else{
            $campos = "pk_num_actividad_planific,fk_pfb001_num_planificacion,fk_pfb004_num_actividad,num_secuencia_actividad,fec_inicio_real_actividad,
            fec_culmina_real_actividad,num_dias_duracion_actividad,num_dias_prorroga_actividad";
        }
        $Union = " INNER JOIN pf_b004_actividad ON pf_c001_actividad_planific.fk_pfb004_num_actividad=pf_b004_actividad.pk_num_actividad ";
        $Union .= " INNER JOIN pf_b003_fase ON pf_b004_actividad.fk_pfb003_num_fase=pf_b003_fase.pk_num_fase ";
        $sql_criterio.=" ORDER BY cod_fase, cod_actividad, num_secuencia_actividad";

        $sql_query="SELECT $campos FROM $tabla$Union".$sql_criterio; //echo $sql_query;exit;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Permite la búsqueda de una prórroga por la id de la mísma ó la última de acuerdo a la id de una actividad.
     * @param $caso
     * @param $id_x
     * @return mixed
     */
    public function metBuscaProrroga($caso,$id_x){
        $sql_criterio="";$criterio=false;
        switch($caso){
            case 'por_idProrroga':
                $sql_criterio .= ($criterio) ? " AND " : " WHERE ";
                $sql_criterio .= "pk_num_prorroga_actividad=$id_x";
            break;
            case 'por_idActividadPlanif':
                $sql_criterio .= ($criterio) ? " AND " : " WHERE ";
                $sql_criterio .= "fk_pfc001_num_actividad_planific=$id_x ORDER BY pk_num_prorroga_actividad DESC";
            break;
        }
        $Query="SELECT pk_num_prorroga_actividad AS id_prorroga,num_dias_prorroga,ind_estado AS estado_prorroga
            FROM pf_c003_prorroga_actividad".$sql_criterio;
        $result=$this->_db->query($Query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetch();
    }
    /**
     *Genera el correlativo (sólo el número) de una nueva prórroga, el mísmo se determina por la cantidad de prórrogas
     *encontradas de la planificación a cuya actividad se le está creando la prórroga.
     * @param $idPlanificacion
     * @return mixed
     */
    public function metCorrelativoProrroga($idPlanificacion){
        $result=$this->_db->query("
            SELECT COUNT(*) AS nroProrrogaActual
            FROM pf_c003_prorroga_actividad
            INNER JOIN pf_c001_actividad_planific ON pf_c003_prorroga_actividad.fk_pfc001_num_actividad_planific=pf_c001_actividad_planific.pk_num_actividad_planific
            WHERE fk_pfb001_num_planificacion=".$idPlanificacion." AND cod_prorroga IS NOT NULL
        ");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetch();
    }
    /**
     * Registra una prórroga
     * @param $arrParams
     * @return bool|int
     */
    public function metIngresaProrroga($arrParams){
        $this->_db->beginTransaction();
        $sqlQuery=$this->_db->prepare("INSERT INTO pf_c003_prorroga_actividad SET
            fk_pfc001_num_actividad_planific=:fk_pfc001_num_actividad_planific,
            cod_prorroga=:cod_prorroga,
            num_dias_prorroga=:num_dias_prorroga,
            ind_motivo=:ind_motivo,
            fk_rhb001_num_empleado_prepapor=:fk_rhb001_num_empleado_prepapor,
            fec_prorroga_preparado=:fec_prorroga_preparado,
            fk_a018_num_seg_user_regpro=:fk_a018_num_seg_user_regpro,
            fec_registro_prorroga=:fec_registro_prorroga,
            fk_a018_num_seg_user_actpro=:fk_a018_num_seg_user_actpro,
            fec_actualiza_prorroga=:fec_actualiza_prorroga
        ");
        $sqlQuery->execute(array(
            'fk_pfc001_num_actividad_planific'=>$arrParams['idActividadPlanif'],
            'cod_prorroga'=>$arrParams['codProrroga'],
            'num_dias_prorroga'=>$arrParams['cantDiasProrroga'],
            'ind_motivo'=>$arrParams['txt_motivoProrroga'],
            'fk_rhb001_num_empleado_prepapor'=>$arrParams['atIdEmpleadoUser'],
            'fec_prorroga_preparado'=>$arrParams['fecha_actual'],
            'fk_a018_num_seg_user_regpro'=>$this->atIdUsuario,
            'fec_registro_prorroga'=>$arrParams['fecha_actual'],
            'fk_a018_num_seg_user_actpro'=>$this->atIdUsuario,
            'fec_actualiza_prorroga'=>$arrParams['fecha_actual']
        ));
        $idProrroga=(int)$this->_db->lastInsertId();
        $error = $sqlQuery->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack(); $retornar=false;
        } else {
            $this->_db->commit(); $retornar=$idProrroga;
        }
        return $retornar;
    }
    /**
     * Actualiza la prórroga
     * @param $arrParams
     * @return bool|string
     */
    public function metActualizaProrroga($arrParams){
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare("
            UPDATE pf_c003_prorroga_actividad SET
                num_dias_prorroga=:num_dias_prorroga,
                ind_motivo=:ind_motivo,
                fec_prorroga_preparado=:fec_prorroga_preparado,
                fk_a018_num_seg_user_actpro=:fk_a018_num_seg_user_actpro,
                fec_actualiza_prorroga=:fec_actualiza_prorroga
              WHERE pk_num_prorroga_actividad=:pk_num_prorroga_actividad
        ");
        $sql_query->execute(array(
            'num_dias_prorroga'=>$arrParams["cantDiasProrroga"],
            'ind_motivo'=>$arrParams["txt_motivoProrroga"],
            'fec_prorroga_preparado'=>$arrParams["fecha_actual"],
            'fk_a018_num_seg_user_actpro'=>$this->atIdUsuario,
            'fec_actualiza_prorroga'=>$arrParams["fecha_actual"],
            'pk_num_prorroga_actividad'=>$arrParams["idProrroga"]
        ));
        $error = $sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$reg_afectado="";
        }else{
            $reg_afectado=$this->_db->commit();
        }
        return $reg_afectado;
    }
    /**
     * Procesa la anulación una prórroga
     * @param $arrParams
     * @return bool|string
     */
    public function metAnulaProrroga($arrParams){
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare("
            UPDATE pf_c003_prorroga_actividad SET
                ind_estado=:ind_estado,
                fk_a018_num_seg_user_actpro=:fk_a018_num_seg_user_actpro,
                fec_actualiza_prorroga=:fec_actualiza_prorroga
              WHERE pk_num_prorroga_actividad=:pk_num_prorroga_actividad
        ");
        $sql_query->execute(array(
            'ind_estado'=>'AN',
            'fk_a018_num_seg_user_actpro'=>$this->atIdUsuario,
            'fec_actualiza_prorroga'=>$arrParams["fecha_actual"],
            'pk_num_prorroga_actividad'=>$arrParams["idProrroga"]
        ));
        $error = $sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$reg_afectado="";
        }else{
            $reg_afectado=$this->_db->commit();
        }
        return $reg_afectado;
    }
    /**
     * Procesa la revisión de la prórroga
     * @param $arrParams
     * @return bool|string
     */
    public function metProcesaRevision($arrParams){
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare("
            UPDATE pf_c003_prorroga_actividad SET
                ind_estado=:ind_estado,
                fk_rhb001_num_empleado_revispor=:fk_rhb001_num_empleado_revispor,
                fec_prorroga_revisado=:fec_prorroga_revisado,
                fk_a018_num_seg_user_actpro=:fk_a018_num_seg_user_actpro,
                fec_actualiza_prorroga=:fec_actualiza_prorroga
              WHERE pk_num_prorroga_actividad=:pk_num_prorroga_actividad
        ");
        $sql_query->execute(array(
            'ind_estado'=>'RV',
            'fk_rhb001_num_empleado_revispor'=>$arrParams["atIdEmpleadoUser"],
            'fec_prorroga_revisado'=>$arrParams["fecha_actual"],
            'fk_a018_num_seg_user_actpro'=>$this->atIdUsuario,
            'fec_actualiza_prorroga'=>$arrParams["fecha_actual"],
            'pk_num_prorroga_actividad'=>$arrParams["idProrroga"]
        ));
        $error = $sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$reg_afectado="";
        }else{
            $reg_afectado=$this->_db->commit();
        }
        return $reg_afectado;
    }
    //Procesa la actualización de las actividades y la aprobación de la prórroga
    public function metProcesaAprobacion($arrParams){
        $arrActividades=$arrParams['setActividades'];
        $this->_db->beginTransaction();
        //Se actualiza las actividades del plan
        $sql_query = $this->_db->prepare("
            UPDATE pf_c001_actividad_planific SET
                fec_inicio_real_actividad=:fec_inicio_real_actividad,
                fec_culmina_real_actividad=:fec_culmina_real_actividad,
                num_dias_prorroga_actividad=:num_dias_prorroga_actividad,
                fk_a018_num_seg_usuaro_map=:fk_a018_num_seg_usuaro_map,
                fec_modifica_ap=:fec_modifica_ap
            WHERE pk_num_actividad_planific=:pk_num_actividad_planific"
        );
        foreach($arrActividades as $fila) {
            $arrValores=array(
                'fec_inicio_real_actividad'=>$fila['fecha_inicialReal'],
                'fec_culmina_real_actividad'=>$fila['fecha_fin_actividadReal'],
                'num_dias_prorroga_actividad'=>$fila['cantDiasProrroga'],
                'fk_a018_num_seg_usuaro_map'=>$this->atIdUsuario,
                'fec_modifica_ap'=>$arrParams['fecha_actual'],
                'pk_num_actividad_planific'=>$fila['idActividaPlanif']
            );
            $sql_query->execute($arrValores);
        }
        $error = $sql_query->errorInfo();
        //Se aprueba la prórroga
        $sql_query2=$this->_db->prepare("
            UPDATE pf_c003_prorroga_actividad SET
                ind_estado=:ind_estado,
                fk_rhb001_num_empleado_aprobpor=:fk_rhb001_num_empleado_aprobpor,
                fec_prorroga_aprobado=:fec_prorroga_aprobado,
                fk_a018_num_seg_user_actpro=:fk_a018_num_seg_user_actpro,
                fec_actualiza_prorroga=:fec_actualiza_prorroga
              WHERE pk_num_prorroga_actividad=:pk_num_prorroga_actividad
        ");
        $sql_query2->execute(array(
            'ind_estado'=>'AP',
            'fk_rhb001_num_empleado_aprobpor'=>$arrParams["atIdEmpleadoUser"],
            'fec_prorroga_aprobado'=>$arrParams["fecha_actual"],
            'fk_a018_num_seg_user_actpro'=>$this->atIdUsuario,
            'fec_actualiza_prorroga'=>$arrParams["fecha_actual"],
            'pk_num_prorroga_actividad'=>$arrParams["idProrroga"]
        ));
        $error2 = $sql_query->errorInfo();
        if((!empty($error[1]) && !empty($error[2])) OR (!empty($error2[1]) && !empty($error2[2]))){
            $this->_db->rollBack();$reg_afectado="";
        }else{
            $reg_afectado=$this->_db->commit();
        }
        return $reg_afectado;
    }
}
?>