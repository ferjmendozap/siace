<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: PLANIFICACIÓN FISCAL
 * PROCESO: INGRESO Y MANTENIMIENTO DE TIPOS PROCESO FISCAL
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 3 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |         0426-5144382           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #3                      |        31-07-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once 'funcionesGenerales.php';
class procesosFiscalModelo extends Modelo
{
    private $atFuncionesGnrles;
    public function __construct(){
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atFuncionesGrles = new funcionesGenerales();
    }
    /**
     * Busca un proceso para modificar
     * @param $idProceso
     * @return mixed
     */
    public function metMostrarTipoProceso($idProceso){
        $tipoProceso = $this->_db->query("
          SELECT pf_b002.*, a018.ind_usuario FROM pf_b002_proceso pf_b002
          INNER JOIN a018_seguridad_usuario a018 ON pf_b002.fk_a018_num_seguridad_usuario = a018.pk_num_seguridad_usuario
          WHERE pk_num_proceso='$idProceso'
        ");
        $tipoProceso->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoProceso->fetch();
    }
    /**
     * Busca los procesos para listarlos en la grilla
     * @return array
     */
    public function metListarTipoProceso(){
        $tipoProceso = $this->_db->query("SELECT * FROM pf_b002_proceso");
        $tipoProceso->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoProceso->fetchAll();
    }
    /**
     * Valida la entrada de datos
     * @param $case
     * @param string $str_d
     * @return array
     */
    function metConsultaProceso($case, $str_d=''){
        $sql_criterio = ""; $tabla="pf_b002_proceso";$campos="*";
        switch($case){
            case "valida_ingreso":
                $sql_criterio = " WHERE txt_descripcion_proceso='".$str_d."'";
                break;
            case "valida_modificar":
                $sql_criterio = " WHERE pk_num_proceso!=".$str_d['idProceso']." AND txt_descripcion_proceso='".$str_d['txt_descripcion_proceso']."'";
                break;
        }
        $sql_query ="SELECT $campos FROM $tabla".$sql_criterio;
        $result = $this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Ingresa un nuevo registro
     * @param array $params
     * @return array
     */
    public function metCrearTipoProceso($params){
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare("
          INSERT INTO pf_b002_proceso SET
            cod_proceso=:cod_proceso,
            txt_descripcion_proceso=:txt_descripcion_proceso,
            num_estatus=:num_estatus,
            fec_ultima_modificacion=NOW(),
            fk_a018_num_seguridad_usuario='$this->atIdUsuario'
        ");
        $sql_query->execute(array(
            'cod_proceso'=>$params["cod_proceso"],
            'txt_descripcion_proceso'=>$params["txt_descripcion_proceso"],
            'num_estatus'=>$params["num_estatus"]
        ));
        $idRegistro=$this->_db->lastInsertId();
        $error = $sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$idRegistro=0;
        }else{
            $this->_db->commit();
        }
        return $idRegistro;

    }
    /**
     * Actualiza un registro
     * @param array $params
     * @return array
     */
    public function metModificarTipoPorceso($params){
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare("
            UPDATE
                pf_b002_proceso
              SET txt_descripcion_proceso=:txt_descripcion_proceso,
                num_estatus=:num_estatus,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              WHERE pk_num_proceso=".$params["idProceso"]);
        $sql_query->execute(array(
            'txt_descripcion_proceso'=>$params["txt_descripcion_proceso"],
            'num_estatus'=>$params["num_estatus"]
        ));
        $error = $sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$reg_afectado=0;
        }else{
            $reg_afectado=$this->_db->commit();
        }
        return $reg_afectado;
    }
    /**
     * Elimina un registro siempre y cuando no esté relacionado.
     * @param $idProceso
     * @return array|bool
     */
    public function metEliminaProceso($idProceso){
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare("DELETE FROM pf_b002_proceso WHERE pk_num_proceso=:pk_num_proceso");
        $sql_query->execute(array('pk_num_proceso'=>$idProceso));
        $error=$sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack(); $reg_afectado=$error;
        }else{
            $reg_afectado=$this->_db->commit();
        }
        return $reg_afectado;
    }
}
?>