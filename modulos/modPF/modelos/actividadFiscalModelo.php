<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: PLANIFICACIÓN FISCAL
 * PROCESO: REGISTRO Y MANTENIMIENTO DE ACTIVIDADES
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 3 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |         0426-5144382           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #3                      |        12-08-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once 'funcionesGenerales.php';
class actividadFiscalModelo extends Modelo{
    private $atFuncionesGnrles;
    public function __construct(){
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atFuncionesGrles = new funcionesGenerales();
    }
    /**
     * Busca una actividad en particulara para su actualización.
     * @param $idActividad
     * @return mixed
     */
    public function metMostrarActividades($idActividad){
        $sql_Query = $this->_db->query("SELECT tbactividad.*, tbfase.fk_pfb002_num_proceso, a018.ind_usuario FROM pf_b004_actividad tbactividad
            INNER JOIN pf_b003_fase tbfase ON tbactividad.fk_pfb003_num_fase = tbfase.pk_num_fase
            INNER JOIN pf_b002_proceso tbproceso ON tbfase.fk_pfb002_num_proceso = tbproceso.pk_num_proceso
            INNER JOIN a018_seguridad_usuario a018 ON tbactividad.fk_a018_num_seguridad_usuario = a018.pk_num_seguridad_usuario
            WHERE pk_num_actividad='$idActividad'
        ");
        $sql_Query->setFetchMode(PDO::FETCH_ASSOC);
        return $sql_Query->fetch();
    }
    /**
     * Busca las actividades de acuerdo a los parámetros pasados para llenar la grilla.
     * @param $params
     * @return array
     */
    public function metListarActividades($params){
        $sql_criterio="";$criterio=false;$Union="";
        $tabla="pf_b004_actividad";$campos="pf_b004_actividad.*,pf_b003_fase.txt_descripcion_fase,pf_b002_proceso.txt_descripcion_proceso";
        $Union=" INNER JOIN pf_b003_fase ON pf_b004_actividad.fk_pfb003_num_fase = pf_b003_fase.pk_num_fase ";
        $Union.="INNER JOIN pf_b002_proceso ON pf_b003_fase.fk_pfb002_num_proceso = pf_b002_proceso.pk_num_proceso ";
        if(isset($params['idProceso']) AND $params['idProceso']>0){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="pk_num_proceso=".$params['idProceso'];
        }
        if(isset($params['idFase']) AND $params['idFase']>0){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";
            $sql_criterio.="pk_num_fase=".$params['idFase'];
        }
        $sql_criterio.=" AND pf_b003_fase.num_estatus=1 ORDER BY cod_proceso,cod_fase,cod_actividad ASC";
        $sql_query="SELECT $campos FROM $tabla$Union".$sql_criterio;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Busca los procesos para llenar los combobox respectivo.
     * @return array
     */
    public function metListarTipoProceso(){
        $tipoProceso = $this->_db->query("SELECT pk_num_proceso,txt_descripcion_proceso FROM pf_b002_proceso WHERE num_estatus='1'");
        $tipoProceso->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoProceso->fetchAll();
    }
    /**
     * Busca las fases de acuerdo a un proceso para llenar el combox su combobox respectivo
     * @param $pkNumProceso
     * @return array
     */
    public function metBuscarFases($pkNumProceso){
        $sql_query =  $this->_db->query(
            "SELECT pk_num_fase, txt_descripcion_fase FROM pf_b003_fase WHERE fk_pfb002_num_proceso = '$pkNumProceso' AND num_estatus='1' "
        );
        $sql_query->setFetchMode(PDO::FETCH_ASSOC);
        return $sql_query->fetchAll();
    }
    /**
     * Valida la actividad antes de ingresar y actualizar
     * @param $case
     * @param string $str_d
     * @return array
     */
    function metValidaActividad($case, $str_d=''){
        $sql_criterio = ""; $tabla="pf_b004_actividad";$campos="*";
        switch($case){
            case "valida_ingreso":
                $sql_criterio=" WHERE fk_pfb003_num_fase=".$str_d['pk_num_fase']." AND txt_descripcion_actividad='".$str_d['txt_descripcion_actividad']."'";
                break;
            case "valida_modificar":
                $sql_criterio=" WHERE pk_num_actividad != ".$str_d['idActividad']." AND fk_pfb003_num_fase=".$str_d['pk_num_fase']." AND txt_descripcion_actividad='".$str_d['txt_descripcion_actividad']."'";
                break;
        }
        $sql_query ="SELECT $campos FROM $tabla".$sql_criterio;
        $result = $this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Ingresa un registro
     * @param $params
     * @return int|string
     */
    public function metIngresaActividad($params){
        $valores = array(
            'fk_pfb003_num_fase' => $params['pk_num_fase'],
            'cod_actividad' => $params['cod_actvidad'],
            'txt_descripcion_actividad' => $params['txt_descripcion_actividad'],
            'txt_comentarios_actividad' => $params['txt_comentarios_actividad'],
            'num_duracion_actividad' => $params['num_duracion_actividad'],
            'ind_afecto_plan' => $params['ind_afecto_plan'],
            'num_estatus' => $params['num_estatus']
        );
        $this->_db->beginTransaction();
        $sql_query = $this->_db->prepare("
              INSERT INTO pf_b004_actividad SET
              fk_pfb003_num_fase=:fk_pfb003_num_fase,
              cod_actividad=:cod_actividad,
              txt_descripcion_actividad=:txt_descripcion_actividad,
              txt_comentarios_actividad=:txt_comentarios_actividad,
              num_duracion_actividad=:num_duracion_actividad,
              ind_afecto_plan=:ind_afecto_plan,
              num_estatus=:num_estatus,
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            ");
        $sql_query->execute($valores);
        $idRegistro = $this->_db->lastInsertId();
        $error = $sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$idRegistro=0;
        }else{
            $this->_db->commit();
        }
        return $idRegistro;
    }
    /**
     * Modifica un registro.
     * @param $params
     * @return bool|int
     */
    public function metModificarActividad($params){
        $valores = array(
            'txt_descripcion_actividad' => $params['txt_descripcion_actividad'],
            'txt_comentarios_actividad' => $params['txt_comentarios_actividad'],
            'num_duracion_actividad' => $params['num_duracion_actividad'],
            'ind_afecto_plan' => $params['ind_afecto_plan'],
            'num_estatus' => $params['num_estatus']
        );
        $this->_db->beginTransaction();
        $sql_query = $this->_db->prepare("
            UPDATE pf_b004_actividad SET
                txt_descripcion_actividad=:txt_descripcion_actividad,
                txt_comentarios_actividad=:txt_comentarios_actividad,
                num_duracion_actividad=:num_duracion_actividad,
                ind_afecto_plan=:ind_afecto_plan,
                num_estatus=:num_estatus,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            WHERE pk_num_actividad='".$params["idActividad"]."'
        ");
        $sql_query->execute($valores);
        $error = $sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$reg_afectado=0;
        }else{
            $reg_afectado=$this->_db->commit();
        }
        return $reg_afectado;
    }
    /**
     * Elimina una actividad siempre y cuando no esté relacionada
     * @param $idActividad
     * @return array|bool
     */
    public function metEliminarActividad($idActividad) {
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare("DELETE FROM pf_b004_actividad WHERE pk_num_actividad=:pk_num_actividad");
        $sql_query->execute(array('pk_num_actividad'=>$idActividad));
        $error=$sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack(); $reg_afectado=$error;
        }else{
            $reg_afectado=$this->_db->commit();
        }
        return $reg_afectado;
    }
}
?>