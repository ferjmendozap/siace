<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Planificación Fiscal
 * PROCESO: Ingreso y mantenmimiento de Misceláneos de Planificación fiscal
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          José Díaz                        |        diaz.jose@cmldc.gob.ve      |         0416-2124302           |
 * | 2 |          Alexis Ontiveros                 |     ontiveros.alexis@cmldc.gob.ve  |         0426-514.43.82         |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        29-07-2015       |         1.0        |
 * |               #2                      |        04-06-2017       |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class miscelaneoModelo extends Modelo{
    private $atIdUsuario;
    public function __construct(){
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }
    /**
     * Extrae sólo los misceláneos maestros de planificación fiscal
     * @return array
     */
    public function metListarMiscelaneos(){
        $miscelaneo = $this->_db->query("
            SELECT pk_num_miscelaneo_maestro,cod_maestro,ind_nombre_maestro,a005.ind_descripcion, a005.num_estatus
            FROM a005_miscelaneo_maestro AS a005
            INNER JOIN a015_seguridad_aplicacion AS a015 ON a005.fk_a015_num_seguridad_aplicacion=a015.pk_num_seguridad_aplicacion
            WHERE a015.cod_aplicacion='PF'
        ");
        $miscelaneo->setFetchMode(PDO::FETCH_ASSOC);
        return $miscelaneo->fetchAll();
    }

    /**
     * Extrae el mísceláneo maestro
     * @param $idMiscelaneo
     * @return mixed
     */
    public function metMostrarMiscelaneo($idMiscelaneo){
        $miscelaneo = $this->_db->query("
            SELECT * FROM a005_miscelaneo_maestro WHERE pk_num_miscelaneo_maestro='$idMiscelaneo'
        ");
        $miscelaneo->setFetchMode(PDO::FETCH_ASSOC);

        return $miscelaneo->fetch();
    }

    /**
     * Extrae los misceláneos detalles
     * @param bool|false $codMaestro
     * @param bool|false $idMiscelaneo
     * @return array|mixed
     */
    public function metMostrarMiscDetalle($codMaestro = false, $idMiscelaneo = false){
        if ($codMaestro) {
            $where = "a005.cod_maestro='$codMaestro'";
        } elseif ($idMiscelaneo) {
            $where = "a006.fk_a005_num_miscelaneo_maestro='$idMiscelaneo'";
        }
        $miscelaneoDetalle = $this->_db->query("
            SELECT
              a006.pk_num_miscelaneo_detalle,
              a006.ind_nombre_detalle,
              a006.cod_detalle,
              a006.num_estatus
            FROM
              a006_miscelaneo_detalle AS a006
            INNER JOIN
              a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
            WHERE $where

        ");
        $miscelaneoDetalle->setFetchMode(PDO::FETCH_ASSOC);

        if ($codMaestro) {
            return $miscelaneoDetalle->fetchAll();
        } elseif ($idMiscelaneo) {
            return $miscelaneoDetalle->fetch();
        }
    }

    /**
     * Valida si existe el código de un misceláneo maestro.
     * @param $txt_codMiscMaestro
     * @return mixed
     */
    public function metValidaMiscMaestro($txt_codMiscMaestro){
        $miscelaneo = $this->_db->query("SELECT cod_maestro FROM a005_miscelaneo_maestro WHERE cod_maestro='".$txt_codMiscMaestro."'");
        $miscelaneo->setFetchMode(PDO::FETCH_ASSOC);
        return $miscelaneo->fetch();
    }

    /**
     * Ingresa un misceláneo maestro y sus misceláneos detalle
     * @param $arrParams
     * @return array|string
     */
    public function metCrearMiscelaneo($arrParams){
        $arrCodsDetalle=$arrParams['codsDetalle'];
        $arrNombresDetalle=$arrParams['nombresDetalle'];
        $arrChksEstatusDet=$arrParams['chksEstatusDet'];
        $this->_db->beginTransaction();
        //Se ingresa le misceláneo maestro
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO a005_miscelaneo_maestro SET
                ind_nombre_maestro=:ind_nombre_maestro,
                ind_descripcion=:ind_descripcion,
                num_estatus=:num_estatus,
                fk_a015_num_seguridad_aplicacion=:fk_a015_num_seguridad_aplicacion,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                cod_maestro=:cod_maestro,
                fec_ultima_modificacion=:fec_ultima_modificacion
            ");
        $NuevoRegistro->execute(array(
            ':ind_nombre_maestro' => $arrParams['txt_nombreMiscMaestro'],
            ':ind_descripcion' => $arrParams['txt_DescMiscMaestro'],
            ':num_estatus' => $arrParams['chk_estatusMaestro'],
            ':fk_a015_num_seguridad_aplicacion' => $arrParams['atIdAplicacion'],
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
            ':cod_maestro' => $arrParams['txt_codMiscMaestro'],
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s')
        ));
        $idMiscMaestro = $this->_db->lastInsertId();
        $error = $NuevoRegistro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            //Se ingresa los misceláneos detalle
            $nuevoDetalle = $this->_db->prepare(
                "INSERT INTO a006_miscelaneo_detalle SET
                cod_detalle=:cod_detalle,
                ind_nombre_detalle=:ind_nombre_detalle,
                num_estatus=:num_estatus,
                fk_a005_num_miscelaneo_maestro=:fk_a005_num_miscelaneo_maestro
            ");
            for ($i = 0; $i < count($arrCodsDetalle); $i++) {
                if (is_array($arrChksEstatusDet)) {
                    if (isset($arrChksEstatusDet[$i])) {
                        $estatusDet = $arrChksEstatusDet[$i];
                    } else {
                        $estatusDet = 0;
                    }
                } else {
                    $estatusDet = 0;
                }
                $nuevoDetalle->execute(array(
                    'cod_detalle' => $arrCodsDetalle[$i],
                    'ind_nombre_detalle' => $arrNombresDetalle[$i],
                    'num_estatus' => $estatusDet,
                    'fk_a005_num_miscelaneo_maestro'=>$idMiscMaestro
                ));
                $error1 = $nuevoDetalle->errorInfo();
                if (!empty($error1[1]) && !empty($error1[2])) {
                    $this->_db->rollBack();
                    return $error;
                }
            }
        }
        $this->_db->commit();
        return $idMiscMaestro;
    }

    /**
     * Actualiza el misceláneo maestro y sus detalles y/o ingresa nuevos detalles
     * @param $arrParams
     * @return bool
     */
    public function metActualizarMiscelaneo($arrParams){
        $arrCodsDetalle=$arrParams['codsDetalle'];
        $arrNombresDetalle=$arrParams['nombresDetalle'];
        $arrChksEstatusDet=$arrParams['chksEstatusDet'];
        $arrPknumMiscDet=$arrParams['set_pknumMiscDetalle'];
        $this->_db->beginTransaction();
        // Actualiza el misceláneo maestro.
        $ActualizaMaestro = $this->_db->prepare(
            "UPDATE a005_miscelaneo_maestro SET
                ind_nombre_maestro=:ind_nombre_maestro,
                ind_descripcion=:ind_descripcion,
                num_estatus=:num_estatus,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=:fec_ultima_modificacion
              WHERE
                pk_num_miscelaneo_maestro=:pk_num_miscelaneo_maestro
            ");
        $ActualizaMaestro->execute(array(
            ':ind_nombre_maestro' => $arrParams['txt_nombreMiscMaestro'],
            ':ind_descripcion' => $arrParams['txt_DescMiscMaestro'],
            ':num_estatus' => $arrParams['chk_estatusMaestro'],
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
            ':pk_num_miscelaneo_maestro' => $arrParams['idMiscelaneo']
        ));
        $error1 = $ActualizaMaestro->errorInfo();
        //Actualiza los misceláneo detalles
        for ($i = 0; $i < count($arrCodsDetalle); $i++) {
            if (isset($arrPknumMiscDet[$i])) {//Actualiza el detalle
                if (isset($arrChksEstatusDet[$i])) {
                    $estatusDet = $arrChksEstatusDet[$i];
                } else {
                    $estatusDet = 0;
                }
                $actualizaDetalle = $this->_db->prepare("
                        UPDATE a006_miscelaneo_detalle SET
                            cod_detalle=:cod_detalle,
                            ind_nombre_detalle=:ind_nombre_detalle,
                            num_estatus=:num_estatus
                        WHERE
                          pk_num_miscelaneo_detalle=:pk_num_miscelaneo_detalle
                    ");
                $actualizaDetalle->execute(array(
                    'cod_detalle' => strtoupper($arrCodsDetalle[$i]),
                    'ind_nombre_detalle' => $arrNombresDetalle[$i],
                    'num_estatus' => $estatusDet,
                    'pk_num_miscelaneo_detalle'=>$arrPknumMiscDet[$i]
                ));
            } else {//Ingresa el detalle
                if (isset($arrChksEstatusDet[$i])) {
                    $estatusDet = $arrChksEstatusDet[$i];
                } else {
                    $estatusDet = 0;
                }
                $actualizaDetalle = $this->_db->prepare("
                        INSERT INTO a006_miscelaneo_detalle SET
                            cod_detalle=:cod_detalle,
                            ind_nombre_detalle=:ind_nombre_detalle,
                            num_estatus=:num_estatus,
                            fk_a005_num_miscelaneo_maestro=:fk_a005_num_miscelaneo_maestro
                    ");
                $actualizaDetalle->execute(array(
                    'cod_detalle' => strtoupper($arrCodsDetalle[$i]),
                    'ind_nombre_detalle' => $arrNombresDetalle[$i],
                    'num_estatus' => $estatusDet,
                    'fk_a005_num_miscelaneo_maestro'=>$arrParams['idMiscelaneo']
                ));
            }
        }
        $error2 = $actualizaDetalle->errorInfo();
        if ((!empty($error1[1]) && !empty($error1[2])) OR (!empty($error2[1]) && !empty($error2[2]))) {
            $this->_db->rollBack();
            return false;
        } else {
            $this->_db->commit();
            return true;
        }
    }
}