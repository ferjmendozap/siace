<?php
/***********************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Planificación Fiscal
 * PROCESO: Reportes de Planficaciones.
 * DESCRIPCIÓN: Permite la extración de datos para la consulta y reportes en pdf de las planificaciones en sus
 *              diferentes proceso fiscales: Actuación fiscal, Valoración preliminar, Potestad investigativa,
 *              Valoración jurídica y Procedimientos administrativos.
 * PROGRAMADORES.______________________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Alexis Ontiveros     | ontiveros.alexis@cmldc.gob.ve        | 0426-514.43.82
 * |___________________________________________________________________________________________________________________
 *
 * VERSION
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        31-07-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 ***********************************************************************************************************************/
class rptPlanificacionesModelo extends Modelo{
    public function metConsultaPlanificacion($params){
        $tabla="pf_b001_planificacion_fiscal tb_planificacion";$sql_criterio="";$criterio=false;
        $campos="tb_planificacion.*,fk_a039_num_ente, tb_planifpadre.cod_planificacion AS cod_planifpadre, tb_planifpadre.fk_b002_num_proceso AS num_procesopadre,
        tb_tipoactuacion.ind_nombre_detalle AS nombre_tipoactuacion, tb_origenactuacion.ind_nombre_detalle AS nombre_origenactuacion";
        $Union=" INNER JOIN a041_persona_ente ON tb_planificacion.fk_a041_num_persona_ente = a041_persona_ente.pk_num_persona_ente";
        $Union.=" LEFT JOIN pf_b001_planificacion_fiscal tb_planifpadre ON tb_planificacion.num_planificacion_padre = tb_planifpadre.pk_num_planificacion";
        $Union.=" INNER JOIN a006_miscelaneo_detalle tb_tipoactuacion ON tb_planificacion.fk_a006_num_miscdet_tipoactuacion = tb_tipoactuacion.pk_num_miscelaneo_detalle";
        $Union.=" INNER JOIN a006_miscelaneo_detalle tb_origenactuacion ON tb_planificacion.fk_a006_num_miscdet_origenactuacion = tb_origenactuacion.pk_num_miscelaneo_detalle";
        if($params['idTipoProceso']){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="tb_planificacion.fk_b002_num_proceso=".$params['idTipoProceso'];
        }
        if($params['idPlanificacion']){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="tb_planificacion.pk_num_planificacion=".$params['idPlanificacion'];
        }else{
            if($params['cbox_codPlanificacion']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_planificacion.cod_planificacion='".$params['cbox_codPlanificacion']."'";
            }
            if($params['id_contraloria']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_planificacion.fk_a001_num_organismo='".$params['id_contraloria']."'";
            }
            if($params['id_dependencia']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_planificacion.fk_a004_num_dependencia=".$params['id_dependencia'];
            }
            if($params['cbox_tipoActuacion']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_planificacion.fk_a006_num_miscdet_tipoactuacion=".$params['cbox_tipoActuacion'];
            }
            if($params['cbox_origenActuacion']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_planificacion.fk_a006_num_miscdet_origenactuacion=".$params['cbox_origenActuacion'];
            }
            if($params['cbox_estadosplan']) {
                $sql_criterio .= ($criterio) ? " AND " : " WHERE ";$criterio = true;
                $sql_criterio .= "tb_planificacion.ind_estado_planificacion='".$params['cbox_estadosplan']."'";
            }
            if($params['cbox_ente']){
                $Union.=" INNER JOIN a039_ente ON a041_persona_ente.fk_a039_num_ente = a039_ente.pk_num_ente";
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="pk_num_ente=".$params['cbox_ente'];
            }
            if($params['txt_fechainic1'] AND $params['txt_fechainic2']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";
                $sql_criterio.=" CAST(tb_planificacion.fec_inicio AS DATE) BETWEEN '".$params['txt_fechainic1']."' AND '".$params['txt_fechainic2']."'";
            }else{
                if($params['cbox_yearplan']){
                    $sql_criterio.=($criterio)? " AND ": " WHERE ";
                    $sql_criterio.="YEAR(tb_planificacion.fec_inicio)='".$params['cbox_yearplan']."'";
                }
            }
        }
        $sql_criterio.=" ORDER BY cod_planificacion ASC";
        $sql_query="SELECT $campos FROM $tabla$Union".$sql_criterio; //echo $sql_query; exit;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Busca auditores asignados a la planificación.
     * @param $idPlanificacion
     * @return array
     */
    public function metAuditoresDesignados($idPlanificacion){
        $tabla="pf_c002_auditor_planificacion"; $campos="*";$sql_criterio="";$criterio=false;
        $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="fk_pfb001_num_planificacion=".$idPlanificacion; $criterio=true;
        $sql_query="SELECT $campos FROM $tabla".$sql_criterio;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Extrea las actividades asignadas de una planificación
     * @param $idPlanificacion
     * @return mixed
     *
     */
    public function metActividadesAsignadas($idPlanificacion){
        $tabla="pf_c001_actividad_planific"; $criterio=false;
        $sql_criterio = ($criterio) ? " AND " : " WHERE ";$criterio = true;
        $sql_criterio .= "fk_pfb001_num_planificacion=" . $idPlanificacion;
        $campos = "fec_inicio_actividad, fec_culmina_real_actividad, num_dias_duracion_actividad, ind_estado_actividad,
        cod_fase, txt_descripcion_fase, txt_descripcion_actividad";
        $Union = " INNER JOIN pf_b004_actividad ON pf_c001_actividad_planific.fk_pfb004_num_actividad=pf_b004_actividad.pk_num_actividad ";
        $Union .= " INNER JOIN pf_b003_fase ON pf_b004_actividad.fk_pfb003_num_fase=pf_b003_fase.pk_num_fase ";
        $sql_criterio.=" ORDER BY cod_fase, cod_actividad";
        $sql_query="SELECT $campos FROM $tabla$Union".$sql_criterio;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    public function metProcesoFiscal($idProceso){
        $result=$this->_db->query("SELECT txt_descripcion_proceso as nombre_proceso FROM pf_b002_proceso WHERE pk_num_proceso=".$idProceso);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetch();
    }
    /**
     * Extrae las personas sancionadas y su sanción en un Procedimiento Administrativo.
     * @param $idPlanificacion
     * @return array
     */
    public function metBuscaFallosProcedimiento($idPlanificacion){
        $result=$this->_db->query("
            SELECT pk_num_decision AS id_decision, ind_tipodecision AS nombre_decision, ind_cedula_documento AS cedula_pers,CONCAT_WS(' ', ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2) AS nombre_persona,
              monto, monto_reparo, pf_c006_decision.fec_ultima_modific_decision AS fecha_decision,flag_num_campo AS cantmontos_decision
            FROM pf_c006_decision
              INNER JOIN pf_c007_tipo_decision ON pf_c006_decision.fk_pfc007_num_tipodecision = pf_c007_tipo_decision.pk_num_tipodecision
              INNER JOIN a003_persona ON pf_c006_decision.fk_a003_num_persona = a003_persona.pk_num_persona
            WHERE fk_pfb001_num_planificacion=".$idPlanificacion." ORDER BY ind_nombre1 ASC
        ");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
}
?>