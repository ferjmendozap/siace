<?php
/***********************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Planificación Fiscal
 * PROCESO: Reportes de Planficaciones.
 * DESCRIPCIÓN: Permite la extración de datos para la consulta y reportes en pdf de planificaciones por
 *              auditor y auditor por planificación en sus diferentes proceso fiscales: Actuación fiscal,
 *              Valoración preliminar, Potestad investigativa, Valoración jurídica y Procedimientos administrativos.
 * PROGRAMADORES.______________________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Alexis Ontiveros     | ontiveros.alexis@cmldc.gob.ve        | 0426-514.43.82
 * |___________________________________________________________________________________________________________________
 *
 * VERSION
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        07-08-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 ***********************************************************************************************************************/
class rptPlanificacionAuditorModelo extends Modelo{
    public function metConsultaPlanificacion($params){
        $tabla="pf_b001_planificacion_fiscal tb_planificacion";$sql_criterio="";$criterio=false;
        $campos="tb_planificacion.*,fk_a039_num_ente, tb_planifpadre.cod_planificacion AS cod_planifpadre, tb_planifpadre.fk_b002_num_proceso AS num_procesopadre,
        tb_tipoactuacion.ind_nombre_detalle AS nombre_tipoactuacion, tb_origenactuacion.ind_nombre_detalle AS nombre_origenactuacion";
        $Union=" INNER JOIN a041_persona_ente ON tb_planificacion.fk_a041_num_persona_ente = a041_persona_ente.pk_num_persona_ente";
        $Union.=" LEFT JOIN pf_b001_planificacion_fiscal tb_planifpadre ON tb_planificacion.num_planificacion_padre = tb_planifpadre.pk_num_planificacion";
        $Union.=" INNER JOIN a006_miscelaneo_detalle tb_tipoactuacion ON tb_planificacion.fk_a006_num_miscdet_tipoactuacion = tb_tipoactuacion.pk_num_miscelaneo_detalle";
        $Union.=" INNER JOIN a006_miscelaneo_detalle tb_origenactuacion ON tb_planificacion.fk_a006_num_miscdet_origenactuacion = tb_origenactuacion.pk_num_miscelaneo_detalle";
        if($params['idTipoProceso']){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="tb_planificacion.fk_b002_num_proceso=".$params['idTipoProceso'];
        }
        if($params['idPlanificacion']){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="tb_planificacion.pk_num_planificacion=".$params['idPlanificacion'];
        }else{
            if($params['txt_codPlanificacion']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_planificacion.cod_planificacion LIKE '%".$params['txt_codPlanificacion']."%'";
            }
            if($params['id_contraloria']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_planificacion.fk_a001_num_organismo='".$params['id_contraloria']."'";
            }
            if($params['idDependencia']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_planificacion.fk_a004_num_dependencia=".$params['idDependencia'];
            }
            if($params['cbox_tipoActuacion']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_planificacion.fk_a006_num_miscdet_tipoactuacion=".$params['cbox_tipoActuacion'];
            }
            if($params['cbox_origenActuacion']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_planificacion.fk_a006_num_miscdet_origenactuacion=".$params['cbox_origenActuacion'];
            }
            if($params['cbox_estadosplan']) {
                $sql_criterio .= ($criterio) ? " AND " : " WHERE ";$criterio = true;
                $sql_criterio .= "tb_planificacion.ind_estado_planificacion='".$params['cbox_estadosplan']."'";
            }
            if($params['cbox_ente']){
                $Union.=" INNER JOIN a039_ente ON a041_persona_ente.fk_a039_num_ente = a039_ente.pk_num_ente";
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="pk_num_ente=".$params['cbox_ente'];
            }
            if($params['txt_fechainic1'] AND $params['txt_fechainic2']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";
                $sql_criterio.=" CAST(tb_planificacion.fec_inicio AS DATE) BETWEEN '".$params['txt_fechainic1']."' AND '".$params['txt_fechainic2']."'";
            }else{
                if($params['cbox_yearplan']){
                    $sql_criterio.=($criterio)? " AND ": " WHERE ";
                    $sql_criterio.="YEAR(tb_planificacion.fec_inicio)='".$params['cbox_yearplan']."'";
                }
            }
        }
        $sql_criterio.=" ORDER BY cod_planificacion ASC";
        $sql_query="SELECT $campos FROM $tabla$Union".$sql_criterio; //echo $sql_query; exit;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    //Extrae las planificaciones en las que está involucrado un auditor en particular
    public function metBuscaPlanificacionAuditor($params){
        $result=$this->_db->query("
                  SELECT cod_planificacion,fk_a041_num_persona_ente,fk_a006_num_miscdet_tipoactuacion,ind_estado_planificacion,num_flag_coordinador,
                    tb_tipoactuacion.ind_nombre_detalle AS nombre_tipoactuacion,fk_a039_num_ente
                  FROM pf_b001_planificacion_fiscal
                    INNER JOIN pf_c002_auditor_planificacion ON pf_b001_planificacion_fiscal.pk_num_planificacion=pf_c002_auditor_planificacion.fk_pfb001_num_planificacion
                    INNER JOIN a006_miscelaneo_detalle tb_tipoactuacion ON pf_b001_planificacion_fiscal.fk_a006_num_miscdet_tipoactuacion = tb_tipoactuacion.pk_num_miscelaneo_detalle
                    INNER JOIN a041_persona_ente ON pf_b001_planificacion_fiscal.fk_a041_num_persona_ente = a041_persona_ente.pk_num_persona_ente
                  WHERE fk_b002_num_proceso=".$params['idTipoProceso']." AND fk_a001_num_organismo=".$params['id_contraloria']." AND fk_a004_num_dependencia=".$params['idDependencia']."
                    AND YEAR(fec_inicio)='".$params['cbox_yearplan']."' AND fk_rhb001_num_empleado=".$params['cbox_persona']." AND pf_c002_auditor_planificacion.num_estatus=1
                  ORDER BY cod_planificacion");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }

    /**
     * Extrae los auditores asignados a la planificación.
     * @param $idPlanificacion
     * @return array
     */
    public function metAuditoresDesignados($idPlanificacion){
        $tabla="pf_c002_auditor_planificacion"; $campos="*";$sql_criterio="";$criterio=false;
        $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="fk_pfb001_num_planificacion=".$idPlanificacion; $criterio=true;
        $sql_query="SELECT $campos FROM $tabla".$sql_criterio;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    public function metProcesoFiscal($idProceso){
        $result=$this->_db->query("SELECT txt_descripcion_proceso as nombre_proceso FROM pf_b002_proceso WHERE pk_num_proceso=".$idProceso);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetch();
    }
    public function metBuscaAuditores($params){
        $criterio=false;
        if($params['idDependencia']){
            $sql_criterio = ($criterio) ? " AND " : " WHERE ";  $sql_criterio .= "pf_b001_planificacion_fiscal.fk_a004_num_dependencia=" . $params['idDependencia']; $criterio = true;
        }
        $query = $this->_db->query("
                SELECT DISTINCT(pk_num_empleado) AS id_empleado,CONCAT_WS(' ',ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2) AS nombre_empleado
                FROM pf_c002_auditor_planificacion
                    INNER JOIN pf_b001_planificacion_fiscal ON pf_c002_auditor_planificacion.fk_pfb001_num_planificacion=pf_b001_planificacion_fiscal.pk_num_planificacion
                    INNER JOIN rh_b001_empleado ON pf_c002_auditor_planificacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                    INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                    INNER JOIN rh_c076_empleado_organizacion ON rh_b001_empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado".$sql_criterio);
        $query->setFetchMode(PDO::FETCH_ASSOC);
        return $query->fetchAll();
    }
}
?>