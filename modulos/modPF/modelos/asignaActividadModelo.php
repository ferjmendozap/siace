<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: PLANIFICACIÓN FISCAL
 * PROCESO: ASIGNACIÓN DE ACTIVIDADES A TIPOS DE ACTUACIÓN
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 3 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |         0426-5144382           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #3                      |        02-09-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class asignaActividadModelo extends Modelo{
    private $atFuncionesGnrles;
    public function __construct(){
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }
    /**
     * Consultas
     * @param $case
     * @param $param
     * @return array
     */
    public function metConsultas($case,$params=""){
        $campos='*';$Union="";
        switch($case){
            case "por_tipo_actuacion":
                $tabla='a005_miscelaneo_maestro';
                $campos='pk_num_miscelaneo_detalle AS idTipoActuacionFiscal,ind_nombre_detalle AS tipo_actuacionfiscal';
                $Union=" INNER JOIN a006_miscelaneo_detalle ON a005_miscelaneo_maestro.pk_num_miscelaneo_maestro = a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro ";
                $sql_criterio = " WHERE cod_maestro='PFTAF' ORDER BY ind_nombre_detalle";
                break;
            case "por_tipo_proceso":
                $tabla='pf_b002_proceso';
                $campos='pk_num_proceso,txt_descripcion_proceso';
                $sql_criterio = " WHERE num_estatus='1'";
                break;
            case "por_fases":
                $tabla='pf_b003_fase';
                $campos='pk_num_fase, txt_descripcion_fase';
                $sql_criterio = " WHERE fk_pfb002_num_proceso = ".$params." AND num_estatus='1'";
                break;
            case "por_actividades":
                $tabla='pf_b004_actividad';
                $campos='pk_num_actividad,cod_actividad, txt_descripcion_actividad, ind_afecto_plan, num_estatus';
                $sql_criterio = " WHERE fk_pfb003_num_fase=".$params." AND num_estatus='1' ORDER BY cod_actividad";
                break;
            case "por_actividad_asignada":
                $tabla='pf_c005_actividad_tipoactuac';
                $campos='pk_num_actividad_tipoactuac';
                $sql_criterio = " WHERE fk_a006_num_miscelaneodet = ".$params['idTipoActuacionFiscal']." AND fk_pfb004_num_actividad=".$params['pk_num_actividad'];
                break;
        }
        $sql_query = "SELECT $campos FROM $tabla$Union".$sql_criterio;
        $sqlQuery =  $this->_db->query($sql_query);
        $sqlQuery->setFetchMode(PDO::FETCH_ASSOC);
        return $sqlQuery->fetchAll();
    }
    /**
     * Registra la asignación de una o mas actividades a un tipo de actuación
     * @param $params
     * @return int|string
     */
    public function metAgregaActividad($params){
        $this->_db->beginTransaction();
        $sql_query = $this->_db->prepare("
              INSERT INTO pf_c005_actividad_tipoactuac SET
              fk_pfb002_num_proceso_tipo=:fk_pfb002_num_proceso_tipo,
              fk_a006_num_miscelaneodet=:fk_a006_num_miscelaneodet,
              fk_pfb004_num_actividad=:fk_pfb004_num_actividad,
              fk_a018_num_seg_user_reg=$this->atIdUsuario,
              fec_registro=NOW()");

        foreach($params['chk_actividades'] as $idActividad) {
            $arrValores=array(
                'fk_pfb002_num_proceso_tipo' => $params['idProceso'],
                'fk_a006_num_miscelaneodet' => $params['idTipoActuacionFiscal'],
                'fk_pfb004_num_actividad' => $idActividad
            );
            $sql_query->execute($arrValores);
        }
        $error = $sql_query->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack(); $retornar = false;
        } else {
            $retornar=$this->_db->commit();
        }
        return $retornar;
    }
    /**
     * Elimina el registro de asignación de una actividad de un tipo de actuación
     * @param $params
     * @return int
     */
    public function metQuitarActividad($params){
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare("DELETE FROM pf_c005_actividad_tipoactuac WHERE pk_num_actividad_tipoactuac=:pk_num_actividad_tipoactuac");
        $sql_query->execute(array('pk_num_actividad_tipoactuac'=>$params["id_x"]));
        $error=$sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$reg_afectado=false;
        }else{
            $reg_afectado=$this->_db->commit();
        }
        return $reg_afectado;
    }
}
?>