<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Organismos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          José Diaz                        |        diaz.jose@cmldc.gob.ve      |         0416-2124302           |
 * | 2 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |         0426-514.43.82         |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        05-01-2017       |         1.0        |
 * |               #2                      |        06-06-2017       |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class entesModelo extends Modelo{
    public function __construct(){
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }
    /**
     * Busca un ente en particular para modificar
     * @param $idEnte
     * @return mixed
     */
    public function metMostrarEnte($idEnte){
        $organismo = $this->_db->query("
          SELECT a039_ente.*, pk_num_miscelaneo_detalle AS pk_miscdet_tipoente FROM a039_ente
            INNER JOIN a038_categoria_ente ON a039_ente.fk_a038_num_categoria_ente=a038_categoria_ente.pk_num_categoria_ente
            INNER JOIN a006_miscelaneo_detalle ON a038_categoria_ente.fk_a006_miscdet_tipoente = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
          WHERE pk_num_ente=$idEnte
        ");
        $organismo->setFetchMode(PDO::FETCH_ASSOC);
        return $organismo->fetch();
    }
    /**
     * Busca los ente spara llenar la grilla principal ó la grilla de entes padres
     * @return array
     */
    public function metBuscaEntesGrilla(){
        $result=$this->_db->query("SELECT pk_num_ente AS id_ente, ind_nombre_ente AS nombre_ente, num_estatus AS estatus_ente FROM a039_ente");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Busca las categorías de acuerdo al id de un tipo ente.
     * @param $idTipoEnte
     * @return array
     */
    public function metCategoriasEnte($idTipoEnte){
        $result=$this->_db->query("
          SELECT pk_num_categoria_ente AS id_categoria, ind_categoria_ente AS nombre_categoria
          FROM a038_categoria_ente
          WHERE fk_a006_miscdet_tipoente=".$idTipoEnte." ORDER BY ind_categoria_ente ASC");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Busca los ids de país, entidad y municipio de acuerdo al id de una parroquia.
     * @param $idParroquia
     * @return mixed
     */
    public function metBuscaIdsPem($idParroquia){
        $result=$this->_db->query("
          SELECT pk_num_municipio AS id_municipio, pk_num_estado AS id_entidad, pk_num_pais AS id_pais
            FROM a012_parroquia
              INNER JOIN a011_municipio ON a012_parroquia.fk_a011_num_municipio = a011_municipio.pk_num_municipio
              INNER JOIN a009_estado ON a011_municipio.fk_a009_num_estado = a009_estado.pk_num_estado
              INNER JOIN a008_pais ON a009_estado.fk_a008_num_pais = a008_pais.pk_num_pais
            WHERE pk_num_parroquia=".$idParroquia
        );
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetch();
    }
    /**
     * Valida la existencia del nombre del ente al ingresar ó modificar uno.
     * @param $case
     * @param $arrParams
     * @return mixed
     */
    public function metValidaEnte($case,$arrParams){
        switch($case){
            case'valida_ingreso':
                $sql_criterio="num_ente_padre=".$arrParams['idEntePadre']." AND ind_nombre_ente='".$arrParams['txt_nombreEnte']."'";
                break;
            case'valida_mod':
                $sql_criterio="pk_num_ente!=".$arrParams['idEnte']." AND num_ente_padre=".$arrParams['idEntePadre']." AND ind_nombre_ente='".$arrParams['txt_nombreEnte']."'";
                break;
        }
        $resultado = $this->_db->query("SELECT * FROM a039_ente WHERE ".$sql_criterio);
        $resultado->setFetchMode(PDO::FETCH_ASSOC);
        return $resultado->fetch();
    }
    /**
     * Valida la existencia de un rif al ingresar ó modificar un ente.
     * @param $case
     * @param $arrParams
     * @return mixed
     */
    public function metValidaRif($case,$arrParams){
        switch($case){
            case'valida_ingreso':
                $sql_criterio="ind_numero_registro='".$arrParams['txt_nroRif']."'";
                break;
            case'valida_mod':
                $sql_criterio="pk_num_ente!=".$arrParams['idEnte']." AND ind_numero_registro='".$arrParams['txt_nroRif']."'";
                break;
        }
        $resultado = $this->_db->query("SELECT * FROM a039_ente WHERE ".$sql_criterio);
        $resultado->setFetchMode(PDO::FETCH_ASSOC);
        return $resultado->fetch();
    }
    /**
     * Ingresa los datos de una nuevo ente u organización
     * @param $arrParams
     * @return array|string
     */
    public function metIngresaEnte($arrParams){
        $this->_db->beginTransaction();
        $valores=array(
            'num_ente_padre'=>$arrParams['idEntePadre'],
            'fk_a038_num_categoria_ente'=>$arrParams['cbox_categoriaEnte'],
            'ind_nombre_ente'=>$arrParams['txt_nombreEnte'],
            'ind_numero_registro'=>$arrParams['txt_nroRif'],
            'ind_tomo_registro'=>$arrParams['txt_tomoRegistro'],
            'fec_fundacion'=>$arrParams['txt_fechaFundacion'],
            'num_sujeto_control'=>$arrParams['chk_sujetoControl'],
            'ind_mision'=>$arrParams['txt_mision'],
            'ind_vision'=>$arrParams['txt_vision'],
            'ind_gaceta'=>$arrParams['txt_nroGaceta'],
            'ind_resolucion'=>$arrParams['txt_resolucion'],
            'ind_otros'=>$arrParams['txt_otros'],
            'ind_pagina_web'=>$arrParams['txt_paginaWeb'],
            'fk_a012_num_parroquia'=>$arrParams['cbox_parroquia'],
            'fk_a010_num_ciudad'=>$arrParams['cbox_ciudad'],
            'ind_direccion'=>$arrParams['txt_direccion'],
            'ind_telefono'=>$arrParams['txt_telefonos'],
            'num_estatus'=>$arrParams['chk_estatusEnte'],
            'fk_a018_num_seg_usermod'=>$this->atIdUsuario,
            'fec_ultima_modificacion'=>date('Y-m-d H:i:s')
        );
        $sql_query=$this->_db->prepare("
            INSERT INTO a039_ente SET
                num_ente_padre=:num_ente_padre,
                fk_a038_num_categoria_ente=:fk_a038_num_categoria_ente,
                ind_nombre_ente=:ind_nombre_ente,
                ind_numero_registro=:ind_numero_registro,
                ind_tomo_registro=:ind_tomo_registro,
                fec_fundacion=:fec_fundacion,
                num_sujeto_control=:num_sujeto_control,
                ind_mision=:ind_mision,
                ind_vision=:ind_vision,
                ind_gaceta=:ind_gaceta,
                ind_resolucion=:ind_resolucion,
                ind_otros=:ind_otros,
                ind_pagina_web=:ind_pagina_web,
                fk_a012_num_parroquia=:fk_a012_num_parroquia,
                fk_a010_num_ciudad=:fk_a010_num_ciudad,
                ind_direccion=:ind_direccion,
                ind_telefono=:ind_telefono,
                num_estatus=:num_estatus,
                fk_a018_num_seg_usermod=:fk_a018_num_seg_usermod,
                fec_ultima_modificacion=:fec_ultima_modificacion
        ");
        $sql_query->execute($valores);
        $idEnte=$this->_db->lastInsertId();
        $error = $sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack(); return $error;
        }else{
            $this->_db->commit(); return $idEnte;
        }
    }
    /**
     * Actualiza los datos de un ente
     * @param $arrParams
     * @return array
     */
    public function metActualizaEnte($arrParams){
        $this->_db->beginTransaction();
        $valores=array(
            'num_ente_padre'=>$arrParams['idEntePadre'],
            'fk_a038_num_categoria_ente'=>$arrParams['cbox_categoriaEnte'],
            'ind_nombre_ente'=>$arrParams['txt_nombreEnte'],
            'ind_numero_registro'=>$arrParams['txt_nroRif'],
            'ind_tomo_registro'=>$arrParams['txt_tomoRegistro'],
            'fec_fundacion'=>$arrParams['txt_fechaFundacion'],
            'num_sujeto_control'=>$arrParams['chk_sujetoControl'],
            'ind_mision'=>$arrParams['txt_mision'],
            'ind_vision'=>$arrParams['txt_vision'],
            'ind_gaceta'=>$arrParams['txt_nroGaceta'],
            'ind_resolucion'=>$arrParams['txt_resolucion'],
            'ind_otros'=>$arrParams['txt_otros'],
            'ind_pagina_web'=>$arrParams['txt_paginaWeb'],
            'fk_a012_num_parroquia'=>$arrParams['cbox_parroquia'],
            'fk_a010_num_ciudad'=>$arrParams['cbox_ciudad'],
            'ind_direccion'=>$arrParams['txt_direccion'],
            'ind_telefono'=>$arrParams['txt_telefonos'],
            'num_estatus'=>$arrParams['chk_estatusEnte'],
            'fk_a018_num_seg_usermod'=>$this->atIdUsuario,
            'fec_ultima_modificacion'=>date('Y-m-d H:i:s'),
            'pk_num_ente'=>$arrParams['idEnte']
        );
        $sql_query=$this->_db->prepare("
            UPDATE a039_ente SET
                num_ente_padre=:num_ente_padre,
                fk_a038_num_categoria_ente=:fk_a038_num_categoria_ente,
                ind_nombre_ente=:ind_nombre_ente,
                ind_numero_registro=:ind_numero_registro,
                ind_tomo_registro=:ind_tomo_registro,
                fec_fundacion=:fec_fundacion,
                num_sujeto_control=:num_sujeto_control,
                ind_mision=:ind_mision,
                ind_vision=:ind_vision,
                ind_gaceta=:ind_gaceta,
                ind_resolucion=:ind_resolucion,
                ind_otros=:ind_otros,
                ind_pagina_web=:ind_pagina_web,
                fk_a012_num_parroquia=:fk_a012_num_parroquia,
                fk_a010_num_ciudad=:fk_a010_num_ciudad,
                ind_direccion=:ind_direccion,
                ind_telefono=:ind_telefono,
                num_estatus=:num_estatus,
                fk_a018_num_seg_usermod=:fk_a018_num_seg_usermod,
                fec_ultima_modificacion=:fec_ultima_modificacion
            WHERE
                pk_num_ente=:pk_num_ente
        ");
        $sql_query->execute($valores);
        $error = $sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack(); return $error;
        }else{
            $this->_db->commit(); return true;
        }
    }
    /**
     * Elimina un ente siempre y cuando no esté relacionado
     * @param $idEnte
     * @return array|bool
     */
    public function metEliminarEnte($idEnte){
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare("DELETE FROM a039_ente WHERE pk_num_ente=:pk_num_ente");
        $sql_query->execute(array('pk_num_ente'=>$idEnte));
        $error=$sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack(); return $error;
        }else{
            $this->_db->commit(); return true;
        }
    }
}