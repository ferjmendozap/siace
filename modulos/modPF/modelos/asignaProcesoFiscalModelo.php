<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: PLANIFICACIÓN FISCAL
 * PROCESO: Asignación de procesos fiscales a dependencias de control
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |         0426-5144382           |
 * |___|___________________________________________|____________________________________|________________________________|
 * VERSION
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #3                      |        15-07-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class asignaProcesoFiscalModelo extends Modelo{
    public function __construct(){
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }
    /**
     * Consultas
     * @param $case
     * @param $param
     * @return array
     */
    public function metConsultas($case,$params=NULL){
        $campos='*';$Union="";
        switch($case){
            case "por_tipo_proceso":
                $tabla='pf_b002_proceso';
                $campos='pk_num_proceso,cod_proceso,txt_descripcion_proceso,num_estatus';
                $sql_criterio = " WHERE num_estatus='1'";
                break;
            case "por_proceso_asignado":
                $tabla='pf_c009_procesodependencia';
                $campos='pk_num_procesodependencia';
                $sql_criterio = " WHERE fk_a004_num_dependencia = ".$params['id_dependencia']." AND fk_pfb002_num_proceso = ".$params['pk_num_proceso'];
                break;
        }
        $sql_query = "SELECT $campos FROM $tabla$Union".$sql_criterio;
        $sqlQuery =  $this->_db->query($sql_query);
        $sqlQuery->setFetchMode(PDO::FETCH_ASSOC);
        return $sqlQuery->fetchAll();
    }
    /**
     * Registra la asignación de uno más procesos ficales a la dependencia de control seleccionada
     * @param $params
     * @return int|string
     */
    public function metAgregaProcesos($params){
        $this->_db->beginTransaction();
        $sql_query = $this->_db->prepare("
              INSERT INTO pf_c009_procesodependencia SET
              fk_a004_num_dependencia=:fk_a004_num_dependencia,
              fk_pfb002_num_proceso=:fk_pfb002_num_proceso");

        foreach($params['chk_proceso'] as $idProceso) {
            $arrValores=array(
                'fk_a004_num_dependencia' => $params['id_dependencia'],
                'fk_pfb002_num_proceso' => $idProceso
            );
            $sql_query->execute($arrValores);
        }
        $error = $sql_query->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack(); $retornar = false;
        } else {
            $retornar=$this->_db->commit();
        }
        return $retornar;
    }
    /**
     * Elimina el registro de asignación de proceso de una dependencia de control fiscal
     * @param $params
     * @return int
     */
    public function metQuitaProcesos($params){
        $this->_db->beginTransaction();
        $sql_query=$this->_db->prepare("DELETE FROM pf_c009_procesodependencia WHERE pk_num_procesodependencia=:pk_num_procesodependencia");
        $sql_query->execute(array('pk_num_procesodependencia'=>$params["id_x"]));
        $error=$sql_query->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();$reg_afectado=false;
        }else{
            $reg_afectado=$this->_db->commit();
        }
        return $reg_afectado;
    }
}
?>