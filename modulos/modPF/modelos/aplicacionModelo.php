<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        17-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class aplicacionModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarAplicacion($idAplicacion)
    {
        $aplicacion = $this->_db->query("
            SELECT * FROM a015_seguridad_aplicacion WHERE pk_num_seguridad_aplicacion='$idAplicacion'
        ");
        $aplicacion->setFetchMode(PDO::FETCH_ASSOC);

        return $aplicacion->fetch();
    }

    public function metListarAplicacion()
    {
        $menu = $this->_db->query("SELECT * FROM a015_seguridad_aplicacion WHERE pk_num_seguridad_aplicacion=9");
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
    }

    public function metCrearAplicacion($cod,$fecCreacion,$descripcion,$modulo,$status)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "INSERT INTO
              a015_seguridad_aplicacion
            SET
              fk_a018_num_seguridad_usuario='$this->atIdUsuario', cod_aplicacion=:cod_aplicacion, fec_creacion=:fec_creacion,
              ind_descripcion=:ind_descripcion, ind_nombre_modulo=:ind_nombre_modulo, num_estatus=:num_estatus
            "
        );
        $NuevoRegistro->execute(array(
            ':cod_aplicacion' => $cod,
            ':fec_creacion' => $fecCreacion,
            ':ind_descripcion' => $descripcion,
            ':ind_nombre_modulo' => $modulo,
            ':num_estatus' => $status
        ));
        $idAplicacion=$this->_db->lastInsertId();
        $error = $NuevoRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idAplicacion;
        }
    }

    public function metModificarAplicacion($cod,$fecCreacion,$descripcion,$modulo,$status,$idAplicacion)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
              a015_seguridad_aplicacion
            SET
              fk_a018_num_seguridad_usuario='$this->atIdUsuario', cod_aplicacion=:cod_aplicacion, fec_creacion=:fec_creacion,
              ind_descripcion=:ind_descripcion, ind_nombre_modulo=:ind_nombre_modulo, num_estatus=:num_estatus
            WHERE
              pk_num_seguridad_aplicacion='$idAplicacion'
            "
        );
        $NuevoRegistro->execute(array(
            ':cod_aplicacion' => $cod,
            ':fec_creacion' => $fecCreacion,
            ':ind_descripcion' => $descripcion,
            ':ind_nombre_modulo' => $modulo,
            ':num_estatus' => $status
        ));
        $error = $NuevoRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idAplicacion;
        }
    }

}
