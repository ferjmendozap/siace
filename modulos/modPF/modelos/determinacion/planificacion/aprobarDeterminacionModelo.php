<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Planificación Fiscal
 * PROCESO: Aprobación de Determinación de Responsabilidades.
 * DESCRIPCIÓN: Procesa Listados, aprobación y anulación de aprobación planificaciones.
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Alexis Ontiveros     | ontiveros.alexis@cmldc.gob.ve        | 0426-514.43.82
 * |_____________________________________________________________________________________
 *
 * VERSION
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        20-02-2016       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/
class aprobarDeterminacionModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct(){
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    /**
     * Busca planificaciones según los criterios de búsqueda
     * @param array $params
     * @return array
     */
    public function metBuscarPlanificaciones($params){
        $tabla="pf_b001_planificacion_fiscal tb_planificacion";$sql_criterio="";$criterio=false;
        $campos="tb_planificacion.*,fk_a039_num_ente, tb_valjuridica.cod_planificacion AS cod_valjuridica";
        $Union=" INNER JOIN a041_persona_ente ON tb_planificacion.fk_a041_num_persona_ente = a041_persona_ente.pk_num_persona_ente";
        $Union.=" INNER JOIN pf_b001_planificacion_fiscal tb_valjuridica ON tb_planificacion.num_planificacion_padre = tb_valjuridica.pk_num_planificacion";
        if(isset($params['idTipoProceso'])){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="tb_planificacion.fk_b002_num_proceso=".$params['idTipoProceso'];
        }
        if(isset($params['idPlanificacion']) AND $params['idPlanificacion']>0){
            $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
            $sql_criterio.="tb_planificacion.pk_num_planificacion=".$params['idPlanificacion'];
        }else{
            if(isset($params['cod_planificacion']) AND $params['cod_planificacion']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_planificacion.cod_planificacion='".$params['cod_planificacion']."'";
            }
            if(isset($params['cbox_dependencia']) AND $params['cbox_dependencia']>0){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_planificacion.fk_a004_num_dependencia=".$params['cbox_dependencia'];
            }
            if(isset($params['cbox_centro_costo']) AND $params['cbox_centro_costo']>0){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_planificacion.fk_a023_num_centro_costo=".$params['cbox_centro_costo'];
            }
            if(isset($params['cbox_estadoplan']) AND $params['cbox_estadoplan']) {
                $sql_criterio.=($criterio) ? " AND " : " WHERE ";$criterio = true;
                if($params['cbox_estadoplan']=='RV'){
                    $sql_criterio.="tb_planificacion.ind_estado_planificacion='" . $params['cbox_estadoplan'] . "'";
                }elseif($params['cbox_estadoplan']=='AP'){
                    $sql_criterio.="tb_planificacion.fk_rhb001_num_empleado_aprobado IS NOT NULL";
                }
            }
            if(isset($params['txt_objetivo_plan']) AND $params['txt_objetivo_plan']){
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.="tb_planificacion.ind_objetivo LIKE('%".$params['txt_objetivo_plan']."%')";
            }
            if(isset($params['cbox_ente']) AND $params['cbox_ente']>0){
                $Union.=" INNER JOIN a039_ente ON a041_persona_ente.fk_a039_num_ente = a039_ente.pk_num_ente";
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.=" pk_num_ente=".$params['cbox_ente'];
            }
            if(isset($params['txt_fechareg1']) AND $params['txt_fechareg1'] OR isset($params['txt_fechareg2']) AND $params['txt_fechareg2']){
                if(!$params['txt_fechareg1']){
                    $params['txt_fechareg1']=$params['txt_fechareg2'];
                }elseif(!$params['txt_fechareg2']){
                    $params['txt_fechareg2']=$params['txt_fechareg1'];
                }
                $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                $sql_criterio.=" CAST(tb_planificacion.fec_registro_planificacion AS DATE) BETWEEN '".$params['txt_fechareg1']."' AND '".$params['txt_fechareg2']."'";
            }else{
                if(isset($params['cbox_yearplan']) AND $params['cbox_yearplan']>0){
                    $sql_criterio.=($criterio)? " AND ": " WHERE ";$criterio=true;
                    $sql_criterio.="YEAR(tb_planificacion.fec_registro_planificacion)='".$params['cbox_yearplan']."'";
                }
            }
        }
        $sql_criterio.=" ORDER BY cod_planificacion ASC";
        $sql_query="SELECT $campos FROM $tabla$Union".$sql_criterio;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Busca auditores asignados a la planificación según los criterios pasados.
     * @param $params
     * @return array
     */
    public function metAuditoresDesignados($params)
    {
        $tabla="pf_c002_auditor_planificacion"; $campos="*";$sql_criterio="";$criterio=false;
        $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="fk_pfb001_num_planificacion=".$params['idPlanificacion']; $criterio=true;
        if(isset($params['pk_num_empleado'])){
            $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="fk_rhb001_num_empleado = ".$params['pk_num_empleado'];$criterio=true;
        }
        if(isset($params['flag_coordinador'])){
            $sql_criterio.=($criterio)? " AND ": " WHERE "; $sql_criterio.="num_flag_coordinador = ".$params['flag_coordinador'];
        }
        $sql_query="SELECT $campos FROM $tabla".$sql_criterio;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
    /**
     * Extrea las actividades asignadas de una planificación
     * @param $params
     * @return mixed     *
     */
    public function metActividadesAsignadas($params){
        $tabla="pf_c001_actividad_planific"; $campos="*";$sql_criterio="";$criterio=false;$Union="";
        if(isset($params['idPlanificacion']) AND $params['idPlanificacion']) {
            $sql_criterio = ($criterio) ? " AND " : " WHERE ";$criterio = true;
            $sql_criterio .= "fk_pfb001_num_planificacion=" . $params['idPlanificacion'];
        }
        if(isset($params['idActividad']) AND $params['idActividad']) {
            $sql_criterio .= ($criterio) ? " AND " : " WHERE ";$criterio = true;
            $sql_criterio .= "fk_pfb004_num_actividad=" . $params['idActividad'];
        }
        if(isset($params['ind_estado']) AND $params['ind_estado']) {
            $sql_criterio .= ($criterio) ? " AND " : " WHERE ";
            $sql_criterio .= "ind_estado_actividad='".$params['ind_estado']."'";
        }
        if(isset($params['completa']) AND $params['completa']) {
            $campos = "pf_c001_actividad_planific.*,num_dias_duracion_actividad AS num_duracion_actividad,pk_num_actividad, txt_descripcion_actividad,ind_afecto_plan,pf_b003_fase.cod_fase,pf_b003_fase.txt_descripcion_fase";
            $Union = " INNER JOIN pf_b004_actividad ON pf_c001_actividad_planific.fk_pfb004_num_actividad=pf_b004_actividad.pk_num_actividad ";
            $Union .= " INNER JOIN pf_b003_fase ON pf_b004_actividad.fk_pfb003_num_fase=pf_b003_fase.pk_num_fase ";
            $sql_criterio.=" ORDER BY cod_fase, cod_actividad";
        }
        if(isset($params['limite']) AND $params['limite']) {
            $sql_criterio .= " LIMIT " . $params['limite'];
        }
        $sql_query="SELECT $campos FROM $tabla$Union".$sql_criterio;
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }

    /**
     * Se procesa la aprobación de una planificación.
     * Se actualiza el estado de la planificación y los estados de sus actividades.
     * @param $params
     * @return bool
     */
    public function metAprobarPlanificacion($params){
        $retornar=false;
        $this->_db->beginTransaction();
        $fecha_actual = date('Y-m-d H:i:s');
        $arrValores = array(
            'cod_planificacion' => $params['cod_planificacion'],
            'fk_rhb001_num_empleado_aprobado' => $params['idEmpleadoUser'],
            'fec_aprobado' => $params['fecha_aprobacion'],
            'ind_estado_planificacion' => 'AP',
            'pk_num_planificacion' => $params['idPlanificacion']
        );
        $sql_query = $this->_db->prepare("
              UPDATE pf_b001_planificacion_fiscal SET
              cod_planificacion=:cod_planificacion,
              fk_rhb001_num_empleado_aprobado=:fk_rhb001_num_empleado_aprobado,
              fec_aprobado=:fec_aprobado,
              ind_estado_planificacion=:ind_estado_planificacion
              WHERE pk_num_planificacion=:pk_num_planificacion");
        $sql_query->execute($arrValores);
        $error = $sql_query->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
        } else {
            //Se atualiza el estado de las actividades.
            $sql_actividades = $this->_db->prepare("
                UPDATE pf_c001_actividad_planific SET
                ind_estado_actividad=:ind_estado_actividad,
                fk_a018_num_seg_usuaro_map=:fk_a018_num_seg_usuaro_map,
                fec_modifica_ap=:fec_modifica_ap
                WHERE pk_num_actividad_planific=:pk_num_actividad_planific");
            foreach ($params['arrIdActividades'] as $fila) {
                if($fila['num_secuencia_actividad']==1){/*Si la actividad es la uno entra y la coloca en estado en ejecución*/
                    $estado_actividad='EJ';
                }else{/*las demás actividades en estado por ejecutar*/
                    $estado_actividad='PE';
                }
                $arrValores=array(
                    'ind_estado_actividad'=>$estado_actividad,
                    'fk_a018_num_seg_usuaro_map'=>$this->atIdUsuario,
                    'fec_modifica_ap'=>$fecha_actual,
                    'pk_num_actividad_planific'=>$fila['pk_num_actividad_planific']
                );
                $sql_actividades->execute($arrValores);
            }
            $error2 = $sql_actividades->errorInfo();
            if (!empty($error2[1]) && !empty($error2[2])) {
                $this->_db->rollBack();
            }else{
                $this->_db->commit();
                $retornar = true;
            }
        }
        return $retornar;
    }
    /**
     * Anula la aprobación de la planificación.
     */
    public function metAnularAprobacion($params){
        $retornar=false;
        /*Se actualiza el estatus de la planificacion a "Revisado"*/
        $this->_db->beginTransaction();
        $arrValores = array(
            'fk_rhb001_num_empleado_aprobado' => NULL,
            'fec_aprobado' => NULL,
            'ind_estado_planificacion' => 'RV',
            'fk_a018_num_seg_usuario_mod' => $this->atIdUsuario,
            'fec_ultima_modific_planificacion' => $params['fecha_anulacion'],
            'pk_num_planificacion' => $params['idPlanificacion']
        );
        $sql_query = $this->_db->prepare("
          UPDATE pf_b001_planificacion_fiscal SET
          fk_rhb001_num_empleado_aprobado=:fk_rhb001_num_empleado_aprobado,
          fec_aprobado=:fec_aprobado,
          ind_estado_planificacion=:ind_estado_planificacion,
          fk_a018_num_seg_usuario_mod=:fk_a018_num_seg_usuario_mod,
          fec_ultima_modific_planificacion=:fec_ultima_modific_planificacion
          WHERE pk_num_planificacion=:pk_num_planificacion");
        $sql_query->execute($arrValores);
        $error = $sql_query->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
        } else {
            /*Se reversa el estatus de las actividades a "PR"*/
            $sql_actividades = $this->_db->prepare("
            UPDATE pf_c001_actividad_planific SET
            ind_estado_actividad=:ind_estado_actividad,
            fk_a018_num_seg_usuaro_map=:fk_a018_num_seg_usuaro_map,
            fec_modifica_ap=:fec_modifica_ap
            WHERE fk_pfb001_num_planificacion=:fk_pfb001_num_planificacion");
            $arrValores = array(
                'ind_estado_actividad' => 'PR',
                'fk_a018_num_seg_usuaro_map' => $this->atIdUsuario,
                'fec_modifica_ap' => $params['fecha_anulacion'],
                'fk_pfb001_num_planificacion' => $params['idPlanificacion']
            );
            $sql_actividades->execute($arrValores);
            $error2 = $sql_actividades->errorInfo();
            if (!empty($error2[1]) && !empty($error2[2])) {
                $this->_db->rollBack();
            } else {
                $this->_db->commit();$retornar = true;
            }
        }
        return $retornar;
    }
}