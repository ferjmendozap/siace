<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Planificación Fiscal
 * Proceso: Valoración Jurídica.
 * Descripción: Lista, revisión y anulación de planificaciones de Valoración Jurídica.
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Alexis Ontiveros     | ontiveros.alexis@cmldc.gob.ve        | 0426-514.43.82
 * |
 *****************************************************************************************/
require_once RUTA_Modulo . 'modPF' . DS . 'modelos' . DS . 'funcionesGenerales.php';
class revisarValoracionControlador extends Controlador{
    private $atValoracionJurModelo; //Para el modelo revisarValoracion.
    private $atFuncGnrles;          //Para las Funciones generales
    private $atIdContraloria;       //Para el Id de la contraloría que ejecuta.
    private $atIdDependenciaUser;   //Para el Id de la dependencia al cual pertenece el usuario.
    private $atIdEmpleadoUser;      //Para el Id empleado del usuario que ejecuta.
    private $atTipoUser;            //Para el tipo de usuario logueado
    private $atIdTipoProceso;       //Para el Id del Proceso (Valoración preliminar).
    private $atUserValido;          //Contendrá la validéz del usuario
    private $atIdTipoProcesoAf;     //Para el Id del Proceso (actuación fiscal).
    public function __construct(){
        parent::__construct();
        Session::metAcceso();
        $this->atValoracionJurModelo = $this->metCargarModelo('revisarValoracion','valoracionPreliminar/planificacion');
        $this->atFuncGnrles = new funcionesGenerales();
        /*Se busca y asigna el id del tipo de proceso (Actuación fiscal)*/
        $arr_tipoproc = $this->atFuncGnrles->metTipoProcesoFiscal(null,'01');
        $this->atIdTipoProcesoAf = $arr_tipoproc[0]['pk_num_proceso'];
        /*Se busca y asigna el id del tipo de proceso*/
        $arr_tipoproc = $this->atFuncGnrles->metTipoProcesoFiscal(null,'02');
        $this->atIdTipoProceso = $arr_tipoproc[0]['pk_num_proceso'];
        $arr_tipoproc = null;
        //Se determina la validéz y el tipo de usuario.
        $arrUser=$this->atFuncGnrles->metValidaUsuario();
        if($arrUser['userValido']){//Entra, si es un Usuario Válido.
            $this->atTipoUser=$arrUser['tipoUser'];
            $this->atUserValido=$arrUser['userValido'];
            $this->atIdContraloria=$arrUser['pk_num_organismo'];
            $this->atIdEmpleadoUser=$arrUser['pk_num_empleado'];
            $this->atIdDependenciaUser=$arrUser['pk_num_dependencia'];
        }else{//De lo contrario, usuario inválido
            $this->atUserValido=$arrUser['userValido'];
        }
        $arrUser=null;
    }
    public function metIndex(){
        if($this->atUserValido) {
            $complementosJs = array(
                'bootstrap-datepicker/bootstrap-datepicker',
            );
            $complementoCss = array(
                'bootstrap-datepicker/datepicker'
            );
            $this->atVista->metCargarCssComplemento($complementoCss);
            $this->atVista->metCargarJsComplemento($complementosJs);
            //Lista los años de acuerdo al año mas bajo de actuaciones.
            $arrYears=$this->atFuncGnrles->metComboBoxYears();
            $this->atVista->assign('listayears', $arrYears);
            $year_actual=date('Y');
            $this->atVista->assign('year_actual', $year_actual);
            /*Lista la(s) dependencia(s) internas de control según usuario*/
            if($this->atTipoUser=='userCtrolFcal'){//Usuario de Control fiscal
                $listaDepContraloria[0]=$this->atFuncGnrles->metBuscaDepControlUsuario($this->atIdDependenciaUser);
            }elseif($this->atTipoUser=='userInvitado'){//Usuario invitado
                $listaDepContraloria = $this->atFuncGnrles->metBuscaDepControlInvitado($this->atIdContraloria,$this->atIdTipoProceso);
            }
            $this->atVista->assign('listaDependencias', $listaDepContraloria);
            /*Lista los entes externos*/
            $entesExternos = $this->metBuscarEntes(0);
            $this->atVista->assign('entesExternos', $entesExternos["filas"]);
            $this->atVista->assign('total_rcset', 0);
            $this->atVista->metRenderizar('listadoPlanificacion');
        }else{
            $this->atVista->assign('valido', $this->atUserValido);
            $this->atVista->metRenderizar('../../error');
        }
    }
    public function metComplementosForm(){
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'modPF/funcionesModPf';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
    }
    /**
     * Busca las dependencias de control fiscal correspondientes de la contraloría ejecutante
     */
    public function metListaDependenciaInternaCf($iddep_padre=NULL){
        $retornar=array("filas"=>false);
        $flag_controlfiscal="";
        if(!$iddep_padre){
            $flag_controlfiscal=1;
        }
        $result=$this->atFuncGnrles->metCargaCboxRecursivo($this->atIdContraloria,$iddep_padre,$flag_controlfiscal);
        if(COUNT($result)>0) {
            foreach ($result as $fila) {
                $data[] = array("id_dependencia"=>$fila["id_dep"],"iddep_padre"=>$fila["iddep_padre"],"nombre_dependencia"=>$fila["nombre_dep"],"toltick"=>$fila["toltick"]);
            }
            $retornar=array("filas"=>$data);
        }
        return $retornar;
    }
    /**
     * Busca entes externos
     * @param $iddep_padre, Id de la dependencia padre.
     * @return array
     */
    public function metBuscarEntes($iddep_padre){
        $retornar=array("filas"=>false);
        $result=$this->atFuncGnrles->metCargaCboxRecursivoEnte($iddep_padre,'');
        if(COUNT($result)>0) {
            $retornar=array("filas"=>$result);
        }
        return $retornar;
    }
    /**
     * Lista las dependencias de un ente externo al ser seleccionado
     */
    public function metListaEntes(){
        $idEnte = $this->metObtenerInt('idEnte');
        $listaEntes = $this->metBuscarEntes($idEnte);
        echo json_encode($listaEntes);
    }
    /**ListarActuaciones
     * Se buscan las planificaciones para listarlas en la grilla ppal.
     */
    public function metListarPlanificaciones(){
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $Exceccion_int=array('cbox_dependencia','cbox_centro_costo','cbox_ente','cbox_estadoplan');
        $arrInt=$this->metValidarFormArrayDatos('form', 'int',$Exceccion_int);
        $Exceccion_txt=array('txt_objetivo_plan','cbox_estadoplan','txt_fechareg1','txt_fechareg2','cod_planificacion');
        $arrTxt=$this->metValidarFormArrayDatos('form', 'txt',$Exceccion_txt);
        $arrParams=array_merge($arrInt,$arrTxt);
        $arrParams["idTipoProceso"]=$this->atIdTipoProceso;
        if($arrParams["txt_fechareg1"]){
            $arrParams["txt_fechareg1"]=$this->atFuncGnrles->metFormateaFechaMsql($arrParams['txt_fechareg1']);
        }
        if($arrParams["txt_fechareg2"]){
            $arrParams["txt_fechareg2"]=$this->atFuncGnrles->metFormateaFechaMsql($arrParams['txt_fechareg2']);
        }
        //Se extrae las planificaciones de acuerdo al tipo de usuario
        if($this->atTipoUser=='userCtrolFcal'){
            $arrParams["cbox_dependencia"]=$this->atIdDependenciaUser;
            $result=$this->atValoracionJurModelo->metBuscarPlanificaciones($arrParams);
        }elseif($this->atTipoUser=='userInvitado'){
            if(!$arrParams["cbox_dependencia"]){//Extrae todas las planificaciones de acuerdo a las dependencias que tiene acceso el usuario invitado
                $arrData=array();
                //Se busca las dependencias de control al cual el usuario está como invitado
                $listaDepInvitado = $this->atFuncGnrles->metBuscaDepControlInvitado($this->atIdContraloria,$this->atIdTipoProceso);
                foreach($listaDepInvitado as $fila){//se extrae los registros de acuerdo a las dependencias de control en la cuales está invitado
                    $arrParams["cbox_dependencia"]=$fila["id_dependencia"];
                    $result=$this->atValoracionJurModelo->metBuscarPlanificaciones($arrParams);
                    if(COUNT($result)>0){
                        $arrData=array_merge($arrData,$result);
                    }
                }
                $result=$arrData;
            }else{//Extrae sólo las planificaciones de la dependencia seleccionada por el usuario invitado
                $result=$this->atValoracionJurModelo->metBuscarPlanificaciones($arrParams);
            }
        }
        $data=array();
        if(COUNT($result)>0){//Se prepara los resultados.
            foreach ($result as $fila) {
                $fila['ind_estado']=$fila['ind_estado_planificacion'];unset($fila['ind_estado_planificacion']);
                $arrEnte=$this->atFuncGnrles->metBuscaRecursivoEntes($fila['fk_a039_num_ente']);
                $fila['nombre_ente']=$arrEnte['cadEntes'];
                $fila['desc_estado']=$this->atFuncGnrles->metEstadoPlanificacion($fila['ind_estado']);
                $fila['fec_inicio']=$this->atFuncGnrles->metFormateaFecha($fila['fec_inicio']);
                $fila['fec_registro']=$this->atFuncGnrles->metFormateaFecha($fila['fec_registro_planificacion']);unset($fila['fec_registro_planificacion']);
                $fila['fec_ultima_modificacion']=$this->atFuncGnrles->metFormateaFecha($fila['fec_ultima_modific_planificacion']);unset($fila['fec_ultima_modific_planificacion']);
                $arrFechasfin=$this->atFuncGnrles->metBuscaFechaFinPlan($fila['pk_num_planificacion']);
                $data[]=$fila=array_merge($fila,$arrFechasfin);
            }
            $this->atVista->assign('listado', $data);
            $this->atVista->metRenderizar('resultadoListado');
        }else{
            return false;
        }
    }
    /**
     * Lista las dependencias de centro de costos al seleccionar un dependencia de control
     */
    public function metDependenciaCentroCostos(){
        $idDependencia = $this->metObtenerInt('idDependencia');
        $campos="pk_num_centro_costo,ind_descripcion_centro_costo";
        $listaCentroCosto=$this->atFuncGnrles->metBuscaDepCentroCostos($campos,'',$idDependencia,'');
        if(count($listaCentroCosto)>0){
            echo json_encode(array("filas"=>$listaCentroCosto));
        }else{
            echo json_encode(array("filas"=>array()));
        }
    }
    /**
     * Se busca las dependencias para la selección de auditores
     * @param $iddep_padre
     * @return array
     */
    public function metListaDependenciaAuditor($iddep_padre){
        $retornar=array("filas"=>false); $data=array();
        $result=$this->atFuncGnrles->metCargaCboxRecursivo($this->atIdContraloria,$iddep_padre);
        if(COUNT($result)>0) {
            foreach ($result as $fila) {
                $data[] = array("id_dep_auditor"=>$fila["id_dep"], "iddep_padre"=>$fila["iddep_padre"], "nombre_dep_auditor"=>$fila["nombre_dep"], "toltick"=>$fila["toltick"]);
            }
            $retornar=array("filas"=>$data);
        }
        return $retornar;
    }
    /**
     * Monta la planificación al clicar sobre el botón respectivo en la lista de ppal.
     */
    public function metMontaPlanificacion(){
        $idPlanificacion = $this->metObtenerInt('idPlanificacion');
        $result = $this->atValoracionJurModelo->metBuscarPlanificaciones(array('idPlanificacion' => $idPlanificacion, 'idTipoProceso' => $this->atIdTipoProceso));
        if (COUNT($result)) {
            $arrData = $result[0];
            $this->metComplementosForm();
            if($arrData['ind_estado_planificacion']=='PR'){
                $apcion_modal='Actualizar';
            }else{
                $apcion_modal='Ver';
            }
            if($arrData['num_subcodigo']>0){//Entra si la planificación es producto de una planificación devuelta
                $arrData['cod_actuacion']=$this->atFuncGnrles->metBuscaProcesoPadre($arrData['num_planificacion_padre'],$this->atIdTipoProcesoAf);
            }else{
                $arrData['cod_actuacion']=$arrData['cod_planifpadre'];unset($arrData['cod_planifpadre']);
            }
            $this->atVista->assign('apcion_modal', $apcion_modal);
            /*Lista la contraloría que ejecuta*/
            $campos="pk_num_organismo AS id_contraloria,ind_descripcion_empresa AS nombre_contraloria";
            $listaContraloria=$this->atFuncGnrles->metBuscaOrganismos($campos,$this->atIdContraloria,1,'I');
            if (COUNT($listaContraloria) > 0) {
                $this->atVista->assign('listaContraloria', $listaContraloria);
            } else {
                $this->atVista->assign('listaContraloria', array());
            }
            /*Se lista las dependencias de control de acuerdo al tipo de usuario*/
            if($this->atTipoUser=='userCtrolFcal'){
                $listaDepContraloria[0]=$this->atFuncGnrles->metBuscaDepControlUsuario($this->atIdDependenciaUser);
            }elseif($this->atTipoUser=='userInvitado'){
                $listaDepContraloria = $this->atFuncGnrles->metBuscaDepControlInvitado($this->atIdContraloria,$this->atIdTipoProceso);
            }
            $this->atVista->assign('listaDepContraloria', $listaDepContraloria);
            /*Lista de centros de costo*/
            $campos="pk_num_centro_costo,ind_descripcion_centro_costo";
            $listaCentroCosto=$this->atFuncGnrles->metBuscaDepCentroCostos($campos,'',$arrData['fk_a004_num_dependencia'],'');
            if (COUNT($listaCentroCosto) > 0) {
                $this->atVista->assign('listaCentroCosto', $listaCentroCosto);
            } else {
                $this->atVista->assign('listaCentroCosto', array());
            }
            /*Lista los tipo de actuación fiscal*/
            $campos="pk_num_miscelaneo_detalle AS idtipo_actuacion,ind_nombre_detalle AS nombretipo_actuacion";
            $listaTipoActuación = $this->atFuncGnrles->metBuscaMiscelanio($campos,'PFTAF','');
            $this->atVista->assign('listaTipoActuacion', $listaTipoActuación);
            /*Busca el ente externo a objeto de fiscalización*/
            $arrEnte = $this->atFuncGnrles->metBuscaRecursivoEntes($arrData['fk_a039_num_ente']);
            $arrData['nombre_ente'] = $arrEnte['cadEntes'];
            /*Lista los origen de actuación*/
            $campos="pk_num_miscelaneo_detalle AS idorigen_actuacion,ind_nombre_detalle AS nombre_origenactuacion";
            $listaOrigenActuacion = $this->atFuncGnrles->metBuscaMiscelanio($campos,'PFODA','');
            $this->atVista->assign('listaOrigenActuacion', $listaOrigenActuacion);

            $arrData['ind_estado'] = $arrData['ind_estado_planificacion']; unset($arrData['ind_estado_planificacion']);
            $arrData['desc_estado'] = $this->atFuncGnrles->metEstadoPlanificacion($arrData['ind_estado']);
            $arrData['fec_inicio'] = $this->atFuncGnrles->metFormateaFecha($arrData['fec_inicio']);
            $this->atVista->assign('idPlanificacion', $idPlanificacion);

            $preparado_por=$this->atFuncGnrles->metBuscaUsuario(null,$arrData['fk_rhb001_num_empleado_preparado']);
            $arrData['preparado_por']=$preparado_por;
            if($arrData['fk_rhb001_num_empleado_revisado']){
                $revisado_por=$this->atFuncGnrles->metBuscaUsuario(null,$arrData['fk_rhb001_num_empleado_revisado']);
                $arrData['revisado_por']=$revisado_por;
            }
            if($arrData['fk_rhb001_num_empleado_aprobado']){
                $aprobado_por=$this->atFuncGnrles->metBuscaUsuario(null,$arrData['fk_rhb001_num_empleado_aprobado']);
                $arrData['aprobado_por']=$aprobado_por;
            }
            $arrData['fec_registro'] = $this->atFuncGnrles->metFormateaFecha($arrData['fec_registro_planificacion']);
            unset($arrData['fec_registro_planificacion']);
            $arrData['fec_ultima_modificacion'] = $this->atFuncGnrles->metFormateaFecha($arrData['fec_ultima_modific_planificacion']);
            unset($arrData['fec_ultima_modific_planificacion']);
            /*Lista de dependencias para seleccionar auditores*/
            $listaDepAuditor = $this->metListaDependenciaAuditor(0);
            $this->atVista->assign('listaDepAuditor', $listaDepAuditor["filas"]);
            $result=$this->atValoracionJurModelo->metAuditoresDesignados(array("idPlanificacion"=>$idPlanificacion));//Se extraen los auditores designados
            if(COUNT($result)>0) {//Se prepara los datos para presentarlos en la vista en la grilla de auditores designados.
                foreach ($result as $fila) {
                    $camposExtraer="ind_cedula_documento AS cedula_auditor,CONCAT_WS(' ',ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2) AS nombre_auditor,ind_nombre_cargo AS cargo_auditor";
                    $arrAuditor=$this->atFuncGnrles->metBuscaEmpledos($camposExtraer,$fila['fk_rhb001_num_empleado']);
                    $dataAuditores[] = array("id_auditorPlanificacion"=>$fila['pk_num_auditor_planificacion'],
                        "pk_num_empleado"=>$fila['fk_rhb001_num_empleado'], "nombre_auditor"=>$arrAuditor[0]['nombre_auditor'],
                        "cargo_auditor" =>$arrAuditor[0]['cargo_auditor'], "flagCordinador"=>$fila['num_flag_coordinador'],
                        "desc_estatus"=>$this->atFuncGnrles->metEstatus($fila['num_estatus']),
                        "fecha_estatus"=>$this->atFuncGnrles->metFormateaFecha($fila['fec_estatus']),
                        "opcion_auditor"=>$this->atFuncGnrles->metOpcionAuditor($arrData['ind_estado'],$fila['num_estatus']));
                }
                $arrData['aditores_asig'] = true;
            }else{
                $arrData['aditores_asig'] = 0; $dataAuditores=array();
            }
            $this->atVista->assign('auditoresDesignados', $dataAuditores);
            $arrTotalesPlan=$this->atFuncGnrles->metBuscaTotalesPlan($idPlanificacion);//Se buscan los totales de la planificación
            $arrData=array_merge($arrData, $arrTotalesPlan);
            $this->atVista->assign('planificacion', $arrData);
            $this->atVista->metRenderizar('revisarValoracion', 'modales');
        } else {
            echo false;
        }
    }
    /**
     * Lista las actividades de la planificación.
     */
    public function metListaActividades($idPlanificacion){
        $retornar=array();
        $result=$this->atValoracionJurModelo->metActividadesAsignadas(array("idPlanificacion"=>$idPlanificacion,"completa"=>true,"limite"=>""));
        if(COUNT($result)>0) {
            foreach ($result as $fila) {
                $fila['fec_inicio_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_inicio_actividad']);
                $fila['fec_inicio_real_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_inicio_real_actividad']);
                $fila['fec_culmina_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_culmina_actividad']);
                $fila['fec_culmina_real_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_culmina_real_actividad']);
                $arrData[]=$fila;
            }
            $arrData=$this->atFuncGnrles->metAgrupaArray($arrData,'txt_descripcion_fase','cod_fase');
            $retornar=$arrData;
        }
        return $retornar;
    }
    /**
     * Busca las actividades de la planificación al clicar sobre la ficha actividades.
     */
    public function metBuscaActividades(){
        $js[] = 'modPF/DataTablePf';
        $this->atVista->metCargarJs($js);
        $idPlanificacion=$this->metObtenerInt('idPlanificacion');
        $arrResult=$this->metListaActividades($idPlanificacion);
        if(COUNT($arrResult)>0){
            if($idPlanificacion){//Se busca el estado de la planificación
                $result=$this->atFuncGnrles->metObtenerDatosPlanificacion($idPlanificacion);
                $this->atVista->assign('estado_planificacion', $result['ind_estado_planificacion']); unset($result);
            }
            $this->atVista->assign('listaActividades', $arrResult);
            $this->atVista->assign('contador', 0);
            $this->atVista->assign('SubTotal_dias', 0);
            $this->atVista->assign('Total_dias', 0);
            $this->atVista->assign('totalDiasNoAfecta', 0);
            $this->atVista->assign('SubTotal_diasProrroga', 0);
            $this->atVista->assign('Totaldias_prorroga', 0);
            $this->atVista->assign('totalGeneralDias', 0);
            $this->atVista->assign('fecha_fin_plan', '');
            $this->atVista->metRenderizar('grillaActividades');
        }else{
            echo "";
        }
    }
    /**
     *Procesa la revisión de una planificación
     */
    public function metProcesaRevision(){
        //var_dump($this->metValidarToken());
        $arrInt=$this->metValidarFormArrayDatos('form', 'int');
        $fecha_actual = date('Y-m-d H:i:s');
        $params=array('idPlanificacion'=>$arrInt['idPlanificacion'],'fecha_revision'=>$fecha_actual,'idEmpleadoUser'=>$this->atIdEmpleadoUser);
        $result = $this->atValoracionJurModelo->metActualizaPlanificacion($params);
        if ($result) {
            $retornar=array("reg_afectado" =>true,"mensaje"=>"La Revisión se procesó exitosamente");
        } else {
            $retornar=array("reg_afectado" =>"","mensaje"=>"La Revisión no se pudo efectuar. Intente mas tarde");
        }
        echo json_encode($retornar);
    }
    /**
     * Procesa la anulación de una planificación
     */
    public function metAnularPlanificacion(){
        $arrInt=$this->metValidarFormArrayDatos('form', 'int');
        $result=$this->atValoracionJurModelo->metAnularPlanificacion($arrInt['idPlanificacion']);
        if ($result) {
            $retornar=array("reg_afectado" =>true,"mensaje"=>"La Anulación se procesó exitosamente");
        } else {
            $retornar=array("reg_afectado" =>"","mensaje"=>"La Anulación no se pudo efectuar. Intente mas tarde");
        }
        echo json_encode($retornar);
    }
}
