<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Planificación Fiscal
 * PROCESO: Procesa las actividades de cada proceso fiscal y muestra el formulario de Detalles Actividades
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          José Díaz                        |        diaz.jose@cmldc.gob.ve      |         0416-2124302           |
 * | 2 |          Alexis Ontiveros                 |    ontiveros.alexis@cmldc.gob.ve   |         0426-514.43.82         |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        20-01-2017       |         1.0        |
 * |               #2                      |        26-06-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . 'modPF' . DS . 'modelos' . DS . 'funcionesGenerales.php';
class procesaActividadControlador extends Controlador{
    private $atProcesaActModelo;    //Para el modelo procesa actividad
    private $atFuncGnrles;          //Para las Funciones generales.
    private $atIdContraloria;       //Para el Id de la contraloría que ejecuta.
    private $atIdDependenciaUser;   //Para el Id de la dependencia al cual pertenece el usuario.
    private $atIdEmpleadoUser;      //Para el Id empleado del usuario que ejecuta.
    private $atTipoUser;            //Para el tipo de usuario logueado.
    private $atUserValido;          //Contendrá la validéz del usuario.
    private $atIdTipoProceso;       //Para el Id del Proceso fiscal en ejecución.
    public function __construct(){
        parent::__construct(); $CodProcesoFiscal='';
        $this->atProcesaActModelo=$this->metCargarModelo('procesaActividad','actuacionFiscal/procesaActividad');
        $this->atFuncGnrles = new funcionesGenerales();
        $this->fechaActual=date('d-m-Y');
        //Se determina el id del tipo de proceso fiscal en ejecución.
        if(isset($_GET['tipoProceso'])){//por acá se obtiene el código a partír de Valoración preliminar en adelante, a través de la opción del menú "Ejecución de Actividad".
            $CodProcesoFiscal=$_GET['tipoProceso'];
        }else{ //por acá se optiene el código cuando se ejecuta desde actuación fiscal ó el id del proceso fiscal cuando se monta una actividad para procesarla.
            $tipoProceso=$this->metObtenerInt('tipoProceso');//Se obtiene el id del tipo de proceso cuando se monta la actividad para procesarla.
            if(!$tipoProceso){ //Si no viene el id del proceso, entra para asignar el código del proceso "Actuación fiscal".
                $CodProcesoFiscal='01';
            }else{ //De lo contrario...
                $this->atIdTipoProceso = $this->metObtenerInt('tipoProceso'); //Se optiene el id del tipo de proceso.
            }
        }
        if($CodProcesoFiscal){ //Si viene el código del tipo de proceso fiscal, entra
            /*Se busca y asigna el id del tipo de proceso*/
            $arr_tipoproc = $this->atFuncGnrles->metTipoProcesoFiscal(null,$CodProcesoFiscal);
            $this->atIdTipoProceso = $arr_tipoproc[0]['pk_num_proceso'];$arr_tipoproc = null;
        }
        //Se determina la validéz y el tipo de usuario.
        $arrUser=$this->atFuncGnrles->metValidaUsuario();
        if($arrUser['userValido']){//Entra, si es un Usuario Válido.
            $this->atTipoUser=$arrUser['tipoUser'];
            $this->atUserValido=$arrUser['userValido'];
            $this->atIdContraloria=$arrUser['pk_num_organismo'];
            $this->atIdEmpleadoUser=$arrUser['pk_num_empleado'];
            $this->atIdDependenciaUser=$arrUser['pk_num_dependencia'];
        }else{//De lo contrario, usuario no válido
            $this->atUserValido=$arrUser['userValido'];
            $this->atVista->assign('valido', $this->atUserValido);
            $this->atVista->metRenderizar('../../error');exit;
        }
        $arrUser=null;
    }
    /**
     * Busca las planificaciones y las actividades en ejecución para montarlas en la grilla del form principal de (Ejecución de Actividades).
     * @throws Exception
     */
    public function metListadoActividadesEjecutar(){
        //Se extrae las planificaciones de acuerdo al tipo de usuario
        if($this->atTipoUser=='userCtrolFcal'){//Entra, cuando es un usuario de una dependencia de control
            $arrData = $this->atProcesaActModelo->metListarActividaPlanificacion($this->atIdTipoProceso,$this->atIdDependenciaUser);
        }elseif($this->atTipoUser=='userInvitado'){//Entra, cuando es un usuario invitado
            $arrData=array();
            //Se busca las dependencias de control al cual el usuario está como invitado
            $listaDepInvitado = $this->atFuncGnrles->metBuscaDepControlInvitado($this->atIdContraloria,$this->atIdTipoProceso);
            //se extrae los registros de acuerdo a las dependencias de control en la cuales el usuario está como invitado
            foreach($listaDepInvitado as $fila){
                $result = $this->atProcesaActModelo->metListarActividaPlanificacion($this->atIdTipoProceso,$fila["id_dependencia"]);
                if(COUNT($result)>0){
                    $arrData=array_merge($arrData,$result);
                }
            }
        }
        if (COUNT($arrData)>0) {
            foreach ($arrData AS $fila) {
                $fila['fec_ini'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_ini']);
                $fila['fec_fin'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_fin']);
                $arrEnte = $this->atFuncGnrles->metBuscaRecursivoEntes($fila['fk_a039_num_ente']);
                $fila['nombre_ente'] = $arrEnte['cadEntes'];
                $array[] = $fila;
            }
            //Se agrupa las actividades por la Fase que le corresponde
            $arrDataAgrupada = $this->atFuncGnrles->metAgrupaArray($array, 'fase', 'codFase');
        } else {
            $arrDataAgrupada = array();
        }
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'modPF/DataTablePf';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado', $arrDataAgrupada);
        $this->atVista->metRenderizar('grillaActEjecutar');
    }
    /**
     * Se monta listado de planificaciones y actividades a ejecutar en el form principal "Ejecución de Actividades".
     * @throws Exception
     */
    public function metIndex(){
        $this->atVista->assign('tipoProceso', $this->atIdTipoProceso);
        $this->atVista->metRenderizar('listadoActEjecutar');
    }

    public function metComplementosForm(){
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'jquery/jquery.numeric.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min',
            'plupload.queue/plupload.full.min',
            'mask/jquery.mask'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
    }

    /**
     * Monta la actividad en el form modal de procesar actividades
     */
    public function metMontaActividad(){
        $fechaActual=date("d-m-Y"); $esUltimaActividad=false;
        $valido = $this->metObtenerInt('valido');
        $idPlanificacion = $this->metObtenerInt('idPlanificacion');
        $idTipoProceso=$this->metObtenerInt('tipoProceso');
        $this->metComplementosForm();
        //Se extrae los datos de la actividad a procesar.
        $arrData=$this->atProcesaActModelo->metMostrarActividaPlanificacion($idPlanificacion)[0];
        $arrEnte= $this->atFuncGnrles->metBuscaRecursivoEntes($arrData["pk_num_ente"]);
        $arrData['nombreEnte']=$arrEnte['cadEntes'];
        $arrData['fec_ini']=$this->atFuncGnrles->metFormateaFecha($arrData['fec_ini']);
        $arrData['fec_fin']=$this->atFuncGnrles->metFormateaFecha($arrData['fec_fin']);
        $procesos=$this->atFuncGnrles->metTipoProcesoFiscal();
        $arrayProcesos='';
        $i=0;
        foreach ($procesos AS $proceso){
            $arrayProcesos[$i++]=$proceso['pk_num_proceso'];
        }
        //Se determina si la actividad actual es la última de la planificación
        $arrSecuencia=$this->atProcesaActModelo->metSecueciaUltimaActividad($idPlanificacion);
        if($arrData['nro_secuenciActividad']==$arrSecuencia["nro_secuenciaUltimaActividad"]){
            $esUltimaActividad=true;$arrSecuencia=null;
        }
        /*Se crea la Etiqueta de acuerdo al tipo de proceso fiscal*/
        $etiquetaProceso='';
        if($arrayProcesos[0]==$idTipoProceso){
            $etiquetaProceso='Actuación';
        }elseif($arrayProcesos[1]==$idTipoProceso){
            $etiquetaProceso='Valoración';
        }elseif($arrayProcesos[2]==$idTipoProceso){
            $etiquetaProceso='Potestad';
        }elseif($arrayProcesos[3]==$idTipoProceso){
            $etiquetaProceso='Valoración';
        }elseif($arrayProcesos[4]==$idTipoProceso){
            $etiquetaProceso='Determinación';
        }
        /*Se determina si una planificación puede o no devolverse. Sólo con Valoración Jurídica*/
        $idPadre=$this->atFuncGnrles->metObtenerDatosPlanificacion($idPlanificacion);
        $idPadre=$idPadre['num_planificacion_padre'];
        $this->atVista->assign('AF', $arrayProcesos[0]);
        $this->atVista->assign('VP', $arrayProcesos[1]);
        $this->atVista->assign('PI', $arrayProcesos[2]);
        $this->atVista->assign('VJ', $arrayProcesos[3]);
        $this->atVista->assign('PA', $arrayProcesos[4]);
        $this->atVista->assign('esUltimaActividad', $esUltimaActividad);
        $this->atVista->assign('tipoProceso', $idTipoProceso);
        $this->atVista->assign('dataActividad', $arrData);
        $this->atVista->assign('fechaActual', $fechaActual);
        $this->atVista->assign('idPadre', $idPadre);
        $this->atVista->assign('etiquetaProceso', $etiquetaProceso);
        $this->atVista->assign('persSacionados', false);
        //Se determina si la planificación tiene tiene personas sancionadas registradas siempre y cuando sea la última actividad
        //y que corresponda a un Procedimiento Administrativo (Determinación de Responsabilidades)
        if($esUltimaActividad AND $idTipoProceso==5){ // 5 igual al proceso fiscal "Procedimiento Administrativo"
            $arrFallos=$this->atProcesaActModelo->metDecisionDeterminacion($idPlanificacion);//Busca si la planificación tiene personas sancionadas registradas.
            if(COUNT($arrFallos)>0){
                $this->atVista->assign('persSacionados', true);
            }
        }
        //Se verifica si la actividad tiene una prórroga sin aprobar
        if($this->atProcesaActModelo->metObtenerProrroga($arrData['pk_num_actividad_planific'])){
            $this->atVista->assign('prorrogaSinAprobar', 1);//NO permite terminar la actividad.
        }else{
            $this->atVista->assign('prorrogaSinAprobar', 0);//Permite terminar la actividad.
        }
       // print_r($this->atVista);
        $this->atVista->metRenderizar('procesar','modales');
    }
    /**
     * Procesa la actividad actual a terminada y culmina las planificaciones como 'Completadas' siempre y cuando sea la última actividad a procesar
     * ó culmina las planificaciones como 'Terminadas' siempre y cuando sea la última actividad afecta y tenga actividades no afectas por procesar.
     */
    public function metProcesarActividad(){
        $valido = $this->metObtenerInt('valido');
        if($valido==1) {
            $arrRetornar = array(); $dataValida = array(); $arrRetornar['resultado'] = false;
            $exceccionInt = array('idPlanifPadre','esUltimaActividad','cantDiasProrroga');
            $arrInt = $this->metValidarFormArrayDatos('form', 'int', $exceccionInt);
            $exceccionTxt = array('ind_observacion','ind_nota_justificacion');
            $arrTxt = $this->metValidarFormArrayDatos('form', 'txt', $exceccionTxt);
            $arrParams = array_merge($arrTxt, $arrInt);
            $arrRetornar["Error"] = "error";
            if ($this->atFuncGnrles->metComparaFechas($arrParams["fec_terminado"],$arrParams["fec_ini"],'esMenor')) {                
                $arrRetornar['mensaje'] = "La 'Fecha Terminado' no puede ser menor a la 'Fecha Inicio Real'";
                $arrRetornar["fec_terminado"] = "error";
            }elseif ($this->atFuncGnrles->metComparaFechas($arrParams["fec_terminado"], $arrParams["fec_fin"],'esMayor')) {
                $arrRetornar['mensaje'] = "La 'Fecha terminado' no puede ser mayor a la 'Fecha Término Real'";
                $arrRetornar["fec_terminado"] = "error";
            }elseif ($arrParams["culminarProcesoDe"]=="DV" AND !$arrParams["ind_nota_justificacion"]) {
                $arrRetornar['mensaje'] = "Por favor ingrese la nota de justificación";
                $arrRetornar["ind_nota_justificacion"] = "error";
            }else{
                unset($arrRetornar["Error"]);
                //Se determina los días de adelanto en los cuales se termina la actividad.
                $arrParams["cant_diasAdelanto"]=0;
                if($arrParams["num_CantDiasCulmina"]==$arrParams["num_LapsoEjecucion"]){
                    $arrParams["cant_diasAdelanto"]=$arrParams["num_LapsoEjecucion"]-1;
                }else{
                    $arrParams["cant_diasAdelanto"]=$arrParams["num_LapsoEjecucion"]-$arrParams["num_CantDiasCulmina"];
                }

                if(!isset($arrParams["ind_observacion"])){
                    $arrParams["ind_observacion"]=null;
                }
                $arrParams["fec_reg_cierre"] = $this->atFuncGnrles->metFormateaFechaMsql($arrParams["fec_reg_cierre"]);
                $arrParams["fec_terminado"] = $this->atFuncGnrles->metFormateaFechaMsql($arrParams["fec_terminado"]);
                //CULMINAR PROCESOS (ACTIVIDADES Y PLANIFICACIONES)
                $arrParams["estadoActvidad"] = "TE";
                if($arrParams["esUltimaActividad"]){//Si es la última actividad, Entra.
                    //Se determina el estado de la planificación para culminar
                    if($arrParams["culminarProcesoDe"]=="CGR"){
                        $arrParams["estadoPlanificacion"]="EV"; //Enviada a la CGR
                    }elseif($arrParams["culminarProcesoDe"]=="VJPA"){
                        $arrParams["estadoPlanificacion"]="VJ"; //Enviada a Determinación de Responsabilidad (valoración jurídica)
                    }elseif($arrParams["culminarProcesoDe"]=="AA"){
                        $arrParams["estadoPlanificacion"]="AA"; //Auto de archivo (valoración preliminar y Jurídica)
                    }elseif($arrParams["culminarProcesoDe"]=="AC"){
                        $arrParams["estadoPlanificacion"]="AC"; //Auto de proceder (valoración preliminar)
                    }elseif($arrParams["culminarProcesoDe"]=="AI"){
                        $arrParams["estadoPlanificacion"]="AI"; //Auto de Inicior (valoración Jurídica)
                    }elseif($arrParams["culminarProcesoDe"]=="DV"){
                        $arrParams["estadoPlanificacion"]="DV"; //Devolución de planificación (valoración Jurídica)
                        $arrParams["flagSituacion"]="DV";
                        $arrParams["IdEmpleadoUser"]=$this->atIdEmpleadoUser;
                    }else{//Actuación fiscal, Potestad investigativa, Procedimientos Administrativos (DDR)
                        $arrParams["estadoPlanificacion"]="CO";
                    }
                    $arrRetornar['mensaje'] = "Ocurrió algo inesperado. Intente de nuevo o más tarde por favor";
                    if($arrParams["culminarProcesoDe"]=="DV"){
                        //Culmina la planificación actual como 'DEVUELTA', termina su última ctividad y actualiza la planificación padre como 'Devuelta'.
                        if($this->atProcesaActModelo->metProcesaDevolucion($arrParams)){
                            $arrRetornar['resultado'] = true; $arrRetornar['titulo'] = "Planificación Devuelta";
                            $arrRetornar['mensaje'] = "La planificación se ha culminado exitosamente";
                        }
                    }else{
                        //Culmina una planificación atual como 'Completada' y termina su última ctividad.
                        if($this->atProcesaActModelo->metCompletarPlanificacion($arrParams)){
                            $arrRetornar['resultado'] = true; $arrRetornar['titulo'] = "Planificación Completada";
                            $arrRetornar['mensaje'] = "La planificación se ha Culminado exitosamente";
                        }
                    }
                }else{//De lo contrario, entra para terminar la actividad en cuestión y la planificación como 'Terminada' si fuera el caso.
                    $arrRetornar['resultado'] = true;
                    if($arrParams["actividad_afectaPlan"]=="S"){//Si la Actividad es del tipo que afecta a la planificación. Entra.
                        $arrParams["actividad_afectaPlan"]="TE";
                        //Se verifica si existen mas actividades que afectan la planificación.
                        if(!$this->atProcesaActModelo->metActividadesAfecta($arrParams["idPlanificacion"],$arrParams["numSecuenciaActividad"])){
                            $arrParams["estadoPlanificacion"]="TE";
                            if($this->atProcesaActModelo->metCulminaPlanificacionYactividad($arrParams)){
                                $arrRetornar['resultado'] = true; $arrRetornar['titulo'] = "Planificación Terminada";
                                $arrRetornar['mensaje'] = "Aún quedan actividades no afectas";
                            }else{
                                $arrRetornar['mensaje'] = "Ocurrió algo inesperado. Intente de nuevo o más tarde por favor";
                            }
                        }else{//Sólo termina la actividad en cuestión.
                            if($this->atProcesaActModelo->metTerminaActividad($arrParams)){
                                $arrRetornar['resultado'] = true; $arrRetornar['titulo'] = "Actividad Terminada";
                                $arrRetornar['mensaje'] = "La operación se efectuó exitosamente";
                            }else{
                                $arrRetornar['mensaje'] = "La actividad no se pudo terminar. Intente de nuevo o más tarde por favor";
                            }
                        }
                    }else{//Termina la actividad del tipo no afecta a la planificación.
                        if($this->atProcesaActModelo->metTerminaActividad($arrParams)){
                            $arrRetornar['resultado'] = true; $arrRetornar['titulo'] = "Actividad Terminada";
                            $arrRetornar['mensaje'] = "La operación se efectuó exitosamente";
                        }else{
                            $arrRetornar['mensaje'] = "La actividad no se pudo terminar. Intente de nuevo o más tarde por favor";
                        }
                    }
                }
            }
            echo json_encode($arrRetornar);
        }//Fin Válido
    }
    /**
     * Determina la cantidad de días al cierre de una actividad
     * @return int
     */
    public function metDiasAlCierreDeLaActividad(){
        $fechaIni=date_format(date_create($_POST["fec_ini"]),'Y-m-d');
        if(isset($_POST["fec_terminado"]) AND !empty($_POST["fec_terminado"])){
            $fechaFin=date_format(date_create($_POST["fec_terminado"]),'Y-m-d');
            $diasCierre=$this->atFuncGnrles->metObtenerDiasHabiles($fechaIni,$fechaFin);
            echo  json_encode($diasCierre);
        }else{
            $fechaFin=date_format(date_create($_POST["form"]["alphaNum"]["fec_terminado"]),'Y-m-d');
            $diasCierre=$this->atFuncGnrles->metObtenerDiasHabiles($fechaIni,$fechaFin);
            return $diasCierre;
        }
    }

    /************************PROCESO DE SANCIÓN A PERSONAS - DETERMINACIÓN DE RESPONSABILIDADES*************************/
    public function metMontaFormDecision(){
        $arrTipoDecision=$this->atProcesaActModelo->metTipoDecisiones();
        $this->atVista->assign('listaTipoDecision', $arrTipoDecision);
        $this->atVista->metRenderizar('formDecision');
    }
    /**
     * Lista las personas a objeto de sanción y los presenta en su respectivo modal
     */
    public function metCargaModalPersonas(){
        $listaResponsables=$this->atProcesaActModelo->metListarResponsables();
        $this->atVista->assign('listaResponsables', $listaResponsables);
        $this->atVista->metRenderizar('seleccionaResponsable', 'modales');
    }
    /**
     * Busca las personas sancionadas de una determinación de responsabilidad para montarlas en su correspondiente grilla.
     * @throws Exception
     */
    public function metBuscaPersSancionadas(){
        $idPlanificacion=$this->metObtenerInt('idPlanificacion');
        $formOrigen=$this->metObtenerTexto('formOrigen');
        $arrSaciones=$this->atProcesaActModelo->metPersonasSanciondas($idPlanificacion);
        if(COUNT($arrSaciones)>0){
            $js[] = 'modPF/DataTable2Pf';
            $this->atVista->metCargarJs($js);
            $arrSaciones=$this->atFuncGnrles->metAgrupaArray($arrSaciones, 'nombre_persona','cedula_pers','&&');
            $this->atVista->assign('listaSancionados', $arrSaciones);
            if($formOrigen=='Decision'){ //Entra cuando viene del form Decisión del caso de uso "Ejecutar Actividades"
                $this->atVista->metRenderizar('grillaSancionados');
            }else{ //Entra cuando viene de listar "Listar Detalle Actividades"
                $this->atVista->metRenderizar('grillaPersSancion');
            }
        }else{
            echo false;
        }
    }
    /**
     * Guarda las sanciones de una determinada persona en el proceso de Determinación de Responsabilidad.
     */
    public function metGuardaDecision(){
        $arrParams=array();
        $excepcion = array('cantCampos','intMontos','intMontoReparo');
        $arrInd = $this->metValidarFormArrayDatos('form', 'int',$excepcion);
        $arrData=array();
        if (isset($arrInd['chk_decision'])) {
            if(isset($arrInd['intMontos'])){
                $arrMontos=$arrInd['intMontos'];
            }else{
                $arrMontos=0;
            }
            $arrCatCampos=$arrInd['cantCampos'];
            if(isset($arrInd['intMontoReparo'])){
                $arrMontoReparo=$arrInd['intMontoReparo'];
            }else{
                $arrMontoReparo=0;
            }
            foreach($arrInd['chk_decision'] as $idTipoDecision){
                if($arrCatCampos[$idTipoDecision]!=0){
                    if($arrCatCampos[$idTipoDecision]==2){
                        if($arrMontos[$idTipoDecision]==0 OR $arrMontoReparo[$idTipoDecision]==0){
                            $arrData=null;
                            $arrRetornar=array("resultado"=>false,"mensaje"=>"Debes ingresar los montos de la decisión seleccionada");
                            break;
                        }else{
                            $arrData[]=array("idPlanificacion"=>$arrInd['idPlanificacionSancion'],"idPersona"=>$arrInd['idPersona'],
                                "idTipo_decision"=>$idTipoDecision,"monto"=>$this->atFuncGnrles->metConsolidaMontoBd($arrMontos[$idTipoDecision]),
                                "montoReparo"=>$this->atFuncGnrles->metConsolidaMontoBd($arrMontoReparo[$idTipoDecision]));
                        }
                    }elseif($arrMontos[$idTipoDecision]==0){
                        $arrData=null;
                        $arrRetornar=array("resultado"=>false,"mensaje"=>"Debes ingresar el monto de la decisión seleccionada");
                        break;
                    }else{
                        if(isset($arrMontoReparo[$idTipoDecision])){
                            $montoReparo=$arrMontoReparo[$idTipoDecision];
                        }else{
                            $montoReparo=0;
                        }
                        $arrData[]=array("idPlanificacion"=>$arrInd['idPlanificacionSancion'],"idPersona"=>$arrInd['idPersona'],
                            "idTipo_decision"=>$idTipoDecision,"monto"=>$this->atFuncGnrles->metConsolidaMontoBd($arrMontos[$idTipoDecision]),
                            "montoReparo"=>$this->atFuncGnrles->metConsolidaMontoBd($montoReparo));
                    }
                }else{
                    $monto=0;$montoReparo=0;
                    $arrData[]=array("idPlanificacion"=>$arrInd['idPlanificacionSancion'],"idPersona"=>$arrInd['idPersona'],
                        "idTipo_decision"=>$idTipoDecision,"monto"=>$monto,"montoReparo"=>$montoReparo);
                }
            }
        }else{
            $arrRetornar=array("resultado"=>false,"mensaje"=>"Debes seleccionar una decisión");
        }
        if(COUNT($arrData)>0){
            //Valida que la persona a sancionar no tenga la misma sanción.
            $procesarSancion=true;
            foreach($arrData as $fila){
                $arrValidaReg=$this->atProcesaActModelo->metValidaSancion($fila);
                if($arrValidaReg){
                    $procesarSancion=false;
                    $arrRetornar=array("resultado"=>false,"nombre_decision"=>$arrValidaReg["nombre_decision"],"mensaje"=>"Esa persona ya tiene la sanción: ".$arrValidaReg["nombre_decision"]);
                    break;
                }
            }
            if($procesarSancion){//Guarda la sanción o sanciones.
                $result=$this->atProcesaActModelo->metIngresaSancion($arrData);
                if($result){
                    $arrRetornar=array("resultado"=>$result,"mensaje"=>"El proceso se efectuó exitosamente");
                }else{
                    $arrRetornar=array("resultado"=>false,"mensaje"=>"El proceso no se efectuó. Intente más tarde por favor");
                }
            }
            $arrValidaReg=null;
        }
        echo json_encode($arrRetornar);
    }
    /**
     * Elimina una sanción correspondiente a una persona de una Determinacion de Responsabilidad
     */
    public function metEliminaSancion(){
        $idDecision=$this->metObtenerInt('idDecision');
        if($this->atProcesaActModelo->metEliminaSancion($idDecision)){
            $arrRetornar=array("reg_afectado"=>true,"mensaje"=>"El proceso se efectuó exitosamente");
        }else{
            $arrRetornar=array("reg_afectado"=>false,"mensaje"=>"El proceso no se efectuó. Intente más tarde por favor");
        }
        echo json_encode($arrRetornar);
    }
    /***************************PROCESO DE ADJUNTAR ARCHIVOS A UNA ACTIVIDAD EN PARTICULAR*****************************/
    /**
     * Monta el formulario de adjunatar documentos tanto en Ejecutar actividades como en Detalles Actividades
     */
    public function metMontaFormAdjuntaDoc(){
        $adjuntarArchivos=$this->metObtenerInt('adjuntarArchivos');
        if($adjuntarArchivos==0){
            $adjuntarArchivos=false;
        }
        $this->atVista->assign('adjuntarArchivos', $adjuntarArchivos);
        $this->atVista->metRenderizar('formAdjuntaDoc');
    }
    /**
     * Se sube el archivo y se registra los metadatos del documento a adjuntar a una actividad.
     */
    public function metSubeArchivos(){
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        // Límite  de 5 minutos de tiempo de ejecución
        @set_time_limit(5 * 60);
        $cleanupTargetDir = false; // Para remover los archivos viejos de la carpeta temporal.
        $maxFileAge = 5 * 3600; // Temp file age in seconds
        //SE crea la carpta contenedora de archivos
        $rutaGuardar='';
        $rutaCarpRaiz=ROOT . 'publico' . DS . 'documentos';
        if($this->atFuncGnrles->metValidaCarpeta($rutaCarpRaiz)){
            $rutaCarpModulo=$rutaCarpRaiz . DS . 'modPF';
            $rutaGuardar='documentos'. DS . 'modPF';
        }
        if($this->atFuncGnrles->metValidaCarpeta($rutaCarpModulo)){
            $rutaCarpDestino=$rutaCarpRaiz . DS . 'modPF' . DS . 'DOCPF'.date('Y');
            $rutaGuardar.= DS . 'DOCPF'.date('Y'). DS;
        }
        if($this->atFuncGnrles->metValidaCarpeta($rutaCarpDestino)){
            $targetDir=$rutaCarpDestino . DS;
            $rutaCarpDestino=$targetDir;

        }else{
            echo false; exit;
        }
        // Se optiene un nombre de archivo
        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }
        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        // Chunking might be enabled
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        // Remueve los archivos temporales. Nota: Inhabilitado.
        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "No se pudo abrir el directorio temporal."}, "id" : "id"}');
            }
            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;
                // If temp file is current file proceed to the next
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                // Remove temp file if it is older than the max age and is not the current file
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }
        // Se abre el archivo temporal
        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "No se pudo abrir la carpeta origen."}, "id" : "id"}');
        }
        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Error al mover el archivo subido."}, "id" : "id"}');
            }
            // Leer flujo de entrada binaria y anexarlo al archivo temporal
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "No se pudo abrir la carpeta destino."}, "id" : "id"}');
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "No se pudo abrir la carpeta destino."}, "id" : "id"}');
            }
        }
        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }
        @fclose($out);
        @fclose($in);
        // Comprueba si se ha cargado el archivo
        if (!$chunks || $chunk == $chunks - 1) {
            //Se eliminar el sufijo .part al renombrar el archivo temporal y luego se guarda los metadatos.
            //Se crea el nombre del archivo
            $fecha = date("dmY").substr((string) microtime(), 1, 8);
            $arFecha = explode('.', $fecha);
            $fecha_s = 'F'.$arFecha[0].'S'.$arFecha[1];
            $arrRuta=explode('/',$filePath);
            $cantElementos=$arrRuta[COUNT($arrRuta)-1];
            $arrArchivo=explode('.',$cantElementos);
            $extArchivo=$arrArchivo[COUNT($arrArchivo)-1];
            $prefijo=$this->atFuncGnrles->metSiglaTipoProceso($_REQUEST["tipoProceso"]);
            $rutaNombreArchivo = $rutaCarpDestino.$prefijo.'_'.$_REQUEST["idPlanificacion"].'_'.$fecha_s.'.'.$extArchivo;
            //ruta y nombre de archivo
            $rutaGuardar.=$prefijo.'_'.$_REQUEST["idPlanificacion"].'_'.$fecha_s.'.'.$extArchivo;
            //Se renombra el archivo
            if(rename("{$filePath}.part", $rutaNombreArchivo)){
                if(!$_REQUEST["txt_nombreDoc"]){
                    chmod($rutaNombreArchivo, 0777); unlink($rutaNombreArchivo);
                    die('{"jsonrpc" : "2.0", "result" : null, "message": "Ingrese el nombre del documento por favor", "id" : "id"}');
                }elseif(!$_REQUEST["txt_fechaDoc"]){
                    chmod($rutaNombreArchivo, 0777); unlink($rutaNombreArchivo);
                    die('{"jsonrpc" : "2.0", "result" : null, "message": "Ingrese la fecha del documento por favor", "id" : "id"}');
                }else{
                    $arrParams=array("idActividadPlanif"=>$_REQUEST["idActividadPlanif"],"txt_nombreDoc"=>$_REQUEST["txt_nombreDoc"],
                        "txt_nroDoc"=>$_REQUEST["txt_nroDoc"],"txt_fechaDoc"=>$this->atFuncGnrles->metFormateaFechaMsql($_REQUEST["txt_fechaDoc"]),
                        "rutaDoc"=>$rutaGuardar);
                    //Se registra los metadatos del archivo ajunto a la actividad correspondiente
                    $result=$this->atProcesaActModelo->metGuardaDatosArchivo($arrParams);
                    if($result){
                        die('{"jsonrpc" : "2.0", "result" : 1, "message": "El proceso se ejecutó exitosamente", "id" : "id"}');
                    }else{
                        chmod($rutaNombreArchivo, 0777); unlink($rutaNombreArchivo);
                        die('{"jsonrpc" : "2.0", "result" : null, "message": "El proceso no se efectuó. Intente de nuevo mas tarde", "id" : "id"}');
                    }
                }
            }else{
                chmod("{$filePath}.part", 0777); unlink("{$filePath}.part");
                die('{"jsonrpc" : "2.0", "result" : null, "message": "El proceso no se efectuó. Intente de nuevo mas tarde", "id" : "id"}');
            }
        }else{
            chmod("{$filePath}.part", 0777); unlink("{$filePath}.part");
            die('{"jsonrpc" : "2.0", "result" : null, "message": "El proceso no se efectuó. Intente de nuevo mas tarde", "id" : "id"}');
        }
    }
    /**
     * Busca y monta los archivos adjuntos en su respectiva grilla
     * @throws Exception
     */
    public function metListarArchivosAdjuntos(){
        $idActividadPlanif=$this->metObtenerInt('idActividadPlanif');
        $adjuntarArchivos=$this->metObtenerInt('adjuntarArchivos');
        if($adjuntarArchivos==0){
            $adjuntarArchivos=false;
        }
        $this->atVista->assign('adjuntarArchivos', $adjuntarArchivos);
        $arrAdjuntos=$this->atProcesaActModelo->metBuscaArchivosAdjuntos('por_idActividadPlanif',$idActividadPlanif);
        $data=array();
        if(COUNT($arrAdjuntos)>0){
            $js[] = 'modPF/DataTable2Pf';
            $this->atVista->metCargarJs($js);
            $rutaCarpRaiz='publico' . DS;
            foreach($arrAdjuntos as $fila){
                $fila["ruta_doc"]=$rutaCarpRaiz.$fila["ruta_doc"];
                $fila["fecha_doc"]=$this->atFuncGnrles->metFormateaFecha($fila["fecha_doc"]);
                $data[]=$fila;
            }
            $this->atVista->assign('listaAdjuntos', $data);
            $this->atVista->metRenderizar('grillaAdjuntos');
        }else{
            echo false;
        }
    }
    /**
     * Elimina el registro y el archivo adjunto de una actividad.
     * @throws Exception
     */
    public function metEliminarArchivoAdjunto(){
        $idDocumento=$this->metObtenerInt('idDocumento');
        $result=$this->atProcesaActModelo->metBuscaArchivosAdjuntos('por_idDocumento',$idDocumento)[0];
        if($result){
            $js[] = 'modPF/DataTable2Pf';
            $this->atVista->metCargarJs($js);
            $rutaArchivo=ROOT . 'publico' . DS .$result["ruta_doc"]; $result=null;
            if($this->atProcesaActModelo->metEliminaArchivosAdjuntos($idDocumento)){
                if (file_exists($rutaArchivo)) { //Elimina el archivo
                    @chmod($rutaArchivo,0777); unlink($rutaArchivo);
                }
                $arrRetornar=array("reg_afectado"=>true,"mensaje"=>"El proceso se ejecutó exitosamente");
            }else{
                $arrRetornar=array("reg_afectado"=>false,"mensaje"=>"El proceso no se efectuó. Intente de nuevo por favor");
            }
        }else{
            $arrRetornar=array("reg_afectado"=>false,"mensaje"=>"El proceso no se efectuó. Intente de nuevo por favor");
        }
        echo json_encode($arrRetornar);
    }
    /**********************************************DETALLES DE ACTIVIDADES************************************************/
    /**
     * Busca las planificaciones para montarlas en la grilla del form principal "Listar Detalles Actividades" .
     * @throws Exception
     */
    public function metBuscaPlanificaciones(){
        $arrListado=array();
        //Se extrae las planificaciones de acuerdo al tipo de usuario
        if($this->atTipoUser=='userCtrolFcal'){//Entra, cuando es un usuario de una dependencia de control
            $arrData = $this->atProcesaActModelo->metBuscarPlanificaciones('por_listado',array("IdTipoProceso"=>$this->atIdTipoProceso,"IdDependenciaCtrol"=>$this->atIdDependenciaUser));
        }elseif($this->atTipoUser=='userInvitado'){//Entra, cuando es un usuario invitado
            $arrData=array();
            //Se busca las dependencias de control al cual el usuario está como invitado
            $listaDepInvitado = $this->atFuncGnrles->metBuscaDepControlInvitado($this->atIdContraloria,$this->atIdTipoProceso);
            foreach($listaDepInvitado as $fila){//se extrae los registros de acuerdo a las dependencias de control en la cuales el usuario está como invitado
                $result = $this->atProcesaActModelo->metBuscarPlanificaciones('por_listado',array("IdTipoProceso"=>$this->atIdTipoProceso,"IdDependenciaCtrol"=>$fila["id_dependencia"]));
                if(COUNT($result)>0){
                    $arrData=array_merge($arrData,$result);
                }
            }
        }
        if(COUNT($arrData)>0){
            //Se prepara la data a listar
            $arrListado=array();
            foreach($arrData as $fila){
                $fila['ind_estado']=$fila['ind_estado_planificacion']; unset($fila['ind_estado_planificacion']);
                $arrEnte=$this->atFuncGnrles->metBuscaRecursivoEntes($fila['fk_a039_num_ente']);
                $fila['nombre_ente']=$arrEnte['cadEntes'];
                $fila['desc_estado']=$this->atFuncGnrles->metEstadoPlanificacion($fila['ind_estado']);
                $fila['fec_inicio']=$this->atFuncGnrles->metFormateaFecha($fila['fec_inicio']);
                $fila['fec_registro']=$this->atFuncGnrles->metFormateaFecha($fila['fec_registro_planificacion']); unset($fila['fec_registro_planificacion']);
                $fila['fec_ultima_modificacion']=$this->atFuncGnrles->metFormateaFecha($fila['fec_ultima_modific_planificacion']); unset($fila['fec_ultima_modific_planificacion']);
                $arrFechasfin=$this->atFuncGnrles->metBuscaFechaFinPlan($fila['pk_num_planificacion']);
                $fila['fec_termino']=$arrFechasfin['fecha_finplan'];
                $fila['fec_termino_real']=$arrFechasfin['fecha_finrealplan'];
                $arrListado[]=$fila;
            }
        }
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado', $arrListado);
        $this->atVista->metRenderizar('listadoConsulta');
    }
    /**
     * Monta el formulario de "Listado Detalles Actividades"
     * @throws Exception
     */
    public function metIndex2(){
        $this->atVista->assign('tipoProceso', $this->atIdTipoProceso);
        $this->atVista->metRenderizar('listadoActuacion');
    }
    public function metBuscaActividades(){
        $js[] = 'modPF/DataTablePf';
        $this->atVista->metCargarJs($js);
        $idPlanificacion=$this->metObtenerInt('idPlanificacion');
        $result=$this->atProcesaActModelo->metActividadesAsignadas($idPlanificacion);

        if(COUNT($result)>0) {
            foreach ($result as $fila) {
                $fila['fec_inicio_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_inicio_actividad']);
                $fila['fec_inicio_real_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_inicio_real_actividad']);
                $fila['fec_culmina_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_culmina_actividad']);
                $fila['fec_culmina_real_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_culmina_real_actividad']);
                $arrData[]=$fila;
            }
            $arrData=$this->atFuncGnrles->metAgrupaArray($arrData,'txt_descripcion_fase','cod_fase');
            $this->atVista->assign('listaActividades', $arrData);
            $this->atVista->assign('contador', 0);
            $this->atVista->assign('SubTotal_dias', 0);
            $this->atVista->assign('Total_dias', 0);
            $this->atVista->assign('totalDiasNoAfecta', 0);
            $this->atVista->assign('SubTotal_diasProrroga', 0);
            $this->atVista->assign('Totaldias_prorroga', 0);
            $this->atVista->assign('totalGeneralDias', 0);
            $this->atVista->assign('fecha_fin_plan', '');
            $this->atVista->metRenderizar('grillaActividades');
        }else{
            echo "";
        }
    }
    /**
     * Visualiza el form modal de detalles de la planificación y sus actividades al hacer click en el botón respectivo de una planificación.
     */
    public function metDetalleActividades(){
        $mostrarBoton=false;
        $idPlanificacion = $this->metObtenerInt('idActuacion');
        $this->metComplementosForm();
        $js[] = 'modPF/DataTable2Pf';
        $this->atVista->metCargarJs($js);
        //Se busca la planificación a montar en el modal "Detalles Actividades"
        $arrData=$this->atProcesaActModelo->metBuscarPlanificaciones('por_idPlanificacion',array("idPlanificacion"=>$idPlanificacion))[0];
        $this->atVista->assign('idPlanificacion', $idPlanificacion);
        $estadoPlanif=$arrData['ind_estado_planificacion'];
        if($estadoPlanif!='AP' AND $estadoPlanif!='TE'){
            $adjuntar=false;
        }else{
            $adjuntar=true;
        }
        $this->atVista->assign('adjuntarArchivos', $adjuntar);
        /*Lista la contraloría que ejecuta*/
        $campos="pk_num_organismo AS id_contraloria,ind_descripcion_empresa AS nombre_contraloria";
        $listaContraloria=$this->atFuncGnrles->metBuscaOrganismos($campos,$this->atIdContraloria,1,'I');
        $this->atVista->assign('listaContraloria', $listaContraloria);
        /*Se lista las dependencias de control de acuerdo al tipo de usuario*/
        if($this->atTipoUser=='userCtrolFcal'){
            $listaDepContraloria[0]=$this->atFuncGnrles->metBuscaDepControlUsuario($this->atIdDependenciaUser);
        }elseif($this->atTipoUser=='userInvitado'){
            $listaDepContraloria = $this->atFuncGnrles->metBuscaDepControlInvitado($this->atIdContraloria);
        }
        $this->atVista->assign('listaDepContraloria', $listaDepContraloria);
        /*Lista de centros de costo*/
        if($arrData['fk_a004_num_dependencia']){
            $campos="pk_num_centro_costo,ind_descripcion_centro_costo";
            $listaCentroCosto=$this->atFuncGnrles->metBuscaDepCentroCostos($campos,'',$arrData['fk_a004_num_dependencia'],'');
            if (COUNT($listaCentroCosto) > 0) {
                $this->atVista->assign('listaCentroCosto', $listaCentroCosto);
            } else {
                $this->atVista->assign('listaCentroCosto', array());
            }
        }else{
            $this->atVista->assign('listaCentroCosto', array());
        }
        /*Lista los tipo de actuación fiscal*/
        $campos="pk_num_miscelaneo_detalle AS idtipo_actuacion,ind_nombre_detalle AS nombretipo_actuacion";
        $listaTipoActuacion=$this->atFuncGnrles->metBuscaMiscelanio($campos,'PFTAF','');
        $this->atVista->assign('listaTipoActuacion', $listaTipoActuacion);
        /*Busca el ente externo a objeto de fiscalización*/
        $arrEnte = $this->atFuncGnrles->metBuscaRecursivoEntes($arrData['fk_a039_num_ente']);
        $arrData['nombre_ente'] = $arrEnte['cadEntes'];
        //$arrTitular=$this->atFuncGnrles->metBuscaPersonaEnte('',$arrData['fk_a041_num_persona_ente']);
        /*Lista los origen de actuación*/
        $campos="pk_num_miscelaneo_detalle AS idorigen_actuacion,ind_nombre_detalle AS nombre_origenactuacion";
        $listaOrigenActuacion=$this->atFuncGnrles->metBuscaMiscelanio($campos,'PFODA','');
        $this->atVista->assign('listaOrigenActuacion', $listaOrigenActuacion);
        //Se busca los totales y fecha fin real de la planificación
        $arrTotalesPlan=$this->atFuncGnrles->metBuscaTotalesPlan($idPlanificacion);
        $arrData=array_merge($arrData,$arrTotalesPlan);
        //Se busca la descripción del estado de la planificación
        $arrData['desc_estado']=$this->atFuncGnrles->metEstadoPlanificacion($estadoPlanif);
        $arrData['estadoPlanif']=$estadoPlanif;
        //Se extraen los auditores designados
        $dataAuditores=array();
        $result=$this->atProcesaActModelo->metAuditoresDesignados(array("idPlanificacion"=>$idPlanificacion));//Se extraen los auditores designados
        if(COUNT($result)>0) {//Se prepara los datos para presentarlos en la vista en la grilla de auditores designados.
            foreach ($result as $fila) {
                $camposExtraer="ind_cedula_documento AS cedula_auditor,CONCAT_WS(' ',ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2) AS nombre_auditor,ind_nombre_cargo AS cargo_auditor";
                $arrAuditor=$this->atFuncGnrles->metBuscaEmpledos($camposExtraer,$fila['fk_rhb001_num_empleado']);
                $dataAuditores[] = array("id_auditorPlanificacion"=>$fila['pk_num_auditor_planificacion'],
                    "pk_num_empleado"=>$fila['fk_rhb001_num_empleado'], "nombre_auditor"=>$arrAuditor[0]['nombre_auditor'],
                    "cargo_auditor" =>$arrAuditor[0]['cargo_auditor'], "flagCordinador"=>$fila['num_flag_coordinador'],
                    "desc_estatus"=>$this->atFuncGnrles->metEstatus($fila['num_estatus']),
                    "fecha_estatus"=>$this->atFuncGnrles->metFormateaFecha($fila['fec_estatus']));
            }
        }
        $this->atVista->assign('auditoresDesignados', $dataAuditores); $result=null;
        //Se busca los tipo de procesos de control fiscal
        $procesos=$this->atFuncGnrles->metTipoProcesoFiscal();
        $arrayProcesos='';
        $i=0;
        foreach ($procesos AS $proceso){
            $arrayProcesos[$i++]=$proceso['pk_num_proceso'];
        }
        $this->atVista->assign('AF', $arrayProcesos[0]);
        $this->atVista->assign('VP', $arrayProcesos[1]);//Valoración Preliminar
        $this->atVista->assign('PI', $arrayProcesos[2]);
        $this->atVista->assign('VD', $arrayProcesos[3]);//Valoración Jurídica
        $this->atVista->assign('DR', $arrayProcesos[4]);
        //Se determina mostrar la pestaña de Decisiones.
        $mostrarPanelDecision=false;
        if($this->atIdTipoProceso==5){// 5: Proceso de Procedimientos Administrativos de Determinación de Responsabilidad
            $result=$this->atProcesaActModelo->metDecisionDeterminacion($idPlanificacion);
            if($result){
                $mostrarPanelDecision=true; $result=null;
            }
        }
        $this->atVista->assign('panelDecision', $mostrarPanelDecision);
        //Se determina la posibilidad de Reversar la planificación
        $reversarPlanif=false;
        //Se verifica si tiene una planificación hija, es decir, que se haya creado otro proceso fiscal a partír de ella.
        if(!$this->atProcesaActModelo->metEsPadre($idPlanificacion)){
            $arrEstadosPlanif=array('CO','EV','AI','DV','AC','VJ'); //OJO revisar esto.
            if(in_array($estadoPlanif,$arrEstadosPlanif)){
                $reversarPlanif=true; //Permite mostrar el botón para reversar la planificación.
            }
        }
        $this->atVista->assign('tipoProceso', $this->atIdTipoProceso);
        $this->atVista->assign('actuacion', $arrData);
        $this->atVista->assign('reversarPlanif', $reversarPlanif);
        // Se carga la ventana modal
        $this->atVista->metRenderizar('detallesActividades', 'modales');
    }
    /**
     * Reversa una planificación culminada al estado anterior(aprodada ó terminada) y su última actividad al estado: 'en ejecución'.
     */
    public function metReversarPlanificacion(){
        $idPlanificacion=$this->metObtenerInt('idActuacion');
        $idPlanificacionPadre=$this->metObtenerInt('idPlanificacionPadre');
        $estadoPlanificActual=$this->metObtenerTexto('estadoPlanif');
        //Se determina el estado anterior de la planificacion actual: 'Aprobada' ó 'Terminada'
        $result=$this->atProcesaActModelo->metBuscaActividadPlanificacion('ulmina_actividad',array("idPlanificacion"=>$idPlanificacion));//Se extrae la última actividad
        if($result){
            $idActividadPlanific=$result["id_ActividadPlanif"];
            if($result["flag_afectaPlan"]=="S"){ //Si la actividad es del tipo afectas, entra
                $estadoPlanific="AP";
            }else{
                $result=$this->atProcesaActModelo->metBuscaActividadPlanificacion('por_actividadAfectas',array("idPlanificacion"=>$idPlanificacion));//Se busca una actividad del tipo afectas
                if($result){//Si se encuentra, entra
                    $estadoPlanific="TE";
                }else{
                    $estadoPlanific="AP";
                }
            }
        }else{
            $arrRetornar=array("resultado"=>false,"titulo"=>"Información del Sistema","mensaje"=>"Ocurrió algo inesperado. Intente más tarde por favor");
            echo json_encode($arrRetornar); exit;
        }
        if($estadoPlanificActual=='DV'){ //Entra cuando se trata de una planificación culminada como devuelta.
            $estadoPlaficPadre="VJ";$flag_situacionPadre="NR";
            //Se busca la planificación padre.
            $result=$this->atProcesaActModelo->metBuscarPlanificaciones('por_idPlanificacion',array("idPlanificacion"=>$idPlanificacionPadre));
            if(COUNT($result)>0){
                $arrPlanifPadre=$result[0];
                //Se determina si a la planificación devuelta ya le fué generada una sub planificación por la dependencia correspondiente
                if($arrPlanifPadre['ind_flag_situacion']=='RP'){
                    $arrRetornar=array("resultado"=>false,"titulo"=>"¡Imposible de Revertír!","mensaje"=>"La planificación devuelta ya le fué generada una sub planificación");
                    echo json_encode($arrRetornar); exit;
                }
            }else{
                $arrRetornar=array("resultado"=>false,"titulo"=>"Información del Sistema","mensaje"=>"Ocurrió algo inesperado. Intente más tarde por favor");
                echo json_encode($arrRetornar); exit;
            }
            $params=array("idPlanificacionPadre"=>$idPlanificacionPadre,"estadoPlaficPadre"=>$estadoPlaficPadre,"flag_situacionPadre"=>$flag_situacionPadre,
                "idPlanificacion"=>$idPlanificacion,"estadoPlanif"=>$estadoPlanific,"flag_situacion"=>"NR","idActividadPlanific"=>$idActividadPlanific,"estadoActividad"=>"EJ");
            $result=$this->atProcesaActModelo->metReversaPlanificacion($params,'DV');//Se procesa el reverso de la planificación de devolución.
        }else{ //Entra por acá para las demás planificaciónes culminadas
            $params=array("idPlanificacion"=>$idPlanificacion,"estadoPlanif"=>$estadoPlanific,"flag_situacion"=>"NR","idActividadPlanific"=>$idActividadPlanific,"estadoActividad"=>"EJ");
            $result=$this->atProcesaActModelo->metReversaPlanificacion($params);//Se procesa el reverso de la planificación.
        }
        if($result){
            $arrRetornar=array("resultado"=>$result,"titulo"=>"Información del Proceso","mensaje"=>"El proceso se efectuó exitosamente");
        }else{
            $arrRetornar=array("resultado"=>false,"titulo"=>"Información del Proceso","mensaje"=>"El proceso falló. Intente más tarde por favor");
        }
        echo json_encode($arrRetornar);
    }
}