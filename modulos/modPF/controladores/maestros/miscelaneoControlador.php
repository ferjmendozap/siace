<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Planificación Fiscal
 * PROCESO: Ingreso y mantenmimiento de Misceláneos de Planificación fiscal
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          José Díaz                        |        diaz.jose@cmldc.gob.ve      |         0416-2124302           |
 * | 2 |          Alexis Ontiveros                 |     ontiveros.alexis@cmldc.gob.ve  |         0426-514.43.82         |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        29-07-2015       |         1.0        |
 * |               #2                      |        04-06-2017       |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . 'modPF' . DS . 'modelos' . DS . 'funcionesGenerales.php';
class miscelaneoControlador extends Controlador{
    private $atMiscelaneoModelo;
    private $atFuncGnrles;          //Para las Funciones generales
    private $atIdContraloria;       //Para el Id de la contraloría que ejecuta.
    private $atIdDependenciaUser;   //Para el Id de la dependencia al cual pertenece el usuario.
    private $atIdEmpleadoUser;      //Para el Id empleado del usuario que ejecuta.
    private $atTipoUser;            //Para el tipo de usuario logueado
    private $atUserValido;          //Contendrá la validéz del usuario
    private $atIdAplicacion;    //Para el id de la aplicación PF
    public function __construct(){
        parent::__construct();
        $this->atMiscelaneoModelo = $this->metCargarModelo('miscelaneo');
        $this->atFuncGnrles = new funcionesGenerales();
        //Se determina la validéz y el tipo de usuario.
        $arrUser=$this->atFuncGnrles->metValidaUsuario();
        if($arrUser['userValido']){//Entra, si es un Usuario Válido.
            $this->atTipoUser=$arrUser['tipoUser'];
            $this->atUserValido=$arrUser['userValido'];
            $this->atIdContraloria=$arrUser['pk_num_organismo'];
            $this->atIdEmpleadoUser=$arrUser['pk_num_empleado'];
            $this->atIdDependenciaUser=$arrUser['pk_num_dependencia'];
            $this->atIdAplicacion = $this->atFuncGnrles->metObtenerIdAplicacion();
        }else{//De lo contrario, usuario no válido
            $this->atUserValido=$arrUser['userValido'];
        }
        $arrUser=null;
    }
    public function metIndex(){
        if($this->atUserValido) {
            $this->atVista->metRenderizar('listado');
        }else{
            $this->atVista->assign('valido', $this->atUserValido);
            $this->atVista->metRenderizar('../../error');
        }
    }
    public function metListarMiscelaneos(){
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $listado = $this->atMiscelaneoModelo->metListarMiscelaneos();
        $this->atVista->assign('listadoMiscMaestro', $listado);
        $this->atVista->metRenderizar('resultadoListado');
    }
    /**
     * Ingresa ó modifica el misceláneo maestro y sus detalles
     * @throws Exception
     */
    public function metCrearModificar(){
        $complementosCss = array(
            'select2/select201ef',
        );
        $complementosJs = array(
            'select2/select2.min',
        );
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'modPF/funcionesModPf';
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $valido = $this->metObtenerInt('valido');
        if ($valido == 1) {
            $arrParams=array();
            $excepcion = array('idMiscelaneo','chk_estatusMaestro');//'idMiscelaneo',
            $arrTxt = $this->metValidarFormArrayDatos('form', 'txt');
            $arrAlphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');
            $arrInd = $this->metValidarFormArrayDatos('form', 'int', $excepcion);
            if (in_array('error', $arrTxt) OR in_array('error', $arrAlphaNum['codsDetalle']) OR in_array('error', $arrAlphaNum['nombresDetalle'])) {
                $arrParams=array_merge($arrTxt,$arrAlphaNum);
                $arrParams['Error'] = 'error';
                echo json_encode($arrParams);
                exit;
            }else{
                $arrParams=array_merge($arrTxt,$arrAlphaNum,$arrInd);
            }
            if (!isset($arrParams['chk_estatusMaestro'])) {
                $arrParams['chk_estatusMaestro'] = 0;
            }

            if (!isset($arrParams['chksEstatusDet'])) {
                $arrParams['chksEstatusDet'] = 0;
            }
            $arrParams['atIdAplicacion']=$this->atIdAplicacion;
            if ($arrParams['idMiscelaneo'] == 0) {//Ingresa misceléneos
                $result=$this->atMiscelaneoModelo->metValidaMiscMaestro($arrParams['txt_codMiscMaestro']);
                if(!$result){
                    $idMiscelaneo = $this->atMiscelaneoModelo->metCrearMiscelaneo($arrParams);
                    if (is_array($idMiscelaneo)) {
                        $arrParams['Error'] = 'errorSql';
                        $arrParams['mensaje'] = '¡Atención! el proceso no se pudo efectuar. Intente mas tarde por favor';
                    }else{
                        $arrParams['idMiscelaneo'] = $idMiscelaneo;
                        $arrParams['mensaje'] = 'El proceso se efectuó exitosamente';
                    }
                }else{
                    $arrParams['Error'] = 'errorSql';
                    $arrParams['mensaje'] = '¡Atención! El código Maestro que ingresó ya Existe';
                }
            } else {//Actualiza misceláneos
                $result = $this->atMiscelaneoModelo->metActualizarMiscelaneo($arrParams);
                if (!$result) {
                    $arrParams['Error'] = 'errorSql';
                    $arrParams['mensaje'] = '¡Atención! el proceso no se pudo efectuar. Intente mas tarde por favor';
                }else{
                    $arrParams['mensaje'] = 'El proceso se efectuó exitosamente';
                }
            }
            echo json_encode($arrParams);

        }else{//Monta los datos de los misceláneos para actualizar ó monta el form vacío para ingresar
            $idMiscelaneo=$this->metObtenerInt('idMiscelaneo');
            if($idMiscelaneo){
                $arrMiscMaestros = $this->atMiscelaneoModelo->metMostrarMiscelaneo($idMiscelaneo);//Extrae el misceláneo maestro
                $arrMiscDetalle = $this->atMiscelaneoModelo->metMostrarMiscDetalle($arrMiscMaestros['cod_maestro']); //Extrae los misceláneos detalles
                $this->atVista->assign('numero', 1);
                $this->atVista->assign('n', 0);
                $this->atVista->assign('formDB', $arrMiscMaestros);
                $this->atVista->assign('formDBDet', $arrMiscDetalle);
                $arrMiscMaestros=null; $arrMiscDetalle=null;
            }
            $this->atVista->assign('idMiscelaneo', $idMiscelaneo);
            $this->atVista->metRenderizar('crearModificar', 'modales');
        }
    }
}
