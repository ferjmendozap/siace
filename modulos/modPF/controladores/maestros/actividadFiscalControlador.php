<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: PLANIFICACIÓN FISCAL
 * PROCESO: REGISTRO Y MANTENIMIENTO DE ACTIVIDADES
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 3 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |        0426-5144382            |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #3                      |        08-08-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . 'modPF' . DS . 'modelos' . DS . 'funcionesGenerales.php';
class actividadFiscalControlador extends Controlador
{
    private $atActividadModelo;
    private $atFuncGnrles;          //Para las Funciones generales
    private $atIdContraloria;       //Para el Id de la contraloría que ejecuta.
    private $atIdDependenciaUser;   //Para el Id de la dependencia al cual pertenece el usuario.
    private $atIdEmpleadoUser;      //Para el Id empleado del usuario que ejecuta.
    private $atTipoUser;            //Para el tipo de usuario logueado
    private $atUserValido;          //Contendrá la validéz del usuario
    public function __construct()    {
        parent::__construct();
        $this->atActividadModelo=$this->metCargarModelo('actividadFiscal');
        $this->atFuncGnrles = new funcionesGenerales();
        //Se determina la validéz y el tipo de usuario.
        $arrUser=$this->atFuncGnrles->metValidaUsuario();
        if($arrUser['userValido']){//Entra, si es un Usuario Válido.
            $this->atTipoUser=$arrUser['tipoUser'];
            $this->atUserValido=$arrUser['userValido'];
            $this->atIdContraloria=$arrUser['pk_num_organismo'];
            $this->atIdEmpleadoUser=$arrUser['pk_num_empleado'];
            $this->atIdDependenciaUser=$arrUser['pk_num_dependencia'];
        }else{//De lo contrario, usuario no válido
            $this->atUserValido=$arrUser['userValido'];
        }
        $arrUser=null;
    }
    public function metIndex($lista=false){
        if($this->atUserValido) {
            $listaProceso = $this->atActividadModelo->metListarTipoProceso();/*Se busca lista de tipo de procesos*/
            $this->atVista->assign('listaProceso', $listaProceso);
            $this->atVista->metRenderizar('listado');
        }else{
            $this->atVista->assign('valido', $this->atUserValido);
            $this->atVista->metRenderizar('../../error');
        }
    }
    /**
     * Lista las fases al seleccionar un proceso
     */
    public function metListarFases(){
        $pkNumProceso = $this->metObtenerInt('idProceso');
        $idFase = $this->metObtenerInt('idFase');
        $arrfase=$this->atActividadModelo->metBuscarFases($pkNumProceso);
        $opcion = '<option value="">&nbsp;</option>';
        foreach($arrfase as $fila){
            if($fila['pk_num_fase']==$idFase){
                $opcion .='<option value="'.$fila['pk_num_fase'].'" selected>'.$fila['txt_descripcion_fase'].'</option>';
            }else{
                $opcion .='<option value="'.$fila['pk_num_fase'].'">'.$fila['txt_descripcion_fase'].'</option>';
            }
        }
        echo  $opcion;
    }
    /**
     * Busca y monta las actividades en la grilla al seleccionar un proceso ó fase.
     */
    public function metListarActividades(){
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $arrParams["idProceso"]=$this->metObtenerInt('idProceso');
        $arrParams["idFase"]=$this->metObtenerInt('idFase');
        $result=$this->atActividadModelo->metListarActividades($arrParams);
        if(count($result)>0){
            $this->atVista->assign('listado',$result);
            $this->atVista->metRenderizar('resultadoListado');
        }else{
            echo false;
        }
    }
    /**
     * Visualiza el form modal para ingresar un nuevo regitro ó actualizar.
     * @throws Exception
     */
    public function metCrearModificar(){
        $js[] = 'modPF/funcionesModPf';
        $this->atVista->metCargarJs($js);
        $valido=$this->metObtenerInt('valido');
        $idActividad=$this->metObtenerInt('idActividad');
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('txt_comentarios_actividad','ind_afecto_plan');
            $alphaNum=$this->metValidarFormArrayDatos('form','txt',$Excceccion);
            $Excceccion2=array('idActividad','num_estatus');
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion2);
            if($alphaNum!=null && $ind==null){
                $arrParams=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $arrParams=$ind;
            }else{
                $arrParams=array_merge($alphaNum,$ind);
            }
            if(in_array('error',$arrParams)){
                $arrParams['status']='error';
                echo json_encode($arrParams);
                exit;
            }
            if(isset($arrParams['ind_afecto_plan'])=='on'){
                $arrParams['ind_afecto_plan']='N';
            }else{
                $arrParams['ind_afecto_plan']='S';
            }
            if(isset($arrParams['num_estatus'])=='on'){
                $arrParams['num_estatus']=1;
            }else{
                $arrParams['num_estatus']=0;
            }
            $idActividad=$arrParams['idActividad']; $error="";
            if($idActividad==0){/*SE INGRESA UN NUEVO REGISTRO*/
                $result=$this->atActividadModelo->metValidaActividad('valida_ingreso',$arrParams);
                if(COUNT($result)==0) {
                    /*Se busca el código de la fase fiscal*/
                    $result = $this->atFuncGnrles->metBuscaRegistro('pf_b003_fase', 'pk_num_fase', $arrParams['pk_num_fase']);
                    if(COUNT($result)>0) {
                        $arrFase = $result[0];
                        //Se determina el correlativo de la actvidad.
                        $codActividad = $this->atFuncGnrles->metGeneraCodigo('pf_b004_actividad', 'cod_actividad', 2, 'fk_pfb003_num_fase', $arrParams['pk_num_fase']);
                        if ($codActividad) {
                            /*Se crea el nuevo código para asignarsela a la nueva actividad.*/
                            $arrParams["cod_actvidad"] = $arrFase['cod_fase'] . $codActividad;
                            $idActividad = $this->atActividadModelo->metIngresaActividad($arrParams);
                        }
                    }
                    if ($idActividad) {
                        $arrResul['status'] = 'nuevo';
                        $arrResul['mensaje'] = 'El proceso se ejecutó exitosamente';
                    } else {
                        $arrResul['mensaje'] = 'Falló el ingreso. Intente nuevamente'; $error=true;
                    }
                }else{
                    $arrResul['mensaje'] = 'Esa fase ya existe'; $error=true;
                }
            }else{/*Se actualiza un registro*/
                $result=$this->atActividadModelo->metValidaActividad('valida_modificar',$arrParams);//Valida si existe
                if(COUNT($result)==0) {
                    $reg_afectado = $this->atActividadModelo->metModificarActividad($arrParams);
                    if ($reg_afectado) {
                        $arrResul['status'] = 'modificar';
                        $arrResul['mensaje'] = 'El proceso se ejecutó exitosamente';
                    } else {
                        $arrResul['mensaje'] = 'Falló la actualización. Intente nuevamente'; $error = true;
                    }
                }else{
                    $arrResul['mensaje'] = 'Esa fase ya existe'; $error=true;
                }
            }
            if($error){
                $arrResul['status']='errorSQL';
            }
            echo json_encode($arrResul);
        }else{ //Muestra el modal vacío para ingresar un nuevo registro ó carga los datos al modal para actualizar.
            $listaProceso=$this->atActividadModelo->metListarTipoProceso();/*Se busca lista de tipo de procesos*/
            if(COUNT($listaProceso)>0){
                $this->atVista->assign('listaProceso',$listaProceso);
            }else{
                echo false; exit;
            }
            if($idActividad){ //Entra cuando se clica en un botón actualizar de la grilla
                $arrActividad=$this->atActividadModelo->metMostrarActividades($idActividad);
                if(COUNT($arrActividad)>0){
                    $listaFase=$this->atActividadModelo->metBuscarFases($arrActividad["fk_pfb002_num_proceso"]);
                    if(COUNT($listaFase)>0){
                        $this->atVista->assign('listaFase',$listaFase);
                        $this->atVista->assign('formDB',$arrActividad);
                        $this->atVista->assign('idActividad',$idActividad);
                    }else{
                        echo false; exit;
                    }
                }else{
                    echo false; exit;
                }
            }
            $this->atVista->assign('idFase',$idActividad);
            $this->atVista->metRenderizar('CrearModificar','modales');
        }
    }
    /**
     * Elimina un registro.
     */
    public function metEliminar(){
        $canRegistro=0;
        $idActividad = $this->metObtenerInt('idActividad');
        $reg_afectado=$this->atActividadModelo->metEliminarActividad($idActividad);
        if(is_array($reg_afectado)){
            $error=$reg_afectado; $retornar['reg_afectado']=false;
            if($error[1]==1451){
                $retornar['mensaje']="Imposible de eliminar ese registro. ¡Está relacionado!";
            }else{
                $retornar['mensaje']="Se a producido un error. Intente mas tarde";
            }
        }else{
            $retornar['reg_afectado']=$reg_afectado;
            if($reg_afectado){
                $retornar['mensaje']="El proceso se efectuó exitosamente";
            }else{
                $retornar['mensaje']="El proceso no se ejecutó. Intente mas tarde";
            }
        }
        echo json_encode($retornar);
    }
}
?>