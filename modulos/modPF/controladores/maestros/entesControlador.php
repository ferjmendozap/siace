<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Planificación Fiscal
 * PROCESO: Ingreso y actualización de entes externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          José Diaz                        |        diaz.jose@cmldc.gob.ve      |         0416-2124302           |
 * | 2 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |         0426-514.43.82         |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        05-01-2017       |         1.0        |
 * |               #2                      |        06-06-2017       |                    |
 * |_______________________________________|_________________________|____________________|
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . 'modPF' . DS . 'modelos' . DS . 'funcionesGenerales.php';
class entesControlador extends Controlador{
    private $atEnteModelo;
    private $atFuncGnrles;          //Para las Funciones generales
    private $atIdContraloria;       //Para el Id de la contraloría que ejecuta.
    private $atIdDependenciaUser;   //Para el Id de la dependencia al cual pertenece el usuario.
    private $atIdEmpleadoUser;      //Para el Id empleado del usuario que ejecuta.
    private $atTipoUser;            //Para el tipo de usuario logueado
    private $atUserValido;          //Contendrá la validéz del usuario
    public function __construct(){
        parent::__construct();
        $this->atFuncGnrles = new funcionesGenerales();
        $this->atEnteModelo=$this->metCargarModelo('entes');
        //Se determina la validéz y el tipo de usuario.
        $arrUser=$this->atFuncGnrles->metValidaUsuario();
        if($arrUser['userValido']){//Entra, si es un Usuario Válido.
            $this->atTipoUser=$arrUser['tipoUser'];
            $this->atUserValido=$arrUser['userValido'];
            $this->atIdContraloria=$arrUser['pk_num_organismo'];
            $this->atIdEmpleadoUser=$arrUser['pk_num_empleado'];
            $this->atIdDependenciaUser=$arrUser['pk_num_dependencia'];
        }else{//De lo contrario, usuario no válido
            $this->atUserValido=$arrUser['userValido'];
        }
        $arrUser=null;
    }
    public function metIndex(){
        if($this->atUserValido) {
            $this->atVista->metRenderizar('listado');
        }else{
            $this->atVista->assign('valido', $this->atUserValido);
            $this->atVista->metRenderizar('../../error');
        }
    }
    public function metComplementosForm(){
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min',
            'inputmask/jquery.inputmask.bundle.min',
            'inputmask/jquery.maskedinput-1.2.2.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'modPF/funcionesModPf';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
    }
    /**
     * Lista los entes Padres externos existente
     */
    public function metCargaGrillaEntes(){
        $arrData=array();
        $result=$this->atEnteModelo->metBuscaEntesGrilla();
        if(COUNT($result)){
            foreach ($result as $fila) {
                $fila["ente_padre"] = $this->atFuncGnrles->metBuscaEntesPadre($fila['id_ente']);
                $arrData[] = $fila;
            }
            $result=null;
        }
        return $arrData;
    }
    /**
     *Busca los entes y los Visualiza en el listado principal
     *@throws Exception
     */
    public function metListadoEntes(){
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $arrEntes=$this->metCargaGrillaEntes();
        $this->atVista->assign('listadoEntes',$arrEntes);
        $this->atVista->metRenderizar('resultadoListado');
    }
    /**
     * Busca los entes para visualizarlos el modal de entes padres.
     */
    public function metCargaEntesPadres(){
        $arrEntes=$this->metCargaGrillaEntes();
        $this->atVista->assign('listaentes', $arrEntes);
        $this->atVista->metRenderizar('listadoEntes', 'modales');
    }
    public function metListarCategoria(){
        $idTipoEnte=$this->metObtenerInt('idTipoEnte');
        $result=$this->atEnteModelo->metCategoriasEnte($idTipoEnte);
        echo "<option value='0'>Seleccione</option>";
        foreach($result as $fila){
            echo "<option value=".$fila['id_categoria'].">".$fila['nombre_categoria']."</option>";
        }
    }
    public function metListarEstado(){
        $idPais = $this->metObtenerInt('idPais');
        $result = $this->atFuncGnrles->metBuscaEntidad($idPais);
        echo "<option value='0'>Seleccione</option>";
        foreach($result as $fila){
            echo "<option value=".$fila['pk_num_estado'].">".$fila['ind_estado']."</option>";
        }
    }
    public function metListarMunicipio(){
        $idEstado = $this->metObtenerInt('idEstado');
        $result=$this->atFuncGnrles->metBuscaMunicipio($idEstado);
        print_r($result);
        echo "<option value='0'>Seleccione</option>";
        foreach($result as $fila){
            echo "<option value=".$fila['pk_num_municipio'].">".$fila['ind_municipio']."</option>";
        }
    }
    public function metListarCiudad(){
        $idEstado = $this->metObtenerInt('idEstado');
        $result=$this->atFuncGnrles->metBuscaCiudad($idEstado);
        echo "<option value='0'>Seleccione</option>";
        foreach($result as $fila){
            echo "<option value=".$fila['pk_num_ciudad'].">".$fila['ind_ciudad']."</option>";
        }
    }
    public function metListarParroquia(){
        $idMunicipio = $this->metObtenerInt('idMunicipio');
        $result=$this->atFuncGnrles->metBuscaParroquia($idMunicipio);
        echo "<option value='0'>Seleccione</option>";
        foreach($result as $fila){
            echo "<option value=".$fila['pk_num_parroquia'].">".$fila['ind_parroquia']."</option>";
        }
    }
    /**
     * Visualiza el form modal para crear un nuevo ente ó actualizar uno existente
     */
    public function metMontaEnte(){
        $idEnte=$this->metObtenerInt('idEnte');
        $this->metComplementosForm();
        $listaPais=$this->atFuncGnrles->metListarPais();
        $this->atVista->assign('listaPais',$listaPais);
        //lista los tipo de entes
        $campos="pk_num_miscelaneo_detalle AS idtipo_ente,ind_nombre_detalle AS nombretipo_ente";
        $listaTiposEnte=$this->atFuncGnrles->metBuscaMiscelanio($campos,'PFTIPOENTE');
        $this->atVista->assign('listaTipoEnte',$listaTiposEnte); $listaTiposEnte=null;
        if($idEnte>0){//Se extraen los datos del ente para modificar
            $formBD = $this->atEnteModelo->metMostrarEnte($idEnte);
            if ($formBD['num_ente_padre']) {//Si viene algún valor, entra para buscar el ente principal.
                $arrEnte = $this->atFuncGnrles->metBuscaRecursivoEntes($formBD['num_ente_padre']);
                $formBD['entePadre'] = $arrEnte['cadEntes']; $arrEnte=null;
            }
            $this->atVista->assign('idEntePadre',$formBD['num_ente_padre']);
            $this->atVista->assign('idEnte',$formBD['pk_num_ente']);
            $listaCategoriaEnte=$this->atEnteModelo->metCategoriasEnte($formBD['pk_miscdet_tipoente']);
            $this->atVista->assign('listaCategoriaEnte',$listaCategoriaEnte); $listaCategoriaEnte=null;
            if($formBD['fec_fundacion']){
                $formBD['fec_fundacion'] = $this->atFuncGnrles->metFormateaFecha($formBD['fec_fundacion']);
            }
            if($formBD['fec_ultima_modificacion']){
                $formBD['fec_ultima_modificacion'] = $this->atFuncGnrles->metFormateaFecha($formBD['fec_ultima_modificacion'],true);
            }
            if($formBD['fk_a012_num_parroquia']){//Si viene algún valor. Entra
                $arrIdsUbicGeo=$this->atEnteModelo->metBuscaIdsPem($formBD['fk_a012_num_parroquia']);//Busca los ids de país, entidad y municipio.
                $formBD=array_merge($formBD,$arrIdsUbicGeo); $arrIdsUbicGeo=null;
                $listaEntidad=$this->atFuncGnrles->metBuscaEntidad($formBD['id_pais']);
                $this->atVista->assign('listaEntidad',$listaEntidad); $listaEntidad=null;
                $listaMunicipio=$this->atFuncGnrles->metBuscaMunicipio($formBD['id_entidad']);
                $this->atVista->assign('listaMunicipio',$listaMunicipio); $listaMunicipio=null;
                $listaParroquia=$this->atFuncGnrles->metBuscaParroquia($formBD['id_municipio']);
                $this->atVista->assign('listaParroquia',$listaParroquia); $listaParroquia=null;
            }
            if($formBD['fk_a010_num_ciudad']){
                $listaCiudad=$this->atFuncGnrles->metBuscaCiudad($formBD['id_entidad']);
                $this->atVista->assign('listaCiudad',$listaCiudad); $listaCiudad=null;
            }
            //Se prepara los números telefónicos para pintarlos.
            if($formBD['ind_telefono']){
                $arrTlfs=explode(',', $formBD['ind_telefono']);
                $it=1;$if=1;
                foreach($arrTlfs as $cadena){
                    $arrTlf=explode('_',$cadena);
                    if($arrTlf[1]=='N'){
                        $formBD['telefono_'.$it]=$arrTlf[0];
                        $it++;
                    }else{
                        $formBD['fax_'.$if]=$arrTlf[0];
                        $if++;
                    }
                }
            }
            $formBD['usuario']=$this->atFuncGnrles->metBuscaUsuario($formBD['fk_a018_num_seg_usermod']);
            $this->atVista->assign('formDB',$formBD);
        }else{//Monta el form modal vacío para crear un nuevo ente.
            $formBD['num_estatus']=1;
            $formBD['num_sujeto_control']=0;
            $this->atVista->assign('idEntePadre',0);
            $this->atVista->assign('idEnte',$idEnte);
            $this->atVista->assign('formDB',$formBD);
        }
        $this->atVista->metRenderizar('CrearModificar','modales');
    }
    /**
     * Permite ingresar ó modificar un ente.
     */
    public function metCrearModificar(){
        $valido=$this->metObtenerInt('valido');
        $this->metComplementosForm();
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('idEnte','idEntePadre','cbox_estado','cbox_municipio','cbox_parroquia','cbox_ciudad','chk_sujetoControl','chk_estatusEnte');
            $arrInd=$this->metValidarFormArrayDatos('form','int',$Excceccion);
            $Excceccion2=array('txt_nroRif','txt_tomoRegistro','txt_fechaFundacion','txt_telefono','txt_telefono2','txt_telefono3','txt_fax','txt_fax2','txt_paginaWeb','txt_nroGaceta','txt_resolucion','txt_mision','txt_vision','txt_otros');
            $arrTxt=$this->metValidarFormArrayDatos('form','txt',$Excceccion2);
            $arrParams=array_merge($arrInd,$arrTxt);
            if(in_array('error',$arrParams)){
                $arrParams['Error']='error';
                echo json_encode($arrParams); exit;
            }elseif($arrParams['idEnte']>0 AND ($arrParams['idEnte']==$arrParams['idEntePadre'])){
                $arrRetorna = array("Error"=>"errorSql", "mensaje"=>"El Ente no debe ser igual al Ente Principal");
                echo json_encode($arrRetorna); exit;
            }
            if($arrParams['txt_fechaFundacion']){
                $arrParams['txt_fechaFundacion']=$this->atFuncGnrles->metFormateaFechaMsql($arrParams['txt_fechaFundacion']);
            }else{
                $arrParams['txt_fechaFundacion']=null;
            }
            //Si campos vienen vacíos se setean a null
            if(!$arrParams['txt_nroRif']){$arrParams['txt_nroRif']=null;}
            if(!$arrParams['txt_tomoRegistro']){$arrParams['txt_tomoRegistro']=null;}
            if(!$arrParams['txt_fechaFundacion']){$arrParams['txt_fechaFundacion']=null;}
            if(!$arrParams['txt_direccion']){$arrParams['txt_direccion']=null;}
            if(!$arrParams['txt_paginaWeb']){$arrParams['txt_paginaWeb']=null;}
            if(!$arrParams['txt_nroGaceta']){$arrParams['txt_nroGaceta']=null;}
            if(!$arrParams['txt_resolucion']){$arrParams['txt_resolucion']=null;}
            if(!$arrParams['txt_mision']){$arrParams['txt_mision']=null;}
            if(!$arrParams['txt_vision']){$arrParams['txt_vision']=null;}
            if(!$arrParams['txt_otros']){$arrParams['txt_otros']=null;}
            if(!isset($arrParams['cbox_parroquia']) OR $arrParams['cbox_parroquia']==0){$arrParams['cbox_parroquia']=null;}
            if(!isset($arrParams['cbox_ciudad']) OR $arrParams['cbox_ciudad']==0){$arrParams['cbox_ciudad']=null;}
            //Se forma una cadena de teléfonos y faxes
            $arrParams['txt_telefonos']=$arrParams['txt_telefono']?$arrParams['txt_telefono'].'_N,':'';
            $arrParams['txt_telefonos'].=$arrParams['txt_telefono2']?$arrParams['txt_telefono2'].'_N,':'';
            $arrParams['txt_telefonos'].=$arrParams['txt_telefono3']?$arrParams['txt_telefono3'].'_N,':'';
            $arrParams['txt_telefonos'].=$arrParams['txt_fax']?$arrParams['txt_fax'].'_F,':'';
            $arrParams['txt_telefonos'].=$arrParams['txt_fax2']?$arrParams['txt_fax2'].'_F,':'';
            $arrParams['txt_telefonos']=substr($arrParams['txt_telefonos'], 0, -1);//Quita la última coma (,) a la cadena de teléfonos
            if(!isset($arrParams['chk_estatusEnte'])){$arrParams['chk_estatusEnte']=0;}
            if(!isset($arrParams['chk_sujetoControl'])){$arrParams['chk_sujetoControl']=0;}
            $arrRetorna=array(); $rif=false;
            if($arrParams['idEnte']==0){//Se Ingresa un nuevo registro.
                $params=array('idEntePadre'=>$arrParams['idEntePadre'],'txt_nombreEnte'=>$arrParams['txt_nombreEnte']);
                if(!$this->atEnteModelo->metValidaEnte('valida_ingreso',$params)){//Valida si existe un nombre de ente igual
                    if($arrParams['txt_nroRif']){//Valida si existe un número de registro fiscal igual
                        $params=array('txt_nroRif'=>$arrParams['txt_nroRif']);
                        $rif=$this->atEnteModelo->metValidaRif('valida_ingreso',$params);
                    }
                    if(!$rif){

                        $idEnte = $this->atEnteModelo->metIngresaEnte($arrParams);
                        if($idEnte){
                            $arrRetorna["idEnte"]=$idEnte; unset($arrParams["idEnte"]);
                            $arrRetorna['mensaje'] = 'El proceso se ejecutó exitosamente';
                            $arrRetorna['dataValidada']=$arrParams;
                        }else{
                            $arrRetorna = array("Error"=>"errorSql", "mensaje"=>"Falló el ingreso. Intente nuevamente");
                        }

                    }else{
                        $arrRetorna = array("Error"=>"errorSql", "mensaje"=>"Ese número de RIF le pertenece a otro ente");
                    }
                }else{
                    $arrRetorna = array("Error"=>"errorSql", "mensaje"=>"Ese nombre de ente ya existe. Intente con otro por favor");
                }
            }else{//Se actualiza un registro
                $params=array('idEnte'=>$arrParams['idEnte'],'idEntePadre'=>$arrParams['idEntePadre'],'txt_nombreEnte'=>$arrParams['txt_nombreEnte']);
                if(!$this->atEnteModelo->metValidaEnte('valida_mod',$params)){//Valida si existe un nombre de ente igual
                    $params=array('idEnte'=>$arrParams['idEnte'],'txt_nroRif'=>$arrParams['txt_nroRif']);
                    $rif=$this->atEnteModelo->metValidaRif('valida_mod',$params);
                    if($rif){
                        //if(!$rif){
                        $reg_afectado = $this->atEnteModelo->metActualizaEnte($arrParams);
                        if($reg_afectado){
                            $arrRetorna["reg_afectado"]=$reg_afectado; unset($arrParams["idEnte"]);
                            $arrRetorna['mensaje'] = 'El proceso se ejecutó exitosamente';
                            $arrRetorna['dataValidada']=$arrParams;
                        }else{
                            $arrRetorna = array("Error"=>"errorSql", "mensaje"=>"Falló la actualización. Intente nuevamente");
                        }

                    }else{
                        $arrRetorna = array("Error"=>"errorSql", "mensaje"=>"Ese número de RIF le pertenece a otro ente");
                    }
                }else{
                    $arrRetorna = array("Error"=>"errorSql", "mensaje"=>"Ese nombre de ente ya existe. Intente con otro por favor");
                }
            }
            echo json_encode($arrRetorna);
        }
    }
    /**
     * Elimina un ente siempre y cuando no esté relacionado.
     */
    public function metEliminarEnte(){
        $idEnte = $this->metObtenerInt('idEnte');
        $reg_afectado=$this->atEnteModelo->metEliminarEnte($idEnte);
        if(is_array($reg_afectado)){
            $error=$reg_afectado; $arrRetorna['reg_afectado']=false;
            if($error[1]==1451){
                $arrRetorna['mensaje']="Imposible de eliminar ese registro. ¡Está relacionado!";
            }else{
                $arrRetorna['mensaje']="Se a producido un error. Intente mas tarde";
            }
        }else{
            $arrRetorna['reg_afectado']=$reg_afectado;
            if($reg_afectado){
                $arrRetorna['mensaje']="El proceso se efectuó exitosamente";
            }else{
                $arrRetorna['mensaje']="El proceso no se ejecutó. Intente mas tarde";
            }
        }
        echo json_encode($arrRetorna);
    }
}