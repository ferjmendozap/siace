<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: PLANIFICACIÓN FISCAL
 * PROCESO: INGRESO Y MANTENIMIENTO DE TIPOS PROCESO FISCAL
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 3 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |         0426-5144382           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #3                      |        08-08-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . 'modPF' . DS . 'modelos' . DS . 'funcionesGenerales.php';
class procesosFiscalControlador extends Controlador{
    private $atProcesoModelo;
    private $atFuncGnrles;          //Para las Funciones generales
    private $atIdContraloria;       //Para el Id de la contraloría que ejecuta.
    private $atIdDependenciaUser;   //Para el Id de la dependencia al cual pertenece el usuario.
    private $atIdEmpleadoUser;      //Para el Id empleado del usuario que ejecuta.
    private $atTipoUser;            //Para el tipo de usuario logueado
    private $atUserValido;          //Contendrá la validéz del usuario
    public function __construct(){
        parent::__construct();
        $this->atProcesoModelo=$this->metCargarModelo('procesosFiscal');
        $this->atFuncGnrles = new funcionesGenerales();
        //Se determina la validéz y el tipo de usuario.
        $arrUser=$this->atFuncGnrles->metValidaUsuario();
        if($arrUser['userValido']){//Entra, si es un Usuario Válido.
            $this->atTipoUser=$arrUser['tipoUser'];
            $this->atUserValido=$arrUser['userValido'];
            $this->atIdContraloria=$arrUser['pk_num_organismo'];
            $this->atIdEmpleadoUser=$arrUser['pk_num_empleado'];
            $this->atIdDependenciaUser=$arrUser['pk_num_dependencia'];
        }else{//De lo contrario, usuario no válido
            $this->atUserValido=$arrUser['userValido'];
        }
        $arrUser=null;
    }
    public function metIndex($lista=false){
        if($this->atUserValido) {
            $this->atVista->metRenderizar('listado');
        }else{
            $this->atVista->assign('valido', $this->atUserValido);
            $this->atVista->metRenderizar('../../error');
        }
    }
    /**
     * Busca y monta los registros de fases en la grilla al clicar en el botón buscar.
     */
    public function metListarProcesos(){
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        //$idProceso=$this->metObtenerInt('idProceso');
        $result=$this->atProcesoModelo->metListarTipoProceso();
        if(count($result)>0){
            $this->atVista->assign('listado',$result);
            $this->atVista->metRenderizar('resultadoListado');
        }else{
            echo false;
        }
    }
    /**
     * Visualiza el form modal para ingresar un nuevo regitro ó actualizar.
     */
    public function metCrearModificar(){
        $js[] = 'modPF/funcionesModPf';
        $this->atVista->metCargarJs($js);
        $valido=$this->metObtenerInt('valido');
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('idProceso','num_estatus');
            $Excceccion2=array('cod_proceso');
            $alphaNum=$this->metValidarFormArrayDatos('form','txt',$Excceccion2);
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
            if($alphaNum!=null && $ind==null){
                $arrParams=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $arrParams=$ind;
            }else{
                $arrParams=array_merge($alphaNum,$ind);
            }
            if(in_array('error',$arrParams)){
                $arrParams['status']='error';
                echo json_encode($arrParams); exit;
            }
            if(!isset($arrParams['num_estatus'])){
                $arrParams['num_estatus']=0;
            }
            $error=""; $idProceso=$arrParams["idProceso"];
            if($idProceso==0){/*Se ingresa un registro*/
                $result=$this->atProcesoModelo->metConsultaProceso('valida_ingreso',$arrParams['txt_descripcion_proceso']);/*Se valida si existe*/
                if(COUNT($result)==0){
                    $codProceso=$this->atFuncGnrles->metGeneraCodigo('pf_b002_proceso','cod_proceso',2);/*Se obtiene el nuevo código para el proceso*/
                    if($codProceso){
                        $arrParams["cod_proceso"]=$codProceso;
                        $idProceso=$this->atProcesoModelo->metCrearTipoProceso($arrParams);
                        if($idProceso){
                            $arrParams["idProceso"]=$idProceso;
                            $arrResul['status'] = 'nuevo';
                            $arrResul['mensaje'] = 'El ingreso se ejecutó exitosamente';
                            $arrResul=array_merge($arrResul,$arrParams);
                        }else{
                            $arrResul['mensaje'] = 'Falló el ingreso. Intente nuevamente';$error=true;
                        }
                    }else{
                        $arrResul['mensaje'] = 'Falló el ingreso. Intente nuevamente';$error=true;
                    }
                }else{
                    $arrResul['mensaje'] = 'Ese tipo de proceso ya existe'; $error=true;
                }
            }else{/*Se actualiza un registro*/
                $result=$this->atProcesoModelo->metConsultaProceso('valida_modificar',$arrParams);/*Se valida si existe*/
                if(COUNT($result)==0) {
                    $reg_afectado = $this->atProcesoModelo->metModificarTipoPorceso($arrParams);
                    if ($reg_afectado) {
                        $arrResul['status'] = 'modificar';
                        $arrResul['mensaje'] = 'La actualización se ejecutó exitosamente';
                    } else {
                        $arrResul['mensaje'] = 'Falló la actualización. Intente nuevamente';$error = true;
                    }
                    $arrResul=array_merge($arrResul,$arrParams);
                }else{
                    $arrResul['mensaje'] = 'Ese tipo de proceso ya existe'; $error=true;
                }
            }
            if($error){
                $arrResul['status']='errorSQL';
            }
            echo json_encode($arrResul);
        }else{//Entra cuando se desea crear un nuevo registro ó actualizar
            $idProceso=$this->metObtenerInt('idProceso');
            if($idProceso>0){//Carga el form modal pra actualizar los datos. De lo contrario, visualiza el form modal en limpio para ingresar.
                $result=$this->atProcesoModelo->metMostrarTipoProceso($idProceso);
                if(is_array($result)){
                    $this->atVista->assign('formDB',$this->atProcesoModelo->metMostrarTipoProceso($idProceso));
                    $this->atVista->assign('idProceso',$idProceso);
                }else{
                    echo false; exit;
                }
            }
            $this->atVista->metRenderizar('CrearModificar','modales');
        }
    }
    /**
     * Elimina un registro
     */
    public function metEliminarProceso(){
        $idProceso = $this->metObtenerInt('idProceso');
        $reg_afectado=$this->atProcesoModelo->metEliminaProceso($idProceso);
        if(is_array($reg_afectado)){
            $error=$reg_afectado; $retornar['reg_afectado']=false;
            if($error[1]==1451){
                $retornar['mensaje']="Imposible de eliminar ese registro. ¡Está relacionado!";
            }else{
                $retornar['mensaje']="Se a producido un error. Intente mas tarde";
            }
        }else{
            $retornar['reg_afectado']=$reg_afectado;
            if($reg_afectado){
                $retornar['mensaje']="El proceso se efectuó exitosamente";
            }else{
                $retornar['mensaje']="El proceso no se ejecutó. Intente mas tarde";
            }
        }
        echo json_encode($retornar);
    }
}
?>