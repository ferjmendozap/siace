<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: PLANIFICACIÓN FISCAL
 * PROCESO: ASIGNACIÓN DE ACTIVIDADES A TIPOS DE ACTUACIÓN
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 3 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |        0426-5144382            |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #3                      |        08-09-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . 'modPF' . DS . 'modelos' . DS . 'funcionesGenerales.php';
class asignaActividadControlador extends Controlador{
    private $atAsignacionModelo;
    private $atFuncGnrles;          //Para las Funciones generales
    private $atIdContraloria;       //Para el Id de la contraloría que ejecuta.
    private $atIdDependenciaUser;   //Para el Id de la dependencia al cual pertenece el usuario.
    private $atIdEmpleadoUser;      //Para el Id empleado del usuario que ejecuta.
    private $atTipoUser;            //Para el tipo de usuario logueado
    private $atUserValido;          //Contendrá la validéz del usuario
    public function __construct(){
        parent::__construct();
        $this->atAsignacionModelo=$this->metCargarModelo('asignaActividad');
        $this->atFuncGnrles = new funcionesGenerales();
        //Se determina la validéz y el tipo de usuario.
        $arrUser=$this->atFuncGnrles->metValidaUsuario();
        if($arrUser['userValido']){//Entra, si es un Usuario Válido.
            $this->atTipoUser=$arrUser['tipoUser'];
            $this->atUserValido=$arrUser['userValido'];
            $this->atIdContraloria=$arrUser['pk_num_organismo'];
            $this->atIdEmpleadoUser=$arrUser['pk_num_empleado'];
            $this->atIdDependenciaUser=$arrUser['pk_num_dependencia'];
        }else{//De lo contrario, usuario no válido
            $this->atUserValido=$arrUser['userValido'];
        }
        $arrUser=null;
    }
    public function metIndex(){
        if($this->atUserValido) {
            $js[] = 'modPF/funcionesModPf';
            $this->atVista->metCargarJs($js);
            $listaTipoActF = $this->atAsignacionModelo->metConsultas('por_tipo_actuacion');/*Se busca los tipos de actuación fiscal*/
            $this->atVista->assign('tipo_actuacionf', $listaTipoActF);
            $listaProceso = $this->atAsignacionModelo->metConsultas('por_tipo_proceso');/*Se busca lista de tipo de procesos*/
            $this->atVista->assign('listaProceso', $listaProceso);
            $this->atVista->metRenderizar('asignarActividad');
        }else{
            $this->atVista->assign('valido', $this->atUserValido);
            $this->atVista->metRenderizar('../../error');
        }
    }
    /**
     * Carga las fases de acuerdo al proceso seleccionado
     */
    public function metCargarFases(){
        $arr_ids=$this->metValidarFormArrayDatos('form','int');
        $arrfase=$this->atAsignacionModelo->metConsultas('por_fases',$arr_ids['idProceso']);
        if(count($arrfase)){
            echo '<option value="">Seleccione</option>';
            foreach($arrfase as $fila){
                echo'<option value="'.$fila['pk_num_fase'].'">'.$fila['txt_descripcion_fase'].'</option>';
            }
        }else{
            echo '<option value="">No hay Fases</option>';
        }
    }
    /**
     * Carga la grilla de actividades asignadas y disponibles de acuerdo a la dependencia de control y fase seleccionada.
     */
    public function metCargarGrilla(){
        $js[] = 'modPF/DataTablePf';
        $this->atVista->metCargarJs($js);
        $params=$this->metValidarFormArrayDatos('form','int');
        unset($params["id_x"]);unset($params["idProceso"]);
        $result=$this->atAsignacionModelo->metConsultas('por_actividades',$params["idFase"]);
        if(COUNT($result)){
            foreach($result as $fila){
                $params["pk_num_actividad"]=$fila["pk_num_actividad"];
                $arrAsignado = $this->atAsignacionModelo->metConsultas('por_actividad_asignada',$params);
                if(COUNT($arrAsignado)>0){
                    $fila["pk_num_actividad_tipoactuac"]=$arrAsignado[0]["pk_num_actividad_tipoactuac"];
                }else{
                    $fila["pk_num_actividad_tipoactuac"]=0;
                }
                $data[]=$fila;
            }
            if(is_array($data)){
                $this->atVista->assign('listado',$data);
                $this->atVista->metRenderizar('resultadoListado');
            }else{
                echo false;
            }
        }else{
            echo false;
        }
    }
    /**
     * Ejecuta el proceso de Agregar las actividades seleccionadas al tipo de actuación fiscal.
     */
    public function metAgregaActividad(){
        $Exceccion_int = array('id_x');
        $arrParams=$this->metValidarFormArrayDatos('form','int',$Exceccion_int);
        if (isset($arrParams['chk_actividades'])==null) {
            $retornar = array('result' => false,'mensaje' => "Debe seleccionar al menos una actividad");
        } else {
            $result = $this->atAsignacionModelo->metAgregaActividad($arrParams);
            if ($result) {
                $retornar = array(
                    'result' => $result,
                    'mensaje' => 'El proceso se efectuó exitosamente'
                );
            } else {
                $retornar = array(
                    'result' => $result,
                    'mensaje' => 'El proceso no se pudo efectuar. ¡Intente de nuevo!'
                );
            }
        }
        echo json_encode($retornar);
    }
    /**
     * Ejecuta el proceso de Quitar una actividad al tipo actuación fiscal.
     */
    public function metQuitaActividad(){
        $arr_ids=$this->metValidarFormArrayDatos('form','int');
        $result=$this->atAsignacionModelo->metQuitarActividad($arr_ids);
        if($result){
            $retornar=array(
                'reg_afectado'=>$result,
                'mensaje'=>'El proceso se efectuó exitosamente'
            );
        }else{
            $retornar=array(
                'reg_afectado'=>$result,
                'mensaje'=>'La actividad no se pudo quitar. Intente de nuevo por favor'
            );
        }
        echo json_encode($retornar);
    }
}
?>