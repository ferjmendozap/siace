<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: PLANIFICACIÓN FISCAL
 * PROCESO: INGRESO Y MANTENIMIENTO DE FASES DE PROCESOS FISCAL
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 3 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |        0426-5144382            |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #3                      |        08-08-2016       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . 'modPF' . DS . 'modelos' . DS . 'funcionesGenerales.php';
class fasesFiscalControlador extends Controlador
{
    private $atFaseModelo;
    private $atFuncGnrles;          //Para las Funciones generales
    private $atIdContraloria;       //Para el Id de la contraloría que ejecuta.
    private $atIdDependenciaUser;   //Para el Id de la dependencia al cual pertenece el usuario.
    private $atIdEmpleadoUser;      //Para el Id empleado del usuario que ejecuta.
    private $atTipoUser;            //Para el tipo de usuario logueado
    private $atUserValido;          //Contendrá la validéz del usuario
    public function __construct(){
        parent::__construct();
        $this->atFaseModelo=$this->metCargarModelo('fasesFiscal');
        $this->atFuncGnrles = new funcionesGenerales();
        //Se determina la validéz y el tipo de usuario.
        $arrUser=$this->atFuncGnrles->metValidaUsuario();
        if($arrUser['userValido']){//Entra, si es un Usuario Válido.
            $this->atTipoUser=$arrUser['tipoUser'];
            $this->atUserValido=$arrUser['userValido'];
            $this->atIdContraloria=$arrUser['pk_num_organismo'];
            $this->atIdEmpleadoUser=$arrUser['pk_num_empleado'];
            $this->atIdDependenciaUser=$arrUser['pk_num_dependencia'];
        }else{//De lo contrario, usuario no válido
            $this->atUserValido=$arrUser['userValido'];
        }
        $arrUser=null;
    }
    public function metIndex($lista=false){
        if($this->atUserValido) {
            $listaProceso = $this->atFaseModelo->metConsultaFase('por_tipo_proceso', '');//Se extrae los procesos
            $this->atVista->assign('listaProceso', $listaProceso);
            $this->atVista->metRenderizar('listado');
        }else{
            $this->atVista->assign('valido', $this->atUserValido);
            $this->atVista->metRenderizar('../../error');
        }
    }
    /**
     * Busca y monta los registros de fases en la grilla al clicar en el botón buscar.
     */
    public function metListarFases(){
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $idProceso=$this->metObtenerInt('idProceso');
        $result=$this->atFaseModelo->metConsultaFase('listado_fases',$idProceso);
        if(count($result)>0){
            $this->atVista->assign('listado',$result);
            $this->atVista->metRenderizar('resultadoListado');
        }else{
            echo false;
        }
    }
    /**
     * Visualiza el form modal para ingresar un nuevo regitro ó actualizar.
     */
    public function metCrearModificar(){
        $js[] = 'modPF/funcionesModPf';
        $this->atVista->metCargarJs($js);
        $valido=$this->metObtenerInt('valido');
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('num_estatus','idFase');
            $Excceccion2=array('cod_fase');
            $alphaNum=$this->metValidarFormArrayDatos('form','txt',$Excceccion2);
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
            if($alphaNum!=null && $ind==null){
                $arrParams=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $arrParams=$ind;
            }else{
                $arrParams=array_merge($alphaNum,$ind);
            }
            if(in_array('error',$arrParams)){
                $arrParams['status']='error';
                echo json_encode($arrParams);
                exit;
            }
            if(!isset($arrParams['num_estatus'])){
                $arrParams['num_estatus']=0;
            }
            $idFase=$arrParams["idFase"]; $error="";
            if($idFase==0){/*SE INGRESA UN NUEVO REGISTRO*/
                $result=$this->atFaseModelo->metConsultaFase('valida_ingreso',$arrParams);//Se valida si ya existe
                if(COUNT($result)==0) {
                    //Se busca el código del proceso fiscal para crear el código de la fase
                    $result = $this->atFuncGnrles->metBuscaRegistro('pf_b002_proceso', 'pk_num_proceso', $arrParams["pk_num_proceso"]);
                    if(COUNT($result)>0){
                        $arrProceso=$result[0];
                        //Se determina el correlativo de la fase.
                        $codFase = $this->atFuncGnrles->metGeneraCodigo('pf_b003_fase', 'cod_fase', 2, 'fk_pfb002_num_proceso', $arrProceso["pk_num_proceso"]);
                        if($codFase){
                            $arrParams["cod_fase"]=$arrProceso['cod_proceso'].$codFase; //se crea el nuevo código para fase a ingresar.
                            $idFase = $this->atFaseModelo->metIngresarFases($arrParams);
                        }
                    }
                    if($idFase){
                        $arrResul['status'] = 'nuevo';
                        $arrResul['mensaje'] = 'El proceso se ejecutó exitosamente';
                    }else{
                        $arrResul['mensaje'] = 'Falló el ingreso. Intente nuevamente';
                        $error=true;
                    }
                }else{
                    $arrResul['mensaje'] = 'Esa fase ya existe'; $error=true;
                }
            }else{/*SE ACTUALIZA UN REGISTRO*/
                $result=$this->atFaseModelo->metConsultaFase('valida_modificar',$arrParams);//Se valida si ya existe
                if(COUNT($result)==0) {
                    $reg_afectado = $this->atFaseModelo->metModificaFases($arrParams);
                    if ($reg_afectado) {
                        $arrResul['status'] = 'modificar';
                        $arrResul['mensaje'] = 'El proceso se ejecutó exitosamente';
                    } else {
                        $arrResul['mensaje'] = 'Falló la actualización. Intente nuevamente'; $error = true;
                    }
                }else{
                    $arrResul['mensaje'] = 'Esa fase ya existe'; $error=true;
                }
            }
            if($error){
                $arrResul['status']='errorSQL';
            }
            echo json_encode($arrResul);
        }else{ //Muestra el modal vacío para ingresar un nuevo registro ó carga los datos al modal para actualizar.
            $idFase=$this->metObtenerInt('idFase');
            $listaProceso=$this->atFaseModelo->metConsultaFase('por_tipo_proceso','');//Lista los tipos de proceso fiscal
            if(COUNT($listaProceso)>0){
                $this->atVista->assign('listaProceso',$listaProceso);
            }else{
                echo false; exit;
            }
            if($idFase){ //Entra cuando se clica en un botón actualizar de la grilla
                $arrFase=$this->atFaseModelo->metConsultaFase('por_fase',$idFase);
                if(COUNT($arrFase)>0){
                    $this->atVista->assign('formDB',$arrFase[0]);
                }else{
                    echo false; exit;
                }
            }
            $this->atVista->assign('idFase',$idFase);
            $this->atVista->metRenderizar('CrearModificar','modales');
        }
    }
    /**
     * Elimina un registro.
     */
    public function metEliminarFase(){
        $idFase = $this->metObtenerInt('idFase');
        $reg_afectado=$this->atFaseModelo->metEliminaFase($idFase);
        if(is_array($reg_afectado)){
            $error=$reg_afectado; $retornar['reg_afectado']=false;
            if($error[1]==1451){
                $retornar['mensaje']="Imposible de eliminar ese registro. ¡Está relacionado!";
            }else{
                $retornar['mensaje']="Se a producido un error. Intente mas tarde";
            }
        }else{
            $retornar['reg_afectado']=$reg_afectado;
            if($reg_afectado){
                $retornar['mensaje']="El proceso se efectuó exitosamente";
            }else{
                $retornar['mensaje']="El proceso no se ejecutó. Intente mas tarde";
            }
        }
        echo json_encode($retornar);
    }
}
?>