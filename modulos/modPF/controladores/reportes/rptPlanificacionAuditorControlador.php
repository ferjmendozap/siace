<?php
/***********************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Planificación Fiscal
 * PROCESO: Reportes de Planficaciones.
 * DESCRIPCIÓN: Permite la ejecución de extración de datos para la consulta y reportes en pdf de planificaciones por
 *              auditor y auditor por planificación en sus diferentes proceso fiscales: Actuación fiscal,
 *              Valoración preliminar, Potestad investigativa, Valoración jurídica y Procedimientos administrativos.
 * PROGRAMADORES.______________________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Alexis Ontiveros     | ontiveros.alexis@cmldc.gob.ve        | 0426-514.43.82
 * |___________________________________________________________________________________________________________________
 *
 * VERSION
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        07-08-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 ***********************************************************************************************************************/
require_once RUTA_Modulo . 'modPF' . DS . 'modelos' . DS . 'funcionesGenerales.php';
class rptPlanificacionAuditorControlador extends Controlador{
    public $atRptModeloPf;
    private $atFuncGnrles;          //Para las Funciones generales
    private $atIdContraloria;       //Para el Id de la contraloría que ejecuta.
    public $atIdDependenciaUser;    //Para el Id de la dependencia al cual pertenece el usuario.
    private $atIdEmpleadoUser;      //Para el Id empleado del usuario que ejecuta.
    private $atTipoUser;            //Para el tipo de usuario logueado
    private $atIdTipoProceso;       //Para el Id del tipo de proceso fiscal.
    private $atUserValido;          //Contendrá la validéz del usuario
    private $atCodTipoProceso;      //Para el código del tipo de proceso
    private $atParametros;          //Para asignar los parámetros
    private $atListaEstadoPlan;     //Contendrá la lista de estados de planificación de acuerdo al tipo de proceso

    public function __construct(){
        parent::__construct();
        $this->atRptModeloPf = $this->metCargarModelo('rptPlanificacionAuditor', 'reportes');
        $this->atFuncGnrles = new funcionesGenerales();
        if(isset($_GET['tipoProceso'])){
            $this->atCodTipoProceso=$_GET['tipoProceso'];//Dato que viene en la url de la opción de menú
        }else{
            if(isset($_GET['obj'])){ //Entra cuando viene de la vista al clicar en el botón principal PDF ó en el botón PDF correspondiente a una planificación
                $arrParams=$_GET['obj']; //Se captura los parámetros codificado en base 64.
                $decodeParams=base64_decode($arrParams); //Se descodifica el json
                $arrParams=json_decode($decodeParams,true); //Se trasforma el json en un array php
                $this->atCodTipoProceso=$arrParams['CodTipoProceso'];
                $this->atParametros = $arrParams;
            }else{//Entra cuando viene de la vista al clicar el botón Buscar
                $this->atCodTipoProceso=$this->metObtenerTexto('CodTipoProceso');
            }
        }
        //Se determina la validéz y el tipo de usuario.
        $arrUser = $this->atFuncGnrles->metValidaUsuario();
        if ($arrUser['userValido']) {//Entra, si es un Usuario Válido.
            $this->atTipoUser = $arrUser['tipoUser'];
            $this->atUserValido = $arrUser['userValido'];
            $this->atIdContraloria = $arrUser['pk_num_organismo'];
            $this->atIdEmpleadoUser = $arrUser['pk_num_empleado'];
            $this->atIdDependenciaUser = $arrUser['pk_num_dependencia'];
            /*Se busca y asigna el id del tipo de proceso de acuerdo al código del mísmo*/
            $arr_tipoproc = $this->atFuncGnrles->metTipoProcesoFiscal(null, $this->atCodTipoProceso);
            $this->atIdTipoProceso = $arr_tipoproc[0]['pk_num_proceso'];
            $arr_tipoproc = null;
        } else {//De lo contrario, usuario no válido
            $this->atVista->assign('valido', $this->atUserValido);
            $this->atVista->metRenderizar('../../error');
        }
        $arrUser = null;
    }
    function __destruct(){
        $atIdDependenciaUser=null; $tituloReporte=null;
    }
    /**
     * Busca entes externos
     */
    public function metBuscarEntes($iddep_padre){
        $retornar = array("filas" => false);
        $result = $this->atFuncGnrles->metCargaCboxRecursivoEnte($iddep_padre, '');
        if (COUNT($result) > 0) {
            $retornar = array("filas" => $result);
        }
        return $retornar;
    }
    /**
     * Lista las dependencias de un ente externo al ser seleccionado
     */
    public function metListaEntes(){
        $idEnte = $this->metObtenerInt('idEnte');
        $listaEntes = $this->metBuscarEntes($idEnte);
        echo json_encode($listaEntes);
    }
    /**
     * Se buscan los auditores disponibles para la planificación de acuerdo a la dependencia seleccionada.
     * @param $id_dependencia
     * @param $idPlanificacion
     * @return array
     */
    public function metListaPersonal($idDependencia=NULL){
        if(!$idDependencia){//Entra cuendo viene de la vista
            $arrParams['idDependencia']=$this->metObtenerInt('idDependencia');
            $result = $this->atRptModeloPf->metBuscaAuditores($arrParams);
            if($result){
                echo json_encode(array('listaPersona'=>$result));
            }else{
                echo json_encode(array('listaPersona'=>false));
            }
        }else{//Entra cuendo viene del Index
            $arrParams['idDependencia']=$idDependencia;
            $result = $this->atRptModeloPf->metBuscaAuditores($arrParams);
            return $result;
        }
    }
    public function metIndex(){
        $userInvitado=false; $arrDependencia=array(); $listaPersona=array();$selectedDep=false;
        $complementosCss = array(
            'bootstrap-datepicker/datepicker'
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->assign('tempYear', date('Y'));
        $this->atVista->assign('CodTipoProceso', $this->atCodTipoProceso);
        //Se extrae una Lista de los tipos de estado de la planificación de acuerdo al tipo de proceso
        //y se determina los títulos del formulario.
        //$perfilesUser=Session::metObtener('perfil');
        if($this->atCodTipoProceso=='01'){
            $tituloForm='Reportes de Actuaciones Fiscales por Auditor';
            $this->atListaEstadoPlan=$this->atFuncGnrles->metListaEstadosPlanific('EstadosAF');
        }elseif($this->atCodTipoProceso=='02'){
            $tituloForm='Reportes de Valoración Preliminar por Auditor';
            $this->atListaEstadoPlan=$this->atFuncGnrles->metListaEstadosPlanific('EstadosVP');
        }elseif($this->atCodTipoProceso=='03'){
            $tituloForm='Reportes de Potestad Investigativa por Auditor';
            $this->atListaEstadoPlan=$this->atFuncGnrles->metListaEstadosPlanific('EstadosPI');
        }elseif($this->atCodTipoProceso=='04'){
            $tituloForm='Reportes de Valoración Jurídica  por Auditor';
            $this->atListaEstadoPlan=$this->atFuncGnrles->metListaEstadosPlanific('EstadosVJ');
        }elseif($this->atCodTipoProceso=='05'){
            $tituloForm='Reportes de Procedimientos Administrativos por Auditor';
            $this->atListaEstadoPlan=$this->atFuncGnrles->metListaEstadosPlanific('EstadosDRA');
        }

        if ($this->atTipoUser == 'userInvitado') {//Si es un usuario invitado, se extraen las dependencias de control al cual está como invitado
            $userInvitado=true;
            $arrDependencia = $this->atFuncGnrles->metBuscaDepControlInvitado($this->atIdContraloria, $this->atIdTipoProceso);
            if ($arrDependencia) {
                if(COUNT($arrDependencia)==1){//Si sólo tiene una dependencia como invitado, Entra.
                    $selectedDep=true;//Permite seleccionar por defecto la dependencia al mostrar el formulario.
                    $listaPersona=$this->metListaPersonal($arrDependencia[0]['id_dependencia']);//Se busca el personal de la dependencia
                }
            }
        }else{
            $listaPersona=$this->metListaPersonal($this->atIdDependenciaUser);
        }
        $this->atVista->assign('listaDependencias', $arrDependencia);
        $this->atVista->assign('listaPersonas', $listaPersona);
        $this->atVista->assign('selectedDep', $selectedDep);
        $this->atVista->assign('userInvitado', $userInvitado);
        //Lista los años a partír de la fecha menor de inicio de las planificaciones.
        $this->atVista->assign('listayears', $this->atFuncGnrles->metComboBoxYears());
        $this->atVista->assign('year_actual', date('Y'));
        /*Lista los tipo de actuación fiscal*/
        $campos = "pk_num_miscelaneo_detalle AS idtipo_actuacion,ind_nombre_detalle AS nombretipo_actuacion";
        if($this->atCodTipoProceso=='04' OR $this->atCodTipoProceso=='05'){//Agrega la opción "Acción Fiscal" a la lista para el combobox Tipo actuación para los casos de Valoración jurídica ó Procedimientos administrativos
            $listaTipoActuacion = $this->atFuncGnrles->metBuscaMiscelanio($campos, 'PFTAF');
        }else{
            $listaTipoActuacion = $this->atFuncGnrles->metBuscaMiscelanio($campos, 'PFTAF','PFAF'); //Menos la opción "Acción Fiscal"
        }
        if(COUNT($listaTipoActuacion)>0){
            $this->atVista->assign('listaTipoActuacion', $listaTipoActuacion);
        }else{
            $this->atVista->assign('listaTipoActuacion', array());
        }
        /*Lista los origen de actuación*/
        $campos="pk_num_miscelaneo_detalle AS idorigen_actuacion,cod_detalle AS cod_origen, ind_nombre_detalle AS nombre_origenactuacion";
        $listaOrigenActuacion=$this->atFuncGnrles->metBuscaMiscelanio($campos,'PFODA','');
        if(COUNT($listaOrigenActuacion)>0){
            $this->atVista->assign('listaOrigenActuacion', $listaOrigenActuacion);
        }else{
            $this->atVista->assign('listaOrigenActuacion', array());
        }
        /*Lista los entes externos*/
        $entesExternos = $this->metBuscarEntes(0);
        if(COUNT($entesExternos)>0){
            $this->atVista->assign('listaentes', $entesExternos["filas"]);
        }else{
            $this->atVista->assign('listaentes', array());
        }
        /*Lista de los estado de la planificación*/
        $this->atVista->assign('tituloForm', $tituloForm);
        $this->atVista->assign('listaEstadosplan', $this->atListaEstadoPlan);
        $this->atVista->metRenderizar('rptPlanificacionAuditor');
    }

    /**
     * Permite listar las planificaciones para mostrarlas en la grilla del formulario ppal.
     */
    public function metListarPlanificaciones(){
        //Se capturan los parámetros
        $arrParams['cbox_yearplan']=$this->metObtenerTexto('cbox_yearplan');
        $arrParams['txt_codPlanificacion']=$this->metObtenerTexto('txt_codPlanificacion'); //OJO configurar
        $arrParams['cbox_dependencia']=$this->metObtenerTexto('cbox_dependencia');
        $arrParams['cbox_tipoActuacion']=$this->metObtenerTexto('cbox_tipoActuacion');
        $arrParams['cbox_origenActuacion']=$this->metObtenerTexto('cbox_origenActuacion');
        $arrParams['cbox_ente']=$this->metObtenerTexto('cbox_ente');
        $arrParams['txt_fechainic1']=$this->metObtenerTexto('txt_fechainic1');
        $arrParams['txt_fechainic2']=$this->metObtenerTexto('txt_fechainic2');
        $arrParams['cbox_estadosplan']=$this->metObtenerTexto('cbox_estadosplan');
        //Se cargan complementos para grilla
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $arrParams["idTipoProceso"] = $this->atIdTipoProceso;
        if($arrParams['txt_fechainic1'] OR $arrParams['txt_fechainic2']) {
            if (!$arrParams['txt_fechainic1']) {
                $arrParams['txt_fechainic1'] = $arrParams['txt_fechainic2'];
            } elseif (!$arrParams['txt_fechainic2']) {
                $arrParams['txt_fechainic2'] = $arrParams['txt_fechainic1'];
            }
            $arrParams['txt_fechainic1'] = $this->atFuncGnrles->metFormateaFechaMsql($arrParams['txt_fechainic1']);
            $arrParams['txt_fechainic2'] = $this->atFuncGnrles->metFormateaFechaMsql($arrParams['txt_fechainic2']);
        }
        $arrParams["id_contraloria"] = $this->atIdContraloria;
        //Si es un usuario invitado, entra
        if ($this->atTipoUser == 'userInvitado') {
            $arrParams["idDependencia"] = $arrParams["cbox_dependencia"];
        }else{
            $arrParams["idDependencia"] = $this->atIdDependenciaUser;
        }
        $arrParams["idPlanificacion"] = null;
        unset($arrParams["cbox_dependencia"]);
        //Lista de planificaciones
        $result = $this->atRptModeloPf->metConsultaPlanificacion($arrParams);
        $data = array();
        if (COUNT($result) > 0) {//Se prepara los resultados.
            foreach ($result as $fila) {
                $fila['ind_estado'] = $fila['ind_estado_planificacion'];
                unset($fila['ind_estado_planificacion']);
                $arrEnte = $this->atFuncGnrles->metBuscaRecursivoEntes($fila['fk_a039_num_ente']);
                $fila['nombre_ente'] = $arrEnte['cadEntes'];
                $fila['desc_estado'] = $this->atFuncGnrles->metEstadoPlanificacion($fila['ind_estado']);
                $fila['fec_inicio'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_inicio']);
                $fila['fec_registro'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_registro_planificacion']);
                unset($fila['fec_registro_planificacion']);
                $fila['fec_ultima_modificacion'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_ultima_modific_planificacion']);
                unset($fila['fec_ultima_modific_planificacion']);
                $arrFechasfin = $this->atFuncGnrles->metBuscaFechaFinPlan($fila['pk_num_planificacion']);
                $data[] = $fila = array_merge($fila, $arrFechasfin);
            }
            $this->atVista->assign('listado', $data);
            $this->atVista->metRenderizar('resultadoListado');
        } else {
            return false;
        }
    }
    /**
     * Permite generar el reporte de planificaciones por auditor.
     * @throws Exception
     */
    public function metGenerarReporteAuditor(){
        //Se asigna los parámetros de búsqueda
        $arrParams=$this->atParametros;
        //Se determina el título del reporte de acuerdo al tipo de proceso fiscal
        if($this->atCodTipoProceso=='01'){
            $tituloReport='ACTUACIÓN FISCAL POR AUDITOR';
            $nombre_pdf='ActuacionFiscal_Auditor.pdf';
        }elseif($this->atCodTipoProceso=='02'){
            $tituloReport='VALORACIÓN PRELIMINAR POR AUDITOR';
            $nombre_pdf='ValoracionPreliminar_Auditor.pdf';
        }elseif($this->atCodTipoProceso=='03'){
            $tituloReport='POTESTAD INVESTIGATIVA POR AUDITOR';
            $nombre_pdf='PotestadInvestigativa_Auditor.pdf';
        }elseif($this->atCodTipoProceso=='04'){
            $tituloReport='VALORACIÓN JURÍDICA POR AUDITOR';
            $nombre_pdf='ValoracionJuridica_Auditor.pdf';
        }elseif($this->atCodTipoProceso=='05'){
            $tituloReport='PROCEDIMIENTOS ADMINISTRATIVOS POR AUDITOR';
            $nombre_pdf='ProcedimientoAdmnttvo_Auditor.pdf';
        }

        if ($this->atTipoUser == 'userInvitado') {//Si es un usuario invitado, entra
            $arrParams["idDependencia"] = $arrParams["idDependencia"];
        }else{//Es un usuario de dependencia de control
            $arrParams["idDependencia"] = $this->atIdDependenciaUser;
        }
        $arrParams['idTipoProceso']=$this->atIdTipoProceso;
        $arrParams['id_contraloria']=$this->atIdContraloria;
        //Se extrae al auditor
        $camposExtraer="ind_cedula_documento AS cedula_auditor,CONCAT_WS(' ',ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2) AS nombre_auditor,ind_nombre_cargo AS cargo_auditor";
        $arrAuditor=$this->atFuncGnrles->metBuscaEmpledos($camposExtraer,$arrParams['cbox_persona'])[0];
        //Se buscan las planificaciones relacionadas con el auditor
        $result = $this->atRptModeloPf->metBuscaPlanificacionAuditor($arrParams);
        //Si hay resultados entra para generar el pdf
        if ($result) {
            $this->metObtenerLibreria('headerRptPlanificacionAuditor', 'modPF');
            $objPdf = new pdfPlanificacionFiscal('L', 'mm', 'Letter');
            //Variables con los valores pertinentes a ser capturados por el encabezado del reporte.
            global $atIdDependenciaUser; $atIdDependenciaUser = $arrParams["idDependencia"]; //permite estraer los datos del encabezado
            global $tituloReporte; $tituloReporte = $tituloReport; //Titulo del reporte
            $objPdf->SetAutoPageBreak(true, 2);
            $objPdf->AddPage();
            $objPdf->SetFillColor(245, 245, 245);
            $objPdf->SetFont('Arial', 'BU', 9);
            $objPdf->Cell(42, 5, utf8_decode('INFORMACIÓN GENERAL.'), 0, 0, 'L', 1);
            $objPdf->Ln(8);
            $objPdf->SetFont('Arial', '', 8);
            $objPdf->Cell(15, 5, utf8_decode('Nombre: '), 0, 0, 'L', 1);
            $objPdf->Cell(55, 5, utf8_decode($arrAuditor['nombre_auditor']), 0, 1, 'L');
            $objPdf->Ln(1);
            $objPdf->Cell(15, 5, utf8_decode('Cargo: '), 0, 0, 'L', 1);
            $objPdf->Cell(55, 5, utf8_decode($arrAuditor['cargo_auditor']), 0, 1, 'L');
            $objPdf->Ln(1);
            //Encabezado de la tabla
            $objPdf->SetFillColor(245, 245, 245);
            $objPdf->SetFont('Arial', 'B', 8);
            $objPdf->SetWidths(array(12, 32, 50, 110, 20, 30));
            $objPdf->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C'));
            $objPdf->Row(array('Nro.', utf8_decode('Código'), utf8_decode('Tipo actuación'), 'Ente', 'Coordinador', utf8_decode('Estado')));
            #Cuerpo de la tabla
            //Fondo filas de las actividades
            $objPdf->SetDrawColor(0, 0, 0);
            $objPdf->SetFillColor(255, 255, 255);
            $objPdf->SetFont('Arial', '', 8);
            $nroFila=1;
            foreach ($result AS $filapln) {
                //Se estrae el ente fiscalizado
                $arrEnte = $this->atFuncGnrles->metBuscaRecursivoEntes($filapln['fk_a039_num_ente'], '\n', 'PDF');
                $filapln['nombre_ente'] = $arrEnte['cadEntes'];
                //Se extrae la descripción del estado de cada planificación
                $filapln['desc_estado'] = $this->atFuncGnrles->metEstadoPlanificacion($filapln['ind_estado_planificacion']);
                //Se determina si el auditor es coordinador ó nó.
                if($filapln['num_flag_coordinador']==1){
                    $coordinador="Si";
                }else{
                    $coordinador="No";
                }
                //Se alinean los datos dentro de las columnas
                $objPdf->SetAligns(array('C', 'C', 'C', 'L', 'C', 'C'));
                //Se emprime los datos en cada fila
                $objPdf->Row(array(
                        $nroFila,
                        $filapln['cod_planificacion'],
                        utf8_decode($filapln['nombre_tipoactuacion']),
                        utf8_decode($filapln['nombre_ente']),
                        $coordinador,
                        utf8_decode($filapln['desc_estado']))
                );
                $nroFila++;
            }
            #salida del pdf
            $objPdf->Output($nombre_pdf, 'I');
        }
    }

    /**
     * Genera un reporte en pdf de Auditores por planificación
     * @throws Exception
     */
    public function metGenerarReporte(){
        $arrParams=$this->atParametros;
        //Se determina el título del reporte de acuerdo al tipo de proceso fiscal
        if($this->atCodTipoProceso=='01'){
            $tituloReport='AUDITORES POR ACTUACIÓN FISCAL';
            $nombre_pdf='Auditor_ActuacionFiscal.pdf';
        }elseif($this->atCodTipoProceso=='02'){
            $tituloReport='AUDITORES POR VALORACIÓN PRELIMINAR';
            $nombre_pdf='Auditor_ValoracionPreliminar.pdf';
        }elseif($this->atCodTipoProceso=='03'){
            $tituloReport='AUDITORES POR POTESTAD INVESTIGATIVA';
            $nombre_pdf='Auditor_PotestadInvestigativa.pdf';
        }elseif($this->atCodTipoProceso=='04'){
            $tituloReport='AUDITORES POR VALORACIÓN JURÍDICA';
            $nombre_pdf='Auditor_ValoracionJuridica.pdf';
        }elseif($this->atCodTipoProceso=='05'){
            $tituloReport='AUDITORES POR PROCEDIMIENTOS ADMINISTRATIVOS';
            $nombre_pdf='Auditor_ProcedimientoAdmnttvo.pdf';
        }
        if ($this->atTipoUser == 'userInvitado') {//Si es un usuario invitado, entra
            $arrParams["idDependencia"] = $arrParams["cbox_dependencia"];
        }else{//Es un usuario de dependencia de control
            $arrParams["idDependencia"] = $this->atIdDependenciaUser;
        }
        $arrParams = array('idPlanificacion' => $arrParams['idPlanificacion'], 'idTipoProceso' => $this->atIdTipoProceso, 'idDependencia' => $arrParams["idDependencia"]);
        $result = $this->atRptModeloPf->metConsultaPlanificacion($arrParams);
        //Si hay resultados entra para generar el pdf.
        if ($result) {
            $filapln=$result[0];
            $this->metObtenerLibreria('headerRptAuditorPlanificacion', 'modPF');
            $objPdf = new pdfPlanificacionFiscal('P', 'mm', 'Letter');
            //Variables con los valores pertinentes a ser capturados por el encabezado del reporte.
            global $atIdDependenciaUser; $atIdDependenciaUser = $arrParams["idDependencia"]; //permite estraer los datos del encabezado
            global $tituloReporte; $tituloReporte = $tituloReport; //Titulo del reporte
            $objPdf->SetAutoPageBreak(true, 2);
            $objPdf->AddPage(); $accionFiscal=false;$tiopProceso=false;
            //Se extraen la fecha fin y totales de la planificación
            $arrTotalesPlan=$this->atFuncGnrles->metBuscaTotalesPlan($filapln['pk_num_planificacion']);
            //Se estrae el ente fiscalizado
            $arrEnte = $this->atFuncGnrles->metBuscaRecursivoEntes($filapln['fk_a039_num_ente'], '\n', 'PDF');
            $filapln['nombre_ente'] = $arrEnte['cadEntes'];

            $filapln['desc_estado'] = $this->atFuncGnrles->metEstadoPlanificacion($filapln['ind_estado_planificacion']);
            $filapln['fec_inicio'] = $this->atFuncGnrles->metFormateaFecha($filapln['fec_inicio']);
            $filapln['fec_registro'] = $this->atFuncGnrles->metFormateaFecha($filapln['fec_registro_planificacion']);
            $filapln['fec_ultima_modificacion'] = $this->atFuncGnrles->metFormateaFecha($filapln['fec_ultima_modific_planificacion']);
            if($filapln['fec_nota_justificacion']){
                $filapln['fec_nota_justificacion'] = $this->atFuncGnrles->metFormateaFecha($filapln['fec_nota_justificacion']);
            }
            if($filapln['fk_a004_num_dependencia_solctte']){//Si se trata de una valoración jurídica del tipo "Acción Fiscal". Entra.
                //Se verifica si la dependencia solicitante es de control fiscal
                $ArrProcsFiscalDep=$this->atFuncGnrles->metBuscaProcesosFiscal($filapln['fk_a004_num_dependencia_solctte']);
                $campos="ind_dependencia AS nombre_depSolctante";
                if(COUNT($ArrProcsFiscalDep)){
                    if($filapln['idplanificacion_referencia']){
                        //Busca el código de la planificación que dió origen a la acción fiscal y su correspondiente proceso fiscal
                        $ArrResult=$this->atFuncGnrles->metBuscaCodPlanificacion(array('idPlanificacion'=>$filapln['idplanificacion_referencia']));
                        $filapln['codPlanificAccf'] = $ArrResult[0]["codPlanificacion"];
                        $procesoFiscal=$this->atRptModeloPf->metProcesoFiscal($ArrResult[0]["idTipoProcFiscal"]);//Se busca el nombre del tipo de proceso fiscal
                        $filapln['nombre_procesoAccf'] = $procesoFiscal['nombre_proceso'];
                        $tiopProceso=true;
                    }
                }
                //Se busca el nombre de la dependencia solicitante de la acción fiscal
                $arrDepSolctante=$this->atFuncGnrles->metBuscaDependenciaInterna($campos,$filapln['fk_a004_num_dependencia_solctte']);
                $filapln['nombre_dependenciaAccf'] = $arrDepSolctante[0]['nombre_depSolctante'];
                $accionFiscal=true;
            }
            $filapln['Desc_tipoProcedimiento']="";
            if($filapln['ind_procdmtoadministrativo']){//Se busca el tipo de procedimiento si lo tiene
                $arrProcdmnto=$this->atFuncGnrles->metTiposProcedimientosAdmin();
                foreach($arrProcdmnto AS $fila){
                    if($fila['valor']==$filapln['ind_procdmtoadministrativo']){
                        $filapln['Desc_tipoProcedimiento']=$fila['descripcion'];
                    }
                }
                $arrProcdmnto=null;
            }
            $objPdf->SetFont('Arial', 'B', 9);
            $objPdf->Cell(60, 10, utf8_decode('Planificación Nro: ' . $filapln['cod_planificacion']), 0, 1, 'L');
            $objPdf->Ln(1);
            $objPdf->SetFillColor(245, 245, 245);
            $objPdf->SetFont('Arial', 'BU', 9);
            $objPdf->Cell(42, 5, utf8_decode('INFORMACIÓN GENERAL.'), 0, 0, 'L', 1);
            $objPdf->Ln(8);
            $objPdf->SetFont('Arial', '', 8);
            $objPdf->Cell(30, 5, utf8_decode('Tipo de Actuación: '), 0, 0, 'L', 1);
            $objPdf->Cell(55, 5, utf8_decode($filapln['nombre_tipoactuacion']), 0, 0, 'L');
            $objPdf->Cell(12, 5, utf8_decode('Origen: '), 0, 0, 'R', 1);
            $objPdf->Cell(40, 5, utf8_decode($filapln['nombre_origenactuacion']), 0, 0, 'L');
            $objPdf->Cell(30, 5, utf8_decode('Fecha inicio:'), 0, 0, 'R', 1);
            $objPdf->Cell(40, 5, $filapln['fec_inicio'], 0, 1, 'L');
            $objPdf->Ln(1);
            if($accionFiscal){
                $objPdf->Cell(30, 5, utf8_decode('Dep. solicitante:'), 0, 0, 'L', 1);
                $objPdf->Cell(55, 5, utf8_decode($filapln['nombre_dependenciaAccf']), 0, 1, 'L');
                $objPdf->Ln(1);
                if($tiopProceso){
                    $objPdf->Cell(30, 5, utf8_decode('Tipo proceso: '), 0, 0, 'L', 1);
                    $objPdf->Cell(40, 5, utf8_decode($filapln['nombre_procesoAccf']), 0, 0, 'L');
                    $objPdf->Cell(30, 5, utf8_decode('Originada por:'), 0, 0, 'R', 1);
                    $objPdf->Cell(40, 5, $filapln['codPlanificAccf'], 0, 1, 'L');
                    $objPdf->Ln(1);
                }
            }
            $objPdf->Cell(30, 5, 'Ente: ', 0, 0, 'L', 1);
            $objPdf->MultiCell(165, 5, utf8_decode($filapln['nombre_ente']), 0, 'L');
            $objPdf->Ln(1);
            $objPdf->Cell(30, 5, 'Objetivo General: ', 0, 0, 'L', 1);
            $objPdf->MultiCell(165, 5, utf8_decode($filapln['ind_objetivo']), 0, 'J');
            $objPdf->Ln(1);
            $objPdf->Cell(30, 5, 'Alcance: ', 0, 0, 'L', 1);
            $objPdf->MultiCell(165, 5, utf8_decode($filapln['ind_alcance']), 0, 'J');
            $objPdf->Ln(1);
            $objPdf->Cell(30, 5, 'Observaciones: ', 0, 0, 'L', 1);
            $objPdf->MultiCell(165, 5, utf8_decode($filapln['ind_observacion']), 0, 'J');
            $objPdf->Ln(1);
            if($filapln['Desc_tipoProcedimiento']){
                $objPdf->Cell(30, 5, 'Procedimiento: ', 0, 0, 'L', 1);
                $objPdf->MultiCell(165, 5, utf8_decode($filapln['Desc_tipoProcedimiento']), 0, 'L');
                $objPdf->Ln(1);
            }
            if($filapln['fec_nota_justificacion'] AND $filapln['ind_nota_justificacion']){
                $objPdf->Cell(30, 5, 'Estado: ', 0, 0, 'L', 1);
                $objPdf->Cell(30, 5, utf8_decode($filapln['desc_estado']), 0, 0, 'L');
                $objPdf->Cell(26, 5, utf8_decode('Fecha devolución:'), 0, 0, 'R', 1);
                $objPdf->Cell(30, 5, utf8_decode($filapln['fec_nota_justificacion']), 0, 1, 'L');
                $objPdf->Ln(1);
                $objPdf->Cell(30, 5, utf8_decode('Nota devolución: '), 0, 0, 'L', 1);
                $objPdf->MultiCell(165, 5, utf8_decode($filapln['ind_nota_justificacion']), 0, 'J');
                $objPdf->Ln(1);
            }else{
                $objPdf->Cell(30, 5, 'Estado: ', 0, 0, 'L', 1);
                $objPdf->Cell(30, 5, utf8_decode($filapln['desc_estado']), 0, 1, 'L');
                $objPdf->Ln(1);
            }
            $objPdf->Cell(30, 5, 'Fecha fin plan: ', 0, 0, 'L', 1);
            $objPdf->Cell(18, 5, $arrTotalesPlan['fecha_fin_plan'], 0, 0, 'L');
            $objPdf->Cell(20, 5, utf8_decode('Días afecta:'), 0, 0, 'R', 1);
            $objPdf->Cell(10, 5, $arrTotalesPlan['cant_dias_afecta'], 0, 0, 'L');
            $objPdf->Cell(20, 5, utf8_decode('Días no afecta:'), 0, 0, 'R', 1);
            $objPdf->Cell(10, 5, $arrTotalesPlan['cant_dias_no_afecta'], 0, 0, 'L');
            $objPdf->Cell(15, 5, utf8_decode('Prórroga:'), 0, 0, 'R', 1);
            $objPdf->Cell(10, 5, $arrTotalesPlan['cant_dias_prorroga'], 0, 0, 'L');
            $objPdf->Cell(22, 5, utf8_decode('Duración Total :'), 0, 0, 'R', 1);
            $objPdf->Cell(10, 5, $arrTotalesPlan['totalDias_Plan'], 0, 1, 'L');
            $objPdf->Ln(5);

            # IMPRIME LOS DATOS DE LOS AUDITORES
            $dataAuditores = array();
            $arrAuditores = $this->atRptModeloPf->metAuditoresDesignados($filapln['pk_num_planificacion']);//Se extraen los auditores designados
            //Título de la sección de auditores
            $objPdf->SetDrawColor(0, 0, 0);
            $objPdf->SetFillColor(255, 255, 255);
            $objPdf->SetFont('Arial', 'BU', 9);
            $objPdf->Cell(30, 5, utf8_decode('AUDITORES.'), 0, 0, 'L', 1);
            $objPdf->Ln(5);
            if (COUNT($arrAuditores) > 0) {
                foreach ($arrAuditores as $fila) {
                    $camposExtraer = "ind_cedula_documento AS cedula_auditor,CONCAT_WS(' ',ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2) AS nombre_auditor,ind_nombre_cargo AS cargo_auditor";
                    $arrAuditor = $this->atFuncGnrles->metBuscaEmpledos($camposExtraer, $fila['fk_rhb001_num_empleado']);
                    $dataAuditores[] = array("id_auditorPlanificacion" => $fila['pk_num_auditor_planificacion'],
                        "pk_num_empleado" => $fila['fk_rhb001_num_empleado'], "nombre_auditor" => $arrAuditor[0]['nombre_auditor'],
                        "cargo_auditor" => $arrAuditor[0]['cargo_auditor'], "flagCordinador" => $fila['num_flag_coordinador'],
                        "desc_estatus" => $this->atFuncGnrles->metEstatus($fila['num_estatus']),
                        "fecha_estatus" => $this->atFuncGnrles->metFormateaFecha($fila['fec_estatus'])
                    );
                }
                //Encabezado de la tabla
                $objPdf->SetFillColor(245, 245, 245);
                $objPdf->SetFont('Arial', 'B', 8);
                $objPdf->Cell(24, 5, utf8_decode('Coordinador(a)'), 1, 0, 'C', 1);
                $objPdf->Cell(80, 5, utf8_decode('Nombre'), 1, 0, 'C', 1);
                $objPdf->Cell(76, 5, utf8_decode('Cargo'), 1, 0, 'C', 1);
                $objPdf->Cell(20, 5, utf8_decode('Credencial'), 1, 0, 'C', 1);
                $objPdf->Ln();
                //Cuerpo de la tabla
                $objPdf->SetDrawColor(0, 0, 0);
                $objPdf->SetFillColor(255, 255, 255);
                $objPdf->SetFont('Arial', '', 8);
                foreach ($dataAuditores AS $row) {
                    $y = $objPdf->GetY();
                    $iCon = '';
                    if ($row['flagCordinador'] == "1") {
                        $iCon = $objPdf->Image('publico/imagenes/modPF/checked.jpg', 20, $y + 0.9, 2, 3);
                    }
                    $objPdf->Cell(24, 5, $iCon, 1);
                    $objPdf->Cell(80, 5, utf8_decode($row['nombre_auditor']), 1);
                    $objPdf->Cell(76, 5, utf8_decode($row['cargo_auditor']), 1, 0, 'C');
                    $objPdf->Cell(20, 5, utf8_decode($row['desc_estatus']), 1, 0, 'C');
                    $objPdf->Ln();
                }
                $objPdf->Cell(200, 0, '', 'T');
                $objPdf->Ln(5);
            }else{
                $objPdf->SetFont('Arial', '', 9);
                $objPdf->Cell(40, 5, 'No hay auditores asignados', 0, 1, 'L');
            }
            #salida del pdf
            $objPdf->Output($nombre_pdf, 'I');
        }
    }
}
?>