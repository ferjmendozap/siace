<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: PLANIFICACION FISCAL
 * PROCESO: REPORTES DEL MODULO
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Idelina Ponte                    |     ponte.idelina@cmldc.gob.ve     |         0412-9045109           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-12-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class reporteAuditoresValoracionJuridicaControlador extends Controlador
{
    private $atFPDF;   
    private $atConsultasComunes;
  
    public function __construct()
    {
        parent::__construct();
        $this->atConsultasComunes = $this->metCargarModelo('consultasGenerales', 'reportes');
        $this->metObtenerLibreria('headerAuditoresValoracionJuridica', 'modPF');
        $this->atFPDF = new pdfAuditoresValoracionJuridica('L', 'mm', 'Letter');
    }

    public function metIndex()
    { 
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('tipoOrganismo', $this->atConsultasComunes->metListarOrganismo());
        $this->atVista->assign('tipoProceso', $this->atConsultasComunes->metListarProceso());
        $this->atVista->metRenderizar('reporteAuditoresValoracionJuridica');
    }
    public function metListarDependencias()
    {
       $id_organismo=$_POST['idOrganismo'];
       $resultado=$this->atConsultasComunes->metListarDependencia($id_organismo);
       echo "<option value=''>Seleccione la Dependencia</option>";
       for ($i=0; $i < count($resultado); $i++) {
          echo "<option value=".$resultado[$i]['pk_num_dependencia'].">".$resultado[$i]['ind_dependencia']."</option>";
       }
    }
      public function metListarAuditoresCombo()
    {
       $id_organismo=$_POST['idOrganismo'];
       $resultado=$this->atConsultasComunes->metListarAuditores($id_organismo);
       echo "<option value=''>Seleccione al Auditor</option>";
       for ($i=0; $i < count($resultado); $i++) {
          echo "<option value=".$resultado[$i]['ind_cedula_documento'].">".$resultado[$i]['nombre']."</option>";
       }
    }
    public function metPdfAuditoresValoracionJuridica($id_organismo,$num_dependencia,$num_proceso,$num_empleado,$id_tipo_reporte)
    {
        $actuacion=$this->atConsultasComunes->metListarActuacionesAuditores($id_organismo,$num_dependencia,$num_proceso,$num_empleado,$id_tipo_reporte);
        $organismo=$this->atConsultasComunes->metListarDatosOrganismo($id_organismo);
        $dependencia=$this->atConsultasComunes->metNombreDependencia($num_dependencia);
        $datAuditor=$this->atConsultasComunes->metDatosAuditor($num_empleado);
        $tipoAct=$this->atConsultasComunes->metTipoActuacion($num_proceso);
        #### PDF ####
        $this->atFPDF->setTipoHeader('valoracion');
        $this->atFPDF->SetOrganismo($organismo[0]['ind_descripcion_empresa']);
        $this->atFPDF->SetNombreAuditor($datAuditor[0]['nombre']);
        $this->atFPDF->SetCargoAuditor($datAuditor[0]['ind_descripcion_cargo']);
        $this->atFPDF->SetDependencia($dependencia[0]['ind_dependencia']);
        $this->atFPDF->SetTipoProceso($tipoAct[0]['ind_nombre_detalle']);
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 5);
        $this->atFPDF->SetAutoPageBreak(10);
        $this->atFPDF->AddPage();
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 8); 
        $n = 1; 
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 8); 
        if (count($actuacion)==0){
            $this->atFPDF->SetFont('Arial', 'B', 11);
            $this->atFPDF->SetXY(15, 68);
            $this->atFPDF->Cell(240, 10, utf8_decode('NO EXISTEN REGISTROS PARA LA CONSULTA REALIZADA. ')." (".strtoupper(utf8_decode($this->atFPDF->getTipoProceso()." - VALORACIÓN JURÍDICA")).") ", 0, 1, 'C');
        }
        for ($i=0; $i < count($actuacion); $i++) {
            $entes=$this->atConsultasComunes->funcionesGenerales->metBuscaRecursivoEntes($actuacion[$i]['fk_a039_num_ente'], $saltolinea=NULL);
            $entes=str_replace("<br>"," ",$entes);
            $ente_padre=explode("-",$entes['cadEntes']);
            $this->atFPDF->SetWidths(array(10,40,37,80,20,45));
            $this->atFPDF->SetAligns(array('C','C','C','C','C','C'));
            $this->atFPDF->Row(array(
                    $n,
                    $actuacion[$i]['cod_planificacion'],
                    ucwords(strtolower(utf8_decode($actuacion[$i]['ind_nombre_detalle']))),
                    utf8_decode($ente_padre[1]."  ".$actuacion[$i]['ind_nombre_ente']),
                    $actuacion[$i]['coordinador'],
                    utf8_decode(ucwords(strtolower($actuacion[$i]['estado_planificacion']))),
                    ));
            $n++;
        }
        $this->atFPDF->Output();
    }
}
?>