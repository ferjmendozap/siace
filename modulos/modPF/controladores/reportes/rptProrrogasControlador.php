<?php
/***********************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Planificación Fiscal
 * PROCESO: Reportes de Prórrogas de actividades.
 * DESCRIPCIÓN: Permite la extración de datos para la consulta y reportes en pdf de las planificaciones cuyas
 *              actividades contienen prórrogas en sus diferentes proceso fiscales tales como: Actuación fiscal,
 *              Valoración preliminar, Potestad investigativa,Valoración jurídica y Procedimientos administrativos.
 * PROGRAMADORES.______________________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Alexis Ontiveros     | ontiveros.alexis@cmldc.gob.ve        | 0426-514.43.82
 * |___________________________________________________________________________________________________________________
 *
 * VERSION
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        02-08-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 ***********************************************************************************************************************/
require_once RUTA_Modulo . 'modPF' . DS . 'modelos' . DS . 'funcionesGenerales.php';
class rptProrrogasControlador extends Controlador{
    public $atRptProrrModelo;
    private $atFuncGnrles;          //Para las Funciones generales
    private $atIdContraloria;       //Para el Id de la contraloría que ejecuta.
    public $atIdDependenciaUser;    //Para el Id de la dependencia al cual pertenece el usuario.
    private $atIdEmpleadoUser;      //Para el Id empleado del usuario que ejecuta.
    private $atTipoUser;            //Para el tipo de usuario logueado
    private $atIdTipoProceso;       //Para el Id del tipo de proceso fiscal.
    private $atUserValido;          //Contendrá la validéz del usuario
    private $atCodTipoProceso;      //Para el código del tipo de proceso
    private $atParametros;          //Para asignar los parámetros

    public function __construct(){
        parent::__construct();
        $this->atRptProrrModelo = $this->metCargarModelo('rptProrrogas', 'reportes');
        $this->atFuncGnrles = new funcionesGenerales();
        if(isset($_GET['tipoProceso'])){
            $this->atCodTipoProceso=$_GET['tipoProceso'];//Dato que viene en la url de la opción de menú
        }else{
            if(isset($_GET['obj'])){ //Entra cuando viene de la vista al clicar en el botón principal PDF ó en el botón PDF correspondiente a una planificación
                $arrParams=$_GET['obj']; //Se captura los parámetros codificado en base 64.
                $decodeParams=base64_decode($arrParams); //Se descodifica el json
                $arrParams=json_decode($decodeParams,true); //Se trasforma el json en un array php
                $this->atCodTipoProceso=$arrParams['CodTipoProceso'];
                $this->atParametros = $arrParams;
            }else{//Entra cuando viene de la vista al clicar el botón Buscar
                $this->atCodTipoProceso=$this->metObtenerTexto('CodTipoProceso');
            }
        }
        //Se determina la validéz y el tipo de usuario.
        $arrUser = $this->atFuncGnrles->metValidaUsuario();
        if ($arrUser['userValido']) {//Entra, si es un Usuario Válido.
            $this->atTipoUser = $arrUser['tipoUser'];
            $this->atUserValido = $arrUser['userValido'];
            $this->atIdContraloria = $arrUser['pk_num_organismo'];
            $this->atIdEmpleadoUser = $arrUser['pk_num_empleado'];
            $this->atIdDependenciaUser = $arrUser['pk_num_dependencia'];
            /*Se busca y asigna el id del tipo de proceso de acuerdo al código del mísmo*/
            $arr_tipoproc = $this->atFuncGnrles->metTipoProcesoFiscal(null, $this->atCodTipoProceso);
            $this->atIdTipoProceso = $arr_tipoproc[0]['pk_num_proceso'];
            $arr_tipoproc = null;
        } else {//De lo contrario, usuario no válido
            $this->atVista->assign('valido', $this->atUserValido);
            $this->atVista->metRenderizar('../../error');
        }
        $arrUser = null;
    }
    function __destruct(){
        $atIdDependenciaUser=null; $tituloReporte=null;
    }
    /**
     * Busca las planificaciones para extraer los correlativos y montarlos el combobox "Nro. Planificación",
     * siempre y cuando existan de acuerdo a los parámetros de filtrado.
     * @return array
     */
    function metListaCodPlanificaciones($vieneIndex=NULL){
        if($vieneIndex){
            $yearActual = date('Y');
        }else{
            $yearActual=$this->metObtenerInt('cbox_yearplan');
            if($this->metObtenerInt('idDependencia')>0){
                $this->atIdDependenciaUser=$this->metObtenerInt('idDependencia');
            }
        }
        $params = array('idTipoProceso' => $this->atIdTipoProceso, 'idContraloria' => $this->atIdContraloria,
            'idDependencia' => $this->atIdDependenciaUser, 'yearPlanificacion' => $yearActual);
        $arrNrosPlanifi = $this->atRptProrrModelo->metBuscaCodigosPlan($params);
        if($vieneIndex){
            return $arrNrosPlanifi;
        }else{
            echo json_encode(array('filas'=>$arrNrosPlanifi));
        }
    }
    public function metIndex(){
        $userInvitado=false; $listaNrosPlanif=array(); $selectedDep=false;
        $this->atVista->assign('tempYear', date('Y'));
        $this->atVista->assign('CodTipoProceso', $this->atCodTipoProceso);
        //Se determina los títulos del formulario.
        //$perfilesUser=Session::metObtener('perfil');
        if($this->atCodTipoProceso=='01'){
            $tituloForm='Reportes de Prórrogas de Actuaciones Fiscales';
        }elseif($this->atCodTipoProceso=='02'){
            $tituloForm='Reportes de Prórrogas de Valoración Preliminar';
        }elseif($this->atCodTipoProceso=='03'){
            $tituloForm='Reportes de Prórrogas de Potestad Investigativa';
        }elseif($this->atCodTipoProceso=='04'){
            $tituloForm='Reportes de Prórrogas de Valoración Jurídica';
        }elseif($this->atCodTipoProceso=='05'){
            $tituloForm='Reportes de Prórrogas de Procedimientos Administrativos';
        }
        //Si es un usuario invitado se extraen las dependencias de control al cual está como invitado
        if ($this->atTipoUser == 'userInvitado') {
            $userInvitado=true;
            $arrDependencia = $this->atFuncGnrles->metBuscaDepControlInvitado($this->atIdContraloria, $this->atIdTipoProceso);
            if ($arrDependencia) {
                if(COUNT($arrDependencia)==1){//Se buscan los códigos de las planificaciones.
                    $this->atIdDependenciaUser=$arrDependencia[0]['id_dependencia'];
                    $listaNrosPlanif = $this->metListaCodPlanificaciones(true);
                    $selectedDep=true;
                }
                $this->atVista->assign('listaDependencias', $arrDependencia);
            } else {
                $this->atVista->assign('listaDependencias', array());
            }
        }else{
            //Lista de números de planificaciones
            $listaNrosPlanif = $this->metListaCodPlanificaciones(true);
        }
        $this->atVista->assign('idDependencia', $this->atIdDependenciaUser);
        $this->atVista->assign('listaNrosPlanif', $listaNrosPlanif);
        $this->atVista->assign('selectedDep', $selectedDep);
        $this->atVista->assign('userInvitado', $userInvitado);
        //Lista los años a partír de la fecha menor de inicio de las planificaciones.
        $this->atVista->assign('listayears', $this->atFuncGnrles->metComboBoxYears());
        $this->atVista->assign('year_actual', date('Y'));
        /*Lista de los estado de la planificación*/
        $this->atVista->assign('tituloForm', $tituloForm);
        //Lanza el formulario
        $this->atVista->metRenderizar('rptProrrogas');
    }
    /**
     * Permite listar las planificaciones para mostrarlas en la grilla del formulario ppal.
     */
    public function metListarPlanificaciones(){
        //Se capturan los parámetros
        $arrParams['cbox_yearplan']=$this->metObtenerTexto('cbox_yearplan');
        $arrParams['cbox_codPlanificacion']=$this->metObtenerTexto('cbox_codPlanificacion');
        $arrParams['cbox_dependencia']=$this->metObtenerTexto('cbox_dependencia');
        //Se cargan complementos para grilla
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $arrParams["idTipoProceso"] = $this->atIdTipoProceso;
        $arrParams["id_contraloria"] = $this->atIdContraloria;
        //Si es un usuario invitado, entra
        if ($this->atTipoUser == 'userInvitado') {
            $arrParams["id_dependencia"] = $arrParams["cbox_dependencia"];
        }else{
            $arrParams["id_dependencia"] = $this->atIdDependenciaUser;
        }
        $arrParams["idPlanificacion"] = null;
        unset($arrParams["cbox_dependencia"]);
        //Lista de planificaciones cuyas actividades tengan prórrogas
        $result = $this->atRptProrrModelo->metConsultaPlanificacion($arrParams);
        $data = array();
        if (COUNT($result) > 0) {//Se prepara los resultados.
            foreach ($result as $fila) {
                $fila['ind_estado'] = $fila['ind_estado_planificacion']; unset($fila['ind_estado_planificacion']);
                $arrEnte = $this->atFuncGnrles->metBuscaRecursivoEntes($fila['fk_a039_num_ente']);
                $fila['nombre_ente'] = $arrEnte['cadEntes'];
                $fila['desc_estado'] = $this->atFuncGnrles->metEstadoPlanificacion($fila['ind_estado']);
                $fila['fec_inicio'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_inicio']);
                unset($fila['fec_registro_planificacion']);unset($fila['fec_ultima_modific_planificacion']);
                //Se busca la fecha fin plan
                $arrFechasfin = $this->atFuncGnrles->metBuscaFechaFinPlan($fila['pk_num_planificacion']);
                $data[] = $fila = array_merge($fila, $arrFechasfin);
            }
            $this->atVista->assign('listado', $data);
            $this->atVista->metRenderizar('resultadoListado');
        } else {
            return false;
        }
    }
    /**
     * Genera un reporte en pdf para una planificación en particular ó varias planificaciones contentivas de prórrogas
     * @throws Exception
     */
    public function metGenerarReporte(){
        $arrParams=$this->atParametros;
        //Se determina el título del reporte de acuerdo al tipo de proceso fiscal
        if($this->atCodTipoProceso=='01'){
            $tituloReport='ACTUACIÓN FISCAL';
            $nombre_pdf='ProrrogaActuacionFiscal.pdf';
        }elseif($this->atCodTipoProceso=='02'){
            $tituloReport='VALORACIÓN PRELIMINAR';
            $nombre_pdf='ProrrogaValoracionPreliminar.pdf';
        }elseif($this->atCodTipoProceso=='03'){
            $tituloReport='POTESTAD INVESTIGATIVA';
            $nombre_pdf='ProrrogaPotestadInvestigativa.pdf';
        }elseif($this->atCodTipoProceso=='04'){
            $tituloReport='VALORACIÓN JURÍDICA';
            $nombre_pdf='ProrrogaValoracionJuridica.pdf';
        }elseif($this->atCodTipoProceso=='05'){
            $tituloReport='PROCEDIMIENTOS ADMINISTRATIVOS';
            $nombre_pdf='ProrrogaProcedimientoAdmnttvo.pdf';
        }

        if ($this->atTipoUser == 'userInvitado') {//Si es un usuario invitado, entra
            $arrParams["id_dependencia"] = $arrParams["cbox_dependencia"];
        }else{//Es un usuario de dependencia de control
            $arrParams["id_dependencia"] = $this->atIdDependenciaUser;
        }
        if ($arrParams['idPlanificacion']) { //Extrae una planificación en particular
            $arrParams = array('idPlanificacion' => $arrParams['idPlanificacion'], 'idTipoProceso' => $this->atIdTipoProceso, 'id_dependencia' => $arrParams["id_dependencia"]);
            $result = $this->atRptProrrModelo->metConsultaPlanificacion($arrParams);
        } else { //Extrae todas ó un lote de planificaciones de acuerdo a los valores de búsqueda
            $arrParams['idTipoProceso']=$this->atIdTipoProceso;
            $arrParams['id_contraloria']=$this->atIdContraloria;
            $result = $this->atRptProrrModelo->metConsultaPlanificacion($arrParams);
        }
        //Si hay resultados entra para generar el pdf de una o varias planficaciones
        if ($result) {
            $this->metObtenerLibreria('headerRptProrrogas', 'modPF');
            $objPdf = new pdfPlanificacionFiscal('P', 'mm', 'Letter');
            //Variables con los valores pertinentes a ser capturados por el encabezado del reporte.
            global $atIdDependenciaUser; $atIdDependenciaUser = $arrParams["id_dependencia"]; //permite estraer los datos del encabezado
            global $tituloReporte; $tituloReporte = $tituloReport; //Titulo del reporte
            $objPdf->SetAutoPageBreak(true, 2);
            foreach ($result AS $filapln) {
                $objPdf->AddPage(); $accionFiscal=false;$tiopProceso=false;
                //Se extraen la fecha fin y totales de la planificación
                $arrTotalesPlan=$this->atFuncGnrles->metBuscaTotalesPlan($filapln['pk_num_planificacion']);
                //Se estrae el ente fiscalizado
                $arrEnte = $this->atFuncGnrles->metBuscaRecursivoEntes($filapln['fk_a039_num_ente'], '\n', 'PDF');
                $filapln['nombre_ente'] = $arrEnte['cadEntes'];

                $filapln['desc_estado'] = $this->atFuncGnrles->metEstadoPlanificacion($filapln['ind_estado_planificacion']);
                $filapln['fec_inicio'] = $this->atFuncGnrles->metFormateaFecha($filapln['fec_inicio']);
                $filapln['fec_registro'] = $this->atFuncGnrles->metFormateaFecha($filapln['fec_registro_planificacion']);
                $filapln['fec_ultima_modificacion'] = $this->atFuncGnrles->metFormateaFecha($filapln['fec_ultima_modific_planificacion']);
                if($filapln['fec_nota_justificacion']){
                    $filapln['fec_nota_justificacion'] = $this->atFuncGnrles->metFormateaFecha($filapln['fec_nota_justificacion']);
                }
                if($filapln['fk_a004_num_dependencia_solctte']){//Si se trata de una planificación del tipo "Acción Fiscal". (Valoración jurídica y Procedimientos administrativos). Entra
                    //Se verifica si la dependencia solicitante es de control fiscal
                    $ArrProcsFiscalDep=$this->atFuncGnrles->metBuscaProcesosFiscal($filapln['fk_a004_num_dependencia_solctte']);
                    $campos="ind_dependencia AS nombre_depSolctante";
                    if(COUNT($ArrProcsFiscalDep)){
                        if($filapln['idplanificacion_referencia']){
                            //Busca el código de la planificación que dió origen a la acción fiscal y su correspondiente proceso fiscal
                            $ArrResult=$this->atFuncGnrles->metBuscaCodPlanificacion(array('idPlanificacion'=>$filapln['idplanificacion_referencia']));
                            $filapln['codPlanificAccf'] = $ArrResult[0]["codPlanificacion"];
                            $procesoFiscal=$this->atRptProrrModelo->metProcesoFiscal($ArrResult[0]["idTipoProcFiscal"]);//Se busca el nombre del tipo de proceso fiscal
                            $filapln['nombre_procesoAccf'] = $procesoFiscal['nombre_proceso'];
                            $tiopProceso=true;
                        }
                    }
                    //Se busca el nombre de la dependencia solicitante de la acción fiscal
                    $arrDepSolctante=$this->atFuncGnrles->metBuscaDependenciaInterna($campos,$filapln['fk_a004_num_dependencia_solctte']);
                    $filapln['nombre_dependenciaAccf'] = $arrDepSolctante[0]['nombre_depSolctante'];
                    $accionFiscal=true;$ArrProcsFiscalDep=null;
                }
                $filapln['Desc_tipoProcedimiento']="";
                if($filapln['ind_procdmtoadministrativo']){//Se busca el tipo de procedimiento si lo tiene (Valoración jurídica y Procedimientos administrativos). Entra
                    $arrProcdmnto=$this->atFuncGnrles->metTiposProcedimientosAdmin();
                    foreach($arrProcdmnto AS $fila){
                        if($fila['valor']==$filapln['ind_procdmtoadministrativo']){
                            $filapln['Desc_tipoProcedimiento']=$fila['descripcion'];
                        }
                    }
                    $arrProcdmnto=null;
                }

                $objPdf->SetFont('Arial', 'B', 9);
                $objPdf->Cell(60, 10, utf8_decode('Planificación Nro: ' . $filapln['cod_planificacion']), 0, 1, 'L');
                $objPdf->Ln(1);
                $objPdf->SetFillColor(245, 245, 245);
                $objPdf->SetFont('Arial', 'BU', 9);
                $objPdf->Cell(42, 5, utf8_decode('INFORMACIÓN GENERAL.'), 0, 0, 'L', 1);
                $objPdf->Ln(8);
                $objPdf->SetFont('Arial', '', 8);
                $objPdf->Cell(30, 5, utf8_decode('Tipo de Actuación: '), 0, 0, 'L', 1);
                $objPdf->Cell(55, 5, utf8_decode($filapln['nombre_tipoactuacion']), 0, 0, 'L');
                $objPdf->Cell(12, 5, utf8_decode('Origen: '), 0, 0, 'R', 1);
                $objPdf->Cell(40, 5, utf8_decode($filapln['nombre_origenactuacion']), 0, 0, 'L');
                $objPdf->Cell(30, 5, utf8_decode('Fecha inicio:'), 0, 0, 'R', 1);
                $objPdf->Cell(40, 5, $filapln['fec_inicio'], 0, 1, 'L');
                $objPdf->Ln(1);
                if($accionFiscal){
                    $objPdf->Cell(30, 5, utf8_decode('Dep. solicitante:'), 0, 0, 'L', 1);
                    $objPdf->Cell(55, 5, utf8_decode($filapln['nombre_dependenciaAccf']), 0, 1, 'L');
                    $objPdf->Ln(1);
                    if($tiopProceso){
                        $objPdf->Cell(30, 5, utf8_decode('Tipo proceso: '), 0, 0, 'L', 1);
                        $objPdf->Cell(40, 5, utf8_decode($filapln['nombre_procesoAccf']), 0, 0, 'L');
                        $objPdf->Cell(30, 5, utf8_decode('Originada por:'), 0, 0, 'R', 1);
                        $objPdf->Cell(40, 5, $filapln['codPlanificAccf'], 0, 1, 'L');
                        $objPdf->Ln(1);
                    }
                }
                $objPdf->Cell(30, 5, 'Ente: ', 0, 0, 'L', 1);
                $objPdf->MultiCell(165, 5, utf8_decode($filapln['nombre_ente']), 0, 'L');
                $objPdf->Ln(1);
                $objPdf->Cell(30, 5, 'Objetivo General: ', 0, 0, 'L', 1);
                $objPdf->MultiCell(165, 5, utf8_decode($filapln['ind_objetivo']), 0, 'J');
                $objPdf->Ln(1);
                $objPdf->Cell(30, 5, 'Alcance: ', 0, 0, 'L', 1);
                $objPdf->MultiCell(165, 5, utf8_decode($filapln['ind_alcance']), 0, 'J');
                $objPdf->Ln(1);
                $objPdf->Cell(30, 5, 'Observaciones: ', 0, 0, 'L', 1);
                $objPdf->MultiCell(165, 5, utf8_decode($filapln['ind_observacion']), 0, 'J');
                $objPdf->Ln(1);
                if($filapln['Desc_tipoProcedimiento']){
                    $objPdf->Cell(30, 5, 'Procedimiento: ', 0, 0, 'L', 1);
                    $objPdf->MultiCell(165, 5, utf8_decode($filapln['Desc_tipoProcedimiento']), 0, 'L');
                    $objPdf->Ln(1);
                }
                if($filapln['fec_nota_justificacion'] AND $filapln['ind_nota_justificacion']){
                    $objPdf->Cell(30, 5, 'Estado: ', 0, 0, 'L', 1);
                    $objPdf->Cell(30, 5, utf8_decode($filapln['desc_estado']), 0, 0, 'L');
                    $objPdf->Cell(26, 5, utf8_decode('Fecha devolución:'), 0, 0, 'R', 1);
                    $objPdf->Cell(30, 5, utf8_decode($filapln['fec_nota_justificacion']), 0, 1, 'L');
                    $objPdf->Ln(1);
                    $objPdf->Cell(30, 5, utf8_decode('Nota devolución: '), 0, 0, 'L', 1);
                    $objPdf->MultiCell(165, 5, utf8_decode($filapln['ind_nota_justificacion']), 0, 'J');
                    $objPdf->Ln(1);
                }else{
                    $objPdf->Cell(30, 5, 'Estado: ', 0, 0, 'L', 1);
                    $objPdf->Cell(30, 5, utf8_decode($filapln['desc_estado']), 0, 1, 'L');
                    $objPdf->Ln(1);
                }
                $objPdf->Cell(30, 5, 'Fecha fin plan: ', 0, 0, 'L', 1);
                $objPdf->Cell(18, 5, $arrTotalesPlan['fecha_fin_plan'], 0, 0, 'L');
                $objPdf->Cell(20, 5, utf8_decode('Días afecta:'), 0, 0, 'R', 1);
                $objPdf->Cell(10, 5, $arrTotalesPlan['cant_dias_afecta'], 0, 0, 'L');
                $objPdf->Cell(20, 5, utf8_decode('Días no afecta:'), 0, 0, 'R', 1);
                $objPdf->Cell(10, 5, $arrTotalesPlan['cant_dias_no_afecta'], 0, 0, 'L');
                $objPdf->Cell(15, 5, utf8_decode('Prórroga:'), 0, 0, 'R', 1);
                $objPdf->Cell(10, 5, $arrTotalesPlan['cant_dias_prorroga'], 0, 0, 'L');
                $objPdf->Cell(22, 5, utf8_decode('Duración Total :'), 0, 0, 'R', 1);
                $objPdf->Cell(10, 5, $arrTotalesPlan['totalDias_Plan'], 0, 1, 'L');
                $objPdf->Ln(5);

                # IMPRIME LAS PRÓRROGAS
                $arrProrrogas = $this->atRptProrrModelo->metBuscaProrrogas($filapln['pk_num_planificacion']);//Se extraen las prórrogas
                if (COUNT($arrProrrogas) > 0) {
                    $totalDias=0;
                    //Título de la sección de prórroga
                    $objPdf->SetDrawColor(0, 0, 0);
                    $objPdf->SetFillColor(255, 255, 255);
                    $objPdf->SetFont('Arial', 'BU', 9);
                    $objPdf->Cell(30, 5, utf8_decode('PRÓRROGAS.'), 0, 0, 'L', 1);
                    $objPdf->Ln(5);
                    //Encabezado de la tabla
                    $objPdf->SetFillColor(245, 245, 245);
                    $objPdf->SetFont('Arial', 'B', 8);
                    $objPdf->SetWidths(array(69, 11, 90, 8, 22));
                    $objPdf->SetAligns(array('C', 'C', 'C', 'C', 'C'));
                    $objPdf->Row(array('Actividad', 'Lapso',utf8_decode('Motivo prórroga'),utf8_decode('Días'), 'Estado'));
                    //Cuerpo de la tabla
                    $objPdf->SetDrawColor(0, 0, 0);
                    $objPdf->SetFillColor(255, 255, 255);
                    $objPdf->SetFont('Arial', '', 8);
                    $objPdf->SetAligns(array('J', 'C', 'J', 'C', 'C'));
                    foreach ($arrProrrogas AS $row) {
                        if($row['estado_prorroga']!="AN"){
                            $totalDias=$totalDias+$row['cant_diasprorroga'];
                        }
                        $objPdf->Row(array(
                            utf8_decode($row['desc_actividad']),
                            $row['lapso_actividad'],
                            utf8_decode($row['motivo_prorroga']),
                            $row['cant_diasprorroga'],
                            utf8_decode($this->atFuncGnrles->metEstadoProrroga($row['estado_prorroga'])))
                        );
                    }
                    //Fila del total de días planficación
                    $objPdf->SetFont('Arial', 'B', 8);
                    $objPdf->SetWidths(array(170, 8, 22));
                    $objPdf->SetAligns(array('L', 'C', 'C'));
                    $objPdf->Row(array(utf8_decode('Total días de prórrogas efectivas'),$totalDias,''));
                }
            }
            #salida del pdf
            $objPdf->Output($nombre_pdf, 'I');
        }
    }
}
?>