<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Planificación Fiscal
 * PROCESO: Carga de data básica
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Keisy Lòpez                      |      lopez.keisy@cmldc.gob.ve      |                                |
 * | 2 |          José Díaz                        |                                    |                                |
 * | 3 |          Alexis Ontiveros                 |    ontiveros.alexis@cmldc.gob.ve   |         0426-514.43.82         |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        02-05-2016       |         1.0        |
 * |               #3                      |        05-07-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once 'datosCarga/dataPlanificacionFiscal.php';
class scriptCargarPfControlador extends Controlador
{
    use dataPlanificacionFiscal;
    private $atScriptCargaPf;

    public function __construct()
    {
        parent::__construct();
        $this->atScriptCargaPf = $this->metCargarModelo('scriptCargaPf');

    }
    public function metIndex()
    {
        echo "INICIANDO CARGA<br>";
        // Procesos Fiscales
        $this->metProcesosFiscales();
        // Fases de Procesos fiscales
        $this->metFasesProceso();
        // Actividades de fases
        $this->metActividadesFase();
        //Tipos de sanciones o fallos de Procedimientos administrativos
        $this->metTipoDecisiones();
        ### categorías de entes
        $this->metDataCategEnte();
        echo "TERMINADO";
    }
    public function metProcesosFiscales ()
    {
        //Carga los procesos fiscales
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE PROCESOS FISCALES<br>';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>PROCESOS FISCALES<br>';
        $this->atScriptCargaPf->metCargarProcesosFiscales($this->metDataProcesosFiscales());
        echo '&nbsp;&nbsp;&nbsp;->>FIN CARGA DATA BASICA<br><br>';
    }
    public function metFasesProceso ()
    {
        //Cargar las fases
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE FASES<br>';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>FASES<br>';
        $this->atScriptCargaPf->metCargarFasesProceso($this->metDataFasesProceso());
        echo '&nbsp;&nbsp;&nbsp;->>FIN CARGA DATA BASICA<br><br>';
    }
    public function metActividadesFase()
    {
        #Cargar las ACTIVIDADES
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE ACTIVIDADES<br>';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>ACTIVIDADES<br>';
        $this->atScriptCargaPf->metCargarActividadesFase($this->metDataActividadesFase());
        echo '&nbsp;&nbsp;&nbsp;->>FIN CARGA DATA BASICA<br><br>';
    }
    public function metTipoDecisiones ()
    {
        //Carga los tipos de sanciones o fallos de procedimientos administrativos
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE TIPOS DE DECISIÓN<br>';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>TIPO DE DECISIÓN<br>';
        $this->atScriptCargaPf->metCargarTipoDecisiones($this->metDataTipoDecision());
        echo '&nbsp;&nbsp;&nbsp;->>FIN CARGA DATA BASICA<br><br>';
    }

    public function metDataCategEnte ()
    {
        //Carga las categorías de entes
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE CATEGORÍAS DE ENTES<br>';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>CATEGORÍAS DE ENTES<br>';
        $this->atScriptCargaPf->metCategoriasDeEntes($this->metDataCategoriasEnte());
        echo '&nbsp;&nbsp;&nbsp;->>FIN CARGA DATA BASICA<br><br>';
    }
}
