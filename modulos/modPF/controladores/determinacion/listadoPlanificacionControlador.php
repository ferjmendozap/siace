<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Planificación Fiscal
 * Proceso: Determinación de Responsabilidades.
 * Descripción: Lista las planificaciones de Determinación de Responsabilidades para su actualizar, anular o cerrar.
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Alexis Ontiveros     | ontiveros.alexis@cmldc.gob.ve        | 0426-514.43.82
 * VERSION
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        20-02-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *****************************************************************************************/
require_once RUTA_Modulo . 'modPF' . DS . 'modelos' . DS . 'funcionesGenerales.php';
class listadoPlanificacionControlador extends Controlador{
    private $atDetRespModelo;        //Para el modelo de determinacion.
    private $atFuncGnrles;          //Para las Funciones generales.
    private $atIdContraloria;       //Para el Id de la contraloría que ejecuta.
    private $atIdDependenciaUser;   //Para el Id de la dependencia al cual pertenece el usuario.
    private $atIdEmpleadoUser;      //Para el Id empleado del usuario que ejecuta.
    private $atTipoUser;            //Para el tipo de usuario logueado.
    private $atUserValido;          //Contendrá la validéz del usuario.
    private $atIdTipoProceso;       //Para el Id del Proceso (Determinación de Responsabilidades).
    public function __construct(){
        parent::__construct();
        $this->atDetRespModelo = $this->metCargarModelo('determinacion','determinacion/planificacion');
        $this->atFuncGnrles = new funcionesGenerales();
        /*Se busca y asigna el id del tipo de proceso (Determinación de Responsabilidad)*/
        $arr_tipoproc = $this->atFuncGnrles->metTipoProcesoFiscal(null,'05');
        $this->atIdTipoProceso = $arr_tipoproc[0]['pk_num_proceso']; $arr_tipoproc = null;
        //Se determina la validéz y el tipo de usuario.
        $arrUser=$this->atFuncGnrles->metValidaUsuario();
        if($arrUser['userValido']){//Entra, si es un Usuario Válido.
            $this->atTipoUser=$arrUser['tipoUser'];
            $this->atUserValido=$arrUser['userValido'];
            $this->atIdContraloria=$arrUser['pk_num_organismo'];
            $this->atIdEmpleadoUser=$arrUser['pk_num_empleado'];
            $this->atIdDependenciaUser=$arrUser['pk_num_dependencia'];
        }else{//De lo contrario, usuario inválido
            $this->atUserValido=$arrUser['userValido'];
        }
        $arrUser=null;
    }
    public function metIndex(){
        if($this->atUserValido) {
            $complementosJs = array(
                'bootstrap-datepicker/bootstrap-datepicker',
            );
            $complementoCss = array(
                'bootstrap-datepicker/datepicker'
            );
            $this->atVista->metCargarCssComplemento($complementoCss);
            $this->atVista->metCargarJsComplemento($complementosJs);
            //Lista los años de acuerdo al año mas bajo de actuaciones.
            $arrYears=$this->atFuncGnrles->metComboBoxYears();
            $this->atVista->assign('listayears', $arrYears);
            $year_actual=date('Y');
            $this->atVista->assign('year_actual', $year_actual);
            /*Lista la(s) dependencia(s) internas de control según usuario*/
            if($this->atTipoUser=='userCtrolFcal'){//Usuario de Control fiscal
                $listaDepContraloria[0]=$this->atFuncGnrles->metBuscaDepControlUsuario($this->atIdDependenciaUser);
            }elseif($this->atTipoUser=='userInvitado'){//Usuario invitado
                $listaDepContraloria = $this->atFuncGnrles->metBuscaDepControlInvitado($this->atIdContraloria,$this->atIdTipoProceso);
            }
            $this->atVista->assign('listaDependencias', $listaDepContraloria);
            /*Lista los entes externos*/
            $entesExternos = $this->metBuscarEntes(0);
            $this->atVista->assign('entesExternos', $entesExternos["filas"]);
            /*Lista los tipos de estados de Determinación de Responsabilidades*/
            $listaEstadosplan = $this->atFuncGnrles->metListaEstadosPlanific('EstadosDRA');
            $this->atVista->assign('listaEstadosplan', $listaEstadosplan);
            $this->atVista->metRenderizar('listadoPlanificacion');
        }else{
            $this->atVista->assign('valido', $this->atUserValido);
            $this->atVista->metRenderizar('../../error');
        }
    }
    public function metComplementosForm(){
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'modPF/funcionesModPf';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
    }

    /**
     * Se busca las dependencias de control fiscal correspondientes de la contraloría ejecutante
     * @param null $iddep_padre
     * @return array
     */
    public function metListaDependenciaInternaCf($iddep_padre=NULL){
        $retornar=array("filas"=>false);
        $flag_controlfiscal="";
        if(!$iddep_padre){
            $flag_controlfiscal=1;
        }
        $result=$this->atFuncGnrles->metCargaCboxRecursivo($this->atIdContraloria,$iddep_padre,$flag_controlfiscal);
        if(COUNT($result)>0) {
            foreach ($result as $fila) {
                $data[] = array("id_dependencia"=>$fila["id_dep"],"iddep_padre"=>$fila["iddep_padre"],"nombre_dependencia"=>$fila["nombre_dep"],"toltick"=>$fila["toltick"]);
            }
            $retornar=array("filas"=>$data);
        }
        return $retornar;
    }

    /**
     * Se busca entes externos
     * @param $iddep_padre
     * @return array
     */
    public function metBuscarEntes($iddep_padre){
        $retornar=array("filas"=>false);
        $result=$this->atFuncGnrles->metCargaCboxRecursivoEnte($iddep_padre,'');
        if(COUNT($result)>0) {
            $retornar=array("filas"=>$result);
        }
        return $retornar;
    }
    /**
     * Lista las dependencias de un ente externo al ser seleccionado
     */
    public function metListaEntes(){
        $idEnte = $this->metObtenerInt('idEnte');
        $listaEntes = $this->metBuscarEntes($idEnte);
        echo json_encode($listaEntes);
        exit;
    }
    /**ListarActuaciones
     * Se buscan las planificaciones para listarlas en la grilla de formulario ppal.
     */
    public function metListarPlanificaciones(){
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $Exceccion_int=array('cbox_dependencia','cbox_centro_costo','cbox_ente','cbox_estadoplan');
        $arrInt=$this->metValidarFormArrayDatos('form', 'int',$Exceccion_int);
        $Exceccion_txt=array('txt_objetivo_plan','cbox_estadoplan','txt_fechareg1','txt_fechareg2','cod_planificacion');
        $arrTxt=$this->metValidarFormArrayDatos('form', 'txt',$Exceccion_txt);
        $arrParams=array_merge($arrInt,$arrTxt);
        $arrParams["idTipoProceso"]=$this->atIdTipoProceso;

        if($arrParams["txt_fechareg1"]){
            $arrParams["txt_fechareg1"]=$this->atFuncGnrles->metFormateaFechaMsql($arrParams['txt_fechareg1']);
        }
        if($arrParams["txt_fechareg2"]){
            $arrParams["txt_fechareg2"]=$this->atFuncGnrles->metFormateaFechaMsql($arrParams['txt_fechareg2']);
        }
        //Se extrae las planificaciones de acuerdo al tipo de usuario
        if($this->atTipoUser=='userCtrolFcal'){
            $result=$this->atDetRespModelo->metBuscarPlanificaciones($arrParams);
        }elseif($this->atTipoUser=='userInvitado'){
            if(!$arrParams["cbox_dependencia"]){//Extrae todas las planificaciones de acuerdo a las dependencias que tiene acceso el usuario invitado
                $arrData=array();
                //Se busca las dependencias de control al cual el usuario está como invitado
                $listaDepInvitado = $this->atFuncGnrles->metBuscaDepControlInvitado($this->atIdContraloria,$this->atIdTipoProceso);
                foreach($listaDepInvitado as $fila){//se extrae los registros de acuerdo a las dependencias de control en la cuales está invitado
                    $arrParams["cbox_dependencia"]=$fila["id_dependencia"];
                    $result=$this->atDetRespModelo->metBuscarPlanificaciones($arrParams);
                    if(COUNT($result)>0){
                        $arrData=array_merge($arrData,$result);
                    }
                }
                $result=$arrData;
            }else{//Extrae sólo las planificaciones de la dependencia seleccionada por el usuario invitado
                $result=$this->atDetRespModelo->metBuscarPlanificaciones($arrParams);
            }
        }
        $data=array();
        if(COUNT($result)>0){//Se prepara los resultados.
            foreach ($result as $fila) {
                $fila['ind_estado']=$fila['ind_estado_planificacion'];unset($fila['ind_estado_planificacion']);
                $arrEnte=$this->atFuncGnrles->metBuscaRecursivoEntes($fila['fk_a039_num_ente']);
                $fila['nombre_ente']=$arrEnte['cadEntes'];
                $fila['desc_estado']=$this->atFuncGnrles->metEstadoPlanificacion($fila['ind_estado']);
                $fila['fec_inicio']=$this->atFuncGnrles->metFormateaFecha($fila['fec_inicio']);
                $fila['fec_registro']=$this->atFuncGnrles->metFormateaFecha($fila['fec_registro_planificacion']);unset($fila['fec_registro_planificacion']);
                $fila['fec_ultima_modificacion']=$this->atFuncGnrles->metFormateaFecha($fila['fec_ultima_modific_planificacion']);unset($fila['fec_ultima_modific_planificacion']);
                $arrFechasfin=$this->atFuncGnrles->metBuscaFechaFinPlan($fila['pk_num_planificacion']);
                $data[]=$fila=array_merge($fila,$arrFechasfin);
            }
            $this->atVista->assign('listado', $data);
            $this->atVista->metRenderizar('resultadoListado');
        }else{
            return false;
        }
    }
    /**
     * Lista las dependencias de centro de costos al seleccionar un dependencia de control de la grilla ppal.
     */
    public function metDependenciaCentroCostos(){
        $idDependencia = $this->metObtenerInt('idDependencia');
        $campos="pk_num_centro_costo,ind_descripcion_centro_costo";
        $listaCentroCosto=$this->atFuncGnrles->metBuscaDepCentroCostos($campos,'',$idDependencia,'');
        echo json_encode(array("filas"=>$listaCentroCosto));
        exit;
    }
    /**
     * Se busca las dependencias para la selección de auditores
     */
    public function metListaDependenciaAuditor($iddep_padre){
        $retornar=array("filas"=>false); $data=array();
        $result=$this->atFuncGnrles->metCargaCboxRecursivo($this->atIdContraloria,$iddep_padre);
        if(COUNT($result)>0) {
            foreach ($result as $fila) {
                $data[] = array("id_dep_auditor"=>$fila["id_dep"], "iddep_padre"=>$fila["iddep_padre"], "nombre_dep_auditor"=>$fila["nombre_dep"], "toltick"=>$fila["toltick"]);
            }
            $retornar=array("filas"=>$data);
        }
        return $retornar;
    }
    /**
     * Lista las dependencia internas de forma recursiva para llenar el combo Auditores
     */
    public function metListaDepAuditor(){
        $idDepAuditor = $this->metObtenerInt('idDepAuditor');
        $listaDepAuditor=$this->metListaDependenciaAuditor($idDepAuditor);
        echo json_encode($listaDepAuditor);
    }
    /**
     * Se buscan los auditores disponibles para la planificación de acuerdo a la dependencia seleccionada.
     * @param $id_dependencia
     * @param $idPlanificacion
     * @return array
     */
    public function metListaAuditores($id_dependencia,$idPlanificacion){
        $retornar=array("filas"=>false); $asignaCoordinador=false;
        if($idPlanificacion){
            /*Se verifica si hay un auditor coordinador en la actuación actual*/
            $params=array("idPlanificacion"=>$idPlanificacion,"flag_coordinador"=>1);
            $resultCoord=$this->atDetRespModelo->metAuditoresDesignados($params);
            if(COUNT($resultCoord)==0){
                $asignaCoordinador=true;/*Se puede asignar el coordinador*/
            }
        }
        $camposExtraer="pk_num_empleado,ind_cedula_documento AS cedula_auditor,CONCAT_WS(' ',ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2) AS nombre_auditor,ind_nombre_cargo";
        $result=$this->atFuncGnrles->metBuscaEmpleadosDependencia($camposExtraer,$this->atIdContraloria,$id_dependencia);
        if(COUNT($result)>0) {
            foreach ($result as $fila) {
                $params=array("idPlanificacion"=>$idPlanificacion,"pk_num_empleado"=>$fila['pk_num_empleado']);
                $rcrset=$this->atDetRespModelo->metAuditoresDesignados($params); //Verifica si está asignado a la planificación
                if(COUNT($rcrset)==0) {//Si no está asignado se agregan los disonibles.
                    $data[] = array("pk_num_empleado"=>$fila['pk_num_empleado'],
                        "cedula_auditor"=>number_format($fila['cedula_auditor'],0,",","."),
                        "nombre_auditor"=>$fila['nombre_auditor'],
                        "cargo_auditor"=>$fila['ind_nombre_cargo'],
                        "asignaCoordinador"=>$asignaCoordinador);
                }
            }
            $retornar=array("filas"=>$data);
        }
        return $retornar;
    }
    /**
     * Lista los empleados para llenar grilla Auditores
     */
    public function metLlenaGrillaAuditor(){
        $idDepAuditor = $this->metObtenerInt('idDepAuditor');
        $idPlanificacion = $this->metObtenerInt('idPlanificacion');
        $listaAuditor=$this->metListaAuditores($idDepAuditor,$idPlanificacion);
        echo json_encode($listaAuditor);
    }
    /**
     * Monta la planificación en el form modal al hacer click en el botón respectivo de la lista ppal. de planificaciones a revisar.
     */
    public function metMontaPlanificacion(){
        $idPlanificacion = $this->metObtenerInt('idPlanificacion');
        $result = $this->atDetRespModelo->metBuscarPlanificaciones(array('idPlanificacion'=>$idPlanificacion, 'idTipoProceso'=>$this->atIdTipoProceso));
        if (COUNT($result)) {
            $arrData = $result[0];
            $this->metComplementosForm();
            if($arrData['ind_estado_planificacion']=='PR'){
                $apcion_modal='Actualizar';
            }else{
                $apcion_modal='Ver';
            }
            $this->atVista->assign('apcion_modal', $apcion_modal);
            if($arrData['fk_a004_num_dependencia_solctte']>0){//Para tratar los datos de una planificación de del tipo acción fiscal
                $this->atVista->assign('pAcionFiscal', true);
                $arrDepSolctte = $this->metListaDependenciaAuditor(0);
                foreach($arrDepSolctte["filas"] as $fila){//Se extrae el nombre de la dependencia solicitante
                    if($fila["id_dep_auditor"]==$arrData['fk_a004_num_dependencia_solctte']){
                        $arrData["dependencia_solctte"]=$fila["nombre_dep_auditor"];break;
                    }
                }
                $arrDepSolctte=null;
                if($arrData['idplanificacion_referencia']){//Se extraen el tipo de proceso fiscal y número de control que dió origen a la solictud de la acción fiscal
                    $ArrResult = $this->atFuncGnrles->metBuscaCodPlanificacion(array('idPlanificacion' => $arrData['idplanificacion_referencia']));
                    $arrData["codPlnfcnSlctte"] = $ArrResult[0]["codPlanificacion"];
                    $ArrProcsFiscalDep = $this->atFuncGnrles->metBuscaProcesosFiscal($arrData['fk_a004_num_dependencia_solctte']);
                    foreach ($ArrProcsFiscalDep as $fila) {
                        if ($fila["idProceso"] == $ArrResult[0]["idTipoProcFiscal"]) {
                            $arrData["descProceso"] = $fila["descProceso"];
                            break;
                        }
                    }
                    $ArrProcsFiscalDep = null;$ArrResult = null;
                }
            }else{
                $this->atVista->assign('pAcionFiscal', false);
            }
            /*Lista la contraloría que ejecuta*/
            $campos="pk_num_organismo AS id_contraloria,ind_descripcion_empresa AS nombre_contraloria";
            $listaContraloria=$this->atFuncGnrles->metBuscaOrganismos($campos,$this->atIdContraloria,1,'I');
            $this->atVista->assign('listaContraloria', $listaContraloria);
            /*Se lista las dependencias de control de acuerdo al tipo de usuario*/
            if($this->atTipoUser=='userCtrolFcal'){
                $listaDepContraloria[0]=$this->atFuncGnrles->metBuscaDepControlUsuario($this->atIdDependenciaUser);
            }elseif($this->atTipoUser=='userInvitado'){
                $listaDepContraloria = $this->atFuncGnrles->metBuscaDepControlInvitado($this->atIdContraloria,$this->atIdTipoProceso);
            }
            $this->atVista->assign('listaDepContraloria', $listaDepContraloria);
            /*Lista de centros de costo*/
            $campos="pk_num_centro_costo,ind_descripcion_centro_costo";
            $listaCentroCosto=$this->atFuncGnrles->metBuscaDepCentroCostos($campos,'',$arrData['fk_a004_num_dependencia'],'');
            if (COUNT($listaCentroCosto) > 0) {
                $this->atVista->assign('listaCentroCosto', $listaCentroCosto);
            } else {
                $this->atVista->assign('listaCentroCosto', array());
            }
            /*Lista los tipo de actuación fiscal*/
            $campos="pk_num_miscelaneo_detalle AS idtipo_actuacion,ind_nombre_detalle AS nombretipo_actuacion";
            $listaTipoActuacion=$this->atFuncGnrles->metBuscaMiscelanio($campos,'PFTAF','');
            $this->atVista->assign('listaTipoActuacion', $listaTipoActuacion);
            /*Busca el ente externo a objeto de fiscalización*/
            $arrEnte = $this->atFuncGnrles->metBuscaRecursivoEntes($arrData['fk_a039_num_ente']);
            //$arrTitular=$this->atFuncGnrles->metBuscaPersonaEnte('',$arrData['fk_a041_num_persona_ente']);
            $arrData['nombre_ente'] = $arrEnte['cadEntes'];
            /*Lista los origen de actuación*/
            $campos="pk_num_miscelaneo_detalle AS idorigen_actuacion,ind_nombre_detalle AS nombre_origenactuacion";
            $listaOrigenActuacion=$this->atFuncGnrles->metBuscaMiscelanio($campos,'PFODA','');
            $this->atVista->assign('listaOrigenActuacion', $listaOrigenActuacion);
            $arrData['ind_estado'] = $arrData['ind_estado_planificacion']; unset($arrData['ind_estado_planificacion']);
            $arrData['desc_estado'] = $this->atFuncGnrles->metEstadoPlanificacion($arrData['ind_estado']);
            $arrData['fec_inicio'] = $this->atFuncGnrles->metFormateaFecha($arrData['fec_inicio']);
            $this->atVista->assign('idPlanificacion', $idPlanificacion);
            $preparado_por=$this->atFuncGnrles->metBuscaUsuario(null,$arrData['fk_rhb001_num_empleado_preparado']);
            $arrData['preparado_por']=$preparado_por;
            if($arrData['fk_rhb001_num_empleado_revisado']){
                $revisado_por=$this->atFuncGnrles->metBuscaUsuario(null,$arrData['fk_rhb001_num_empleado_revisado']);
                $arrData['revisado_por']=$revisado_por;
            }
            if($arrData['fk_rhb001_num_empleado_aprobado']){
                $aprobado_por=$this->atFuncGnrles->metBuscaUsuario(null,$arrData['fk_rhb001_num_empleado_aprobado']);
                $arrData['aprobado_por']=$aprobado_por;
            }
            $arrData['fec_registro'] = $this->atFuncGnrles->metFormateaFecha($arrData['fec_registro_planificacion']);
            unset($arrData['fec_registro_planificacion']);
            $arrData['fec_ultima_modificacion'] = $this->atFuncGnrles->metFormateaFecha($arrData['fec_ultima_modific_planificacion']);
            unset($arrData['fec_ultima_modific_planificacion']);
            /*Lista de dependencias para seleccionar auditores*/
            $listaDepAuditor = $this->metListaDependenciaAuditor(0);
            $this->atVista->assign('listaDepAuditor', $listaDepAuditor["filas"]);
            //Se extraen los auditores designados
            $dataAuditores=array();
            $result=$this->atDetRespModelo->metAuditoresDesignados(array("idPlanificacion"=>$idPlanificacion));//Se extraen los auditores designados
            if(COUNT($result)>0) {//Se prepara los datos para presentarlos en la vista en la grilla de auditores designados.
                foreach ($result as $fila) {
                    $camposExtraer="ind_cedula_documento AS cedula_auditor,CONCAT_WS(' ',ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2) AS nombre_auditor,ind_nombre_cargo AS cargo_auditor";
                    $arrAuditor=$this->atFuncGnrles->metBuscaEmpledos($camposExtraer,$fila['fk_rhb001_num_empleado']);
                    $dataAuditores[] = array("id_auditorPlanificacion"=>$fila['pk_num_auditor_planificacion'],
                        "pk_num_empleado"=>$fila['fk_rhb001_num_empleado'], "nombre_auditor"=>$arrAuditor[0]['nombre_auditor'],
                        "cargo_auditor" =>$arrAuditor[0]['cargo_auditor'], "flagCordinador"=>$fila['num_flag_coordinador'],
                        "desc_estatus"=>$this->atFuncGnrles->metEstatus($fila['num_estatus']),
                        "fecha_estatus"=>$this->atFuncGnrles->metFormateaFecha($fila['fec_estatus']),
                        "opcion_auditor"=>$this->atFuncGnrles->metOpcionAuditor($arrData['ind_estado'],$fila['num_estatus']));
                }
                $arrData['aditores_asig'] = true;
            }else{
                $arrData['aditores_asig'] = 0;
            }
            $this->atVista->assign('auditoresDesignados', $dataAuditores);
            $this->atVista->assign('habilitaAsignacionResp', $this->atFuncGnrles->metEstadoObjetosAuditor($arrData['ind_estado']));
            //Se verifica si tiene actividades asignadas
            $result=$this->atDetRespModelo->metActividadesAsignadas(array("idPlanificacion"=>$idPlanificacion,"completa"=>"","limite"=>1));
            $boton_cerrarplan=false;
            if(COUNT($result)){
                //Se busca los totales y fecha fin real de la planificación
                $arrTotalesPlan=$this->atFuncGnrles->metBuscaTotalesPlan($idPlanificacion);
                $arrData=array_merge($arrData,$arrTotalesPlan);
                $this->atVista->assign('opcion_actividades', 'actualizar');
                //Se determina la visualización del botón "CERRAR"
                if($arrData["ind_estado"]=="TE"){//Implica mostrar el botón cerrar hasta terminar la última actividad
                    $boton_cerrarplan=true;
                }elseif($arrData["ind_estado"]=="AP"){//Se verifica si hay actividades terminadas
                    $params=array("idPlanificacion"=>$idPlanificacion,"estado_actividad"=>"TE");
                    $campos="ind_estado_actividad";
                    $arrActividad=$this->atFuncGnrles->metObtenerActividad($campos,$params);
                    if(COUNT($arrActividad)>0){
                        $boton_cerrarplan=true;
                    }
                    $arrActividad=null;
                }
            }else{
                $arrTotalesPlan=$arrActuacion=array('fecha_fin_plan'=>"",'cant_dias_afecta'=>0,'cant_dias_no_afecta'=>0,'cant_dias_prorroga'=>0,'totalDias_Plan'=>0);
                $arrData=array_merge($arrData,$arrTotalesPlan);
                $this->atVista->assign('opcion_actividades', 'ingresar');
            }
            $arrData["bton_cerrarplan"]=$boton_cerrarplan;
            $arrData["tipoProcAdmin"] = $this->atFuncGnrles->metValidaTipoProcAdmin();//Verifica si la planificación incluye el indicativo del tipo de procedimiento administrativo
            if($arrData["tipoProcAdmin"]){
                $listaTiposproc=$this->atFuncGnrles->metTiposProcedimientosAdmin();
                $this->atVista->assign('listaTiposproc', $listaTiposproc);
            }
            $this->atVista->assign('planificacion', $arrData);
            $this->atVista->metRenderizar('CrearModificar', 'modales');
        } else {
            echo false;
        }
    }
    /**
     * Actualiza el registro principal de la planificación ó el registro principal de la planificación y las actividades relacionadas
     * cuando la fecha de inicio sea cambiada.
     */
    public function metActualizarPlanificacion(){
        //var_dump($this->metValidarToken());
        $retornar="";
        $Exceccion_int=array('prorroga','aditores_asig');
        $arrInt=$this->metValidarFormArrayDatos('form', 'int',$Exceccion_int);
        $Exceccion_txt=array('ind_observacion','temp_fecha_inicio','hd_notanulacion');
        $arrTxt=$this->metValidarFormArrayDatos('form', 'txt',$Exceccion_txt);
        if($arrTxt!=null && $arrInt==null){
            $dataValida=$arrTxt;
        }elseif($arrTxt==null && $arrInt!=null){
            $dataValida=$arrInt;
        }else{
            $dataValida=array_merge($arrTxt,$arrInt);
        }
        if(in_array('error',$dataValida)){
            $dataValida['Error']='error';
            echo json_encode($dataValida);
            exit;
        }else{
            if(!isset($dataValida['cbox_tipoprocadmin'])){
                $dataValida['cbox_tipoprocadmin']="";
            }
            $fecha_inicio_plan=$dataValida['fec_inicio'];
            $dataValida['fec_inicio']=$this->atFuncGnrles->metFormateaFechaMsql($dataValida['fec_inicio']);
            if($this->atFuncGnrles->metObtenerDiasHabiles ($dataValida['fec_inicio'], $dataValida['fec_inicio'])){//verifica que la fecha de inicio de la planificación sea válida.
                $dataValida['idEmpleadoUser']=$this->atIdEmpleadoUser;
                if (($dataValida['temp_fecha_inicio'] != $fecha_inicio_plan) AND $dataValida['opcion_actividad']=="actualizar") {
                    //Se actualiza las actividades según la nueva fecha de inicio de la planificación
                    $listActividades = $this->atDetRespModelo->metActividadesAsignadas(array("idPlanificacion"=>$dataValida["idPlanificacion"], "completa"=>true, "limite"=>""));
                    if (COUNT($listActividades) > 0) {
                        foreach ($listActividades as $fila) {//Se crea un arreglo con el número de dias y el id de la actividad respectiva.
                            $arrdata[] = $fila['fk_pfb004_num_actividad'] . 'N' . $fila['num_dias_duracion_actividad'];
                        }
                        //Se reformula las actividades restantes.
                        $arrParams = array('fecha_inicio_actuacion'=>$fecha_inicio_plan, 'hdd_dataActividad'=>$arrdata);
                        $arrDatos = $this->atFuncGnrles->metRecalculaActividadesPlan($arrParams);
                        //Se formatean las fecha de las actividades a mysql.
                        $arrDatos = $this->atFuncGnrles->metFechaMsqlActividades($arrDatos);
                        //Se actualizan las actividades de la planificación.
                        $arrParams = "";
                        $arrParams = array('idPlanificacion'=>$dataValida["idPlanificacion"], 'hdd_dataActividad'=>$arrDatos);
                        $arrParams=array_merge($dataValida,$arrParams);
                        if ($this->atDetRespModelo->metActualizaPlanYActividades($arrParams)) {
                            $arrResult["reg_afectado"] = true;
                            $arrResult["mensaje"] = "La actualización se afectuó exitosamente";
                            $retornar = $arrResult;
                        } else {//Si falla, se actualiza el registro ppal. de nuevo con la fecha anterior
                            $arrResult["reg_afectado"] = false;
                            $arrResult["Error"] = "errorSql";
                            $arrResult["mensaje"] = "La actualización no se afectuó. Intente mas tarde";
                            $retornar = $arrResult;
                        }
                    } else {
                        $arrResult["reg_afectado"] = false;
                        $arrResult["Error"] = "errorSql";
                        $arrResult["mensaje"] = "La actualización no se afectuó. Intente mas tarde";
                        $retornar = $arrResult;
                    }
                } else {
                    if ($this->atDetRespModelo->metActualizaPlanificacion($dataValida)) {
                        $arrResult["reg_afectado"] = true;
                        $arrResult["mensaje"] = "La actualización se afectuó exitosamente";
                        $retornar = $arrResult;
                    } else {
                        $arrResult["reg_afectado"] = false;
                        $arrResult["Error"] = "errorSql";
                        $retornar["mensaje"] = "La actualización no se pudo efectuar. Intente de nuevo por favor";
                    }
                }
            }else{
                $retornar=array("idPlanificacion" =>"", "Error"=>"errorSql", "mensaje"=>"Esa fecha de inicio no es válida para la planificación. Intente con otra");
            }
            $retornar["dataError"]=$dataValida;
            echo json_encode($retornar);
        }
    }
    public function metBuscaDesignados($idPlanificacion,$estadoPlanificacion){
        $arrData="";
        $result=$this->atDetRespModelo->metAuditoresDesignados(array("idPlanificacion"=>$idPlanificacion));//Se extraen los auditores designados
        if(COUNT($result)>0) {//Se prepara los datos para presentarlos en la vista en la grilla de auditores designados.
            foreach ($result as $fila) {
                $camposExtraer="ind_cedula_documento AS cedula_auditor,CONCAT_WS(' ',ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2) AS nombre_auditor,ind_nombre_cargo AS cargo_auditor";
                $arrAuditor=$this->atFuncGnrles->metBuscaEmpledos($camposExtraer,$fila['fk_rhb001_num_empleado']);
                if($fila['num_estatus']==1){
                    $fechaEstatus=$this->atFuncGnrles->metFormateaFecha($fila['fec_registro_auditor']);
                }else{
                    $fechaEstatus=$this->atFuncGnrles->metFormateaFecha($fila['fec_estatus']);
                }
                $data[] = array("id_auditorPlanificacion"=>$fila['pk_num_auditor_planificacion'],
                    "pk_num_empleado"=>$fila['fk_rhb001_num_empleado'], "nombre_auditor"=>$arrAuditor[0]['nombre_auditor'],
                    "cargo_auditor" =>$arrAuditor[0]['cargo_auditor'], "flagCordinador"=>$fila['num_flag_coordinador'],
                    "desc_estatus"=>$this->atFuncGnrles->metEstatus($fila['num_estatus']),"fecha_estatus"=>$fechaEstatus,
                    "opcion_auditor"=>$this->atFuncGnrles->metOpcionAuditor($estadoPlanificacion,$fila['num_estatus']));
            }
            $arrData=array("filas"=>$data);

        }
        return $arrData;
    }
    /**
     * Asigna un auditor a la planificación al clicar sobre el botón respectivo.
     */
    public function metAsignaAuditor(){
        $data = "";
        $params["idPlanificacion"]=$this->metObtenerInt('idPlanificacion');
        $params["pk_num_empleado"]=$this->metObtenerInt('pk_num_empleado');
        $params["flagCoord"]=$this->metObtenerInt('flagCoord');
        $estado_planificacion=$this->metObtenerTexto('estado_planificacion');
        $arrResult=$this->atDetRespModelo->metIngresaAuditor($params);//Se ingresa el registro
        if($arrResult['idDesignado']){
            $arrResult["AudDesignados"]=$this->metBuscaDesignados($params["idPlanificacion"],$estado_planificacion);
            $arrResult["mensaje"]="La designación se procesó exitosamente";
        }else{
            $arrResult["mensaje"]="No se pudo asignar la persona a la planificación";
        }
        echo json_encode($arrResult);
    }
    /**
     * Elimina el registro del auditor
     */
    public function metQuitarAuditor(){
        $idPlanificacion=$this->metObtenerInt('idPlanificacion');
        $IdResponsable=$this->metObtenerInt('IdResponsable');
        $estado_planificacion=$this->metObtenerTexto('estado_planificacion');
        $retornar=array("reg_afectado"=>false,"mensaje"=>"El proceso no se efectuó. Intente mas tarde");
        if($this->metObtenerTexto('estado_planificacion')=='AP'){//Inactiva el designado de la planificación
            $result=$this->atDetRespModelo->metEstatusResponsable($IdResponsable,$idPlanificacion,0);
            if($result){
                $retornar=array("reg_afectado"=>$result,"mensaje"=>"El proceso se efectuó exitosamente");
                $retornar["AudDesignados"]=$this->metBuscaDesignados($idPlanificacion,$estado_planificacion);
            }
        }else{//Quita al designado de la planificación
            $result=$this->atDetRespModelo->metQuitarResponsable($IdResponsable,$idPlanificacion);
            if($result){
                $retornar=array("reg_afectado"=>$result,"mensaje"=>"El proceso se efectuó exitosamente");
            }
        }
        echo json_encode($retornar);
    }

    /**
     * Activa un auditor inactivo a la planificación actual
     */
    public function metActivarAuditor(){
        $idPlanificacion=$this->metObtenerInt('idPlanificacion');
        $IdResponsable=$this->metObtenerInt('IdResponsable');
        $estado_planificacion=$this->metObtenerTexto('estado_planificacion');
        $retornar=array("reg_afectado"=>false,"mensaje"=>"El proceso no se efectuó. Intente mas tarde");
        $result=$this->atDetRespModelo->metEstatusResponsable($IdResponsable,$idPlanificacion,1);
        if($result){
            $retornar=array("reg_afectado"=>$result,"mensaje"=>"El proceso se efectuó exitosamente");
            $retornar["AudDesignados"]=$this->metBuscaDesignados($idPlanificacion,$estado_planificacion);
        }
        echo json_encode($retornar);
    }
    /**
     * Selecciona a un responsable como coordinador de la planificación actual
     */
    public function metSeleccionarCoordinado(){
        $idPlanificacion=$this->metObtenerInt('idPlanificacion');
        $IdResponsable=$this->metObtenerInt('IdResponsable');
        $estado_planificacion=$this->metObtenerTexto('estado_planificacion');
        $retornar=array("reg_afectado"=>false,"mensaje"=>"El proceso no se efectuó. Intente mas tarde");
        $result=$this->atDetRespModelo->metSeleccionaCoordinador($IdResponsable,$idPlanificacion);
        if($result){
            $retornar=array("reg_afectado"=>$result,"mensaje"=>"El proceso se efectuó exitosamente");
            $retornar["AudDesignados"]=$this->metBuscaDesignados($idPlanificacion,$estado_planificacion);
        }
        echo json_encode($retornar);
    }
    /**
     * Permite la reprogramación de las actividades.
     */
    public function  metRecalcularActividades(){
        $fecha_inicial = $this->metObtenerTexto('fecha_inicial');
        $set_duracion = $this->metObtenerTexto('set_duracion');
        $arrDiasDuracion=explode('S',$set_duracion);
        foreach($arrDiasDuracion as $cadena){
            $arrDatos=explode(',',$cadena); $dias_duracion=$arrDatos[0]; $indiceFila=$arrDatos[1];
            $duracion_actividad=$dias_duracion;
            $fecha_inicial=date_format(date_create($fecha_inicial),'Y-m-d');
            $fecha_fin_actividad=$this->atFuncGnrles->metSumarDiasHabiles($fecha_inicial, (int) $duracion_actividad);
            $fila=array('dias_duracion'=>$dias_duracion,'fecha_inicial'=>$fecha_inicial,'fecha_fin_actividad'=>$fecha_fin_actividad,'indiceFila'=>$indiceFila);
            $duracion_actividad++;
            $fecha_inicial=$this->atFuncGnrles->metSumarDiasHabiles($fecha_inicial, (int) $duracion_actividad);
            $fila['fecha_inicial']=date_format(date_create($fila['fecha_inicial']),'d-m-Y');
            $fila['fecha_fin_actividad']=date_format(date_create($fila['fecha_fin_actividad']),'d-m-Y');
            $arrData[]=$fila;
        }
        echo json_encode(array("filas"=>$arrData));
    }
    /***
     * Agrega actividades a la planificación ó las actualiza .
     */
    public function metGuardaActividades(){
        $arrInt=$this->metValidarFormArrayDatos('form', 'int');
        $arrInt["idPlanificacion"]=$arrInt["idPlanificacion_act"];unset($arrInt["idPlanificacion_act"]);
        $dataDataActividades = $this->metValidarFormArrayDatos('form', 'txt');
        //Se convierte las fecha de las actividades a mysql
        $arrActividades=$dataDataActividades["hdd_dataActividad"];
        $arrActividades=$this->atFuncGnrles->metFechaMsqlActividades($arrActividades);
        $dataDataActividades["hdd_dataActividad"]=$arrActividades;
        $params=array_merge($arrInt,$dataDataActividades);
        if($dataDataActividades['opcion_actividades']=="ingresar"){
            $arrResult=$this->atDetRespModelo->metIngresaActividades($params);
            if($arrResult["result"]){
                $arrResult["Error"]="";
                $arrResult["opcion_actividades"]="actualizar";
                $arrResult["mensaje"]="Las actividades fueron ingresadas a la planificación exitosamente";
            }else{
                $arrResult["Error"]="errorSql";
                $arrResult["mensaje"]="No se pudo asignar las actividades. Intente de nuevo por favor";
            }
        }else{
            if($this->atDetRespModelo->metActualizaActividades($params)){
                $arrResult["result"]=true;
                $arrResult["Error"]="";
                $arrResult["opcion_actividades"]="actualizar";
                $arrResult["mensaje"]="El proceso se ejecutó exitosamente";
            }else{
                $arrResult["Error"]="errorSql";
                $arrResult["mensaje"]="La actualización de no se efectuó. Intente de nuevo por favor";
            }
        }
        echo json_encode($arrResult);
    }
    /**
     * Lista las actividades para la planificación.
     */
    public function metListaActividades($params){
        $retornar=array();
        if($params["opcion_actividades"]=="actualizar"){/*Si entra, busca las actividades asignadas a la planificación*/
            $result=$this->atDetRespModelo->metActividadesAsignadas(array("idPlanificacion"=>$params["idPlanificacion"],"completa"=>true,"limite"=>""));
            if(COUNT($result)>0) {
                foreach ($result as $fila) {
                    $fila['fec_inicio_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_inicio_actividad']);
                    $fila['fec_inicio_real_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_inicio_real_actividad']);
                    $fila['fec_culmina_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_culmina_actividad']);
                    $fila['fec_culmina_real_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_culmina_real_actividad']);
                    $arrData[]=$fila;
                }
            }
            $arrData=$this->atFuncGnrles->metAgrupaArray($arrData,'txt_descripcion_fase','cod_fase');
            $retornar=$arrData;
        }else{/*Se buscan las actividades para asignar a la actuación*/
            $fecha_inicio=$this->atFuncGnrles->metFormateaFechaMsql($params['fec_inicio']);
            $result=$this->atDetRespModelo->metListaActividadesDisp(array("idTipoProceso"=>$params["idTipoProceso"],"idTipoActuacion"=>$params["idTipoActuacion"]));
            if(COUNT($result)>0) {
                //Se define la fecha de inicio y fin de cada actividad de acuerdo a la fecha de inicio de la planificación.
                foreach ($result as $fila) {
                    $fila['pk_num_actividad_planific'] = 0;
                    $duracion_actividad=$fila['num_duracion_actividad'];
                    $fila['fec_inicio_actividad'] = $fecha_inicio;
                    $fecha_fin_actividad=$this->atFuncGnrles->metSumarDiasHabiles($fecha_inicio, (int) $duracion_actividad);
                    $fila['fec_inicio_real_actividad'] = $fecha_inicio;
                    $duracion_actividad++;
                    $fecha_inicio=$this->atFuncGnrles->metSumarDiasHabiles($fecha_inicio, (int) $duracion_actividad);
                    $fila['num_dias_duracion_actividad'] = $fila['num_duracion_actividad']; unset($fila['num_duracion_actividad']);

                    $fila['fec_inicio_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_inicio_actividad']);
                    $fila['fec_inicio_real_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_inicio_real_actividad']);
                    $fila['fec_culmina_actividad'] = $this->atFuncGnrles->metFormateaFecha($fecha_fin_actividad);
                    $fila['fec_culmina_real_actividad'] = $this->atFuncGnrles->metFormateaFecha($fecha_fin_actividad);
                    $fila['num_dias_prorroga_actividad'] = 0;
                    $arrData[]=$fila;
                }
                //Se agrupan las actividades por fase.
                $arrData=$this->atFuncGnrles->metAgrupaArray($arrData,'txt_descripcion_fase','cod_fase');
                $retornar=$arrData;
            }
        }
        return $retornar;
    }
    /**
     * Busca actividades y las visualiza.
     */
    public function metBuscaActividades(){
        $activDisponibles=false;
        $js[] = 'modPF/DataTablePf';
        $this->atVista->metCargarJs($js);
        $idTipoActuacion = $this->metObtenerInt('idtipo_actuacion');
        $feccha_inicio_plan = $this->metObtenerTexto('fec_inicio');
        $opcion_actividades=$this->metObtenerTexto('opcion_actividades');
        $idPlanificacion=$this->metObtenerInt('idPlanificacion');
        $apcion_modal=$this->metObtenerTexto('apcion_modal');
        $params=array("opcion_actividades"=>$opcion_actividades,"idPlanificacion"=>$idPlanificacion,"idTipoProceso"=>$this->atIdTipoProceso,"idTipoActuacion"=>$idTipoActuacion,"fec_inicio"=>$feccha_inicio_plan);
        $arrResult=$this->metListaActividades($params);
        if(COUNT($arrResult)>0){
            if($idPlanificacion){//Se busca el estado de la planificación
                $result=$this->atFuncGnrles->metObtenerDatosPlanificacion($idPlanificacion);
                $this->atVista->assign('estado_planificacion', $result['ind_estado_planificacion']); unset($result);
            }
            if($opcion_actividades=='actualizar'){//Se verifica si hay actividades disponibles para el plan.
                $params=array("idTipoProceso"=>$this->atIdTipoProceso,"idTipoActuacion"=>$idTipoActuacion,);
                $result=$this->atDetRespModelo->metListaActividadesDisp($params);
                if(COUNT($result)>0) {
                    foreach ($result as $fila) {
                        $tbdatos=array(array('tabla'=>'pf_c001_actividad_planific','campo1'=>'fk_pfb001_num_planificacion','valorc1'=>$idPlanificacion,
                            'campo2'=>'fk_pfb004_num_actividad','valorc2'=>$fila['pk_num_actividad']));
                        if(!$this->atFuncGnrles->metVerificaRelacion($tbdatos)){//Se verifica si la actividad no está asignada al plan.
                            $activDisponibles=true;
                            break;
                        }
                    }
                }
            }
            $this->atVista->assign('listaActividades', $arrResult);
            $this->atVista->assign('apcion_modal', $apcion_modal);
            $this->atVista->assign('contador', 0);
            $this->atVista->assign('ifilasubtotal', 0);
            $this->atVista->assign('SubTotal_dias', 0);
            $this->atVista->assign('Total_dias', 0);
            $this->atVista->assign('totalDiasNoAfecta', 0);
            $this->atVista->assign('SubTotal_diasProrroga', 0);
            $this->atVista->assign('Totaldias_prorroga', 0);
            $this->atVista->assign('totalGeneralDias', 0);
            $this->atVista->assign('fecha_fin_plan', '');
            $this->atVista->assign('activDisponibles', $activDisponibles);
            $this->atVista->metRenderizar('grillaActividades');
        }else{
            echo "";
        }
    }
    /**
     * Crea la grilla de actividades disponibles a agregar.
     */
    public function metCreaGrillaActividadesDisp($arrData)
    {
        $tbody = "";
        $contador = 0;
        foreach ($arrData as $fase=>$actividades) {
            $tbody .= '<tr class="info"><td colspan="4" style="width: 100%;">' . $fase . '</td></tr>';
            foreach ($actividades as $fila) {
                if ($fila['ind_afecto_plan'] == 'S') {
                    $no_afecta_plan = 'md md-check';
                } else {
                    $no_afecta_plan = 'md md-not-interested';
                }
                $tbody.='<tr id="tr_actdisp'.$fila['pk_num_actividad'].'">
                <td id="td_desc'.$fila['pk_num_actividad'].'" style="width: 70%;">'.$fila['txt_descripcion_actividad'].'</td>
                <td id="td_cantdias_'.$fila['pk_num_actividad'].'" style="width: 4%;"><input type="text" class="form-control text-center" id="txt_cantdias'.$fila['pk_num_actividad'].'" name="txt_cantdias[]" size="2" value="'.$fila['num_duracion_actividad'].'"/></td>
                <td id="td_noafecta'.$contador.'" style="width: 10%;" align="center"><i class="'.$no_afecta_plan.'"></i></td>
                <td id="td_agregar'.$contador.' style="width: 10%;" align="center">
                <a id="a_agregar'.$contador.'" href="#" button class="btn btn-xs btn-info" title="Click para Agregar" alt="Click para Agregar" onclick="metAgregarActividad('.$fila["pk_num_actividad"].')">
                    <i class="md md-add" style="color: #ffffff;"></i>
                    </button>
                </a>
                </td>
                </tr>';
                $contador++;
            }
        }
        return $tbody;
    }
    /***
     * Busca actividades disponibles para agregar a una planificación
     */
    public function metBuscaActividadesDisp(){
        $arrData=array();
        $idTipoActuacion = $this->metObtenerInt('idtipo_actuacion');
        $idPlanificacion = $this->metObtenerInt('idPlanificacion');
        $params=array("idTipoProceso"=>$this->atIdTipoProceso,"idTipoActuacion"=>$idTipoActuacion);
        $result=$this->atDetRespModelo->metListaActividadesDisp($params);//Se extraen la actividades según el tipo de actuación.
        if(COUNT($result)>0) {
            foreach ($result as $fila) {
                $tbdatos=array(array('tabla'=>'pf_c001_actividad_planific','campo1'=>'fk_pfb001_num_planificacion','valorc1'=>$idPlanificacion,
                    'campo2'=>'fk_pfb004_num_actividad','valorc2'=>$fila['pk_num_actividad']));
                if(!$this->atFuncGnrles->metVerificaRelacion($tbdatos)){//Se verifica si la actividad no está asignada al plan.
                    unset($fila['txt_comentarios_actividad']);unset($fila['num_estatus']);unset($fila['fec_ultima_modificacion']);
                    unset($fila['fk_a018_num_seguridad_usuario']);
                    $arrData[]=$fila;//Se crea el arreglo de actividades disponibles.
                }
            }
            if(COUNT($arrData)>0){
                //Se agrupan las actividades por su respectiva fase.
                $arrResult=$this->atFuncGnrles->metAgrupaArray($arrData,'txt_descripcion_fase','cod_fase');
                $grilla=$this->metCreaGrillaActividadesDisp($arrResult);//Se prepara las actividades para presentarlas en la vista.
                $retornar = array('result'=>true,'grilla'=>$grilla);
            }else{
                $retornar = array('result'=>false,'mensaje'=>'No se encontró actividades disponibles para el Tipo de Actuación de la planificación');
            }
        }else{
            $retornar = array('result'=>false,'mensaje'=>'No se encontró actividades disponibles para el Tipo de Actuación de la planificación');
        }
        echo json_encode($retornar);
    }
    /**
     * Agregar una actividad a la planificación actual.
     */
    function metAgregaActividad(){
        $id_actividad = $this->metObtenerInt('id_actividad');
        $txt_cantdias = $this->metObtenerInt('txt_cantdias');
        $idPlanificacion = $this->metObtenerInt('idPlanificacion');
        $fecha_inicio_plan = $this->metObtenerTexto('fec_inicio');
        //Se prepara los datos para el ingreso de la actividad.
        $cadena[]=$id_actividad.'N'.$txt_cantdias.'N'.date('Y-m-d').'N'.date('Y-m-d').'N'.date('Y-m-d').'N'.date('Y-m-d');
        $arrData=array('idPlanificacion'=>$idPlanificacion,'hdd_dataActividad'=>$cadena);
        $resultIngreso=$this->atDetRespModelo->metIngresaActividades($arrData);//Se ingresa la actividad.
        if($resultIngreso["result"]){//Se extraen las actividades de la planificación.
            $result=$this->atDetRespModelo->metActividadesAsignadas(array("idPlanificacion"=>$idPlanificacion,"completa"=>true,"limite"=>""));
            foreach($result as $fila) {//Se crea un arreglo con el núemro de dias y el id de la actividad respectiva.
                $arrdata[]=$fila['fk_pfb004_num_actividad'].'N'.$fila['num_dias_duracion_actividad'];
            }
            //Se reformula las actividades restantes.
            $arrParams=array('fecha_inicio_actuacion'=>$fecha_inicio_plan,'hdd_dataActividad'=>$arrdata);
            $arrDatos=$this->atFuncGnrles->metRecalculaActividadesPlan($arrParams);
            //Se formatean las fecha de las actividades a mysql.
            $arrDatos=$this->atFuncGnrles->metFechaMsqlActividades($arrDatos);
            //Se actualizan las actividades de la planificación.
            $arrParams="";
            $arrParams=array('idPlanificacion'=>$idPlanificacion,'hdd_dataActividad'=>$arrDatos);
            if($this->atDetRespModelo->metActualizaActividades($arrParams)){
                $resultIngreso["mensaje"]="El proceso se efectuó exitosamente";
            }else{
                $resultIngreso["result"]=false;
                $this->atDetRespModelo->metQuitaActividad(array('idPlanificacion'=>$idPlanificacion,'idActividad'=>$id_actividad));
                $resultIngreso["mensaje"]="La operación falló. Intente de nuevo por favor";
            }
        }else{
            $resultIngreso["mensaje"]="La operación falló. Intente de nuevo por favor";
        }
        echo json_encode($resultIngreso);
    }
    /**
     * Remueve una actividad de la planificación.
     */
    public function metQuitarActividad(){
        $params=$this->metValidarFormArrayDatos('form', 'int');
        $arrText = $this->metValidarFormArrayDatos('form', 'txt');
        if($this->atDetRespModelo->metQuitaActividad($params)){//Se elimina la actividad.
            $retornar["result"]=true;
            $retornar["buscar_actividades_disp"]=false;
            $retornar["mensaje"]="El proceso se ejecutó exitosamente";
            //Se buscan las actividades restantes.
            $result=$this->atDetRespModelo->metActividadesAsignadas(array("idPlanificacion"=>$params["idPlanificacion_act"],"completa"=>true,"limite"=>""));
            if(COUNT($result)>0){
                foreach($result as $fila) {//Se crea un arreglo con el número de dias y el id de la actividad respectiva.
                    $arrdata[]=$fila['fk_pfb004_num_actividad'].'N'.$fila['num_dias_duracion_actividad'];
                }
                //Se reformula las actividades restantes.
                $arrParams=array('fecha_inicio_actuacion'=>$arrText['fecha_inicio_actuacion'],'hdd_dataActividad'=>$arrdata);
                $arrDatos=$this->atFuncGnrles->metRecalculaActividadesPlan($arrParams);
                //Se formatean las fecha de las actividades a mysql.
                $arrDatos=$this->atFuncGnrles->metFechaMsqlActividades($arrDatos);
                //Se actualizan las actividades restantes.
                $arrParams=array('idPlanificacion'=>$params["idPlanificacion_act"],'hdd_dataActividad'=>$arrDatos);
                $respActualiza=$this->atDetRespModelo->metActualizaActividades($arrParams);
            }else{
                $retornar["result"]=true;
                $retornar["mensaje"]="El proceso se ejecutó exitosamente. la planificasión ya no contiene actividades asignadas";
                $retornar["buscar_actividades_disp"]=true;
            }
        }else{
            $retornar["mensaje"]="El proceso falló. Intente mas tarde";
            $retornar["result"]=false;
        }
        echo json_encode($retornar);
    }
    /**
     * Anula la planificación actual.
     */
    public function metAnularPlanificacion(){
        $retornar="";
        $Exceccion_int=array('idPlanificacion','prorroga','aditores_asig');
        $arrInt=$this->metValidarFormArrayDatos('form', 'int',$Exceccion_int);
        $Exceccion_txt=array('ind_observacion','temp_fecha_inicio');
        $arrTxt=$this->metValidarFormArrayDatos('form', 'txt',$Exceccion_txt);
        $params=array_merge($arrTxt,$arrInt);
        $params['idEmpleadoUser'] = $this->atIdEmpleadoUser;
        $result=$this->atDetRespModelo->metAnularPlanificacion($params);
        if($result){
            $retornar = array("reg_afectado"=>true, "mensaje"=>"La planificación actual se anuló exitosamente");
        }else{
            $retornar = array("reg_afectado"=>"", "mensaje"=>"La anulación no se pudo efectuar. Intente mas tarde");
        }
        echo json_encode($retornar);
    }
    /**
     * Cierra la planificación actual.
     */
    public function metCierraPlanificacion(){
        $retornar="";
        $Exceccion_int=array('idPlanificacion','prorroga','aditores_asig');
        $arrInt=$this->metValidarFormArrayDatos('form', 'int',$Exceccion_int);
        $Exceccion_txt=array('ind_observacion','temp_fecha_inicio');
        $arrTxt=$this->metValidarFormArrayDatos('form', 'txt',$Exceccion_txt);
        $params=array_merge($arrTxt,$arrInt);
        $params['idEmpleadoUser'] = $this->atIdEmpleadoUser;
        $result=$this->atDetRespModelo->metCerrarPlanificacion($params);
        if($result){
            $retornar = array("reg_afectado"=>true, "mensaje"=>"La planificación actual se cerró exitosamente");
        }else{
            $retornar = array("reg_afectado"=>"", "mensaje"=>"El cierre de la planificación no se pudo efectuar. Intente mas tarde");
        }
        echo json_encode($retornar);
    }
    /**
     * Busca las decisiones, responsables y multas de las planificaciones completadas.
     * Retorna: Una grilla con los datos respectivos
     */
    public function metBuscaDecisionesPlan(){
        $js[] = 'modPF/DataTable2Pf';
        $this->atVista->metCargarJs($js);
        $idPlanificacion=$this->metObtenerInt('idPlanificacion');
        $this->atVista->assign('listaDecisiones', array());
        $arrDecisiones=$this->atFuncGnrles->metBuscaDesicionesPlan($idPlanificacion);
        if(COUNT($arrDecisiones)>0){
            $arrDecisiones=$this->atFuncGnrles->metAgrupaArray($arrDecisiones, 'nombre_persona','cedula_pers','&&');
            $this->atVista->assign('listaDecisiones', $arrDecisiones);
            $this->atVista->metRenderizar('grillaDecisiones');
        }else{
            echo "";
        }
    }
}
