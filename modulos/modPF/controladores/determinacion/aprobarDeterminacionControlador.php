<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Planificación Fiscal
 * Proceso: Aprobación de Determinación de Responsabilidades.
 * Descripción: Listado, generación del número de control y aprobación ó anulación de planificaciones.
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Alexis Ontiveros     | ontiveros.alexis@cmldc.gob.ve        | 0426-514.43.82
 * VERSION
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        20-02-2016       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *****************************************************************************************/
require_once RUTA_Modulo . 'modPF' . DS . 'modelos' . DS . 'funcionesGenerales.php';
class aprobarDeterminacionControlador extends Controlador{
    private $atAprDetResModelo;     //Para el modelo de aprobarDeterminacion.
    private $atFuncGnrles;          //Para las Funciones generales.
    private $atIdContraloria;       //Para el Id de la contraloría que ejecuta.
    private $atIdDependenciaUser;   //Para el Id de la dependencia al cual pertenece el usuario.
    private $atIdEmpleadoUser;      //Para el Id empleado del usuario que ejecuta.
    private $atTipoUser;            //Para el tipo de usuario logueado.
    private $atUserValido;          //Contendrá la validéz del usuario.
    private $atCodTipoProceso;      //Para el código del tipo del Proceso.
    private $atIdTipoProceso;       //Para el Id del Proceso (Determinación de Responsabilidades).
    public function __construct(){
        parent::__construct();
        $this->atAprDetResModelo = $this->metCargarModelo('aprobarDeterminacion','determinacion/planificacion');
        $this->atFuncGnrles = new funcionesGenerales();
        /*Se busca y asigna el id del tipo de proceso*/
        $arr_tipoproc = $this->atFuncGnrles->metTipoProcesoFiscal(null,'05');
        $this->atIdTipoProceso = $arr_tipoproc[0]['pk_num_proceso'];
        $this->atCodTipoProceso = $arr_tipoproc[0]['cod_proceso'];
        $arr_tipoproc = null;
        //Se determina la validéz y el tipo de usuario.
        $arrUser=$this->atFuncGnrles->metValidaUsuario();
        if($arrUser['userValido']){//Entra, si es un Usuario Válido.
            $this->atTipoUser=$arrUser['tipoUser'];
            $this->atUserValido=$arrUser['userValido'];
            $this->atIdContraloria=$arrUser['pk_num_organismo'];
            $this->atIdEmpleadoUser=$arrUser['pk_num_empleado'];
            $this->atIdDependenciaUser=$arrUser['pk_num_dependencia'];
        }else{//De lo contrario, usuario inválido
            $this->atUserValido=$arrUser['userValido'];
        }
        $arrUser=null;
    }
    public function metIndex(){
        if($this->atUserValido) {
            $complementosJs = array(
                'bootstrap-datepicker/bootstrap-datepicker',
            );
            $complementoCss = array(
                'bootstrap-datepicker/datepicker'
            );
            $this->atVista->metCargarCssComplemento($complementoCss);
            $this->atVista->metCargarJsComplemento($complementosJs);
            //Lista los años de acuerdo al año mas bajo de actuaciones.
            $arrYears=$this->atFuncGnrles->metComboBoxYears(2000);
            $this->atVista->assign('listayears', $arrYears);
            $year_actual=date('Y');
            $this->atVista->assign('year_actual', $year_actual);
            /*Lista la(s) dependencia(s) internas de control según usuario*/
            if($this->atTipoUser=='userCtrolFcal'){//Usuario de Control fiscal
                $listaDepContraloria[0]=$this->atFuncGnrles->metBuscaDepControlUsuario($this->atIdDependenciaUser);
            }elseif($this->atTipoUser=='userInvitado'){//Usuario invitado
                $listaDepContraloria = $this->atFuncGnrles->metBuscaDepControlInvitado($this->atIdContraloria,$this->atIdTipoProceso);
            }
            $this->atVista->assign('listaDependencias', $listaDepContraloria);
            /*Lista los entes externos*/
            $entesExternos = $this->metBuscarEntes(0);
            $this->atVista->assign('entesExternos', $entesExternos["filas"]);
            $this->atVista->assign('total_rcset', 0);
            $this->atVista->metRenderizar('listadoPlanificacion');
        }else{
            $this->atVista->assign('valido', $this->atUserValido);
            $this->atVista->metRenderizar('../../error');
        }
    }
    public function metComplementosForm(){
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'modPF/funcionesModPf';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
    }
    /**
     * Se busca las dependencias de control fiscal correspondientes de la contraloría ejecutante
     */
    public function metListaDependenciaInternaCf($iddep_padre=NULL){
        $retornar=array("filas"=>false);
        $flag_controlfiscal="";
        if(!$iddep_padre){
            $flag_controlfiscal=1;
        }
        $result=$this->atFuncGnrles->metCargaCboxRecursivo($this->atIdContraloria,$iddep_padre,$flag_controlfiscal);
        if(count($result)>0) {
            foreach ($result as $fila) {
                $data[] = array("id_dependencia"=>$fila["id_dep"],"iddep_padre"=>$fila["iddep_padre"],"nombre_dependencia"=>$fila["nombre_dep"],"toltick"=>$fila["toltick"]);
            }
            $retornar=array("filas"=>$data);
        }
        return $retornar;
    }
    /**
     * Se busca entes externos
     */
    public function metBuscarEntes($iddep_padre){
        $retornar=array("filas"=>false);
        $result=$this->atFuncGnrles->metCargaCboxRecursivoEnte($iddep_padre,'');
        if(count($result)>0) {
            $retornar=array("filas"=>$result);
        }
        return $retornar;
    }
    /**
     * Lista las dependencias de un ente externo al ser seleccionado
     */
    public function metListaEntes(){
        $idEnte = $this->metObtenerInt('idEnte');
        $listaEntes = $this->metBuscarEntes($idEnte);
        echo json_encode($listaEntes);
    }
    /**
     * Se buscan las planificaciones para listarlas en la grilla ppal.
     */
    public function metListarPlanificaciones(){
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $Exceccion_int=array('cbox_dependencia','cbox_centro_costo','cbox_ente','cbox_estadoplan');
        $arrInt=$this->metValidarFormArrayDatos('form', 'int',$Exceccion_int);
        $Exceccion_txt=array('txt_objetivo_plan','cbox_estadoplan','txt_fechareg1','txt_fechareg2','cod_planificacion');
        $arrTxt=$this->metValidarFormArrayDatos('form', 'txt',$Exceccion_txt);
        $arrParams=array_merge($arrInt,$arrTxt);
        $arrParams["idTipoProceso"]=$this->atIdTipoProceso;
        if($arrParams["txt_fechareg1"]){
            $arrParams["txt_fechareg1"]=$this->atFuncGnrles->metFormateaFechaMsql($arrParams['txt_fechareg1']);
        }
        if($arrParams["txt_fechareg2"]){
            $arrParams["txt_fechareg2"]=$this->atFuncGnrles->metFormateaFechaMsql($arrParams['txt_fechareg2']);
        }
        //Se extrae las planificaciones de acuerdo al tipo de usuario
        if($this->atTipoUser=='userCtrolFcal'){
            $result=$this->atAprDetResModelo->metBuscarPlanificaciones($arrParams);
        }elseif($this->atTipoUser=='userInvitado'){
            if(!$arrParams["cbox_dependencia"]){//Extrae todas las planificaciones de acuerdo a las dependencias que tiene acceso el usuario invitado
                $arrData=array();
                //Se busca las dependencias de control al cual el usuario está como invitado
                $listaDepInvitado = $this->atFuncGnrles->metBuscaDepControlInvitado($this->atIdContraloria,$this->atIdTipoProceso);
                foreach($listaDepInvitado as $fila){//se extrae los registros de acuerdo a las dependencias de control en la cuales está invitado
                    $arrParams["cbox_dependencia"]=$fila["id_dependencia"];
                    $result=$this->atAprDetResModelo->metBuscarPlanificaciones($arrParams);
                    if(COUNT($result)>0){
                        $arrData=array_merge($arrData,$result);
                    }
                }
                $result=$arrData;
            }else{//Extrae sólo las planificaciones de la dependencia seleccionada por el usuario invitado
                $result=$this->atAprDetResModelo->metBuscarPlanificaciones($arrParams);
            }
        }
        $data=array();
        if(COUNT($result)>0){//Se prepara los resultados.
            foreach ($result as $fila) {
                $fila['ind_estado']=$fila['ind_estado_planificacion'];unset($fila['ind_estado_planificacion']);
                $arrEnte=$this->atFuncGnrles->metBuscaRecursivoEntes($fila['fk_a039_num_ente']);
                $fila['nombre_ente']=$arrEnte['cadEntes'];
                $fila['desc_estado']=$this->atFuncGnrles->metEstadoPlanificacion($fila['ind_estado']);
                $fila['fec_inicio']=$this->atFuncGnrles->metFormateaFecha($fila['fec_inicio']);
                $fila['fec_registro']=$this->atFuncGnrles->metFormateaFecha($fila['fec_registro_planificacion']);unset($fila['fec_registro_planificacion']);
                $fila['fec_ultima_modificacion']=$this->atFuncGnrles->metFormateaFecha($fila['fec_ultima_modific_planificacion']);unset($fila['fec_ultima_modific_planificacion']);
                $arrFechasfin=$this->atFuncGnrles->metBuscaFechaFinPlan($fila['pk_num_planificacion']);
                $data[]=$fila=array_merge($fila,$arrFechasfin);
            }
            $this->atVista->assign('listado', $data);
            $this->atVista->metRenderizar('resultadoListado');
        }else{
            return false;
        }
    }
    /**
     * Lista las dependencias de centro de costos al seleccionar un dependencia de control de la grilla ppal.
     */
    public function metDependenciaCentroCostos(){
        $idDependencia = $this->metObtenerInt('idDependencia');
        $campos="pk_num_centro_costo,ind_descripcion_centro_costo";
        $listaCentroCosto=$this->atFuncGnrles->metBuscaDepCentroCostos($campos,'',$idDependencia,'');
        if(count($listaCentroCosto)>0){
            echo json_encode(array("filas"=>$listaCentroCosto));
        }else{
            echo json_encode(array("filas"=>array()));
        }
    }
    /**
     * Se busca las dependencias para la selección de auditores
     */
    public function metListaDependenciaAuditor($iddep_padre){
        $retornar=array("filas"=>false); $data=array();
        $result=$this->atFuncGnrles->metCargaCboxRecursivo($this->atIdContraloria,$iddep_padre);
        if(count($result)>0) {
            foreach ($result as $fila) {
                $data[] = array("id_dep_auditor"=>$fila["id_dep"], "iddep_padre"=>$fila["iddep_padre"], "nombre_dep_auditor"=>$fila["nombre_dep"], "toltick"=>$fila["toltick"]);
            }
            $retornar=array("filas"=>$data);
        }
        return $retornar;
    }
    /**
     * Monta la planificación al clicar sobre el botón respectivo en la lista de ppal.
     */
    public function metMontaPlanificacion(){
        $idPlanificacion = $this->metObtenerInt('idPlanificacion');
        $result = $this->atAprDetResModelo->metBuscarPlanificaciones(array('idPlanificacion' => $idPlanificacion, 'idTipoProceso' => $this->atIdTipoProceso));
        if (count($result)) {
            $arrData = $result[0];
            $this->metComplementosForm();
            if($arrData['ind_estado_planificacion']=='PR'){
                $apcion_modal='Actualizar';
            }else{
                $apcion_modal='Ver';
            }
            $this->atVista->assign('apcion_modal', $apcion_modal);
            if($arrData['fk_a004_num_dependencia_solctte']>0){//Para tratar los datos de una planificación de del tipo acción fiscal
                $this->atVista->assign('pAcionFiscal', true);
                $arrDepSolctte = $this->metListaDependenciaAuditor(0);
                foreach($arrDepSolctte["filas"] as $fila){//Se extrae el nombre de la dependencia solicitante
                    if($fila["id_dep_auditor"]==$arrData['fk_a004_num_dependencia_solctte']){
                        $arrData["dependencia_solctte"]=$fila["nombre_dep_auditor"];break;
                    }
                }
                $arrDepSolctte=null;
                if($arrData['idplanificacion_referencia']){//Se extraen el tipo de proceso fiscal y número de control que dió origen a la solictud de la acción fiscal
                    $ArrResult = $this->atFuncGnrles->metBuscaCodPlanificacion(array('idPlanificacion' => $arrData['idplanificacion_referencia']));
                    $arrData["codPlnfcnSlctte"] = $ArrResult[0]["codPlanificacion"];
                    $ArrProcsFiscalDep = $this->atFuncGnrles->metBuscaProcesosFiscal($arrData['fk_a004_num_dependencia_solctte']);
                    foreach ($ArrProcsFiscalDep as $fila) {
                        if ($fila["idProceso"] == $ArrResult[0]["idTipoProcFiscal"]) {
                            $arrData["descProceso"] = $fila["descProceso"];
                            break;
                        }
                    }
                    $ArrProcsFiscalDep = null;$ArrResult = null;
                }
            }else{
                $this->atVista->assign('pAcionFiscal', false);
            }
            /*Lista la contraloría que ejecuta*/
            $campos="pk_num_organismo AS id_contraloria,ind_descripcion_empresa AS nombre_contraloria";
            $listaContraloria=$this->atFuncGnrles->metBuscaOrganismos($campos,$this->atIdContraloria,1,'I');
            if (count($listaContraloria) > 0) {
                $this->atVista->assign('listaContraloria', $listaContraloria);
            } else {
                $this->atVista->assign('listaContraloria', array());
            }
            /*Se lista las dependencias de control de acuerdo al tipo de usuario*/
            if($this->atTipoUser=='userCtrolFcal'){
                $listaDepContraloria[0]=$this->atFuncGnrles->metBuscaDepControlUsuario($this->atIdDependenciaUser);
            }elseif($this->atTipoUser=='userInvitado'){
                $listaDepContraloria = $this->atFuncGnrles->metBuscaDepControlInvitado($this->atIdContraloria,$this->atIdTipoProceso);
            }
            $this->atVista->assign('listaDepContraloria', $listaDepContraloria);
            /*Lista de centros de costo*/
            $campos="pk_num_centro_costo,ind_descripcion_centro_costo";
            $listaCentroCosto=$this->atFuncGnrles->metBuscaDepCentroCostos($campos,'',$arrData['fk_a004_num_dependencia'],'');
            if (count($listaCentroCosto) > 0) {
                $this->atVista->assign('listaCentroCosto', $listaCentroCosto);
            } else {
                $this->atVista->assign('listaCentroCosto', array());
            }
            /*Lista los tipo de actuación fiscal*/
            $campos="pk_num_miscelaneo_detalle AS idtipo_actuacion,ind_nombre_detalle AS nombretipo_actuacion";
            $listaTipoActuación = $this->atFuncGnrles->metBuscaMiscelanio($campos,'PFTAF','');
            $this->atVista->assign('listaTipoActuacion', $listaTipoActuación);
            /*Busca el ente externo a objeto de fiscalización*/
            $arrEnte = $this->atFuncGnrles->metBuscaRecursivoEntes($arrData['fk_a039_num_ente']);
            $arrData['nombre_ente'] = $arrEnte['cadEntes'];
            /*Lista los origen de actuación*/
            $campos="pk_num_miscelaneo_detalle AS idorigen_actuacion,ind_nombre_detalle AS nombre_origenactuacion";
            $listaOrigenActuacion = $this->atFuncGnrles->metBuscaMiscelanio($campos,'PFODA','');
            $this->atVista->assign('listaOrigenActuacion', $listaOrigenActuacion);
            $arrData['ind_estado'] = $arrData['ind_estado_planificacion']; unset($arrData['ind_estado_planificacion']);
            $arrData['desc_estado'] = $this->atFuncGnrles->metEstadoPlanificacion($arrData['ind_estado']);
            $arrData['fec_inicio'] = $this->atFuncGnrles->metFormateaFecha($arrData['fec_inicio']);
            $this->atVista->assign('idPlanificacion', $idPlanificacion);
            $preparado_por=$this->atFuncGnrles->metBuscaUsuario(null,$arrData['fk_rhb001_num_empleado_preparado']);
            $arrData['preparado_por']=$preparado_por;
            if($arrData['fk_rhb001_num_empleado_revisado']){
                $revisado_por=$this->atFuncGnrles->metBuscaUsuario(null,$arrData['fk_rhb001_num_empleado_revisado']);
                $arrData['revisado_por']=$revisado_por;
            }
            if($arrData['fk_rhb001_num_empleado_aprobado']){
                $aprobado_por=$this->atFuncGnrles->metBuscaUsuario(null,$arrData['fk_rhb001_num_empleado_aprobado']);
                $arrData['aprobado_por']=$aprobado_por;
            }
            $arrData['fec_registro'] = $this->atFuncGnrles->metFormateaFecha($arrData['fec_registro_planificacion']);
            unset($arrData['fec_registro_planificacion']);
            $arrData['fec_ultima_modificacion'] = $this->atFuncGnrles->metFormateaFecha($arrData['fec_ultima_modific_planificacion']);
            unset($arrData['fec_ultima_modific_planificacion']);
            /*Lista de dependencias para seleccionar auditores*/
            $listaDepAuditor = $this->metListaDependenciaAuditor(0);
            $this->atVista->assign('listaDepAuditor', $listaDepAuditor["filas"]);
            $result=$this->atAprDetResModelo->metAuditoresDesignados(array("idPlanificacion"=>$idPlanificacion));//Se extraen los auditores designados
            if(count($result)>0) {//Se prepara los datos para presentarlos en la vista en la grilla de auditores designados.
                foreach ($result as $fila) {
                    $camposExtraer="ind_cedula_documento AS cedula_auditor,CONCAT_WS(' ',ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2) AS nombre_auditor,ind_nombre_cargo AS cargo_auditor";
                    $arrAuditor=$this->atFuncGnrles->metBuscaEmpledos($camposExtraer,$fila['fk_rhb001_num_empleado']);
                    $dataAuditores[] = array("id_auditorPlanificacion"=>$fila['pk_num_auditor_planificacion'],
                        "pk_num_empleado"=>$fila['fk_rhb001_num_empleado'], "nombre_auditor"=>$arrAuditor[0]['nombre_auditor'],
                        "cargo_auditor" =>$arrAuditor[0]['cargo_auditor'], "flagCordinador"=>$fila['num_flag_coordinador'],
                        "desc_estatus"=>$this->atFuncGnrles->metEstatus($fila['num_estatus']),
                        "fecha_estatus"=>$this->atFuncGnrles->metFormateaFecha($fila['fec_estatus']),
                        "opcion_auditor"=>$this->atFuncGnrles->metOpcionAuditor($arrData['ind_estado'],$fila['num_estatus']));
                }
                $arrData['aditores_asig'] = true;
            }else{
                $arrData['aditores_asig'] = 0; $dataAuditores=array();
            }
            $this->atVista->assign('auditoresDesignados', $dataAuditores);
            $arrTotalesPlan=$this->atFuncGnrles->metBuscaTotalesPlan($idPlanificacion);//Se buscan los totales de la planificación
            $arrData=array_merge($arrData, $arrTotalesPlan);
            //Se determina la visualización del botón "ANULAR"
            $boton_anularplan=false; $campos="ind_estado_actividad";
            $params=array("idPlanificacion"=>$idPlanificacion, "num_secuencia"=>1, "estado_actividad"=>"TE");
            $arrActividad=$this->atFuncGnrles->metObtenerActividad($campos,$params);
            if(COUNT($arrActividad)==0 AND $arrData["ind_estado"]=="AP"){ //Verifica si la primera actividad está terminada
                $boton_anularplan=true;
            }
            $arrData["boton_anularplan"]=$boton_anularplan;
            $arrData["tipoProcAdmin"] = $this->atFuncGnrles->metValidaTipoProcAdmin();//Valida si la planificación incluye el indicativo del tipo de procedimiento administrativo
            if($arrData["tipoProcAdmin"]){
                $listaTiposproc=$this->atFuncGnrles->metTiposProcedimientosAdmin();
                $this->atVista->assign('listaTiposproc', $listaTiposproc);
            }
            $this->atVista->assign('planificacion', $arrData);
            $this->atVista->metRenderizar('aprobarDeterminacion', 'modales');
        } else {
            echo false;
        }
    }
    /**
     * Lista las actividades para una planificación nueva o ya existente.
     */
    public function metListaActividades($idPlanificacion){
        $retornar=array();
        $result=$this->atAprDetResModelo->metActividadesAsignadas(array("idPlanificacion"=>$idPlanificacion,"completa"=>true,"limite"=>""));
        if(count($result)>0) {
            foreach ($result as $fila) {
                $fila['fec_inicio_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_inicio_actividad']);
                $fila['fec_inicio_real_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_inicio_real_actividad']);
                $fila['fec_culmina_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_culmina_actividad']);
                $fila['fec_culmina_real_actividad'] = $this->atFuncGnrles->metFormateaFecha($fila['fec_culmina_real_actividad']);
                $arrData[]=$fila;
            }
            $arrData=$this->atFuncGnrles->metAgrupaArray($arrData,'txt_descripcion_fase','cod_fase');
            $retornar=$arrData;
        }
        return $retornar;
    }
    /**
     * Busca las actividades de la planificación al clicar sobre la ficha actividades.
     */
    public function metBuscaActividades(){
        $js[] = 'modPF/DataTablePf';
        $this->atVista->metCargarJs($js);
        $idPlanificacion=$this->metObtenerInt('idPlanificacion');
        $arrResult=$this->metListaActividades($idPlanificacion);
        if(COUNT($arrResult)>0){
            if($idPlanificacion){//Se busca el estado de la planificación
                $result=$this->atFuncGnrles->metObtenerDatosPlanificacion($idPlanificacion);
                $this->atVista->assign('estado_planificacion', $result['ind_estado_planificacion']); unset($result);
            }
            $this->atVista->assign('listaActividades', $arrResult);
            $this->atVista->assign('contador', 0);
            $this->atVista->assign('SubTotal_dias', 0);
            $this->atVista->assign('Total_dias', 0);
            $this->atVista->assign('totalDiasNoAfecta', 0);
            $this->atVista->assign('SubTotal_diasProrroga', 0);
            $this->atVista->assign('Totaldias_prorroga', 0);
            $this->atVista->assign('totalGeneralDias', 0);
            $this->atVista->assign('fecha_fin_plan', '');
            $this->atVista->metRenderizar('grillaActividades');
        }else{
            echo "";
        }
    }
    /**
     * Se extrae las actividades, se crea el número de control, actualiza el resgitro principal con el núnero de control, el estado aprobada,
     * fecha y el usuario que aprueba y actualiza la primera actividad con el estatus "en ejecución" y las demás con el estatus "Por ejecutar".
     */
    public function metProcesaAprobacion(){
        //var_dump($this->metValidarToken());
        $params=$this->metValidarFormArrayDatos('form', 'int');
        $Exceccion_txt = array('cod_planificacion');
        $params=array_merge($params,$this->metValidarFormArrayDatos('form', 'txt',$Exceccion_txt));
        $YearPlanificacion = substr($params['fec_inicio'], -4);
        //Se extraen las actividades asignadas de la planificación.
        $resultActividades=$this->atAprDetResModelo->metActividadesAsignadas(array("idPlanificacion"=>$params['idPlanificacion'],"completa"=>true,"limite"=>""));
        $retornar=array("reg_afectado" =>"","mensaje"=>"La Aprobación no se pudo efectuar. Intente mas tarde");
        if(count($resultActividades)>0) {
            $fecha_actual = date('Y-m-d H:i:s'); $params["nuevoNroControl"]=false;
            //Se crea un array con los ids de asignación de cada actividad y su respectiva secuencia.
            foreach ($resultActividades as $fila) {
                $arrIdActividades[]=array('pk_num_actividad_planific'=>$fila['pk_num_actividad_planific'],'num_secuencia_actividad'=>$fila['num_secuencia_actividad']);
            }
            //Se ordena el array por secuencia de actividad.
            $arryOrdenado=$this->atFuncGnrles->metOrderMultiDimensionalArray ($arrIdActividades, 'num_secuencia_actividad', $inverso = false);
            if(!$params["cod_planificacion"]){ //Se genera el código de la planificación
                $tipoProcedimiento="";
                if(isset($params['cbox_tipoprocadmin'])){
                    $tipoProcedimiento=$this->atFuncGnrles->metIndicativoTipoProcAdmin($params["cbox_tipoprocadmin"]);
                }
                $params["cod_planificacion"]=$this->atFuncGnrles->metCorrelativoPlanificacion($params['id_dependencia'],$this->atIdTipoProceso,$YearPlanificacion,$this->atCodTipoProceso,$tipoProcedimiento);
                $params["nuevoNroControl"]=true;
            }
            if($params["cod_planificacion"]){//Se adicionan los parámetros a enviar para el proceso
                $params["idEmpleadoUser"]=$this->atIdEmpleadoUser;
                $params["fecha_aprobacion"]=$fecha_actual;
                $params["arrIdActividades"]=$arryOrdenado;
                $result = $this->atAprDetResModelo->metAprobarPlanificacion($params); //se ejecuta el el proceso de aprobación.
                if ($result) {
                    $retornar=array("reg_afectado" =>true,"nuevoNroControl" =>$params["nuevoNroControl"],"cod_planificacion"=>$params["cod_planificacion"],"estado_plan"=>"Aprobada","mensaje"=>"La Aprobación se procesó exitosamente");
                }
            }
        }
        echo json_encode($retornar);
    }
    /**
     * Ejecuta la anulación de la aprobación de una planificación siempre y cuando no tenga actividades terminadas ó
     * prórrogas creadas.
     * Nota: el número de control de la planificación permanece intacto.
     */
    public function metAnulaAprobacion(){
        $arrInt=$this->metValidarFormArrayDatos('form', 'int');
        //Se verifica si tiene la primera actidad terminada
        $result = $this->atAprDetResModelo->metActividadesAsignadas(array("completa" => true, "idPlanificacion" => $arrInt['idPlanificacion'], "ind_estado" => "TE","limite" => 1));
        if(count($result)==0) {
            //Se verifica si existe prórrogas para la primera actividad.
            $arrProrroga=$this->atFuncGnrles->metVerificaProrroga($arrInt['idPlanificacion']);
            if(!$arrProrroga){ exit;
                $fecha_actual = date('Y-m-d H:i:s');
                $params=array('idPlanificacion'=>$arrInt['idPlanificacion'],'fecha_anulacion'=>$fecha_actual);
                $result = $this->atAprDetResModelo->metAnularAprobacion($params);
                if ($result) {
                    $retornar = array("reg_afectado" => true, "mensaje" => "La Anulación se procesó exitosamente");
                } else {
                    $retornar = array("reg_afectado" => "", "mensaje" => "La Anulación no se pudo efectuar. Intente mas tarde");
                }
            }else{
                $retornar = array("reg_afectado" => "", "mensaje" => "¡Imposible de anular! tiene una prórroga relacionada");
            }
        }else{
            $retornar = array("reg_afectado" => "", "mensaje" => "¡Imposible de anular! tiene actividades terminadas");
        }
        echo json_encode($retornar);
    }
}
