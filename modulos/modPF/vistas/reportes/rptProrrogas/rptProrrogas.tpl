<section class="style-default-bright">
    <div class="section-header">
        <h2 id="tituloReporte" class="text-primary">{$tituloForm}</h2>
    </div>
    <div class="section-body">
        <form id="form1" class="form" role="form" method="post">
            <input id="year_actual" type="hidden" value="{$year_actual}" />
            <input type="hidden" id="idDependencia" name="form[int][idDependencia]" value="{$userInvitado}" />
            <input type="hidden" id="invitado" name="form[int][invitado]" value="{$userInvitado}" />
            <input type="hidden" id="CodTipoProceso" name="form[int][CodTipoProceso]" value="{$CodTipoProceso}" />
            {if $userInvitado}
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group" id="cbox_dependenciaError">
                            <select id="cbox_dependencia" name="form[int][cbox_dependencia]" class="form-control">
                                <option value="" selected></option>
                                {if $listaDependencias|count > 0}
                                    {foreach item=fila from=$listaDependencias}
                                        <option value="{$fila.id_dependencia}" {if $selectedDep}selected{/if}>{$fila.nombre_dependencia}</option>
                                    {/foreach}
                                {/if}
                            </select>
                            <label for="cbox_dependencia">Dependencias de Control Fiscal:</label>
                        </div>
                    </div>
                </div>
            {/if}

            <div class="row">
                <div class="col-sm-1">
                    <div class="form-group" id="cbox_yearplanError">
                        <select id="cbox_yearplan" name="form[int][cbox_yearplan]" class="form-control">
                            <option value=""></option>
                            {if $listayears|count > 0}
                                {foreach item=fila from=$listayears}
                                    {if $fila eq $year_actual}
                                        <option value="{$fila}" {if $userInvitado==false}selected{/if}>{$fila}</option>
                                    {else}
                                        <option value="{$fila}">{$fila}</option>
                                    {/if}
                                {/foreach}
                            {/if}
                        </select>
                        <label for="cbox_yearplan">Año fiscal:</label>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group" id="cbox_codPlanificacionError">
                        <select id="cbox_codPlanificacion" name="form[txt][cbox_codPlanificacion]" class="form-control">
                            <option value=""selected></option>
                            {if $listaNrosPlanif|count > 0}
                                {foreach item=fila from=$listaNrosPlanif}
                                    <option value="{$fila.nro_planificacion}">{$fila.nro_planificacion}</option>
                                {/foreach}
                            {/if}
                        </select>
                        <label for="cbox_codPlanificacion">Nro. Planificación:</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div align="center">
                        <button id="btn_Limpiar" class="btn btn-default btn-raised" title="Click para limpiar" alt="Click para limpiar">Limpiar</button>
                        {*if in_array('PF-01-01-01-01-L',$_Parametros.perfil)*}
                            <button id="btn_Buscar" class="btn btn-primary ink-reaction btn-raised" title="Click para listar primero" alt="Click para listar primero"> Buscar</button>
                            <button id="btn_Pdf" titulo="Reporte de Actuación Fiscal" class="btn btn-warning ink-reaction btn-raised"
                                    title="Click para visualizar en pdf" alt="Click para visualizar en pdf">
                                    <i class="md md-remove-red-eye"></i>Pdf
                            </button>
                        {*/if*}
                    </div>
                </div>
            </div>
            <span class="clearfix"></span>
        </form>
        <div id="resultadoConsulta"></div>
    </div>
</section>
<style type="text/css">
    {literal}
    .form-group > label, .form-group .control-label, .form-control{
        font-size: 12px;
        margin-bottom: 0;
    }
    .form-group > label{
        color: #313534;
    }
    .form-group > select{height: 27px;}
    .form-group > input{height: 27px;}
    {/literal}
</style>
<script type="text/javascript">
{literal}
    var metPrintPdf='';
    $(document).ready(function () {
        $("#form1").submit(function(){ return false; });
        var url = 'modPF/reportes/rptProrrogasCONTROL/';
        $("#cbox_dependencia").change(function(){
            $('#resultadoConsulta').html('');$("#cbox_codPlanificacion").html("");
            $("#cbox_yearplan").val($("#year_actual").val()); $("#cbox_yearplan").change();
        });
        /**
         * Lista los códigos de planificaciones
         */
        $("#cbox_yearplan").change(function(){
            var idDependencia=0;
            $("#cbox_codPlanificacion").html("");
            if(!$("#cbox_dependencia").val() && $("#invitado").val()){
                $("#cbox_yearplan").val("");
                swal("¡Atención!", "Seleccione una dependencia primero", "error"); return
            }else{
                if($("#cbox_yearplan").val()){
                    if($("#cbox_dependencia").length>0){
                        idDependencia=$("#cbox_dependencia").val();
                    }
                    $.post(url+'ListaCodPlanificacionesMET',{cbox_yearplan:$("#cbox_yearplan").val(),idDependencia:idDependencia,CodTipoProceso:$('#CodTipoProceso').val()},function(data){
                        $.each(data.filas, function(indice,fila){
                            if(indice==0){
                                $('#cbox_codPlanificacion').append('<option value=""></option>');
                            }
                            $('#cbox_codPlanificacion').append('<option value="'+fila.nro_planificacion+'">'+fila.nro_planificacion+'</option>');
                        });
                    },"json");
                }
            }
        });
        /**
         *Prepara los parámetros a ser enviados
         */
        function metPreparaParams(vPdf,idPlanificacion){
            var idDependencia=null;
            if($('#cbox_dependencia').length>0){
                idDependencia=$('#cbox_dependencia').val();
            }
            var $params = {
                cbox_yearplan:$('#cbox_yearplan').val(),
                CodTipoProceso:$('#CodTipoProceso').val(),
                cbox_dependencia:idDependencia,
                cbox_codPlanificacion:$('#cbox_codPlanificacion').val(),
                idPlanificacion:idPlanificacion
            };
            if(vPdf){
                return btoa(JSON.stringify($params));
            }else{
                return $params;
            }
        }
        /*Se buscan las planificaciones*/
        $('#btn_Buscar').click(function(){
            if($('#invitado').val() && !$('#cbox_dependencia').val()){
                swal("¡Atención!", "Debes seleccionar la dependencia de control fiscal", "error"); return
            }else if(!$('#cbox_yearplan').val()){
                swal("¡Atención!", "Seleccione el año por favor", "error"); return
            }
            $('#resultadoConsulta').html('');
            $.post(url+'ListarPlanificacionesMET', metPreparaParams(0), function (data) {
                if(data){
                    $('#resultadoConsulta').html(data);
                }else{
                    swal("Información del Sistema", "No hay resultados", "error");
                }
            });
        });
        /**
         * Genera un pdf de la planificación al hacer click en el botón de una planificación de la lista.
         * @param idPlanificacion
         */
        metPrintPdf=function(idPlanificacion){
            if($('#invitado').val() && !$('#cbox_dependencia').val()){
                swal("¡Atención!", "Debes seleccionar una dependencia de control fiscal", "error"); return
            }else if(!$('#cbox_yearplan').val()){
                swal("¡Atención!", "Seleccione el año por favor", "error"); return
            }
            $('#modalAncho').css( 'width', '85%' );
            $('#formModalLabel').html($('#tituloReporte').html());
            $('#ContenidoModal').html("");
            var $parametros=metPreparaParams(true,idPlanificacion);
            var $enviar=url+'generarReporteMET/?obj='+$parametros;
            $('#ContenidoModal').html('<iframe src="'+$enviar+'" width="100%" height="950"></iframe>');
        }
        /**
         * Genera un pdf de varias planificaciones de acuerdo a los valores de los filtros de búsqueda al hacer click en el botón PDF principal
         */
        $('#btn_Pdf').click(function () {
            if($('#invitado').val() && !$('#cbox_dependencia').val()){
                swal("¡Atención!", "Debes seleccionar una dependencia de control fiscal", "error"); return;
            }else if(!$('#cbox_yearplan').val()){
                swal("¡Atención!", "Seleccione el año por favor", "error"); return
            }else{
                $('#formModal').modal('show');
            }
            $('#modalAncho').css('width', '85%');
            $('#formModalLabel').html($('#tituloReporte').html());
            $('#ContenidoModal').html("");
            var $parametros = metPreparaParams(true,0);
            var $enviar = url + 'generarReporteMET/?obj=' + $parametros;
            $('#ContenidoModal').html('<iframe src="' + $enviar + '" width="100%" height="950"></iframe>');
        });
        $('#btn_Limpiar').click(function(){
            if($('#cbox_dependencia').length>0){
                $('#cbox_dependencia').val("");
            }
            $('#cbox_yearplan').val("");
            $('#cbox_codPlanificacion').html('');
            $('#resultadoConsulta').html('');
        });
    });
{/literal}
</script>