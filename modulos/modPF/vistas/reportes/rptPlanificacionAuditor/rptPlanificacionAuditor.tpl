<section class="style-default-bright">
    <div class="section-header">
        <h2 id="tituloReporte" class="text-primary">{$tituloForm}</h2>
    </div>
    <div class="section-body">
        <form id="form1" class="form" role="form" method="post">
            <input id="year_actual" type="hidden" value="{$year_actual}" />
            {*<input type="hidden" id="idDependencia" name="form[int][idDependencia]" value="{$userInvitado}" />*}
            <input type="hidden" id="invitado" name="form[int][invitado]" value="{$userInvitado}" />
            <input type="hidden" id="CodTipoProceso" name="form[int][CodTipoProceso]" value="{$CodTipoProceso}" />

            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group" id="cbox_tipoReporteError">
                        <select id="cbox_tipoReporte" name="form[txt][cbox_tipoReporte]" class="form-control">
                            <option value="PA">Por auditor</option>
                            <option value="PP">Por Planificación</option>
                        </select>
                        <label for="cbox_tipoReporte">Tipo de reporte:</label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="form-group" id="cbox_yearplanError">
                        <select id="cbox_yearplan" name="form[int][cbox_yearplan]" class="form-control">
                            {if $listayears|count > 0}
                                {foreach item=fila from=$listayears}
                                    {if $fila eq $year_actual}
                                        <option value="{$fila}" {if $userInvitado==false}selected{/if}>{$fila}</option>
                                    {else}
                                        <option value="{$fila}">{$fila}</option>
                                    {/if}
                                {/foreach}
                            {/if}
                        </select>
                        <label for="cbox_yearplan">Año fiscal:</label>
                    </div>
                </div>
                <div class="col-sm-2 objPorplanificacion">
                    <div class="form-group" id="cbox_tipoActuacionError">
                        <select id="cbox_tipoActuacion" name="form[int][cbox_tipoActuacion]" class="form-control">
                            <option value=""selected></option>
                            {if $listaTipoActuacion|count > 0}
                                {foreach item=tip_fila from=$listaTipoActuacion}
                                    <option value="{$tip_fila.idtipo_actuacion}">{$tip_fila.nombretipo_actuacion}</option>
                                {/foreach}
                            {/if}
                        </select>
                        <label for="cbox_tipoActuacion">Tipo actuación:</label>
                    </div>
                </div>
            </div>
            <div class="row">
                {if $userInvitado}
                    <div class="col-sm-5">
                        <div class="form-group" id="cbox_dependenciaError">
                            <select id="cbox_dependencia" name="form[int][cbox_dependencia]" class="form-control">
                                <option value="" selected></option>
                                {if $listaDependencias|count > 0}
                                    {foreach item=fila from=$listaDependencias}
                                        <option value="{$fila.id_dependencia}" {if $selectedDep}selected{/if}>{$fila.nombre_dependencia}</option>
                                    {/foreach}
                                {/if}
                            </select>
                            <label for="cbox_dependencia">Dependencias de Control Fiscal:</label>
                        </div>
                    </div>
                {/if}
                <div id="contentPersona" class="col-sm-4">
                    <div class="form-group" id="cbox_personaError">
                        <select id="cbox_persona" name="form[int][cbox_persona]" class="form-control">
                            <option value="" selected></option>
                            {if $listaPersonas|count > 0}
                                {foreach item=fila from=$listaPersonas}
                                    <option value="{$fila.id_empleado}">{$fila.nombre_empleado}</option>
                                {/foreach}
                            {/if}
                        </select>
                        <label for="cbox_persona">Personal:</label>
                    </div>
                </div>
            </div>
            <div class="row objPorplanificacion">
                <div class="col-sm-2">
                    <div class="form-group" id="txt_codPlanificacionError">
                        <input id="txt_codPlanificacion" name="form[txt][codPlanificacion]" type="text" class="form-control" value="">
                        <label for="txt_codPlanificacion">Nro. Planificación:</label>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group" id="cbox_origenActuacionError">
                        <select id="cbox_origenActuacion" name="form[int][cbox_origenActuacion]" class="form-control">
                            <option value=""selected></option>
                            {if $listaOrigenActuacion|count > 0}
                                {foreach item=tip_fila from=$listaOrigenActuacion}
                                    <option value="{$tip_fila.idorigen_actuacion}">{$tip_fila.nombre_origenactuacion}</option>
                                {/foreach}
                            {/if}
                        </select>
                        <label for="cbox_origenActuacion">Origen actuación:</label>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div id="txt_fechainic1Error" class="form-group">
                        <input id="txt_fechainic1" type="text" name="form[txt][txt_fechainic1]" class="form-control texto-size date">
                        <label for="txt_fechainic1"><i class="fa fa-calendar"></i>Fecha desde</label>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div id="txt_fechainic2Error" class="form-group">
                        <input id="txt_fechainic2" type="text" name="form[txt][txt_fechainic2]" class="form-control texto-size date">
                        <label for="txt_fechainic2"><i class="fa fa-calendar"></i>Fecha hasta</label>
                    </div>
                </div>
            </div>
            <div class="row objPorplanificacion">
                <div class="col-sm-4">
                    <div class="form-group" id="cbox_enteError">
                        <select id="cbox_ente" name="form[int][cbox_ente]" class="form-control">
                            <option value="" selected></option>
                            {if $listaentes|count > 0}
                                {foreach item=fila from=$listaentes}
                                    <option value="{$fila.id_ente}">{$fila.nombre_ente}</option>
                                {/foreach}
                            {/if}
                        </select>
                        <label for="cbox_ente"><i class="fa fa-institution"></i>Entes:</label>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group" id="cbox_estadosplanError">
                        <select id="cbox_estadosplan" name="form[txt][cbox_estadosplan]" class="form-control">
                            <option value="">Todos</option>
                            {if $listaEstadosplan|count > 0}
                                {foreach item=fila from=$listaEstadosplan}
                                    <option value="{$fila.estado}">{$fila.desc_estado}</option>
                                {/foreach}
                            {/if}
                        </select>
                        <label for="cbox_estadosplan">Estados plan:</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div align="center">
                        <button id="btn_Limpiar" class="btn btn-default btn-raised" title="Click para limpiar" alt="Click para limpiar">Limpiar</button>
                        {*if in_array('PF-01-01-01-01-L',$_Parametros.perfil)*}
                            <button id="btn_Buscar" class="btn btn-primary ink-reaction btn-raised" title="Click para listar primero" alt="Click para listar primero"> Buscar</button>
                            <button id="btn_Pdf" titulo="Reporte de Actuación Fiscal" class="btn btn-warning ink-reaction btn-raised"
                                    title="Click para visualizar en pdf" alt="Click para visualizar en pdf">
                                    <i class="md md-remove-red-eye"></i>Pdf
                            </button>
                        {*/if*}
                    </div>
                </div>
            </div>
            <span class="clearfix"></span>
        </form>
        <div id="resultadoConsulta"></div>
    </div>
</section>
<style type="text/css">
    {literal}
    .form-group > label, .form-group .control-label, .form-control{
        font-size: 12px;
        margin-bottom: 0;
    }
    .form-group > label{
        color: #313534;
    }
    .form-group > select{height: 27px;}
    .form-group > input{height: 27px;}
    {/literal}
</style>
<script type="text/javascript">
{literal}
    var metPrintPdf='';
    $(document).ready(function () {
        $(".objPorplanificacion").hide(); $("#btn_Buscar").hide();
        $("#form1").submit(function(){ return false; });
        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "dd-mm-yyyy",language: 'es'});
        var url = 'modPF/reportes/rptPlanificacionAuditorCONTROL/';
        $("#txt_fechainic1").hover(
            function(){$('#txt_fechainic1').attr({ placeholder:"Fecha inicio plan" });},
            function(){$('#txt_fechainic1').removeAttr('placeholder');}
        );
        $("#txt_fechainic2").hover(
            function(){$('#txt_fechainic2').attr({ placeholder:"Fecha inicio plan" });},
            function(){$('#txt_fechainic2').removeAttr('placeholder');}
        );
        $("#cbox_tipoReporte").change(function(){
            if($("#cbox_tipoReporte").val()=='PP'){
                $(".objPorplanificacion").show(); $("#btn_Buscar").show();$("#btn_Pdf").hide(); $('#contentPersona').hide();
            }else{
                $('#btn_Limpiar').click();
                $("#resultadoConsulta").html();
                $(".objPorplanificacion").hide(); $("#btn_Buscar").hide();$("#btn_Pdf").show();$('#contentPersona').show();
            }
        });
        /**
         * Lista los Auditores de acuerdo a una dependencia seleccionada
         */
        $("#cbox_dependencia").change(function(){
            $("#cbox_persona").html(""); $('#resultadoConsulta').html('');
            if(!$("#cbox_dependencia").val() && $("#invitado").val()){
                swal("¡Atención!", "Seleccione una dependencia primero", "error"); return
            }else{
                $.post(url+'ListaPersonalMET',{idDependencia:$("#cbox_dependencia").val(),CodTipoProceso:$('#CodTipoProceso').val()},function(data){
                    $.each(data.listaPersona, function(indice,fila){
                        if(indice==0){
                            $('#cbox_persona').append('<option value=""></option>');
                        }
                        $('#cbox_persona').append('<option value="'+fila.id_empleado+'">'+fila.nombre_empleado+'</option>');
                    });
                },"json");
            }
        });
        /**
         * Monta de forma recursiva las dependencias de un ente al ser seleccionado
         */
        function metCargCboxRcsvo(idEnte){
            var $tultic='',valor_filtro=idEnte;
            $.post(url+'ListaEntesMET',{idEnte:idEnte,CodTipoProceso:$('#CodTipoProceso').val()},function(data){
                $('#cbox_ente').html("");
                if(data.filas){
                    $.each(data.filas, function(i,fila){
                        $tultic='title="'+fila.toltick+'" alt="' +  fila.toltick  +'"';
                        if(i==0){
                            if(valor_filtro==0){
                                $('#cbox_ente').append('<option value="" selected></option>');
                                $('#cbox_ente').append('<option value="'+fila.id_ente+'"'+$tultic+'>'+fila.nombre_ente+'</option>');
                                $('#cbox_ente').val("");
                            }else{
                                $('#cbox_ente').append('<option value="'+fila.id_ente+'"'+$tultic+'>'+fila.nombre_ente+'</option>');
                                $('#cbox_ente').val(fila.id_ente);
                            }
                            $id_nivel_ant=fila.idente_padre;
                        }else{
                            $('#cbox_ente').append('<option value="'+fila.id_ente+'"'+$tultic+'>'+fila.nombre_ente+'</option>');
                        }
                    });
                }
                if(valor_filtro!=0){
                    $('#cbox_ente').append('<option value="'+$id_nivel_ant+'">-----Nivel Anterior-----</option>');
                }
            }, "json");
        }
        /**
         * Consulta y monta los entes en el combo de forma recursiva
         */
        $('#cbox_ente').change(function(){
            if($('#cbox_ente').val()){
                metCargCboxRcsvo($('#cbox_ente').val());
            }
        });
        /**
         *Prepara los parámetros a ser enviados
         */
        function metPreparaParams(vPdf,idPlanificacion){
            var idDependencia=null;
            if($('#cbox_dependencia').length>0){
                idDependencia=$('#cbox_dependencia').val();
            }
            var $params = {
                cbox_yearplan:$('#cbox_yearplan').val(),
                CodTipoProceso:$('#CodTipoProceso').val(),
                cbox_dependencia:idDependencia,
                txt_codPlanificacion:$('#txt_codPlanificacion').val(),
                cbox_tipoActuacion:$('#cbox_tipoActuacion').val(),
                cbox_origenActuacion:$('#cbox_origenActuacion').val(),
                cbox_ente:$('#cbox_ente').val(),
                txt_fechainic1:$('#txt_fechainic1').val(),
                txt_fechainic2:$('#txt_fechainic2').val(),
                cbox_estadosplan:$('#cbox_estadosplan').val(),
                idPlanificacion:idPlanificacion
            };
            if(vPdf){
                return btoa(JSON.stringify($params));
            }else{
                return $params;
            }
        }
        /*Se buscan las planificaciones*/
        $('#btn_Buscar').click(function(){
            if($('#invitado').val() && !$('#cbox_dependencia').val()){
                swal("¡Atención!", "Debes seleccionar la dependencia de control fiscal", "error"); return
            }else if(!$('#cbox_yearplan').val()){
                swal("¡Atención!", "Seleccione el año por favor", "error"); return
            }
            $('#resultadoConsulta').html('');
            $.post(url+'ListarPlanificacionesMET', metPreparaParams(0), function (data) {
                if(data){
                    $('#resultadoConsulta').html(data);
                }else{
                    swal("Información del Sistema", "No hay resultados", "error");
                }
            });
        });
        /**
         * Genera un retporte en pdf de auditores por planificación al hacer click en el botón de una planificación de la lista.
         * @param idPlanificacion
         */
        metPrintPdf=function(idPlanificacion){
            if($('#invitado').val() && !$('#cbox_dependencia').val()){
                swal("¡Atención!", "Debes seleccionar una dependencia de control fiscal", "error"); return
            }else if(!$('#cbox_yearplan').val()){
                swal("¡Atención!", "Seleccione el año por favor", "error"); return
            }
            $('#modalAncho').css( 'width', '85%' );
            $('#formModalLabel').html($('#tituloReporte').html());
            $('#ContenidoModal').html("");
            var $parametros=metPreparaParams(true,idPlanificacion);
            var $enviar=url+'GenerarReporteMET/?obj='+$parametros;
            $('#ContenidoModal').html('<iframe src="'+$enviar+'" width="100%" height="950"></iframe>');
        }
        /**
         * Genera un reporte en pdf de planificaciones por auditor hacer click en el botón PDF principal y de acuerdo al auditor seleccionado
         */
        $('#btn_Pdf').click(function () {
            if ($('#invitado').val()){
                if(!$('#cbox_dependencia').val()) {
                    swal("¡Atención!", "Debes seleccionar una dependencia de control fiscal", "error"); return;
                } else if (!$('#cbox_persona').val()) {
                    swal("¡Atención!", "Seleccione una persona por favor", "error"); return;
                }
            }else{
                if (!$('#cbox_persona').val()) {
                    swal("¡Atención!", "Seleccione una persona por favor", "error"); return;
                }
            }
            var idDependencia=null;
            if($('#cbox_dependencia').length>0){
                idDependencia=$('#cbox_dependencia').val();
            }
            var $params = {
                cbox_yearplan:$('#cbox_yearplan').val(),
                CodTipoProceso:$('#CodTipoProceso').val(),
                idDependencia:idDependencia,
                cbox_persona:$('#cbox_persona').val()
            };
            $parametros=btoa(JSON.stringify($params));
            $('#formModal').modal('show');
            $('#modalAncho').css('width', '85%');
            $('#formModalLabel').html($('#tituloReporte').html());
            $('#ContenidoModal').html("");
            var $enviar = url + 'GenerarReporteAuditorMET/?obj=' + $parametros;
            $('#ContenidoModal').html('<iframe src="' + $enviar + '" width="100%" height="950"></iframe>');
        });
        $('#btn_Limpiar').click(function(){
            if($('#cbox_dependencia').length>0){
                $('#cbox_dependencia').val("");
                $('#cbox_persona').html('');
            }else{
                $('#cbox_persona').val("");
            }
            $('#cbox_yearplan').val($('#year_actual').val());
            $('#txt_codPlanificacion').val('');$('#cbox_tipoActuacion').val('');
            $('#cbox_origenActuacion').val('');$('#txt_fechainic1').val('');$('#txt_fechainic2').val('');
            if($('#cbox_ente').val()){metCargCboxRcsvo(0);}
            $('#resultadoConsulta').html('');$('#cbox_estadosplan').val('');
        });
    });
{/literal}
</script>