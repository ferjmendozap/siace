<section class="style-default-bright">
    <div class="section-header">
        <h3 class="text-primary"> Reporte Determinación de Responsabilidades por Auditor</h3>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <form class="form-horizontal" role="form">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="col-sm-6">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="fk_pk_num_organismo">Organismo</label>
                                <div class="col-sm-9">
                                    <select id="fk_pk_num_organismo" name="form[int][fk_pk_num_organismo]" class="form-control select2-list">
                                       <option value="">Seleccione El Organismo</option>
                                        {foreach item=i from=$tipoOrganismo}
                                            <option value="{$i.pk_num_organismo}">{$i.ind_descripcion_empresa}</option>
                                        {/foreach}
                                    </select>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="pk_num_miscelaneo_detalle">Proceso</label>
                                <div class="col-sm-9">
                                   <select id="pk_num_miscelaneo_detalle" name="form[txt][pk_num_miscelaneo_detalle]" class="form-control select2-list select2"> 
                                    <option value="">Seleccione el Proceso</option>
                                     {foreach item=i from=$tipoProceso}
                                            <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                     {/foreach}
                                </select>  
                                
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="pk_num_dependencia">Dependencia</label>
                            <div class="col-sm-9">
                                <select id="pk_num_dependencia" name="form[int][pk_num_dependencia]" class="form-control select2-list">
                                        <option value="">Seleccione la Dependencia</option> 
                                    </select>                         
                                <div class="form-control-line"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-5">

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="ind_cedula_documento">Auditor</label>
                            <div class="col-sm-9">
                                <select id="ind_cedula_documento" name="form[txt][ind_cedula_documento]" class="form-control select2-list select2">
                                    <option value="">Seleccione al Auditor</option>
                                </select>
                                <div class="form-control-line"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-5">

                            </div>
                        </div>
                    </div>
                    <div class="form-control-line"></div>
                    <div class="col-sm-12 text-center">
                        <input id="id_tipo_reporte" name="id_tipo_reporte" type="hidden" value="4">
                        <button type="button" class="btn btn-xs btn-primary" id="selectEmpleado" titulo="Ver Reporte">Buscar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<br>
<div class="row" id="pdfAuditoresDeterminacionResponsabilidades">
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var app = new  AppFunciones();
        $("#fk_pk_num_organismo").change(function () {
          var $url='{$_Parametros.url}modPF/reportes/reporteAuditoresDeterminacionResponsabilidadesCONTROL/listarDependenciasMET';
          var $url2='{$_Parametros.url}modPF/reportes/reporteAuditoresDeterminacionResponsabilidadesCONTROL/ListarAuditoresComboMET';
          $("#fk_pk_num_organismo option:selected").each(function () {
            idOrganismo=$(this).val();
            $.post($url, { idOrganismo: idOrganismo }, function($data){
                $("#pk_num_dependencia").html($data);
            });
            $.post($url2, { idOrganismo: idOrganismo }, function($data){
                $("#ind_cedula_documento").html($data);
            });

           });
        })

        $('#selectEmpleado').click(function(){
          if($('#fk_pk_num_organismo').val()=='' || $('#pk_num_dependencia').val()=='' || $('#pk_num_miscelaneo_detalle').val()==''|| $('#ind_cedula_documento').val()==''){
                if($('#fk_pk_num_organismo').val()==''){
                    swal("Error!", 'Disculpa Debes Seleccionar el Organismo', "error");
                }else if($('#pk_num_dependencia').val()==''){
                    swal("Error!", 'Disculpa Debes Seleccionar la Dependencia', "error");
                }else if($('#pk_num_miscelaneo_detalle').val()==''){
                    swal("Error!", 'Disculpa Debes Seleccionar el Proceso', "error");
                }else if($('#ind_cedula_documento').val()==''){
                    swal("Error!", 'Disculpa Debes Seleccionar al Auditor', "error");
                }
            }else{
                $('#pdfAuditoresDeterminacionResponsabilidades').html('<iframe frameborder="0" src="{$_Parametros.url}modPF/reportes/reporteAuditoresDeterminacionResponsabilidadesCONTROL/pdfAuditoresDeterminacionResponsabilidadesMET/'+$('#fk_pk_num_organismo').val()+'/'+$('#pk_num_dependencia').val()+'/'+$('#pk_num_miscelaneo_detalle').val()+'/'+$('#ind_cedula_documento').val()+'/'+$('#id_tipo_reporte').val()+'" width="100%" height="540px"></iframe>');
           }
        });
    });
</script> 