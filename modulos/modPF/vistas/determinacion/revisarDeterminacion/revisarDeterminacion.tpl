<style type="text/css">
    {literal}
    .tab-pane{padding: 0}
    .form-group > label, .form-group .control-label, .form-control {
        font-size: 12px;
        margin-bottom: 0;
        opacity: 1;
    }
    {/literal}
</style>
<div class="modal-body" style="padding:0;">
    <div class="container-fluid">
        <div id="rootWizard" class="form-wizard form-wizard-horizontal">
            <div class="form-wizard-nav">
                <div class="progress">
                    <div class="progress-bar progress-bar-primary"></div>
                </div>
                <ul class="nav nav-justified">
                    <li><a id="tab_1" class="active" href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">INFORMACIÓN GENERAL</span></a></li>
                    <li><a id="tab_2" href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">AUDITORES</span></a></li>
                    <li><a id="tab_3" href="#tab3" data-toggle="tab"><span class="step">3</span> <span class="title">ACTIVIDADES</span></a></li>
                </ul>
            </div>
            <div class="tab-content clearfix">
                <!--INFORMACIÓN DE LA PLANIFICACIÓN-->
                <div class="tab-pane active" id="tab1">
                    <div class="card" style="padding:0;">
                        <div class="card-head style-primary-light">
                            <header>Información General</header>
                        </div>
                        <div class="card-body">
                            <form id="formAjaxTab1" action="{$_Parametros.url}modPF/determinacion/revisarDeterminacionCONTROL/ProcesaRevisionMET" class="form-horizontal floating-label form-validation" novalidate="novalidate" role="form" method="post">
                                <input type="hidden" name="valido" value="1" />
                                <input type="hidden" id="idPlanificacion" name="form[int][idPlanificacion]" value="{$planificacion.pk_num_planificacion}" />
                                <div class="form-group">
                                    <!--Código de la  Actuacion-->
                                    <div class="col-sm-2 text-right">
                                        <label for="lbl_codplanificacion" class="control-label">Determinación Resp. Nro.:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div id="lbl_codplanificacion" style="margin-top: 8px;;">{$planificacion.cod_planificacion}</div>
                                    </div>
                                    <!--Código de la  Actuacion-->
                                    <div class="col-sm-2 text-right">
                                        <label for="lbl_codValjuridica" class="control-label">Valoración Nro:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div id="lbl_codValjuridica" style="margin-top: 8px;;">{$planificacion.cod_valjuridica}</div>
                                    </div>
                                    <!--Estado de la Actuación-->
                                    <div class="col-sm-2 text-right">
                                        <label for="ind_estado" class="control-label">Estado:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div id="lbl_estado" style="margin-top: 8px;;">{if isset($planificacion.desc_estado)}{$planificacion.desc_estado}{/if}</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!--organismo ejecutante-->
                                    <div class="col-sm-2 text-right">
                                        <label for="id_contraloria" class="control-label">Contraloría:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="id_contraloriaError">
                                            <select id="id_contraloria" name="form[int][id_contraloria]" class="form-control select2" disabled="disabled">
                                                {foreach item=fila from=$listaContraloria}
                                                    <option value="{$fila.id_contraloria}" selected>{$fila.nombre_contraloria}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                    <!--fecha inicio actuacion-->
                                    <div class="col-sm-2 text-right">
                                        <label for="fec_inicio" class="control-label">Fecha Inicio:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input id="fec_inicio" type="text" name="form[txt][fec_inicio]" class="form-control" style="margin-top: 2px;" value="{$planificacion.fec_inicio}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!--dependencia interna de la contraloría-->
                                    <div class="col-sm-2 text-right">
                                        <label for="id_dependencia" class="control-label">Dependencia:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="id_dependenciaError">
                                            <select id="id_dependencia" name="form[int][id_dependencia]" class="form-control select2">
                                                <option value="">Seleccione..</option>
                                                {foreach item=fila from=$listaDepContraloria}
                                                    {if isset($planificacion.fk_a004_num_dependencia)}
                                                        {if $fila.id_dependencia eq $planificacion.fk_a004_num_dependencia}
                                                            <option value="{$fila.id_dependencia}" selected>{$fila.nombre_dependencia}</option>
                                                        {else}
                                                            <option value="{$fila.id_dependencia}">{$fila.nombre_dependencia}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$fila.id_dependencia}">{$fila.nombre_dependencia}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                    <!--fecha termino actuacion-->
                                    <div class="col-sm-2 text-right">
                                        <label for="fec_termino" class="control-label">Fecha Termino:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div id="lbl_fecha_fin_plan" style="margin-top: 8px;;">{$planificacion.fecha_fin_plan}</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!--Dependencias Centro de Costo:-->
                                    <div class="col-sm-2 text-right">
                                        <label for="pk_num_centro_costo"class="control-label">Centro de Costo:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="pk_num_centro_costoError">
                                            <select id="pk_num_centro_costo" name="form[int][pk_num_centro_costo]" class="form-control select2">
                                                <option value="">Seleccione..</option>
                                                {if $listaCentroCosto|count > 0}
                                                    {foreach item=fila from=$listaCentroCosto}
                                                        {if $planificacion.fk_a023_num_centro_costo eq $fila.pk_num_centro_costo}
                                                            <option value="{$fila.pk_num_centro_costo}" selected>{$fila.ind_descripcion_centro_costo}</option>
                                                        {else}
                                                            <option value="{$fila.pk_num_centro_costo}">{$fila.ind_descripcion_centro_costo}</option>
                                                        {/if}
                                                    {/foreach}
                                                {/if}
                                            </select>
                                        </div>
                                    </div>
                                    <!--Duracion afecta -->
                                    <div class="col-sm-2 text-right">
                                        <label for="duracion_afecta" class="control-label">Duración Afecta:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div id="lbl_duracion_afectacion" style="margin-top: 8px;;">{$planificacion.cant_dias_afecta}</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!--Tipo de actuación:-->
                                    <div class="col-sm-2 text-right">
                                        <label for="idtipo_actuacion"class="control-label">Tipo Actuacion:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="idtipo_actuacionError">
                                            <select id="idtipo_actuacion" name="form[int][idtipo_actuacion]" class="form-control select2" disabled="disabled">
                                                <option value="">Seleccione..</option>
                                                {if $listaTipoActuacion|count > 0}
                                                    {foreach item=filatipo from=$listaTipoActuacion}
                                                        {if $planificacion.fk_a006_num_miscdet_tipoactuacion eq $filatipo.idtipo_actuacion}
                                                            <option value="{$filatipo.idtipo_actuacion}" selected>{$filatipo.nombretipo_actuacion}</option>
                                                        {else}
                                                            <option value="{$filatipo.idtipo_actuacion}">{$filatipo.nombretipo_actuacion}</option>
                                                        {/if}
                                                    {/foreach}
                                                {/if}
                                            </select>
                                        </div>
                                    </div>
                                    <!--Duracion no afecta -->
                                    <div class="col-sm-2 text-right">
                                        <label for="duracion_no_afecta" class="control-label">Duración No Afecta:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div id="lbl_duracion_no_afectacion" style="margin-top: 8px;;">{$planificacion.cant_dias_no_afecta}</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!--Entes Externos:-->
                                    <div class="col-sm-2 text-right">
                                        <label for="id_ente" class="control-label">Ente:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="nombre_ente">{$planificacion.nombre_ente}</div>
                                    </div>
                                    <!--Prorroga-->
                                    <div class="col-sm-2 text-right">
                                        <label for="prorroga" class="control-label">Prorroga:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div id="lbl_prorroga" style="margin-top: 8px;;">{$planificacion.cant_dias_prorroga}</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!--origen de la actuacion:-->
                                    <div class="col-sm-2 text-right">
                                        <label for="idorigen_actuacion" class="control-label">{if !$pAcionFiscal}Origen actuación:{else}Dep. solicitante:{/if}</label>
                                    </div>
                                    <div class="col-sm-6">
                                        {if !$pAcionFiscal}
                                            <div id="idorigen_actuacionError">
                                                <select id="idorigen_actuacion" name="form[int][idorigen_actuacion]" class="form-control select2">
                                                    <option value="">Seleccione..</option>
                                                    {if $listaOrigenActuacion|count > 0}
                                                        {foreach item=fila from=$listaOrigenActuacion}
                                                            {if $planificacion.fk_a006_num_miscdet_origenactuacion eq $fila.idorigen_actuacion}
                                                                <option value="{$fila.idorigen_actuacion}" selected>{$fila.nombre_origenactuacion}</option>
                                                            {else}
                                                                <option value="{$fila.idorigen_actuacion}">{$fila.nombre_origenactuacion}</option>
                                                            {/if}
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </div>
                                        {else}
                                            <div style="margin-top: 8px;">{$planificacion.dependencia_solctte}</div>
                                        {/if}
                                    </div>
                                    <!--Duración total -->
                                    <div class="col-sm-2 text-right">
                                        <label for="duracionTotal" class="control-label">Duración total:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div id="lbl_duracionTotal" style="margin-top: 8px;;">{$planificacion.totalDias_Plan}</div>
                                    </div>
                                </div>
                                {if $planificacion.idplanificacion_referencia}
                                    <div class="form-group">
                                        <!--Tipos de proceso fiscal y código de control de planificación-->
                                        <div class="col-sm-2 text-right">
                                            <label for="idorigen_actuacion" class="control-label">Tipo de Proceso:</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <div style="margin-top: 8px;">{$planificacion.descProceso}</div>
                                        </div>

                                        <div class="col-sm-2 text-right">
                                            <label for="duracionTotal" class="control-label">Nro. Planificación:</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div style="margin-top: 8px;">{$planificacion.codPlnfcnSlctte}</div>
                                        </div>
                                    </div>
                                {/if}
                                <div class="form-group">
                                    <!---objetivo general-->
                                    <div class="col-sm-2 text-right">
                                        <label for="ind_objetivo" class="control-label">Objetivo General:</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <div id="ind_objetivoError" >
                                            <textarea id="ind_objetivo" class="form-control" rows="2" name="form[txt][ind_objetivo]">{if isset($planificacion.ind_objetivo)}{$planificacion.ind_objetivo}{/if}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!---alcance-->
                                    <div class="col-sm-2 text-right">
                                        <label for="ind_alcance" class="control-label">Alcance:</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <div id="ind_alcanceError" >
                                            <textarea id="ind_alcance" class="form-control" rows="2" name="form[txt][ind_alcance]">{if isset($planificacion.ind_alcance)}{$planificacion.ind_alcance}{/if}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!---observación-->
                                    <div class="col-sm-2 text-right">
                                        <label for="ind_observacion" class="control-label">Observación:</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <div>
                                            <textarea id="ind_observacion" class="form-control" rows="2" name="form[txt][ind_observacion]">{if isset($planificacion.ind_observacion)}{$planificacion.ind_observacion}{/if}</textarea>
                                        </div>
                                    </div>
                                </div>
                                {if $planificacion.tipoProcAdmin}
                                    <div class="form-group">
                                        <!--origen de la actuacion:-->
                                        <div class="col-sm-2 text-right">
                                            <label for="idorigen_actuacion" class="control-label">Procedimiento:</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <div id="cbox_tipoprocadminError">
                                                <select id="cbox_tipoprocadmin" name="form[txt][cbox_tipoprocadmin]" class="form-control select2" disabled="disabled">
                                                    <option value="">Seleccione..</option>
                                                    {if $listaTiposproc|count > 0}
                                                        {foreach item=fila from=$listaTiposproc}
                                                            {if $planificacion.ind_procdmtoadministrativo eq $fila.valor}
                                                                <option value="{$fila.valor}" selected>{$fila.descripcion}</option>
                                                            {else}
                                                                <option value="{$fila.valor}">{$fila.descripcion}</option>
                                                            {/if}
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                {/if}
                            </form>
                        </div>
                    </div>
                </div>
                <!--PANEL AUDITORES-->
                <div class="tab-pane floating-label" id="tab2">
                    <div class="card">
                        <div class="card-head style-primary-light">
                            <header>Auditores</header>
                        </div>
                        <div class="card-body">
                            <div class="col-lg-12">
                                {*Grilla Auditores asignados*}
                                <div class="table-responsive">
                                    <table id="tb_auditores_plan" class="table table-hover table-striped no-margin">
                                        <thead>
                                        <tr>
                                            <th>Coordinador(a)</th>
                                            <th>Nombre Completo</th>
                                            <th>Cargo</th>
                                            <th>Credencial</th>
                                            <th>Fecha</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {if $auditoresDesignados|count > 0}
                                            {foreach item=fila from=$auditoresDesignados}
                                                <tr id="tr_auditor_{$fila.id_auditorPlanificacion}">
                                                    <td><i class="{if $fila.flagCordinador==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                                    <td id="td_auditor_{$fila.id_auditorPlanificacion}">{$fila.nombre_auditor}</td>
                                                    <td>{$fila.cargo_auditor}</td>
                                                    <td>{$fila.desc_estatus}</td>
                                                    <td>{$fila.fecha_estatus}</td>
                                                </tr>
                                            {/foreach}
                                        {/if}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--PANEL DE ACTIVIDADES-->
                <div class="tab-pane floating-label" id="tab3">
                    <div class="card">
                        <div class="card-head style-primary-light">
                            <header>Actividades</header>
                        </div>
                        <div class="card-body">
                            {*Contenedor de Actividades*}
                            <div id="tb_actividades"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="div_estado">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group floating-label">
                            <label for="preparador">Preparado por:</label>
                            <div style="font-size: 10px">{if isset($planificacion.preparado_por)}{$planificacion.preparado_por}{else}{''}{/if}</div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group floating-label">
                            <label for="revisador">Revisado por:</label>
                            <div style="font-size: 10px">{if isset($planificacion.revisado_por)}{$planificacion.revisado_por}{else}{''}{/if}</div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group floating-label">
                            <label for="aprobador">Aprobado por:</label>
                            <div style="font-size: 10px">{if isset($planificacion.aprobado_por)}{$planificacion.aprobado_por}{else}{''}{/if}</div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group floating-label">
                            <label for="ultmodificacion"><i class="fa fa-calendar"></i>Fecha registro:</label>
                            <div id="lbl_fehareg" style="font-size: 10px">{if isset($planificacion.fec_ultima_modificacion)}{$planificacion.fec_ultima_modificacion}{else}{$fec_ultima_modificacion}{/if}</div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group floating-label">
                            <label for="ultmodificacion"><i class="fa fa-calendar"></i>Última modificación:</label>
                            <div style="font-size: 10px">{if isset($planificacion.fec_ultima_modificacion)}{$planificacion.fec_ultima_modificacion}{else}{$fec_ultima_modificacion}{/if}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="barra_opciones" align="center">
                <button type="button" class="btn btn-default btn-raised" data-dismiss="modal" title="Click para Salir"  alt="Click para Salir">
                    <span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar
                </button>
                {*Botón Anular planificación*}
                {if in_array('PF-01-03-02-02-02-02-A',$_Parametros.perfil) and $planificacion.ind_estado eq 'RV'}
                    <button type="button" id="btn_anulaPlan" class="btn btn-primary btn-raised" title="Click para anular la revisión"  alt="Click para anular la revisión">
                        <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Anular Revisión
                    </button>
                {/if}
                {*Botón procesar revisión planificación*}
                {if in_array('PF-01-03-02-02-02-01-R',$_Parametros.perfil) and $planificacion.ind_estado eq 'PR'}
                    <button type="button" id="btn_procesarRevision" class="btn btn-primary btn-raised" title="Click para procesar la revisión"  alt="Click para procesar la revisión">
                        <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Procesar Revisión
                    </button>
                {/if}
            </div>
        </div>
    </div>
</div>
<span class="clearfix"></span>
<script type="text/javascript">
    $(document).ready(function () {
        var appf=new AppfFunciones();
        var app = new AppFunciones();
        app.metWizard();
        $('#modalAncho').css("width", "90%"); $('#ContenidoModal').css("padding-top", "0"); /*Ancho de la Modal*/
        //Complementos
        $('.select2').select2({ allowClear: true });
        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "dd-mm-yyyy",language: 'es'});
        var url = '{$_Parametros.url}modPF/determinacion/revisarDeterminacionCONTROL/';
        $("#formAjaxTab1").submit(function(){ return false; });
        function metHabilitaObjetos(){
            appf.metEstadoObjForm('id_dependencia,pk_num_centro_costo,idtipo_actuacion,id_ente,id_ente,idorigen_actuacion,ind_objetivo,ind_alcance,ind_observacion,fec_inicio',true);
        }
        function metDeshabilitaObjetos(){
            appf.metEstadoObjForm('id_dependencia,pk_num_centro_costo,idtipo_actuacion,id_ente,id_ente,idorigen_actuacion,ind_objetivo,ind_alcance,ind_observacion,cbox_tipoprocadmin,fec_inicio',false);
        }
        /**
         * Busca y monta las actividades
         */
        function metMontaActividades(){
            $('#tb_actividades').html('');
            $.post(url+'BuscaActividadesMET',{ idPlanificacion:$('#idPlanificacion').val() }, function (data) {
                if (data) {
                    $('#tb_actividades').html(data);
                } else {
                    swal("Atención!", "No se pudo montar las actividades de la planificación", "error");
                }
            });
        }
        $('#tab_3').click(function(){ /*Ficha de actividades*/
            $("#tab_3").attr({ "data-toggle":"tab" });
            if(!$("#tb_actividades").html()) {
                metMontaActividades();
            }
        });
        /**
         * Ejecuta el proceso de revisión de la planificación
         */
        $('#btn_procesarRevision').click(function(){
            metHabilitaObjetos();
            swal({
                title: "Procesar Revisión",
                text: "Se va a procesar la revisión de la planificación actual. Estas de Acuerdo",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if(isConfirm){
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post($("#formAjaxTab1").attr("action"), $("#formAjaxTab1").serialize(), function (data) {
                        if(data.reg_afectado){
                            msjBuscar=false;
                            $('#btn_Limpiar').click();
                            $('#btn_Buscar').click();
                            swal("Revisión procesada", data.mensaje, "success");
                            $('#ContenidoModal').html('');$("#cerrarModal").click();
                        } else {
                            swal("¡Atención!", data.mensaje, "error");
                            metDeshabilitaObjetos();
                        }
                    }, 'json');
                }else{
                    swal("¡Cancelado!", "Proceso cancelado", "error");
                    metDeshabilitaObjetos();
                }
            });
        });
        /**
         * Ejecuta la anulación de la planificación
         */
        $('#btn_anulaPlan').click(function(){
            metHabilitaObjetos();
            swal({
                title: "Anular planificación",
                text: "Se va a procesar la anulación de revisión de la planificación actual. Estas de Acuerdo",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if(isConfirm){
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post(url+'AnularPlanificacionMET', $("#formAjaxTab1").serialize(), function (data) {
                        if (data.reg_afectado) {
                            msjBuscar=false;
                            swal("Información del proceso", data.mensaje, "success");
                            $('#btn_Buscar').click();//Refresca la grilla principal
                            $('#ContenidoModal').html('');$('#cerrarModal').click();
                        }else{
                            swal("Información del proceso", data.mensaje, "error");
                        }
                    }, 'json');
                }else{
                    swal("¡Cancelado!", "Proceso cancelado", "error");
                    metDeshabilitaObjetos();
                }
            });
        });
        metDeshabilitaObjetos();
    });
</script>