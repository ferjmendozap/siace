<style type="text/css">
    {literal}
        .table tbody>tr>td.vert-align{vertical-align: middle;}
        .puntero{cursor:help;}
    {/literal}
</style>
<div class="section-body">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">Determinación Nro</th>
                        <th class="text-center">Objetivo</th>
                        <th class="text-center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ente&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th class="text-center">Fecha Inicio</th>
                        <th class="text-center">Fecha fin</th>
                        <th class="text-center">Fec fin Real</th>
                        <th class="text-center">Estado</th>
                        <th class="text-center">Acci&oacute;n</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=fila from=$listado}
                        <tr id="tr_atuacion_'.{$fila.pk_num_planificacion}'">
                            <td class="vert-align">{$fila.cod_planificacion}</td>
                            <td>{$fila.ind_objetivo}</td>
                            <td>{$fila.nombre_ente}</td>
                            <td class="vert-align">{$fila.fec_inicio}</td>
                            <td class="vert-align">{$fila.fecha_finplan}</td>
                            <td class="vert-align">{$fila.fecha_finrealplan}</td>
                            <td class="vert-align {if $fila.ind_estado eq 'AN'}puntero{/if}" {if $fila.ind_estado eq 'AN'}title="{$fila.ind_nota_justificacion}" alt="{$fila.ind_nota_justificacion}"{/if}>{$fila.desc_estado}</td>
                            <td class="vert-align">
                                {if in_array('PF-01-03-02-02-02-01-R',$_Parametros.perfil)}
                                    {if $fila.ind_estado == 'PR'}
                                        <button id="btn_actualizar" class="btn btn-raised btn-primary btn-xs"
                                                data-toggle="modal" data-target="#formModal"
                                                onclick="metMontaPlanificacion({$fila.pk_num_planificacion})"
                                                title="Click para revisar la planificación" alt="Click para revisar la planificación">
                                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                                        </button>
                                    {else}
                                        <button id="btn_ver" class="logsUsuario btn btn-raised btn-xs btn-warning"
                                                data-toggle="modal" data-target="#formModal"
                                                onclick="metMontaPlanificacion({$fila.pk_num_planificacion})"
                                                title="Ver planificación" alt="Ver planificación">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                                    {/if}
                                {else}
                                    <button class="btn btn-raised btn-xs btn-default"
                                            title="No tienes permiso para esta opción" alt="No tienes permiso para esta opción">
                                        <i class=" glyphicon glyphicon-edit"></i>
                                    </button>
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
{*if $fila.ind_estado eq 'AP' OR $fila.ind_estado eq 'RV' OR $fila.ind_estado eq 'CO' OR $fila.ind_estado eq 'CE' OR $fila.ind_estado eq 'AN'*}