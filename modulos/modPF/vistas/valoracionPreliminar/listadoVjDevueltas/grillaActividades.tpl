<style type="text/css">
    {literal}
        .fondo-fase:hover {
            color: #fdfdfd;
            background-color: #1D8888 !important;
        }
        tr.fondo-Subtotal,
        tr.fondo-Subtotal:hover {
            background-color: #bcd  !important;
        }
        tr.fondo-totales,
        tr.fondo-totales:hover {
            font-weight: bold;
            background-color: #90a4ae  !important;
        }
        .table thead>tr>th.vert-align{
            font-weight: bold; vertical-align: middle;
        }
    {/literal}
</style>
<input type="hidden" id="activDisponibles" value="{$activDisponibles}"/>
<table id="datatablePf" class="table table-condensed table-hover" width="100%">
    <thead>
    <tr>
        <th class="text-center vert-align">Opc.</th>
        <th class="text-center vert-align">Actividad</th>
        <th class="text-center vert-align" style="width:5%">Días</th>
        <th class="text-center vert-align">Inicio</th>
        <th class="text-center vert-align">Fin</th>
        <th class="text-center vert-align">Prorr.</th>
        <th class="text-center vert-align">Inicio Real</th>
        <th class="text-center vert-align">Fin Real</th>
        <th class="text-center vert-align">No Afe.</th>
    </tr>
    </thead>
    <tbody id="tbody_actividades">
        {if $listaActividades|count > 0}
            {foreach key=nombre_fase item=fases from=$listaActividades}
                <tr id="tr_fase" class="style-primary fondo-fase">
                    <td></td>
                    <td>{$nombre_fase}:</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                {foreach item=fila from=$fases}
                    {if $fila.ind_afecto_plan eq 'S'}
                        {$SubTotal_dias=$SubTotal_dias+$fila.num_dias_duracion_actividad}
                        {$SubTotal_diasProrroga=$SubTotal_diasProrroga+$fila.num_dias_prorroga_actividad}
                        {$afecta_plan='S'}
                        {$no_afecta_plan=''}
                    {else}
                        {$afecta_plan='N'}
                        {$no_afecta_plan='md md-check'}
                        {$totalDiasNoAfecta=$totalDiasNoAfecta+$fila.num_dias_duracion_actividad}
                    {/if}
                    <tr id="tr_actividad_{$fila.pk_num_actividad}">
                        <td align="center">
                            {if $estado_planificacion=='PR' OR $estado_planificacion=='DV'}
                                <a id="fila_" href="#" button class="btn btn-xs btn-danger" title="Click para Quitar" alt="Click para Quitar" onclick="metQuitarActividad({$fila.pk_num_actividad}{','}{$contador})">
                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                    </button>
                                </a>
                            {else}
                                <button class="btn btn-xs" disabled><i class="md md-delete" style="color: #ffffff;"></i></button>
                            {/if}
                        </td>
                        <td id="td_descActividad_{$fila.pk_num_actividad}">
                            <span id="spn_descActividad_{$fila.pk_num_actividad}">{$fila.txt_descripcion_actividad}</span>
                            <input type="hidden" id="hdd_dataActividad{$contador}" name="form[txt][hdd_dataActividad][]" value="{$fila.pk_num_actividad}{'N'}{$fila.num_dias_duracion_actividad}{'N'}{$fila.fec_inicio_actividad}{'N'}{$fila.fec_culmina_actividad}{'N'}{$fila.fec_inicio_real_actividad}{'N'}{$fila.fec_culmina_real_actividad}"/>
                        </td>
                        <td id="afecta_plan_{$afecta_plan}" align="center">
                            {if $apcion_modal!='Ver'}
                                <input type="number" class="form-control text-center" id="txt_duracion_actividad{$contador}" name="txt_duracion_actividad[]" size="2" min="1" max="99" value="{$fila.num_dias_duracion_actividad}" onclick="appf.metSeleccionaCampo('txt_duracion_actividad{$contador}')" onkeypress="return appf.metValidaCampNum(event)" onkeyup="metRecalcularActividades({$contador})" onchange="metRecalcularActividades({$contador})" />
                            {else}
                                {$fila.num_dias_duracion_actividad}
                            {/if}
                        </td>
                        <td id="td_fecha_inicio{$contador}" class="text-center">{$fila.fec_inicio_actividad}</td>
                        <td id="td_fecha_fin{$contador}" class="text-center">{$fila.fec_culmina_actividad}</td>
                        <td class="text-center">{$fila.num_dias_prorroga_actividad}</td>
                        <td id="td_fecha_inicio_real{$contador}" class="text-center">{$fila.fec_inicio_real_actividad}</td>
                        <td id="td_fecha_fin_real{$contador}" class="text-center">{$fila.fec_culmina_real_actividad}</td>
                        <td id="td_no_afecta{$contador}" class="text-center"><i class="{$no_afecta_plan}"></i></td>
                    </tr>
                    {$contador=$contador+1}
                    {$fecha_fin_plan="'{$fila.fec_culmina_real_actividad}'"}
                {/foreach}
                <tr id="tr_subtotal_" class="fondo-Subtotal">
                    <td align="center"></td>
                    <td id="td_1">Total dias Fase:</td>
                    <td id="td_Subtotal{$ifilasubtotal++}" name="td_Subtotal" class="text-center">{$SubTotal_dias}</td>
                    <td></td>
                    <td></td>
                    <td class="text-center">{$SubTotal_diasProrroga}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                {$Total_dias=$Total_dias+$SubTotal_dias}
                {$Totaldias_prorroga=$Totaldias_prorroga+$SubTotal_diasProrroga}
                {$SubTotal_dias=0}
                {$SubTotal_diasProrroga=0}
            {/foreach}
            <tr id="tr_total_" class="fondo-totales">
                <td align="center"></td>
                <td id="td_1">Total dias duración afecta:</td>
                <td id="td_total" class="text-center">{$Total_dias}</td>
                <td></td>
                <td></td>
                <td class="text-center">{$Totaldias_prorroga}</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            {$totalGeneralDias=$Total_dias+$totalDiasNoAfecta+$Totaldias_prorroga}
        {/if}
    </tbody>
</table>
<script type="text/javascript">
    $('#datatablePf_info').html('Total Actividades:'+{$contador});
    $('#lbl_fecha_fin_plan').html({$fecha_fin_plan});
    $('#lbl_duracion_afectacion').html({$Total_dias});
    $('#lbl_duracion_no_afectacion').html({$totalDiasNoAfecta});
    {*$('#lbl_prorroga').html({$Totaldias_prorroga});*}
    $('#lbl_duracionTotal').html({$totalGeneralDias});
    if($('#activDisponibles').val()){
        $('#btn_actividadDisp').show();
    }else{
        $('#btn_actividadDisp').hide();
    }
</script>