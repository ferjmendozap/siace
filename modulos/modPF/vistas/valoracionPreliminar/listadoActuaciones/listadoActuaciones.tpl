<style type="text/css">
    {literal}
    .texto-size{font-size: 12px}
    {/literal}
</style>
<section class="style-default-bright">
    <div class="col-lg-12">
        <div class="section-header"><h2 class="text-primary">&nbsp;Listar Actuaciones Fiscales</h2></div>
    </div>
    <div class="section-body">
        <form id="form1" class="form-horizontal" role="form" method="post">
            <div class="row">
                <div class="col-sm-1 text-right">
                    <label for="text_year_fiscal" class="control-label">Año fiscal:</label>
                </div>
                <div class="col-sm-1">
                    <div class="form-group" id="cbox_yearplanError">
                        <select id="cbox_yearplan" name="form[int][cbox_yearplan]" class="form-control texto-size" style="height: 27px;">
                            {if $listayears|count > 0}
                                {foreach item=fila from=$listayears}
                                    {if $fila eq $year_actual}
                                        <option value="{$fila}" selected>{$fila}</option>
                                    {else}
                                        <option value="{$fila}">{$fila}</option>
                                    {/if}
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                </div>
                <div class="col-sm-1 text-right">
                    <label for="text_year_fiscal" class="control-label">Fecha Reg.:</label>
                </div>
                <div class="col-sm-1">
                    <div class="input-group date" id="txt_fechareg1Error" >
                        <div class="input-group-content">
                            <input id="txt_fechareg1" type="text" name="form[txt][txt_fechareg1]" class="form-control texto-size" placeholder="Fecha desde">
                        </div>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="input-group date" id="txt_fechareg2Error" >
                        <div class="input-group-content">
                            <input id="txt_fechareg2" type="text" name="form[txt][txt_fechareg2]" class="form-control texto-size" placeholder="Fecha hasta">
                        </div>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
            </div>

            <div class="row">
                <label class="col-xs-1 text-right" for="cbox_dependencia" class="control-label" style="margin-top: 10px;">Dependencia:</label>
                <div class="col-sm-5">
                    <div class="form-group" id="cbox_dependenciaError">
                        <select id="cbox_dependencia" name="form[int][cbox_dependencia]" class="form-control texto-size" style="height: 27px;">
                            <option value="">Seleccione..</option>
                            {if $listaDependencias|count > 0}
                                {foreach item=fila from=$listaDependencias}
                                    <option value="{$fila.id_dependencia}">{$fila.nombre_dependencia}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-1 text-right control-label" for="cbox_centro_costo">Centro Costo:</label>
                <div class="col-sm-5">
                    <div class="form-group" id="cbox_centro_costoError">
                        <select id="cbox_centro_costo" name="form[int][cbox_centro_costo]" class="form-control texto-size" style="height: 27px;"></select>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="control-label col-sm-1 text-right" for="cbox_ente">Ente:</label>
                <div class="col-sm-4">
                    <div class="form-group" id="cbox_enteError">
                        <select id="cbox_ente" name="form[int][cbox_ente]" class="form-control texto-size" style="height: 27px;">
                            <option value="">Seleccione..</option>
                            {if $entesExternos|count > 0}
                                {foreach item=fila from=$entesExternos}
                                    <option value="{$fila.id_ente}">{$fila.nombre_ente}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-1 text-right">
                    <label for="txt_objetivo" class="control-label" style="margin-top: 10px;">Objetivo:</label>
                </div>
                <div class="col-sm-8">
                    <input id="txt_objetivo_plan" type="text" name="form[txt][txt_objetivo_plan]" class="form-control texto-size">
                </div>
            </div>
            <input type="hidden" id="total_regs" name="form[int][total_regs]" value="{$total_regs}" />
            <input type="hidden" id="rango_buscar" name="form[int][rango_buscar]" value="{$rango_buscar}" />
            <input type="hidden" id="total_rcset" name="form[int][total_rcset]" value="{$total_rcset}" />
        </form>
        <div class="row">
            <div class="col-sm-12">
                <div align="center">
                    <button id="btn_Limpiar" class="btn btn-raised btn-default " title="Click para limpiar" alt="Click para limpiar">
                        Limpiar
                    </button>
                    {if in_array('PF-01-02-01-01-L',$_Parametros.perfil)}
                        <button id="btn_Buscar" class="btn btn-raised btn-primary" title="Click para buscar" alt="Click para buscar">
                            Buscar
                        </button>
                    {/if}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div id="resultadoConsulta"></div>
            </div>
        </div>
    </div>
    <div class="clearfix visible-sm"></div>
</section>
{* SECCIÓN DE JAVASCRIPT *}
<script type="text/javascript">
    var metMontaPlanificacion="";
    var msjBuscar=true; //controla el mensaje de resultado buscar actuaciones versus generar valoración.
    $(document).ready(function () {
        $('#ContenidoModal').html("");
        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "dd-mm-yyyy",language: 'es'});
        /**
         * Monta las dependencias centros de costo
         */
        function metLlenaComboBoxCcosto(){
            $('#cbox_centro_costo option').remove();
            if($('#cbox_dependencia').val()){
                var url='{$_Parametros.url}modPF/valoracionPreliminar/listadoActuacionesCONTROL/DependenciaCentroCostosMET';
                $.post(url, { idDependencia:$('#cbox_dependencia').val() }, function (rcrset) {
                    $.each(rcrset.filas, function(indice,fila){
                        if(indice==0){
                            $('#cbox_centro_costo').append('<option value="" selected>Seleccione...</option>');
                        }
                        $('#cbox_centro_costo').append('<option value="' + fila.pk_num_centro_costo + '">' + fila.ind_descripcion_centro_costo + '</option>');
                    });
                }, 'json');
            }
        }
        $('#cbox_dependencia').change(function (){
            metLlenaComboBoxCcosto(); /*Monta las dependencias de centro costo*/
        });
        /**
         * Monta de forma recursiva las dependencias de un ente al ser seleccionado
         */
        $('#cbox_ente').change(function (){
            var nombre_opcion=$("#cbox_ente").find("option[value='"+$("#cbox_ente").val()+"']").text();
            var url='{$_Parametros.url}modPF/valoracionPreliminar/listadoActuacionesCONTROL/ListaEntesMET';
            if($("#cbox_ente").val()) {
                var idEnte=$("#cbox_ente").val();
                $('#cbox_ente option').remove();
                $.post(url, { idEnte:idEnte }, function (rcrset) {
                    if(rcrset.filas){
                        $.each(rcrset.filas, function(indice,fila){
                            $tultic='title="'+fila.toltick+'" alt="' +  fila.toltick  +'"';
                            if(indice==0){
                                if(nombre_opcion=="-----Nivel Anterior-----"){
                                    $('#cbox_ente').append('<option value="">Seleccione</option>');
                                    $('#cbox_ente').append('<option value="'+fila.id_ente+'"'+$tultic+'>'+fila.nombre_ente+'</option>');
                                }else{
                                    $('#cbox_ente').append('<option value="'+fila.id_ente+'"'+$tultic+'>'+fila.nombre_ente+'</option>');
                                }
                                $id_nivel_ant=fila.idente_padre;
                            }else{
                                $('#cbox_ente').append('<option value="'+fila.id_ente+'"'+$tultic+'>'+fila.nombre_ente+'</option>');
                            }
                        });
                    }
                    if(idEnte>0){
                        if(!$id_nivel_ant){ $id_nivel_ant=0; }
                        $('#cbox_ente').append('<option value="'+$id_nivel_ant+'">-----Nivel Anterior-----</option>');
                    }else{
                        $("#cbox_ente").find("option[value='0']").remove();
                    }
                }, 'json');
            }
        });
        /**
         * Se buscan las actuaciones fiscales
         */
        $('#btn_Buscar').click(function(){
            $('#resultadoConsulta').html('');
            var url='{$_Parametros.url}modPF/valoracionPreliminar/listadoActuacionesCONTROL/ListarActuacionesMET';
            $.post(url, $("#form1").serialize(), function (data) {
                if(data){
                    $('#resultadoConsulta').html(data);
                }else if(msjBuscar){
                    swal("Información del Sistema", "No hay resultados", "error");
                }
                msjBuscar=true;
            });
        });

        $('#btn_Buscar').click();
        /**
         * Monta los datos de la planificación al hacer click en su respectivo icon
         */
        metMontaPlanificacion=function(idActuacion){
            $('#ContenidoModal').html('');
            var $url = '{$_Parametros.url}modPF/valoracionPreliminar/listadoActuacionesCONTROL/MontaActuacionMET';
            $.post($url, { idActuacion:idActuacion, idPlanificacion:0, apcion_modal:'Actualizar' }, function (dato) {
                if(dato){
                    $('#formModalLabel').html('<i class="fa fa-edit"></i> Generar Planificación de Valoración Preliminar');
                    $('#ContenidoModal').html(dato);
                }else{
                    $('#formModalLabel').html('');
                    $('#ContenidoModal').html('');
                    $('#cerrarModal').click();
                    swal("Información del Sistema", "¡¡Disculpe!! La Planificación no se pudo montar. Intente de nuevo", "error");
                }
            });
        }
        $('#btn_Limpiar').click(function(){
            $('#btn_Buscar').show();
            $('#cbox_estadoplan').val(''); $('#txt_fechareg1').val(''); $('#txt_fechareg2').val('');
            $('#txt_objetivo_plan').val('');
            $('#cbox_dependencia').val('');$('#cbox_centro_costo').html('');
            $('#cbox_ente').html('');
            $('#cbox_ente').html('<option value="0" selected>-----Nivel Anterior-----</option>');
            $('#cbox_ente').change();
            $('#resultadoConsulta').html('');
        });
    });
</script>