<style type="text/css">
    {literal}
        .fondo-fase:hover {
            color: #fdfdfd;
            background-color: #1D8888 !important;
        }
        tr.fondo-Subtotal,
        tr.fondo-Subtotal:hover {
            background-color: #bcd  !important;
        }
        tr.fondo-totales,
        tr.fondo-totales:hover {
            font-weight: bold;
            background-color: #90a4ae  !important;
        }
        .table thead>tr>th.vert-align{
            font-weight: bold; vertical-align: middle;
        }
    {/literal}
</style>
<table id="datatablePf" class="table table-condensed table-hover" width="100%">
    <thead>
    <tr>
        <th class="text-center vert-align">Estatus</th>
        <th class="text-center vert-align">Actividad</th>
        <th class="text-center vert-align" style="width:5%">Días</th>
        <th class="text-center vert-align">Inicio</th>
        <th class="text-center vert-align">Fin</th>
        <th class="text-center vert-align">Prorr.</th>
        <th class="text-center vert-align">Inicio Real</th>
        <th class="text-center vert-align">Fin Real</th>
        <th class="text-center vert-align">No Afe.</th>
    </tr>
    </thead>
    <tbody>
        {if $listaActividades|count > 0}
            {foreach key=nombre_fase item=fases from=$listaActividades}
                <tr id="tr_fase" class="style-primary fondo-fase">
                    <td></td>
                    <td>{$nombre_fase}:</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                {foreach item=fila from=$fases}
                    {if $fila.ind_afecto_plan eq 'S'}
                        {$SubTotal_dias=$SubTotal_dias+$fila.num_dias_duracion_actividad}
                        {$SubTotal_diasProrroga=$SubTotal_diasProrroga+$fila.num_dias_prorroga_actividad}
                        {$afecta_plan='S'}
                        {$no_afecta_plan=''}
                    {else}
                        {$afecta_plan='N'}
                        {$no_afecta_plan='md md-check'}
                        {$totalDiasNoAfecta=$totalDiasNoAfecta+$fila.num_dias_duracion_actividad}
                    {/if}
                    <tr id="tr_actividad_{$fila.pk_num_actividad}">
                        <td align="center">
                            {if $fila.ind_estado_actividad=='PR'}
                                <i class="md md-lens" style="color: red;"></i>
                            {elseif $fila.ind_estado_actividad=='EJ'}
                                <i class="md md-lens" style="color: green;"></i>
                            {elseif $fila.ind_estado_actividad=='PE'}
                                <i class="md md-lens" style="color: red;"></i>
                            {elseif $fila.ind_estado_actividad=='TE'}
                                <i class="md md-done all" style="color: green;"></i>
                            {/if}
                        </td>
                        <td>
                            <span id="spn_descActividad_{$fila.pk_num_actividad}">{$fila.txt_descripcion_actividad}</span>
                        </td>
                        <td class="text-center">{$fila.num_dias_duracion_actividad}</td>
                        <td class="text-center">{$fila.fec_inicio_actividad}</td>
                        <td class="text-center">{$fila.fec_culmina_actividad}</td>
                        <td class="text-center">{$fila.num_dias_prorroga_actividad}</td>
                        <td class="text-center">{$fila.fec_inicio_real_actividad}</td>
                        <td class="text-center">{$fila.fec_culmina_real_actividad}</td>
                        <td class="text-center"><i class="{$no_afecta_plan}"></i></td>
                    </tr>
                    {$contador=$contador+1}
                    {$fecha_fin_plan="'{$fila.fec_culmina_real_actividad}'"}
                {/foreach}
                <tr class="fondo-Subtotal">
                    <td align="center"></td>
                    <td>Total dias Fase:</td>
                    <td class="text-center">{$SubTotal_dias}</td>
                    <td></td>
                    <td></td>
                    <td class="text-center">{$SubTotal_diasProrroga}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                {$Total_dias=$Total_dias+$SubTotal_dias}
                {$Totaldias_prorroga=$Totaldias_prorroga+$SubTotal_diasProrroga}
                {$SubTotal_dias=0}
                {$SubTotal_diasProrroga=0}
            {/foreach}
            <tr class="fondo-totales">
                <td align="center"></td>
                <td>Total dias duración afecta:</td>
                <td class="text-center">{$Total_dias}</td>
                <td></td>
                <td></td>
                <td class="text-center">{$Totaldias_prorroga}</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            {$totalGeneralDias=$Total_dias+$totalDiasNoAfecta+$Totaldias_prorroga}
        {/if}
    </tbody>
    <tfoot>
        <tr>
            <td colspan="9">
                <div>
                    <i class="md md-lens" style="color: green;"></i> Actividad en Ejecución.&nbsp;&nbsp;
                    <i class="md md-lens" style="color: red;"></i> Actividad por Ejecutar.&nbsp;&nbsp;
                    <i class="md md-done all" style="color: green;"></i> Actividad terminada.
                </div>
            </td>
        </tr>
    </tfoot>
</table>
<script type="text/javascript">
    $('#datatablePf_info').html('Total Actividades:'+{$contador});
    $('#lbl_fecha_fin_plan').html({$fecha_fin_plan});
    $('#lbl_duracion_afectacion').html({$Total_dias});
    $('#lbl_duracion_no_afectacion').html({$totalDiasNoAfecta});
    {*$('#lbl_prorroga').html({$Totaldias_prorroga});*}
    $('#lbl_duracionTotal').html({$totalGeneralDias});
</script>