<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Procesos</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div id="resultadoConsulta"></div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    var metMontaProceso="",metEliminaProceso="",metCrear="",metBuscar="";
    $(document).ready(function() {
        var url='{$_Parametros.url}modPF/maestros/procesosFiscalCONTROL/';
        metCrear=function(){
            $('#formModalLabel').html('<i class="icm icm-cog3"></i> Crear Proceso');
            $.post(url+'crearModificarMET',{ idProceso:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        };
        /**
         * Lista los procesos
         */
        metBuscar=function(){
            $.post(url+'ListarProcesosMET', '', function (data) {
                if(data){
                    $('#resultadoConsulta').html(data);
                }else{
                    swal("Información del Sistema", "No hay resultados", "error");
                }
            });
        };
        /**
         * Busca y monta los datos en el form modal para modificar.
         * @param idProceso
         */
        metMontaProceso=function(idProceso){
            $('#formModalLabel').html('');$('#ContenidoModal').html('');
            $.post(url+'crearModificarMET', { idProceso:idProceso }, function (data) {
                if(data){
                    $('#formModalLabel').html('<i class="fa fa-edit"></i> Modificar proceso');
                    $('#ContenidoModal').html(data);
                }else{
                    $('#formModalLabel').html('');$('#ContenidoModal').html('');$('#cerrarModal').click();
                    swal("Información del Sistema", "Disculpe, no se pudo montar los datos. Intente nuevamente", "error");
                }
            });
        }
        /**
         * Eliminar un registro al clicar en el botón respectivo de la grilla
         * @param idCargo
         */
        metEliminaProceso=function(idProceso){
            swal({
                title: "Confirmación de proceso",
                text: "Se va a eliminar el proceso '"+$('#td_'+idProceso).html()+"'. ¿Estas de acuerdo? ",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false
            }, function (isConfirm) {
                if(isConfirm) {
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post(url+'EliminarProcesoMET', { idProceso:idProceso }, function (dato) {
                        if (dato.reg_afectado) {
                            swal("Registro Eliminado", dato.mensaje, "success");
                            metBuscar();
                        } else {
                            swal("¡Atención!", dato.mensaje, "error");
                        }
                    }, 'json');
                }else{
                    swal("¡Cancelado!", "Proceso cancelado", "error");
                }
            });
        }
        metBuscar();
    });
</script>