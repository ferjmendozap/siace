<form action="{$_Parametros.url}modPF/maestros/procesosFiscalCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$cadCantReg}" name="cadCantReg"/>
        <input type="hidden" id="idProceso" name="form[int][idProceso]" value="{if isset($idProceso)}{$idProceso}{else}0{/if}" />
        <div class="row">
            <div class="col-sm-5">
                <div class="form-group floating-label" id="cod_procesoError">
                    <input id="cod_proceso" name="form[txt][cod_proceso]" type="text" disabled class="form-control" value="{if isset($formDB.cod_proceso)}{$formDB.cod_proceso}{/if}">
                    <label for="cod_proceso"><i class="fa fa-code"></i> Cod Proceso</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group floating-label" id="txt_descripcion_procesoError">
                    <input id="txt_descripcion_proceso" name="form[txt][txt_descripcion_proceso]" type="text" class="form-control" value="{if isset($formDB.txt_descripcion_proceso)}{$formDB.txt_descripcion_proceso}{/if}">
                    <label for="txt_descripcion_proceso"><i class="icm icm-cog3"></i> Nombre del proceso</label>
                </div>
                <div class="checkbox checkbox-styled">
                    <label>
                        <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked{/if} value="1" name="form[int][num_estatus]">
                        <span>Estatus</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group floating-label">
                    <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                    <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label">
                    <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                    <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal" title="Click para cancelar" alt="Click para cancelar">
            <span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar
        </button>
        {if in_array('PF-01-05-01-01-02-M',$_Parametros.perfil)}
            <button id="btn_guardar" type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" title="Click para guardar" alt="Click para guardar">
                <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Guardar
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        $("#formAjax").submit(function(){ return false; });
        var appf=new AppfFunciones();
        $('#modalAncho').css("width","35%");
        /**
         * Ingresa ó actualiza un registro
         */
        $('#btn_guardar').click(function(){
            appf.metDepuraText('txt_descripcion_proceso');
            var msj="";
            if($("#idProceso").val()==0){
                msj="Se va a ingresar un nuevo Proceso";
            }else{
                msj="Se va a actualizar el registro actual";
                msj+=". ¿Estas de acuerdo?";
            }
            swal({
                title: "Confirmación de proceso",
                text: msj,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false
            }, function () {
                swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (data) {
                    if (data.status == 'error') {
                        appf.metActivaError(data);
                        swal("¡Atención!", "Los campos con una x en rojo no debe quedar vacíos", "error");
                    } else if (data.status == 'errorSQL') {
                        swal("¡Atención!", data.mensaje, "error");
                    } else if (data.status == 'nuevo') {
                        swal("Registro Ingresado!", data.mensaje, "success");
                        metBuscar(); /*Refresca la grilla*/
                        $('#ContenidoModal').html(''); $('#cerrarModal').click();
                    } else if (data.status == 'modificar') {
                        metBuscar(); /*Refresca la grilla*/
                        swal("Registro Modificado!", data.mensaje, "success");
                        $('#ContenidoModal').html(''); $('#cerrarModal').click();
                    }
                }, 'json');
            });
        });
    });
</script>