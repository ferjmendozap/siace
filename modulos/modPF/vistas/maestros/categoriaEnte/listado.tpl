<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Categorías de Entes Externos</h2>
    </div>
    <div class="section-body">
        <form id="form1" class="form-horizontal" role="form" method="post">
            <div class="row">
                <div class="col-sm-1 text-right">
                    <label for="cbox_tipoente" class="control-label">Tipos ente:</label>
                </div>
                <div class="col-sm-2">
                    <div class="form-group" id="cboxp_tipoenteError">
                        <select id="cboxp_tipoente" name="form[int][cboxp_tipoente]" class="form-control texto-size">
                            {if $listadoTipoEnte|count > 0}
                                <option value="">Todo</option>
                                {foreach item=fila from=$listadoTipoEnte}
                                    <option value="{$fila.idtipo_ente}">{$fila.nombretipo_ente}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-sm-12">
                <div id="resultadoConsulta"></div>
            </div>
        </div>
    </div>
    <div class="clearfix visible-sm"></div>
</section>
<style type="text/css">
    {literal}
    .texto-size{font-size: 12px;}
    {/literal}
</style>
<script type="text/javascript">
    var metMontaCategoria="",metEliminaCategoria="",metCrear="",metBuscar="";
    $(document).ready(function() {
        var url='{$_Parametros.url}modPF/maestros/categoriaEnteCONTROL/';
        /**
         * Listar las categorías de entes
         */
        metBuscar=function(){
            $.post(url+'ListarCategoriaMET', $("#form1").serialize(), function (data) {
                $('#resultadoConsulta').html(data);
            });
        };
        $('#cboxp_tipoente').change(function(){
            metBuscar();
        });
        metCrear=function(){
            $('#formModalLabel').html("<i class='icm icm-cog3'></i> Crear Categoría");$('#ContenidoModal').html('');
            $.post(url+'crearModificarMET',{ idCategoria:0 },function(datos){
                $('#ContenidoModal').html(datos);
            });
        };
        /**
         * Busca y monta los datos en el form modal para modificar.
         * @param idCategoria
         */
        metMontaCategoria=function(idCategoria){
            $('#formModalLabel').html('');$('#ContenidoModal').html('');
            $.post(url+'MontaCategoriaMET', { idCategoria:idCategoria }, function (data) {
                if(data){
                    $('#formModalLabel').html('<i class="fa fa-edit"></i> Modificar categoría');
                    $('#ContenidoModal').html(data);
                }else{
                    swal("Información del Sistema", "No hay resultados", "error");
                }
            });
        }
        /**
         * Ejecuta el proceso de eliminar un registro
         * @param idCargo
         */
        metEliminaCategoria=function(idCategoria){
            swal({
                title: "Confirmación de proceso",
                text: "Se va a eliminar la categoría '"+$('#td_'+idCategoria).html()+"'. ¿Estas de acuerdo? ",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false
            }, function (isConfirm) {
                if(isConfirm) {
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post(url+'EliminarCategiriaMET', { idCategoria:idCategoria }, function (dato) {
                        if (dato.reg_afectado) {
                            swal("Registro Eliminado", dato.mensaje, "success");
                            metBuscar();
                        } else {
                            swal("¡Atención!", dato.mensaje, "error");
                        }
                    }, 'json');
                }else{
                    swal("¡Cancelado!", "Proceso cancelado", "error");
                }
            });
        }
        metBuscar();
    });
</script>