<div class="section-body">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Cedula</th>
                            <th>Nombres</th>
                            <th>Estatus</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        {if $listado|count > 0}
                            {foreach item=fila from=$listado}
                                <tr id="id_persona{$fila.id_persona}">
                                    <td><label>{number_format($fila.cedula_persona,0,',','.')}</label></td>
                                    <td><label>{$fila.nombre_persona}</label></td>
                                    <td><i class="{if $fila.estatus_persona==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                    <td align="center">
                                        {if in_array('PF-01-05-02-03-02-M',$_Parametros.perfil)}
                                            <button class="modificar btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                onclick="metMontaRespresentante({$fila.id_persona})"
                                                title="Click para modificar" alt="Click para modifica"">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {else}
                                            <button class="btn btn-raised btn-xs btn-default" title="No tienes permiso para esta opción" alt="No tienes permiso para esta opción">
                                                <i class="glyphicon glyphicon-edit"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        {/if}
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="4" align="center">
                            {if in_array('PF-01-05-02-03-01-N',$_Parametros.perfil)}
                                <a href="#" id="btn_nuevo" class="button logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal"
                                   data-keyboard="false"data-backdrop="static"title="Click para ingresar un nuevo representante" alt="Click para ingresar un nuevo representante"
                                   onclick="metCrear()">
                                    <i class="md md-create"></i>&nbsp;Nuevo Representante
                                </a>
                            {else}
                                <button class="button btn btn-raised btn-default" title="No tienes permiso para esta opción" alt="No tienes permiso para esta opción">
                                    <i class="md md-create"></i>&nbsp;Nuevo Representante
                                </button>
                            {/if}
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>