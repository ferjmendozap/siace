<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Miscelaneo - Listado</h2>
    </div>
    <div id="resultadoConsulta" class="section-body"></div>
</section>
<script type="text/javascript">
    var metMontaMiscelaneos="",metCrear="",metBuscar="";
    $(document).ready(function() {
        var url='{$_Parametros.url}modPF/maestros/miscelaneoCONTROL/';
        /**
         * Lista los misceláneos maestros
         */
        metBuscar=function(){
            $.post(url+'ListarMiscelaneosMET', '', function (data) {
                $('#resultadoConsulta').html(data);
            });
        };
        metBuscar();
        /**
         * Monta el form modal para el ingreso de un miscelaneo y sus detalles
         */
        metCrear=function(){
            $('#ContenidoModal').html("");
            $('#formModalLabel').html('<i class="icm icm-cog3"></i> Crear Responsable de Entes');
            $.post(url+'crearModificarMET',{ idMiscelaneo:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        };
        /**
         * Monta los datos en el form modal para la actualización
         * @param idResponsable
         */
        metMontaMiscelaneos=function(idMiscelaneo){
            $('#formModalLabel').html('');$('#ContenidoModal').html('');
            $.post(url+'crearModificarMET', { idMiscelaneo:idMiscelaneo }, function (data) {
                if(data){
                    $('#formModalLabel').html('<i class="fa fa-edit"></i> Actualizar Responsables de Entes');
                    $('#ContenidoModal').html(data);
                }else{
                    $('#formModalLabel').html('');$('#ContenidoModal').html('');$('#cerrarModal').click();
                    swal("Información del Sistema", "Disculpe, no se pudo montar los datos. Intente nuevamente", "error");
                }
            });
        }
    });
</script>