<form action="{$_Parametros.url}modPF/maestros/miscelaneoCONTROL/crearModificarMET" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" id="idMiscelaneo" name="form[int][idMiscelaneo]" value="{$idMiscelaneo}" />

        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-7">
                    <div class="form-group floating-label" id="txt_nombreMiscMaestroError">
                        <input id="txt_nombreMiscMaestro" name="form[txt][txt_nombreMiscMaestro]" type="text" class="form-control" value="{if isset($formDB.ind_nombre_maestro)}{$formDB.ind_nombre_maestro}{/if}">
                        <label for="txt_nombreMiscMaestro"><i class="md md-border-color"></i> Nombre del miscelaneo maestro</label>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group floating-label" id="txt_codMiscMaestroError">
                        <input id="txt_codMiscMaestro" name="form[txt][txt_codMiscMaestro]" maxlength="10" type="text" class="form-control lMayuscula" value="{if isset($formDB.cod_maestro)}{$formDB.cod_maestro}{/if}" {if isset($formDB.cod_maestro)} readonly {/if}>
                        <label for="txt_codMiscMaestro"><i class="fa fa-barcode"></i> Cod. maestro</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="txt_DescMiscMaestroError">
                        <input id="txt_DescMiscMaestro" name="form[txt][txt_DescMiscMaestro]" type="text" class="form-control" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}">
                        <label for="txt_DescMiscMaestro"><i class="md md-insert-comment"></i> Descripción del Misceláneo</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input name="form[int][chk_estatusMaestro]" type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked{/if} value="1">
                            <span>Estatus</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
                <div class="card">
                    <div class="card-body no-padding">
                        <div class="table-responsive no-margin">
                            <div class="text-center">Detalle de Misceláneos</div>
                            <table id="contenidoTabla" class="table table-striped no-margin" style="width: 100%; display:block">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width: 1%;">#</th>
                                        <th style="width: 20%;">Código</th>
                                        <th style="width: 60%;">Nombre opción</th>
                                        <th style="width: 19%;">&nbsp;&nbsp;Estatus&nbsp;&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody style="height: 250px; width: 100%; display:inline-block; overflow: auto">
                                    {if isset($formDBDet)}
                                        {foreach item=fila from=$formDBDet}
                                            <tr>
                                                <input type="hidden" value="{$fila.pk_num_miscelaneo_detalle}" name="form[int][set_pknumMiscDetalle][{$n}]">
                                                <td class="text-right" style="width: 1%;vertical-align: middle;">
                                                    {$numero++}
                                                </td>
                                                <td style="width: 20%; vertical-align: middle;">
                                                    <div class="form-group floating-label" id="codsDetalle{$n}Error">
                                                        <input id="codsDetalle{$n}" name="form[alphaNum][codsDetalle][{$n}]" type="text" class="form-control lMayuscula" maxlength="4" value="{$fila.cod_detalle}" {if $formDB.cod_maestro=='PFCPEXT'}readonly{/if}>
                                                        <label for="codsDetalle{$n}"><i class="md md-insert-comment"></i>Código</label>
                                                    </div>
                                                </td>
                                                <td style="width: 60%; vertical-align: middle;">
                                                    <div class="form-group floating-label" id="nombresDetalle{$n}Error">
                                                        <input id="nombresDetalle{$n}" name="form[alphaNum][nombresDetalle][{$n}]" type="text" class="form-control" value="{$fila.ind_nombre_detalle}">
                                                        <label for="nombresDetalle{$n}"><i class="md md-insert-comment"></i> Nombre </label>
                                                    </div>
                                                </td>
                                                <td  class="text-center" style="width: 19%; vertical-align: middle;">
                                                    <div class="checkbox checkbox-styled">
                                                        <label>
                                                            <input name="form[int][chksEstatusDet][{$n++}]" type="checkbox" {if $fila.num_estatus == 1}checked{/if} value="1">
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                        {/foreach}
                                    {/if}
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th align="center">
                                            <div class="col-sm-offset-4">
                                                <button type="button" id="nuevoCampo" class="btn btn-info ink-reaction btn-raised" title="Click para agregar un nuevo detalle en blanco" alt="Click para agregar un nuevo detalle en blanco">
                                                    <i class="md md-add"></i> Insertar Nuevo Detalle
                                                </button>
                                            </div>
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <div class="text-center">
            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal">Cancelar</button>
            <button type="button" id="btn_guardar" class="btn btn-primary btn-raised" title="Click para guardar"  alt="Click para guardar">
                <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Guardar
            </button>
        </div>
    </div>
</form>

<script type="text/javascript">
    var appf="";
    $(document).ready(function() {
        var app = new  AppFunciones();
        var appf=new AppfFunciones();
        $('#modalAncho').css("width", "90%"); $('#ContenidoModal').css("padding-top", "0");/*ancho de la Modal*/
        $('#contenidoTabla  tbody').on('keyup change', 'tr td .floating-label input' ,function (e) {
            var input = $(e.currentTarget);
            if ($.trim(input.val()) !== '') {
                input.addClass('dirty').removeClass('static');
            } else {
                input.removeClass('dirty').removeClass('static');
            }
        });
        $('#contenidoTabla').on('click', '.delete', function () {
            $(this).parent('td').parent('tr').remove();
        });
        appf.metConvertMayuscula($('.lMayuscula'));
        $("#nuevoCampo").click(function() {
            var idtabla= $("#contenidoTabla");
            var tr= $("#contenidoTabla > tbody > tr");
            var numTr=tr.length - 1;
            var nuevoTr=numTr+1;
            var numero=nuevoTr+1;
            var nombreObciones='<div class="form-group" id="nombresDetalle'+nuevoTr+'Error">'+
                                    '<input type="text" class="form-control" value="" name="form[alphaNum][nombresDetalle]['+nuevoTr+']" id="nombresDetalle'+nuevoTr+'">'+
                                    '<label for="nombresDetalle'+nuevoTr+'"><i class="md md-insert-comment"></i> Nombre </label>'+
                                '</div>';
            var codObciones='<div class="form-group" id="codsDetalle'+nuevoTr+'Error">'+
                                    '<input id="codsDetalle'+nuevoTr+'" name="form[alphaNum][codsDetalle]['+nuevoTr+']" type="text" class="form-control" maxlength="4" >'+
                                    '<label for="codsDetalle'+nuevoTr+'"><i class="md md-insert-comment"></i>Código</label>'+
                                '</div>';

            var checkOpciones='<div class="checkbox checkbox-styled">'+
                                '<label><input type="checkbox" value="1" name="form[int][chksEstatusDet]['+nuevoTr+']" ><span></span></label>'+
                                '</div>';
            idtabla.append('<tr>'+
                                '<td class="text-right" style="width: 1%;vertical-align: middle;">'+numero+'</td>'+
                                '<td style="width: 20%; vertical-align: middle;">'+codObciones+'</td>'+
                                '<td style="width: 60%; vertical-align: middle;">'+nombreObciones+'</td>'+
                                '<td class="text-center" style="width: 19%; vertical-align: middle;">'+checkOpciones+'<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" title="Click para quitar" alt="Click para quitar"><i class="md md-delete"></i></button></td>'+
                            '</tr>');
            if($('#txt_codMiscMaestro').val()=='PFCPEXT'){ //Entra, cuando se trata de Cargos de personal externo para colocar el número como código.
                $('#codsDetalle'+nuevoTr).val(numero);
                $('#codsDetalle'+nuevoTr).attr({ readonly:true });
            }
        });
        /**
         * Ingrese ó actualiza un misceleneo maestro y sus detalles
         */
        $('#btn_guardar').click(function(){
            if(($("#contenidoTabla > tbody > tr").length==0)){
                swal({ title: "¡Atención!",text: "Falta ingresar al menos un detalle"});
                return;
            };
            var msj="";
            if($("#idMiscelaneo").val()==0){
                msj="Se va a ingresar los nuevos misceláneos";
            }else{
                msj="Se van a actualizar los datos actuales. ¿Estas de Acuerdo?";
            }
            swal({
                title: "Confirmación de proceso",
                text: msj,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if(isConfirm){
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (data) {
                        if (data.Error == 'error') {
                            app.metValidarError(data,"Los campos marcados con X en rojo son obligatorios");
                        } else if (data.Error == 'errorSql') {
                            swal("¡Atención!", data.mensaje, "error");
                        } else {
                            swal("Información del proceso", data.mensaje, "success");
                            $('#ContenidoModal').html(''); $('#cerrarModal').click();
                            metBuscar();
                        }
                    }, 'json');
                }else{
                    swal("¡Cancelado!", "Proceso cancelado", "error");
                }
            });
        });
    });
</script>