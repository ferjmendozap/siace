<div class="section-body">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Cod Maestro</th>
                        <th>Nombre</th>
                        <th>Descripcion</th>
                        <th>Estatus</th>
                        <th>Accion</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=fila from=$listadoMiscMaestro}
                        <tr id="tr_idMiscelaneo{$fila.pk_num_miscelaneo_maestro}">
                            <td><label>{$fila.cod_maestro}</label></td>
                            <td><label>{$fila.ind_nombre_maestro}</label></td>
                            <td><label>{$fila.ind_descripcion}</label></td>
                            <td>
                                <i class="{if $fila.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                            </td>
                            <td align="center">
                                {if in_array('PF-01-05-03-02-M',$_Parametros.perfil)}
                                    <button class="modificar btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                        onclick="metMontaMiscelaneos({$fila.pk_num_miscelaneo_maestro})"
                                        title="Click para modificar" alt="Click para modifica"">
                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                    </button>
                                {else}
                                    <button class="btn btn-raised btn-xs btn-default" title="No tienes permiso para esta opción" alt="No tienes permiso para esta opción">
                                        <i class="glyphicon glyphicon-edit"></i>
                                    </button>
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="5" class="text-center">
                            {if in_array('PF-01-05-03-01-N',$_Parametros.perfil)}
                                <a href="#" id="btn_nuevo" class="button logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal"
                                   data-keyboard="false"data-backdrop="static"title="Click para ingresar un nuevo responsable" alt="Click para ingresar un nuevo responsable"
                                   onclick="metCrear()">
                                    <i class="md md-create"></i>&nbsp;Nuevo Miscelaneo
                                </a>
                            {else}
                                <button class="button btn btn-raised btn-default" title="No tienes permiso para esta opción" alt="No tienes permiso para esta opción">
                                    <i class="md md-create"></i>&nbsp;Nuevo Responsable
                                </button>
                            {/if}
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>