<div class="section-body">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>Nombre Actividad</th>
                        <th>Afecta Plan</th>
                        <th>Estatus</th>
                        <th>Nombre Fase</th>
                        <th>Nombre Proceso</th>
                        <th>&nbsp;&nbsp;&nbsp;&nbsp;Accion&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=fila from=$listado}
                        <tr>
                            <td>{$fila.cod_actividad}</td>
                            <td id="td_{$fila.pk_num_actividad}">{$fila.txt_descripcion_actividad}</td>
                            <td><i class="{if $fila.ind_afecto_plan=='S'}md md-check{else}md md-not-interested{/if}"></i></td>
                            <td><i class="{if $fila.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i></td>
                            <td>{$fila.txt_descripcion_fase}</td>
                            <td>{$fila.txt_descripcion_proceso}</td>
                            <td align="center">
                                {if in_array('PF-01-05-01-03-02-M',$_Parametros.perfil)}
                                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                        onclick="metMontaActividad({$fila.pk_num_actividad})"
                                        title="Click para modificar" alt="Click para modifica">
                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                    </button>
                                {/if}
                                &nbsp;&nbsp;
                                {if in_array('PF-01-05-01-03-03-E',$_Parametros.perfil)}
                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                        onclick="metEliminaActividad({$fila.pk_num_actividad})"
                                        title="Click para eliminar" alt="Click para eliminar"">
                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                    </button>
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="6" align="center">
                                {if in_array('PF-01-05-01-03-01-N',$_Parametros.perfil)}
                                    <a href="#" id="btn_nuevo" class="button logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal"
                                       data-keyboard="false"data-backdrop="static" title="Click para crear una nueva actividad" alt="Click para crear una nueva actividad"
                                       onclick="metCrear()">
                                        <i class="md md-create"></i>&nbsp;Nueva Actividad
                                    </a>
                                {/if}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>