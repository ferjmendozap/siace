<form action="{$_Parametros.url}modPF/maestros/actividadFiscalCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input id="idActividad" name="form[int][idActividad]" type="hidden" value="{if isset($idActividad) }{$idActividad}{else}0{/if}" />
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group floating-label" id="cod_actividadError">
                    <input type="text" disabled class="form-control" value="{if isset($formDB.cod_actividad)}{$formDB.cod_actividad}{/if}" name="form[txt][cod_actividad]" id="cod_actividad">
                    <label for="cod_actividad"><i class="fa fa-code"></i> Cod. Actividad</label>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="form-group floating-label" id="pk_num_procesoError">
                    <select name="form[int][pk_num_proceso]" class="form-control select2" id="pk_num_proceso" data-placeholder="Seleccione el proceso" {if isset($idActividad)}disabled{/if}>
                        <option value=""></option>
                        {foreach item=proceso from=$listaProceso}
                            {if isset($idActividad) and $formDB.fk_pfb002_num_proceso eq $proceso.pk_num_proceso}
                                <option value="{$proceso.pk_num_proceso}" selected>{$proceso.txt_descripcion_proceso}</option>
                            {else}
                                <option value="{$proceso.pk_num_proceso}">{$proceso.txt_descripcion_proceso}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="cod_proceso"><i class="icm icm-cog3"></i> Proceso</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group floating-label" id="pk_num_faseError">
                <select name="form[int][pk_num_fase]" id="pk_num_fase" name="pk_num_fase" class="form-control" {if isset($formDB.fk_pfb003_num_fase)}disabled{/if}>
                    <option value="">&nbsp;</option>
                    {foreach item=fila from=$listaFase}
                        {if $fila.pk_num_fase == $formDB.fk_pfb003_num_fase}
                            <option selected value="{$fila.pk_num_fase}">{$fila.txt_descripcion_fase}</option>
                        {else}
                            <option value="{$fila.pk_num_fase}">{$fila.txt_descripcion_fase}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="pk_num_fase"><i class="icm icm-cog3"></i> Fase</label>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group floating-label" id="txt_descripcion_actividadError">
                    <input type="text" class="form-control" value="{if isset($formDB.txt_descripcion_actividad)}{$formDB.txt_descripcion_actividad}{/if}" name="form[txt][txt_descripcion_actividad]" id="txt_descripcion_actividad">
                    <label for="txt_descripcion_actividad"><i class="icm icm-cog3"></i> Nombre actividad</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group floating-label" id="txt_comentarios_actividadError">
                    <textarea class="form-control" rows="2" name="form[txt][txt_comentarios_actividad]" id="txt_comentarios_actividad">{if isset($formDB.txt_comentarios_actividad)}{$formDB.txt_comentarios_actividad}{/if}</textarea>
                    <label for="txt_comentarios_actividad"><i class="icm icm-cog3"></i> Descripción</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group floating-label" id="num_duracion_actividadError">
                    <input type="text" class="form-control" name="form[int][num_duracion_actividad]" id="num_duracion_actividad" value="{if isset($formDB.num_duracion_actividad)}{$formDB.num_duracion_actividad}{/if}" onkeypress="return appf.metValidaCampNum(event)">
                    <label for="num_duracion_actividad"><i class="md md-av-timer"></i> Duración</label>
                </div>
            </div>

            <div class="col-sm-9">
                <div class="checkbox checkbox-styled">
                    <label class="checkbox-inline">
                        <input type="checkbox" {if isset($formDB.ind_afecto_plan) and $formDB.ind_afecto_plan=='N'} checked {/if} name="form[txt][ind_afecto_plan]">
                        <span>No afecta planificación</span>
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus=='1'} checked {/if} name="form[int][num_estatus]">
                        <span>Estatus</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group floating-label">
                    <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                    <label for="ind_usuario"><i class="md md-person"></i> Último Usuario</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="cod_faseError">
                    <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                    <label for="cod_actividad"><i class="fa fa-calendar"></i> Última Modificación</label>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal" title="Click para cancelar" alt="Click para cancelar">
            <span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar
        </button>
        {if in_array('PF-01-05-01-03-02-M',$_Parametros.perfil)}
            <button id="btn_guardar" type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" title="Click para guardar" alt="Click para guardar">
                <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Guardar
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    var appf='';
    $(document).ready(function() {
        $("#formAjax").submit(function(){ return false; });
        $('#modalAncho').css("width","50%");
        var url="{$_Parametros.url}modPF/maestros/actividadFiscalCONTROL/";
        appf=new AppfFunciones();
        $("#formAjax").keypress(function(e){
            if (e.which == 13){ return false; }else{ return true; }
        });
        $('#pk_num_proceso').change(function(){
            $("#pk_num_fase").html("");
            $.post(url+'ListarFasesMET', { idProceso:$('#pk_num_proceso').val() }, function (data) {
                if(data){
                    $('#pk_num_fase').html(data);
                }else{
                    swal("Información del Sistema", "No hay fases para el proceso seleccionado", "error");
                }
            });
        });
        /**
         * Ingresa ó actualiza un registro
         */
        $('#btn_guardar').click(function(){
            appf.metDepuraText2('txt_descripcion_fase');
            appf.metEstadoObjForm('pk_num_proceso,pk_num_fase',true);
            var msj="";
            if($("#idActividad").val()==0){
                msj="Se va a ingresar una nueva actividad";
            }else{
                msj="Se va a actualizar el registro actual";
                msj+=". ¿Estas de Acuerdo?";
            }
            swal({
                title: "Confirmación de proceso",
                text: msj,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false
            }, function () {
                swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (data) {
                    if (data.status == 'error') {
                        appf.metActivaError(data);
                        swal("¡Atención!", "Los campos con una x en rojo no debe quedar vacíos", "error");
                    } else if (data.status == 'errorSQL') {
                        swal("¡Atención!", data.mensaje, "error");
                    } else if (data.status == 'nuevo') {
                        swal("Registro Ingresado!", data.mensaje, "success");
                        $('#cbox_proceso').val($('#pk_num_proceso').val());
                        metBuscar(); /*Refresca la grilla*/
                        $('#ContenidoModal').html(''); $('#cerrarModal').click();
                    } else if (data.status == 'modificar') {
                        metBuscar(); /*Refresca la grilla*/
                        swal("Registro Modificado!", data.mensaje, "success");
                        $('#ContenidoModal').html(''); $('#cerrarModal').click();
                    }
                }, 'json');
            });
        });
    });
</script>