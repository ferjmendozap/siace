<div class="card">
    <div class="card-body">
        <div class="table-responsive">
            <table id="datatable1" class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Nombre ente</th>
                    <th>Ente principal</th>
                    <th>Estatus</th>
                    <th class="text-center">Acción</th>
                </tr>
                </thead>
                <tbody>
                {foreach item=fila from=$listadoEntes}
                    <tr id="tr_idEnte{$fila.id_ente}">
                        <td id="td_ente{$fila.id_ente}">{$fila.nombre_ente}</td>
                        <td>{$fila.ente_padre}</td>
                        <td>
                            <i class="{if $fila.estatus_ente==1}md md-check{else}md md-not-interested{/if}"></i>
                        </td>
                        <td align="center">
                            {if in_array('PF-01-05-02-02-02-M',$_Parametros.perfil)}
                                <button class="modificar btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                    onclick="metMontaModalEnte({$fila.id_ente})"
                                    title="Click para modificar" alt="Click para modifica">
                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                </button>
                            {else}
                                <button class="btn btn-raised btn-xs btn-default" title="No tienes permiso para esta opción" alt="No tienes permiso para esta opción">
                                    <i class="glyphicon glyphicon-edit"></i>
                                </button>
                            {/if}
                            &nbsp;
                            {if in_array('PF-01-05-02-02-03-E',$_Parametros.perfil)}
                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                    onclick="metEliminaEnte({$fila.id_ente})"
                                    title="Click para eliminar" alt="Click para eliminar">
                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                </button>
                            {/if}
                        </td>
                    </tr>
                {/foreach}
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="4" align="center">
                        {if in_array('PF-01-05-02-02-01-N',$_Parametros.perfil)}
                            <a href="#" id="btn_nuevo" class="button logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal"
                               data-keyboard="false"data-backdrop="static"title="Click para ingresar un nuevo responsable" alt="Click para ingresar un nuevo responsable"
                               onclick="metCrear()">
                                <i class="md md-create"></i>&nbsp;Nuevo Ente
                            </a>
                        {else}
                            <button class="button btn btn-raised btn-default" title="No tienes permiso para esta opción" alt="No tienes permiso para esta opción">
                                <i class="md md-create"></i>&nbsp;Nuevo Ente
                            </button>
                        {/if}
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>