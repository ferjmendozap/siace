<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Entes</h2>
    </div>
    <div class="section-body">
        <div id="resultadoConsulta"></div>
    </div>
</section>

<script type="text/javascript">
    var metMontaModalEnte="",metCrear="",metBuscar="",metEliminaEnte="";
    $(document).ready(function() {
        var url = '{$_Parametros.url}modPF/maestros/entesCONTROL/';
        /**
         * Lista los entes
         */
        metBuscar = function () {
            $.post(url + 'ListadoEntesMET', '', function (data) {
                $('#resultadoConsulta').html(data);
            });
        };
        metBuscar();
        /**
         * Monta el form modal para el ingreso de un nuevo ente
         */
        metCrear = function () {
            $('#ContenidoModal').html("");
            $.post(url + 'MontaEnteMET', { idEnte: 0 }, function ($dato) {
                $('#formModalLabel').html('<i class="icm icm-cog3"></i> Crear Entes');
                $('#ContenidoModal').html($dato);
            });
        };
        /**
         * Monta los datos en el form modal para la actualizar al clicar sobre un botón de la grilla
         * @param idEnte
         */
        metMontaModalEnte = function (idEnte) {
            $.post(url + 'MontaEnteMET', { idEnte: idEnte }, function (data) {
                $('#formModalLabel').html('');$('#ContenidoModal').html('');
                if (data) {
                    $('#formModalLabel').html('<i class="fa fa-edit"></i> Actualizar Ente');
                    $('#ContenidoModal').html(data);
                } else {
                    $('#formModalLabel').html('');
                    $('#ContenidoModal').html('');
                    $('#cerrarModal').click();
                    swal("Información del Sistema", "Disculpe, no se pudo montar los datos. Intente nuevamente", "error");
                }
            });
        }
        /**
         * Elimina un ente al clicar un botón de eliminar de la grilla
         * @param idActividad
         */
        metEliminaEnte = function (idEnte) {
            swal({
                title: "Confirmación de proceso",
                text: "Se va a eliminar el ente '" + $('#td_ente'+idEnte).html() + "'. ¿Estas de acuerdo? ",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false
            }, function (isConfirm) {
                if (isConfirm) {
                    swal({ title: "¡Por favor espere!", text: "Procesando...", showConfirmButton: false });
                    $.post(url + 'EliminarEnteMET', { idEnte: idEnte }, function (dato) {
                        if (dato.reg_afectado) {
                            swal("Registro Eliminado", dato.mensaje, "success");
                            metBuscar();
                        } else {
                            swal("¡Atención!", dato.mensaje, "error");
                        }
                    }, 'json');
                } else {
                    swal("¡Cancelado!", "Proceso cancelado", "error");
                }
            });
        }
    });
</script>