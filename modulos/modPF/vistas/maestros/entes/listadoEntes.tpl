<style type="text/css">
    {literal}
    .table tbody>tr>td.vert-align{
        vertical-align: middle;
    }
    .puntero{cursor:pointer;}
    {/literal}
</style>
<div id="entesPpal" class="section-body">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="datatable3" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">Nombre ente</th>
                        <th class="text-center">Ente Prinicipal</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=fila from=$listaentes}
                        <tr onclick="metMontaEnte({$fila.id_ente},'{$fila.ente_padre}','{$fila.nombre_ente}')" title="Click para seleccionar" alt="Click para seleccionar" class="puntero">
                            <td class="vert-align">{$fila.nombre_ente}</td>
                            <td>{$fila.ente_padre}</td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var metMontaEnte="";
    $('#datatable3').DataTable({
        "dom": 'lCfrtip',
        "order": [],
        "colVis": {
            "buttonText": "Columnas",
            "overlayFade": 0,
            "align": "right"
        },
        "language": {
            "lengthMenu": 'Mostrar _MENU_',
            "search": '<i class="fa fa-search"></i>',
            "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                "next": '<i class="fa fa-angle-right"></i>'
            }
        }
    });
    /**
     * Monta el ente seleccionado en form modal.
     * @param id_ente
     * @param idPersonaEnte
     * @param entesPadre: Nombre(s) de ente(s) padre
     * @param nombreEnte: nombre del ente seleccionado como principal de uno nuevo.
     */
    metMontaEnte=function(idEnte,entesPadre,nombreEnte){
        var saltolinea="",espacios="";
        if(entesPadre){ /*Se determina el salto de linea y los espacios para la dependencia de menor nivel*/
            entePadre=entesPadre.split('<br>'); var cant=entePadre.length;
            for(i=1; i<=cant; i++){
                espacios+='&nbsp;';
            }
            saltolinea='<br>';
        }
        $('#idEntePadre').val(idEnte);
        $('#entePadre').html(entesPadre+saltolinea+espacios+'-'+'<span class="text-bold">'+nombreEnte+'</span>');
        $('#formModalLabel3').html('');$('#ContenidoModal3').html('');
        $('#cerrarModal3').click();
    }
</script>