<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Asignación de Actividades a Tipos de Actuación</h2>
    </div>
    <div class="section-body">
        <form autocomplete="off" id="formAjax" class="form" role="form" method="post">
            <input type="hidden" id="id_x" name="form[int][id_x]" />
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group" id="idtipo_actuacionfiscalError">
                        <select id="cbox_actuacionfiscal" name="form[int][idTipoActuacionFiscal]" class="form-control texto-size">
                            <option value="">Seleccione</option>
                            {foreach item=fila from=$tipo_actuacionf}
                                <option value="{$fila.idTipoActuacionFiscal}">{$fila.tipo_actuacionfiscal}</option>
                            {/foreach}
                        </select>
                        <label for="idtipo_actuacionfiscal"><i class="icm icm-cog3"></i> Tipos de Actuación Fiscal</label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group" id="cod_procesoError">
                        <select id="cbox_proceso" name="form[int][idProceso]" class="form-control texto-size">
                            <option value="">Seleccione</option>
                            {foreach item=proceso from=$listaProceso}
                                <option value="{$proceso.pk_num_proceso}">{$proceso.txt_descripcion_proceso}</option>
                            {/foreach}
                        </select>
                        <label for="pk_num_proceso"><i class="icm icm-cog3"></i> Proceso</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group" id="cbox_faseError">
                        <select id="cbox_fase" name="form[int][idFase]" class="form-control texto-size"></select>
                        <label for="pk_num_fase"><i class="icm icm-cog3"></i> Fase</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div id="resultadoConsulta"></div>
                </div>
            </div>
        </form>
    </div>
</section>
<style type="text/css">
    {literal}
    .texto-size{font-size: 12px;}
    {/literal}
</style>
<script type="text/javascript">
    var metAgregarActividad="",metQuitar="";
    $(document).ready(function(){
        var appf=new AppfFunciones();
        var url="{$_Parametros.url}modPF/maestros/asignaActividadCONTROL/";
        $("#formAjax").submit(function(){ return false; });
        $('#cbox_actuacionfiscal').change(function(){
            $("#resultadoConsulta").html('');$("#cbox_fase").html('');$('#cbox_proceso').val('');
        });
        /**
         * Lista las fases al seleccionar un proceso
         */
        function mCargarFase(){
            $.post(url+'CargarFasesMET',$( "#formAjax" ).serialize(), function (opcion) {
                $("#cbox_fase").html(opcion);
            });
        }
        $('#cbox_proceso').change(function(){
            $("#resultadoConsulta").html('');$("#cbox_fase").html('');
            if($('#cbox_proceso').val()){
                if(!$('#cbox_actuacionfiscal').val()) {
                    swal("Atención!", 'Primero, seleccione el Tipo de Actuación Fiscal', "error");
                    $('#cbox_proceso').val('');
                }else{
                    mCargarFase();
                }
            }
        });
        /**
         * Lista las actividades al una fase
         */
        function metBuscar(){
            $.post(url+'CargarGrillaMET', $( "#formAjax" ).serialize(), function (data) {
                if(data){
                    $('#resultadoConsulta').html(data);
                }else{
                    swal("Información del Sistema", "No hay resultados", "error");
                }
            });
        };
        $('#cbox_fase').change(function(){
            if($('#cbox_fase').val()){
                metBuscar();
            }else{
                $('#resultadoConsulta').html('');
                $('#resultadoConsulta').html('');
            }
        });
        metAgregarActividad=function (){
            swal({
                title: 'Confirmación de proceso',
                text: 'Se va agregar la(s) actividad(es) seleccionda(s)',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                $.post(url+'AgregaActividadMET', $( "#formAjax" ).serialize(), function(dato){
                    if(dato.result){
                        swal("Exito", dato.mensaje, "success");
                        metBuscar();
                    }else{
                        swal("Atención", dato.mensaje, "error");
                    }
                },'json');
            });
        };
        metQuitar=function (id_actividad_tipoactuac) {
            $( "#id_x" ).val(id_actividad_tipoactuac);
            swal({
                title: 'Quitar Actividad',
                text: 'Seguro que desea quitar la Actividad "'+$('#td_'+id_actividad_tipoactuac).html()+'" del tipo de actuación seleccionado',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                $.post(url+'QuitaActividadMET', $( "#formAjax" ).serialize(),function(dato){
                    if(dato['reg_afectado']){
                        swal("Eliminado!", dato['mensaje'], "success");
                        $('#id_x').val(''); metBuscar();
                    }else{
                        swal("Error!", dato['mensaje'], "error");
                    }
                },'json');
            });
        };
    });
</script>