<form id="formAjax" action="{$_Parametros.url}modPF/maestros/fasesFiscalCONTROL/crearModificarMET" autocomplete="off" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" id="idFase" name="form[int][idFase]" value="{if isset($formDB.pk_num_fase) }{$formDB.pk_num_fase}{else}0{/if}" />
        <div class="row">
            <div class="col-sm-5">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="cod_faseError">
                        <input type="text" disabled class="form-control" value="{if isset($formDB.cod_fase)}{$formDB.cod_fase}{/if}" name="form[txt][cod_fase]" id="cod_fase">
                        <label for="cod_fase"><i class="fa fa-code"></i> Cod Fase</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="form-group floating-label" id="pk_num_procesoError">
                    <select name="form[int][pk_num_proceso]" class="form-control select2" id="pk_num_proceso" data-placeholder="Seleccione el proceso" {if isset($formDB.fk_pfb002_num_proceso)}disabled{/if}>
                        <option value=""></option>
                        {foreach item=proceso from=$listaProceso}
                            {if isset($idFase) and $formDB.fk_pfb002_num_proceso eq $proceso.pk_num_proceso}
                                <option value="{$proceso.pk_num_proceso}" selected>{$proceso.txt_descripcion_proceso}</option>
                            {else}
                                <option value="{$proceso.pk_num_proceso}">{$proceso.txt_descripcion_proceso}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="cod_proceso" style="margin-top: -10px;"><i class="icm icm-cog3"></i> Procesos</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group floating-label" id="txt_descripcion_faseError">
                    <input type="text" class="form-control" value="{if isset($formDB.txt_descripcion_fase)}{$formDB.txt_descripcion_fase}{/if}" name="form[txt][txt_descripcion_fase]" id="txt_descripcion_fase">
                    <label for="txt_descripcion_fase"><i class="icm icm-cog3"></i> Fase</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-7">
                <div class="col-sm-12">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked{/if} value="1" name="form[int][num_estatus]">
                            <span>Estatus</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group floating-label">
                    <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                    <label for="ind_usuario"><i class="md md-person"></i> Último Usuario</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="cod_faseError">
                    <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                    <label for="cod_fase"><i class="fa fa-calendar"></i> Última Modificación</label>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal" title="Click para cancelar" alt="Click para cancelar">
            <span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar
        </button>
        {if in_array('PF-01-05-01-02-02-M',$_Parametros.perfil)}
            <button id="btn_guardar" type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" title="Click para guardar" alt="Click para guardar">
                <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Guardar
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        var appf=new AppfFunciones();
        $("#formAjax").submit(function(){ return false; });
        $('#modalAncho').css("width","50%");
        /**
         * Ingresa ó actualiza un registro
         */
        $('#btn_guardar').click(function(){
            appf.metDepuraText2('txt_descripcion_fase');
            appf.metEstadoObjForm('cod_fase,pk_num_proceso',true);
            var msj="";
            if($("#idFase").val()==0){
                msj="Se va a ingresar una nueva fase";
            }else{
                msj="Se va a actualizar el registro actual";
                msj+=". ¿Estas de Acuerdo?";
            }
            swal({
                title: "Confirmación de proceso",
                text: msj,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false
            }, function () {
                swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (data) {
                    if (data.status == 'error') {
                        appf.metActivaError(data);
                        swal("¡Atención!", "Los campos con una x en rojo no debe quedar vacíos", "error");
                    } else if (data.status == 'errorSQL') {
                        swal("¡Atención!", data.mensaje, "error");
                    } else if (data.status == 'nuevo') {
                        swal("Registro Ingresado!", data.mensaje, "success");
                        $('#cbox_proceso').val($('#pk_num_proceso').val());
                        metBuscar(); /*Refresca la grilla*/
                        $('#ContenidoModal').html(''); $('#cerrarModal').click();
                    } else if (data.status == 'modificar') {
                        metBuscar(); /*Refresca la grilla*/
                        swal("Registro Modificado!", data.mensaje, "success");
                        $('#ContenidoModal').html(''); $('#cerrarModal').click();
                    }
                }, 'json');
            });
        });

    });
</script>