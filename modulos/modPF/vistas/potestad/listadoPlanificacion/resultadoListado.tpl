<style type="text/css">
    {literal}
    .table tbody>tr>td.vert-align{vertical-align: middle;}
    .puntero{cursor:help;}
    {/literal}
</style>
<div class="section-body">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">Potestad Nro</th>
                        <th class="text-center">Objetivo</th>
                        <th class="text-center">Ente</th>
                        <th class="text-center">Fecha Inicio</th>
                        <th class="text-center">Fecha fin</th>
                        <th class="text-center">Fec fin Real</th>
                        <th class="text-center">Estado</th>
                        <th class="text-center">Acci&oacute;n</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=fila from=$listado}
                        <tr id="tr_atuacion_'.{$fila.pk_num_planificacion}'">
                            <td class="vert-align">{$fila.cod_planificacion}</td>
                            <td>{$fila.ind_objetivo}</td>
                            <td>{$fila.nombre_ente}</td>
                            <td class="vert-align">{$fila.fec_inicio}</td>
                            <td class="vert-align">{$fila.fecha_finplan}</td>
                            <td class="vert-align">{$fila.fecha_finrealplan}</td>
                            <td class="vert-align {if $fila.ind_estado eq 'AN'}puntero{/if}" {if $fila.ind_estado eq 'AN'}title="{$fila.ind_nota_justificacion}" alt="{$fila.ind_nota_justificacion}"{/if}>{$fila.desc_estado}</td>
                            <td class="vert-align">
                                {if in_array('PF-01-02-02-03-01-01-M',$_Parametros.perfil)}
                                    {if $fila.ind_estado == 'PR'}
                                        <button id="btn_actualizar" class="btn btn-raised btn-primary btn-xs"
                                                data-toggle="modal" data-target="#formModal"
                                                onclick="metMontaPlanificacion({$fila.pk_num_planificacion})"
                                                title="Click para actualizar la planificación" alt="Click para actualizar la planificación">
                                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                                        </button>
                                    {else}
                                        <button id="btn_ver" class="logsUsuario btn btn-raised btn-xs btn-warning"
                                                data-toggle="modal" data-target="#formModal"
                                                onclick="metMontaPlanificacion({$fila.pk_num_planificacion})"
                                                title="Ver planificación" alt="Ver planificación">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                                    {/if}
                                {else}
                                    <button id="btn_generar" class="btn btn-raised btn-xs btn-default"
                                            title="No tienes permiso para esta opción" alt="No tienes permiso para esta opción">
                                        <i class=" glyphicon glyphicon-edit"></i>
                                    </button>
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>