<style type="text/css">
    {literal}
    .table tbody>tr>td.vert-align{
        vertical-align: middle;
    }
    {/literal}
</style>
<div class="section-body">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">Actuación</th>
                        <th class="text-center">Objetivo</th>
                        <th class="text-center">Ente</th>
                        <th class="text-center">Fecha Inicio</th>
                        <th class="text-center">Fecha fin</th>
                        <th class="text-center">Fec fin Real</th>
                        <th class="text-center">Estado</th>
                        <th class="text-center">Acci&oacute;n</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=fila from=$listado}
                        <tr id="tr_atuacion_'.$fila['pk_num_planificacion'].'">
                            <td class="vert-align">{$fila.cod_planificacion}</td>
                            <td>{$fila.ind_objetivo}</td>
                            <td>{$fila.nombre_ente}</td>
                            <td class="vert-align">{$fila.fec_inicio}</td>
                            <td class="vert-align">{$fila.fecha_finplan}</td>
                            <td class="vert-align">{$fila.fecha_finrealplan}</td>
                            <td class="vert-align">{$fila.desc_estado}</td>
                            <td class="vert-align">
                                {if in_array('PF-01-01-01-01-02-M',$_Parametros.perfil)}
                                    {if $fila.ind_estado == 'PR'}
                                        <button id="btn_actualizar" class="btn btn-raised btn-primary btn-xs"
                                                data-toggle="modal" data-target="#formModal"
                                                onclick="metMontaPlanificacion({$fila.pk_num_planificacion})"
                                                title="Click para actualizar la planificación" alt="Click para actualizar la planificación">
                                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                                        </button>
                                    {else}
                                        <button id="btn_ver" class="logsUsuario btn btn-raised btn-xs btn-warning"
                                                data-toggle="modal" data-target="#formModal"
                                                onclick="metMontaPlanificacion({$fila.pk_num_planificacion})"
                                                title="Ver actuación" alt="Ver actuación">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                                    {/if}
                                {else}
                                    <button id="btn_generar" class="btn btn-raised btn-xs btn-default"
                                            title="No tienes permiso para esta opción" alt="No tienes permiso para esta opción">
                                        <i class="glyphicon glyphicon-edit"></i>
                                    </button>
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="8" align="center">
                            {if in_array('PF-01-01-01-01-01-N',$_Parametros.perfil)}
                                <button id="btn_nuevo" class="btn btn-info btn-raised" title="Click para crear una nueva actuación fiscal" alt="Click para crear una nueva actuación fiscal"
                                        descripcion="Nueva Planificación"
                                        data-toggle="modal"
                                        data-target="#formModal"
                                        titulo="Registrar nueva Actuación Fiscal">
                                    <span class="md md-create"></span>
                                    Nueva Planificación
                                </button>
                            {/if}
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    var $url = '{$_Parametros.url}modPF/actuacionFiscal/actuacionFiscalCONTROL/CrearActuacionMET';
    //Nueva planficacion
    $('#btn_nuevo').click(function () {
       // $('#btn_Limpiar').click();
        $('#formModalLabel').html('');
        $('#formModalLabel').html($(this).attr('titulo'));
        $.post($url, { idPlanificacion:0 }, function ($dato) {
            $('#ContenidoModal').html($dato);
        });
    });
</script>