<style type="text/css">
    {literal}
        .fondo-fase:hover {
            color: #fdfdfd;
            background-color: #1D8888 !important;
        }
        tr.fondo-Subtotal,
        tr.fondo-Subtotal:hover {
            background-color: #bcd  !important;
        }
        tr.fondo-totales,
        tr.fondo-totales:hover {
            font-weight: bold;
            background-color: #90a4ae  !important;
        }
        .table thead>tr>th.vert-align{
            font-weight: bold; vertical-align: middle;
        }
    {/literal}
</style>
<table id="datatablePf" class="table table-condensed table-hover" width="100%">
    <thead>
        <tr>
            <th class="text-center vert-align">Estado</th>
            <th class="text-center vert-align">Actividad</th>
            <th class="text-center vert-align" style="width:5%">Lapso</th>
            <th class="text-center vert-align">Inicio</th>
            <th class="text-center vert-align">Fin</th>
            <th class="text-center vert-align">Prorga Acu.</th>
            <th class="text-center vert-align" style="width:5%">Prorga.</th>
            <th class="text-center vert-align">Inicio Real</th>
            <th class="text-center vert-align">Fin Real</th>
            <th class="text-center vert-align">No Afe.</th>
        </tr>
    </thead>
    <tbody id="tbody_actividades">
        {if $listaActividades|count > 0}
            {foreach key=nombre_fase item=fases from=$listaActividades}
                <tr id="tr_fase" class="style-primary fondo-fase">
                    <td></td>
                    <td>{$nombre_fase}:</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                {foreach item=fila from=$fases}
                    {$campoProrga=false}
                    {if $fila.ind_estado_actividad=='EJ'} {*Actividad en ejecusión*}
                        {$iCon='<i class="md md-lens" style="color: green;"></i>'}
                        {if $fila.estado_prorroga=='PR' OR $fila.estado_prorroga=='RV' OR !$fila.estado_prorroga}
                            {$campoProrga=true}
                        {/if}
                    {elseif $fila.ind_estado_actividad=='PE'} {*Actividad por ejecutar*}
                        {$iCon='<i class="md md-lens" style="color: red;"></i>'}
                    {elseif $fila.ind_estado_actividad=='TE'} {*Actividad por terminada*}
                        {$iCon='<i class="md md-done all" style="color: green;"></i>'}
                    {else} {*Actividad por ejecutar*}
                        {$iCon='<i class="md md-lens" style="color: red;"></i>'}
                    {/if}
                    {if !$fila.num_dias_prorroga} {*si es vacío, entra*}
                        {$fila.num_dias_prorroga=0}
                    {/if}

                    {if $fila.ind_afecto_plan eq 'S'}
                        {$SubTotal_dias=$SubTotal_dias+(($fila.num_dias_duracion_actividad + $fila.num_dias_prorroga_actividad) + $fila.num_dias_prorroga)}
                        {$afecta_plan='S'}
                        {$no_afecta_plan=''}
                    {else}
                        {$afecta_plan='N'}
                        {$no_afecta_plan='md md-check'}
                        {$totalDiasNoAfecta=$totalDiasNoAfecta+$fila.num_dias_duracion_actividad}
                    {/if}
                    {$subTotalAcumProrga=$subTotalAcumProrga+$fila.num_dias_prorroga_actividad}
                    {$subTotalProrga=$subTotalProrga+$fila.num_dias_prorroga}
                    <tr id="tr_actividad">
                        <td align="center">{$iCon}</td>
                        <td id="td_descActividad_{$fila.pk_num_actividad}">
                            <span id="spn_descActividad_{$fila.pk_num_actividad}">{$fila.txt_descripcion_actividad}</span>
                        </td>
                        <td id="td_lapsoActividad" align="center" actividaAfecta="{$afecta_plan}">{$fila.num_dias_duracion_actividad}</td>
                        <td id="td_fecha_inicio{$contador}" class="text-center">{$fila.fec_inicio_actividad}</td>
                        <td id="td_fecha_fin{$contador}" class="text-center">{$fila.fec_culmina_actividad}</td>
                        <td id="td_prorrAcum{$contador}" class="text-center">{$fila.num_dias_prorroga_actividad}</td>
                        <td id="td_cantDiasProrga" class="text-center" campoProrga="{$campoProrga}">
                            {if $campoProrga}
                                <input id="txt_dias_prorroga" type="number" name="txt_dias_prorroga[]" class="form-control text-center" size="2" min="0" max="99"
                                       value="{$fila.num_dias_prorroga}" onclick="appf.metSeleccionaCampo('txt_dias_prorroga')"
                                       onkeypress="return appf.metValidaCampNum(event)" onkeyup="metRecalcularActividades()"
                                       onchange="metRecalcularActividades()" {if !$habilitarCampo}disabled{/if}/>
                            {else}
                                {$fila.num_dias_prorroga}
                            {/if}
                        </td>
                        <td id="td_fecha_inicio_real{$contador}" class="text-center">{$fila.fec_inicio_real_actividad}</td>
                        <td id="td_fecha_fin_real{$contador}" class="text-center">{$fila.fec_culmina_real_actividad}</td>
                        <td id="td_no_afecta{$contador}" class="text-center"><i class="{$no_afecta_plan}"></i></td>
                    </tr>
                    {$contador=$contador+1}
                    {$fecha_fin_plan="'{$fila.fec_culmina_real_actividad}'"}
                {/foreach}
                <tr id="tr_subtotalFase" class="fondo-Subtotal">
                    <td align="center"></td>
                    <td id="td_1">Total dias Fase:</td>
                    <td id="td_subtotalFase{$ifilasubtotal++}" name="td_Subtotal" class="text-center">{$SubTotal_dias}</td>
                    <td></td>
                    <td></td>
                    <td id="td_subTotalProrgaAcumFase" class="text-center">{$subTotalAcumProrga}</td>
                    <td id="td_subTotalProrgaFase" class="text-center">{$subTotalProrga}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                {$Total_dias=$Total_dias+$SubTotal_dias}
                {$Totaldias_prorroga_acum = $Totaldias_prorroga_acum + $subTotalAcumProrga}
                {$Totaldias_prorroga = $Totaldias_prorroga + $subTotalProrga}
                {$SubTotal_dias=0} {$subTotalAcumProrga=0} {$subTotalProrga=0}
            {/foreach}
            <tr id="tr_totalPlan" class="fondo-totales">
                <td align="center"></td>
                <td id="td_1">Total días duración afecta:</td>
                <td id="td_totalAfecta" class="text-center">{$Total_dias}</td>
                <td></td>
                <td></td>
                <td id="td_totalProrgaAcum" class="text-center">{$Totaldias_prorroga_acum}</td>
                <td id="td_totalProrroga" class="text-center">{$Totaldias_prorroga}</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        {/if}
    </tbody>
    <tfoot>
        <tr>
            <td colspan="9">
                <div>
                    <i class="md md-lens" style="color: green;"></i> Actividad en Ejecución.&nbsp;&nbsp;
                    <i class="md md-lens" style="color: red;"></i> Actividad por Ejecutar.&nbsp;&nbsp;
                    <i class="md md-done all" style="color: green;"></i> Actividad terminada.
                </div>
            </td>
        </tr>
    </tfoot>
</table>
<script type="text/javascript">
    $('#datatablePf_info').html('Total Actividades:'+{$contador});
</script>