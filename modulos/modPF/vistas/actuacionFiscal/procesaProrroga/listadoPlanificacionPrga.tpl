<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">{$tituloForm}</h2>
    </div>
    <div class="section-body">
        <form id="form1" class="form" role="form" method="post">
            <input id="year_actual" type="hidden" value="{$year_actual}" />
            <input type="hidden" id="invitado" name="form[int][invitado]" value="{$userInvitado}" />
            <input type="hidden" id="codTipoProceso" name="form[int][codTipoProceso]" value="{$codTipoProceso}" />
            <input type="hidden" id="opcionMenu" name="form[int][opcionMenu]" value="{$opcionMenu}" />
            <input type="hidden" id="idPlanificacionList" value="" />
            <input type="hidden" id="estadoPlanificacion" value="" />
            <input type="hidden" id="codPlanificacionList" value="" />
            <input type="hidden" id="idProrrogaList" value="" />
            <input type="hidden" id="estadoProrrogaList" value="" />
            <input type="hidden" id="verSelectBoxBuscarPor" value="{$verCboxBuscarPor}" />
            <div class="row">
                <div id="divBuscarPor" class="col-sm-2">
                    <div class="form-group" id="cbox_buscarPorError">
                        <select id="cbox_buscarPor" name="form[txt][cbox_buscarPor]" class="form-control">
                            <option value="PG">Prórrogas</option> {*PR*}
                            <option value="PL">Planificaciones</option>
                        </select>
                        <label for="cbox_buscarPor">Buscar por:</label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="form-group" id="cbox_yearplanError">
                        <select id="cbox_yearplan" name="form[int][cbox_yearplan]" class="form-control">
                            {if $listayears|count > 0}
                                {foreach item=fila from=$listayears}
                                    {if $fila eq $year_actual}
                                        <option value="{$fila}" {if $userInvitado==false}selected{/if}>{$fila}</option>
                                    {else}
                                        <option value="{$fila}">{$fila}</option>
                                    {/if}
                                {/foreach}
                            {/if}
                        </select>
                        <label for="cbox_yearplan">Año fiscal:</label>
                    </div>
                </div>
                <div class="col-sm-2 objPorplanificacion">
                    <div class="form-group" id="cbox_tipoActuacionError">
                        <select id="cbox_tipoActuacion" name="form[int][cbox_tipoActuacion]" class="form-control">
                            <option value=""selected></option>
                            {if $listaTipoActuacion|count > 0}
                                {foreach item=tip_fila from=$listaTipoActuacion}
                                    <option value="{$tip_fila.idtipo_actuacion}">{$tip_fila.nombretipo_actuacion}</option>
                                {/foreach}
                            {/if}
                        </select>
                        <label for="cbox_tipoActuacion">Tipo actuación:</label>
                    </div>
                </div>
            </div>
            <div class="row">
                {if $userInvitado}
                    <div class="col-sm-5">
                        <div class="form-group" id="cbox_dependenciaError">
                            <select id="cbox_dependencia" name="form[int][cbox_dependencia]" class="form-control">
                                <option value="" selected></option>
                                {if $listaDependencias|count > 0}
                                    {foreach item=fila from=$listaDependencias}
                                        <option value="{$fila.id_dependencia}" {if $selectedDep}selected{/if}>{$fila.nombre_dependencia}</option>
                                    {/foreach}
                                {/if}
                            </select>
                            <label for="cbox_dependencia">Dependencias de Control Fiscal:</label>
                        </div>
                    </div>
                {/if}
            </div>
            <div class="row objPorplanificacion">
                <div class="col-sm-2">
                    <div class="form-group" id="txt_codPlanificacionError">
                        <input id="txt_codPlanificacion" name="form[txt][codPlanificacion]" type="text" class="form-control" value="">
                        <label for="txt_codPlanificacion">Nro. Planificación:</label>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group" id="cbox_origenActuacionError">
                        <select id="cbox_origenActuacion" name="form[int][cbox_origenActuacion]" class="form-control">
                            <option value=""selected></option>
                            {if $listaOrigenActuacion|count > 0}
                                {foreach item=tip_fila from=$listaOrigenActuacion}
                                    <option value="{$tip_fila.idorigen_actuacion}">{$tip_fila.nombre_origenactuacion}</option>
                                {/foreach}
                            {/if}
                        </select>
                        <label for="cbox_origenActuacion">Origen actuación:</label>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div id="txt_fechainic1Error" class="form-group">
                        <input id="txt_fechainic1" type="text" name="form[txt][txt_fechainic1]" class="form-control texto-size date">
                        <label for="txt_fechainic1"><i class="fa fa-calendar"></i>Fecha desde</label>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div id="txt_fechainic2Error" class="form-group">
                        <input id="txt_fechainic2" type="text" name="form[txt][txt_fechainic2]" class="form-control texto-size date">
                        <label for="txt_fechainic2"><i class="fa fa-calendar"></i>Fecha hasta</label>
                    </div>
                </div>
            </div>
            <div class="row objPorplanificacion">
                <div class="col-sm-4">
                    <div class="form-group" id="cbox_enteError">
                        <select id="cbox_ente" name="form[int][cbox_ente]" class="form-control">
                            <option value="" selected></option>
                            {if $listaentes|count > 0}
                                {foreach item=fila from=$listaentes}
                                    <option value="{$fila.id_ente}">{$fila.nombre_ente}</option>
                                {/foreach}
                            {/if}
                        </select>
                        <label for="cbox_ente"><i class="fa fa-institution"></i>Entes:</label>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group objPlanSinProg" id="cbox_estadosplanError">
                        <select id="cbox_estadosplan" name="form[txt][cbox_estadosplan]" class="form-control">
                            <option value="">Todos</option>
                            {if $listaEstadosplan|count > 0}
                                {foreach item=fila from=$listaEstadosplan}
                                    <option value="{$fila.estado}">{$fila.desc_estado}</option>
                                {/foreach}
                            {/if}
                        </select>
                        <label for="cbox_estadosplan">Estado planificación:</label>
                    </div>
                    <div class="form-group objPlanConProg" id="cbox_estadosProrrogaError">
                        <select id="cbox_estadosProrroga" name="form[txt][cbox_estadosProrroga]" class="form-control">
                            <option value="">Todos</option>
                            {if $listaEstadoProrroga|count > 0}
                                {foreach item=fila from=$listaEstadoProrroga}
                                    <option value="{$fila.estado}">{$fila.desc_estado}</option>
                                {/foreach}
                            {/if}
                        </select>
                        <label for="cbox_estadosProrroga">Estado Prorroga</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div align="center">
                        <button id="btn_Limpiar" class="btn btn-default btn-raised" title="Click para limpiar" alt="Click para limpiar">Limpiar</button>
                        <button id="btn_Buscar" class="btn btn-primary ink-reaction btn-raised" title="Click para listar primero" alt="Click para listar primero"> Buscar</button>
                    </div>
                </div>
            </div>
            <span class="clearfix"></span>
        </form>
        {*Contenedor de la Grilla de planificaciones ó prórrogas*}
        <div id="resultadoConsulta"></div>
    </div>
</section>
<style type="text/css">
    {literal}
    .form-group > label, .form-group .control-label, .form-control{
        font-size: 12px;
        margin-bottom: 0;
    }
    .form-group > label{
        color: #313534;
    }
    .form-group > select{height: 27px;}
    .form-group > input{height: 27px;}
    {/literal}
</style>
<script type="text/javascript">
{literal}
    var metMontarProrroga='',metValoresPlanificacion='',metCrear='',metMontarProrroga='',metBuscar='';
    $(document).ready(function () {
        $(".objPlanSinProg").hide();
        $("#form1").submit(function(){ return false; });
        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "dd-mm-yyyy",language: 'es'});
        var url = 'modPF/actuacionFiscal/procesaProrrogaCONTROL/';
        /**
         * Oculta el combobox "Buscar por" si se trata de la ejecución de una revisión ó aprobación de una prórroga.
         */
        if(!$("#verSelectBoxBuscarPor").val()){
            $("#divBuscarPor").hide();
        }
        /**
         * Permite visualizar el placeholder de acuerdo a la opción seleccionada en el combobox "Buscar por"
         */
        $("#txt_fechainic1").hover(
            function(){
                if($("#cbox_buscarPor").val()=='PG'){
                    $('#txt_fechainic1').attr({ placeholder:"Fecha registro prog." });
                }else{
                    $('#txt_fechainic1').attr({ placeholder:"Fecha inicio plan." });
                }
            },
            function(){$('#txt_fechainic1').removeAttr('placeholder');}
        );
        /**
         * Permite visualizar el placeholder de acuerdo a la opción seleccionada en el combobox "Buscar por"
         */
        $("#txt_fechainic2").hover(
            function(){
                if($("#cbox_buscarPor").val()=='PG'){
                    $('#txt_fechainic2').attr({ placeholder:"Fecha registro prog." });
                }else{
                    $('#txt_fechainic2').attr({ placeholder:"Fecha inicio plan." });
                }
            },
            function(){$('#txt_fechainic2').removeAttr('placeholder');}
        );
        /**
         * Permite la disposición de filtros para búsqueda de planificaciones ó prórrogas
         */
        $("#cbox_buscarPor").change(function(){
            if($("#cbox_buscarPor").val()=='PG'){
                $(".objPlanSinProg").hide();
                $(".objPlanConProg").show();
            }else{
                $("#resultadoConsulta").html();
                $(".objPlanConProg").hide();
                $(".objPlanSinProg").show();
            }
        });
        /**
         * Monta de forma recursiva las dependencias de un ente al ser seleccionado
         */
        function metCargCboxRcsvo(idEnte){
            var $tultic='',valor_filtro=idEnte;
            $.post(url+'ListaEntesMET',{idEnte:idEnte,codTipoProceso:$('#codTipoProceso').val()},function(data){
                $('#cbox_ente').html("");
                if(data.filas){
                    $.each(data.filas, function(i,fila){
                        $tultic='title="'+fila.toltick+'" alt="' +  fila.toltick  +'"';
                        if(i==0){
                            if(valor_filtro==0){
                                $('#cbox_ente').append('<option value="" selected></option>');
                                $('#cbox_ente').append('<option value="'+fila.id_ente+'"'+$tultic+'>'+fila.nombre_ente+'</option>');
                                $('#cbox_ente').val("");
                            }else{
                                $('#cbox_ente').append('<option value="'+fila.id_ente+'"'+$tultic+'>'+fila.nombre_ente+'</option>');
                                $('#cbox_ente').val(fila.id_ente);
                            }
                            $id_nivel_ant=fila.idente_padre;
                        }else{
                            $('#cbox_ente').append('<option value="'+fila.id_ente+'"'+$tultic+'>'+fila.nombre_ente+'</option>');
                        }
                    });
                }
                if(valor_filtro!=0){
                    $('#cbox_ente').append('<option value="'+$id_nivel_ant+'">-----Nivel Anterior-----</option>');
                }
            }, "json");
        }
        /**
         * Permite la consulta de dependencias de entes al seleccionar uno
         */
        $('#cbox_ente').change(function(){
            if($('#cbox_ente').val()){
                metCargCboxRcsvo($('#cbox_ente').val());
            }
        });
        /**
         *Prepara los parámetros a ser enviados
         */
        function metPreparaParams(idPlanificacion){
            var idDependencia=null;
            if($('#cbox_dependencia').length>0){
                idDependencia=$('#cbox_dependencia').val();
            }
            var $params = {
                cbox_buscarPor:$('#cbox_buscarPor').val(),
                opcionMenu:$('#opcionMenu').val(),
                cbox_yearplan:$('#cbox_yearplan').val(),
                codTipoProceso:$('#codTipoProceso').val(),
                cbox_dependencia:idDependencia,
                txt_codPlanificacion:$('#txt_codPlanificacion').val(),
                cbox_tipoActuacion:$('#cbox_tipoActuacion').val(),
                cbox_origenActuacion:$('#cbox_origenActuacion').val(),
                cbox_ente:$('#cbox_ente').val(),
                txt_fechainic1:$('#txt_fechainic1').val(),
                txt_fechainic2:$('#txt_fechainic2').val(),
                cbox_estadosplan:$('#cbox_estadosplan').val(),
                cbox_estadosProrroga:$('#cbox_estadosProrroga').val(),
                idPlanificacion:idPlanificacion
            };
            return $params;
        }
        /*Se buscan las planificaciones ó prórrogas*/
        metBuscar=function(opcionMsj){
            if($('#invitado').val() && !$('#cbox_dependencia').val()){
                swal("¡Atención!", "Debes seleccionar la dependencia de control fiscal", "error"); return
            }else if(!$('#cbox_yearplan').val()){
                swal("¡Atención!", "Seleccione el año por favor", "error"); return
            }
            $('#resultadoConsulta').html('');
            $.post(url+'ListarPlanificacionesMET', metPreparaParams(0), function (data) {
                if(data){
                    $('#resultadoConsulta').html(data);
                }else{
                    if(opcionMsj){
                        swal("Información del Sistema", "No hay nada que mostrar", "error");
                    }
                }
            });
        }
        $('#btn_Buscar').click(function(){
            metBuscar(true);
        });
        metValoresPlanificacion=function(idPlanificacion,codPlanificacion,estadoPlan){
            $('#idPlanificacionList').val(idPlanificacion);
            $('#codPlanificacionList').val(codPlanificacion);
            $('#estadoPlanificacion').val(estadoPlan);
        }
        metCrear=function(){
            if($('#cbox_buscarPor').val()=='PL'){
                var accion="nueva";
                var preTituloForm=' Nueva ';
            }else{
                var preTituloForm=' Actualizar ';
                var accion="modificar";
            }
            if($('#opcionMenu').val()=='RV'){
                preTituloForm=' Revisar ';
            }else if($('#opcionMenu').val()=='AP'){
                preTituloForm=' Aprobar ';
            }
            $('#ContenidoModal').html('');
            if($('#idPlanificacionList').val()){
                var idPlanificacion=$('#idPlanificacionList').val();
                var codPlanificacion=$('#codPlanificacionList').val();
                var estadoPlanificacion=$('#estadoPlanificacion').val();
                $('#idPlanificacionList').val('');$('#codPlanificacionList').val('');$('#estadoPlanificacion').val('');
                $('#rd_planificacion'+idPlanificacion).prop( "checked", false );
                var params={ idPlanificacion:idPlanificacion,codPlanificacion:codPlanificacion,
                    estadoPlanificacion:estadoPlanificacion,codTipoProceso:$('#codTipoProceso').val(),
                    opcionMenu:$('#opcionMenu').val(),accion:accion,idProrroga:$('#idProrrogaList').val(),
                    estadoProrroga:$('#estadoProrrogaList').val() };
                $.post(url+'MontaProrrogaMET', params, function (dato) {
                    if(dato){
                        $('#formModal').modal({ show: 'true' });
                        $('#formModalLabel').html('<i class="fa fa-edit"></i><span id="preTituloForm">'+preTituloForm+'</span><span id="tituloForm"></span>');
                        $('#ContenidoModal').html(dato);
                    }else{
                        swal("Información del Sistema", "¡¡Disculpe!! Ocurrió un error de conexión. Intente de nuevo", "error");
                    }
                });
            }else{
                swal("¡Atención", "Primero debe seleccionar la planificación", "error");
            }
        };
        metMontarProrroga=function(idPlanificacion,codPlanificacion,estadoPlan,idProrroga,estadoProrroga){
            $('#idPlanificacionList').val(idPlanificacion);
            $('#codPlanificacionList').val(codPlanificacion);
            $('#estadoPlanificacion').val(estadoPlan);
            $('#idProrrogaList').val(idProrroga);
            $('#estadoProrrogaList').val(estadoProrroga);
            metCrear();
        }
        $('#btn_Limpiar').click(function(){
            if($('#cbox_dependencia').length>0 && ($('#codTipoProceso').val()!='04' && $('#codTipoProceso').val()!='05')){
                $('#cbox_dependencia').val("");
            }
            $('#cbox_yearplan').val($('#year_actual').val());
            $('#txt_codPlanificacion').val(''); $('#cbox_tipoActuacion').val('');
            $('#cbox_origenActuacion').val(''); $('#txt_fechainic1').val('');$('#txt_fechainic2').val('');
            if($('#cbox_ente').val()){metCargCboxRcsvo(0);}
            $('#resultadoConsulta').html(''); $('#cbox_estadosplan').val(''); $('#cbox_estadosProrroga').val('');
        });
        $('#cbox_buscarPor').change(function(){
            $('#btn_Limpiar').click();
        });
    });
{/literal}
</script>