<style type="text/css">
    {literal}
    .fondo-headgrupo:hover {
        color: #fdfdfd;
        background-color: #1D8888 !important;
    }
    tr.fondo-piedegrupo,
    tr.fondo-piedegrupo:hover {
        background-color: #bcd  !important;
    }
    tr.fondo-totales,
    tr.fondo-totales:hover {
        font-weight: bold;
        background-color: #90a4ae  !important;
    }
    .table thead>tr>th.vert-align{
        font-weight: bold; vertical-align: middle;
    }
    {/literal}
</style>
<div class="card">
    {*<div class="card-head style-primary-light"><header>Procesa Fallo</header></div>*}
    <div class="card-body" style="margin-bottom: 0;">
        <table id="DataTable2Pf" class="table table-condensed table-hover" width="100%">
            <thead>
            <tr>
                <th class="text-center vert-align">Persona</th>
                <th class="text-center vert-align">Decisión</th>
                <th class="text-center vert-align">Monto multa</th>
                <th class="text-center vert-align">Monto reparo</th>
                <th class="text-center vert-align">Opción</th>
            </tr>
            </thead>
            <tbody id="tbody_sanciones">
                {if $listaSancionados|count > 0}
                    {foreach key=nombre_persona item=grupoPersona from=$listaSancionados}
                        {*Se crea un array para extraer la cédula y el nombre por separado*}
                        {$arrPersona=explode('&&',$nombre_persona)}
                        {*se imprime la cédula y el nombre de la persona*}
                        <tr class="style-primary fondo-headgrupo">
                            <td>Cédula:{number_format($arrPersona[0],0,',','.')}, {$arrPersona[1]}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        {*Se imprime las sanciones*}
                        {foreach item=fila from=$grupoPersona}
                            <tr>
                                <td>Fecha: {$fila.fecha_decision|date_format:"%d-%m-%Y"}</td>
                                <td>{$fila.nombre_decision}</td>
                                <td align="center">{number_format($fila.monto, 2, ',', '.')}</td>
                                <td class="text-center">{number_format($fila.monto_reparo, 2, ',', '.')}</td>
                                <td class="text-center">
                                    <button class="eliminar btn ink-reaction btn-raised btn-xs btn-danger"
                                            onclick="metEliminaSancion({$fila.id_decision},'{$fila.nombre_decision}','{$arrPersona[1]}')"
                                            title="Click para eliminar" alt="Click para eliminar">
                                            <i class="md md-delete" style="color: #ffffff;"></i>
                                    </button>
                                </td>
                            </tr>
                        {/foreach}
                    {/foreach}
                {/if}
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    $('#DataTable2Pf_info').html('');
</script>