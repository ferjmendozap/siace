<style type="text/css">
    {literal}
    .tab-pane{padding: 0}
    .form-group > label, .form-group .control-label, .form-control {
        font-size: 12px;
        margin-bottom: 0;
        opacity: 1;
    }
    {/literal}
</style>
<div class="modal-body" style="padding:0;">
    <div class="container-fluid">
        <div id="rootWizard" class="form-wizard form-wizard-horizontal">
            <div class="form-wizard-nav">
                <div class="progress">
                    <div class="progress-bar progress-bar-primary"></div>
                </div>
                <ul class="nav nav-justified">
                    <li><a id="tab_1" class="active" href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">INFORMACIÓN GENERAL</span></a></li>
                    <li><a id="tab_2" href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">AUDITORES</span></a></li>
                    <li><a id="tab_3" href="#tab3" data-toggle="tab"><span class="step">3</span> <span class="title">ACTIVIDADES</span></a></li>
                    {if $panelDecision}
                        <li><a id="tab_4" href="#tab4" data-toggle="tab"><span class="step">4</span> <span class="title">DECISIONES</span></a></li>
                    {/if}
                </ul>
            </div>
            <div class="tab-content clearfix">
                <div class="tab-pane active" id="tab1">
                    <div class="card" style="padding:0;">
                        <div class="card-head style-primary-light">
                            <header>Información General</header>
                        </div>
                        <div class="card-body">
                            <form id="formAjaxTab1" action="{$_Parametros.url}modPF/actuacionFiscal/actuacionFiscalCONTROL/CrearActuacionMET" class="form form-horizontal floating-label form-validation" novalidate="novalidate" role="form" method="post">
                                <input type="hidden" name="valido" id="valido" value="1" />
                                <input type="hidden" id="idPlanificacion" name="form[int][idPlanificacion]" value="{$idPlanificacion}" />
                                <input type="hidden" id="idPlanificacionPadre" name="form[int][idPlanificacionPadre]" value="{$actuacion.num_planificacion_padre}" />
                                <input type="hidden" id="estadoPlanif" name="form[txt][estadoPlanif]" value="{$actuacion.ind_estado_planificacion}" />
                                <input type="hidden" id="tipoProceso" name="form[int][tipoProceso]" value="{$tipoProceso}" />
                                <input type="hidden" id="adjuntarArchivos" value="{$adjuntarArchivos}" />
                                <div class="form-group">
                                    <!--Código de la  Actuacion-->
                                    <div class="col-sm-2 text-right">
                                        <label for="lbl_codplanificacion" class="control-label">Planificación Nro.:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div id="lbl_codplanificacion" style="margin-top: 8px;">{$actuacion.cod_planificacion}</div>
                                    </div>
                                    <!--Estado de la Actuación-->
                                    <div class="col-sm-6 text-right">
                                        <label for="ind_estado" class="control-label">Estado:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div id="lbl_estado" style="margin-top: 8px;">{if isset($actuacion.desc_estado)}{$actuacion.desc_estado}{/if}</div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <!--organismo ejecutante-->
                                    <div class="col-sm-2 text-right">
                                        <label for="id_contraloria" class="control-label">Contraloría:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="id_contraloriaError">
                                            <select id="id_contraloria" name="form[int][id_contraloria]" class="form-control select2" disabled="disabled">
                                                {foreach item=fila from=$listaContraloria}
                                                    <option value="{$fila.id_contraloria}" selected>{$fila.nombre_contraloria}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                    <!--fecha inicio actuacion-->
                                    <div class="col-sm-2 text-right">
                                        <label for="fec_inicio" class="control-label">Fecha Inicio:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="input-group" id="fec_inicioError" >
                                            <div class="input-group-content">
                                                <input id="fec_inicio" type="text" name="form[txt][fec_inicio]" class="form-control" style="margin-top: 2px;" value="{$actuacion.fec_inicio}" disabled>
                                            </div>
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <!--dependencia interna de la contraloría-->
                                    <div class="col-sm-2 text-right">
                                        <label for="id_dependencia" class="control-label">Dependencia:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="id_dependenciaError">
                                            <select id="id_dependencia" name="form[int][id_dependencia]" class="form-control select2" disabled>
                                                <option value="">Seleccione..</option>
                                                {foreach item=fila from=$listaDepContraloria}
                                                    {if isset($actuacion.fk_a004_num_dependencia)}
                                                        {if $fila.id_dependencia eq $actuacion.fk_a004_num_dependencia}
                                                            <option value="{$fila.id_dependencia}" selected>{$fila.nombre_dependencia}</option>
                                                        {else}
                                                            <option value="{$fila.id_dependencia}">{$fila.nombre_dependencia}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$fila.id_dependencia}">{$fila.nombre_dependencia}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                    <!--fecha termino actuacion-->
                                    <div class="col-sm-2 text-right">
                                        <label for="fec_termino" class="control-label">Fecha Fin:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div id="lbl_fecha_fin_plan" style="margin-top: 8px;">{$actuacion.fecha_fin_plan}</div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <!--Dependencias Centro de Costo:-->
                                    <div class="col-sm-2 text-right">
                                        <label for="pk_num_centro_costo"class="control-label">Centro de Costo:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="pk_num_centro_costoError">
                                            <select id="pk_num_centro_costo" name="form[int][pk_num_centro_costo]" class="form-control select2" disabled>
                                                <option value="">Seleccione..</option>
                                                {if $listaCentroCosto|count > 0}
                                                    {foreach item=fila from=$listaCentroCosto}
                                                        {if $actuacion.fk_a023_num_centro_costo eq $fila.pk_num_centro_costo}
                                                            <option value="{$fila.pk_num_centro_costo}" selected>{$fila.ind_descripcion_centro_costo}</option>
                                                        {else}
                                                            <option value="{$fila.pk_num_centro_costo}">{$fila.ind_descripcion_centro_costo}</option>
                                                        {/if}
                                                    {/foreach}
                                                {/if}
                                            </select>
                                        </div>
                                    </div>
                                    <!--Duracion afecta -->
                                    <div class="col-sm-2 text-right">
                                        <label for="duracion_afecta" class="control-label">Duración Afecta:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div id="lbl_duracion_afectacion" style="margin-top: 8px;">{$actuacion.cant_dias_afecta}</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!--Tipo de actuación:-->
                                    <div class="col-sm-2 text-right">
                                        <label for="idtipo_actuacion"class="control-label">Tipo Actuacion:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="idtipo_actuacionError">
                                            <select id="idtipo_actuacion" name="form[int][idtipo_actuacion]" class="form-control select2" disabled="disabled">
                                                <option value="">Seleccione..</option>
                                                {if $listaTipoActuacion|count > 0}
                                                    {foreach item=filatipo from=$listaTipoActuacion}
                                                        {if $actuacion.fk_a006_num_miscdet_tipoactuacion eq $filatipo.idtipo_actuacion}
                                                            <option value="{$filatipo.idtipo_actuacion}" selected>{$filatipo.nombretipo_actuacion}</option>
                                                        {else}
                                                            <option value="{$filatipo.idtipo_actuacion}">{$filatipo.nombretipo_actuacion}</option>
                                                        {/if}
                                                    {/foreach}
                                                {/if}
                                            </select>
                                        </div>
                                    </div>
                                    <!--Duracion no afecta -->
                                    <div class="col-sm-2 text-right">
                                        <label for="duracion_no_afecta" class="control-label">Duración No Afecta:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div id="lbl_duracion_no_afectacion" style="margin-top: 8px;">{$actuacion.cant_dias_no_afecta}</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!--Entes Externos:-->
                                    <div class="col-sm-2 text-right">
                                        <label for="id_ente" class="control-label">Ente:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="nombre_ente">{$actuacion.nombre_ente}</div>
                                    </div>
                                    <!--Prorroga-->
                                    <div class="col-sm-2 text-right">
                                        <label for="prorroga" class="control-label">Prorroga:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div id="lbl_prorroga" style="margin-top: 8px;">{$actuacion.cant_dias_prorroga}</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!--origen de la actuacion:-->
                                    <div class="col-sm-2 text-right">
                                        <label for="idorigen_actuacion" class="control-label">{if !$pAcionFiscal}Origen actuación:{else}Dep. solicitante:{/if}</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div id="idorigen_actuacionError">
                                            <select id="idorigen_actuacion" name="form[int][idorigen_actuacion]" class="form-control select2"disabled>
                                                <option value="">Seleccione..</option>
                                                {if $listaOrigenActuacion|count > 0}
                                                    {foreach item=fila from=$listaOrigenActuacion}
                                                        {if $actuacion.fk_a006_num_miscdet_origenactuacion eq $fila.idorigen_actuacion}
                                                            <option value="{$fila.idorigen_actuacion}" selected>{$fila.nombre_origenactuacion}</option>
                                                        {else}
                                                            <option value="{$fila.idorigen_actuacion}">{$fila.nombre_origenactuacion}</option>
                                                        {/if}
                                                    {/foreach}
                                                {/if}
                                            </select>
                                        </div>
                                        {if $pAcionFiscal}
                                            <div style="margin-top: 8px;">{$actuacion.dependencia_solctte}</div>
                                        {/if}
                                    </div>
                                    <!--Duración total -->
                                    <div class="col-sm-2 text-right">
                                        <label for="duracionTotal" class="control-label">Duración total:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div id="lbl_duracionTotal" style="margin-top: 8px;">{$actuacion.totalDias_Plan}</div>
                                    </div>
                                </div>
                                {if $actuacion.idplanificacion_referencia}
                                    <div class="form-group">
                                        <!--Tipos de proceso fiscal y código de control de planificación-->
                                        <div class="col-sm-2 text-right">
                                            <label for="idorigen_actuacion" class="control-label">Tipo de Proceso:</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <div style="margin-top: 8px;">{$actuacion.descProceso}</div>
                                        </div>

                                        <div class="col-sm-2 text-right">
                                            <label for="duracionTotal" class="control-label">Nro. Planificación:</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div style="margin-top: 8px;">{$actuacion.codPlnfcnSlctte}</div>
                                        </div>
                                    </div>
                                {/if}

                                <div class="form-group">
                                    <!---objetivo general-->
                                    <div class="col-sm-2 text-right">
                                        <label for="ind_objetivo" class="control-label">Objetivo General:</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <div id="ind_objetivoError" >
                                            <textarea id="ind_objetivo" class="form-control" rows="2" name="form[txt][ind_objetivo]" disabled>{$actuacion.ind_objetivo}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!---alcance-->
                                    <div class="col-sm-2 text-right">
                                        <label for="ind_alcance" class="control-label">Alcance:</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <div id="ind_alcanceError" >
                                            <textarea id="ind_alcance" class="form-control" rows="2" name="form[txt][ind_alcance]" disabled>{$actuacion.ind_alcance}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!---observación-->
                                    <div class="col-sm-2 text-right">
                                        <label for="ind_observacion" class="control-label">Observación:</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <div id="ind_observacionError" >
                                            <textarea id="ind_observacion" class="form-control" rows="2" name="form[txt][ind_observacion]"disabled>{$actuacion.ind_observacion}</textarea>
                                        </div>
                                    </div>
                                </div>
                                {if $actuacion.tipoProcAdmin}
                                    <div class="form-group">
                                        <!--origen de la actuacion:-->
                                        <div class="col-sm-2 text-right">
                                            <label for="cbox_tipoprocadmin" class="control-label">Procedimiento:</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <div id="cbox_tipoprocadminError">
                                                <select id="cbox_tipoprocadmin" name="form[txt][cbox_tipoprocadmin]" class="form-control select2" disabled="disabled">
                                                    <option value="">Seleccione..</option>
                                                    {if $listaTiposproc|count > 0}
                                                        {foreach item=fila from=$listaTiposproc}
                                                            {if $actuacion.ind_procdmtoadministrativo eq $fila.valor}
                                                                <option value="{$fila.valor}" selected>{$fila.descripcion}</option>
                                                            {else}
                                                                <option value="{$fila.valor}">{$fila.descripcion}</option>
                                                            {/if}
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                {/if}
                            </form>
                        </div>
                    </div>
                </div>
                <!--PANEL AUDITORES-->
                <div class="tab-pane floating-label" id="tab2">
                    <div class="card">
                        <div class="card-head style-primary-light">
                            <header>Auditores</header>
                        </div>
                        <div class="card-body">
                            <div class="col-lg-12">
                                {*Grilla Auditores asignados*}
                                <div class="table-responsive">
                                    <table id="tb_auditores_plan" class="table table-hover table-striped no-margin">
                                        <thead>
                                        <tr>
                                            <th>Coordinador(a)</th>
                                            <th>Nombre Completo</th>
                                            <th>Cargo</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {if $auditoresDesignados|count > 0}
                                            {foreach item=fila from=$auditoresDesignados}
                                                <tr id="tr_auditor_{$fila.id_auditorPlanificacion}">
                                                    <td><i class="{if $fila.flagCordinador==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                                    <td id="td_auditor_{$fila.id_auditorPlanificacion}">{$fila.nombre_auditor}</td>
                                                    <td>{$fila.cargo_auditor}</td>
                                                </tr>
                                            {/foreach}
                                        {/if}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--PANEL DE ACTIVIDADES-->
                <div class="tab-pane" id="tab3">
                    <input id="idActividadPlanif" type="hidden" />
                    <div class="card">
                        <div class="card-head style-primary-light">
                            <header id="etiqActididad">Actividades</header>
                        </div>
                        <div class="card-body">
                            {*Contenedor para las Actividades*}
                            <div id="contentActividades">
                                <div id="tb_actividades"></div>
                                <div>
                                    <i class="md md-lens" style="color: green;"></i> Actividad en Ejecución.
                                    <i class="md md-lens" style="color: red;"></i> Actividad por Ejecutar.
                                    <i class="md md-done all" style="color: green;"></i> Actividad terminada.
                                </div>
                            </div>
                            <div id="formAdjuntar"></div>
                        </div>
                    </div>
                </div>
                <!--PANEL DE DECISIONES-->
                <div class="tab-pane floating-label" id="tab4">
                    <form id="formAjaxTab4" class="form floating-label form-validation" role="form" method="post">
                        <input type="hidden" name="valido" id="valido" value="1" />
                        <input type="hidden" id="idPlanificacion" name="idPlanificacion" value="{$idPlanificacion}" />
                        <input type="hidden" id="estadoPlanificacion" value="{$estadoPlanificacion}" />
                        <div class="card">
                            <div class="card-head style-primary-light">
                                <header>Decisiones</header>
                            </div>
                            <div class="card-body">
                                {*Contenedor para las Sanciones de Procedimientos Administrativos*}
                                <div id="tb_persSancionados"></div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--BOTONES DE CONTROL-->
                <div  align="center">
                    <button id="bton_cancelar" type="button" class="btn btn-default btn-raised" data-dismiss="modal" title="Click para Salir"  alt="Click para Salir">
                        <span class="glyphicon glyphicon-floppy-remove"></span>Cancelar
                    </button>
                    {if $reversarPlanif==TRUE}
                        {if ($tipoProceso==$AF AND in_array('PF-01-01-02-02-01-M',$_Parametros.perfil)) OR ($tipoProceso==$VP AND in_array('PF-01-02-01-04-02-01-M',$_Parametros.perfil)) OR ($tipoProceso==$PI AND in_array('PF-01-02-02-04-02-01-M',$_Parametros.perfil)) OR ($tipoProceso==$VD AND in_array('PF-01-03-01-05-02-01-M',$_Parametros.perfil)) OR ($tipoProceso==$DR AND in_array('PF-01-03-02-03-02-01-M',$_Parametros.perfil)) }
                            <button type="button" id="btn_reversaPlanif" class="btn btn-danger btn-raised" title="Click para reversar la planificación" alt="Click para reversar la planificación">
                                <span class="glyphicon glyphicon-ban-circle"></span>
                                Reversar
                            </button>
                        {else}
                            <button type="button" class="btn btn-default" title="No tienes permiso para esta opción" alt="No tienes permiso para esta opción">
                                <span class="glyphicon glyphicon-ban-circle"></span>
                                Reversar
                            </button>
                        {/if}
                    {/if}
                </div>
            </div>
        </div>
    </div>
</div>
<span class="clearfix"></span>
</div>
<script type="text/javascript">
    var metArchivosAdjuntos='',metListarAjuntos='',$Gtemp_archiv = '',url='',metProcesaAjuntos='',metEliminaAdjunto='',metLimpiaFormAdjuntar='',
        metCancelAdjuntar='',metVerAdjunto='';
    $(document).ready(function () {
        var app = new AppFunciones();
        app.metWizard();
        $('#modalAncho').css("width", "90%"); $('#ContenidoModal').css("padding-top", "0"); // ancho de la Modal
        $('.select2').select2({ allowClear: true });
        url = '{$_Parametros.url}modPF/actuacionFiscal/procesaActividadCONTROL/';
        $("#formAjaxTab1").submit(function(){ return false; });
        $('#tab_3').click(function(){ /*Ficha de actividades*/
            if($('#tb_actividades').html().length==0){
                $.post(url+'BuscaActividadesMET',{ idPlanificacion:$('#idPlanificacion').val() }, function (data) {
                    if(data){
                        $('#tb_actividades').html(data);
                    }else{
                        swal("Atención!", "No se encontró actividades disponibles para el Tipo de Actuación", "error");
                    }
                });
            }
        });
        $('#tab_4').click(function(){ /*Ficha de sancionados*/
            if($('#formAdjuntar').html().length>0) {
                metCancelAdjuntar();
            }
            if($('#tb_persSancionados').html().length==0) {
                $.post(url+'BuscaPersSancionadasMET', { idPlanificacion: $('#idPlanificacion').val(), formOrigen: 'DetalleActiv' }, function (data) {
                    if (data) {
                        $('#tb_persSancionados').html(data);
                    }
                });
            }
        });
        /**
         * Reversa una planificación siempre y cuando esté completada a la última actividad a en ejecución
         */
        $('#btn_reversaPlanif').click(function(){
            swal({
                title: 'Reversar Planificación',
                text: '¿Esta seguro que desea Reversar la planificación?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm){
                if(isConfirm){
                    //swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post(url+'reversarPlanificacionMET', { idActuacion: $('#idPlanificacion').val(),idPlanificacionPadre:$('#idPlanificacionPadre').val(),estadoPlanif:$('#estadoPlanif').val(),tipoProceso:{$tipoProceso} }, function(data){
                        if(data.resultado){
                            metListarPlanificaciones();
                            swal(data.titulo, data.mensaje, "success");
                            $('#ContenidoModal').html(''); $('#cerrarModal').click();
                        }else{
                            swal(data.titulo, data.mensaje, "error");
                        }
                    },'JSON');
                }else{
                    swal("¡Cancelado!", "Proceso cancelado", "error");
                }
            });
        });
        /********************************DOCUMENTOS ADJUNTOS********************************/
        $('#formAdjuntar').hide();
        /**
         *Monta o Visualiza el formulario de adjuntar documentos
         */
        metArchivosAdjuntos=function($idActividadPlanif){
            $('#idActividadPlanif').val($idActividadPlanif);
            $('#bton_cancelar').hide();$('#btn_reversaPlanif').hide();
            $('#etiqActididad').html('Actividades / Archivos adjuntos');
            $('#contentActividades').fadeOut(800,function(){
                $('#contentActividades').hide();
                $('#formAdjuntar').fadeIn(800,function(){
                    if($('#formAdjuntar').html().length==0){ //si no está el formulario lo monta
                        $.post(url + 'MontaFormAdjuntaDocMET', { idPlanificacion:$('#idPlanificacion').val(), adjuntarArchivos:$('#adjuntarArchivos').val() },function (data) {
                            if (data) {
                                $("#formAdjuntar").html(data);//Monta el formulario
                            }
                        });
                    }
                    if($('#formAdjuntar').show()){ //Visualizar al montar o que ya esté montado y luego lista los documento adjunatos si existen.
                        metListarAjuntos($idActividadPlanif);
                    }
                });
            });
        };
        //Oculta el formulario de adjuntar documentos.
        metCancelAdjuntar=function(){
            metLimpiaFormAdjuntar(); $('#tb_archiAdjuntos').html('');
            $('#bton_cancelar').show();$('#btn_reversaPlanif').show();
            $('#etiqActididad').html('Actividades');
            $('#formAdjuntar').fadeOut(800,function(){
                $('#formAdjuntar').hide();
                $('#contentActividades').fadeIn(800,function(){ $('#contentActividades').show(); });
            });
        }
        metListarAjuntos = function () {
            $('#tb_archiAdjuntos').html('');
            $.post(url+'ListarArchivosAdjuntosMET', { idActividadPlanif:$('#idActividadPlanif').val(),adjuntarArchivos:$('#adjuntarArchivos').val() }, function (data) {
                if(data){
                    $('#tb_archiAdjuntos').html(data);
                }
            });
        }
    });
</script>