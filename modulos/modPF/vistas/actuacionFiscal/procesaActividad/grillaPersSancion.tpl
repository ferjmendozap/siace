<style type="text/css">
    {literal}
        .fondo-head:hover {
            color: #fdfdfd;
            background-color: #1D8888 !important;
        }
        tr.fondo-Subtotal,
        tr.fondo-Subtotal:hover {
            background-color: #bcd  !important;
        }
        tr.fondo-totales,
        tr.fondo-totales:hover {
            font-weight: bold;
            background-color: #90a4ae  !important;
        }
        .table thead>tr>th.vert-align{
            font-weight: bold; vertical-align: middle;
        }
    {/literal}
</style>
<table id="DataTable2Pf" class="table table-condensed table-hover" width="100%">
    <thead>
    <tr>
        <th class="text-center vert-align">Persona</th>
        <th class="text-center vert-align">Decisión</th>
        <th class="text-center vert-align">Monto multa</th>
        <th class="text-center vert-align">Monto reparo</th>
        <th class="text-center vert-align">Sub total</th>
    </tr>
    </thead>
    <tbody id="tbody_actividades">
    {if $listaSancionados|count > 0}
        {foreach key=nombre_persona item=grupoPersona from=$listaSancionados}
            {*Se crea un array para extraer la cédula y el nombre por separado*}
            {$arrPersona=explode('&&',$nombre_persona)}
            {*se imprime la cédula y el nombre de la persona*}
            <tr class="style-primary fondo-head">
                <td>Cédula:{number_format($arrPersona[0],0,',','.')}, {$arrPersona[1]}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            {*Se imprime el detalle por persona*}
            {foreach item=fila from=$grupoPersona}
                {$sub_total=$fila.monto + $fila.monto_reparo}
                {$totalMontoMulta=$totalMontoMulta+$fila.monto}
                {$totalMontoreparo=$totalMontoreparo+$fila.monto_reparo}
                {$totalxPersona=$totalxPersona+$sub_total}
                {$sub_total=number_format(($fila.monto + $fila.monto_reparo), 2, ',', '.')}
                <tr>
                    <td></td>
                    <td>{$fila.nombre_decision}</td>
                    <td align="center">{number_format($fila.monto, 2, ',', '.')}</td>
                    <td class="text-center">{number_format($fila.monto_reparo, 2, ',', '.')}</td>
                    <td class="text-center">{$sub_total}</td>
                </tr>
            {/foreach}
            <tr class="fondo-Subtotal">
                <td>Total:</td>
                <td></td>
                <td class="text-center">{number_format($totalMontoMulta, 2, ',', '.')}</td>
                <td class="text-center">{number_format($totalMontoreparo, 2, ',', '.')}</td>
                <td class="text-center">{number_format($totalxPersona, 2, ',', '.')}</td>
            </tr>
            {$totalGralMontoMulta=$totalGralMontoMulta+$totalMontoMulta}
            {$totalGralMontoreparo=$totalGralMontoreparo+$totalMontoreparo}
            {$totalGneral=$totalGneral+$totalxPersona}
            {$totalMontoMulta=0}
            {$totalMontoreparo=0}
            {$totalxPersona=0}
        {/foreach}
        {*Se imprime lo totales generales*}
        <tr class="fondo-totales">
            <td>Total general:</td>
            <td></td>
            <td class="text-center">{number_format($totalGralMontoMulta, 2, ',', '.')}</td>
            <td class="text-center">{number_format($totalGralMontoreparo, 2, ',', '.')}</td>
            <td class="text-center">{number_format($totalGneral, 2, ',', '.')}</td>
        </tr>
    {/if}
    </tbody>
</table>
<script type="text/javascript">
    $('#DataTable2Pf_info').html('');
</script>