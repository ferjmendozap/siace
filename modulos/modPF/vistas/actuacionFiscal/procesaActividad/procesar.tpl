<style type="text/css">
    {literal}
    .tab-pane{padding: 0}
    .form-group > label, .form-group .control-label, .form-control {
        font-size: 12px;
        margin-bottom: 0;
        opacity: 1;
    }
    {/literal}
</style>
<div class="modal-body">
    <div id="formActividad">
        <div class="card">
            <div class="card-body">
                <form id="formAjax" action="" class="form form-validation" novalidate="novalidate" role="form" method="post">
                    <input type="hidden" value="1" name="valido" />
                    <input id="idPlanificacion" name="form[int][idPlanificacion]" type="hidden" value="{$dataActividad.pk_num_planificacion}" />
                    <input id="idPlanifPadre" name="form[int][idPlanifPadre]" type="hidden" value="{$dataActividad.idPlanifPadre}" />
                    <input id="idActividadPlanif" name="form[int][idActividadPlanif]" type="hidden" value="{$dataActividad.pk_num_actividad_planific}" />
                    <input id="tipoProceso" name="form[int][tipoProceso]" type="hidden" value="{$tipoProceso}" />
                    <input id="esUltimaActividad" name="form[int][esUltimaActividad]" type="hidden" value="{$esUltimaActividad}" />
                    <input id="cantDiasProrroga" name="form[int][cantDiasProrroga]" type="hidden" value="{$dataActividad.diasProrroga}" />
                    <input id="numSecuenciaActividad" name="form[int][numSecuenciaActividad]" type="hidden" value="{$dataActividad.nro_secuenciActividad}" />
                    <input id="actividad_afectaPlan" name="form[txt][actividad_afectaPlan]" type="hidden" value="{$dataActividad.actividad_afectaPlan}" />
                    <input id="culminarProcesoDe" name="form[txt][culminarProcesoDe]" type="hidden" />
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" disabled value="{$dataActividad.codigo}">
                                <label for="ind_descripcion"><i class="md md-description"></i>&nbsp;{$etiquetaProceso} Nro:</label>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group">
                                <div align="left" style="font-">{$dataActividad.nombreEnte}</div>
                                <label for="ind_direccion"><i class="md md-store"></i>&nbsp;Ente</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea rows="1" cols="40" class="form-control" disabled >{$dataActividad.objetivo}</textarea>
                                <label for="ind_direccion"><i class="md md-store"></i>&nbsp;Objetivo General</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group">
                                <textarea rows="1" cols="40" class="form-control" disabled >{$dataActividad.actividad}</textarea>
                                <label for="ind_objetivo"><i class="fa fa-file-text"></i>&nbsp;Actividad</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <a href="#" id="btn_AdjuntArch" class="bottom btn ink-reaction btn-raised btn-info btn-xs" data-backdrop="static"
                                   title="Click para adjuntar documentos a la actividad actual" alt="Click para adjuntar documentos a la actividad actual">
                                    <i class="md md-attach-file"></i> Adjuntar
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input id="fec_ini" name="form[txt][fec_ini]" type="text" class="form-control" value="{$dataActividad.fec_ini}" readonly>
                                <label for="fec_ini"><i class="md md-account-circle"></i>&nbsp;Fecha Inicio Real:</label>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <input id="fec_fin" name="form[txt][fec_fin]" type="text" class="form-control" value="{$dataActividad.fec_fin}" readonly>
                                <label for="fec_fin"><i class="md md-perm-contact-cal"></i>&nbsp;Fecha Término Real:</label>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <input id="fec_reg_cierre" name="form[txt][fec_reg_cierre]" type="text" class="form-control" value="{$fechaActual}" readonly>
                                <label for="fec_cierre"><i class="md md-perm-contact-cal"></i>&nbsp;Fecha Registro Cierre:</label>
                            </div>
                        </div>
                        {if $dataActividad.diasProrroga>0}{$dataActividad.lapso_actividad = $dataActividad.lapso_actividad + $dataActividad.diasProrroga}{/if}
                        <div class="col-sm-2">
                            <div class="form-group" >
                                <input id="num_LapsoEjecucion" name="form[int][num_LapsoEjecucion]" type="text" class="form-control" value="{$dataActividad.lapso_actividad}" readonly >
                                <label for="num_LapsoEjecucion"><i class="md md-perm-contact-cal"></i>&nbsp;Lapso de ejecución:</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="fec_terminadoError">
                                <input id="fec_terminado" name="form[txt][fec_terminado]" type="text" class="form-control date" value="{$dataActividad.fec_fin}" readonly>
                                <label for="fec_terminado"><i class="md md-perm-contact-cal"></i>&nbsp;Fecha Terminado:</label>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <input id="num_CantDiasCulmina" name="form[int][num_CantDiasCulmina]" type="text" class="form-control" value="{$dataActividad.lapso_actividad}" readonly>
                                <label for="num_CantDiasCulmina"><i class="md md-perm-contact-cal"></i>&nbsp;Días al terminar:</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <textarea id="ind_observacion" name="form[txt][ind_observacion]" rows="1" cols="40" class="form-control">{$dataActividad.observ_actividad}</textarea>
                                <label for="ind_observacion"><i class="fa fa-file-text"></i>&nbsp;Observaciones</label>
                            </div>
                        </div>
                    </div>
                    <!--Contenedor de los elementos de devolución-->
                    <div class="row">
                        <div id="contentDevolucion" class="col-sm-12"></div>
                    </div>
                    <!--Botones de procesos-->
                    <div id="contentBotones" class="text-center">
                        <button type="button" class="btn btn-default ink-reaction btn-raised"
                                data-dismiss="modal" title="Click para salir" alt="Click para salir">
                            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
                        </button>
                        {if $prorrogaSinAprobar==0}
                            {if (in_array('PF-01-03-01-05-01-02-D',$_Parametros.perfil) AND $VJ==$tipoProceso AND $idPadre!=0 AND $esUltimaActividad)}
                                <button id="btn_devolverPlanif" type="button" class="btn btn-danger ink-reaction btn-raised logsUsuarioModal">
                                    <span class="glyphicon glyphicon-send"></span> Devolver Planificación
                                </button>
                            {/if}
                            {if (in_array('PF-01-01-02-01-01-M',$_Parametros.perfil) AND {$tipoProceso}=={$AF})
                                OR (in_array('PF-01-02-01-04-01-01-M',$_Parametros.perfil) AND {$tipoProceso}=={$VP})
                                OR (in_array('PF-01-02-02-04-01-01-M',$_Parametros.perfil) AND {$tipoProceso}=={$PI})
                                OR (in_array('PF-01-03-01-05-01-01-M',$_Parametros.perfil) AND {$tipoProceso}=={$VJ})
                                OR (in_array('PF-01-03-02-03-01-01-M',$_Parametros.perfil) AND {$tipoProceso}=={$PA})}
                                {if $esUltimaActividad} <!--Botón enviar a la CGR-->
                                    <button type="button" class="btnTerminar btn btn-warning ink-reaction btn-raised logsUsuarioModal" boton="Si, Enviar"
                                        value="CGR" descipcion="Se envio la planificacion a la CGR" titulo="Culminar Planificación" mensaje="La planificación se culminará como enviada a la C.G.R. ¿Estas Seguro?"
                                        title="Click para culminar la planificación como enviada a la C.G.R." alt="Click para culminar la planificación como enviada a la C.G.R.">
                                        <span class="glyphicon glyphicon-send"></span> Enviar C.G.R.
                                    </button>

                                    {if $VP==$tipoProceso} <!--Botón enviar a Valoración Jurídica-->
                                        <button type="button" class="btnTerminar btn btn-info ink-reaction btn-raised logsUsuarioModal" boton="Si, Enviar"
                                            value="VJPA" titulo="Enviar a Determinación de Responsabilidades" mensaje="¿Estas seguro que desea proceder?"
                                            title="Click para enviar a Determinación de Responsabilidades" alt="Click para enviar a Determinación de Responsabilidades">
                                            <span class="glyphicon glyphicon-send"></span> Enviar VJPA
                                        </button>
                                    {/if}
                                    {if ($VP==$tipoProceso) or ($VJ==$tipoProceso)} <!--Botón para auto de archivo - Valoración Preliminar y Valoración Jurídica-->
                                        <button type="button" class="btnTerminar btn btn-danger ink-reaction btn-raised logsUsuarioModal" boton="Si"
                                            value="AA"  titulo="Auto de Archivo" mensaje="¿Estas seguro que desea proceder?"
                                            title="Click para culminar la planificación como Auto de Archivo" alt="Click para culminar la planificación como Auto de Archivo">
                                            <span class="glyphicon glyphicon-send"></span> Auto de Archivo
                                        </button>
                                    {/if}
                                {/if}
                                <button type="button"
                                    class="
                                        {if $esUltimaActividad AND ($PA==$tipoProceso)}
                                            btnDecision
                                        {else}
                                            btnTerminar
                                        {/if}
                                        btn btn-primary ink-reaction btn-raised logsUsuarioModal
                                    "
                                        boton="Si"
                                        {if $esUltimaActividad AND ($VP==$tipoProceso)}
                                            value="AC"
                                        {elseif $esUltimaActividad AND ($VJ==$tipoProceso)}
                                            value="AI"
                                        {elseif $esUltimaActividad AND ($AF==$tipoProceso)}
                                            value="CO"
                                        {elseif $esUltimaActividad AND ($PA==$tipoProceso)}
                                            value="DC"
                                        {else}
                                            value="ACT"
                                        {/if}
                                        titulo="
                                        {if $esUltimaActividad AND ($VP==$tipoProceso)}
                                            Auto de Proceder
                                        {elseif ($VJ==$tipoProceso AND $esUltimaActividad)}
                                            Auto de Inicio
                                        {else}
                                            Terminar Actividad
                                        {/if}
                                    "
                                        mensaje="¿Estas seguro?"
                                        {if $esUltimaActividad AND ($VP==$tipoProceso)}
                                            title="Click para culminar la planificación como Auto de Proceder" alt="Click para culminar la planificación como Auto de Proceder"
                                        {elseif $esUltimaActividad AND ($VJ==$tipoProceso)}
                                            title="Click para culminar la planificación como Auto de Inicio" alt="Click para culminar la planificación como Auto de Inicio"
                                        {elseif $esUltimaActividad AND ($PA==$tipoProceso)}
                                            title="Click para procesar decisiones" alt="Click para procesar decisiones"
                                        {else}
                                            title="Click para terminar la actividad" alt="Click para terminar la actividad"
                                        {/if}
                                    >
                                    <span class="glyphicon glyphicon-floppy-disk"></span>
                                    {if $esUltimaActividad AND ($VP==$tipoProceso)} <!--Botón Valoración Preliminar-->
                                        Auto de Proceder
                                    {elseif $esUltimaActividad AND ($VJ==$tipoProceso)} <!--Botón Valoración Jurídica-->
                                        Auto de Inicio
                                    {elseif $esUltimaActividad AND ($PA==$tipoProceso)} <!--Botón Decisiones - Procedimiento Administrativo-->
                                        Decisión
                                    {else} <!--Botón Actividad - en todos los procesos fiscales-->
                                        Terminar Actividad
                                    {/if}
                                </button>
                                {if $esUltimaActividad AND $persSacionados > 0 AND ($tipoProceso==$PA)} <!--Botón del proceso Procedimiento Administrativo-->
                                    <button type="button" class="btnTerminar btn btn-danger ink-reaction btn-raised"
                                        titulo="Culminar Planificación"
                                        mensaje="¿Estas seguro?"
                                        value="CO" title="Click para culminar la planificación" alt="Click para culminar la planificación">
                                        <span class="glyphicon glyphicon-floppy-disk"></span> Culminar Planificación
                                    </button>
                                {/if}
                            {/if}
                        {/if}
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="formAdjuntar"></div>
    <div id="formDecision"></div>
</div>
<span class="clearfix"></span>
<script type="text/javascript">
    var url='',metProcesaDevolucion='',metCancelDevolucion='';
    var metChkDecision='',metEliminaSancion='',metCancelFallo='',metLimpiarDecision='', metGuardaDecision='',metBuscaPersonal='';
    var metLimpiaFormAdjuntar='',metCancelAdjuntar='',metVerAdjunto='',metProcesaAjuntos='',metListarAjuntos='',metEliminaAdjunto='',$Gtemp_archiv = '';
    $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "dd-mm-yyyy", language: 'es'});
    $( document ).ready(function() {
        var app = new AppFunciones();
        $("#formDecision").hide();
        url='{$_Parametros.url}modPF/actuacionFiscal/procesaActividadCONTROL/';
        if({$prorrogaSinAprobar}){
            swal("¡Actividad con Prórroga sin Aprobar!", "La prórroga debe ser aprobada para terminar la actividad", "error");
        }
        $("#fec_terminado").change(function () {
            fec_ini=$('#fec_ini').val(); fec_terminado=$(this).val();
            $.post(url+'diasAlCierreDeLaActividadMET', { fec_ini:fec_ini, fec_terminado:fec_terminado }, function(cant_dias){
                $("#num_CantDiasCulmina").val(cant_dias);
            });
        });
        $('.btnTerminar').click(function(){
            $('#culminarProcesoDe').val($(this).val());
            var titulo=$(this).attr('titulo');
            var mensaje=$(this).attr('mensaje');
            swal({
                title: titulo,
                text: mensaje,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if(isConfirm){
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post(url+'ProcesarActividadMET',$("#formAjax").serialize(), function (data) {
                        if (data.Error == 'error') {
                            app.metValidarError(data,data.mensaje);
                        }else if(data.resultado){
                            metListadoActividadesEjecutar();
                            swal(data.titulo, data.mensaje, "success");
                            $('#ContenidoModal').html('');$('#cerrarModal').click();
                        }else{
                            swal("¡Atención!", data.mensaje, "error");
                        }
                    },'json');
                }else{
                    swal("¡Cancelado!", "Proceso cancelado", "error");
                }
            });
        });
        /********************************************DEVOLVER PLANIFICACIÓN********************************************/
        $('#btn_devolverPlanif').click(function(){
            var elemDevolucion='<div class="form-group" id="ind_nota_justificacionError">'
                    +'<textarea rows="1" cols="40" class="form-control" id="ind_nota_justificacion" name="form[txt][ind_nota_justificacion]"></textarea>'
                    +'<label for="ind_nota_justificacion"><i class="fa fa-file-text"></i>&nbsp;Nota de Justificación</label></div><div class="text-center">'
                    +'<button id="btn_cancelDevolver" type="button" class="btn btn-default ink-reaction btn-raised" title="Click para cancelar" alt="Click para cancelar" onclick="metCancelDevolucion()">Cancelar</button>'
                    +'<button id="btn_procesaDevolucion" type="button" class="btn btn-danger ink-reaction btn-raised" onclick="metProcesaDevolucion()"><span class="glyphicon glyphicon-send"></span>Procesar Devolución</button>'
                    +'</div>';
            $('#contentBotones').fadeOut(800,function(){
                $('#contentBotones').hide();
                $('#contentDevolucion').fadeIn(800,function(){
                    $('#contentDevolucion').html(elemDevolucion);
                });
            });
        });
        metCancelDevolucion=function(){
            $('#contentDevolucion').fadeOut(800,function(){
                $('#contentDevolucion').html('');
                $('#contentBotones').fadeIn(800,function(){
                    $('#contentBotones').show();
                });
            });
        };
        metProcesaDevolucion=function(){
            $('#culminarProcesoDe').val("DV");
            swal({
                title: "Devolver Planificación",
                text: "¿Esta seguro que desea proceder?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if(isConfirm){
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post(url+'ProcesarActividadMET',$("#formAjax").serialize(), function (data) {
                        if (data.Error == 'error') {
                            app.metValidarError(data,data.mensaje);
                        }else if(data.resultado){
                            metListadoActividadesEjecutar();
                            swal(data.titulo, data.mensaje, "success");
                            $('#ContenidoModal').html('');$('#cerrarModal').click();
                        }else{
                            swal("¡Atención!", data.mensaje, "error");
                        }
                    },'json');
                }else{
                    swal("¡Cancelado!", "Proceso cancelado", "error");
                }
            });
        }
        {*****************PROCESA DECISIONES DE SANCIONES A PERSONAS******************}
        //Visualiza el formulario de procesar fallos y oculta el de procesar actividad
        $('.btnDecision').click(function(){
            $('#formModalLabel').html('<i class="fa fa-edit"></i> Procesa Actividad / Procesa Fallo');
            $('#formActividad').fadeOut(800,function(){
                $('#formActividad').hide();
                $('#formDecision').fadeIn(800,function(){
                    if($('#formDecision').html().length==0){ //si no está el formulario lo monta
                        $.post(url + 'MontaFormDecisionMET', '',function (data) {
                            if (data) {
                                $("#formDecision").html(data); //Monta el formulario
                            }
                        });
                    }
                    if($('#formDecision').show()){ //Visualiza el formulario al montarlo o que ya esté montado y lista las personas sancionadas si existen.
                        metLlenaGrillaSancionados();
                    }
                });
            });
        });
        //Oculta el formulario de procesar fallos y visualiza el de procesar actividad
        metCancelFallo=function(){
            $('#idPlanificacionSancion').val(''); $('#tb_sanciones').html(''); metLimpiarDecision();
            $('#formModalLabel').html('<i class="md md-settings"></i> Procesa Actividad');
            $('#formDecision').fadeOut(800,function(){
                $('#formDecision').hide();
                $('#formActividad').fadeIn(800,function(){ $('#formActividad').show(); });
            });
        }
        /**
         * Carga el modal del listado de personas sujetas a sanción del proceso de determinación de responsabilidad.
         */
        metBuscaPersonal=function() {
            $.post(url+'CargaModalPersonasMET', function (dato) {
                if (dato) {
                    $('#formModalLabel3').html('<i class="md md-search"></i> Listado de Personas a Objeto de Sanciones');
                    $('#ContenidoModal3').html('');
                    $('#ContenidoModal3').html(dato);
                }
            });
        }
        /**
         * Llena la grilla con la ó las personas sancionadas.
         */
        function metLlenaGrillaSancionados(){
            if($('#idPlanificacion').val()) {
                $('#tb_sanciones').html('');
                $.post(url + 'BuscaPersSancionadasMET', { idPlanificacion:$('#idPlanificacion').val(), formOrigen:'Decision' },function (data) {
                    if (data) {
                       $("#tb_sanciones").html(data);
                    }
                });
            }
        }
        $('.monto').mask('#.##0,00', { reverse: true});
        /**
         * Deshabilita todos las decisiones menos la decisión sin campos para montos seleccionada.
         */
        function metBloqueaCampos(IdTipoDecision){
            $('input[id="chk_decision"]').each( function(i){
               if($('#cantCampos'+$(this).val()).attr('id') != $('#cantCampos'+IdTipoDecision).attr('id')){
                   $('#intMontoReparo'+$(this).val()).val('');
                   $('#intMontoReparo'+$(this).val()).attr({ disabled:true });
                   $('#intMontos'+$(this).val()).val('');
                   $('#intMontos'+$(this).val()).attr({ disabled:true });
                   $(this).attr({ checked:false });
                   $(this).attr({ disabled:true });
               }
            });
        }
        /**
         * Habilita todos los checkbox de la decisiones.
         */
        function metDesBloqueaCampos(IdTipoDecision){
            $('input[id="chk_decision"]').each( function(i){
               if($('#cantCampos'+$(this).val()).attr('id') != $('#cantCampos'+IdTipoDecision).attr('id')){
                   $(this).attr({ disabled:false });
               }
            });
        }
        /**
         * Habilita o Deshabilita los campos de decisiones.
         */
        metChkDecision=function(IdTipoDecision){
            if($('#cantCampos'+IdTipoDecision).val()==0){ //Por acá entra si el checkbox seleccionado no tiene campos para montos.
                if($('.chkDecision'+IdTipoDecision).prop('checked')){
                    metBloqueaCampos(IdTipoDecision);
                }else{
                    metDesBloqueaCampos(IdTipoDecision);
                }
            }else{ //Por acá entra si el checkbox seleccionado tiene campos para montos.
                if($('#intMontos'+IdTipoDecision).is(':disabled')){ //Habilita los campos de la decisión seleccionada
                    if($('#cantCampos'+IdTipoDecision).val()==2){
                        $('#intMontoReparo'+IdTipoDecision).removeAttr('disabled');
                    }
                    $('#intMontos'+IdTipoDecision).removeAttr('disabled');
                }else{ //Deshabilita los campos de la decisión seleccionada
                    if($('#cantCampos'+IdTipoDecision).val()==2){
                        $('#intMontoReparo'+IdTipoDecision).val('');
                        $('#intMontoReparo'+IdTipoDecision).attr({ disabled:true });
                    }
                    $('#intMontos'+IdTipoDecision).val('');
                    $('#intMontos'+IdTipoDecision).attr({ disabled:true });
                }
            }
        }
        metGuardaDecision=function(){
            $('#idPlanificacionSancion').val($('#idPlanificacion').val());
            if(!$('#idPersona').val()){
                swal("¡Atención!", "Debes seleccionar la persona a objeto de sación", "error"); return;
            }
            swal({
                title: "Confirmación de proceso",
                text: "Se va a guardar el fallo actual ¿Estás de Acuerdo?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if(isConfirm){
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post(url+'GuardaDecisionMET',$("#formAjaxDesicion").serialize(), function (data) {
                        if(data.resultado){
                            metLimpiarDecision();
                            swal("Información del proceso", data.mensaje, "success");
                            metLlenaGrillaSancionados();
                        }else{
                            swal("¡Atención!", data.mensaje, "error");
                        }
                    },'json');
                }else{
                    swal("¡Cancelado!", "Proceso cancelado", "error");
                }
            });

        }
        /**
         * Elimina una una sanción de una persona a hacer click sobre el botón respectivo del form Decisiones.
         * @param idDecision
         * @param nombreSancion
         * @param nombrePersona
         */
        metEliminaSancion=function(idDecision,nombreSancion,nombrePersona){
            swal({
                title: 'Confirmación de proceso',
                text: 'Vas a eliminar la sanción "'+nombreSancion+'" a '+nombrePersona+'. ¿Estás de Acuerdo?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if(isConfirm){
                    swal({ title: "¡Por favor espere!",text: "Procesando...",showConfirmButton: false });
                    $.post(url+'EliminaSancionMET',{ idDecision:idDecision }, function (data) {
                        if(data.reg_afectado){
                            swal("Información del proceso", data.mensaje, "success");
                            metLlenaGrillaSancionados();
                        }else{
                            swal("¡Atención!", data.mensaje, "error");
                        }
                    },'json');
                }else{
                    swal("¡Cancelado!", "Proceso cancelado", "error");
                }
            });
        }
        metLimpiarDecision=function(){
            $('#idPersona').val('');$('#cedulaPersona').html('');$('#nombrePersona').html('');
            $('input[id="chk_decision"]').each( function(i){
                if($('#cantCampos'+$(this).val()).val()==2){
                    $('#intMontoReparo'+$(this).val()).val('');
                    $('#intMontoReparo'+$(this).val()).attr({ disabled:true });
                }
                $('#intMontos'+$(this).val()).val('');
                $('#intMontos'+$(this).val()).attr({ disabled:true });
                $(this).attr({ disabled:false });
                $(this).prop("checked",false);
            });
        };

        /********************************ADJUNTAR DOCUMENTOS********************************/
        $('#formAdjuntar').hide();
        /**
         *Monta o Visualiza el formulario de adjuntar documentos
         */
        $('#btn_AdjuntArch').click(function(){
            $('#formModalLabel').html('<i class="md-attach-file"></i> Procesa Actividad / Ajuntar Documentos');
            $('#formActividad').fadeOut(800,function(){
                $('#formActividad').hide();
                $('#formAdjuntar').fadeIn(800,function(){
                    if($('#formAdjuntar').html().length==0){ //si no está el formulario lo monta
                        $.post(url + 'MontaFormAdjuntaDocMET', { idPlanificacion:$('#idPlanificacion').val(),adjuntarArchivos:1 },function (data) {
                            if (data) {
                                $("#formAdjuntar").html(data);//Monta el formulario
                            }
                        });
                    }
                    if($('#formAdjuntar').show()){ //Se lista los documento adjuntos si existen.
                        metListarAjuntos();
                    }
                });
            });
        });
        //Oculta el formulario de adjuntar documentos. btn_cancelAdjuntar
        metCancelAdjuntar=function(){
            metLimpiaFormAdjuntar(); $('#tb_archiAdjuntos').html('');
            $('#formModalLabel').html('<i class="md md-settings"></i> Procesa Actividad');
            $('#formAdjuntar').fadeOut(800,function(){
                $('#formAdjuntar').hide();
                $('#formActividad').fadeIn(800,function(){ $('#formActividad').show(); });
            });
        }
        metListarAjuntos = function () {
            $('#tb_archiAdjuntos').html('');
            $.post(url+'ListarArchivosAdjuntosMET', { idActividadPlanif:$('#idActividadPlanif').val(),adjuntarArchivos:1 }, function (data) {
                if(data){
                    $('#tb_archiAdjuntos').html(data);
                }
            });
        }
    });
</script>