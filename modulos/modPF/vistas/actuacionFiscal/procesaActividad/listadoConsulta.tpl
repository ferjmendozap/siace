<div class="card">
    <div class="card-body">
        <div class="table-responsive">
            <table id="datatable1" class="table no-margin table-striped table-hover">
                <thead>
                <tr>
                    <th class="text-center">Planificación Nro.</th>
                    <th width="20%" class="text-center">Objetivo</th>
                    <th class="text-center">Ente</th>
                    <th class="text-center">Fecha Inicio</th>
                    <th class="text-center">Fecha Fin</th>
                    <th class="text-center">Fecha Fin Real</th>
                    <!--<th>Fecha Registro</th>-->
                    <th class="text-center">Estado</th>
                    <th width="7%" class="text-center">Opci&oacute;n</th>
                </tr>
                </thead>
                <tbody>
                {if $listado|count > 0}
                    {foreach item=fila from=$listado}
                        <tr id="tr_atuacion_{$fila.pk_num_planificacion}">
                            <td class="text-center">{$fila.cod_planificacion}</td>
                            <td width="20%" class="text-justify">{$fila.ind_objetivo}</td>
                            <td>{$fila.nombre_ente}</td>
                            <td class="text-center">{$fila.fec_inicio}</td>
                            <td class="text-center">{$fila.fec_termino}</td>
                            <td class="text-center">{$fila.fec_termino_real}</td>
                            <td class="text-center">{$fila.desc_estado}</td>
                            <td class="text-center">
                                {if $fila.ind_estado == 'AP' OR $fila.ind_estado == 'RV' OR $fila.ind_estado == 'CO' OR $fila.ind_estado == 'CE' OR $fila.ind_estado == 'PR' OR $fila.ind_estado == 'TE' OR $fila.ind_estado == 'AC' OR $fila.ind_estado == 'AA'OR $fila.ind_estado == 'EV' OR $fila.ind_estado == 'DV' OR $fila.ind_estado == 'VJ' OR $fila.ind_estado == 'AI'}
                                    <button id="btn_ver" class="logsUsuario btn btn-raised btn-xs btn-warning"data-toggle="modal"
                                        data-target="#formModal" onclick="metMontaPlanificacion({$fila.pk_num_planificacion})"
                                        title="Ver planificación" alt="Ver planificación">
                                        <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                    </button>
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                {/if}
                </tbody>
            </table>
        </div>
    </div>
</div>