<section class="style-default-bright">
    <div class="col-lg-12">
        <div class="section-header"><h2 class="text-primary">&nbsp;Ejecución de Actividades</h2></div>
    </div>
    <div class="section-body" id="listadoConsulta">
        <div class="row">
            <div class="col-lg-12 ">
                <div id="tb_grillaActEjecutar"></div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    var metMontaActividad='',metListadoActividadesEjecutar='';
    $(document).ready(function() {
        var url='{$_Parametros.url}modPF/actuacionFiscal/procesaActividadCONTROL/';
        metListadoActividadesEjecutar=function (){
            $('#tb_grillaActEjecutar').html('');
            $.post(url+'ListadoActividadesEjecutarMET',{ tipoProceso:{$tipoProceso} }, function (dato) {
                if (dato) {
                    $('#tb_grillaActEjecutar').html(dato);
                }
            });
        }
        metListadoActividadesEjecutar();
        var url='{$_Parametros.url}modPF/actuacionFiscal/procesaActividadCONTROL/';
        metMontaActividad=function(idPlanificacion) {
            $('#modalAncho').css("width", "80%");
            $('#formModalLabel').html('<i class="md md-settings"></i> Procesar Actividad');
            $.post(url+'MontaActividadMET', { idPlanificacion:idPlanificacion,tipoProceso:{$tipoProceso} }, function ($dato) {
                $('#ContenidoModal').html('');
                $('#ContenidoModal').html($dato);
            });
        }
    });
</script>