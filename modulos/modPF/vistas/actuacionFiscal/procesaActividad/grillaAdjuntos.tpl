<style type="text/css">
    {literal}
    .table thead>tr>th.vert-align{
        font-weight: bold; vertical-align: middle;
    }
    {/literal}
</style>
<div class="card">
    {*<div class="card-head style-primary-light"><header>Procesa Fallo</header></div>*}
    <div class="card-body" style="margin-bottom: 0;">
        <table id="DataTable2Pf" class="table table-condensed table-hover" width="100%">
            <thead>
            <tr>
                <th class="text-center vert-align">Nombre documento</th>
                <th class="text-center vert-align">Nro. documento</th>
                <th class="text-center vert-align">Fecha</th>
                <th class="text-center vert-align">Opción</th>
            </tr>
            </thead>
            <tbody id="tbody_adjuntos">
                {foreach item=fila from=$listaAdjuntos}
                    <tr>
                        <td id="tdAjunto{$fila.id_doc}">{$fila.nombre_doc}</td>
                        <td align="center">{$fila.nro_doc}</td>
                        <td align="center">{$fila.fecha_doc}</td>
                        <td class="text-center">
                            {if $adjuntarArchivos==1}
                                <button class="btn ink-reaction btn-raised btn-xs btn-danger" onclick="metEliminaAdjunto({$fila.id_doc})"
                                    title="Click para eliminar" alt="Click para eliminar"><i class="md md-delete" style="color: #ffffff;"></i>
                                </button>
                            {else}
                                <button class="btn ink-reaction btn-raised btn-xs btn-defoult"
                                        title="Opción inactiva" alt="Opción inactiva"><i class="md md-delete" style="color: #ffffff;"></i>
                                </button>
                            {/if}
                            <button class="btn ink-reaction btn-raised btn-xs btn-warning" onclick="metVerAdjunto({"'"}{$fila.ruta_doc}{"'"})"
                                title="Click para visualizar el documento" alt="Click para visualizar el documento"><i class="md md-remove-red-eye"></i>
                            </button>
                        </td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    $('#DataTable2Pf_info').html('');
</script>