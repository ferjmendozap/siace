<style type="text/css">
    {literal}
    .table tbody>tr>td.vert-align{
        vertical-align: middle;
    }
    .puntero{cursor:pointer;}
    {/literal}
</style>
<div class="section-body">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="datatable3" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">Cedula</th>
                        <th class="text-center">Nombres</th>
                        <th class="text-center">Apellidos</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=fila from=$listaResponsables}
                        {$cedulaPersona=number_format($fila.cedula,0,',','.')}
                        <tr {if isset($fila.nombres)}
                            {if $fila.estatus==1}
                                onclick="metMontaPersona('{$fila.idResponsable}','{$cedulaPersona}','{$fila.nombres}','{$fila.apellidos}')"
                                title="Click para seleccionar" alt="Click para seleccionar" class="puntero"
                            {else}
                                title="Representante inactivo no se puede seleccionar" alt="Representante inactivo no se puede seleccionar"
                            {/if}
                                {else}
                            title="Para seleccionar debe primero relacionar el representante a este ente" alt="Para seleccionar debe primero relacionar el representante a este ente"
                                {/if}>
                            <td class="vert-align">
                                {$cedulaPersona}
                            </td>
                            <td class="vert-align">
                                {$fila.nombres}
                            </td>
                            <td class="vert-align">
                                {$fila.apellidos}
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var metMontaPersona="";
    $('#datatable3').DataTable({
        "dom": 'lCfrtip',
        "order": [],
        "colVis": {
            "buttonText": "Columnas",
            "overlayFade": 0,
            "align": "right"
        },
        "language": {
            "lengthMenu": 'Mostrar _MENU_',
            "search": '<i class="fa fa-search"></i>',
            "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                "next": '<i class="fa fa-angle-right"></i>'
            }
        }
    });
    /**
     * Monta los datos de la persona seleccionada en el form modal.
     * @param idPersona
     * @param cedula
     * @param nombres.
     * @param apellidos.
     */
    metMontaPersona=function(idPersona,cedula,nombres,apellidos){
        $('#idPersona').val(idPersona);
        if(!cedula || cedula==0){ cedula='S/C' }
        $('#cedulaPersona').html(cedula);
        $('#nombrePersona').html(nombres+' '+apellidos);
        $('#formModalLabel3').html('');$('#ContenidoModal3').html('');
        $('#cerrarModal3').click();
    }
</script>