<!--
Grilla para montar las actividades en ejecución
-->
<style type="text/css">
    {literal}
    .fondo-fase:hover {
        color: #fdfdfd;
        background-color: #1D8888 !important;
    }
    .table thead>tr>th.vert-align{
        font-weight: bold; vertical-align: middle;
    }
    {/literal}
</style>
<table id="datatablePf" class="table no-margin table-condensed table-hover">
    <thead>
    <tr>
        <th class="text-center vert-align" >Planificación</th>
        <th class="text-center vert-align" >Actividad</th>
        <th class="text-center vert-align" >Ente</th>
        <th class="text-center vert-align" >Días</th>
        <th class="text-center vert-align" >Fecha Inicio</th>
        <th class="text-center vert-align" >Fecha término Real</th>
        <th class="text-center vert-align" >&nbsp;&nbsp;&nbsp;&nbsp;Opción&nbsp;&nbsp;&nbsp;&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    {if count($listado)>0}
        {foreach key=key item=listad2 from=$listado}
            <tr class="style-primary fondo-fase">
                <td class="style-primary"></td>
                <td class="style-primary">{$key}:</td>
                <td class="style-primary"></td>
                <td class="style-primary"></td>
                <td class="style-primary"></td>
                <td class="style-primary"></td>
                <td class="style-primary"></td>
            </tr>
            {foreach item=fila from=$listad2}
                <tr id="idPlanificacion{$fila.pk_num_planificacion}">
                    <td class="text-center" ><label>{$fila.codigo}</label></td>
                    <td class="text-center" ><label>{$fila.actividad}</label></td>
                    <td><label>{$fila.nombre_ente}</label></td>
                    <td class="text-center" ><label>{$fila.dias}</label></td>
                    <td class="text-center" ><label>{$fila.fec_ini}</label></td>
                    <td class="text-center" ><label>{$fila.fec_fin}</label></td>
                    <td align="center">
                        <button onclick="metMontaActividad({$fila.pk_num_planificacion})" class="logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                data-keyboard="false" data-backdrop="static" title="Finalizar actividad" alt="Finalizar actividad" >
                            <i class="md md-remove-red-eye"></i>
                        </button>
                    </td>
                </tr>
                {$contador=$contador+1}
            {/foreach}
        {/foreach}
    {else}
        <tr>
            <td style="border: hidden;"></td>
            <td style="border: hidden;"></td>
            <td style="border: hidden;"></td>
            <td style="border: hidden; text-align: center;" class="center-align limpiar" >
            <label style="font-weight: bold;">{'No hay actividades para procesar'}</label></td>
            <td style="border: hidden;"></td>
            <td style="border: hidden;"></td>
            <td style="border: hidden;"></td>
        </tr>
        {$contador=0}
    {/if}
    </tbody>
</table>

<script type="text/javascript">
    $('#datatablePf_info').html('Total Actividades : '+{$contador});
</script>