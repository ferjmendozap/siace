<!--
Archivo para proceso de fallos "Multas y Sanciones administrativas a personas".
Nota: es montado desde Ejecución de Actividades.
-->
<div class="card">
    {*<div class="card-head style-primary-light"><header>Procesa Fallo</header></div>*}
    <div class="card-body">
        <form id="formAjaxDesicion" class="form form-validation" novalidate="novalidate">
            <input type="hidden" id="idPlanificacionSancion" name="form[int][idPlanificacionSancion]"/>
            <input type="hidden" id="idPersona" name="form[int][idPersona]"/>
            <div class="row">
                <div class="col-sm-6">
                    <!--<label for="entePadre" id="entePadreError" class="control-label" style="margin-top: 10px; margin-bottom: 0;">
                        <span class="md md-person-add"><span class="text-bold">Persona:</span></span>
                    </label>-->
                    <a href="#" onclick="metBuscaPersonal()" class="bottom btn ink-reaction btn-raised btn-xs btn-primary"
                       data-toggle="modal" data-target="#formModal3" data-keyboard="false" data-backdrop="static"
                       title="Click para seleccionar la persona" alt="Click para seleccionar la persona"><span class="md md-person-add">Persona</span>
                    </a>
                    <br />
                    <div><span class="text-bold">Cédula:&nbsp;</span><span id="cedulaPersona"></span><span class="text-bold">&nbsp;&nbsp;&nbsp;Nombre:</span> <span id="nombrePersona"></span></div>

                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    {*Grilla para las decisiones*}
                    <div class="table-responsive">
                        <table id="tb_decisiones" class="table">
                            <thead>
                            <tr>
                                <th class="text-center">Fallo</th>
                                <th class="text-center">Decisiones</th>
                                <th class="text-center">Monto</th>
                                <th class="text-center">Monto reparo</th>
                            </tr>
                            </thead>
                            <tbody>
                                {if $listaTipoDecision|count > 0}
                                    {foreach item=fila from=$listaTipoDecision}
                                        <tr id="tr_'{$fila.idTipo_decision}">
                                            <td class="text-center">
                                                <input type="checkbox" class="chkDecision{$fila.idTipo_decision}" id="chk_decision" name="form[int][chk_decision][{$fila.idTipo_decision}]" title="Click para seleccionar" alt="Click para seleccionar" value="{$fila.idTipo_decision}" onClick="metChkDecision({$fila.idTipo_decision})" />
                                                <input type="hidden" id="cantCampos{$fila.idTipo_decision}" name="form[int][cantCampos][{$fila.idTipo_decision}]" value="{$fila.cant_montos}"/>
                                            </td>
                                            <td>{$fila.nombre_decision}</td>
                                            <td class="text-center">
                                                {if $fila.cant_montos==1 or $fila.cant_montos==2}
                                                    <input id="intMontos{$fila.idTipo_decision}" type="text" name="form[int][intMontos][{$fila.idTipo_decision}]" disabled class="monto" placeholder="0,00"/>
                                                {/if}
                                            </td>
                                            <td class="text-center">
                                                {if $fila.cant_montos==2}
                                                    <input id="intMontoReparo{$fila.idTipo_decision}" type="text" name="form[int][intMontoReparo][{$fila.idTipo_decision}]" disabled class="monto" placeholder="0,00"/>
                                                {/if}
                                            </td>
                                        </tr>
                                    {/foreach}
                                {/if}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="card-foot">
        <div class="text-center">
            <button type="button" class="btn btn-defoult btn-raised" onclick="metCancelFallo()" title="Click para volver" alt="Click para volver">
                <span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar
            </button>
            <button type="button" class="btn btn-defoult btn-raised" onclick="metLimpiarDecision()" title="Click para cerrar la Planificación" alt="Click para cerrar la Planificación">Limpiar</button>
            <button type="button" class="btn btn-primary btn-raised" onclick="metGuardaDecision()" title="Click para anular la Planificación" alt="Click para anular la Planificación">
                <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Guardar
            </button>
        </div>
    </div>
</div>
<div id="tb_sanciones"></div>{*Contenedor para montar la grilla de personas sancionadas*}