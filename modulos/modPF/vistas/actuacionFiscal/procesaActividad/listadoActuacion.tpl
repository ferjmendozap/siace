<section class="style-default-bright">
    <div class="col-lg-12">
        <h2 class="text-primary">&nbsp;Listado Detalles de Actividades</h2>
    </div>
    <div class="section-body ">
        <div id="tb_listadoConsulta"></div>
    </div>
</section>
{* SECCIÓN DE JAVASCRIPT *}
<script type="text/javascript">
    var metMontaPlanificacion="", metListarPlanificaciones='';
    $(document).ready(function () {
        var url = '{$_Parametros.url}modPF/actuacionFiscal/procesaActividadCONTROL/';
        metListarPlanificaciones=function(){
            $('#tb_listadoConsulta').html('');
            $.post(url+'BuscaPlanificacionesMET',{ tipoProceso:{$tipoProceso} }, function (dato) {
                if (dato) {
                    $('#tb_listadoConsulta').html(dato);
                }
            });
        }
        metListarPlanificaciones();
        metMontaPlanificacion=function(idActuacion,opcion){
            $('#formModalLabel').html('<i class="md md-visibility"></i> Planificación');
            $.post(url+'DetalleActividadesMET', { idActuacion:idActuacion, tipoProceso: {$tipoProceso} }, function (dato) {
                $('#ContenidoModal').html('');
                $('#ContenidoModal').html(dato);
            });
        };
    });
</script>
