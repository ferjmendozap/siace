<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Persona - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>foto</th>
                                <th>Num Ident</th>
                                <th>Nombre 1</th>
                                <th>Apellido 1</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=persona from=$listado}
                                <tr id="idPersona{$persona.pk_num_persona}">
                                    <td><label>{$persona.ind_foto}</label></td>
                                    <td><label>{$persona.ind_cedula_documento}</label></td>
                                    <td><label>{$persona.ind_nombre1}</label></td>
                                    <td><label>{$persona.ind_apellido1}</label></td>
                                    <td>
                                        <i class="{if $persona.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                    </td>
                                    <td align="center">
                                        {if in_array('CV-01-03-01-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idPersona="{$persona.pk_num_persona}"
                                                    descipcion="El Usuario a Modificado registros de la persona" titulo="Modificar Persona">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        &nbsp;&nbsp;
                                        {if in_array('CV-01-03-01-03-V',$_Parametros.perfil)}
                                            <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idPersona="{$persona.pk_num_persona}"
                                                    descipcion="El Usuario esta viendo una Persona" titulo="<i class='icm icm-calculate2'></i> Ver Persona">
                                                <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        {if in_array('CV-01-03-01-04-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPersona="{$persona.pk_num_persona}"  boton="si, Eliminar"
                                                    descipcion="El usuario a eliminado a una persona" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la persona!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="6">
                                {if in_array('CV-01-03-01-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario a creado una Persona"  titulo="Crear Persona" id="nuevo" >
                                        <i class="md md-create"></i>&nbsp; Nueva Persona
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var url='{$_Parametros.url}modCV/maestros/personaCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idPersona:0 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post(url,{ idPersona: $(this).attr('idPersona') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
        $('#datatable1 tbody').on('click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post('{$_Parametros.url}modCV/maestros/personaCONTROL/verMET', { idPersona: $(this).attr('idPersona') }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idPersona=$(this).attr('idPersona');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCV/maestros/personaCONTROL/eliminarMET';
                $.post($url, { idPersona: idPersona },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idPersona'+dato['idPersona'])).html('');
                        swal("Eliminado!", "La persona fue eliminada satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });

    });
</script>