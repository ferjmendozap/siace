<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Persona - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Nombre y Apellidos</th>
                            <th>N° de Identificacion</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$listado}
                                    <tr idPersona="{$i.pk_num_persona}" nombre="{$i.ind_nombre1} {$i.ind_apellido1}">
                                        <td><label>{$i.ind_nombre1} {$i.ind_apellido1}</label></td>
                                        <td><label> {$i.ind_cedula_documento} </label></td>
                                    </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="2">
                                {if in_array('CV-01-03-01-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                            descipcion="el Usuario a creado una Persona"  titulo="Crear Persona" id="personavisita" >
                                        <i class="md md-create"></i>&nbsp; Nueva Persona
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<span class="clearfix"></span>

<script>
    $(document).ready(function() {
        var url='{$_Parametros.url}modCV/maestros/personaCONTROL/crearModificarMET';
        $('#personavisita').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idPersona:0, visita: 1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        $('#datatable2 tbody').on( 'click', 'tr', function () {
            var idPersona = $(this).attr('idPersona');

            $.post('{$_Parametros.url}modCV/visita/visitaCONTROL/registrarVisitaMET',{ idPersona:idPersona },function(dato){
                $('#ContenidoModal').html(dato);
            });
            console.log(idPersona);
        });

    });
</script>
