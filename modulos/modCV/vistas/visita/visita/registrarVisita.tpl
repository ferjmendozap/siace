<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<form action="{$_Parametros.url}modCV/visita/visitaCONTROL/registrarVisitaMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body ">
        <input type="hidden" value="1" name="valido" />

        <input type="hidden" value="{$idPersona}" name="form[int][idPersona]"/>
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-6" id="zonaFoto"></div>
                <div class="col-sm-6" id="upload_results" style="background-color:#eee;">
                    {if isset($formDB.ind_foto)}
                        <img src="{$_Parametros.url}publico/imagenes/modCV/fotosVisitantes/{$formDB.ind_foto}.jpg" onerror="this.src='{$_Parametros.url}publico/imagenes/modRH/fotos/{$formDB.ind_foto}'" width="300" height="250">
                    {else}
                        No Tiene Foto Cargada
                    {/if}
                </div>
                <div class="col-sm-12">
                    <input type=button value="Tomar Foto" id="tomarFoto">
                    &nbsp;&nbsp;
                    <input type=button value="Limpiar" onClick="webcam.reset()">
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-8">
                    <div class="form-group floating-label" id="ind_cedula_documentoError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_cedula_documento)}{$formDB.ind_cedula_documento}{/if}" name="form[alphaNum][ind_cedula_documento]" id="ind_cedula_documento"{if isset($ver) }readonly{/if}>
                        <label for="ind_cedula_documento"><i class="md md-perm-identity"></i> Cedula </label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_nombre1Error">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_nombre1)}{$formDB.ind_nombre1}{/if}" name="form[alphaNum][ind_nombre1]" id="ind_nombre1"{if isset($ver) }disabled{/if}>
                        <label for="ind_nombre1"><i class="fa fa-pencil"></i> Nombre </label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_apellido1Error">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_apellido1)}{$formDB.ind_apellido1}{/if}" name="form[alphaNum][ind_apellido1]" id="ind_apellido1"{if isset($ver) }disabled{/if}>
                        <label for="ind_apellido1"><i class="fa fa-pencil"></i> Apellido </label>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-3 form-group">
                        <label for="tipoVisita"><i class="md md-border-color"></i> Tipo de Visita: </label>
                    </div>
                    <div class="radio radio-styled tipoVisita">
                        <label>
                            <span>Particular</span> <input type="radio" class="tipoVisita" name="form[alphaNum][ind_tipo_visita]" value="P" checked>
                        </label>
                        <label>
                            <span>Organismo Público</span> <input type="radio" class="tipoVisita" name="form[alphaNum][ind_tipo_visita]" value="I">
                        </label>
                        <label>
                            <span>Empresa Privada</span> <input type="radio" class="tipoVisita" name="form[alphaNum][ind_tipo_visita]" value="E">
                        </label>
                        <label>
                            <span>Jubilados</span> <input type="radio" class="tipoVisita" name="form[alphaNum][ind_tipo_visita]" value="J">
                        </label>
                    </div>
                </div>
                <div id="organismo" style="display: none">
                    <div class="col-sm-6">
                        <div class="form-group"
                             id="id_enteError" style="margin-bottom: 10px;">
                            <label for="id_ente"><i
                                        class="md md-domain"></i> Organismo/Ente que </label>
                            <select id="id_ente" name="form[int][id_ente]" class="form-control select2" style="height: 27px;" {if (isset($ver) and $ver==1) OR isset($idSolicitud)} disabled {/if}>
                                <option value="">Seleccione..</option>
                                {foreach item=fila from=$ente}
                                    {if $fila.pk_num_ente eq $formDB.fk_a039_num_ente}
                                        <option value="{$fila.pk_num_ente}" selected>{if $fila.num_ente_padre!=0}  - {/if} {$fila.ind_nombre_ente} </option>
                                    {else}
                                        <option value="{$fila.pk_num_ente}">{if $fila.num_ente_padre!=0}  -  {/if} {$fila.ind_nombre_ente}</option>
                                    {/if}
                                {/foreach}
                            </select>
                        </div>
                    </div>

                </div>
                <div id="empresa" style="display: none">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <div class="col-lg-12">
                                <label for="proveedor" class="control-label">Proveedor Recomendado:</label>
                            </div>
                            <div class="col-lg-12" id="proveedorError">
                                <div class="col-lg-8">
                                    <input type="hidden" name="form[int][proveedor]" id="idProveedor" class="form-control proveedor" value="">
                                    <input type="text" disabled id="persona" class="form-control proveedor" value="">
                                </div>
                                <div class="col-lg-3">
                                    <input type="text" disabled id="cedula" class="form-control proveedor" value="">
                                </div>
                                <div class="col-lg-1">
                                    <button
                                            class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                            type="button"
                                            data-toggle="modal" data-target="#formModal2"
                                            titulo="Buscar Proveedor"
                                            url="{$_Parametros.url}modLG/compras/requerimientoCONTROL/proveedorMET/proveedor/"
                                            >
                                        <i class="md md-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group" id="fk_a006_num_miscelaneo_detalle_destinoError">
                        <label for="fk_a006_num_miscelaneo_detalle_destino"><i class="md md-map"></i> Destino</label>
                        <select id="fk_a006_num_miscelaneo_detalle_destino" name="form[alphaNum][fk_a006_num_miscelaneo_detalle_destino]" class="form-control select2-list select2">
                            <option value="">Seleccione el Destino</option>
                            {if isset($destino)}
                                {foreach item=i from=$destino}
                                    <option value="{$i.id}">{$i.nombre}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group" id="fk_a006_num_miscelaneo_detalle_motivo_visitaError">
                        <label for="fk_a006_num_miscelaneo_detalle_motivo_visita"><i class="md md-map"></i> Motivo</label>
                        <select id="fk_a006_num_miscelaneo_detalle_motivo_visita" name="form[int][fk_a006_num_miscelaneo_detalle_motivo_visita]" class="form-control select2-list select2">
                            <option value="">Seleccione el Motivo de la Visita</option>
                            {if isset($motivo)}
                                {foreach item=i from=$motivo}
                                        <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                </div>
                <div class="form-group floating-label col-lg-12" id="asisAError">
                    <div class="col-lg-11">
                        <div class="col-lg-3">
                            <label for="asisA"><i class="md md-people"></i>
                                Empleado
                            </label>
                        </div>
                        <div class="col-lg-9">
                            <input type="hidden" class="form-control" name="form[int][asisA]"
                                   value=""
                                   id="idEmpleado">
                            <input type="text" class="form-control disabled" id="personaResp" value="" disabled>
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <button {if isset($ver) and $ver==1} disabled {/if}
                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                type="button"
                                title="Buscar Empleado"
                                data-toggle="modal" data-target="#formModal2"
                                data-keyboard="false" data-backdrop="static"
                                titulo="Buscar Empleado"
                                url="{$_Parametros.url}modLG/maestros/almacenesCONTROL/empleadoMET/persona1/"
                                >
                            <i class="md md-search" style="color: #ffffff;"></i>
                        </button>
                    </div>
                </div>


            </div>
        </div>

        <div class="row">
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                  <div class="col-sm-5">
                    <div class="form-group floating-label" id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                  </div>
                </div>
            </div>
           <span class="clearfix"></span>
        </div>
  {if isset($ver)}

  {else}
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal">
            <i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
            <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
    </div>
  {/if}
</form>

<script type="text/javascript">
    $(document).ready(function() {
        var app = new AppFunciones();
        webcam.set_api_url( '{$_Parametros.url}modCV/visita/visitaCONTROL/cargarFotoMET/'+$('#ind_cedula_documento').val() );
        webcam.set_quality( 90 ); // JPEG quality (1 - 100)
        webcam.set_shutter_sound( true, '{$_Parametros.ruta_Complementos}JPEGCam/shutter.mp3' ); // play shutter click sound
        webcam.set_swf_url( '{$_Parametros.ruta_Complementos}JPEGCam/webcam.swf' ); // play shutter click sound
        $('#zonaFoto').html(webcam.get_html(320, 240));
        $("#formAjax").submit(function(){
            return false;
        });
        $('#modalAncho').css("width","60%");
        $('#tomarFoto').click(function () {
            webcam.freeze()
            setTimeout(function (){
                do_upload();
            }, 700);
        });
        $('#accion').click(function () {
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (dato) {
                var arrayCheck = ["num_estatus"];
                var arrayMostrarOrden = ['ind_cedula_documento', 'ind_nombre1','ind_apellido1','ind_direccion',
                                        'ind_email','ind_telefono'];
                if (dato['status'] == 'error') {
                    app.metValidarError(dato, 'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                } else if (dato['status'] == 'registrarSalida') {
                    app.metActualizarRegistroTablaJson('dataTablaJson', ' modificaron satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                } else if (dato['status'] == 'registrarEntrada') {
                    {if isset($visita)}

                    {else}
                        app.metNuevoRegistroTablaJson('dataTablaJson', 'Los datos de la Persona se guardaron satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                    {/if}
                }
            }, 'json');
        });
    });
</script>
<script language="JavaScript">
    webcam.set_hook( 'onComplete', 'my_completion_handler' );
    function do_upload() {
        // upload to server
        document.getElementById('upload_results').innerHTML = '<h1>Uploading...</h1>';
        webcam.upload();
    }
    function my_completion_handler(msg) {
        // extract URL out of PHP output
        if (msg.match(/(http\:\/\/\S+)/)) {
            var image_url = RegExp.$1;
            // show JPEG image in page
            $(document.getElementById('upload_results')).html('');
            document.getElementById('upload_results').innerHTML =
                    '<img src="' + image_url + '">';
            // reset camera for another shot
            webcam.reset();
        }
        else alert("PHP Error: " + msg);
    }
    $('.accionModal').click(function () {
        $('#modalAncho2').css("width", "60%");
        $('#formModalLabel2').html($(this).attr('titulo'));
        $.post($(this).attr('url'), { cargar: 0 }, function (dato) {
            $('#ContenidoModal2').html(dato);
        });
    });
    $(".tipoVisita").click(function(){
        var tipo = $(this).val();

//        $('option:selected').attr('');
        $('.select2').prop('selectedIndex',0);
        $('.proveedor').val('');

        if (tipo == 'P') {
            document.getElementById("organismo").style.display = "none";
            document.getElementById("empresa").style.display = "none";

        }else if(tipo == 'I'){
            document.getElementById("organismo").style.display = "inline";
            document.getElementById("empresa").style.display = "none";
        }else if(tipo == 'E'){
            document.getElementById("organismo").style.display = "none";
            document.getElementById("empresa").style.display = "inline";
        }else{

        }

    });
</script>