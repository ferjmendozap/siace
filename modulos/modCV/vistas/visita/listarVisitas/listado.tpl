<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Registros de Visitas</h2>
    </div>

    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Cédula.</th>
                            <th>Visitante</th>
                            <th>Destino</th>
                            <th>Motivo</th>
                            <th>Entradas</th>
                            <th>Accion</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th colspan="3">
                                {if in_array('CV-01-01',$_Parametros.perfil)}
                                    <button id="nuevo" class="logsUsuario btn ink-reaction btn-raised btn-info"
                                            data-toggle="modal"
                                            data-target="#formModal"
                                            data-keyboard="false"
                                            data-backdrop="static" titulo="Nuevo Registro">
                                        <i class="md md-create"></i> Nuevo Registro
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        var url='{$_Parametros.url}modCV/visita/visitaCONTROL';
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modCV/visita/listarVisitasCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "ind_cedula_documento" },
                    { "data": "nombre"},
                    { "data": "destino"},
                    { "data": "ind_motivo"},
                    { "data": "fec_fecha_entrada"},
                    { "orderable": false,"data": "acciones"}
                ]
        );
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idPersona:0 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.salida', function () {
            var idVisita= $(this).attr('idVisita');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCV/visita/visitaCONTROL/registrarSalidaMET';
                $.post($url, { idVisita: idVisita }, function(dato) {
                    if(dato['status']=='ok'){
                        app.metActualizarRegistroTablaJson('dataTablaJson', ' La persona a egresado del Organismo satisfactoriamente.', false, false);
//                        $(document.getElementById('id'+dato['idVisita'])).remove();
//                        swal("Salida!", "La persona a egresado del Organismo satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });


</script>

