<form action="" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body ">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idPersona}"/>
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-6" id="zonaFoto"></div>
                <div class="col-sm-6" id="upload_results" style="background-color:#eee;">
                    {if isset($formDB.ind_foto)}
                        <img src="{$_Parametros.url}publico/imagenes/modCV/fotosVisitantes/{$formDB.ind_foto}.jpg" onerror="this.src='{$_Parametros.url}publico/imagenes/modRH/fotos/{$formDB.ind_foto}'" width="300" height="250">
                    {else}
                        No Tiene Foto Cargada
                    {/if}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-8">
                    <div class="form-group floating-label" id="ind_cedula_documentoError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_cedula_documento)}{$formDB.ind_cedula_documento}{/if}" name="form[alphaNum][ind_cedula_documento]" id="ind_cedula_documento"{if isset($ver) }readonly{/if}>
                        <label for="ind_cedula_documento"><i class="md md-perm-identity"></i> Cedula </label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_nombre1Error">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_nombre1)}{$formDB.ind_nombre1}{/if}" name="form[alphaNum][ind_nombre1]" id="ind_nombre1"{if isset($ver) }disabled{/if}>
                        <label for="ind_nombre1"><i class="fa fa-pencil"></i> Nombre </label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_apellido1Error">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_apellido1)}{$formDB.ind_apellido1}{/if}" name="form[alphaNum][ind_apellido1]" id="ind_apellido1"{if isset($ver) }disabled{/if}>
                        <label for="ind_apellido1"><i class="fa fa-pencil"></i> Apellido </label>
                    </div>
                </div>
                <div id="organismo" style="display: none">
                    <div class="col-sm-6">
                        <div class="form-group"
                             id="id_enteError" style="margin-bottom: 10px;">
                            <label for="id_ente"><i
                                        class="md md-domain"></i> Organismo/Ente que Representa</label>
                            <select id="id_ente" name="form[int][id_ente]" class="form-control select2" style="height: 27px;" {if (isset($ver) and $ver==1) OR isset($idSolicitud)} disabled {/if}>
                                <option value="">Seleccione..</option>
                                {foreach item=fila from=$ente}
                                    {if $fila.pk_num_ente eq $formDB.fk_a039_num_ente}
                                        <option value="{$fila.pk_num_ente}" selected>{if $fila.num_ente_padre!=0}  - {/if} {$fila.ind_nombre_ente} </option>
                                    {else}
                                        <option value="{$fila.pk_num_ente}">{if $fila.num_ente_padre!=0}  -  {/if} {$fila.ind_nombre_ente}</option>
                                    {/if}
                                {/foreach}
                            </select>
                        </div>
                    </div>

                </div>
                <div class="col-sm-12">
                    <div class="form-group" id="fk_a006_num_miscelaneo_detalle_destinoError">
                        <label for="fk_a006_num_miscelaneo_detalle_destino"><i class="md md-map"></i> Destino</label>
                        <select id="fk_a006_num_miscelaneo_detalle_destino" name="form[int][fk_a006_num_miscelaneo_detalle_destino]" class="form-control select2-list select2"
                                {if isset($ver) }disabled{/if}>
                            <option value="">Seleccione el Destino</option>
                            {if isset($destino)}
                                {foreach item=i from=$destino}
                                    {if isset($formDB.fk_a004_num_dependencia) and $formDB.fk_a004_num_dependencia == $i.pk_num_dependencia}
                                        <option value="{$i.pk_num_dependencia}" selected>{$i.ind_dependencia}</option>
                                    {else}
                                        <option value="{$i.pk_num_dependencia}">{$i.ind_dependencia}</option>
                                    {/if}
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group" id="fk_a006_num_miscelaneo_detalle_motivo_visitaError">
                        <label for="fk_a006_num_miscelaneo_detalle_motivo_visita"><i class="md md-map"></i> Motivo</label>
                        <select id="fk_a006_num_miscelaneo_detalle_motivo_visita" name="form[int][fk_a006_num_miscelaneo_detalle_motivo_visita]" class="form-control select2-list select2"
                                {if isset($ver) }disabled{/if}>
                            <option value="">Seleccione el Motivo de la Visita</option>
                            {if isset($motivo)}
                                {foreach item=i from=$motivo}
                                    {if isset($formDB.fk_a006_num_miscelaneo_detalle_motivo_visita) and $formDB.fk_a006_num_miscelaneo_detalle_motivo_visita == $i.pk_num_miscelaneo_detalle}
                                        <option value="{$i.pk_num_miscelaneo_detalle}" selected>{$i.ind_nombre_detalle}</option>
                                    {else}
                                        <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                    {/if}
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                </div>
            </div>
        </div>
            <div class="row">
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group floating-label">
                                <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                                <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group floating-label" id="fec_ultima_modificacionError">
                                <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                                <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                            </div>
                        </div>
                    </div>
                </div>
                <span class="clearfix"></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
                    <i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
            </div>
</form>