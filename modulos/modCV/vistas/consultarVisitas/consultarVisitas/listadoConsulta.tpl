<div class="section-body contain-lg">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>...</th>
                        <th>Cedula</th>
                        <th>Visitante</th>
                        <th>Organo/Ente Externo</th>
                        <th>Dependencia Interna</th>
                        <th>Entradas</th>
                        <th>Salida</th>
                        <th>Accion</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=i from=$listado}
                        <tr id="id{$i.pk_num_registro_visita}">
                            <td><label><img class="img-circle width-0 ampliar_foto" src="{$_Parametros.url}publico/imagenes/modCV/fotosVisitantes/{$i.ind_foto}.jpg" onerror="this.src='{$_Parametros.url}publico/imagenes/modRH/fotos/{$i.ind_foto}'" title="Ampliar Foto" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" ind_foto="{$i.ind_foto}" titulo="{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}"/></label></td>
                            <td><label>{$i.ind_cedula_documento}</label></td>
                            <td><label>{$i.ind_nombre1} {$i.ind_apellido1}</label></td>
                            <td><label>{$i.ind_nombre_ente}</label></td>
                            <td><label>{$i.ind_destino}</label></td>
                            <td><label>{$i.fec_fecha_entrada}</label></td>
                            <td><label>{$i.fec_fecha_salida}</label></td>
                            <td align="center">
                                {if in_array('CV-01-01-02',$_Parametros.perfil)}
                                    <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" idVisita="{$i.pk_num_registro_visita}" title="Cosultar"
                                            descipcion="El Usuario esta viendo una Visita" titulo="<i class='icm icm-calculate2'></i> Consultar Visita">
                                        <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                    </button>
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="3">
                            <div align="rigth">
                                <button class="pdf logsUsuario btn ink-reaction btn-raised btn-primary" data-toggle="modal" data-target="#formModal"
                                        data-keyboard="false" data-backdrop="static" titulo="<i class='md md-print'></i> Reporte de Visitas">
                                    EXPORTAR PDF
                                </button>
                            </div>
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function() {
                var $url_ampliar_foto = '{$_Parametros.url}modCV/consultarVisitas/consultarVisitasCONTROL/AmpliarFotoMET';
                var url='{$_Parametros.url}modCV/consultarVisitas/consultarVisitasCONTROL/consultarVisitaMET';

                $('#modalAncho').css("width", "60%");
                $('#datatable1 tbody').on( 'click', '.ver', function () {
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $.post(url,{ idVisita: $(this).attr('idVisita'), ver:1},function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });

                //exportar PDF
                $('.pdf').click(function(){
                    $('#modalAncho').css("width", "80%");
                    var organismo = $('#org').val();       var dependencia = $('#dep').val();
                    var visitante = $('#codigo').val();    var desde = $('#desde').val();
                    var hasta = $('#hasta').val();         var motivo = $('#motivo').val();
                    var tipo = $('#tipoVisita').val();

                    var url='{$_Parametros.url}modCV/consultarVisitas/consultarVisitasCONTROL/imprimirReporteMET/?organismo='+organismo+'&'+'dependencia='+dependencia+'&'+'visitante='+visitante+'&'+'desde='+desde+'&'+'hasta='+hasta+'&'+'motivo='+motivo+'&'+'tipo='+tipo;
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $.post( url,{  },function(dato){
                        $('#ContenidoModal').html('<iframe frameborder="0" src="'+url+'" width="100%" height="540px"></iframe>');
                    });
                });

                //al dar click para ampliar la foto del empleado
                $('.ampliar_foto').click(function(){
                    $('#formModalLabel').html($(this).attr('titulo'));
                    $.post($url_ampliar_foto, { foto:$(this).attr('ind_foto')} ,function($dato){
                        $('#ContenidoModal').html($dato);
                    });
                });

            });
            function ampliar_foto(foto,nombre)
            {
                var $url_ampliar_foto = '{$_Parametros.url}modCV/consultarVisitas/consultarVisitasCONTROL/AmpliarFotoMET';
                $('#formModalLabel').html(nombre);
                $.post($url_ampliar_foto, { foto: foto} ,function($dato){
                    $('#ContenidoModal').html($dato);
                });
            }
        </script>

