<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Visitas
 * PROCESO: Registros de Persona
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Sergio Zabaleta                  |zsergio01@gmail.com                 |         0414-3638131           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-10-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class personaModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarSelect($codMaestro, $codDetalle)
    {
        $miscelaneoDetalle = $this->_db->query("
            SELECT
              a006.pk_num_miscelaneo_detalle
            FROM
              a006_miscelaneo_detalle AS a006
            INNER JOIN
              a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
            WHERE
              a005.cod_maestro='$codMaestro' AND
              a006.cod_detalle='$codDetalle'
        ");
        $miscelaneoDetalle->setFetchMode(PDO::FETCH_ASSOC);
        return $miscelaneoDetalle->fetch();
    }

    public function metMostrarPersona($idPersona)
    {
        $persona = $this->_db->query("
          SELECT
            a003_persona.*,
            a018_seguridad_usuario.ind_usuario,
            a036_persona_direccion.ind_direccion,
            a007_persona_telefono.ind_telefono
          FROM
            a003_persona
          INNER JOIN
            a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = a003_persona.fk_a018_num_seguridad_usuario
          LEFT JOIN
            a036_persona_direccion ON a003_persona.pk_num_persona = a036_persona_direccion.fk_a003_num_persona
          LEFT JOIN
            a007_persona_telefono ON a003_persona.pk_num_persona = a007_persona_telefono.fk_a003_num_persona

          WHERE
            pk_num_persona='$idPersona'
        ");
        //var_dump($persona);
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }

    public function metListarPersona()
    {
        $menu = $this->_db->query("
            SELECT
              *
            FROM
              a003_persona
            INNER JOIN
              a006_miscelaneo_detalle ON a003_persona.fk_a006_num_miscelaneo_det_tipopersona = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
            WHERE
              a006_miscelaneo_detalle.cod_detalle = 'PART'
        ");
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
    }

    public function metCrearPersona( $cedula,$nombre1,$apellido1,$direccion,$email,$foto,$telefono,$estatus )
    {
        $tipoPersona = $this->metMostrarSelect('TIP_PERSON', 'PART');
        $this->_db->beginTransaction();
        $nuevoRegistro = $this->_db->prepare("
                  INSERT INTO
                    a003_persona
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fec_ultima_modificacion=NOW(),
                    ind_cedula_documento=:ind_cedula_documento,
                    ind_nombre1=:ind_nombre1,
                    ind_apellido1=:ind_apellido1,
                    ind_email=:ind_email,
                    ind_foto=:ind_foto,
                    num_estatus=:num_estatus,
                    ind_tipo_persona='A',
                    fk_a006_num_miscelaneo_det_tipopersona=:fk_a006_num_miscelaneo_det_tipopersona
                ");

        $nuevoRegistro->execute(array(
            'fk_a006_num_miscelaneo_det_tipopersona'=> $tipoPersona['pk_num_miscelaneo_detalle'],
            'ind_cedula_documento'=> $cedula,
            'ind_nombre1'=> $nombre1,
            'ind_apellido1'=> $apellido1,
            'ind_email'=> $email,
            'ind_foto' => $foto,
            'num_estatus' => $estatus
        ));


        $idRegistro = $this->_db->lastInsertId();
        $registroDireccion = $this->_db->prepare("
                  INSERT INTO
                    a036_persona_direccion
                  SET
                    fk_a003_num_persona ='$idRegistro', ind_direccion=:ind_direccion
                ");
        $registroDireccion->execute(array(
            'ind_direccion' => $direccion

        ));

        $registroTelefono = $this->_db->prepare("
                  INSERT INTO
                    a007_persona_telefono
                  SET
                    fk_a003_num_persona ='$idRegistro', ind_telefono=:ind_telefono
                ");
        $registroTelefono->execute(array(
            'ind_telefono' => $telefono

        ));

        $error = $nuevoRegistro->errorInfo();
        $error1 = $registroDireccion->errorInfo();
        $error2 = $registroTelefono->errorInfo();

        if (!empty($error[1]) || !empty($error[2]) || !empty($error[3]) || !empty($error1[1]) || !empty($error1[2]) || !empty($error1[3])
            || !empty($error2[1]) || !empty($error2[2]) || !empty($error2[3])) {
            $this->_db->rollBack();
            return $error = array_merge($error, $error1, $error2);
        } else {
            $this->_db->commit();
            return $idRegistro;
        }
    }

    Public function metModificarPersona ($nombre1,$apellido1,$direccion,$email,$foto,$telefono,$estatus, $idPersona)
    {
        $this->_db->beginTransaction();
        $nuevoRegistro = $this->_db->prepare("
                  UPDATE
                       a003_persona
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    ind_nombre1=:ind_nombre1, ind_apellido1=:ind_apellido1, ind_email=:ind_email, ind_foto=:ind_foto, num_estatus=:num_estatus
                  WHERE
                  pk_num_persona='$idPersona'
            ");

        $nuevoRegistro->execute(array(
            'ind_nombre1'=> $nombre1,
            'ind_apellido1'=> $apellido1,
            'ind_email'=> $email,
            'ind_foto' => $foto,
            'num_estatus' => $estatus

        ));

        $registroDireccion = $this->_db->prepare("
                  UPDATE
                      a036_persona_direccion
                  SET
                     ind_direccion =:ind_direccion

                  WHERE
                   fk_a003_num_persona = '$idPersona'
                ");

        $registroDireccion->execute(array(
            'ind_direccion' => $direccion

        ));

        $registroTelefono = $this->_db->prepare("
                  UPDATE
                     a007_persona_telefono
                  SET
                     ind_telefono=:ind_telefono

                  WHERE
                   fk_a003_num_persona = '$idPersona'
                ");

        $registroTelefono->execute(array(
            'ind_telefono' => $telefono

        ));


        $error = $nuevoRegistro->errorInfo();
        $error1 = $registroDireccion->errorInfo();
        $error2 = $registroTelefono->errorInfo();
        if (!empty($error[1]) && !empty($error[2]) && !empty($error[3]) && !empty($error1[1]) && !empty($error1[2]) && !empty($error1[3])
            && !empty($error2[1]) && !empty($error2[2]) && !empty($error2[3])) {

            $this->_db->rollBack();
            return $error = array_merge($error, $error1, $error2);
        } else {
            $this->_db->commit();
            return $idPersona;
        }
    }

    public function metEliminarPersona($idPersona)
    {
        $this->_db->beginTransaction();
        $elimar2 = $this->_db->prepare("
                DELETE FROM a007_persona_telefono WHERE fk_a003_num_persona=:fk_a003_num_persona
            ");
        $elimar2->execute(array(
            'fk_a003_num_persona' => $idPersona
        ));
        $elimar1 = $this->_db->prepare("
                DELETE FROM a036_persona_direccion WHERE fk_a003_num_persona=:fk_a003_num_persona
            ");
        $elimar1->execute(array(
            'fk_a003_num_persona' => $idPersona
        ));

        $elimar = $this->_db->prepare("
                DELETE FROM a003_persona WHERE pk_num_persona =:pk_num_persona
            ");
        $elimar->execute(array(
            'pk_num_persona' => $idPersona
        ));
        $error2 = $elimar2->errorInfo();
        $error1 = $elimar1->errorInfo();
        $error = $elimar->errorInfo();
        if (!empty($error2[1]) || !empty($error2[2]) || !empty($error2[3]) || !empty ($error1[1]) || !empty($error1[2]) || !empty($error1[3])
            || !empty($error[1]) || !empty($error[2]) || !empty($error[3])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idPersona;
        }

    }

}
