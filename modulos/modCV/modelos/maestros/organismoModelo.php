<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Visitas
 * PROCESO: Registros de Organismos y Entes
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Sergio Zabaleta                  |zsergio01@gmail.com                 |         0414-3638131           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        19-10-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class organismoModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metMostrarOrganismo($idOrganismo)
    {
        $organismo = $this->_db->query("
          SELECT
            a001_organismo.*,
            a018_seguridad_usuario.ind_usuario,
            a029_organismo_telefono.ind_telefono
          FROM
            a001_organismo
          INNER JOIN
            a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = a001_organismo.fk_a018_num_seguridad_usuario
          LEFT JOIN
            a029_organismo_telefono ON a001_organismo.pk_num_organismo = a029_organismo_telefono.fk_a001_num_organismo

          WHERE
            pk_num_organismo='$idOrganismo'
        ");
        $organismo->setFetchMode(PDO::FETCH_ASSOC);
        return $organismo->fetch();
    }

    public function metListarOrganismo()
    {
        $organismo = $this->_db->query(
            "SELECT * FROM a001_organismo"
        );
        $organismo->setFetchMode(PDO::FETCH_ASSOC);
        return $organismo->fetchAll();
    }

    public function metCrearOrganismo($nombre, $direccion, $telefono, $status)
    {
        $this->_db->beginTransaction();
        $nuevoRegistro = $this->_db->prepare("
                  INSERT INTO
                    a001_organismo
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),num_estatus=:num_estatus,
                    ind_descripcion_empresa=:ind_descripcion_empresa, ind_direccion=:ind_direccion
                ");
        $nuevoRegistro->execute(array(
            'num_estatus' => $status,
            'ind_descripcion_empresa' => $nombre,
            'ind_direccion' => $direccion


        ));
        $idRegistro = $this->_db->lastInsertId();

        $registroTelefono = $this->_db->prepare("
                  INSERT INTO
                    a029_organismo_telefono
                  SET
                    fk_a001_num_organismo='$idRegistro', ind_telefono=:ind_telefono
                ");
        $registroTelefono->execute(array(
            'ind_telefono' => $telefono

        ));


        $error = $nuevoRegistro->errorInfo();
        $error1 = $registroTelefono->errorInfo();
        if (!empty($error[1]) && !empty($error[2]) && !empty($error1[1]) && !empty($error1[2])) {
            $this->_db->rollBack();
            return $error = array_merge($error, $error1);
        } else {
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarOrganismo($nombre, $direccion, $telefono, $status,$idOrganismo)
    {
        $this->_db->beginTransaction();
        $nuevoRegistro = $this->_db->prepare("
                      UPDATE
                        a001_organismo
                      SET
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                        num_estatus=:num_estatus, ind_descripcion_empresa=:ind_descripcion_empresa, ind_direccion=:ind_direccion
                      WHERE
                        pk_num_organismo='$idOrganismo'
            ");
        $nuevoRegistro->execute(array(
            'num_estatus' => $status,
            'ind_descripcion_empresa' => $nombre,
            'ind_direccion' => $direccion

        ));

        $registroTelefono = $this->_db->prepare("
                  UPDATE
                    a029_organismo_telefono
                  SET
                     ind_telefono=:ind_telefono

                  WHERE
                    fk_a001_num_organismo='$idOrganismo'

                ");
        $registroTelefono->execute(array(
            'ind_telefono' => $telefono

        ));


        $error = $nuevoRegistro->errorInfo();
        $error1 = $registroTelefono->errorInfo();
        if (!empty($error[1]) && !empty($error[2]) && !empty($error1[1]) && !empty($error1[2])) {
            $this->_db->rollBack();
            return $error = array_merge($error, $error1);
        } else {
            $this->_db->commit();
            return $idOrganismo;
        }
    }

    public function metEliminarOrganismo($idOrganismo)
    {
        $this->_db->beginTransaction();
        $elimar1 = $this->_db->prepare("
                DELETE FROM a029_organismo_telefono WHERE fk_a001_num_organismo=:fk_a001_num_organismo
            ");
        $elimar1->execute(array(
            'fk_a001_num_organismo' => $idOrganismo
        ));

        $elimar = $this->_db->prepare("
                DELETE FROM a001_organismo WHERE pk_num_organismo =:pk_num_organismo
            ");
        $elimar->execute(array(
            'pk_num_organismo' => $idOrganismo
        ));





        $error1 = $elimar1->errorInfo();
        $error = $elimar->errorInfo();
        if (!empty($error1[1]) && !empty($error1[2]) && !empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idOrganismo;
        }

    }

}
