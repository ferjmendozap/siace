<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Visita
 * PROCESO: Listar visitas
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Sergio Zabaleta                            |zsergio01@gmail.com                 |0414-3638131                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |08/12/2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
class consultarVisitasModelo extends Modelo
{

    public $atMiscelaneoModelo;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo=new miscelaneoModelo();
    }

    public function metListaPersona()
    {
        $menu = $this->_db->query("
            SELECT
              a003_persona.*
            FROM
              a003_persona
            INNER JOIN cv_b001_registro_visita ON a003_persona.pk_num_persona = cv_b001_registro_visita.fk_a003_num_persona
            GROUP BY a003_persona.pk_num_persona
         ");
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
    }

    public function metListarEntes()
    {
        $menu = $this->_db->query("
            SELECT
            a039_ente.*
            FROM
             a039_ente
            INNER JOIN cv_b001_registro_visita ON a039_ente.pk_num_ente = cv_b001_registro_visita.fk_a039_num_ente
            GROUP BY a039_ente.pk_num_ente
            ");
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
    }

    public function metListarDependencias()
    {
        $organismo = $this->_db->query(
            "SELECT * FROM a004_dependencia"
        );
        $organismo->setFetchMode(PDO::FETCH_ASSOC);
        return $organismo->fetchAll();
    }

    public function metMostrarVisita($idPersona=false)
    {
        if ($idPersona) {
            $were ="WHERE cv_b001_registro_visita.fk_a003_num_persona='$idPersona' and cv_b001_registro_visita.fec_fecha_salida IS NULL";
        }
        else{
            $were='';
        }
        $visita = $this->_db->query("
          SELECT
            cv_b001_registro_visita.*,
            a018_seguridad_usuario.ind_usuario,
            a003_persona.ind_foto,
            a003_persona.ind_nombre1,
            a003_persona.ind_apellido1,
            personaAnfitrion.ind_apellido1 AS personaAnfitrionApellido,
            personaAnfitrion.ind_nombre1 AS personaAnfitrionNombre,
            motivo.ind_nombre_detalle AS ind_motivo
          FROM
            cv_b001_registro_visita
          INNER JOIN
            a006_miscelaneo_detalle AS motivo ON motivo.pk_num_miscelaneo_detalle = cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_motivo_visita
          INNER JOIN
            a003_persona ON a003_persona.pk_num_persona = cv_b001_registro_visita.fk_a003_num_persona
          INNER JOIN
            rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = cv_b001_registro_visita.fk_rh_b001_empleado_anfitrion
          INNER JOIN
            a003_persona AS personaAnfitrion ON personaAnfitrion.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
          INNER JOIN
            a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = cv_b001_registro_visita.fk_a018_num_seguridad_usuario
          LEFT JOIN
            a039_ente ON a039_ente.pk_num_ente = cv_b001_registro_visita.fk_a039_num_ente
          LEFT JOIN
            a006_miscelaneo_detalle AS destino ON destino.pk_num_miscelaneo_detalle = cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_destino
           $were
        ");
       // var_dump($visita);
        $visita->setFetchMode(PDO::FETCH_ASSOC);
        if($idPersona){
            return $visita->fetch();
        }else{
            return $visita->fetchAll();
        }
    }

    public function metConsultarVisita($idVisita=false)
    {

        $visita = $this->_db->query("
          
          SELECT
              cv_b001_registro_visita.*,
              a018_seguridad_usuario.ind_usuario,
              a003_persona.ind_foto,
              a003_persona.ind_cedula_documento,
              a003_persona.ind_nombre1,
              a003_persona.ind_apellido1,
              personaAnfitrion.ind_apellido1 AS personaAnfitrionApellido,
              personaAnfitrion.ind_nombre1 AS personaAnfitrionNombre,
              motivo.ind_nombre_detalle AS ind_motivo
            FROM
              cv_b001_registro_visita
            INNER JOIN
              a006_miscelaneo_detalle AS motivo ON motivo.pk_num_miscelaneo_detalle = cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_motivo_visita
            INNER JOIN
              a003_persona ON a003_persona.pk_num_persona = cv_b001_registro_visita.fk_a003_num_persona
            INNER JOIN
              a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = cv_b001_registro_visita.fk_a018_num_seguridad_usuario
            LEFT JOIN
              rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = cv_b001_registro_visita.fk_rh_b001_empleado_anfitrion
            LEFT JOIN
              a003_persona AS personaAnfitrion ON personaAnfitrion.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            LEFT JOIN
              a039_ente ON a039_ente.pk_num_ente = cv_b001_registro_visita.fk_a039_num_ente
            LEFT JOIN
              a006_miscelaneo_detalle AS destino ON destino.pk_num_miscelaneo_detalle = cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_destino
              
          WHERE
          cv_b001_registro_visita.pk_num_registro_visita='$idVisita'
        ");
        $visita->setFetchMode(PDO::FETCH_ASSOC);
        return $visita->fetch();

    }

    /**
     * @param bool|false $organismo
     * @param bool|false $dependencia
     * @param bool|false $visitante
     * @param bool|false $desde
     * @param bool|false $hasta
     * @param bool|false $motivo
     * @return array
     */
    public function metBuscarVisita($ente = false ,$dependencia = false, $visitante = false,
                                    $desde = false, $hasta = false, $motivo = false, $tipo = false)
    {
        $filtro="";
        if ($ente) {
            $filtro .= "AND cv_b001_registro_visita.fk_a039_num_ente = '$ente' ";
        }
        if ($dependencia) {
            $pos = strpos($dependencia, 'D');
            if ($pos === false) {
                $destino = preg_replace('/[^0-9 .,]/', '', $dependencia);
                $filtro .= "AND cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_destino = '$destino' ";
            } else {
                $dep = preg_replace('/[^0-9 .,]/', '', $dependencia);
                $filtro .= "AND  cv_b001_registro_visita.fk_a004_num_dependencia = '$dep' ";
            }
        }
        if ($visitante) {
            $filtro .= "AND a003_persona.pk_num_persona = '$visitante' ";
        }
        if ($desde and $hasta){
            $filtro .= "AND cv_b001_registro_visita.fec_fecha_entrada >= '$desde 00:00:00'
                      AND cv_b001_registro_visita.fec_fecha_entrada <= '$hasta 23:59:59' ";
        }
        if ($motivo) {
            $filtro .= "AND cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_motivo_visita = '$motivo' ";
        }
        if ($tipo) {
            $filtro .= "AND cv_b001_registro_visita.ind_tipo_visita = '$tipo' ";
        }
        $visita = $this->_db->query("
          SELECT
            cv_b001_registro_visita.*,
            a018_seguridad_usuario.ind_usuario,
            a003_persona.ind_foto,
            a003_persona.ind_nombre1,
            a003_persona.ind_apellido1,
            a003_persona.ind_cedula_documento,
            motivo.ind_nombre_detalle AS ind_motivo,
            concat_ws(' ',destino.ind_nombre_detalle,a004_dependencia.ind_dependencia)
             AS ind_destino,
             a039_ente.ind_nombre_ente
          FROM
            cv_b001_registro_visita
          INNER JOIN
            a003_persona ON a003_persona.pk_num_persona = cv_b001_registro_visita.fk_a003_num_persona
          INNER JOIN
            a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = a003_persona.fk_a018_num_seguridad_usuario
          INNER JOIN
            a006_miscelaneo_detalle AS motivo ON motivo.pk_num_miscelaneo_detalle = cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_motivo_visita
          LEFT JOIN
            a006_miscelaneo_detalle AS destino ON destino.pk_num_miscelaneo_detalle = cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_destino
          LEFT JOIN 
            a004_dependencia ON a004_dependencia.pk_num_dependencia = cv_b001_registro_visita.fk_a004_num_dependencia
          LEFT JOIN
            a039_ente ON a039_ente.pk_num_ente = cv_b001_registro_visita.fk_a039_num_ente
          WHERE 1 $filtro
        ");
//        var_dump($dependencia);
        $visita->setFetchMode(PDO::FETCH_ASSOC);
        return $visita->fetchAll();

    }

    public function metListarVisita()
    {
        $registro = $this->_db->query(
            "SELECT * FROM cv_b001_registro_visita"
        );
        $registro->setFetchMode(PDO::FETCH_ASSOC);
        return $registro->fetchAll();
    }

    public function metListarDependenciaVisita()
    {
        $registro = $this->_db->query(
            "SELECT * 
            FROM a004_dependencia
            INNER JOIN cv_b001_registro_visita ON cv_b001_registro_visita.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
            GROUP BY a004_dependencia.pk_num_dependencia"
        );
        $registro->setFetchMode(PDO::FETCH_ASSOC);
        return $registro->fetchAll();
    }

    public function metListarDestinoVisita()
    {
        $registro = $this->_db->query(
            "SELECT * 
            FROM a006_miscelaneo_detalle
            INNER JOIN cv_b001_registro_visita ON cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_destino = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
            GROUP BY a006_miscelaneo_detalle.pk_num_miscelaneo_detalle"
        );
        $registro->setFetchMode(PDO::FETCH_ASSOC);
        return $registro->fetchAll();
    }



}
