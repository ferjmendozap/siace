<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Visitas
 * PROCESO: Registros de Visitas
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Sergio Zabaleta                  |zsergio01@gmail.com                 |         0414-3638131           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-10-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class personaControlador extends Controlador
{
    private $atPersona;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atPersona = $this->metCargarModelo('persona','maestros');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $complementosJs[] = 'JPEGCam/webcam';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->assign('listado', $this->atPersona->metListarPersona());
        $this->atVista->metRenderizar('listado');
    }

    public function metConsultaCne()
    {

        $idCedula=$this->metObtenerInt('idCedula');
        $url="http://www.cne.gov.ve/web/registro_electoral/ce.php?nacionalidad=V&cedula=$idCedula";
        // Compruebo si existe el modulo de curl
        if(!in_array('curl', get_loaded_extensions())) {
            $validacion['status'] = 'errorCurl';
            echo json_encode($validacion);
            exit;
        }


        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
// almacene en una variable
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, FALSE);
// Para informar todo lo relacionado al header de la conexion
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $xxx1 = curl_exec($ch);
        curl_close($ch);
// Quitamos todas las etiquetas html existente dentro del retornado
        $page = strip_tags($xxx1);
// Dividimos el resultado en arreglos donde encuentre :
        $info = explode(":", $page);

// Dividimos este un resultado de la cedula en dos para sacar nac y cedula
        $cn=explode('-',substr(trim($info[1]),0,-6));

        if(isset($info[2])){
            $persona = explode(' ',trim($info[2]));
            $rows=count($persona);
            $rows2=count($info);

            // Procedimiento cuando devuelve un nombre completo ejemplo
            //Array ( [0] => ADRIANA [1] => DEL [2] => CARMEN [3] => VAAMONDE [4] => MUÃ‘OZ Estado ) 5

            if($rows==5){
                $nombre1=$persona[0];
                $nombre2=$persona[1].' '.$persona[2];
                $apellido1=$persona[3];
                $apellido2=$persona[4];
                $cual = 1;
                // Procedimiento cuando devuelve los nombre completo de forma normal
            }elseif($rows==4){
                $nombre1=$persona[0];
                $nombre2=$persona[1];
                $apellido1=$persona[2];
                $apellido2=$persona[3];
                $cual = 2;
            }else{
                // Procedimiento cuando solo no esta registrado en el CNE que devuelve arreglos diferente a los demás
                // sobreescribo la variable $info debido que el comportamiento es diferente
                $info = explode(' ',$page);
                $rows2=count($info);
                $ced=substr($info[1],0,-6);
                $nombre1=substr($info[9],0,-7);
                $nombre2=substr($info[11],0,-7);
                $apellido1=substr($info[13],0,-7);
                $apellido2=substr($info[15],0,-7);
                $cual = 3;
            }

            $cantCar = strlen($apellido2);
            $ape2='';
            for ($i=0;$i<$cantCar;$i++){
                if(substr($apellido2,$i,1)!='
'){
                    $ape2.=substr($apellido2,$i,1);
                } else {

                    $i = $cantCar;
                }
            }
            $validacion['nombres'] = $nombre1." ".$nombre2;
            $validacion['apellidos'] = $apellido1." ".$ape2;
            $validacion['status'] = 'correcto';
            $validacion['cual'] = $cual;
            $validacion['ape2'] = $ape2;
            $validacion['info'] = $info;
        } else {
            $validacion['status'] = 'errorIns';
        }



        echo json_encode($validacion);
        exit;



    }

    public function metValidarCorreo($correo)
    {
        $cont = strlen($correo);
        $arroba = '';
        $punto = '';
        for ($i=0; $i<$cont; $i++){
            if(substr($correo,$i,1)=='@'){
                $arroba = 'ok';
            }
            if(substr($correo,$i,4)=='.com'){
                $punto = 'ok';
            }
            if(substr($correo,$i,5)=='@.com'){
                $arroba = '';
                $punto = '';
            }
        }
        if ($punto == 'ok' AND $arroba=='ok'){
            return 1;
        } else {
            return 0;
        }
    }

    public function metCrearModificar($visita=false)
    {
        header( "Expires: Mon, 20 Dec 1998 01:00:00 GMT" );
        header( "Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT" );
        header( "Cache-Control: no-cache, must-revalidate" );
        header( "Pragma: no-cache" );
        $complementoJs =array(
            'inputmask/jquery.inputmask.bundle.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementoJs);

        $valido=$this->metObtenerInt('valido');
        $visita=$this->metObtenerInt('visita');
        $idPersona=$this->metObtenerInt('idPersona');
        if($valido==1){
            $this->metValidarToken();
            $excepcion=array('num_estatus','ind_email');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$excepcion);
            $ind=$this->metValidarFormArrayDatos('form','int',$excepcion);
            $formula=$this->metValidarFormArrayDatos('form','formula',$excepcion);

            if ($alphaNum != null && $ind == null && $formula == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $formula == null) {
                $validacion = $ind;
            } elseif ($alphaNum != null && $ind != null && $formula == null) {
                $validacion = array_merge($alphaNum, $ind);
            } elseif ($alphaNum != null && $formula != null && $ind == null) {
                $validacion = array_merge($alphaNum, $formula);
            } elseif ($ind != null && $formula != null && $alphaNum == null) {
                $validacion = array_merge($ind, $formula);
            } else {
                $validacion = array_merge($ind, $formula, $alphaNum);
            }

            if($validacion['ind_email']){
                $val = $this->metValidarCorreo($validacion['ind_email']);
                if($val==0){
                    $validacion['ind_email'] = 'error';
                }
            }


            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }

            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            }

            if(!isset($val)){
                $val = null;
            }

            if($idPersona==0){
                $id=$this->atPersona->metCrearPersona($validacion['ind_cedula_documento'],$validacion['ind_nombre1'],$validacion['ind_apellido1'],
                                                       $validacion['ind_direccion'],$val,$validacion['ind_cedula_documento'],$validacion['ind_telefono'],$validacion['num_estatus']);
                if($visita==1){
                    $validacion['status']='visita';
                }else{
                    $validacion['status']='nuevo';
                }
            }else{
                $id=$this->atPersona->metModificarPersona($validacion['ind_nombre1'],$validacion['ind_apellido1'],
                                                            $validacion['ind_direccion'],$validacion['ind_email'],$validacion['ind_cedula_documento'],$validacion['ind_telefono'],$validacion['num_estatus'],$idPersona);
                $validacion['status']='modificar';
            }

            if(is_array($id)){

                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['idPersona']=$id;
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idPersona']=$id;

            echo json_encode($validacion);
            exit;
        }


        if($idPersona!=0){
            $this->atVista->assign('formDB',$this->atPersona->metMostrarPersona($idPersona));
            $this->atVista->assign('idPersona',$idPersona);
            $this->atVista->assign('visita',$visita);
        }
        $this->atVista->assign('visita',$visita);
        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    public function metVer()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $idPersona = $this->metObtenerInt('idPersona');

        $this->atVista->assign('formDB',$this->atPersona->metMostrarPersona($idPersona));
        $this->atVista->assign('idPersona', $idPersona);
        $this->atVista->assign('ver', 1);
        $this->atVista->metRenderizar('CrearModificar', 'modales');
    }

    public function metEliminar()
    {
        $idPersona = $this->metObtenerInt('idPersona');
        if($idPersona!=0){
            $id=$this->atPersona->metEliminarPersona($idPersona);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero la persona posee otros registros no se puede eliminar se sujiere desactivar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idPersona'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $urlfoto = BASE_URL . 'publico/imagenes'.DS.'modCV'.DS.'fotosVisitantes'.DS;
        $sql = "
            SELECT
              *,
              CONCAT('<img width=\"60\" height=\"60\" src=$urlfoto',a003_persona.ind_foto,'.jpg>') AS ind_foto,
              a003_persona.num_estatus
            FROM
              a003_persona
            INNER JOIN
              a006_miscelaneo_detalle ON a003_persona.fk_a006_num_miscelaneo_det_tipopersona = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
            WHERE
              a006_miscelaneo_detalle.cod_detalle = 'PART'
        ";
        if ($busqueda['value']) {
            $sql .= " AND
                        (
                        ind_cedula_documento LIKE '%$busqueda[value]%' OR
                        ind_nombre1 LIKE '%$busqueda[value]%' OR
                        ind_apellido1 LIKE '%$busqueda[value]%'
                        )
                        ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('ind_foto','ind_cedula_documento','ind_nombre1','ind_apellido1','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_persona';

        if (in_array('CV-01-03-01-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static" idPersona="'.$clavePrimaria.'" title="Editar"
                        descipcion="El Usuario a Modificado registros de la persona" titulo="Modificar Persona">
                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('CV-01-03-01-03-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static" idPersona="'.$clavePrimaria.'" title="Cosultar"
                        descipcion="El Usuario esta viendo una Persona" titulo="<i class=\'icm icm-calculate2\'></i> Ver Persona">
                    <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('CV-01-03-01-04-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" title="Eliminar" idPersona="'.$clavePrimaria.'"  boton="si, Eliminar"
                        descipcion="El usuario a eliminado a una persona" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la persona!!">
                    <i class="md md-delete" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }

}
