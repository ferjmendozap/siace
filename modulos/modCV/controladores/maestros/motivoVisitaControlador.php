<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Visitas
 * PROCESO: Registros de Motivos de Visitas
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Sergio Zabaleta                  |zsergio01@gmail.com                 |         0414-3638131           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        13-10-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class motivoVisitaControlador extends Controlador
{
    private $atMotivoVisita;

    public function __construct()
    {
        parent::__construct();
        $this->atMotivoVisita=$this->metCargarModelo('motivoVisita','maestros');
    }

    public function metIndex()
    {

            $complementosCss = array(
                'DataTables/jquery.dataTables',
                'DataTables/extensions/dataTables.colVis941e',
                'DataTables/extensions/dataTables.tableTools4029',
            );
            $js[] = 'materialSiace/core/demo/DemoTableDynamic';
            $this->atVista->metCargarCssComplemento($complementosCss);
            $this->atVista->metCargarJs($js);

        $this->atVista->assign('listado',$this->atMotivoVisita->metListarMotivoVisita());

            $this->atVista->metRenderizar('listado');

    }

    public function metCrearModificar()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);

        $valido=$this->metObtenerInt('valido');
        $idMotivo=$this->metObtenerInt('idMotivo');
        if($valido==1){
            $this->metValidarToken();
            $excepcion=array('num_estatus');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
            $ind=$this->metValidarFormArrayDatos('form','int',$excepcion);
            if($ind!=null && $alphaNum == null){
                $validacion=$ind;
            }elseif($ind==null && $alphaNum != null){
                $validacion=$alphaNum;
            }else{
                $validacion=array_merge($ind, $alphaNum);
            }

            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }

            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            }

            if($idMotivo==0){
                $id=$this->atMotivoVisita->metCrearMotivoVisita($validacion['ind_detalle'],$validacion['num_estatus']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atMotivoVisita->metModificarMotivoVisita($validacion['ind_detalle'],$validacion['num_estatus'],$idMotivo);
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                var_dump($id);
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idMotivo']=$id;

            echo json_encode($validacion);
            exit;
        }

        if($idMotivo!=0){
            $this->atVista->assign('formDB',$this->atMotivoVisita->metMostrarMotivoVisita($idMotivo));
            $this->atVista->assign('idMotivo',$idMotivo);
        }

        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    public function metEliminar()
    {
        $idMotivo = $this->metObtenerInt('idMotivo');
        if($idMotivo!=0){
            $id=$this->atMotivoVisita->metEliminarMotivoVisita($idMotivo);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Motivo se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idMotivo'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
}