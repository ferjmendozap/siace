<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Visitas
 * PROCESO: Registro de Visistas
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Sergio Zabaleta                            |zsergio01@gmail.com                 |0414-3638131                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |08-10-2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class consultarVisitasControlador extends Controlador
{
    private $atVisita;
    private $atFPDF;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atVisita = $this->metCargarModelo('consultarVisitas','consultarVisitas');
        $this->metObtenerLibreria('cabecera','modCV');
        $this->atFPDF = new pdf('L','mm','Letter');
    }

    public function metIndex()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado', $this->atVisita->metMostrarVisita());
        $this->atVista->assign('ente', $this->atVisita->metListarEntes());
        $destino=array();
        foreach($this->atVisita->metListarDependenciaVisita() AS $i){
            $destino[] = array(
                'id'=>'D'.$i['pk_num_dependencia'],
                'nombre'=>$i['ind_dependencia']
            );
        }
        foreach($this->atVisita->metListarDestinoVisita() AS $i){
            $destino[] = array(
                'id'=>'M'.$i['pk_num_miscelaneo_detalle'],
                'nombre'=>$i['ind_nombre_detalle']
            );
        }

        $this->atVista->assign('selectDependencia', $destino);
        #$this->atVista->assign('selectDependencia', $this->atVisita->atMiscelaneoModelo->metMostrarSelect('CVDESVIS'));

        $this->atVista->assign('selectMotivo', $this->atVisita->atMiscelaneoModelo->metMostrarSelect('CVMOTVIS'));
        $this->atVista->metRenderizar('listado');

    }

    public function metListaPersona($persona, $idCampo = false)
    {
        $this->atVista->assign('lista', $this->atVisita->metListaPersona());
        $this->atVista->assign('listaPersona', $persona);
        $this->atVista->assign('idCampo', $idCampo);
        $this->atVista->metRenderizar('listadoVisitante', 'modales');
    }

    public function metConsultarVisita()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $idVisita=$this->metObtenerInt('idVisita');
        $ver=$this->metObtenerInt('ver');

        $formBD=$this->atVisita->metConsultarVisita($idVisita);
        $destino=$this->atVisita->metListarDependenciaVisita($formBD['fk_a004_num_dependencia']);

        $this->atVista->assign('idVisita',$idVisita);
        $this->atVista->assign('ver',$ver);
        $this->atVista->assign('organismo', $this->atVisita->metListarEntes());
        $this->atVista->assign('destino', $destino);
        $this->atVista->assign('motivo', $this->atVisita->atMiscelaneoModelo->metMostrarSelect('CVMOTVIS'));

        $this->atVista->assign('formDB', $formBD);

        $this->atVista->metRenderizar('consultarVisita','modales');
    }


    public function metActualizar()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $organismo = $this->metObtenerInt('organismo');
        $dependencia = $this->metObtenerAlphaNumerico('dependencia');
        $visitante = $this->metObtenerInt('visitante');
        $motivo = $this->metObtenerInt('motivo');
        $desde = $this->metObtenerFormulas('desde');
        $hasta = $this->metObtenerFormulas('hasta');
        $tipo = $this->metObtenerFormulas('tipoVisita');
        $this->atVista->assign('listado', $this->atVisita->metBuscarVisita($organismo,$dependencia,$visitante,
                                                                             $desde,$hasta,$motivo,$tipo));
        $this->atVista->metRenderizar('listadoConsulta');


    }

    public function metImprimirReporte()
    {
        $organismo = $_GET['organismo']; $dependencia = $_GET['dependencia'];
        $visitante = $_GET['visitante']; $desde = $_GET['desde'];
        $hasta = $_GET['hasta']; $motivo = $_GET['motivo'];
        $tipo=$_GET['tipo'];


        //
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->AddPage();
        $this->atFPDF->SetAutoPageBreak(true,10);

        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 10, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetXY(33, 10); $this->atFPDF->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->atFPDF->Cell(10,5,'Fecha:',0,0,'');$this->atFPDF->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->atFPDF->SetXY(33, 15); $this->atFPDF->Cell(200, 5, utf8_decode('ÁREA DE MENSAJERÍA Y CORRESPONDENCIA'), 0, 0, 'L');
        $this->atFPDF->Cell(10,5,utf8_decode('Página:'.$this->atFPDF->PageNo().' de {nb}'),0,1,'');
        $this->atFPDF->SetXY(233, 20);
        $this->atFPDF->Cell(7,5,utf8_decode('Año:'),0,0,'L');$this->atFPDF->Cell(5,5,date('Y'),0,1,'L');
        $this->atFPDF->Ln(2);
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->Cell(250, 5, utf8_decode('REPORTE DE VISITANTES '),0,1,'C');
        $this->atFPDF->Ln(4);
        //
        $this->atFPDF->SetDrawColor(0, 0, 0); $this->atFPDF->SetFillColor(200, 200, 200); $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->Cell(10, 5,utf8_decode('NRO'), 1, 0, 'C', 1);
        $this->atFPDF->Cell(28, 5,utf8_decode('FECHA / HORA ENTRADA'), 1, 0, 'C', 1);
        $this->atFPDF->Cell(18, 5,utf8_decode('CEDULA'), 1, 0, 'C', 1);
        $this->atFPDF->Cell(60, 5,utf8_decode('NOMBRES Y APELLIDOS'), 1, 0, 'C', 1);
//        $this->atFPDF->Cell(40, 5,utf8_decode('ORGANISMO'), 1, 0, 'C', 1);
        $this->atFPDF->Cell(22, 5,utf8_decode('TIPO DE VISITA'), 1, 0, 'C', 1);
        $this->atFPDF->Cell(51, 5,utf8_decode('MOTIVO'), 1, 0, 'C', 1);
        $this->atFPDF->Cell(51, 5,utf8_decode('DESTINO'), 1, 0, 'C', 1);
        $this->atFPDF->Cell(25, 5,utf8_decode('FECHA / HORA SALIDA'), 1, 0, 'C', 1);
        $this->atFPDF->Ln();
        //
        $datos = $this->atVisita->metBuscarVisita($organismo,$dependencia,$visitante,$desde,$hasta,$motivo,$tipo);
        $num=0;
        foreach ($datos as $i){
            $num++;
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', '', 7);
            $this->atFPDF->SetWidths(array(10,28,18,60, 22, 51, 51, 25));
            $this->atFPDF->SetAligns(array('C','C','C','C','C','C','C','C'));
            if($i['ind_tipo_visita']=='P'){
                $tipoVisita = 'PARTICULAR';
            } elseif($i['ind_tipo_visita']=='I'){
                $tipoVisita = 'INSTITUCIONAL';
            } elseif($i['ind_tipo_visita']=='E'){
                $tipoVisita = 'EMPRESA';
            } elseif($i['ind_tipo_visita']=='J'){
                $tipoVisita = 'JUBILADOS';
            }
            $this->atFPDF->Row(array(
                $num,
                $i['fec_fecha_entrada'],
                $i['ind_cedula_documento'],
                utf8_decode($i['ind_nombre1'].' '.$i['ind_apellido1']),
                $tipoVisita,
                utf8_decode($i['ind_motivo']),
                utf8_decode($i['ind_destino']),
                $i['fec_fecha_salida']
            ));
        }
        $this->atFPDF->ln();
        $this->atFPDF->ln();
        $this->atFPDF->ln();
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 7);
        $this->atFPDF->SetWidths(array(20,10));
        $this->atFPDF->SetAligns(array('C','C'));
        $this->atFPDF->Row(array('TOTAL DE VISITANTES:',$num));

        //	Muestro el contenido del pdf.
        $this->atFPDF->Output();

    }
    public function metAmpliarFoto()
    {
        $foto = $this->metObtenerTexto('foto');

        $this->atVista->assign('foto',$foto);
        $this->atVista->metRenderizar('foto_visita', 'modales');
    }
}
