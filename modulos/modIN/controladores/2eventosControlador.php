<?php 
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: INTRANET
 * PROCESO: GESTIONAR LOS EVENTOS
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1  |         Maikol Isava                                  |    maikol.isava@gmail.com      |          0426-1814058          |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        26-05-2016       |         1.0        |  Ultima modificacion 24-08-2016
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
 class eventosControlador extends Controlador
{
    private $atModeloEventos;
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atIdUsuario = Session::metObtener('idUsuario');
		$this->atModeloEventos = $this->metCargarModelo('eventos');

    }

    
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker',
            'jqueryCalendarioCodrops/css/calendar',
            'jqueryCalendarioCodrops/css/custom_1',
            'jqueryCalendarioCodrops/css/texto_alternativo'

        );

        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
           	'jqueryCalendarioCodrops/js/modernizr.custom.63321',
            'jqueryCalendarioCodrops/js/jquery.calendario'

        );

        $js = array('materialSiace/core/demo/DemoTableDynamic',
                'materialSiace/core/demo/DemoFormWizard',
                'materialSiace/core/demo/DemoFormComponents'

        );

		
		$this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
		
		$datosEventos = $this->atModeloEventos->metBusquedaEventos();
        
        $cad_array_eventos = " { ";
        for($i=0; $i<count($datosEventos); $i++)
        {
            $aux = explode('-',$datosEventos[$i]['fec_registro']);//a-m-d
            $fecha_eventos = $aux[1]."-".$aux[2]."-".$aux[0];//m-d-a

            $cad_array_eventos = $cad_array_eventos." '".$fecha_eventos."' : '<a >Eventos (".$datosEventos[$i]['total'].")</a>'";
            if(($i+1) < count($datosEventos))//no es la ultima iteracion
            {
                $cad_array_eventos = $cad_array_eventos." , ";
            }

        }

        $cad_array_eventos = $cad_array_eventos." } ";

        //==
        $datosEventosAnual = $this->atModeloEventos->metBusquedaEventosAnual();
        $cad_array_eventosAnual = " { ";
        for($i=0; $i<count($datosEventosAnual); $i++)
        {
            $aux = explode('-',$datosEventosAnual[$i]['fec_registro']);//a-m-d
            $fecha_eventos = $aux[1]."-".$aux[2];//m-d

            $cad_array_eventosAnual = $cad_array_eventosAnual." '".$fecha_eventos."' : '<a >Eventos Anuales (".$datosEventosAnual[$i]['total'].")</a>'";
            if(($i+1) < count($datosEventosAnual))//no es la ultima iteracion
            {
                $cad_array_eventosAnual = $cad_array_eventosAnual." , ";
            }

        }
        $cad_array_eventosAnual = $cad_array_eventosAnual." } ";

        //==
        $this->atVista->assign('cad_array_eventosAnual', $cad_array_eventosAnual);
        $this->atVista->assign('cad_array_eventos', $cad_array_eventos);  
        $this->atVista->metRenderizar('agenda');
		
	}

     public function metBuscarListadoEventos()
     {
         $fecha = $_POST['fecha'];
         $datosEventos = $this->atModeloEventos->metBusquedaEventosXfecha($fecha);
         if($datosEventos)
         {
             echo json_encode($datosEventos);
         }
         else
         {
             $aux = explode("-", $fecha);
             $dia = $aux[2];
             $mes = $aux[1];
             $datosEventos = $this->atModeloEventos->metBusquedaEventosXfechaAnual($dia, $mes);
             if($datosEventos)
             {
                 echo json_encode($datosEventos);
             }
             else
             {
                 echo 0;
             }
         }

     }

    public function metRegistrarEventos()
    {
        $complementosCss = array(
            'jquery-validation/dist/site-demo',
        );

        $complementoJs = array(
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
        );

        $js = array(
            'materialSiace/core/demo/DemoFormComponents'
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $estadosEventos = $this->atModeloEventos->metBuscarEstados();

        $this->atVista->assign('estadosEventosArray', $estadosEventos);

        $this->atVista->metRenderizar('registrar', 'modales');
    }

    public function metBuscarMunicipio()
    {
        $fk_a009_num_estado = $_POST['fk_a009_num_estado'];
        $municipioEventos = $this->atModeloEventos->metBuscarMunicipio($fk_a009_num_estado);

        if($municipioEventos)
        {
            $cad_municipios = "<option value='..' >No aplica</option>";
            for ($i = 0; $i < count($municipioEventos); $i++)
            {
                $cad_municipios .= "<option value=" . $municipioEventos[$i]["pk_num_municipio"] . ">" . $municipioEventos[$i]["ind_municipio"] . "</option>";
            }

            echo $cad_municipios;
        }
        else
        {
            echo "<option value='...' >No hay municipios registrados para este estado...</option>";
        }




    }

    public function metBuscarParroquia()
    {
        $fk_a011_num_municipio = $_POST['fk_a011_num_municipio'];
        $parroquiaEventos = $this->atModeloEventos->metBuscarParroquia($fk_a011_num_municipio);

        if($parroquiaEventos)
        {
            $cad_parroquia = "<option value='..' >No aplica</option>";
            for ($i = 0; $i < count($parroquiaEventos); $i++)
            {
                $cad_parroquia .= "<option value=" . $parroquiaEventos[$i]["pk_num_parroquia"] . ">" . $parroquiaEventos[$i]["ind_parroquia"] . "</option>";
            }

            echo $cad_parroquia;
        }
        else
        {
            echo "<option value='...' >No hay Parroquias registradas para este municipio...</option>";
        }
    }

    public function metRegistrarEvento()
    {

        $cod_img= mt_rand();
        $destino =  "publico/imagenes/modIN/eventos/";
        if(isset($_FILES["ind_ruta_img"]))
        {
            $img_f   = $cod_img."_".$_FILES["ind_ruta_img"]["name"];
            $ruta    = $destino.$img_f;
            if (!file_exists($ruta))
            {
                $resultado = move_uploaded_file($_FILES["ind_ruta_img"]["tmp_name"], $ruta);
                chmod($ruta, 0777);

                if ($resultado)
                {
                  
                   $fec_registro = $_POST["fec_registro"];
                   $aux = explode("-", $fec_registro);
                   $fec_registro = $aux[2]."-".$aux[1]."-".$aux[0];

                   $hora_inicio = $_POST["hora_inicio"];
                   $horario_inicio = $_POST["horario_inicio"];

                   $hora_final = $_POST["hora_final"];
                   $horario_final = $_POST["horario_final"];

                   $ind_descripcion = strtoupper($_POST["ind_descripcion"]);
                   $ind_lugar = strtoupper($_POST["ind_lugar"]);

                   $ind_estado = $_POST["ind_estado"];
                   if(strcmp($ind_estado,'..') == 0 )
                   {
                        $ind_estado = 0;
                        $ind_municipio = 0;
                        $ind_parroquia = 0;
                   }
                   else
                   {
                        $ind_municipio = $_POST["ind_municipio"];
                        if(strcmp($ind_municipio,'..') == 0 )
                        {
                            $ind_municipio = 0;
                            $ind_parroquia = 0;
                        }
                        else
                        {
                            $ind_parroquia = $_POST["ind_parroquia"];
                            if(strcmp($ind_parroquia,'..') == 0 )
                            {
                                $ind_parroquia = 0;
                            }
                        }    
                   }

                   if(isset($_POST["ind_anual"])) 
                   {
                        $ind_anual = $_POST["ind_anual"];//anual es Y
                   }
                   else
                   {
                        $ind_anual = '';//evento diario no anual
                   } 


                   $resp  = $this->atModeloEventos->metInsertarEvento($fec_registro, $hora_inicio, $horario_inicio, $hora_final, $horario_final,
                            $img_f, $ind_descripcion, $ind_lugar, $ind_estado, $ind_municipio, $ind_parroquia, $ind_anual); 
                   if($resp)
                   {
                     echo 1;
                   }
                   else
                   {
                     echo -3;// error al insertar
                   }     
                }
                else
                {
                     echo -2;//ocurrio un error al mover el archivo
                }    
            }
            else
            {
                echo 0;//el nombre ya existe no se registra
            } 
        }
        else
        {
           echo -1;//error al cargar el input file 
        }

        
    }

    public function metModificarEvento()
    {
        $complementosCss = array(
            'jquery-validation/dist/site-demo',
        );

        $complementoJs = array(
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
        );

        $js = array(
            'materialSiace/core/demo/DemoFormComponents'
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $pk_num_evento = $_POST['pk_num_evento'];
        
        $arrayDatosEvento  = $this->atModeloEventos->metBusquedaUnicaEvento($pk_num_evento);

        if($arrayDatosEvento)
        {
             $aux = explode('-', $arrayDatosEvento['fec_registro']);
             $arrayDatosEvento['fec_registro'] = $aux[2]."-".$aux[1]."-".$aux[0];


            //*************************************************************
             
             if($arrayDatosEvento['ind_estado'] != 0)
             {
                
                if($arrayDatosEvento['ind_municipio'] != 0)
                {

                   $arrayDatosMunicipioEventos = $this->atModeloEventos->metBuscarMunicipio($arrayDatosEvento['ind_estado']);
                   $this->atVista->assign('arrayDatosMunicipioEventos', $arrayDatosMunicipioEventos);

                   if($arrayDatosEvento['ind_parroquia'] != 0)
                   {
                        $arrayDatosParroquiaEventos = $this->atModeloEventos->metBuscarParroquia($arrayDatosEvento['ind_municipio']); 
                        $this->atVista->assign('arrayDatosParroquiaEventos', $arrayDatosParroquiaEventos);
                   }   

                }    
             }
            
            //*************************************************************** 
             
             $estadosEventosArray = $this->atModeloEventos->metBuscarEstados();

             $this->atVista->assign('estadosEventosArray', $estadosEventosArray);
             $this->atVista->assign('arrayDatosEvento', $arrayDatosEvento);

             $this->atVista->metRenderizar('modificar', 'modales');
        }
        else
        {
            echo "ERROR AL BUSCAR EVENTO: ".$pk_num_evento;
        }

       

    }

    public function metEditarEvento()
    {
        $cod_img= mt_rand();
        $destino =  "publico/imagenes/modIN/eventos/";

        if($_POST['swImagen']=='1')//se cambio la imagen
        {

            if(isset($_FILES["ind_ruta_img"]))
            {
                $img_f   = $cod_img."_".$_FILES["ind_ruta_img"]["name"];
                $ruta    = $destino.$img_f;
                if (!file_exists($ruta))//False si no existe
                {
                    if (is_uploaded_file($_FILES['ind_ruta_img']['tmp_name']))
                    {
                        $resultado = move_uploaded_file($_FILES["ind_ruta_img"]["tmp_name"], $ruta);

                        if ($resultado) {
                            chmod($ruta, 0777);
                            $bandFile = 1;
                        } else {

                            $bandFile = "-2 :" . $resultado;//error al mover el archivo
                        }
                    }
                    else
                    {
                        $temp_file = sys_get_temp_dir();
                        $bandFile = "-3 : nombre del archivo ->  ". $_FILES['ind_ruta_img']['tmp_name']." Directorio temporal -> ".$temp_file." Error -> ".$_FILES['ind_ruta_img']['error'];
                    }
                }
                else
                {
                   $bandFile= 0;//el nombre ya existe no se registra la noticia
                } 
            }
            else
            {
                $bandFile= -1;//error al cargar el input file
            }
        }
        else//No se cambio la imagen
        {
            $bandFile=1;
            $img_f = $_POST["ind_ruta_img_f"];
        }

        if($bandFile==1)//se registra
        {
            $fec_registro = $_POST["fec_registro"];
            $aux = explode("-", $fec_registro);
            $fec_registro = $aux[2]."-".$aux[1]."-".$aux[0];

            $hora_inicio = $_POST["hora_inicio"];
            $horario_inicio = $_POST["horario_inicio"];

            $hora_final = $_POST["hora_final"];
            $horario_final = $_POST["horario_final"];

            $ind_descripcion = trim(strtoupper($_POST["ind_descripcion"]));
            $ind_lugar = trim(strtoupper($_POST["ind_lugar"]));

            $ind_estado = $_POST["ind_estado"];
            if(strcmp($ind_estado,'..') == 0 )
            {
                $ind_estado = 0;
                $ind_municipio = 0;
                $ind_parroquia = 0;
            }
            else
            {
                if(isset($_POST["ind_municipio"]))
                {    
                    $ind_municipio = $_POST["ind_municipio"];
                    if(strcmp($ind_municipio,'..') == 0 )
                    {
                        $ind_municipio = 0;
                        $ind_parroquia = 0;
                    }
                    else
                    {
                        if(isset($_POST["ind_parroquia"]))
                        {    
                            $ind_parroquia = $_POST["ind_parroquia"];
                            if(strcmp($ind_parroquia,'..') == 0 )
                            {
                                $ind_parroquia = 0;
                            }
                        }
                        else
                        {
                             $ind_parroquia = 0;
                        }    

                    }
                }
                else
                {
                    $ind_municipio = 0;
                    $ind_parroquia = 0;
                }        
            }

            if(isset($_POST["ind_anual"])) 
            {
                $ind_anual = $_POST["ind_anual"];//anual es Y
            }
            else
            {
                $ind_anual = '';//evento diario no anual
            }

            $pk_num_evento_editar = $_POST["pk_num_evento_editar"];
            $fec_ultima_modificacion = date('Y-m-d H:i:s');

            $resp  = $this->atModeloEventos->metModificarEvento($pk_num_evento_editar, $fec_registro, $hora_inicio, $horario_inicio, $hora_final, $horario_final,
                            $img_f, $ind_descripcion, $ind_lugar, $ind_estado, $ind_municipio, $ind_parroquia, $ind_anual, $fec_ultima_modificacion);
            if($resp)
            {
                echo 1;
            }
            else
            {
                echo -4;//error al actualozar
            } 

        }
        else
        {
            echo $bandFile; 
        } 

       
    }

    public function metEliminarVisualizarEvento()
    {
        $pk_num_evento = $_POST['pk_num_evento'];

        $arrayDatosEvento  = $this->atModeloEventos->metBusquedaUnicaEvento($pk_num_evento);

        if($arrayDatosEvento)
        {
            $aux = explode('-', $arrayDatosEvento['fec_registro']);
            $arrayDatosEvento['fec_registro'] = $aux[2]."-".$aux[1]."-".$aux[0];


            //*************************************************************

            $cad_estado_mucpo_parro = " ";

            if($arrayDatosEvento['ind_estado'] != 0)
            {
                $datosEstadoEvento = $this->atModeloEventos->metBusquedaUnicaEstado($arrayDatosEvento['ind_estado']);
                $cad_estado_mucpo_parro =  "EDO. ".$datosEstadoEvento['ind_estado'];


                if($arrayDatosEvento['ind_municipio'] != 0)
                {

                    $datosMunicipioEvento = $this->atModeloEventos->metBusquedaUnicaMunicipio($arrayDatosEvento['ind_municipio']);
                    $cad_estado_mucpo_parro = $cad_estado_mucpo_parro.", MCPO. ".$datosMunicipioEvento['ind_municipio'];

                    if($arrayDatosEvento['ind_parroquia'] != 0)
                    {
                        $datosParroquiaEvento = $this->atModeloEventos->metBusquedaUnicaParroquia($arrayDatosEvento['ind_parroquia']);
                        $cad_estado_mucpo_parro = $cad_estado_mucpo_parro.", PARROQUIA ".$datosParroquiaEvento["ind_parroquia"];
                    }


                }

            }

            if($arrayDatosEvento['ind_lugar'] == '')
            {
                    $arrayDatosEvento['ind_lugar'] = "...";
            }
            else
            {
               if($cad_estado_mucpo_parro != " ")
               {
                   $arrayDatosEvento['ind_lugar'] = $arrayDatosEvento['ind_lugar'] . ", " . $cad_estado_mucpo_parro;
               }

            }

            if($arrayDatosEvento['ind_anual']== "Y")
            {
                $arrayDatosEvento['ind_anual']= '<i class="md-done"></i>';
            }
            else
            {
                $arrayDatosEvento['ind_anual']= '<i class="md-not-interested"></i>';
            }





            //***************************************************************
            $band =  $_POST['band'];
            $this->atVista->assign('arrayDatosEvento', $arrayDatosEvento);
            $this->atVista->assign('tipoFormulario', $band);
            $this->atVista->metRenderizar('eliminarVisualizar', 'modales');

        }
        else
        {
            echo "Error al buscar el evento: ".$pk_num_evento;
        }


    }

    public function metQuitarEvento()
    {
        $pk_num_evento = $_POST['pk_num_evento'];
        $resp  = $this->atModeloEventos->metEleminarEvento($pk_num_evento);
        if($resp)
        {
            echo 1;
        }
        else
        {
            echo $resp;
        }


    }
	
};
 
?>