<?php 
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: INTRANET
 * PROCESO: GESTIONAR DOCUMENTOS INTERNOS
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1  |         Maikol Isava                                  |    maikol.isava@gmail.com      |          0426-1814058          |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        31-08-2016       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
 class documentosControlador extends Controlador
{
    private $atModeloDocumentos;
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atIdUsuario = Session::metObtener('idUsuario');
		$this->atModeloDocumentos = $this->metCargarModelo('documentos');

    }


     public function metIndex()
     {

         $complementosCss = array(
             'DataTables/jquery.dataTables',
             'DataTables/extensions/dataTables.colVis941e',
             'DataTables/extensions/dataTables.tableTools4029',
             'wizard/wizardfa6c',
             'jquery-validation/dist/site-demo',
             'select2/select201ef',
             'bootstrap-datepicker/datepicker',
             'jqueryCalendarioCodrops/css/calendar',
             'jqueryCalendarioCodrops/css/custom_1',
             'jqueryCalendarioCodrops/css/texto_alternativo'

         );

         $complementoJs = array(
             'jquery-validation/dist/jquery.validate.min',
             'jquery-validation/dist/additional-methods.min',
             'inputmask/jquery.inputmask.bundle.min',
             'select2/select2.min',
             'wizard/jquery.bootstrap.wizard.min',
             'bootstrap-datepicker/bootstrap-datepicker',
             'jqueryCalendarioCodrops/js/modernizr.custom.63321',
             'jqueryCalendarioCodrops/js/jquery.calendario',
             'bootstrap-treeview/public/js/bootstrap-treeview'

         );

         $js = array('materialSiace/core/demo/DemoTableDynamic',
             'materialSiace/core/demo/DemoFormWizard',
             'materialSiace/core/demo/DemoFormComponents',
             'materialSiace/core/cache/63d0445130d69b2868a8d28c93309746'

         );


         $this->atVista->metCargarCssComplemento($complementosCss);
         $this->atVista->metCargarJsComplemento($complementoJs);
         $this->atVista->metCargarJs($js);

         $datosOrganismo  = $this->atModeloDocumentos->metConsultarOrganismos();

         $datosDependencias = $this->atModeloDocumentos->metConsultarSeguridadAlterna($this->atIdUsuario);

         //****************************************************************
         $this->atVista->assign('datosDependencias', $datosDependencias);
         $this->atVista->assign('datosOrganismo', $datosOrganismo);

         if(isset($_POST['origenFormDoc']))
         {
             $this->atVista->assign('origenFormDoc', $_POST['origenFormDoc']);
         }
         else
         {
             $this->atVista->assign('origenFormDoc', 'L');
         }

         $this->atVista->metRenderizar('listar');

     }

     public function metBuscarCarpetasDocumentos()
     {
        $pk_num_dependencia = $_POST['pk_num_dependencia'];

        if( $pk_num_dependencia != 'T')
        {
            $datos = $this->atModeloDocumentos->metBuscarCarpetasDocumentos($pk_num_dependencia);
        }
        else
        {
            $datos = $this->atModeloDocumentos->metBuscarTodasCarpetasDocumentos();
        }


        if($datos)
        {
           echo  json_encode($datos);
        }
        else
        {
            echo 0;
        }
     }

     public function metBuscarDocumentos()
     {
         $pk_num_carpetas_documentos = $_POST['pk_num_carpetas_documentos'];
         $datos = $this->atModeloDocumentos->metBuscarDocumentos($pk_num_carpetas_documentos);
         if($datos)
         {
             echo  json_encode($datos);
         }
         else
         {
             echo 0;
         }
     }

     public function metBuscarDepencias()
     {
         $ependencia = $this->atModeloDocumentos->metConsultarDepencias();
         if($ependencia)
         {
             echo  json_encode($ependencia);
         }

     }

     public function metEliminarDocumento()
     {
         $pk_num_documentos = trim($_POST['pk_num_documentos']);
         $nombre_carpeta = trim($_POST['carpeta_eliminar']);
         $pk_carpeta_eliminar = trim($_POST['pk_carpeta_eliminar']);


         $aux = explode('.',$_POST['nombre_archivo_eliminar']);
         $ind_formato = $aux[1];
         $nombre_archivo_eliminar = $this->atModeloDocumentos->sanear_string(mb_strtoupper($aux[0]));
         $nombre_archivo_eliminar = $nombre_archivo_eliminar.".".$ind_formato;

         $resp = $this->atModeloDocumentos->metBorrarDocumento($pk_num_documentos);
         if($resp)
         {
            $ruta_archivo = 'publico/imagenes/modIN/documentos/'.$nombre_carpeta.'_'.$pk_carpeta_eliminar.'/'.$nombre_archivo_eliminar;

            if(unlink($ruta_archivo))
            {
               echo 1;
            }
            else
            {
                echo -1;//error al borrar la carpeta
            }
         }
         else
         {
             echo -2;//error borrar documento bd
         }
     }

     public function metEditarMenu()
     {

         $datosDependencias  = $this->atModeloDocumentos->metConsultarDepencias();
         $pk_num_dependencia = $_POST['pk_num_dependencia'];

         $clave_dependencias[] = array();

         if( $pk_num_dependencia != 'T')
         {
             $nombre_dependencia = $_POST['nombre_dependencia'];
             $this->atVista->assign('nombre_dependencia', $nombre_dependencia);
             $this->atVista->assign('clave_dependencia', $pk_num_dependencia);
             $carpetas = $this->atModeloDocumentos->metBuscarCarpetasDocumentos($pk_num_dependencia);

             for($i=0; $i < count($carpetas); $i++)
             {
                 $carpetas[$i]["ind_descripcion_format"] =  str_replace("_", " ", $carpetas[$i]["ind_descripcion_carpeta"]);
             }

         }
         else
         {
             $carpetas = $this->atModeloDocumentos->metBuscarTodasCarpetasDocumentos();
             for($i=0; $i < count($datosDependencias); $i++)
             {
                 for($j=0; $j < count($carpetas); $j++)
                 {
                     if ($carpetas[$j]['fk_a004_dependencia'] == $datosDependencias[$i]['pk_num_dependencia'])
                     {
                         $clave_dependencias[$j] = $datosDependencias[$i]['pk_num_dependencia'];
                         $carpetas[$j]['fk_a004_dependencia'] = $datosDependencias[$i]['ind_dependencia'];

                     }
                 }

                 for($z=0; $z < count($carpetas); $z++)
                 {
                     $carpetas[$z]["ind_descripcion_format"] =  str_replace("_", " ", $carpetas[$z]["ind_descripcion_carpeta"]);
                 }
             }

         }

         $numElementos = array();
         for($i=0; $i < count($carpetas); $i++)
         {
             $resp = $this->atModeloDocumentos->metCalcularNumElementosCarpetas($carpetas[$i]['pk_num_carpetas_documentos']);
             $numElementos[$i] = $resp['TOTAL'];
         }

         $this->atVista->assign('numElementos', $numElementos);
         $this->atVista->assign('clave_dependencias', $clave_dependencias);
         $this->atVista->assign('datosCarpetas', $carpetas);
         $this->atVista->assign('pk_num_dependencia', $pk_num_dependencia);

         $this->atVista->metRenderizar('editarMenu');
     }

     public  function metRenombrarCarpeta()
     {
         $pk_num_carpetas_documentos = $_POST['pk_num_carpetas_documentos'];
         $titulo = trim($_POST['titulo']);
         $clave_dependencia = $_POST['clave_dependencia'];
         $this->atVista->assign('pk_num_carpetas_documentos', $pk_num_carpetas_documentos);
         $this->atVista->assign('nombreAntesCarpeta', $titulo);
         $this->atVista->assign('clave_dependencia', $clave_dependencia);
         $tituloFormat = str_replace("_", " ", $titulo);
         $this->atVista->assign('tituloFormat', $tituloFormat);
         $this->atVista->metRenderizar('renombrarCarpeta');
     }

     public  function metCambiarNombreCarpeta()
     {
         $pk_num_carpetas_documentos = $_POST['pk_num_carpetas_documentos'];


         $nombre_nuevo_carpeta = mb_strtoupper(trim($this->metObtenerTexto('nombre_nuevo_carpeta')));
         $nombre_nuevo_carpeta = $this->atModeloDocumentos->sanear_string($nombre_nuevo_carpeta);

         $fec_ultima_modificacion = date('Y-m-d');
         $pk_num_dependencia = $_POST['clave_dependencia'];

         $nombreViejo = trim($_POST["nombreViejo"]);

         $resp = $this->atModeloDocumentos->metBuscarCarpetasDocumentosNombre($pk_num_dependencia, $nombre_nuevo_carpeta);

         if($resp['TOTAL'] == 0)
         {
             $resp = $this->atModeloDocumentos->renombrarCarpeta($pk_num_carpetas_documentos, $nombre_nuevo_carpeta, $fec_ultima_modificacion);
             if ($resp)
             {
                 //****************************************************
                 $ruta_carpeta = 'publico/imagenes/modIN/documentos/'.$nombreViejo.'_'.$pk_num_carpetas_documentos.'/';
                 if(is_dir($ruta_carpeta))
                 {
                     $nombre_nuevo_carpeta = mb_strtoupper($nombre_nuevo_carpeta);
                     $ruta_carpeta_nueva = 'publico/imagenes/modIN/documentos/'.$nombre_nuevo_carpeta.'_'.$pk_num_carpetas_documentos.'/';

                     if(rename($ruta_carpeta, $ruta_carpeta_nueva))
                     {
                         echo mb_strtoupper($nombre_nuevo_carpeta);
                     }
                     else
                     {
                         echo -3;//error al renombrar
                     }

                 }
                 else
                 {
                     echo -2;//error no existe la carpeta a renombrar
                 }


             }
             else
             {
                 echo -1;
             }
         }
         else
         {
             echo 2;
         }
     }

     public function metNuevaCarpeta()
     {
         $pk_num_dependencia = $_POST['pk_num_dependencia'];
         $tipo_form = $_POST['tipo_form'];

         if($pk_num_dependencia == 'T')
         {
             $ind_dependencia_nueva_carpeta  = $this->atModeloDocumentos->metConsultarDepencias();
             $this->atVista->assign('ind_dependencia_nueva_carpeta', $ind_dependencia_nueva_carpeta);
         }
         else
         {

             $ind_dependencia = $_POST['ind_dependencia'];
             $this->atVista->assign('ind_dependencia_nueva_carpeta', $ind_dependencia);
         }

         $this->atVista->assign('pk_num_dependencia_nueva_carpeta', $pk_num_dependencia);
         $this->atVista->assign('tipo_form', $tipo_form);
         $this->atVista->metRenderizar('nuevaCarpeta');
     }

     public function metCrearCarpeta()
     {
         $pk_num_dependencia = $_POST['ind_dependencia_nueva_carpeta'];
         $ind_descripcion = mb_strtoupper(trim($this->metObtenerTexto('nombre_nuevo_crear_carpeta')));

         $ind_descripcion = $this->atModeloDocumentos->sanear_string($ind_descripcion);

         $fec_ultima_modificacion= date('Y-m-d');
         $fec_creacion = date('Y-m-d');


         $resp = $this->atModeloDocumentos->metBuscarCarpetasDocumentosNombre($pk_num_dependencia, $ind_descripcion);

         if($resp['TOTAL'] == 0)
         {
             $resp = $this->atModeloDocumentos->metCrearCarpeta($pk_num_dependencia, $ind_descripcion, $fec_ultima_modificacion, $this->atIdUsuario, $fec_creacion);
             if ($resp > 0)
             {
                 //****************************************************
                 $nombre_carpeta = mb_strtoupper($ind_descripcion);
                 $ruta_carpeta = 'publico/imagenes/modIN/documentos/'.$nombre_carpeta.'_'.$resp.'/';
                 if(!is_dir($ruta_carpeta))
                 {
                     if(mkdir($ruta_carpeta, 0777))
                     {
                         echo 1;
                     }
                     else
                     {
                         echo -3;
                     }

                 }
                 else
                 {
                     echo -2;//ya existe error porque debe corresponderse con bd
                 }


             }
             else
             {
                 echo -1;
             }
         }
         else
         {
             echo 2; //el nombre de la carpeta ya existe en esta dependencia
         }

     }

     public function metEliminarCarpeta()
     {
         $pk_num_carpetas_documentos = trim($_POST['pk_num_carpetas_documentos']);
         $nombre_carpeta = trim($_POST["nombre_carpeta"]);

         $resp = $this->atModeloDocumentos->metBorrarCarpeta($pk_num_carpetas_documentos);//borra carpeta y documentos por bd
         if($resp)
         {
             $ruta = 'publico/imagenes/modIN/documentos/'.$nombre_carpeta.'_'.$pk_num_carpetas_documentos;
             if($this->atModeloDocumentos->metBorrarDirectorio($ruta))
             {
                 echo 1;
             }
             else
             {
                 echo -5; //error al borrar el directorio
             }


         }
         else
         {
             echo -1;//error borrar carpeta en bd
         }

     }

     public function metSubirArchivo()
     {
         $pk_num_dependencia = $_POST['pk_num_dependencia'];
         $this->atVista->assign('pk_num_dependencia_archivo', $pk_num_dependencia);

         $datosOrganismo  = $this->atModeloDocumentos->metConsultarOrganismos();
         $datosDependencias  = $this->atModeloDocumentos->metConsultarSeguridadAlterna($this->atIdUsuario);

         $this->atVista->assign('datosOrganismoArchivo', $datosOrganismo);
         $this->atVista->assign('datosDependenciasArchivo', $datosDependencias);

         $complementosCss = array(
             'dropzone/downloads/css/dropzone'
         );//basic

         $complementoJs = array(
             'dropzone/downloads/dropzone.min'
         );

         $this->atVista->metCargarCssComplemento($complementosCss);
         $this->atVista->metCargarJsComplemento($complementoJs);

         $this->atVista->metRenderizar('subirArchivos');
     }

     public function metSubirArchivos()
     {
         if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
         {

             if(isset($_FILES["file"]["name"]))
             {

                 $carpeta_subir_archivo = $_POST['carpeta_subir_archivo'];
                 $nombre_carpeta_subir_archivo = trim($_POST['nombre_carpeta_subir_archivo']);

                 $ruta_carpeta =  "publico/imagenes/modIN/documentos/".$nombre_carpeta_subir_archivo."_".$carpeta_subir_archivo;

                 if(is_dir($ruta_carpeta))
                 {
                     $aux = explode('.',$_FILES["file"]["name"]);
                     $ind_formato = $aux[1];
                     $aux2 = $this->atModeloDocumentos->sanear_string(mb_strtoupper($aux[0]));
                     $ind_descripcion = $aux2;
                     $ruta = $ruta_carpeta.'/'.$ind_descripcion.".".$ind_formato;
                     if (!file_exists($ruta))
                     {
                         $resultado = move_uploaded_file($_FILES["file"]["tmp_name"], $ruta);
                         if ($resultado)
                         {
                             chmod($ruta, 0777);

                             $fec_ultima_modificacion = date('Y-m-d');
                             $fec_creacion = date('Y-m-d');


                             $filesize = $_FILES["file"]["size"];
                             $ind_peso = $this->atModeloDocumentos->metTamanoArchivo($filesize);

                             $resp = $this->atModeloDocumentos->metRegistrarArchivo($fec_creacion, $ind_peso, $ind_formato, $ind_descripcion, $carpeta_subir_archivo, $this->atIdUsuario, $fec_ultima_modificacion);
                             if($resp > 0)
                             {
                                 echo $resp;
                             }
                             else
                             {
                                 echo -4;//error al insertar el archivo
                             }
                         }
                         else
                         {
                             echo -3;//error al mover archivo
                         }
                     }
                     else
                     {
                         echo 0;//ya existe el archivo
                     }
                 }
                 else
                 {
                     echo -1;//error carpeta no existe
                 }

             }
             else
             {
                echo -2;//error no se envio el archivo
             }
         }

     }

     public function metBuscarCarpetasSubirArchivos()
     {
         $pk_num_dependencia = $_POST['dependencia'];

         $datos = $this->atModeloDocumentos->metBuscarCarpetasDocumentos($pk_num_dependencia);

         if($datos)
         {
             $cad_carpetas = "<option value='J' >Seleccione la carpeta...</option>";
             for ($i = 0; $i < count($datos); $i++)
             {
                 $cad_carpetas .= "<option value=" . $datos[$i]["pk_num_carpetas_documentos"] . ">" . $datos[$i]["ind_descripcion_carpeta"] . "</option>";
             }

             echo $cad_carpetas." ";

         }
         else//No hay carpetas
         {
             echo "<option value='J' >Seleccione la carpeta...</option>";
         }

     }

    public function metRenombrarArchivo()
    {
        $nombre_carpeta = $_POST['nombre_carpeta'];
        $pk_num_carpeta = $_POST['pk_num_carpeta'];
        $nombre_documento = $_POST['nombre_documento'];
        $pk_num_documentos = $_POST['pk_num_documentos'];
        $formato_documento = $_POST['formato_documento'];

        $this->atVista->assign('nombreMostrarArchivo', str_replace("_", " ", $nombre_documento));
        $this->atVista->assign('nombreAntesArchivo', $nombre_documento);
        $this->atVista->assign('nombreCarpeta', $nombre_carpeta);
        $this->atVista->assign('pkCarpeta', $pk_num_carpeta);
        $this->atVista->assign('pkArchivo', $pk_num_documentos);
        $this->atVista->assign('formatoDocumento', $formato_documento);

        $this->atVista->metRenderizar('renombrarArchivo');
    }

    public function metCambiarNombreArchivo()
    {
        $nombreNuevoArchivo = mb_strtoupper(trim($this->metObtenerTexto('nombreNuevoArchivo')));
        $nombreNuevoArchivo = $this->atModeloDocumentos->sanear_string($nombreNuevoArchivo);
        $nombreAntesArchivo = trim($_POST['nombreAntesArchivo']);
        $nombreCarpeta = trim($_POST['nombreCarpeta']);
        $pkCarpeta = trim($_POST['pkCarpeta']);
        $pkArchivo = trim($_POST['pkArchivo']);
        $formatoDocumento = trim($_POST['formatoDocumento']);

        $ruta_archivo_nueva = 'publico/imagenes/modIN/documentos/'.$nombreCarpeta.'_'.$pkCarpeta.'/'.$nombreNuevoArchivo.'.'.$formatoDocumento;
        $ruta_archivo_antes = 'publico/imagenes/modIN/documentos/'.$nombreCarpeta.'_'.$pkCarpeta.'/'.$nombreAntesArchivo.'.'.$formatoDocumento;
        if (!file_exists($ruta_archivo_nueva))
        {
            if(rename($ruta_archivo_antes, $ruta_archivo_nueva))
            {
                $fec_ultima_modificacion = date('Y-m-d');
                $resp = $this->atModeloDocumentos->renombrarArchivo($pkArchivo, $nombreNuevoArchivo, $fec_ultima_modificacion);
                if($resp)
                {
                    echo $nombreNuevoArchivo;
                }
                else
                {
                    echo -2; //error al renombrar el archivo en bd
                }
            }
            else
            {
                echo -1; //error al renombrar archivo
            }
        }
        else
        {
            echo 0;//el nombre ya existe
        }

    }

    public function metBuscarCarpetasFiltrar()
    {
         $pk_num_dependencia = $_POST['pk_num_dependencia'];
         $patron = $_POST['patron'];

         if( $pk_num_dependencia != 'T')//busqueda en una dependencia
         {
             $datos = $this->atModeloDocumentos->metBuscarCarpetasDocumentosFiltrar($pk_num_dependencia, $patron);
         }
         else
         {
             $datos = $this->atModeloDocumentos->metBuscarTodasCarpetasDocumentosFiltrar($patron);
         }


         if($datos)
         {
             echo  json_encode($datos);
         }
         else
         {
             echo 0;
         }
     }
	
};
 
?>