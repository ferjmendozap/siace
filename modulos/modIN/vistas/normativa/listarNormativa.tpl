<section class="style-default-light">
<!--TITULO DEL FORMULARIO-->
{if $origenFormNorm != 'I'}
<div class="row">
	<div class="col-lg-12">
		<div class="card-head"><h2 class="text-primary">Listar Normativa</h2></div>
	</div>
	&nbsp;
</div>
{/if}
<div class="row">
	
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body ">
				<form id="formAjax" class="form form-validation" role="form" >
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<select id="ind_tipo_normativa" required="" class="form-control" name="ind_tipo_normativa" >
										<option value="T" selected="selected" >Todas</option>
										<option value="L" >Leyes</option>
										<option value="N" >Normativas</option>
										<option value="C" >Circulares</option>
										<option value="R" >Resoluciones</option>
										<option value="M" >Manuales de Normas y Procedimientos</option>
									</select>
									<label for="ind_tipo_normativa">Tipo de Normativa</label>
								</div>
							</div>
							<div class="col-lg-6">
								<br>
								<button id="btn_ninguno" titulo="" type="button"  class="gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="Seleccionar ninguno" ><i class="md-panorama-fisheye "></i></button>
							</div>
					    </div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group" >
									<div class="input-group">
										<div class="input-group-addon">
											<div class=" radio radio-inline radio-styled">
												<label>
													<input id="radio_1" type="radio" name="radio_filtro" value="1">
													<span></span>
												</label>
											</div>
										</div>
										<div class="input-group-content">
											<div id="demo-date-range1" class="input-daterange input-group">
												<div class="input-group-content">
													<input disabled id="start_fecha_gaceta" type="text" class="form-control" name="start_fecha_gaceta"  value="" required aria-required="true" aria-invalid="true" >
													<label>Fecha de la gaceta:</label>
												</div>
												<span class="input-group-addon">a</span>
												<div class="input-group-content">
													<input disabled  id="end_fecha_gaceta" class="form-control" type="text" name="end_fecha_gaceta"  value="" required aria-required="true" aria-invalid="true" >
													<div class="form-control-line"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group" >
									<div class="input-group">
										<div class="input-group-addon">
											<div class="radio radio-inline radio-styled">
												<label>
													<input id="radio_2" type="radio" name="radio_filtro" value="2">
													<span></span>
												</label>
											</div>
										</div>
										<div class="input-group-content">
											<div id="demo-date-range2" class="input-daterange input-group">
												<div class="input-group-content">
													<input disabled id="start_fecha_resolucion" type="text" class="form-control" name="start_fecha_resolucion"  value="" required aria-required="true" aria-invalid="true" >
													<label>Fecha de la resolución:</label>
												</div>
												<span class="input-group-addon">a</span>
												<div class="input-group-content">
													<input disabled  id="end_fecha_resolucion" class="form-control" type="text" name="end_fecha_resolucion"  value="" required aria-required="true" aria-invalid="true" >
													<div class="form-control-line"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row" >
							<div class="col-lg-6"  >
								<div class="form-group" >
									<div class="input-group">
										<div class="input-group-addon">
											<div class="radio radio-inline radio-styled">
												<label>
													<input id="radio_3" type="radio" name="radio_filtro" value="3" >
													<span></span>
												</label>
											</div>
										</div>
										<div class="input-group-content">
											<input type="text" name="num_tipo_normativa" id="num_tipo_normativa" class="form-control" disabled style="text-transform: uppercase" onkeypress="return validarNumero(event)" required aria-required="true" aria-invalid="true" >
											<label for="num_gaceta_ley">Número de gaceta/Resolución/Circular</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group" >
									<div class="input-group">
										<div class="input-group-addon">
											<div class="radio radio-inline radio-styled">
												<label>
													<input id="radio_4" type="radio" name="radio_filtro" value="4">
													<span></span>
												</label>
											</div>
										</div>
										<div class="input-group-content">
											<div id="demo-date-range3" class="input-daterange input-group">
												<div class="input-group-content">
													<input disabled id="start_fecha_circular" type="text" class="form-control" name="start_fecha_circular"  value="" required aria-required="true" aria-invalid="true" >
													<label>Fecha del circular :</label>
												</div>
												<span class="input-group-addon">a</span>
												<div class="input-group-content">
													<input disabled  id="end_fecha_circular" class="form-control" type="text" name="end_fecha_circular"  value="" required aria-required="true" aria-invalid="true" >
													<div class="form-control-line"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					    <div class="row"></div>
					<div class="col-lg-12" >
						<div align="center">
			    			<button type="submit" id="botonRegistrar" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" >Buscar</button>
            			</div>	
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
<div class="row">
	<div class="col-lg-12">

		<div class="table-responsive">
		<table id="datatable1" class="table table-striped table-hover"  >{*todas*}
			<thead>
				<tr  align="center">
                    <th style="width: 70px;"><i class=""></i>N°</th>
					<th style="width: 70px;"><i class=""></i>Tipo</th>
					<th style="width: 690px !important;" ><i class=""></i>Descripción</th>
					<th style="width: 90px;" ><i class=""  ></i>N° gac./cir.</th>
					<th style="width: 90px;" ><i class=""></i>F. gac./cir.</th>
					<th style="width: 100px;" ><i class=""></i>N° resolución</th>
					<th style="width: 160px;"><i class=""></i>F. resolución</th>
                    <th style="width: 80px;"><i class=""></i>Ámbito</th>
                    <th style="width: 80px;"><i class=""></i>Emitido</th>
					<th style="width: 80px;"><i class=""></i>Ver</th>
					<th style="width: 80px;"><i class=""></i>Editar</th>
					<th style="width: 80px;"><i class=""></i>Eliminar</th>
	            </tr>
			</thead>
			<tbody >
				{foreach name=for_normativa item=indice from=$datosNormativas}
				<tr class="fila_agregar" >
					<td>{$smarty.foreach.for_normativa.iteration}</td>
					<td>{$indice.ind_tipo_normativa}</td>
					<td>{$indice.ind_descripcion}</td>
					<td >{$indice.ind_gaceta_circular}</td>
					<td>{$indice.fec_gaceta_circular}</td>
					<td>{$indice.ind_resolucion}</td>
					<td>{$indice.fec_resolucion}</td>
					{if $indice.ind_tipo_ley_circular != 'Nacional' && $indice.ind_tipo_ley_circular != 'Estadal' }
						<td>---</td>
					{else}
						<td>{$indice.ind_tipo_ley_circular}</td>
					{/if}
					{if $indice.ind_tipo_ley_circular != 'Interno' && $indice.ind_tipo_ley_circular != 'CGR' }
						<td>---</td>
					{else}
						<td>{$indice.ind_tipo_ley_circular}</td>
					{/if}
					<td><button id="btn_todas_1" rutaPdf="{$indice.ind_ruta_archivo}"  titulo="{$indice.ind_tipo_normativa}" type="button"  pk_num_normativa_legal = "{$indice.pk_num_normativa_legal}" class="ver gsUsuario btn ink-reaction btn-raised btn-xs btn-info" data-target="#formModal2" data-toggle="modal" data-keyboard="false" data-backdrop="static" ><i class="md md-search"></i></button></td>
					<td>
					{if in_array('IN-01-03-02-01-M',$_Parametros.perfil)}
						<button id="btn_todas_2" type="button"  pk_num_normativa_legal = "{$indice.pk_num_normativa_legal}" class="editar gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal2" data-toggle="modal" data-keyboard="false" data-backdrop="static" ><i class="fa fa-edit"></i></button>
					{else}
						<i class="md-not-interested"></i>
					{/if}
					</td>
					<td>
					{if in_array('IN-01-03-02-02-E',$_Parametros.perfil)}
						<button id="btn_todas_3" rutaPdf="{$indice.ind_ruta_archivo}" titulo="{$indice.ind_descripcion}" type="button"  pk_num_normativa_legal = "{$indice.pk_num_normativa_legal}" class="eliminar gsUsuario btn ink-reaction btn-raised btn-xs btn-danger"   ><i class="md-delete"></i></button>
					{else}
						<i class="md-not-interested"></i>
					{/if}
					</td>
				</tr>
				{/foreach}
			</tbody>
		</table>
		</div>

		<div class="table-responsive"><!-- TABLA RESPONSIBLE TODO EL ANCHO DE LA VENTANA -->
		<table align="center" id="datatable2" class=" invisible_norm_legal table table-striped table-hover" width="100%" style="width: 100%;" > {*leyes*}
				<thead  align="center" >
				<tr >
					<th style="width: 30px;" ><i class=""></i>N°</th>
					<th style="width: 50px;" ><i class=""></i>Tipo</th>
					<th style="width: 690px;" ><i class=""></i>Descripción</th>
					<th style="width: 80px;" ><i class=""></i>N° gaceta</th>
					<th style="width: 90px;" ><i class=""></i>Fec. gaceta</th>
					<th style="width: 70px;" ><i class=""></i>Ámbito</th>
					<th style="width: 100px;" align="center" ><i class=""></i>Interpretación</th>
					<th style="width: 60px;" ><i class=""></i>Ver</th>
					<th style="width: 70px;" ><i class=""></i>Editar</th>
					<th style="width: 70px;" ><i class=""></i>Eliminar</th>
				</tr>
				</thead>
				<tbody   >

					<tr class="fila_agregar" >
						<td></td>
						<td></td>
						<td></td>
						<td ></td>
						<td></td>
						<td></td>
						<td align="center"></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>

				</tbody>
		</table>
		</div>
		<div class="table-responsive">
		<table id="datatable3" class=" invisible_norm_legal table table-striped table-hover" width="100%" style="width: 100%;"   > {*normativas/resolucion/manuales*}
			<thead>
			<tr  align="center">
				<th style="width: 30px;" ><i class=""></i>N°</th>
				<th style="width: 50px;" ><i class=""></i>Tipo</th>
				<th style="width: 700px;" ><i class=""></i>Descripción</th>
				<th style="width: 80px;" ><i class=""></i>N° gaceta</th>
				<th style="width: 100px;" ><i class=""></i>Fec. gaceta</th>
				<th style="width: 100px;"><i class=""></i>N° Resolución</th>
				<th style="width: 110px;"><i class=""></i>Fec. Resolución</th>
				<th style="width: 70px;"><i class=""></i>Ver</th>
				<th style="width: 70px;"><i class=""></i>Editar</th>
				<th style="width: 70px;"><i class=""></i>Eliminar</th>
			</tr>
			</thead>
			<tbody >

			<tr class="fila_agregar" >
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td >&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>

			</tbody>
		</table>
		</div>
		<div class="table-responsive">
		<table id="datatable4" class="  invisible_norm_legal table table-striped table-hover" width="100%" style="width: 100%;"   > {*circulares*}
			<thead>
			<tr  align="center">
				<th style="width: 30px;" ><i class=""></i>N°</th>
				<th style="width: 50px;"  ><i class=""></i>Tipo</th>
				<th style="width: 700px;" ><i class=""></i>Descripción</th>
				<th style="width: 80px;" ><i class=""></i>N° circular</th>
				<th style="width: 100px;"  ><i class=""></i>Fec. circular</th>
				<th style="width: 100px;"><i class=""></i>Emitido</th>
				<th style="width: 110px;"><i class=""></i>Depemdencia</th>
				<th style="width: 70px;"><i class=""></i>Ver</th>
				<th style="width: 70px;"><i class=""></i>Editar</th>
				<th style="width: 70px;"><i class=""></i>Eliminar</th>
			</tr>
			</thead>
			<tbody >

			<tr class="fila_agregar" >
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td >&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>

			</tbody>
		</table>
		</div>

	</div>
</div>
<input type="hidden" id="ultima_tabla" value="datatable1" >
</section>

<style>

	.invisible_norm_legal{
		display: none;
	}

	.visible_norm_legal{
		display: block;
	}

</style>

<script>
//*************************************************************
//*  FORMATEAR LA FECHA 
//*************************************************************
$("#demo-date-range1").datepicker({
	todayHighlight: true,
	format:'dd-mm-yyyy',
	autoclose: true,
	language:'es'
});

$("#demo-date-range2").datepicker({
	todayHighlight: true,
	format:'dd-mm-yyyy',
	autoclose: true,
	language:'es'
});

$("#demo-date-range3").datepicker({
	todayHighlight: true,
	format:'dd-mm-yyyy',
	autoclose: true,
	language:'es'
});
//*************************************************************
//*
//*************************************************************
function validarNumero(evt){

	var charCode = evt.which || evt.keyCode;
	if (charCode==8||charCode==13||charCode==0)
		return true;
	var str = String.fromCharCode(charCode);
	if (!/[0-9\-\.\A-Z\a-z]/.test(str)) {
		return false;//evt.preventDefault();
	};
}

$(document).ready(function() {

//***************************************************************************
//*  DECLARACION DE TABLAS
//***************************************************************************
	$('#datatable1').DataTable({ //ley
		"dom": 'lCfrtip',
		"order": [],
		"colVis": {
			"buttonText": "Columnas",
			"overlayFade": 0,
			"align": "right"
		},
		"language": {
			"lengthMenu": 'Mostrar _MENU_',
			"search": '<i class="fa fa-search"></i>',
			"paginate": {
				"previous": '<i class="fa fa-angle-left"></i>',
				"next": '<i class="fa fa-angle-right"></i>'
			}
		}
	});


//**************************************************************
//*   ACTIVAR DESACTIVAR LOS CAMPOS
//**************************************************************
	$('#btn_ninguno').click(function(){

		$('#radio_1').prop('checked', false);
		$('#radio_2').prop('checked', false);
		$('#radio_3').prop('checked', false);
		$('#radio_4').prop('checked', false);

		$('input[id="start_fecha_gaceta"]').prop('disabled', true);
		$('input[id="start_fecha_gaceta"]').val('');
		$('input[id="end_fecha_gaceta"]').prop('disabled', true);
		$('input[id="end_fecha_gaceta"]').val('');

		$('input[id="start_fecha_resolucion"]').prop('disabled', true);
		$('input[id="start_fecha_resolucion"]').val('');
		$('input[id="end_fecha_resolucion"]').prop('disabled', true);
		$('input[id="end_fecha_resolucion"]').val('');

		$('input[id="num_tipo_normativa"]').prop('disabled', true);
		$('input[id="num_tipo_normativa"]').val('');

		$('input[id="start_fecha_circular"]').prop('disabled', true);
		$('input[id="start_fecha_circular"]').val('');
		$('input[id="end_fecha_circular"]').prop('disabled', true);
		$('input[id="end_fecha_circular"]').val('');


		$('select[id="ind_tipo_normativa"]').focus();

	});

	$(document).on('change','#radio_3', function(){

		if (this.checked){
			$('input[id="num_tipo_normativa"]').prop('disabled', false);
			$('input[id="num_tipo_normativa"]').val('');
			$('input[id="num_tipo_normativa"]').focus();

			$('input[id="start_fecha_gaceta"]').prop('disabled', true);
			$('input[id="start_fecha_gaceta"]').val('');
			$('input[id="end_fecha_gaceta"]').prop('disabled', true);
			$('input[id="end_fecha_gaceta"]').val('');

			$('input[id="start_fecha_resolucion"]').prop('disabled', true);
			$('input[id="start_fecha_resolucion"]').val('');
			$('input[id="end_fecha_resolucion"]').prop('disabled', true);
			$('input[id="end_fecha_resolucion"]').val('');

			$('input[id="start_fecha_circular"]').prop('disabled', true);
			$('input[id="start_fecha_circular"]').val('');
			$('input[id="end_fecha_circular"]').prop('disabled', true);
			$('input[id="end_fecha_circular"]').val('');



		}
	});

	$(document).on('change','#radio_1', function(){

		if (this.checked){

			$('input[id="start_fecha_gaceta"]').prop('disabled', false);
			$('input[id="start_fecha_gaceta"]').val('');
			$('input[id="end_fecha_gaceta"]').prop('disabled', false);
			$('input[id="end_fecha_gaceta"]').val('');
			$('input[id="start_fecha_gaceta"]').focus();

			$('input[id="start_fecha_resolucion"]').prop('disabled', true);
			$('input[id="start_fecha_resolucion"]').val('');
			$('input[id="end_fecha_resolucion"]').prop('disabled', true);
			$('input[id="end_fecha_resolucion"]').val('');

			$('input[id="num_tipo_normativa"]').prop('disabled', true);
			$('input[id="num_tipo_normativa"]').val('');

			$('input[id="start_fecha_circular"]').prop('disabled', true);
			$('input[id="start_fecha_circular"]').val('');
			$('input[id="end_fecha_circular"]').prop('disabled', true);
			$('input[id="end_fecha_circular"]').val('');
		}
	});

	$(document).on('change','#radio_2', function(){

		if (this.checked){

			$('input[id="start_fecha_resolucion"]').prop('disabled', false);
			$('input[id="start_fecha_resolucion"]').val('');
			$('input[id="end_fecha_resolucion"]').prop('disabled', false);
			$('input[id="end_fecha_resolucion"]').val('');

			$('input[id="start_fecha_resolucion"]').focus();


			$('input[id="start_fecha_gaceta"]').prop('disabled', true);
			$('input[id="start_fecha_gaceta"]').val('');
			$('input[id="end_fecha_gaceta"]').prop('disabled', true);
			$('input[id="end_fecha_gaceta"]').val('');

			$('input[id="num_tipo_normativa"]').prop('disabled', true);
			$('input[id="num_tipo_normativa"]').val('');

			$('input[id="start_fecha_circular"]').prop('disabled', true);
			$('input[id="start_fecha_circular"]').val('');
			$('input[id="end_fecha_circular"]').prop('disabled', true);
			$('input[id="end_fecha_circular"]').val('');

			//$('select[id="ind_tipo_normativa"]').val('T');
		}

	});

	$(document).on('change','#radio_4', function(){

		if (this.checked){

			$('input[id="start_fecha_resolucion"]').prop('disabled', true);
			$('input[id="start_fecha_resolucion"]').val('');
			$('input[id="end_fecha_resolucion"]').prop('disabled', true);
			$('input[id="end_fecha_resolucion"]').val('');

			$('input[id="start_fecha_resolucion"]').focus();


			$('input[id="start_fecha_gaceta"]').prop('disabled', true);
			$('input[id="start_fecha_gaceta"]').val('');
			$('input[id="end_fecha_gaceta"]').prop('disabled', true);
			$('input[id="end_fecha_gaceta"]').val('');

			$('input[id="num_tipo_normativa"]').prop('disabled', true);
			$('input[id="num_tipo_normativa"]').val('');

			$('input[id="start_fecha_circular"]').prop('disabled', false);
			$('input[id="start_fecha_circular"]').val('');
			$('input[id="end_fecha_circular"]').prop('disabled', false);
			$('input[id="end_fecha_circular"]').val('');

			//$('select[id="ind_tipo_normativa"]').val('T');
		}

	});

	$('#ind_tipo_normativa').change(function() {

		var ind_tipo_normativa2 = $('select[id="ind_tipo_normativa"]').val();

		if(ind_tipo_normativa2 == 'L' )
		{
			$('input[id="start_fecha_gaceta"]').prop('disabled', true);
			$('input[id="start_fecha_gaceta"]').val('');
			$('input[id="end_fecha_gaceta"]').prop('disabled', true);
			$('input[id="end_fecha_gaceta"]').val('');

			$('#radio_1').prop('disabled', false);
			$('#radio_1').prop('checked', false);
			//-------------------------------------
			$('input[id="start_fecha_resolucion"]').prop('disabled', true);
			$('input[id="start_fecha_resolucion"]').val('');
			$('input[id="end_fecha_resolucion"]').prop('disabled', true);
			$('input[id="end_fecha_resolucion"]').val('');

			$('#radio_2').prop('checked', false);
			$('#radio_2').prop('disabled', true);
			//--------------------------------------------
			$('input[id="start_fecha_circular"]').prop('disabled', true);
			$('input[id="start_fecha_circular"]').val('');
			$('input[id="end_fecha_circular"]').prop('disabled', true);
			$('input[id="end_fecha_circular"]').val('');

			$('#radio_4').prop('checked', false);
			$('#radio_4').prop('disabled', true);
			//---------------------------------------
			$('#num_tipo_normativa').val('');
			$('#num_tipo_normativa').prop('disabled', true);

			$('#radio_3').prop('disabled', false);
			$('#radio_3').prop('checked', false);


		}
		else
		{

			if(ind_tipo_normativa2 == 'C')
			{
				$('input[id="start_fecha_gaceta"]').prop('disabled', true);
				$('input[id="start_fecha_gaceta"]').val('');
				$('input[id="end_fecha_gaceta"]').prop('disabled', true);
				$('input[id="end_fecha_gaceta"]').val('');

				$('#radio_1').prop('checked', false);
				$('#radio_1').prop('disabled', true);
				//-------------------------------------------------------------
				$('input[id="start_fecha_resolucion"]').prop('disabled', true);
				$('input[id="start_fecha_resolucion"]').val('');
				$('input[id="end_fecha_resolucion"]').prop('disabled', true);
				$('input[id="end_fecha_resolucion"]').val('');

				$('#radio_2').prop('checked', false);
				$('#radio_2').prop('disabled', true);
				//--------------------------------------------------------------
				$('input[id="start_fecha_circular"]').prop('disabled', true);
				$('input[id="start_fecha_circular"]').val('');
				$('input[id="end_fecha_circular"]').prop('disabled', true);
				$('input[id="end_fecha_circular"]').val('');

				$('#radio_4').prop('disabled', false);
				$('#radio_4').prop('checked', false);
				//-----------------------------------------------------------
				$('#num_tipo_normativa').val('');
				$('#num_tipo_normativa').prop('disabled', true);

				$('#radio_3').prop('disabled', false);
				$('#radio_3').prop('checked', false);


			}
			else// reolucion normativa manuales
			{


				$('input[id="start_fecha_gaceta"]').prop('disabled', true);
				$('input[id="start_fecha_gaceta"]').val('');
				$('input[id="end_fecha_gaceta"]').prop('disabled', true);
				$('input[id="end_fecha_gaceta"]').val('');

				$('#radio_1').prop('checked', false);
				$('#radio_1').prop('disabled', false);
				//--------------------------------------------------------
				$('input[id="start_fecha_resolucion"]').prop('disabled', true);
				$('input[id="start_fecha_resolucion"]').val('');
				$('input[id="end_fecha_resolucion"]').prop('disabled', true);
				$('input[id="end_fecha_resolucion"]').val('');

				$('#radio_2').prop('checked', false);
				$('#radio_2').prop('disabled', false);
				//-----------------------------------------------------------
				$('#num_tipo_normativa').val('');
				$('#num_tipo_normativa').prop('disabled', true);

				$('#radio_3').prop('disabled', false);
				$('#radio_3').prop('checked', false);
				//--------------------------------------------------------------
				$('input[id="start_fecha_circular"]').prop('disabled', true);
				$('input[id="start_fecha_circular"]').val('');
				$('input[id="end_fecha_circular"]').prop('disabled', true);
				$('input[id="end_fecha_circular"]').val('');

				$('#radio_4').prop('disabled', true);
				$('#radio_4').prop('checked', false);
				//--------------------------------------------------------
				if(ind_tipo_normativa2 == 'T')
				{
					$('#radio_4').prop('checked', false);
					$('#radio_4').prop('disabled', false);
				}


			}

		}


	});
//**************************************************************
//* FUNCIONES VER EDITAR Y ELIMINAR
//**************************************************************
	$('#datatable2 tbody').on('click','.ver', function(){

		var rutaPdf = $(this).attr('rutaPdf');


		$('#modalAncho2').css("width", "85%");
		$('#formModalLabel2').html("");
		$('#formModalLabel2').html($(this).attr('titulo')+' - Descarga y visualización');
		$('#ContenidoModal2').html("");

		var urlReporte = '{$_Parametros.url}publico/imagenes/modIN/normativa/'+rutaPdf;
		$('#ContenidoModal2').html('<iframe src="' + urlReporte + '" width="100%" height="950"></iframe>');

	});

	$('#datatable1 tbody').on('click','.ver', function(){


		var rutaPdf = $(this).attr('rutaPdf');

		$('#modalAncho2').css("width", "85%");
		$('#formModalLabel2').html("");
		$('#formModalLabel2').html($(this).attr('titulo')+' - Descarga y visualización');
		$('#ContenidoModal2').html("");

		var urlReporte = '{$_Parametros.url}publico/imagenes/modIN/normativa/'+rutaPdf;
		$('#ContenidoModal2').html('<iframe src="' + urlReporte + '" width="100%" height="950"></iframe>');

	});

	$('#datatable3 tbody').on('click','.ver', function(){

		var rutaPdf = $(this).attr('rutaPdf');


		$('#modalAncho2').css("width", "85%");
		$('#formModalLabel2').html("");
		$('#formModalLabel2').html($(this).attr('titulo')+' - Descarga y visualización');
		$('#ContenidoModal2').html("");

		var urlReporte = '{$_Parametros.url}publico/imagenes/modIN/normativa/'+rutaPdf;
		$('#ContenidoModal2').html('<iframe src="' + urlReporte + '" width="100%" height="950"></iframe>');

	});

	$('#datatable4 tbody').on('click','.ver', function(){

		var rutaPdf = $(this).attr('rutaPdf');


		$('#modalAncho2').css("width", "85%");
		$('#formModalLabel2').html("");
		$('#formModalLabel2').html($(this).attr('titulo')+' - Descarga y visualización');
		$('#ContenidoModal2').html("");

		var urlReporte = '{$_Parametros.url}publico/imagenes/modIN/normativa/'+rutaPdf;
		$('#ContenidoModal2').html('<iframe src="' + urlReporte + '" width="100%" height="950"></iframe>');

	});
	//---------------------------------------------------------

	$('#datatable1 tbody').on('click','.editar', function(){

		var pk_num_normativa_legal = $(this).attr('pk_num_normativa_legal');

		var urVisualizar = '{$_Parametros.url}modIN/normativaCONTROL/EditarNormativaMET';
		$('#modalAncho2').css("width", "90%");
		$('#formModalLabel2').html("");
		$('#formModalLabel2').html('Editar Normativa legal');
		$('#ContenidoModal2').html("");
		$.post(urVisualizar, { pk_num_normativa_legal: pk_num_normativa_legal }, function(dato){

			$('#ContenidoModal2').html(dato);
		});

	});

	$('#datatable2 tbody').on('click','.editar', function(){

		var pk_num_normativa_legal = $(this).attr('pk_num_normativa_legal');

		var urVisualizar = '{$_Parametros.url}modIN/normativaCONTROL/EditarNormativaMET';
		$('#modalAncho2').css("width", "90%");
		$('#formModalLabel2').html("");
		$('#formModalLabel2').html('Editar Normativa legal');
		$('#ContenidoModal2').html("");
		$.post(urVisualizar, { pk_num_normativa_legal: pk_num_normativa_legal }, function(dato){

			$('#ContenidoModal2').html(dato);
		});

	});

	$('#datatable3 tbody').on('click','.editar', function(){

		var pk_num_normativa_legal = $(this).attr('pk_num_normativa_legal');

		var urVisualizar = '{$_Parametros.url}modIN/normativaCONTROL/EditarNormativaMET';
		$('#modalAncho2').css("width", "90%");
		$('#formModalLabel2').html("");
		$('#formModalLabel2').html('Editar Normativa legal');
		$('#ContenidoModal2').html("");
		$.post(urVisualizar, { pk_num_normativa_legal: pk_num_normativa_legal }, function(dato){

			$('#ContenidoModal2').html(dato);
		});

	});

	$('#datatable4 tbody').on('click','.editar', function(){

		var pk_num_normativa_legal = $(this).attr('pk_num_normativa_legal');

		var urVisualizar = '{$_Parametros.url}modIN/normativaCONTROL/EditarNormativaMET';
		$('#modalAncho2').css("width", "90%");
		$('#formModalLabel2').html("");
		$('#formModalLabel2').html('Editar Normativa legal');
		$('#ContenidoModal2').html("");
		$.post(urVisualizar, { pk_num_normativa_legal: pk_num_normativa_legal }, function(dato){

			$('#ContenidoModal2').html(dato);
		});

	});

//**********************************************************
//*   INTERPRETACIÒN DE LA LEY
//**********************************************************
	$('#datatable2').on('click','.interpretacion', function(){

		var pk_num_normativa_legal = $(this).attr('pk_num_normativa_legal');
		var titulo =  $(this).attr('titulo');
		var urInterpretacion = '{$_Parametros.url}modIN/normativaCONTROL/InterpretacionMET';
		$('#modalAncho2').css("width", "70%");
		$('#formModalLabel2').html("");
		$('#ContenidoModal2').html("");
		$.post(urInterpretacion, { pk_num_normativa_legal: pk_num_normativa_legal }, function(dato){
			$('#formModalLabel2').html('LEY '+titulo);
			$('#ContenidoModal2').html(dato);
		});

	});
//************************************************************
//*  ELIMINAR NORMATIVA
//************************************************************
	$('#datatable1 tbody').on('click','.eliminar', function(){

		var urEliminar = '{$_Parametros.url}modIN/normativaCONTROL/EliminarNormativaMET';
		var pk_num_normativa_legal =  $(this).attr('pk_num_normativa_legal');
		var titulo =  $(this).attr('titulo');
		var rutaPdf = $(this).attr('rutaPdf');
		var fila = $(this);
		swal({
			title:'Eliminar Normativa?',
			text: 'SE ELIMINARÁ: '+titulo,
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: 'Eliminar',
			closeOnConfirm: false
		}, function(){
			$.post(urEliminar, { pk_num_normativa_legal: pk_num_normativa_legal, rutaPdf: rutaPdf }, function(resp){
				if(resp > 0)
				{
					tabla_todas.row( fila.parents('tr') )
							.remove()
							.draw();
					swal("Normativa Eliminada", "Satisfactoriamente", "success");
				}
				else
				{
					alert(resp);
				}

			});
		});

	});

	//------------------------------------------------------------
	$('#datatable2 tbody').on('click','.eliminar', function(){

		var urEliminar = '{$_Parametros.url}modIN/normativaCONTROL/EliminarNormativaMET';
		var pk_num_normativa_legal =  $(this).attr('pk_num_normativa_legal');
		var titulo =  $(this).attr('titulo');
		var rutaPdf = $(this).attr('rutaPdf');
		var fila = $(this);
		swal({
			title:'Eliminar Ley?',
			text: 'SE ELIMINARÁ: '+titulo,
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: 'Eliminar',
			closeOnConfirm: false
		}, function(){
			$.post(urEliminar, { pk_num_normativa_legal: pk_num_normativa_legal, rutaPdf: rutaPdf }, function(resp){
				if(resp > 0)
				{
					tabla_ley.row( fila.parents('tr') )
							.remove()
							.draw();
					swal("Ley Eliminada", "Satisfactoriamente", "success");
				}
				else
				{
					alert(resp);
				}

			});
		});

	});
	//------------------------------------------------------------
	$('#datatable3 tbody').on('click','.eliminar', function(){

		var urEliminar = '{$_Parametros.url}modIN/normativaCONTROL/EliminarNormativaMET';
		var pk_num_normativa_legal =  $(this).attr('pk_num_normativa_legal');
		var titulo =  $(this).attr('titulo');
		var rutaPdf = $(this).attr('rutaPdf');
		var fila = $(this);
		swal({
			title:'Eliminar Normativa Legal?',
			text: 'SE ELIMINARÁ: '+titulo,
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: 'Eliminar',
			closeOnConfirm: false
		}, function(){
			$.post(urEliminar, { pk_num_normativa_legal: pk_num_normativa_legal, rutaPdf: rutaPdf }, function(resp){
				if(resp > 0)
				{
					tabla_normas_r_m.row( fila.parents('tr') )
							.remove()
							.draw();
					swal("Normativa Eliminada", "Satisfactoriamente", "success");
				}
				else
				{
					alert(resp);
				}

			});
		});

	});
	//------------------------------------------------------------
	$('#datatable4 tbody').on('click','.eliminar', function(){

		var urEliminar = '{$_Parametros.url}modIN/normativaCONTROL/EliminarNormativaMET';
		var pk_num_normativa_legal =  $(this).attr('pk_num_normativa_legal');
		var titulo =  $(this).attr('titulo');
		var rutaPdf = $(this).attr('rutaPdf');
		var fila = $(this);
		swal({
			title:'Eliminar Circular?',
			text: 'SE ELIMINARÁ EL CIRCULAR: '+titulo,
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: 'Eliminar',
			closeOnConfirm: false
		}, function(){
			$.post(urEliminar, { pk_num_normativa_legal: pk_num_normativa_legal, rutaPdf: rutaPdf }, function(resp){
				if(resp > 0)
				{
					tabla_circular.row( fila.parents('tr') )
							.remove()
							.draw();
					swal("Circular Eliminado!", "Satisfactoriamente", "success");
				}
				else
				{
					alert(resp);
				}

			});
		});

	});






//**************************************************************
//*   FILTRO Y ACTUALIZACIÓN DE LA LISTA
//**************************************************************

var tabla_todas = $('#datatable1').DataTable();
var tabla_normas_r_m;
var	tabla_ley;
var tabla_circular;
 $("#formAjax").validate({
    
    submitHandler: function(form) {

    	var url_listar='{$_Parametros.url}modIN/normativaCONTROL/BuscarNormativaFiltroMET';
		$.post(url_listar,$(form).serialize(),function(respuesta_post){
			
			//****************************************************
			//*   DATA TABLE
			//****************************************************
			
			if(respuesta_post != 0 && respuesta_post != 2 )
			{
				var tipo = $('select[id="ind_tipo_normativa"]').val();
				if(tipo == 'L')//leyes
				{

					var cad_t = $('#ultima_tabla').val();

					if(cad_t != 'datatable2')//tabla activa actual
					{
						$('#datatable2').DataTable({ //ley
							"dom": 'lCfrtip',
							"order": [],
							"colVis": {
								"buttonText": "Columnas",
								"overlayFade": 0,
								"align": "right"
							},
							"language": {
								"lengthMenu": 'Mostrar _MENU_',
								"search": '<i class="fa fa-search"></i>',
								"paginate": {
									"previous": '<i class="fa fa-angle-left"></i>',
									"next": '<i class="fa fa-angle-right"></i>'
								}
							}
						});
					}

					tabla_ley = $('#datatable2').DataTable();

					if(cad_t == 'datatable1')//todas
					{
							tabla_todas.destroy();
							$('#datatable1').removeClass("visible_norm_legal").addClass( "invisible_norm_legal" );


					}
					else
					{
						if (cad_t == 'datatable3')//normas, resoluciones y manuales
						{
							tabla_normas_r_m.destroy();
							$('#datatable3').removeClass("visible_norm_legal").addClass("invisible_norm_legal");


						}
						else {
							if (cad_t == 'datatable4')//circulares
							{
								tabla_circular.destroy();
								$('#datatable4').removeClass("visible_norm_legal").addClass("invisible_norm_legal");
							}


						}
					}

					tabla_ley.clear().draw();
					$('#datatable2').removeClass("invisible_norm_legal").addClass("visible_norm_legal");
					$('#ultima_tabla').val('datatable2');

					//CARGA DE DATOS EN LA TABLA*************************************************
					var contador = 0;
					var cad_modificar_ley = '';
					var cad_eliminar_ley = '';
					for(var i=0; i<respuesta_post.length; i++)
					{
						if(respuesta_post[i].ind_tipo_ley_circular == 'NAC')
							respuesta_post[i].ind_tipo_ley_circular = 'Nacional';
						else
							respuesta_post[i].ind_tipo_ley_circular = 'Estadal';

						{if in_array('IN-01-03-02-01-M',$_Parametros.perfil)}
						cad_modificar_ley = '<button id="btn_leyes_3" type="button"  pk_num_normativa_legal = "'+respuesta_post[i].pk_num_normativa_legal+'" class="editar gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal2" data-toggle="modal" data-keyboard="false" data-backdrop="static" ><i class="fa fa-edit"></i></button>';
						{else}
						cad_modificar_ley = '<i class="md-not-interested"></i>';
						{/if}

						{if in_array('IN-01-03-02-02-E',$_Parametros.perfil)}
						cad_eliminar_ley = '<button id="btn_leyes_4" rutaPdf="'+respuesta_post[i].ind_ruta_archivo+'" titulo="'+respuesta_post[i].ind_descripcion+'" type="button"  pk_num_normativa_legal = "'+respuesta_post[i].pk_num_normativa_legal+'" class="eliminar gsUsuario btn ink-reaction btn-raised btn-xs btn-danger"><i class="md-delete "></i></button>';
						{else}
						cad_eliminar_ley = '<i class="md-not-interested"></i>';
						{/if}


						contador = i+1;

						tabla_ley.row.add([
							contador,
							'Ley',
							respuesta_post[i].ind_descripcion,
							respuesta_post[i].ind_gaceta_circular,
							respuesta_post[i].fec_gaceta_circular,
							respuesta_post[i].ind_tipo_ley_circular,
							'<button id="btn_leyes_1"  type="button" titulo="'+respuesta_post[i].ind_descripcion+'"  pk_num_normativa_legal = "'+respuesta_post[i].pk_num_normativa_legal+'" class="interpretacion gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal2" data-toggle="modal" data-keyboard="false" data-backdrop="static" ><i class="md-my-library-books"></i></button>',
							'<button id="btn_leyes_2" type="button" titulo="Ley" rutaPdf="'+respuesta_post[i].ind_ruta_archivo+'"  pk_num_normativa_legal = "'+respuesta_post[i].pk_num_normativa_legal+'" class="ver gsUsuario btn ink-reaction btn-raised btn-xs btn-info" data-target="#formModal2" data-toggle="modal" data-keyboard="false" data-backdrop="static" ><i class="md md-search"></i></button>',
							cad_modificar_ley,
							cad_eliminar_ley
						]).draw()
								.nodes()
								.to$()
								.addClass( '' );
					}


					//**********************************************************************

				}
				else
				{
					if(tipo == 'N' || tipo == 'R' || tipo == 'M' )//normativa, resoluciones y manuales
					{
						var cad_t = $('#ultima_tabla').val();

						if(cad_t != 'datatable3')//tabla activa actual
						{
							$('#datatable3').DataTable({ //ley
								"dom": 'lCfrtip',
								"order": [],
								"colVis": {
									"buttonText": "Columnas",
									"overlayFade": 0,
									"align": "right"
								},
								"language": {
									"lengthMenu": 'Mostrar _MENU_',
									"search": '<i class="fa fa-search"></i>',
									"paginate": {
										"previous": '<i class="fa fa-angle-left"></i>',
										"next": '<i class="fa fa-angle-right"></i>'
									}
								}
							});
						}

						tabla_normas_r_m = $('#datatable3').DataTable();

						if(cad_t == 'datatable1')//todas
						{
							tabla_todas.destroy();
							$('#datatable1').removeClass("visible_norm_legal").addClass( "invisible_norm_legal" );


						}
						else
						{
							if (cad_t == 'datatable2')//ley
							{
								tabla_ley.destroy();
								$('#datatable2').removeClass("visible_norm_legal").addClass("invisible_norm_legal");


							}
							else {
								if (cad_t == 'datatable4')//circulares
								{
									tabla_circular.destroy();
									$('#datatable4').removeClass("visible_norm_legal").addClass("invisible_norm_legal");
								}


							}
						}

						tabla_normas_r_m.clear().draw();
						$('#datatable3').removeClass("invisible_norm_legal").addClass("visible_norm_legal");
						$('#ultima_tabla').val('datatable3');

						//CARGA DE DATOS EN LA TABLA--------------------------------------------------------------
						var contador = 0;
						var cad_modificar_normativas = '';
						var cad_eliminar_normativas = '';
						var tipo_normativa = $('#ind_tipo_normativa option:selected').text();
						for(var i=0; i<respuesta_post.length; i++)
						{

							{if in_array('IN-01-03-02-01-M',$_Parametros.perfil)}
							cad_modificar_normativas = '<button id="btn_norma_r_m_2" type="button"  pk_num_normativa_legal = "'+respuesta_post[i].pk_num_normativa_legal+'" class="editar gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal2" data-toggle="modal" data-keyboard="false" data-backdrop="static" ><i class="fa fa-edit"></i></button>';
							{else}
							cad_modificar_normativas = '<i class="md-not-interested"></i>';
							{/if}

							{if in_array('IN-01-03-02-02-E',$_Parametros.perfil)}
							cad_eliminar_normativas = '<button id="btn_norma_r_m_3" rutaPdf="'+respuesta_post[i].ind_ruta_archivo+'" titulo="'+respuesta_post[i].ind_descripcion+'" type="button"  pk_num_normativa_legal = "'+respuesta_post[i].pk_num_normativa_legal+'" class="eliminar gsUsuario btn ink-reaction btn-raised btn-xs btn-danger" ><i class="md-delete"></i></button>';
							{else}
							cad_eliminar_normativas = '<i class="md-not-interested"></i>';
							{/if}

							contador = i+1;

							tabla_normas_r_m.row.add([
								contador,
								tipo_normativa,
								respuesta_post[i].ind_descripcion,
								respuesta_post[i].ind_gaceta_circular,
								respuesta_post[i].fec_gaceta_circular,
								respuesta_post[i].ind_resolucion,
								respuesta_post[i].fec_resolucion,
								'<button id="btn_norma_r_m_1" type="button" titulo="'+tipo_normativa+'" rutaPdf="'+respuesta_post[i].ind_ruta_archivo+'"  pk_num_normativa_legal = "'+respuesta_post[i].pk_num_normativa_legal+'" class="ver gsUsuario btn ink-reaction btn-raised btn-xs btn-info" data-target="#formModal2" data-toggle="modal" data-keyboard="false" data-backdrop="static" ><i class="md md-search"></i></button>',
								cad_modificar_normativas,
								cad_eliminar_normativas
							]).draw()
									.nodes()
									.to$()
									.addClass( '' );
						}
						//**********************************************************************

					}
					else //Circular
					{
						if(tipo == 'C' )
						{
							var cad_t = $('#ultima_tabla').val();

							if(cad_t != 'datatable4')//tabla activa actual
							{
								$('#datatable4').DataTable({ //circular
									"dom": 'lCfrtip',
									"order": [],
									"colVis": {
										"buttonText": "Columnas",
										"overlayFade": 0,
										"align": "right"
									},
									"language": {
										"lengthMenu": 'Mostrar _MENU_',
										"search": '<i class="fa fa-search"></i>',
										"paginate": {
											"previous": '<i class="fa fa-angle-left"></i>',
											"next": '<i class="fa fa-angle-right"></i>'
										}
									}
								});
							}

							tabla_circular = $('#datatable4').DataTable();

							if(cad_t == 'datatable1')//todas
							{
								tabla_todas.destroy();
								$('#datatable1').removeClass("visible_norm_legal").addClass( "invisible_norm_legal" );


							}
							else
							{
								if (cad_t == 'datatable2')//ley
								{
									tabla_ley.destroy();
									$('#datatable2').removeClass("visible_norm_legal").addClass("invisible_norm_legal");


								}
								else
								{
									if (cad_t == 'datatable3')//normas resoluciones manuales
									{
										tabla_normas_r_m.destroy();
										$('#datatable3').removeClass("visible_norm_legal").addClass("invisible_norm_legal");
									}


								}
							}

							tabla_circular.clear().draw();
							$('#datatable4').removeClass("invisible_norm_legal").addClass("visible_norm_legal");
							$('#ultima_tabla').val('datatable4');

							//CARGA DE DATOS EN LA TABLA----------------------------------------------------
							var contador = 0;
							var tipo_normativa = $('#ind_tipo_normativa option:selected').text();
							var cad_emitido;
							var cad_dependencia;
							var cad_modificar_circulares = '';
							var cad_eliminar_circulares = '';
							for(var i=0; i<respuesta_post.length; i++)
							{

								{if in_array('IN-01-03-02-01-M',$_Parametros.perfil)}
								cad_modificar_circulares = '<button id="btn_norma_r_m_2" type="button"  pk_num_normativa_legal = "'+respuesta_post[i].pk_num_normativa_legal+'" class="editar gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal2" data-toggle="modal" data-keyboard="false" data-backdrop="static" ><i class="fa fa-edit"></i></button>';
								{else}
								cad_modificar_circulares = '<i class="md-not-interested"></i>';
								{/if}

								{if in_array('IN-01-03-02-02-E',$_Parametros.perfil)}
								cad_eliminar_circulares = '<button id="btn_norma_r_m_3" rutaPdf="'+respuesta_post[i].ind_ruta_archivo+'" titulo="'+respuesta_post[i].ind_descripcion+'" type="button"  pk_num_normativa_legal = "'+respuesta_post[i].pk_num_normativa_legal+'" class="eliminar gsUsuario btn ink-reaction btn-raised btn-xs btn-danger" ><i class="md-delete"></i></button>';
								{else}
								cad_eliminar_circulares = '<i class="md-not-interested"></i>';
								{/if}

								if(respuesta_post[i].ind_tipo_ley_circular == 'INT')
								{
									cad_emitido = 'Interno';
								}
								else
								{
									cad_emitido = respuesta_post[i].ind_tipo_ley_circular;
								}

								if(respuesta_post[i].fk_a004_num_dependencia)
								{
									cad_dependencia = respuesta_post[i].fk_a004_num_dependencia;
								}
								else
								{
									cad_dependencia = '---';
								}

								contador = i+1;

								tabla_circular.row.add([
									contador,
									tipo_normativa,
									respuesta_post[i].ind_descripcion,
									respuesta_post[i].ind_gaceta_circular,
									respuesta_post[i].fec_gaceta_circular,
									cad_emitido,
									cad_dependencia,
									'<button id="btn_norma_r_m_1" titulo="Circular" rutaPdf="'+respuesta_post[i].ind_ruta_archivo+'" type="button"  pk_num_normativa_legal = "'+respuesta_post[i].pk_num_normativa_legal+'" class="ver gsUsuario btn ink-reaction btn-raised btn-xs btn-info" data-target="#formModal2" data-toggle="modal" data-keyboard="false" data-backdrop="static" ><i class="md md-search"></i></button>',
									cad_modificar_circulares,
									cad_eliminar_circulares
								]).draw()
										.nodes()
										.to$()
										.addClass( '' );
							}


							//**********************************************************************


						}
						else
						{
							if(tipo == 'T')
							{
								var cad_t = $('#ultima_tabla').val();

								if(cad_t != 'datatable1')//tabla activa actual
								{
									$('#datatable1').DataTable({ //circular
										"dom": 'lCfrtip',
										"order": [],
										"colVis": {
											"buttonText": "Columnas",
											"overlayFade": 0,
											"align": "right"
										},
										"language": {
											"lengthMenu": 'Mostrar _MENU_',
											"search": '<i class="fa fa-search"></i>',
											"paginate": {
												"previous": '<i class="fa fa-angle-left"></i>',
												"next": '<i class="fa fa-angle-right"></i>'
											}
										}
									});
								}

								tabla_todas = $('#datatable1').DataTable();

								if(cad_t == 'datatable2')//leyes
								{
									tabla_ley.destroy();
									$('#datatable2').removeClass("visible_norm_legal").addClass( "invisible_norm_legal" );


								}
								else
								{
									if (cad_t == 'datatable3')//normativa manuales resolucion
									{
										tabla_normas_r_m.destroy();
										$('#datatable3').removeClass("visible_norm_legal").addClass("invisible_norm_legal");


									}
									else
									{
										if (cad_t == 'datatable4')//circulares
										{
											tabla_circular.destroy();
											$('#datatable4').removeClass("visible_norm_legal").addClass("invisible_norm_legal");
										}


									}
								}

								tabla_todas.clear().draw();
								$('#datatable1').removeClass("invisible_norm_legal").addClass("visible_norm_legal");
								$('#ultima_tabla').val('datatable1');

								//CARGA DE DATOS EN LA TABLA-------------------------------------------------------------------
								var contador = 0;
								var cad_ambito;
								var cad_emitido;
								var cad_modificar_todas_n = '';
								var cad_eliminar_todas_n = '';
								for(var i=0; i<respuesta_post.length; i++)
								{

									{if in_array('IN-01-03-02-01-M',$_Parametros.perfil)}
									cad_modificar_todas_n = '<button id="btn_norma_r_m_2" type="button"  pk_num_normativa_legal = "'+respuesta_post[i].pk_num_normativa_legal+'" class="editar gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal2" data-toggle="modal" data-keyboard="false" data-backdrop="static" ><i class="fa fa-edit"></i></button>';
									{else}
									cad_modificar_todas_n = '<i class="md-not-interested"></i>';
									{/if}

									{if in_array('IN-01-03-02-02-E',$_Parametros.perfil)}
									cad_eliminar_todas_n = '<button id="btn_norma_r_m_3" rutaPdf="'+respuesta_post[i].ind_ruta_archivo+'" titulo="'+respuesta_post[i].ind_descripcion+'" type="button"  pk_num_normativa_legal = "'+respuesta_post[i].pk_num_normativa_legal+'" class="eliminar gsUsuario btn ink-reaction btn-raised btn-xs btn-danger" ><i class="md-delete"></i></button>';
									{else}
									cad_eliminar_todas_n = '<i class="md-not-interested"></i>';
									{/if}

									switch (respuesta_post[i].ind_tipo_normativa)
									{
										case "L" :
											respuesta_post[i].ind_tipo_normativa = "Ley";
											respuesta_post[i].ind_resolucion = "---";
											respuesta_post[i].fec_resolucion = "---";
											if(respuesta_post[i].ind_tipo_ley_circular == 'NAC')
											{
												cad_ambito = 'Nacional';
											}
											else
											{
												cad_ambito = 'Estadal';
											}
											cad_emitido = "---";
											break;
										case "N":
											respuesta_post[i].ind_tipo_normativa = "Normativa";
											cad_ambito = '---';
											cad_emitido = "---";
											break;
										case "C":
											respuesta_post[i].ind_tipo_normativa = "Circular";
											if( respuesta_post[i].ind_tipo_ley_circular == 'INT')
											{
												cad_emitido = 'Interno';
											}
											else
											{
												cad_emitido = 'CGR';
											}
											respuesta_post[i].ind_resolucion = "---";
											respuesta_post[i].fec_resolucion = "---";
											cad_ambito = '---';

											break;
										case "R":
											respuesta_post[i].ind_tipo_normativa = "Resolución";
											cad_ambito = '---';
											cad_emitido = "---";
											break;
										case "M":
											respuesta_post[i].ind_tipo_normativa = "Manual";
											cad_ambito = '---';
											cad_emitido = "---";
											break;
										default:
											alert('error casos...');
									}

									contador = i+1;

									tabla_todas.row.add([
										contador,
										respuesta_post[i].ind_tipo_normativa,
										respuesta_post[i].ind_descripcion,
										respuesta_post[i].ind_gaceta_circular,
										respuesta_post[i].fec_gaceta_circular,
										respuesta_post[i].ind_resolucion,
										respuesta_post[i].fec_resolucion,
										cad_ambito,
										cad_emitido,
										'<button id="btn_norma_r_m_1" rutaPdf="'+respuesta_post[i].ind_ruta_archivo+'" type="button" titulo="'+respuesta_post[i].ind_tipo_normativa+'"  pk_num_normativa_legal = "'+respuesta_post[i].pk_num_normativa_legal+'" class="ver gsUsuario btn ink-reaction btn-raised btn-xs btn-info" data-target="#formModal2" data-toggle="modal" data-keyboard="false" data-backdrop="static" ><i class="md md-search"></i></button>',
										cad_modificar_todas_n,
										cad_eliminar_todas_n
									]).draw()
											.nodes()
											.to$()
											.addClass( '' );
								}


								//**********************************************************************

							}
							else
							{
								alert('error al mostrar las tablas');
							}

						}
					}
				}


			}
			else
			{

					if(respuesta_post == 0)
					{
						swal("No hay resultados!", "Seleccione otro criterio de búsqueda...", "warning");

					}
					else
					{
						swal("Error!", "Error al buscar la data: "+respuesta_post, "error");
					}



			}
			
			//****************************************************
			//*   
			//****************************************************

		
		}, 'json');

    }

 });


});


</script>