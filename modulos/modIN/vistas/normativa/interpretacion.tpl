<section class="style-default-light">&nbsp;
    <div class="row">
            <div class="col-lg-12">
                <div class="card card-underline card-outlined style-gray" >
                    <div class="card-head" >
                        <header><i class="md-toc"></i>&nbsp;Listado de Interpretaciones - Total <span id="total_inter" >{$total_inter}</span></header>

                    </div>
                    <div class="card-body no-padding height-6 scroll style-default-bright" style="margin:10px; padding:0px; " >
                       <div id="contenedor_lista" style="height: 300px;">
                        {if $band > 0 }
                            <ul id="listado_inter" class="list divider-full-bleed">
                               {foreach name=for_interpretacion item=indice from=$datosInterpretacion}
                               <li class="tile">
                                   <a class="tile-content ink-reaction" >
                                       <div class="tile-icon">
                                           <i class="md-label-outline"> {$smarty.foreach.for_interpretacion.iteration}</i>
                                       </div>
                                       <div class="tile-text" ><div class="text-xs">{$indice.ind_descripcion}</div></div>
                                   </a>

                                   <a class="btn btn-flat ink-reaction ver_inter btn-info " title="Ver/Descargar" rutaPdf_inter="{$indice.ind_ruta_archivo}" pk_num_interpretacion="{$indice.pk_num_interpretacion}" data-target="#formModal3" data-toggle="modal" >
                                       <i class="fa md-search "></i>
                                   </a>
                                   {if in_array('IN-01-03-02-04-E',$_Parametros.perfil)}
                                   <a class="btn btn-flat ink-reaction eliminar_inter btn-danger " title="Eliminar" titulo_interpretacion="{$indice.ind_descripcion}" pk_num_interpretacion="{$indice.pk_num_interpretacion}" rutaPdf_inter="{$indice.ind_ruta_archivo}" >
                                       <i class="fa fa-trash"></i>
                                   </a>
                                   {/if}
                               </li>
                              {/foreach}
                           </ul>
                        {else}
                            <ul id="listado_inter" class="list divider-full-bleed">
                                <li class="tile">
                                    <a class="tile-content ink-reaction" href="#2">
                                        <div class="tile-icon">
                                            <i class="md-label-outline"></i>
                                        </div>
                                        <div class="tile-text" ><div class="text-xs">NO HAY INTERPRETACIONES...</div></div>
                                    </a>

                                </li>
                            </ul>
                        {/if}
                       </div>
                    </div>
                    {*<hr class="ruler-sm">*}
                    {if in_array('IN-01-03-02-03-N',$_Parametros.perfil)}
                    <div class="card-body no-padding style-default " style="margin:0px; padding-top:10px !important;
                     padding-right: 10px !important; padding-left: 10px !important;" >
                        <form id="formAjax2" class="form form-validation" role="form" novalidate="novalidate" >
                            <div class="col-lg-9">
                                <div class="form-group"  >
                                    <input style="text-transform: uppercase" type="text" name="descripcion_interpretacion" id="descripcion_interpretacion" class="form-control" required aria-required="true" aria-invalid="true" onkeypress="return validarDescripcionInter(event)" >
                                    <label for="descripcion_interpretacion"><span class="text-default-dark">Descripción</span></label>
                                    <p class="help-block"><span class="text-default-dark">Obligatorio</span></p>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group"  >
                                    <input type="file" name="ind_ruta_pdf_inter" id="ind_ruta_pdf_inter" style="display: none" />
                                    <input type="hidden" name="ind_ruta_pdf_inter_f" id="ind_ruta_pdf_inter_f" value="1" >
                                    <div id="pdfCargado" align="center">
                                        <img src="{$_Parametros.ruta_Img}modIN/pdf_icon_transp.png" alt="Cargar PDF" title="Cargar PDF" id="cargarPdfInter" style="cursor: pointer;" width="70"/>
                                        <div align="center" id="cargarPdf_descripcion_pdf" style=" width: 180px; "></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12" align="left">
                                <button id="botonCerrar" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal" type="button"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                                <button type="submit" id="botonRegistrarInter" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" ><span class="glyphicon glyphicon-floppy-disk"></span> añadir interpretacion</button>
                            </div>
                            <div class="col-lg-12" align="left">
                                &nbsp;
                            </div>
                            <input type="hidden" id="pk_num_normativa_inter" name="pk_num_normativa_inter" value="{$pk_num_normativa_legal}">
                        </form>
                    </div>
                    {/if}
                </div>
            </div>
    </div>
</section>

<script>

function validarDescripcionInter(evt){

    var charCode = evt.which || evt.keyCode;
    if (charCode==8||charCode==13||charCode==0 || charCode==37 || charCode==39)//37 izq 39 der
        return true;
    var str = String.fromCharCode(charCode);
    if (!/[0-9\.\A-Z\a-z\á\é\í\ó\ú\ñ\Ñ\š\ü\,\;\(\)\-\ ]/.test(str)) {
        return false;//evt.preventDefault();
    };
}

$(document).ready(function() {

    $("#cargarPdfInter").click(function () {

        $("#ind_ruta_pdf_inter").click();
    });

    $("#ind_ruta_pdf_inter").change(function (e) {

        var files = e.target.files;
        var file = $("#ind_ruta_pdf_inter")[0].files[0];
        var fileName = file.name;

        var aux_i = fileName.split('.');
        var aux_i2 = 0;
        if ( aux_i.length > 2 || fileName.indexOf(',') > -1 || fileName.indexOf('"') > -1 || fileName.indexOf("'") > -1 || fileName.indexOf('(') > -1 || fileName.indexOf(')') > -1 || fileName.indexOf('}') > -1 )
        {
            aux_i2 = 1;
        }

        if(aux_i2 == 0)
        {
            fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
            if (fileExtension == 'pdf' || fileExtension == 'PDF')
            {
                $("#cargarPdfInter").attr("src", "{$_Parametros.ruta_Img}modIN/ico-pdf-ok_transp.png");
                $("#cargarPdf_descripcion_pdf").html(fileName.replace(/_/g,' '));
                $('#ind_ruta_pdf_inter_f').val('1');
            }
            else
            {
                swal("¡Extensión de archivo no válido!", "Asegurese de subir un archivo de tipo: pdf/PDF", "warning");
                $('#ind_ruta_pdf_inter_f').val('2');
            }
        }
        else
        {
            swal("¡Nombre de archivo no válido!", "Renombre el archivo, sin caracteres especiales", "warning");
            $('#ind_ruta_pdf_inter_f').val('2');
        }
    });

    $("#formAjax2").validate({
        submitHandler: function (form) {

            if($('#ind_ruta_pdf_inter_f').val() == '1' )
            {
                var ind_ruta_pdf_inter = $("#ind_ruta_pdf_inter").val();
                if (ind_ruta_pdf_inter != '')
                {

                    var url_normativa = '{$_Parametros.url}modIN/normativaCONTROL/RegistrarInterpretacionMET';
                    var data = new FormData($("#formAjax2")[0]);
                    $.ajax({

                        url: url_normativa,
                        data: data,
                        type: 'POST',
                        dataType: 'json',
                        contentType: false,
                        processData: false,
                        success: function (respuesta_post) {

                            if (respuesta_post < 0) {

                                swal("Error!", "Error: " + respuesta_post, "error");

                            } else {

                                var cad = '';
                                var total = respuesta_post.length;
                                for (var i = 0; i < total; i++) {
                                    //md-local-library
                                    cad = cad + '<li class="tile"><a class="tile-content ink-reaction" href="#2"><div class="tile-icon"><i class="md-label-outline "> ' + (i + 1) + '</i></div><div class="tile-text" ><div class="text-xs">' + respuesta_post[i].ind_descripcion + '</div></div></a><a class="btn btn-flat ink-reaction ver_inter btn-info" rutaPdf_inter="' + respuesta_post[i].ind_ruta_archivo + '" title="Ver/Descargar" pk_num_interpretacion="' + respuesta_post[i].pk_num_interpretacion + '" data-target="#formModal3" data-toggle="modal" ><i class="fa md-search "></i></a>{if in_array('IN-01-03-02-04-E',$_Parametros.perfil)}<a class="btn btn-flat ink-reaction eliminar_inter btn-danger" title="Eliminar" titulo_interpretacion="' + respuesta_post[i].ind_descripcion + '" rutaPdf_inter="' + respuesta_post[i].ind_ruta_archivo + '" pk_num_interpretacion="' + respuesta_post[i].pk_num_interpretacion + '"><i class="fa fa-trash"></i></a>{/if}</li>';

                                }
                                $("#total_inter").html(total);
                                $("#listado_inter").html('');
                                $("#listado_inter").html(cad);
                                //---------------

                                $("#descripcion_interpretacion").val('');
                                $("#ind_ruta_pdf_inter").val('');
                                $("#cargarPdfInter").attr("src", "{$_Parametros.ruta_Img}modIN/pdf_icon_transp.png");
                                $("#cargarPdf_descripcion_pdf").html('');
                            }

                        }
                    });

                }
                else
                {

                    swal("¡El archivo es requerido!", "Solo archivo con extensión pdf/PDF", "warning");

                }
            }
            else
            {
                swal("¡El archivo es requerido!", "Solo archivo con extensión pdf/PDF", "warning");
            }

        }
    });

    $('#listado_inter').on('click', '.ver_inter', function () {


        var rutaPdf = $(this).attr('rutaPdf_inter');

        $('#modalAncho3').css("width", "75%");
        $('#formModalLabel3').html("");
        $('#formModalLabel3').html('Ver Interpretación');
        $('#ContenidoModal3').html("");

        var urlReporte = '{$_Parametros.url}publico/imagenes/modIN/interpretacion/' + rutaPdf;
        $('#ContenidoModal3').html('<iframe src="' + urlReporte + '" width="100%" height="950"></iframe>');
    });

    $('#listado_inter').on('click', '.eliminar_inter', function () {

        var urEliminar = '{$_Parametros.url}modIN/normativaCONTROL/EliminarInterpretacionMET';
        var pk_num_interpretacion =  $(this).attr('pk_num_interpretacion');
        var titulo =  $(this).attr('titulo_interpretacion');
        var rutaPdf = $(this).attr('rutaPdf_inter');
        var objeto = $(this);

        swal({
            title:'¿Descartar la Interpretación?',
            text: 'SE ELIMINARÁ: '+titulo,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: 'Descartar',
            closeOnConfirm: false
        }, function(){
            $.post(urEliminar, { pk_num_interpretacion: pk_num_interpretacion, rutaPdf: rutaPdf }, function(resp){
                if(resp > 0)
                {
                    objeto.closest('li').remove();
                    swal("Interpretacion Eliminada", "Satisfactoriamente", "success");
                }
                else
                {
                    swal("Error!", "Error: " + resp, "error");
                }

            });
        });
    });

});

</script>


