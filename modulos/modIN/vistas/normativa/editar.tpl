<section class="style-default-light">&nbsp;

<!--CUERPO DEL FORMULARIO-->
    <form id="formAjax2" class="form form-validation" role="form" novalidate="novalidate" >
<div class="row">
        <div class="col-lg-12">
			<div class="card">
				<div class="card-body">


                       <div class="row" >
                           <div class="col-sm-6">
                               <div class="form-group" >
                                   <select id="ind_tipo_normativa2" required="" class="form-control" name="ind_tipo_normativa2" >
                                       <option value="" selected="selected" ></option>
                                       <option value="L"  >Leyes</option>
                                       <option value="N" >Normativas</option>
                                       <option value="C" >Circulares</option>
                                       <option value="R" >Resoluciones</option>
                                       <option value="M" >Manuales de Normas y Procedimientos</option>
                                   </select>
                                   <label for="ind_tipo_normativa2">Tipo de Normativa</label>
                               </div>
                           </div>
                           <div>
                               <div class="col-sm-6">
                                   &nbsp;
                               </div>
                           </div>
                       </div>

                       <div class="row" id="fila_ley">
                                   <div class="col-sm-6">
                                       <div class="form-group"  >
                                           <input type="text" name="num_gaceta_ley_edt" id="num_gaceta_ley_edt" class="form-control" data-rule-number="true"  required aria-required="true" aria-invalid="true" >
                                           <label for="num_gaceta_ley_edt">Número de gaceta</label>
                                           <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                       </div>
                                   </div>
                                   <div class="col-sm-6">
                                       <div class="form-group">
                                           <input type="text" name="fec_publicacion_gaceta_ley_edt" id="fec_publicacion_gaceta_ley_edt" class="form-control" required aria-required="true" aria-invalid="true" >
                                           <label for="fec_publicacion_gaceta_ley_edt" class="control-label">Fecha de publicación de la gaceta</label>
                                           <p class="help-block">DD-MM-YYYY <span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                       </div>
                                   </div>


                                   <div class="col-sm-8">
                                       <div class="form-group" >
                                           <input style="text-transform: uppercase"  type="text" name="ind_descripcion_ley_edt" id="ind_descripcion_ley_edt" class="form-control" required aria-required="true" aria-invalid="true" onkeypress="return validarDescripcion(event)" >
                                           <label for="ind_descripcion_ley_edt">Descripción/Título/Resumen/Vista prevía parcial</label>
                                           <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                       </div>
                                   </div>
                                   <div class="col-sm-4">
                                       <div class="form-group" >
                                           <select id="ind_tipo_ley_edt" class="form-control " name="ind_tipo_ley_edt" required aria-required="true" aria-invalid="true" >
                                               <option value="" selected="selected">Selccione</option>
                                               <option value="NAC" >Nacionales</option>
                                               <option value="EST" >Estadales</option>
                                           </select>
                                           <label for="ind_tipo_ley_edt">Ámbito</label>
                                           <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                       </div>
                                   </div>
                       </div>

                       <div class="row invisible_norm_legal" id="fila_normativa"  >

                           <div class="col-sm-4">
                               <div class="form-group" >
                                   <input disabled="disabled" type="text" name="num_gaceta_normativa_edt" id="num_gaceta_normativa_edt" class="form-control" data-rule-number="true" required aria-required="true" aria-invalid="true" >
                                   <label for="num_gaceta_normativa_edt">Número de gaceta</label>
                                   <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                               </div>
                           </div>
                           <div class="col-sm-4">
                               <div class="form-group">
                                   <input disabled="disabled" type="text" name="fec_publicacion_gaceta_normativa_edt" id="fec_publicacion_gaceta_normativa_edt" class="form-control" required aria-required="true" aria-invalid="true"  >
                                   <label for="fec_publicacion_gaceta_normativa_edt" class="control-label">Fecha de publicación de la gaceta</label>
                                   <p class="help-block">DD-MM-YYYY <span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                               </div>
                           </div>
                           <div class="col-sm-4">
                               <div class="form-group" >
                                   <input style="text-transform: uppercase" disabled="disabled" type="text" name="num_resolucion_normativa_edt" id="num_resolucion_normativa_edt" class="form-control" required aria-required="true" aria-invalid="true" onkeypress="return validarNumeroResolucion(event)" >
                                   <label for="num_resolucion_normativa_edt">Numero de resolución</label>
                                   <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                               </div>
                           </div>
                           <div class="col-sm-4">
                               <div class="form-group">
                                   <input disabled="disabled" type="text" name="fec_publicacion_resolucion_normativa_edt" id="fec_publicacion_resolucion_normativa_edt" class="form-control" required aria-required="true" aria-invalid="true" >
                                   <label for="fec_publicacion_resolucion_normativa_edt" class="control-label">Fecha de publicación de la resolución</label>
                                   <p class="help-block">DD-MM-YYYY <span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                               </div>
                           </div>
                           <div class="col-sm-8">
                               <div class="form-group" >
                                   <input disabled="disabled" type="text" name="ind_descripcion_normativa_edt" id="ind_descripcion_normativa_edt" class="form-control" required aria-required="true" aria-invalid="true" onkeypress="return validarDescripcion(event)" style="text-transform: uppercase"  >
                                   <label for="ind_descripcion_normativa_edt">Descripción/Título/Resumen/Vista prevía parcial</label>
                                   <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                               </div>
                           </div>

                       </div>

                       <div class="row invisible_norm_legal " id="fila_circulares"   >

                           <div class="col-sm-4">
                              <div class="form-group" >
                                   <select disabled="disabled" id="ind_tipo_circular_edt" required aria-required="true" aria-invalid="true" data-msg-required="Seleccione el Organismo que emite el circular" class="form-control " name="ind_tipo_circular_edt" >
                                       <option value="" selected="selected">Seleccione...</option>
                                       <option value="INT" >{$datosOrganismo[0]['ind_descripcion_empresa'] }</option>
                                       <option value="CGR" >CONTRALORÍA GENERAL DE LA REPUBLICA</option>
                                   </select>
                                   <label for="ind_tipo_circular_edt">Fuente</label>
                                   <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                               </div>
                           </div>

                           <div class="col-sm-4">
                               <div class="form-group" >
                                   <input disabled="disabled" type="text" name="num_circular_edt" id="num_circular_edt" class="form-control" required aria-required="true" aria-invalid="true" onkeypress="return validarNumeroGuion(event)" >
                                   <label for="num_circular_edt">Numero de circular/oficio</label>
                                   <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                               </div>
                           </div>

                           <div class="col-sm-4">
                               <div class="form-group">
                                   <input disabled="disabled" type="text" name="fec_publicacion_circular_edt" id="fec_publicacion_circular_edt" class="form-control" required aria-required="true" aria-invalid="true" >
                                   <label for="fec_publicacion_circular_edt" class="control-label">Fecha de publicación</label>
                                   <p class="help-block">DD-MM-YYYY <span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                               </div>
                           </div>

                           <div class="col-sm-8">
                               <div class="form-group" >
                                   <input style="text-transform: uppercase"  disabled="disabled" type="text" name="ind_descripcion_circular_edt" id="ind_descripcion_circular_edt" class="form-control" required aria-required="true" aria-invalid="true" onkeypress="return validarDescripcion(event)" >
                                   <label  for="ind_descripcion_circular_edt">Descripción/Título/Resumen/Vista prevía parcial</label>
                                   <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                               </div>
                           </div>

                           <div class="col-sm-4" >
                               <div class="form-group" >
                                   <select disabled="disabled" id="ind_dependencia_circular_edt" class="form-control " name="ind_dependencia_circular_edt" required aria-required="true" aria-invalid="true" data-msg-required="Seleccione la dependencia" >
                                       <option value="" selected="selected"></option>
                                       {foreach item=indice from=$datosDependencias}
                                           <option value="{$indice.pk_num_dependencia }" >{$indice.ind_dependencia}</option>
                                       {/foreach}
                                   </select>
                                   <label for="ind_dependencia_circular_edt">Dependencia</label>
                               </div>
                           </div>


                       </div>



                        <div class="row invisible_norm_legal" id="fila_resoluciones_manuales"  >

                            <div class="col-sm-4">
                                <div class="form-group" >
                                    <input style="text-transform: uppercase" disabled="disabled" type="text" name="num_resolucion_resolucion_manuales_edt" id="num_resolucion_resolucion_manuales_edt" class="form-control" required aria-required="true" aria-invalid="true" onkeypress="return validarNumeroResolucion(event)" >
                                    <label for="num_resolucion_resolucion_manuales_edt">Número de resoluciòn</label>
                                    <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input disabled="disabled" type="text" name="fec_publicacion_resolucion_resolucion_manuales_edt" id="fec_publicacion_resolucion_resolucion_manuales_edt" class="form-control" required aria-required="true" aria-invalid="true" >
                                    <label for="fec_publicacion_resolucion_resolucion_manuales_edt" class="control-label">Fecha de resolución</label>
                                    <p class="help-block">DD-MM-YYYY <span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group" >
                                    <input disabled="disabled" type="text" name="num_gaceta_resolucion_manuales_edt" id="num_gaceta_resolucion_manuales_edt" class="form-control" data-rule-number="true" required aria-required="true" aria-invalid="true" >
                                    <label for="num_gaceta_resolucion_manuales_edt">Número de gaceta de la resolución</label>
                                    <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input disabled="disabled" type="text" name="fec_publicacion_gaceta_resolucion_manuales_edt" id="fec_publicacion_gaceta_resolucion_manuales_edt" class="form-control" required aria-required="true" aria-invalid="true" >
                                    <label for="fec_publicacion_gaceta_resolucion_manuales_edt" class="control-label">Fecha de gaceta</label>
                                    <p class="help-block">DD-MM-YYYY <span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                </div>
                            </div>

                            <div class="col-sm-8">
                                <div class="form-group" >
                                    <input style="text-transform: uppercase"  disabled="disabled" type="text" name="ind_descripcion_resolucion_manuales_edt" id="ind_descripcion_resolucion_manuales_edt" class="form-control" required aria-required="true" aria-invalid="true" onkeypress="return validarDescripcion(event)" >
                                    <label for="ind_descripcion_resolucion_manuales_edt">Descripción/Título/Resumen/Vista prevía parcial</label>
                                    <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                </div>
                            </div>

                        </div>

                       <div class="row">

                           <div class="col-sm-8" align="center">

                           </div>

                           <div class="col-sm-4" align="center">
                                <div class="form-group"  >
                                    <div id="pdfCargado">
                                        <div class="row">
                                            <img src="{$_Parametros.ruta_Img}modIN/pdf-icon.jpg" alt="Cargar PDF" title="Cargar PDF" id="cargarPdf" style="cursor: pointer;" width="90"/>
                                        </div>
                                        <div class="row" align="center">
                                            <div class="col-sm-12" align="center">
                                            <span id="cargarPdf_descripcion_pdf"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="file" name="ind_ruta_pdf" id="ind_ruta_pdf" style="display: none" />
                                    <input type="hidden" name="ind_ruta_pdf_f" id="ind_ruta_pdf_f" value="{$datosNormativas.ind_ruta_archivo}" >
                                    <input type="hidden" name="ind_tipo_normativa_edit" id="ind_tipo_normativa_edit" >
                                    <input type="hidden" name="pk_num_normativa_legal_edit" id="pk_num_normativa_legal_edit" value="{$datosNormativas.pk_num_normativa_legal}" >
                                </div>

                            </div>

                       </div>
                       <div class="row">
                           &nbsp;
                       </div>
                       <div class="row">
                           <div align="center">
                               <button id="botonCerrar" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal" type="button"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                               <button id="botonGuardar" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" type="submit"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                           </div>
                       </div>

			    </div>
		    </div>
        </div>
</div>
    </form>

<!--FIN DEL TITULO DEL FORMULARIO-->
</section>

<style>

    .invisible_norm_legal{
        display: none;
    }

    .visible_norm_legal{
        display: block;
    }

</style>

<script>
//*******************************************
//*  ASIGNACINACIÓN DE VALORES
//*******************************************
$('select[id="ind_tipo_normativa2"]').prop('value', '{$datosNormativas.ind_tipo_normativa}');
$('select[id="ind_tipo_normativa2"]').change();
$('select[id="ind_tipo_normativa2"]').prop('disabled', true);

var ind_tipo_normativa_edit = $('select[id="ind_tipo_normativa2"]').val();
var normativa_edicion = $('select[id="ind_tipo_normativa2"]').val();

 if(normativa_edicion == 'R' || normativa_edicion == 'M')
 {
     normativa_edicion = 'RM';
 }

 switch (normativa_edicion)
 {

 case "L" :

 //-----------------------------------------------------------------------
 $('input[id="num_gaceta_ley_edt"]').val('{$datosNormativas.ind_gaceta_circular}');
 $('input[id="fec_publicacion_gaceta_ley_edt"]').val('{$datosNormativas.fec_gaceta_circular}');
 $('input[id="ind_descripcion_ley_edt"]').val('{$datosNormativas.ind_descripcion}');
 $('select[id="ind_tipo_ley_edt"]').val('{$datosNormativas.ind_tipo_ley_circular}');
 $('#cargarPdf_descripcion_pdf').html('{$datosNormativas.ind_ruta_archivo}'.replace(/_/g,' '));
 $('#ind_tipo_normativa_edit').val(ind_tipo_normativa_edit);

 $('input[id="num_gaceta_ley_edt"]').prop('disabled', false);
 $('input[id="fec_publicacion_gaceta_ley_edt"]').prop('disabled', false);
 $('input[id="ind_descripcion_ley_edt"]').prop('disabled', false);
 $('select[id="ind_tipo_ley_edt"]').prop('disabled', false);
//--------------------------------------------------------------------
 //Normativa
 $('input[id="num_gaceta_normativa_edt"]').val('');
 $('input[id="fec_publicacion_gaceta_normativa_edt"]').val('');
 $('input[id="num_resolucion_normativa_edt"]').val('');
 $('input[id="fec_publicacion_resolucion_normativa_edt"]').val('');
 $('input[id="ind_descripcion_normativa_edt"]').val('');

 $('input[id="num_gaceta_normativa_edt"]').prop('disabled', true);
 $('input[id="fec_publicacion_gaceta_normativa_edt"]').prop('disabled', true);
 $('input[id="num_resolucion_normativa_edt"]').prop('disabled', true);
 $('input[id="fec_publicacion_resolucion_normativa_edt"]').prop('disabled', true);
 $('input[id="ind_descripcion_normativa_edt"]').prop('disabled', true);

 //circulares
 $('input[id="num_circular_edt"]').val('');
 $('input[id="fec_publicacion_circular_edt"]').val('');
 $('input[id="ind_descripcion_circular_edt"]').val('');
 $('select[id="ind_dependencia_circular_edt"]').val('...');

 $('select[id="ind_tipo_circular_edt"]').prop('disabled', true);
 $('input[id="num_circular_edt"]').prop('disabled', true);
 $('input[id="fec_publicacion_circular_edt"]').prop('disabled', true);
 $('input[id="ind_descripcion_circular_edt"]').prop('disabled', true);
 $('select[id="ind_dependencia_circular_edt"]').prop('disabled', true);
 //resoluciones y normas
 $('input[id="num_resolucion_resolucion_manuales_edt"]').val('');
 $('input[id="fec_publicacion_resolucion_resolucion_manuales_edt"]').val('');
 $('input[id="num_gaceta_resolucion_manuales_edt"]').val('');
 $('input[id="fec_publicacion_gaceta_resolucion_manuales_edt"]').val('');
 $('input[id="ind_descripcion_resolucion_manuales_edt"]').val('');

 $('input[id="num_resolucion_resolucion_manuales_edt"]').prop('disabled', true);
 $('input[id="fec_publicacion_resolucion_resolucion_manuales_edt"]').prop('disabled', true);
 $('input[id="num_gaceta_resolucion_manuales_edt"]').prop('disabled', true);
 $('input[id="fec_publicacion_gaceta_resolucion_manuales_edt"]').prop('disabled', true);
 $('input[id="ind_descripcion_resolucion_manuales_edt"]').prop('disabled', true);
 //

 $('#fila_ley').attr('class', 'visible_norm_legal');
 $('#fila_normativa').attr('class', 'invisible_norm_legal');
 $('#fila_circulares').attr('class', 'invisible_norm_legal');
 $('#fila_resoluciones_manuales').attr('class', 'invisible_norm_legal');

 $('input[id="num_gaceta_ley_edt"]').focus();

 break;

 case "N" :
 //-----------------------------------------------------------------------
 $('input[id="num_gaceta_normativa_edt"]').val('{$datosNormativas.ind_gaceta_circular}');
 $('input[id="fec_publicacion_gaceta_normativa_edt"]').val('{$datosNormativas.fec_gaceta_circular}');
 $('input[id="num_resolucion_normativa_edt"]').val('{$datosNormativas.ind_resolucion}');
 $('input[id="fec_publicacion_resolucion_normativa_edt"]').val('{$datosNormativas.fec_resolucion}');
 $('input[id="ind_descripcion_normativa_edt"]').val('{$datosNormativas.ind_descripcion}');
 $('#cargarPdf_descripcion_pdf').html('{$datosNormativas.ind_ruta_archivo}'.replace(/_/g,' '));
 $('#ind_tipo_normativa_edit').val(ind_tipo_normativa_edit);

 $('input[id="num_gaceta_normativa_edt"]').prop('disabled', false);
 $('input[id="fec_publicacion_gaceta_normativa_edt"]').prop('disabled', false);
 $('input[id="num_resolucion_normativa_edt"]').prop('disabled', false);
 $('input[id="fec_publicacion_resolucion_normativa_edt"]').prop('disabled', false);
 $('input[id="ind_descripcion_normativa_edt"]').prop('disabled', false);
 //---------------------------------------------------------------------

 //leyes
 $('input[id="num_gaceta_ley_edt"]').val('');
 $('input[id="fec_publicacion_gaceta_ley_edt"]').val('');
 $('input[id="ind_descripcion_ley_edt"]').val('');
 $('select[id="ind_tipo_ley_edt"]').val('');

 $('input[id="num_gaceta_ley_edt"]').prop('disabled', true);
 $('input[id="fec_publicacion_gaceta_ley_edt"]').prop('disabled', true);
 $('input[id="ind_descripcion_ley_edt"]').prop('disabled', true);
 $('select[id="ind_tipo_ley_edt"]').prop('disabled', true);

 //circulares
 $('select[id="ind_tipo_circular_edt"]').val('...');
 $('input[id="num_circular_edt"]').val('');
 $('input[id="fec_publicacion_circular_edt"]').val('');
 $('input[id="ind_descripcion_circular_edt"]').val('');
 $('select[id="ind_dependencia_circular_edt"]').val('');

 $('select[id="ind_tipo_circular_edt"]').prop('disabled', true);
 $('input[id="num_circular_edt"]').prop('disabled', true);
 $('input[id="fec_publicacion_circular_edt"]').prop('disabled', true);
 $('input[id="ind_descripcion_circular_edt"]').prop('disabled', true);
 $('select[id="ind_dependencia_circular_edt"]').prop('disabled', true);

 //resoluciones y manuales

 $('input[id="num_resolucion_resolucion_manuales_edt"]').val('');
 $('input[id="fec_publicacion_resolucion_resolucion_manuales_edt"]').val('');
 $('input[id="num_gaceta_resolucion_manuales_edt"]').val('');
 $('input[id="fec_publicacion_gaceta_resolucion_manuales_edt"]').val('');
 $('input[id="ind_descripcion_resolucion_manuales_edt"]').val('');

 $('input[id="num_resolucion_resolucion_manuales_edt"]').prop('disabled', true);
 $('input[id="fec_publicacion_resolucion_resolucion_manuales_edt"]').prop('disabled', true);
 $('input[id="num_gaceta_resolucion_manuales_edt"]').prop('disabled', true);
 $('input[id="fec_publicacion_gaceta_resolucion_manuales_edt"]').prop('disabled', true);
 $('input[id="ind_descripcion_resolucion_manuales_edt"]').prop('disabled', true);

 //
 $('#fila_ley').attr('class', 'invisible_norm_legal');
 $('#fila_normativa').attr('class', 'visible_norm_legal');
 $('#fila_circulares').attr('class', 'invisible_norm_legal');
 $('#fila_resoluciones_manuales').attr('class', 'invisible_norm_legal')

 $('input[id="num_gaceta_normativa_edt"]').focus();



 break;

 case "C" :

  //------------------------------------------------------------------
  $('select[id="ind_tipo_circular_edt"]').val('{$datosNormativas.ind_tipo_ley_circular}');
  if( '{$datosNormativas.ind_tipo_ley_circular}' == 'CGR')
  {
      $('select[id="ind_dependencia_circular_edt"]').val('');
      $('select[id="ind_dependencia_circular_edt"]').prop('disabled', true);
  }
  else
  {
      $('select[id="ind_dependencia_circular_edt"]').val('{$datosNormativas.fk_a004_num_dependencia}');
      $('select[id="ind_dependencia_circular_edt"]').prop('disabled', false);
  }

  $('input[id="num_circular_edt"]').val('{$datosNormativas.ind_gaceta_circular}');
  $('input[id="fec_publicacion_circular_edt"]').val('{$datosNormativas.fec_gaceta_circular}');
  $('input[id="ind_descripcion_circular_edt"]').val('{$datosNormativas.ind_descripcion}');
  $('#cargarPdf_descripcion_pdf').html('{$datosNormativas.ind_ruta_archivo}'.replace(/_/g,' '));
  $('#ind_tipo_normativa_edit').val(ind_tipo_normativa_edit);

  $('select[id="ind_tipo_circular_edt"]').prop('disabled', false);
  $('input[id="num_circular_edt"]').prop('disabled', false);
  $('input[id="fec_publicacion_circular_edt"]').prop('disabled', false);
  $('input[id="ind_descripcion_circular_edt"]').prop('disabled', false);
 //------------------------------------------------

 //leyes
 $('input[id="num_gaceta_ley_edt"]').val('');
 $('input[id="fec_publicacion_gaceta_ley_edt"]').val('');
 $('input[id="ind_descripcion_ley_edt"]').val('');
 $('select[id="ind_tipo_ley_edt"]').val('');

 $('input[id="num_gaceta_ley_edt"]').prop('disabled', true);
 $('input[id="fec_publicacion_gaceta_ley_edt"]').prop('disabled', true);
 $('input[id="ind_descripcion_ley_edt"]').prop('disabled', true);
 $('select[id="ind_tipo_ley_edt"]').prop('disabled', true);

 //Normativa
 $('input[id="num_gaceta_normativa_edt"]').val('');
 $('input[id="fec_publicacion_gaceta_normativa_edt"]').val('');
 $('input[id="num_resolucion_normativa_edt"]').val('');
 $('input[id="fec_publicacion_resolucion_normativa_edt"]').val('');
 $('input[id="ind_descripcion_normativa_edt"]').val('');

 $('input[id="num_gaceta_normativa_edt"]').prop('disabled', true);
 $('input[id="fec_publicacion_gaceta_normativa_edt"]').prop('disabled', true);
 $('input[id="num_resolucion_normativa_edt"]').prop('disabled', true);
 $('input[id="fec_publicacion_resolucion_normativa_edt"]').prop('disabled', true);
 $('input[id="ind_descripcion_normativa_edt"]').prop('disabled', true);

 //resoluciones y manuales

 $('input[id="num_resolucion_resolucion_manuales_edt"]').val('');
 $('input[id="fec_publicacion_resolucion_resolucion_manuales_edt"]').val('');
 $('input[id="num_gaceta_resolucion_manuales_edt"]').val('');
 $('input[id="fec_publicacion_gaceta_resolucion_manuales_edt"]').val('');
 $('input[id="ind_descripcion_resolucion_manuales_edt"]').val('');

 $('input[id="num_resolucion_resolucion_manuales_edt"]').prop('disabled', true);
 $('input[id="fec_publicacion_resolucion_resolucion_manuales_edt"]').prop('disabled', true);
 $('input[id="num_gaceta_resolucion_manuales_edt"]').prop('disabled', true);
 $('input[id="fec_publicacion_gaceta_resolucion_manuales_edt"]').prop('disabled', true);
 $('input[id="ind_descripcion_resolucion_manuales_edt"]').prop('disabled', true);

 //
 $('#fila_ley').attr('class', 'invisible_norm_legal');
 $('#fila_normativa').attr('class', 'invisible_norm_legal');
 $('#fila_circulares').attr('class', 'visible_norm_legal');
 $('#fila_resoluciones_manuales').attr('class', 'invisible_norm_legal');

 $('select[id="ind_tipo_circular_edt"]').focus();

 break;

 case "RM" :
//------------------------------------------------------------------------------
 $('input[id="num_resolucion_resolucion_manuales_edt"]').val('{$datosNormativas.ind_resolucion}');
 $('input[id="fec_publicacion_resolucion_resolucion_manuales_edt"]').val('{$datosNormativas.fec_resolucion}');
 $('input[id="num_gaceta_resolucion_manuales_edt"]').val('{$datosNormativas.ind_gaceta_circular}');
 $('input[id="fec_publicacion_gaceta_resolucion_manuales_edt"]').val('{$datosNormativas.fec_gaceta_circular}');
 $('input[id="ind_descripcion_resolucion_manuales_edt"]').val('{$datosNormativas.ind_descripcion}');
 $('#cargarPdf_descripcion_pdf').html('{$datosNormativas.ind_ruta_archivo}'.replace(/_/g,' '));
 $('#ind_tipo_normativa_edit').val(ind_tipo_normativa_edit);

 $('input[id="num_resolucion_resolucion_manuales_edt"]').prop('disabled', false);
 $('input[id="fec_publicacion_resolucion_resolucion_manuales_edt"]').prop('disabled', false);
 $('input[id="num_gaceta_resolucion_manuales_edt"]').prop('disabled', false);
 $('input[id="fec_publicacion_gaceta_resolucion_manuales_edt"]').prop('disabled', false);
 $('input[id="ind_descripcion_resolucion_manuales_edt"]').prop('disabled', false);
 //-----------------------------------------------------------------------------------------
 //leyes
 $('input[id="num_gaceta_ley_edt"]').val('');
 $('input[id="fec_publicacion_gaceta_ley_edt"]').val('');
 $('input[id="ind_descripcion_ley_edt"]').val('');
 $('select[id="ind_tipo_ley_edt"]').val('');

 $('input[id="num_gaceta_ley_edt"]').prop('disabled', true);
 $('input[id="fec_publicacion_gaceta_ley_edt"]').prop('disabled', true);
 $('input[id="ind_descripcion_ley_edt"]').prop('disabled', true);
 $('select[id="ind_tipo_ley_edt"]').prop('disabled', true);

 //Normativa
 $('input[id="num_gaceta_normativa_edt"]').val('');
 $('input[id="fec_publicacion_gaceta_normativa_edt"]').val('');
 $('input[id="num_resolucion_normativa_edt"]').val('');
 $('input[id="fec_publicacion_resolucion_normativa_edt"]').val('');
 $('input[id="ind_descripcion_normativa_edt"]').val('');

 $('input[id="num_gaceta_normativa_edt"]').prop('disabled', true);
 $('input[id="fec_publicacion_gaceta_normativa_edt"]').prop('disabled', true);
 $('input[id="num_resolucion_normativa_edt"]').prop('disabled', true);
 $('input[id="fec_publicacion_resolucion_normativa_edt"]').prop('disabled', true);
 $('input[id="ind_descripcion_normativa_edt"]').prop('disabled', true);

 //circulares
 $('select[id="ind_tipo_circular_edt"]').val('');
 $('input[id="num_circular_edt"]').val('');
 $('input[id="fec_publicacion_circular_edt"]').val('');
 $('input[id="ind_descripcion_circular_edt"]').val('');
 $('select[id="ind_dependencia_circular_edt"]').val('');

 $('select[id="ind_tipo_circular_edt"]').prop('disabled', true);
 $('input[id="num_circular_edt"]').prop('disabled', true);
 $('input[id="fec_publicacion_circular_edt"]').prop('disabled', true);
 $('input[id="ind_descripcion_circular_edt"]').prop('disabled', true);
 $('select[id="ind_dependencia_circular_edt"]').prop('disabled', true);

 //
 $('#fila_ley').attr('class', 'invisible_norm_legal');
 $('#fila_normativa').attr('class', 'invisible_norm_legal');
 $('#fila_circulares').attr('class', 'invisible_norm_legal');
 $('#fila_resoluciones_manuales').attr('class', 'visible_norm_legal');

 $('input[id="num_resolucion_resolucion_manuales_edt"]').focus();


 break;

 default:
 alert('Error en el switch');
 }



//*******************************************
//*  VALIDACIONES CAMPOS
//********************************************
function validarNumeroGuion(evt){

    var charCode = evt.which || evt.keyCode;
    if (charCode==8||charCode==13||charCode==0)
        return true;
    var str = String.fromCharCode(charCode);
    if (!/[0-9]|-/.test(str)) {
        return false;//evt.preventDefault();
    };
}

function validarDescripcion(evt){

    var charCode = evt.which || evt.keyCode;
    if (charCode==8||charCode==13||charCode==0 || charCode==37 || charCode==39)//37 izq 39 der
        return true;
    var str = String.fromCharCode(charCode);
    if (!/[0-9\.\A-Z\a-z\á\é\í\ó\ú\ñ\Ñ\š\ü\,\;\(\)\-\ ]/.test(str)) {
        return false;//evt.preventDefault();
    };
}

function validarNumeroResolucion(evt){ //el numero de resolucion puede tener solo numeros/letras y numeros/guiones y numeros

    var charCode = evt.which || evt.keyCode;
    if (charCode==8||charCode==13||charCode==0)
        return true;
    var str = String.fromCharCode(charCode);
    if (!/[0-9\-\A-Z\a-z]/.test(str)) {
        return false;//evt.preventDefault();
    };
}

//*************************************
//*
//************************************
$(document).ready(function() {

    $('#fec_publicacion_gaceta_ley_edt').datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });
    $('#fec_publicacion_gaceta_normativa_edt').datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });
    $('#fec_publicacion_resolucion_normativa_edt').datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });
    $('#fec_publicacion_circular_edt').datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });
    $('#fec_publicacion_resolucion_resolucion_manuales_edt').datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });
    $('#fec_publicacion_gaceta_resolucion_manuales_edt').datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });

    //**********************************************
    //*  ACTIVAR LA DEPENCIA DEL CIRCULAR
    //***********************************************
    $('select[id="ind_tipo_circular_edt"]').change(function() {


        var valor = $('select[id="ind_tipo_circular_edt"]').val();

        $('select[id="ind_dependencia_circular_edt"]').val('');

        if(valor != 'CGR')
        {
            $('select[id="ind_dependencia_circular_edt"]').prop('disabled', false);
        }
        else
        {
            $('select[id="ind_dependencia_circular_edt"]').prop('disabled', true);
        }

    });

	//*********************************************
	//*			CARGAR PDF
	//*********************************************
	$("#cargarPdf").click(function() { //la imagen de carga de pdf

        $("#ind_ruta_pdf").click();//ejecuto un clic al input file oculto
    });
	
	$("#ind_ruta_pdf").change(function(e) {
				
		var files = e.target.files;
        var file = $("#ind_ruta_pdf")[0].files[0]; //Obtenemos el primer imputfile (unico que hay en este caso)
        var fileName = file.name;

        var aux_e = fileName.split('.');
        var aux_e2 = 0;
        if ( aux_e.length > 2 || fileName.indexOf(',') > -1 || fileName.indexOf('"') > -1 || fileName.indexOf("'") > -1 || fileName.indexOf('(') > -1 || fileName.indexOf(')') > -1 || fileName.indexOf('}') > -1 )
        {
            aux_e2 = 1;
        }

        if(aux_e2 == 0)
        {
            fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
            if (fileExtension == 'pdf' || fileExtension == 'PDF') {
                $("#cargarPdf").attr("src", "{$_Parametros.ruta_Img}modIN/ico-pdf-ok.jpg");
                $("#cargarPdf_descripcion_pdf").html(fileName.replace(/_/g,' '));
                $("#ind_ruta_pdf_f").val(fileName);

            } else {

                swal("¡Extensión de archivo no válido!", "Asegurese de subir un archivo de tipo: pdf/PDF", "warning");
                $("#cargarPdf").attr("src", "{$_Parametros.ruta_Img}modIN/pdf-icon.jpg");
                $("#cargarPdf_descripcion_pdf").html('');
                $("#ind_ruta_pdf_f").val('');//es el que se va a validar
            }
        }
        else
        {
            swal("¡Nombre de archivo no válido!", "Renombre el archivo, sin caracteres especiales", "warning");
            $("#cargarPdf").attr("src", "{$_Parametros.ruta_Img}modIN/pdf-icon.jpg");
            $("#cargarPdf_descripcion_pdf").html('');
            $("#ind_ruta_pdf_f").val('');//es el que se va a validar
        }


    });
    //**********************************************
    //* ENVIAR FORMULARIO
    //**********************************************

    $("#formAjax2").validate({

        submitHandler: function(form) {


            if($("#ind_ruta_pdf_f").val()!='')
            {
                var ind_tipo_normativa = $('select[id="ind_tipo_normativa2"]').val();
                var url_normativa = '{$_Parametros.url}modIN/normativaCONTROL/ModificarNormativaMET';
                var data = new FormData($("#formAjax2")[0]);

                $.ajax({

                    url: url_normativa,
                    data: data,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function (respuesta_post) {

                        if (respuesta_post == 1) {

                            swal({
                                title: 'Cambios guardados',
                                text: 'la normativa fue modificada',
                                type: "success"
                            }, function () {

                                $("#formAjax").submit();
                                $(document.getElementById('cerrarModal2')).click();
                                {*se esta cerrando la modal 1*}
                            });

                        }
                        else {
                            if (respuesta_post == -4) {
                                swal("¡Ya Existe un archivo con el mismo nombre!", "Renombre el archivo e intente de nuevo", "warning");
                            }
                            else {
                                swal("Error registrar", respuesta_post, "error");
                            }
                        }

                    }
                });

            }
            else
            {
                swal("¡El archivo es requerido!", "Solo archivo con extensión pdf/PDF", "warning");
            }


          
        }//FIN DEL submitHandler

    });//FIN DEL validate




});//FIN DEL ready

</script>


