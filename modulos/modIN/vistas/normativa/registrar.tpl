<section class="style-default-light">&nbsp;

<!--TITULO DEL FORMULARIO-->
<div class="row">
	<div class="col-lg-12">
		<div class="card-head"><h2 class="text-primary">Registrar Normativa Legal</h2></div>
	</div>&nbsp;
</div>

<!--CUERPO DEL FORMULARIO-->
    <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >
<div class="row">
        <div class="col-lg-12">
			<div class="card">
				<div class="card-body">


                       <div class="row" >
                           <div class="col-sm-6">
                               <div class="form-group" >
                                   <select id="ind_tipo_normativa" required="" class="form-control" name="ind_tipo_normativa" >
                                       <option value="L" selected="selected" >Leyes</option>
                                       <option value="N" >Normativas</option>
                                       <option value="C" >Circulares</option>
                                       <option value="R" >Resoluciones</option>
                                       <option value="M" >Manuales de Normas y Procedimientos</option>
                                   </select>
                                   <label for="ind_tipo_normativa">Tipo de Normativa</label>
                               </div>
                           </div>
                           <div>
                               <div class="col-sm-6">
                                   &nbsp;
                               </div>
                           </div>
                       </div>

                       <div class="row" id="fila_ley">
                                   <div class="col-sm-6">
                                       <div class="form-group"  >
                                           <input type="text" name="num_gaceta_ley" id="num_gaceta_ley" class="form-control" data-rule-number="true"  required aria-required="true" aria-invalid="true" >
                                           <label for="num_gaceta_ley">Número de gaceta</label>
                                           <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                       </div>
                                   </div>
                                   <div class="col-sm-6">
                                       <div class="form-group">
                                           <input type="text" name="fec_publicacion_gaceta_ley" id="fec_publicacion_gaceta_ley" class="form-control" required aria-required="true" aria-invalid="true" >
                                           <label for="fec_publicacion_gaceta_ley" class="control-label">Fecha de publicación de la gaceta</label>
                                           <p class="help-block">DD-MM-YYYY <span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                       </div>
                                   </div>


                                   <div class="col-sm-8">
                                       <div class="form-group" >
                                           <input style="text-transform: uppercase"  type="text" name="ind_descripcion_ley" id="ind_descripcion_ley" class="form-control" required aria-required="true" aria-invalid="true" onkeypress="return validarDescripcion(event)" >
                                           <label for="ind_descripcion_ley">Descripción/Titulo/Resumen/Vista prevía parcial</label>
                                           <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                       </div>
                                   </div>
                                   <div class="col-sm-4">
                                       <div class="form-group" >
                                           <select id="ind_tipo_ley" class="form-control " name="ind_tipo_ley" required aria-required="true" aria-invalid="true" >
                                               <option value="" selected="selected">Selccione</option>
                                               <option value="NAC" >Nacionales</option>
                                               <option value="EST" >Estadales</option>
                                           </select>
                                           <label for="ind_tipo_ley">Ámbito</label>
                                           <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                       </div>
                                   </div>
                       </div>

                       <div class="row invisible_norm_legal" id="fila_normativa"  >

                           <div class="col-sm-4">
                               <div class="form-group" >
                                   <input disabled="disabled" type="text" name="num_gaceta_normativa" id="num_gaceta_normativa" class="form-control" data-rule-number="true" required aria-required="true" aria-invalid="true" >
                                   <label for="num_gaceta_normativa">Número de gaceta</label>
                                   <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                               </div>
                           </div>
                           <div class="col-sm-4">
                               <div class="form-group">
                                   <input disabled="disabled" type="text" name="fec_publicacion_gaceta_normativa" id="fec_publicacion_gaceta_normativa" class="form-control" required aria-required="true" aria-invalid="true"  >
                                   <label for="fec_publicacion_gaceta_normativa" class="control-label">Fecha de publicación de la gaceta</label>
                                   <p class="help-block">DD-MM-YYYY <span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                               </div>
                           </div>
                           <div class="col-sm-4">
                               <div class="form-group" >
                                   <input style="text-transform: uppercase" disabled="disabled" type="text" name="num_resolucion_normativa" id="num_resolucion_normativa" class="form-control" required aria-required="true" aria-invalid="true" onkeypress="return validarNumeroResolucion(event)" >
                                   <label for="num_resolucion_normativa">Numero de resoluciòn</label>
                                   <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                               </div>
                           </div>
                           <div class="col-sm-4">
                               <div class="form-group">
                                   <input disabled="disabled" type="text" name="fec_publicacion_resolucion_normativa" id="fec_publicacion_resolucion_normativa" class="form-control" required aria-required="true" aria-invalid="true" >
                                   <label for="fec_publicacion_resolucion_normativa" class="control-label">Fecha de publicación de la resolución</label>
                                   <p class="help-block">DD-MM-YYYY <span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                               </div>
                           </div>
                           <div class="col-sm-8">
                               <div class="form-group" >
                                   <input disabled="disabled" type="text" name="ind_descripcion_normativa" id="ind_descripcion_normativa" class="form-control" required aria-required="true" aria-invalid="true" onkeypress="return validarDescripcion(event)" style="text-transform: uppercase"  >
                                   <label for="ind_descripcion_normativa">Descripción/Titulo/Resumen/Vista prevía parcial</label>
                                   <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                               </div>
                           </div>

                       </div>

                       <div class="row invisible_norm_legal " id="fila_circulares"   >

                           <div class="col-sm-4">
                              <div class="form-group" >
                                   <select disabled="disabled" id="ind_tipo_circular" required aria-required="true" aria-invalid="true" data-msg-required="Seleccione el Organismo que emite el circular" class="form-control " name="ind_tipo_circular" >
                                       <option value="" selected="selected">Seleccione...</option>
                                       <option value="INT" >{$datosOrganismo[0]['ind_descripcion_empresa'] }</option>
                                       <option value="CGR" >CONTRALORÍA GENERAL DE LA REPUBLICA</option>
                                   </select>
                                   <label for="ind_tipo_circular">Fuente</label>
                                  <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                               </div>
                           </div>

                           <div class="col-sm-4">
                               <div class="form-group" >
                                   <input disabled="disabled" type="text" name="num_circular" id="num_circular" class="form-control" required aria-required="true" aria-invalid="true" onkeypress="return validarNumeroGuion(event)" >
                                   <label for="num_circular">Numero de circular/oficio</label>
                                   <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                               </div>
                           </div>

                           <div class="col-sm-4">
                               <div class="form-group">
                                   <input disabled="disabled" type="text" name="fec_publicacion_circular" id="fec_publicacion_circular" class="form-control" required aria-required="true" aria-invalid="true" >
                                   <label for="fec_publicacion_circular" class="control-label">Fecha de publicación</label>
                                   <p class="help-block">DD-MM-YYYY <span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                               </div>
                           </div>

                           <div class="col-sm-8">
                               <div class="form-group" >
                                   <input style="text-transform: uppercase"  disabled="disabled" type="text" name="ind_descripcion_circular" id="ind_descripcion_circular" class="form-control" required aria-required="true" aria-invalid="true" onkeypress="return validarDescripcion(event)" >
                                   <label  for="ind_descripcion_circular">Descripción/Titulo/Resumen/Vista prevía parcial</label>
                                   <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                               </div>
                           </div>

                           <div class="col-sm-4" >
                               <div class="form-group" >
                                   <select disabled="disabled" id="ind_dependencia_circular" class="form-control " name="ind_dependencia_circular" required aria-required="true" aria-invalid="true" data-msg-required="Seleccione la dependencia" >
                                       <option value="" selected="selected"></option>
                                       {foreach item=indice from=$datosDependencias}
                                           <option value="{$indice.pk_num_dependencia }" >{$indice.ind_dependencia}</option>
                                       {/foreach}
                                   </select>
                                   <label for="ind_dependencia_circular">Dependencia</label>
                               </div>
                           </div>


                       </div>



                        <div class="row invisible_norm_legal" id="fila_resoluciones_manuales"  >

                            <div class="col-sm-4">
                                <div class="form-group" >
                                    <input style="text-transform: uppercase" disabled="disabled" type="text" name="num_resolucion_resolucion_manuales" id="num_resolucion_resolucion_manuales" class="form-control" required aria-required="true" aria-invalid="true" onkeypress="return validarNumeroResolucion(event)" >
                                    <label for="num_resolucion_resolucion_manuales">Numero de resoluciòn</label>
                                    <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input disabled="disabled" type="text" name="fec_publicacion_resolucion_resolucion_manuales" id="fec_publicacion_resolucion_resolucion_manuales" class="form-control" required aria-required="true" aria-invalid="true" >
                                    <label for="fec_publicacion_resolucion_resolucion_manuales" class="control-label">Fecha de resolución</label>
                                    <p class="help-block">DD-MM-YYYY <span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group" >
                                    <input disabled="disabled" type="text" name="num_gaceta_resolucion_manuales" id="num_gaceta_resolucion_manuales" class="form-control" data-rule-number="true" required aria-required="true" aria-invalid="true" >
                                    <label for="num_gaceta_resolucion_manuales">Número de gaceta de la resolución</label>
                                    <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input disabled="disabled" type="text" name="fec_publicacion_gaceta_resolucion_manuales" id="fec_publicacion_gaceta_resolucion_manuales" class="form-control" required aria-required="true" aria-invalid="true" >
                                    <label for="fec_publicacion_gaceta_resolucion_manuales" class="control-label">Fecha de gaceta</label>
                                    <p class="help-block">DD-MM-YYYY <span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                </div>
                            </div>

                            <div class="col-sm-8">
                                <div class="form-group" >
                                    <input style="text-transform: uppercase"  disabled="disabled" type="text" name="ind_descripcion_resolucion_manuales" id="ind_descripcion_resolucion_manuales" class="form-control" required aria-required="true" aria-invalid="true" onkeypress="return validarDescripcion(event)" >
                                    <label for="ind_descripcion_resolucion_manuales">Descripción/Titulo/Resumen/Vista prevía parcial</label>
                                    <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                </div>
                            </div>

                        </div>

                       <div class="row">

                           <div class="col-sm-8" align="center">

                           </div>

                           <div class="col-sm-4" align="center">
                                <div class="form-group"  >
                                    <div id="pdfCargado">
                                        <div class="row">
                                            <img src="{$_Parametros.ruta_Img}modIN/pdf-icon.jpg" alt="Cargar PDF" title="Cargar PDF" id="cargarPdf" style="cursor: pointer;" width="90"/>
                                        </div>
                                        <div class="row" align="center">
                                            <div class="col-sm-12" align="center">
                                            <span id="cargarPdf_descripcion_pdf"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="file" name="ind_ruta_pdf" id="ind_ruta_pdf" style="display: none" />
                                    <input type="hidden" name="ind_ruta_pdf_f" id="ind_ruta_pdf_f" >
                                </div>

                            </div>

                       </div>
                       <div class="row">
                           &nbsp;
                       </div>
                       <div class="row">
                           <div class="col-sm-12" align="center">
                             <button id="botonGuardar" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" type="submit"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                           </div>
                       </div>
                    <div class="row">
                        <div class="col-sm-12"><p class="text-xs text-bold text-right">EL PDF Y LOS CAMPOS MARCADOS CON ASTERISCOS <span class="text-lg text-bold" style="color: red"> ( * ) </span>SON OBLIGATORIOS</p></div>
                    </div>

			    </div>
		    </div>
        </div>
</div>
    </form>

<!--FIN DEL TITULO DEL FORMULARIO-->
</section>

<style>

    .invisible_norm_legal{
        display: none;
    }

    .visible_norm_legal{
        display: block;
    }

</style>

<script>
//*******************************************
//*  VALIDACIONES CAMPOS
//********************************************
function validarNumeroGuion(evt){

    var charCode = evt.which || evt.keyCode;
    if (charCode==8||charCode==13||charCode==0)
        return true;
    var str = String.fromCharCode(charCode);
    if (!/[0-9]|-/.test(str)) {
        return false;//evt.preventDefault();
    };
}

function validarDescripcion(evt){

    var charCode = evt.which || evt.keyCode;
    if (charCode==8||charCode==13||charCode==0 || charCode==37 || charCode==39)//37 izq 39 der
        return true;
    var str = String.fromCharCode(charCode);
    if (!/[0-9\.\A-Z\a-z\á\é\í\ó\ú\ñ\Ñ\š\ü\,\;\(\)\-\ ]/.test(str)) {
        return false;//evt.preventDefault();
    };
}

function validarNumeroResolucion(evt){ //el numero de resolucion puede tener solo numeros/letras y numeros/guiones y numeros

    var charCode = evt.which || evt.keyCode;
    if (charCode==8||charCode==13||charCode==0)
        return true;
    var str = String.fromCharCode(charCode);
    if (!/[0-9\-\A-Z\a-z]/.test(str)) {
        return false;//evt.preventDefault();
    };
}

//*************************************
//*
//************************************
$(document).ready(function() {

    $('#fec_publicacion_gaceta_ley').datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });
    $('#fec_publicacion_gaceta_normativa').datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });
    $('#fec_publicacion_resolucion_normativa').datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });
    $('#fec_publicacion_circular').datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });
    $('#fec_publicacion_resolucion_resolucion_manuales').datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });
    $('#fec_publicacion_gaceta_resolucion_manuales').datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });



	//*********************************************
    //*   ACTUVAR CAMPOS POR TYPO DE NORMATIVA
    //*********************************************
    $('#ind_tipo_normativa').change(function() {

        var ind_tipo_normativa2 = $('select[id="ind_tipo_normativa"]').val();

        if(ind_tipo_normativa2 == 'R' || ind_tipo_normativa2 == 'M')
        {
            ind_tipo_normativa2 = 'RM';
        }

        switch (ind_tipo_normativa2)
        {

            case "L" :

                $('input[id="num_gaceta_ley"]').val('');
                $('input[id="fec_publicacion_gaceta_ley"]').val('');
                $('input[id="ind_descripcion_ley"]').val('');
                $('select[id="ind_tipo_ley"]').val('');

                $('input[id="num_gaceta_ley"]').prop('disabled', false);
                $('input[id="fec_publicacion_gaceta_ley"]').prop('disabled', false);
                $('input[id="ind_descripcion_ley"]').prop('disabled', false);
                $('select[id="ind_tipo_ley"]').prop('disabled', false);

                //Normativa
                $('input[id="num_gaceta_normativa"]').val('');
                $('input[id="fec_publicacion_gaceta_normativa"]').val('');
                $('input[id="num_resolucion_normativa"]').val('');
                $('input[id="fec_publicacion_resolucion_normativa"]').val('');
                $('input[id="ind_descripcion_normativa"]').val('');

                $('input[id="num_gaceta_normativa"]').prop('disabled', true);
                $('input[id="fec_publicacion_gaceta_normativa"]').prop('disabled', true);
                $('input[id="num_resolucion_normativa"]').prop('disabled', true);
                $('input[id="fec_publicacion_resolucion_normativa"]').prop('disabled', true);
                $('input[id="ind_descripcion_normativa"]').prop('disabled', true);

                //circulares
                $('select[id="ind_tipo_circular"]').val('');
                $('input[id="num_circular"]').val('');
                $('input[id="fec_publicacion_circular"]').val('');
                $('input[id="ind_descripcion_circular"]').val('');
                $('select[id="ind_dependencia_circular"]').val('...');

                $('select[id="ind_tipo_circular"]').prop('disabled', true);
                $('input[id="num_circular"]').prop('disabled', true);
                $('input[id="fec_publicacion_circular"]').prop('disabled', true);
                $('input[id="ind_descripcion_circular"]').prop('disabled', true);
                $('select[id="ind_dependencia_circular"]').prop('disabled', true);
               //resoluciones y normas
                $('input[id="num_resolucion_resolucion_manuales"]').val('');
                $('input[id="fec_publicacion_resolucion_resolucion_manuales"]').val('');
                $('input[id="num_gaceta_resolucion_manuales"]').val('');
                $('input[id="fec_publicacion_gaceta_resolucion_manuales"]').val('');
                $('input[id="ind_descripcion_resolucion_manuales"]').val('');

                $('input[id="num_resolucion_resolucion_manuales"]').prop('disabled', true);
                $('input[id="fec_publicacion_resolucion_resolucion_manuales"]').prop('disabled', true);
                $('input[id="num_gaceta_resolucion_manuales"]').prop('disabled', true);
                $('input[id="fec_publicacion_gaceta_resolucion_manuales"]').prop('disabled', true);
                $('input[id="ind_descripcion_resolucion_manuales"]').prop('disabled', true);
               //

                $('#fila_ley').attr('class', 'visible_norm_legal');
                $('#fila_normativa').attr('class', 'invisible_norm_legal');
                $('#fila_circulares').attr('class', 'invisible_norm_legal');
                $('#fila_resoluciones_manuales').attr('class', 'invisible_norm_legal');

                $('input[id="num_gaceta_ley"]').focus();

            break;

            case "N" :

                $('input[id="num_gaceta_normativa"]').val('');
                $('input[id="fec_publicacion_gaceta_normativa"]').val('');
                $('input[id="num_resolucion_normativa"]').val('');
                $('input[id="fec_publicacion_resolucion_normativa"]').val('');
                $('input[id="ind_descripcion_normativa"]').val('');

                $('input[id="num_gaceta_normativa"]').prop('disabled', false);
                $('input[id="fec_publicacion_gaceta_normativa"]').prop('disabled', false);
                $('input[id="num_resolucion_normativa"]').prop('disabled', false);
                $('input[id="fec_publicacion_resolucion_normativa"]').prop('disabled', false);
                $('input[id="ind_descripcion_normativa"]').prop('disabled', false);

                //leyes
                $('input[id="num_gaceta_ley"]').val('');
                $('input[id="fec_publicacion_gaceta_ley"]').val('');
                $('input[id="ind_descripcion_ley"]').val('');
                $('select[id="ind_tipo_ley"]').val('');

                $('input[id="num_gaceta_ley"]').prop('disabled', true);
                $('input[id="fec_publicacion_gaceta_ley"]').prop('disabled', true);
                $('input[id="ind_descripcion_ley"]').prop('disabled', true);
                $('select[id="ind_tipo_ley"]').prop('disabled', true);

                //circulares
                $('select[id="ind_tipo_circular"]').val('...');
                $('input[id="num_circular"]').val('');
                $('input[id="fec_publicacion_circular"]').val('');
                $('input[id="ind_descripcion_circular"]').val('');
                $('select[id="ind_dependencia_circular"]').val('');

                $('select[id="ind_tipo_circular"]').prop('disabled', true);
                $('input[id="num_circular"]').prop('disabled', true);
                $('input[id="fec_publicacion_circular"]').prop('disabled', true);
                $('input[id="ind_descripcion_circular"]').prop('disabled', true);
                $('select[id="ind_dependencia_circular"]').prop('disabled', true);

                //resoluciones y manuales

                $('input[id="num_resolucion_resolucion_manuales"]').val('');
                $('input[id="fec_publicacion_resolucion_resolucion_manuales"]').val('');
                $('input[id="num_gaceta_resolucion_manuales"]').val('');
                $('input[id="fec_publicacion_gaceta_resolucion_manuales"]').val('');
                $('input[id="ind_descripcion_resolucion_manuales"]').val('');

                $('input[id="num_resolucion_resolucion_manuales"]').prop('disabled', true);
                $('input[id="fec_publicacion_resolucion_resolucion_manuales"]').prop('disabled', true);
                $('input[id="num_gaceta_resolucion_manuales"]').prop('disabled', true);
                $('input[id="fec_publicacion_gaceta_resolucion_manuales"]').prop('disabled', true);
                $('input[id="ind_descripcion_resolucion_manuales"]').prop('disabled', true);

                //
                $('#fila_ley').attr('class', 'invisible_norm_legal');
                $('#fila_normativa').attr('class', 'visible_norm_legal');
                $('#fila_circulares').attr('class', 'invisible_norm_legal');
                $('#fila_resoluciones_manuales').attr('class', 'invisible_norm_legal')

                $('input[id="num_gaceta_normativa"]').focus();



            break;

            case "C" :

                $('select[id="ind_tipo_circular"]').val('');
                $('input[id="num_circular"]').val('');
                $('input[id="fec_publicacion_circular"]').val('');
                $('input[id="ind_descripcion_circular"]').val('');
                $('select[id="ind_dependencia_circular"]').val('');

                $('select[id="ind_tipo_circular"]').prop('disabled', false);
                $('input[id="num_circular"]').prop('disabled', false);
                $('input[id="fec_publicacion_circular"]').prop('disabled', false);
                $('input[id="ind_descripcion_circular"]').prop('disabled', false);
                $('select[id="ind_dependencia_circular"]').prop('disabled', true);

                //leyes
                $('input[id="num_gaceta_ley"]').val('');
                $('input[id="fec_publicacion_gaceta_ley"]').val('');
                $('input[id="ind_descripcion_ley"]').val('');
                $('select[id="ind_tipo_ley"]').val('');

                $('input[id="num_gaceta_ley"]').prop('disabled', true);
                $('input[id="fec_publicacion_gaceta_ley"]').prop('disabled', true);
                $('input[id="ind_descripcion_ley"]').prop('disabled', true);
                $('select[id="ind_tipo_ley"]').prop('disabled', true);

                //Normativa
                $('input[id="num_gaceta_normativa"]').val('');
                $('input[id="fec_publicacion_gaceta_normativa"]').val('');
                $('input[id="num_resolucion_normativa"]').val('');
                $('input[id="fec_publicacion_resolucion_normativa"]').val('');
                $('input[id="ind_descripcion_normativa"]').val('');

                $('input[id="num_gaceta_normativa"]').prop('disabled', true);
                $('input[id="fec_publicacion_gaceta_normativa"]').prop('disabled', true);
                $('input[id="num_resolucion_normativa"]').prop('disabled', true);
                $('input[id="fec_publicacion_resolucion_normativa"]').prop('disabled', true);
                $('input[id="ind_descripcion_normativa"]').prop('disabled', true);

                //resoluciones y manuales

                $('input[id="num_resolucion_resolucion_manuales"]').val('');
                $('input[id="fec_publicacion_resolucion_resolucion_manuales"]').val('');
                $('input[id="num_gaceta_resolucion_manuales"]').val('');
                $('input[id="fec_publicacion_gaceta_resolucion_manuales"]').val('');
                $('input[id="ind_descripcion_resolucion_manuales"]').val('');

                $('input[id="num_resolucion_resolucion_manuales"]').prop('disabled', true);
                $('input[id="fec_publicacion_resolucion_resolucion_manuales"]').prop('disabled', true);
                $('input[id="num_gaceta_resolucion_manuales"]').prop('disabled', true);
                $('input[id="fec_publicacion_gaceta_resolucion_manuales"]').prop('disabled', true);
                $('input[id="ind_descripcion_resolucion_manuales"]').prop('disabled', true);

                //
                $('#fila_ley').attr('class', 'invisible_norm_legal');
                $('#fila_normativa').attr('class', 'invisible_norm_legal');
                $('#fila_circulares').attr('class', 'visible_norm_legal');
                $('#fila_resoluciones_manuales').attr('class', 'invisible_norm_legal');

                $('select[id="ind_tipo_circular"]').focus();

            break;

            case "RM" :

                $('input[id="num_resolucion_resolucion_manuales"]').val('');
                $('input[id="fec_publicacion_resolucion_resolucion_manuales"]').val('');
                $('input[id="num_gaceta_resolucion_manuales"]').val('');
                $('input[id="fec_publicacion_gaceta_resolucion_manuales"]').val('');
                $('input[id="ind_descripcion_resolucion_manuales"]').val('');

                $('input[id="num_resolucion_resolucion_manuales"]').prop('disabled', false);
                $('input[id="fec_publicacion_resolucion_resolucion_manuales"]').prop('disabled', false);
                $('input[id="num_gaceta_resolucion_manuales"]').prop('disabled', false);
                $('input[id="fec_publicacion_gaceta_resolucion_manuales"]').prop('disabled', false);
                $('input[id="ind_descripcion_resolucion_manuales"]').prop('disabled', false);

                //leyes
                $('input[id="num_gaceta_ley"]').val('');
                $('input[id="fec_publicacion_gaceta_ley"]').val('');
                $('input[id="ind_descripcion_ley"]').val('');
                $('select[id="ind_tipo_ley"]').val('');

                $('input[id="num_gaceta_ley"]').prop('disabled', true);
                $('input[id="fec_publicacion_gaceta_ley"]').prop('disabled', true);
                $('input[id="ind_descripcion_ley"]').prop('disabled', true);
                $('select[id="ind_tipo_ley"]').prop('disabled', true);

                //Normativa
                $('input[id="num_gaceta_normativa"]').val('');
                $('input[id="fec_publicacion_gaceta_normativa"]').val('');
                $('input[id="num_resolucion_normativa"]').val('');
                $('input[id="fec_publicacion_resolucion_normativa"]').val('');
                $('input[id="ind_descripcion_normativa"]').val('');

                $('input[id="num_gaceta_normativa"]').prop('disabled', true);
                $('input[id="fec_publicacion_gaceta_normativa"]').prop('disabled', true);
                $('input[id="num_resolucion_normativa"]').prop('disabled', true);
                $('input[id="fec_publicacion_resolucion_normativa"]').prop('disabled', true);
                $('input[id="ind_descripcion_normativa"]').prop('disabled', true);

                //circulares
                $('select[id="ind_tipo_circular"]').val('');
                $('input[id="num_circular"]').val('');
                $('input[id="fec_publicacion_circular"]').val('');
                $('input[id="ind_descripcion_circular"]').val('');
                $('select[id="ind_dependencia_circular"]').val('');

                $('select[id="ind_tipo_circular"]').prop('disabled', true);
                $('input[id="num_circular"]').prop('disabled', true);
                $('input[id="fec_publicacion_circular"]').prop('disabled', true);
                $('input[id="ind_descripcion_circular"]').prop('disabled', true);
                $('select[id="ind_dependencia_circular"]').prop('disabled', true);

                //
                $('#fila_ley').attr('class', 'invisible_norm_legal');
                $('#fila_normativa').attr('class', 'invisible_norm_legal');
                $('#fila_circulares').attr('class', 'invisible_norm_legal');
                $('#fila_resoluciones_manuales').attr('class', 'visible_norm_legal');

                $('input[id="num_resolucion_resolucion_manuales"]').focus();


            break;

            default:
               alert('Error en el switch');
        }



    });

    //**********************************************
    //*  ACTIVAR LA DEPENCIA DEL CIRCULAR
    //***********************************************
    $('select[id="ind_tipo_circular"]').change(function() {


        var valor = $('select[id="ind_tipo_circular"]').val();

        $('select[id="ind_dependencia_circular"]').val('');

        if(valor != 'CGR')
        {
            $('select[id="ind_dependencia_circular"]').prop('disabled', false);
        }
        else
        {
            $('select[id="ind_dependencia_circular"]').prop('disabled', true);
        }

    });

	//*********************************************
	//*			CARGAR PDF
	//*********************************************
	$("#cargarPdf").click(function() { //la imagen de carga de pdf

        $("#ind_ruta_pdf").click();//ejecuto un clic al input file oculto
    });
	
	$("#ind_ruta_pdf").change(function(e) {
				
		var files = e.target.files;
        var file = $("#ind_ruta_pdf")[0].files[0]; //Obtenemos el primer imputfile (unico que hay en este caso)
        var fileName = file.name;

        var aux_r = fileName.split('.');
        var aux_r2 = 0;
        if ( aux_r.length > 2 || fileName.indexOf(',') > -1 || fileName.indexOf('"') > -1 || fileName.indexOf("'") > -1 || fileName.indexOf('(') > -1 || fileName.indexOf(')') > -1 || fileName.indexOf('}') > -1 )
        {
            aux_r2 = 1;
        }

        if(aux_r2 == 0)
        {
            fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
            if (fileExtension == 'pdf' || fileExtension == 'PDF') {
                $("#cargarPdf").attr("src", "{$_Parametros.ruta_Img}modIN/ico-pdf-ok.jpg");
                $("#cargarPdf_descripcion_pdf").html(fileName.replace(/_/g,' '));
                $("#ind_ruta_pdf_f").val(fileName);

            } else {

                swal("¡Extensión de archivo no válido!", "Asegurese de subir un archivo de tipo: pdf/PDF", "warning");
                $("#cargarPdf").attr("src", "{$_Parametros.ruta_Img}modIN/pdf-icon.jpg");
                $("#cargarPdf_descripcion_pdf").html('');
                $("#ind_ruta_pdf_f").val('');//es el que se va a validar
            }
        }
        else
        {
            swal("¡Nombre de archivo no válido!", "Renombre el archivo, sin caracteres especiales", "warning");
            $("#cargarPdf").attr("src", "{$_Parametros.ruta_Img}modIN/pdf-icon.jpg");
            $("#cargarPdf_descripcion_pdf").html('');
            $("#ind_ruta_pdf_f").val('');//es el que se va a validar
        }
    });
    //**********************************************
    //* ENVIAR FORMULARIO
    //**********************************************

    $("#formAjax").validate({

        submitHandler: function(form) {

            var ind_tipo_normativa = $('select[id="ind_tipo_normativa"]').val();

            var ind_ruta_pdf_f = $("#ind_ruta_pdf_f").val();

                if (ind_ruta_pdf_f == "")
                {
                    swal("¡El archivo es requerido!", "Solo archivo con extensión pdf/PDF", "warning");
                }
                else
                {
                    var url_normativa = '{$_Parametros.url}modIN/normativaCONTROL/RegistrarMET';
                    var data = new FormData($("#formAjax")[0]);

                    $.ajax({

                        url: url_normativa,
                        data: data,
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        success: function (respuesta_post) {

                            if (respuesta_post == 1) {

                                swal("Normativa Registrada", "guardado satisfactoriamente", "success");

                                $('#formAjax').each(function () { //resetear los campos del formulario
                                    this.reset();
                                });

                                //****************************************

                                $('input[id="num_gaceta_ley"]').prop('disabled', false);
                                $('input[id="fec_publicacion_gaceta_ley"]').prop('disabled', false);
                                $('input[id="ind_descripcion_ley"]').prop('disabled', false);
                                $('select[id="ind_tipo_ley"]').prop('disabled', false);

                                $('#fila_ley').attr('class', 'visible_norm_legal');
                                $('#fila_normativa').attr('class', 'invisible_norm_legal');
                                $('#fila_circulares').attr('class', 'invisible_norm_legal');
                                $('#fila_resoluciones_manuales').attr('class', 'invisible_norm_legal');

                                $('input[id="num_gaceta_ley"]').focus();
                                //****************************************
                                $("#cargarPdf").attr("src", "{$_Parametros.ruta_Img}modIN/pdf-icon.jpg");
                                $("#cargarPdf_descripcion_pdf").html('');
                                $("#ind_ruta_pdf_f").val('');


                            }
                            else {
                                if (respuesta_post == -4) {
                                    swal("Ya Existe un archivo con el mismo nombre!", "Renombre el archivo e intente de nuevo", "warning");
                                }
                                else {
                                    swal("Error registrar", respuesta_post, "error");
                                }
                            }

                        }
                    });

                }


          
        }//FIN DEL submitHandler

    });//FIN DEL validate




});//FIN DEL ready
</script>


