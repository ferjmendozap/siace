<section class="style-default-light">&nbsp;
<!--CUERPO DEL DOCUMENTO -->

<div class="row">
    
    <div class="col-lg-12">
    	<div class="card">
			<div class="card-body ">
				<form id="formAjax" class="form  form-validation" role="form" >
					<div class="col-lg-6">
						<div class="row">
							<div class="form-group" >
								<input id="fec_registro" type="text" class="form-control input-sm" name="fec_registro"  required aria-required="true" aria-invalid="true" value="" >
								<label for="fec_registro">Fecha del Evento</label>
								<p class="help-block">DD-MM-YYYY <span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
							</div>
						</div >
						<div class="row" >
							<div class="col-lg-6">
								<div class="form-group" >
									<input type="text" class="form-control time-mask input-sm" id="hora_inicio" name="hora_inicio" required aria-required="true" aria-invalid="true" >
									<label for="hora_inicio">Hora de Inicio</label>
									<p class="help-block">00:00 <span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
									
								</div>
							</div>
							<div class="col-lg-6">	
								<div class="form-group" >				
									<select id="horario_inicio" class="form-control input-sm" name="horario_inicio" >
									 	<option value="am" selected="selected">am</option>
									 	<option value="pm" >pm</option>
									</select>
									<label for="horario_inicio">Horario <span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></label>
								</div>	
							</div>
						</div>
						<div class="row" >
							<div class="col-lg-6">
								<div class="form-group" >
									<input type="text" class="form-control time-mask input-sm" id="hora_final" name="hora_final" required aria-required="true" aria-invalid="true" >
									<label for="hora_final">Hora de culminación</label>
									<p class="help-block">00:00 <span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
									
								</div>
							</div>
							<div class="col-lg-6">	
								<div class="form-group" >				
									<select id="horario_final" class="form-control input-sm" name="horario_final" >
									 	<option value="am" selected="selected">am</option>
									 	<option value="pm" >pm</option>
									</select>
									<label for="horario_final">Horario</label>
								</div>	
							</div>
						</div>
					</div>
					<div class="col-lg-6" align="center">
						<div class="form-group"  >
							<div id="fotoCargada" >
								<img class="img-thumbnail border-gray border-lg img-responsive auto-width" src="{$_Parametros.ruta_Img}modIN/imagen_evento.png" alt="Cargar Imagen" title="Cargar Imagen" id="cargarImagen" style="cursor: pointer; "/>
							</div>
							<input type="file" name="ind_ruta_img" id="ind_ruta_img" style="display: none" />
							<input type="hidden" name="ind_ruta_img_f" id="ind_ruta_img_f" >
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12" >
							<div class="form-group"  >
								<textarea name="ind_descripcion" id="ind_descripcion" class="form-control input-sm" rows="2" placeholder="" required aria-required="true" aria-invalid="true"  style="text-transform: uppercase;"  onkeypress="return validarDescripcion(event)"   ></textarea>
								<label for="ind_descripcion">Descripción</label>
								<p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12" >
							<div class="form-group"  >
								<input type="text" name="ind_lugar" id="ind_lugar" class="form-control input-sm" style="text-transform: uppercase;" onkeypress="return validarDescripcion(event)" >
								<label for="ind_descripcion">Lugar</label>
								<p class="help-block">Opcional</p>									
							</div>
						</div>
					</div>
					<div class="row" >
						<div class="col-lg-4" >
							<div class="form-group" >
								<select id="ind_estado" required="" class="form-control input-sm" name="ind_estado"  >
									<option value=".." selected="selected" >No aplica...</option>
									{foreach item=indice from=$estadosEventosArray}
										<option value="{$indice.pk_num_estado }">{$indice.ind_estado}</option>
									{/foreach}
								</select>
								<label for="ind_estado">Estado</label>
							</div>
						</div>
						<div class="col-lg-4" >
							<div class="form-group" >
								<select id="ind_municipio" disabled="disabled" class="form-control input-sm" name="ind_municipio" >
									<option value=".." selected="selected">No aplica</option>
								</select>
								<label for="ind_municipio">Municipio</label>
							</div>
						</div>
						<div class="col-lg-4" >
							<div class="form-group" >
								<select id="ind_parroquia" disabled="disabled" class="form-control input-sm" name="ind_parroquia" >
									<option value=".." selected="selected">No aplica</option>
								</select>
								<label for="ind_parroquia">Parroquia</label>
							</div>
						</div>
					</div>
					<div class="row" >
						<div class="col-lg-12" >
							<div class="checkbox checkbox-styled ">
								<label>
									<input class="input-sm" type="checkbox" id="ind_anual" name="ind_anual" value="Y">{* si no esta check no se envia *}
									<span>Repetir Anualmente</span>
								</label>
							</div>
						</div>
					</div>
					<div class="row" >
						<div class="col-lg-12" >
							<div align="center">
								&nbsp;
								<button id="botonCerrar" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal" type="button"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
								<button type="submit" id="botonGuardar" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" type="button"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
							</div>
							<p class="text-xs text-bold text-right">LA IMAGEN Y LOS CAMPOS MARCADOS CON ASTERISCOS <span class="text-lg text-bold" style="color: red"> ( * ) </span>SON OBLIGATORIOS</p>
						</div>
					</div>

				</form>
			</div>
    	</div>
    </div>

</div>

</section>

<script>


	function validarDescripcion(evt){

		var charCode = evt.which || evt.keyCode;
		if (charCode==8||charCode==13||charCode==0 || charCode==37 || charCode==39)//37 izq 39 der
			return true;
		var str = String.fromCharCode(charCode);
		if (!/[0-9\.\A-Z\a-z\á\é\í\ó\ú\ñ\Ñ\š\ü\,\;\(\)\-\ ]/.test(str)) {
			return false;//evt.preventDefault();
		};
	}
	//*************************************************************
	//*  FORMATEAR LA FECHA
	//*************************************************************

	$('#fec_registro').datepicker({
		todayHighlight: true,
		format:'dd-mm-yyyy',
		autoclose: true,
		language:'es'
	});
	$("#ind_estado").select2();//Actibar la busqueda en los selects
	$("#ind_municipio").select2();
	$("#ind_parroquia").select2();

	$(document).ready(function() {

		//*********************************************
		//*			CARGAR IMAGEN
		//*********************************************
		$("#cargarImagen").click(function() {
			$("#ind_ruta_img").click();//input file
		});

		$("#ind_ruta_img").change(function(e) {

			var files = e.target.files;
			var file = $("#ind_ruta_img")[0].files[0];
			var fileName = file.name;
			$("#ind_ruta_img_f").val(fileName);

			var aux_v = fileName.split('.');
			var aux_v2 = 0;
			if ( aux_v.length > 2 || fileName.indexOf(',') > -1 || fileName.indexOf('"') > -1 || fileName.indexOf("'") > -1 || fileName.indexOf('(') > -1 || fileName.indexOf(')') > -1 || fileName.indexOf('}') > -1 )
			{
				aux_v2 = 1;
			}

			if(aux_v2 == 0)
			{
				fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
				if (fileExtension == 'png' || fileExtension == 'PNG' || fileExtension == 'jpg' || fileExtension == 'JPG' || fileExtension == 'jpeg' || fileExtension == 'JPEG')
				{

					// Obtenemos la imagen del campo "file".
					for (var i = 0, f; f = files[i]; i++) {

						if (!f.type.match('image.*')) {

							continue;
						}

						var reader = new FileReader();
						reader.onload = (function (theFile) {

							return function (e) {

								// Insertamos la imagen en la etiqueta img para dar el
								// efecto que cargo temporalmente la imagen nueva
								$("#cargarImagen").attr("src", e.target.result);
								$("#cargarImagen").css({
									'width': '140px',
									'height': '140px'
								});


							};

						})(f);
						reader.readAsDataURL(f);
					}


				}
				else
				{
					swal("¡Extensión de archivo no válido!", "Asegurese de subir un archivo de tipo imagen: jpg/jpeg/png", "warning");
					$("#ind_ruta_img").val('');
					$("#ind_ruta_img_f").val('');
					$("#cargarImagen").attr("src",'{$_Parametros.ruta_Img}modIN/imagen_evento.png');
					$("#cargarImagen").css({ 'width':'140px', 'height':'140px' })
				}
			}
			else
			{
				swal("¡Nombre de archivo no válido!", "Renombre el archivo, sin caracteres especiales", "warning");
				$("#ind_ruta_img").val('');
				$("#ind_ruta_img_f").val('');
				$("#cargarImagen").attr("src",'{$_Parametros.ruta_Img}modIN/imagen_evento.png');
				$("#cargarImagen").css({ 'width':'140px', 'height':'140px' })
			}
		});
		//*************************************************************
		//* ENVIO Y VALIDACUÓN DEL FORMULARIO
		//*************************************************************
		$("#formAjax").validate({
			submitHandler: function(form) {

				var ind_ruta_img_f = $("#ind_ruta_img_f").val();
				if(ind_ruta_img_f == ""){
					swal("¡La imagen del evento es requerida!", "Cargue una imagen tipo: jpg - jpeg - png", "warning");
				}
				else
				{
					//===================================================
					//  ENVIO DEL FORMULARIO
					//===================================================
					var url_nueva_solicitud = '{$_Parametros.url}modIN/eventosCONTROL/RegistrarEventoMET';
                    var data = new FormData($("#formAjax")[0]);
                    //data.append('ckeditor',txt_cuerpo);
                    $.ajax({
                                url: url_nueva_solicitud,
                                data: data,
                                type: 'POST',
                                contentType: false,
                                processData: false,
                                success: function(respuesta_post){

		                        if(respuesta_post == 1) {
		                            
		                            
		                            $('#formAjax').each(function () { {*resetear los campos del formulario*}
		                                this.reset();
		                            });

		                            $("#ind_ruta_img_f").val('');
                                    $("#ind_ruta_img").val('');
                                    
                                    $("#cargarImagen").attr("src",'{$_Parametros.ruta_Img}modIN/imagen_evento.png');
                                    
                                    $('select[id="ind_estado"]').val('..');
									$("#ind_estado").change();
									
									$('select[id="ind_municipio"]').val('..');
									$("#ind_municipio").change();
									$('select[id="ind_municipio"]').prop('disabled', true);

									$('select[id="ind_parroquia"]').val('..');
									$("#ind_parroquia").change();
									$('select[id="ind_parroquia"]').prop('disabled', true);

									
									swal({
					                title:'¡Evento registrado!',
					                text: 'guardado satisfactoriamente.',
					                type: "success"                
					                }, function(){


										$(document.getElementById('cerrarModal')).click();
										$.post('{$_Parametros.url}modIN/eventosCONTROL/',{  },function(dato){
											$('#_contenido').html(dato);
										});
										/*$('#formModalLabel').html("");
										$('#ContenidoModal').html("");*/
					                   //parent.window.location.reload();
					                });		
									
		                            
		                        }else{

		                            alert(respuesta_post);
		                        }

	                    } 
	                });
					//===================================================
					//
					//====================================================
				}


			}
		});

		//**********************************************************
		//* SELECCIONAR ESTADO
		//************************************************************
		$("#ind_estado").change(function () {
			$("#ind_estado option:selected").each(function () {

				estado = $(this).val();

				if(estado != '..')
				{
					$('select[id="ind_municipio"]').prop('disabled', false);
					
					$.post('{$_Parametros.url}modIN/eventosCONTROL/BuscarMunicipioMET', { fk_a009_num_estado : estado }, function (data) {
						$("#ind_municipio").html(data);
					});
			    }
				else
				{
					$('select[id="ind_municipio"]').val('..');
					$("#ind_municipio").change();
					$('select[id="ind_municipio"]').prop('disabled', true);				
					
				}
			});
		});
		//*********************************************************
		// SELECCIONE UN MUNICIPIO
		//**********************************************************
		$("#ind_municipio").change(function () {
			$("#ind_municipio option:selected").each(function () {

				municipio = $(this).val();

				if(municipio != '..')
				{
					
					$('select[id="ind_parroquia"]').prop('disabled', false);
					$.post('{$_Parametros.url}modIN/eventosCONTROL/BuscarParroquiaMET', { fk_a011_num_municipio : municipio }, function (data) {
						$("#ind_parroquia").html(data);
					});
			    }
				else
				{
					$('select[id="ind_parroquia"]').val('..');
					$("#ind_parroquia").change();
					$('select[id="ind_parroquia"]').prop('disabled', true);
					
				}
			});
		});
		//******************************************************************
		//*
		//******************************************************************


	});

</script>
