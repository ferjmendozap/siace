<section class="style-default-light">&nbsp;

<!--CUERPO DEL FORMULARIO-->
 <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >
    <div class="row">
        <div class="col-sm-12 ">
            <div class="form-group" >
                <input style="text-transform: uppercase" value=""  maxlength="50" type="text" name="nombre_nuevo_crear_carpeta" id="nombre_nuevo_crear_carpeta" class="form-control" required aria-required="true" aria-invalid="true" onkeypress="return validarNombreCarpeta(event)" >
                <label for="nombre_nuevo_crear_carpeta">Nombre de la carpeta</label>
                <p class="help-block">Solo se admiten hasta 50 caracteres</p>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group" >
                <select id="ind_dependencia_nueva_carpeta" class="form-control " name="ind_dependencia_nueva_carpeta" required aria-required="true" aria-invalid="true" data-msg-required="Seleccione la dependencia" >
                {if $pk_num_dependencia_nueva_carpeta == 'T' }
                    {foreach item=indice from=$ind_dependencia_nueva_carpeta}
                        <option value="{$indice.pk_num_dependencia }" >{$indice.ind_dependencia}</option>
                    {/foreach}
                {else}
                    <option value="{$pk_num_dependencia_nueva_carpeta }" selected="selected">{$ind_dependencia_nueva_carpeta}</option>
                {/if}
                </select>
                <label for="ind_dependencia_nueva_carpeta">Dependencia</label>
            </div>
        </div>
        <div class="row">
            <div align="right" class="col-sm-12">
                <button id="botonCerrar" class="btn btn-sm btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal" type="button"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                <button class="btn btn-sm btn-primary ink-reaction btn-raised logsUsuarioModal" type="submit">NUEVA CARPETA</button>
            </div>
        </div>
    </div>
 </form>
<!--FIN DEL TITULO DEL FORMULARIO-->
</section>
<script>
$("#ind_dependencia_nueva_carpeta").select2();
function validarNombreCarpeta(evt){

    var charCode = evt.which || evt.keyCode;
    if (charCode==8||charCode==13||charCode==0 || charCode==37 || charCode==39)//37 izq 39 der
       return true;
    var str = String.fromCharCode(charCode);
    if (!/[0-9\A-Z\a-z ]/.test(str)) {
            return false;
    };
}

$(document).ready(function() {


    $("#formAjax").validate({

        submitHandler: function(form) {


            var urlCrearCarpeta = '{$_Parametros.url}modIN/documentosCONTROL/CrearCarpetaMET';
            var nombre_nuevo_crear_carpeta = $('#nombre_nuevo_crear_carpeta').val();
            var ind_dependencia_nueva_carpeta = $('select[id="ind_dependencia_nueva_carpeta"]').val();
            $.post(urlCrearCarpeta, { nombre_nuevo_crear_carpeta: nombre_nuevo_crear_carpeta, ind_dependencia_nueva_carpeta: ind_dependencia_nueva_carpeta }, function (resp) {

                if(resp == 1)
                {

                    swal({
                        title:'Carpeta creada',
                        text: 'Satisfactoriamente',
                        type: "success",
                        closeOnConfirm: true
                    }, function(){


                        if('{$tipo_form}' == 'L')//desde form listar
                        {
                            $(document.getElementById('cerrarModal3')).click();
                            construirMenuDependencias();
                        }
                        else//desde form subir archivo
                        {
                            var ind_dependencia = $('select[id="ind_dependencia"]').val();
                            if(ind_dependencia != 'I')
                            {
                                construirMenuDependencias();
                            }

                            var ind_dependencia_subir_archivo = $('select[id="ind_dependencia_subir_archivo"]').val();

                            var urlbuscarCarpetas = '{$_Parametros.url}modIN/documentosCONTROL/BuscarCarpetasSubirArchivosMET';
                                 $.post(urlbuscarCarpetas, { dependencia: ind_dependencia_subir_archivo }, function (resp) {

                                 if(resp != 0)//hay carpetas para la dependencia
                                 {
                                    $('select[id="carpeta_subir_archivo"]').html(resp);
                                    $('select[id="carpeta_subir_archivo"]').change();
                                    $('select[id="carpeta_subir_archivo"]').prop('disabled', false);
                                 }
                                 else
                                 {
                                    alert('error al cargar las dependencias: '+resp);
                                 }
                             });

                            $(document.getElementById('cerrarModal3')).click();
                        }
                    });

                }
                else
                {
                    if(resp == 2)
                    {
                        swal({
                            title:'El nombre ya existe en está dependencia',
                            text: 'Suministre otro nombre para la carpeta y vuelva a intentarlo...',
                            type: "warning",
                            closeOnConfirm: true
                        } );
                    }
                    else
                    {
                        alert('Error al crear la carpeta.');
                    }


                }
            });
        }
    });

});

</script>


