
<section class="style-default-light">&nbsp;

<!--CUERPO DEL FORMULARIO-->
 <form id="formAjax2" class="form form-validation" role="form" novalidate="novalidate" >
    <div class="row">

    </div>
    <div class="row">
        <div class="col-sm-12 ">
            <div class="form-group" >
                <input style="text-transform: uppercase" value="{$tituloFormat}" maxlength="50"  type="text" name="nombre_nuevo_carpeta" id="nombre_nuevo_carpeta" class="form-control" required aria-required="true" aria-invalid="true" onkeypress="return validarNombreCarpeta(event)" >
                <label for="nombre_nuevo_carpeta">Nombre de la carpeta</label>
                <p class="help-block">Solo se admiten hasta 50 caracteres</p>
            </div>
        </div>
    </div>
    <div class="row">&nbsp;</div>
    <div class="row">
         <div class="col-sm-12" align="right">
             <button id="botonCerrar" class="btn btn-sm btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal" type="button"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
             <button id="botonRenombrarCarpeta" class="btn btn-sm btn-primary ink-reaction btn-raised logsUsuarioModal" type="submit">Aceptar</button>
         </div>
    </div>
 </form>
<!--FIN DEL TITULO DEL FORMULARIO-->
</section>
<script>

function validarNombreCarpeta(evt){

    var charCode = evt.which || evt.keyCode;
    if (charCode==8||charCode==13||charCode==0 || charCode==37 || charCode==39)//37 izq 39 der
       return true;
    var str = String.fromCharCode(charCode);
    if (!/[0-9\A-Z\a-z\á\é\í\ó\ú\ñ\Ñ\š\ü ]/.test(str)) {
            return false;
    };
}

document.getElementById('nombre_nuevo_carpeta').focus();

$(document).ready(function() {


    $("#formAjax2").validate({

        submitHandler: function(form) {

            var urlRenombrar2 = '{$_Parametros.url}modIN/documentosCONTROL/CambiarNombreCarpetaMET';
            var clave_dependencia = '{$clave_dependencia}';
            var nombre_nuevo_carpeta = $('#nombre_nuevo_carpeta').val();
            var nombreViejo = '{$nombreAntesCarpeta}';
            $.post(urlRenombrar2, { pk_num_carpetas_documentos: '{$pk_num_carpetas_documentos}', nombre_nuevo_carpeta: nombre_nuevo_carpeta, clave_dependencia: clave_dependencia, nombreViejo: nombreViejo }, function (resp) {

                if(resp != -1 && resp != 2 && resp != -3 && resp != -2)
                {

                    swal({
                        title:'Carpeta Renombrada',
                        text: 'La Carpeta se renombró exitosamente',
                        type: "success",
                        closeOnConfirm: true
                    }, function(){
                        $("#datatable2 tr td[id=" + "{$pk_num_carpetas_documentos}" + "]").html(resp.replace(/_/g,' '));
                        $("#btn_"+"{$pk_num_carpetas_documentos}").attr("titulo",resp);
                        $("#btn_e_"+"{$pk_num_carpetas_documentos}").attr("titulo",resp);
                        $(document.getElementById('cerrarModal3')).click();
                        construirMenuDependencias();
                    });

                }
                else
                {
                    if(resp == 2)
                    {
                        swal({
                            title:'El nombre ya Existe',
                            text: 'Suministre otro nombre para la Carpeta',
                            type: "warning",
                            closeOnConfirm: true
                        } );
                    }
                    else
                    {
                        alert('Error al crear la carpeta.');
                    }
                }
            });
        }
    });

});

</script>