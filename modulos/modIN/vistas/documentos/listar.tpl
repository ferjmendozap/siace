<section class="style-default-light">
<!--TITULO DEL FORMULARIO-->
{if $origenFormDoc != 'I'}
<div class="row">
	<div class="col-lg-12">
		<h2 class="text-primary">&nbsp;Listar documentos y datos internos</h2>
	</div>
</div>
{/if}
<div class="row">&nbsp;</div>
<!--CUERPO DEL FORMULARIO-->
    <form class="form" role="form" >
    <div class="row">
        <div class="col-sm-12 ">
            <div class="card">
                <div class="card-body">
                    <div class="col-sm-6">
                        <div class="form-group" >
                            <select id="ind_dependencia" class="form-control " name="ind_dependencia" required aria-required="true" aria-invalid="true" data-msg-required="Seleccione la dependencia" >
                                <option value="I" selected="selected">Seleccione la dependecia...</option>

                                {foreach item=indice from=$datosDependencias}
                                <option value="{$indice.pk_num_dependencia }" >{$indice.ind_dependencia}</option>
                                {/foreach}
                            </select>
                            <label for="ind_dependencia">Dependencia</label>
                        </div>
                    </div>
                    <div class="col-sm-3">&nbsp;</div>
                    <div  class="col-sm-3">
                        <div class="form-group" >
                            {if in_array('IN-01-04-01-04-N', $_Parametros.perfil) || in_array('IN-01-04-01-08-AD', $_Parametros.perfil) }
                            <button type="button" id="subir_documento"  data-backdrop="static" data-keyboard="false" data-target="#formModal2" data-toggle="modal" class="logsUsuario btn ink-reaction btn-raised btn-info">
                                    SUBIR ARCHIVO <i class="md md-file-upload"></i>
                            </button>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3 ">
                <div class="card card-bordered style-primary "   >
                    <div class="card-head">
                        <header>
                            <i class="fa md-storage "></i>
                            MENÚ
                        </header>
                        <div class="tools" style="padding: 0 !important;">
                            <div class="btn-group">
                                {if in_array('IN-01-04-01-03-N', $_Parametros.perfil) || in_array('IN-01-04-01-08-AD', $_Parametros.perfil) }
                                    <a title="Nueva carpeta" id="nueva_carpeta_menu" disabled="disabled" class="btn btn-icon-toggle" data-target="#formModal3" data-toggle="modal" data-keyboard="false" data-backdrop="static"><i class="md md-folder-open"></i></a>
                                {/if}
                                <a title="Editar menú" id="editar_menu" disabled="disabled" class="btn btn-icon-toggle" data-target="#formModal2" data-toggle="modal" data-keyboard="false" data-backdrop="static"><i class="md md-edit "></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="style-default-bright">
                        <div class="card-body style-default-bright  " style="margin:10px; padding:0px;"  aling="center" >
                            <form class="form">
                                <div class="="form-group">
                                    <div class="input-group">
                                        <div class="input-group-content">
                                            <input class="form-control input-xs campo_filtro_carpeta" id="campo_filtro_carpeta" disabled="disabled" placeholder="Filtrar por nombre de carpetas..." type="text" >
                                        </div>
                                        <div class="input-group-btn">
                                            <button id="btn_buscar_carpeta" class="btn btn-default  ink-reaction btn-sm" type="button" disabled="disabled" title="Buscar carpetas"><i class="fa fa-search"></i></button>
                                        </div>
                                        <div class="input-group-btn">
                                            <button id="btn_recargar_menu" class="btn btn-default  ink-reaction btn-sm" type="button" disabled="disabled" title="Recargar menú"><i class="glyphicon glyphicon-refresh"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-body style-default-bright  height-12 scroll" style="margin:10px; padding:0px;"  aling="center" >
                            <div id="tree" ></div>
                        </div>
                    </div>
               </div>
        </div>
        <div class="col-sm-9 ">
            <div class="card card-bordered style-primary "   >
                <div class="card-head">
                    <header>
                        <i class="md-folder"></i>
                        CONTENIDO <span id="carpeta_cont"></span>
                    </header>
                </div>
                <div class="card-body style-default-bright" >
                    <div class="table-responsive">
                    <table id="datatable1" class="  table table-striped table-hover" width="100%" style="width: 100%;"   >
                    <thead>
                    <tr  align="center">
                        <th style="" ><i class=""></i>N°</th>
                        <th style=""  ><i class=""></i>Nombre</th>
                        <th style="" ><i class=""></i>Formato</th>
                        <th style=""  ><i class=""></i>Peso</th>
                        <th style=""><i class=""></i>Fecha</th>
                        <th style=""><i class=""></i>Depemdencia</th>
                        <th style=""><i class=""></i>Vista previa</th>
                        <th style=""><i class=""></i>Descargar</th>
                        <th style=""><i class=""></i>Editar</th>
                        <th style=""><i class=""></i>Eliminar</th>
                    </tr>
                    </thead>
                    <tbody >

                    <tr class="fila_agregar" >
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td >&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td ></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>

                    </tbody>
                </table>
                </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">&nbsp;</div>
    </form>
<!--FIN DEL TITULO DEL FORMULARIO-->
</section>
<a id="link_descarga_boton" onclick="" style="display:none;" ></a>
<a id="link_ver_boton" style="display:none;" data-target="#formModal2" data-toggle="modal" data-keyboard="false" data-backdrop="static" ></a>
<audio id="link_reproducir_boton" src="" style="display:none;" > </audio>

<style>

    .treeview .list-group-item a { /*estilos para el menu*/
        text-decoration: none;
        font-size: 10px;
        color: #313534 !important;
    }

    .fuenteTituloArchivos{
       font-size: 10px;
    }

</style>

<script>

//******************************************
//*  LISTADO DE CONTENIDO
//******************************************

var CARPETA_DOC_INTER = '';
var tabla_contenido = $('#datatable1').DataTable();

function accionNodo(id, dependencia, nombre_carpeta){
    CARPETA_DOC_INTER = nombre_carpeta;

    var elemento = nombre_carpeta;
    elemento = elemento.replace(/_/g,' ');

    $("#carpeta_cont").html('<i class="glyphicon glyphicon-chevron-right"></i> '+elemento);
    var tabla_contenido = $('#datatable1').DataTable();
    tabla_contenido.clear().draw();

    var urlDocumentos = '{$_Parametros.url}modIN/documentosCONTROL/BuscarDocumentosMET';
    $.post(urlDocumentos, { pk_num_carpetas_documentos: id }, function(respuesta_post){

        if(respuesta_post != 0)
        {
            var descripcionArchivo2 = '';
            var descripcionArchivo = '';
            var contador = 0;
            var cad_ver = '';
            for(var i=0; i<respuesta_post.length; i++)
            {
                if(respuesta_post[i].ind_formato == 'pdf' || respuesta_post[i].ind_formato == 'PDF' || respuesta_post[i].ind_formato == 'JPG' || respuesta_post[i].ind_formato == 'jpg' || respuesta_post[i].ind_formato == 'jpeg' || respuesta_post[i].ind_formato == 'JPEG' || respuesta_post[i].ind_formato == 'png' || respuesta_post[i].ind_formato == 'PNG' || respuesta_post[i].ind_formato == 'gif' || respuesta_post[i].ind_formato == 'GIF' )
                    cad_ver ='<button type="button" nombre_carpeta="'+nombre_carpeta+'" pk_num_carpeta="'+id+'" formato="'+respuesta_post[i].ind_formato+'" nombre_documento = "'+respuesta_post[i].ind_descripcion+'.'+respuesta_post[i].ind_formato+'" pk_num_documentos = "'+respuesta_post[i].pk_num_documentos+'" class="ver gsUsuario btn ink-reaction btn-raised btn-xs btn-info"  ><i class="md-search "></i></button>';
                else
                  if(respuesta_post[i].ind_formato == 'mp3' || respuesta_post[i].ind_formato == 'MP3' )
                      cad_ver ='<button type="button" nombre_carpeta="'+nombre_carpeta+'" pk_num_carpeta="'+id+'" pk_num_documentos = "'+respuesta_post[i].pk_num_documentos+'" formato="'+respuesta_post[i].ind_formato+'" nombre_documento = "'+respuesta_post[i].ind_descripcion+'.'+respuesta_post[i].ind_formato+'" class="reproducir gsUsuario btn ink-reaction btn-raised btn-xs btn-primary"  ><i class="md-play-circle-outline"></i></button>';
                  else
                      cad_ver = '<button disabled="disabled" type="button" nombre_carpeta="'+nombre_carpeta+'" pk_num_carpeta="'+id+'"  class="gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="No tiene vista previa"  ><i class="md-visibility-off"></i></button>';

                contador = i+1;
                descripcionArchivo = respuesta_post[i].ind_descripcion;
                descripcionArchivo2 = descripcionArchivo.replace(/_/g,' ');

                tabla_contenido.row.add([
                    contador,
                    descripcionArchivo2,
                    respuesta_post[i].ind_formato,
                    respuesta_post[i].ind_peso,
                    respuesta_post[i].fec_creacion,
                    dependencia,
                    {if in_array('IN-01-04-01-07-L', $_Parametros.perfil) || in_array('IN-01-04-01-08-AD', $_Parametros.perfil) }
                    cad_ver,
                    {else}
                    '<i class="md-not-interested"></i>',
                    {/if}
                    {if in_array('IN-01-04-01-07-L', $_Parametros.perfil) || in_array('IN-01-04-01-08-AD', $_Parametros.perfil) }
                    '<button type="button" nombre_carpeta="'+nombre_carpeta+'" pk_num_carpeta="'+id+'" nombre_documento = "'+respuesta_post[i].ind_descripcion+'.'+respuesta_post[i].ind_formato+'" pk_num_documentos = "'+respuesta_post[i].pk_num_documentos+'" class="descargar gsUsuario btn ink-reaction btn-raised btn-xs btn-primary"><i class="md-file-download"></i></button>',
                    {else}
                        '<i class="md-not-interested"></i>',
                    {/if}
                    {if in_array('IN-01-04-01-06-M', $_Parametros.perfil) || in_array('IN-01-04-01-08-AD', $_Parametros.perfil) }
                    '<button type="button" nombre_carpeta="'+nombre_carpeta+'" pk_num_carpeta="'+id+'" nombre="'+respuesta_post[i].ind_descripcion+'" formato_documento="'+respuesta_post[i].ind_formato+'" pk_num_documentos = "'+respuesta_post[i].pk_num_documentos+'" class="editar gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal2" data-toggle="modal" data-keyboard="false" data-backdrop="static"  ><i class="md-mode-edit"></i></button>',
                    {else}
                    '<i class="md-not-interested"></i>',
                    {/if}
                    {if in_array('IN-01-04-01-05-E', $_Parametros.perfil) || in_array('IN-01-04-01-08-AD', $_Parametros.perfil) }
                    '<button type="button" nombre_carpeta="'+nombre_carpeta+'" pk_num_carpeta="'+id+'" nombre_documento = "'+respuesta_post[i].ind_descripcion+'.'+respuesta_post[i].ind_formato+'" pk_num_documentos = "'+respuesta_post[i].pk_num_documentos+'" class="eliminar gsUsuario btn ink-reaction btn-raised btn-xs btn-danger"><i class="md md-delete"></i></button>'
                    {else}
                    '<i class="md-not-interested"></i>',
                    {/if}
                ]).draw().nodes().to$().addClass( 'fuenteTituloArchivos').attr('style','').find('td').eq(1).attr('id_nom_archivo',respuesta_post[i].pk_num_documentos).attr('width','200px');

            }
        }



    }, 'json');





}

$('#datatable1 tbody').on('click','.descargar', function(){

    var nombre_carpeta =  $(this).attr('nombre_carpeta');
    var pk_num_carpeta =  $(this).attr('pk_num_carpeta');

    var nombre_documento = $(this).attr('nombre_documento');

    $("#link_descarga_boton").attr("onclick", "document.location='{$_Parametros.url}publico/imagenes/modIN/documentos/descarga_archivos.php?file="+nombre_documento+"&nombre_carpeta="+nombre_carpeta+"_"+pk_num_carpeta+"'");
    $("#link_descarga_boton").click();

});

$('#datatable1 tbody').on('click','.ver', function(){


     var nombre_carpeta =  $(this).attr('nombre_carpeta');
     var pk_num_carpeta =  $(this).attr('pk_num_carpeta');

     var nombre_documento = $(this).attr('nombre_documento');
     var ruta = '{$_Parametros.url}publico/imagenes/modIN/documentos/'+nombre_carpeta+'_'+pk_num_carpeta+'/'+nombre_documento;
     var formato = $(this).attr('formato');
     if(formato == 'JPEG' || formato == 'jpeg' || formato == 'JPG' || formato == 'jpg' || formato == 'png' || formato == 'PNG' || formato == 'gif' || formato == 'GIF' )
     {
         $('#modalAncho2').css("width", "60%");
         $('#formModalLabel2').html('');
         $('#ContenidoModal2').html('');


         var nombre_carpeta_format = CARPETA_DOC_INTER.replace(/_/g,' ');
         var nombre_archivo_format = $(this).attr('nombre_documento').replace(/_/g,' ');

         $('#formModalLabel2').html('IMAGEN: '+nombre_carpeta_format+' <i class="glyphicon glyphicon-chevron-right"></i> '+nombre_archivo_format);

         var imagenNoticia = '<div align="center"><img width="100%" src="'+ruta+'"></div>';
         $('#ContenidoModal2').html(imagenNoticia);

         $("#link_ver_boton").click();
     }
     else
     {
         if(formato == 'pdf' || formato == 'PDF')
         {
             $('#modalAncho2').css("width", "80%");
             $('#formModalLabel2').html('');
             $('#ContenidoModal2').html('');

             var nombre_carpeta_format = CARPETA_DOC_INTER.replace(/_/g,' ');
             var nombre_archivo_format = $(this).attr('nombre_documento').replace(/_/g,' ');

             $('#formModalLabel2').html('PDF: '+nombre_carpeta_format+' <i class="glyphicon glyphicon-chevron-right"></i> '+nombre_archivo_format);
             $('#ContenidoModal2').html('<iframe src="' + ruta + '" width="100%" height="950"></iframe>');

             $("#link_ver_boton").click();
         }

     }

 });


var PLAYING = false;
$('#datatable1 tbody').on('click','.reproducir', function(){


    var nombre_carpeta =  $(this).attr('nombre_carpeta');
    var pk_num_carpeta =  $(this).attr('pk_num_carpeta');

    var pk_num_documentos = $(this).attr('pk_num_documentos');
    var nombre_documento = $(this).attr('nombre_documento');
    $("#link_reproducir_boton").attr("src","{$_Parametros.url}publico/imagenes/modIN/documentos/"+nombre_carpeta+'_'+pk_num_carpeta+"/"+nombre_documento);


    $(this).toggleClass("down");

    if(PLAYING == false)
    {
        document.getElementById('link_reproducir_boton').play();
        PLAYING = true;
        $(this).html('<i class="md-play-circle-fill "></i>');

    }
    else
    {
        document.getElementById('link_reproducir_boton').pause();
        PLAYING = false;
        $(this).html('<i class="md-play-circle-outline"></i>');
    }


});

//*************************************
//* CONSTRUCCION DEL MENU
//************************************
$("#ind_dependencia").select2();

function construirMenuDependencias()
{
    var pk_num_dependencia = $('select[id="ind_dependencia"]').val();


    if(pk_num_dependencia != 'T' && pk_num_dependencia != 'I')
    {
        var urlcarpetasDocumentos = '{$_Parametros.url}modIN/documentosCONTROL/BuscarCarpetasDocumentosMET';
        $.post(urlcarpetasDocumentos, { pk_num_dependencia: pk_num_dependencia }, function (dato) {

            tabla_contenido.clear().draw();

            var texto = $('select[id="ind_dependencia"] option:selected').html();

            var carpetas = [];

            if (dato != 0) {

                var len = dato.length;
                for (var i = 0; i < len; i++) {
                    carpetas.push({
                        text: '<p style="padding-left:60px !important; position: absolute; top: 30%; bottom: 10%;" >'+dato[i].ind_descripcion_carpeta.replace(/_/g,' ')+'</p>',//'<div style="padding-left:60px !important; position: absolute; bottom: 10%;">'+dato[i].ind_descripcion_carpeta+'</div>',
                        icon: "glyphicon glyphicon-folder-close",
                        selectedIcon: "glyphicon glyphicon-folder-open",
                        href: 'javascript:accionNodo(' + dato[i].pk_num_carpetas_documentos + ', "' + texto + '", "' + dato[i].ind_descripcion_carpeta + '" );'
                    });
                }
            }


            var menuContenido = [
                {
                    text: texto,
                    icon: "glyphicon glyphicon-hdd ",
                    selectedIcon: "md-folder-open ",
                    color: "#000000",
                    selectable: false,
                    href: "javascript:void(0);",
                    state: {
                        checked: true,
                        disabled: false,
                        expanded: true,
                        selected: false
                    },
                    tags: ['available'],
                    nodes: carpetas
                }
            ];

            $('#tree').treeview({ data: menuContenido });

            $('#nueva_carpeta_menu').attr('disabled', false);
            $('#editar_menu').attr('disabled', false);

            $('#btn_buscar_carpeta').attr('disabled', false);
            $('#campo_filtro_carpeta').attr('disabled', false);
            $('#btn_recargar_menu').attr('disabled', false);




        }, 'json');//FIN DEL POST
    }
    else
    {
        if(pk_num_dependencia == 'T')
        {
            var urlcarpetasDocumentos = '{$_Parametros.url}modIN/documentosCONTROL/BuscarDepenciasMET';
            $.post(urlcarpetasDocumentos, { }, function (dependecias) {

                if(dependecias != 0 )
                {
                    var urlcarpetasDocumentos = '{$_Parametros.url}modIN/documentosCONTROL/BuscarCarpetasDocumentosMET';
                    $.post(urlcarpetasDocumentos, { pk_num_dependencia: 'T' }, function (carpetas) {

                        var menucarpetas = [];
                        var menudependencia = [];
                        for (var i = 0; i < dependecias.length; i++)
                        {
                            menucarpetas = [];
                            for (var j = 0; j < carpetas.length; j++)
                            {
                                if(carpetas[j].fk_a004_dependencia == dependecias[i].pk_num_dependencia)
                                {
                                    menucarpetas.push({
                                        text: '<p style="padding-left:60px !important; position: absolute; top: 30%; bottom: 10%;" >'+carpetas[j].ind_descripcion_carpeta.replace(/_/g,' ')+'</p>',
                                        icon: "glyphicon glyphicon-folder-close",
                                        selectedIcon: "glyphicon glyphicon-folder-open",
                                        href: 'javascript:accionNodo(' + carpetas[j].pk_num_carpetas_documentos + ', "' + dependecias[i].ind_dependencia + '", "' + carpetas[j].ind_descripcion_carpeta + '" );'
                                    });


                                }
                            }


                            menudependencia.push({
                                text: dependecias[i].ind_dependencia,
                                icon: "glyphicon glyphicon-hdd ",
                                selectedIcon: "md-folder-open ",
                                color: "#000000",
                                selectable: false,
                                href: "javascript:void(0);",
                                state: {
                                    checked: true,
                                    disabled: false,
                                    expanded: true,
                                    selected: false
                                },
                                tags: ['available'],
                                nodes: menucarpetas
                            });

                        }

                        $('#tree').treeview({ data: menudependencia });
                        tabla_contenido.clear().draw();
                        $('#editar_menu').attr('disabled',false);
                        $('#nueva_carpeta_menu').attr('disabled',false);

                        $('#btn_buscar_carpeta').attr('disabled', false);
                        $('#campo_filtro_carpeta').attr('disabled', false);
                        $('#btn_recargar_menu').attr('disabled', false);


                    }, 'json');
                }
                else
                {
                    alert('Error: Buscar dependencia...');
                }

            }, 'json');
        }
        else
        {
            tabla_contenido.clear().draw();
            $('#tree').html('');
            $('#editar_menu').attr('disabled',true);
            $('#nueva_carpeta_menu').attr('disabled',true);

            $('#btn_buscar_carpeta').attr('disabled', true);
            $('#campo_filtro_carpeta').attr('disabled', true);
            $('#btn_recargar_menu').attr('disabled', true);
        }
    }
}

//***********************************************************
//* CONSTRUIR MENU SOLO CON LAS CARPETAS FILTRADAS EN BUSCAR
//***********************************************************
function construirMenuDependenciaFiltro($patron)
{
    var pk_num_dependencia = $('select[id="ind_dependencia"]').val();
    if(pk_num_dependencia != 'T' && pk_num_dependencia != 'I')//No son todas las dependencias a mostrar en el menu
    {
        var urlcarpetasDocumentos = '{$_Parametros.url}modIN/documentosCONTROL/BuscarCarpetasFiltrarMET';
        $.post(urlcarpetasDocumentos, { pk_num_dependencia: pk_num_dependencia, patron: $patron }, function (dato) {

            tabla_contenido.clear().draw();

            var texto = $('select[id="ind_dependencia"] option:selected').html();

            var carpetas = [];

            if (dato != 0) {

                var len = dato.length;
                for (var i = 0; i < len; i++) {
                    carpetas.push({
                        text: '<p style="padding-left:60px !important; position: absolute; top: 30%; bottom: 10%;" >'+dato[i].ind_descripcion_carpeta.replace(/_/g,' ')+'</p>',
                        icon: "glyphicon glyphicon-folder-close",
                        selectedIcon: "glyphicon glyphicon-folder-open",
                        href: 'javascript:accionNodo(' + dato[i].pk_num_carpetas_documentos + ', "' + texto + '", "' + dato[i].ind_descripcion_carpeta + '" );'
                    });
                }
            }


            var menuContenido = [
                {
                    text: texto,
                    icon: "glyphicon glyphicon-hdd ",
                    selectedIcon: "md-folder-open ",
                    color: "#000000",
                    selectable: false,
                    href: "javascript:void(0);",
                    state: {
                        checked: true,
                        disabled: false,
                        expanded: true,
                        selected: false
                    },
                    tags: ['available'],
                    nodes: carpetas
                }
            ];

            $('#tree').treeview({ data: menuContenido });

            $('#nueva_carpeta_menu').attr('disabled', false);
            $('#editar_menu').attr('disabled', false);

            $('#btn_buscar_carpeta').attr('disabled', false);
            $('#campo_filtro_carpeta').attr('disabled', false);
            $('#btn_recargar_menu').attr('disabled', false);




        }, 'json');//FIN DEL POST
    }
    else
    {
        if(pk_num_dependencia == 'T')//todas la dependencias al mostrar en el menu
        {
            var urlcarpetasDocumentos = '{$_Parametros.url}modIN/documentosCONTROL/BuscarDepenciasMET';
            $.post(urlcarpetasDocumentos, { }, function (dependecias) {

                if(dependecias != 0 )
                {
                        var urlcarpetasDocumentos = '{$_Parametros.url}modIN/documentosCONTROL/BuscarCarpetasFiltrarMET';
                        $.post(urlcarpetasDocumentos, { pk_num_dependencia: 'T', patron: $patron }, function (carpetas) {
                        var sw_carpeta;

                        var menucarpetas = [];
                        var menudependencia = [];

                        for (var i = 0; i < dependecias.length; i++)
                        {
                            menucarpetas = [];
                            sw_carpeta = 0;
                            for (var j = 0; j < carpetas.length; j++)
                            {
                                if(carpetas[j].fk_a004_dependencia == dependecias[i].pk_num_dependencia)
                                {
                                    sw_carpeta = 1;
                                    menucarpetas.push({
                                        text: '<p style="padding-left:60px !important; position: absolute; top: 30%; bottom: 10%;" >'+carpetas[j].ind_descripcion_carpeta.replace(/_/g,' ')+'</p>',
                                        icon: "glyphicon glyphicon-folder-close",
                                        selectedIcon: "glyphicon glyphicon-folder-open",
                                        href: 'javascript:accionNodo(' + carpetas[j].pk_num_carpetas_documentos + ', "' + dependecias[i].ind_dependencia + '", "' + carpetas[j].ind_descripcion_carpeta + '" );'
                                    });


                                }
                            }

                            if(sw_carpeta == 1) //la dependencia tiene al menos una carpeta
                            {
                                menudependencia.push({
                                    text: dependecias[i].ind_dependencia,
                                    icon: "glyphicon glyphicon-hdd ",
                                    selectedIcon: "md-folder-open ",
                                    color: "#000000",
                                    selectable: false,
                                    href: "javascript:void(0);",
                                    state: {
                                        checked: true,
                                        disabled: false,
                                        expanded: true,
                                        selected: false
                                    },
                                    tags: ['available'],
                                    nodes: menucarpetas
                                });
                            }
                        }

                        $('#tree').treeview({ data: menudependencia });
                        tabla_contenido.clear().draw();
                        $('#editar_menu').attr('disabled',false);
                        $('#nueva_carpeta_menu').attr('disabled',false);
                        $('#buscar_carpeta_menu').attr('disabled', false);

                    }, 'json');
                }
                else
                {
                    alert('Error: Buscar dependencia...');
                }

            }, 'json');
        }
        else
        {
            tabla_contenido.clear().draw();
            $('#tree').html('');
            $('#editar_menu').attr('disabled',true);
            $('#nueva_carpeta_menu').attr('disabled',true);

            $('#btn_buscar_carpeta').attr('disabled', true);
            $('#campo_filtro_carpeta').attr('disabled', true);
            $('#btn_recargar_menu').attr('disabled', true);
        }
    }
}

$(document).ready(function()
{
    //*************************************
    //*     MENU
    //*************************************

    $('#ind_dependencia').change(function() {
        construirMenuDependencias();
    });

    //********************************************
    //* ELIMINAR CONTENIDO
    //********************************************
    $('#datatable1 tbody').on('click','.eliminar', function(){

        var urEliminar = '{$_Parametros.url}modIN/documentosCONTROL/EliminarDocumentoMET';
        var pk_num_documentos =  $(this).attr('pk_num_documentos');
        var titulo =  $(this).attr('nombre_documento');
        var nombre_archivo_eliminar = $(this).attr('nombre_documento');
        var fila = $(this);

        var nombre_carpeta =  $(this).attr('nombre_carpeta');
        var pk_num_carpeta =  $(this).attr('pk_num_carpeta');

        var nombre_archivo_format = titulo.replace(/_/g,' ');

        swal({
            title:'¿Eliminar Documento?',
            text: 'Se eliminará: '+nombre_archivo_format,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: 'Sí, Eliminar',
            cancelButtonText: "Cancelar",
            closeOnConfirm: false
        }, function(){
            $.post(urEliminar, { pk_num_documentos: pk_num_documentos, nombre_archivo_eliminar: nombre_archivo_eliminar, carpeta_eliminar: nombre_carpeta, pk_carpeta_eliminar: pk_num_carpeta }, function(resp){
                if(resp > 0)
                {
                    tabla_contenido.row( fila.parents('tr') )
                            .remove()
                            .draw();
                    swal("Documento Eliminado", "Documento eliminado exitosamente", "success");
                }
                else
                {
                    alert(resp);
                }

            });

        });

    });
    //***********************************************
    //*  EDITAR MENU - ELIMINAR Y RENOMBRAR CARPETAS
    //***********************************************
    $('#editar_menu').click(function(){

        $('#modalAncho2').css("width", "70%");
        $('#formModalLabel2').html('');
        $('#ContenidoModal2').html('');
        var pk_num_dependencia = $('select[id="ind_dependencia"]').val();
        var nombre_dependencia = $('select[id="ind_dependencia"] option:selected').html();
        urlEditar = '{$_Parametros.url}modIN/documentosCONTROL/EditarMenuMET';
        $.post(urlEditar, { pk_num_dependencia: pk_num_dependencia, nombre_dependencia: nombre_dependencia }, function (resp) {
            $('#formModalLabel2').html('<i class="md-mode-edit"></i> Editar Menú - Carpetas');
            $('#ContenidoModal2').html(resp);
        });


    });
    //****************************************
    //* NUEVA CARPETA
    //***************************************
    $('#nueva_carpeta_menu').click(function(){

        $('#modalAncho3').css("width", "40%");
        $('#formModalLabel3').html('');
        $('#ContenidoModal3').html('');

        $('#formModalLabel3').html('<i class="md-mode-edit"></i> Nueva carpeta');

        var pk_num_dependencia = $('select[id="ind_dependencia"]').val();
        var ind_dependencia = $('select[id="ind_dependencia"] option:selected').html();
        var urlNuevaCarpeta = '{$_Parametros.url}modIN/documentosCONTROL/NuevaCarpetaMET';
        $.post(urlNuevaCarpeta, { pk_num_dependencia: pk_num_dependencia, ind_dependencia: ind_dependencia, tipo_form: 'L' }, function (resp) {
            $('#ContenidoModal3').html(resp);
        });

    });
    //****************************************
    //* BUSCAR CARPETA MENU
    //***************************************
    $('#btn_buscar_carpeta').click(function(){

       var patron_filtro = $("#campo_filtro_carpeta").val();
       if(patron_filtro == "")
       {
           swal("Introduzca el nombre de la carpeta...", "", "warning");
       }
       else
       {
           construirMenuDependenciaFiltro(patron_filtro);
       }

    });

    $("#campo_filtro_carpeta").keyup(function(){

        construirMenuDependenciaFiltro($(this).val());

    });
    //******************************************
    //* RECARGAR MENU
    //******************************************
    $('#btn_recargar_menu').click(function(){
        construirMenuDependencias();
        $('#campo_filtro_carpeta').val('');

    });
    //*******************************************
    //* MODAL SUBIR ARCHIVOS
    //*******************************************
    $('#subir_documento').click(function(){

        $('#modalAncho2').css("width", "80%");
        $('#formModalLabel2').html('');
        $('#ContenidoModal2').html('');

        $('#formModalLabel2').html('<i class="md md-file-upload"></i> Subir archivo');

        var pk_num_dependencia = $('select[id="ind_dependencia"]').val();
        var urlSubirArchivo = '{$_Parametros.url}modIN/documentosCONTROL/SubirArchivoMET';
        $.post(urlSubirArchivo, { pk_num_dependencia: pk_num_dependencia }, function (resp) {
            $('#ContenidoModal2').html(resp);
        });

    });

    //*****************************************
    //*  FUNCION RECORTAR TEXTO DE ARCHIVOS (TITULO)
    //*****************************************
    function ellipsis_box(elemento, max_chars)
    {
        limite_text = elemento;
        if (limite_text.length > max_chars)
        {
            limite = limite_text.substr(0, max_chars)+" ...";
            return limite;
        }
        else
          return elemento;
    }

    //*****************************************
    //*   RENOMBRAR ARCHIVO
    //*****************************************
    $('#datatable1 tbody').on('click','.editar', function(){

        $('#modalAncho2').css("width", "40%");
        $('#formModalLabel2').html('');
        $('#ContenidoModal2').html('');

        var nombre_carpeta =  $(this).attr('nombre_carpeta');
        var pk_num_carpeta =  $(this).attr('pk_num_carpeta');
        var nombre_documento = $(this).attr('nombre');
        var pk_num_documentos = $(this).attr('pk_num_documentos');
        var formato_documento = $(this).attr('formato_documento');
        var ruta = '{$_Parametros.url}publico/imagenes/modIN/documentos/'+nombre_carpeta+'_'+pk_num_carpeta+'/'+nombre_documento;

        var elemento = nombre_documento+'.'+formato_documento;
        elemento = elemento.replace(/_/g,' ');
        $('#formModalLabel2').html('<i class="md-mode-edit"></i>&nbsp;Renombrar el archivo <i class="md-keyboard-arrow-right" ></i>&nbsp;'+ellipsis_box(elemento, 40));

        var urlRenombrar = '{$_Parametros.url}modIN/documentosCONTROL/RenombrarArchivoMET';
        $.post(urlRenombrar, { nombre_carpeta: nombre_carpeta, pk_num_carpeta: pk_num_carpeta, nombre_documento: nombre_documento, pk_num_documentos: pk_num_documentos, formato_documento: formato_documento }, function (resp) {
            $('#ContenidoModal2').html(resp);
        });

    });

});

</script>


