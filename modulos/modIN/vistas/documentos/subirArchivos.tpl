<link type="text/css" rel="stylesheet" href="{$_Parametros.ruta_Css}MaterialSiace.css" />
<section class="style-default-light">&nbsp;

 <form id="formAjax3" class="form form-validation" role="form" novalidate="novalidate" >
    <div class="row">
        <div class="col-sm-12" >
            <div class="card">
                <div class="card-body">
                    <div class="col-sm-12 ">

                            <div class="form-group" >
                                <select id="ind_dependencia_subir_archivo" class="form-control " name="ind_dependencia_subir_archivo" required aria-required="true" aria-invalid="true" data-msg-required="Seleccione la dependencia" >
                                    <option value="I" selected="selected">Seleccione la dependecia...</option>
                                    {foreach item=indice from=$datosDependenciasArchivo}
                                        <option value="{$indice.pk_num_dependencia }" >{$indice.ind_dependencia}</option>
                                    {/foreach}
                                </select>
                                <label for="ind_dependencia_subir_archivo">Dependencia</label>
                            </div>
                    </div>
                    <!-- ********************************************* -->
                    <div class="col-sm-9" >
                        <div class="form-group" >
                            <select disabled="disabled" id="carpeta_subir_archivo" class="form-control " name="carpeta_subir_archivo" required aria-required="true" aria-invalid="true" data-msg-required="Seleccione la dependencia" >
                                <option value='J' >Seleccione la carpeta...</option>
                            </select>
                            <label for="carpeta_subir_archivo">Carpetas en la dependencia</label>
                        </div>
                    </div>
                    <div class="col-sm-3" style="padding-top: 15px !important;" >
                        {if in_array('IN-01-04-01-03-N', $_Parametros.perfil) || in_array('IN-01-04-01-08-AD', $_Parametros.perfil) }
                        <button title="Nueva carpeta" id="btn_nueva_carpeta_subir_arch" type="button" disabled="disabled" class="btn ink-reaction btn-floating-action btn-sm btn-primary" data-target="#formModal3" data-toggle="modal" data-keyboard="false" data-backdrop="static"   ><i class="md md-folder-open"></i></button>
                        {/if}
                    </div>
                    <!-- ********************************************* -->
                    <div class="col-sm-12">
                        <div style="padding: 20px;">
                            <div class="form-group"  >
                                <div class="card" >
                                    <div id="cabecera_subir_archivo" class="card-head style-primary">
                                        <div class="card-head">
                                            <header><i class="md-file-upload "></i> SUBIR DOCUMENTOS</header>&nbsp;&nbsp;<i class="md-keyboard-arrow-right"></i><spnan id="ruta_subida_dependencia" style="text-transform: uppercase; "></spnan><span id="ruta_subida_carpeta" style="text-transform: uppercase; "></span>
                                        </div>
                                    </div>
                                    <div id="cuerpo_card_dropzone"  class="card-body  " >
                                        <div id="dropzone" class="dropzone no-padding dz-clickable dz-started style-primary-bright">
                                            <div align="center">
                                                <h3 class="text-primary">Soltar el archivo o haga clic en esta área para guardarlo.</h3>
                                                <em>Los <span style="font-weight: bold !important;">archivos</span> seran cargados en la <span style="font-weight: bold !important;">Dependencia</span> y <span style="font-weight: bold !important;">Carpeta</span> especificada anteriormente</em>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <em class="text-caption"></em>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12" align="center">
                        <button id="botonCerrar" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal" type="button"><span class=""></span> cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
 </form>

</section>
<style>
    .dz-default.dz-message {
        display: none !important;
    }
</style>
<script>

function validarNombreCarpeta(evt){
    var charCode = evt.which || evt.keyCode;
    if (charCode==8||charCode==13||charCode==0 || charCode==37 || charCode==39)//37 izq 39 der
    {
        return true;
    }
    var str = String.fromCharCode(charCode);
    if (!/[0-9\A-Z\a-z ]/.test(str))
    {
       return false;
    }
    else
    {
        return true;
    }
}

$("#ind_dependencia_subir_archivo").select2();
$("#carpeta_subir_archivo").select2();
var pk_archivo_subir = '';

$(document).ready(function() {

    //*************************************
    //* DROPZONE
    //*************************************
    //Cambio en el php.ini para subir los archivos
    //post_max_size: tamaño máximo de datos enviados por POST 100MB.
    //upload_max_filesize: tamaño máximo para subir archivos.100MB
    //max_execution_time: tiempo máximo de ejecución de cada script en segundos. 1000
    //max_input_time: tiempo máximo para analizar la petición de datos.1000
    var archivos_permitidos = '.jpeg, .jpg, .png, .gif, .JPEG, .JPG, .PNG, .GIF, .bmp, .BMP, .pdf, .PDF,' +
            ' .DOCX, .docx, .doc, .DOC, .txt, .TXT, .XLS, .xls, .XLSX, .xlsx, .PPT, .ppt, .PPTX, .pptx,' +
            ' .odt, .ODT, .ODS, .ods, .odp, .ODP' +
            ' .MP3, .mp3, .wma, .WMA, .flv, .FLV,' +
            ' .psd, .PSD,  .cdr, .CDR, .XCF, .xcf' +
            ' .zip, .ZIP, .rar, .RAR,' +
            ' .phatch, .PHATCH, .ttf, .TTF, .sql, .SQL';
    var archivos_permitidos_array = ['jpeg','jpg','png','gif','JPEG','JPG','PNG','GIF','bmp', 'BMP','pdf','PDF','DOCX',
        'docx','doc','DOC','txt','TXT', 'XLS', 'xls', 'XLSX', 'xlsx', 'PPT','ppt','PPTX','pptx','odt','ODT','ODS','ods',
        'odp','ODP','MP3','mp3','wma','WMA','flv','FLV','psd','PSD','cdr','CDR','XCF','xcf','zip','ZIP','rar','RAR',
        'phatch', 'PHATCH', 'ttf','TTF', 'SQL', 'sql'];

    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone("div#dropzone", {

        url: "{$_Parametros.url}modIN/documentosCONTROL/SubirArchivosMET",
        dictDefaultMessage:'',
        addRemoveLinks: true,
        dictCancelUpload: "Subiendo...",
        dictCancelUploadConfirmation: "Está seguro que deseas cancelar la subida del archivo?",
        maxFileSize: 1000,
        dictResponseError: "Ha ocurrido un error en el servidor...",
        autoProcessQueue: true,
        dictRemoveFile:'Eliminar',
        accept: function (file, done) { //se añade el archivo para la subida

            var fileName = file.name;
            var numeroPuntos = 2;
            //****************************************
            var aux = fileName.split('.');

            if ( aux.length > 2 || fileName.indexOf(',') > -1 || fileName.indexOf('"') > -1 || fileName.indexOf("'") > -1 || fileName.indexOf('(') > -1 || fileName.indexOf(')') > -1 || fileName.indexOf('}') > -1 )
            {
                numeroPuntos = 1;
            }
            //*****************************************
            if(numeroPuntos == 2)
            {
                var fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
                var sw_extension = 0;
                for (var i = 0; i < archivos_permitidos_array.length; i++) {
                    if (fileExtension == archivos_permitidos_array[i]) {
                        sw_extension = 1;
                        break;
                    }
                }

                if (sw_extension == 1) {
                    var sw = 0;
                    var carpeta_subir_archivo = $('#carpeta_subir_archivo').val();
                    var ind_dependencia_subir_archivo = $('select[id="ind_dependencia_subir_archivo"]').val();


                    if (ind_dependencia_subir_archivo == 'I') {

                        swal("Campo Requerido", "Por favor suministre la Dependencia", "warning", function () {
                            done('x');
                        });

                        sw = 1;
                    }
                    else {
                        if (carpeta_subir_archivo == 'J') {
                            swal("Campo Requerido", "Por favor suministre la carpeta", "warning", function () {
                                done('x');
                            });
                            sw = 1;
                        }

                    }

                    if (sw == 0)
                    {
                        done();
                        setTimeout(function () {
                            construirMenuDependencias();
                        }, 500);
                        return file.previewElement.classList.add("dz-success");

                    }
                    else
                    {
                        var element;
                        (element = file.previewElement) != null ?
                                element.parentNode.removeChild(file.previewElement) : false; //elimina la vista previa
                    }
                }
                else {
                    swal("Tipo de Documento no permitido", "Formatos permitidos: " + archivos_permitidos, "warning", function () {
                        done('x');
                    });
                    var element;
                    (element = file.previewElement) != null ?
                            element.parentNode.removeChild(file.previewElement) : false; //elimina la vista previa
                }
            }
            else
            {
                swal("Nombre de Archivo no válido", "Renombre el archivo, sin caracteres especiales y verifique la extensión del archivo", "warning", function () {
                    done('x');
                });
                var element;
                (element = file.previewElement) != null ?
                        element.parentNode.removeChild(file.previewElement) : false; //elimina la vista previa
            }

        },
        removedfile: function(file, serverFileName)
        {

             swal({
             title: '¿Eliminar Documento Guardado?',
             text: 'Se eliminará: ' + file.name + ' de la Dependencia y Carpeta especificada',
             type: "warning",
             showCancelButton: true,
             confirmButtonColor: "#DD6B55",
             confirmButtonText: 'Sí, Eliminar',
             cancelButtonText: "Cancelar",
             closeOnConfirm: false
             }, function () {

                 var linkEliminar = $(file.previewElement).children('a');
                 var carpeta_eliminar = $(linkEliminar).attr('carpeta_eliminar');
                 var pk_archivo_eliminar = $(linkEliminar).attr('pk_archivo_eliminar');
                 var pk_carpeta_eliminar = $(linkEliminar).attr('pk_carpeta_eliminar');

                 $.ajax({
                 type: "POST",
                 url: "{$_Parametros.url}modIN/documentosCONTROL/EliminarDocumentoMET",
                 data: { nombre_archivo_eliminar: file.name, pk_num_documentos: pk_archivo_eliminar, carpeta_eliminar: carpeta_eliminar, pk_carpeta_eliminar: pk_carpeta_eliminar },
                 success: function (data) {

                         if (data == 1) {
                         var element;
                         (element = file.previewElement) != null ?
                         element.parentNode.removeChild(file.previewElement) :
                         false;
                         swal("Documento eliminado", "Archivo eliminado exitosamente", "success");
                         }
                         else
                         {
                             swal("Error: ", "Error al eliminar el archivo: "+data, "error");
                         }
                     }
                 });

             });
        },
        success: function(file, serverFileName) {

            if(serverFileName > 0)
            {
                pk_archivo_subir = serverFileName;

                if (file.previewElement) {

                    var linkEliminar = $(file.previewElement).children('a');
                    $(linkEliminar).attr('id_eliminar', 'link_eliminar_'+pk_archivo_subir);
                    var carpeta_eliminar = $('select[id="carpeta_subir_archivo"] option:selected').html();
                    $(linkEliminar).attr('carpeta_eliminar', carpeta_eliminar);
                    var pk_carpeta_eliminar = $('select[id="carpeta_subir_archivo"] option:selected').val();
                    $(linkEliminar).attr('pk_carpeta_eliminar', pk_carpeta_eliminar);
                    $(linkEliminar).attr('pk_archivo_eliminar', pk_archivo_subir);

                    swal("Archivo Guardado", "Archivo guardado exitosamente en la Dependencia y Carpeta especificada", "success");
                    return file.previewElement.classList.add("dz-success");


                }

            }
            else
            {
                if(serverFileName == 0)
                {
                    swal("El nombre ya existe!!!", "Renombre el archivo y vuelva a intentarlo...", "warning");
                    var element;
                    (element = file.previewElement) != null ?
                            element.parentNode.removeChild(file.previewElement) : false;

                }
                else
                {
                    swal("ERROR AL GUARDAR EL ARCHIVO!!!", "ERROR: "+serverFileName, "warning");
                    var element;
                    (element = file.previewElement) != null ?
                            element.parentNode.removeChild(file.previewElement) : false;
                }
            }

        },
        addedfile: function(file) {

            var node, removeFileEvent, removeLink, _i, _j, _k, _len, _len1, _len2, _ref, _ref1, _ref2, _results;
            if (this.element === this.previewsContainer) {
                this.element.classList.add("dz-started");
            }
            if (this.previewsContainer) {
                file.previewElement = Dropzone.createElement(this.options.previewTemplate.trim());
                file.previewTemplate = file.previewElement;
                this.previewsContainer.appendChild(file.previewElement);
                _ref = file.previewElement.querySelectorAll("[data-dz-name]");
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    node = _ref[_i];
                    node.textContent = file.name;
                }
                _ref1 = file.previewElement.querySelectorAll("[data-dz-size]");
                for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
                    node = _ref1[_j];
                    node.innerHTML = this.filesize(file.size);
                }
                if (this.options.addRemoveLinks) {
                    file._removeLink = Dropzone.createElement("<a id_eliminar=\" \" carpeta_eliminar=\" \" pk_archivo_eliminar = \" \" pk_carpeta_eliminar=\" \"  class=\"dz-remove\" href=\"javascript:undefined;\" data-dz-remove>" + this.options.dictRemoveFile + "</a>");
                    file.previewElement.appendChild(file._removeLink);

                }
                removeFileEvent = (function(_this) {
                    return function(e) {
                        e.preventDefault();
                        e.stopPropagation();
                        if (file.status === Dropzone.UPLOADING)
                        {
                            swal({
                                title: '¿Está seguro que deseas cancelar la subida del archivo?',
                                text: 'No se guardará el archivo',
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: 'Sí, Cancelar',
                                cancelButtonText: "Cancelar",
                                closeOnConfirm: false
                            }, function () {
                                var element;
                                (element = file.previewElement) != null ?
                                        element.parentNode.removeChild(file.previewElement) :
                                        false;
                                swal("!Subida cancelada¡", "No se guardó el archivo", "success");
                                return;

                            });
                            /*return Dropzone.confirm(_this.options.dictCancelUploadConfirmation, function() {
                                var element;
                                (element = file.previewElement) != null ?
                                        element.parentNode.removeChild(file.previewElement) :
                                        false;//return _this.removeFile(file);
                            });*/
                        }
                        else
                        {
                            if (_this.options.dictRemoveFileConfirmation) {
                                return Dropzone.confirm(_this.options.dictRemoveFileConfirmation, function() {
                                    return _this.removeFile(file);
                                });
                            } else {
                                return _this.removeFile(file);
                            }
                        }
                    };
                })(this);
                _ref2 = file.previewElement.querySelectorAll("[data-dz-remove]");
                _results = [];
                for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
                    removeLink = _ref2[_k];
                    _results.push(removeLink.addEventListener("click", removeFileEvent));
                }
                return _results;
            }
        }
    });


    myDropzone.on("sending", function(file, xhr, formData) {

        var ind_dependencia_subir_archivo = $('select[id="ind_dependencia_subir_archivo"]').val();
        var carpeta_subir_archivo = $('select[id="carpeta_subir_archivo"]').val();
        var nombre_carpeta_subir_archivo = $('select[id="carpeta_subir_archivo"] option:selected').html();


        formData.append("ind_dependencia_subir_archivo", ind_dependencia_subir_archivo);
        formData.append("carpeta_subir_archivo", carpeta_subir_archivo);
        formData.append("nombre_carpeta_subir_archivo", nombre_carpeta_subir_archivo);

    });

    //***************************************************
    //  ACTIVAR CAMPOS
    //****************************************************
    $('#ind_dependencia_subir_archivo').change(function() {



        var dependencia = $('select[id="ind_dependencia_subir_archivo"]').val();
        if( dependencia !='I')
        {
            var urlbuscarCarpetas = '{$_Parametros.url}modIN/documentosCONTROL/BuscarCarpetasSubirArchivosMET';
            $.post(urlbuscarCarpetas, { dependencia: dependencia }, function (resp) {

                if(resp != 0)//hay carpetas para la dependencia
                {
                    $('select[id="carpeta_subir_archivo"]').html(resp);
                    $('select[id="carpeta_subir_archivo"]').change();
                    $('select[id="carpeta_subir_archivo"]').prop('disabled', false);


                }
                else
                {
                    $('select[id="carpeta_subir_archivo"]').html(resp);
                    $('select[id="carpeta_subir_archivo"]').change();
                    $('select[id="carpeta_subir_archivo"]').prop('disabled', true);
                }


                var texto = $('select[id="ind_dependencia_subir_archivo"] option:selected').html();
                $('#ruta_subida_dependencia').html(texto+'&nbsp;<i class="md-keyboard-arrow-right">');

                $("#btn_nueva_carpeta_subir_arch").prop('disabled', false);

            });
        }
        else
        {
            $('select[id="carpeta_subir_archivo"]').html("<option value='N' >Seleccione la carpeta...</option>");
            $('select[id="carpeta_subir_archivo"]').change();
            $('select[id="carpeta_subir_archivo"]').prop('disabled', true);

            $('#ruta_subida_dependencia').html('');
            $("#btn_nueva_carpeta_subir_arch").prop('disabled', true)

        }

    });

    $('#carpeta_subir_archivo').change(function() {

        var texto = $('select[id="carpeta_subir_archivo"] option:selected').html();
        $('#ruta_subida_carpeta').html(texto);

    });

    $('#botonCerrar').click(function(){
        $(document.getElementById('cerrarModal2')).click();
        $('#formModalLabel2').html("");
        $('#ContenidoModal2').html("");
    });

    $("#btn_nueva_carpeta_subir_arch").click(function() {

        $('#modalAncho3').css("width", "40%");
        $('#formModalLabel3').html('');
        $('#ContenidoModal3').html('');

        $('#formModalLabel3').html('<i class="md-mode-edit"></i> Nueva carpeta');

        var pk_num_dependencia = $('select[id="ind_dependencia_subir_archivo"]').val();
        var ind_dependencia = $('select[id="ind_dependencia_subir_archivo"] option:selected').html();
        var urlNuevaCarpeta = '{$_Parametros.url}modIN/documentosCONTROL/NuevaCarpetaMET';

        $.post(urlNuevaCarpeta, {
            pk_num_dependencia: pk_num_dependencia,
            ind_dependencia: ind_dependencia,
            tipo_form: 'S'
        }, function (resp) {
            $('#ContenidoModal3').html(resp);

        });
    });

});


</script>


