<section class="style-default-light">&nbsp;

<!--TITULO DEL FORMULARIO-->
<div class="row">
	<div class="col-lg-12">
			{if $pk_num_dependencia != 'T' }
			<div class="table-responsive">
			<table id="datatable2" class="table table-striped table-hover" WIDTH="90%" >
			<thead>
				<tr class="info" style="background-color:#D9EDF7 !important; font-weight: bold !important;" >
					<td colspan="6" align="center">CARPETAS EN {$nombre_dependencia}</td>
				</tr>
				<tr  align="center">
                    <th ><i class=""></i>N°</th>
					<th ><i class=""></i>Nombre carpeta</th>
					<th ><i class=""></i>Fecha creación</th>
					<th ><i class=""></i>Elementos</th>
                    <th ><i class=""></i>Renombrar</th>
                    <th ><i class=""></i>Eliminar</th>
	            </tr>
			</thead>
			<tbody >
					{$i=0}
					{foreach name=for_datosCarpetas item=indice from=$datosCarpetas}
					<tr>
						<td>{$smarty.foreach.for_datosCarpetas.iteration}</td>
						<td id="{$indice.pk_num_carpetas_documentos}" >{$indice.ind_descripcion_format}</td>
						<td>{$indice.fec_creacion_carpeta}</td>
						<td>{$numElementos[$i++]}</td>
						<td>
						{if in_array('IN-01-04-01-02-M', $_Parametros.perfil) || in_array('IN-01-04-01-08-AD', $_Parametros.perfil) }
						<button id="btn_{$indice.pk_num_carpetas_documentos}" clave_dependencia="{$clave_dependencia}" titulo="{$indice.ind_descripcion_carpeta}" type="button"  pk_num_carpetas_documentos = "{$indice.pk_num_carpetas_documentos}" class="renombrar_carpeta gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal3" data-toggle="modal" data-keyboard="false" data-backdrop="static" ><i class="md-mode-edit"></i></button>
						{else}
							<i class="md-not-interested"></i>
						{/if}
						</td>
						<td>
						{if in_array('IN-01-04-01-01-E', $_Parametros.perfil) || in_array('IN-01-04-01-08-AD', $_Parametros.perfil) }
							<button id="btn_e_{$indice.pk_num_carpetas_documentos}" titulo="{$indice.ind_descripcion_carpeta}" type="button"  pk_num_carpetas_documentos = "{$indice.pk_num_carpetas_documentos}" class="eliinar_carpeta gsUsuario btn ink-reaction btn-raised btn-xs btn-danger" ><i class="md md-delete"></i></button>
						{else}
							<i class="md-not-interested"></i>
						{/if}
						</td>
					</tr>
					{/foreach}
			</tbody>
			</table>
			</div>
			{else}
				<div class="table-responsive">
				<table id="datatable2" class="table table-striped table-hover" WIDTH="90%" >
					<thead>
					<tr class="info" style="background-color:#D9EDF7 !important; font-weight: bold !important;" >
						<td colspan="7" align="center">CARPETAS POR DEPENDENCIA</td>
					</tr>
					<tr  align="center">
						<th ><i class=""></i>N°</th>
						<th ><i class=""></i>Dependencia</th>
						<th ><i class=""></i>Nombre carpeta</th>
						<th ><i class=""></i>Fecha creación</th>
						<th ><i class=""></i>Elementos</th>
						<th ><i class=""></i>Renombrar</th>
						<th ><i class=""></i>Eliminar</th>
					</tr>
					</thead>
					<tbody >
					{$i=0} {$j=0}
					{foreach name=for_datosCarpetas item=indice from=$datosCarpetas}
						<tr>
							<td>{$smarty.foreach.for_datosCarpetas.iteration}</td>
							<td>{$indice.fk_a004_dependencia}</td>
							<td id="{$indice.pk_num_carpetas_documentos}">{$indice.ind_descripcion_format}</td>
							<td>{$indice.fec_creacion_carpeta}</td>
							<td>{$numElementos[$i++]}</td>
							<td><button id="btn_{$indice.pk_num_carpetas_documentos}"  clave_dependencia="{$clave_dependencias[$j++]}"  titulo="{$indice.ind_descripcion_carpeta}" type="button"  pk_num_carpetas_documentos = "{$indice.pk_num_carpetas_documentos}" class="renombrar_carpeta gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal3" data-toggle="modal" data-keyboard="false" data-backdrop="static" ><i class="md-mode-edit"></i></button></td>
							<td><button id="btn_e_{$indice.pk_num_carpetas_documentos}" titulo="{$indice.ind_descripcion_carpeta}" type="button"  pk_num_carpetas_documentos = "{$indice.pk_num_carpetas_documentos}" class="eliinar_carpeta gsUsuario btn ink-reaction btn-raised btn-xs btn-danger" ><i class="md md-delete"></i></button></td>
						</tr>

					{/foreach}
					</tbody>
				</table>
				</div>
			{/if}
	</div>
</div>
<div class="row">
	<div class="col-sm-12" align="center">
		<button id="botonCerrar" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal" type="button"><span class=""></span> Cerrar</button>
	</div>
</div>
</section>

<script>

//*************************************************************
//*
//*************************************************************
$(document).ready(function() {

	//**********************************
	//*
	//**********************************
	$('#datatable2').DataTable({ //ley
		"dom": 'lCfrtip',
		"order": [],
		"colVis": {
			"buttonText": "Columnas",
			"overlayFade": 0,
			"align": "right"
		},
		"language": {
			"lengthMenu": 'Mostrar _MENU_',
			"search": '<i class="fa fa-search"></i>',
			"paginate": {
				"previous": '<i class="fa fa-angle-left"></i>',
				"next": '<i class="fa fa-angle-right"></i>'
			}
		}
	});
	//************************************
	//*
	//**************************************
	$('#datatable2 tbody').on('click','.renombrar_carpeta', function(){

		$('#modalAncho3').css("width", "40%");
		$('#formModalLabel3').html('');
		$('#ContenidoModal3').html('');
		var titulo =  $(this).attr('titulo');
		$('#formModalLabel3').html('<i class="md-mode-edit"></i> Renombrar la carpeta '+titulo.replace(/_/g,' '));

		var pk_num_carpetas_documentos = $(this).attr('pk_num_carpetas_documentos');
		var clave_dependencia = $(this).attr('clave_dependencia');
		var urlRenombrar = '{$_Parametros.url}modIN/documentosCONTROL/RenombrarCarpetaMET';
		$.post(urlRenombrar, { pk_num_carpetas_documentos: pk_num_carpetas_documentos, titulo: titulo, clave_dependencia: clave_dependencia }, function (resp) {
			$('#ContenidoModal3').html(resp);
		});

	});
	//************************************
	//* ELIMINAR CARPETA
	//**************************************
	$('#datatable2 tbody').on('click','.eliinar_carpeta', function(){

		var titulo =  $(this).attr('titulo');
		var fila = $(this);

		var urlListadoContenido = '{$_Parametros.url}modIN/documentosCONTROL/BuscarDocumentosMET';
		var pk_num_carpetas_documentos =  $(this).attr('pk_num_carpetas_documentos');
		$.post(urlListadoContenido, { pk_num_carpetas_documentos: pk_num_carpetas_documentos }, function(listado){

				var urEliminarCarpeta = '{$_Parametros.url}modIN/documentosCONTROL/EliminarCarpetaMET';

				if(listado == 0)
				{
					swal({
						title:'¿Eliminar Carpeta?',
						text: 'Se eliminará la Carpeta '+titulo,
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: 'Sí, Eliminar',
						cancelButtonText: "Cancelar",
						closeOnConfirm: false
					}, function(){
						$.post(urEliminarCarpeta, { pk_num_carpetas_documentos: pk_num_carpetas_documentos, nombre_carpeta: titulo }, function(resp){
							if(resp > 0)
							{
								var tabla_editar_menu = $('#datatable2').DataTable();
								tabla_editar_menu.row( fila.parents('tr') )
										.remove()
										.draw();
								swal("Carpeta Eliminada", "Carpeta Eliminada Exitosamente", "success");
								construirMenuDependencias();
							}
							else
							{
								alert(resp);
							}

						});

					});
				}
				else
				{
					var listado_html = '<div class="card" style="height: 200px; overflow-y: scroll !important; overflow-x: hidden !important; "><div class="card-body style-default-bright" aling="center" ><ul style="padding: 0px !important; " class="list divider-full-bleed">';
					for(var i=0; i< listado.length; i++)
					{
						listado_html = listado_html+'<li class="tile"><a class="tile-content ink-reaction "><div class="tile-icon"><i class=" md-report-problem "></i></div><div class="" ><p class="text-sm " style="font-weight: bold" ><span class="text-danger" style="padding: 5px; " >'+listado[i].ind_descripcion.replace(/_/g,' ')+'.'+listado[i].ind_formato+'</span></p></div></a></li>';
					}
					listado_html = listado_html+'</ul></div></div>';

					swal({
						title: "Contenido de la Carpeta a Eliminar",
						text: listado_html,
						html: true,
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Sí, Eliminar",
						cancelButtonText: "Cancelar",
						closeOnConfirm: false

					}, function(){


						swal({
							title: "¿Está seguro que desea Eliminar la Carpeta y su Contenido?",
							text: 'La Carpeta '+titulo+' y su contenido se eliminaran permanentemente',
							html: true,
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "Sí, Eliminar",
							cancelButtonText: "Cancelar",
							closeOnConfirm: false

						},function() {

							$.post(urEliminarCarpeta, {
									pk_num_carpetas_documentos: pk_num_carpetas_documentos,
									nombre_carpeta: titulo
								}, function (resp) {
									if (resp > 0) {
										var tabla_editar_menu = $('#datatable2').DataTable();
										tabla_editar_menu.row(fila.parents('tr'))
												.remove()
												.draw();
										swal("Carpeta eliminada", "La Carpeta y su contenido se eliminaron exitosamente", "success");
										construirMenuDependencias();
									}
									else {
										alert(resp);
									}

								});
						});


					});

				}

		}, 'json');
	});

});

</script>