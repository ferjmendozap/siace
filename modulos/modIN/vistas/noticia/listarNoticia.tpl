<section class="style-default-light">

<!--TITULO DEL FORMULARIO-->
<div class="row">
	<div class="col-lg-12">
		<div class="card-head"><h2 class="text-primary">&nbsp;Listar Noticias</h2></div>
	</div>
</div>&nbsp;
<div class="row">
	
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body ">
				<form id="formAjax2" class="form form-validation" role="form" >
					<div class="col-lg-6">
						<div class="form-group" >
							<div class="input-group">
								<div class="input-group-addon">
									<div class="checkbox checkbox-inline checkbox-styled">
										<label>
                                        	<input class="caja1" checked id="check_1" type="checkbox">
											<span></span>
                                        </label>
									</div>
								</div>
								<div class="input-group-content">
                                    <select id="ind_descripcion_empresa_listar" class="form-control input-sm" name="ind_descripcion_empresa_listar"  >
                                        <option value="{$datosOrganismo[0]['pk_num_organismo'] }" selected="selected">{$datosOrganismo[0]['ind_descripcion_empresa'] }</option>
                                        <option value="{$datosOrganismo[1]['pk_num_organismo'] }" >{$datosOrganismo[1]['ind_descripcion_empresa'] }</option>
                                        <option value="t" >TODAS</option>
                                    </select>
                                    <label for="ind_descripcion_empresa_listar">Fuente</label>
                                </div>
							</div>
						</div>	
					</div>
					<div class="col-lg-6">
						<div class="form-group" >
							<div class="input-group">
								<div class="input-group-addon">
									<div class="checkbox checkbox-inline checkbox-styled">
										<label>
                                        	<input class="caja2" id="check_2" type="checkbox">
											<span></span>
                                        </label>
									</div>
								</div>
								<div class="input-group-content">
									<div id="demo-date-rangeN" class="input-daterange input-group">
                                        <div class="input-group-content">
                                            <input disabled id="start_fecha_prepa" type="text" class="form-control" name="start_fecha_prepa"  aria-required="true" aria-invalid="true" required value="" >
                                            <label>Fecha de registro:</label>
                                        </div>
                                        <span class="input-group-addon">a</span>
                                        <div class="input-group-content">
                                            <input disabled  id="end_fecha_prepa" class="form-control" type="text" name="end_fecha_prepa"  aria-required="true" aria-invalid="true" required="" value="" >
                                            <div class="form-control-line"></div>
                                        </div>
                                    </div>
								</div>
							</div>	
						</div>
					</div>
					<div class="col-lg-12" >
						<div align="center">
			    			<button type="submit" id="botonRegistrar" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" >Buscar</button>
            			</div>	
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
<div class="row">
	<div class="col-lg-12">
		<div class="table-responsive">
		<table id="datatable1" class="table table-striped table-hover  " WIDTH="90%" >
			<thead>
				<tr  align="center">
                    <th ><i class=""></i>N°</th>
					<th ><i class=""></i>Imagen</th>{* md-insert-photo*}
					<th ><i class=""></i>Título</th>
					<th width="20%" ><i class=""></i>Descripción</th>
                    <th ><i class=""></i>Fecha</th>{*md-today*}
                    <th ><i class=""></i>Hora</th>{*md-query-builder*}
                    <th ><i class=""></i>Fuente</th>
					<th ><i class=""></i>Publicado</th>{*md-remove-red-eye*}
					<th ><i class=""></i>Editar</th>{*md-border-color*}
					<th ><i class=""></i>Ver</th>{*md-local-library*}					
	            </tr>
			</thead>
			{if $datosNoticias == false}
			<tbody >
			<tr class="fila_agregar" >
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			</tbody>
			{else}
			<tbody >
				{foreach name=for_noticias item=indice from=$datosNoticias}
				<tr class="fila_agregar" >
					<td>{$smarty.foreach.for_noticias.iteration}</td>
					<td ><img class="img-circle width-0 ampliar_foto imagenNoticia" src="{$_Parametros.ruta_Img}modIN/noticias/{$indice.ind_ruta_img}" alt="" style="cursor: pointer" title="Ampliar Foto" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" ind_foto="{$indice.ind_ruta_img}" titulo="{$indice.ind_titulo}"></td>
					<td>{$indice.ind_titulo}</td>
					<td>{$indice.ind_descripcion}</td>
					<td>{$indice.fec_registro}</td>
					<td>{$indice.hora_regisro}</td>
					<td>{$indice.ind_descripcion_empresa}</td>
					{if $indice.ind_status == 'm'}
					<td><i class="md-done"></i></td>
					{else}
					<td></td>
					{/if}
					{if in_array('IN-01-01-02-01-M', $_Parametros.perfil) }
					<td><button  type="button"  pk_num_noticia = "{$indice.pk_num_noticia}" class="editar gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal" data-toggle="modal" data-keyboard="false" data-backdrop="static"><i class="fa fa-edit"></i></button></td>
					{else}
					<td><i class="md-not-interested "></i></td>
					{/if}
					<td><button titulo="{$indice.ind_titulo}" type="button"  pk_num_noticia = "{$indice.pk_num_noticia}" class="ver gsUsuario btn ink-reaction btn-raised btn-xs btn-info" data-target="#formModal" data-toggle="modal" data-keyboard="false" data-backdrop="static" ><i class="md md-search"></i></button></td>
				</tr>
				{/foreach}
			</tbody>
			{/if}
		</table>
		</div>
	</div>
</div>

</section>

<script>
//*************************************************************
//*  FORMATEAR LA FECHA 
//*************************************************************
$('#demo-date-rangeN').datepicker({ {*Para el correcto funcionamiento deben ir antes del ready*}
	todayHighlight: true,
	format:'dd-mm-yyyy',
	autoclose: true,
	language:'es'
});

//*************************************************************
//*
//*************************************************************
$(document).ready(function() {
//**************************************************************
//*   VER IMAGEN DE LA NOTICIA
//**************************************************************
 $('#datatable1 tbody').on( 'click', '.imagenNoticia', function () {

	var titulo2 = '';
	if($(this).attr('titulo') != 'null')
	{
		titulo2 = $(this).attr('titulo');
	}

	$('#modalAncho').css("width", "60%");
    $('#formModalLabel').html('');
    $('#formModalLabel').html(titulo2);
    var srcImagen = $(this).attr('src');
    var imagenNoticia = '<div align="center"><img width="80%" src="'+srcImagen+'"></div>';    
    $('#ContenidoModal').html('');
    $('#ContenidoModal').html(imagenNoticia);
 });
//**************************************************************
//*   ACTIVAR DESACTIVAR EL RANGO DE FECHAS Y ORGANISMOS
//**************************************************************
$(document).on('change','.caja1', function(){

        if (this.checked){
            $('select[id="ind_descripcion_empresa_listar"]').prop('disabled', false); 
            $('input[id="ind_descripcion_empresa_listar"]').focus();
        }else{
            $('select[id="ind_descripcion_empresa_listar"]').prop('disabled', true);

        }
    });

$(document).on('change','.caja2', function(){

        if (this.checked){
            $('input[id="start_fecha_prepa"]').prop('disabled', false);
            $('input[id="start_fecha_prepa"]').val('');
            $('input[id="end_fecha_prepa"]').prop('disabled', false);
            $('input[id="end_fecha_prepa"]').val('');
            $('input[id="start_fecha_prepa"]').focus();
        }else{
            $('input[id="start_fecha_prepa"]').prop('disabled', true);
            $('input[id="start_fecha_prepa"]').val('');
            $('input[id="end_fecha_prepa"]').prop('disabled', true);
            $('input[id="end_fecha_prepa"]').val('');


        }
});
//**************************************************************
//*   LLAMAR A LA VISTA DE EDITAR Y VISUALIZAR
//**************************************************************
$('#datatable1 tbody').on('click','.editar', function(){

            var pk_num_noticia = $(this).attr('pk_num_noticia');

                var urlEditar = '{$_Parametros.url}modIN/noticiaCONTROL/EditarNoticiaMET';
                $('#modalAncho').css("width", "75%");
                $('#formModalLabel').html("");
                $('#formModalLabel').html('Editar noticia');
                $('#ContenidoModal').html("");

                $.post(urlEditar, { pk_num_noticia: pk_num_noticia }, function(dato){
                    $('#ContenidoModal').html(dato);
            });

	});

$('#datatable1 tbody').on('click','.ver', function(){


	var titulo2 = '';
	if($(this).attr('titulo') != 'null')
	{
		titulo2 = $(this).attr('titulo');
	}

				var pk_num_noticia = $(this).attr('pk_num_noticia');

                var urVisualizar = '{$_Parametros.url}modIN/noticiaCONTROL/VisualizarNoticiaMET';
                $('#modalAncho').css("width", "75%");
                $('#formModalLabel').html("");
                $('#formModalLabel').html(titulo2);
                $('#ContenidoModal').html("");
                $.post(urVisualizar, { pk_num_noticia: pk_num_noticia }, function(dato){
                    
                    $('#ContenidoModal').html(dato);
            });

});
//**************************************************************
//*   FILTRO Y ACTUALIZACIÓN DE LA LISTA
//**************************************************************
 var tabla_listado = $('#datatable1').DataTable();
 $("#formAjax2").validate({
    
    submitHandler: function(form) {

    	var url_listar='{$_Parametros.url}modIN/noticiaCONTROL/BuscarNoticiasFiltroMET';
		$.post(url_listar,$(form).serialize(),function(respuesta_post){
			
			//****************************************************
			//*   DATA TABLE
			//****************************************************
			
			if(respuesta_post)
			{ 
				tabla_listado.clear().draw();
				var status = '';
				var contador = 0;	
				var cad_fuente;
				for(var i=0; i<respuesta_post.length; i++)
				{
					if(respuesta_post[i].ind_status == 'm')
						status = '<i class="md-done"></i>';
					else
						status = '';

					if(respuesta_post[i].fk_a001_num_organismo == '0')
						cad_fuente = 'CONTRALORIA GENERAL DE LA REPUBLICA';
					else
						cad_fuente = respuesta_post[i].ind_descripcion_empresa;

					{if in_array('IN-01-01-02-01-M',$_Parametros.perfil)}
					cad_modificar_noticia = '<button  type="button"  pk_num_noticia = "'+respuesta_post[i].pk_num_noticia+'" class="editar gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal" data-toggle="modal" data-keyboard="false" data-backdrop="static" ><i class="fa fa-edit"></i></button>';
					{else}
					cad_modificar_noticia = '<i class="md-not-interested"></i>';
					{/if}

					contador = i+1;

					tabla_listado.row.add([
									contador,
									'<img class="img-circle width-0 ampliar_foto imagenNoticia" src="{$_Parametros.ruta_Img}modIN/noticias/'+respuesta_post[i].ind_ruta_img+'" alt="" style="cursor: pointer" title="Ampliar Foto" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" ind_foto="'+respuesta_post[i].ind_ruta_img+'" titulo="'+respuesta_post[i].ind_titulo+'">',
									respuesta_post[i].ind_titulo,
						            respuesta_post[i].ind_descripcion,
									respuesta_post[i].fec_registro,
									respuesta_post[i].hora_regisro,
									cad_fuente,
									status,
									cad_modificar_noticia,
									'<button titulo="'+respuesta_post[i].ind_titulo+'" type="button"  pk_num_noticia = "'+respuesta_post[i].pk_num_noticia+'" class="ver gsUsuario btn ink-reaction btn-raised btn-xs btn-info" data-target="#formModal" data-toggle="modal" data-keyboard="false" data-backdrop="static" ><i class="md md-search"></i></button>'
									]).draw()
									.nodes()
									.to$()
									.addClass( '' );	
				}	
			}
			else
			{
				swal("No hay resultados!", "Introduzca otro criterio de búsqueda...", "warning");
				tabla_listado.clear().draw();	
			}
			
			//****************************************************
			//*   
			//****************************************************

		
		}, 'json');

    }

 });


});


</script>