<section class="style-default-light">&nbsp;

<!--CUERPO DEL FORMULARIO-->

<div class="row">
        <div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<form id="formAjax" class="form form-validation" role="form"  method="post" >
						<div class="form-group" >
							<input type="text" name="ind_titulo" id="ind_titulo" class="form-control" value="{$datoNoticia.ind_titulo}" onkeypress="return validarDescripcion(event)" >
							<label for="ind_titulo">Título de la Noticia</label>
                            <p class="help-block">Opcional</p>
						</div>
						<div class="form-group" >
							<textarea name="ind_descripcion" id="ind_descripcion" class="form-control" rows="2" placeholder="" onkeypress="return validarDescripcion(event)" >{$datoNoticia.ind_descripcion}</textarea>
							<label for="ind_descripcion">Descripción</label>
                            <p class="help-block">Opcional</p>
						</div>
						<div class="form-group" >
							<textarea id="ckeditor" class="form-control control-12-rows" style="visibility: hidden; display: none;" data-msg-required="Introduzca el cuerpo de la Noticia" aria-required="true" aria-invalid="true" required>{$datoNoticia.txt_cuerpo}</textarea>
                            <p class="help-block">Opcional</p>
                        </div>
						<div class="col-sm-6">
							<div class="row"> 
								<div class="form-group" >
									<select id="txt_fuente" required="" class="form-control" name="txt_fuente" >
										<option value="{$datosOrganismo[0]['pk_num_organismo']}" >{$datosOrganismo[0]['ind_descripcion_empresa'] }</option>
										<option value="{$datosOrganismo[1]['pk_num_organismo']}" >{$datosOrganismo[1]['ind_descripcion_empresa'] }</option>
									 </select>
									 <label for="txt_fuente">Fuente</label>
								</div>
						    </div>
						    <div class="row">
						    	<div class="form-group" >
									<select id="ind_status_editar" required="" class="form-control" name="ind_status_editar" >
										<option value="m" >Publicado</option>
										<option value="n" >No publicado</option>
									 </select>
									 <label for="txt_fuente">Estatus</label>
								</div>
						    </div>

						</div>
						<div class="col-sm-6" align="center">
							<div class="form-group"  >
							       	<div id="fotoCargada">
                                    	<img class="img-thumbnail border-gray border-lg img-responsive auto-width" src="{$_Parametros.ruta_Img}modIN/imagen_noticia.png" alt="Cargar Imagen" title="Cargar Imagen" id="cargarImagen" style="cursor: pointer; "/>
                                    </div>
                                    <input type="file" name="ind_ruta_img" id="ind_ruta_img" style="display: none" />
                                    <input type="hidden" name="ind_ruta_img_f" id="ind_ruta_img_f" value="{$datoNoticia.ind_ruta_img}" >
                            </div>

						</div>
                        <div class="row">&nbsp;</div>
                        <div class="row">&nbsp;</div>
                        <div class="row">

                            <div align="center">
                                <button id="botonCerrar" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal" type="button"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                                <button id="botonGuardar" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" type="submit"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                            </div>
                            <p class="text-xs text-bold text-right">SOLO LA IMAGEN ES OBLIGATORIA</p>
                        </div>
					</form>
				</div>
			</div>
		</div>
                
</div>


<!--FIN DEL TITULO DEL FORMULARIO-->
</section>

<script>

function validarDescripcion(evt){

   var charCode = evt.which || evt.keyCode;
     if (charCode==8||charCode==13||charCode==0 || charCode==37 || charCode==39)//37 izq 39 der
        return true;
   var str = String.fromCharCode(charCode);
     if (!/[0-9\.\A-Z\a-z\á\é\í\ó\ú\ñ\Ñ\š\ü\,\;\(\)\-\ ]/.test(str)) {
        return false;//evt.preventDefault();
   };
}

$(document).ready(function() {

//*********************************************
//* ASIGNACIÓN DE VALORES
//*********************************************
$('select[id="txt_fuente"]').prop('value', '{$datoNoticia.fk_a001_num_organismo}');
$('select[id="txt_fuente"]').change();
$('select[id="ind_status_editar"]').prop('value', '{$datoNoticia.ind_status}');
$('select[id="ind_status_editar"]').change();

$("#cargarImagen").attr("src",'{$_Parametros.ruta_Img}modIN/noticias/{$datoNoticia.ind_ruta_img}');
$("#cargarImagen").css({ 'width':'400px', 'height':'140px' });
var swImagen = 0;

//*********************************************
//*			CARGAR IMAGEN
//*********************************************
$("#cargarImagen").click(function() {
       $("#ind_ruta_img").click();//input file
});

$("#ind_ruta_img").change(function(e) {
				
		var files = e.target.files;
        var file = $("#ind_ruta_img")[0].files[0];
        var fileName = file.name;

        var aux_n = fileName.split('.');
        var aux_n2 = 0;
        if ( aux_n.length > 2 || fileName.indexOf(',') > -1 || fileName.indexOf('"') > -1 || fileName.indexOf("'") > -1 || fileName.indexOf('(') > -1 || fileName.indexOf(')') > -1 || fileName.indexOf('}') > -1 )
        {
            aux_n2 = 1;
        }

        if(aux_n2 == 0)
        {
            $("#ind_ruta_img_f").val(fileName);
            swImagen = 1;
            fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
            if (fileExtension == 'png' || fileExtension == 'PNG' || fileExtension == 'jpg' || fileExtension == 'jpeg' || fileExtension == 'JPEG' || fileExtension == 'JPG') {

                for (var i = 0, f; f = files[i]; i++) {

                    if (!f.type.match('image.*')) {

                        continue;
                    }

                    var reader = new FileReader();
                    reader.onload = (function (theFile) {

                        return function (e) {

                            // Insertamos la imagen en la etiqueta img para dar el
                            // efecto que cargo temporalmente la imagen nueva
                            $("#cargarImagen").attr("src", e.target.result);
                            $("#cargarImagen").css({
                                'width': '400px',
                                'height': '140px'
                            });


                        };

                    })(f);
                    reader.readAsDataURL(f);
                }


            }
            else
            {
                swal("¡Extensión de archivo no válido!", "Asegurese de subir un archivo de tipo imagen: jpg/jpeg/png", "warning");
                $("#ind_ruta_img").val('');
                $("#ind_ruta_img_f").val('');
                $("#cargarImagen").attr("src",'{$_Parametros.ruta_Img}modIN/imagen_noticia.png');
                $("#cargarImagen").css({ 'width':'400px', 'height':'140px' });

            }
        }
        else
        {
            swal("¡Nombre de archivo no válido!", "Renombre el archivo, sin caracteres especiales", "warning");
            $("#ind_ruta_img").val('');
            $("#ind_ruta_img_f").val('');
            $("#cargarImagen").attr("src",'{$_Parametros.ruta_Img}modIN/imagen_noticia.png');
            $("#cargarImagen").css({ 'width':'400px', 'height':'140px' });
        }
    });	

//**********************************************
//* ENVIAR FORMULARIO
//**********************************************

    $("#formAjax").validate({
        submitHandler: function(form) {
            
            var editor = CKEDITOR.instances.ckeditor;
            var txt_cuerpo = editor.getData();
            
                var ind_ruta_img_f = $("#ind_ruta_img_f").val();
                if(ind_ruta_img_f == ""){
                    swal("¡La imagen de la noticia es requerida!", "Cargue una imagen tipo: jpg/jpeg/png", "warning");
                }
                else
                {
                   var url_noticia = '{$_Parametros.url}modIN/noticiaCONTROL/ModificarNoticiaMET';
                   var data = new FormData($("#formAjax")[0]);
                   data.append('ckeditor',txt_cuerpo);
                   data.append('fec_registro_editar','{$datoNoticia.fec_registro}');
                   data.append('hora_regisro_editar','{$datoNoticia.hora_regisro}');
                   data.append('pk_num_noticia_editar','{$datoNoticia.pk_num_noticia}');
                   data.append('swImagen',swImagen);
                        $.ajax({
                                url: url_noticia,
                                data: data,
                                type: 'POST',
                                contentType: false,
                                processData: false,
                                success: function(respuesta_post){

                                    if(respuesta_post == 1)
                                    {
                                        
                                        swal("Noticia registrada", "guardado satisfactoriamente", "success");
                                        $(document.getElementById('cerrarModal')).click();{*se esta cerrando la modal 1*}
                            			$(document.getElementById('ContenidoModal')).html('');
                            			$( "#formAjax2" ).submit();{* reenvio el formulario del listado para que ecargue la lista actualizada*}



                                    }
                                    else
                                    {
                                        if(respuesta_post == 0)
                                        {
                                           swal("¡Ya Existe una imagen con el mismo nombre!", "Renombre la imagen e intente de nuevo", "warning");
                                        }
                                        else
                                        {
                                            swal("Error Al subri la imagen",respuesta_post, "error");
                                        }
                                    }
                                                
                            }
                        });            
                           
                }
            //}

          
        }//FIN DEL submitHandler

    });//FIN DEL validate
//************************************************
//*
//************************************************

});//FIN DEL ready



</script>
