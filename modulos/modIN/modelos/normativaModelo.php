<?php 
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Intranet
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Maikol Isava         | acc.desarrollo@cgesucre.gob.ve       | 0426-1814058
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class normativaModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

    }
	
	 public function metConsultarOrganismos()
    {

        $con = $this->_db->query("
                SELECT * FROM `a001_organismo`
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();//RETORNA FALSE SI NO HAY RESULTADO
    }
	
	public function metUsuario($usuario, $metodo)
    {
       $usuario = $this->_db->query("select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2 from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado=b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona");
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1) {  //para acceder de manera individual: $usuario[0]['ind_apellido1']
            return $usuario->fetch();//una sola fila
        } else {
            return $usuario->fetchAll();
        }

    }

    public function metConsultarDepencias()
    {
        $usuario = $this->_db->query("SELECT * FROM `a004_dependencia`");
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        return $usuario->fetchAll();
    }

    public function metInsertarNormativaLegal($num_gaceta_circular, $ind_tipo_normativa, $fec_gaceta_circular, $txt_descripcion, $ind_tipo_ley_circular,$fk_a004_num_dependencia, $num_resolucion, $fec_resolucion, $ind_ruta_archivo, $fk_a018_num_seguridad_usuario, $fec_ultima_modificacion)
    {

        $this->_db->beginTransaction();
        $NuevaSolicitud=$this->_db->prepare(//el orden identico al de la tabla
            "insert into in_c004_normativa_legal values (
             null,
             :ind_tipo_normativa,
             :ind_gaceta_circular,
             :fec_gaceta_circular,
             :ind_descripcion,
             :ind_tipo_ley_circular,
             :fk_a004_num_dependencia,
             :ind_resolucion,
             :fec_resolucion,
             :ind_ruta_archivo,
             :fk_a018_num_seguridad_usuario,
             :fec_ultima_modificacion)"
        );
        $NuevaSolicitud->execute(array(
            ':ind_tipo_normativa' => $ind_tipo_normativa,
            ':ind_gaceta_circular' => $num_gaceta_circular,
            ':fec_gaceta_circular' => $fec_gaceta_circular,
            ':ind_descripcion' => $txt_descripcion,
            ':ind_tipo_ley_circular' => $ind_tipo_ley_circular,
            ':fk_a004_num_dependencia' => $fk_a004_num_dependencia,
            ':ind_resolucion' => $num_resolucion,
            ':fec_resolucion' => $fec_resolucion,
            ':ind_ruta_archivo' => $ind_ruta_archivo,
            ':fk_a018_num_seguridad_usuario' => $fk_a018_num_seguridad_usuario,
            ':fec_ultima_modificacion' => $fec_ultima_modificacion

        ));

        $pk_num_normativa_legal = $this->_db->lastInsertId();
        $this->_db->commit();
        return $pk_num_normativa_legal;
    }


    public function metListarNormativa($opc)
    {
        $cad_consulta = "
        SELECT
        `pk_num_normativa_legal` ,
         `ind_tipo_normativa` ,
         `ind_gaceta_circular` ,
          DATE_FORMAT(fec_gaceta_circular,'%d-%m-%Y' ) AS fec_gaceta_circular,
         `ind_descripcion` ,
         `ind_tipo_ley_circular` ,
         `fk_a004_num_dependencia` ,
         `ind_resolucion` ,
         DATE_FORMAT(fec_resolucion,'%d-%m-%Y' ) AS fec_resolucion,
         `ind_ruta_archivo` ,
         `fk_a018_num_seguridad_usuario`,
         `fec_ultima_modificacion`
         FROM
         `in_c004_normativa_legal` ";

        if($opc == 'T')
        {
            $cad_consulta = $cad_consulta." ORDER BY pk_num_normativa_legal DESC ";
        }

        $listado = $this->_db->query($cad_consulta);
        $listado->setFetchMode(PDO::FETCH_ASSOC);
        $datos = $listado->fetchAll();

        return $datos;
        

    }

    public function formatFechaAMD($fecha)
    {
        list($d, $m, $a)= explode('-', $fecha);

        if ($fecha == "00-00-0000") return ""; else return "$a-$m-$d";
    }

    public function metBuscarNormativaFiltro($radio, $start_fecha, $end_fecha, $num, $ind_tipo_normativa)
    {
        $cad_consulta = "
                  SELECT
                  `pk_num_normativa_legal`,
                  `ind_tipo_normativa`,
                  `ind_gaceta_circular`,
                  DATE_FORMAT(fec_gaceta_circular,'%d-%m-%Y' ) AS fec_gaceta_circular,
                  `ind_descripcion`,
                  `ind_tipo_ley_circular`,
                  `fk_a004_num_dependencia`,
                  `ind_resolucion`,
                   DATE_FORMAT(fec_resolucion,'%d-%m-%Y' ) AS fec_resolucion,
                  `ind_ruta_archivo`,
                  `fk_a018_num_seguridad_usuario`,
                  `fec_ultima_modificacion`
                   FROM in_c004_normativa_legal ";
        $band = 0;
        if($radio == 1 || $radio == 4 )
        {
           $cad_consulta = $cad_consulta." WHERE ( fec_gaceta_circular >= '".$start_fecha."') AND ( fec_gaceta_circular <= '".$end_fecha."')";
            $band = 1;
        }
        else
        {
           if($radio == 3)
           {
               $band = 1;
               $cad_consulta = $cad_consulta." WHERE ( ind_gaceta_circular = '".$num."' OR ind_resolucion = '".$num."' ) ";

           }
           else
           {
               if($radio == 2)
               {
                   $band = 1;
                   $cad_consulta = $cad_consulta." WHERE ( 	fec_resolucion >= '".$start_fecha."') AND ( fec_resolucion <= '".$end_fecha."')";
               }
           }
        }


        if($ind_tipo_normativa != 'T' && $band == 1)
        {
            $cad_consulta = $cad_consulta." AND ind_tipo_normativa = '".$ind_tipo_normativa."' ";
        }
        else
        {
            if($ind_tipo_normativa != 'T')
            {
                $cad_consulta = $cad_consulta." WHERE ind_tipo_normativa = '".$ind_tipo_normativa."' ";
            }
        }

        $cad_consulta = $cad_consulta." ORDER BY pk_num_normativa_legal DESC";
        //echo $cad_consulta;
        $listado = $this->_db->query($cad_consulta);
        $listado->setFetchMode(PDO::FETCH_ASSOC);
        $datos = $listado->fetchAll();

        return $datos;
    }

    public function metBusquedaUnicaNormativa($pk_num_normativa_legal)
    {
        $con = $this->_db->query("
         SELECT
        `pk_num_normativa_legal`,
        `ind_tipo_normativa`,
        `ind_gaceta_circular`,
         date_format(fec_gaceta_circular,'%d-%m-%Y') AS fec_gaceta_circular,
        `ind_descripcion`,
        `ind_tipo_ley_circular`,
        `fk_a004_num_dependencia`,
        `ind_resolucion`,
        date_format(fec_resolucion,'%d-%m-%Y') AS fec_resolucion,
        `ind_ruta_archivo`,
        `fk_a018_num_seguridad_usuario`,
        `fec_ultima_modificacion`
        FROM `in_c004_normativa_legal`
        WHERE `pk_num_normativa_legal` = '$pk_num_normativa_legal' ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }


    public function metModificarNormativaLegal($pk_num_normativa_legal_edit, $num_gaceta_otro, $fec_gaceta_otro, $ind_descripcion, $ind_tipo_ley_otro,$ind_dependencia_circular, $num_resolucion, $fec_resolucion, $ind_ruta_pdf, $atIdUsuario, $fecha_ultima_modificacion)
    {

        if($fec_gaceta_otro == null)
        {
            $cad1 = " `fec_gaceta_circular`= NULL,";
        }
        else
        {
            $cad1 = " `fec_gaceta_circular`= '$fec_gaceta_otro',";
        }

        if($ind_dependencia_circular == null)
        {
            $cad2 = " `fk_a004_num_dependencia`= NULL, ";
        }
        else
        {
            $cad2 = " `fk_a004_num_dependencia`= '$ind_dependencia_circular',";
        }

        if($fec_resolucion == null)
        {
            $cad3 = " `fec_resolucion`= NULL,";
        }
        else
        {
            $cad3 = " `fec_resolucion`= '$fec_resolucion',";
        }


        $this->_db->beginTransaction();
        $modificacion =  $this->_db->query("
        UPDATE `in_c004_normativa_legal` SET ".$cad1.$cad2.$cad3."
        `ind_gaceta_circular`='$num_gaceta_otro',
        `ind_descripcion`='$ind_descripcion',
        `ind_tipo_ley_circular`='$ind_tipo_ley_otro',
        `ind_resolucion`='$num_resolucion',
        `ind_ruta_archivo`='$ind_ruta_pdf',
        `fk_a018_num_seguridad_usuario`='$atIdUsuario',
        `fec_ultima_modificacion`='$fecha_ultima_modificacion'
         WHERE `pk_num_normativa_legal`= '$pk_num_normativa_legal_edit'");

        $this->_db->commit();

        return $modificacion;
    }

    public function metBorrarNormativaLegal($pk_num_normativa_legal)
    {
        $this->_db->beginTransaction();

        $respuesta = $this->_db->query(
            "DELETE FROM in_c004_normativa_legal WHERE pk_num_normativa_legal = '$pk_num_normativa_legal'"
        );

        $this->_db->commit();
        return $respuesta;

    }

    public function metRegistrarInterpretacion($fk_inc004_num_normativa_legal, $ind_descripcion, $ind_ruta_archivo, $fk_a018_num_seguridad_usuario, $fec_ultima_modificacion)
    {

        $this->_db->beginTransaction();
        $interpretacion =$this->_db->prepare(
            "insert into in_c005_interpretacion_ley values (
             null,
             :fk_inc004_num_normativa_legal,
             :ind_descripcion,
             :ind_ruta_archivo,
             :fk_a018_num_seguridad_usuario,
             :fec_ultima_modificacion)"
        );

        $interpretacion->execute(array(
            ':fk_inc004_num_normativa_legal' => $fk_inc004_num_normativa_legal,
            ':ind_descripcion' => $ind_descripcion,
            ':ind_ruta_archivo' => $ind_ruta_archivo,
            ':fk_a018_num_seguridad_usuario' => $fk_a018_num_seguridad_usuario,
            ':fec_ultima_modificacion' => $fec_ultima_modificacion
    ));

        $pk_num_interpretacion = $this->_db->lastInsertId();
        $this->_db->commit();

        return 	$pk_num_interpretacion;
    }

    public function metBuscarListadoInterpretaciones($fk_inc004_num_normativa_legal)
    {
        $con = $this->_db->query("
        SELECT *
        FROM  `in_c005_interpretacion_ley`
        WHERE  fk_inc004_num_normativa_legal = '$fk_inc004_num_normativa_legal'
        ORDER BY pk_num_interpretacion DESC ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $datos =  $con->fetchAll();
        return $datos;
    }

    public function metEliminarInterpretacion($pk_num_interpretacion)
    {
        $this->_db->beginTransaction();

        $respuesta = $this->_db->query(
            "DELETE FROM `in_c005_interpretacion_ley` WHERE pk_num_interpretacion = '$pk_num_interpretacion'"
        );

        $this->_db->commit();
        return $respuesta;
    }

};

?>

