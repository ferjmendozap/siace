<?php 
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Intranet
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Maikol Isava         | acc.desarrollo@cgesucre.gob.ve       | 0426-1814058
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class eventosModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

    }
	
	public function metBusquedaEventos()
    {
            $con = $this->_db->query(" 
            SELECT COUNT( fec_registro ) AS total , fec_registro
            FROM  `in_c002_eventos` 
            WHERE ind_anual = ''
            GROUP BY fec_registro");
            
            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetchAll();
    }

    public function metBusquedaEventosAnual()
    {
        $con = $this->_db->query(" 
            SELECT COUNT( fec_registro ) AS total , fec_registro
            FROM  `in_c002_eventos` 
            WHERE ind_anual = 'Y'
            GROUP BY fec_registro");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    public function metBusquedaEventosXfecha($fecha)
    {
        $con = $this->_db->query(" 
            SELECT
            `pk_num_evento`,
             DATE_FORMAT(fec_registro,'%d-%m-%Y' ) AS fec_registro,
            `hora_inicio`,
            `hora_final`,
            `horario_inicio`,
            `horario_final`,
            `ind_descripcion`,
            `ind_anual`, `ind_lugar`,
            `ind_estado`,
            `ind_municipio`,
            `ind_parroquia`,
            `ind_ruta_img`,
            `fec_ultima_modificacion`
            FROM  `in_c002_eventos` 
            WHERE fec_registro = '$fecha'");
            
            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetchAll();//SI NO HAY RESULTADO DEVUELVEN FALSE

    }

    public function metBusquedaEventosXfechaAnual($dia, $mes, $anio)
    {
        $con = $this->_db->query(" 
            SELECT
            `pk_num_evento`,
             DATE_FORMAT(fec_registro,'%d-%m-%Y' ) AS fec_registro,
            `hora_inicio`,
            `hora_final`,
            `horario_inicio`,
            `horario_final`,
            `ind_descripcion`,
            `ind_anual`, `ind_lugar`,
            `ind_estado`,
            `ind_municipio`,
            `ind_parroquia`,
            `ind_ruta_img`,
            `fec_ultima_modificacion`
            FROM  `in_c002_eventos` 
            WHERE ( MONTH(fec_registro) = '$mes' AND DAY (fec_registro) = '$dia' ) AND (  ind_anual = 'Y' OR YEAR (fec_registro) = '$anio' )
            " );


        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();//SI NO HAY RESULTADO DEVUELVEN FALSE
    }

    public function formatFechaAMD($fecha) {
        list($d, $m, $a)=explode('/', $fecha);
        if ($fecha == "00-00-0000") return ""; else return "$a-$m-$d";
    }

    public function metBuscarEstados()
    {
        $con = $this->_db->query("
            SELECT *
            FROM  `a009_estado`
            LIMIT 0 , 30 ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    public function metBuscarMunicipio($estado)
    {
        $con = $this->_db->query("
            SELECT *
            FROM `a011_municipio`
            WHERE  fk_a009_num_estado = '$estado' ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    public function metBuscarParroquia($municipio)
    {
        $con = $this->_db->query("
            SELECT *
            FROM `a012_parroquia`
            WHERE  fk_a011_num_municipio = '$municipio' ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    public function metInsertarEvento($fec_registro, $hora_inicio, $horario_inicio, $hora_final, $horario_final,
    $ind_ruta_img, $ind_descripcion, $ind_lugar, $ind_estado, $ind_municipio, $ind_parroquia, $ind_anual)
    {
        $this->_db->beginTransaction();
        $NuevaSolicitud=$this->_db->prepare(
            "insert into in_c002_eventos values (null, :fec_registro, :hora_inicio, :hora_final, :horario_inicio, :horario_final, :ind_descripcion, :ind_anual, :ind_lugar, :ind_estado, :ind_municipio, :ind_parroquia, :ind_ruta_img, null)"
        );
        $NuevaSolicitud->execute(array(
            ':fec_registro' => $fec_registro,
            ':hora_inicio' => $hora_inicio,
            ':hora_final' => $hora_final,
            ':horario_inicio' => $horario_inicio,
            ':horario_final' => $horario_final,
            ':ind_descripcion' => $ind_descripcion,
            ':ind_anual' => $ind_anual,
            ':ind_lugar' => $ind_lugar,
            ':ind_estado' => $ind_estado,
            ':ind_municipio' => $ind_municipio,
            ':ind_parroquia' => $ind_parroquia,
            ':ind_ruta_img' => $ind_ruta_img            

        ));
        $pk_num_evento = $this->_db->lastInsertId();
        $this->_db->commit();
        return $pk_num_evento;
    }

    public function metBusquedaUnicaEvento($pk_num_evento)
    {
        $con = $this->_db->query(" 
            SELECT *
            FROM  `in_c002_eventos` 
            WHERE pk_num_evento = '$pk_num_evento'");
            
            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetch();//SI NO HAY RESULTADO DEVUELVEN FALSE

    }

    public function metModificarEvento($pk_num_evento, $fec_registro, $hora_inicio, $horario_inicio, $hora_final, $horario_final,
                            $ind_ruta_img, $ind_descripcion, $ind_lugar, $ind_estado, $ind_municipio, $ind_parroquia, $ind_anual, $fec_ultima_modificacion)
    {
        $this->_db->beginTransaction();
        $modificacion =  $this->_db->query("
        UPDATE
        `in_c002_eventos`
        SET `fec_registro`= '$fec_registro',
            `hora_inicio`= '$hora_inicio',
            `horario_inicio`= '$horario_inicio',
            `hora_final`='$hora_final',
            `horario_final`='$horario_final',
            `ind_ruta_img`='$ind_ruta_img',
            `ind_descripcion`='$ind_descripcion',
            `ind_lugar`='$ind_lugar',
            `ind_estado`='$ind_estado',
            `ind_municipio`='$ind_municipio',
            `ind_parroquia`='$ind_parroquia',
            `ind_anual`='$ind_anual',
            `fec_ultima_modificacion`='$fec_ultima_modificacion'
        WHERE
            pk_num_evento = '$pk_num_evento'");

        $this->_db->commit();

        return $modificacion;
    }

    public function metBusquedaUnicaEstado($pk_num_estado)
    {
        $con = $this->_db->query("
            SELECT *
            FROM  `a009_estado` WHERE pk_num_estado = '$pk_num_estado'
            LIMIT 0 , 1 ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    public function metBusquedaUnicaMunicipio($pk_num_municipio)
    {
        $con = $this->_db->query("
            SELECT *
            FROM `a011_municipio`
            WHERE  pk_num_municipio = '$pk_num_municipio' ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    public function metBusquedaUnicaParroquia($pk_num_parroquia)
    {
        $con = $this->_db->query("
            SELECT *
            FROM `a012_parroquia`
            WHERE  pk_num_parroquia = '$pk_num_parroquia' ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    public function metEleminarEvento($pk_num_evento)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $eliminar = $this->_db->query(
            "delete from `in_c002_eventos` where pk_num_evento= '$pk_num_evento' "
        );
        #commit — Consigna una transacción
        $this->_db->commit();

        return $eliminar;
    }


};

?>
