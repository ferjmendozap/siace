<?php 
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Intranet
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Maikol Isava         | acc.desarrollo@cgesucre.gob.ve       | 0426-1814058
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class noticiaModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

    }
	
	 public function metConsultarOrganismos($id_cgr)
    {

        $con = $this->_db->query("
                SELECT * FROM `a001_organismo` WHERE ind_tipo_organismo = 'I' OR pk_num_organismo = '$id_cgr' ORDER BY pk_num_organismo ASC
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();//RETORNA FALSE SI NO HAY RESULTADO
    }
	
	public function metUsuario($usuario, $metodo)
    {
       $usuario = $this->_db->query("select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2 from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado=b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona");
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1) {  //para acceder de manera individual: $usuario[0]['ind_apellido1']
            return $usuario->fetch();//una sola fila
        } else {
            return $usuario->fetchAll();
        }

    }

    public function metInsertarNoticia($ind_titulo, $ind_descripcion, $txt_cuerpo, $txt_fuente, $fec_registro,$hora_regisro, $ind_ruta_img, $ind_status)
    {
        $this->_db->beginTransaction();
        $NuevaSolicitud=$this->_db->prepare(
            "insert into in_c001_noticias values (null, :ind_titulo, :ind_descripcion, :txt_cuerpo, :fk_a001_num_organismo, :fec_registro,:hora_regisro, :ind_ruta_img, :ind_status, null)"
        );
        $NuevaSolicitud->execute(array(
            ':ind_titulo' => $ind_titulo,
            ':ind_descripcion' => $ind_descripcion,
            ':txt_cuerpo' => $txt_cuerpo,
            ':fk_a001_num_organismo' => $txt_fuente,
            ':fec_registro' => $fec_registro,
            ':hora_regisro' => $hora_regisro,
            ':ind_ruta_img' => $ind_ruta_img,
            ':ind_status' => $ind_status
            

        ));
        $pkSolicitud = $this->_db->lastInsertId();
        $this->_db->commit();
        return $pkSolicitud;
    }

    public function metListarNoticias($organismo, $fecha_registro1, $fecha_registro2)
    {

        $cad_consulta = "
        SELECT
        `pk_num_noticia`,
        `ind_titulo`,
        `ind_descripcion`,
        `txt_cuerpo`,
        `fk_a001_num_organismo`,
        DATE_FORMAT(fec_registro,'%d-%m-%Y' ) AS fec_registro,
        `hora_regisro`,
        `ind_ruta_img`,
        `ind_status`,
        in_c001_noticias.fec_ultima_modificacion,
        `pk_num_organismo`,
        `ind_descripcion_empresa`
        FROM in_c001_noticias LEFT JOIN a001_organismo ON in_c001_noticias.fk_a001_num_organismo = a001_organismo.pk_num_organismo  ";//inner cuando se inluya cgr en la tabla organismo

        $band = 0;
        if($organismo != '')
        { 
            
            if( $organismo != 't')//todas
            {

                $cad_consulta = $cad_consulta." WHERE in_c001_noticias.fk_a001_num_organismo = '$organismo'"; //$cad_consulta." WHERE a001_organismo.ind_descripcion_empresa = '$organismo'";
                $band = 1;
            }

                        
        }

        if(!empty($fecha_registro1) && !empty($fecha_registro2)){

           if($band == 1)
            $cad_consulta = $cad_consulta." AND ( fec_registro >= '".$fecha_registro1."') AND ( fec_registro <= '".$fecha_registro2."')";
           else
             $cad_consulta = $cad_consulta." WHERE ( fec_registro >= '".$fecha_registro1."') AND ( fec_registro <= '".$fecha_registro2."')";

        }
        
        $cad_consulta = $cad_consulta." ORDER BY pk_num_noticia DESC";

        //echo $cad_consulta;
        
        $listado = $this->_db->query($cad_consulta);
        $listado->setFetchMode(PDO::FETCH_ASSOC);
        $datos = $listado->fetchAll();

        return $datos;
        

    }

    public function metBusquedaUnicaNoticia($pk_num_noticia)
    {       //INNER
            $con = $this->_db->query("
            SELECT *
            FROM in_c001_noticias
            LEFT JOIN a001_organismo ON in_c001_noticias.fk_a001_num_organismo = a001_organismo.pk_num_organismo
            WHERE   `ind_status` = 'm' AND  `pk_num_noticia` = '$pk_num_noticia'    ");
            
            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetch();
    }

    public function metBusquedaUnicaNoticia2($pk_num_noticia)
    {       //INNER
            $con = $this->_db->query("SELECT *
            FROM in_c001_noticias
            LEFT JOIN a001_organismo ON in_c001_noticias.fk_a001_num_organismo = a001_organismo.pk_num_organismo
            WHERE `pk_num_noticia` = '$pk_num_noticia' ");
            
            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetch();
    }

    public function metModificarNoticia($pk_num_noticia, $ind_titulo, $ind_descripcion, $txt_cuerpo, $txt_fuente, $fec_registro,$hora_regisro, $ind_ruta_img, $ind_status, $fec_ultima_modificacion )
    {

        $this->_db->beginTransaction();
        $modificacion =  $this->_db->query("
        UPDATE
        `in_c001_noticias`
        SET `ind_titulo`= '$ind_titulo',
            `ind_descripcion`= '$ind_descripcion',
            `txt_cuerpo`= '$txt_cuerpo',
            `fk_a001_num_organismo`='$txt_fuente',
            `fec_registro`='$fec_registro',
            `hora_regisro`='$hora_regisro',
            `ind_ruta_img`='$ind_ruta_img',
            `ind_status`='$ind_status',
            `fec_ultima_modificacion`='$fec_ultima_modificacion'
        WHERE
            pk_num_noticia = '$pk_num_noticia'");

        $this->_db->commit();

        return $modificacion;


    }

    public function formatFechaAMD($fecha) {
        list($d, $m, $a)=explode('-', $fecha);
        if ($fecha == "00-00-0000") return ""; else return "$a-$m-$d";
    }




};

?>
