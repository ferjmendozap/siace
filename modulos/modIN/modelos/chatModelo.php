<?php 
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Intranet
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Maikol Isava         | acc.desarrollo@cgesucre.gob.ve       | 0426-1814058
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class chatModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

    }

    public function metDatosPersonalesUsuario($idEmpleado)
    {
        $rol = $this->_db->query("
          SELECT

			persona.pk_num_persona,
			persona.ind_cedula_documento,
			persona.ind_documento_fiscal,
			persona.ind_nombre1,
			persona.ind_nombre2,
			persona.ind_apellido1,
			persona.ind_apellido2,
			persona.fk_a006_num_miscelaneo_detalle_nacionalidad,
			persona.fec_nacimiento,
			persona.ind_foto,
			persona.ind_tipo_persona,
			persona.num_estatus AS status_persona,
            empleado.pk_num_empleado,
            empleado.ind_estado_aprobacion,
            empleado.num_estatus AS status_empleado,
            empleado.ind_estado_aprobacion

          FROM
            a003_persona AS persona
          INNER JOIN
            rh_b001_empleado AS empleado ON empleado.fk_a003_num_persona = persona.pk_num_persona
          WHERE
            empleado.pk_num_empleado='$idEmpleado'
          GROUP BY
            persona.pk_num_persona
        ");
        $rol->setFetchMode(PDO::FETCH_ASSOC);
        return $rol->fetch();
    }

    public function metBuscarMensajesChat($start_fec_conversacion, $end_fec_conversacion, $opc)
    {
        if($opc != 'T')
        {
            $consulta = "SELECT
                 chat.pk_num_chat,
                 chat.ind_mensaje,
                 DATE_FORMAT(chat.fec_registro,'%d-%m-%Y' ) as fec_registro,
                 DATE_FORMAT(chat.fec_registro, '%T' ) as hora,
                 chat.fk_b001_num_empleado,
                 chat.fec_ultima_modificacion,
                 persona.ind_nombre1 as nombre,
                 persona.ind_apellido1 as apellido,
                 persona.ind_foto,
                 empleado.pk_num_empleado
                 FROM
                 `in_c003_chat` AS chat
                 INNER JOIN rh_b001_empleado AS empleado ON empleado.pk_num_empleado = chat.fk_b001_num_empleado
                 INNER JOIN a003_persona AS persona ON  persona.pk_num_persona = empleado.fk_a003_num_persona
                 WHERE ( DATE_FORMAT(chat.fec_registro,'%Y-%m-%d' ) >= '".$start_fec_conversacion."') AND ( DATE_FORMAT(chat.fec_registro,'%Y-%m-%d' ) <= '".$end_fec_conversacion."')
                 ORDER BY chat.pk_num_chat ASC  ";
        }
        else
        {
            $consulta = "SELECT chat.pk_num_chat, chat.ind_mensaje,
            DATE_FORMAT(chat.fec_registro,'%d-%m-%Y' ) as fec_registro,
            DATE_FORMAT( chat.fec_registro,  '%T' ) AS hora,
            chat.fk_b001_num_empleado,
            chat.fec_ultima_modificacion,
            persona.ind_nombre1 AS nombre,
            persona.ind_apellido1 AS apellido,
            persona.ind_foto, empleado.pk_num_empleado
            FROM  `in_c003_chat` AS chat
            INNER JOIN rh_b001_empleado AS empleado ON empleado.pk_num_empleado = chat.fk_b001_num_empleado
            INNER JOIN a003_persona AS persona ON persona.pk_num_persona = empleado.fk_a003_num_persona
            GROUP BY chat.fec_registro
            ORDER BY chat.pk_num_chat ASC ";
        }

        $verTema = $this->_db->query( $consulta );
        $verTema->setFetchMode(PDO::FETCH_ASSOC);
        return $verTema->fetchAll();
    }

    public function metBuscarFechasChat()
    {
        $consulta = "
        SELECT
        DATE_FORMAT(fec_registro,'%d-%m-%Y' ) as fec_registro
        FROM `in_c003_chat`
        GROUP BY DATE_FORMAT( fec_registro,  '%d-%m-%Y' )
        ORDER BY pk_num_chat DESC";

        $fechasConversaciones = $this->_db->query( $consulta );
        $fechasConversaciones->setFetchMode(PDO::FETCH_ASSOC);
        return $fechasConversaciones->fetchAll();
    }

    public function metBuscarFechasChat2($start_fec_conversacion, $end_fec_conversacion)
    {
        $consulta = "
        SELECT
        DATE_FORMAT(fec_registro,'%d-%m-%Y' ) as fec_registro
        FROM `in_c003_chat`
        WHERE ( DATE_FORMAT(fec_registro,'%Y-%m-%d' ) >= '".$start_fec_conversacion."') AND ( DATE_FORMAT(fec_registro,'%Y-%m-%d' ) <= '".$end_fec_conversacion."')
        GROUP BY DATE_FORMAT( fec_registro,  '%Y-%m-%d' )
        ORDER BY pk_num_chat DESC";

        $fechasConversaciones = $this->_db->query( $consulta );
        $fechasConversaciones->setFetchMode(PDO::FETCH_ASSOC);
        return $fechasConversaciones->fetchAll();
    }

    public function metEliminarMensaje($pk_num_chat)
    {
        $this->_db->beginTransaction();

        $respuesta = $this->_db->query(
            "DELETE FROM `in_c003_chat` WHERE pk_num_chat = '$pk_num_chat'"
        );

        $this->_db->commit();
        return $respuesta;
    }

    public function formatFechaAMD($fecha) {
        list($d, $m, $a)=explode('-', $fecha);
        if ($fecha == "00-00-0000") return ""; else return "$a-$m-$d";
    }

    public function metEliminarConversacion($tipo, $start_fec_conversacion, $end_fec_conversacion )
    {
        if($tipo == 'T')
        {
            $consulta = "DELETE FROM `in_c003_chat`";
        }
        else
        {
            $consulta = "DELETE FROM `in_c003_chat`
                         WHERE ( DATE_FORMAT(fec_registro,'%d-%m-%Y' ) >= '".$start_fec_conversacion."')
                         AND ( DATE_FORMAT(fec_registro,'%d-%m-%Y' ) <= '".$end_fec_conversacion."')";
        }

        $this->_db->beginTransaction();
        $respuesta = $this->_db->query($consulta);
        $this->_db->commit();
        return $respuesta;
    }


};

?>
