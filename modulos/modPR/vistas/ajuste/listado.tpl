<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Ajuste - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Cod. Ajuste</th>
                                <th>Ejer. Presupuestario</th>
								<th>Periodo</th>
								<th>Descripcion</th>
								<th>Monto Ajuste</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=ajuste from=$listado}
                                <tr id="idAjuste{$ajuste.pk_num_ajuste_presupuestario}">
                                    <td><label>{$ajuste.ind_cod_ajuste_presupuestario}</label></td>
                                    <td><label>{$ajuste.fec_anio}</label></td>
									<td><label>{$ajuste.fec_mes}</label></td>
									<td><label>{$ajuste.ind_descripcion}</label></td>
									<td><label>{$ajuste.num_total_ajuste|number_format:2:",":"."}</label></td>
                                    <td>
								
                                     <label>{if $ajuste.ind_estado=='PR'}Preparaci&oacute;n{else if $ajuste.ind_estado=='AP'}Aprobado{else if     
									 $ajuste.ind_estado=='GE'}Generado{else if $ajuste.ind_estado=='AN'}Anulado{/if}</label>
                                    </td>
                                    <td>
									 {if !$status}
                                       {if $ajuste.ind_estado=='PR'}
                                            {if in_array('PR-01-05-01-02-M',$_Parametros.perfil)}
                                             <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idAjuste="{$ajuste.pk_num_ajuste_presupuestario}" title="Modificar Ajuste"
                                                    descipcion="El Usuario a Modificado un ajuste del sistema" titulo="Modificar ajuste">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                             </button>
                                            {/if}
											
										{if in_array('PR-01-05-01-02-M',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" 
											idAjuste="{$ajuste.pk_num_ajuste_presupuestario}" title="Eliminar Ajuste" boton="si, Eliminar"
                                                    descipcion="El usuario a eliminado un ajuste del sistema" titulo="Estas Seguro?" mensaje="Estas seguro que desea 
													eliminar el Ajuste!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
											
                                        {/if}
										
										
										{if $ajuste.ind_estado=='AP'}
										  {if in_array('PR-01-05-01-02-M',$_Parametros.perfil)}
                                            <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idAjuste="{$ajuste.pk_num_ajuste_presupuestario}" title="Consultar Ajuste"
                                                descipcion="El Usuario esta consultando un Ajuste" titulo="<i class='icm icm-calculate2'></i> Ver Ajuste">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                                          {/if}	
										 {/if} 
										
										{elseif $status=='PR'}
                                         {if in_array('PR-01-05-01-04-A',$_Parametros.perfil)}
                                            <button class="aprobar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idAjuste="{$ajuste.pk_num_ajuste_presupuestario}" 
													title="Aprobar Ajuste" descipcion="El Usuario aprobado un Ajuste" titulo="Aprobar un ajuste"> 
												<i class="fa fa-check" style="color: #ffffff;"></i>
                                            </button>
											
											<button class="anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idAjuste="{$ajuste.pk_num_ajuste_presupuestario}" 
													title="Anular Ajuste" descipcion="El Usuario generar un Ajuste" titulo="Anular Ajuste"> 
												<i class="md md-not-interested" style="color: #ffffff;"></i>
                                            </button>
                                         {/if}
                                     
									  
									  {elseif $status=='AP'}
                                        {if in_array('PR-01-05-01-04-G',$_Parametros.perfil)}
                                            <button class="generar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idAjuste="{$ajuste.pk_num_ajuste_presupuestario}" 
													title="Generar Proyecto" descipcion="El Usuario generar el Perfil de Detalle" titulo="Generar Perfil Detalle"> 
												<i class="md md-sync" style="color: #ffffff;"></i>
                                            </button>
											
											
                                        {/if}
																				
                                      {/if}
								   
                                     
									  
                                      &nbsp;&nbsp;
                                      
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="7">
							<div>
							  {if $status!='AP' && $status!='GE' && $status!='PR' } 
                                {if in_array('PR-01-02-01-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario a creado una ajuste del sistema" title="Crear Ajuste"  titulo="Nuevo Ajuste" id="nuevo" >
                                        Nuevo Ajuste &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-create"></i>
                                    </button>
                                {/if}
							  {/if}	
							  </div>
                            </th>
							
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modPR/ajusteCONTROL/crearModificarMET';
       
	   $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idAjuste:0 ,status:'PR',inicial:'IN'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
				

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idAjuste: $(this).attr('idAjuste'), status:'PR',inicial:''},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		
			
		
		$('#datatable1 tbody').on( 'click', '.aprobar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idAjuste: $(this).attr('idAjuste'), status:'AP'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		
		
		
		$('#datatable1 tbody').on( 'click', '.anular', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idAjuste: $(this).attr('idAjuste'), status:'AN'},function($dato){
                $('#ContenidoModal').html($dato);
            });
         });
		
		
		
		$('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idAjuste: $(this).attr('idAjuste'), status:'VER'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
      
		
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idAjuste=$(this).attr('idAjuste');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modPR/ajusteCONTROL/eliminarMET';
                $.post($url, { idAjuste: idAjuste },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idAjuste'+dato['idAjuste'])).html('');
                        swal("Eliminado!", "El Ajuste fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>