<form action="{$_Parametros.url}modPR/ajusteCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
	     <input type="hidden" value="1" name="valido" />
	   
	   {if $status =='AP' || $status =='AN'}
            <input type="hidden" value="1" name="ver" id="ver" />
       {/if}
	   
	   {if $status =='VER'}
            <input type="hidden" value="2" name="ver" id="ver" />
       {/if}
	   
	    <input type="hidden" value="{$status}" name="status"  id="status"/>
        <input type="hidden" value="{$idAjuste}" name="idAjuste" id="idAjuste"/>
		<input type="hidden" value="IN" name="inicial"/>
		
		<input type="hidden" value="{if isset($formDB.ind_tipo_presupuesto)}{$formDB.ind_tipo_presupuesto} {else} {$tipoPresupuesto}{/if}" name="ind_tipo_presupuesto" id="ind_tipo_presupuesto"/>
		
		 <div>
        
         <!-- Nav tabs -->
             <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#Datos" aria-controls="home" role="tab" data-toggle="tab">Datos Generales</a></li>
                <li role="presentation"><a href="#Detalle" aria-controls="profile" role="tab" data-toggle="tab">Detalle de Presupuesto</a></li>
             </ul>

  <!-- Tab panes -->
            <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="Datos">Datos Generales
			   
		   <div class="row">
		
           	  <div class="col-sm-2">
                    <div class="form-group floating-label" >
                        <input type="text" readonly class="form-control" value="{if isset($formDB.ind_cod_ajuste_presupuestario)}{$formDB.ind_cod_ajuste_presupuestario}{/if}" name="form[alphaNum][ind_cod_ajuste_presupuestario]" id="ind_cod_ajuste_presupuestario">
                        <label for="ind_cod_ajuste_presupuestario">Cod. Ajuste Presupuestario</label>
                    </div>
                </div>
				
			    <div class="col-sm-3">
                    <div class="form-group floating-label" id="fk_prb004_num_presupuestoError">
                        <select id="fk_prb004_num_presupuesto" name="form[alphaNum][fk_prb004_num_presupuesto]" class="form-control dirty">
                            {foreach item=app from=$presupuesto}
                                {if isset($formDB.fk_prb004_num_presupuesto)}
                                    {if $app.pk_num_presupuesto==$formDB.fk_prb004_num_presupuesto}
                                        <option selected value="{$app.pk_num_presupuesto}|{$app.ind_tipo_presupuesto}">{$app.ind_cod_presupuesto}</option>
                                        {else}
                                        <option value="{$app.pk_num_presupuesto}|{$app.ind_tipo_presupuesto}">{$app.ind_cod_presupuesto}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_presupuesto}|{$app.ind_tipo_presupuesto}">{$app.ind_cod_presupuesto}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb004_num_presupuesto">Seleccione Presupuesto</label>
                    </div>
                </div>
		    
				 
				 				 
				 <div class="col-sm-3">
                    <div class="form-group floating-label" id="fk_a006_num_miscelaneo_det_tipo_ajusteError">
                        <select id="fk_a006_num_miscelaneo_det_tipo_ajuste" name="form[int][fk_a006_num_miscelaneo_det_tipo_ajuste]" class="form-control">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$tipoajuste}
                                {if isset($formDB.fk_a006_num_miscelaneo_det_tipo_ajuste)}
                                    {if $app.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_det_tipo_ajuste}
                                        <option selected value="{$app.pk_num_miscelaneo_detalle}">{$app.ind_nombre_detalle}</option>
                                        {else}
                                        <option value="{$app.pk_num_miscelaneo_detalle}">{$app.ind_nombre_detalle}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_miscelaneo_detalle}">{$app.ind_nombre_detalle}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_a006_num_miscelaneo_det_tipo_ajuste">Tipo Ajuste</label>
                    </div>
                </div>
		    </div>
			  
		 <div class="row">
		
		       <div class="col-sm-2">
                    <div class="form-group floating-label" id="fec_anioError">
                        <input readonly type="text" class="form-control fecha_anio" value="{if isset($formDB.fec_anio)}{$formDB.fec_anio}{else}{'Y'|date}{/if}" name="form[alphaNum][fec_anio]" id="fec_anio">
                        <label for="fec_anio">A&ntilde;o</label>
                    </div>
                 </div>
				 
			<!--	 <div class="col-sm-2">
                    <div class="form-group floating-label" id="fec_mesError">
                        <input type="text" class="form-control fecha_mes" value="{if isset($formDB.fec_mes)}{$formDB.fec_mes}{/if}" name="form[alphaNum][fec_mes]" id="fec_mes">
                        <label for="fec_mes">Mes</label>
                    </div>
                 </div>-->
				 
				 
				  <div class='col-sm-2'>
                   <div class="form-group floating-label" id="fec_ajusteError">
                       
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.fec_ajuste)}{$formDB.fec_ajuste|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fec_ajuste]">
					          <label for="fec_ajuste"><i class="fa fa-calendar"></i> Fecha Ajuste</label>
                           
                    </div>
                </div>	
       
		 </div>
						
		<div class="row">
		
		   <div class="col-sm-2">
                <div class="form-group floating-label" id="ind_numero_gacetaError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_numero_gaceta)}{$formDB.ind_numero_gaceta}{/if}" name="form[alphaNum][ind_numero_gaceta]" id="ind_numero_gaceta">
                        <label for="ind_numero_gaceta">Numero Gaceta</label>
                 </div>
            </div>	
				
				
			  <div class='col-sm-2'>
                   <div class="form-group floating-label" id="fec_gacetaError">
                       
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.fec_gaceta)}{$formDB.fec_gaceta|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fec_gaceta]">
					          <label for="fec_gaceta"><i class="fa fa-calendar"></i> Fecha Gaceta</label>
                           
                    </div>
                </div>		

         </div>
			
			
	   <div class="row">
		
		   <div class="col-sm-2">
                <div class="form-group floating-label" id="ind_numero_resolucionError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_numero_resolucion)}{$formDB.ind_numero_resolucion}{/if}" name="form[alphaNum][ind_numero_resolucion]" id="ind_numero_resolucion">
                        <label for="ind_numero_resolucion">Numero Resolucion</label>
                 </div>
            </div>	
			
			
			  <div class='col-sm-2'>
                   <div class="form-group floating-label" id="fec_resolucionError">
                     
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.fec_resolucion)}{$formDB.fec_resolucion|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fec_resolucion]" >
					          <label for="fec_resolucion"><i class="fa fa-calendar"></i> Fecha Resolucion</label>
                           
                    </div>
                </div>		 
        </div>		
			
		 <div class="row">	
		    <div class="col-lg-6">
                       <div class="form-group" id="ind_descripcionError">
                                        <textarea required="" placeholder="" rows="3" class="form-control" id="ind_descripcion" name="ind_descripcion">{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}</textarea>
                                        <label for="ind_descripcion">Descripcion</label>
                       </div>
            </div>
			 	
		  </div>	
			

          <div class="row">	
				 <div class="col-sm-5">
                    <div class="form-group floating-label"  id="num_total_ajusteError">
                        <input type="text" readonly="readonly" class="form-control monto dirty" value="{if isset($formDB.num_total_ajuste)}{$formDB.num_total_ajuste|number_format:2:",":"."}{/if}" name=  
						"form[int][num_total_ajuste]" id="num_total_ajuste">
                        <label for="num_total_ajuste">Monto Total Ajuste</label>
                    </div>
                 </div>
		  
		  
	

                <div class="col-sm-2">
                    <div class="form-group floating-label" >
                        <input type="text" readonly class="form-control" value="{if isset($formDB.ind_estado)}{$formDB.ind_estado}{/if}" name="form[alphaNum][ind_estado]" id="ind_estado">
                        <label for="ind_estado">Estatus</label>
                    </div>
                 </div>
        </div>
		

        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
			   
	   <div class="modal-footer">
          <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>Cancelar</          button>
		  
	{if ($status!='VER')}  
         <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
            
		    {if $status=='AP'}
                Aprobar
            {elseif $status=='AN'}
                Anular
			{elseif $status=='VER'}
                Cerrar
            {elseif isset($idAjuste)}
                Modificar
            {else}
                <span class="glyphicon glyphicon-floppy-disk"></span>Guardar
            {/if}
        </button>
	{/if}	
       </div>
				   
 </div>
             
		 
	<!-----------DETALLE PRESUPUESTO---------------------------------->		 
	
    <div role="tabpanel" class="tab-pane" id="Detalle">Detalle de Presupuesto
               <div id="listapartidas"> </div>
       </div>
    </div>
	
	
	
	
   
</form>

<script type="text/javascript">
    $(document).ready(function() {
	    
		 
        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
			
	
        }
		
        $("#formAjax").submit(function(){
            return false;
        });
		  
		
		
		//$('#fk_prb004_num_presupuesto').load(function() {
		
			 var $url_ajuste='{$_Parametros.url}modPR/ajusteCONTROL/BuscarPartidasMET';
			 
			 var cod_presupuesto= '{if isset($formDB.fk_prb004_num_presupuesto)}{$formDB.fk_prb004_num_presupuesto}{/if}';
			 var cod_ajuste=$(document.getElementById('idAjuste')).val();
			 var status=$(document.getElementById('status')).val();
			 
				$('#formModalLabel').html($(this).attr('titulo'));
				$.post($url_ajuste,{ idPresupuesto:cod_presupuesto, idAjuste:cod_ajuste, status:status, seleccionado:0},function($dato){
					 $('#listapartidas').html($dato);
				});
		
		   
      //      }); 
       
		
		
	     var nuevo=$(document.getElementById("idAjuste")).val();
		 
		  if (nuevo==0){
	      	 var pr = new  PrFunciones();
				 var anio = '{if isset($formDB.fec_anio)}{$formDB.fec_anio}{/if}';
				 var idPresupuesto = '{if isset($formDB.fk_prb004_num_presupuesto)}{$formDB.fk_prb004_num_presupuesto}{/if}';
				 
				 pr.metJsonPresupuesto('{$_Parametros.url}modPR/reformulacionCONTROL/JsonPresupuestoMET',anio,idPresupuesto);
				 
		 }	
		
				
		  /***************************DESHABILITAR CONTROLES*****************************/
		  var disabled=$(document.getElementById("ver")).val();
		   if (disabled==1){
		       $("#formAjax input,textarea").prop("readonly", true);
			   //$("#formAjax select").prop("disabled", true);
			   $("#formAjax select").attr("readonly", true);
			   $("#formAjax input" ).removeClass('fechas2');
			   $("#formAjax input" ).removeClass('fecha_anio');
			   $("#formAjax input" ).removeClass('fecha_mes');
			   
			   //$("#formAjax" ).removeClass('hasDatepicker')
  
		   }	   
		   else if (disabled==2){
		       $("#formAjax input,textarea,select").prop("disabled", true);
		   }
	   /********************************************************************************/	 
		 
		
		
		$('#agregarPartida').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
		
		
		 $('#codpartida').on('click', '.borrar', function () {
            var borrar = $(this);
            $(document.getElementsByClassName(borrar.attr('borrar'))).remove();
         });
		
		
	
			
	      $('#fk_prb004_num_presupuesto').change(function () {
		  
		       var $url='{$_Parametros.url}modPR/ajusteCONTROL/BuscarPartidasMET';
		
			   var $posicion=$(this).val().indexOf('|');
			   var tipo_presupuesto=$(this).val();
			   tipo_presupuesto= tipo_presupuesto.substr(-1);
			   
			   var cod_presupuesto=$(this).val().substr(0,$posicion);
			 
			   var cod_ajuste=$(document.getElementById('idAjuste')).val();
			 
			   var status=$(document.getElementById('status')).val();
			 
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idPresupuesto:cod_presupuesto, idAjuste:cod_ajuste, status:status, seleccionado:1},function($dato){
                 $('#listapartidas').html($dato);
            });
			
          });
	
	
		
		 $('.fecha_mes').datepicker({ format: 'mm', viewMode: "months", minViewMode: "months",language:'es' }); 
		 $('#fechas').datepicker({ autoclose: true, todayHighlight: true, format: "dd-mm-yyyy", language:'es' }); 
		 $('.fechas2').datepicker({ autoclose: true, todayHighlight: true, format: "dd-mm-yyyy", language:'es' });
		// $('#fec_anio').datepicker({ format: 'yyyy', viewMode: "years", minViewMode: "years", startDate: '-1y', endDate: '+1y' ,language:'es' }); 
    	
		
        $('#modalAncho').css("width","100%");
		
        $('#accion').click(function(){
		   swal({
                title: "�Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            }); 
		
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
		
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
					 //----------------------------------------------------
					
			   }else if(dato['status']=='generado'){
                   
					$(document.getElementById('idAjuste'+dato['idAjuste'])).html('');		
                    swal("Registro Generado!", "El Ajuste fue generado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
					
					//------------------------------------------------------
					
			   }else if(dato['status']=='aprobado'){
                   
                    $(document.getElementById('idAjuste'+dato['idAjuste'])).html('');		
                    swal("Registro Aprobado!", "El Ajuste fue aprobado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
					
					//------------------------------------------------------
					
                }else if(dato['status']=='modificar'){
                   
                    if(dato['ind_estado']=='PR' ){ var estado='Preparaci&oacuten' }else if (dato['ind_estado']=='RV'){ var estado='Revisado' } else if (dato['ind_estado']=='AP'){ var estado='Aprobado' } else { var estado='Preparaci&oacuten' }
                    $(document.getElementById('idAjuste'+dato['idAjuste'])).html('<td>'+dato['ind_cod_ajuste_presupuestario']+'</td>' +
                            '<td>'+dato['fec_anio']+'</td>' +
							'<td>'+dato['fec_mes']+'</td>' +
							'<td>'+dato['ind_descripcion']+'</td>' +
                            '<td>'+dato['num_total_ajuste']+'</td>' +
							'<td>'+estado+'</td>' +
                            '<td>' +	
                            '{if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idAjuste="'+dato['idAjuste']+'"' +
                            'descipcion="El Usuario a Modificado un Ajuste" titulo="Modificar Ajuste">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idAjuste="'+dato['idAjuste']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado un Ajuste" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Ajuste!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Modificado!", "El Ajuste fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
					
                }else if(dato['status']=='nuevo'){
				     if(dato['ind_estado']=='PR' ){ var estado='Preparaci&oacuten' }else if (dato['ind_estado']=='RV'){ var estado='Revisado' } else if (dato['ind_estado']=='AP'){ var estado='Aprobado' } else { var estado='Preparaci&oacuten' }
                     $(document.getElementById('datatable1')).append('<tr  id="idAjuste'+dato['idAjuste']+'">' +
                            '<td>'+dato['ind_cod_ajuste_presupuestario']+'</td>' +
                            '<td>'+dato['fec_anio']+'</td>' +
							'<td>'+dato['fec_mes']+'</td>' +
							'<td>'+dato['ind_descripcion']+'</td>' +
                            '<td>'+dato['num_total_ajuste']+'</td>' +
							'<td>'+estado+'</td>' +
                            '<td>' +
                            '{if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idAjuste="'+dato['idAjuste']+'"' +
                            'descipcion="El Usuario a Modificado un Ajuste" titulo="Modificar Ajuste">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idAjuste="'+dato['idAjuste']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado un Ajuste" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Ajuste!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Creado!", "El Ajuste fue creado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
    });

	
	
function actualizarMonto(CodPartida,aespecifica1) {
var suma=0;
		var suma=0; 
		var codigo=""; 
		var codigo1="";
		var valor=""; 
		

   $("#codpartida td").each(function (){
   
				 $(this).find('input').each (function() {
				   
				      var presupuesto = $(document.getElementById('ind_tipo_presupuesto')).val().trim(); 
					  
					 aespecifica = $(this).attr('aespecifica');
				     idpartida1 = $(this).attr('id');
					 monto_a='';
					 //tipo=$(document.getElementById("fk_a006_num_miscelaneo_det_tipo_ajuste")).val();
					 
					 
					 tipo = $("#fk_a006_num_miscelaneo_det_tipo_ajuste option:selected").html();
					 
					
					 // monto=$(document.getElementById(idpartida_inicial)).val();
					 // monto_inicial=setNumero(monto);
					 
					//alert (monto_inicial);
					 
					   
					  
					 //if (tipo.trim()!="Incremento" && monto_inicial>0)
					 if (tipo.trim()!="Incremento")
					 {
					    if (idpartida1.substring(0, 1)!="-" )
					    {
					        if ( idpartida1.trim()!="partidas" )
						    {
						     //  if ( idpartida1.trim()!="Aespecifica" )
						     //  {
							 
							 //alert(idpartida1);
						       
									idpartida_inicial="-"+idpartida1;
									idpartida_compromiso="-c"+idpartida1;
									idpartida_disponible="-p"+idpartida1;
									  
									 monto=$(document.getElementById(idpartida_inicial)).val();
									 
									 monto_c=$(document.getElementById(idpartida_compromiso)).val();
									 
									 monto_p_c=$(document.getElementById(idpartida_disponible)).val();
									 
									 monto_a=$(document.getElementById(idpartida1)).val();
									 
									 
									 monto_inicial=setNumero(monto);
									 
									 monto_compromiso=setNumero(monto_c);
									 
									 monto_disponible=setNumero(monto_p_c);
									 
									 monto_ajuste=setNumero(monto_a);
									 
									 monto_inicial=monto_disponible;
									 
								   
									  if (monto_inicial<monto_ajuste)
									  {
										  alert ("MONTO DEL AJUSTE ES MAYOR A DISPONIBLE");
										  $(this).val("0,00");
									  }
								// }//---------	  
						    } 
					      }
					   }
					 
					 
					  valor= $(this).val();  
					  $(this).val(valor).formatCurrency();
					  			  
					  
			 if(presupuesto=="P") 
		     {
				if (idpartida1.substring(0, 1)!="-" )
				{ 
				     codigo= idpartida1.substring(0, 6);
					 codigo= codigo.concat(".00.00");
					 
					  //alert (codigo+' / '+CodPartida+' / '+aespecifica1+' / '+aespecifica);
					 
					 				 
					  if (CodPartida==codigo && aespecifica1==aespecifica){
					     monto= setNumero($(this).val());
						 suma=suma+Number(monto);
					     
						 }
					 
					 
					 codigo1= idpartida1.substring(0, 3);
					 codigo1= codigo1.concat(".00.00.00");
					 
					 
					 
					  if (CodPartida==codigo1 && aespecifica1==aespecifica){
						 monto= setNumero($(this).val());
						 suma=suma+Number(monto);
						 }
						 
						 
					 codigo2= idpartida1.substring(0, 9);
					 codigo2= codigo2.concat(".00");
					 
					  if (CodPartida==codigo2 && aespecifica1==aespecifica){
						 monto= setNumero($(this).val());
						 suma=suma+Number(monto);
					 }
					 
					}//----------- 
				}
				 else
				  {
				     codigo= idpartida1.substring(0, 6);
					 codigo= codigo.concat(".00.00");
					 
					 
				      if (CodPartida==codigo){
					    monto= setNumero($(this).val());
					    suma=suma+Number(monto);
					 }
					 
					 
					  codigo1= idpartida1.substring(0, 3);
					  codigo1= codigo1.concat(".00.00.00");
					 
					  if (CodPartida==codigo1){
					     monto= setNumero($(this).val());
						 suma=suma+Number(monto);
					  }
						 
						 
					   codigo2= idpartida1.substring(0, 9);
					   codigo2= codigo2.concat(".00");
					 
					   if (CodPartida==codigo2 ){
						 monto= setNumero($(this).val());
						 suma=suma+Number(monto);
					  }
				  }
					  
					 
	             }) //imput
			 })	
			 
			 return suma; 
}

//	funcion para convertir un numero formateado en su valor real
function setNumero(num_formateado) {
	var num = num_formateado.toString();
	num = num.replace(/[.]/gi, "");
	num = num.replace(/[,]/gi, ".");
	
	var numero = new Number(num);
	return numero;
}



</script>


