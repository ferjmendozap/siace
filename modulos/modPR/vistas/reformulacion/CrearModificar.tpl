<form action="{$_Parametros.url}modPR/reformulacionCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
	     <input type="hidden" value="1" name="valido" />
	   
	    {if $status =='AP' || $status =='GE' || $status =='AN'}
            <input type="hidden" value="1" name="ver" id="ver" />
       {/if}
	   
	   {if $status =='VER'}
            <input type="hidden" value="2" name="ver" id="ver" />
       {/if}
	   
	    <input type="hidden" value="{$status}" name="status" />
        <input type="hidden" value="{$idReformulacion}" name="idReformulacion"/>
	  <!--  <input type="hidden" value="{$formDB.ind_tipo_presupuesto}" name="form[alphaNum][ind_tipo_presupuesto]"  id="ind_tipo_presupuesto"/>-->
		<input type="hidden" value="G" name="form[alphaNum][ind_tipo_presupuesto]"  id="ind_tipo_presupuesto"/> 
		  
		 <div>
         <!-- Nav tabs -->
             <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#Datos" aria-controls="home" role="tab" data-toggle="tab">Datos Generales</a></li>
                <li role="presentation"><a href="#Detalle" aria-controls="profile" role="tab" data-toggle="tab">Detalle de Partidas</a></li>
               
             </ul>

  <!-- Tab panes -->
            <div class="tab-content">
               <div role="tabpanel" class="tab-pane active" id="Datos">Datos Generales
			   
			    <div class="row">
		
           	 <div class="col-sm-2">
                    <div class="form-group floating-label">
                        <input type="text" readonly class="form-control" value="{if isset($formDB.cod_reformulacion)}{$formDB.cod_reformulacion}{/if}" name="form[alphaNum][cod_reformulacion]" id="cod_reformulacion">
                        <label for="cod_reformulacion">Cod. Reformulacion</label>
                    </div>
                 </div>
				 
				 
				 
				 <div class="col-sm-3">
                    <div class="form-group floating-label" id="fk_prb004_num_presupuestoError">
                        <select id="fk_prb004_num_presupuesto" name="form[alphaNum][fk_prb004_num_presupuesto]" class="form-control dirty">
                            {foreach item=app from=$presupuesto}
                                {if isset($formDB.fk_prb004_num_presupuesto)}
                                    {if $app.pk_num_presupuesto==$formDB.fk_prb004_num_presupuesto}
                                        <option selected value="{$app.pk_num_presupuesto}|{$app.ind_tipo_presupuesto}">{$app.ind_cod_presupuesto}</option>
                                        {else}
                                        <option value="{$app.pk_num_presupuesto}|{$app.ind_tipo_presupuesto}">{$app.ind_cod_presupuesto}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_presupuesto}|{$app.ind_tipo_presupuesto}">{$app.ind_cod_presupuesto}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb004_num_presupuesto">Seleccione Presupuesto</label>
                    </div>
                </div>
		    </div>
			  
		 <div class="row">
		
		 <input type="hidden" class="form-control fecha_anio" value="{if isset($formDB.fec_anio)}{$formDB.fec_anio}{else}{'Y'|date}{/if}" name="form[alphaNum][fec_anio]" id="fec_anio">
		
	     <div class="col-sm-2">
                   <div class="form-group floating-label" id="fec_anioError">
                        <input readonly type="text" class="form-control fecha_anio" value="{if isset($formDB.fec_anio)}{$formDB.fec_anio}{else}{'Y'|date}{/if}" name="form[alphaNum][fec_anio]" id="fec_anio">
                        <label for="fec_anio">A&ntilde;o</label>
                   </div>
         </div>
				 
				 
				   <div class='col-sm-3'>
                   <div class="form-group floating-label" id="fec_reformulacionError">
                       
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.fec_reformulacion)}{$formDB.fec_reformulacion|date_format:"%d-%m-%Y"}{else} {'d-m-Y'|date}{/if}" name="form[txt][fec_reformulacion]">
					          <label for="fec_reformulacion"><i class="fa fa-calendar"></i> Fecha Reformulacion</label>
                           
                    </div>
                </div>	
		 </div>
						
		<div class="row">
		
		   <div class="col-sm-3">
                <div class="form-group floating-label" id="ind_numero_gacetaError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_numero_gaceta)}{$formDB.ind_numero_gaceta}{/if}" name="form[alphaNum][ind_numero_gaceta]" id="ind_numero_gaceta">
                        <label for="ind_numero_gaceta">Numero Gaceta</label>
                 </div>
            </div>	
				
				
			  <div class='col-sm-3'>
                   <div class="form-group floating-label" id="fec_gacetaError">
                       
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.fec_gaceta)}{$formDB.fec_gaceta|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fec_gaceta]">
					          <label for="fec_gaceta"><i class="fa fa-calendar"></i> Fecha Gaceta</label>
                           
                    </div>
                </div>		

         </div>
			
			
	    <div class="row">
		
		   <div class="col-sm-3">
                <div class="form-group floating-label" id="ind_numero_resolucionError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_numero_resolucion)}{$formDB.ind_numero_resolucion}{/if}" name="form[alphaNum][ind_numero_resolucion]" id="ind_numero_resolucion">
                        <label for="ind_numero_resolucion">Numero Resolucion</label>
                 </div>
            </div>	
			
			
			  <div class='col-sm-3'>
                   <div class="form-group floating-label" id="fec_resolucionError">
                     
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.fec_resolucion)}{$formDB.fec_resolucion|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fec_resolucion]" >
					          <label for="fec_resolucion"><i class="fa fa-calendar"></i> Fecha Resolucion</label>
                           
                    </div>
                </div>		 
        </div>		
			
       <div class="row">
	          <div class="col-lg-12">
                       <div class="form-group" id="ind_descripcionError">
                                        <textarea required="" placeholder="" rows="3" class="form-control" id="ind_descripcion" 
										name="form[alphaNum][ind_descripcion]">{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}</textarea>
                                        <label for="textarea1">Descripcion</label>
                       </div>
              </div>	
       </div>


        <div class="row">
				  <div class="col-sm-5">
                    <div class="form-group"  id="num_monto_reformulacionError">
                        <input type="text"  class="form-control dirty" value="{if isset($formDB.num_monto_reformulacion)}{$formDB.num_monto_reformulacion|number_format:2:",":"."}{/if}" name= "form[int][num_monto_reformulacion]" id="num_monto_reformulacion"  readonly="readonly" >
                        <label for="num_monto_reformulacion">Monto Reformulacion</label>
                    </div>
                 </div>
		  
		       	

                <div class="col-sm-2">
                    <div class="form-group floating-label" >
                        <input type="text" readonly class="form-control" value="{if isset($formDB.ind_estado)}{$formDB.ind_estado}{/if}" name="form[alphaNum][ind_estado]" id="ind_estado">
                        <label for="ind_estado">Estatus</label>
                    </div>
                 </div>
        </div>
		
			

        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                  <div class="col-sm-12">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
			   
	  
				   
 </div>
             
		 
	<!-----------DETALLE PRESUPUESTO---------------------------------->		 
    <div role="tabpanel" class="tab-pane" id="Detalle">Detalle de Presupuesto
			   
			      <div>
				  
				     <div class="table-responsive">
                                      <table class="table no-margin" id="tablapartida">
                                                        <thead>
                                                        <tr>
                                                            <th>Codigo</th>
                                                            <th>Partida</th>
															<th>Monto</th>
														<!--	<th>Aespecifica</th>-->
                                                           <!-- <th>Acciones</th>-->
                                                        </tr>
                                                        </thead>
                                                        <tbody  id="codpartida">
                                                        <!--{if isset($partidas)}-->
														  {if isset($partidas)}
                                                            {foreach item=i from=$partidas}
															   
                                                    
																	
														{if $formDB.ind_tipo_presupuesto =='P'}
							                                           <tr class="idPartida{$i.aespecifica|cat:'_'|cat:$i.cod|replace:".":"_"}">
                                                                    <input type="hidden" value="{$i.id}" name="form[int][fk_prb002_num_partida_presupuestaria][]" class="partidaInput" partida="{$i.cod}" id="{$i.aespecifica|cat:'_'|cat:$i.cod|replace:".":"_"}"/> 	
														{else}	  
																      <tr class="idPartida{'0_'|cat:$i.cod|replace:".":"_"}">
                                                                    <input type="hidden" value="{$i.id}" name="form[int][fk_prb002_num_partida_presupuestaria][]" class="partidaInput" partida="{$i.cod}" id="{$i.aespecifica|cat:'_'|cat:$i.cod|replace:".":"_"}"/> 
						                                 {/if}	
																	
																	
																	
																	
															{if $i.tipo=='E'}			
																	<td style="font-family:Arial, Geneva, sans-serif; font-weight:bold; color:blue">{$i.cod}</td>
                                                                    <td style="font-family:Arial, Geneva, sans-serif; font-weight:bold; color:blue">{$i.partida}</td>
																	<input type="hidden" class="form-control" value="" name="form[int][monto][]" id="{$i.cod}" aespecifica="{$i.aespecifica}" >
																   
																	
															 {else if $i.tipo=='T'}			
																	<th>{$i.cod}</th>
                                                                    <th id >{$i.partida}</th>
																    <th>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input style="text-align:right;" type="text" class="form-control" value="{$i.monto|number_format:2:",":"."}" name="form[int][monto][]" id="{$i.cod}" aespecifica="{$i.aespecifica}">
                                                                           </div>
                                                                      </div>
																	</th>
																{else}			
                                                                    <td>{$i.cod}</td>
                                                                    <td>{$i.partida}</td>
																    <td>
																	  <div class="col-sm-12">
                                                                           <div class="form-group">
                                                                              <input style="text-align:right;" type="text" class="form-control" value="{$i.monto|number_format:2:",":"."}" name="form[int][monto][]" id="{$i.cod}" aespecifica="{$i.aespecifica}">
                                                                           </div>
                                                                      </div>
																	</td>
																	
																	 
					                                                 <td><!-- ACCION ESPECIFICA-->
					    									  
																	{if $formDB.ind_tipo_presupuesto=='P'}
																		<label width="6" type="text" disabled class="form-control" id="{$i.cod}" >{"00"|cat:$i.aespecifica}</label>	
																		<input type="hidden" value="{$i.aespecifica}" name="form[alphaNum][Aespecifica][]"  id="Aespecifica"/>	
																    {/if}

                       					  
																	</td> <!--FIN ACCION ESPECIFICA-->
																	
																	<td>{if isset($ver) || $status=='AP' || $status=='VER'} {else} <button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idPartida{$i.aespecifica|cat:'_'|cat:$i.cod|replace:".":"_"}"><i class="md md-delete" style="color: #ffffff;"></i></button>{/if}</td>			
																	
																 {/if} 
                                                           
                                                                </tr>
															  {/foreach}	
                                                           
														 {/if}	
                                                       <!--  {/if} -->
                                                        </tbody>
														
														 <!-- Total  -->
														<tr>
														   <td align="right"  style="font-weight:bold" colspan="2">
														      TOTAL=
														   </td>
														    <th id="total" align="right"  style="text-align:right; font-family:Verdana, Geneva, sans-serif; font-weight:bold">
														     <input style="text-align:right;"  type="text" class="form-control" value="{$suma_total|number_format:2:",":"."}" disabled name="monto_total" id="monto_total" />                            
															</th> 
														</tr>
														
                                                        <tfoot>
                                                        <tr>
                                                            <td colspan="4" align="center">
                                                                {if isset($ver) || $status=='AP' } {else}
                                                                    <button type="button"
                                                                            class="btn btn-primary ink-reaction btn-raised"
                                                                            id="agregarPartida"
                                                                            data-toggle="modal" data-target="#formModal2"
                                                                            data-keyboard="false" data-backdrop="static"
                                                                            titulo="Agregar Partidas"
                                                                            url="{$_Parametros.url}modPR/maestros/partidaCONTROL/PartidaMET/MODAL"
                                                                            ><span class="glyphicon glyphicon-plus"></span> Agregar Partida
                                                                    </button>
                                                                {/if}
                                                            </td>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
				  
			      </div>
				  
			   </div>
			   
           </div>
		   
		   
		    <div class="modal-footer">
				  <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>Cancelar</button>
				{if $status !='VER'}  
					 <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
						
						{if $status=='AP'}
							<span class="glyphicon glyphicon-ok"></span>Aprobar
						{elseif $status=='GE'}
							Generar
						{elseif $status=='AN'}
							<span class="glyphicon glyphicon-remove"></span> Anular	
						{elseif isset($idReformulacion)}
							<span class="glyphicon glyphicon-floppy-save"></span>Modificar
						{else}
							<span class="glyphicon glyphicon-floppy-disk"></span>Guardar
						{/if}
					</button>
				 {/if}	
            </div>
		   
		   
       </div>
		
    </div>
   
</form>

<script type="text/javascript">
    $(document).ready(function() {
        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
		
		
		 var nuevo=$(document.getElementById("idReformulacion")).val();
		    if (nuevo==0){
				 var pr = new  PrFunciones();
				 var anio = '{if isset($formDB.fec_anio)}{$formDB.fec_anio}{/if}';
				 var idPresupuesto = '{if isset($formDB.fk_prb004_num_presupuesto)}{$formDB.fk_prb004_num_presupuesto}{/if}';
				 
				 pr.metJsonPresupuesto('{$_Parametros.url}modPR/reformulacionCONTROL/JsonPresupuestoMET',anio,idPresupuesto);
		     }
		
		
		/******************************************************/
		 var disabled=$(document.getElementById("ver")).val();
		   if (disabled==1){
		       $("#formAjax input,textarea").prop("readonly", true);
			   $("#formAjax select").prop("disabled", true);
			   $("#formAjax input" ).removeClass('fechas2');
			   $("#formAjax input" ).removeClass('fecha_mes');
			   $("#formAjax input" ).removeClass('fecha_anio');
  
		   }	   
		   else if (disabled==2)
		       $("#formAjax input,textarea,select").prop("disabled", true);
		/******************************************************/ 
		
		
		$('#agregarPartida').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
				
		$('#codpartida').on('click', '.borrar', function () {
            var borrar = $(this);
			var contador=0;
			var contador2=0;
			var contador3=0;
			var valor2="";
			var programa = 0;
			var valor=borrar.attr('borrar');
			
			if (valor.length<23)
			{
			  programa=1;
              valor=valor.substring(9, 21);
			  valor='idPartida0_'+valor;
			}
			
			valor2=valor.substring(9, 14);
			valor2= valor2.concat("_00_00_00");
			
			
			valor= valor.substring(9, 17);
			valor= valor.concat("_00_00");
			
			
				$("#codpartida tr").each(function (){
					
							idpartida1 = $(this).attr('class');
							
							if (idpartida1.length<23)
			                {
								  idpartida1=idpartida1.substring(9, 21);
								  idpartida1='idPartida0_'+idpartida1;
								  
		                 	}
							
							codigo= idpartida1.substring(9, 17);
							codigo= codigo.concat("_00_00");
							
							codigo2= idpartida1.substring(9, 14);
							codigo2= codigo2.concat("_00_00_00");

						if (valor==codigo)	
						{
							contador=contador+1;
						}	
						
						if (valor2==codigo2)	
						{
							contador2=contador2+1;
						}	
					//});	
					
				});
				if (contador==2)
				{
				  if (programa==1)
				      valor='idPartida'+valor.substring(2, 14);
				  else	  
				      valor='idPartida'+valor;
					  
					  
				  $(document.getElementsByClassName(valor)).remove();
				}
				
				if (contador2==3)
				{    
				
				 if (programa==1)
				      valor2='idPartida'+valor2.substring(2, 14);
				  else	
				      valor2='idPartida'+valor2;
				  
				  $(document.getElementsByClassName(valor2)).remove();
				}
				
				$(document.getElementsByClassName(borrar.attr('borrar'))).remove();
				
				 contador3=0;
		         $("#codpartida th").each(function (){
				 $(this).find('input').each (function() {
				     
					  
					  contador3=contador3+1;    
					 // alert(contador3);
						 	 		 
	              }) //imput
			    })
							
				
				$('#codpartida').change();
        });
		
		
		$('#fk_prb004_num_presupuesto').change(function () {
		      //alert($(document.getElementById('ind_tipo_presupuesto')).val());
			  var tipo_presupuesto=$(this).val();
			
			  
			   tipo_presupuesto= tipo_presupuesto.substr(-1);
			   $(document.getElementById('ind_tipo_presupuesto')).val(tipo_presupuesto);
		});
		
		
		
		
		$('#codpartida').change(function () {
		
		var suma=0;
		var suma1='';
		var total=0;
		var codigo=0; 
		var codigo1=0; 
		var partida1='';
		var monto=0;
		var monto1=0;
		String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g,'') }
		
		  $("#codpartida th").each(function (){
		   
				 $(this).find('input').each (function() {
				     idpartida1 = $(this).attr('id');	
					 
					  aespecifica = $(this).attr('aespecifica');
					 				
					 suma=actualizarMonto(idpartida1,aespecifica);
					 $(this).val(suma).formatCurrency();

	             }) //imput
			 })	 
			 
			 
			  $("#codpartida th").each(function (){
		   
				 $(this).find('input').each (function() {
				 idpartida1 = $(this).attr('id');					  
					  codigo1= idpartida1.substring(4, 12);
					  if (codigo1=="00.00.00"){
					    monto= setNumero($(this).val());
						total=total+Number(monto);
				  }
						 	 		 
	             }) //imput
			 })	
			  
			   
			 $("#total").each(function (){
		   
				  $(this).find('input').each (function() {				  
				  $(this).val(total).formatCurrency();
				  $(document.getElementById('num_monto_reformulacion')).val(total).formatCurrency();
					
	             }) //imput
			 })	
         });
		
		 $('.fecha_mes').datepicker({ format: 'mm', viewMode: "months", minViewMode: "months", language:'es' }); 
		 $('#fec_anio').datepicker({ format: 'yyyy', viewMode: "years", minViewMode: "years", startDate: '-1y', endDate: '+1y' ,language:'es' });
		 $('#fechas').datepicker({ autoclose: true, todayHighlight: true, format: "dd-mm-yyyy", language:'es' }); 
		 $('.fechas2').datepicker({ autoclose: true, todayHighlight: true, format: "dd-mm-yyyy", language:'es' });
		 $(".monto").inputmask( "999.999.999,99",{ numericInput: true});
		
        $('#modalAncho').css("width","75%");
		
        $('#accion').click(function(){
		    swal({
                title: "ˇPor favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
		
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
			
			
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                
				}else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
					 //----------------------------------------------------
					
					
				}else if(dato['status']=='aprobado'){
                   
              
                    $(document.getElementById('idReformulacion'+dato['idReformulacion'])).html('');		
                    swal("Registro Aprobado!", "La Reformulaci&oacute;n fue aprobada satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
					
					//----------------------------------------------------
					
			   }else if(dato['status']=='anulado'){
                   
					$(document.getElementById('idReformulacion'+dato['idReformulacion'])).html('');		
                    swal("Registro Generado!", "La Reformulacion fue anulada satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
					
					//------------------------------------------------------
					
				}else if(dato['status']=='existe'){
                   
					$(document.getElementById('idReformulacion'+dato['idReformulacion'])).html('');		
                    swal("Registro procesado con errores!", "Algunas de las Partidas ya existen en el Presupuesto y no se guardaron.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
					//------------------------------------------------------
					
					
               }else if(dato['status']=='modificar'){
                   
                     if(dato['ind_estado']=='PR' ){ var estado='Preparaci&oacuten' }else if (dato['ind_estado']=='RV'){ var estado='Revisado' } else if (dato['ind_estado']=='AP'){ var estado='Aprobado' } else { var estado='Preparaci&oacuten' }
                    $(document.getElementById('idReformulacion'+dato['idReformulacion'])).html('<td>'+dato['cod_reformulacion']+'</td>' +
                            '<td>'+dato['fec_anio']+'</td>' +
							'<td>'+dato['fec_mes']+'</td>' +
							'<td>'+dato['fec_reformulacion']+'</td>' +
                            '<td align="right">'+dato['num_monto_reformulacion']+'</td>' +
							'<td>'+estado+'</td>' +
                            '<td>' +
                            '{if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idReformulacion="'+dato['idReformulacion']+'"' +
                            'descipcion="El Usuario a Modificado una Reformulaci&oacute;n" titulo="Modificar Reformulaci&oacute;n">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idReformulacion="'+dato['idReformulacion']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado una Reformulaci&oacute;n" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la  Reformulación!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Modificado!", "La Reformulación fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
					
                }else if(dato['status']=='nuevo'){
                   
                     if(dato['ind_estado']=='PR' ){ var estado='Preparaci&oacuten' }else if (dato['ind_estado']=='RV'){ var estado='Revisado' } else if (dato['ind_estado']=='AP'){ var estado='Aprobado' } else { var estado='Preparaci&oacuten' }
                    $(document.getElementById('datatable1')).append('<tr  id="idReformulacion'+dato['idReformulacion']+'">' +
                            '<td>'+dato['cod_reformulacion']+'</td>' +
                            '<td>'+dato['fec_anio']+'</td>' +
							'<td>'+dato['fec_mes']+'</td>' +
							'<td>'+dato['fec_reformulacion']+'</td>' +
                            '<td align="right">'+dato['num_monto_reformulacion']+'</td>' +
							'<td>'+estado+'</td>' +
                            '<td>' +
                            '{if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idReformulacion="'+dato['idReformulacion']+'"' +
                            'descipcion="El Usuario a Modificado una Reformulacion" titulo="Modificar Reformulacion">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idReformulacion="'+dato['idReformulacion']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado un Reformulaci&oacute;n" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la el Reformulacion!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Creado!", "La Reformulación fue creada satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
    });

	
function actualizarMonto(CodPartida,aespecifica1) {
var suma=0;
		var suma=0; 
		var codigo=""; 
		var codigo1=""; 
		var id_campo="";
		

   $("#codpartida td").each(function (){
		   
				 $(this).find('input').each (function() {
				 
				     var presupuesto = $(document.getElementById('ind_tipo_presupuesto')).val().trim(); 
					
				    // var aesp_nueva = $(document.getElementById('Aespecifica')).val(); 
					 aespecifica = $(this).attr('aespecifica');
					 
				     idpartida1 = $(this).attr('id');
					 
					 valor= $(this).val();  
					// $(this).val(valor).formatCurrency();
					
					id_campo= $(this).attr('id').trim();
					
			    	if (id_campo!="Aespecifica")
					{
					   $(this).val(0).formatCurrency();
					 }  
					 
					 if(presupuesto=="P") 
				  {
				     codigo= idpartida1.substring(0, 6);
					 codigo= codigo.concat(".00.00");
					 
					 //alert (codigo+' '+aespecifica1+' '+aespecifica);
					 
					 				 
					  if (CodPartida==codigo && aespecifica1==aespecifica){
					     monto= setNumero($(this).val());
						 suma=suma+Number(monto);
						 }
					 
					 
					 codigo1= idpartida1.substring(0, 3);
					 codigo1= codigo1.concat(".00.00.00");
					 
					  if (CodPartida==codigo1 && aespecifica1==aespecifica){
						 monto= setNumero($(this).val());
						 suma=suma+Number(monto);
						 }
						 
						 
					 codigo2= idpartida1.substring(0, 9);
					 codigo2= codigo2.concat(".00");
					 
					  if (CodPartida==codigo2 && aespecifica1==aespecifica){
						 monto= setNumero($(this).val());
						 suma=suma+Number(monto);
					 }
				  }
				  else
				  {
				     codigo= idpartida1.substring(0, 6);
					 codigo= codigo.concat(".00.00");
					 
					 
				      if (CodPartida==codigo){
					    monto= setNumero($(this).val());
					    suma=suma+Number(monto);
					 }
					 
					 
					  codigo1= idpartida1.substring(0, 3);
					  codigo1= codigo1.concat(".00.00.00");
					 
					  if (CodPartida==codigo1){
					     monto= setNumero($(this).val());
						 suma=suma+Number(monto);
					  }
						 
						 
					   codigo2= idpartida1.substring(0, 9);
					   codigo2= codigo2.concat(".00");
					 
					   if (CodPartida==codigo2 ){
						 monto= setNumero($(this).val());
						 suma=suma+Number(monto);
					  }
				  }
					 
	             }) //imput
			 })	
			 
			 return suma; 
}


//	funcion para convertir un numero frmateado en su valor real
function setNumero(num_formateado) {
	var num = num_formateado.toString();
	num = num.replace(/[.]/gi, "");
	num = num.replace(/[,]/gi, ".");
	
	var numero = new Number(num);
	return numero;
}

	
</script>

