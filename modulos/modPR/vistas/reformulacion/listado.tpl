<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Reformulacion - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Cod. Reformulacion</th>
                                <th>A&ntilde;o Ref</th>
								<th>Mes</th>
								<th>Fecha Ref</th>
								<th>Monto</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=reformulacion from=$listado}
                                <tr id="idReformulacion{$reformulacion.pk_num_reformulacion_presupuesto}">
                                    <td><label>{$reformulacion.cod_reformulacion}</label></td>
                                    <td><label>{$reformulacion.fec_anio}</label></td>
									<td><label>{$reformulacion.fec_mes}</label></td>
									<td><label>{$reformulacion.fec_reformulacion|date_format:" %d-%m-%Y"}</label></td>
									<td align="right"><label>{$reformulacion.num_monto_reformulacion|number_format:2:",":"."}</label></td>
                                    <td>
									
                                     <label>{if $reformulacion.ind_estado=='PR'}Preparaci&oacute;n{else if $reformulacion.ind_estado=='AP'}Aprobado{else if     
									 $reformulacion.ind_estado=='GE'}Generado{else if $reformulacion.ind_estado=='AN'}Anulado{/if}</label>
                                    </td>
                                    <td >
									 {if !$status}
                                       {if $reformulacion.ind_estado=='PR'}
                                            {if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}
                                             <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idReformulacion="{$reformulacion.pk_num_reformulacion_presupuesto}"
                                                    descipcion="El Usuario a Modificado una Reformulaci&oacute;n del sistema" titulo="Modificar Reformulaci&oacute;n" 
													title="Modificar Reformulaci&oacute;n">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                             </button>
                                            {/if}
											
										    {if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idReformulacion=  
											"{$reformulacion.pk_num_reformulacion_presupuesto}"  boton="si, Eliminar"
                                                    descipcion="El usuario a eliminado una Reformulacion del sistema" titulo="Estas Seguro?" mensaje="Estas seguro que desea 
													eliminar el Reformulacion!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                            {/if}
											
                                      {/if}
									  
									
										{elseif $status=='PR'}
                                         {if in_array('PR-01-02-01-04-R',$_Parametros.perfil)}
                                            <button class="aprobar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idReformulacion="{$reformulacion.pk_num_reformulacion_presupuesto}" 
													title="Aprobar Reformulaci&oacute;n" descipcion="El Usuario ha aprobado la Reformulacion" titulo="Aprobar Reformulaci&oacute;n"> 
												<i class="fa fa-check" style="color: #ffffff;"></i>
                                            </button>
											
											
											<button class="anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idReformulacion="{$reformulacion.pk_num_reformulacion_presupuesto}" 
													title="Anular Reformulaci&oacute;n" descipcion="El Usuario generaro la reformulacion" titulo="Anular la Reformulaci&oacute;n"> 
												<i class="md md-not-interested" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
										
							  
									 
                                      {/if}
									  
									   {if $reformulacion.ind_estado=='AP'}
									       {if in_array('PR-01-02-01-04-R',$_Parametros.perfil)}
                                                 <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                  data-keyboard="false" data-backdrop="static"idReformulacion="{$reformulacion.pk_num_reformulacion_presupuesto}" 
												  title="Ver Reformulacion"
                                                  descipcion="El Usuario esta viendo una Reformulacion" titulo="<i class='icm icm-calculate2'></i> Ver Reformulacion">
                                                 <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                                 </button>
											{/if}
										 {/if}
                                      
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="7">
							<div>
                                {if in_array('PR-01-02-01-01-N',$_Parametros.perfil) && $status!='PR'}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario a creado una Reformulacion"  titulo="Crear Reformulacion" id="nuevo" >
                                        Nueva Reformulacion &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-create"></i>
                                    </button>
                                {/if}
							</div>	
                            </th>
							
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modPR/reformulacionCONTROL/crearModificarMET';
       
	   $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idReformulacion:0 ,status:'PR'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
				

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idReformulacion: $(this).attr('idReformulacion'), status:'PR'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
				
		$('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idReformulacion: $(this).attr('idReformulacion'), status:'VER'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		
		
		$('#datatable1 tbody').on( 'click', '.aprobar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idReformulacion: $(this).attr('idReformulacion'), status:'AP'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		
		
		$('#datatable1 tbody').on( 'click', '.generar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idReformulacion: $(this).attr('idReformulacion'), status:'GE'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
      
	  
        $('#datatable1 tbody').on( 'click', '.anular', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idReformulacion: $(this).attr('idReformulacion'), status:'AN'},function($dato){
                $('#ContenidoModal').html($dato);
            });
         });
	  
		
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idReformulacion=$(this).attr('idReformulacion');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modPR/reformulacionCONTROL/eliminarMET';
                $.post($url, { idReformulacion: idReformulacion },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idReformulacion'+dato['idReformulacion'])).html('');
                        swal("Eliminado!", "El Reformulacion fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>