<!--********************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Presupuesto
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           Liduvica Bastardo            |          liduvica@hotmail.com       |         04249080200            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-12-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
************************************************************************************************************************
 -->
<div class="card">
    <div class="card-head">
        <header> <h2 class="text-primary">PARTIDAS PRESUPUESTARIA </h2></header>
    </div>
    <div class="col-md-12">
        <h5>Reporte Partidas Presupuestarias</h5>
    </div><!--end .col -->

</div><!--end .card -->

<div class="card   ">
    <div class="card-head">
        <header>Búsqueda</header>
    </div>


        <form   class="form" action="{$_Parametros.url}modPR/reporteCONTROL/partidasReportePdfMET"   method="post" target="iReporte">
        <div class="col-lg-12">
           
			 
            <div class="col-sm-2">
               <div class="form-group floating-label">
                        <input type="text" class="form-control dirty" value="{if isset($formDB.cod_partida)}{$formDB.cod_partida}{/if}" name="form[txt][cod_partida]" 
						id="cod_partida">
                        <label for="cod_partida">Código Partida</label>
               </div>
             </div>

</div>
<div align="center" class="form-group  col-lg-12 ">

    <button type="submit" class="btn ink-reaction btn-raised btn-info" id="btnConsultar" style="margin-top:7px">Buscar</button>
    </button>
        </form>
</div>

</div><!--end .card -->

<div class="card">
    <div align="center" style="width:100%; background:#66CCCC">&nbsp;&nbsp;<font color="#FFFFFF"><b>Listado de Partidas</b></font></div>
    <div id="contenidoReporteGeneral">
          <center><iframe name="iReporte" id="iReporte" style="border:solid 1px #CDCDCD; width:1000px; height:400px;"></iframe></center>
    </div>
</div>	

<section class="style-default-bright">




    <script type="text/javascript">

        $(document).ready(function() {
            $('#modalAncho').css("width", "85%");
            $('#fechas').datepicker({ format: 'dd/mm/yyyy' });
			$('#fec_anio').datepicker({ format: 'yyyy', viewMode: "years", minViewMode: "years" }); 

        });
    </script>


