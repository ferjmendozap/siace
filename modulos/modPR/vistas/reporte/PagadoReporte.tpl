<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Presupuesto
* PROCESO: Reporte Pagado
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           Liduvica Bastardo            |        liduvica@hotmail.com         |         04249080200            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div class="card">
    <div class="card-head">
        <header> <h2 class="text-primary">PAGADOS</h2></header>
    </div>
    <div class="col-md-12">
        <h5>Reporte Pagados</h5>
    </div><!--end .col -->

</div><!--end .card -->

<div class="card   ">
    <div class="card-head">
        <header>Búsqueda</header>
    </div>


        <form   class="form" action="{$_Parametros.url}modPR/reporteCONTROL/pagadoReportePdfMET"   method="post" target="iReporte">
		
		<div class="col-lg-4">
			 <div class="form-group floating-label">
                        <select  id="fk_a001_num_organismo"  {if isset($veranular)}disabled{/if}  name="form[int][fk_a001_num_organismo]" class="form-control dirty" >
                            
                            {foreach item=app from=$organismoint}
                                {if isset($formDB.fk_a001_num_organismo)}
                                    {if $app.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                        <option selected value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                        {else}
                                        <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                {/if}
                            {/foreach}                        
						</select>
                        <label for="fk_a001_num_organismo"><i class="md md-event"></i>&nbsp;Organismo</label>
                </div>
		  </div> 
		  
		  <div class="col-sm-2">
             <div class="form-group floating-label">
                        <input type="text" class="form-control dirty" value="{if isset($formDB.fec_anio)}{$formDB.fec_anio}{else}{'Y'|date}{/if}" name="form[txt][fec_anio]" id="fec_anio">
                        <label for="fec_anio">A&ntilde;o</label>
              </div>
         </div>

		
         <div class="col-sm-2">
            <div class="form-group">
                <select id="idpresupuesto" class="form-control" name="form[int][idpresupuesto]">
                    <option value=""> </option>
                    {foreach item=tipo from=$_PresupuestoPost}

                            <option value="{$tipo.pk_num_presupuesto}"> {$tipo.ind_cod_presupuesto}</option>

                    {/foreach}
                </select>
                <label for="select1">Presupuesto</label>
            </div>
		 </div>	
			
       

<div align="center" class="form-group col-lg-12 ">

    <button type="submit" class="btn ink-reaction btn-raised btn-info" id="btnConsultar" style="margin-top:7px">Buscar</button>
    </button>
        </form>
</div>

</div><!--end .card -->

<section class="style-default-bright">

<div class="card">
    <div align="center" style="width:100%; background:#66CCCC">&nbsp;&nbsp;<font color="#FFFFFF"><b>Listado Pagados</b></font></div>
    <div id="contenidoReporteGeneral">
          <center><iframe name="iReporte" id="iReporte" style="border:solid 1px #CDCDCD; width:1000px; height:400px;"></iframe></center>
    </div>
</div>	


    <script type="text/javascript">

        $(document).ready(function() {
		 
		 	var pr = new  PrFunciones();
	        var anio = '{if isset($formDB.fec_anio)}{$formDB.fec_anio}{else}{'Y'|date}{/if}';
            pr.metJsonPresupuestoReporte('{$_Parametros.url}modPR/reporteCONTROL/JsonPresupuestoReporteMET',anio);
		
            $('#modalAncho').css("width", "85%");
            $('#fechas').datepicker({ format: 'dd/mm/yyyy' });
			$('#fec_anio').datepicker({ format: 'yyyy', viewMode: "years", minViewMode: "years" }); 

        });
    </script>


