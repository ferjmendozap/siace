<div class="card   ">
    <div class="card-head">
        <header></header>
    </div>

<form action="{$_Parametros.url}modPR/reporteCONTROL/cierremesReportePdf2MET" autocomplete="off" id="formAjax" class="form"  method="post" target="iReporte">
    <div class="modal-body">
	    <input type="hidden" value="1" name="valido" />
	   
	    {if $status =='AP' || $status =='GE'}
            <input type="hidden" value="1" name="ver" id="ver" />
       {/if}
	   
	   {if $status =='VER'}
            <input type="hidden" value="2" name="ver" id="ver" />
       {/if}
	   
	    <input type="hidden" value="{$status}" name="status" />
		
        <input type="hidden" value="{$idCierreMes}" name="idCierreMes" id="idCierreMes"/>
		
		 <div>
        
        
  <!-- Tab panes -->
           
        
		   <div class="col-sm-4">
                    <div class="form-group floating-label" id="fk_prb004_num_presupuestoError">
                        <select id="fk_prb004_num_presupuesto" name="form[int][fk_prb004_num_presupuesto]" class="form-control">
                            {foreach item=app from=$presupuesto}
                                {if isset($formDB.fk_prb004_num_presupuesto)}
                                    {if $app.pk_num_presupuesto==$formDB.fk_prb004_num_presupuesto}
                                        <option selected value="{$app.pk_num_presupuesto}">{$app.ind_cod_presupuesto}</option>
                                        {else}
                                        <option value="{$app.pk_num_presupuesto}">{$app.ind_cod_presupuesto}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_presupuesto}">{$app.ind_cod_presupuesto}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb004_num_presupuesto">Seleccione Presupuesto</label>
                   </div>
            </div>
 
		 <div class="row">
		     <div class="col-sm-2">
                   <div class="form-group floating-label" id="fec_anioError">
                        <input type="text" class="form-control" value="{if isset($formDB.fec_anio)}{$formDB.fec_anio}{else}{'Y'|date}{/if}" name="form[alphaNum][fec_anio]" id="fec_anio">
                        <label for="fec_anio">Anio</label>
                    </div>
                 </div>
				 
				 <div class="col-sm-2">
                    <div class="form-group floating-label" id="fec_mesError">
                        <input type="text" class="form-control" value="{if isset($formDB.fec_mes)}{$formDB.fec_mes}{else}{'m'|date}{/if}" name="form[alphaNum][fec_mes]" id="fec_mes">
                        <label for="fec_mes">Mes</label>
                    </div>
                 </div>
		      
				 				
		 </div>
						
        <span class="clearfix"></span>
		
		
					 <!------------------------------------------------->
					 <div align="center" class="form-group   col-lg-12 ">		
					<!--	  <button type="submit" class="btn ink-reaction btn-raised btn-info" id="btnConsultar" style="margin-top:7px">Buscar</button>
						 </button>	-->
						  <button type="button" class="btn ink-reaction btn-raised btn-info" id="precierre" style="margin-top:7px">Buscar</button>
						 </button>
						 
					 </div>	 
		
			   
				   <div class="modal-footer">
					   <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss=
					   "modal">Cancelar</button>
					  
					  {if ($status!='VER')}  
						  <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
							  Cerrar Periodo
						  </button>	  
						  
					  {/if}
				   </div>
				   
 </div>
             
		  
			   </div>
			   
           </div>
</form>

<div class="card">
    <div align="center" style="width:100%; background:#66CCCC">&nbsp;&nbsp;<font color="#FFFFFF"><b>Cierre Mensual</b></font></div>
    <div id="contenidoReporteGeneral">
          <center><iframe name="iReporte" id="iReporte" style="border:solid 1px #CDCDCD; width:1000px; height:400px;"></iframe></center>
    </div>	
	
</div>

<section class="style-default-bright">

<script type="text/javascript">
    $(document).ready(function() {
        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
		
		
		/***************************DESHABILITAR CONTROLES*****************************
		   var disabled=$(document.getElementById("ver")).val();
		   if (disabled==1){
		       $("#formAjax input,textarea").prop("readonly", true);
			   $("#formAjax select").prop("disabled", true);
			   $("#formAjax input" ).removeClass('fechas2');
			   $("#formAjax input" ).removeClass('fecha_anio');
			   
			   //$("#formAjax" ).removeClass('hasDatepicker')
  
		   }	   
		   else if (disabled==2){
		       $("#formAjax input,textarea,select").prop("disabled", true);
		   }
	   ********************************************************************************/	 
		
		  var nuevo=$(document.getElementById("idCierreMes")).val();
		   
		   if (nuevo==0){
	      	 var pr = new  PrFunciones();
			 
				 var anio = '{if isset($formDB.fec_anio)}{$formDB.fec_anio}{else}{'Y'|date}{/if}';
				 var idPresupuesto = '{if isset($formDB.fk_prb004_num_presupuesto)}{$formDB.fk_prb004_num_presupuesto}{/if}';
				 
				 pr.metJsonPresupuesto('{$_Parametros.url}modPR/cierremesCONTROL/JsonPresupuestoMET',anio,idPresupuesto);
		  }	
		
		
	
		  var disabled=$(document.getElementById("ver")).val();
		  if (disabled==1){
		     $("#formAjax input,textarea,select").prop("disabled", true);
		  }	 
			 
		 $('#fec_mes').datepicker({ format: 'mm', viewMode: "months", minViewMode: "months" }); 
		 $('#fec_anio').datepicker({ format: 'yyyy', viewMode: "years", minViewMode: "years", startDate: '-1y', endDate: '+1y' ,language:'es' });
		 $(".monto").inputmask( "999.999.999,99",{ numericInput: true});
            
 		  
		
        $('#modalAncho').css("width","85%");
		
				
        $('#precierre').click(function(){
		//alert($("#formAjax").attr("action"));
		     var $url='{$_Parametros.url}modPR/cierremesCONTROL/crearModificarPreCierreMET';
            $.post($url, $( "#formAjax" ).serialize(),function(dato){
                
				if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
					 //----------------------------------------------------
					
					 }else if(dato['status']=='error_cierre'){
                       // swal("ERROR!", "El periodo ya fue cerrado", "error");
					    var $url='{$_Parametros.url}modPR/reporteCONTROL/cierremesReportePdf2MET';
						 document.getElementById("formAjax").submit();
						
					 //----------------------------------------------------
					 
					 }else if(dato['status']=='error_cierre2'){
					  var $url='{$_Parametros.url}modPR/reporteCONTROL/cierremesReportePdf2MET';
						 document.getElementById("formAjax").submit();
					     swal("ERROR!", "El periodo anterior no ha sido cerrado", "error");
					 //----------------------------------------------------
					
					
					 }else if(dato['status']=='nuevo'){
					  var $url='{$_Parametros.url}modPR/reporteCONTROL/cierremesReportePdf2MET';
						 document.getElementById("formAjax").submit();
					  //   swal("ERROR!", "Nuevo");
					 //----------------------------------------------------
					

                }
				
            },'json');
       });
		
		
        $('#accion').click(function(){
		//alert($("#formAjax").attr("action"));
		     var $url='{$_Parametros.url}modPR/cierremesCONTROL/crearModificarMET';
            $.post($url, $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
					 //----------------------------------------------------
					
					 }else if(dato['status']=='error_cierre'){
                        swal("ERROR!", "El periodo ya fue cerrado", "error");
					 //----------------------------------------------------
					 
					 }else if(dato['status']=='error_cierre2'){
					    swal("ERROR!", "El periodo anterior no ha sido cerrado", "error");
					 //----------------------------------------------------
					
					  }else if(dato['status']=='aprobado'){
                   
                    //if(dato['ind_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('idCierreMes'+dato['idCierreMes'])).html('<td>'+dato['pk_num_ejecucion_presupuestaria']+'</td>' +
                            '<td>'+dato['fec_anio']+'</td>' +
                            '<td>'+dato['num_disponibilidad_presupuestaria']+'</td>' +
							'<td>'+dato['num_disponibilidad_financiera']+'</td>' +
                            '<td>' +
                            '{if in_array('PR-01-07-01-L',$_Parametros.perfil)}
							<button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idCierreMes="'+dato['idCierreMes']+'"' +
                            'descipcion="El Usuario esta consultando un Cierre Mensual" titulo="Ver Cierre Mensual">' +
                            '<i class="md md-remove-red-eye" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Aprobado!", "El Cierre Mensual fue aprobado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
					
					
					//----------------------------------------------------
               }else if(dato['status']=='nuevo'){
                   
                    //if(dato['ind_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('datatable1')).append('<tr  id="idCierreMes'+dato['idCierreMes']+'">' +
                            '<td>'+dato['ind_cod_presupuesto']+'</td>' +
                            '<td>'+dato['fec_anio']+'-'+dato['fec_mes']+'</td>' +
                            '<td>'+dato['num_disponibilidad_presupuestaria']+'</td>' +
							'<td>'+dato['num_disponibilidad_financiera']+'</td>' +
                            '<td>' +
							'<button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idCierreMes="'+dato['idCierreMes']+'"' +
                            'descipcion="El Usuario esta consultando un Cierre Mensual" titulo="Ver Cierre Mensual">' +
                            '<i class="md md-remove-red-eye" style="color: #ffffff;"></i></button>' +
                           
							' <button title="Reporte Cierre Mensual"' +
                            'class="generar logsUsuario btn ink-reaction btn-raised btn-xs btn-info"' +
                            'titulo="Ver Cierre Mensual Pdf" id="generar">' +
                            ' <i class="md md-assignment"></i>' +
                            '</td>'
							);
                    swal("Registro Cerrado!", "El Cierre Mensual fue realizado satisfactoriamente.", "success");
                   // $(document.getElementById('cerrarModal')).click();
                    //$(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
    });

	

</script>

