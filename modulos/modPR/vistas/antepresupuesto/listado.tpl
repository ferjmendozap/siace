<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Antepresupuesto - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Cod. Antepresupuesto</th>
                                <th>Ejer. Presupuestario</th>
								<th>Fecha Inicio</th>
								<th>Fecha Fin</th>
								<th>Monto</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
						
                        <tbody>
                            {foreach item=antepresupuesto from=$listado}
                                <tr id="idAntepresupuesto{$antepresupuesto.pk_num_antepresupuesto}">
                                    <td><label>{$antepresupuesto.cod_antepresupuesto}</label></td>
                                    <td><label>{$antepresupuesto.fec_anio}</label></td>
									<td><label>{$antepresupuesto.fec_inicio|date_format:" %d-%m-%Y"}</label></td>
									<td><label>{$antepresupuesto.dec_fin|date_format:" %d-%m-%Y"}</label></td>
									<td><label>{$antepresupuesto.num_monto_presupuestado|number_format:2:",":"."}</label></td>
                                    <td>
									  <!--<label>{$antepresupuesto.ind_estado}</label>-->
                                    <label>{if $antepresupuesto.ind_estado=='PR'}Preparaci&oacute;n{else if $antepresupuesto.ind_estado=='AP'}Aprobado{else if     
									 $antepresupuesto.ind_estado=='GE'}Generado{else if $antepresupuesto.ind_estado=='RV'}Revisado{else if $antepresupuesto.ind_estado=='AN'}Anulado{/if}</label>
                                    </td>
                                    <td>
									 {if !$status}
                                      
									   {if $antepresupuesto.ind_estado=='PR'}
                                              {if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}
                                               <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idAntepresupuesto="{$antepresupuesto.pk_num_antepresupuesto}"  title="Modificar Antepresupuesto"
                                                    descipcion="El Usuario a Modificado un antepresupuesto del sistema" titulo="Modificar antepresupuesto">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                               </button>
                                             {/if}
											
										     {if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}
                                               <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idAntepresupuesto=  
											       "{$antepresupuesto.pk_num_antepresupuesto}" title="Eliminar Antepresupuesto" boton="si, Eliminar"
                                                    descipcion="El usuario a eliminado un antepresupuesto del sistema" titulo="Estas Seguro?" mensaje="Estas seguro que desea 
													eliminar el Antepresupuesto!!">
                                                  <i class="md md-delete" style="color: #ffffff;"></i>
                                               </button>
                                             {/if}
									
                                        {/if}
										
										   {if $antepresupuesto.ind_estado=='AP' || $antepresupuesto.ind_estado=='AN' || $antepresupuesto.ind_estado=='GE' ||     
								   $antepresupuesto.ind_estado=='RV'}
										       {if in_array('PR-01-02-01-07-V',$_Parametros.perfil)}
													 <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
													  data-keyboard="false" data-backdrop="static" idAntepresupuesto="{$antepresupuesto.pk_num_antepresupuesto}"
													  title="Ver Antepresupuesto"
													  descipcion="El Usuario esta viendo un Antepresupuesto" titulo="<i class='icm icm-calculate2'></i> Ver Antepresupuesto">
													 <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
													 </button>
                                               {/if}
										     {/if}
										
										
									 {elseif $status=='PR'}
                                         {if in_array('PR-01-02-01-04-R',$_Parametros.perfil)}
                                            <button class="revisar logsUsuario btn ink-reaction btn-raised btn-xs btn-success" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idAntepresupuesto="{$antepresupuesto.pk_num_antepresupuesto}" 
													title="Revisar Antepresupuesto" descipcion="El Usuario Revisado el Antepresupuesto" titulo="Revisar Antepresupuesto"> 
												<i class="fa fa-check-circle-o" style="color: #ffffff;"></i>
                                            </button>
											
											<button class="anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idAntepresupuesto="{$antepresupuesto.pk_num_antepresupuesto}" 
													title="Anular Antepresupuesto" descipcion="El Usuario generar el Antepresupuesto" titulo="Anular Antepresupuesto"> 
												<i class="md md-not-interested" style="color: #ffffff;"></i>
                                            </button>
                                         {/if}
										
										
										 
										{elseif $status=='RV'}
                                         {if in_array('PR-01-02-01-07-V',$_Parametros.perfil)}
                                            <button class="aprobar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idAntepresupuesto="{$antepresupuesto.pk_num_antepresupuesto}" 
													title="Revisar Antepresupuesto" descipcion="El Usuario a aprobado un Antepresupuesto" titulo="Aprobar Antepresupuesto"> 
												<i class="fa fa-check" style="color: #ffffff;"></i>
                                            </button>
											
											<button class="reversar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idAntepresupuesto="{$antepresupuesto.pk_num_antepresupuesto}" 
													title="Reversar Antepresupuesto" descipcion="El Usuario eta Reversando el Antepresupuesto" titulo="Reversar Antepresupuesto"> 
												<i class="fa fa-undo" style="color: #ffffff;"></i>
                                            </button>
                                         {/if}
                                     
									  
									  {elseif $status=='AP'}
                                        {if in_array('PR-01-02-01-06-G',$_Parametros.perfil)}
                                            <button class="generar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idAntepresupuesto="{$antepresupuesto.pk_num_antepresupuesto}" 
													title="Generar Proyecto" descipcion="El Usuario esta generando el Antepresupuesto" titulo="Generar Antepresupuesto"> 
												<i class="md md-sync" style="color: #ffffff;"></i>
                                            </button>
											
											<button class="reversar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idAntepresupuesto="{$antepresupuesto.pk_num_antepresupuesto}" 
													title="Reversar Antepresupuesto" descipcion="El Usuario esta reversando el Antepresupuesto" titulo="Reversar Antepresupuesto"> 
												<i class="fa fa-undo" style="color: #ffffff;"></i>
                                            </button>

                                        {/if}
                                   {/if}
								   
								   
								  
                                </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="7">
							<div>
							  {if $status!='AP' && $status!='GE' && $status!='PR' && $status!='RV'  } 
                                {if in_array('PR-01-02-01-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario a creado una antepresupuesto del sistema" title="Nuevo Antepresupuesto" titulo="Crear Antepresupuesto" id="nuevo" >
                                        Nuevo Antepresupuesto &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-create"></i>
                                    </button>
                                {/if}
							  {/if}
							  </div>	
                            </th>
							
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
	
	
</section>



<script type="text/javascript">
    $(document).ready(function() {
      
	    var $url='{$_Parametros.url}modPR/antepresupuestoCONTROL/crearModificarMET';
		
    	   
	   $('#nuevo').click(function(){
	       $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idAntepresupuesto:0 ,status:'PR'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
				

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idAntepresupuesto: $(this).attr('idAntepresupuesto'), status:'PR'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		
		
		$('#datatable1 tbody').on( 'click', '.aprobar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idAntepresupuesto: $(this).attr('idAntepresupuesto'), status:'AP'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		
		
		$('#datatable1 tbody').on( 'click', '.revisar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idAntepresupuesto: $(this).attr('idAntepresupuesto'), status:'RV'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		
		
		$('#datatable1 tbody').on( 'click', '.anular', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idAntepresupuesto: $(this).attr('idAntepresupuesto'), status:'AN'},function($dato){
                $('#ContenidoModal').html($dato);
            });
         });
		
		
		$('#datatable1 tbody').on( 'click', '.reversar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idAntepresupuesto: $(this).attr('idAntepresupuesto'), status:'RE'},function($dato){
                $('#ContenidoModal').html($dato);
            });
         });
		
		
		$('#datatable1 tbody').on( 'click', '.generar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idAntepresupuesto: $(this).attr('idAntepresupuesto'), status:'GE'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		
		
		$('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idAntepresupuesto: $(this).attr('idAntepresupuesto'), status:'VER'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
      
		
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idAntepresupuesto=$(this).attr('idAntepresupuesto');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modPR/antepresupuestoCONTROL/eliminarMET';
                $.post($url, { idAntepresupuesto: idAntepresupuesto },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idAntepresupuesto'+dato['idAntepresupuesto'])).html('');
                        swal("Eliminado!", "El Antepresupuesto fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>