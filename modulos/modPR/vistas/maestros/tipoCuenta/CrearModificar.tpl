<form action="{$_Parametros.url}modPR/maestros/tipoCuentaCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idCuenta) }
            <input type="hidden" value="{$idCuenta}" name="idCuenta"/>
        {/if}
        <div class="row">
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="cod_tipo_cuentaError">
                        <input type="text" class="form-control" value="{if isset($formDB.cod_tipo_cuenta)}{$formDB.cod_tipo_cuenta}{/if}" name="form[alphaNum][cod_tipo_cuenta]" id="cod_tipo_cuenta">
                        <label for="cod_tipo_cuenta">Cod Tipo Cuenta</label>
                    </div>
                </div>
				
                 <div class="col-sm-12">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked  {elseif  isset($formDB.num_estatus) and $formDB.num_estatus==0}} {else} checked {/if} value="1" name="form[int][num_estatus]">
                            <span>Estatus</span>
                        </label>
                    </div>
                </div>

            </div>
            <div class="col-sm-7">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="ind_descripcionError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}" name="form[alphaNum][ind_descripcion]" id="ind_descripcion">
                        <label for="ind_descripcion"><i class="icm icm-cog3"></i> Descripcion</label>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="cod_procesoError">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
        $('#modalAncho').css("width","60%");
        $('#accion').click(function(){
		    swal({
                title: "�Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                   
                    if(dato['num_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('idCuenta'+dato['idCuenta'])).html('<td>'+dato['cod_tipo_cuenta']+'</td>' +
                            '<td>'+dato['ind_descripcion']+'</td>' +
                            '<td><i class="'+icono+'"></i></td>' +
                            '<td  align="left">' +
                            '{if in_array('PR-01-01-02-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idCuenta="'+dato['idCuenta']+'"' +
                            'descipcion="El Usuario a Modificado un Cuenta de Nomina" titulo="Modificar Cuenta">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;' +

                            '{if in_array('PR-01-01-02-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idCuenta="'+dato['idCuenta']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado un Cuenta" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la el Cuenta!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Modificado!", "El Cuenta fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }else if(dato['status']=='nuevo'){
                   
                    if(dato['num_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('datatable1')).append('<tr  id="idCuenta'+dato['idCuenta']+'">' +
                            '<td>'+dato['cod_tipo_cuenta']+'</td>' +
                            '<td>'+dato['ind_descripcion']+'</td>' +
                            '<td><i class="'+icono+'"></i></td>' +
                            '<td  align="left">' +
                            '{if in_array('PR-01-01-02-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idCuenta="'+dato['idCuenta']+'"' +
                            'descipcion="El Usuario a Modificado una Cuenta" titulo="Modificar Cuenta">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;' +
							
                            '{if in_array('PR-01-01-02-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idCuenta="'+dato['idCuenta']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado un Cuenta" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la el Cuenta!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Modificado!", "La Cuenta fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
    });
</script>