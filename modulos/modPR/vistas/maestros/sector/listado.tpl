<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Sector - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th width="70%">Descripcion</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=sector from=$listado}
                                <tr id="idSector{$sector.pk_num_sector}">
                                    <td><label>{$sector.ind_cod_sector}</label></td>
                                    <td><label>{$sector.ind_descripcion}</label></td>
                                    <td>
                                        <i class="{if $sector.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                    </td>
                                    <td align="left">
                                        {if in_array('PR-01-01-02-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idSector="{$sector.pk_num_sector}"
                                                    descipcion="El Usuario a Modificado un sector" titulo="Modificar un sector">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        &nbsp;&nbsp;
                                        {if in_array('PR-01-01-02-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idSector="{$sector.pk_num_sector}"  boton="si, Eliminar"
                                                    descipcion="El usuario a eliminado una acci&oacute;n sector" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el sector!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="4">
							<div>
                                {if in_array('PR-01-01-02-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario a creado un nuevo sector"  titulo="Crear un sector" id="nuevo" >
                                        Nuevo sector &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-create"></i>
                                    </button>
                                {/if}
							</div>
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
	

        var $url='{$_Parametros.url}modPR/maestros/sectorCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idSector:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
		 
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idSector: $(this).attr('idSector')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idSector=$(this).attr('idSector');
			
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modPR/maestros/sectorCONTROL/eliminarMET';
                $.post($url, { idSector: idSector },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idSector'+dato['idSector'])).html('');
                        swal("Eliminado!", "El Sector fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>