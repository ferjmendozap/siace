<form>
<div class="modal-body">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Check</th>
                            <th>Partida</th>
                            <th>Denominacion</th>
                            <th>Tipo</th>
                            
                        </tr>
                        </thead>
                        <tbody>
						
                        {foreach item=partida from=$listado}
                            <tr id="idPartida{$partida.partidacod}">
                                <td>
                                    <div class="checkbox checkbox-styled">
									{if $partida.ind_tipo=='T'}
                                        <label>
                                            <input type="checkbox" class="valores" disabled id="check"
											       id_patida="{$partida.id}"
                                                   idPartida="{$partida.pk_num_partida_presupuestaria}"
                                                   partida="{$partida.ind_denominacion}" cod="{$partida.cod_partida}" 
												   partidacod="{$partida.partidacod}" tipo="{$partida.ind_tipo}">
                                        </label>
									 {else}	
									    <label>
                                            <input type="checkbox" class="valores" 
											       id_partida="{$partida.id}"
                                                   idPartida="{$partida.pk_num_partida_presupuestaria}"
                                                   partida="{$partida.ind_denominacion}" cod="{$partida.cod_partida}" 
												   partidacod="{$partida.partidacod}" tipo="{$partida.ind_tipo}">
                                        </label>
									 {/if}	
                                    </div>
                                </td>
								{if $partida.ind_tipo=='T'}
                                  <td><label>{$partida.cod_partida}</label></td>
                                  <td><label>{$partida.ind_denominacion}</label></td>
                                  <td><label>{$partida.ind_tipo}</label></td>
								  
                              <!--  <td>
                                    <i class="{if $partida.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                              </td>-->
							 {else}		
							      <td><label>{$partida.cod_partida}</label></td>
                                  <td><label>{$partida.ind_denominacion}</label></td>
                                  <td><label>{$partida.ind_tipo}</label></td>
								
								 
                                <!--  <td>
                                    <i class="{if $partida.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                </td>-->
							  {/if}
							 
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
        </div>
    </div>
</div>
<span class="clearfix"></span>
<div class="modal-footer">
    <button type="button" class="btn btn-default ink-reaction btn-raised" data-dismiss="modal">Cancelar</button>
    <button type="button" class="btn btn-primary ink-reaction btn-raised" id="agregarPartidaSeleccionada">Agregar
    </button>
</div>

</form>

<script type="text/javascript">
    $(document).ready(function () {
	  
	  var p=0;
	    if (p<=0)
	  {
	    var input = $('.valores');
	    var input2 = $('.valores');
	  
	    p=9;
	  }  
	 
    
        $('#datatable2').DataTable({    
            "dom":  'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
				"processing": "Procesando ...",
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

     
		  
	    
        $('#agregarPartidaSeleccionada').click(function () {
		
		
		
		  var tipo_presupuesto= document.forms['formAjax'].elements['form[alphaNum][ind_tipo_presupuesto]'].value;
		  
		  //$(document.getElementById('ind_tipo_presupuesto')).val()
	
           // var input = $('.valores');
			var idpartida1='';
			var valor='';
			var insertar='';
			var pos='';
			var id='';
			var Aespecifica;
			
			
          for (i = 0; i < input.length; i++) {
			
			    var codigo ='';
				var codigo2 ='';
				var codigo3 ='';
				
							
                if (input[i].checked == true) {
				
				
			/*	 $("#datatable2 td").each(function (){
				     $(this).find('select').each (function() {
					 
					 if(input[i].getAttribute('cod')==$(this).attr('cod'))
					 {
					       Aespecifica=$(this).val();
						   Aesp=parseInt(Aespecifica);
						   
					  }	   
						   
					})
				  
				 })*/
				
				
		 	//--------------------------------	
				
	 for (j = 0; j < input2.length; j++) { 
				 
				     codigo= input[i].getAttribute('cod').substring(0, 6);
					 codigo= codigo.concat(".00.00");
					 
					 codigo2= input[i].getAttribute('cod').substring(0, 3);
					 codigo2= codigo2.concat(".00.00.00");
					 
					 codigo3= input[i].getAttribute('cod').substring(0, 9);
					 codigo3= codigo3.concat(".00");
					 
					 pk_partida=input[i].getAttribute('idPartida');
					 
					
		if ( (input2[j].getAttribute('cod')==codigo || input2[j].getAttribute('cod')==codigo2 || input2[j].getAttribute('cod')==codigo3) && input2[j].getAttribute('tipo')=='T')        {
		     	
		
	if (tipo_presupuesto=='P')
    {  	
	 
		partida=Aesp+'_'+input2[j].getAttribute('partidacod');
		
	   if (!$(document.getElementById(partida)).length){
		
		   insertar='append'
		  
		  
		  nueva=Aesp+'_'+input2[j].getAttribute('cod').replace(/[.]/gi, "\_");
		   
		   
		    $("#codpartida tr").each(function () 
             { 
				    idpartida1 = $(this).attr('class');
					
				if (idpartida1){
					  valor =idpartida1.substring(9).replace(".", "_", "gi");
					  
				//	if (valor.length>=11)
				//	{   
					 if ( valor>nueva){ 
					     insertar=idpartida1;
						 id=valor;
						 return false;
					  }
					  else {
					        insertar='append'
						   }
				 //}	 
			   } 
			
              }) 
				 
				 
			
				 if (insertar=='append'){
				 
				 id=Aesp+'_'+input2[j].getAttribute('partidacod');
				
                    $(document.getElementById('codpartida')).append(
                            '<tr class="idPartida' +id+'">' +
                            '<input type="hidden" value="' + input2[j].getAttribute('idPartida') + '" name="form[int][fk_prb002_num_partida_presupuestaria][]" id="'+ id +'" class="partidaInput" partida="' + input2[j].getAttribute('partida') + '" />' +
                            '<th>' + input2[j].getAttribute('cod') + '</th>' +
                            '<th>' + input2[j].getAttribute('partida') + '</th>' +
							'<th> <input type="text" style="text-align:right;" id="'+input2[j].getAttribute('cod')+'" class="form-control" value="0,00" name="form[int][monto][]" aespecifica= '+Aesp+' readonly></th>' +
                     
                            '</tr>'
                    );
					
					}else{
					id_partida_i=Aesp+'_'+input2[j].getAttribute('partidacod');
				
					 $('#'+id+'').closest("tr").before('<tr class="idPartida' +id_partida_i+ '">' +
                            '<input type="hidden" value="' + input2[j].getAttribute('idPartida') + '" name="form[int][fk_prb002_num_partida_presupuestaria][]"  id="'+ id_partida_i +'" class="partidaInput" partida="' + input2[j].getAttribute('partida') + '" />' +
                            '<th>' + input2[j].getAttribute('cod') + '</th>' +
                            '<th>' + input2[j].getAttribute('partida') + '</th>' +
							'<th> <input type="text"  style="text-align:right;"  id="'+input2[j].getAttribute('cod')+'" class="form-control" value="0,00" name="form[int][monto][]" aespecifica= '+Aesp+' readonly></th>' +
                         
                            '</tr>'); 
					
					}
					
			
	 }// fin if !
				
				
	}else{			//ELSE TIPO PRESUPUESTO
			 
	 if (!$(document.getElementsByClassName('idPartida0_' + input2[j].getAttribute('partidacod'))).length){
		
		
		   insertar='append'
		  
		   //nueva=input2[j].getAttribute('cod').replace(".", "_", "gi");  
		   
		    nueva=input2[j].getAttribute('cod').replace(/[.]/gi, "\_");
		   
		  
		    $("#codpartida tr").each(function () 
                 { 
				    idpartida1 = $(this).attr('class');
					if (idpartida1){
					  //alert(idpartida1);
					  valor =idpartida1.substring(9).replace(".", "_", "gi");
					  if ( valor>nueva){ 
					     insertar=idpartida1;
						 id='0_'+valor;
						 return false;
					  }
					  else {
					     insertar='append'
						
						 }
					 } 
			
                 }) 
				 
			 if (insertar=='append'){ 
			 
			        id= 0+'_'+input[i].getAttribute('partidacod');
					
                    $(document.getElementById('codpartida')).append(
                            '<tr class="idPartida0_' + input2[j].getAttribute('partidacod') +'" >' +
                            '<input type="hidden" value="' + input2[j].getAttribute('idPartida') + '" name="form[int][fk_prb002_num_partida_presupuestaria][]"  id="'+ id +'" class="partidaInput" partida="' + input2[j].getAttribute('partida') + '" />' +
                            '<th>' + input2[j].getAttribute('cod') + '</th>' +
                            '<th>' + input2[j].getAttribute('partida') + '</th>' +
							'<th> <input style="text-align:right;"  type="text"  id="'+input2[j].getAttribute('cod')+'" class="form-control" value="0,00" name="form[int][monto][]" readonly></th>' +
							
							 '<td> <label width="6" type="text" disabled ></label> <input type="hidden" value=" " name="blanco"  id="Aespecifica"></td>' +
							
                        <!--    '<td><button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idPartida' + input2[j].getAttribute('partidacod') + '"><i class="md md-delete" style="color: #ffffff;"></i></button></td>' +-->
                            '</tr>'
                    );
					
					}else{
					
                     id_i= 0+'_'+input[i].getAttribute('partidacod');					
					
					 $('#'+id+'').closest("tr").before('<tr class="idPartida0_' + input2[j].getAttribute('partidacod') + '" >' +
                            '<input type="hidden" value="' + input2[j].getAttribute('idPartida') + '" name="form[int][fk_prb002_num_partida_presupuestaria][]" id="'+ id_i +'" class="partidaInput" partida="' + input2[j].getAttribute('partida') + '" />' +
                            '<th>' + input2[j].getAttribute('cod') + '</th>' +
                            '<th>' + input2[j].getAttribute('partida') + '</th>' +
							'<th> <input style="text-align:right;"  type="text" readonly id="'+input2[j].getAttribute('cod')+'" class="form-control" value="0,00" name="form[int][monto][]" aespecifica= '+Aesp+'></th>' +
							
							 '<td> <label width="6" type="text" disabled ></label> <input type="hidden" value=" " name="blanco"  id="Aespecifica"></td>' +
							
                         <!--   '<td><button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idPartida' + input2[j].getAttribute('partidacod') + '"><i class="md md-delete" style="color: #ffffff;"></i></button></td>' +-->
                            '</tr>'); 
					
					}
			
			//}	//Fin tipo de presupuesto	
					
					
		    }// fin if !
			
			}//FIN ELSE TIPO PRESUPUESTO
			
                 }//fin if false
				
				}//if checked
				//------------------------------------------------
				
		 if (tipo_presupuesto=='P')
		{		
		
			 partida=Aesp+'_'+input[i].getAttribute('cod');
		
		
		       insertar='append'
		  
		       //pos=Aesp+'_'+input[i].getAttribute('cod').replace(".", "_", "gi");replace(/[.]/gi, "\_");
			   
			     pos=Aesp+'_'+input[i].getAttribute('cod').replace(/[.]/gi, "\_");
				 
			 // alert ('pos'+pos);
				 
				 $("#codpartida tr").each(function () 
                 { 
				    idpartida1 = $(this).attr('class');
					
					if (idpartida1){
					  valor =idpartida1.substring(9).replace(".", "_", "gi");
					  				
					 // alert (idpartida1);
					//	if (valor.length>=11)
					//	{  
							  if ( valor>pos){ 
							 
								 insertar=idpartida1;
								 id=valor;
								 return false;
							  }
							  else {
								 insertar='append'
								
							  }	
					    
					 } //fin if partida
			
                 }) 
          
		    
			 if (insertar=='append'){
			 
			  id= Aesp+'_'+input[i].getAttribute('partidacod');
			 
			  
                    $(document.getElementById('codpartida')).append(
                            '<tr class="idPartida' +id+'">' +
                            '<input type="hidden" value="' + input[i].getAttribute('idPartida') + '" name="form[int][fk_prb002_num_partida_presupuestaria][]" id="'+ id +'" class="partidaInput" partida="' + input[i].getAttribute('partida') + '" />' +
                            '<td>' + input[i].getAttribute('cod') + '</td>' +
                            '<td>' + input[i].getAttribute('partida') + '</td>' +
							'<td> <input type="text" style="text-align:right;" class="form-control" value="" name="form[int][monto][]" aespecifica= '+Aesp+' id="'+input[i].getAttribute('cod')+'"></td>' +
							
								'<td> <label width="6" type="text" class="form-control" >'+Aespecifica+'</label> <input type="hidden" value="'+Aesp+'" name="form[alphaNum][Aespecifica][]"  id="Aespecifica" readonly></td>' +
							
							
                            '<td><button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idPartida' + id + ' "><i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
                            '</tr>'
                    );
				 }
				 
				 else{ 
				           //id=Aesp+'.'+id;
						   id_i=Aesp+'_'+input[i].getAttribute('partidacod');
						   
						   //alert (id);
				 
                          $('#'+id+'').closest("tr").before('<tr class="idPartida' +id_i+ '">' +
                            '<input type="hidden" value="' + input[i].getAttribute('idPartida') + '" name="form[int][fk_prb002_num_partida_presupuestaria][]" id="'+ id_i +'"class="partidaInput" partida="' + input[i].getAttribute('partida') + '" />' +
                            '<td>' + input[i].getAttribute('cod') + '</td>' +
                            '<td>' + input[i].getAttribute('partida') + '</td>' +
							'<td> <input type="text" style="text-align:right;" class="form-control" value="" name="form[int][monto][]" aespecifica= '+Aesp+' id="'+input[i].getAttribute('cod')+'"  ></td>' +
							
								'<td> <label width="6" type="text" class="form-control" >'+Aespecifica+'</label> <input type="hidden" value="'+Aesp+'" name="form[alphaNum][Aespecifica][]"  id="Aespecifica" readonly></td>' +
							
							     '<td><button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idPartida' + id_i + ' "><i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
							
						
                            '</tr>'); 
				 
				 }
				  

             //   }//if false
				
				
				}else{ //ELSE TIPO DE PRESUPUESTO
				
				
				 if (!$(document.getElementsByClassName('idPartida0_' + input[i].getAttribute('partidacod'))).length){
				 
				 //---------------------------------------------
				 
				// pos=input[i].getAttribute('cod').replace(".", "_","gi");
				
				pos=input[i].getAttribute('cod').replace(/[.]/gi, "\_");
				 
				 
				 $("#codpartida tr").each(function () 
                 { 
				    idpartida1 = $(this).attr('class');
					if (idpartida1){
					  valor =idpartida1.substring(9).replace(".", "_", "gi");
					 
					  if ( valor>pos){ 
					     insertar=idpartida1;
						 id='0_'+valor;
						 return false;
					  }
					  else {
					     insertar='append'
						
						 }	 
					 } 
			
                 }) 
          
			
				 if (insertar=='append'){
				 
				 id= 0+'_'+input[i].getAttribute('partidacod');
				 
                    $(document.getElementById('codpartida')).append(
                            '<tr class="idPartida0_' + input[i].getAttribute('partidacod') + '">' +
                            '<input type="hidden" value="' + input[i].getAttribute('idPartida') + '" name="form[int][fk_prb002_num_partida_presupuestaria][]" id="'+ id +'" class="partidaInput" partida="' + input[i].getAttribute('cod') + '" />' +
                            '<td>' + input[i].getAttribute('cod') + '</td>' +
                            '<td>' + input[i].getAttribute('partida') + '</td>' +
							
							'<td> <input style="text-align:right;"  type="text" class="form-control ant" value="" name="form[int][monto][]" id="'+input[i].getAttribute('cod')+'" aespecifica="0"></td>' +
							
							 '<td> <label width="6" type="text" disabled ></label> <input type="hidden" value=" " name="blanco"  id="blanco"></td>' +
							
							
                            '<td><button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idPartida0_' + input[i].getAttribute('partidacod') + '"><i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
                            '</tr>'
                    );
				 }
				 
				 else{ 
				          //alert(id);
						  
						  id_i= 0+'_'+input[i].getAttribute('partidacod');
				 
                          $('#'+id+'').closest("tr").before('<tr class="idPartida=0_' + input[i].getAttribute('partidacod') + '">' +
                            '<input type="hidden" value="' + input[i].getAttribute('idPartida') + '" name="form[int][fk_prb002_num_partida_presupuestaria][]"  id="'+ id_i +'"  class="partidaInput" partida="' + input[i].getAttribute('cod') + '" />' +
                            '<td>' + input[i].getAttribute('cod') + '</td>' +
                            '<td>' + input[i].getAttribute('partida') + '</td>' +
							'<td> <input style="text-align:right;"  type="text" class="form-control" value="" name="form[int][monto][]" id="'+input[i].getAttribute('cod')+'"></td>' +
							
						 '<td> <label width="6" type="text" disabled ></label> <input type="hidden" value=" " name="blanco"  id="blanco"></td>' +
							
                            '<td><button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idPartida0_' + input[i].getAttribute('partidacod') + '"><i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
                            '</tr>'); 
			
				  }
				 
                }//if false
				
			  	
			    }//FIN TIPO DE PRESUPUESTO	
				
			  }// for
            }
			
            $('#ContenidoModal2').html('');
            $('#cerrarModal2').click();
        });
    });
	
	//---------------------------------------------------------------------------
	
	
</script>