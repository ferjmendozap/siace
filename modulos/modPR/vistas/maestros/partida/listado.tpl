<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Partidas - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Partida</th>
                                <th>Denominacion</th>
                                <th>Tipo</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th colspan="5">
                                    {if in_array('PR-01-01-01-01-N',$_Parametros.perfil)}
                                         <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="El Usuario a creado una partida del sistema"  titulo="Crear Partida" id="nuevo" >
                                        Nueva Partida &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-create"></i>
                                    </button>
                                    {/if}
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modPR/maestros/partidaCONTROL/CrearModificarMET';
        var pr = new PrFunciones();
        var dt = pr.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modPR/maestros/partidaCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Presupuesto/datatableSpanish.json",
                [
                    { "data": "cod_partida" },
                    { "data": "ind_denominacion"},
                    { "data": "ind_tipo" },
                    { "orderable": false,"data": "num_estatus",'width':30 },
                    { "orderable": false,"data": "acciones",'width':100 }
                ]
        );

         $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idPartida:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });


        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idPartida: $(this).attr('idPartida')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        
        $('#dataTablaJson tbody').on( 'click', '.eliminar', function () {
            var idPartida=$(this).attr('idPartida');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modPR/maestros/partidaCONTROL/eliminarMET';
                $.post($url, { idPartida: idPartida },function(dato){
                    if(dato['status']=='ok'){
                        app.metEliminarRegistroJson('dataTablaJson','"El Proceso fue eliminado satisfactoriamente.','cerrarModal','ContenidoModal');
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>