<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Empleado - Listado</h2>
    </div>

 <div class="row">
    <div class="col-lg-12 contain-lg">
        <div class="table-responsive">
            <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th width="6">Check</th>
                             <th width="60">Nombres y Apellidos</th>
                           <th width="90">Dependencia</th>
							 <th width="60">Cargo</th>
						</tr>
                        </thead>
                        <tbody>
                        {foreach item=particular from=$listado}
                            <tr id="idParticular{$particular.pk_num_persona}">
                                <td>
                                   <div class="checkbox checkbox-styled">
                                        <label>
                       <input type="checkbox" class="valores" idParticular="{$particular.pk_num_persona}" 
					   particular="{$particular.nombre_empleado}" cedula="{$particular.ind_cedula_documento}" cargo="{$particular.cargoempleado} " dependenciaEmp="{$particular.ind_dependencia}" cargook="{$particular.pk_num_puestos}"  dependenciaPK="{$particular.pk_num_dependencia}" >
					   					</label>
                                    </div>
                                </td>
								 <td width="60"><label>{$particular.nombre_empleado}</label></td>
                                <td width="90"><label>{$particular.ind_dependencia}</label></td>
								  <td  width="60"><label>{$particular.cargoempleado}</label></td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<span class="clearfix"></span>
<div class="modal-footer">
    <button type="button"   title="Cancelar"  class="btn btn-default ink-reaction btn-raised" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar</button>
	<button type="button"  title="Agregar"  class="btn btn-primary ink-reaction btn-raised" id="agregarParticularSeleccionado"><span class="glyphicon glyphicon-floppy-disk"></span> &nbsp;Agregar</button>
    </button>
</div>

<script type="text/javascript">
    $(document).ready(function () {
	
		 $('#datatable2').DataTable({    
            "dom":  'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
				"processing": "Procesando ...",
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
		
        $('#agregarParticularSeleccionado').click(function () {
            var input = $('.valores');
			
			
            for (i = 0; i < input.length; i++) {
                if (input[i].checked == true) {
                    $(document.getElementsByClassName('idParticular' + input[i].getAttribute('idParticular'))).remove();
					
					r=0;
					$("#contenidoTablaResponsable tr").each(function () 
					 { 
						   r=r+1;
					 })
					 r=r-1; 	
					 p=r-1;
					
					
					$(document.getElementById('contenidoTablaResponsable')).append(
                            '<tr class="idParticular' + input[i].getAttribute('idParticular') + '">' +
                            '<input type="hidden" value="' + input[i].getAttribute('idParticular') + '" name="form[alphaNum][pk_num_persona]['+p+']" class="particularInput" particular="' + input[i].getAttribute('idParticular') + '" />' +
			
							'<td>' + r + '</td>' +
							'<td> <input  type="text" style="text-align:right;" class="form-control" value="'+ input[i].getAttribute('cedula') +'" name="form[txt][cod_responsable]['+p+']" id="cod_responsable'+p+'"  ></td>'  +	
							
							'<td> <input type="text" style="text-align:left;" class="form-control" value="'+  input[i].getAttribute('particular') +'" name="form[txt][ind_nombre_responsable]['+p+']" id="ind_nombre_responsable'+p+'"  ></td>'  +
							
							
							'<td> <div class="checkbox checkbox-styled"> <label> <input checked type="checkbox" value="1" name="form[int][num_estatusDetRes]['+p+']" id="num_estatusDetRes'+p+'" ></label></div></td>'  +	
							
							 '<td  class="text-center" style="vertical-align: middle;"><button class="btn ink-reaction btn-raised btn-xs btn-danger delete"><i class="md md-delete"></i></button></td>'+				
							 
                            '</tr>'							
                    );
           
                }
            }
            $('#ContenidoModal2').html('');
            $('#cerrarModal2').click();
        });
    });
</script>