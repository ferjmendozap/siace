<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Dependencias - Listado</h2>
    </div>

        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable3" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th width="6">Check</th>
                            <th width="100">Dependencia</th>
                            <th width="80">Representante Legal</th>
                            <th width="50">Cargo</th>
						</tr>
                        </thead>
                        <tbody>
                        {foreach item=dependencia from=$listado}
                            <tr id="idDependencia{$dependencia.pk_num_dependencia}">
                                <td>
                                   <div class="checkbox checkbox-styled">
                                        <label>
                       <input type="checkbox" class="valores" idDependencia="{$dependencia.pk_num_dependencia}" 
					   dependencia="{$dependencia.ind_dependencia}" coddependencia="{$dependencia.ind_codinterno}" representantedep="{$dependencia.nombre_apellidos}" representante="{$dependencia.fk_a003_num_persona_responsable}" 
					   cargodep="{$dependencia.cargodepen}" cargot="{$dependencia.pk_num_cargo}">
					   	   
                                        </label>
                                    </div>
                                </td>
                                <td><label>{$dependencia.ind_dependencia}</label></td>
                                <td><label>{$dependencia.nombre_apellidos}</label></td>
                                <td><label>{$dependencia.cargodepen}</label></td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<span class="clearfix"></span>
<div class="modal-footer">
    <button type="button"   title="Cancelar"  class="btn btn-default ink-reaction btn-raised" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar</button>
  <button type="button"  title="Agregar"  class="btn btn-primary ink-reaction btn-raised" id="agregarDependenciaSeleccionado"><span class="glyphicon glyphicon-floppy-disk"></span> &nbsp;Agregar</button>

    </button>
</div>

<script type="text/javascript">
 var input = $('.valores');
 var n=0;
 var m=0;
 
    $(document).ready(function () {
	
	
	 $('#datatable3').DataTable({    
            "dom":  'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
				"processing": "Procesando ...",
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
		
		
        $('#agregarDependenciaSeleccionado').click(function () {
           
            for (i = 0; i < input.length; i++) {
                if (input[i].checked == true) {
                    $(document.getElementsByClassName('idDependencia' + input[i].getAttribute('idDependencia'))).remove();
					
					n=0;
					$("#contenidoTabla tr").each(function () 
					 { 
						   n=n+1;
					 }) 
					 n=n-1;	
					 m=n-1;
					
					
                    $(document.getElementById('contenidoTabla')).append(
                            '<tr class="idDependencia' + input[i].getAttribute('idDependencia') + '">' +
                            '<input type="hidden" value="' + input[i].getAttribute('idDependencia') + '" name="form[alphaNum][pk_num_unidad_ejecutora_dependencia]['+m+']" class="dependenciaInput" dependencia="' + input[i].getAttribute('idDependencia') + '" />' +
			
							'<td>' + n + '</td>' +
							'<td> <input  type="text" style="text-align:right;" class="form-control" value="'+ input[i].getAttribute('coddependencia') +'" name="form[txt][cod_unidad_ejecutora_dependencia]['+m+']" id="cod_unidad_ejecutora_dependencia'+m+'"  ></td>'  +	
							
							'<td> <input type="text" style="text-align:left;" class="form-control" value="'+  input[i].getAttribute('dependencia') +'" name="form[txt][ind_nombre_unidad_ejecutora_dependencia]['+m+']" id="ind_nombre_unidad_ejecutora_dependencia'+m+'"  ></td>'  +	
							
							
							'<td> <div class="checkbox checkbox-styled">  <label> <input type="checkbox" checked value="1" name="form[int][num_estatusDet]['+m+']" id="num_estatusDet'+m+'" ></label></div></td>'  +	
							
							 '<td  class="text-center" style="vertical-align: middle;"><button class="btn ink-reaction btn-raised btn-xs btn-danger delete"><i class="md md-delete"></i></button></td>'+				
							 			  									  
                            '</tr>'
							
											
                    );
                }
            }
            $('#ContenidoModal2').html('');
            $('#cerrarModal2').click();
        });
    });
</script>