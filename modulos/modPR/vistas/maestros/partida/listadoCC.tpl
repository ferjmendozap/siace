<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Centro de Costo</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Centro</th>
                            <th>Descripcion</th>
                            <th>Grupo</th>
                            <th>Sub-Grupo</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=cc from=$lista}
                            <tr id="idCentroCosto{$cc.pk_num_centro_costo}">
                                <input type="hidden" value="{$cc.pk_num_centro_costo}" class="persona"
                                       codigo="{$cc.pk_num_centro_costo}"  descripcion="{$cc.ind_descripcion_centro_costo}">
                                <td><label>{$cc.pk_num_centro_costo}</label></td>
                                <td><label>{$cc.ind_descripcion_centro_costo} </label></td>
                                <td><label>{$cc.fk_a025_num_subgrupo_centro_costo}</label></td>
                                <td><label>{$cc.fk_a025_num_subgrupo_centro_costo}</label></td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

</div>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatable2').on('click', 'tbody tr', function () {
            var input = $(this).find('input');
            if('{$parametro}' == 'partida'){
                $(document.getElementById('{$idCampo}CC_C')).val(input.attr('codigo'));
                $(document.getElementById('{$idCampo}CC')).val(input.attr('codigo'));

            }else if('{$parametro}' == 'transacciones'){
                $(document.getElementById('{$idCampo}centroCosto')).val(input.attr('codigo'));
            }
            else
            {
                $(document.getElementById('fk_a023_num_centro_de_costo')).val(input.attr('codigo'));
                $(document.getElementById('centroCosto')).val(input.attr('codigo'));  
				$(document.getElementById('ind_descripcion_centro_costo')).val(input.attr('descripcion'));
            }
            $('#cerrarModal2').click();
           
        });
    });
</script>
