<div class="row">
    <div class="col-lg-12 contain-lg">
        <div class="table-responsive">
            <table id="datatable2" class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Codigo</th>
                    <th>Descripcion</th>
                    
                </tr>
                </thead>
                <tbody>
                {foreach item=i from=$lista}
                    <tr>
                        <input type="hidden" value="{$i.pk_num_cuenta}" class="cuenta"
                         codigo="{$i.cod_cuenta} "
						 id = "{$i.pk_num_cuenta}"
                         decripcion="{$i.ind_descripcion}">
                        <td>{$i.cod_cuenta}</td>
                        <td>{$i.ind_descripcion}</td>
                       
                    </tr>
                {/foreach}
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

       // $('#datatable2 tbody tr').click(function() {
		 $('#datatable2').on('click', 'tbody tr', function () {
            var input = $(this).find('input');
			
			   	if ('{$idCampo}'=='PUB20')
				{
                 $(document.getElementById('fk_cbb004_num_plan_cuenta_pub20')).val(input.attr('id'));
                 $(document.getElementById('cod_cuenta')).val(input.attr('codigo'));
				 $(document.getElementById('ind_descripcion')).val(input.attr('decripcion'));
				}
				
				if ('{$idCampo}'=='ONCOP')
				{
				  $(document.getElementById('fk_cbb004_num_plan_cuenta_onco')).val(input.attr('id'));
                  $(document.getElementById('cod_cuenta_onco')).val(input.attr('codigo'));
				  $(document.getElementById('ind_descripcion_onco')).val(input.attr('decripcion'));
				}
				
				if ('{$idCampo}'=='GASTO')
				{
				  $(document.getElementById('fk_cbb004_num_plan_cuenta_gastopub20')).val(input.attr('id'));
                  $(document.getElementById('cod_cuenta_gasto')).val(input.attr('codigo'));
				  $(document.getElementById('ind_descripcion_gasto')).val(input.attr('decripcion'));
				}
                //$(document.getElementById('pk_num_persona')).val(input.val());
                $('#cerrarModal2').click();
           
        });
    });
</script>