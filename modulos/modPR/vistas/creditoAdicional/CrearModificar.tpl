<form action="{$_Parametros.url}modPR/creditoAdicionalCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
	     <input type="hidden" value="1" name="valido" />
	   
	   {if $status =='AP' || $status =='AN'}
            <input type="hidden" value="1" name="ver" id="ver" />
       {/if}
	   
	   {if $status =='VER'}
            <input type="hidden" value="2" name="ver" id="ver" />
       {/if}
	   
	    <input type="hidden" value="{$status}" name="status" id="status"/>
        <input type="hidden" value="{$idCreditoAdicional}" name="idCreditoAdicional" id="idCreditoAdicional"/>
		<input type="hidden" value="IN" name="inicial"/>
		<input type="hidden" value="{if isset($formDB.ind_tipo_presupuesto)}{$formDB.ind_tipo_presupuesto} {else} {$tipoPresupuesto}{/if}" name="ind_tipo_presupuesto" id="ind_tipo_presupuesto"/>
		
		 <div>
        
         <!-- Nav tabs -->
             <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#Datos" aria-controls="home" role="tab" data-toggle="tab">Datos Generales</a></li>
                <li role="presentation"><a href="#Detalle" aria-controls="profile" role="tab" data-toggle="tab">Detalle de Presupuesto</a></li>
             </ul>

  <!-- Tab panes -->
            <div class="tab-content">
               <div role="tabpanel" class="tab-pane active" id="Datos">Datos Generales
			   
		   <div class="row">
		
           	     <div class="col-sm-3">
                    <div class="form-group floating-label">
                        <input type="text" readonly class="form-control" value="{if isset($formDB.ind_cod_credito_adicional)}{$formDB.ind_cod_credito_adicional}{/if}" name="form[alphaNum][ind_cod_credito_adicional]" id="ind_cod_credito_adicional">
                        <label for="ind_cod_credito_adicional">Cod. Credito Adicional</label>
                    </div>
                 </div>
				 
				 <div class="col-sm-2">
                    <div class="form-group floating-label" id="fec_anioError">
                        <input selected type="text" class="form-control fecha_anio" value="{if isset($formDB.fec_anio)}{$formDB.fec_anio}{else}{'Y'|date}{/if}" name="form[alphaNum][fec_anio]" id="fec_anio">
                        <label for="fec_anio">A&ntilde;o</label>
                    </div>
                </div> 	
				 
				 
				 <div class="col-sm-3">
                    <div class="form-group floating-label" id="fk_prb004_num_presupuestoError">
                        <select readonly id="fk_prb004_num_presupuesto" name="form[alphaNum][fk_prb004_num_presupuesto]" class="form-control dirty">
                            {foreach item=app from=$presupuesto}
                                {if isset($formDB.fk_prb004_num_presupuesto)}
                                    {if $app.pk_num_presupuesto==$formDB.fk_prb004_num_presupuesto}
                                        <option value="{$app.pk_num_presupuesto}|{$app.ind_tipo_presupuesto}">{$app.ind_cod_presupuesto}</option>
                                        {else}
                                        <option value="{$app.pk_num_presupuesto}|{$app.ind_tipo_presupuesto}">{$app.ind_cod_presupuesto}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_presupuesto}|{$app.ind_tipo_presupuesto}">{$app.ind_cod_presupuesto}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_prb004_num_presupuesto">Seleccione Presupuesto</label>
                    </div>
                </div>
				
				<div class='col-sm-2'>
                   <div class="form-group floating-label" id="fec_credito_adicionalError">
                       
                              <input type="text" class="form-control fechas2 dirty" value="{if isset($formDB.fec_credito_adicional)}{$formDB.fec_credito_adicional|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fec_credito_adicional]">
					          <label for="fec_credito_adicional"><i class="fa fa-calendar"></i> Fecha Credito Adicional</label>
                           
                    </div>
                </div>	
				
		    </div>
			  
		<div class="row">
		
		   <div class="col-sm-3">
                <div class="form-group floating-label" id="ind_numero_oficioError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_numero_oficio)}{$formDB.ind_numero_oficio}{/if}" name="form[alphaNum][ind_numero_oficio]" id="ind_numero_oficio">
                        <label for="ind_numero_oficio">Numero Oficio</label>
                 </div>
            </div>	
				
				
			  <div class='col-sm-3'>
                   <div class="form-group floating-label" id="fec_oficioError">
                       
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.fec_oficio)}{$formDB.fec_oficio|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fec_oficio]">
					          <label for="fec_oficio"><i class="fa fa-calendar"></i> Fecha Oficio</label>
                           
                    </div>
                </div>		

        
		   <div class="col-sm-3">
                <div class="form-group floating-label"  id="ind_numero_decretoError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_numero_decreto)}{$formDB.ind_numero_decreto}{/if}" name="form[alphaNum][ind_numero_decreto]" id="ind_numero_decreto">
                        <label for="ind_numero_decreto">Numero Drecreto</label>
                 </div>
            </div>	
			
			
			  <div class='col-sm-3'>
                   <div class="form-group floating-label" id="fec_decretoError">
                     
                              <input type="text" class="form-control fechas2" value="{if isset($formDB.fec_decreto)}{$formDB.fec_decreto|date_format:"%d-%m-%Y"}{/if}" name="form[txt][fec_decreto]" >
					          <label for="fec_decreto"><i class="fa fa-calendar"></i> Fecha Decreto</label>
                           
                    </div>
                </div>		 
        </div>		
			
		 <div class="row">	
		      <div class="col-lg-10">
                       <div class="form-group" id="ind_motivoError">
                                        <textarea required="" placeholder="" rows="3" class="form-control" id="ind_motivo" name="form[formula][ind_motivo]">{if isset($formDB.ind_motivo)}{$formDB.ind_motivo}{/if}</textarea>
                                        <label for="ind_motivo">Motivo</label>
                      </div>
               </div> 	
		  </div>	
			

          <div class="row">	
				 <div class="col-sm-5">
                    <div class="form-group floating-label"  id="num_monto_totalError">
                        <input type="text" readonly="readonly" class="form-control monto dirty" value="{if isset($formDB.num_monto_total)}{$formDB.num_monto_total|number_format:2:",":"."}{/if}" name=  
						"form[int][num_monto_total]" id="num_monto_total">
                        <label for="num_monto_total">Monto Total Credito Adicional</label>
                    </div>
                 </div>
		  
                <div class="col-sm-2">
                    <div class="form-group floating-label" >
                        <input type="text" readonly class="form-control" value="{if isset($formDB.ind_estado)}{$formDB.ind_estado}{/if}" name="form[alphaNum][ind_estado]" id="ind_estado">
                        <label for="ind_estado">Estatus</label>
                    </div>
                 </div>
        </div>
		

        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i>Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
				   
 </div>
             
		 
	<!-----------DETALLE PRESUPUESTO---------------------------------->		 
    <div role="tabpanel" class="tab-pane" id="Detalle">Detalle de Presupuesto
			    <div id="listapartidas"></div>

		   
         </div>
		
		      <div class="modal-footer">
						  <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss=
						  "modal">
						  <span class="glyphicon glyphicon-floppy-remove"></span>Cancelar</button>
						 <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
							
							{if $status=='AP'}
								<span class="glyphicon glyphicon-ok"></span>Aprobar
						    {elseif $status=='GE'}
								Generar 
							{elseif $status=='AN'}
								<span class="glyphicon glyphicon-remove"></span>Anular 	
							{elseif $status=='RV'}
								<span class="glyphicon glyphicon-ok-circle"></span>Revisar		
							{elseif $status=='VER'}
								<span class="glyphicon glyphicon-remove"></span>Cerrar
							{elseif $status=='RE'}
								<span class="glyphicon glyphicon-arrow-left"></span>Reversar		
							{elseif isset($idCreditoAdicional)}
								<span class="glyphicon glyphicon-floppy-save"></span>Modificar
							{else}
								<span class="glyphicon glyphicon-floppy-disk"></span>Guardar
							{/if}
						</button>
		       </div>
		
    </div>
   
</form>

<script type="text/javascript">
    $(document).ready(function() {
	
        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
			
        }
		    
	
		   var disabled=$(document.getElementById("ver")).val();
		   if (disabled==1){
		       $("#formAjax input,textarea").prop("readonly", true);
			   $("#formAjax select").prop("disabled", true);
			   $("#formAjax input" ).removeClass('fechas2');

		   }	   
		   else if (disabled==2)
		       $("#formAjax input,textarea,select").prop("disabled", true);
		 
		
			$("#formAjax").submit(function(){
				return false;
			});
		
		
		      var $url_credito='{$_Parametros.url}modPR/creditoAdicionalCONTROL/BuscarPartidasMET';
			 
			  var cod_presupuesto= '{if isset($formDB.fk_prb004_num_presupuesto)}{$formDB.fk_prb004_num_presupuesto}{/if}';
			  var cod_credito=$(document.getElementById('idCreditoAdicional')).val();
			  var status=$(document.getElementById('status')).val();
			 
				$('#formModalLabel').html($(this).attr('titulo'));
				$.post($url_credito,{ idPresupuesto:cod_presupuesto, idCreditoAdicional:cod_credito, status:status, seleccionado:0},function($dato){
					 $('#listapartidas').html($dato);
				});
				
		
		 var nuevo=$(document.getElementById("idCreditoAdicional")).val();
		    if (nuevo==0){
				 var pr = new  PrFunciones();
				 var anio = '{if isset($formDB.fec_anio)}{$formDB.fec_anio}{/if}';
				 var idPresupuesto = '{if isset($formDB.fk_prb004_num_presupuesto)}{$formDB.fk_prb004_num_presupuesto}{/if}';
				 
				 pr.metJsonPresupuesto('{$_Parametros.url}modPR/creditoAdicionalCONTROL/JsonPresupuestoMET',anio,idPresupuesto);
				 
			}	
		
		
		$('#agregarPartida').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
		
		
		 $('#codpartida').on('click', '.borrar', function () {
            var borrar = $(this);
            $(document.getElementsByClassName(borrar.attr('borrar'))).remove();
        });
		
		
		 $('#fk_prb004_num_presupuesto').change(function () {
	
		     var $url='{$_Parametros.url}modPR/creditoAdicionalCONTROL/BuscarPartidasMET';
			   
			   var $posicion=$(this).val().indexOf('|');
			   var tipo_presupuesto=$(this).val();
			   tipo_presupuesto= tipo_presupuesto.substr(-1);
			   
			   var cod_presupuesto=$(this).val().substr(0,$posicion);
			   var cod_credito=$(document.getElementById('idCreditoAdicional')).val();
			   var status=$(document.getElementById('status')).val();
			 
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idPresupuesto:cod_presupuesto, idCreditoAdicional:cod_credito, status:status, seleccionado:1},function($dato){
                 $('#listapartidas').html($dato);
            });
			
          });
		
	
		 $('#fec_mes').datepicker({ format: 'mm', viewMode: "months", minViewMode: "months", language:'es' }); 
		 $('#fec_anio').datepicker({ format: 'yyyy', viewMode: "years", minViewMode: "years", startDate: '-1y', endDate: '+1y' ,language:'es' });
		 $('#fechas').datepicker({ autoclose: true, todayHighlight: true, format: "dd-mm-yyyy", language:'es' });
		 $('.fechas2').datepicker({ autoclose: true, todayHighlight: true, format:  "dd-mm-yyyy", language:'es' });
   	
		
        $('#modalAncho').css("width","80%");
		
        $('#accion').click(function(){
		 
		    swal({
                title: "�Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
		  
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
					 //----------------------------------------------------
					
					
			    }else if(dato['status']=='anulado'){
                   
					$(document.getElementById('idCreditoAdicional'+dato['idCreditoAdicional'])).html('');		
                    swal("Registro Anulado!", "El Ajuste fue anulado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
			   //------------------------------------------------------		
				
			    }else if(dato['status']=='reversado'){
                   
					$(document.getElementById('idCreditoAdicional'+dato['idCreditoAdicional'])).html('');		
                    swal("Registro Reversado!", "El Ajuste fue reversado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
			    }else if(dato['status']=='aprobado'){
                   
                    $(document.getElementById('idCreditoAdicional'+dato['idCreditoAdicional'])).html('');		
                    swal("Registro Aprobado!", "El Credito Adicional fue aprobado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
					
					//------------------------------------------------------
					
					
				}else if(dato['status']=='revisado'){
                   
                    $(document.getElementById('idCreditoAdicional'+dato['idCreditoAdicional'])).html('');		
                    swal("Registro Revisado!", "El Credito Adicional fue revisado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
					
					//-------------------------------------------------------
					
                }else if(dato['status']=='modificar'){
                   
                    if(dato['ind_estado']=='PR' ){ var estado='Preparaci&oacuten' }else if (dato['ind_estado']=='RV'){ var estado='Revisado' } else if (dato['ind_estado']=='AP'){ var estado='Aprobado' } else { var estado='Preparaci&oacuten' }
                    $(document.getElementById('idCreditoAdicional'+dato['idCreditoAdicional'])).html('<td>'+dato['ind_cod_credito_adicional']+'</td>' +
                            '<td>'+dato['fec_anio']+'</td>' +
							'<td>'+dato['ind_motivo']+'</td>' +
                            '<td>'+dato['num_monto_total']+'</td>' +
							'<td>'+estado+'</td>' +
                            '<td>' +	
                            '{if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idCreditoAdicional="'+dato['idCreditoAdicional']+'"' +
                            'descipcion="El Usuario a Modificado un Credito Adicional" titulo="Modificar Ajuste">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idCreditoAdicional="'+dato['idCreditoAdicional']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado un Credito Adicional" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Credito Adicional!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Modificado!", "El Credito Adicional fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					
					
                }else if(dato['status']=='nuevo'){
                   
                   if(dato['ind_estado']=='PR' ){ var estado='Preparaci&oacuten' }else if (dato['ind_estado']=='RV'){ var estado='Revisado' } else if (dato['ind_estado']=='AP'){ var estado='Aprobado' } else { var estado='Preparaci&oacuten' }
                    $(document.getElementById('datatable1')).append('<tr  id="idCreditoAdicional'+dato['idCreditoAdicional']+'">' +
                            '<td>'+dato['ind_cod_credito_adicional']+'</td>' +
                            '<td>'+dato['fec_anio']+'</td>' +
							'<td>'+dato['ind_motivo']+'</td>' +
                            '<td>'+dato['num_monto_total']+'</td>' +
							'<td>'+estado+'</td>' +
                            '<td>' +
                            '{if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idCreditoAdicional="'+dato['idCreditoAdicional']+'"' +
                            'descipcion="El Usuario a Creado un Credito Adicional" titulo="Modificar Ajuste">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idCreditoAdicional="'+dato['idCreditoAdicional']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado un Credito Adicional" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la el Credito Adicional!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Creado!", "El Credito Adicional fue creado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
    });


	
function actualizarMonto(CodPartida,aespecifica1) {
var suma=0;
		var suma=0; 
		var codigo=""; 
		var codigo1="";
		var codigo2="";
		var valor=""; 
		

   $("#codpartida td").each(function (){
		   
				 $(this).find('input').each (function() {
				 
				     var presupuesto = $(document.getElementById('ind_tipo_presupuesto')).val().trim(); 
					 aespecifica = $(this).attr('aespecifica');
					 
				     idpartida1 = $(this).attr('id');
					 	 
					 valor= $(this).val();  
					 $(this).val(valor).formatCurrency();
					 
					
					 
				  if(presupuesto=="P") 
				  {
				     codigo= idpartida1.substring(0, 6);
					 codigo= codigo.concat(".00.00");
					 
					 				 
					  if (CodPartida==codigo && aespecifica1==aespecifica){
					     monto= setNumero($(this).val());
						 suma=suma+Number(monto);
					   }
					 
					 
					 codigo1= idpartida1.substring(0, 3);
					 codigo1= codigo1.concat(".00.00.00");
					 
					  if (CodPartida==codigo1 && aespecifica1==aespecifica){
						 monto= setNumero($(this).val());
						 suma=suma+Number(monto);
					  }
						 
						 
					 codigo2= idpartida1.substring(0, 9);
					 codigo2= codigo2.concat(".00");
					 
					  if (CodPartida==codigo2 && aespecifica1==aespecifica){
						 monto= setNumero($(this).val());
						 suma=suma+Number(monto);
					 }
				  }
				  else
				  {
				     codigo= idpartida1.substring(0, 6);
					 codigo= codigo.concat(".00.00");
					 
					 
				      if (CodPartida==codigo){
					    monto= setNumero($(this).val());
					    suma=suma+Number(monto);
					 }
					 
					 
					  codigo1= idpartida1.substring(0, 3);
					  codigo1= codigo1.concat(".00.00.00");
					 
					  if (CodPartida==codigo1){
					     monto= setNumero($(this).val());
						 suma=suma+Number(monto);
					  }
						 
						 
					   codigo2= idpartida1.substring(0, 9);
					   codigo2= codigo2.concat(".00");
					 
					   if (CodPartida==codigo2 ){
						 monto= setNumero($(this).val());
						 suma=suma+Number(monto);
					  }
				  }
					  
					 				  
	             }) //imput
			 })	
			 
			 return suma; 
}


//	funcion para convertir un numero frmateado en su valor real
function setNumero(num_formateado) {
	var num = num_formateado.toString();
	num = num.replace(/[.]/gi, "");
	num = num.replace(/[,]/gi, ".");
	
	var numero = new Number(num);
	return numero;
}



</script>

