 <section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Credito Adicional - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Cod. Credito Adicional</th>
                                <th>Ejer. Presupuestario</th>
								<th>Motivo</th>
								<th>Monto Credito Adicional</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=creditoadicional from=$listado}
                                <tr id="idCreditoAdicional{$creditoadicional.pk_num_credito_adicional}">
                                    <td><label>{$creditoadicional.ind_cod_credito_adicional}</label></td>
                                    <td><label>{$creditoadicional.fec_anio}</label></td>
									<td><label>{$creditoadicional.ind_motivo}</label></td>
									<td><label>{$creditoadicional.num_monto_total|number_format:2:",":"."}</label></td>
                                    <td>
									
                                     <label>{if $creditoadicional.ind_estado=='PR'}Preparaci&oacute;n{else if $creditoadicional.ind_estado=='AP'}Aprobado{else if     
									 $creditoadicional.ind_estado=='RV'}Revisado{else if $creditoadicional.ind_estado=='AN'}Anulado{/if}</label>
                                    </td>
                                    <td>
									 {if !$status}
                                       {if $creditoadicional.ind_estado=='PR'}
                                            {if in_array('PR-01-02-01-02-M',$_Parametros.perfil)}
                                             <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idCreditoAdicional="{$creditoadicional.pk_num_credito_adicional}"
                                                    descipcion="El Usuario a Modificado un Cr&eacute;dito Adicional del sistema" titulo="Modificar Cr&eacute;dito Adicional">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                             </button>
                                            {/if}
											
											{if in_array('PR-01-02-01-03-E',$_Parametros.perfil)}
												<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" 
												idCreditoAdicional="{$creditoadicional.pk_num_credito_adicional}"  boton="si, Eliminar"
														descipcion="El usuario a eliminado un credito adicional del sistema" titulo="Estas Seguro?" mensaje="Estas seguro que desea 
														eliminar el Cr&eacute;dito Adicional!!">
													<i class="md md-delete" style="color: #ffffff;"></i>
												</button>
											{/if}
                                        {/if}
										
										
										{if $creditoadicional.ind_estado=='AP' || $creditoadicional.ind_estado=='RV'}
										  {if in_array('PR-01-03-01-01-V',$_Parametros.perfil)}
                                            <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idCreditoAdicional="{$creditoadicional.pk_num_credito_adicional}" title="Ver Credito Adicional"
                                                descipcion="El Usuario esta viendo un Cr&eacute;dito Adicional" titulo="<i class='icm icm-calculate2'></i> Ver Cr&eacute;dito Adicional">
                                                <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                            </button>
                                          {/if}	
										 {/if} 
										
										{elseif $status=='PR'}
                                         {if in_array('PR-01-02-01-04-R',$_Parametros.perfil)}
                                            <button class=" revisar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idCreditoAdicional="{$creditoadicional.pk_num_credito_adicional}" title="Aprobar                                                    Proceso" descipcion="El Usuario ha revisado un Cr&eacute;dito Adicional" titulo="Revisar Cr&eacute;dito Adicional"> 
												<i class="fa fa-check-square" style="color: #ffffff;"></i>
                                            </button>
												
											<button class="anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idCreditoAdicional="{$creditoadicional.pk_num_credito_adicional}" title="Anular                                                    Credito Adicional" descipcion="El Usuario generar un Cr&eacute;dito Adicional" titulo="Anular Cr&eacute;dito Adicional"> 
												<i class="md md-not-interested" style="color: #ffffff;"></i>
                                            </button>
                                         {/if}
										 
										 {elseif $status=='RV'}
                                         {if in_array('PR-01-02-01-04-R',$_Parametros.perfil)}
                                            <button class="aprobar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idCreditoAdicional="{$creditoadicional.pk_num_credito_adicional}" title="Aprobar                                                    Cr&eacute;dito Adicional" descipcion="El Usuario aprobado un Cr&eacute;dito Adicional" titulo="Aprobar Cr&eacute;dito Adicional"> 
												<i class="fa fa-check" style="color: #ffffff;"></i>
                                            </button>
											
											<button class="reversar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idCreditoAdicional="{$creditoadicional.pk_num_credito_adicional}" 
													title="Reversar Credito Adicional" descipcion="El Usuario esta Reversando el Cr&eacute;dito Adicional" titulo="Reversar Cr&eacute;dito Adicional"> 
												<i class="fa fa-undo" style="color: #ffffff;"></i>
                                            </button>
                                         {/if}
										 									
                                      {/if}
						
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="6">
							<div>
							  {if !$status } 
                                {if in_array('PR-01-02-01-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario a creado un Credito Adicioal del sistema"  titulo="Crear Credito Adicional" id="nuevo" >
                                        Nuevo Cr&eacute;dito Adicional &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-create"></i>
                                    </button>
                                {/if}
							  {/if}	
							</div>
                            </th>
							
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modPR/creditoAdicionalCONTROL/crearModificarMET';
       
	   $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idCreditoAdicional:0 ,status:'PR',inicial:'IN'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
				

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idCreditoAdicional: $(this).attr('idCreditoAdicional'), status:'PR',inicial:''},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		
			$('#datatable1 tbody').on( 'click', '.aprobar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idCreditoAdicional: $(this).attr('idCreditoAdicional'), status:'AP'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		
		
		$('#datatable1 tbody').on( 'click', '.anular', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idCreditoAdicional: $(this).attr('idCreditoAdicional'), status:'AN'},function($dato){
                $('#ContenidoModal').html($dato);
            });
         });
		 
		$('#datatable1 tbody').on( 'click', '.reversar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idCreditoAdicional: $(this).attr('idCreditoAdicional'), status:'RE'},function($dato){
                $('#ContenidoModal').html($dato);
            });
         });
		 
		$('#datatable1 tbody').on( 'click', '.revisar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idCreditoAdicional: $(this).attr('idCreditoAdicional'), status:'RV'},function($dato){
                $('#ContenidoModal').html($dato);
            });
         });
		 
		
		$('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idCreditoAdicional: $(this).attr('idCreditoAdicional'), status:'VER'},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
      
		
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idCreditoAdicional=$(this).attr('idCreditoAdicional');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modPR/creditoAdicionalCONTROL/eliminarMET';
                $.post($url, { idCreditoAdicional: idCreditoAdicional },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idCreditoAdicional'+dato['idCreditoAdicional'])).html('');
                        swal("Eliminado!", "El Cr&eacute;dito Adicional fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>