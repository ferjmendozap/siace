<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO                   |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                |  lbastardo@contraloriadebolivar.gob.ve |         0424-9080200           |
 * |   |                                           |                                        |                                |
 * |___|___________________________________________|________________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        01-09-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class cierremesControlador extends Controlador
{
    private $atCierreMes;
	
    public function __construct()
    {
        parent::__construct();
         $this->atCierreMes=$this->metCargarModelo('cierremes');
		 $this->atReformulacion=$this->metCargarModelo('reformulacion');
		 $this->_ReporteModelo = $this->metCargarModelo('reporte');
		//$this->atPartida=$this->metCargarModelo('partida', 'maestros' ,'modPR');
    }

     public function metIndex($status=false)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
			
        );
        $js =  array('materialSiace/core/demo/DemoTableDynamic','ModPR/prFunciones');
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->atCierreMes->metListarCierreMes($status));
		$this->atVista->assign('status', $status);
        $this->atVista->metRenderizar('listado');
    }


     public function metJsonPresupuesto()
    {

        $anio = $this->metObtenerInt('anio');
		$filtro  = $this->atReformulacion->metListarPresupuestoAnio($anio);
        echo json_encode($filtro);
        exit;

    }


    public function metCrearModificar()
    {
	
	   $complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
			'inputmask/jquery.inputmask.bundle.min',
        );
		
		

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
	
        $valido=$this->metObtenerInt('valido');
        $idCierreMes=$this->metObtenerInt('idCierreMes');
		$status = $this->metObtenerAlphaNumerico('status');
		$suma_total=0;	 
			 
		//$idCierreMes=0;	 
		//$valido=0;
	     
		 if($valido==1){
		
		
	   // if($status == 'PR') {
            $this->metValidarToken();
			
			//$Excceccion=array('fec_anio','fec_mes');

         
			$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
            $ind=$this->metValidarFormArrayDatos('form','int');
			$formTxt=$this->metObtenerTexto('form','txt');
			
		 
		  foreach ($alphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if(!empty($alphaNum[$tituloAlphaNum])){
                    $validacion[$tituloAlphaNum]=$valorAlphaNum;
                }else{
                    $validacion[$tituloAlphaNum]='error';

                }
            }
		 
		 
		   foreach ($ind as $tituloInt => $valorInt) {
                if(!empty($ind[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
		  
		 
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
			
			
			 if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }
			
		
            if($idCierreMes==0){
			
		   
               $id=$this->atCierreMes->metCrearCierreMes($validacion['fec_anio'], $validacion['fec_mes'], $validacion['fk_prb004_num_presupuesto']);
		   
		       if ($id==1)
                $validacion['status']='nuevo';
			   if ($id==0)	
			     $validacion['status']='error_cierre';
			   if ($id==2)
                 $validacion['status']='error_cierre2'; 	 
				 
            }/*else{
			
			// $id=$this->atAntepresupuesto->metModificarAntepresupuesto( '2015', '2015-01-01','2015-01-01','2015-01-01','7383','2015-01-01','7383','2015-01-01',2,2,0,0,1);
			
           $id=$this->atCierreMes->metModificarPresupuesto($validacion['fec_anio'], $validacion['fec_presupuesto'],$validacion['fec_inicio'],$validacion['dec_fin'],$validacion['ind_numero_gaceta'],$validacion['fec_gaceta'], $validacion['ind_numero_decreto'],$validacion['fec_decreto'],$validacion['num_monto_presupuestado'],$validacion['num_monto_generado'], $validacion['fk_prb002_num_partida_presupuestaria'], $validacion['monto'],$idCierreMes);
                $validacion['status']='modificar';
            }*/
			
  		/*	if ($id==0)
			{
		     	$this->metcierremesReportePdf2();  
			}*/	
			 
           if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
							
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idCierreMes']=$id;
		
            echo json_encode($validacion);
            exit;
        }

         if($idCierreMes!=0){
		 
            $this->atVista->assign('formDB',$this->atCierreMes->metMostrarCierreMensual($idCierreMes));
			
            $this->atVista->assign('idCierreMes',$idCierreMes);
			
		//$this->atVista->assign('suma_total',$suma_total);
        }
		
		    $this->atVista->assign('presupuesto',$this->atCierreMes->metPresupuestoListar());
		    $this->atVista->assign('status',$status);
            $this->atVista->metRenderizar('CrearModificar','modales');
    }


     public function metCrearModificarPreCierre()
    {
	
	   $complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
			'inputmask/jquery.inputmask.bundle.min',
        );
		
		

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
	
        $valido=$this->metObtenerInt('valido');
        $idCierreMes=$this->metObtenerInt('idCierreMes');
		$status = $this->metObtenerAlphaNumerico('status');
		$suma_total=0;	
		$resp=0; 
			 
		//$idCierreMes=0;	 
		//$valido=0;
		
		 if($valido==1){
		
		
	   // if($status == 'PR') {
            $this->metValidarToken();
			
			$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
            $ind=$this->metValidarFormArrayDatos('form','int');
			$formTxt=$this->metObtenerTexto('form','txt');
			
		 
		  foreach ($alphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if(!empty($alphaNum[$tituloAlphaNum])){
                    $validacion[$tituloAlphaNum]=$valorAlphaNum;
                }else{
                    $validacion[$tituloAlphaNum]='error';

                }
            }
		 
		 
		   foreach ($ind as $tituloInt => $valorInt) {
                if(!empty($ind[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
		  
		 
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
			
			
			 if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }
			
            if($idCierreMes==0){
			
		   //$validacion['ind_numero_decreto']= $validacion['fk_prb002_num_partida_presupuestaria']; 
		   //$id=$this->atCierreMes->metCrearCierreMes('2016','01',8);
		   
               $id=$this->atCierreMes->metCrearPreCierreMes($validacion['fec_anio'], $validacion['fec_mes'], $validacion['fk_prb004_num_presupuesto']);
			   
		   
		       if ($id==1)
                $validacion['status']='nuevo';
				
			   if ($id==0)	
			   {
			      $validacion['status']='error_cierre';
			   }	 
			   if ($id==2)
                $validacion['status']='error_cierre2'; 	 
				 
            }
			
			  if($idCierreMes!=0){
			          $id=1;
			          $validacion['status']='nuevo';
					  $this->atVista->assign('formDB',$this->atCierreMes->metMostrarCierreMensual($idCierreMes));
					  $this->atVista->assign('idCierreMes',$idCierreMes);
		//$this->atVista->assign('suma_total',$suma_total);
              }
  			 
            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
							
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idCierreMes']=$id;
		
            echo json_encode($validacion);
            exit;
        }


		
		    $this->atVista->assign('presupuesto',$this->atCierreMes->metPresupuestoListar());
		    $this->atVista->assign('status',$status);
            $this->atVista->metRenderizar('CrearModificar','modales');
    }


    public function metEliminar()
    {
        $idCierreMes = $this->metObtenerInt('idCierreMes');
        if($idCierreMes!=0){
            $id=$this->atCierreMes->metEliminarPresupuesto($idCierreMes);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Presupuesto se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idCierreMes'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
	
	
	protected function FormatoFecha($fecha)
    {
        $resultado=explode("-",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }	
	
	
	protected function FormatoMonto($monto_f)
    {
       if(!isset($monto_f)){
                $monto_f=0;
            }
			else {
			     $monto_f=str_replace(".", "", $monto_f);
			     $monto_f=str_replace(",", ".", $monto_f);
				 
			}
			 return $monto_f;
    }	
	
	
	
	//--------------------------------------------------------------------------
	protected function metprecierremesReportePdf()
    {

       #Recibiendo las variables del formulario que utiliza la clase veh�culo

        $formInt=$this->metObtenerInt('form','int');

        $formTxt=$this->metObtenerTexto('form','txt');
		
		$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
		
		
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
		
		 foreach ($alphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if(!empty($alphaNum[$tituloAlphaNum])){
                    $validacion[$tituloAlphaNum]=$valorAlphaNum;
                }else{
                    $validacion[$tituloAlphaNum]='error';

                }
            }
		
	
		   if($validacion['fec_mes']!="error"){
            $fec_mes=$validacion['fec_mes'];
        }else{
            $fec_mes="";
        }
			
		  if($validacion['fec_anio']!="error"){
            $fec_anio=$validacion['fec_anio'];
        }else{
            $fec_anio="";
        }

        $this->_ReporteModelo = $this->metCargarModelo('reporte');
		$organismo= $this->_ReporteModelo-> metListarOrganismoInt(1);
		
		

        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabecera','modPR');
        $pdf=new pdfReporteCierreMes('L','mm','Legal');
		$pdf->organismo = $organismo[0]['ind_descripcion_empresa'];
		$pdf->logo = $organismo[0]['ind_logo'];
		$pdf->logo_secundario = $organismo[0]['ind_logo_secundario'];
		
        $pdf->AliasNbPages();
        $pdf->AddPage();
				
		
		$pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);

		// $this->_ReporteModelo = $this->metCargarModelo('reporte');
		
       switch ($validacion['fec_mes']) {
		case "01": $fec_mes = "ENERO"; break;  
		case "02": $fec_mes= "FEBRERO"; break; 
		case "03": $fec_mes= "MARZO"; break;   
		case "04": $fec_mes= "ABRIL"; break;   
		case "05": $fec_mes = "MAYO"; break;    
		case "06": $fec_mes  = "JUNIO"; break;
		case "07": $fec_mes = "JULIO"; break;
		case "08": $fec_mes  = "AGOSTO"; break;
		case "09": $fec_mes  = "SEPTIEMBRE"; break;
		case "10": $fec_mes  = "OCTUBRE"; break;
		case "11": $fec_mes = "NOVIEMBRE"; break;
		case "12": $fec_mes  = "DICIEMBRE"; break;
    } 
	
	
		
	$pdf->SetFont('Arial', 'B', 10);
	$pdf->Cell(105, 10, '', 0, 0, 'C');
	$pdf->Cell(58, 10, utf8_decode(' REPORTE DE CIERRE MES '), 0, 0, 'C');
    $pdf->Cell(13, 10, $fec_mes , 0, 0, 'C'); $pdf->Cell(20, 10, $fec_anio, 0, 1, 'C');
	$pdf->Ln(3);

		$pdf->Ln(8);
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
       // $pdf->Cell(5, 5,utf8_decode('N�'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, utf8_decode('COD. PARTIDA'), 1, 0, 'C', 1);
        $pdf->Cell(95, 5, 'DESCRIPCION', 1, 0, 'C', 1);
        $pdf->Cell(22, 5, 'DISP. PRESUP.', 1, 0, 'C', 1);
		$pdf->Cell(22, 5, 'DISP. FINANC.', 1, 0, 'C', 1);
		$pdf->Cell(20, 5, 'INCREMENTO', 1, 0, 'C', 1); 
		$pdf->Cell(20, 5, utf8_decode('DISMINUCI�N'), 1, 0, 'C', 1);
		$pdf->Cell(22, 5, 'PRESUP. AJUST', 1, 0, 'C', 1);
		$pdf->Cell(22, 5, 'COMPROMETIDO', 1, 0, 'C', 1);
		$pdf->Cell(20, 5, 'CAUSADO', 1, 0, 'C', 1);
		$pdf->Cell(20, 5, 'PAGADO', 1, 0, 'C', 1);
		$pdf->Cell(22, 5, 'DISP. PRESUP', 1, 0, 'C', 1);
		$pdf->Cell(22, 5, 'DISP. FINAN.', 1, 0, 'C', 1);
       
        $pdf->SetFont('Arial', '', 7);

	
		
        $consulta= $this->_ReporteModelo-> getReporteCierremes($validacion['fk_prb004_num_presupuesto'],$validacion['fec_anio'],$validacion['fec_mes'] );
		$partidas_titulo= $this->_ReporteModelo-> getReportePartidasTitulo();
		$aespecifica = $this->_ReporteModelo->metAespecifica();
				
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(4);
	
        $montot=0; $montofi=0; $montoin=0; $montodi=0; $montoat=0; $montoct=0; $montocat=0; $montopt=0; $montodt=0; $montopfnt=0;

	    $p=0; $cont=0;
		
		
	  if (isset($consulta[0]['ind_tipo_presupuesto']))	
	  if ($consulta[0]['ind_tipo_presupuesto']=='P')
      {	
	  
		$pdf->Ln(2);
		for($a=0;$a<count($aespecifica);$a++)
	    {
		   $cont=0;
		   $existeAespecifica = count($this->_ReporteModelo->metExisteAespecificaEs4($aespecifica[$a]['pk_num_aespecifica'], $validacion['fk_prb004_num_presupuesto']));
		   
		   $pdf->SetFont('Arial', 'B', 8);
		   $pdf->SetFillColor(215, 215 ,215);
			
			$pdf->SetWidths(array(25,307));
            $pdf->SetAligns(array('L','L'));
		   
		   if ($existeAespecifica>0)
		   {
		      //$pdf->Cell(5,5); 
		      $pdf->Row(array(
                    utf8_decode($aespecifica[$a]['ind_cod_aespecifica']),
                    utf8_decode($aespecifica[$a]['ind_descripcion'])));
			
		   }
		
	   
         for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
         {

			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);

				$pdf->SetXY($x,$y);
			}
			
			
            $pdf->SetWidths(array(25,95,22,22,20,20,22,22,20,20,22,22));
            $pdf->SetAligns(array('L','L','R','R','R','R','R','R','R','R','R','R'));
            
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(235, 235, 235);
			
			
	      if ($aespecifica[$a]['pk_num_aespecifica']==$consulta[$i]['fk_pr006_num_aespecifica'])
		  {		
				
			$monto=number_format($consulta[$i]['num_disponibilidad_presupuestaria_inicial'],2,',','.');  $montot=$montot+$consulta[$i]['num_disponibilidad_presupuestaria_inicial'];
			$montof=number_format($consulta[$i]['num_disponibilidad_financiera_inicial'],2,',','.');  $montofi=$montofi+$consulta[$i]['num_disponibilidad_financiera_inicial'];
			$montoi=number_format($consulta[$i]['num_incremento'],2,',','.');  $montoin=$montoin+$consulta[$i]['num_incremento'];
			$montods=number_format($consulta[$i]['num_disminucion'],2,',','.');  $montodi=$montodi+$consulta[$i]['num_disminucion'];
			$montoa=number_format($consulta[$i]['num_presupuesto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_presupuesto_ajustado'];
			$montoc=number_format($consulta[$i]['num_compromiso'],2,',','.'); $montoct=$montoct+$consulta[$i]['num_compromiso'];
			$montoca=number_format($consulta[$i]['num_causado'],2,',','.'); $montocat=$montocat+$consulta[$i]['num_causado'];
			$montop=number_format($consulta[$i]['num_pagado'],2,',','.'); $montopt=$montopt+$consulta[$i]['num_pagado'];
			$montod=$consulta[$i]['num_presupuesto_ajustado']-$consulta[$i]['num_compromiso']; $montodt=$montodt+$montod;
			$montofn=number_format($consulta[$i]['num_disponibilidad_financiera'],2,',','.'); $montopfnt=$montopfnt+$consulta[$i]['num_disponibilidad_financiera'];
			
			
				
				
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			   
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTituloCierre($validacion['fk_prb004_num_presupuesto'],$partida,$validacion['fec_anio'],$validacion['fec_mes'],$aespecifica[$a]['pk_num_aespecifica']);
					$suma_presupuestado=$suma['suma_presupuesto_inicial'];
					$suma_financiero=$suma['suma_financiero_inicial'];
					$suma_incremento=$suma['suma_incremento'];
					$suma_disminucion=$suma['suma_disminucion'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_causado=$suma['suma_causado'];
					$suma_pagado=$suma['suma_pagado'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
					$suma_financiera_final=$suma_ajustado-$suma_pagado;
				
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_financiero,2,',','.'),number_format($suma_incremento,2,',','.'),
					number_format($suma_disminucion,2,',','.'), number_format($suma_ajustado,2,',','.'),
					number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),
					number_format($suma_disponible,2,',','.'),number_format($suma_financiera_final,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTituloCierre($validacion['fk_prb004_num_presupuesto'],$partida,$validacion['fec_anio'],$validacion['fec_mes'],$aespecifica[$a]['pk_num_aespecifica']);
					$suma_presupuestado=$suma['suma_presupuesto_inicial'];
					$suma_financiero=$suma['suma_financiero_inicial'];
					$suma_incremento=$suma['suma_incremento'];
					$suma_disminucion=$suma['suma_disminucion'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_causado=$suma['suma_causado'];
					$suma_pagado=$suma['suma_pagado'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
					$suma_financiera_final=$suma_ajustado-$suma_pagado;

				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_financiero,2,',','.'),number_format($suma_incremento,2,',','.'),
					number_format($suma_disminucion,2,',','.'), number_format($suma_ajustado,2,',','.'),
					number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),
					number_format($suma_disponible,2,',','.'),number_format($suma_financiera_final,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
			
			}//fin for
		  
			/*FIN TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);
			
			
			
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto,$montof,$montoi,$montods,$montoa,$montoc,$montoca,$montop,number_format($montod,2,',','.'),$montofn));
			 }//FIN IF ACCION ESP	
			 
         }
		}//FIN FOR AESPECIFICA
		
		    $montot=number_format($montot,2,',','.'); 
			$montofi=number_format($montofi,2,',','.'); 
			$montoin=number_format($montoin,2,',','.'); 
			$montodi=number_format($montodi,2,',','.'); 
			$montoat=number_format($montoat,2,',','.');
			$montoct=number_format($montoct,2,',','.');
			$montocat=number_format($montocat,2,',','.'); 
			$montopt=number_format($montopt,2,',','.'); 
			$montodt=number_format($montodt,2,',','.');
			$montopfnt=number_format($montopfnt,2,',','.');
			
			$pdf->SetFont('Arial', 'B', 7);
		    $pdf->Row(array('','TOTALES', $montot,$montofi,$montoin,$montodi,$montoat,$montoct,$montocat,$montopt,$montodt,$montopfnt));
			
		
		
	  }
	  else //TIPO PRESUPUESTO
	  {
	      for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
         {

			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);

				
				$pdf->SetXY($x,$y);
			}
			
            $pdf->SetWidths(array(5,20,95,22,22,20,20,22,22,20,20,22,22));
            $pdf->SetAligns(array('C','L','L','R','R','R','R','R','R','R','R','R','R'));
            
			$monto=number_format($consulta[$i]['num_disponibilidad_presupuestaria_inicial'],2,',','.');  $montot=$montot+$consulta[$i]['num_disponibilidad_presupuestaria_inicial'];
			$montof=number_format($consulta[$i]['num_disponibilidad_financiera_inicial'],2,',','.');  $montofi=$montofi+$consulta[$i]['num_disponibilidad_financiera_inicial'];
			$montoi=number_format($consulta[$i]['num_incremento'],2,',','.');  $montoin=$montoin+$consulta[$i]['num_incremento'];
			$montods=number_format($consulta[$i]['num_disminucion'],2,',','.');  $montodi=$montodi+$consulta[$i]['num_disminucion'];
			$montoa=number_format($consulta[$i]['num_presupuesto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_presupuesto_ajustado'];
			$montoc=number_format($consulta[$i]['num_compromiso'],2,',','.'); $montoct=$montoct+$consulta[$i]['num_compromiso'];
			$montoca=number_format($consulta[$i]['num_causado'],2,',','.'); $montocat=$montocat+$consulta[$i]['num_causado'];
			$montop=number_format($consulta[$i]['num_pagado'],2,',','.'); $montopt=$montopt+$consulta[$i]['num_pagado'];
			$montod=$consulta[$i]['num_presupuesto_ajustado']-$consulta[$i]['num_compromiso']; $montodt=$montodt+$montod;
			$montofn=number_format($consulta[$i]['num_disponibilidad_financiera'],2,',','.'); $montopfnt=$montopfnt+$consulta[$i]['num_disponibilidad_financiera'];
			
			
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(202, 202, 202);
			
				
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			   
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTituloCierre($validacion['fk_prb004_num_presupuesto'],$partida,$validacion['fec_anio'],$validacion['fec_mes'],false);
					$suma_presupuestado=$suma['suma_presupuesto_inicial'];
					$suma_financiero=$suma['suma_financiero_inicial'];
					$suma_incremento=$suma['suma_incremento'];
					$suma_disminucion=$suma['suma_disminucion'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_causado=$suma['suma_causado'];
					$suma_pagado=$suma['suma_pagado'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
					$suma_financiera_final=$suma_ajustado-$suma_pagado;
				
				    $pdf->Row(array('',
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_financiero,2,',','.'),number_format($suma_incremento,2,',','.'),
					number_format($suma_disminucion,2,',','.'), number_format($suma_ajustado,2,',','.'),
					number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),
					number_format($suma_disponible,2,',','.'),number_format($suma_financiera_final,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTituloCierre($validacion['fk_prb004_num_presupuesto'],$partida,$validacion['fec_anio'],$validacion['fec_mes'],false);
					$suma_presupuestado=$suma['suma_presupuesto_inicial'];
					$suma_financiero=$suma['suma_financiero_inicial'];
					$suma_incremento=$suma['suma_incremento'];
					$suma_disminucion=$suma['suma_disminucion'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_causado=$suma['suma_causado'];
					$suma_pagado=$suma['suma_pagado'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
					$suma_financiera_final=$suma_ajustado-$suma_pagado;

				    $pdf->Row(array('',
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_financiero,2,',','.'),number_format($suma_incremento,2,',','.'),
					number_format($suma_disminucion,2,',','.'), number_format($suma_ajustado,2,',','.'),
					number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),
					number_format($suma_disponible,2,',','.'),number_format($suma_financiera_final,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
			
			}//fin for
			/*FIN TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);
			
			
			
            $pdf->Row(array($i+1,
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto,$montof,$montoi,$montods,$montoa,$montoc,$montoca,$montop,number_format($montod,2,',','.'),$montofn));
			 
        } 
	  
	  
	        $montot=number_format($montot,2,',','.'); 
			$montofi=number_format($montofi,2,',','.'); 
			$montoin=number_format($montoin,2,',','.'); 
			$montodi=number_format($montodi,2,',','.'); 
			$montoat=number_format($montoat,2,',','.');
			$montoct=number_format($montoct,2,',','.');
			$montocat=number_format($montocat,2,',','.'); 
			$montopt=number_format($montopt,2,',','.'); 
			$montodt=number_format($montodt,2,',','.');
			$montopfnt=number_format($montopfnt,2,',','.');
			
			$pdf->SetFont('Arial', 'B', 7);
		    $pdf->Row(array('','','TOTALES', $montot,$montofi,$montoin,$montodi,$montoat,$montoct,$montocat,$montopt,$montodt,$montopfnt));
			
	  }//FIN TIPO PRESUPUESTO
	  		
				
        $pdf->Output();


    }

	
	
	
	//---------------------------------------------------------------------------
	
}