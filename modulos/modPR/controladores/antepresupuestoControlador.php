<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO                   |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                |  lbastardo@contraloriadebolivar.gob.ve |         0424-9080200           |
 * |   |                                           |                                        |                                |
 * |___|___________________________________________|________________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        01-09-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class antepresupuestoControlador extends Controlador
{
    private $atAntepresupuesto;
	
    public function __construct()
    {
        parent::__construct();
        $this->atAntepresupuesto=$this->metCargarModelo('antepresupuesto');
		$this->atIndicepresupuestario=$this->metCargarModelo('indicepresupuestario');
		 
		$this->atPartida=$this->metCargarModelo('partida','maestros');
		$this->atSector=$this->metCargarModelo('sector','maestros');
		$this->atPrograma=$this->metCargarModelo('programa','maestros');
		$this->atSubPrograma=$this->metCargarModelo('subprograma','maestros');
		$this->atProyecto=$this->metCargarModelo('proyecto','maestros');
		$this->atActividad=$this->metCargarModelo('actividad','maestros');
		$this->atUnidadEjecutora=$this->metCargarModelo('unidadejecutora','maestros');
		
   }
	

    public function metIndex($status=false)
    {
		   
	    $js= array('materialSiace/core/demo/DemoTableDynamic','ModPR/prFunciones');
		 
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
 
		
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarCssComplemento($complementosCss);
       
		
        $this->atVista->assign('listado',$this->atAntepresupuesto->metListarAntepresupuesto($status));
		$this->atVista->assign('status', $status);
        $this->atVista->metRenderizar('listado');
    }


      public function metJsonIndicePresupuestario()
    {

        $ind_tipo_presupuesto = $this->metObtenerTexto('tipo_presupuesto');
		$ind_tipo_categoria = $this->metObtenerTexto('tipo_categoria');
		
		$filtro  = $this->atAntepresupuesto->metMostrarPresupuestoSelect($ind_tipo_presupuesto,$ind_tipo_categoria);
		
        echo json_encode($filtro);
        exit;
    }

    
	 public function metJsonIndiceValores()
    {

        $indice_presupuestario = $this->metObtenerInt('indice');		
		$filtro  = $this->atAntepresupuesto->metMostrarIndicePresupuestarioSelect($indice_presupuestario);
		
        echo json_encode($filtro);
        exit;

    }
   
 
    public function metCrearModificar()
    {
	   
	   $js[] = 'ModPR/jquery.formatCurrency-1.4.0';
	  
	  
	    $complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
		
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
			'inputmask/jquery.inputmask.bundle.min',
			
        );
		
		
		$this->atVista->metCargarJs($js);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
	
        $valido=$this->metObtenerInt('valido');
        $idAntepresupuesto=$this->metObtenerInt('idAntepresupuesto');
		
		
		$status = $this->metObtenerAlphaNumerico('status');
		$suma_total=0;	
		
        if($valido==1){
		
		
           $this->metValidarToken();
           $Excceccion=array('num_monto_generado','monto','ind_numero_gaceta','ind_numero_decreto','fec_decreto','fec_gaceta','Aespecifica','ind_estado','cod_antepresupuesto',
		   'fk_prb008_num_sector', 'fk_prb009_num_programa', 'fk_prb010_num_subprograma', 'fk_prb005_num_proyecto', 'fk_prb011_num_actividad',  'fk_prb012_unidad_ejecutora');
		    
			$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
			$formTxt=$this->metObtenerTexto('form','txt',$Excceccion);
			
			
          if($alphaNum!=null && $ind==null){
                $validacion=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $validacion=$ind;
            }else{
                $validacion=array_merge($alphaNum,$ind);
          }
        
		 
		  foreach ($alphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if(!empty($alphaNum[$tituloAlphaNum])){
                    $validacion[$tituloAlphaNum]=$valorAlphaNum;
                }else{
              				  
				   if (in_array($tituloAlphaNum, $Excceccion ) )
					  $validacion[$tituloAlphaNum]='';
				    else		
                      $validacion[$tituloAlphaNum]='error';

                }
            }
		 
		 
		   foreach ($ind as $tituloInt => $valorInt) {
                if(!empty($ind[$tituloInt]) && $ind[$tituloInt]>0 ){
                    $validacion[$tituloInt]=$valorInt;
                }else{
					if ($tituloInt=='num_monto_presupuestado')
					  $validacion[$tituloInt]='error';
				    else		
                      $validacion[$tituloInt]='0,00';
                }
            }
		 
		 	
			 if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                         if ($tituloTxt=='fec_gaceta' || $tituloTxt=='fec_decreto')
					        $validacion[$tituloTxt]='0000-00-00';
				         else		
                            $validacion[$tituloTxt]='error';
                    }
                }
            }
			
			
            if(!isset($validacion['fk_prb002_num_partida_presupuestaria'])){
               $validacion[$tituloTxt]='error';
            }
			
			
			if(!isset($validacion['monto'])){
                $validacion['monto']=false;
            }
			
			
			//ACCION ESPECIFICA
			if(!isset($validacion['Aespecifica'])){
                $validacion['Aespecifica']=false;
            }
			
			
			  if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
             }
			
			
			$validacion['num_monto_generado']=$this->FormatoMonto($validacion['num_monto_generado']); 
			$validacion['num_monto_presupuestado']=$this->FormatoMonto($validacion['num_monto_presupuestado']); 
			
			
			$validacion['fec_gaceta']=$this->FormatoFecha($validacion['fec_gaceta']); 
		    $validacion['fec_decreto']=$this->FormatoFecha($validacion['fec_decreto']); 	
		    $validacion['fec_antepresupuesto']=$this->FormatoFecha($validacion['fec_antepresupuesto']); 	
			$validacion['fec_inicio']=$this->FormatoFecha($validacion['fec_inicio']); 
			$validacion['dec_fin']=$this->FormatoFecha($validacion['dec_fin']); 
			
		   if($status == 'PR') {	
			
             if($idAntepresupuesto==0){
		 	
					$id=$this->atAntepresupuesto->metCrearAntepresupuesto( $validacion['fec_anio'], 
					$validacion['fec_antepresupuesto'],$validacion['fec_inicio'],$validacion['dec_fin'],$validacion['ind_numero_gaceta'],$validacion['fec_gaceta'], $validacion[
					'ind_numero_decreto'],$validacion['fec_decreto'],$validacion['ind_tipo_presupuesto'],$validacion['num_monto_presupuestado'],$validacion['num_monto_generado'],
				$validacion['fk_prb002_num_partida_presupuestaria'], $validacion['monto'], $validacion['Aespecifica'],$validacion['fk_a001_num_organismo'],$validacion['fk_prb008_num_sector'], $validacion['fk_prb009_num_programa'], $validacion['fk_prb010_num_subprograma'], $validacion['fk_prb005_num_proyecto'], $validacion['fk_prb011_num_actividad'], $validacion['fk_prb012_unidad_ejecutora'], $validacion['fk_prb016_indice_presupuestario']);
				
						$validacion['status']='nuevo';
              }else{
			
				
					$id=$this->atAntepresupuesto->metModificarAntepresupuesto($validacion['fec_anio'], $validacion['fec_antepresupuesto'],$validacion['fec_inicio'],
					$validacion['dec_fin'],$validacion['ind_numero_gaceta'],$validacion['fec_gaceta'], $validacion['ind_numero_decreto'],$validacion['fec_decreto'],
					$validacion['ind_tipo_presupuesto'],$validacion['num_monto_presupuestado'],$validacion['num_monto_generado'],  
					$validacion['fk_prb002_num_partida_presupuestaria'], $validacion['monto'], $validacion['Aespecifica'],$status,$validacion['fk_a001_num_organismo'], $validacion['fk_prb008_num_sector'], $validacion['fk_prb009_num_programa'], $validacion['fk_prb010_num_subprograma'], $validacion['fk_prb005_num_proyecto'], $validacion['fk_prb011_num_actividad'], $validacion['fk_prb012_unidad_ejecutora'], $validacion['fk_prb016_indice_presupuestario'], $idAntepresupuesto);     
					
						  $validacion['status']='modificar';
						
			   }
			
			}
			else if($status=='AP' || $status =='GE' || $status =='AN'  || $status=='RE' || $status=='RV'){
			   
	
                $id = $this->atAntepresupuesto->metAccionesAntepresupuesto($idAntepresupuesto,$status,$validacion['fk_prb002_num_partida_presupuestaria'], $validacion['monto']);
                if($status=='AP'){
                    $validacion['status'] = 'aprobado';
			    }else if($status=='RV'){
                    $validacion['status'] = 'revisado';
                }else if($status=='GE'){
                    $validacion['status'] = 'generado';
                }else if($status=='RE'){
                    $validacion['status'] = 'reversado';
				}else if($status=='AN'){
                    $validacion['status'] = 'anulado';
                }
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (is_array($validacion[$titulo])) {
                        if (strpos($id[2], $titulo)) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idAntepresupuesto']=$id;

            echo json_encode($validacion);
            exit;
        }


       
	    $this->atVista->assign('indicepresupuestario',$this->atIndicepresupuestario->metListarIndicePresupuestarioPrograma());
	   
        if($idAntepresupuesto!=0){
		
		    $this->atVista->assign('indicepresupuestario',$this->atIndicepresupuestario->metListarIndicePresupuestario());
		   
            $this->atVista->assign('formDB',$this->atAntepresupuesto->metMostrarAntepresupuesto($idAntepresupuesto));
			
            $this->atVista->assign('idAntepresupuesto',$idAntepresupuesto);
			
				
			//AGREGADO PARA MOSTRAR LAS PARTIDAS--------------------
			 $partidaDetalle = $this->atAntepresupuesto->metMostrarAntepresupuestoDetalle($idAntepresupuesto);
			 
			 $partidaTitulo = $this->atAntepresupuesto->metMostrarAntepresupuestoTitulo();
			 
			 
  //////////////////////////////////////////PRESUPUESTO POR PROYECTO///////////////////////////////////////////////
  if($partidaDetalle)
  if($partidaDetalle[0]['ind_tipo_presupuesto']=='P')
  {		
  
             $aespecifica = $this->atPartida->metAespecifica();
  	
			 $partida1='';
			 $partidat='';
			 
			 $suma=0;
			 $suma2=0;
			 $suma3=0;
			 $suma_total=0;
			  
			 $f=0;
			 $p=0;
			 $c=0;
			 
			 $cod_partida='';
			 $existe=0;
			 
	 $cuenta =0;
	 for($a=0;$a<count($aespecifica);$a++){
	    
	        $suma=0;
			$suma2=0;
			$suma3=0;
			$existeAespecifica=0;
		
			
		    $existeAespecifica = count($this->atAntepresupuesto->metExisteAespecificaEs($aespecifica[$a]['pk_num_aespecifica'], $idAntepresupuesto));
				 
			if ($existeAespecifica>0)
			{
		        $partidas[$cuenta]=array(
                        'id'=>$aespecifica[$a]['pk_num_aespecifica'],
                        'cod'=>$aespecifica[$a]['ind_cod_aespecifica'],
                        'partida'=>$aespecifica[$a]['ind_descripcion'],
						'monto'=>0,
						'tipo'=>'E',
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
                    );
			 }
		 
		
		
		for($i=0;$i<count($partidaDetalle) ;$i++){	
		   
		   if($f==0){
		      $partida1= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
			  $f=1;
			}  
		
		   if (trim($partida1)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5))){
		      $suma=0;
			  $f=0;
		   }
		   
		    if($p==0){
		      $partidat= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
			  $p=1;
			}  
		
		   if (trim($partidat)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8))){
		      $suma2=0;
			  $p=0;
		   }
		   
		  
		    if($c==0){
		      $partidat2= substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
			  $c=1;
			}  
		
		   if (trim($partidat2)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2))){
		      $suma3=0;
			  $c=0;
		   }   
		
		   
		   
		  if ($aespecifica[$a]['pk_num_aespecifica']==$partidaDetalle[$i]['fk_pr_b006_aespecifica'])
		  {
		        $suma=$suma+$partidaDetalle[$i]['num_monto_presupuestado']; 
			    $suma2=$suma2+$partidaDetalle[$i]['num_monto_presupuestado']; 
			    $suma3=$suma3+$partidaDetalle[$i]['num_monto_presupuestado']; 
			    $suma_total=$suma_total+$partidaDetalle[$i]['num_monto_presupuestado'];
			 
		     
			 for($j=0;$j<count($partidaTitulo);$j++){ 	
			  
			 
			    $codigo=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
				$codigo3=substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
				
		       			
			     $existe=0;
				 $posnum = 0;
				 $pos=0; 
				 reset($partidas);
				 foreach($partidas as  $key=>$item)
                  {
				        if (isset($item['cod']))
						{
						
						   if(trim($partidaTitulo[$j]['cod_partida'])==trim($item['cod']) && $item['aespecifica']==$aespecifica[$a]['pk_num_aespecifica'] && $item['tipo']=='T')
						   {
						       $pos= $key;
							   //echo ($item['cod']."-".$item['aespecifica']."-".$pos);
						       $existe=1; 
							   break 1;
						   } 
						  
						}
				      $posnum++;      
                  } 
				 				  
				
		if ($existe==1)		
		{
		   //echo ($posnum);
		  
		   if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			    $partidas[$pos]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma2,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr_b006_aespecifica'],
						'pk'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria']
						
                    );
				   
		   }
				
		  //-----------------------------------------------
			
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				   $partidas[$pos]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr_b006_aespecifica'],
						'pk'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria']
						
					
                    );
				   
				  }
				  
		//-----------------------------------------------	  
				   if (trim($codigo3)==trim($partidaTitulo[$j]['cod_partida'])){
				
				   $partidas[$pos]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma3,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr_b006_aespecifica'],
						'pk'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria']
					
                    );
				   
				  }
		
		}
		else //if existe
		{
		      $cuenta=$cuenta+1;
			  
			//---------------------------------------------	
				if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			    $partidas[$cuenta]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma2,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr_b006_aespecifica'],
						'pk'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria']
						
                    );
				   
				  }
				
		  //-----------------------------------------------
			
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				   $partidas[$cuenta]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr_b006_aespecifica'],
						'pk'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria']
						
					
                    );
				   
				  }
				  
		//-----------------------------------------------	  
				   if (trim($codigo3)==trim($partidaTitulo[$j]['cod_partida'])){
				
				   $partidas[$cuenta]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma3,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr_b006_aespecifica'],
						'pk'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria']
					
                    );
				   
				  }
			
			}	  
				 
				  
			 }//FIN FOR TITULO
			
			 //}//FIN FOR IF AESPECIFICA
			  //-----------------------------------------------
			      
			        $cuenta=$cuenta+1; 
					
                    $partidas[$cuenta]=array(
                        'id'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'],
                        'cod'=>$partidaDetalle[$i]['cod_partida'],
                        'partida'=>$partidaDetalle[$i]['ind_denominacion'],
						'monto'=>$partidaDetalle[$i]['num_monto_presupuestado'],
						'tipo'=>$partidaDetalle[$i]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr_b006_aespecifica'],
						'pk'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria']
                    );
					
					
				  }//FIN  IF AESPECIFICA
					 
               }//FIN FOR DETALLE
			   
			   
			    $cuenta=$cuenta+1;
             }//FIN FOR AESPECIFICA
		 
			
		
		//}//FIN IF ANTEPRESUPUESTO
		
		 $this->atVista->assign('aespecifica',$this->atPartida->metAespecifica());
		}
		//////////////////////////////////////////PRESDUPUESTO POR PROGRAMA///////////////////////////////////////////////
		else 
		{
		   
		     $partida1='';
			 $partidat='';
			 $partidat2='';
			 $partidatd='';
			 
			 $suma=0;
			 $suma2=0;
			 $suma3=0;
			 $suma_total=0;
			  
			 $f=0;
			 $p=0;
			 $c=0;
			 $cuenta=0;
			 $partidas=array();
			 
			  
		for($i=0;$i<count($partidaDetalle);$i++){	
		
		
		   //Comparaci�n partidas .00
		   if (trim($partidatd)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2))){
		      $suma3=0;
			  $c=0;
		   }
		   
		   
		   if($c==0){
		      $partidatd= substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
			  $c=1;
		   }  
		
			//Comparaci�n partidas .00.00
		   if (trim($partidat2)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5))){
		      $suma=0;
			  $f=0;
		   }
		   
		      
		   if($f==0){
		      $partidat2= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
			  $f=1;
			} 
		   
		   //Comparaci�n partidas .00.00.00
		   if (trim($partidat)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8))){
		      $suma2=0;
			  $p=0;
		   }
		   
		   if($p==0){
		      $partidat= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
			  $p=1;
			}  

		  		   
		   /////////////////
		 
		     $suma=$suma+$partidaDetalle[$i]['num_monto_presupuestado']; 
			 
			 $suma2=$suma2+$partidaDetalle[$i]['num_monto_presupuestado']; 
			 
			 $suma3=$suma3+$partidaDetalle[$i]['num_monto_presupuestado']; 
			 
			 $suma_total=$suma_total+$partidaDetalle[$i]['num_monto_presupuestado'];
			 
		   
		     for($j=0;$j<count($partidaTitulo);$j++){	
			 
			    $codigo=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
				$codigo3=substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
				
				
				$existe=0;
				$posnum = 0;
				$pos=0;
				
				 if (count($partidas)>0)
				 {
					 reset($partidas);
					 foreach($partidas as  $key=>$item)
					  {
							if (isset($item['cod']))
							{
							
							   if(trim($partidaTitulo[$j]['cod_partida'])==trim($item['cod']) && $item['tipo']=='T')
							   {
								   $pos= $key;
								   //echo ($item['cod']."-".$item['aespecifica']."-".$pos);
								   $existe=1; 
								   break 1;
							   } 
							  
							}
						  $posnum++;      
					  } 
				 }
				
	   if ($existe==1)		
		{
		  
		  //---------------------------------------------	
				if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			   $partidas[$pos]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma2,
						'aespecifica'=>0,
						'tipo'=>$partidaTitulo[$j]['ind_tipo']
					
                    );
				   
				  }
				
				
		  //-----------------------------------------------
			
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				   $partidas[$pos]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma,
						'aespecifica'=>0,
						'tipo'=>$partidaTitulo[$j]['ind_tipo']
                    );
				   
				  }
				  
			//-----------------------------------------------	  
				   if (trim($codigo3)==trim($partidaTitulo[$j]['cod_partida'])){
				   $partidas[$pos]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma3,
						'aespecifica'=>0,
						'tipo'=>$partidaTitulo[$j]['ind_tipo']
					
                    );
				   
				  }
				  
				   $cuenta=$cuenta+1;
		
		}
		else //IF EXISTE*/
		//if ($existe==0)	
		{
			
			//---------------------------------------------	
				if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			    $partidas[$cuenta]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma2,
						'aespecifica'=>0,
						'tipo'=>$partidaTitulo[$j]['ind_tipo']
					
                    );
				   
				  }
				
				
		  //-----------------------------------------------
			
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$cuenta]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma,
						'aespecifica'=>0,
						'tipo'=>$partidaTitulo[$j]['ind_tipo']
					
                    );
				   
				  }
				  
			//-----------------------------------------------	  
				   if (trim($codigo3)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$cuenta]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma3,
						'aespecifica'=>0,
						'tipo'=>$partidaTitulo[$j]['ind_tipo']
					
                    );
				   
				 }
				   $cuenta=$cuenta+1;
			 
			 
			 }//FIN EXITE
			 
			}//FIN J
			  //-----------------------------------------------
			
                    $partidas[$cuenta]=array(
                        'id'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'],
                        'cod'=>$partidaDetalle[$i]['cod_partida'],
                        'partida'=>$partidaDetalle[$i]['ind_denominacion'],
						'monto'=>$partidaDetalle[$i]['num_monto_presupuestado'],
						'aespecifica'=>0,
						'tipo'=>$partidaDetalle[$i]['ind_tipo']
                    );
					
                    $cuenta=$cuenta+1;
			 		
           //}// ?
				   
		}//FIN I
		
		
	 }//FIN IF ANTEPRESUPUESTO
	 //var_dump($partidas);
	 //exit;
	 
	 }//?????
	 
		    if(isset($partidas)){
                $this->atVista->assign('partidas', $partidas);
            }
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		$this->atVista->assign('suma_total',$suma_total);
        $this->atVista->assign('cuenta',$this->atAntepresupuesto->metCuentaListar());
		$this->atVista->assign('status',$status);
		
        $this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());
		$this->atVista->assign('sector',$this->atSector->metListarSector());
		$this->atVista->assign('programa',$this->atPrograma->metListarPrograma());
		$this->atVista->assign('subprograma',$this->atSubPrograma->metListarSubprograma());
		$this->atVista->assign('proyecto',$this->atProyecto->metListarProyecto());
		$this->atVista->assign('actividad',$this->atActividad->metListarActividad());
		$this->atVista->assign('unidadejecutora',$this->atUnidadEjecutora->metListarUnidadEjecutora());
		
		
		
		
		
		 if($status =='GE'){
            
			$this->atVista->metRenderizar('CrearModificarGenerar','modales');
		 }else {
		   
		    $this->atVista->metRenderizar('CrearModificar','modales');
		 }	
    }

 
 
    public function metEliminar()
    {
        $idAntepresupuesto = $this->metObtenerInt('idAntepresupuesto');
        if($idAntepresupuesto!=0){
            $id=$this->atAntepresupuesto->metEliminarAntepresupuesto($idAntepresupuesto);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el antepresupuesto se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idAntepresupuesto'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
	
	
	
	protected function FormatoFecha($fecha)
    {
        $resultado=explode("-",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }	
	
	
	protected function FormatoMonto($monto_f)
    {
       if(!isset($monto_f)){
                $monto_f=0;
            }
			else {
			     $monto_f=str_replace(".", "", $monto_f);
			     $monto_f=str_replace(",", ".", $monto_f);
				 
			}
			 return $monto_f;
    }	
	
	
}