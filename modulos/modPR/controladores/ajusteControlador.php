<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO                   |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                |  lbastardo@contraloriadebolivar.gob.ve |         0424-9080200           |
 * |   |                                           |                                        |                                |
 * |___|___________________________________________|________________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        01-09-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class ajusteControlador extends Controlador
{
    private $atAjuste;
	
    public function __construct()
    {
        parent::__construct();
        $this->atAjuste=$this->metCargarModelo('ajuste');
	    $this->atPresupuesto=$this->metCargarModelo('presupuesto');
		$this->atPartida=$this->metCargarModelo('partida','maestros');
		$this->atReformulacion=$this->metCargarModelo('reformulacion');
		
    }
	
    /**
	*Funcion indice de ajuste presupuestario
	*
	*Parametro: $status=false
	*Return:
	*Salida: $this->atVista->metRenderizar('listado')
	*
	**/
    public function metIndex($status=false)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
			'form/form2',
			
        );
       
	   $js = array('materialSiace/core/demo/DemoTableDynamic','ModPR/jquery.formatCurrency-1.4.0','ModPR/prFunciones');
		
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->atAjuste->metListarAjuste($status));
		$this->atVista->assign('status', $status);
        $this->atVista->metRenderizar('listado');
    }


    /**
	*Carga de select de presupuesto segun el a�o
	*
	*Parametro: .
	*Return: json_encode($filtro)
	*
	**/
    public function metJsonPresupuesto()
    {

        $anio = $this->metObtenerInt('anio');
		$filtro  = $this->atReformulacion->metListarPresupuestoAnio($anio);
        echo json_encode($filtro);
        exit;

    }


    /**
	*Crear, Modifica y Consultar Ajustes presupuestarios 
	*
	*Parametro: 
	*Return: 
	*
	**/
    public function metCrearModificar()
    {
	
	   $complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
			'inputmask/jquery.inputmask.bundle.min',
        );
		

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
	
        $valido=$this->metObtenerInt('valido');
        $idAjuste=$this->metObtenerInt('idAjuste');
		$status = $this->metObtenerAlphaNumerico('status');
		$inicial = $this->metObtenerAlphaNumerico('inicial');
		$suma_total=0;	
		$suma_total_d=0;
		
		//$valido=1;
		//$status='AP';
		
        if($valido==1){
		
        $this->metValidarToken();  
		   
           $Excceccion=array('monto','fk_prc002_num_presupuesto_det','ind_cod_ajuste_presupuestario','ind_estado', 'Aespecifica','fk_prb004_num_presupuesto');
		
			$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
			$formTxt=$this->metObtenerTexto('form','txt',$Excceccion);
			
		 
		  foreach ($alphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if(!empty($alphaNum[$tituloAlphaNum])){
                    $validacion[$tituloAlphaNum]=$valorAlphaNum;
                }else{
                   if ($tituloAlphaNum=='ind_cod_ajuste_presupuestario' || $tituloAlphaNum=='ind_estado' || $tituloAlphaNum=='Aespecifica')
					  $validacion[$tituloAlphaNum]='';
				    else		
                      $validacion[$tituloAlphaNum]='error';

                }
            }
		 
		 
		   foreach ($ind as $tituloInt => $valorInt) {
                if(!empty($ind[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='0,00';
                }
            }
		 
		 
			 if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    }else{
					if ($tituloInt=='num_total_ajuste')
					  $validacion[$tituloInt]='error';
				    else		
                      $validacion[$tituloInt]='0,00';
                }
                }
            }
		
		
		     //ACCION ESPECIFICA
			if(!isset($validacion['Aespecifica']) || $validacion['Aespecifica']=='' ){
                $validacion['Aespecifica']=false;
           }	
       		
			 if(!isset($validacion['fk_prc002_num_presupuesto_det'])){
                $validacion['fk_prc002_num_presupuesto_det']=false;
            }
			
			
			if(!isset($validacion['monto'])){
                $validacion['monto']=false;
            }
			
			if(!isset($validacion['monto_inicial'])){
                $validacion['monto_inicial']=false;
            }

            $formFormula=$this->metObtenerFormulas('ind_descripcion');
			if($formFormula) {
                $validacion['ind_descripcion'] = $formFormula;
            } else {
                $validacion['ind_descripcion'] = 'error';
            }
		
			
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
		
		
			$validacion['num_total_ajuste']=$this->FormatoMonto($validacion['num_total_ajuste']); 
			$validacion['fec_ajuste']=$this->FormatoFecha($validacion['fec_ajuste']); 
			$validacion['fec_gaceta']=$this->FormatoFecha($validacion['fec_gaceta']); 
		    $validacion['fec_resolucion']=$this->FormatoFecha($validacion['fec_resolucion']);
			$anio=$validacion['fec_anio'];
			$mes=substr($validacion['fec_ajuste'], 5,2);
			
			
		 if($status=='PR'){
					
             if($idAjuste==0){
			
	         $id=$this->atAjuste->metCrearAjuste( $validacion['fk_prb004_num_presupuesto'],$validacion['fk_a006_num_miscelaneo_det_tipo_ajuste'],$validacion['fec_anio'], $mes,$validacion['ind_numero_gaceta'],$validacion['fec_gaceta'], $validacion['ind_numero_resolucion'],$validacion['fec_resolucion'],$validacion['num_total_ajuste'],$validacion['ind_descripcion'], $validacion['fk_prc002_num_presupuesto_det'], $validacion['monto'], $validacion['monto_inicial'], $validacion['Aespecifica']);
                $validacion['status']='nuevo';
               }else{
			
              $id=$this->atAjuste->metModificarAjuste( $validacion['fk_a006_num_miscelaneo_det_tipo_ajuste'],$validacion['fec_anio'], $mes,$validacion['ind_numero_gaceta'],$validacion['fec_gaceta'], $validacion['ind_numero_resolucion'],$validacion['fec_resolucion'],$validacion['num_total_ajuste'],$validacion['ind_descripcion'], $validacion['fk_prc002_num_presupuesto_det'], $validacion['monto'], $validacion['monto_inicial'],$validacion['Aespecifica'],$idAjuste);
                $validacion['status']='modificar';
               }
			
			}
			else if($status=='AP'  || $status =='AN'){
			   
			    //$id = $this->atAjuste->metAccionesAjuste(1,'AP',4);
                $id = $this->atAjuste->metAccionesAjuste($idAjuste,$status,$validacion['fk_a006_num_miscelaneo_det_tipo_ajuste']);
                if($status=='AP'){
                    $validacion['status'] = 'aprobado';
                }else{
                    $validacion['status'] = 'anulado';
                }
            }
			

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idAjuste']=$id;

            echo json_encode($validacion);
            exit;
        }

       $suma_total_p=0; 
	   $suma_total_c=0;

        if($idAjuste!=0){
		    
            $this->atVista->assign('formDB',$this->atAjuste->metMostrarAjuste($idAjuste));
			
            $this->atVista->assign('idAjuste',$idAjuste);
			
			
			//AGREGADO PARA MOSTRAR LAS PARTIDAS--------------------
			 $partidaDetalle = $this->atAjuste->metMostrarAjusteDetalle($idAjuste,$status);
			 
			 //var_dump($partidaDetalle);
			 
			 //$codPresupuesto=   $partidaDetalle[0]['ind_cod_presupuesto'];
			 
			 $partidaTitulo = $this->atPresupuesto->metMostrarPresupuestoTitulo();
			 
	   
	   if($partidaDetalle)		 
         if($partidaDetalle[0]['ind_tipo_presupuesto']=='P')
         {
		      $aespecifica = $this->atPartida->metAespecifica();
			  
			  $partida1='';
			  $partidat='';
			  $partidat2='';
			 
			  $suma=0;
			  $suma2=0;
			  $suma3=0;
			  
			  $suma3_d=0;
			  $suma_total=0;
			 
			  $suma_d=0;
			  $suma2_d=0;			 
			  
			  $suma_c=0;
			  $suma2_c=0;	
			  
			  $f=0;
			  $p=0;
			  $c=0;
			  $cuenta=0;
			  
			  $suma_total_p=0; 
			  $suma_total_c=0;
			  
			  $pre_compromiso=0;
			  
			  
	     for($a=0;$a<count($aespecifica);$a++){
	    
				$suma=0;
				$suma2=0;
				$suma3=0;
				
				$suma_c=0;
				$suma2_c=0;
				$suma3_c=0;
				
				$suma_d=0;
				$suma2_d=0;
				$suma3_d=0;
				
				$suma_precompromiso=0;
			    $suma_precompromiso_t=0;	 
			    $suma_precompromiso_d=0;
				
				
				$existeAespecifica = count($this->atAjuste->metExisteAespecificaEs($aespecifica[$a]['pk_num_aespecifica'], $partidaDetalle[0]['pk_num_presupuesto']));
					 
				if ($existeAespecifica>0)
				{
					$partidas[$cuenta]=array(
					        'pk'=>0,
							'id'=>$aespecifica[$a]['pk_num_aespecifica'],
							'cod'=>$aespecifica[$a]['ind_cod_aespecifica'],
							'partida'=>$aespecifica[$a]['ind_descripcion'],
							'monto'=>0,
							'tipo'=>'E',
							'aespecifica'=>''
						);
						
				 }		  
				 
			  
		for($i=0;$i<count($partidaDetalle);$i++){	
		
		    //Comparaci�n partidas .00	
		    if (trim($partidat2)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2))){
		      $suma3_d=0;
		      $suma3=0;
			  $suma_precompromiso_d=0;
			  $c=0;
		   }   	
		
		
	        if($c==0){
		      $partidat2= substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
			  $c=1;
			}  
		
		   
	       //Comparaci�n partidas .00.00	
		   if (trim($partida1)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5))){
		      $suma_d=0;
		      $suma=0;
			  $suma_precompromiso=0;
			  $f=0;
		   }

		
		   if($f==0){
		      $partida1= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
			  $f=1;
			}  
		
	
		    //Comparaci�n partidas .00.00.00	
		    if (trim($partidat)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8))){
		      $suma2_d=0;
		      $suma2=0;
			  $suma_precompromiso_t=0;
			  $p=0;
		   }
		   
		    if($p==0){
		      $partidat= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
			  $p=1;
			}  
		
		   
		  if ($aespecifica[$a]['pk_num_aespecifica']==$partidaDetalle[$i]['fk_pr006_num_aespecifica'])
		  {
		    
			$pre_compromiso = $this->atPresupuesto->metMostrarPresupuestoDetallepc($partidaDetalle[0]['pk_num_presupuesto'], $partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'], $partidaDetalle[$i]['fk_pr006_num_aespecifica'] );
			
		     $suma_d=$suma_d+$partidaDetalle[$i]['num_monto_disponible']; 
		     $suma=$suma+$partidaDetalle[$i]['num_monto_ajustado']; 
			 
			 $suma2_d=$suma2_d+$partidaDetalle[$i]['num_monto_disponible']; 
			 $suma2=$suma2+$partidaDetalle[$i]['num_monto_ajustado']; 
			 
			 $suma3_d=$suma3_d+$partidaDetalle[$i]['num_monto_disponible']; 
			 $suma3=$suma3+$partidaDetalle[$i]['num_monto_ajustado']; 
			 
			 
			 $suma_total_d=$suma_total_d+$partidaDetalle[$i]['num_monto_disponible'];
			 $suma_total=$suma_total+$partidaDetalle[$i]['num_monto_ajustado'];
			 
			 
			 //-----------------MONTO COMPROMISO------------------//
			 $suma_c=$suma_c+$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma2_c=$suma2_c+$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma3_c=$suma3_c+$partidaDetalle[$i]['num_monto_compromiso']; 
		     $suma_total_c=$suma_total_c+$partidaDetalle[$i]['num_monto_compromiso'];
			 
			 
			 //------------------MONTO PRE-COMPROMISO--------------//
			 
			 $suma_precompromiso_t=$suma_precompromiso_t+$pre_compromiso;
			 $suma_precompromiso=$suma_precompromiso+$pre_compromiso;
			 $suma_precompromiso_d=$suma_precompromiso_d+$pre_compromiso;
			 $suma_total_p=$suma_total_p+$pre_compromiso;		
			 
			 //------------------------------------------------------
			 
		     for($j=0;$j<count($partidaTitulo);$j++){	
			   
			    $cuenta=$cuenta+1;
			 
			    $codigo=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
				$codigo3=substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
				
				$existe=0;
				$posnum = 0;
				$pos=0; 
				 reset($partidas);
				 foreach($partidas as  $key=>$item)
                  {
				        if (isset($item['cod']))
						{
						   if(trim($partidaTitulo[$j]['cod_partida'])==trim($item['cod']) && $item['aespecifica']==$aespecifica[$a]['pk_num_aespecifica'] && $item['tipo']=='T')
						   {
						       $pos= $key;
							   //echo ($item['cod']."-".$item['aespecifica']."-".$pos);
						       $existe=1; 
							   break 1;
						   } 
						  
						}
				      $posnum++;      
                  } 
				  
		 if ($existe==1)		
		 {	
		    //---------------------------------------------	
				if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			    $partidas[$pos]=array(
			            'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma2_d,
						'monto_comprometido'=>$suma2_c,
						'monto_disponible'=>$suma2_d-$suma2_c,
						'monto'=>$suma2,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }
				
		  //-----------------------------------------------
			
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				   $partidas[$pos]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma_d,
						'monto_comprometido'=>$suma_c,
						'monto_disponible'=>$suma_d-$suma_c,
						'monto'=>$suma,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }	 
				  
				  
				  if (trim($codigo3)==trim($partidaTitulo[$j]['cod_partida'])){
				
				   $partidas[$pos]=array(
                        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma3_d,
						'monto_comprometido'=>$suma3_c,
						'monto_disponible'=>$suma3_d-$suma3_c,
						'monto'=>$suma3,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }
	 
		 }
		 else//IF EXISTE
		 {	
			//---------------------------------------------	
				if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			   $partidas[$cuenta]=array(
			            'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma2_d,
						'monto_comprometido'=>$suma2_c,
						'monto_disponible'=>$suma2_d-$suma2_c,
						'monto'=>$suma2,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }
				
		  //-----------------------------------------------
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$cuenta]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma_d,
						'monto_comprometido'=>$suma_c,
						'monto_disponible'=>$suma_d-$suma_c,
						'monto'=>$suma,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }	 
				  
				  
				  if (trim($codigo3)==trim($partidaTitulo[$j]['cod_partida'])){
				
				   $partidas[$cuenta]=array(
                        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						//'monto_inicial'=>$suma3_d,
						'monto_inicial'=>$suma3,
						'monto_comprometido'=>$suma3_c,
						'monto_disponible'=>$suma3_d-$suma3_c,
						'monto'=>$suma3,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }
				  
			   }//FIN IF EXISTE 
			 }//FIN FOR TITULO
			        $cuenta=$cuenta+1;
                    $partidas[$cuenta]=array(
					    'pk'=>$partidaDetalle[$i]['pk_num_presupuesto_det'],
                        'id'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'],
                        'cod'=>$partidaDetalle[$i]['cod_partida'],
                        'partida'=>$partidaDetalle[$i]['ind_denominacion'],
						//'monto_inicial'=>$partidaDetalle[$i]['num_monto_disponible'],
						'monto_inicial'=>$partidaDetalle[$i]['num_monto_ajustado'],
						'monto'=>$partidaDetalle[$i]['num_monto_ajustado'],
						'monto_comprometido'=>$partidaDetalle[$i]['num_monto_compromiso'],
						'monto_disponible'=>$partidaDetalle[$i]['num_monto_disponible']-$partidaDetalle[$i]['num_monto_compromiso'],
						'tipo'=>$partidaDetalle[$i]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
                    );
					
                }//FIN  IF AESPECIFICA   
			   
             }//FIN FOR DETALLE
			    $cuenta=$cuenta+1;
		    }//FIN FOR AESPECIFICA	
		    	
		 }	//FIN PRESUPUESTO POR PROYECTO 
		 else //ELSE TIPO PRESUPUESTO
		 {
			 
			  $partida1='';
			  $partidat='';
		      $partidat2='';
			 
			  $suma=0;
			  $suma2=0;
			  $suma3=0;
			  $sumac=0;
			  $suma_total=0;
			  $suma_total_d=0;
			  $suma_total_c=0;
			  
			 
			  $suma_d=0;
			  $suma2_d=0;
			  $suma3_d=0;
			  	
			  $suma_c=0;
			  $suma2_c=0;
			  $suma3_c=0;	
			  
			  $suma_precompromiso=0;
			  $suma_precompromiso_t=0;	 
			  $suma_precompromiso_d=0;	
			  $suma_total_p=0; 
			  $suma_total_c=0;
			  
			  $f=0;
			  $p=0;
			 
			  
		for($i=0;$i<count($partidaDetalle);$i++){	
		
		$pre_compromiso=0;
		
		   //Comparaci�n partidas .00	
		    if (trim($partidat2)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2))){
		      $suma3_d=0;
		      $suma3=0;
			  $suma_precompromiso_d=0;	
			  $c=0;
		   }   	
		
		
	        if($c==0){
		      $partidat2= substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
			  $c=1;
			}  
			
		
		   //Comparaci�n Partidas .00.00
		   if (trim($partida1)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5))){
		      $suma_d=0;
		      $suma=0;
			  $suma_precompromiso=0;	
			  $f=0;
		   }
		
		
		   if($f==0){
		      $partida1= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
			  $f=1;
			}  
		
		  
		  //Comparaci�n Partidas .00.00.00
		   if (trim($partidat)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8))){
		      $suma2_d=0;
		      $suma2=0;
			  $suma_precompromiso_t=0;	
			  $p=0;
		   }

		   
		    if($p==0){
		      $partidat= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
			  $p=1;
			}  
		    
			 $pre_compromiso = $this->atPresupuesto->metMostrarPresupuestoDetallepc($partidaDetalle[0]['pk_num_presupuesto'], $partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'] );
		   
		     $suma_d=$suma_d+$partidaDetalle[$i]['num_monto_disponible'];   
		     $suma=$suma+$partidaDetalle[$i]['num_monto_ajustado'];      
			 
			 $suma2_d=$suma2_d+$partidaDetalle[$i]['num_monto_disponible'];
			 $suma2=$suma2+$partidaDetalle[$i]['num_monto_ajustado']; 
			 
			 $suma_total_d=$suma_total_d+$partidaDetalle[$i]['num_monto_disponible'];  
			 $suma_total=$suma_total+$partidaDetalle[$i]['num_monto_ajustado'];
			 
			 
			  //-----------------MONTO COMPROMISO------------------//
			 $suma_c=$suma_c+$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma2_c=$suma2_c+$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma3_c=$suma3_c+$partidaDetalle[$i]['num_monto_compromiso']; 
			 
		     $suma_total_c=$suma_total_c+$partidaDetalle[$i]['num_monto_compromiso'];
			 
			 
			 //--------------------MONTO PRE-COMPROMISO--------------------
			 
			 $suma_precompromiso_t=$suma_precompromiso_t+$pre_compromiso;
			 $suma_precompromiso=$suma_precompromiso+$pre_compromiso;
			 $suma_precompromiso_d=$suma_precompromiso_d+$pre_compromiso;
			 $suma_total_p=$suma_total_p+$pre_compromiso;		
			 
			 //------------------------------------------------------
		   
		     for($j=0;$j<count($partidaTitulo);$j++){	
			   
			 
			    $codigo=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
				$codigo3=substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
				
			//---------------------------------------------	
				if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			   $partidas[$partidaTitulo[$j]['pk_num_partida_presupuestaria']]=array(
			            'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						//'monto_inicial'=>$suma2_d,
						'monto_inicial'=>$suma2,
						'monto_comprometido'=>$suma2_c,
						'monto_disponible'=>$suma2_d-$suma2_c,
						'monto'=>$suma2,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
					
                    );
				   
				  }
				
		  //-----------------------------------------------
			
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$partidaTitulo[$j]['pk_num_partida_presupuestaria']]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						//'monto_inicial'=>$suma_d,
						'monto_inicial'=>$suma,
						'monto_comprometido'=>$suma_c,
						'monto_disponible'=>$suma_d-$suma_c,
						'monto'=>$suma,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
                    );
				   
				  }	 
				  
				  
				  if (trim($codigo3)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$partidaTitulo[$j]['pk_num_partida_presupuestaria']]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						//'monto_inicial'=> $suma3_d,
						'monto_inicial'=> $suma3,
						'monto_comprometido'=>$suma3_c,
						'monto_disponible'=>$suma3_d-$suma3_c,
						'monto'=>$suma,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
                    );
				   
				  }	 
			    
			 }
			        
                    $partidas[$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria']]=array(
					    'pk'=>$partidaDetalle[$i]['pk_num_presupuesto_det'],
                        'id'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'],
                        'cod'=>$partidaDetalle[$i]['cod_partida'],
                        'partida'=>$partidaDetalle[$i]['ind_denominacion'],
						//'monto_inicial'=>$partidaDetalle[$i]['num_monto_disponible'],
						'monto_inicial'=>$partidaDetalle[$i]['num_monto_ajustado'],
						'monto'=>$partidaDetalle[$i]['num_monto_ajustado'],
						'monto_comprometido'=>$partidaDetalle[$i]['num_monto_compromiso'],
						'monto_disponible'=>$partidaDetalle[$i]['num_monto_disponible']-$partidaDetalle[$i]['num_monto_compromiso'],
						'tipo'=>$partidaDetalle[$i]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
                    );
					
               
            }
		}//FIN TIPO DE PRESUPUESTO	
			
			 if(isset($partidas)){
                $this->atVista->assign('partidas', $partidas);
            }
			
			
        }
		$this->atVista->assign('suma_total',$suma_total_d);
		$this->atVista->assign('suma_total_comp',$suma_total_c);
		$this->atVista->assign('suma_total_disponible',$suma_total_d-$suma_total_c);
		$this->atVista->assign('suma_total_ajuste',$suma_total);
        $this->atVista->assign('tipoajuste',$this->atAjuste->metTipoAjusteListar());
		$this->atVista->assign('status',$status);
		
		
		//------------------------------------------------------------------------
		//------------------------------------------------------------------------
		
		$this->atVista->assign('presupuesto',$this->atReformulacion->metPresupuestoListar());
		$this->atVista->metRenderizar('CrearModificar','modales');
		
       //if ($idAjuste==0)
	      // $this->atVista->metRenderizar('ListadoPartidas');
		
    }


     /**
	*Carga las consulta de partidas de ajuste presupuestario
	*
	*Parametro: 
	*Return: 
	*
	**/
    public function metBuscarPartidas()
   {
      $suma_total=0;
      $idAjuste=$this->metObtenerInt('idAjuste');
	  $status=$this->metObtenerInt('status');
	  $Presupuesto=$this->metObtenerInt('idPresupuesto');
	  $seleccionado=$this->metObtenerInt('seleccionado');
	
	    
      if ($idAjuste==0)
	  {
          $suma_total_d=0;
		
		 // var_dump($Presupuesto);
		  
		      $IdPresupuesto = $this->atAjuste->metIdPresupuesto();
			  
			   if ($seleccionado==0)
			        $Presupuesto=$IdPresupuesto[0]['pk_num_presupuesto'];
					
					//var_dump($Presupuesto);
			
			  
			   if( $IdPresupuesto)
				 {
			       $codPresupuesto = $IdPresupuesto[0]['ind_cod_presupuesto'];
			       $tipoPresupuesto = $IdPresupuesto[0]['ind_tipo_presupuesto'];  
				   $partidaDetalle = $this->atPresupuesto->metMostrarPresupuestoDetalle($Presupuesto); 
				   
				 } 
				 else
				 {
				    $codPresupuesto = '0000';
			        $tipoPresupuesto = 'G';   
					$partidaDetalle=NULL;
				 }
			  
			 
			 $partidaTitulo = $this->atPresupuesto->metMostrarPresupuestoTitulo();
			 
			 $suma_total_d=0; 
			 $suma_total_c=0;
			 
			 
			 //-----------------------------------------------------------------------------------
			 
		 if($partidaDetalle)		 
         if($partidaDetalle[0]['ind_tipo_presupuesto']=='P')
         {
		      $aespecifica = $this->atPartida->metAespecifica();
			  
			  $partida1='';
			  $partidat='';
			  $partidat2='';
			 
			  $suma=0;
			  $suma2=0;
			  $suma_total=0;
			 
			  $suma_d=0;
			  $suma2_d=0;	
			  
			  $suma_c=0;
			  $suma2_c=0;
			  $suma3_c=0;
			  $suma_total_c=0;		
			  
			  $suma_precompromiso=0;
			  $suma_precompromiso_t=0;	 
			  $suma_total_p=0;
			  
			  $suma_disponible=0;
			  $suma_disponible_t=0;	 
			  $suma_total_disponible=0;
			  
			  $f=0;
			  $p=0;
			  $c=0;
			  $cuenta=0;
			  
	     for($a=0;$a<count($aespecifica);$a++){
	    
				$suma=0;
				$suma2=0;
				$suma3=0;
				
				$suma_c=0;
				$suma2_c=0;
				$suma3_c=0;
				
				$suma_d=0;
				$suma2_d=0;
				$suma3_d=0;
				
				$suma_precompromiso=0;
			    $suma_precompromiso_t=0;	 
			    $suma_precompromiso_d=0;
				
				
				$existeAespecifica = count($this->atAjuste->metExisteAespecificaEs($aespecifica[$a]['pk_num_aespecifica'], $partidaDetalle[0]['pk_num_presupuesto']));
					 
				if ($existeAespecifica>0)
				{
					$partidas[$cuenta]=array(
					        'pk'=>0,
							'id'=>$aespecifica[$a]['pk_num_aespecifica'],
							'cod'=>$aespecifica[$a]['ind_cod_aespecifica'],
							'partida'=>$aespecifica[$a]['ind_descripcion'],
							'monto'=>0,
							'tipo'=>'E',
							'aespecifica'=>''
						);
						
				 }		  
				 
			  
		for($i=0;$i<count($partidaDetalle);$i++){	
		
		    //Comparaci�n partidas .00	
		    if (trim($partidat2)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2))){
		      $suma3_d=0;
		      $suma3=0;
			  $suma_precompromiso_d=0;
			  $c=0;
		   }   	
		
		
	        if($c==0){
		      $partidat2= substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
			  $c=1;
			}  
		
		   
	       //Comparaci�n partidas .00.00	
		   if (trim($partida1)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5))){
		      $suma_d=0;
		      $suma=0;
			  $suma_precompromiso=0;
			  $f=0;
		   }

		
		   if($f==0){
		      $partida1= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
			  $f=1;
			}  
		
	
		    //Comparaci�n partidas .00.00.00	
		    if (trim($partidat)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8))){
		      $suma2_d=0;
		      $suma2=0;
			  $suma_precompromiso_t=0;
			  $p=0;
		   }
		   
		    if($p==0){
		      $partidat= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
			  $p=1;
			}  
		
		   
		   
		  if ($aespecifica[$a]['pk_num_aespecifica']==$partidaDetalle[$i]['fk_pr006_num_aespecifica'])
		  {
		    
			$pre_compromiso = $this->atPresupuesto->metMostrarPresupuestoDetallepc($partidaDetalle[0]['pk_num_presupuesto'], $partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'], $partidaDetalle[$i]['fk_pr006_num_aespecifica'] );
			
		     $suma_d=$suma_d+$partidaDetalle[$i]['num_monto_disponible']; 
		     $suma=$suma+$partidaDetalle[$i]['num_monto_ajustado']; 
			 
			 $suma2_d=$suma2_d+$partidaDetalle[$i]['num_monto_disponible']; 
			 $suma2=$suma2+$partidaDetalle[$i]['num_monto_ajustado']; 
			 
			 $suma3_d=$suma3_d+$partidaDetalle[$i]['num_monto_disponible']; 
			 $suma3=$suma3+$partidaDetalle[$i]['num_monto_ajustado']; 
			 
			 
			 $suma_total_d=$suma_total_d+$partidaDetalle[$i]['num_monto_disponible'];
			 $suma_total=$suma_total+$partidaDetalle[$i]['num_monto_ajustado'];
			 
			 
			 //-----------------MONTO COMPROMISO------------------//
			 $suma_c=$suma_c+$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma2_c=$suma2_c+$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma3_c=$suma3_c+$partidaDetalle[$i]['num_monto_compromiso']; 
		     $suma_total_c=$suma_total_c+$partidaDetalle[$i]['num_monto_compromiso'];
			 
			 
			 //------------------MONTO PRE-COMPROMISO--------------//
			 
			 $suma_precompromiso_t=$suma_precompromiso_t+$pre_compromiso;
			 $suma_precompromiso=$suma_precompromiso+$pre_compromiso;
			 $suma_precompromiso_d=$suma_precompromiso_d+$pre_compromiso;
			 $suma_total_p=$suma_total_p+$pre_compromiso;		
			 
			 //------------------------------------------------------
			 
		     for($j=0;$j<count($partidaTitulo);$j++){	
			   
			    $cuenta=$cuenta+1;
			 
			    $codigo=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
				$codigo3=substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
				
				$existe=0;
				$posnum = 0;
				$pos=0; 
				 reset($partidas);
				 foreach($partidas as  $key=>$item)
                  {
				        if (isset($item['cod']))
						{
						   if(trim($partidaTitulo[$j]['cod_partida'])==trim($item['cod']) && $item['aespecifica']==$aespecifica[$a]['pk_num_aespecifica'] && $item['tipo']=='T')
						   {
						       $pos= $key;
							   //echo ($item['cod']."-".$item['aespecifica']."-".$pos);
						       $existe=1; 
							   break 1;
						   } 
						  
						}
				      $posnum++;      
                  } 
				  
		 if ($existe==1)		
		 {	
		    //---------------------------------------------	
				if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			    $partidas[$pos]=array(
			            'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						//'monto_inicial'=>$suma2_d,
						'monto_inicial'=>$suma2,
						'monto_comprometido'=>$suma2_c,
						//'monto_disponible'=>$suma2_d-$suma2_c,
						'monto_disponible'=>$suma2-$suma2_c,
						'monto'=>0,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }
				
		  //-----------------------------------------------
			
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				   $partidas[$pos]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						//'monto_inicial'=>$suma_d,
						'monto_inicial'=>$suma,
						'monto_comprometido'=>$suma_c,
						//'monto_disponible'=>$suma_d-$suma_c,
						'monto_disponible'=>$suma-$suma_c,
						'monto'=>0,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }	 
				  
				  
				  if (trim($codigo3)==trim($partidaTitulo[$j]['cod_partida'])){
				
				   $partidas[$pos]=array(
                        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						//'monto_inicial'=>$suma3_d,
						'monto_inicial'=>$suma3,
						'monto_comprometido'=>$suma3_c,
						//'monto_disponible'=>$suma3_d-$suma3_c,
						'monto_disponible'=>$suma3-$suma3_c,
						'monto'=>0,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }
				  
		 
		 }
		 else//IF EXISTE
		 {	
			//---------------------------------------------	
				if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			   $partidas[$cuenta]=array(
			            'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						//'monto_inicial'=>$suma2_d,
						'monto_inicial'=>$suma2,
						'monto_comprometido'=>$suma2_c,
						//'monto_disponible'=>$suma2_d-$suma2_c,
						'monto_disponible'=>$suma2-$suma2_c,
						'monto'=>0,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }
				
		  //-----------------------------------------------
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$cuenta]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						//'monto_inicial'=>$suma_d,
						'monto_inicial'=>$suma,
						'monto_comprometido'=>$suma_c,
						//'monto_disponible'=>$suma_d-$suma_c,
						'monto_disponible'=>$suma-$suma_c,
						'monto'=>0,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }	 
				  
				  
				  if (trim($codigo3)==trim($partidaTitulo[$j]['cod_partida'])){
				
				   $partidas[$cuenta]=array(
                        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						//'monto_inicial'=>$suma3_d,
						'monto_inicial'=>$suma3,
						'monto_comprometido'=>$suma3_c,
						//'monto_disponible'=>$suma3_d-$suma3_c,
						'monto_disponible'=>$suma3-$suma3_c,
						'monto'=>0,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }
				  
			   }//FIN IF EXISTE 
			 }//FIN FOR TITULO
			        $cuenta=$cuenta+1;
                    $partidas[$cuenta]=array(
					    'pk'=>$partidaDetalle[$i]['pk_num_presupuesto_det'],
                        'id'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'],
                        'cod'=>$partidaDetalle[$i]['cod_partida'],
                        'partida'=>$partidaDetalle[$i]['ind_denominacion'],
						//'monto_inicial'=>$partidaDetalle[$i]['num_monto_disponible'],
						'monto_inicial'=>$partidaDetalle[$i]['num_monto_ajustado'],
						'monto'=>0,
						'monto_comprometido'=>$partidaDetalle[$i]['num_monto_compromiso'],
						//'monto_disponible'=>$partidaDetalle[$i]['num_monto_disponible']-$partidaDetalle[$i]['num_monto_compromiso'],
						'monto_disponible'=>$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso'],
						'tipo'=>$partidaDetalle[$i]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
                    );
					
                }//FIN  IF AESPECIFICA   
			   
             }//FIN FOR DETALLE
			    $cuenta=$cuenta+1;
		    }//FIN FOR AESPECIFICA	
		    	
		 }	//FIN PRESUPUESTO POR PROYECTO 
		 else //ELSE TIPO PRESUPUESTO
		 {
			 
			  $partida1='';
			  $partidat='';
			  $partidat2='';
			 
			  $suma=0;
			  $suma2=0;
			  $suma_total=0;
			 
			  $suma_d=0;
			  $suma2_d=0;	
			  
			  $suma_c=0;
			  $suma2_c=0;
			  $suma3_c=0;
			  $suma_total_c=0;		
			  
			  $suma_precompromiso=0;
			  $suma_precompromiso_t=0;	 
			  $suma_total_p=0;
			  
			  $suma_disponible=0;
			  $suma_disponible_t=0;	 
			  $suma_total_disponible=0;
			  
			  $f=0;
			  $p=0;
			  $c=0;
			 
			  
		for($i=0;$i<count($partidaDetalle);$i++){	
		
		$pre_compromiso=0;
		
		  //Comparaci�n partidas .00	
		    if (trim($partidat2)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2))){
		      $suma3_d=0;
		      $suma3=0;
			  $suma_precompromiso_d=0;	
			  $c=0;
		   }   	
		
		
	        if($c==0){
		      $partidat2= substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
			  $c=1;
			}  
		
		
	       //Comparaci�n Partidas .00.00	
		   if (trim($partida1)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5))){
		      $suma_d=0;
		      $suma=0;
			  $suma_c=0;
			  $suma_precompromiso=0;
			  $f=0;
		   }
		
		   if($f==0){
		      $partida1= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
			  $f=1;
			}  
		
		  
		    //Comparaci�n Partidas .00.00.00
		    if (trim($partidat)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8))){
		      $suma2_d=0;
		      $suma2=0;
			  $suma2_c=0;
			  $suma_precompromiso_t=0;
			  $p=0;
		   }
		   
		    if($p==0){
		      $partidat= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
			  $p=1;
			}  
		    
		/*	 $pre_compromiso = $this->atPresupuesto->metMostrarPresupuestoDetallepc($partidaDetalle[0]['pk_num_presupuesto'], $partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'] );*/
		   
		     $suma_d=$suma_d+$partidaDetalle[$i]['num_monto_disponible'];   
		     $suma=$suma+$partidaDetalle[$i]['num_monto_ajustado'];      
			 
			 
			 $suma2_d=$suma2_d+$partidaDetalle[$i]['num_monto_disponible'];
			 $suma2=$suma2+$partidaDetalle[$i]['num_monto_ajustado']; 
			 
			 
			 $suma_total_d=$suma_total_d+$partidaDetalle[$i]['num_monto_disponible'];  
			 $suma_total=$suma_total+$partidaDetalle[$i]['num_monto_ajustado'];
			 
			 
			  //-----------------MONTO COMPROMISO------------------//
			 $suma_c=$suma_c+$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma2_c=$suma2_c+$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma3_c=$suma3_c+$partidaDetalle[$i]['num_monto_compromiso']; 
			 
		     $suma_total_c=$suma_total_c+$partidaDetalle[$i]['num_monto_compromiso'];
			 
			 
			 //--------------------MONTO PRE-COMPROMISO--------------------
			 
			 $suma_precompromiso_t=$suma_precompromiso_t+$pre_compromiso;
			 $suma_precompromiso=$suma_precompromiso+$pre_compromiso;
			 $suma_precompromiso_d=$suma_precompromiso_d+$pre_compromiso;
			 $suma_total_p=$suma_total_p+$pre_compromiso;		
			 
			 //------------------------------------------------------
		   
		     for($j=0;$j<count($partidaTitulo);$j++){	
			   
			 
			    $codigo=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
				$codigo3=substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
				
			//---------------------------------------------	
				if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			   $partidas[$partidaTitulo[$j]['pk_num_partida_presupuestaria']]=array(
			            'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						//'monto_inicial'=>$suma2_d,
						'monto_inicial'=>$suma2,
						'monto_comprometido'=>$suma2_c,
						//'monto_disponible'=>$suma2_d-$suma2_c,
						'monto_disponible'=>$suma2-$suma2_c,
						'monto'=>0,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
					
                    );
				   
				  }
				
				
		  //-----------------------------------------------
			
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$partidaTitulo[$j]['pk_num_partida_presupuestaria']]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						//'monto_inicial'=>$suma_d,
						'monto_inicial'=>$suma,
						'monto_comprometido'=>$suma_c,
						//'monto_disponible'=>$suma_d-$suma_c,
						'monto_disponible'=>$suma-$suma_c,
						'monto'=>0,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
                    );
				   
				  }	 
				  
				  
				  if (trim($codigo3)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$partidaTitulo[$j]['pk_num_partida_presupuestaria']]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						//'monto_inicial'=> $suma3_d,
						'monto_inicial'=> $suma3,
						'monto_comprometido'=>$suma3_c,
						'monto_disponible'=>$suma3-$suma3_c,
						'monto'=>0,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
                    );
				   
				  }	 
			    
			 }
			        
                    $partidas[$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria']]=array(
					    'pk'=>$partidaDetalle[$i]['pk_num_presupuesto_det'],
                        'id'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'],
                        'cod'=>$partidaDetalle[$i]['cod_partida'],
                        'partida'=>$partidaDetalle[$i]['ind_denominacion'],
						//'monto_inicial'=>$partidaDetalle[$i]['num_monto_disponible'],
						'monto_inicial'=>$partidaDetalle[$i]['num_monto_ajustado'],
						'monto'=>0,
						'monto_comprometido'=>$partidaDetalle[$i]['num_monto_compromiso'],
						//'monto_disponible'=>$partidaDetalle[$i]['num_monto_disponible']-$partidaDetalle[$i]['num_monto_compromiso'],
						'monto_disponible'=>$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso'],
						'tipo'=>$partidaDetalle[$i]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
                    );
					
               
            }
		}//FIN TIPO DE PRESUPUESTO	
	
		
		 if(isset($partidas)){
                $this->atVista->assign('partidas', $partidas);
            }
			//$this->atVista->assign('suma_total',$suma_total_d);
			$this->atVista->assign('suma_total',$suma_total);
			$this->atVista->assign('suma_total_comp',$suma_total_c);
			//$this->atVista->assign('suma_total_disponible',$suma_total_d-$suma_total_c);
			$this->atVista->assign('suma_total_disponible',$suma_total-$suma_total_c);
			
			$this->atVista->assign('formDB',$this->atAjuste->metSelectPresupuesto());
			$this->atVista->assign('codPresupuesto',$codPresupuesto);
			$this->atVista->assign('tipoPresupuesto',$tipoPresupuesto);
				
		//}//fin Inicial
		//-----------------------------------------------------------------------
		//-----------------------------------------------------------------------
		
		$this->atVista->assign('presupuesto',$this->atReformulacion->metPresupuestoListar());
		//$this->atVista->metRenderizar('ListadoPartidas');
		
		
	}else{ //else inicial	
		
		
		//AGREGADO PARA MOSTRAR LAS PARTIDAS--------------------
			 $partidaDetalle = $this->atAjuste->metMostrarAjusteDetalle($idAjuste,$status);
			 
			 //var_dump($partidaDetalle);
			 
			 //$codPresupuesto=   $partidaDetalle[0]['ind_cod_presupuesto'];
			 
			 $partidaTitulo = $this->atPresupuesto->metMostrarPresupuestoTitulo();
			  
	   
	   if($partidaDetalle)		 
         if($partidaDetalle[0]['ind_tipo_presupuesto']=='P')
         {
		      $aespecifica = $this->atPartida->metAespecifica();
			  
			  $partida1='';
			  $partidat='';
			  $partidat2='';
			 
			  $suma=0;
			  $suma2=0;
			  $suma3=0;
			  
			  $suma3_d=0;
			  $suma_total=0;
			 
			  $suma_d=0;
			  $suma2_d=0;			 
			  
			  $suma_c=0;
			  $suma2_c=0;	
			  
			  $f=0;
			  $p=0;
			  $c=0;
			  $cuenta=0;
			  
			  $suma_total_p=0; 
			  $suma_total_c=0;
			  
			  $pre_compromiso=0;
			  
			  
	     for($a=0;$a<count($aespecifica);$a++){
	    
				$suma=0;
				$suma2=0;
				$suma3=0;
				
				$suma_c=0;
				$suma2_c=0;
				$suma3_c=0;
				
				$suma_d=0;
				$suma2_d=0;
				$suma3_d=0;
				
				$suma_precompromiso=0;
			    $suma_precompromiso_t=0;	 
			    $suma_precompromiso_d=0;
				
				
				$existeAespecifica = count($this->atAjuste->metExisteAespecificaEs($aespecifica[$a]['pk_num_aespecifica'], $partidaDetalle[0]['pk_num_presupuesto']));
					 
				if ($existeAespecifica>0)
				{
					$partidas[$cuenta]=array(
					        'pk'=>0,
							'id'=>$aespecifica[$a]['pk_num_aespecifica'],
							'cod'=>$aespecifica[$a]['ind_cod_aespecifica'],
							'partida'=>$aespecifica[$a]['ind_descripcion'],
							'monto'=>0,
							'tipo'=>'E',
							'aespecifica'=>''
						);
						
				 }		  
				 
			  
		for($i=0;$i<count($partidaDetalle);$i++){	
		
		    //Comparaci�n partidas .00	
		    if (trim($partidat2)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2))){
		      $suma3_d=0;
		      $suma3=0;
			  $suma_precompromiso_d=0;
			  $c=0;
		   }   	
		
		
	        if($c==0){
		      $partidat2= substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
			  $c=1;
			}  
		
		   
	       //Comparaci�n partidas .00.00	
		   if (trim($partida1)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5))){
		      $suma_d=0;
		      $suma=0;
			  $suma_precompromiso=0;
			  $f=0;
		   }

		
		   if($f==0){
		      $partida1= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
			  $f=1;
			}  
		
	
		    //Comparaci�n partidas .00.00.00	
		    if (trim($partidat)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8))){
		      $suma2_d=0;
		      $suma2=0;
			  $suma_precompromiso_t=0;
			  $p=0;
		   }
		   
		    if($p==0){
		      $partidat= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
			  $p=1;
			}  
		
		  if ($aespecifica[$a]['pk_num_aespecifica']==$partidaDetalle[$i]['fk_pr006_num_aespecifica'])
		  {
		    
			$pre_compromiso = $this->atPresupuesto->metMostrarPresupuestoDetallepc($partidaDetalle[0]['pk_num_presupuesto'], $partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'], $partidaDetalle[$i]['fk_pr006_num_aespecifica'] );
			
		     $suma_d=$suma_d+$partidaDetalle[$i]['ajustado']; 
		     $suma=$suma+$partidaDetalle[$i]['num_monto_ajustado']; 
			 
			 $suma2_d=$suma2_d+$partidaDetalle[$i]['ajustado']; 
			 $suma2=$suma2+$partidaDetalle[$i]['num_monto_ajustado']; 
			 
			 $suma3_d=$suma3_d+$partidaDetalle[$i]['ajustado']; 
			 $suma3=$suma3+$partidaDetalle[$i]['num_monto_ajustado']; 
			 
			 
			 $suma_total_d=$suma_total_d+$partidaDetalle[$i]['ajustado'];
			 $suma_total=$suma_total+$partidaDetalle[$i]['num_monto_ajustado'];
			 
			 
			 //-----------------MONTO COMPROMISO------------------//
			 $suma_c=$suma_c+$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma2_c=$suma2_c+$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma3_c=$suma3_c+$partidaDetalle[$i]['num_monto_compromiso']; 
		     $suma_total_c=$suma_total_c+$partidaDetalle[$i]['num_monto_compromiso'];
			 
			 
			 //------------------MONTO PRE-COMPROMISO--------------//
			 
			 $suma_precompromiso_t=$suma_precompromiso_t+$pre_compromiso;
			 $suma_precompromiso=$suma_precompromiso+$pre_compromiso;
			 $suma_precompromiso_d=$suma_precompromiso_d+$pre_compromiso;
			 $suma_total_p=$suma_total_p+$pre_compromiso;		
			 
			 //------------------------------------------------------
			 
		     for($j=0;$j<count($partidaTitulo);$j++){	
			   
			    $cuenta=$cuenta+1;
			 
			    $codigo=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
				$codigo3=substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
				
				$existe=0;
				$posnum = 0;
				$pos=0; 
				 reset($partidas);
				 foreach($partidas as  $key=>$item)
                  {
				        if (isset($item['cod']))
						{
						   if(trim($partidaTitulo[$j]['cod_partida'])==trim($item['cod']) && $item['aespecifica']==$aespecifica[$a]['pk_num_aespecifica'] && $item['tipo']=='T')
						   {
						       $pos= $key;
							   //echo ($item['cod']."-".$item['aespecifica']."-".$pos);
						       $existe=1; 
							   break 1;
						   } 
						  
						}
				      $posnum++;      
                  } 
				  
		 if ($existe==1)		
		 {	
		    //---------------------------------------------	
				if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			    $partidas[$pos]=array(
			            'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma2_d,
						'monto_comprometido'=>$suma2_c,
						'monto_disponible'=>$suma2_d-$suma2_c,
						'monto'=>$suma2,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }
				
		  //-----------------------------------------------
			
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				   $partidas[$pos]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma_d,
						'monto_comprometido'=>$suma_c,
						'monto_disponible'=>$suma_d-$suma_c,
						'monto'=>$suma,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }	 
				  
				  
				  if (trim($codigo3)==trim($partidaTitulo[$j]['cod_partida'])){
				
				   $partidas[$pos]=array(
                        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma3_d,
						'monto_comprometido'=>$suma3_c,
						'monto_disponible'=>$suma3_d-$suma3_c,
						'monto'=>$suma3,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }	  
		 
		 }
		 else//IF EXISTE
		 {	
			//---------------------------------------------	
				if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			   $partidas[$cuenta]=array(
			            'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma2_d,
						'monto_comprometido'=>$suma2_c,
						'monto_disponible'=>$suma2_d-$suma2_c,
						'monto'=>$suma2,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }
				
		  //-----------------------------------------------
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$cuenta]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma_d,
						'monto_comprometido'=>$suma_c,
						'monto_disponible'=>$suma_d-$suma_c,
						'monto'=>$suma,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }	 
				  
				  
				  if (trim($codigo3)==trim($partidaTitulo[$j]['cod_partida'])){
				
				   $partidas[$cuenta]=array(
                        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma3_d,
						'monto_comprometido'=>$suma3_c,
						'monto_disponible'=>$suma3_d-$suma3_c,
						'monto'=>$suma3,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }
				  
			   }//FIN IF EXISTE 
			 }//FIN FOR TITULO
			        $cuenta=$cuenta+1;
                    $partidas[$cuenta]=array(
					    'pk'=>$partidaDetalle[$i]['pk_num_presupuesto_det'],
                        'id'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'],
                        'cod'=>$partidaDetalle[$i]['cod_partida'],
                        'partida'=>$partidaDetalle[$i]['ind_denominacion'],
						//'monto_inicial'=>$partidaDetalle[$i]['num_monto_disponible'],
						'monto_inicial'=>$partidaDetalle[$i]['ajustado'],
						'monto'=>$partidaDetalle[$i]['num_monto_ajustado'],
						'monto_comprometido'=>$partidaDetalle[$i]['num_monto_compromiso'],
						'monto_disponible'=>$partidaDetalle[$i]['num_monto_disponible']-$partidaDetalle[$i]['num_monto_compromiso'],
						'tipo'=>$partidaDetalle[$i]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
                    );
					
                }//FIN  IF AESPECIFICA   
			   
             }//FIN FOR DETALLE
			    $cuenta=$cuenta+1;
		    }//FIN FOR AESPECIFICA	
		    	
		 }	//FIN PRESUPUESTO POR PROYECTO 
		 else //ELSE TIPO PRESUPUESTO
		 {
			 
			  $partida1='';
			  $partidat='';
		      $partidat2='';
			 
			  $suma=0;
			  $suma2=0;
			  $suma3=0;
			  $sumac=0;
			  $suma_total=0;
			  $suma_total_d=0;
			  $suma_total_c=0;
			  
			 
			  $suma_d=0;
			  $suma2_d=0;
			  $suma3_d=0;
			  	
			  $suma_c=0;
			  $suma2_c=0;
			  $suma3_c=0;	
			  
			  $suma_precompromiso=0;
			  $suma_precompromiso_t=0;	 
			  $suma_precompromiso_d=0;	
			  $suma_total_p=0; 
			  $suma_total_c=0;
			  
			  $f=0;
			  $p=0;
			 
			  
		for($i=0;$i<count($partidaDetalle);$i++){	
		
		
		$pre_compromiso=0;
		
		   //Comparaci�n partidas .00	
		    if (trim($partidat2)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2))){
		      $suma3_d=0;
		      $suma3=0;
			  $suma_precompromiso_d=0;	
			  $c=0;
		   }   	
		
		
	        if($c==0){
		      $partidat2= substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
			  $c=1;
			}  
			
		
		   //Comparaci�n Partidas .00.00
		   if (trim($partida1)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5))){
		      $suma_d=0;
		      $suma=0;
			  $suma_precompromiso=0;	
			  $f=0;
		   }
		
		
		   if($f==0){
		      $partida1= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
			  $f=1;
			}  
		
		  
		  //Comparaci�n Partidas .00.00.00
		   if (trim($partidat)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8))){
		      $suma2_d=0;
		      $suma2=0;
			  $suma_precompromiso_t=0;	
			  $p=0;
		   }

		   
		    if($p==0){
		      $partidat= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
			  $p=1;
			}  
		    
			 $pre_compromiso = $this->atPresupuesto->metMostrarPresupuestoDetallepc($partidaDetalle[0]['pk_num_presupuesto'], $partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'] );
		   
		     $suma_d=$suma_d+$partidaDetalle[$i]['ajustado'];   
		     $suma=$suma+$partidaDetalle[$i]['num_monto_ajustado'];      
			 
			 
			 $suma2_d=$suma2_d+$partidaDetalle[$i]['ajustado'];
			 $suma2=$suma2+$partidaDetalle[$i]['num_monto_ajustado']; 
			 
			 
			 $suma_total_d=$suma_total_d+$partidaDetalle[$i]['ajustado'];  
			 $suma_total=$suma_total+$partidaDetalle[$i]['num_monto_ajustado'];
			 
			 
			  //-----------------MONTO COMPROMISO------------------//
			 $suma_c=$suma_c+$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma2_c=$suma2_c+$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma3_c=$suma3_c+$partidaDetalle[$i]['num_monto_compromiso']; 
			 
		     $suma_total_c=$suma_total_c+$partidaDetalle[$i]['num_monto_compromiso'];
			 
			 
			 //--------------------MONTO PRE-COMPROMISO--------------------
			 
			 $suma_precompromiso_t=$suma_precompromiso_t+$pre_compromiso;
			 $suma_precompromiso=$suma_precompromiso+$pre_compromiso;
			 $suma_precompromiso_d=$suma_precompromiso_d+$pre_compromiso;
			 $suma_total_p=$suma_total_p+$pre_compromiso;		
			 
			 //------------------------------------------------------
		   
		     for($j=0;$j<count($partidaTitulo);$j++){	
			   
			 
			    $codigo=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
				$codigo3=substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
				
			//---------------------------------------------	
				if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			   $partidas[$partidaTitulo[$j]['pk_num_partida_presupuestaria']]=array(
			            'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma2_d,
						'monto_comprometido'=>$suma2_c,
						'monto_disponible'=>$suma2_d-$suma2_c,
						'monto'=>$suma2,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
					
                    );
				   
				  }
				
				
		  //-----------------------------------------------
			
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$partidaTitulo[$j]['pk_num_partida_presupuestaria']]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma_d,
						'monto_comprometido'=>$suma_c,
						'monto_disponible'=>$suma_d-$suma_c,
						'monto'=>$suma,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
                    );
				   
				  }	 
				  
				  
				  if (trim($codigo3)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$partidaTitulo[$j]['pk_num_partida_presupuestaria']]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=> $suma3_d,
						'monto_comprometido'=>$suma3_c,
						'monto_disponible'=>$suma3_d-$suma3_c,
						'monto'=>$suma,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
                    );
				   
				  }	 
			    
			 }
			        
                    $partidas[$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria']]=array(
					    'pk'=>$partidaDetalle[$i]['pk_num_presupuesto_det'],
                        'id'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'],
                        'cod'=>$partidaDetalle[$i]['cod_partida'],
                        'partida'=>$partidaDetalle[$i]['ind_denominacion'],
						//'monto_inicial'=>$partidaDetalle[$i]['num_monto_disponible'],
						'monto_inicial'=>$partidaDetalle[$i]['ajustado'],
						'monto'=>$partidaDetalle[$i]['num_monto_ajustado'],
						'monto_comprometido'=>$partidaDetalle[$i]['num_monto_compromiso'],
						'monto_disponible'=>$partidaDetalle[$i]['ajustado']-$partidaDetalle[$i]['num_monto_compromiso'],
						'tipo'=>$partidaDetalle[$i]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr006_num_aespecifica']
                    );
					
               
            }
		}//FIN TIPO DE PRESUPUESTO	
			
			 if(isset($partidas)){
                $this->atVista->assign('partidas', $partidas);
            }
			
		/*$this->atVista->assign('suma_total',$suma_total);
		$this->atVista->assign('suma_total_comp',$suma_total_c);
		$this->atVista->assign('suma_total_disponible',$suma_total-$suma_total_c);
		$this->atVista->assign('suma_total_ajuste',0);*/
		
		$this->atVista->assign('suma_total',$suma_total_d);
		$this->atVista->assign('suma_total_comp',$suma_total_c);
		$this->atVista->assign('suma_total_disponible',$suma_total_d-$suma_total_c);
		$this->atVista->assign('suma_total_ajuste',$suma_total);
			
        }//FIN INICIAL	
		
		
        //$this->atVista->assign('tipoajuste',$this->atAjuste->metTipoAjusteListar());
		  $this->atVista->assign('formDB',$this->atAjuste->metMostrarAjuste($idAjuste));
		
		$this->atVista->assign('status',$status);
		$this->atVista->metRenderizar('ListadoPartidas');
		
		
	}
				
  
    /**
	*Elimina un ajuste presupuestario
	*
	*Parametro: 
	*Return: echo json_encode($valido)
	*
	**/
    public function metEliminar()
    {
        $idAjuste = $this->metObtenerInt('idAjuste');
		 
		
        if($idAjuste!=0){
            $id=$this->atAjuste->metEliminarAjuste($idAjuste);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el antepresupuesto se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idAjuste'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
	
	
	/**
	*Dar formato de fecha a�o-mes-dia
	*
	*Parametro: $fecha
	*Return: $resultado
	*
	**/
	protected function FormatoFecha($fecha)
    {
        $resultado=explode("-",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }	
	
	/**
	*Dar formato a los montos sin separador de miles y , como separador de decimales
	*
	*Parametro: $monto_f
	*Return: $monto_f
	*
	**/
	protected function FormatoMonto($monto_f)
    {
       if(!isset($monto_f)){
                $monto_f=0;
            }
			else {
			     $monto_f=str_replace(".", "", $monto_f);
			     $monto_f=str_replace(",", ".", $monto_f);
				 
			}
			 return $monto_f;
    }	
	
	
}