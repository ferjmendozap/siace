<?php


require_once 'datosCarga/dataPresupuesto.php';

class scriptCargaPresupuestoControlador extends Controlador
{
   
    use dataPresupuesto;
   
    private $atScriptCarga;

    public function __construct()
    {
        parent::__construct();
        $this->atScriptCarga = $this->metCargarModelo('scriptCargaPresupuesto');
    }


public function metIndex()
    {
        echo "INICIANDO<br>";
        
        ### PRESUPUESTO ###
        $this->metTipoCuenta();
        $this->metPartidas();
		$this->metAespecifica();
        
        

        echo "TERMINADO";
    }

    public function metPartidas()
    {
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE PARTIDAS<br>';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>PARTIDAS PRESUPUESTARIAS<br>';
        $this->atScriptCarga->metPresupuestoPartidasPresupuestarias($this->metDataPartidasPreupuestarias());
        echo '&nbsp;&nbsp;&nbsp;->>FIN CARGA DE PARTIDAS<br><br>';
    }


    public function metTipoCuenta()
    {
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA TIPO CUENTA<br>';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>TIPO CUENTA<br>';
        $this->atScriptCarga->metTipoCuenta($this->metDataTipoCuenta());
        echo '&nbsp;&nbsp;&nbsp;->>FIN CARGA DE TIPO CUENTA<br><br>';
    }

     public function metAespecifica()
    {
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA ACCION ESPECIFICA<br>';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>TIPO CUENTA<br>';
        $this->atScriptCarga->metAespecifica($this->metDataAespecifica());
        echo '&nbsp;&nbsp;&nbsp;->>FIN CARGA ACCION ESPECIFICA<br><br>';
    }

}
?>