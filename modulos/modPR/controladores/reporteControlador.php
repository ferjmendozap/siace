<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Presupuesto
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           Liduvica Bastardo            |          liduvica@hotmail.com       |         04249080200            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-12-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/



class reporteControlador extends Controlador
{
    private $atReporteModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atReporteModelo = $this->metCargarModelo('reporte');

    }


    #Metodo Index del controlador Prueba.
    public function metIndex()
    {
       $this->metObtenerLibreria('cabecera','modPR');
    }
	
	

    public function metJsonPresupuestoReporte()
    {

        $anio = $this->metObtenerInt('anio');
		
		$filtro  = $this->atReporteModelo->metListarPresupuestoAnio($anio);
        echo json_encode($filtro);
        exit;

    }
	
	
	public function metJsonAntePresupuestoReporte()
    {
        $anio = $this->metObtenerInt('anio');
		$filtro  = $this->atReporteModelo->metListarAntePresupuestoAnio($anio);
        echo json_encode($filtro);
        exit;
    }
    
	
	public function metJsonReformulacionReporte()
    {
        $presupuesto = $this->metObtenerInt('presupuesto');
		$filtro  = $this->atReporteModelo->metListarPresupuestoReformulacion($presupuesto);
		
        echo json_encode($filtro);
        exit;
    }
	
	
	public function metJsonAjusteReporte()
    {
        $presupuesto = $this->metObtenerInt('presupuesto');
		$filtro  = $this->atReporteModelo->metListarPresupuestoAjuste($presupuesto);
		
        echo json_encode($filtro);
        exit;
    }
	

    public function metpresupuestoReporte()
    {
        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents','ModPR/prFunciones',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );
		
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
	    $this->atVista->metCargarCssComplemento($complementosCss);

       
        $this->_IdPresupuesto = $this->metCargarModelo('presupuesto');
        $this->atVista->assign('_PresupuestoPost',$this->_IdPresupuesto-> metListarPresupuesto());
		
		$this->atAntepresupuesto=$this->metCargarModelo('antepresupuesto');
		$this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());


        $this->atVista->metRenderizar('ListadoPresupuesto' );

    }
	
	
	public function metpresupuestoantepresupuestoReporte()
    {
        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents','ModPR/prFunciones',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );
		
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
	    $this->atVista->metCargarCssComplemento($complementosCss);

       	$this->atAntepresupuesto=$this->metCargarModelo('antepresupuesto');
		$this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());

	   
        $this->_IdPresupuesto = $this->metCargarModelo('presupuesto');
        $this->atVista->assign('_PresupuestoPost',$this->_IdPresupuesto-> metListarPresupuesto());


        $this->atVista->metRenderizar('ListadoPresupuestoAntepresupuesto');

    }
	
	
	public function metAntepresupuestoReporte()
    {
        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents','ModPR/prFunciones'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );
		
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
	    $this->atVista->metCargarCssComplemento($complementosCss);

        $this->_IdPresupuesto = $this->metCargarModelo('antepresupuesto');
        $this->atVista->assign('_PresupuestoPost',$this->_IdPresupuesto-> metListarAntepresupuesto());
		
		$this->atAntepresupuesto=$this->metCargarModelo('antepresupuesto');
		$this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());

        $this->atVista->metRenderizar('ListadoAntePresupuesto' );
    }

    
	public function metAntepresupuestoAprobadoReporte()
    {
	   

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );
		
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
	    $this->atVista->metCargarCssComplemento($complementosCss);

        $this->_IdPresupuesto = $this->metCargarModelo('antepresupuesto');
        $this->atVista->assign('_PresupuestoPost',$this->_IdPresupuesto-> metListarAntepresupuesto());


        $this->atVista->metRenderizar('ListadoAntePresupuestoAprobado' );


    }


    public function metreformulacionReporte()
    {
        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents','ModPR/prFunciones',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );
		
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
	    $this->atVista->metCargarCssComplemento($complementosCss);

       
        $this->_IdPresupuesto = $this->metCargarModelo('presupuesto');
        $this->atVista->assign('_PresupuestoPost',$this->_IdPresupuesto-> metListarPresupuesto());
		
		$this->_IdReformulacion = $this->metCargarModelo('reformulacion');
        $this->atVista->assign('_ReformulacionPost',$this->_IdReformulacion-> metListarReformulacion());
		
		$this->atAntepresupuesto=$this->metCargarModelo('antepresupuesto');
		$this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());


        $this->atVista->metRenderizar('ListadoReformulacion' );

    }
   

	public function metajusteReporte()
    {
	   

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents','ModPR/prFunciones',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );
		
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
	    $this->atVista->metCargarCssComplemento($complementosCss);
		
		$this->atAntepresupuesto=$this->metCargarModelo('antepresupuesto');
		$this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());

        $this->_IdPresupuesto = $this->metCargarModelo('presupuesto');
        $this->atVista->assign('_PresupuestoPost',$this->_IdPresupuesto-> metListarPresupuesto());
		
		$this->_IdAjuste = $this->metCargarModelo('ajuste');
        $this->atVista->assign('_AjustePost',$this->_IdAjuste-> metListarAjuste());
		$this->atVista->assign('_TipoAjustePost',$this->_IdAjuste-> metTipoAjusteListar());


        $this->atVista->metRenderizar('ListadoAjuste' );


    }
	
	//-------------------------------------------------------------------------------
	
	
	public function metcierremesReporte()
    {
        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents','ModPR/prFunciones',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );
		
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
	    $this->atVista->metCargarCssComplemento($complementosCss);

        $this->atAntepresupuesto=$this->metCargarModelo('antepresupuesto');
		$this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());

        $this->_IdPresupuesto = $this->metCargarModelo('presupuesto');
        $this->atVista->assign('_PresupuestoPost',$this->_IdPresupuesto-> metListarPresupuesto());


        $this->atVista->metRenderizar('ListadoCierreMes' );


    }
	
	//--------------------------------------------------------------------------------

	
	public function metejecucionReporte()
    {
	   
        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents','ModPR/prFunciones'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );
		
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
	    $this->atVista->metCargarCssComplemento($complementosCss);


        $this->_IdPresupuesto = $this->metCargarModelo('presupuesto');
        $this->atVista->assign('_PresupuestoPost',$this->_IdPresupuesto-> metListarPresupuesto());
		
		$this->_IdCierre = $this->metCargarModelo('cierremes');
        $this->atVista->assign('_CierrePost',$this->_IdCierre-> metListarCierreMes());
		
		$this->atAntepresupuesto=$this->metCargarModelo('antepresupuesto');
		$this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());


        $this->atVista->metRenderizar('EjecucionReporte' );
    }
	
	
	public function metejecucionDetalladaReporte()
    {
	   
        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents','ModPR/prFunciones'
        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );
		
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
	    $this->atVista->metCargarCssComplemento($complementosCss);

        $this->atAntepresupuesto=$this->metCargarModelo('antepresupuesto');
		$this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());

        $this->_IdPresupuesto = $this->metCargarModelo('presupuesto');
        $this->atVista->assign('_PresupuestoPost',$this->_IdPresupuesto-> metListarPresupuesto());
		


        $this->atVista->metRenderizar('EjecucionDetalladaReporte' );
    }
	
	//--------------------------------------------------------------------------------


    public function metpartidasReporte()
    {
	   

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );
		
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
	    $this->atVista->metCargarCssComplemento($complementosCss);
     

        $this->atVista->metRenderizar('PartidaReporte' );
    }
	

	//--------------------------------------------------------------------------------


	
	public function metejecucionxpartidaReporte()
    {
        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents','ModPR/prFunciones',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );
		
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
	    $this->atVista->metCargarCssComplemento($complementosCss);
		
		$this->atAntepresupuesto=$this->metCargarModelo('antepresupuesto');
		$this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());

        $this->_IdPresupuesto = $this->metCargarModelo('presupuesto');
        $this->atVista->assign('_PresupuestoPost',$this->_IdPresupuesto-> metListarPresupuesto());

        $this->atVista->metRenderizar('EjecucionxPartidaReporte' );
    }
	

	public function metcompromisoReporte()
    {
        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents','ModPR/prFunciones',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );
		
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
	    $this->atVista->metCargarCssComplemento($complementosCss);

        $this->atAntepresupuesto=$this->metCargarModelo('antepresupuesto');
		$this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());

        $this->_IdPresupuesto = $this->metCargarModelo('presupuesto');
        $this->atVista->assign('_PresupuestoPost',$this->_IdPresupuesto-> metListarPresupuesto());
		
		$this->_IdCierre = $this->metCargarModelo('cierremes');
        $this->atVista->assign('_CierrePost',$this->_IdCierre-> metListarCierreMes());


        $this->atVista->metRenderizar('CompromisoReporte' );
    }
	
	
	
	public function metincrementoReporte()
    {
	   

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents','ModPR/prFunciones',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );
		
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
	    $this->atVista->metCargarCssComplemento($complementosCss);

        $this->atAntepresupuesto=$this->metCargarModelo('antepresupuesto');
		$this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());

        $this->_IdPresupuesto = $this->metCargarModelo('presupuesto');
        $this->atVista->assign('_PresupuestoPost',$this->_IdPresupuesto-> metListarPresupuesto());
		
        $this->atVista->metRenderizar('IncrementoReporte' );
    }
	
	
	
	
     public function metdisminucionReporte()
    {
	   

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents','ModPR/prFunciones',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );
		
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
	    $this->atVista->metCargarCssComplemento($complementosCss);
       
        $this->atAntepresupuesto=$this->metCargarModelo('antepresupuesto');
		$this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());

        $this->_IdPresupuesto = $this->metCargarModelo('presupuesto');
        $this->atVista->assign('_PresupuestoPost',$this->_IdPresupuesto-> metListarPresupuesto());
		

        $this->atVista->metRenderizar('DisminucionReporte' );
    }
	


    public function metcausadoReporte()
    {
	   

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents','ModPR/prFunciones',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );
		
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
	    $this->atVista->metCargarCssComplemento($complementosCss);
		
		$this->atAntepresupuesto=$this->metCargarModelo('antepresupuesto');
		$this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());
		
		$this->_IdPresupuesto = $this->metCargarModelo('presupuesto');
        $this->atVista->assign('_PresupuestoPost',$this->_IdPresupuesto-> metListarPresupuesto());

       
        $this->atVista->metRenderizar('CausadoReporte' );
    }
	

     public function metpagadoReporte()
    {
	   

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents','ModPR/prFunciones',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );
		
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
	    $this->atVista->metCargarCssComplemento($complementosCss);
		
		$this->atAntepresupuesto=$this->metCargarModelo('antepresupuesto');
		$this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());

        $this->_IdPresupuesto = $this->metCargarModelo('presupuesto');
        $this->atVista->assign('_PresupuestoPost',$this->_IdPresupuesto-> metListarPresupuesto());
		
        $this->atVista->metRenderizar('PagadoReporte' );
    }
	

    
	public function metproyeccionReporte()
    {
	   

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents','ModPR/prFunciones',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );
		
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
	    $this->atVista->metCargarCssComplemento($complementosCss);

		$this->atAntepresupuesto=$this->metCargarModelo('antepresupuesto');
		$this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());

        $this->_IdPresupuesto = $this->metCargarModelo('presupuesto');
        $this->atVista->assign('_PresupuestoPost',$this->_IdPresupuesto-> metListarPresupuesto());


        $this->atVista->metRenderizar('ListadoProyeccion' );
    }

	
	
	public function metejecucionespecificaReporte()
    {
	   

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents','ModPR/prFunciones',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );
		
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );


        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
	    $this->atVista->metCargarCssComplemento($complementosCss);

        $this->atAntepresupuesto=$this->metCargarModelo('antepresupuesto');
		$this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());

        $this->_IdPresupuesto = $this->metCargarModelo('presupuesto');
        $this->atVista->assign('_PresupuestoPost',$this->_IdPresupuesto-> metListarPresupuesto());
		

        $this->atVista->metRenderizar('EjecucionEspecificaReporte' );
    }



    public function metpartidasReportepdf()
    {		
       
        $formTxt=$this->metObtenerTexto('form','txt');
		
		  if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
		$this->_ReporteModelo = $this->metCargarModelo('reporte');
		
		$organismo= $this->_ReporteModelo-> metListarOrganismoInt(1);
   
        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabecera','modPR');
        $pdf=new pdfReporte('L','mm','Letter');
		$pdf->organismo = $organismo[0]['ind_descripcion_empresa'];
		$pdf->logo = $organismo[0]['ind_logo'];
		$pdf->logo_secundario = $organismo[0]['ind_logo_secundario'];
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(10, 5);
        $pdf->Cell(10, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, utf8_decode('COD. PARTIDA'), 1, 0, 'C', 1);
        $pdf->Cell(200, 5, utf8_decode('DESCRIPCION'), 1, 0, 'C', 1);
        $pdf->Cell(20, 5, utf8_decode('ESTADO'), 1, 0, 'C', 1);
       
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', '', 6);

        //$this->_PartidaModelo = $this->metCargarModelo('partida','maestros');
       
        $consulta=$this->_ReporteModelo-> metListarPartida($validacion['cod_partida']);
       
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
	    $pdf->SetFont('Arial', '', 8);
	    $pdf->SetWidths(array(10, 25, 200,20));
	    $pdf->SetAligns(array('C','C','L','C'));
		$pdf->Ln(5);
        for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
            {
			//$pdf->Ln(4);
			$pdf->Cell(10, 5);
           
			
			$pdf->Row(array(utf8_decode(($i+1)),utf8_decode($consulta[$i]['cod_partida']),utf8_decode($consulta[$i]['ind_denominacion']),utf8_decode($consulta[$i]['num_estatus'])));
			
        }
        $pdf->Output();
    }

	 

    public function metpresupuestoantepresupuestoReportePdf()
    {
      
        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formTxt=$this->metObtenerTexto('form','txt');
		
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
		
		
		$this->_ReporteModelo = $this->metCargarModelo('reporte');
		
		$organismo= $this->_ReporteModelo-> metListarOrganismoInt($validacion['fk_a001_num_organismo']);
		
        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabecera','modPR');
        $pdf=new pdfReporte('L','mm','Letter');
		$pdf->organismo = $organismo[0]['ind_descripcion_empresa'];
		$pdf->logo = $organismo[0]['ind_logo'];
		$pdf->logo_secundario = $organismo[0]['ind_logo_secundario'];
        $pdf->AliasNbPages();
        $pdf->AddPage();
				
		$pdf->Cell(250, 5, utf8_decode('REPORTE COMPARITIVO ANTEPRESUPUESTO-PRESUPUESTO'),0,1,'C');
		
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(10, 5);
        //$pdf->Cell(5, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, utf8_decode('COD. PARTIDA'), 1, 0, 'C', 1);
        $pdf->Cell(150, 5, 'DESCRIPCION', 1, 0, 'C', 1);
		$pdf->Cell(40, 5, 'MONTO PRESUPUESTADO', 1, 0, 'C', 1);
        $pdf->Cell(40, 5, 'MONTO APROBADO', 1, 0, 'C', 1);
       
        $pdf->SetFont('Arial', '', 7);
		
        $consulta= $this->_ReporteModelo-> getReportePresupuestoAntepresupuesto($validacion['idpresupuesto'],$validacion['fec_anio'],$validacion['fk_a001_num_organismo']);
		
		$partidas_titulo= $this->_ReporteModelo-> getReportePartidasTitulo();
		$aespecifica = $this->_ReporteModelo->metAespecifica();

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(4);
		
		$pdf->SetWidths(array(25,150,40,40));
        $pdf->SetAligns(array('C','L','R','R'));
 	  
	     $total=0;
   
   if (count($consulta)>0 && ($validacion['idpresupuesto']!="error" || $validacion['fec_anio']!="error"))
   
	 if($consulta[0]['ind_tipo_presupuesto']=='P')
     {
	    for($a=0;$a<count($aespecifica);$a++)
	    {
		   $cont=0;
		   $existeAespecifica = count($this->_ReporteModelo->metExisteAespecificaEs2($aespecifica[$a]['pk_num_aespecifica'], $validacion['idpresupuesto']));
		   
		   $pdf->SetFont('Arial', 'B', 8);
		   $pdf->SetFillColor(202, 202, 202);
		   
		   $pdf->SetWidths(array(25,210));
           $pdf->SetAligns(array('C','L'));
		   
		   
		   if ($existeAespecifica>0)
		   {
		      $pdf->Cell(10,5); 
		      $pdf->Row(array(
                    utf8_decode($aespecifica[$a]['ind_cod_aespecifica']),
                    utf8_decode($aespecifica[$a]['ind_descripcion'])
                    ));		
		   }
		   
		   
        $cont=0;	
        for($i=0; $i< count($consulta); $i++)
        {
		    //-------------------------
			
			
			 /*TITULOS DE PARTIDAS*/
		    $pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(235, 235, 235);
			$pdf->SetWidths(array(25,150,40,40));
            $pdf->SetAligns(array('C','L','R','R'));
		
		 if ($aespecifica[$a]['pk_num_aespecifica']==$consulta[$i]['fk_pr006_num_aespecifica'])
		 {
		
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				$codigo3=substr_replace($consulta[$i]['cod_partida'],'00', 10,2);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTituloPresupuesto($validacion['idpresupuesto'],$partida,$aespecifica[$a]['pk_num_aespecifica']);
					
				     $pdf->Cell(10,5); 
				     $pdf->Row(array(
                     utf8_decode($partidas_titulo[$j]['cod_partida']),
                     utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                     number_format($suma['suma_presupuestado'],2,',','.')));
				
				    $cont=$j+1;  
				}//FIN IF
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTituloPresupuesto($validacion['idpresupuesto'],$partida,$aespecifica[$a]['pk_num_aespecifica']);
					
                    $pdf->Cell(10,5); 
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma['suma_presupuestado'],2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
		//-----------------------------------------------		
				if (trim($codigo3)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 9);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTituloPresupuesto($validacion['idpresupuesto'],$partida,'error');
					
                    $pdf->Cell(10,5); 
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma['suma_presupuestado'],2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				//------------------------------
			
			}//fin for
			/*FIN TITULOS DE PARTIDAS*/
			
			
			//---------------------------
		    
			 $pdf->SetFont('Arial', 'B', 7);
		     $pdf->SetFillColor(255, 255, 255);
			 
            $pdf->Cell(10,5); //
			$total = $total+$consulta[$i]['num_monto_aprobado'];
            $monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');
			
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto));
         }
		
		}//FIN IF ACCION ESPECIFICA
	
	  }//FIN AESPECIFICA 	
		
	 }//Fin Presupuesto por proyecto	
	 else
	 {
	  
	    $suma_presupuestado=0;
		$suma_aprobado=0;
		$total_aprobado=0;
	    $cont=0;	
        for($i=0; $i< count($consulta); $i++)
        {
		
			/*TITULOS DE PARTIDAS*/
		     $pdf->SetFont('Arial', 'B', 7);
		     $pdf->SetFillColor(202, 202, 202);
		
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);	
				$codigo3=substr_replace($consulta[$i]['cod_partida'],'00', 10,2);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					$suma=$this->_ReporteModelo->getReporteMontosTituloPresupuestoAntepresupuesto($validacion['idpresupuesto'],$partida);
					
				     $pdf->Cell(10,5); 
					 
					$suma_presupuestado=number_format($suma[0]['suma'],2,',','.');
					//$suma_aprobado=number_format($suma[1]['suma'],2,',','.');
					
					if(!isset($suma[1]['suma'])){
                         $suma_aprobado=$suma_presupuestado;
                    }
					else
					 $suma_aprobado=number_format($suma[1]['suma'],2,',','.');
					 
				     $pdf->Row(array(
                     utf8_decode($partidas_titulo[$j]['cod_partida']),
                     utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                     $suma_presupuestado,
                     $suma_aprobado));
					 
				    $cont=$j+1;  
				}//FIN IF
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
				    $suma=$this->_ReporteModelo-> getReporteMontosTituloPresupuestoAntepresupuesto($validacion['idpresupuesto'],$partida);
					
                    $pdf->Cell(10,5); 
					
					$suma_presupuestado=number_format($suma[0]['suma'],2,',','.');
					//$suma_aprobado=number_format($suma[1]['suma'],2,',','.');
					
					if(!isset($suma[1]['suma'])){
                         $suma_aprobado=$suma_presupuestado;
                    }
					else
					 $suma_aprobado=number_format($suma[1]['suma'],2,',','.');
					
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),$suma_presupuestado, $suma_aprobado ));
					
				    $cont=$j+1;  
				}//FIN IF
				
		//-----------------------------------------------		
				if (trim($codigo3)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 9);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTituloPresupuesto($validacion['idpresupuesto'],$partida);
					
                    $pdf->Cell(10,5); 
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma['suma_presupuestado'],2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				//------------------------------
			
			}//fin for
			/*FIN TITULOS DE PARTIDAS*/
			
			
			//---------------------------
		    
			 $pdf->SetFont('Arial', 'B', 7);
		     $pdf->SetFillColor(255, 255, 255);
			 
            $pdf->Cell(10,5); //
            $monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');
			
			$total = $total+$consulta[$i]['num_monto_presupuestado'];
		    $total_aprobado=$total_aprobado+$consulta[$i]['num_monto_aprobado'];
			
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                number_format($consulta[$i]['num_monto_presupuestado'],2,',','.'),number_format($consulta[$i]['num_monto_aprobado'],2,',','.')));
        }
		
	 
	 }//FIN TIPO PRESUPUESTO
	     $pdf->SetFont('Arial', 'B', 8);
		 $pdf->SetFillColor(202, 202, 202);
	 
	     $pdf->Cell(10,5);
	     $pdf->Row(array('','TOTAL',number_format($total,2,',','.'),number_format($total_aprobado,2,',','.')));
	 
         $pdf->Output();

    }


      public function metpresupuestoReportePdf()
    {

        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formTxt=$this->metObtenerTexto('form','txt');
		

        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
		$this->_ReporteModelo = $this->metCargarModelo('reporte');
		
		$organismo= $this->_ReporteModelo-> metListarOrganismoInt($validacion['fk_a001_num_organismo']);
		
        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabecera','modPR');
        $pdf=new pdfReporte('L','mm','Letter');
		
		$pdf->organismo = $organismo[0]['ind_descripcion_empresa'];
		$pdf->logo = $organismo[0]['ind_logo'];
		$pdf->logo_secundario = $organismo[0]['ind_logo_secundario'];
		
        $pdf->AliasNbPages();
        $pdf->AddPage();
				
		$pdf->Cell(250, 5, utf8_decode('REPORTE PRESUPUESTO'),0,1,'C');
		
		$pdf->SetFont('Arial', 'B', 6);
		$pdf->Cell(25, 5, utf8_decode('COD. PRESUPUESTO:'), 0, 1, 'L', 0);
		$pdf->Cell(25, 5, utf8_decode('TIPO DE PRESUPUESTO:'), 0, 1, 'L', 0);
		$pdf->Cell(25, 5, utf8_decode('CATEGORIA DE PRESUPUESTO:'), 0, 1, 'L', 0);

        $pdf->Ln(8);
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(10, 5);
        //$pdf->Cell(5, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, utf8_decode('COD. PARTIDA'), 1, 0, 'C', 1);
        $pdf->Cell(180, 5, 'DESCRIPCION', 1, 0, 'C', 1);
        $pdf->Cell(30, 5, 'MONTO APROBADO', 1, 0, 'C', 1);
       
        $pdf->SetFont('Arial', '', 7);
		
        $consulta= $this->_ReporteModelo-> getReportePresupuesto($validacion['idpresupuesto'],$validacion['fec_anio'],$validacion['fk_a001_num_organismo']);
		$partidas_titulo= $this->_ReporteModelo-> getReportePartidasTitulo();
		$aespecifica = $this->_ReporteModelo->metAespecifica();

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(4);
		
		
		 $pdf->SetWidths(array(25,180,30));
         $pdf->SetAligns(array('C','L','R'));
 	  
	     $total=0;
		 $tipo_presupuesto='';
   
   if (count($consulta)>0 && ($validacion['idpresupuesto']!="error" || $validacion['fec_anio']!="error"))
   
	 if($consulta[0]['ind_tipo_presupuesto']=='P')
     {
	    for($a=0;$a<count($aespecifica);$a++)
	    {
		   $cont=0;
		   $existeAespecifica = count($this->_ReporteModelo->metExisteAespecificaEs2($aespecifica[$a]['pk_num_aespecifica'], $validacion['idpresupuesto']));
		   
		   $pdf->SetFont('Arial', 'B', 8);
		   $pdf->SetFillColor(202, 202, 202);
		   
		   $pdf->SetWidths(array(25,210));
           $pdf->SetAligns(array('C','L'));
		   
		   
		   if ($existeAespecifica>0)
		   {
		      $pdf->Cell(10,5); 
		      $pdf->Row(array(
                    utf8_decode($aespecifica[$a]['ind_cod_aespecifica']),
                    utf8_decode($aespecifica[$a]['ind_descripcion'])
                    ));		
		   }
		   
		   
        $cont=0;	
        for($i=0; $i< count($consulta); $i++)
        {
		    //-------------------------
			 /*TITULOS DE PARTIDAS*/
		    $pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(235, 235, 235);
			$pdf->SetWidths(array(25,180,30));
            $pdf->SetAligns(array('C','L','R'));
		
		 if ($aespecifica[$a]['pk_num_aespecifica']==$consulta[$i]['fk_pr006_num_aespecifica'])
		 {
		
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{ 
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				$codigo3=substr_replace($consulta[$i]['cod_partida'],'00', 10,2);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTituloPresupuesto($validacion['idpresupuesto'],$partida,$aespecifica[$a]['pk_num_aespecifica']);
					
				     $pdf->Cell(10,5); 
				     $pdf->Row(array(
                     utf8_decode($partidas_titulo[$j]['cod_partida']),
                     utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                     number_format($suma['suma_presupuestado'],2,',','.')));
				
				    $cont=$j+1;  
				}//FIN IF
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTituloPresupuesto($validacion['idpresupuesto'],$partida,$aespecifica[$a]['pk_num_aespecifica']);
					
                    $pdf->Cell(10,5); 
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma['suma_presupuestado'],2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
		//-----------------------------------------------		
				if (trim($codigo3)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 9);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTituloPresupuesto($validacion['idpresupuesto'],$partida,'error');
					
                    $pdf->Cell(10,5); 
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma['suma_presupuestado'],2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				//------------------------------
			
			}//fin for
			/*FIN TITULOS DE PARTIDAS*/
			
			
			//---------------------------
		    
			 $pdf->SetFont('Arial', 'B', 7);
		     $pdf->SetFillColor(255, 255, 255);
			 
            $pdf->Cell(10,5); //
			$total = $total+$consulta[$i]['num_monto_aprobado'];
            $monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');
			
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto));
         }
		
		}//FIN IF ACCION ESPECIFICA
	
	  }//FIN AESPECIFICA 	
		
	 }//Fin Presupuesto por proyecto	
	 else
	 {
	    $cont=0;	
        for($i=0; $i< count($consulta); $i++)
        {
 		   //-------------------------
		     if ($i==0)
			{
			    $pdf->SetFont('Arial', 'B', 6);
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->Cell(25, 5, utf8_decode($consulta[$i]['ind_cod_presupuesto']), 0, 0, 'L', 0);
				
				$pdf->SetXY(50,41);
				  if($consulta[$i]['ind_tipo_presupuesto']=='G')
				     $tipo_presupuesto='PROGRAMA';
				  else 	if($consulta[$i]['ind_tipo_presupuesto']=='O')
				     $tipo_presupuesto='PROYECTO';
					 
				$pdf->Cell(25, 5, utf8_decode($tipo_presupuesto), 0, 0, 'L', 0);
				
				$pdf->SetXY(50,45);
				  if($consulta[$i]['ind_tipo_categoria']=='G')
				     $tipo_presupuesto='PROGRAMA';
				  else 	if($consulta[$i]['ind_tipo_categoria']=='O')
				     $tipo_presupuesto='PROYECTO';
				  else 	if($consulta[$i]['ind_tipo_categoria']=='C')
				     $tipo_presupuesto='ACCION CENTRALIZADA';	 
				  	 
					 
				$pdf->Cell(25, 5, utf8_decode($tipo_presupuesto), 0, 0, 'L', 0);
				
				
				$pdf->SetXY($x,$y);
			}
		    //-------------------------
			
			
			 /*TITULOS DE PARTIDAS*/
		     $pdf->SetFont('Arial', 'B', 7);
		     $pdf->SetFillColor(202, 202, 202);
		
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			   
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);	
				$codigo3=substr_replace($consulta[$i]['cod_partida'],'00', 10,2);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					$suma=$this->_ReporteModelo-> getReporteMontosTituloPresupuesto($validacion['idpresupuesto'],$partida);
					
				     $pdf->Cell(10,5); 
				     $pdf->Row(array(
                     utf8_decode($partidas_titulo[$j]['cod_partida']),
                     utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                     number_format($suma['suma_presupuestado'],2,',','.')));
				
				    $cont=$j+1;  
				}//FIN IF
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTituloPresupuesto($validacion['idpresupuesto'],$partida);
					
                    $pdf->Cell(10,5); 
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma['suma_presupuestado'],2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
		//-----------------------------------------------		
				if (trim($codigo3)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 9);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTituloPresupuesto($validacion['idpresupuesto'],$partida);
					
                    $pdf->Cell(10,5); 
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma['suma_presupuestado'],2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				//------------------------------
			
			}//fin for
			/*FIN TITULOS DE PARTIDAS*/
			
			
			//---------------------------
		    
			 $pdf->SetFont('Arial', 'B', 7);
		     $pdf->SetFillColor(255, 255, 255);
			 
             $pdf->Cell(10,5); //
             $monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');
			
			 $total = $total+$consulta[$i]['num_monto_aprobado'];
			
             $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto));
        }
		
	 
	 }//FIN TIPO PRESUPUESTO
	     $pdf->SetFont('Arial', 'B', 8);
		 $pdf->SetFillColor(202, 202, 202);
	 
	     $pdf->Cell(10,5);
	     $pdf->Row(array('','TOTAL',number_format($total,2,',','.')));
	 
        $pdf->Output();

    }

  
    public function metantepresupuestoReportePdf()
    {


        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formTxt=$this->metObtenerTexto('form','txt');
		

        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
				
		
        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
		
		$this->_ReporteModelo = $this->metCargarModelo('reporte');
		
		$organismo= $this->_ReporteModelo-> metListarOrganismoInt($validacion['fk_a001_num_organismo']);
		
        $this->metObtenerLibreria('cabecera','modPR');
        $pdf=new pdfReporteAntepresupuesto('L','mm','Letter');
		$pdf->organismo = $organismo[0]['ind_descripcion_empresa'];
		$pdf->logo = $organismo[0]['ind_logo'];
		$pdf->logo_secundario = $organismo[0]['ind_logo_secundario'];
		
		
        $pdf->AliasNbPages();
        $pdf->AddPage();
				
		//$pdf->Cell(250, 5, utf8_decode('REPORTE ANTEPRESUPUESTO'),0,1,'C');

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(10, 5);
        //$pdf->Cell(5, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, utf8_decode('COD. PARTIDA'), 1, 0, 'C', 1);
        $pdf->Cell(180, 5, 'DESCRIPCION', 1, 0, 'C', 1);
        $pdf->Cell(30, 5, 'MONTO', 1, 0, 'C', 1);
       
        $pdf->SetFont('Arial', '', 7);

        $consulta= $this->_ReporteModelo-> getReporteAntePresupuesto($validacion['idpresupuesto'],$validacion['fec_anio'],$validacion['fk_a001_num_organismo']);
		 
		$partidas_titulo= $this->_ReporteModelo-> getReportePartidasTitulo();
		$aespecifica = $this->_ReporteModelo->metAespecifica();
		

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(4);

        $cont=0;
		
		$pdf->SetWidths(array(25,180,30));
        $pdf->SetAligns(array('L','L','R'));
		//PRESUPUESTO POR PROYECTO   
		$total=0;
		
	//if (isset($consulta[0]['ind_tipo_presupuesto']) && ($validacion['idpresupuesto']!="error" && $validacion['fec_anio']!="error"))	
	
	
	if ($validacion['idpresupuesto']!="error" || $validacion['fec_anio']!="error")	
	 if ($consulta)
	   if ($consulta[0]['ind_tipo_presupuesto']=='P')
       {
		  
		for($a=0;$a<count($aespecifica);$a++)
	    {
		   $cont=0;
		   $existeAespecifica = count($this->_ReporteModelo->metExisteAespecificaEs($aespecifica[$a]['pk_num_aespecifica'], $validacion['idpresupuesto']));
		   
		   $pdf->SetFont('Arial', 'B', 8);
			$pdf->SetFillColor(202, 202, 202);
		   
		   if ($existeAespecifica>0)
		   {
		      $pdf->Cell(10,5); 
		      $pdf->Row(array(
                    utf8_decode($aespecifica[$a]['ind_cod_aespecifica']),
                    utf8_decode($aespecifica[$a]['ind_descripcion']),
                    ''));
			
		   }
		  
		  $pdf->Ln(1);
		  
          for($i=0; $i< count($consulta); $i++)
          {
          
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(235, 235, 235);

          if ($aespecifica[$a]['pk_num_aespecifica']==$consulta[$i]['fk_pr_b006_aespecifica'])
		  {
            
            for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				$codigo3=substr_replace($consulta[$i]['cod_partida'],'00', 10,2);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
				
					$suma=$this->_ReporteModelo-> getReporteMontosTituloAntepresupuesto($validacion['idpresupuesto'],$partida,$aespecifica[$a]['pk_num_aespecifica']);
					$suma_presupuestado=number_format($suma['suma_presupuestado'],2,',','.');
					
					$pdf->Cell(10,5); 
					$pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    $suma_presupuestado));
				    $cont=$j+1; 	
                }
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTituloAntepresupuesto($validacion['idpresupuesto'],$partida,$aespecifica[$a]['pk_num_aespecifica']);
					$suma_presupuestado=number_format($suma['suma_presupuestado'],2,',','.');
					
					$pdf->Cell(10,5); 
					$pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    $suma_presupuestado));
				    $cont=$j+1; 	
                }
				
				if (trim($codigo3)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 9);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTituloAntepresupuesto($validacion['idpresupuesto'],$partida,$partida,$aespecifica[$a]['pk_num_aespecifica']);
					$suma_presupuestado=number_format($suma['suma_presupuestado'],2,',','.');
					 
					$pdf->Cell(10,5);  
					$pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    $suma_presupuestado));
				    $cont=$j+1; 	
                }
				
			  }//FIN FOR j
            //-----------------------------------------------------------------------------------

            $pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);

            $pdf->Cell(10,5); 
            $monto=number_format($consulta[$i]['num_monto_presupuestado'],2,',','.');
			$total = $total+$consulta[$i]['num_monto_presupuestado'];
			
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto));

        }
        }//FIN FOR i
	   }//FIN FOR ACCION ESPECIFICA
	  }//FIN PRESUPUESTO POR PROYECTO
	  
	  else//PRESUPUESTO POR PROGRAMAS
	  {
	      for($i=0; $i< count($consulta); $i++)
          {
          
            $pdf->SetWidths(array(25,180,30));
            $pdf->SetAligns(array('L','L','R'));
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(202, 202, 202);

            
            for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			   
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				$codigo3=substr_replace($consulta[$i]['cod_partida'],'00', 10,2);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTituloAntepresupuesto($validacion['idpresupuesto'],$partida,'error');
					$suma_presupuestado=number_format($suma['suma_presupuestado'],2,',','.');
					
					$pdf->Cell(10,5);
					$pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    $suma_presupuestado));
				    $cont=$j+1; 	
                }
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTituloAntepresupuesto($validacion['idpresupuesto'],$partida,'error');
					$suma_presupuestado=number_format($suma['suma_presupuestado'],2,',','.');
					
					$pdf->Cell(10,5);
					$pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    $suma_presupuestado));
				    $cont=$j+1; 	
                }
				
				if (trim($codigo3)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 9);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTituloAntepresupuesto($validacion['idpresupuesto'],$partida,'error');
					$suma_presupuestado=number_format($suma['suma_presupuestado'],2,',','.');
					
					$pdf->Cell(10,5); 
					$pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    $suma_presupuestado));
				    $cont=$j+1; 	
                }
				
			  }
            //-----------------------------------------------------------------------------------

            $pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);

            $pdf->Cell(10,5); 
            $monto=number_format($consulta[$i]['num_monto_presupuestado'],2,',','.');
			$total = $total+$consulta[$i]['num_monto_presupuestado'];
			
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto));


         }
	   }
	  
	     $pdf->SetFont('Arial', 'B', 8);
		 $pdf->SetFillColor(202, 202, 202);
	 
	     $pdf->Cell(10,5);
	     $pdf->Row(array('','TOTAL',number_format($total,2,',','.')));
	 
		
        $pdf->Output();

    }

   
   public function metreformulacionReportePdf()
    {

        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formTxt=$this->metObtenerTexto('form','txt');
		
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
		ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
		
		$this->_ReporteModelo = $this->metCargarModelo('reporte');
		$organismo= $this->_ReporteModelo-> metListarOrganismoInt($validacion['fk_a001_num_organismo']);
		
		$this->metObtenerLibreria('cabecera','modPR');
		$pdf=new pdfReporteReformulacion('P','mm','Letter');
		 
		$pdf->organismo = $organismo[0]['ind_descripcion_empresa'];
		$pdf->logo = $organismo[0]['ind_logo'];
		$pdf->logo_secundario = $organismo[0]['ind_logo_secundario'];
		
		
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
				
        
		$pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0,0, 0);
		$pdf->SetFont('Arial', 'B', 8);
		
		$pdf->SetFont('Arial', 'B', 6);
		$pdf->Cell(25, 5, utf8_decode('COD. PRESUPUESTO:'), 0, 1, 'L', 0);
		$pdf->Cell(25, 5, utf8_decode('TIPO DE PRESUPUESTO:'), 0, 1, 'L', 0);
		$pdf->Cell(25, 5, utf8_decode('CATEGORIA DE PRESUPUESTO:'), 0, 1, 'L', 0);

		 $pdf->Ln(2);
		 
		$pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(10, 5);
		 
		    $pdf->Cell(35, 5, utf8_decode('COD. PARTIDA'), 1, 0, 'C', 1);
            $pdf->Cell(100, 5, 'DENOMINACION', 1, 0, 'C', 1);
            $pdf->Cell(40, 5, 'MONTO REFORMULACION', 1, 0, 'C', 1);
		   
       
        $pdf->SetFont('Arial', '', 7);
		
		
		if(isset($validacion['idreformulacion']))
          $consulta= $this->_ReporteModelo-> getReporteReformulacion($validacion['idpresupuesto'],$validacion['idreformulacion']);
		

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(4);

        if (isset($consulta) && count($consulta)>0 && ($validacion['idpresupuesto']!='error' || $validacion['idreformulacion']!='error'))
        for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {
		
		    $pdf->Cell(10, 5);
            $pdf->SetWidths(array(10,25,100,40));
            $pdf->SetAligns(array('C','C','C','L'));
            $pdf->Cell(10,5); //

           // $monto=number_format($consulta[$i]['num_monto_reformulacion'],2,',','.');
			
			$consultadetalle= $this->_ReporteModelo-> getReporteReformulacionDetalle($consulta[$i]['pk_num_reformulacion_presupuesto']);
		
			/* if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->Cell(25, 5, utf8_decode($consultadetalle[$i]['ind_cod_presupuesto']), 0, 0, 'C', 0);
				
				$pdf->SetXY($x,$y);
			}*/
			
			 //-------------------------
		     if ($i==0)
			{
			    $pdf->SetFont('Arial', 'B', 6);
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->Cell(25, 5, utf8_decode($consultadetalle[$i]['ind_cod_presupuesto']), 0, 0, 'L', 0);
				
				$pdf->SetXY(50,41);
				  if($consultadetalle[$i]['ind_tipo_presupuesto']=='G')
				     $tipo_presupuesto='PROGRAMA';
				  else 	if($consultadetalle[$i]['ind_tipo_presupuesto']=='O')
				     $tipo_presupuesto='PROYECTO';
					 
				$pdf->Cell(25, 5, utf8_decode($tipo_presupuesto), 0, 0, 'L', 0);
				
				$pdf->SetXY(50,45);
				  if($consultadetalle[$i]['ind_tipo_categoria']=='G')
				     $tipo_presupuesto='PROGRAMA';
				  else 	if($consultadetalle[$i]['ind_tipo_categoria']=='O')
				     $tipo_presupuesto='PROYECTO';
				  else 	if($consultadetalle[$i]['ind_tipo_categoria']=='C')
				     $tipo_presupuesto='ACCION CENTRALIZADA';	 
				  	 
					 
				$pdf->Cell(25, 5, utf8_decode($tipo_presupuesto), 0, 0, 'L', 0);
				
				
				$pdf->SetXY($x,$y);
			}
		    //-------------------------
			
			
			if (count($consultadetalle)>0)
			for($j=0; $j< count($consultadetalle); $j++)
            {
				  
				  $pdf->SetDrawColor(255, 255, 255);			
			      $pdf->SetWidths(array(10,25,100,40)); $pdf->SetAligns(array('C','C','L','R'));
                  $pdf->Cell(5,5);
			       
			      $pdf->SetFont('Arial', '', 7);
				
			        $monto_reformulacion=$consultadetalle[$j]['num_monto'];
				
				  $pdf->Ln(2);  
				  $monto=number_format($consultadetalle[$j]['num_monto'],2,',','.');
				  $pdf->Cell(10,5);
				  
				if (isset($consultadetalle[0]['ind_tipo_presupuesto']))	
	            if ($consultadetalle[0]['ind_tipo_presupuesto']=='P')
                {  
							  $pdf->Row(array('00'.$consultadetalle[$j]['fk_pr_b006_aespecifica'],
							   utf8_decode($consultadetalle[$j]['cod_partida']),
							   utf8_decode($consultadetalle[$j]['ind_denominacion']),
							   $monto));
				}else{
				
				                $pdf->Row(array('',
							    utf8_decode($consultadetalle[$j]['cod_partida']),
								utf8_decode($consultadetalle[$j]['ind_denominacion']),
								$monto));
				
				}				
             }
			 
			 $pdf->SetDrawColor(0, 0, 0);
        }
        $pdf->Output();

    }

    
    public function metajusteReportePdf()
    {

        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formTxt=$this->metObtenerTexto('form','txt');
		
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
		$this->_ReporteModelo = $this->metCargarModelo('reporte');
		
		$organismo= $this->_ReporteModelo-> metListarOrganismoInt($validacion['fk_a001_num_organismo']);
		
        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabecera','modPR');
		$pdf=new pdfReporteAjuste('P','mm','Letter');
	    $pdf->organismo = $organismo[0]['ind_descripcion_empresa'];
		$pdf->logo = $organismo[0]['ind_logo'];
		$pdf->logo_secundario = $organismo[0]['ind_logo_secundario'];
       
        $pdf->AliasNbPages();
        $pdf->AddPage();
		
		$pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0,0, 0);
		$pdf->SetFont('Arial', 'B', 8);
		
		 $pdf->Cell(25, 5, utf8_decode('COD. PRESUPUESTO:'), 1, 1, 'L', 0);
		 $pdf->Ln(2);
				
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(5, 5);
        $pdf->Cell(5, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, utf8_decode('COD. AJUSTE'), 1, 0, 'C', 1);
		$pdf->Cell(20, 5, utf8_decode('TIPO'), 1, 0, 'C', 1);
        $pdf->Cell(100, 5, 'DESCRIPCION', 1, 0, 'C', 1);
        $pdf->Cell(30, 5, 'MONTO AJUSTE', 1, 0, 'C', 1);
		
		
        $pdf->SetFont('Arial', '', 7);
		
		if (isset ($validacion['idajuste']))
          $consulta= $this->_ReporteModelo-> getReporteAjuste($validacion['idpresupuesto'],$validacion['idajuste'],$validacion['tipoajuste'],$validacion['fk_a001_num_organismo']);
		

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(4);
        

        if (isset ($consulta)) 
         for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
         {
           
            $pdf->SetWidths(array(5,25,20,100,30));
            $pdf->SetAligns(array('C','C','L','L','R'));
            $pdf->Cell(5,5); //
			
			 
            $pdf->Row(array($i+1,
                utf8_decode($consulta[$i]['ind_cod_ajuste_presupuestario']),
				utf8_decode($consulta[$i]['ind_nombre_detalle']),
                utf8_decode($consulta[$i]['ind_descripcion']),number_format($consulta[$i]['num_total_ajuste'],2,',','.')));
				
			
			$consultadetalle= $this->_ReporteModelo-> getReporteAjusteDetalle($consulta[$i]['pk_num_ajuste_presupuestario']);
			
			$pdf->Ln(2);
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->Cell(5,5);
			$pdf->Cell(20, 5,utf8_decode('AESP'), 1, 0, 'C', 1);	
	              $pdf->Cell(25, 5, utf8_decode('COD. PARTIDA'), 1, 0, 'C', 1);
                  $pdf->Cell(105, 5, 'DENOMINACION', 1, 0, 'C', 1);
                  $pdf->Cell(30, 5, 'MONTO AJUSTADO', 1, 0, 'C', 1);
				  $pdf->Ln(4);
				  
			  
			for($j=0; $j< count($consultadetalle); $j++)
            {
			
			 if ($j==0)
			 {
					$x=$pdf->getX();
					$y=$pdf->getY();
					
					$pdf->SetXY(50,36);
					$pdf->Cell(25, 5, utf8_decode($consultadetalle[$j]['ind_cod_presupuesto']), 0, 0, 'C', 0);
					
					$pdf->SetXY($x,$y);
			 }
			
				  $pdf->Ln(2);
				  $pdf->SetDrawColor(255, 255, 255);			
			      $pdf->SetWidths(array(20,25,100,30)); $pdf->SetAligns(array('C','L','L','R'));
                  $pdf->Cell(5,5);
			       
			      $pdf->SetFont('Arial', '', 7);
				 if ($consulta[$i]['fk_a006_num_miscelaneo_det_tipo_ajuste']==4)
			        $monto_ajuste=$consultadetalle[$j]['num_monto_disponible']+$consultadetalle[$j]['num_monto_ajustado'];
			     else
			        $monto_ajuste=$consultadetalle[$j]['num_monto_disponible']-$consultadetalle[$j]['num_monto_ajustado'];
				
				  $monto=number_format($consultadetalle[$j]['num_monto_ajustado'],2,',','.');
                  $pdf->Row(array($consultadetalle[$j]['ind_cod_aespecifica'] ,
                    utf8_decode($consultadetalle[$j]['cod_partida']),
                    utf8_decode($consultadetalle[$j]['ind_denominacion']),
                    $monto));
					
             }
			 $pdf->Ln(4);
			 $pdf->SetDrawColor(0, 0, 0);
        }
        $pdf->Output();

    }


    //------------------------------------------------------------------------------------------
	
	
	public function metcierremesReportePdf()
    {
	

       #Recibiendo las variables del formulario que utiliza la clase vehículo
	   

        $formInt=$this->metObtenerInt('form','int');

        $formTxt=$this->metObtenerTexto('form','txt');
		
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
	
		   if($validacion['fec_mes']!="error"){
            $fec_mes=$validacion['fec_mes'];
        }else{
            $fec_mes="";
        }
			
		  if($validacion['fec_anio']!="error"){
            $fec_anio=$validacion['fec_anio'];
        }else{
            $fec_anio="";
        }

        $this->_ReporteModelo = $this->metCargarModelo('reporte');
		$organismo= $this->_ReporteModelo-> metListarOrganismoInt($validacion['fk_a001_num_organismo']);

	     
        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabecera','modPR');
        $pdf=new pdfReporteCierreMes('L','mm','Legal');
		
		$pdf->organismo = $organismo[0]['ind_descripcion_empresa'];
		$pdf->logo = $organismo[0]['ind_logo'];
		$pdf->logo_secundario = $organismo[0]['ind_logo_secundario'];
        $pdf->AliasNbPages();
        $pdf->AddPage();
				
		
		$pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);

		// $this->_ReporteModelo = $this->metCargarModelo('reporte');
		
       switch ($validacion['fec_mes']) {
		case "01": $fec_mes = "ENERO"; break;  
		case "02": $fec_mes= "FEBRERO"; break; 
		case "03": $fec_mes= "MARZO"; break;   
		case "04": $fec_mes= "ABRIL"; break;   
		case "05": $fec_mes = "MAYO"; break;    
		case "06": $fec_mes  = "JUNIO"; break;
		case "07": $fec_mes = "JULIO"; break;
		case "08": $fec_mes  = "AGOSTO"; break;
		case "09": $fec_mes  = "SEPTIEMBRE"; break;
		case "10": $fec_mes  = "OCTUBRE"; break;
		case "11": $fec_mes = "NOVIEMBRE"; break;
		case "12": $fec_mes  = "DICIEMBRE"; break;
    } 
	
	
		
	$pdf->SetFont('Arial', 'B', 10);
	$pdf->Cell(120, 10, '', 0, 0, 'C');
	$pdf->Cell(58, 10, utf8_decode(' REPORTE DE CIERRE MES '), 0, 0, 'C');
    $pdf->Cell(13, 10, $fec_mes , 0, 0, 'C'); $pdf->Cell(20, 10, $fec_anio, 0, 1, 'C');
	$pdf->Ln(3);

		$pdf->Ln(8);
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
       // $pdf->Cell(5, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, utf8_decode('COD. PARTIDA'), 1, 0, 'C', 1);
        $pdf->Cell(95, 5, 'DESCRIPCION', 1, 0, 'C', 1);
        $pdf->Cell(22, 5, 'DISP. PRESUP.', 1, 0, 'C', 1);
		$pdf->Cell(22, 5, 'DISP. FINANC.', 1, 0, 'C', 1);
		$pdf->Cell(20, 5, 'INCREMENTO', 1, 0, 'C', 1); 
		$pdf->Cell(20, 5, utf8_decode('DISMINUCIÓN'), 1, 0, 'C', 1);
		$pdf->Cell(22, 5, 'PRESUP. AJUST', 1, 0, 'C', 1);
		$pdf->Cell(22, 5, 'COMPROMETIDO', 1, 0, 'C', 1);
		$pdf->Cell(20, 5, 'CAUSADO', 1, 0, 'C', 1);
		$pdf->Cell(20, 5, 'PAGADO', 1, 0, 'C', 1);
		$pdf->Cell(22, 5, 'DISP. PRESUP', 1, 0, 'C', 1);
		$pdf->Cell(22, 5, 'DISP. FINAN.', 1, 0, 'C', 1);
       
        $pdf->SetFont('Arial', '', 7);

		
        $consulta= $this->_ReporteModelo-> getReporteCierremes($validacion['idpresupuesto'],$validacion['fec_anio'],$validacion['fec_mes'] );
		$partidas_titulo= $this->_ReporteModelo-> getReportePartidasTitulo();
		$aespecifica = $this->_ReporteModelo->metAespecifica();
				
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(4);
	
        $montot=0; $montofi=0; $montoin=0; $montodi=0; $montoat=0; $montoct=0; $montocat=0; $montopt=0; $montodt=0; $montopfnt=0;

	    $p=0; $cont=0;
		
		
	  if (isset($consulta[0]['ind_tipo_presupuesto']))	
	  if ($consulta[0]['ind_tipo_presupuesto']=='P')
      {	
	  
		$pdf->Ln(2);
		for($a=0;$a<count($aespecifica);$a++)
	    {
		   $cont=0;
		   $existeAespecifica = count($this->_ReporteModelo->metExisteAespecificaEs4($aespecifica[$a]['pk_num_aespecifica'], $validacion['idpresupuesto']));
		   
		   $pdf->SetFont('Arial', 'B', 8);
		   $pdf->SetFillColor(215, 215 ,215);
			
			$pdf->SetWidths(array(25,307));
            $pdf->SetAligns(array('L','L'));
		   
		   if ($existeAespecifica>0)
		   {
		      //$pdf->Cell(5,5); 
		      $pdf->Row(array(
                    utf8_decode($aespecifica[$a]['ind_cod_aespecifica']),
                    utf8_decode($aespecifica[$a]['ind_descripcion'])));
			
		   }
		
	   
         for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
         {

			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);

				$pdf->SetXY($x,$y);
			}
			
			
            $pdf->SetWidths(array(25,95,22,22,20,20,22,22,20,20,22,22));
            $pdf->SetAligns(array('L','L','R','R','R','R','R','R','R','R','R','R'));
            
			
			
			
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(235, 235, 235);
			
			
	      if ($aespecifica[$a]['pk_num_aespecifica']==$consulta[$i]['fk_pr006_num_aespecifica'])
		  {		
				$monto=number_format($consulta[$i]['num_disponibilidad_presupuestaria_inicial'],2,',','.');  $montot=$montot+$consulta[$i]['num_disponibilidad_presupuestaria_inicial'];
				$montof=number_format($consulta[$i]['num_disponibilidad_financiera_inicial'],2,',','.');  $montofi=$montofi+$consulta[$i]['num_disponibilidad_financiera_inicial'];
				$montoi=number_format($consulta[$i]['num_incremento'],2,',','.');  $montoin=$montoin+$consulta[$i]['num_incremento'];
				$montods=number_format($consulta[$i]['num_disminucion'],2,',','.');  $montodi=$montodi+$consulta[$i]['num_disminucion'];
				$montoa=number_format($consulta[$i]['num_presupuesto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_presupuesto_ajustado'];
				$montoc=number_format($consulta[$i]['num_compromiso'],2,',','.'); $montoct=$montoct+$consulta[$i]['num_compromiso'];
				$montoca=number_format($consulta[$i]['num_causado'],2,',','.'); $montocat=$montocat+$consulta[$i]['num_causado'];
				$montop=number_format($consulta[$i]['num_pagado'],2,',','.'); $montopt=$montopt+$consulta[$i]['num_pagado'];
				$montod=$consulta[$i]['num_presupuesto_ajustado']-$consulta[$i]['num_compromiso']; $montodt=$montodt+$montod;
				$montofn=number_format($consulta[$i]['num_disponibilidad_financiera'],2,',','.'); $montopfnt=$montopfnt+$consulta[$i]['num_disponibilidad_financiera'];
			
			
				
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			   
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTituloCierre($validacion['idpresupuesto'],$partida,$validacion['fec_anio'],$validacion['fec_mes'],$aespecifica[$a]['pk_num_aespecifica']);
					$suma_presupuestado=$suma['suma_presupuesto_inicial'];
					$suma_financiero=$suma['suma_financiero_inicial'];
					$suma_incremento=$suma['suma_incremento'];
					$suma_disminucion=$suma['suma_disminucion'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_causado=$suma['suma_causado'];
					$suma_pagado=$suma['suma_pagado'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
					$suma_financiera_final=$suma_ajustado-$suma_pagado;
				
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_financiero,2,',','.'),number_format($suma_incremento,2,',','.'),
					number_format($suma_disminucion,2,',','.'), number_format($suma_ajustado,2,',','.'),
					number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),
					number_format($suma_disponible,2,',','.'),number_format($suma_financiera_final,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTituloCierre($validacion['idpresupuesto'],$partida,$validacion['fec_anio'],$validacion['fec_mes'],$aespecifica[$a]['pk_num_aespecifica']);
					$suma_presupuestado=$suma['suma_presupuesto_inicial'];
					$suma_financiero=$suma['suma_financiero_inicial'];
					$suma_incremento=$suma['suma_incremento'];
					$suma_disminucion=$suma['suma_disminucion'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_causado=$suma['suma_causado'];
					$suma_pagado=$suma['suma_pagado'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
					$suma_financiera_final=$suma_ajustado-$suma_pagado;

				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_financiero,2,',','.'),number_format($suma_incremento,2,',','.'),
					number_format($suma_disminucion,2,',','.'), number_format($suma_ajustado,2,',','.'),
					number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),
					number_format($suma_disponible,2,',','.'),number_format($suma_financiera_final,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
			
			}//fin for
		  
			/*FIN TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);
			
			
			
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto,$montof,$montoi,$montods,$montoa,$montoc,$montoca,$montop,number_format($montod,2,',','.'),$montofn));
			 }//FIN IF ACCION ESP	
			 
         }
		}//FIN FOR AESPECIFICA
		
		    $montot=number_format($montot,2,',','.'); 
			$montofi=number_format($montofi,2,',','.'); 
			$montoin=number_format($montoin,2,',','.'); 
			$montodi=number_format($montodi,2,',','.'); 
			$montoat=number_format($montoat,2,',','.');
			$montoct=number_format($montoct,2,',','.');
			$montocat=number_format($montocat,2,',','.'); 
			$montopt=number_format($montopt,2,',','.'); 
			$montodt=number_format($montodt,2,',','.');
			$montopfnt=number_format($montopfnt,2,',','.');
			
			$pdf->SetFont('Arial', 'B', 7);
		    $pdf->Row(array('','TOTALES', $montot,$montofi,$montoin,$montodi,$montoat,$montoct,$montocat,$montopt,$montodt,$montopfnt));
			
		
		
	  }
	  else //TIPO PRESUPUESTO
	  {
	      for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
         {

			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);

				
				$pdf->SetXY($x,$y);
			}
			
            $pdf->SetWidths(array(5,20,95,22,22,20,20,22,22,20,20,22,22));
            $pdf->SetAligns(array('C','L','L','R','R','R','R','R','R','R','R','R','R'));
            
			$monto=number_format($consulta[$i]['num_disponibilidad_presupuestaria_inicial'],2,',','.');  $montot=$montot+$consulta[$i]['num_disponibilidad_presupuestaria_inicial'];
			$montof=number_format($consulta[$i]['num_disponibilidad_financiera_inicial'],2,',','.');  $montofi=$montofi+$consulta[$i]['num_disponibilidad_financiera_inicial'];
			$montoi=number_format($consulta[$i]['num_incremento'],2,',','.');  $montoin=$montoin+$consulta[$i]['num_incremento'];
			$montods=number_format($consulta[$i]['num_disminucion'],2,',','.');  $montodi=$montodi+$consulta[$i]['num_disminucion'];
			$montoa=number_format($consulta[$i]['num_presupuesto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_presupuesto_ajustado'];
			$montoc=number_format($consulta[$i]['num_compromiso'],2,',','.'); $montoct=$montoct+$consulta[$i]['num_compromiso'];
			$montoca=number_format($consulta[$i]['num_causado'],2,',','.'); $montocat=$montocat+$consulta[$i]['num_causado'];
			$montop=number_format($consulta[$i]['num_pagado'],2,',','.'); $montopt=$montopt+$consulta[$i]['num_pagado'];
			$montod=$consulta[$i]['num_presupuesto_ajustado']-$consulta[$i]['num_compromiso']; $montodt=$montodt+$montod;
			$montofn=number_format($consulta[$i]['num_disponibilidad_financiera'],2,',','.'); $montopfnt=$montopfnt+$consulta[$i]['num_disponibilidad_financiera'];
			
			
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(202, 202, 202);
			
				
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			   
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTituloCierre($validacion['idpresupuesto'],$partida,$validacion['fec_anio'],$validacion['fec_mes'],false);
					$suma_presupuestado=$suma['suma_presupuesto_inicial'];
					$suma_financiero=$suma['suma_financiero_inicial'];
					$suma_incremento=$suma['suma_incremento'];
					$suma_disminucion=$suma['suma_disminucion'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_causado=$suma['suma_causado'];
					$suma_pagado=$suma['suma_pagado'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
					$suma_financiera_final=$suma_ajustado-$suma_pagado;
				
				    $pdf->Row(array('',
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_financiero,2,',','.'),number_format($suma_incremento,2,',','.'),
					number_format($suma_disminucion,2,',','.'), number_format($suma_ajustado,2,',','.'),
					number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),
					number_format($suma_disponible,2,',','.'),number_format($suma_financiera_final,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTituloCierre($validacion['idpresupuesto'],$partida,$validacion['fec_anio'],$validacion['fec_mes'],false);
					$suma_presupuestado=$suma['suma_presupuesto_inicial'];
					$suma_financiero=$suma['suma_financiero_inicial'];
					$suma_incremento=$suma['suma_incremento'];
					$suma_disminucion=$suma['suma_disminucion'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_causado=$suma['suma_causado'];
					$suma_pagado=$suma['suma_pagado'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
					$suma_financiera_final=$suma_ajustado-$suma_pagado;

				    $pdf->Row(array('',
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_financiero,2,',','.'),number_format($suma_incremento,2,',','.'),
					number_format($suma_disminucion,2,',','.'), number_format($suma_ajustado,2,',','.'),
					number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),
					number_format($suma_disponible,2,',','.'),number_format($suma_financiera_final,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
			
			}//fin for
			/*FIN TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);
			
			
			
            $pdf->Row(array($i+1,
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto,$montof,$montoi,$montods,$montoa,$montoc,$montoca,$montop,number_format($montod,2,',','.'),$montofn));
			 
        } 
	  
	  
	        $montot=number_format($montot,2,',','.'); 
			$montofi=number_format($montofi,2,',','.'); 
			$montoin=number_format($montoin,2,',','.'); 
			$montodi=number_format($montodi,2,',','.'); 
			$montoat=number_format($montoat,2,',','.');
			$montoct=number_format($montoct,2,',','.');
			$montocat=number_format($montocat,2,',','.'); 
			$montopt=number_format($montopt,2,',','.'); 
			$montodt=number_format($montodt,2,',','.');
			$montopfnt=number_format($montopfnt,2,',','.');
			
			$pdf->SetFont('Arial', 'B', 7);
		    $pdf->Row(array('','','TOTALES', $montot,$montofi,$montoin,$montodi,$montoat,$montoct,$montocat,$montopt,$montodt,$montopfnt));
			
	  }//FIN TIPO PRESUPUESTO
	  		
				
        $pdf->Output();


    }


//------------------------------------------------------



 //------------------------------------------------------------------------------------------
	
	
	public function metcierremesReportePdf2()
    {
       #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formTxt=$this->metObtenerTexto('form','txt');
		
		$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
		
		
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
		
		 foreach ($alphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if(!empty($alphaNum[$tituloAlphaNum])){
                    $validacion[$tituloAlphaNum]=$valorAlphaNum;
                }else{
                    $validacion[$tituloAlphaNum]='error';

                }
            }
		
	
		   if($validacion['fec_mes']!="error"){
            $fec_mes=$validacion['fec_mes'];
        }else{
            $fec_mes="";
        }
			
		  if($validacion['fec_anio']!="error"){
            $fec_anio=$validacion['fec_anio'];
        }else{
            $fec_anio="";
        }

        $this->_ReporteModelo = $this->metCargarModelo('reporte');
		$organismo= $this->_ReporteModelo-> metListarOrganismoInt(1);

        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabecera','modPR');
        $pdf=new pdfReporteCierreMes('L','mm','Legal');
		$pdf->organismo = $organismo[0]['ind_descripcion_empresa'];
		$pdf->logo = $organismo[0]['ind_logo'];
		$pdf->logo_secundario = $organismo[0]['ind_logo_secundario'];
        $pdf->AliasNbPages();
        $pdf->AddPage();
				
		
		$pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);

		// $this->_ReporteModelo = $this->metCargarModelo('reporte');
		
       switch ($validacion['fec_mes']) {
		case "01": $fec_mes = "ENERO"; break;  
		case "02": $fec_mes= "FEBRERO"; break; 
		case "03": $fec_mes= "MARZO"; break;   
		case "04": $fec_mes= "ABRIL"; break;   
		case "05": $fec_mes = "MAYO"; break;    
		case "06": $fec_mes  = "JUNIO"; break;
		case "07": $fec_mes = "JULIO"; break;
		case "08": $fec_mes  = "AGOSTO"; break;
		case "09": $fec_mes  = "SEPTIEMBRE"; break;
		case "10": $fec_mes  = "OCTUBRE"; break;
		case "11": $fec_mes = "NOVIEMBRE"; break;
		case "12": $fec_mes  = "DICIEMBRE"; break;
    } 
	
	
		
	$pdf->SetFont('Arial', 'B', 10);
	$pdf->Cell(105, 10, '', 0, 0, 'C');
	$pdf->Cell(58, 10, utf8_decode(' REPORTE DE CIERRE MES '), 0, 0, 'C');
    $pdf->Cell(13, 10, $fec_mes , 0, 0, 'C'); $pdf->Cell(20, 10, $fec_anio, 0, 1, 'C');
	$pdf->Ln(3);

		$pdf->Ln(8);
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
       // $pdf->Cell(5, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, utf8_decode('COD. PARTIDA'), 1, 0, 'C', 1);
        $pdf->Cell(95, 5, 'DESCRIPCION', 1, 0, 'C', 1);
        $pdf->Cell(22, 5, 'DISP. PRESUP.', 1, 0, 'C', 1);
		$pdf->Cell(22, 5, 'DISP. FINANC.', 1, 0, 'C', 1);
		$pdf->Cell(20, 5, 'INCREMENTO', 1, 0, 'C', 1); 
		$pdf->Cell(20, 5, utf8_decode('DISMINUCIÓN'), 1, 0, 'C', 1);
		$pdf->Cell(22, 5, 'PRESUP. AJUST', 1, 0, 'C', 1);
		$pdf->Cell(22, 5, 'COMPROMETIDO', 1, 0, 'C', 1);
		$pdf->Cell(20, 5, 'CAUSADO', 1, 0, 'C', 1);
		$pdf->Cell(20, 5, 'PAGADO', 1, 0, 'C', 1);
		$pdf->Cell(22, 5, 'DISP. PRESUP', 1, 0, 'C', 1);
		$pdf->Cell(22, 5, 'DISP. FINAN.', 1, 0, 'C', 1);
       
        $pdf->SetFont('Arial', '', 7);
		$precierre=0;

		
        $consulta= $this->_ReporteModelo-> getReporteCierremes($validacion['fk_prb004_num_presupuesto'],$validacion['fec_anio'],$validacion['fec_mes'] );
		
		
		if (isset($consulta))	
		{
		 $consulta= $this->_ReporteModelo-> getReportePreCierremes($validacion['fk_prb004_num_presupuesto'],$validacion['fec_anio'],$validacion['fec_mes'] );
		 $precierre=1;
		}
	
		
		$partidas_titulo= $this->_ReporteModelo-> getReportePartidasTitulo();
		$aespecifica = $this->_ReporteModelo->metAespecifica();
				
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(4);
	
        $montot=0; $montofi=0; $montoin=0; $montodi=0; $montoat=0; $montoct=0; $montocat=0; $montopt=0; $montodt=0; $montopfnt=0;

	    $p=0; $cont=0;
		
		
	  if (isset($consulta[0]['ind_tipo_presupuesto']))	
	  if ($consulta[0]['ind_tipo_presupuesto']=='P')
      {	
	  
		$pdf->Ln(2);
		for($a=0;$a<count($aespecifica);$a++)
	    {
		   $cont=0;
		   $existeAespecifica = count($this->_ReporteModelo->metExisteAespecificaEs4($aespecifica[$a]['pk_num_aespecifica'], $validacion['fk_prb004_num_presupuesto']));
		   
		   $pdf->SetFont('Arial', 'B', 8);
		   $pdf->SetFillColor(215, 215 ,215);
			
			$pdf->SetWidths(array(25,307));
            $pdf->SetAligns(array('L','L'));
		   
		   if ($existeAespecifica>0)
		   {
		      //$pdf->Cell(5,5); 
		      $pdf->Row(array(
                    utf8_decode($aespecifica[$a]['ind_cod_aespecifica']),
                    utf8_decode($aespecifica[$a]['ind_descripcion'])));
			
		   }
		
	   
         for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
         {

			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);

				$pdf->SetXY($x,$y);
			}
			
			
            $pdf->SetWidths(array(25,95,22,22,20,20,22,22,20,20,22,22));
            $pdf->SetAligns(array('L','L','R','R','R','R','R','R','R','R','R','R'));
            
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(235, 235, 235);
			
			
	      if ($aespecifica[$a]['pk_num_aespecifica']==$consulta[$i]['fk_pr006_num_aespecifica'])
		  {		
				
			$monto=number_format($consulta[$i]['num_disponibilidad_presupuestaria_inicial'],2,',','.');  $montot=$montot+$consulta[$i]['num_disponibilidad_presupuestaria_inicial'];
			$montof=number_format($consulta[$i]['num_disponibilidad_financiera_inicial'],2,',','.');  $montofi=$montofi+$consulta[$i]['num_disponibilidad_financiera_inicial'];
			$montoi=number_format($consulta[$i]['num_incremento'],2,',','.');  $montoin=$montoin+$consulta[$i]['num_incremento'];
			$montods=number_format($consulta[$i]['num_disminucion'],2,',','.');  $montodi=$montodi+$consulta[$i]['num_disminucion'];
			$montoa=number_format($consulta[$i]['num_presupuesto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_presupuesto_ajustado'];
			$montoc=number_format($consulta[$i]['num_compromiso'],2,',','.'); $montoct=$montoct+$consulta[$i]['num_compromiso'];
			$montoca=number_format($consulta[$i]['num_causado'],2,',','.'); $montocat=$montocat+$consulta[$i]['num_causado'];
			$montop=number_format($consulta[$i]['num_pagado'],2,',','.'); $montopt=$montopt+$consulta[$i]['num_pagado'];
			$montod=$consulta[$i]['num_presupuesto_ajustado']-$consulta[$i]['num_compromiso']; $montodt=$montodt+$montod;
			$montofn=number_format($consulta[$i]['num_disponibilidad_financiera'],2,',','.'); $montopfnt=$montopfnt+$consulta[$i]['num_disponibilidad_financiera'];
			
			
				
				
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			   
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTituloCierre($validacion['fk_prb004_num_presupuesto'],$partida,$validacion['fec_anio'],$validacion['fec_mes'],$aespecifica[$a]['pk_num_aespecifica']);
					$suma_presupuestado=$suma['suma_presupuesto_inicial'];
					$suma_financiero=$suma['suma_financiero_inicial'];
					$suma_incremento=$suma['suma_incremento'];
					$suma_disminucion=$suma['suma_disminucion'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_causado=$suma['suma_causado'];
					$suma_pagado=$suma['suma_pagado'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
					$suma_financiera_final=$suma_ajustado-$suma_pagado;
				
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_financiero,2,',','.'),number_format($suma_incremento,2,',','.'),
					number_format($suma_disminucion,2,',','.'), number_format($suma_ajustado,2,',','.'),
					number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),
					number_format($suma_disponible,2,',','.'),number_format($suma_financiera_final,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTituloCierre($validacion['fk_prb004_num_presupuesto'],$partida,$validacion['fec_anio'],$validacion['fec_mes'],$aespecifica[$a]['pk_num_aespecifica']);
					$suma_presupuestado=$suma['suma_presupuesto_inicial'];
					$suma_financiero=$suma['suma_financiero_inicial'];
					$suma_incremento=$suma['suma_incremento'];
					$suma_disminucion=$suma['suma_disminucion'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_causado=$suma['suma_causado'];
					$suma_pagado=$suma['suma_pagado'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
					$suma_financiera_final=$suma_ajustado-$suma_pagado;

				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_financiero,2,',','.'),number_format($suma_incremento,2,',','.'),
					number_format($suma_disminucion,2,',','.'), number_format($suma_ajustado,2,',','.'),
					number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),
					number_format($suma_disponible,2,',','.'),number_format($suma_financiera_final,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
			
			}//fin for
		  
			/*FIN TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);
			
			
			
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto,$montof,$montoi,$montods,$montoa,$montoc,$montoca,$montop,number_format($montod,2,',','.'),$montofn));
			 }//FIN IF ACCION ESP	
			 
         }
		}//FIN FOR AESPECIFICA
		
		    $montot=number_format($montot,2,',','.'); 
			$montofi=number_format($montofi,2,',','.'); 
			$montoin=number_format($montoin,2,',','.'); 
			$montodi=number_format($montodi,2,',','.'); 
			$montoat=number_format($montoat,2,',','.');
			$montoct=number_format($montoct,2,',','.');
			$montocat=number_format($montocat,2,',','.'); 
			$montopt=number_format($montopt,2,',','.'); 
			$montodt=number_format($montodt,2,',','.');
			$montopfnt=number_format($montopfnt,2,',','.');
			
			$pdf->SetFont('Arial', 'B', 7);
		    $pdf->Row(array('','TOTALES', $montot,$montofi,$montoin,$montodi,$montoat,$montoct,$montocat,$montopt,$montodt,$montopfnt));
			
		
		
	  }
	  else //TIPO PRESUPUESTO
	  {
	      for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
         {

			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);

				
				$pdf->SetXY($x,$y);
			}
			
            $pdf->SetWidths(array(5,20,95,22,22,20,20,22,22,20,20,22,22));
            $pdf->SetAligns(array('C','L','L','R','R','R','R','R','R','R','R','R','R'));
            
			$monto=number_format($consulta[$i]['num_disponibilidad_presupuestaria_inicial'],2,',','.');  $montot=$montot+$consulta[$i]['num_disponibilidad_presupuestaria_inicial'];
			$montof=number_format($consulta[$i]['num_disponibilidad_financiera_inicial'],2,',','.');  $montofi=$montofi+$consulta[$i]['num_disponibilidad_financiera_inicial'];
			$montoi=number_format($consulta[$i]['num_incremento'],2,',','.');  $montoin=$montoin+$consulta[$i]['num_incremento'];
			$montods=number_format($consulta[$i]['num_disminucion'],2,',','.');  $montodi=$montodi+$consulta[$i]['num_disminucion'];
			$montoa=number_format($consulta[$i]['num_presupuesto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_presupuesto_ajustado'];
			$montoc=number_format($consulta[$i]['num_compromiso'],2,',','.'); $montoct=$montoct+$consulta[$i]['num_compromiso'];
			$montoca=number_format($consulta[$i]['num_causado'],2,',','.'); $montocat=$montocat+$consulta[$i]['num_causado'];
			$montop=number_format($consulta[$i]['num_pagado'],2,',','.'); $montopt=$montopt+$consulta[$i]['num_pagado'];
			$montod=$consulta[$i]['num_presupuesto_ajustado']-$consulta[$i]['num_compromiso']; $montodt=$montodt+$montod;
			$montofn=number_format($consulta[$i]['num_disponibilidad_financiera'],2,',','.'); $montopfnt=$montopfnt+$consulta[$i]['num_disponibilidad_financiera'];
			
			
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(202, 202, 202);
			
				
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			   
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					if ($precierre==1) 
					  $suma=$this->_ReporteModelo-> getReporteMontosTituloPreCierre($validacion['fk_prb004_num_presupuesto'],$partida,$validacion['fec_anio'],$validacion['fec_mes'],false);
					else  
					  $suma=$this->_ReporteModelo-> getReporteMontosTituloCierre($validacion['fk_prb004_num_presupuesto'],$partida,$validacion['fec_anio'],$validacion['fec_mes'],false);
					$suma_presupuestado=$suma['suma_presupuesto_inicial'];
					$suma_financiero=$suma['suma_financiero_inicial'];
					$suma_incremento=$suma['suma_incremento'];
					$suma_disminucion=$suma['suma_disminucion'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_causado=$suma['suma_causado'];
					$suma_pagado=$suma['suma_pagado'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
					$suma_financiera_final=$suma_ajustado-$suma_pagado;
				
				    $pdf->Row(array('',
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_financiero,2,',','.'),number_format($suma_incremento,2,',','.'),
					number_format($suma_disminucion,2,',','.'), number_format($suma_ajustado,2,',','.'),
					number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),
					number_format($suma_disponible,2,',','.'),number_format($suma_financiera_final,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					
					if ($precierre==1) 
				    	$suma=$this->_ReporteModelo-> getReporteMontosTituloPreCierre($validacion['fk_prb004_num_presupuesto'],$partida,$validacion['fec_anio'],$validacion['fec_mes'],false);
				    else
					     $suma=$this->_ReporteModelo-> getReporteMontosTituloCierre($validacion['fk_prb004_num_presupuesto'],$partida,$validacion['fec_anio'],$validacion['fec_mes'],false);
							 
					$suma_presupuestado=$suma['suma_presupuesto_inicial'];
					$suma_financiero=$suma['suma_financiero_inicial'];
					$suma_incremento=$suma['suma_incremento'];
					$suma_disminucion=$suma['suma_disminucion'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_causado=$suma['suma_causado'];
					$suma_pagado=$suma['suma_pagado'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
					$suma_financiera_final=$suma_ajustado-$suma_pagado;

				    $pdf->Row(array('',
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_financiero,2,',','.'),number_format($suma_incremento,2,',','.'),
					number_format($suma_disminucion,2,',','.'), number_format($suma_ajustado,2,',','.'),
					number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),
					number_format($suma_disponible,2,',','.'),number_format($suma_financiera_final,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
			
			}//fin for
			/*FIN TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);
			
            $pdf->Row(array($i+1,
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto,$montof,$montoi,$montods,$montoa,$montoc,$montoca,$montop,number_format($montod,2,',','.'),$montofn));
			 
        } 
	  
	        $montot=number_format($montot,2,',','.'); 
			$montofi=number_format($montofi,2,',','.'); 
			$montoin=number_format($montoin,2,',','.'); 
			$montodi=number_format($montodi,2,',','.'); 
			$montoat=number_format($montoat,2,',','.');
			$montoct=number_format($montoct,2,',','.');
			$montocat=number_format($montocat,2,',','.'); 
			$montopt=number_format($montopt,2,',','.'); 
			$montodt=number_format($montodt,2,',','.');
			$montopfnt=number_format($montopfnt,2,',','.');
			
			$pdf->SetFont('Arial', 'B', 7);
		    $pdf->Row(array('','','TOTALES', $montot,$montofi,$montoin,$montodi,$montoat,$montoct,$montocat,$montopt,$montodt,$montopfnt));
			
	  }//FIN TIPO PRESUPUESTO
	  		
				
        $pdf->Output();

    }



//-------------------------------------------------------


    public function metejecucionReportePdf()
    {

        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formTxt=$this->metObtenerTexto('form','txt');
		
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
		$this->_ReporteModelo = $this->metCargarModelo('reporte');
		$organismo= $this->_ReporteModelo-> metListarOrganismoInt($validacion['fk_a001_num_organismo']);
		
        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabecera','modPR');
        $pdf=new pdfReporteEjecucion('L','mm','Legal');
		$pdf->organismo = $organismo[0]['ind_descripcion_empresa'];
		$pdf->logo = $organismo[0]['ind_logo'];
		$pdf->logo_secundario = $organismo[0]['ind_logo_secundario'];
        $pdf->AliasNbPages();
        $pdf->AddPage();
		$pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
		
        $pdf->SetFont('Arial', 'B', 6);
		$pdf->Cell(25, 5, utf8_decode('COD. PRESUPUESTO:'), 0, 1, 'L', 0);
		$pdf->Cell(25, 5, utf8_decode('TIPO DE PRESUPUESTO:'), 0, 1, 'L', 0);
		$pdf->Cell(25, 5, utf8_decode('CATEGORIA DE PRESUPUESTO:'), 0, 1, 'L', 0);

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
		//$pdf->Cell(10,5);
        $pdf->Cell(25, 5, utf8_decode('COD. PARTIDA'), 1, 0, 'C', 1);
        $pdf->Cell(140, 5, 'DESCRIPCION', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'T. PRESUPUESTADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. AJUSTADOR', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'T. CREDITOS', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. COMPROMETIDO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. CAUSADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. PAGADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. DISPONIBLE', 1, 0, 'C', 1);
       
        $pdf->SetFont('Arial', '', 7);

		
        $consulta= $this->_ReporteModelo-> getReporteEjecucion($validacion['idpresupuesto'],$validacion['fec_anio'],$validacion['fk_a001_num_organismo']);
		$partidas_titulo= $this->_ReporteModelo-> getReportePartidasTitulo();
		$aespecifica = $this->_ReporteModelo->metAespecifica();
		

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(4);

       $montot=0; $montoat=0; $montoct=0; $montocat=0; $montopt=0; $montodt=0;
       $montocrt=0;//monto creditos nuevo 04-08-2017
	   $suma_presupuestado=0;
	   $p=0;
	   
	   $cont=0;
	   
	    //PRESUPUESTO POR PROYECTO 
	  if(count($consulta) && ($validacion['idpresupuesto']!='error' || $validacion['fec_anio']!='error'))  
	  if($consulta[0]['ind_tipo_presupuesto']=='P')
      {
		
	  for($a=0;$a<count($aespecifica);$a++)
	    {

		   $cont=0;
		   $existeAespecifica = count($this->_ReporteModelo->metExisteAespecificaEs2($aespecifica[$a]['pk_num_aespecifica'], $validacion['idpresupuesto']));
		   
		
		   $pdf->SetFont('Arial', 'B', 8);
		   $pdf->SetFillColor(202, 202, 202);  
		   $pdf->SetWidths(array(25,290));
           $pdf->SetAligns(array('C','L'));
		   
		  if ($existeAespecifica>0)
		  {
		     
		      $pdf->Ln(1);
			  $pdf->Cell(10,5);
		      $pdf->Row(array(
                    utf8_decode($aespecifica[$a]['ind_cod_aespecifica']),
                    utf8_decode($aespecifica[$a]['ind_descripcion'])));
			
		   }
	  
        for($i=0; $i< count($consulta); $i++)
        {
			
			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->Cell(25, 5, utf8_decode($consulta[$i]['ind_cod_presupuesto']), 0, 0, 'C', 0);
				$pdf->SetXY(54,46);
				$pdf->Cell(25, 5, number_format($consulta[$i]['monto_aprobado'],2,',','.'), 0, 0, 'C', 0);
				
				
				$pdf->SetXY($x,$y);
			}
			  
		//-------------------------------------------
				$pdf->SetWidths(array(25,140,25,25,25,25,25,25));
				$pdf->SetAligns(array('L','L','R','R','R','R','R','R'));
			   
				/*TITULOS DE PARTIDAS*/
				
				$pdf->SetFont('Arial', 'B', 7);
			    $pdf->SetFillColor(235, 235, 235);
				$pdf->SetWidths(array(25,140,25,25,25,25,25,25));
				$pdf->SetAligns(array('L','L','R','R','R','R','R','R'));
			    /*$pdf->SetWidths(array(25,180,30));
                $pdf->SetAligns(array('C','L','L'));*/
			
		  if ($aespecifica[$a]['pk_num_aespecifica']==$consulta[$i]['fk_pr006_num_aespecifica'])
		  {	
		  
		        $monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');  $montot=$montot+$consulta[$i]['num_monto_aprobado'];
				$montoa=number_format($consulta[$i]['num_monto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_monto_ajustado'];
				$montoc=number_format($consulta[$i]['compromiso_dist'],2,',','.'); $montoct=$montoct+$consulta[$i]['compromiso_dist'];
				$montoca=number_format($consulta[$i]['causado_dist'],2,',','.'); $montocat=$montoct+$consulta[$i]['causado_dist'];
				$montop=number_format($consulta[$i]['pagado_dist'],2,',','.'); $montopt=$montopt+$consulta[$i]['pagado_dist'];
				$montod=$consulta[$i]['num_monto_ajustado']-$consulta[$i]['compromiso_dist']; $montodt=$montodt+$montod;
		       
		  
		  
				for($j=$cont; $j< count($partidas_titulo); $j++)
				{
				   
					$codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
					$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
					$codigo3=substr_replace($consulta[$i]['cod_partida'],'00', 10,2);
					
					
					if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
					
						$partida=substr($consulta[$i]['cod_partida'], 0, 3);
						 
						$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida, $aespecifica[$a]['pk_num_aespecifica']);
						$suma_presupuestado=$suma['suma_aprobado'];
						$suma_ajustado=$suma['suma_ajustado'];
						$suma_compromiso=$suma['suma_compromiso_dist'];
						$suma_causado=$suma['suma_causado_dist'];
						$suma_pagado=$suma['suma_pagado_dist'];
						$suma_disponible=$suma_ajustado-$suma_compromiso;
					
					    $pdf->Cell(10,5);
						$pdf->Row(array(
						utf8_decode($partidas_titulo[$j]['cod_partida']),
						utf8_decode($partidas_titulo[$j]['ind_denominacion']),
						number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),number_format($suma_disponible,2,',','.')));
						$cont=$j+1;  
					}//FIN IF
					
					if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
					  
						$partida=substr($consulta[$i]['cod_partida'], 0, 6);
						
						$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida, $aespecifica[$a]['pk_num_aespecifica']);
						$suma_presupuestado=$suma['suma_aprobado'];
						$suma_ajustado=$suma['suma_ajustado'];
						$suma_compromiso=$suma['suma_compromiso_dist'];
						$suma_causado=$suma['suma_causado_dist'];
						$suma_pagado=$suma['suma_pagado_dist'];
						$suma_disponible=$suma_ajustado-$suma_compromiso;
	 
	                    $pdf->Cell(10,5);
			        	$pdf->Row(array(
						utf8_decode($partidas_titulo[$j]['cod_partida']),
						utf8_decode($partidas_titulo[$j]['ind_denominacion']),
						 number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),number_format($suma_disponible,2,',','.')));
						$cont=$j+1;  
					}//FIN IF
					
			//-----------------------------------------------		
					if (trim($codigo3)==trim($partidas_titulo[$j]['cod_partida'])){
					  
						$partida=substr($consulta[$i]['cod_partida'], 0, 9);
						
						$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida);
						$suma_presupuestado=$suma['suma_aprobado'];
						$suma_ajustado=$suma['suma_ajustado'];
						$suma_compromiso=$suma['suma_compromiso_dist'];
						$suma_causado=$suma['suma_causado_dist'];
						$suma_pagado=$suma['suma_pagado_dist'];
						$suma_disponible=$suma_ajustado-$suma_compromiso;
	
	                    $pdf->Cell(10,5);
						$pdf->Row(array(
						utf8_decode($partidas_titulo[$j]['cod_partida']),
						utf8_decode($partidas_titulo[$j]['ind_denominacion']),
						 number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),number_format($suma_disponible,2,',','.')));
						$cont=$j+1;  
					}//FIN IF
					
					//------------------------------
				
				}//fin for
				/*FIN TITULOS DE PARTIDAS*/
				$pdf->SetFont('Arial', '', 7);
				$pdf->SetFillColor(255, 255, 255);
				
				$pdf->Cell(10,5);
				$pdf->Row(array(
					utf8_decode($consulta[$i]['cod_partida']),
					utf8_decode($consulta[$i]['ind_denominacion']),
					$monto,$montoa,$montoc,$montoca,$montop,number_format($montod,2,',','.')));
			   }//FIN IF ACCION ESPECIFICA	
				 
			 } //FIN for i detalle
			
		  }//FIN IF TIPO PRESUPUESTO POR PROYECTO
		  
		  }//FIN FOR ACCION ESPECIFICA
		  else
		  {
		    for($i=0; $i< count($consulta); $i++)
            {
			
			   //-------------------------
		     if ($i==0)
			{
			    $pdf->SetFont('Arial', 'B', 6);
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->Cell(25, 5, utf8_decode($consulta[$i]['ind_cod_presupuesto']), 0, 0, 'L', 0);
				
				$pdf->SetXY(50,41);
				  if($consulta[$i]['ind_tipo_presupuesto']=='G')
				     $tipo_presupuesto='PROGRAMA';
				  else 	if($consulta[$i]['ind_tipo_presupuesto']=='O')
				     $tipo_presupuesto='PROYECTO';
					 
				$pdf->Cell(25, 5, utf8_decode($tipo_presupuesto), 0, 0, 'L', 0);
				
				$pdf->SetXY(50,45);
				  if($consulta[$i]['ind_tipo_categoria']=='G')
				     $tipo_presupuesto='PROGRAMA';
				  else 	if($consulta[$i]['ind_tipo_categoria']=='O')
				     $tipo_presupuesto='PROYECTO';
				  else 	if($consulta[$i]['ind_tipo_categoria']=='C')
				     $tipo_presupuesto='ACCION CENTRALIZADA';	 
				  	 
					 
				$pdf->Cell(25, 5, utf8_decode($tipo_presupuesto), 0, 0, 'L', 0);
				
				
				$pdf->SetXY($x,$y);
			}
		    //-------------------------
			
				$pdf->SetWidths(array(25,140,25,25,25,25,25,25,25));
				$pdf->SetAligns(array('L','L','R','R','R','R','R','R','R'));
			   
				$monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');  $montot=$montot+$consulta[$i]['num_monto_aprobado'];
				$montoa=number_format($consulta[$i]['num_monto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_monto_ajustado'];
				$montoc=number_format($consulta[$i]['compromiso_dist'],2,',','.'); $montoct=$montoct+$consulta[$i]['compromiso_dist'];
				$montoca=number_format($consulta[$i]['causado_dist'],2,',','.'); $montocat=$montoct+$consulta[$i]['causado_dist'];
				$montop=number_format($consulta[$i]['pagado_dist'],2,',','.'); $montopt=$montopt+$consulta[$i]['pagado_dist'];
                $montocr=number_format($consulta[$i]['num_monto_credito'],2,',','.');  $montocrt=$montocrt+$consulta[$i]['num_monto_credito'];//nuevo 04-08-2017
				$montod=$consulta[$i]['num_monto_ajustado']-$consulta[$i]['compromiso_dist']; $montodt=$montodt+$montod;
				
			
				/*TITULOS DE PARTIDAS*/
				$pdf->SetFont('Arial', 'B', 7);
				$pdf->SetFillColor(202, 202, 202);
				
				
				for($j=$cont; $j< count($partidas_titulo); $j++)
				{
				   
					$codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
					$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
					$codigo3=substr_replace($consulta[$i]['cod_partida'],'00', 10,2);
					
					if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
					
						$partida=substr($consulta[$i]['cod_partida'], 0, 3);
						
						 
						$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida);
						$suma_presupuestado=$suma['suma_aprobado'];
						$suma_ajustado=$suma['suma_ajustado'];
						$suma_compromiso=$suma['suma_compromiso_dist'];
						$suma_causado=$suma['suma_causado_dist'];
						$suma_pagado=$suma['suma_pagado_dist'];
						$suma_credito = $suma['suma_credito'];
						$suma_disponible=$suma_ajustado-$suma_compromiso;
					
					    //$pdf->Cell(10,5);
						$pdf->Row(array(
						utf8_decode($partidas_titulo[$j]['cod_partida']),
						utf8_decode($partidas_titulo[$j]['ind_denominacion']),
						number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_credito,2,',','.'),number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),number_format($suma_disponible,2,',','.')));
						$cont=$j+1;  
					}//FIN IF
					
					if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
					  
						$partida=substr($consulta[$i]['cod_partida'], 0, 6);
						//var_dump($partida);
						
						$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida);
						$suma_presupuestado=$suma['suma_aprobado'];
						$suma_ajustado=$suma['suma_ajustado'];
						$suma_compromiso=$suma['suma_compromiso_dist'];
						$suma_causado=$suma['suma_causado_dist'];
						$suma_pagado=$suma['suma_pagado_dist'];
                        $suma_credito = $suma['suma_credito'];
						$suma_disponible=$suma_ajustado-$suma_compromiso;
	                     
						//$pdf->Cell(10,5);
						$pdf->Row(array(
						utf8_decode($partidas_titulo[$j]['cod_partida']),
						utf8_decode($partidas_titulo[$j]['ind_denominacion']),
						 number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_credito,2,',','.'),number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),number_format($suma_disponible,2,',','.')));
						$cont=$j+1;  
					}//FIN IF
					
			//-----------------------------------------------		
					if (trim($codigo3)==trim($partidas_titulo[$j]['cod_partida'])){
					  
						$partida=substr($consulta[$i]['cod_partida'], 0, 9);
						
						$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida);
						$suma_presupuestado=$suma['suma_aprobado'];
						$suma_ajustado=$suma['suma_ajustado'];
						$suma_compromiso=$suma['suma_compromiso_dist'];
						$suma_causado=$suma['suma_causado_dist'];
						$suma_pagado=$suma['suma_pagado_dist'];
                        $suma_credito = $suma['suma_credito'];
						$suma_disponible=$suma_ajustado-$suma_compromiso;
	                   
					    //$pdf->Cell(10,5);
						$pdf->Row(array(
						utf8_decode($partidas_titulo[$j]['cod_partida']),
						utf8_decode($partidas_titulo[$j]['ind_denominacion']),
						 number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_credito,2,',','.'),number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),number_format($suma_disponible,2,',','.')));
						$cont=$j+1;  
					}//FIN IF
					
					//------------------------------
				
				}//fin for
				/*FIN TITULOS DE PARTIDAS*/
				$pdf->SetFont('Arial', '', 7);
				$pdf->SetFillColor(255, 255, 255);
				
				//$pdf->Cell(10,5);
				$pdf->Row(array(
					utf8_decode($consulta[$i]['cod_partida']),
					utf8_decode($consulta[$i]['ind_denominacion']),
					$monto,$montoa,$montocr,$montoc,$montoca,$montop,number_format($montod,2,',','.')));
				 
			 } //FIN for i detalle
		    
		  
		  }//FIN TIPO DE PROYECTO
			
		    $montot=number_format($montot,2,',','.'); $montoat=number_format($montoat,2,',','.');$montoct=number_format($montoct,2,',','.');
			$montocat=number_format($montocat,2,',','.'); $montopt=number_format($montopt,2,',','.'); $montodt=number_format($montodt,2,',','.');
            $montocrt=number_format($montocrt,2,',','.');
			
			$pdf->SetFont('Arial', 'B', 7);
			//$pdf->Cell(10,5);
		    $pdf->Row(array('','', $montot,$montoat,$montocrt,$montoct,$montocat,$montopt,$montodt));
				
        $pdf->Output();

    }
	
  

//-------------------------------------------------------


    public function metejecucionDetalladaReportePdf()
    {

        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formTxt=$this->metObtenerTexto('form','txt');
		
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
		
		$this->_ReporteModelo = $this->metCargarModelo('reporte');
		$organismo= $this->_ReporteModelo-> metListarOrganismoInt($validacion['fk_a001_num_organismo']);
		
        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabecera','modPR');
        $pdf=new pdfReporteEjecucionDetallada('L','mm',Array(215.9 , 445));
		$pdf->organismo = $organismo[0]['ind_descripcion_empresa'];
		$pdf->logo = $organismo[0]['ind_logo'];
		$pdf->logo_secundario = $organismo[0]['ind_logo_secundario'];
        $pdf->AliasNbPages();
        $pdf->AddPage();
		//$pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
        //$pdf->SetFont('Arial', 'B', 7);
		
        //$pdf->Cell(25, 5, utf8_decode('COD. PRESUPUESTO:'), 1, 1, 'L', 0);
        //$pdf->Cell(50, 5, 'DESCRIPCION', 1, 1, 'L', 0);
        //$pdf->Cell(30, 5, 'MONTO APROBADO', 1, 1, 'L', 0);

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
		
		$pdf->Cell(105, 5, '', 1, 0, 'C', 1);
		$pdf->Cell(50, 5, 'ASIGNADO', 1, 0, 'C', 1);
		$pdf->Cell(75, 5, 'EJECUTADO', 1, 0, 'C', 1);
		$pdf->Cell(75, 5, '% EJECUTADO', 1, 0, 'C', 1);
		$pdf->Cell(50, 5, 'DISPONIBLE', 1, 0, 'C', 1);
		$pdf->Cell(50, 5, '% DISPONIBLE', 1, 0, 'C', 1);
		
		$pdf->Ln(4);
        $pdf->Cell(25, 5, utf8_decode('COD. PARTIDA'), 1, 0, 'C', 1);
        $pdf->Cell(80, 5, 'DESCRIPCION', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'T. PRESUPUESTADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. AJUSTADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. COMPROMETIDO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. CAUSADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. PAGADO', 1, 0, 'C', 1);
		
		$pdf->Cell(25, 5, '% COMPROMETIDO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, '% CAUSADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, '%  PAGADO', 1, 0, 'C', 1);
		
		
		$pdf->Cell(25, 5, 'T. DISPONIBLE P.', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. DISPONIBLE F.', 1, 0, 'C', 1);
		
		$pdf->Cell(25, 5, '% DISPONIBLE P.', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, '% DISPONIBLE F.', 1, 0, 'C', 1);
		
        $pdf->SetFont('Arial', '', 7);

		
        $consulta= $this->_ReporteModelo-> getReporteEjecucion($validacion['idpresupuesto'],$validacion['fec_anio'],$validacion['fk_a001_num_organismo']);
		$partidas_titulo= $this->_ReporteModelo-> getReportePartidasTitulo();
		$aespecifica = $this->_ReporteModelo->metAespecifica();
		

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(4);

       $montot=0; $montoat=0; $montoct=0; $montocat=0; $montopt=0; $montodt=0; $montodf=0; $montodft=0;
	   $suma_presupuestado=0;
	   $p=0;
	   
	   $cont=0;
	   
	    //PRESUPUESTO POR PROYECTO 
	  if(count($consulta) && ($validacion['idpresupuesto']!='error' || $validacion['fec_anio']!='error'))  
	  if($consulta[0]['ind_tipo_presupuesto']=='P')
      {
		
	  for($a=0;$a<count($aespecifica);$a++)
	    {

		   $cont=0;
		   $existeAespecifica = count($this->_ReporteModelo->metExisteAespecificaEs2($aespecifica[$a]['pk_num_aespecifica'], $validacion['idpresupuesto']));
		   
		
		   $pdf->SetFont('Arial', 'B', 8);
		   $pdf->SetFillColor(202, 202, 202);  
		   $pdf->SetWidths(array(25,290));
           $pdf->SetAligns(array('C','L'));
		   
		   
		  if ($existeAespecifica>0)
		  {
		     
		      $pdf->Ln(1);
			
		      $pdf->Row(array(
                    utf8_decode($aespecifica[$a]['ind_cod_aespecifica']),
                    utf8_decode($aespecifica[$a]['ind_descripcion'])));
			
		   }
	  
        for($i=0; $i< count($consulta); $i++)
        {
			
			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->Cell(25, 5, utf8_decode($consulta[$i]['ind_cod_presupuesto']), 0, 0, 'C', 0);
				$pdf->SetXY(54,46);
				$pdf->Cell(25, 5, number_format($consulta[$i]['monto_aprobado'],2,',','.'), 0, 0, 'C', 0);
				
				
				$pdf->SetXY($x,$y);
			}
		  
		//-------------------------------------------
				$pdf->SetWidths(array(25,80,25,25,25,25,25,25,25,25,25,25,25,25));
				$pdf->SetAligns(array('L','L','R','R','R','R','R','R','R','R','R','R','R','R'));
			   
			
				/*TITULOS DE PARTIDAS*/
				
				$pdf->SetFont('Arial', 'B', 7);
			    $pdf->SetFillColor(235, 235, 235);
				$pdf->SetWidths(array(25,80,25,25,25,25,25,25,25,25,25,25,25,25));
				$pdf->SetAligns(array('L','L','R','R','R','R','R','R','R','R','R','R','R','R'));
			    /*$pdf->SetWidths(array(25,180,30));
                $pdf->SetAligns(array('C','L','L'));*/
			
		  if ($aespecifica[$a]['pk_num_aespecifica']==$consulta[$i]['fk_pr006_num_aespecifica'])
		  {	
		  
		        $monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');  $montot=$montot+$consulta[$i]['num_monto_aprobado'];
				$montoa=number_format($consulta[$i]['num_monto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_monto_ajustado'];
				$montoc=number_format($consulta[$i]['compromiso_dist'],2,',','.'); $montoct=$montoct+$consulta[$i]['compromiso_dist'];
				$montoca=number_format($consulta[$i]['causado_dist'],2,',','.'); $montocat=$montoct+$consulta[$i]['causado_dist'];
				$montop=number_format($consulta[$i]['pagado_dist'],2,',','.'); $montopt=$montopt+$consulta[$i]['pagado_dist'];
				$montod=$consulta[$i]['num_monto_ajustado']-$consulta[$i]['compromiso_dist']; $montodt=$montodt+$montod;
		       
		  
		  
				for($j=$cont; $j< count($partidas_titulo); $j++)
				{
				   
					$codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
					$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
					$codigo3=substr_replace($consulta[$i]['cod_partida'],'00', 10,2);
					
					
					if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
					
						$partida=substr($consulta[$i]['cod_partida'], 0, 3);
						
						$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida, $aespecifica[$a]['pk_num_aespecifica']);
						$suma_presupuestado=$suma['suma_aprobado'];
						$suma_ajustado=$suma['suma_ajustado'];
						$suma_compromiso=$suma['suma_compromiso_dist'];
						$suma_causado=$suma['suma_causado_dist'];
						$suma_pagado=$suma['suma_pagado_dist'];
						$suma_disponible=$suma_ajustado-$suma_compromiso;
					
					    $pdf->Cell(10,5);
						$pdf->Row(array(
						utf8_decode($partidas_titulo[$j]['cod_partida']),
						utf8_decode($partidas_titulo[$j]['ind_denominacion']),
						number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),number_format($suma_disponible,2,',','.')));
						$cont=$j+1;  
					}//FIN IF
					
					if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
					  
						$partida=substr($consulta[$i]['cod_partida'], 0, 6);
						
						$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida, $aespecifica[$a]['pk_num_aespecifica']);
						$suma_presupuestado=$suma['suma_aprobado'];
						$suma_ajustado=$suma['suma_ajustado'];
						$suma_compromiso=$suma['suma_compromiso_dist'];
						$suma_causado=$suma['suma_causado_dist'];
						$suma_pagado=$suma['suma_pagado_dist'];
						$suma_disponible=$suma_ajustado-$suma_compromiso;
	 
	                    $pdf->Cell(10,5);
			        	$pdf->Row(array(
						utf8_decode($partidas_titulo[$j]['cod_partida']),
						utf8_decode($partidas_titulo[$j]['ind_denominacion']),
						 number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),number_format($suma_disponible,2,',','.')));
						$cont=$j+1;  
					}//FIN IF
					
			//-----------------------------------------------		
					if (trim($codigo3)==trim($partidas_titulo[$j]['cod_partida'])){
					  
						$partida=substr($consulta[$i]['cod_partida'], 0, 9);
						
						$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida);
						$suma_presupuestado=$suma['suma_aprobado'];
						$suma_ajustado=$suma['suma_ajustado'];
						$suma_compromiso=$suma['suma_compromiso_dist'];
						$suma_causado=$suma['suma_causado_dist'];
						$suma_pagado=$suma['suma_pagado_dist'];
						$suma_disponible=$suma_ajustado-$suma_compromiso;
	
						$pdf->Row(array(
						utf8_decode($partidas_titulo[$j]['cod_partida']),
						utf8_decode($partidas_titulo[$j]['ind_denominacion']),
						 number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),number_format($suma_disponible,2,',','.')));
						$cont=$j+1;  
					}//FIN IF
					
					//------------------------------
				
				}//fin for
				/*FIN TITULOS DE PARTIDAS*/
				$pdf->SetFont('Arial', '', 7);
				$pdf->SetFillColor(255, 255, 255);
				
				
				$pdf->Row(array(
					utf8_decode($consulta[$i]['cod_partida']),
					utf8_decode($consulta[$i]['ind_denominacion']),
					$monto,$montoa,$montoc,$montoca,$montop,number_format($montod,2,',','.')));
			   }//FIN IF ACCION ESPECIFICA	
				 
			 } //FIN for i detalle
			
		  }//FIN IF TIPO PRESUPUESTO POR PROYECTO
		  
		  }//FIN FOR ACCION ESPECIFICA
		  else
		  {
		    for($i=0; $i< count($consulta); $i++)
            {
			
			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->Cell(25, 5, utf8_decode($consulta[$i]['ind_cod_presupuesto']), 0, 0, 'C', 0);
				$pdf->SetXY(54,46);
				$pdf->Cell(25, 5, number_format($consulta[$i]['monto_aprobado'],2,',','.'), 0, 0, 'C', 0);
				
				
				$pdf->SetXY($x,$y);
			}
			
				$pdf->SetWidths(array(25,80,25,25,25,25,25,25,25,25,25,25,25,25));
				$pdf->SetAligns(array('L','L','R','R','R','R','R','R','R','R','R','R','R','R'));
			   
				$monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');
				$montot=$montot+$consulta[$i]['num_monto_aprobado'];

				$montoa=number_format($consulta[$i]['num_monto_ajustado'],2,',','.');
				$montoat=$montoat+$consulta[$i]['num_monto_ajustado'];

				$montoc=number_format($consulta[$i]['compromiso_dist'],2,',','.');
				$montoct=$montoct+$consulta[$i]['compromiso_dist'];

				$montoca=number_format($consulta[$i]['causado_dist'],2,',','.');
				$montocat=$montocat+$consulta[$i]['causado_dist'];

				$montop=number_format($consulta[$i]['pagado_dist'],2,',','.');
				$montopt=$montopt+$consulta[$i]['pagado_dist'];

				$montod=$consulta[$i]['num_monto_ajustado']-$consulta[$i]['compromiso_dist'];
				$montodt=$montodt+$montod;

				$montodf=$consulta[$i]['num_monto_ajustado']-$consulta[$i]['pagado_dist'];
				$montodft=$montodft+$montodf;
			
				/*TITULOS DE PARTIDAS*/
				$pdf->SetFont('Arial', 'B', 7);
				$pdf->SetFillColor(202, 202, 202);
				
				
				for($j=$cont; $j< count($partidas_titulo); $j++)
				{
				   
					$codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
					$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
					
					$codigo3=substr_replace($consulta[$i]['cod_partida'],'00', 10,2);
					
					if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
					
						$partida=substr($consulta[$i]['cod_partida'], 0, 3);
						
						 
						$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida);
						$suma_presupuestado=$suma['suma_aprobado'];
						$suma_ajustado=$suma['suma_ajustado'];
						if ($suma_ajustado<=0)
						   $suma_ajustado=1;
						$suma_compromiso=$suma['suma_compromiso_dist'];
						$suma_causado=$suma['suma_causado_dist'];
						$suma_pagado=$suma['suma_pagado_dist'];
						$suma_disponible=$suma_ajustado-$suma_compromiso;
						$suma_disponible_f=$suma_ajustado-$suma_pagado;
						
						
						$porcsuma_compromiso=($suma_compromiso/$suma_ajustado)*100;
						$porcsuma_compromiso=number_format($porcsuma_compromiso,2,',','.')." %";
						
						$porcsuma_causado=($suma_causado/$suma_ajustado)*100;
						$porcsuma_causado=number_format($porcsuma_causado,2,',','.')." %";
						
						$porcsuma_pagado=($suma_pagado/$suma_ajustado)*100;
						$porcsuma_pagado=number_format($porcsuma_pagado,2,',','.')." %";
						
						$porcsuma_disponible=($suma_disponible/$suma_ajustado)*100;
						$porcsuma_disponible=number_format($porcsuma_disponible,2,',','.')." %";
						
						$porcsuma_disponible_f=($suma_disponible_f/$suma_ajustado)*100;
						$porcsuma_disponible_f=number_format($porcsuma_disponible_f,2,',','.')." %";
					
						$pdf->Row(array(
						utf8_decode($partidas_titulo[$j]['cod_partida']),
						utf8_decode($partidas_titulo[$j]['ind_denominacion']),
						number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),$porcsuma_compromiso,$porcsuma_causado,$porcsuma_pagado,number_format($suma_disponible,2,',','.'),number_format($suma_disponible_f,2,',','.'),$porcsuma_disponible,$porcsuma_disponible_f));
						$cont=$j+1;  
					}//FIN IF
					
					if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
					  
						$partida=substr($consulta[$i]['cod_partida'], 0, 6);
						//var_dump($partida);
						
						$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida);
						$suma_presupuestado=$suma['suma_aprobado'];
						$suma_ajustado=$suma['suma_ajustado'];
						if ($suma_ajustado<=0)
						   $suma_ajustado=1;
						$suma_compromiso=$suma['suma_compromiso_dist'];
						$suma_causado=$suma['suma_causado_dist'];
						$suma_pagado=$suma['suma_pagado_dist'];
						$suma_disponible=$suma_ajustado-$suma_compromiso;
						$suma_disponible_f=$suma_ajustado-$suma_pagado;
						
						$porcsuma_compromiso=($suma_compromiso/$suma_ajustado)*100;
						$porcsuma_compromiso=number_format($porcsuma_compromiso,2,',','.')." %";
						
						$porcsuma_causado=($suma_causado/$suma_ajustado)*100;
						$porcsuma_causado=number_format($porcsuma_causado,2,',','.')." %";
						
						$porcsuma_pagado=($suma_pagado/$suma_ajustado)*100;
						$porcsuma_pagado=number_format($porcsuma_pagado,2,',','.')." %";
						
						$porcsuma_disponible=($suma_disponible/$suma_ajustado)*100;
						$porcsuma_disponible=number_format($porcsuma_disponible,2,',','.')." %";
						
						$porcsuma_disponible_f=($suma_disponible_f/$suma_ajustado)*100;
						$porcsuma_disponible_f=number_format($porcsuma_disponible_f,2,',','.')." %";
	                     
						$pdf->Row(array(
						utf8_decode($partidas_titulo[$j]['cod_partida']),
						utf8_decode($partidas_titulo[$j]['ind_denominacion']),
						 number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),$porcsuma_compromiso,$porcsuma_causado,$porcsuma_pagado,number_format($suma_disponible,2,',','.'),number_format($suma_disponible_f,2,',','.'),$porcsuma_disponible, $porcsuma_disponible_f));
						$cont=$j+1;  
					}//FIN IF
					
			//-----------------------------------------------		
					if (trim($codigo3)==trim($partidas_titulo[$j]['cod_partida'])){
					  
						$partida=substr($consulta[$i]['cod_partida'], 0, 9);
						
						$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida);
						$suma_presupuestado=$suma['suma_aprobado'];
						$suma_ajustado=$suma['suma_ajustado'];
						if ($suma_ajustado<=0)
						   $suma_ajustado=1;
						$suma_compromiso=$suma['suma_compromiso_dist'];
						$suma_causado=$suma['suma_causado_dist'];
						$suma_pagado=$suma['suma_pagado_dist'];
						$suma_disponible=$suma_ajustado-$suma_compromiso;
						$suma_disponible_f=$suma_ajustado-$suma_pagado;
						
						$porcsuma_compromiso=($suma_compromiso/$suma_ajustado)*100;
						$porcsuma_compromiso=number_format($porcsuma_compromiso,2,',','.')." %";
						
						$porcsuma_causado=($suma_causado/$suma_ajustado)*100;
						$porcsuma_causado=number_format($porcsuma_causado,2,',','.')." %";
						
						$porcsuma_pagado=($suma_pagado/$suma_ajustado)*100;
						$porcsuma_pagado=number_format($porcsuma_pagado,2,',','.')." %";
						
						$porcsuma_disponible=($suma_disponible/$suma_ajustado)*100;
						$porcsuma_disponible=number_format($porcsuma_disponible,2,',','.')." %";
						
						$porcsuma_disponible_f=($suma_disponible_f/$suma_ajustado)*100;
						$porcsuma_disponible_f=number_format($porcsuma_disponible_f,2,',','.')." %";
					
	                   
					   
						$pdf->Row(array(
						utf8_decode($partidas_titulo[$j]['cod_partida']),
						utf8_decode($partidas_titulo[$j]['ind_denominacion']),
						 number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_compromiso,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_pagado,2,',','.'),$porcsuma_compromiso,$porcsuma_causado,$porcsuma_pagado,number_format($suma_disponible,2,',','.'),number_format($suma_disponible_f,2,',','.'),$porcsuma_disponible, $porcsuma_disponible_f));
						$cont=$j+1;  
					}//FIN IF
					
					//------------------------------
				
				}//fin for
				/*FIN TITULOS DE PARTIDAS*/
				$pdf->SetFont('Arial', '', 7);
				$pdf->SetFillColor(255, 255, 255);
				
				$num_monto_ajustado=$consulta[$i]['num_monto_ajustado'];
				if ($num_monto_ajustado<=0)
				    $num_monto_ajustado=1;
				
				$porcmontoc = ($consulta[$i]['compromiso_dist']/$num_monto_ajustado)*100;
				$porcmontoc =  number_format($porcmontoc,2,',','.')." %";
				
				$porcmontoca = ($consulta[$i]['causado_dist']/$num_monto_ajustado)*100;
				$porcmontoca =  number_format($porcmontoca,2,',','.')." %";

				$porcmontop = ($consulta[$i]['pagado_dist']/$num_monto_ajustado)*100;
				$porcmontop =  number_format($porcmontop,2,',','.')." %";
				
				$porcmontod = ($montod/$num_monto_ajustado)*100;
				$porcmontod =  number_format($porcmontod,2,',','.')." %";
				
				$porcmontodf = ($montodf/$num_monto_ajustado)*100;
				$porcmontodf =  number_format($porcmontodf,2,',','.')." %";
				
				
				$pdf->Row(array(
					utf8_decode($consulta[$i]['cod_partida']),
					utf8_decode($consulta[$i]['ind_denominacion']),
					$monto,$montoa,$montoc,$montoca,$montop, $porcmontoc,$porcmontoca,$porcmontop,number_format($montod,2,',','.'),number_format($montodf,2,',','.'),$porcmontod,$porcmontodf));
				 
			 } //FIN for i detalle
		    
		  
		  }//FIN TIPO DE PROYECTO
		  
		  
		  	$porcmontoct = ($montoct/$montoat)*100;
			$porcmontoct =  number_format($porcmontoct,2,',','.')." %";
				
			$porcmontocat = ($montocat/$montoat)*100;
			$porcmontocat =  number_format($porcmontocat,2,',','.')." %";

			$porcmontopt = ($montopt/$montoat)*100;
			$porcmontopt =  number_format($porcmontopt,2,',','.')." %";
			
			$porcmontodt = ($montodt/$montoat)*100;
			$porcmontodt =  number_format($porcmontodt,2,',','.')." %";
			
			$porcmontodft = ($montodft/$montoat)*100;
			$porcmontodft =  number_format($porcmontodft,2,',','.')." %";
			
		    $montot=number_format($montot,2,',','.');
		    $montoat=number_format($montoat,2,',','.');
		    $montoct=number_format($montoct,2,',','.');
			$montocat=number_format($montocat,2,',','.');
			$montopt=number_format($montopt,2,',','.');
			$montodt=number_format($montodt,2,',','.');
			$montodft=number_format($montodft,2,',','.'); 
				
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(202, 202, 202);
			
		    $pdf->Row(array('','TOTALES', $montot,$montoat,$montoct,$montocat,$montopt,$porcmontoct,$porcmontocat,$porcmontopt,$montodt,$montodft,$porcmontodt,$porcmontodft));

        //
            $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->Cell(100,10,'',0,1,'L');
            $pdf->SetWidths(array(90, 90,90));
            $pdf->SetAligns(array('L','L','L'));
            $pdf->Row(array('ELABORADO POR:','APROBADO POR:','REVISADO POR:'),0);
            $pdf->Ln(5);
            $pdf->Row(array(utf8_decode ('Lcdo. Manuel Maita'),utf8_decode ('Lcda. Sorielma Salmerón'),'Lcda. Rena Salas'));
            $pdf->Row(array(utf8_decode ('JEFE DE PRESUPUESTO Y CONTABILIDAD (E)'),utf8_decode ('DIRECTORA DE ADMINISTRACIÓN Y PRESUPUESTO'),'DIRECTORA GENERAL'));

            $pdf->Output();

    }
  
	//------------------------------------------------------------------------------------------



    public function metejecucionxpartidaReportePdf()
    {
        $formInt=$this->metObtenerInt('form','int');

        $formTxt=$this->metObtenerTexto('form','txt');

        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
		$this->_ReporteModelo = $this->metCargarModelo('reporte');
		$organismo= $this->_ReporteModelo-> metListarOrganismoInt($validacion['fk_a001_num_organismo']);
		

        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabecera','modPR');
        $pdf=new pdfReporteEjecucion('L','mm','Legal');
		$pdf->organismo = $organismo[0]['ind_descripcion_empresa'];
		$pdf->logo = $organismo[0]['ind_logo'];
		$pdf->logo_secundario = $organismo[0]['ind_logo_secundario'];
        $pdf->AliasNbPages();
        $pdf->AddPage();
		
		$pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        
        $pdf->Cell(25, 5, utf8_decode('COD. PRESUPUESTO:'), 1, 1, 'L', 0);
        //$pdf->Cell(50, 5, 'DESCRIPCION', 1, 1, 'L', 0);
        //$pdf->Cell(30, 5, 'MONTO APROBADO', 1, 1, 'L', 0);

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
         $pdf->Cell(25, 5, utf8_decode('COD. PARTIDA'), 1, 0, 'C', 1);
        $pdf->Cell(140, 5, 'DESCRIPCION', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'T. PRESUPUESTADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. AJUSTADOR', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. COMPROMETIDO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. CAUSADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. PAGADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. DISPONIBLE', 1, 0, 'C', 1);
       
        $pdf->SetFont('Arial', '', 7);

        $this->_ReporteModelo = $this->metCargarModelo('reporte');
		
        $consulta= $this->_ReporteModelo-> getReporteEjecucionxPartida($validacion['idpresupuesto'],$validacion['fec_anio'], $validacion['cod_partida'],$validacion['fk_a001_num_organismo']);
		
		$partidas_titulo= $this->_ReporteModelo-> getReportePartidasTitulo();
		
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(4);

       $montot=0; $montoat=0; $montoct=0; $montocat=0; $montopt=0; $montodt=0;
	   $suma_presupuestado=0;
	   $p=0;
	   
	   $cont=0;
       for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {
		
			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->Cell(25, 5, utf8_decode($consulta[$i]['ind_cod_presupuesto']), 0, 0, 'C', 0);
				
				$pdf->SetXY($x,$y);
			}
			
            $pdf->SetWidths(array(25,140,25,25,25,25,25,25));
            $pdf->SetAligns(array('L','L','R','R','R','R','R','R'));

            $monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');  $montot=$montot+$consulta[$i]['num_monto_aprobado'];
			$montoa=number_format($consulta[$i]['num_monto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_monto_ajustado'];

			$montoc=number_format($consulta[$i]['compromiso_dist'],2,',','.'); $montoct=$montoct+$consulta[$i]['compromiso_dist'];
			$montoca=number_format($consulta[$i]['causado_dist'],2,',','.'); $montocat=$montoct+$consulta[$i]['causado_dist'];
			$montop=number_format($consulta[$i]['pagado_dist'],2,',','.'); $montopt=$montopt+$consulta[$i]['pagado_dist'];
			$montod=$consulta[$i]['num_monto_ajustado']-$consulta[$i]['compromiso_dist']; $montodt=$montodt+$montod;
			
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(202, 202, 202);
		
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);
			
			
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto,$montoa,$montoc,$montoca,$montop,number_format($montod,2,',','.')));
			 
		   } 
		
		    $pdf->SetFont('Arial', 'B', 8);
			$pdf->SetFillColor(202, 202, 202);
		
		    $montot=number_format($montot,2,',','.'); $montoat=number_format($montoat,2,',','.');$montoct=number_format($montoct,2,',','.');
			$montocat=number_format($montocat,2,',','.'); $montopt=number_format($montopt,2,',','.'); $montodt=number_format($montodt,2,',','.');
			
		    $pdf->Row(array('','TOTALES', $montot,$montoat,$montoct,$montocat,$montopt,$montodt));
				
        $pdf->Output();

    }

  
  
   public function metcompromisoReportePdf()
    {

        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formTxt=$this->metObtenerTexto('form','txt');

        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
		$this->_ReporteModelo = $this->metCargarModelo('reporte');
		$organismo= $this->_ReporteModelo-> metListarOrganismoInt($validacion['fk_a001_num_organismo']);

        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabecera','modPR');
        $pdf=new pdfReporteCompromiso('L','mm','Legal');
		$pdf->organismo = $organismo[0]['ind_descripcion_empresa'];
		$pdf->logo = $organismo[0]['ind_logo'];
		$pdf->logo_secundario = $organismo[0]['ind_logo_secundario'];
		
        $pdf->AliasNbPages();
        $pdf->AddPage();
	
		$pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
       
	    $pdf->Cell(10,5);
        $pdf->Cell(25, 5, utf8_decode('COD. PRESUPUESTO:'), 1, 1, 'L', 0);
		//$pdf->Cell(50, 5, 'PROYECTO:', 1, 1, 'L', 0);
		//$pdf->Cell(50, 5, 'ACCION ESPECIFICA:', 1, 1, 'L', 0);

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(10,5); 
        $pdf->Cell(25, 5, utf8_decode('COD. PARTIDA'), 1, 0, 'C', 1);
        $pdf->Cell(180, 5, 'DESCRIPCION', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'T. PRESUPUESTADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. AJUSTADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. COMPROMETIDO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. DISPONIBLE', 1, 0, 'C', 1);
		
		
       
        $pdf->SetFont('Arial', '', 7);

        
		
        $consulta= $this->_ReporteModelo-> getReporteCompromiso($validacion['idpresupuesto'],$validacion['fec_anio'],$validacion['fk_a001_num_organismo']);
		$partidas_titulo= $this->_ReporteModelo-> getReportePartidasTitulo();
		$aespecifica = $this->_ReporteModelo->metAespecifica();
		

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(4);

       $montot=0; $montoat=0; $montoct=0; $montocat=0; $montopt=0; $montodt=0;
	   $suma_presupuestado=0;
	   $p=0;
	   
	 if (count($consulta)>0)
	 if($consulta[0]['ind_tipo_presupuesto']=='P')
     {
	  
	   for($a=0;$a<count($aespecifica);$a++)
	    {
		 $pdf->Ln(1);
		   $cont=0;
		   $existeAespecifica = count($this->_ReporteModelo->metExisteAespecificaEs2($aespecifica[$a]['pk_num_aespecifica'], $validacion['idpresupuesto']));
		   
		   $pdf->SetFont('Arial', 'B', 8);
		   $pdf->SetFillColor(202, 202, 202);
		   
		   $pdf->SetWidths(array(25,280));
           $pdf->SetAligns(array('C','L'));
		   
		   
		   if ($existeAespecifica>0)
		   {
		      $pdf->Cell(10,5); 
		      $pdf->Row(array(
                    utf8_decode($aespecifica[$a]['ind_cod_aespecifica']),
                    utf8_decode($aespecifica[$a]['ind_descripcion'])
                    ));		
		   }
		   
	 
	   
	   $cont=0;
       for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {
			
			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->Cell(25, 5, utf8_decode($consulta[$i]['ind_cod_presupuesto']), 0, 0, 'C', 0);
				
				$pdf->SetXY($x,$y);
			}
			
            $pdf->SetWidths(array(25,180,25,25,25,25,25,25));
            $pdf->SetAligns(array('C','L','R','R','R','R','R','R','R'));
            
            $monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');  $montot=$montot+$consulta[$i]['num_monto_aprobado'];
			$montoa=number_format($consulta[$i]['num_monto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_monto_ajustado'];
			$montoc=number_format($consulta[$i]['compromiso_dist'],2,',','.'); $montoct=$montoct+$consulta[$i]['compromiso_dist'];
			$montod=$consulta[$i]['num_monto_ajustado']-$consulta[$i]['compromiso_dist']; $montodt=$montodt+$montod;
			
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(235, 235, 235);
			
		 if ($aespecifica[$a]['pk_num_aespecifica']==$consulta[$i]['fk_pr006_num_aespecifica'])
		 {
			
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			   
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida,$aespecifica[$a]['pk_num_aespecifica']);
					$suma_presupuestado=$suma['suma_aprobado'];
					$suma_ajustado=$suma['suma_ajustado'];

					$suma_compromiso=$suma['suma_compromiso_dist'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
				
				    $pdf->Cell(10,5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_compromiso,2,',','.'),number_format($suma_disponible,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida,$aespecifica[$a]['pk_num_aespecifica']);
					$suma_presupuestado=$suma['suma_aprobado'];
					$suma_ajustado=$suma['suma_ajustado'];

					$suma_compromiso=$suma['suma_compromiso_dist'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;

                    $pdf->Cell(10,5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),

					 number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_compromiso,2,',','.'),number_format($suma_disponible,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
			
			}//fin for
			
			/*FIN TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);
			
			
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto,$montoa,$montoc,number_format($montod,2,',','.')));
			 
        }
		
		}//FIN IF IF AESPECIFICA
	   }//FIN FOR AESPECIFICA	
	   
	   
	  }else{//ELSE TIPO PRESUPUESTO
	  
	  $cont=0;
       for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {
			
			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->Cell(25, 5, utf8_decode($consulta[$i]['ind_cod_presupuesto']), 0, 0, 'C', 0);
				
				$pdf->SetXY($x,$y);
			}
			
            $pdf->SetWidths(array(25,180,25,25,25,25,25,25));
            $pdf->SetAligns(array('C','L','R','R','R','R','R','R','R'));
            
            $monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');  $montot=$montot+$consulta[$i]['num_monto_aprobado'];
			$montoa=number_format($consulta[$i]['num_monto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_monto_ajustado'];
			$montoc=number_format($consulta[$i]['compromiso_dist'],2,',','.'); $montoct=$montoct+$consulta[$i]['compromiso_dist'];
			$montod=$consulta[$i]['num_monto_ajustado']-$consulta[$i]['compromiso_dist']; $montodt=$montodt+$montod;
			
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(202, 202, 202);
			
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			   
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida);
					$suma_presupuestado=$suma['suma_aprobado'];
					$suma_ajustado=$suma['suma_ajustado'];

					$suma_compromiso=$suma['suma_compromiso_dist'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
				
				    $pdf->Cell(10,5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_compromiso,2,',','.'),number_format($suma_disponible,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida);
					$suma_presupuestado=$suma['suma_aprobado'];
					$suma_ajustado=$suma['suma_ajustado'];

					$suma_compromiso=$suma['suma_compromiso_dist'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
                    
					$pdf->Cell(10,5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),

					 number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_compromiso,2,',','.'),number_format($suma_disponible,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
			
			}//fin for
			/*FIN TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);
			
			
			$pdf->Cell(10,5);
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto,$montoa,$montoc,number_format($montod,2,',','.')));
			 
        }
	  
	  
	  }	
	        $pdf->SetFont('Arial', 'B', 8);
		    $pdf->SetFillColor(202, 202, 202);
	  
		    $montot=number_format($montot,2,',','.'); $montoat=number_format($montoat,2,',','.');$montoct=number_format($montoct,2,',','.');
			$montodt=number_format($montodt,2,',','.');
			
			$pdf->Cell(10,5);
		    $pdf->Row(array('','TOTALES', $montot,$montoat,$montoct,$montodt));
				
        $pdf->Output();

    }
	
 
 //-------------------------------------------------- 
   public function metincrementoReportePdf()
    {

        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formTxt=$this->metObtenerTexto('form','txt');

        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
		
		$this->_ReporteModelo = $this->metCargarModelo('reporte');
		$organismo= $this->_ReporteModelo-> metListarOrganismoInt($validacion['fk_a001_num_organismo']);


        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');

        $this->metObtenerLibreria('cabecera','modPR');
        $pdf=new pdfReporteIncremento('L','mm','Legal');
		$pdf->organismo = $organismo[0]['ind_descripcion_empresa'];
		$pdf->logo = $organismo[0]['ind_logo'];
		$pdf->logo_secundario = $organismo[0]['ind_logo_secundario'];
		
        $pdf->AliasNbPages();
        $pdf->AddPage();
		$pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(10, 5);
        $pdf->Cell(25, 5, utf8_decode('COD. PRESUPUESTO:'), 1, 1, 'L', 0);
		//$pdf->Cell(50, 5, 'PROYECTO:', 1, 1, 'L', 0);
		//$pdf->Cell(50, 5, 'ACCION ESPECIFICA:', 1, 1, 'L', 0);

        $pdf->Cell(10, 5);
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
       
        $pdf->Cell(25, 5, utf8_decode('COD. PARTIDA'), 1, 0, 'C', 1);
        $pdf->Cell(200, 5, 'DESCRIPCION', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'T. PRESUPUESTADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. AJUSTADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. INCREMENTADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. DISPONIBLE', 1, 0, 'C', 1);
		
        $pdf->SetFont('Arial', '', 7);

       
        $consulta= $this->_ReporteModelo-> getReporteCompromiso($validacion['idpresupuesto'],$validacion['fec_anio'],$validacion['fk_a001_num_organismo']);
		$partidas_titulo= $this->_ReporteModelo-> getReportePartidasTitulo();
		$aespecifica = $this->_ReporteModelo->metAespecifica();
		

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(4);

       $montot=0; $montoat=0; $montoct=0; $montocat=0; $montopt=0; $montodt=0;
	   $suma_presupuestado=0;
	   $p=0;
	   
	 if (count($consulta)>0 && ($validacion['idpresupuesto']!='error' || $validacion['fec_anio']!='error'))
	 {
	 if($consulta[0]['ind_tipo_presupuesto']=='P')
     {
	   
	  $montot=0;
	  $montoat=0;
	  $montoct=0;
	  $montodt=0;
			
	   
	   
	   for($a=0;$a<count($aespecifica);$a++)
	    {
		   $cont=0;
		   $existeAespecifica = count($this->_ReporteModelo->metExisteAespecificaEs2($aespecifica[$a]['pk_num_aespecifica'], $validacion['idpresupuesto']));
		   
		   $pdf->SetFont('Arial', 'B', 8);
		   $pdf->SetFillColor(202, 202, 202);
		   
		   $pdf->SetWidths(array(25,300));
           $pdf->SetAligns(array('C','L'));
		   
		   
		   if ($existeAespecifica>0)
		   {
		      
			  $pdf->Ln(1);
			  $pdf->Cell(10,5); 
		      $pdf->Row(array(
                    utf8_decode($aespecifica[$a]['ind_cod_aespecifica']),
                    utf8_decode($aespecifica[$a]['ind_descripcion'])
                    ));		
		   }
	   
	   $cont=0;
       for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {
 
			
			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->Cell(25, 5, utf8_decode($consulta[$i]['ind_cod_presupuesto']), 0, 0, 'C', 0);
				
				$pdf->SetXY($x,$y);
			}
			
            $pdf->SetWidths(array(25,200,25,25,25,25,25,25));
            $pdf->SetAligns(array('C','L','L','R','R','R','R','R','R'));
          
			
			$monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.'); 
			$montoa=number_format($consulta[$i]['num_monto_ajustado'],2,',','.'); 
			$montoc=number_format($consulta[$i]['num_monto_incremento'],2,',','.'); 
			$montod=$consulta[$i]['num_monto_ajustado']-$consulta[$i]['num_monto_compromiso']; 
			
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(202, 202, 202);
			
		  if ($aespecifica[$a]['pk_num_aespecifica']==$consulta[$i]['fk_pr006_num_aespecifica'])
		  {
			
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			   
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida,$aespecifica[$a]['pk_num_aespecifica']);
					$suma_presupuestado=$suma['suma_aprobado'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_incremento=$suma['suma_incremento'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
					
					
					 $montot=$montot+$suma_presupuestado;
	                 $montoat=$montoat+$suma_ajustado;
	                 $montoct=$montoct+$suma_incremento;
	                 
				
				    $pdf->Cell(10, 5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_incremento,2,',','.'),number_format($suma_disponible,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida,$aespecifica[$a]['pk_num_aespecifica']);
					$suma_presupuestado=$suma['suma_aprobado'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_incremento=$suma['suma_incremento'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;

                    $pdf->Cell(10, 5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
 					 number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_incremento,2,',','.'),number_format($suma_disponible,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
			
			}//fin for
			/*FIN TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);
			
			$pdf->Cell(10, 5);
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto,$montoa,$montoc,number_format($montod,2,',','.')));
				
				$montodt=$montodt+$montod;
		  }//FIN IF AESPECIFICA	 
         }
		}//FIN FOR AESPECIFICA
	   }
	   else
	   {
	   
	      	   $cont=0;
       for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {
 
			
			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->Cell(25, 5, utf8_decode($consulta[$i]['ind_cod_presupuesto']), 0, 0, 'C', 0);
				
				$pdf->SetXY($x,$y);
			}
			
            $pdf->SetWidths(array(25,200,25,25,25,25,25,25));
            $pdf->SetAligns(array('C','L','L','R','R','R','R','R','R'));
          
            $monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');  $montot=$montot+$consulta[$i]['num_monto_aprobado'];
			$montoa=number_format($consulta[$i]['num_monto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_monto_ajustado'];
			$montoc=number_format($consulta[$i]['num_monto_incremento'],2,',','.'); $montoct=$montoct+$consulta[$i]['num_monto_incremento'];
			$montod=$consulta[$i]['num_monto_ajustado']-$consulta[$i]['num_monto_compromiso']; $montodt=$montodt+$montod;
			
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(202, 202, 202);
			
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			   
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida,'error');
					$suma_presupuestado=$suma['suma_aprobado'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_incremento=$suma['suma_incremento'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
				
				    $pdf->Cell(10, 5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_incremento,2,',','.'),number_format($suma_disponible,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida,'error');
					$suma_presupuestado=$suma['suma_aprobado'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_incremento=$suma['suma_incremento'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;

                    $pdf->Cell(10, 5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
 					 number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_incremento,2,',','.'),number_format($suma_disponible,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
			
			}//fin for
			/*FIN TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);
			
			$pdf->Cell(10, 5);
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto,$montoa,$montoc,number_format($montod,2,',','.')));
			 
        }
	   
	   }	
		    $montot=number_format($montot,2,',','.'); $montoat=number_format($montoat,2,',','.');$montoct=number_format($montoct,2,',','.');
			$montodt=number_format($montodt,2,',','.');
			
			$pdf->Cell(10, 5);
		    $pdf->Row(array('','TOTALES', $montot,$montoat,$montoct,$montodt));
	}//FIN CONSULTA			
        $pdf->Output();

  }


//-------------------------------------------------- 
   public function metdisminucionReportePdf()
    {

        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formTxt=$this->metObtenerTexto('form','txt');

        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
		
		$this->_ReporteModelo = $this->metCargarModelo('reporte');
		$organismo= $this->_ReporteModelo-> metListarOrganismoInt($validacion['fk_a001_num_organismo']);


        $this->metObtenerLibreria('cabecera','modPR');
        $pdf=new pdfReporteDisminucion('L','mm','Legal');
		$pdf->organismo = $organismo[0]['ind_descripcion_empresa'];
		$pdf->logo = $organismo[0]['ind_logo'];
		$pdf->logo_secundario = $organismo[0]['ind_logo_secundario'];
        $pdf->AliasNbPages();
        $pdf->AddPage();
		
		$pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);

        $pdf->Cell(10, 5);
        $pdf->Cell(25, 5, utf8_decode('COD. PRESUPUESTO:'), 1, 1, 'L', 0);
		//$pdf->Cell(50, 5, 'PROYECTO:', 1, 1, 'L', 0);
		//$pdf->Cell(50, 5, 'ACCION ESPECIFICA:', 1, 1, 'L', 0);

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(10, 5);
        $pdf->Cell(25, 5, utf8_decode('COD. PARTIDA'), 1, 0, 'C', 1);
        $pdf->Cell(180, 5, 'DESCRIPCION', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'T. PRESUPUESTADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. AJUSTADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, utf8_decode('T. DISMINUCIÓN'), 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. DISPONIBLE', 1, 0, 'C', 1);
		
		
       
        $pdf->SetFont('Arial', '', 7);

		
        $consulta= $this->_ReporteModelo-> getReporteCompromiso($validacion['idpresupuesto'],$validacion['fec_anio'],$validacion['fk_a001_num_organismo']);
		$partidas_titulo= $this->_ReporteModelo-> getReportePartidasTitulo();
		$aespecifica = $this->_ReporteModelo->metAespecifica();
		

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(4);

       $montot=0; $montoat=0; $montoct=0; $montocat=0; $montopt=0; $montodt=0;
	   $suma_presupuestado=0;
	   $p=0;
	   
	   
	 if (count($consulta)>0 && ($validacion['idpresupuesto']!='error' || $validacion['fec_anio']!='error'))
	 if($consulta[0]['ind_tipo_presupuesto']=='P')
     {
	   $montot=0;
	   $montoat=0;
	   $montoct=0;
	   $montodt=0;
	   
	  
		  for($a=0;$a<count($aespecifica);$a++)
	      {
		   $cont=0;
		   $existeAespecifica = count($this->_ReporteModelo->metExisteAespecificaEs2($aespecifica[$a]['pk_num_aespecifica'], $validacion['idpresupuesto']));
		   
		   $pdf->SetFont('Arial', 'B', 8);
		   $pdf->SetFillColor(202, 202, 202);
		   
		   $pdf->SetWidths(array(25,280));
           $pdf->SetAligns(array('C','L'));
		   
		   
		   if ($existeAespecifica>0)
		   {
		      $pdf->Ln(1);
		      $pdf->Cell(10,5); 
		      $pdf->Row(array(
                    utf8_decode($aespecifica[$a]['ind_cod_aespecifica']),
                    utf8_decode($aespecifica[$a]['ind_descripcion'])
                    ));		
		   }
		   
		   $cont=0;
           for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
           {
			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->Cell(25, 5, utf8_decode($consulta[$i]['ind_cod_presupuesto']), 0, 0, 'C', 0);
				
				$pdf->SetXY($x,$y);
			}
			
            $pdf->SetWidths(array(25,180,25,25,25,25,25,25));
            $pdf->SetAligns(array('C','L','R','R','R','R','R','R','R'));
 
            //$monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');  $montot=$montot+$consulta[$i]['num_monto_aprobado'];
			//$montoa=number_format($consulta[$i]['num_monto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_monto_ajustado'];
			//$montoc=number_format($consulta[$i]['num_monto_disminucion'],2,',','.'); $montoct=$montoct+$consulta[$i]['num_monto_disminucion'];
			//$montod=$consulta[$i]['num_monto_ajustado']-$consulta[$i]['num_monto_compromiso']; $montodt=$montodt+$montod;
			
			
			$monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');  
			$montoa=number_format($consulta[$i]['num_monto_ajustado'],2,',','.');  
			$montoc=number_format($consulta[$i]['num_monto_disminucion'],2,',','.');
			$montod=$consulta[$i]['num_monto_ajustado']-$consulta[$i]['num_monto_compromiso']; 
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(202, 202, 202);
			
		 if ($aespecifica[$a]['pk_num_aespecifica']==$consulta[$i]['fk_pr006_num_aespecifica'])
		 {
			
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			   
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida,$aespecifica[$a]['pk_num_aespecifica']);
					$suma_presupuestado=$suma['suma_aprobado'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_disminucion=$suma['suma_disminucion'];
					
					$montot=$montot+$suma_presupuestado;
					$montoat=$montoat+$suma_ajustado;
					$montoct=$montoct+$suma_disminucion;
					
					
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
				
				    $pdf->Cell(10, 5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_disminucion,2,',','.'),number_format($suma_disponible,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida,$aespecifica[$a]['pk_num_aespecifica']);
					$suma_presupuestado=$suma['suma_aprobado'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_disminucion=$suma['suma_disminucion'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;

                    $pdf->Cell(10, 5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
					 number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_disminucion,2,',','.'),number_format($suma_disponible,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
			
			}//fin for
			
			
			/*FIN TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);
			
			$pdf->Cell(10, 5);
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto,$montoa,$montoc,number_format($montod,2,',','.')));
				
				$montodt=$montodt+$montod;
				
		 } //FIN IF AESPECIFICA
        }
		
	  }//FIN FOR AESPECIFICA
	  
	  
	}
	else//ELSE TIPO PRESUPUESTO
	{
	      $cont=0;
		  $suma_disminucion=0;
       for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {
		
			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->Cell(25, 5, utf8_decode($consulta[$i]['ind_cod_presupuesto']), 0, 0, 'C', 0);
				
				$pdf->SetXY($x,$y);
			}
			
            $pdf->SetWidths(array(25,180,25,25,25,25,25,25));
            $pdf->SetAligns(array('C','L','R','R','R','R','R','R','R'));
 
            $monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');  $montot=$montot+$consulta[$i]['num_monto_aprobado'];
			$montoa=number_format($consulta[$i]['num_monto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_monto_ajustado'];
			$montoc=number_format($consulta[$i]['num_monto_disminucion'],2,',','.'); $montoct=$montoct+$consulta[$i]['num_monto_disminucion'];
			$montod=$consulta[$i]['num_monto_ajustado']-$consulta[$i]['num_monto_compromiso']; $montodt=$montodt+$montod;
			
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(202, 202, 202);
			
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			   
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida,'error');
					$suma_presupuestado=$suma['suma_aprobado'];
					$suma_ajustado=$suma['suma_ajustado'];
					//$suma_disminucion=$suma['suma_disminucion'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
				
				    $pdf->Cell(10, 5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_disminucion,2,',','.'),number_format($suma_disponible,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida,'error');
					$suma_presupuestado=$suma['suma_aprobado'];
					$suma_ajustado=$suma['suma_ajustado'];
					//$suma_disminucion=$suma['suma_disminucion'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;

                    $pdf->Cell(10, 5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
					 number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_disminucion,2,',','.'),number_format($suma_disponible,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
			
			}//fin for
			/*FIN TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);
			
			$pdf->Cell(10, 5);
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto,$montoa,$montoc,number_format($montod,2,',','.')));
			 
        }
	
	} 	
		    $montot=number_format($montot,2,',','.'); $montoat=number_format($montoat,2,',','.');$montoct=number_format($montoct,2,',','.');
			$montodt=number_format($montodt,2,',','.');
			
			$pdf->Cell(10, 5);
		    $pdf->Row(array('','', $montot,$montoat,$montoct,$montodt));
				
        $pdf->Output();

    }

//---------------------------------------------------------------------------------  
  
  
  public function metcausadoReportePdf()
    {

        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formTxt=$this->metObtenerTexto('form','txt');

        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
		
		$this->_ReporteModelo = $this->metCargarModelo('reporte');
		$organismo= $this->_ReporteModelo-> metListarOrganismoInt($validacion['fk_a001_num_organismo']);

        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabecera','modPR');
        $pdf=new pdfReporteCausados('L','mm','Legal');
		$pdf->organismo = $organismo[0]['ind_descripcion_empresa'];
		$pdf->logo = $organismo[0]['ind_logo'];
		$pdf->logo_secundario = $organismo[0]['ind_logo_secundario'];
        $pdf->AliasNbPages();
        $pdf->AddPage();
				
		$pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
      
	    $pdf->Cell(10,5);
        $pdf->Cell(25, 5, utf8_decode('COD. PRESUPUESTO:'), 1, 1, 'L', 0);
		//$pdf->Cell(50, 5, 'PROYECTO:', 1, 1, 'L', 0);
		//$pdf->Cell(50, 5, 'ACCION ESPECIFICA:', 1, 1, 'L', 0);
		
		$pdf->Cell(10,5);
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(25, 5, utf8_decode('COD. PARTIDA'), 1, 0, 'C', 1);
        $pdf->Cell(180, 5, 'DESCRIPCION', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'T. PRESUPUESTADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. AJUSTADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. CAUSADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'T. DISPONIBLE', 1, 0, 'C', 1);
       
        $pdf->SetFont('Arial', '', 7);

		
        $consulta= $this->_ReporteModelo-> getReporteCausado($validacion['idpresupuesto'],$validacion['fec_anio'],$validacion['fk_a001_num_organismo']);
		$partidas_titulo= $this->_ReporteModelo-> getReportePartidasTitulo();
		$aespecifica = $this->_ReporteModelo->metAespecifica();
		

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(4);

       $montot=0; $montoat=0; $montoct=0; $montocat=0; $montopt=0; $montodt=0;
	   $suma_presupuestado=0;
	   $p=0;
	   
	 if (count($consulta)>0)
	 if($consulta[0]['ind_tipo_presupuesto']=='P')
     {
	   
	    for($a=0;$a<count($aespecifica);$a++)
	    {
		   $cont=0;
		   $existeAespecifica = count($this->_ReporteModelo->metExisteAespecificaEs2($aespecifica[$a]['pk_num_aespecifica'], $validacion['idpresupuesto']));
		   
		   $pdf->SetFont('Arial', 'B', 8);
		   $pdf->SetFillColor(202, 202, 202);
		   
		   $pdf->SetWidths(array(25,280));
           $pdf->SetAligns(array('C','L'));
		   
		   
		   if ($existeAespecifica>0)
		   {
		      $pdf->Ln(1);
		      $pdf->Cell(10,5); 
		      $pdf->Row(array(
                    utf8_decode($aespecifica[$a]['ind_cod_aespecifica']),
                    utf8_decode($aespecifica[$a]['ind_descripcion'])
                    ));		
		   }
	   
	   $cont=0;
       for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {
			
			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->Cell(25, 5, utf8_decode($consulta[$i]['ind_cod_presupuesto']), 0, 0, 'C', 0);
				
				$pdf->SetXY($x,$y);
			}
			
			
            $pdf->SetWidths(array(25,180,25,25,25,25,25,25));
            $pdf->SetAligns(array('C','L','R','R','R','R','R','R','R'));
            $monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');  $montot=$montot+$consulta[$i]['num_monto_aprobado'];
			$montoa=number_format($consulta[$i]['num_monto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_monto_ajustado'];

			$montoca=number_format($consulta[$i]['causado_dist'],2,',','.'); $montocat=$montocat+$consulta[$i]['causado_dist'];

			$montod=$consulta[$i]['num_monto_ajustado']-$consulta[$i]['causado_dist']; $montodt=$montodt+$montod;
			
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(235, 235, 235);
			
			
		   if ($aespecifica[$a]['pk_num_aespecifica']==$consulta[$i]['fk_pr006_num_aespecifica'])
		   {
			
			
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			   
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida,$aespecifica[$a]['pk_num_aespecifica']);
					$suma_presupuestado=$suma['suma_aprobado'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_causado=$suma['suma_causado_dist'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
				
				    $pdf->Cell(10,5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_disponible,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida,$aespecifica[$a]['pk_num_aespecifica']);
					$suma_presupuestado=$suma['suma_aprobado'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_causado=$suma['suma_causado_dist'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;

                    $pdf->Cell(10,5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                  	number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_disponible,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
			
			}//fin for
			/*FIN TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);
			
			$pdf->Cell(10,5);
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto,$montoa,$montoca,number_format($montod,2,',','.')));
				
				
		 }//FIN IF AESPECIFICA	 
        }//FIN I
	   }//FIN FOR AESPECIFICA
		
		}else //ELSE TIPO PRESUPUESTO
		{
		
		   $cont=0;
       for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {
			
			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->Cell(25, 5, utf8_decode($consulta[$i]['ind_cod_presupuesto']), 0, 0, 'C', 0);
				
				$pdf->SetXY($x,$y);
			}
			
			
            $pdf->SetWidths(array(25,180,25,25,25,25,25,25));
            $pdf->SetAligns(array('C','L','R','R','R','R','R','R','R'));
            $monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');  $montot=$montot+$consulta[$i]['num_monto_aprobado'];
			$montoa=number_format($consulta[$i]['num_monto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_monto_ajustado'];

			$montoca=number_format($consulta[$i]['causado_dist'],2,',','.'); $montocat=$montocat+$consulta[$i]['causado_dist'];

			$montod=$consulta[$i]['num_monto_ajustado']-$consulta[$i]['causado_dist']; $montodt=$montodt+$montod;
			
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(202, 202, 202);
			
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			   
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida,'error');
					$suma_presupuestado=$suma['suma_aprobado'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_causado=$suma['suma_causado_dist'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
				
				    $pdf->Cell(10,5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_disponible,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida,'error');
					$suma_presupuestado=$suma['suma_aprobado'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_causado=$suma['suma_causado_dist'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;

                    $pdf->Cell(10,5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                  	number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_causado,2,',','.'),number_format($suma_disponible,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
			
			}//fin for
			/*FIN TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);
			
			$pdf->Cell(10,5);
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto,$montoa,$montoca,number_format($montod,2,',','.')));
			 
        }//FIN I
		
	}//FIN TIPO PRESUPUESTO
	
	        $pdf->SetFont('Arial', 'B', 8);
			$pdf->SetFillColor(202, 202, 202);
	
		    $montot=number_format($montot,2,',','.'); $montoat=number_format($montoat,2,',','.');$montocat=number_format($montocat,2,',','.');
			$montodt=number_format($montodt,2,',','.');
			
			$pdf->Cell(10,5);
		    $pdf->Row(array('','TOTALES', $montot,$montoat,$montocat,$montodt));
				
        $pdf->Output();

    }

  
   public function metpagadoReportePdf()
    {

        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formTxt=$this->metObtenerTexto('form','txt');
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
		
		$this->_ReporteModelo = $this->metCargarModelo('reporte');
		$organismo= $this->_ReporteModelo-> metListarOrganismoInt($validacion['fk_a001_num_organismo']);

        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');

        $this->metObtenerLibreria('cabecera','modPR');
        $pdf=new pdfReportePagados('L','mm','Legal');
		$pdf->organismo = $organismo[0]['ind_descripcion_empresa'];
		$pdf->logo = $organismo[0]['ind_logo'];
		$pdf->logo_secundario = $organismo[0]['ind_logo_secundario'];
        $pdf->AliasNbPages();
        $pdf->AddPage();
				
		//$pdf->Cell(250, 5, utf8_decode('REPORTE PRESUPUESTO'),0,1,'C');
		
		$pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
    
	    $pdf->Cell(10,5);
        $pdf->Cell(25, 5, utf8_decode('COD. PRESUPUESTO:'), 1, 1, 'L', 0);
		//$pdf->Cell(50, 5, 'PROYECTO:', 1, 1, 'L', 0);
		//$pdf->Cell(50, 5, 'ACCION ESPECIFICA:', 1, 1, 'L', 0);
		
  
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(10,5);
        $pdf->Cell(25, 5, utf8_decode('COD. PARTIDA'), 1, 0, 'C', 1);
        $pdf->Cell(180, 5, 'DESCRIPCION', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'PRESUPUESTADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'AJUSTADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'PAGADO', 1, 0, 'C', 1);
		$pdf->Cell(25, 5, 'DISPONIBLE', 1, 0, 'C', 1);
			
       
        $pdf->SetFont('Arial', '', 7);
		
        $consulta= $this->_ReporteModelo-> getReportePagado($validacion['idpresupuesto'],$validacion['fec_anio']);
		$partidas_titulo= $this->_ReporteModelo-> getReportePartidasTitulo();
		$aespecifica = $this->_ReporteModelo->metAespecifica();
		

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(4);

        $montot=0; $montoat=0; $montoct=0; $montocat=0; $montopt=0; $montodt=0;
	    $suma_presupuestado=0;
	    $p=0;
	   
	  if($consulta[0]['ind_tipo_presupuesto']=='P')
     {
	  
	  for($a=0;$a<count($aespecifica);$a++)
	    {
		 
		   $cont=0;
		   $existeAespecifica = count($this->_ReporteModelo->metExisteAespecificaEs2($aespecifica[$a]['pk_num_aespecifica'], $validacion['idpresupuesto']));
		   
		   $pdf->SetFont('Arial', 'B', 8);
		   $pdf->SetFillColor(202, 202, 202);
		   
		   $pdf->SetWidths(array(25,280));
           $pdf->SetAligns(array('C','L'));
		   
		   $pdf->Ln(1);
		   
		   if ($existeAespecifica>0)
		   {
		      $pdf->Cell(10,5); 
		      $pdf->Row(array(
                    utf8_decode($aespecifica[$a]['ind_cod_aespecifica']),
                    utf8_decode($aespecifica[$a]['ind_descripcion'])
                    ));		
		   }
		   
	 
	   
	    $cont=0;
        for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {
			
			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->Cell(25, 5, utf8_decode($consulta[$i]['ind_cod_presupuesto']), 0, 0, 'C', 0);
				
				$pdf->SetXY($x,$y);
			}
			
            $pdf->SetWidths(array(25,180,25,25,25,25,25,25));
            $pdf->SetAligns(array('C','L','R','R','R','R','R','R','R'));

            $monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');  $montot=$montot+$consulta[$i]['num_monto_aprobado'];
			$montoa=number_format($consulta[$i]['num_monto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_monto_ajustado'];
		    $montop=number_format($consulta[$i]['pagado_dist'],2,',','.'); $montopt=$montopt+$consulta[$i]['pagado_dist'];
			
			$montod=$consulta[$i]['num_monto_ajustado']-$consulta[$i]['pagado_dist']; $montodt=$montodt+$montod;
			
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(235, 235, 235);
			
		  if ($aespecifica[$a]['pk_num_aespecifica']==$consulta[$i]['fk_pr006_num_aespecifica'])
		  {
			
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			   
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida,$aespecifica[$a]['pk_num_aespecifica']);
					$suma_presupuestado=$suma['suma_aprobado'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_causado=$suma['suma_causado'];
					$suma_pagado=$suma['suma_pagado_dist'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
				
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_pagado,2,',','.'),number_format($suma_disponible,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida,$aespecifica[$a]['pk_num_aespecifica']);
					$suma_presupuestado=$suma['suma_aprobado'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_causado=$suma['suma_causado'];
					$suma_pagado=$suma['suma_pagado_dist'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;

                    $pdf->Cell(10,5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                  	number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_pagado,2,',','.'),number_format($suma_disponible,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
			
			}//fin for
			/*FIN TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);
			
			$pdf->Cell(10,5);
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto,$montoa,$montop,number_format($montod,2,',','.')));
		 
		 }//FIN IF AESPECIFICA	 
        }
		
	   }//FIN FOR AESPECIFICA
		
		
		}else{//ELSE TIPO PRESUPUESTO
		 
		  
		   $cont=0;
        for($i=0; $i< count($consulta); $i++)// recibe las casillas marcadas
        {
			
			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->Cell(25, 5, utf8_decode($consulta[$i]['ind_cod_presupuesto']), 0, 0, 'C', 0);
				
				$pdf->SetXY($x,$y);
			}
			
            $pdf->SetWidths(array(25,180,25,25,25,25,25,25));
            $pdf->SetAligns(array('C','L','R','R','R','R','R','R','R'));

            $monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');  $montot=$montot+$consulta[$i]['num_monto_aprobado'];
			$montoa=number_format($consulta[$i]['num_monto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_monto_ajustado'];
		    $montop=number_format($consulta[$i]['pagado_dist'],2,',','.'); $montopt=$montopt+$consulta[$i]['pagado_dist'];
			
			$montod=$consulta[$i]['num_monto_ajustado']-$consulta[$i]['pagado_dist']; $montodt=$montodt+$montod;
			
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(202, 202, 202);
			
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			   
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida,'error');
					$suma_presupuestado=$suma['suma_aprobado'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_causado=$suma['suma_causado'];
					$suma_pagado=$suma['suma_pagado_dist'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;
				
				    $pdf->Cell(10,5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                    number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_pagado,2,',','.'),number_format($suma_disponible,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTitulo($validacion['idpresupuesto'],$partida,'error');
					$suma_presupuestado=$suma['suma_aprobado'];
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_causado=$suma['suma_causado'];
					$suma_pagado=$suma['suma_pagado_dist'];
					$suma_compromiso=$suma['suma_compromiso'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;

                    $pdf->Cell(10,5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),
                  	number_format($suma_presupuestado,2,',','.'),number_format($suma_ajustado,2,',','.'),number_format($suma_pagado,2,',','.'),number_format($suma_disponible,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
			
			}//fin for
			/*FIN TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);
			
			$pdf->Cell(10,5);
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
                $monto,$montoa,$montop,number_format($montod,2,',','.')));
			 
        } 
		
		}
		
		    $pdf->SetFont('Arial', 'B', 8);
			$pdf->SetFillColor(202, 202, 202);
			
		    $montot=number_format($montot,2,',','.'); $montoat=number_format($montoat,2,',','.');$montopt=number_format($montopt,2,',','.');
			$montodt=number_format($montodt,2,',','.');
			
			$pdf->Cell(10,5);
		    $pdf->Row(array('','TOTALES', $montot,$montoat,$montopt,$montodt));
				
        $pdf->Output();

    }



      // Reporte de Proyeccion de Presupuesto 
	public function metproyeccionReportePdf()
    {

        $formInt=$this->metObtenerInt('form','int');
        $formTxt=$this->metObtenerTexto('form','txt');
		
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
		
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
		
		$fec_actual=date("m");
		$meses_proyeccion=0;
		$validacion['fec_mes']=date("m");
		$validacion['fec_anio']=date("Y");
		
		
		if($validacion['fec_mes']!="error"){
            $fec_mes=$validacion['fec_mes'];
        }else{
            $fec_mes="";
        }
			
			
		if($validacion['fec_anio']!="error"){
            $fec_anio=$validacion['fec_anio'];
        }else{
            $fec_anio="";
        }

        $this->_ReporteModelo = $this->metCargarModelo('reporte');
		$organismo= $this->_ReporteModelo-> metListarOrganismoInt($validacion['fk_a001_num_organismo']);

        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabecera','modPR');
        $pdf=new pdfReporteCierreMes('L','mm','Legal');
		$pdf->organismo = $organismo[0]['ind_descripcion_empresa'];
		$pdf->logo = $organismo[0]['ind_logo'];
		$pdf->logo_secundario = $organismo[0]['ind_logo_secundario'];
        $pdf->AliasNbPages();
        $pdf->AddPage();
				
		
		 $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
         $pdf->SetFont('Arial', 'B', 7);

		
        switch ($validacion['fec_mes']) {
		  case "01": $fec_mes= "ENERO"; break;  
		  case "02": $fec_mes= "FEBRERO"; break; 
		  case "03": $fec_mes= "MARZO";  break;    
		  case "04": $fec_mes= "ABRIL";  break;   
		  case "05": $fec_mes= "MAYO";  break;    
		  case "06": $fec_mes= "JUNIO";  break;
		  case "07": $fec_mes= "JULIO";  break;
		  case "08": $fec_mes= "AGOSTO";  break;
		  case "09": $fec_mes= "SEPTIEMBRE";   break;
		  case "10": $fec_mes= "OCTUBRE";  break;
		  case "11": $fec_mes= "NOVIEMBRE";  break;
		  case "12": $fec_mes= "DICIEMBRE";  break;
        }
		
		
	    $pdf->SetFont('Arial', 'B', 10);
	    $pdf->Cell(105, 10, '', 0, 0, 'C');
	    $pdf->Cell(100, 10, utf8_decode(' REPORTE DE PROYECCION DE PRESUPUESTO MES '), 0, 0, 'C');
        $pdf->Cell(13, 10, $fec_mes , 0, 0, 'C'); $pdf->Cell(20, 10, $fec_anio, 0, 1, 'C');
	
		
		$pdf->Ln(3);
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);

        //$pdf->Cell(5, 5,utf8_decode('N°'), 1, 0, 'C', 1);
		$pdf->Cell(10, 5);
        $pdf->Cell(20, 5, utf8_decode('COD. PARTIDA'), 1, 0, 'C', 1);
        $pdf->Cell(160, 5, 'DESCRIPCION', 1, 0, 'C', 1);
        $pdf->Cell(20, 5, 'ULT. CIERRE', 1, 0, 'C', 1);
		$pdf->Cell(22, 5, 'PRESUP. AJUST', 1, 0, 'C', 1);
		$pdf->Cell(20, 5, 'PAGADO', 1, 0, 'C', 1);
		$pdf->Cell(22, 5, 'DISP. PRESUP', 1, 0, 'C', 1);
		$pdf->Cell(22, 5, 'PAG. PROYECCION', 1, 0, 'C', 1);
		$pdf->Cell(22, 5, 'DISP. PROYEC.', 1, 0, 'C', 1);
       
        $pdf->SetFont('Arial', '', 7);

        $this->_ReporteModelo = $this->metCargarModelo('reporte');
		
        $consulta=  $this->_ReporteModelo-> getReporteproyeccion($validacion['idpresupuesto'],$validacion['fec_anio'],$validacion['fec_mes'],$validacion['cod_partida'],1);
		
		
	if (count($consulta)>0)
    {
		$fec_mes=$consulta[0]['fec_mes_ant'];
		
		$fec_mes_anterior=substr($fec_mes,1, 1);
		
		$fec_mes_a=$fec_mes_anterior-1;
		
		if ($fec_mes_a < 10)
		   $fec_mes_anterior='0'.$fec_mes_a;
		   
		  //var_dump($fec_mes_anterior);
		   //exit;
     		   
		
		$consulta2= $this->_ReporteModelo-> getReporteproyeccion($validacion['idpresupuesto'],$validacion['fec_anio'], $fec_mes_anterior,$validacion['cod_partida'],2);
		
		$partidas_titulo= $this->_ReporteModelo-> getReportePartidasTitulo();
		$aespecifica = $this->_ReporteModelo->metAespecifica();
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(4);
	
        $montot=0; $montofi=0; $montoin=0; $montodi=0; $montoat=0; $montoct=0; $montocat=0; $montopt=0; $montodt=0; $montopfnt=0;
		
		$dmontot=0; $dmontofi=0; $dmontoin=0; $dmontodi=0; $dmontoat=0; $dmontoct=0; $dmontocat=0; $dmontopt=0; $dmontodt=0; $dmontopfnt=0; $montoppt=0;
     }
 
	    
		
	 if (count($consulta)>0 && count($consulta2)>0)
	 if($consulta[0]['ind_tipo_presupuesto']=='P')
     {	
	 
	    for($a=0;$a<count($aespecifica);$a++)
	    {
		   $cont=0;
		   $existeAespecifica = count($this->_ReporteModelo->metExisteAespecificaEs2($aespecifica[$a]['pk_num_aespecifica'], $validacion['idpresupuesto']));
		   
		   $pdf->SetFont('Arial', 'B', 8);
		   $pdf->SetFillColor(202, 202, 202);
		   
		   $pdf->SetWidths(array(25,283));
           $pdf->SetAligns(array('C','L'));
		   
		   
		   if ($existeAespecifica>0)
		   {
		      $pdf->Ln(1);
		      $pdf->Cell(10,5); 
		      $pdf->Row(array(
                    utf8_decode($aespecifica[$a]['ind_cod_aespecifica']),
                    utf8_decode($aespecifica[$a]['ind_descripcion'])
                    ));		
		   }
	 
	    $cont=0;
        for($i=0; $i< count($consulta); $i++) // recibe las casillas marcadas
        {

			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->SetXY($x,$y);
			}
			
			
			$meses_proyeccion=$validacion['meses'];
			 
			
            $pdf->SetWidths(array(20,160,20,22,20,22,22,22));
            $pdf->SetAligns(array('L','L','L','R','R','R','R','R','R'));
            
			$monto=number_format($consulta[$i]['num_disponibilidad_presupuestaria_inicial'],2,',','.');  $montot=$montot+$consulta[$i]['num_disponibilidad_presupuestaria_inicial'];
			$montof=number_format($consulta[$i]['num_disponibilidad_financiera_inicial'],2,',','.');  $montofi=$montofi+$consulta[$i]['num_disponibilidad_financiera_inicial'];
			$montoi=number_format($consulta[$i]['num_incremento'],2,',','.');  $montoin=$montoin+$consulta[$i]['num_incremento'];
			$montods=number_format($consulta[$i]['num_disminucion'],2,',','.');  $montodi=$montodi+$consulta[$i]['num_disminucion'];
			$montoa=number_format($consulta[$i]['num_presupuesto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_presupuesto_ajustado'];
			$montoc=number_format($consulta[$i]['num_compromiso'],2,',','.'); $montoct=$montoct+$consulta[$i]['num_compromiso'];
			$montoca=number_format($consulta[$i]['num_causado'],2,',','.'); $montocat=$montocat+$consulta[$i]['num_causado'];
			$montop=number_format($consulta[$i]['num_pagado'],2,',','.'); $montopt=$montopt+$consulta[$i]['num_pagado'];
			$montopp=number_format($consulta[$i]['num_pagado']*$meses_proyeccion,2,',','.'); $montoppt=$montoppt+$consulta[$i]['num_pagado']*$meses_proyeccion;
			
			$montod=$consulta[$i]['num_presupuesto_ajustado']-$consulta[$i]['num_pagado']; $montodt=$montodt+$montod;
			//Se resta el presupuesto ajuastado - el comprometido para sacar la idsponibilidad presupuestaria final
			$montofn=$montod-($consulta[$i]['num_pagado']*$meses_proyeccion); $montopfnt=$montopfnt+$montofn*$meses_proyeccion;
			
			
			//---------------------------------------------------------------------------------------------------------
			//MONTO PERIODO ANTERIOR
						
		     $dmontof=number_format($consulta2[$i]['num_disponibilidad_financiera_inicial'],2,',','.');  $dmontofi=$dmontofi+$consulta2[$i]['num_disponibilidad_financiera_inicial'];
			 $dmontoi=number_format($consulta2[$i]['num_incremento'],2,',','.');  $dmontoin=$dmontoin+$consulta2[$i]['num_incremento'];
			 $dmontods=number_format($consulta2[$i]['num_disminucion'],2,',','.');  $dmontodi=$dmontodi+$consulta2[$i]['num_disminucion'];
			 $dmontoa=number_format($consulta2[$i]['num_presupuesto_ajustado'],2,',','.');  $dmontoat=$dmontoat+$consulta2[$i]['num_presupuesto_ajustado'];
			 $dmontoc=number_format($consulta2[$i]['num_compromiso'],2,',','.'); $dmontoct=$dmontoct+$consulta2[$i]['num_compromiso'];
			 $dmontoca=number_format($consulta2[$i]['num_causado'],2,',','.'); $dmontocat=$dmontocat+$consulta2[$i]['num_causado'];
			 
			 $dmontop=number_format($consulta2[$i]['num_pagado']*$meses_proyeccion,2,',','.'); $dmontopt=$dmontopt+$consulta2[$i]['num_pagado']*$meses_proyeccion;
			//Se resta el presupuesto ajuastado - el comprometido para sacar la idsponibilidad presupuestaria final
			 $dmontod=$consulta2[$i]['num_presupuesto_ajustado']-$consulta2[$i]['num_compromiso']; $dmontodt=$dmontodt+$dmontod;
			//Se resta el presupuesto ajuastado - el comprometido para sacar la idsponibilidad presupuestaria final
			 $dmontofn=$consulta2[$i]['num_presupuesto_ajustado']-$consulta2[$i]['num_pagado']; $dmontopfnt=$dmontopfnt+$dmontofn;
			 
			 
			$dmontoat=number_format($dmontoat,2,',','.');
			$dmontoct=number_format($dmontoct,2,',','.');
			$dmontocat=number_format($dmontocat,2,',','.'); 
			$dmontopt=number_format($dmontopt,2,',','.'); 
			$dmontodt=number_format($dmontodt,2,',','.');
			$dmontopfnt=number_format($dmontopfnt,2,',','.');
			
			//---------------------------------------------------------------------------------------------------------
			
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(235, 235, 235);
			
		  if ($aespecifica[$a]['pk_num_aespecifica']==$consulta[$i]['fk_pr006_num_aespecifica'])
		 {	
			
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{  
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTituloProyeccion($validacion['idpresupuesto'],$partida,$consulta[0]['fec_mes']);
					
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_pagado_proy=$suma['suma_pagado']*$meses_proyeccion;
					
					$suma_pagado=$suma['suma_pagado'];
					
					$suma_disponible=$suma_ajustado-$suma_pagado;
					$suma_disponible_proy=$suma_disponible-$suma_pagado_proy;
				
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),'',
                    number_format($suma_ajustado,2,',','.'),number_format($suma_pagado,2,',','.'),number_format($suma_disponible,2,',','.'),number_format($suma_pagado_proy,2,',','.'),number_format($suma_disponible_proy,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTituloProyeccion($validacion['idpresupuesto'],$partida,$consulta[0]['fec_mes']);
					
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_pagado_proy=$suma['suma_pagado']*$meses_proyeccion;
					
					$suma_pagado=$suma['suma_pagado'];
					
					$suma_disponible=$suma_ajustado-$suma_pagado;
					$suma_disponible_proy=$suma_disponible-$suma_pagado_proy;

                    $pdf->Cell(10, 5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),'',
                   number_format($suma_ajustado,2,',','.'),number_format($suma_pagado,2,',','.'),number_format($suma_disponible,2,',','.'),number_format($suma_pagado_proy,2,',','.'),number_format($suma_disponible_proy,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
			
			}//fin for
			/*FIN TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);
			
			$pdf->Cell(10, 5);
			 $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
				utf8_decode($consulta[$i]['fec_mes']),
                $montoa,$montop,number_format($montod,2,',','.'),$montopp,number_format($montofn,2,',','.')));
				
			}//FIN IF AESPECIFICA	
					
		  }
			
		 }//FIN FOR AESPECIFICA	
		    $pdf->SetFont('Arial', 'B', 8);
			$pdf->SetFillColor(202, 202, 202);
		 
		    $montoat=number_format($montoat,2,',','.');
			$montoct=number_format($montoct,2,',','.');
			$montocat=number_format($montocat,2,',','.'); 
			$montopt=number_format($montopt,2,',','.'); 
			$montoppt=number_format($montoppt,2,',','.'); 
			$montodt=number_format($montodt,2,',','.');
			$montopfnt=number_format($montopfnt,2,',','.');
			
			$pdf->Cell(10, 5);
		    $pdf->Row(array('','','', $montoat,$montopt,$montodt,$montoppt,$montopfnt));
		}
		else
		{
		  $cont=0;
        for($i=0; $i< count($consulta); $i++) // recibe las casillas marcadas
        {

			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->SetXY($x,$y);
			}
			
			
			$meses_proyeccion=$validacion['meses'];
			 
			
            $pdf->SetWidths(array(20,160,20,22,20,22,22,22));
            $pdf->SetAligns(array('C','L','L','R','R','R','R','R','R','R'));
            
			$monto=number_format($consulta[$i]['num_disponibilidad_presupuestaria_inicial'],2,',','.');  $montot=$montot+$consulta[$i]['num_disponibilidad_presupuestaria_inicial'];
			$montof=number_format($consulta[$i]['num_disponibilidad_financiera_inicial'],2,',','.');  $montofi=$montofi+$consulta[$i]['num_disponibilidad_financiera_inicial'];
			$montoi=number_format($consulta[$i]['num_incremento'],2,',','.');  $montoin=$montoin+$consulta[$i]['num_incremento'];
			$montods=number_format($consulta[$i]['num_disminucion'],2,',','.');  $montodi=$montodi+$consulta[$i]['num_disminucion'];
			$montoa=number_format($consulta[$i]['num_presupuesto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_presupuesto_ajustado'];
			$montoc=number_format($consulta[$i]['num_compromiso'],2,',','.'); $montoct=$montoct+$consulta[$i]['num_compromiso'];
			$montoca=number_format($consulta[$i]['num_causado'],2,',','.'); $montocat=$montocat+$consulta[$i]['num_causado'];
			$montop=number_format($consulta[$i]['num_pagado'],2,',','.'); $montopt=$montopt+$consulta[$i]['num_pagado'];
			$montopp=number_format($consulta[$i]['num_pagado']*$meses_proyeccion,2,',','.'); $montoppt=$montoppt+$consulta[$i]['num_pagado']*$meses_proyeccion;
			
			$montod=$consulta[$i]['num_presupuesto_ajustado']-$consulta[$i]['num_pagado']; $montodt=$montodt+$montod;
			//Se resta el presupuesto ajuastado - el comprometido para sacar la idsponibilidad presupuestaria final
			$montofn=$montod-($consulta[$i]['num_pagado']*$meses_proyeccion); $montopfnt=$montopfnt+$montofn*$meses_proyeccion;
			
			
			//---------------------------------------------------------------------------------------------------------
			//MONTO PERIODO ANTERIOR
						
		     $dmontof=number_format($consulta2[$i]['num_disponibilidad_financiera_inicial'],2,',','.');  $dmontofi=$dmontofi+$consulta2[$i]['num_disponibilidad_financiera_inicial'];
			 $dmontoi=number_format($consulta2[$i]['num_incremento'],2,',','.');  $dmontoin=$dmontoin+$consulta2[$i]['num_incremento'];
			 $dmontods=number_format($consulta2[$i]['num_disminucion'],2,',','.');  $dmontodi=$dmontodi+$consulta2[$i]['num_disminucion'];
			 $dmontoa=number_format($consulta2[$i]['num_presupuesto_ajustado'],2,',','.');  $dmontoat=$dmontoat+$consulta2[$i]['num_presupuesto_ajustado'];
			 $dmontoc=number_format($consulta2[$i]['num_compromiso'],2,',','.'); $dmontoct=$dmontoct+$consulta2[$i]['num_compromiso'];
			 $dmontoca=number_format($consulta2[$i]['num_causado'],2,',','.'); $dmontocat=$dmontocat+$consulta2[$i]['num_causado'];
			 
			 $dmontop=number_format($consulta2[$i]['num_pagado']*$meses_proyeccion,2,',','.'); $dmontopt=$dmontopt+$consulta2[$i]['num_pagado']*$meses_proyeccion;
			//Se resta el presupuesto ajuastado - el comprometido para sacar la idsponibilidad presupuestaria final
			 $dmontod=$consulta2[$i]['num_presupuesto_ajustado']-$consulta2[$i]['num_compromiso']; $dmontodt=$dmontodt+$dmontod;
			//Se resta el presupuesto ajuastado - el comprometido para sacar la idsponibilidad presupuestaria final
			 $dmontofn=$consulta2[$i]['num_presupuesto_ajustado']-$consulta2[$i]['num_pagado']; $dmontopfnt=$dmontopfnt+$dmontofn;
			 
			 
			$dmontoat=number_format($dmontoat,2,',','.');
			$dmontoct=number_format($dmontoct,2,',','.');
			$dmontocat=number_format($dmontocat,2,',','.'); 
			$dmontopt=number_format($dmontopt,2,',','.'); 
			$dmontodt=number_format($dmontodt,2,',','.');
			$dmontopfnt=number_format($dmontopfnt,2,',','.');
			
			//---------------------------------------------------------------------------------------------------------
			
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(202, 202, 202);
			
			
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{  
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTituloProyeccion($validacion['idpresupuesto'],$partida,$consulta[0]['fec_mes']);
					
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_pagado_proy=$suma['suma_pagado']*$meses_proyeccion;
					
					$suma_pagado=$suma['suma_pagado'];
					
					$suma_disponible=$suma_ajustado-$suma_pagado;
					$suma_disponible_proy=$suma_disponible-$suma_pagado_proy;
				
				    $pdf->Cell(10, 5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),'',
                    number_format($suma_ajustado,2,',','.'),number_format($suma_pagado,2,',','.'),number_format($suma_disponible,2,',','.'),number_format($suma_pagado_proy,2,',','.'),number_format($suma_disponible_proy,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				  
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTituloProyeccion($validacion['idpresupuesto'],$partida,$consulta[0]['fec_mes']);
					
					$suma_ajustado=$suma['suma_ajustado'];
					$suma_pagado_proy=$suma['suma_pagado']*$meses_proyeccion;
					
					$suma_pagado=$suma['suma_pagado'];
					
					$suma_disponible=$suma_ajustado-$suma_pagado;
					$suma_disponible_proy=$suma_disponible-$suma_pagado_proy;
                    
					$pdf->Cell(10, 5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),'',
                   number_format($suma_ajustado,2,',','.'),number_format($suma_pagado,2,',','.'),number_format($suma_disponible,2,',','.'),number_format($suma_pagado_proy,2,',','.'),number_format($suma_disponible_proy,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
			
			}//fin for
			/*FIN TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);
			
			 $pdf->Cell(10, 5);
			 $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),
				utf8_decode($consulta[$i]['fec_mes']),
                $montoa,$montop,number_format($montod,2,',','.'),$montopp,number_format($montofn,2,',','.')));
					
			}
			
			$pdf->SetFont('Arial', 'B', 8);
			$pdf->SetFillColor(202, 202, 202);

			
			$montoat=number_format($montoat,2,',','.');
			$montoct=number_format($montoct,2,',','.');
			$montocat=number_format($montocat,2,',','.'); 
			$montopt=number_format($montopt,2,',','.'); 
			$montoppt=number_format($montoppt,2,',','.'); 
			$montodt=number_format($montodt,2,',','.');
			$montopfnt=number_format($montopfnt,2,',','.');
			
			$pdf->Cell(10, 5);
		    $pdf->Row(array('','','', $montoat,$montopt,$montodt,$montoppt,$montopfnt));
				
		
		
		}	
			
        $pdf->Output();
    }




     //---------------------------------------------------------------------------
	 public function metejecucionespecificaReportePdf()
    {

        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formTxt=$this->metObtenerTexto('form','txt');

        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
		
		$this->_ReporteModelo = $this->metCargarModelo('reporte');
		$organismo= $this->_ReporteModelo-> metListarOrganismoInt($validacion['fk_a001_num_organismo']);
		
		$estado='';
		if ($validacion['estado']=='CO')
		   $estado=' COMPROMETIDO';
		if ($validacion['estado']=='CA')
		   $estado=' CAUSADO';
		if ($validacion['estado']=='PA')
		  $estado=' PAGADO';

        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabecera','modPR');
        $pdf=new pdfReporteEjecucionEspecifica('P','mm','Letter');
		$pdf->organismo = $organismo[0]['ind_descripcion_empresa'];
		$pdf->logo = $organismo[0]['ind_logo'];
		$pdf->logo_secundario = $organismo[0]['ind_logo_secundario'];
		$pdf->estado = $estado;
		
        $pdf->AliasNbPages();
        $pdf->AddPage();
		
		$pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
       
	    $pdf->Cell(10, 5);
        $pdf->Cell(25, 5, utf8_decode('COD. PRESUPUESTO:'), 1, 1, 'L', 0);
		//$pdf->Cell(50, 5, 'PROYECTO:', 1, 1, 'L', 0);
		//$pdf->Cell(50, 5, 'ACCION ESPECIFICA:', 1, 1, 'L', 0);

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(10, 5);
        $pdf->Cell(25, 5, utf8_decode('COD. PARTIDA'), 1, 0, 'C', 1);
        $pdf->Cell(140, 5, 'DESCRIPCION', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'MONTO', 1, 0, 'C', 1);

		$pdf->Ln(2);
		
        $pdf->SetFont('Arial', '', 7);
		$pdf->SetDrawColor(255, 255, 255);

        $this->_ReporteModelo = $this->metCargarModelo('reporte');
		
        $consulta= $this->_ReporteModelo-> getReporteEjecucionEspecifica($validacion['idpresupuesto'],$validacion['fec_anio'], $validacion['estado'], $validacion['desde'], $validacion['hasta']);
		
		//var_dump($consulta);
		//exit;
		$aespecifica = $this->_ReporteModelo->metAespecifica();
		
		$partidas_titulo= $this->_ReporteModelo-> getReportePartidasTitulo();
        $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(4);

       $montot=0; $montoat=0; $montoct=0; $montocat=0; $montopt=0; $montodt=0;
	   $suma_presupuestado=0; $suma_monto_distribucion=0;
	   $p=0;
	   
	   $cont=0;
	   
	   $pdf->SetWidths(array(25,140,25));
       $pdf->SetAligns(array('C','L','R'));
	   
	 //PRESUPUESTO POR PROYECTO 
	
	 
	 if(count($consulta)>0 && ($validacion['idpresupuesto']!='error' || $validacion['fec_anio']!='error'))
	  if($consulta[0]['ind_tipo_presupuesto']=='P')
      {
		  
		for($a=0;$a<count($aespecifica);$a++)
	    {
		   $cont=0;
		   $existeAespecifica = count($this->_ReporteModelo->metExisteAespecificaEs3($aespecifica[$a]['pk_num_aespecifica'], $validacion['idpresupuesto']));
		
		   $pdf->SetFont('Arial', 'B', 8);
		   $pdf->SetFillColor(202, 202, 202);
		   $pdf->SetDrawColor(202, 202, 202);
		   
		   if ($existeAespecifica>0)
		   {
		      $pdf->Cell(10, 5);
		      $pdf->Row(array(
                    utf8_decode($aespecifica[$a]['ind_cod_aespecifica']),
                    utf8_decode($aespecifica[$a]['ind_descripcion']),
                    ''));
			
		   }
		//-------------------------------------------
       for($i=0; $i< count($consulta); $i++)
        {
			
			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->Cell(25, 5, utf8_decode($consulta[$i]['ind_cod_presupuesto']), 0, 0, 'C', 0);
				
				$pdf->SetXY($x,$y);
			}
			
            $monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');  $montot=$montot+$consulta[$i]['num_monto_aprobado'];
			$montoa=number_format($consulta[$i]['num_monto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_monto_ajustado'];
			
			
		 if ($aespecifica[$a]['pk_num_aespecifica']==$consulta[$i]['fk_pr006_num_aespecifica'])
		  {
		     $pdf->SetWidths(array(25,140,25));
             $pdf->SetAligns(array('C','L','R'));
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(230, 230, 230);$pdf->SetDrawColor(230, 230, 230);
			
			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);
				
				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTituloEjecucionEspecifica($validacion['idpresupuesto'],$partida,$aespecifica[$a]['pk_num_aespecifica'],$validacion['estado'],$validacion['desde'], $validacion['hasta']);
					
					$suma_monto_distribucion=$suma['suma_monto_distribucion'];
					
					/*$suma_ajustado=$suma['suma_ajustado'];

					$suma_compromiso=$suma['suma_compromiso_dist'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;*/
				    $pdf->Cell(10, 5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),''));
				    $cont=$j+1;  
				}//FIN IF
				
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTituloEjecucionEspecifica($validacion['idpresupuesto'],$partida,$aespecifica[$a]['pk_num_aespecifica'],$validacion['estado'], $validacion['desde'], $validacion['hasta']);
					
					$suma_monto_distribucion=$suma['suma_monto_distribucion'];
					/*$suma_ajustado=$suma['suma_ajustado'];

					$suma_compromiso=$suma['suma_compromiso_dist'];
					$suma_disponible=$suma_ajustado-$suma_compromiso;*/

                    $pdf->Cell(10, 5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),number_format($suma_monto_distribucion,2,',','.') ));
				    $cont=$j+1;  
				}//FIN IF
			
			}//fin for
			
			
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);
			
			$pdf->Cell(10, 5);
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion'])));
				
				$pdf->SetDrawColor(255,255, 255);
				$pdf->Ln(1);
				
				$consulta_detalle= $this->_ReporteModelo-> getReporteEjecucionEspecificaDetalle($validacion['idpresupuesto'],$validacion['fec_anio'], $consulta[$i]['cod_partida'],$validacion['estado'], $validacion['desde'], $validacion['hasta']);
				$cont=0;
				for($j=$cont; $j< count($consulta_detalle); $j++)
			    {
				   $pdf->Row(array(
                       'OP-'.$consulta_detalle[$j]['ind_num_orden'],$consulta_detalle[$j]['ind_comentarios'],number_format($consulta_detalle[$j]['num_monto'],2,',','.')));

				}
			 
			 }
        
		}// FIN FOR IF
		
		    // $montot=number_format($montot,2,',','.'); $montoat=number_format($montoat,2,',','.');$montoct=number_format($montoct,2,',','.');
			// $montodt=number_format($montodt,2,',','.');
			
		
		 }//FIN FOR ACCION ESPECIFICA
	  }//FIN PRESUPUESTO POR PROYECTO
	  else
      {
	    for($i=0; $i< count($consulta); $i++)
        {
			
			if ($i==0)
			{
			    $x=$pdf->getX();
				$y=$pdf->getY();
				
			    $pdf->SetXY(50,36);
				$pdf->Cell(25, 5, utf8_decode($consulta[$i]['ind_cod_presupuesto']), 0, 0, 'C', 0);
				
				$pdf->SetXY($x,$y);
			}
			
            $pdf->SetWidths(array(25,140,25,25,25,25,25,25));
            $pdf->SetAligns(array('C','L','R','R','R','R','R','R','R'));
            
            $monto=number_format($consulta[$i]['num_monto_aprobado'],2,',','.');  $montot=$montot+$consulta[$i]['num_monto_aprobado'];
			$montoa=number_format($consulta[$i]['num_monto_ajustado'],2,',','.');  $montoat=$montoat+$consulta[$i]['num_monto_ajustado'];
			
			/*TITULOS DE PARTIDAS*/
			$pdf->SetFont('Arial', 'B', 7);
			$pdf->SetFillColor(202, 202, 202);$pdf->SetDrawColor(202, 202, 202);

			for($j=$cont; $j< count($partidas_titulo); $j++)
			{
			   
			    $codigo=substr_replace($consulta[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($consulta[$i]['cod_partida'],'00.00.00', 4,8);


				if (trim($codigo2)==trim($partidas_titulo[$j]['cod_partida'])){
				
				    $partida=substr($consulta[$i]['cod_partida'], 0, 3);
					 
					$suma=$this->_ReporteModelo-> getReporteMontosTituloEjecucionEspecifica($validacion['idpresupuesto'],$partida,false,$validacion['estado'], $validacion['desde'], $validacion['hasta']);

					$suma_monto_distribucion=$suma['suma_monto_distribucion'];

                    $suma_monto_distribucion = 0;
                    for($i2=0; $i2< count($consulta); $i2++)
                    {
                        $consulta_detalle= $this->_ReporteModelo->getReporteEjecucionEspecificaDetalle($validacion['idpresupuesto'],$validacion['fec_anio'], $consulta[$i2]['cod_partida'],$validacion['estado'], $validacion['desde'], $validacion['hasta']);
                        for($k=0; $k< count($consulta_detalle); $k++)
                        {
                            if($partidas_titulo[$j]['ind_partida']==$consulta[$i2]['ind_partida']) {
                                $suma_monto_distribucion = $suma_monto_distribucion + $consulta_detalle[$k]['num_monto'];
                            }
                        }
                    }
					
				    $pdf->Cell(10, 5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),number_format($suma_monto_distribucion,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
				
				if (trim($codigo)==trim($partidas_titulo[$j]['cod_partida'])){

                    $suma_monto_distribucion = 0;
                    for($i2=0; $i2< count($consulta); $i2++)
                    {
                        $consulta_detalle= $this->_ReporteModelo->getReporteEjecucionEspecificaDetalle($validacion['idpresupuesto'],$validacion['fec_anio'], $consulta[$i2]['cod_partida'],$validacion['estado'], $validacion['desde'], $validacion['hasta']);
                        for($k=0; $k< count($consulta_detalle); $k++)
                        {
                            if(
                                $partidas_titulo[$j]['ind_partida']==$consulta[$i2]['ind_partida'] AND
                                $partidas_titulo[$j]['ind_generica']==$consulta[$i2]['ind_generica'] AND
                                $partidas_titulo[$j]['ind_generica'] != '00'

                            ) {
                                $suma_monto_distribucion = $suma_monto_distribucion + $consulta_detalle[$k]['num_monto'];
                            }
                        }
                    }

				    $partida=substr($consulta[$i]['cod_partida'], 0, 6);
					
					$suma=$this->_ReporteModelo-> getReporteMontosTituloEjecucionEspecifica($validacion['idpresupuesto'],$partida,false,$validacion['estado'], $validacion['desde'], $validacion['hasta']);

#					$suma_monto_distribucion=$suma['suma_monto_distribucion'];
				
				    $pdf->Cell(10, 5);
				    $pdf->Row(array(
                    utf8_decode($partidas_titulo[$j]['cod_partida']),
                    utf8_decode($partidas_titulo[$j]['ind_denominacion']),number_format($suma_monto_distribucion,2,',','.')));
				    $cont=$j+1;  
				}//FIN IF
			
			}//fin for
			
			
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255);

			$partida=substr($consulta[$i]['cod_partida'], 0, 8);
			$suma=$this->_ReporteModelo-> getReporteMontosTituloEjecucionEspecifica($validacion['idpresupuesto'],$partida,false,$validacion['estado'], $validacion['desde'], $validacion['hasta']);
            $suma_monto_distribucion=$suma['suma_monto_distribucion'];
            $montoT = 0; $montoG = 0; $cont2=0;
            $consulta_detalle= $this->_ReporteModelo-> getReporteEjecucionEspecificaDetalle($validacion['idpresupuesto'],$validacion['fec_anio'], $consulta[$i]['cod_partida'],$validacion['estado'], $validacion['desde'], $validacion['hasta']);
            for($k=$cont2; $k< count($consulta_detalle); $k++)
            {
                  $montoT = $montoT + $consulta_detalle[$k]['num_monto'];
                  $montoG = $montoG + $montoT;
            }

			$pdf->Cell(10, 5);
            $pdf->Row(array(
                utf8_decode($consulta[$i]['cod_partida']),
                utf8_decode($consulta[$i]['ind_denominacion']),number_format($montoT,2,',','.')));
				
				$pdf->SetDrawColor(255,255, 255);
				$pdf->Ln(1);


				for($k=$cont2; $k< count($consulta_detalle); $k++)
			    {
				   $pdf->Cell(10, 5);
                    if(!isset($consulta_detalle[$k]['ind_orden'])){
                        $orden = 'OP-'.$consulta_detalle[$k]['ind_num_orden'];
                        $descripcion = $consulta_detalle[$k]['ind_comentarios'];
                        $fecha = $consulta_detalle[$k]['fec_orden_pago'];
                    }else{
                        $orden = $consulta_detalle[$k]['ind_tipo_orden'].'-'.$consulta_detalle[$k]['ind_orden'];
                        $descripcion = $consulta_detalle[$k]['ind_descripcion'];
                        $fecha = $consulta_detalle[$k]['fec_revision'];
                    }

                   $pdf->Row(array(
                       substr($fecha,0,10)
                    ));
                    $pdf->Cell(10, 5);

				   $pdf->Row(array(
                       $orden,
                       utf8_decode($descripcion),
                       number_format($consulta_detalle[$k]['num_monto'],2,',','.')
                   ));
				    
				}
        }
	  }
        $pdf->Output();

    }
	//------------------------------------------------------------------------------------------------------------------------------


    protected function FormatoFecha($fecha)
    {  
	 
        $resultado=explode("-",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
 
		return $resultado;

    }



}
