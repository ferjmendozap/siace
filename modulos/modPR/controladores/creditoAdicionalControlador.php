<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO                   |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                |  lbastardo@contraloriadebolivar.gob.ve |         0424-9080200           |
 * |   |                                           |                                        |                                |
 * |___|___________________________________________|________________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        01-09-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class creditoAdicionalControlador extends Controlador
{
    private $atCreditoAdicional;
	
    public function __construct()
    {
        parent::__construct();
        $this->atCreditoAdicional=$this->metCargarModelo('creditoAdicional');
	    $this->atPresupuesto=$this->metCargarModelo('presupuesto');
		$this->atAjuste=$this->metCargarModelo('ajuste');
		$this->atPartida=$this->metCargarModelo('partida','maestros');
		$this->atReformulacion=$this->metCargarModelo('reformulacion');
		
    }

    public function metIndex($status=false)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
			
        );
      
	   
	   $js = array('materialSiace/core/demo/DemoTableDynamic','ModPR/jquery.formatCurrency-1.4.0','ModPR/prFunciones');
		
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->atCreditoAdicional->metListarCreditoAdicional($status));
		$this->atVista->assign('status', $status);
        $this->atVista->metRenderizar('listado');
    }

   
     public function metJsonPresupuesto()
    {

        $anio = $this->metObtenerInt('anio');
		$filtro  = $this->atReformulacion->metListarPresupuestoAnio($anio);
        echo json_encode($filtro);
        exit;

    }



    public function metCrearModificar()
    {
	
	   $complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
			'inputmask/jquery.inputmask.bundle.min',
        );
		

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
	
        $valido=$this->metObtenerInt('valido');
        $idCreditoAdicional=$this->metObtenerInt('idCreditoAdicional');
		$status = $this->metObtenerAlphaNumerico('status');
		$inicial = $this->metObtenerAlphaNumerico('inicial');
		$suma_total=0;	
		$suma_total_d=0;
	 
		
        if($valido==1){
			   
        $this->metValidarToken();
		   
		   
           $Excceccion=array('monto','fk_prc002_num_presupuesto_det','ind_cod_credito_adicional','ind_estado');
		
			$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
            $formula=$this->metValidarFormArrayDatos('form','formula',$Excceccion);
			$formTxt=$this->metObtenerTexto('form','txt',$Excceccion);


		 
		  foreach ($alphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if(!empty($alphaNum[$tituloAlphaNum])){
                    $validacion[$tituloAlphaNum]=$valorAlphaNum;
                }else{
                   if ($tituloAlphaNum=='ind_cod_credito_adicional' || $tituloAlphaNum=='ind_estado')
					  $validacion[$tituloAlphaNum]='';
				    else		
                      $validacion[$tituloAlphaNum]='error';

                }
            }
		 
		 
		 foreach ($ind as $tituloInt => $valorInt) {
                if(!empty($ind[$tituloInt] ) && $ind[$tituloInt]>0 ){
                    $validacion[$tituloInt]=$valorInt;
                }else{
				    if ($tituloInt=='num_monto_total')
					  $validacion[$tituloInt]='error';
				    else		
                      $validacion[$tituloInt]='0,00';
                }
            }

		 
			
			 if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '' ) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
					      $validacion[$tituloTxt]='error';
                    }
                }
            }
		
       		
			 if(!isset($validacion['fk_prc002_num_presupuesto_det'])){
                $validacion['fk_prc002_num_presupuesto_det']=false;
            }
			
			
			if(!isset($validacion['monto'])){
                $validacion['monto']=false;
            }

            $validacion = array_merge($validacion,$formula);
			
	
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
		
		
			$validacion['num_monto_total']=$this->FormatoMonto($validacion['num_monto_total']);
			$validacion['fec_oficio']=$this->FormatoFecha($validacion['fec_oficio']); 
		    $validacion['fec_decreto']=$this->FormatoFecha($validacion['fec_decreto']);
			$validacion['fec_credito_adicional']=$this->FormatoFecha($validacion['fec_credito_adicional']);
			
		 if($status == 'PR') {	
			
			
            if($idCreditoAdicional==0){

	  $id=$this->atCreditoAdicional->metCrearCreditoAdicional( $validacion['fk_prb004_num_presupuesto'],$validacion['fec_anio'], $validacion['ind_numero_oficio'],$validacion['fec_oficio'], $validacion['ind_numero_decreto'],$validacion['fec_decreto'],$validacion['num_monto_total'],$validacion['ind_motivo'], $validacion['fk_prc002_num_presupuesto_det'], $validacion['monto'], $validacion['fec_credito_adicional']);
                $validacion['status']='nuevo';
            }else{
			
			
           $id=$this->atCreditoAdicional->metModificarCreditoAdicional( $validacion['fec_anio'], $validacion['ind_numero_oficio'],$validacion['fec_oficio'], $validacion['ind_numero_decreto'],$validacion['fec_decreto'],$validacion['num_monto_total'],$validacion['ind_motivo'], $validacion['fk_prc002_num_presupuesto_det'], $validacion['monto'],$validacion['fec_credito_adicional'], $idCreditoAdicional);
                $validacion['status']='modificar';
            }
			
			
			}
			else if($status=='AP' || $status =='AN' || $status =='RV' || $status =='RE'){
			
			   
                $id = $this->atCreditoAdicional->metAccionesCreditoAdicional($idCreditoAdicional,$status);
                
				if($status=='AP'){
                    $validacion['status'] = 'aprobado';
                }else if($status=='AN') {
                    $validacion['status'] = 'anulado';
                }else if($status=='RV') {
                    $validacion['status'] = 'revisado';
				}else if($status=='RE') {
                    $validacion['status'] = 'reversado';
                }
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idCreditoAdicional']=$id;

            echo json_encode($validacion);
            exit;
        }

        if($idCreditoAdicional!=0){
		   
            $this->atVista->assign('formDB',$this->atCreditoAdicional->metMostrarCreditoAdicional($idCreditoAdicional));
            $this->atVista->assign('idCreditoAdicional',$idCreditoAdicional);
			
			
			//AGREGADO PARA MOSTRAR LAS PARTIDAS--------------------
			 $partidaDetalle = $this->atCreditoAdicional->metMostrarCreditoAdicionalDetalle($idCreditoAdicional,$status);
			 
			 $partidaTitulo = $this->atPresupuesto->metMostrarPresupuestoTitulo();
			 
		 	 
		 if($partidaDetalle)
         if($partidaDetalle[0]['ind_tipo_presupuesto']=='P')
         {
		     $aespecifica = $this->atPartida->metAespecifica();
			 
			 $partida1='';
			 $partidat='';
			 
			 $suma=0;
			 $suma2=0;
			 $suma_total=0;
			 
			 $suma_d=0;
			 $suma2_d=0;
			 
			  
			 $f=0;
			 $p=0;
			 $cuenta=0;
			 
			 
			 
			 for($a=0;$a<count($aespecifica);$a++){
	    
				$suma=0;
				$suma2=0;
				$suma3=0;
				
				$suma_d=0;
				$suma2_d=0;
				$suma3_d=0;
				
				$existeAespecifica = count($this->atAjuste->metExisteAespecificaEs($aespecifica[$a]['pk_num_aespecifica'], $partidaDetalle[0]['pk_num_presupuesto']));
					 
				if ($existeAespecifica>0)
				{
					$partidas[$cuenta]=array(
					        'pk'=>0,
							'id'=>$aespecifica[$a]['pk_num_aespecifica'],
							'cod'=>$aespecifica[$a]['ind_cod_aespecifica'],
							'partida'=>$aespecifica[$a]['ind_descripcion'],
							'monto'=>0,
							'tipo'=>'E',
							'aespecifica'=>''
						);
						
				 }		  
				 
			 
			  
		for($i=0;$i<count($partidaDetalle);$i++){	
		
		
		   //Comparaci�n partidas 00.00
		   if (trim($partida1)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5))){
		      $suma_d=0;
		      $suma=0;
			  $f=0;
		   }
		   
		   if($f==0){
		      $partida1= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
			  $f=1;
			}  
		
		   
		   //Comparaci�n partidas 00.00.00
		   if (trim($partidat)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8))){
		      $suma2_d=0;
		      $suma2=0;
			  $p=0;
		   }
		   
		    if($p==0){
		      $partidat= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
			  $p=1;
			}  
		
		   
		  if ($aespecifica[$a]['pk_num_aespecifica']==$partidaDetalle[$i]['fk_pr006_num_aespecifica'])
		  {
		   
		     $suma_d=$suma_d+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso']; 
		     $suma=$suma+$partidaDetalle[$i]['num_monto_credito']; 
			 
			 $suma2_d=$suma2_d+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma2=$suma2+$partidaDetalle[$i]['num_monto_credito']; 
			 
			 
			 $suma_total_d=$suma_total_d+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso'];
			 $suma_total=$suma_total+$partidaDetalle[$i]['num_monto_credito'];
			 
		   
		     for($j=0;$j<count($partidaTitulo);$j++){	
			   
			    $cuenta=$cuenta+1;
			 
			    $codigo=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
				
				
				$existe=0;
				$posnum = 0;
				$pos=0; 
				 reset($partidas);
				 foreach($partidas as  $key=>$item)
                  {
				        if (isset($item['cod']))
						{
						
						   if(trim($partidaTitulo[$j]['cod_partida'])==trim($item['cod']) && $item['aespecifica']==$aespecifica[$a]['pk_num_aespecifica'] && $item['tipo']=='T')
						   {
						       $pos= $key;
							   //echo ($item['cod']."-".$item['aespecifica']."-".$pos);
						       $existe=1; 
							   break 1;
						   } 
						  
						}
				      $posnum++;      
                  } 
						
						
		 if ($existe==1)		
		 {				
				
			//---------------------------------------------	
				if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			   $partidas[$pos]=array(
			            'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma2_d,
						'monto'=>$suma2,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }
				
		  //-----------------------------------------------
			
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$pos]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma_d,
						'monto'=>$suma,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }	 
				  
				  }else {
				  
				  
				  //---------------------------------------------	
				if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			   $partidas[$cuenta]=array(
			            'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma2_d,
						'monto'=>$suma2,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }
				
		  //-----------------------------------------------
			
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$cuenta]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma_d,
						'monto'=>$suma,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }	 
				  }//FIN EXISTE
			    }//FIN FOR TITULO 
			 
                    $partidas[$cuenta]=array(
					    'pk'=>$partidaDetalle[$i]['pk_num_presupuesto_det'],
                        'id'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'],
                        'cod'=>$partidaDetalle[$i]['cod_partida'],
                        'partida'=>$partidaDetalle[$i]['ind_denominacion'],
						'monto_inicial'=>$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso'],
						'monto'=>$partidaDetalle[$i]['num_monto_credito'],
						'tipo'=>$partidaDetalle[$i]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
                    );
					
					
               }//FIN IF AESPECIFICA 
              }//FIN FOR DETALLE
			   $cuenta=$cuenta+1;
			}//FIN FOR AESPECIFICA
		 }//FIN PRESUPUESTO POR PROYECTO
		 else
		 {			 
			 $partida1='';
			 $partidat='';
			 
			 $suma=0;
			 $suma2=0;
			 $suma_total=0;
			 
			 $suma_d=0;
			 $suma2_d=0;
			 		  
			 $f=0;
			 $p=0;
			 
		for($i=0;$i<count($partidaDetalle);$i++){	
		
		   //Comparaci�n partdas 00.00
		   if (trim($partida1)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5))){
		      $suma_d=0;
		      $suma=0;
			  $f=0;
		   }
		   
		
		   if($f==0){
		      $partida1= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
			  $f=1;
		   }  
		
		  
		   //Comparaci�n partdas 00.00.00
		    if (trim($partidat)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8))){
		      $suma2_d=0;
		      $suma2=0;
			  $p=0;
		   }
		   
		   
		   if($p==0){
		      $partidat= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
			  $p=1;
			}  
		
		  
		   
		     $suma_d=$suma_d+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso']; 
		     $suma=$suma+$partidaDetalle[$i]['num_monto_credito']; 
			 
			 $suma2_d=$suma2_d+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma2=$suma2+$partidaDetalle[$i]['num_monto_credito']; 
			 
			 
			 $suma_total_d=$suma_total_d+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso'];
			 $suma_total=$suma_total+$partidaDetalle[$i]['num_monto_credito'];
			 
		   
		     for($j=0;$j<count($partidaTitulo);$j++){	
			   
			 
			    $codigo=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
				
			//---------------------------------------------	
				if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			   $partidas[$partidaTitulo[$j]['pk_num_partida_presupuestaria']]=array(
			            'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma2_d,
						'monto'=>$suma2,
						'tipo'=>$partidaTitulo[$j]['ind_tipo']
					
                    );
				   
				  }
				
		  //-----------------------------------------------
			
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$partidaTitulo[$j]['pk_num_partida_presupuestaria']]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma_d,
						'monto'=>$suma,
						'tipo'=>$partidaTitulo[$j]['ind_tipo']
					
                    );
				   
				  }	 
			    
			 }
			        
                    $partidas[$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria']]=array(
					    'pk'=>$partidaDetalle[$i]['pk_num_presupuesto_det'],
                        'id'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'],
                        'cod'=>$partidaDetalle[$i]['cod_partida'],
                        'partida'=>$partidaDetalle[$i]['ind_denominacion'],
						'monto_inicial'=>$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso'],
						'monto'=>$partidaDetalle[$i]['num_monto_credito'],
						'tipo'=>$partidaDetalle[$i]['ind_tipo']
                    );
					
               
            }
		  }//FIN TIPO DE PRESUPUESTO
			
			 if(isset($partidas)){
                $this->atVista->assign('partidas', $partidas);
            }
			
			
        }
		$this->atVista->assign('suma_total',$suma_total_d);
		$this->atVista->assign('suma_total_creditoadicional',$suma_total);

		$this->atVista->assign('status',$status);
	
		
		//-----------------------------------------------------------------------
	
		$this->atVista->assign('presupuesto',$this->atReformulacion->metPresupuestoListar());
		$this->atVista->metRenderizar('CrearModificar','modales');
		
    }
	
	
	//---------------------------------
	 public function metBuscarPartidas()
    {
	  $idCreditoAdicional=$this->metObtenerInt('idCreditoAdicional');
	  $status=$this->metObtenerInt('status');
	  $Presupuesto=$this->metObtenerInt('idPresupuesto');
	  $seleccionado=$this->metObtenerInt('seleccionado');
	  $suma_total_d=0;
	
	 if($idCreditoAdicional!=0){
		   
            $this->atVista->assign('formDB',$this->atCreditoAdicional->metMostrarCreditoAdicional($idCreditoAdicional));
            $this->atVista->assign('idCreditoAdicional',$idCreditoAdicional);
			
			
			//AGREGADO PARA MOSTRAR LAS PARTIDAS--------------------
			 $partidaDetalle = $this->atCreditoAdicional->metMostrarCreditoAdicionalDetalle($idCreditoAdicional,$status);
			 
			 $partidaTitulo = $this->atPresupuesto->metMostrarPresupuestoTitulo();
			 
		 	 
		 if($partidaDetalle)
         if($partidaDetalle[0]['ind_tipo_presupuesto']=='P')
         {
		     $aespecifica = $this->atPartida->metAespecifica();
			 
			 $partida1='';
			 $partidat='';
			 
			 $suma=0;
			 $suma2=0;
			 $suma_total=0;
			 
			 $suma_d=0;
			 $suma2_d=0;
			 
			  
			 $f=0;
			 $p=0;
			 $cuenta=0;
			 
			 
			 
			 for($a=0;$a<count($aespecifica);$a++){
	    
				$suma=0;
				$suma2=0;
				$suma3=0;
				
				$suma_d=0;
				$suma2_d=0;
				$suma3_d=0;
				
				$existeAespecifica = count($this->atAjuste->metExisteAespecificaEs($aespecifica[$a]['pk_num_aespecifica'], $partidaDetalle[0]['pk_num_presupuesto']));
					 
				if ($existeAespecifica>0)
				{
					$partidas[$cuenta]=array(
					        'pk'=>0,
							'id'=>$aespecifica[$a]['pk_num_aespecifica'],
							'cod'=>$aespecifica[$a]['ind_cod_aespecifica'],
							'partida'=>$aespecifica[$a]['ind_descripcion'],
							'monto'=>0,
							'tipo'=>'E',
							'aespecifica'=>''
						);
						
				 }		  
				 
			 
			  
		for($i=0;$i<count($partidaDetalle);$i++){	
		
		
		   //Comparaci�n partidas 00.00
		   if (trim($partida1)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5))){
		      $suma_d=0;
		      $suma=0;
			  $f=0;
		   }
		   
		   if($f==0){
		      $partida1= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
			  $f=1;
			}  
		
		   
		   //Comparaci�n partidas 00.00.00
		   if (trim($partidat)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8))){
		      $suma2_d=0;
		      $suma2=0;
			  $p=0;
		   }
		   
		    if($p==0){
		      $partidat= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
			  $p=1;
			}  
		

		   
		   
		  if ($aespecifica[$a]['pk_num_aespecifica']==$partidaDetalle[$i]['fk_pr006_num_aespecifica'])
		  {
		   
		     $suma_d=$suma_d+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso']; 
		     $suma=$suma+$partidaDetalle[$i]['num_monto_credito']; 
			 
			 $suma2_d=$suma2_d+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma2=$suma2+$partidaDetalle[$i]['num_monto_credito']; 
			 
			 
			 $suma_total_d=$suma_total_d+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso'];
			 $suma_total=$suma_total+$partidaDetalle[$i]['num_monto_credito'];
			 
		   
		     for($j=0;$j<count($partidaTitulo);$j++){	
			   
			    $cuenta=$cuenta+1;
			 
			    $codigo=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
				
				
				$existe=0;
				$posnum = 0;
				$pos=0; 
				 reset($partidas);
				 foreach($partidas as  $key=>$item)
                  {
				        if (isset($item['cod']))
						{
						
						   if(trim($partidaTitulo[$j]['cod_partida'])==trim($item['cod']) && $item['aespecifica']==$aespecifica[$a]['pk_num_aespecifica'] && $item['tipo']=='T')
						   {
						       $pos= $key;
							   //echo ($item['cod']."-".$item['aespecifica']."-".$pos);
						       $existe=1; 
							   break 1;
						   } 
						  
						}
				      $posnum++;      
                  } 
						
						
		 if ($existe==1)		
		 {				
				
			//---------------------------------------------	
				if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			   $partidas[$pos]=array(
			            'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma2_d,
						'monto'=>$suma2,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }
				
		  //-----------------------------------------------
			
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$pos]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma_d,
						'monto'=>$suma,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }	 
				  
				  }else {
				  
				  
				  //---------------------------------------------	
				if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			   $partidas[$cuenta]=array(
			            'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma2_d,
						'monto'=>$suma2,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }
				
		  //-----------------------------------------------
			
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$cuenta]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma_d,
						'monto'=>$suma,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }	 
				  }//FIN EXISTE
			    }//FIN FOR TITULO 
			 
                    $partidas[$cuenta]=array(
					    'pk'=>$partidaDetalle[$i]['pk_num_presupuesto_det'],
                        'id'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'],
                        'cod'=>$partidaDetalle[$i]['cod_partida'],
                        'partida'=>$partidaDetalle[$i]['ind_denominacion'],
						'monto_inicial'=>$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso'],
						'monto'=>$partidaDetalle[$i]['num_monto_credito'],
						'tipo'=>$partidaDetalle[$i]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
                    );
					
					
               }//FIN IF AESPECIFICA 
              }//FIN FOR DETALLE
			   $cuenta=$cuenta+1;
			}//FIN FOR AESPECIFICA
		 }//FIN PRESUPUESTO POR PROYECTO
		 else
		 {			 
			 $partida1='';
			 $partidat='';
			 
			 $suma=0;
			 $suma2=0;
			 $suma_total=0;
			 
			 $suma_d=0;
			 $suma2_d=0;
			 		  
			 $f=0;
			 $p=0;
			 
		for($i=0;$i<count($partidaDetalle);$i++){	
		
		   //Comparaci�n partdas 00.00
		   if (trim($partida1)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5))){
		      $suma_d=0;
		      $suma=0;
			  $f=0;
		   }
		   
		
		   if($f==0){
		      $partida1= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
			  $f=1;
		   }  
		
		  
		   //Comparaci�n partdas 00.00.00
		    if (trim($partidat)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8))){
		      $suma2_d=0;
		      $suma2=0;
			  $p=0;
		   }
		   
		   
		   if($p==0){
		      $partidat= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
			  $p=1;
			}  
		
		  
		   
		     $suma_d=$suma_d+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso']; 
		     $suma=$suma+$partidaDetalle[$i]['num_monto_credito']; 
			 
			 $suma2_d=$suma2_d+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso']; 
			 $suma2=$suma2+$partidaDetalle[$i]['num_monto_credito']; 
			 
			 
			 $suma_total_d=$suma_total_d+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso'];
			 $suma_total=$suma_total+$partidaDetalle[$i]['num_monto_credito'];
			 
		   
		     for($j=0;$j<count($partidaTitulo);$j++){	
			   
			 
			    $codigo=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
				
			//---------------------------------------------	
				if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			   $partidas[$partidaTitulo[$j]['pk_num_partida_presupuestaria']]=array(
			            'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma2_d,
						'monto'=>$suma2,
						'tipo'=>$partidaTitulo[$j]['ind_tipo']
					
                    );
				   
				  }
				
		  //-----------------------------------------------
			
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$partidaTitulo[$j]['pk_num_partida_presupuestaria']]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma_d,
						'monto'=>$suma,
						'tipo'=>$partidaTitulo[$j]['ind_tipo']
					
                    );
				   
				  }	 
			    
			 }
			        
                    $partidas[$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria']]=array(
					    'pk'=>$partidaDetalle[$i]['pk_num_presupuesto_det'],
                        'id'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'],
                        'cod'=>$partidaDetalle[$i]['cod_partida'],
                        'partida'=>$partidaDetalle[$i]['ind_denominacion'],
						'monto_inicial'=>$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso'],
						'monto'=>$partidaDetalle[$i]['num_monto_credito'],
						'tipo'=>$partidaDetalle[$i]['ind_tipo']
                    );
					
               
            }
		  }//FIN TIPO DE PRESUPUESTO
			
			 if(isset($partidas)){
                $this->atVista->assign('partidas', $partidas);
            }
			
			
			$this->atVista->assign('suma_total',$suma_total_d);
		    $this->atVista->assign('suma_total_creditoadicional',$suma_total);

		   $this->atVista->assign('status',$status);
			
        }
		
	
		
		//------------------------------------------------------------------------
		//------------------------------------------------------------------------
		
		if ($idCreditoAdicional==0)
		{
		
		//AGREGADO PARA MOSTRAR LAS PARTIDAS--------------------
		
		     $IdPresupuesto = $this->atCreditoAdicional->metIdPresupuesto();
			 
			 if($seleccionado==0)
			    $Presupuesto=$IdPresupuesto[0]['pk_num_presupuesto'];
			   
			 
			 if ($IdPresupuesto)
			 {
			    $codPresupuesto=  $IdPresupuesto[0]['ind_cod_presupuesto'];
			    $tipoPresupuesto= $IdPresupuesto[0]['ind_tipo_presupuesto'];
			    $partidaDetalle = $this->atPresupuesto->metMostrarPresupuestoDetalle($Presupuesto);
			 }
			 else
			 {
			   $codPresupuesto=  '0000';
			   $tipoPresupuesto= 'G';
			   $partidaDetalle = NULL;
			 }	 
				 
			 //$partidaDetalle = $this->atPresupuesto->metMostrarPresupuestoDetalle(9);
			 
			 
			 $partidaTitulo = $this->atPresupuesto->metMostrarPresupuestoTitulo();
			 
			 
		     if($partidaDetalle[0]['ind_tipo_presupuesto']=='P')
            {
			
			 $aespecifica = $this->atPartida->metAespecifica();
			 
			 
			 $partida1='';
			 $partidat='';
			 $partida2='';
			 
			 $suma=0;
			 $suma2=0;
			 $suma_total=0;
			
			 
			 $f=0;
			 $p=0;
			 $c=0;
			 $cuenta=0;
			 
			 
			 
			for($a=0;$a<count($aespecifica);$a++){
			
				$suma=0;
				$suma2=0;
				$suma3=0;
				
				$suma_d=0;
				$suma2_d=0;
				$suma3_d=0;
				
				$existeAespecifica = count($this->atAjuste->metExisteAespecificaEs($aespecifica[$a]['pk_num_aespecifica'], $partidaDetalle[0]['pk_num_presupuesto']));
					 
				if ($existeAespecifica>0)
				{
					$partidas[$cuenta]=array(
					        'pk'=>0,
							'id'=>$aespecifica[$a]['pk_num_aespecifica'],
							'cod'=>$aespecifica[$a]['ind_cod_aespecifica'],
							'partida'=>$aespecifica[$a]['ind_descripcion'],
							'monto'=>0,
							'tipo'=>'E',
							'aespecifica'=>''
						);
						
				  }		 
				 
			 
			  
		for($i=0;$i<count($partidaDetalle);$i++){	
		
		   //Comparaci�n partidas 00.00
		   if (trim($partida1)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5))){
		      $suma=0;
			  $f=0;
		   }
		   
		   if($f==0){
		      $partida1= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
			  $f=1;
			}  
		
    
	        //Comparaci�n partidas 00.00.00
            if (trim($partidat)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8))){
		      $suma2=0;
			  $p=0;
		    }
		   
		    if($p==0){
		      $partidat= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
			  $p=1;
			}  
		
		   
		    ///////////////////REVISAR-------------------------
		    if($c==0){
		      $partidat2= substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
			  $c=1;
			}  
		
		    if (trim($partidat2)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2))){
		      $suma3=0;
			  $c=0;
		    }   
		   ///////////////////REVISAR---------------------------
		   
		   if ($aespecifica[$a]['pk_num_aespecifica']==$partidaDetalle[$i]['fk_pr006_num_aespecifica'])
		   {
		      
		   
					 $suma=$suma+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso']; 
					 
					 $suma2=$suma2+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso']; 
					 
					 $suma_total=$suma_total+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso'];
					 
				   
					/*$suma_d=$suma_d+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso']; 
					 $suma=$suma+$partidaDetalle[$i]['num_monto_credito']; 
					 
					 $suma2_d=$suma2_d+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso']; 
					 $suma2=$suma2+$partidaDetalle[$i]['num_monto_credito']; 
					 
					 
					 $suma_total_d=$suma_total_d+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso'];
					 $suma_total=$suma_total+$partidaDetalle[$i]['num_monto_credito'];*/
					
					 
					 for($j=0;$j<count($partidaTitulo);$j++){	
					 
					     $cuenta=$cuenta+1;
					 
					   
						$codigo=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
						$codigo2=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
						$codigo3=substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
						
						
				$existe=0;
				$posnum = 0;
				$pos=0; 
				 reset($partidas);
				 foreach($partidas as  $key=>$item)
                  {
				        if (isset($item['cod']))
						{
						
						   if(trim($partidaTitulo[$j]['cod_partida'])==trim($item['cod']) && $item['aespecifica']==$aespecifica[$a]['pk_num_aespecifica'] && $item['tipo']=='T')
						   {
						       $pos= $key;
							   //echo ($item['cod']."-".$item['aespecifica']."-".$pos);
						       $existe=1; 
							   break 1;
						   } 
						  
						}
				      $posnum++;      
                  } 
						
						
		 if ($existe==1)		
		 {				
					//---------------------------------------------	
						if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
					   $partidas[$pos]=array(
								'pk'=>0,
								'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
								'cod'=>$partidaTitulo[$j]['cod_partida'],
								'partida'=>$partidaTitulo[$j]['ind_denominacion'],
								'monto_inicial'=>$suma2,
								'monto'=>0,
								'tipo'=>$partidaTitulo[$j]['ind_tipo'],
								'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
							
							);
						   
						  }
							
			 //-----------------------------------------------
			
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$pos]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma,
						'monto'=>0,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }
				  
			 //-----------------------------------------------				  
				  
				   if (trim($codigo3)==trim($partidaTitulo[$j]['cod_partida'])){
					   $partidas[$pos]=array(
							'pk'=>0,
							'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
							'cod'=>$partidaTitulo[$j]['cod_partida'],
							'partida'=>$partidaTitulo[$j]['ind_denominacion'],
							'monto_inicial'=>$suma3,
							'monto'=>0,
							'tipo'=>$partidaTitulo[$j]['ind_tipo'],
							'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }
				  
			   }else{ //ELSE EXISTE
			   
			     	//---------------------------------------------	
						if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
					   $partidas[$cuenta]=array(
								'pk'=>0,
								'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
								'cod'=>$partidaTitulo[$j]['cod_partida'],
								'partida'=>$partidaTitulo[$j]['ind_denominacion'],
								'monto_inicial'=>$suma2,
								'monto'=>0,
								'tipo'=>$partidaTitulo[$j]['ind_tipo'],
								'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
							
							);
						   
						  }
				
				
			 //-----------------------------------------------
			
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$cuenta]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma,
						'monto'=>0,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }
				  
				   if (trim($codigo3)==trim($partidaTitulo[$j]['cod_partida'])){
					   $partidas[$cuenta]=array(
							'pk'=>0,
							'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
							'cod'=>$partidaTitulo[$j]['cod_partida'],
							'partida'=>$partidaTitulo[$j]['ind_denominacion'],
							'monto_inicial'=>$suma3,
							'monto'=>0,
							'tipo'=>$partidaTitulo[$j]['ind_tipo'],
							'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
					
                    );
				   
				  }
				  
			   }//FIN EXISTE	 
			 }//FIN FOR J
			 
                    $partidas[$cuenta]=array(
					    'pk'=>$partidaDetalle[$i]['pk_num_presupuesto_det'],
                        'id'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'],
                        'cod'=>$partidaDetalle[$i]['cod_partida'],
                        'partida'=>$partidaDetalle[$i]['ind_denominacion'],
						'monto_inicial'=>$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso'],
						'monto'=>0,
						'tipo'=>$partidaDetalle[$i]['ind_tipo'],
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
                    );	
					
					
				}//FIN IF AESPECIFICA 	
                
            }//FIN FOR I
			 $cuenta=$cuenta+1;
		}//FIN FOR AESPECIFICA	
			
			 if(isset($partidas)){
                $this->atVista->assign('partidas', $partidas);
            }
			$this->atVista->assign('suma_total',$suma_total);
			$this->atVista->assign('formDB',$this->atCreditoAdicional->metSelectPresupuesto());
			$this->atVista->assign('codPresupuesto',$codPresupuesto);
			$this->atVista->assign('tipoPresupuesto',$tipoPresupuesto);
			
		}	
		else	//TIPO PRESUPUESTO
		{
		     $partida1='';
			 $partidat='';
			 
			 $suma=0;
			 $suma2=0;
			 $suma_total=0;
			
			 
			 $f=0;
			 $p=0;
			 
			  
		for($i=0;$i<count($partidaDetalle);$i++){	
		
		   //Comparaci�n Partidas 00.00
		   if (trim($partida1)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5))){
		      $suma=0;
			  $f=0;
		   }
		   
		   
		   if($f==0){
		      $partida1= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
			  $f=1;
			}  
		

           //Comparaci�n Partidas 00.00.00
		   if (trim($partidat)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8))){
		      $suma2=0;
			  $p=0;
		   }
		    
		    if($p==0){
		      $partidat= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
			  $p=1;
			}  
		

		 
		     $suma=$suma+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso']; 
			 
			 $suma2=$suma2+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso']; 
			 
			 $suma_total=$suma_total+$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso'];
			
			 
		   
		     for($j=0;$j<count($partidaTitulo);$j++){	
			 
			   
			 
			    $codigo=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
				
			//---------------------------------------------	
				if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			   $partidas[$partidaTitulo[$j]['pk_num_partida_presupuestaria']]=array(
			            'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma2,
						'monto'=>0,
						'tipo'=>$partidaTitulo[$j]['ind_tipo']
					
                    );
				   
				  }
				
				
			 //-----------------------------------------------
			
				
			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$partidaTitulo[$j]['pk_num_partida_presupuestaria']]=array(
				        'pk'=>0,
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto_inicial'=>$suma,
						'monto'=>0,
						'tipo'=>$partidaTitulo[$j]['ind_tipo']
					
                    );
				   
				  }
				 
			 }
			        
                    $partidas[$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria']]=array(
					    'pk'=>$partidaDetalle[$i]['pk_num_presupuesto_det'],
                        'id'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'],
                        'cod'=>$partidaDetalle[$i]['cod_partida'],
                        'partida'=>$partidaDetalle[$i]['ind_denominacion'],
						'monto_inicial'=>$partidaDetalle[$i]['num_monto_ajustado']-$partidaDetalle[$i]['num_monto_compromiso'],
						'monto'=>0,
						'tipo'=>$partidaDetalle[$i]['ind_tipo']
                    );	
                
            }//FIN FOR I
			
			
			 if(isset($partidas)){
                $this->atVista->assign('partidas', $partidas);
            }
			$this->atVista->assign('suma_total',$suma_total);
			$this->atVista->assign('formDB',$this->atCreditoAdicional->metSelectPresupuesto());
			$this->atVista->assign('codPresupuesto',$codPresupuesto); 
			$this->atVista->assign('tipoPresupuesto',$tipoPresupuesto); 
			
			
		
		}//FIN TIPO PRESUPUESTO
			
				
		}//fin Inicial
		//-----------------------------------------------------------------------
	
		$this->atVista->assign('suma_total_creditoadicional',$suma_total);
		$this->atVista->metRenderizar('ListadoPartidas');
		
    }

	
	//---------------------------------
	
	

   

    public function metEliminar()
    {
        $idCreditoAdicional = $this->metObtenerInt('idCreditoAdicional');
		 
		
        if($idCreditoAdicional!=0){
            $id=$this->atCreditoAdicional->metEliminarCreditoAdicional($idCreditoAdicional);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el antepresupuesto se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idCreditoAdicional'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
	
	
	protected function FormatoFecha($fecha)
    {
        $resultado=explode("-",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }	
	
	
	protected function FormatoMonto($monto_f)
    {
       if(!isset($monto_f)){
                $monto_f=0;
            }
			else {
			     $monto_f=str_replace(".", "", $monto_f);
			     $monto_f=str_replace(",", ".", $monto_f);
				 
			}
			 return $monto_f;
    }	
	
	
	
}