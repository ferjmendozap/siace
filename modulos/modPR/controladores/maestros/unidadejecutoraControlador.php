<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                |      liduvica@hotmail.com          |         0424-9080200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        29-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class unidadejecutoraControlador extends Controlador
{
    private $atUnidadEjecutora;

    public function __construct()
    {
        parent::__construct();
        //Session::metAcceso();
        $this->atUnidadEjecutora = $this->metCargarModelo('unidadejecutora','maestros');
		$this->atAntepresupuesto= $this->metCargarModelo('antepresupuesto');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
       
		$js = array('materialSiace/core/demo/DemoTableDynamic','ModPR/jquery.formatCurrency-1.4.0','ModPR/prFunciones');
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

      
        $listado = $this->atUnidadEjecutora->metListarMiscelaneos();
		
		$this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());	
        $this->atVista->assign('listado', $listado);
        $this->atVista->metRenderizar('listado');
    }


    public function metCrearModificar()
    {
     /*   $complementosCss = array(
            'select2/select201ef',
        );
        $complementosJs = array(
            'select2/select2.min',
        );
       $js = array('ModPR/prFunciones');
		
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);*/

        $idUnidadEjecutora = $this->metObtenerInt('idUnidadEjecutora');
        $valido = $this->metObtenerInt('valido');
        if ($valido === 1) {
            $excepcion = array('num_estatus');
            $alphaNum = $this->metValidarFormArrayDatos('form', 'txt');
            $ind = $this->metValidarFormArrayDatos('form', 'int', $excepcion);
            if ($alphaNum != null && $ind == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null) {
                $validacion = $ind;
            } else {
                $validacion = array_merge($alphaNum, $ind);
            }

            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }

            if (!isset($validacion['num_estatus'])) {
                $validacion['num_estatus'] = 0;
            }

            if (!isset($validacion['num_estatusDet'])) {
                $validacion['num_estatusDet'] = false;
            }
			
			 if (!isset($validacion['num_estatusDetRes'])) {
                $validacion['num_estatusDetRes'] = false;
            }
			
			 if (!isset($validacion['cod_responsable'])) {
                $validacion['cod_responsable'] = false;
            }

            if ($idUnidadEjecutora == 0) {
                $validacion['status'] = 'nuevo';
                $id = $this->atUnidadEjecutora->metCrearUnidadEjecutora($validacion['ind_nombre_unidad_ejecutora'], $validacion['ind_descripcion'],
                    $validacion['cod_unidad_ejecutora'], $validacion['fk_a001_num_organismo'],
                    $validacion['num_estatus'], $validacion['ind_nombre_unidad_ejecutora_dependencia'], $validacion['num_estatusDet'], 
					$validacion['cod_unidad_ejecutora_dependencia'], $validacion['fk_a023_num_centro_de_costo'],
					$validacion['num_estatusDetRes'], $validacion['cod_responsable']);
					
					
            } else {
                $validacion['status'] = 'modificar';
				
				
                $id = $this->atUnidadEjecutora->metActualizarMiscelaneo($idUnidadEjecutora, $validacion['ind_nombre_unidad_ejecutora'], $validacion['ind_descripcion'],
                    $validacion['cod_unidad_ejecutora'], $validacion['fk_a001_num_organismo'], $validacion['num_estatus'],
                    $validacion['ind_nombre_unidad_ejecutora_dependencia'], $validacion['num_estatusDet'], $validacion['pk_num_unidad_ejecutora_dependencia'], 
					$validacion['cod_unidad_ejecutora_dependencia'], $validacion['fk_a023_num_centro_de_costo'],
					$validacion['ind_nombre_responsable'], $validacion['num_estatusDetRes'],$validacion['pk_num_responsable'],  $validacion['cod_responsable']);
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])){
                        if (strpos($id[2], (string)$validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idUnidadEjecutora'] = $id;
            echo json_encode($validacion);
            exit;
        }
        
		if ($idUnidadEjecutora != 0) {
		
		
            $miscelaneo = $this->atUnidadEjecutora->metMostrarMiscelaneo($idUnidadEjecutora);
            $miscelaneoDetalle = $this->atUnidadEjecutora->metMostrarSelect($miscelaneo['cod_unidad_ejecutora']);
			$miscelaneoDetalleRes = $this->atUnidadEjecutora->metMostrarResSelect($miscelaneo['cod_unidad_ejecutora']);
		
			
            $this->atVista->assign('numero', 1);
            $this->atVista->assign('n', 0);
            $this->atVista->assign('formDB', $miscelaneo);
            $this->atVista->assign('formDBSelect', $miscelaneoDetalle);
			
			$this->atVista->assign('numerores', 1);
            $this->atVista->assign('r', 0);
			$this->atVista->assign('formDBSelectRes', $miscelaneoDetalleRes);
			
        }
		
       // $this->atVista->assign('aplicacion', $this->atUnidadEjecutora->metListarAplicacion());
		
		$this->atVista->assign('organismoint',$this->atAntepresupuesto->metListarOrganismoInt());	
        $this->atVista->assign('idUnidadEjecutora', $idUnidadEjecutora);
        $this->atVista->metRenderizar('CrearModificar', 'modales');
    }
	
	 public function metEliminar()
    {
        $idUnidadEjecutora = $this->metObtenerInt('idUnidadEjecutora');
		 
		
        if($idUnidadEjecutora!=0){
            $id=$this->atUnidadEjecutora->metEliminarUnidadEjecutora($idUnidadEjecutora);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el antepresupuesto se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idUnidadEjecutora'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
	

}
