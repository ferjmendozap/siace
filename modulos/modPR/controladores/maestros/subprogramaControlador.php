<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                |      liduvica@hotmail.com          |         0424-9080200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class subprogramaControlador extends Controlador
{
    private $atSubprograma;

    public function __construct()
    {
        parent::__construct();
        $this->atSubprograma=$this->metCargarModelo('subprograma','maestros');
		$this->atPrograma=$this->metCargarModelo('programa','maestros');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
		
        $this->atVista->assign('listado',$this->atSubprograma->metListarSubprograma());
        $this->atVista->metRenderizar('listado');
    }
	
	

    public function metCrearModificar()
    {
        $valido=$this->metObtenerInt('valido');
        $idSubprograma=$this->metObtenerInt('idSubprograma');
	
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('num_estatus', 'ind_cod_subprograma');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
			
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
			
			 
            if($alphaNum!=null && $ind==null){
                $validacion=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $validacion=$ind;
            }else{
                $validacion=array_merge($alphaNum,$ind);
            }


            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
           
            
            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            }

            if($idSubprograma==0){
                $id=$this->atSubprograma->metCrearSubprograma($validacion['ind_descripcion'],$validacion['num_estatus'],$validacion['fk_prb009_num_programa'],$validacion['num_cod_subprograma']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atSubprograma->metModificarSubprograma($validacion['ind_descripcion'],$validacion['num_estatus'],$validacion['fk_prb009_num_programa'],$validacion['num_cod_subprograma'], $idSubprograma);
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idSubprograma']=$id;

            echo json_encode($validacion);
            exit;
        }

        if($idSubprograma!=0){
            $this->atVista->assign('formDB',$this->atSubprograma->metMostrarSubprograma($idSubprograma));
            $this->atVista->assign('idSubprograma',$idSubprograma);
        }
		
        $this->atVista->assign('programa',$this->atPrograma->metListarPrograma());
        $this->atVista->metRenderizar('CrearModificar','modales');
    }


    public function metEliminar()
    {
        $idSubprograma = $this->metObtenerInt('idSubprograma');
		
        if($idSubprograma!=0){
            $id=$this->atSubprograma->metEliminarSubprograma($idSubprograma);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Sub-programa se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idSubprograma'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
}