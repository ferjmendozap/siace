<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                |      liduvica@hotmail.com          |         0424-9080200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class actividadControlador extends Controlador
{
    private $atActividad;

    public function __construct()
    {
        parent::__construct();
        $this->atActividad=$this->metCargarModelo('actividad','maestros');
		$this->atProyecto=$this->metCargarModelo('proyecto','maestros');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
		
        $this->atVista->assign('listado',$this->atActividad->metListarActividad());
        $this->atVista->metRenderizar('listado');
    }
	
	

    public function metCrearModificar()
    {
        $valido=$this->metObtenerInt('valido');
        $idActividad=$this->metObtenerInt('idActividad');
	
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('num_estatus', 'ind_cod_actividad','fk_prb005_num_proyecto');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
			
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
			
			 
            if($alphaNum!=null && $ind==null){
                $validacion=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $validacion=$ind;
            }else{
                $validacion=array_merge($alphaNum,$ind);
            }


            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
           
            
            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            }

             if(!isset($validacion['num_flags_aespecifica'])){
                $validacion['num_flags_aespecifica']=0;
            }
           

            if($idActividad==0){
			
			
               $id=$this->atActividad->metCrearActividad($validacion['ind_descripcion'],$validacion['num_estatus'],$validacion['fk_prb005_num_proyecto'],$validacion['num_cod_actividad'], $validacion['num_flags_aespecifica']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atActividad->metModificarActividad($validacion['ind_descripcion'],$validacion['num_estatus'],$validacion['fk_prb005_num_proyecto'],$validacion['num_cod_actividad'], $validacion['num_flags_aespecifica'], $idActividad);
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idActividad']=$id;

            echo json_encode($validacion);
            exit;
        }

        if($idActividad!=0){
            $this->atVista->assign('formDB',$this->atActividad->metMostrarActividad($idActividad));
			
            $this->atVista->assign('idActividad',$idActividad);
        }
		
        $this->atVista->assign('proyecto',$this->atProyecto->metListarProyecto());
        $this->atVista->metRenderizar('CrearModificar','modales');
    }


    public function metEliminar()
    {
        $idActividad = $this->metObtenerInt('idActividad');
		
        if($idActividad!=0){
            $id=$this->atActividad->metEliminarActividad($idActividad);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero la Actividad se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idActividad'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
}