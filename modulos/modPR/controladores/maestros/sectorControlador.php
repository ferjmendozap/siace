<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                |      liduvica@hotmail.com          |         0424-9080200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class sectorControlador extends Controlador
{
    private $atSector;

    public function __construct()
    {
        parent::__construct();
        $this->atSector=$this->metCargarModelo('sector','maestros');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
		
        $this->atVista->assign('listado',$this->atSector->metListarSector());
        $this->atVista->metRenderizar('listado');
    }
	
	

    public function metCrearModificar()
    {
        $valido=$this->metObtenerInt('valido');
        $idSector=$this->metObtenerInt('idSector');
	
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('num_estatus', 'ind_cod_sector');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
			
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
			
			 
            if($alphaNum!=null && $ind==null){
                $validacion=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $validacion=$ind;
            }else{
                $validacion=array_merge($alphaNum,$ind);
            }


            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
           
            
            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            }

            if($idSector==0){
			
			
               $id=$this->atSector->metCrearSector($validacion['ind_descripcion'],$validacion['num_estatus']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atSector->metModificarSector($validacion['ind_descripcion'],$validacion['num_estatus'],$idSector);
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idSector']=$id;

            echo json_encode($validacion);
            exit;
        }

        if($idSector!=0){
            $this->atVista->assign('formDB',$this->atSector->metMostrarSector($idSector));
			
            $this->atVista->assign('idSector',$idSector);
        }

        $this->atVista->metRenderizar('CrearModificar','modales');
    }


    public function metEliminar()
    {
        $idSector = $this->metObtenerInt('idSector');
		
        if($idSector!=0){
            $id=$this->atSector->metEliminarSector($idSector);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Sector se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idSector'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
}