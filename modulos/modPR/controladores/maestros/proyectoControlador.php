<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                |      liduvica@hotmail.com          |         0424-9080200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class proyectoControlador extends Controlador
{
    private $atProyecto;

    public function __construct()
    {
        parent::__construct();
        $this->atProyecto=$this->metCargarModelo('proyecto','maestros');
		$this->atSubPrograma=$this->metCargarModelo('subprograma','maestros');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
		
        $this->atVista->assign('listado',$this->atProyecto->metListarProyecto());
        $this->atVista->metRenderizar('listado');
    }
	
	

    public function metCrearModificar()
    {
        $valido=$this->metObtenerInt('valido');
        $idProyecto=$this->metObtenerInt('idProyecto');
		
		//$valido==1;
	
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('num_estatus', 'ind_cod_proyecto');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
			
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
			
			 
            if($alphaNum!=null && $ind==null){
                $validacion=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $validacion=$ind;
            }else{
                $validacion=array_merge($alphaNum,$ind);
            }


            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
           
            
            if(!isset($validacion['num_estatus'])){  
                $validacion['num_estatus']=0;
            }
			
			
			if(!isset($validacion['num_flags_acentralizada'])){  
                $validacion['num_flags_acentralizada']=0;
            }

            if($idProyecto==0){
			
			   // $id=$this->atProyecto->metCrearProyecto('prueba',0);
               $id=$this->atProyecto->metCrearProyecto($validacion['ind_descripcion'],$validacion['num_estatus'],$validacion['fk_prb010_num_subprograma'],$validacion['num_cod_proyecto'], $validacion['num_flags_acentralizada']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atProyecto->metModificarProyecto($validacion['ind_descripcion'],$validacion['num_estatus'],$validacion['fk_prb010_num_subprograma'],$validacion['num_cod_proyecto'],$validacion['num_flags_acentralizada'],$idProyecto);
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idProyecto']=$id;

            echo json_encode($validacion);
            exit;
        }


        if($idProyecto!=0){
            $this->atVista->assign('formDB',$this->atProyecto->metMostrarProyecto($idProyecto));
			
            $this->atVista->assign('idProyecto',$idProyecto);
        }

        $this->atVista->assign('subprograma',$this->atSubPrograma->metListarSubPrograma());
        $this->atVista->metRenderizar('CrearModificar','modales');
		
    }


    public function metEliminar()
    {
        $idProyecto = $this->metObtenerInt('idProyecto');
		
        if($idProyecto!=0){
            $id=$this->atProyecto->metEliminarProyecto($idProyecto);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Proyecto se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idProyecto'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
}