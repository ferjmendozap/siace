<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO                   |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                |  lbastardo@contraloriadebolivar.gob.ve |         0424-9080200           |
 * |   |                                           |                                        |                                |
 * |___|___________________________________________|________________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        01-09-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class reformulacionControlador extends Controlador
{
    private $atReformulacion;
	
    public function __construct()
    {
        parent::__construct();
        $this->atReformulacion=$this->metCargarModelo('reformulacion');
		$this->atPresupuesto=$this->metCargarModelo('presupuesto');
		$this->atPartida=$this->metCargarModelo('partida','maestros');
		
    }

    public function metIndex($status=false)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
			
        );
   
		$js = array('materialSiace/core/demo/DemoTableDynamic','ModPR/jquery.formatCurrency-1.4.0','ModPR/prFunciones');
		//$js[] = 'ModPR/prFunciones';
		
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->atReformulacion->metListarReformulacion($status));
		$this->atVista->assign('status', $status);
        $this->atVista->metRenderizar('listado');
    }

    public function metJsonPresupuesto()
    {

        $anio = $this->metObtenerInt('anio');
		$filtro  = $this->atReformulacion->metListarPresupuestoAnio($anio);
        echo json_encode($filtro);
        exit;

    }


    public function metCrearModificar()
    {
	
	   $complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
			'inputmask/jquery.inputmask.bundle.min',
        );
		
        
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
	
        $valido=$this->metObtenerInt('valido');
        $idReformulacion=$this->metObtenerInt('idReformulacion');
		$status = $this->metObtenerAlphaNumerico('status');
		$suma_total=0;	 
		
		 
		
        if($valido==1){
		
		 
          $this->metValidarToken();
		  $Excceccion=array('fk_prb002_num_partida_presupuestaria', 'Aespecifica','cod_reformulacion','ind_tipo_presupuesto','ind_estado','num_monto_reformulacion');
		
			$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
			$formTxt=$this->metObtenerTexto('form','txt',$Excceccion);
			
		
			
          if($alphaNum!=null && $ind==null){
                $validacion=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $validacion=$ind;
            }else{
                $validacion=array_merge($alphaNum,$ind);
          }
        
		 
		 foreach ($alphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if(!empty($alphaNum[$tituloAlphaNum])){
                    $validacion[$tituloAlphaNum]=$valorAlphaNum;
                }else{
                   if ($tituloAlphaNum=='cod_reformulacion' || $tituloAlphaNum=='ind_estado')
					  $validacion[$tituloAlphaNum]='';
				    else		
                      $validacion[$tituloAlphaNum]='error';

                }
            }
		 
		 
		  foreach ($ind as $tituloInt => $valorInt) {
                if(!empty($ind[$tituloInt]) && $ind[$tituloInt]>0 ){
                    $validacion[$tituloInt]=$valorInt;
                }else{
					/*if ($tituloInt=='num_monto_reformulacion')
					  $validacion[$tituloInt]='error';
				    else	*/	
                      $validacion[$tituloInt]='0,00';
                }
            }
		 
		 
           if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
			
			
			 if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }
			
		   //ACCION ESPECIFICA
			if(!isset($validacion['Aespecifica'])){
                $validacion['Aespecifica']=false;
            }	
			
           if(!isset($validacion['fk_prb002_num_partida_presupuestaria'])){
                $validacion['fk_prb002_num_partida_presupuestaria']=false;
            }
			
			if(!isset($validacion['monto'])){
                $validacion['monto']=false;
            }
			
			$validacion['num_monto_reformulacion']=$this->FormatoMonto($validacion['num_monto_reformulacion']); 
			$validacion['fec_reformulacion']=$this->FormatoFecha($validacion['fec_reformulacion']); 
			$validacion['fec_gaceta']=$this->FormatoFecha($validacion['fec_gaceta']); 
		    $validacion['fec_resolucion']=$this->FormatoFecha($validacion['fec_resolucion']);
			$anio=$validacion['fec_anio'];
			$mes=substr($validacion['fec_reformulacion'], 5,2);
			
			//$pos = strpos($validacion['fk_prb004_num_presupuesto'], '|');
			
			
			
	    if($status == 'PR') {
			
            if($idReformulacion==0){
			
			$validacion['fk_prb004_num_presupuesto'] = trim($validacion['fk_prb004_num_presupuesto'],'G');
			
			
			$id=$this->atReformulacion->metCrearReformulacion( $validacion['fk_prb004_num_presupuesto'],$anio, $mes,$validacion['ind_numero_gaceta'],$validacion['fec_gaceta'], $validacion['ind_numero_resolucion'],$validacion['fec_resolucion'],$validacion['num_monto_reformulacion'],$validacion['ind_descripcion'], $validacion['fk_prb002_num_partida_presupuestaria'], $validacion['monto'], $validacion['Aespecifica']);
			
			if ($id<0)
			   $validacion['status']='existe';
			else  
			   $validacion['status']='nuevo';
				
				
            }else{
			
				
          $id=$this->atReformulacion->metModificarReformulacion($validacion['fk_prb004_num_presupuesto'],$anio, $mes,$validacion['ind_numero_gaceta'],$validacion['fec_gaceta'], $validacion['ind_numero_resolucion'],$validacion['fec_resolucion'],$validacion['num_monto_reformulacion'],$validacion['ind_descripcion'], $validacion['fk_prb002_num_partida_presupuestaria'], $validacion['monto'],$validacion['Aespecifica'],$idReformulacion);
                
				if ($id<0) 
			      $validacion['status']='existe';
			    else  
				 $validacion['status']='modificar';
            }
			
			
		}
	    else if($status=='AP' || $status =='AN'){
            $id = $this->atReformulacion->metAccionesReformulacion($idReformulacion,$status);
			
        if($status=='AP'){
             $validacion['status'] = 'aprobado';
        }else{
             $validacion['status'] = 'anulado';
        } 
    }
	

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idReformulacion']=$id;

            echo json_encode($validacion);
            exit;
        }

        
		
        if($idReformulacion!=0){
		    //$this->atVista->assign('presupuesto',$this->atReformulacion->metPresupuestoListarTodos());
            $this->atVista->assign('formDB',$this->atReformulacion->metMostrarReformulacion($idReformulacion));
            $this->atVista->assign('idReformulacion',$idReformulacion);
			
			
			//AGREGADO PARA MOSTRAR LAS PARTIDAS--------------------
			 $partidaDetalle = $this->atReformulacion->metMostrarReformulacionDetalle($idReformulacion);
			 
			 $partidaTitulo = $this->atReformulacion->metMostrarReformulacionTitulo();
			
			 
		 if($partidaDetalle)
         if($partidaDetalle[0]['ind_tipo_presupuesto']=='P')
         {	
		   
		      $aespecifica = $this->atPartida->metAespecifica();
			  
		      $partida1='';
			  $partidat='';
			  $partidat2='';
			 
			  $suma=0;
			  $suma2=0;
			  $suma3=0;
			  $suma_total=0;
			 
			  $f=0;
			  $p=0;
			  $c=0;
			  $cuenta=0;
			  
	    for($a=0;$a<count($aespecifica);$a++){
	    
	        $suma=0;
			$suma2=0;
			$suma3=0;
			$existeAespecifica=0;
			
			
			 $existeAespecifica = count($this->atReformulacion->metExisteAespecificaEs($aespecifica[$a]['pk_num_aespecifica'], $idReformulacion));
				 
			if ($existeAespecifica>0)
			{
		        $partidas[$cuenta]=array(
                        'id'=>$aespecifica[$a]['pk_num_aespecifica'],
                        'cod'=>$aespecifica[$a]['ind_cod_aespecifica'],
                        'partida'=>$aespecifica[$a]['ind_descripcion'],
						'monto'=>0,
						'tipo'=>'E',
						'aespecifica'=>$aespecifica[$a]['pk_num_aespecifica']
                    );
					
					
			 }		  
			 
			  
		for($i=0;$i<count($partidaDetalle);$i++){	
		
		
		   if($f==0){
		      $partida1= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
			  $f=1;
			}  
		
		   if (trim($partida1)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5))){
		      $suma=0;
			  $f=0;
		   }
		   
		   
		    if($p==0){
		      $partidat= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
			  $p=1;
			}  
		
		   if (trim($partidat)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8))){
		      $suma2=0;
			  $p=0;
		   }
		   
		   
		    ///////////////////REVISAR-------------------------
		    if($c==0){
		      $partidat2= substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
			  $c=1;
			}  
		
		    if (trim($partidat2)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2))){
		      $suma3=0;
			  $c=0;
		    }   
		   ///////////////////REVISAR---------------------------
		   
		  if ($aespecifica[$a]['pk_num_aespecifica']==$partidaDetalle[$i]['fk_pr_b006_aespecifica'])
		  {
		     $suma=$suma+$partidaDetalle[$i]['num_monto']; 
			 $suma2=$suma2+$partidaDetalle[$i]['num_monto']; 
			 $suma_total=$suma_total+$partidaDetalle[$i]['num_monto'];
		   
		   
		     for($j=0;$j<count($partidaTitulo);$j++){	
			 
			    $cuenta=$cuenta+1;
			   		 
			    $codigo=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
				$codigo3=substr_replace($partidaDetalle[$i]['cod_partida'],'00', 10,2);
				
				
				 $existe=0;
				 $posnum = 0;
				 $pos=0; 
				 reset($partidas);
				 foreach($partidas as  $key=>$item)
                  {
				        if (isset($item['cod']))
						{
						
						   if(trim($partidaTitulo[$j]['cod_partida'])==trim($item['cod']) && $item['aespecifica']==$aespecifica[$a]['pk_num_aespecifica'] && $item['tipo']=='T')
						   {
						       $pos= $key;
							   //echo ($item['cod']."-".$item['aespecifica']."-".$pos);
						       $existe=1; 
							   break 1;
						   } 
						  
						}
				      $posnum++;      
                  } 
				  
				  
		 if ($existe==1)		
		 {	
		    //---------------------------------------------	
			    if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			    $partidas[$pos]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma2,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr_b006_aespecifica']
					
                    );
				   
				  }
				
				
			 //-----------------------------------------------
			
				
			     if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				   $partidas[$pos]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr_b006_aespecifica']
					
                    );
				   
				   }
				  
				  
				  if (trim($codigo3)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$pos]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma3,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr_b006_aespecifica']
					
                    );
				   
				  }
		 
		 }else //IF EXISTE
		 {
		 
			  //---------------------------------------------	
			    if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			    $partidas[$cuenta]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma2,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr_b006_aespecifica']
					
                    );
				   
				  }
				
				
			 //-----------------------------------------------
			
				
			     if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				   $partidas[$cuenta]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr_b006_aespecifica']
					
                    );
				   
				   }
				  
				  
				  if (trim($codigo3)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$cuenta]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma3,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr_b006_aespecifica']
					
                    );
				   
				  }
				 
			    }//FIN IF EXISTE
			 }       
			        $cuenta=$cuenta+1;
                    $partidas[$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria']]=array(
                        'id'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'],
                        'cod'=>$partidaDetalle[$i]['cod_partida'],
                        'partida'=>$partidaDetalle[$i]['ind_denominacion'],
						'monto'=>$partidaDetalle[$i]['num_monto'],
						'tipo'=>$partidaDetalle[$i]['ind_tipo'],
						'aespecifica'=>$partidaDetalle[$i]['fk_pr_b006_aespecifica']
                    );
					
             }//FIN  IF AESPECIFICA    
            
			}//FIN FOR DETALLE
			 $cuenta=$cuenta+1;
		  }//FIN IF AESPECIFICA
		   
		    $this->atVista->assign('aespecifica',$this->atPartida->metAespecifica());
		   }//FIN PRESUPUESTO POR PROYECTO
		   else
		   {
			 
			 $partida1='';
			 $partidat='';
			 
			 $suma=0;
			 $suma2=0;
			 $suma_total=0;
			 
			 $f=0;
			 $p=0;
			 
			  
		for($i=0;$i<count($partidaDetalle);$i++){	
		
		
		   if($f==0){
		      $partida1= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
			  $f=1;
			}  
		
		   if (trim($partida1)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5))){
		      $suma=0;
			  $f=0;
		   }
		   
		   
		    if($p==0){
		      $partidat= substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
			  $p=1;
			}  
		
		   if (trim($partidat)!= trim(substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8))){
		      $suma2=0;
			  $p=0;
		   }
		   
		 
		     $suma=$suma+$partidaDetalle[$i]['num_monto']; 
			 $suma2=$suma2+$partidaDetalle[$i]['num_monto']; 
			 $suma_total=$suma_total+$partidaDetalle[$i]['num_monto'];
		   
		   
		     for($j=0;$j<count($partidaTitulo);$j++){	
			   		 
			    $codigo=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00', 7,5);
				$codigo2=substr_replace($partidaDetalle[$i]['cod_partida'],'00.00.00', 4,8);
				
			//---------------------------------------------	
			  if (trim($codigo2)==trim($partidaTitulo[$j]['cod_partida'])){
			   $partidas[$partidaTitulo[$j]['pk_num_partida_presupuestaria']]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma2,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>0
					
                    );
				   
				  }
				

			    if (trim($codigo)==trim($partidaTitulo[$j]['cod_partida'])){
				
				
				   $partidas[$partidaTitulo[$j]['pk_num_partida_presupuestaria']]=array(
                        'id'=>$partidaTitulo[$j]['pk_num_partida_presupuestaria'],
                        'cod'=>$partidaTitulo[$j]['cod_partida'],
                        'partida'=>$partidaTitulo[$j]['ind_denominacion'],
						'monto'=>$suma,
						'tipo'=>$partidaTitulo[$j]['ind_tipo'],
						'aespecifica'=>0
					
                    );
				   
				  }
				 
			    
			 } 
                    $partidas[$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria']]=array(
                        'id'=>$partidaDetalle[$i]['fk_prb002_num_partida_presupuestaria'],
                        'cod'=>$partidaDetalle[$i]['cod_partida'],
                        'partida'=>$partidaDetalle[$i]['ind_denominacion'],
						'monto'=>$partidaDetalle[$i]['num_monto'],
						'tipo'=>$partidaDetalle[$i]['ind_tipo'],
						'aespecifica'=>0
                    );
					
                
            }
			
		}//FIN IF PRESUPUESTO	
			
			 if(isset($partidas)){
                $this->atVista->assign('partidas', $partidas);
            }
			
		    $this->atVista->assign('suma_total',$suma_total);	
        }
		$this->atVista->assign('suma_total',$suma_total);	
		$this->atVista->assign('status',$status);
		$this->atVista->assign('presupuesto',$this->atReformulacion->metPresupuestoListar());
        $this->atVista->metRenderizar('CrearModificar','modales');
		
    }

  
  
    public function metEliminar()
    {
        $idReformulacion = $this->metObtenerInt('idReformulacion');
        if($idReformulacion!=0){
            $id=$this->atReformulacion->metEliminarReformulacion($idReformulacion);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero la Reformulacion se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idReformulacion'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
	
	
	protected function FormatoFecha($fecha)
    {
        $resultado=explode("-",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }	
	
	
	protected function FormatoMonto($monto_f)
    {
       if(!isset($monto_f)){
                $monto_f=0;
            }
			else {
			     $monto_f=str_replace(".", "", $monto_f);
			     $monto_f=str_replace(",", ".", $monto_f);
				 
			}
			 return $monto_f;
    }	
	
	
	
}