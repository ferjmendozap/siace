<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * Antepresupuesto: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO                   |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                | lbastardo@contraloriadebolivar.gob.ve  |         0424-9080200           |
 * |   |                                           |                                        |                                |
 * |___|___________________________________________|________________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        01-09-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class ajusteModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }


    /**
	*Consulta para mostrar datos de un ajuste especifico
	*
	*Parametro: $idAjuste
	*Return:  $ajuste->fetch()
	*
	**/
    public function metMostrarAjuste($idAjuste)
    {
        $ajuste = $this->_db->query("
         SELECT pap.*, pbp.pk_num_presupuesto, pbp.ind_cod_presupuesto, pbp.ind_tipo_presupuesto  FROM pr_d002_ajuste_presupuestario pap 
		 INNER JOIN  pr_d003_ajuste_presupuestario_det pdap ON pap.pk_num_ajuste_presupuestario = pdap.fk_prd002_num_ajuste_presupuestario
		 INNER JOIN  pr_c002_presupuesto_det ppd ON pdap.fk_prc002_num_presupuesto_det = ppd.pk_num_presupuesto_det
		 INNER JOIN pr_b004_presupuesto  pbp ON ppd.fk_prb004_num_presupuesto=pbp.pk_num_presupuesto
		 WHERE pk_num_ajuste_presupuestario='$idAjuste'
        ");
		
		
        $ajuste->setFetchMode(PDO::FETCH_ASSOC);
        return $ajuste->fetch();
    }

    
	/**
	*Consulta para mostrar ajustes presupuestario con el detalle de sus partidas
	*
	*Parametro: $idAjuste,$status
	*Return:  $partida->fetchAll()
	*
	**/
	public function metMostrarAjusteDetalle($idAjuste,$status)
    {
	
      if ($status=='AP') {	
	   $partida = $this->_db->query(
            "SELECT
              pbp.ind_tipo_presupuesto, pbp.pk_num_presupuesto, pbp.ind_tipo_presupuesto, pbp.ind_cod_presupuesto, pap.*, papd.*,pbd.*,ppd.pk_num_presupuesto_det, ppd.fk_prb002_num_partida_presupuestaria, ppd.num_monto_compromiso, ppd.fk_pr006_num_aespecifica, ppd.num_monto_ajustado as ajustado
            FROM
              pr_d002_ajuste_presupuestario pap

               INNER JOIN pr_d003_ajuste_presupuestario_det papd ON pap.pk_num_ajuste_presupuestario = papd.fk_prd002_num_ajuste_presupuestario
			   
			   INNER JOIN pr_c002_presupuesto_det ppd ON papd.fk_prc002_num_presupuesto_det = ppd.pk_num_presupuesto_det
			   
			   INNER JOIN pr_b004_presupuesto pbp ON ppd.fk_prb004_num_presupuesto = pbp.pk_num_presupuesto

               INNER JOIN pr_b002_partida_presupuestaria pbd ON ppd.fk_prb002_num_partida_presupuestaria = pbd.pk_num_partida_presupuestaria

             WHERE pap.pk_num_ajuste_presupuestario='$idAjuste' and  papd.num_monto_ajustado>0

            ORDER by pbd.pk_num_partida_presupuestaria ASC
          "
        );}
		
		else {
        $partida = $this->_db->query(
            "SELECT
              pbp.ind_tipo_presupuesto, pbp.pk_num_presupuesto, pbp.ind_cod_presupuesto, pap.*, papd.*,pbd.*,ppd.pk_num_presupuesto_det, ppd.fk_prb002_num_partida_presupuestaria, ppd.num_monto_compromiso, ppd.fk_pr006_num_aespecifica, ppd.num_monto_ajustado AS ajustado 
            FROM
              pr_d002_ajuste_presupuestario pap

               INNER JOIN pr_d003_ajuste_presupuestario_det papd ON pap.pk_num_ajuste_presupuestario = papd.fk_prd002_num_ajuste_presupuestario
			   
			   INNER JOIN pr_c002_presupuesto_det ppd ON papd.fk_prc002_num_presupuesto_det = ppd.pk_num_presupuesto_det
			   
			   INNER JOIN pr_b004_presupuesto pbp ON ppd.fk_prb004_num_presupuesto = pbp.pk_num_presupuesto

               INNER JOIN pr_b002_partida_presupuestaria pbd ON ppd.fk_prb002_num_partida_presupuestaria = pbd.pk_num_partida_presupuestaria

            WHERE pap.pk_num_ajuste_presupuestario='$idAjuste'

            ORDER by pbd.pk_num_partida_presupuestaria ASC
          "
        );}
			

        $partida->setFetchMode(PDO::FETCH_ASSOC);
        return $partida->fetchAll();
    }

    
	 
    /**
	*Consulta para mostrar partidas tipo titulo
	*
	*Parametro: N/A
	*Return:  $titulo->fetchAll()
	*
	**/ 
	public function metMostrarAntepresupuestoTitulo()
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_b002_partida_presupuestaria 
            WHERE ind_tipo='T'
            ORDER by cod_partida ASC
          "
        );
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
	

    /**
	*Consulta para deteminar si es una accion especifica
	*
	*Parametro: N/A
	*Return:  $titulo->fetchAll()
	*
	**/ 
    public function metExisteAespecificaEs($id,$idPresupuesto)
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_c002_presupuesto_det 
            WHERE  fk_pr006_num_aespecifica='$id' AND fk_prb004_num_presupuesto='$idPresupuesto'
            
          "
        );
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
		
        return $titulo->fetchAll();
    }
     //----------------------------------------------------------------------------------------------------------
  
    /**
	*Consulta para deteminar si es un ajuste se incremento o disminuci�n
	*
	*Parametro: N/A
	*Return:    $tipoajuste->fetchAll()
	*
	**/ 
    public function metTipoAjusteListar()
    {
        $tipoajuste =  $this->_db->query(
            "SELECT amd.pk_num_miscelaneo_detalle, amd.ind_nombre_detalle FROM a005_miscelaneo_maestro amm
             INNER JOIN a006_miscelaneo_detalle amd ON (amd.fk_a005_num_miscelaneo_maestro=pk_num_miscelaneo_maestro)
             WHERE cod_maestro='TDA'"
        );
        $tipoajuste->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoajuste->fetchAll();
    }
	

    /**
	*Consulta para listar ajuster presupuestarios seg�n su estado
	*
	*Parametro: $status = false
	*Return:    $ajuste->fetchAll()
	*
	**/ 
    public function metListarAjuste($status = false)
    {
	  if ($status) {
            $status = "WHERE ind_estado='$status'";
        }
	
        $ajuste = $this->_db->query(
            "SELECT * FROM pr_d002_ajuste_presupuestario $status"
        );
        $ajuste->setFetchMode(PDO::FETCH_ASSOC);
        return $ajuste->fetchAll();
    }
	
	
	
	/**
	*Retorna el presupuesto aprobado para el a�o actual
	*
	*Parametro: N/A
	*Return:    $id->fetchAll()
	*
	**/ 
	public function metIdPresupuesto()
    {
	    $year_actual=date("Y"); 
        $id =  $this->_db->query(
            "SELECT
               pk_num_presupuesto, ind_cod_presupuesto, ind_tipo_presupuesto
            FROM
               pr_b004_presupuesto
            WHERE fec_anio = $year_actual AND ind_estado='AP' 
          "
        );
		
				
        $id->setFetchMode(PDO::FETCH_ASSOC);
        return $id->fetchAll();
    }
		
	
	
    /**
	*Consulta para mostrar datos de presupuesto seg�n el a�o en curso
	*
	*Parametro: N/A
	*Return:    $ano->fetchAll()
	*
	**/ 
	public function metSelectPresupuesto()
    {
	    $year_actual=date("Y"); 
        $ano =  $this->_db->query(
            "SELECT
              pba.pk_num_presupuesto, pba.pk_num_presupuesto, pba.ind_cod_presupuesto, pba.ind_cod_presupuesto, pba.ind_tipo_presupuesto, pca.*
            FROM
               pr_b004_presupuesto pba
               INNER JOIN pr_c002_presupuesto_det pca ON pba.pk_num_presupuesto = pca.fk_prb004_num_presupuesto
               INNER JOIN pr_b002_partida_presupuestaria pbp ON pbp.pk_num_partida_presupuestaria = pca.fk_prb002_num_partida_presupuestaria
            WHERE pba.fec_anio = '$year_actual' AND pba.ind_estado='AP'
            ORDER by pca.fk_prb002_num_partida_presupuestaria ASC
          "
        );
		

        $ano->setFetchMode(PDO::FETCH_ASSOC);
        return $ano->fetchAll();
    }
	
	
	/**
	*Consulta para obtener el codigo del �ltimo ajuste registrado
	*
	*Parametro: N/A
	*Return:    $registro['ind_cod_ajuste_presupuestario']
	*
	**/ 
    public function metObtenerUltimoAjuste()
    {
	 
       $antepresupuesto = $this->_db->query(
            "SELECT ind_cod_ajuste_presupuestario FROM pr_d002_ajuste_presupuestario ORDER BY ind_cod_ajuste_presupuestario DESC LIMIT 1"
        );
		
        $antepresupuesto->setFetchMode(PDO::FETCH_UNIQUE);
		$registro=$antepresupuesto->fetch();
        return $registro['ind_cod_ajuste_presupuestario'];
    }
	
	
    /**
	*Crea nuevos ajuste presupuestarios
	*
	*Parametro: $fk_prb004_num_presupuesto,$fk_a006_num_miscelaneo_det_tipo_ajuste, $fec_anio, $fec_mes, $ind_numero_gaceta, $fec_gaceta, $ind_numero_resolucion, $fec_resolucion, 		    *$num_total_ajuste, $ind_descripcion, $partidas=false, $monto=false, $monto_inicial=false, $aespecifica=false
	*Return: $idRegistro
	*
	**/ 
    public function metCrearAjuste($fk_prb004_num_presupuesto,$fk_a006_num_miscelaneo_det_tipo_ajuste, $fec_anio, $fec_mes, $ind_numero_gaceta, $fec_gaceta, $ind_numero_resolucion, 	$fec_resolucion, $num_total_ajuste, $ind_descripcion, $partidas=false, $monto=false, $monto_inicial=false, $aespecifica=false)
    {

	
        $this->_db->beginTransaction();
		
		    $registro=$this->metObtenerUltimoAjuste();
			$registro=(int) $registro+1;
			$mun="0";
            for($i=0;$i<(3-strlen($registro));$i++){
                $mun.="0";
            }
            $registro=$mun.$registro;
			
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    pr_d002_ajuste_presupuestario
                  SET
                    fk_prb004_num_presupuesto=:fk_prb004_num_presupuesto,fk_rhb001_num_empleado_creado='$this->atIdUsuario',   
					fk_rhb001_num_empleado_aprobado=:fk_rhb001_num_empleado_aprobado,fec_anio=:fec_anio, fec_mes=:fec_mes, 
					ind_numero_gaceta=:ind_numero_gaceta,fec_gaceta=:fec_gaceta,ind_numero_resolucion=:ind_numero_resolucion,fec_resolucion=:fec_resolucion,
					num_total_ajuste=:num_total_ajuste,   
					ind_descripcion=:ind_descripcion,fec_creado=CURDATE(),fec_aprobado=:fec_aprobado,fec_ajuste=CURDATE(),ind_estado=:ind_estado,   
					fec_ultima_modificacion=NOW(),fk_a018_num_seguridad_usuario='$this->atIdUsuario',
					ind_cod_ajuste_presupuestario=:ind_cod_ajuste_presupuestario, fk_a006_num_miscelaneo_det_tipo_ajuste=:fk_a006_num_miscelaneo_det_tipo_ajuste
                ");
            $nuevoRegistro->execute(array(
			    'fk_prb004_num_presupuesto'=>$fk_prb004_num_presupuesto,
			    'ind_cod_ajuste_presupuestario'=>$registro,
				'fec_anio'=>$fec_anio,
				'fec_mes'=>$fec_mes,
				'ind_numero_gaceta'=>$ind_numero_gaceta,
				'fec_gaceta'=>$fec_gaceta,
				'ind_numero_resolucion'=>$ind_numero_resolucion,
				'fec_resolucion'=>$fec_resolucion,
                'num_total_ajuste'=>$num_total_ajuste,
				'ind_descripcion'=>$ind_descripcion,
				'fec_aprobado'=>'0000-00-00',
                'ind_estado'=>'PR',
				'fk_rhb001_num_empleado_aprobado'=>0,
				'fk_a006_num_miscelaneo_det_tipo_ajuste'=>$fk_a006_num_miscelaneo_det_tipo_ajuste,
            ));		
            $idRegistro=$this->_db->lastInsertId();
			
		$cont=0;
		if(isset($partidas) && $partidas!=false){	
		 for($i=0;$i<count($partidas);$i++) {
		  
		  $id=$partidas[$i];
		  
		 
		  if(isset($aespecifica[$cont]))
			  $ae= $aespecifica[$cont]/100;
		  else  
			  $ae=0;
			  
			  $cont=$cont+1;
		 
		  
		   if ($id>0)
		   {
		       $monto_inicial[$i]=$this->FormatoMonto_modelo($monto_inicial[$i]); 
		       $monto[$i]=$this->FormatoMonto_modelo($monto[$i]); 
			   
			   $nuevaPartida=$this->_db->prepare("
                  INSERT INTO
                    pr_d003_ajuste_presupuestario_det
                  SET
                    fk_prd002_num_ajuste_presupuestario=:fk_prd002_num_ajuste_presupuestario,
                    fk_prc002_num_presupuesto_det=:fk_prc002_num_presupuesto_det,  
					num_monto_disponible=:num_monto_disponible, num_monto_ajustado=:num_monto_ajustado, 
					fec_ultima_modificacion=NOW(),fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
					fk_pr_b006_aespecifica=:fk_pr_b006_aespecifica	
               ");
			
              $nuevaPartida->execute(array(
			    'fk_prd002_num_ajuste_presupuestario'=>$idRegistro,
                'fk_prc002_num_presupuesto_det'=>$partidas[$i],
				'num_monto_disponible'=> $monto_inicial[$i],
				'num_monto_ajustado'=> $monto[$i],
				'fk_pr_b006_aespecifica'=>$ae,
				
              ));	
			
		   } //$id 
	     }	// for
        } //fin $partidas
		
		
		
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }


    /**
	*Modificar ajuste presupuestarios
	*
	*Parametro: $fk_a006_num_miscelaneo_det_tipo_ajuste, $fec_anio, $fec_mes, $ind_numero_gaceta, $fec_gaceta, $ind_numero_resolucion, $fec_resolucion, $num_total_ajuste, 	     
	*$ind_descripcion, $partidas=false, $monto=false, $monto_inicial=false, $aespecifica=false ,$idAjuste
	*Return: $idAjuste
	*
	**/ 
    public function metModificarAjuste($fk_a006_num_miscelaneo_det_tipo_ajuste, $fec_anio, $fec_mes, $ind_numero_gaceta, $fec_gaceta, $ind_numero_resolucion, $fec_resolucion, $num_total_ajuste, $ind_descripcion, $partidas=false, $monto=false, $monto_inicial=false, $aespecifica=false ,$idAjuste)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                         pr_d002_ajuste_presupuestario
                      SET
					     fec_anio=:fec_anio, fec_mes=:fec_mes, 
					     ind_numero_gaceta=:ind_numero_gaceta,fec_gaceta=:fec_gaceta,ind_numero_resolucion=:ind_numero_resolucion,fec_resolucion=:fec_resolucion,
					     num_total_ajuste=:num_total_ajuste,   
					     ind_descripcion=:ind_descripcion,fec_ajuste=CURDATE(),
					     fec_ultima_modificacion=NOW(),fk_a018_num_seguridad_usuario='$this->atIdUsuario',
					     fk_a006_num_miscelaneo_det_tipo_ajuste=:fk_a006_num_miscelaneo_det_tipo_ajuste
					 WHERE 
					    pk_num_ajuste_presupuestario='$idAjuste' 
            ");
            $nuevoRegistro->execute(array(
				'fec_anio'=>$fec_anio,
				'fec_mes'=>$fec_mes,
				'ind_numero_gaceta'=>$ind_numero_gaceta,
				'fec_gaceta'=>$fec_gaceta,
				'ind_numero_resolucion'=>$ind_numero_resolucion,
				'fec_resolucion'=>$fec_resolucion,
                'num_total_ajuste'=>$num_total_ajuste,
				'ind_descripcion'=>$ind_descripcion,
				'fk_a006_num_miscelaneo_det_tipo_ajuste'=>$fk_a006_num_miscelaneo_det_tipo_ajuste,
               
            ));
			
		
		  $eliminarDet = $this->_db->query(
            "DELETE FROM pr_d003_ajuste_presupuestario_det WHERE fk_prd002_num_ajuste_presupuestario='$idAjuste'"
        );	
		
		 $cont=0;	
         if(isset($partidas) && $partidas!=false){	
		 for($i=0;$i<count($partidas);$i++) {
		  
		  $id=$partidas[$i];
		 
		  // $indicador=count($this->atPresupuesto->metMostrarPartidasTituloEs($id));
		  
		   if(isset($aespecifica[$cont]))
			  $ae= $aespecifica[$cont]/100;
		   else  
			  $ae=0;
		  
		    $cont=$cont+1;
		  
		   if ($id>0 )
		   {
		       $monto_inicial[$i]=$this->FormatoMonto_modelo($monto_inicial[$i]); 
		       $monto[$i]=$this->FormatoMonto_modelo($monto[$i]); 
			   
		    //if ($monto[$id]>0){
			   $nuevaPartida=$this->_db->prepare("
                  INSERT INTO
                    pr_d003_ajuste_presupuestario_det
                  SET
                    fk_prd002_num_ajuste_presupuestario=:fk_prd002_num_ajuste_presupuestario,
                    fk_prc002_num_presupuesto_det=:fk_prc002_num_presupuesto_det,  
					num_monto_disponible=:num_monto_disponible, num_monto_ajustado=:num_monto_ajustado, 
					fec_ultima_modificacion=NOW(),fk_a018_num_seguridad_usuario='$this->atIdUsuario',
					fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
					fk_pr_b006_aespecifica=:fk_pr_b006_aespecifica	
               ");
			
              $nuevaPartida->execute(array(
			    'fk_prd002_num_ajuste_presupuestario'=>$idAjuste,
                'fk_prc002_num_presupuesto_det'=>$partidas[$i],
				'num_monto_disponible'=> $monto_inicial[$i],
				'num_monto_ajustado'=> $monto[$i],
				'fk_pr_b006_aespecifica'=> 0,
				'fk_prb002_num_partida_presupuestaria'=>1,
              ));	
			//}// $monto[$id]	
		   } //$id 
	     }	// for
        } //fin $partidas
		
			  
           $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idAjuste;
            }
    }


    /**
	*Eliminar ajuste presupuestario
	*
	*Parametro: $idAjuste
	*Return:    $idAjuste
	*
	**/ 
    public function metEliminarAjuste($idAjuste)
    {
	   
        $this->_db->beginTransaction();
		
		   $eliminarAntePresupuestoDet = $this->_db->query(
            "DELETE FROM pr_d003_ajuste_presupuestario_det WHERE fk_prd002_num_ajuste_presupuestario='$idAjuste'"
			);
		
		  			
		    $eliminar = $this->_db->query(
            "DELETE FROM pr_d002_ajuste_presupuestario WHERE pk_num_ajuste_presupuestario='$idAjuste'"
			);
				
             $error=$eliminar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idAjuste;
            }
    }
	

    /**
	*Obtener nombre del un miscelaneo seg�n su codigo
	*
	*Parametro: $cod
	*Return:    $registro['ind_nombre_detalle']
	*
	**/ 
    public function metObtenertipo($cod)
    {
	 
       $antepresupuesto = $this->_db->query(
            "SELECT  ind_nombre_detalle FROM  a006_miscelaneo_detalle WHERE pk_num_miscelaneo_detalle='$cod' "
        );
		
        $antepresupuesto->setFetchMode(PDO::FETCH_UNIQUE);
		$registro=$antepresupuesto->fetch();
        return $registro['ind_nombre_detalle'];
    }

    

	/**
	*Ejecuta una acci�n de ajuste presupuestario seg�n  estatus enviado
	*
	*Parametro: $idAjuste, $status, $tipo=false
	*Return:    $ajuste->fetchAll()
	*
	**/ 
	public function metAccionesAjuste($idAjuste, $status, $tipo=false)
    {

	  $desc= $this->metObtenertipo($tipo);
	  
	    
        $this->_db->beginTransaction();
        if ($status=='AP') {
           $aprobarregistro = $this->_db->prepare("
                  UPDATE
                    pr_d002_ajuste_presupuestario
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    fk_rhb001_num_empleado_aprobado='$this->atIdUsuario', fec_aprobado=NOW(), ind_estado=:ind_estado
                  WHERE
                    pk_num_ajuste_presupuestario='$idAjuste'
            ");
       
	   
        $aprobarregistro->execute(array(
            'ind_estado' => $status,
			
        ));
		
		if($desc=='Incremento'){
		 $actualizarpresupuesto=$this->_db->query("
                UPDATE pr_c002_presupuesto_det ppd
                INNER JOIN pr_d003_ajuste_presupuestario_det pap ON (ppd.pk_num_presupuesto_det=pap.fk_prc002_num_presupuesto_det AND pap.num_monto_ajustado>0)
                INNER JOIN pr_d002_ajuste_presupuestario papp
                    ON (pap.fk_prd002_num_ajuste_presupuestario=papp.pk_num_ajuste_presupuestario)
                INNER JOIN pr_b004_presupuesto ppdd
                    ON (ppd.fk_prb004_num_presupuesto=ppdd.pk_num_presupuesto)
                SET  ppd.num_monto_ajustado=pap.num_monto_ajustado+ppd.num_monto_ajustado, ppd.num_monto_incremento=pap.num_monto_ajustado+ppd.num_monto_incremento
                WHERE  pap.fk_prd002_num_ajuste_presupuestario='$idAjuste'
            ");	
		}else{
		 $actualizarpresupuesto=$this->_db->query("
                UPDATE pr_c002_presupuesto_det ppd
                INNER JOIN pr_d003_ajuste_presupuestario_det pap ON (ppd.pk_num_presupuesto_det=pap.fk_prc002_num_presupuesto_det AND pap.num_monto_ajustado>0)
                INNER JOIN pr_d002_ajuste_presupuestario papp
                    ON (pap.fk_prd002_num_ajuste_presupuestario=papp.pk_num_ajuste_presupuestario)
                INNER JOIN pr_b004_presupuesto ppdd
                    ON (ppd.fk_prb004_num_presupuesto=ppdd.pk_num_presupuesto)
                SET  ppd.num_monto_ajustado=ppd.num_monto_ajustado-pap.num_monto_ajustado, ppd.num_monto_disminucion=pap.num_monto_ajustado+ppd.num_monto_disminucion
                WHERE  pap.fk_prd002_num_ajuste_presupuestario='$idAjuste' AND pap.num_monto_ajustado<ppd.num_monto_ajustado
            ");	
		  }	
			
			
			
		 $eliminarDet = $this->_db->query(
                "DELETE FROM pr_d003_ajuste_presupuestario_det WHERE fk_prd002_num_ajuste_presupuestario='$idAjuste' AND num_monto_ajustado<=0" 
			 );
			 
		 
	}	
		
            $error =   $aprobarregistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idAjuste;
            }
    }
	

	/**
	*Listado de presupuesto seg�n su estatus 
	*
	*Parametro: $status = false
	*Return:    $ajuste->fetchAll()
	*
	**/ 
	public function metListarPresupuesto($status = false)
    {
	  if ($status) {
            $status = "WHERE ind_estado='$status'";
        }
	
        $ajuste = $this->_db->query(
            "SELECT * FROM pr_b004_presupuesto $status"
        );
        $ajuste->setFetchMode(PDO::FETCH_ASSOC);
        return $ajuste->fetchAll();
    }
	
	
   	/**
	*Dar formato a los montos sin separador de miles y coma (,) como separador de decimales
	*
	*Parametro: $monto_f
	*Return: $monto_f
	*
	**/
	public function FormatoMonto_modelo($monto_f)
    {
       if(!isset($monto_f)){
                $monto_f=0;
            }
			else {
			     $monto_f=str_replace(".", "", $monto_f);
			     $monto_f=str_replace(",", ".", $monto_f);
				 
			}
			 return $monto_f;
    }	
	
	
}
