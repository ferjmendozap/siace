<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class tipoCuentaModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarTipoCuenta($idCuenta)
    {
        $tipoCuenta = $this->_db->query("
          SELECT
               *
          FROM
            pr_b001_tipo_cuenta 
          WHERE
            pk_num_tipo_cuenta='$idCuenta'
        ");
		
	
        $tipoCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCuenta->fetch();
    }

    public function metListarTipoCuenta()
    {
        $tipoCuenta = $this->_db->query(
            "SELECT * FROM pr_b001_tipo_cuenta"
        );
        $tipoCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCuenta->fetchAll();
    }

    public function metCrearTipoCuenta($codCuenta,$nombre,$status)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    pr_b001_tipo_cuenta
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), cod_tipo_cuenta=:cod_tipo_cuenta,
                    num_estatus=:num_estatus ,ind_descripcion=:ind_descripcion 
                ");
            $nuevoRegistro->execute(array(
                'cod_tipo_cuenta'=>$codCuenta,
                'num_estatus'=>$status,
                'ind_descripcion'=>$nombre,
               
            ));
            $idRegistro= $this->_db->lastInsertId();
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }

    public function metModificarTipoCuenta($codCuenta,$nombre,$status,$idCuenta)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        pr_b001_tipo_cuenta
                      SET
                       fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), cod_tipo_cuenta=:cod_tipo_cuenta,
                       num_estatus=:num_estatus ,ind_descripcion=:ind_descripcion 
                      WHERE
                        pk_num_tipo_cuenta='$idCuenta'
            ");
            $nuevoRegistro->execute(array(
                'cod_tipo_cuenta'=>$codCuenta,
                'num_estatus'=>$status,
                'ind_descripcion'=>$nombre,
            ));
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idCuenta;
            }
    }

    public function metEliminarTipoCuenta($idCuenta)
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                DELETE FROM  pr_b001_tipo_cuenta WHERE pk_num_tipo_cuenta=:pk_num_tipo_cuenta
            ");
            $elimar->execute(array(
                'pk_num_tipo_cuenta'=>$idCuenta
            ));
            $error=$elimar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idCuenta;
            }

    }

}
