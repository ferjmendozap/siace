<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |         Liduvica Bastardo                 |         liduvica@hotmail.com       |        0424-9080200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class subprogramaModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarSubprograma($idSubprograma)
    {
        $tipoCuenta = $this->_db->query("
          SELECT
               *
          FROM
            pr_b010_subprograma 
          WHERE
            pk_num_subprograma='$idSubprograma'
        ");
		
	
        $tipoCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCuenta->fetch();
    }

    public function metListarSubprograma()
    {
        $tipoCuenta = $this->_db->query(
            "SELECT * FROM pr_b010_subprograma"
        );
        $tipoCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCuenta->fetchAll();
    }

     
	 public function metMostrarSubprogramaSelect($idPrograma)
    {
        $cuenta = $this->_db->query("
          SELECT
               *
          FROM
            pr_b010_subprograma 
          WHERE
            fk_prb009_num_programa=$idPrograma");
			
		
		$cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
		
    }


    public function metObtenerUltimaSubprograma()
    {
	 
       $antepresupuesto = $this->_db->query(
            "SELECT ind_cod_subprograma FROM pr_b010_subprograma ORDER BY ind_cod_subprograma DESC LIMIT 1"
        );
		
        $antepresupuesto->setFetchMode(PDO::FETCH_UNIQUE);
		$registro=$antepresupuesto->fetch();
        return $registro['ind_cod_subprograma'];
    }




    public function metCrearSubprograma($nombre,$status,$programa,$numsubprograma)
    {
	
	   $registro=$this->metObtenerUltimaSubprograma();
			$registro=(int) $registro+1;
			$mun="0";
            for($i=0;$i<(2-strlen($registro));$i++){
                $mun.="0";
            }
            $registro=$mun.$registro;
			
	
	
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    pr_b010_subprograma
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), ind_cod_subprograma=:ind_cod_subprograma,
                    num_estatus=:num_estatus ,ind_descripcion=:ind_descripcion, fk_prb009_num_programa=:fk_prb009_num_programa, num_cod_subprograma=:num_cod_subprograma
                ");
            $nuevoRegistro->execute(array(
                'ind_cod_subprograma'=>$registro,
                'num_estatus'=>$status,
                'ind_descripcion'=>$nombre,
				'fk_prb009_num_programa'=>$programa,
				'num_cod_subprograma'=>$numsubprograma,
               
            ));
            $idRegistro= $this->_db->lastInsertId();
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }

    public function metModificarSubprograma($nombre,$status,$programa,$numsubprograma, $idSubprograma)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        pr_b010_subprograma
                      SET
                       fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), 
                       num_estatus=:num_estatus ,ind_descripcion=:ind_descripcion, fk_prb009_num_programa=:fk_prb009_num_programa, num_cod_subprograma=:num_cod_subprograma
                      WHERE
                        pk_num_subprograma='$idSubprograma'
            ");
            $nuevoRegistro->execute(array(
                
                'num_estatus'=>$status,
                'ind_descripcion'=>$nombre,
				'fk_prb009_num_programa'=>$programa,
				'num_cod_subprograma'=>$numsubprograma,
            ));
			
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idSubprograma;
            }
    }

    public function metEliminarSubprograma($idSubprograma)
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                DELETE FROM  pr_b010_subprograma WHERE pk_num_subprograma=:pk_num_subprograma
            ");
            $elimar->execute(array(
                'pk_num_subprograma'=>$idSubprograma
            ));
            $error=$elimar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idSubprograma;
            }

    }

}
