<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class aespecificaModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarAespecifica($idAespecifica)
    {
        $tipoCuenta = $this->_db->query("
          SELECT
               *
          FROM
            pr_b006_aespecifica 
          WHERE
            pk_num_aespecifica='$idAespecifica'
        ");
		
	
        $tipoCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCuenta->fetch();
    }

    public function metListarAespecifica()
    {
        $tipoCuenta = $this->_db->query(
            "SELECT * FROM pr_b006_aespecifica"
        );
        $tipoCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCuenta->fetchAll();
    }


    public function metObtenerUltimaAespecifica()
    {
	 
       $antepresupuesto = $this->_db->query(
            "SELECT ind_cod_aespecifica FROM pr_b006_aespecifica ORDER BY ind_cod_aespecifica DESC LIMIT 1"
        );
		
        $antepresupuesto->setFetchMode(PDO::FETCH_UNIQUE);
		$registro=$antepresupuesto->fetch();
        return $registro['ind_cod_aespecifica'];
    }




    public function metCrearAespecifica($nombre,$status)
    {
	
	   $registro=$this->metObtenerUltimaAespecifica();
			$registro=(int) $registro+1;
			$mun="0";
            for($i=0;$i<(2-strlen($registro));$i++){
                $mun.="0";
            }
            $registro=$mun.$registro;
			
	
	
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    pr_b006_aespecifica
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), ind_cod_aespecifica=:ind_cod_aespecifica,
                    num_estatus=:num_estatus ,ind_descripcion=:ind_descripcion 
                ");
            $nuevoRegistro->execute(array(
                'ind_cod_aespecifica'=>$registro,
                'num_estatus'=>$status,
                'ind_descripcion'=>$nombre,
               
            ));
            $idRegistro= $this->_db->lastInsertId();
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }

    public function metModificarAespecifica($nombre,$status,$idAespecifica)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        pr_b006_aespecifica
                      SET
                       fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), 
                       num_estatus=:num_estatus ,ind_descripcion=:ind_descripcion 
                      WHERE
                        pk_num_aespecifica='$idAespecifica'
            ");
            $nuevoRegistro->execute(array(
                
                'num_estatus'=>$status,
                'ind_descripcion'=>$nombre,
            ));
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idAespecifica;
            }
    }

    public function metEliminarAespecifica($idAespecifica)
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                DELETE FROM  pr_b006_aespecifica WHERE pk_num_aespecifica=:pk_num_aespecifica
            ");
            $elimar->execute(array(
                'pk_num_aespecifica'=>$idAespecifica
            ));
            $error=$elimar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idAespecifica;
            }

    }

}
