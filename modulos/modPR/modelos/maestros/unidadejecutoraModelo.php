<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        29-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

//require_once RUTA_MODELO . 'unidadejecutoraModelo.php';


class unidadejecutoraModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }
	
	public function metListarAplicacion()
    {
        $menu = $this->_db->query("SELECT * FROM a015_seguridad_aplicacion");	
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
    }


    public function metListarMiscelaneos($aplicacion = false, $estatus = false)
    {
        if ($aplicacion) {
            $filtro = "WHERE
              a005.num_estatus='$estatus' AND a005.fk_a001_num_organismo='$aplicacion'";
        } elseif ($estatus && ($estatus == 0 || $estatus == 1)) {
            $filtro = "WHERE
              a005.fk_a001_num_organismo='$aplicacion'";
        } else {
            $filtro = "";
        }

        $miscelaneo = $this->_db->query("
            SELECT
             *
            FROM
              pr_b012_unidad_ejecutora AS a005
            INNER JOIN
              a001_organismo AS a015 ON a005.fk_a001_num_organismo=a015.pk_num_organismo
              $filtro
        ");
		
        $miscelaneo->setFetchMode(PDO::FETCH_ASSOC);
		
        return $miscelaneo->fetchAll();
    }
	
	public function metListarUnidadEjecutora()
    {
       
        $miscelaneo = $this->_db->query("
            SELECT
             *
            FROM
              pr_b012_unidad_ejecutora AS a005
            INNER JOIN
              a001_organismo AS a015 ON a005.fk_a001_num_organismo=a015.pk_num_organismo
        ");
		
        $miscelaneo->setFetchMode(PDO::FETCH_ASSOC);
		
        return $miscelaneo->fetchAll();
    }
	
	
    public function metMostrarMiscelaneo($idUnidadEjecutora)
    {
        $miscelaneo = $this->_db->query("
            SELECT * FROM pr_b012_unidad_ejecutora pue
			   INNER JOIN a023_centro_costo acc ON (pue.fk_a023_num_centro_de_costo = acc.pk_num_centro_costo)
			WHERE pue.pk_num_unidad_ejecutora='$idUnidadEjecutora'");
        $miscelaneo->setFetchMode(PDO::FETCH_ASSOC);

        return $miscelaneo->fetch();
    }

    public function metMostrarSelect($codMaestro = false, $idUnidadEjecutora = false)
    {
        if ($codMaestro) {
            $where = "a005.cod_unidad_ejecutora='$codMaestro'";
        } elseif ($idUnidadEjecutora) {
            $where = "a006.fk_prb012_unidad_ejecutora='$idUnidadEjecutora'";
        }

        $miscelaneoDetalle = $this->_db->query("
            SELECT
              a006.pk_num_unidad_ejecutora_dependencia,
              a006.ind_nombre_unidad_ejecutora_dependencia,
              a006.cod_unidad_ejecutora_dependencia,
              a006.num_estatus
            FROM
              pr_b013_unidad_ejecutora_dependencia AS a006
            INNER JOIN
              pr_b012_unidad_ejecutora AS a005 ON a006.fk_prb012_unidad_ejecutora=a005.pk_num_unidad_ejecutora
            WHERE $where

        ");
		
        $miscelaneoDetalle->setFetchMode(PDO::FETCH_ASSOC);
		
		 return $miscelaneoDetalle->fetchAll();
    }


    public function metMostrarResSelect($codMaestro = false, $idUnidadEjecutora = false)
    {
        if ($codMaestro) {
            $where = "a005.cod_unidad_ejecutora='$codMaestro'";
        } elseif ($idUnidadEjecutora) {
            $where = "a006.fk_prb012_unidad_ejecutora='$idUnidadEjecutora'";
        }

      
        $miscelaneoDetalle = $this->_db->query("
            SELECT
              a006.pk_num_responsable,
              a006.ind_nombre_responsable,
              a006.cod_responsable,
              a006.num_estatus
            FROM
              pr_b014_unidad_ejecutora_responsable AS a006
            INNER JOIN
              pr_b012_unidad_ejecutora AS a005 ON a006.fk_prb012_unidad_ejecutora=a005.pk_num_unidad_ejecutora
            WHERE $where

        ");
		
        $miscelaneoDetalle->setFetchMode(PDO::FETCH_ASSOC);
		
		 return $miscelaneoDetalle->fetchAll();
    }


    public function metCrearUnidadEjecutora($nombre, $descripcion, $CodMaestro, $aplicacion, $estatus, $detalle = false, $statusDetalle = false, $codDetalle = false , $centrocosto,
	$detalleRes = false, $statusDetalleRes=false, $codDetalleRes = false)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                pr_b012_unidad_ejecutora
              SET
                fk_a018_num_seguridad_usuario='$this->atIdUsuario', ind_nombre_unidad_ejecutora=:ind_nombre_unidad_ejecutora,
                ind_descripcion=:ind_descripcion, cod_unidad_ejecutora=:cod_unidad_ejecutora, num_estatus=:num_estatus,
                fec_ultima_modificacion=NOW(), fk_a001_num_organismo=:fk_a001_num_organismo, fk_a023_num_centro_de_costo=:fk_a023_num_centro_de_costo
            ");
        $NuevoRegistro->execute(array(
            ':ind_nombre_unidad_ejecutora' => $nombre,
            ':ind_descripcion' => $descripcion,
            ':cod_unidad_ejecutora' => $CodMaestro,
            ':fk_a001_num_organismo' => $aplicacion,
            ':num_estatus' => $estatus,
			':fk_a023_num_centro_de_costo' => $centrocosto,
        ));
        $idUnidadEjecutora = $this->_db->lastInsertId();
		$idUnidadEjecutoraRes=$idUnidadEjecutora;
		
      
	  
	    if ($detalle) {
            $nuevoDetalle = $this->_db->prepare(
                "INSERT INTO
                pr_b013_unidad_ejecutora_dependencia
              SET
                fk_prb012_unidad_ejecutora=$idUnidadEjecutora, num_estatus=:num_estatus, ind_nombre_unidad_ejecutora_dependencia=:ind_nombre_unidad_ejecutora_dependencia, cod_unidad_ejecutora_dependencia=:cod_unidad_ejecutora_dependencia
            ");
            for ($i = 0; $i < count($detalle); $i++) {
                if ($statusDetalle) {
                    if (isset($statusDetalle[$i])) {
                        $estatus = $statusDetalle[$i];
                    } else {
                        $estatus = 0;
                    }
                } else {
                    $estatus = 0;
                }
                $nuevoDetalle->execute(array(
                    'num_estatus' => $estatus,
                    'ind_nombre_unidad_ejecutora_dependencia' => $detalle[$i],
                    'cod_unidad_ejecutora_dependencia' => $codDetalle[$i],
                ));
            }
        }
		
		
		if ($detalleRes) {
            $nuevoDetalleRes = $this->_db->prepare(
                "INSERT INTO
                   pr_b014_unidad_ejecutora_responsable
                SET
                  fk_prb012_unidad_ejecutora=:fk_prb012_unidad_ejecutora, num_estatus=:num_estatus, ind_nombre_responsable=:ind_nombre_responsable, 
				  cod_responsable=:cod_responsable");
				  
            for ($i = 0; $i < count($detalleRes); $i++) {
                if ($statusDetalleRes) {
                    if (isset($statusDetalleRes[$i])) {
                        $estatus = $statusDetalleRes[$i];
                    } else {
                        $estatus = 0;
                    }
                } else {
                    $estatus = 0;
                }
                $nuevoDetalleRes->execute(array(
				    'fk_prb012_unidad_ejecutora' => $idUnidadEjecutoraRes,
                    'num_estatus' => $estatus,
                    'ind_nombre_responsable' => $detalleRes[$i],
                    'cod_responsable' => $codDetalleRes[$i],
                ));
            }
        }
		
		
        $error = $NuevoRegistro->errorInfo();
        $error1 = $nuevoDetalle->errorInfo();
        if (!empty($error[1]) && !empty($error[2]) && !empty($error1[1]) && !empty($error1[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idUnidadEjecutora;
        }
    }

    public function metActualizarMiscelaneo($idUnidadEjecutora, $nombre, $descripcion, $CodMaestro, $aplicacion, $estatus, $detalle = false, $statusDetalle = false, $idDetalle = false, $codDetalle = false, $centrocosto, $detalleRes = false, $statusDetalleRes=false, $idDetalleRes = false, $codDetalleRes = false)
    {
        $this->_db->beginTransaction();
        $ActualizarRegistro = $this->_db->prepare(
            "UPDATE
                pr_b012_unidad_ejecutora
              SET
                fk_a018_num_seguridad_usuario='$this->atIdUsuario', ind_nombre_unidad_ejecutora=:ind_nombre_unidad_ejecutora,
                ind_descripcion=:ind_descripcion, num_estatus=:num_estatus,
                fec_ultima_modificacion=NOW(), fk_a001_num_organismo=:fk_a001_num_organismo, fk_a023_num_centro_de_costo=:fk_a023_num_centro_de_costo
              WHERE
                pk_num_unidad_ejecutora='$idUnidadEjecutora'
            ");
        $ActualizarRegistro->execute(array(
            ':ind_nombre_unidad_ejecutora' => $nombre,
            ':ind_descripcion' => $descripcion,
            ':fk_a001_num_organismo' => $aplicacion,
            ':num_estatus' => $estatus,
			':fk_a023_num_centro_de_costo' => $centrocosto,
        ));

		
		
        if ($detalle) {
            for ($i = 0; $i < count($detalle); $i++) {
                if (isset($idDetalle[$i])) {
                    $nuevoDetalle = $this->_db->prepare("
                        UPDATE
                          pr_b013_unidad_ejecutora_dependencia
                        SET
                          num_estatus=:num_estatus, ind_nombre_unidad_ejecutora_dependencia=:ind_nombre_unidad_ejecutora_dependencia, cod_unidad_ejecutora_dependencia=:cod_unidad_ejecutora_dependencia
                        WHERE
                          fk_prb012_unidad_ejecutora=$idUnidadEjecutora AND pk_num_unidad_ejecutora_dependencia='$idDetalle[$i]'
                    ");
                } else {
                    $nuevoDetalle = $this->_db->prepare("
                        INSERT INTO
                          pr_b013_unidad_ejecutora_dependencia
                        SET
                          fk_prb012_unidad_ejecutora=$idUnidadEjecutora, num_estatus=:num_estatus, ind_nombre_unidad_ejecutora_dependencia=:ind_nombre_unidad_ejecutora_dependencia, cod_unidad_ejecutora_dependencia=:cod_unidad_ejecutora_dependencia
                    ");
					
                }

                if ($statusDetalle) {
                    if (isset($statusDetalle[$i])) {
                        $estatus = $statusDetalle[$i];
                    } else {
                        $estatus = 0;
                    }
                } else {
                    $estatus = 0;
                }
                $nuevoDetalle->execute(array(
                    'num_estatus' => $estatus,
                    'ind_nombre_unidad_ejecutora_dependencia' => $detalle[$i],
                    'cod_unidad_ejecutora_dependencia' => $codDetalle[$i]
                ));
            }
        }
			
		
		if ($detalleRes) {
            for ($i = 0; $i < count($detalleRes); $i++) {
                if (isset($idDetalleRes[$i])) {
                    $nuevoDetalleRes = $this->_db->prepare("
                        UPDATE
                          pr_b014_unidad_ejecutora_responsable
                        SET
                          num_estatus=:num_estatus, ind_nombre_responsable=:ind_nombre_responsable, 
				          cod_responsable=:cod_responsable
                        WHERE
                          fk_prb012_unidad_ejecutora=$idUnidadEjecutora AND pk_num_responsable='$idDetalleRes[$i]'
                    ");
                } else {
                    $nuevoDetalleRes = $this->_db->prepare("
                        INSERT INTO
                           pr_b014_unidad_ejecutora_responsable
                        SET
                          fk_prb012_unidad_ejecutora=$idUnidadEjecutora, num_estatus=:num_estatus, ind_nombre_responsable=:ind_nombre_responsable, 
				          cod_responsable=:cod_responsable
                    ");
					
					//var_dump($nuevoDetalle);
                }

                if ($statusDetalleRes) {
                    if (isset($statusDetalleRes[$i])) {
                        $estatus = $statusDetalleRes[$i];
                    } else {
                        $estatus = 0;
                    }
                } else {
                    $estatus = 0;
                }
                $nuevoDetalleRes->execute(array(
                    'num_estatus' => $estatus,
                    'ind_nombre_responsable' => $detalleRes[$i],
                    'cod_responsable' => $codDetalleRes[$i],
                ));
            }
        }
		
		
        $error = $ActualizarRegistro->errorInfo();
        if (isset($nuevoDetalle)) {
            $error1 = $nuevoDetalle->errorInfo();
        } else {
            $error1 = array(1 => null, 2 => null);
        }

        if (!empty($error[1]) && !empty($error[2]) && !empty($error1[1]) && !empty($error1[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idUnidadEjecutora;
        }
    }

    public function metEliminarUnidadEjecutora($idUnidadEjecutora)
    {
	   
        $this->_db->beginTransaction();
		
		   $eliminarUnidadEjecutoraDep = $this->_db->query(
            "DELETE FROM pr_b013_unidad_ejecutora_dependencia WHERE fk_prb012_unidad_ejecutora='$idUnidadEjecutora'"
			);
			
			$eliminarUnidadEjecutoraRes = $this->_db->query(
            "DELETE FROM pr_b014_unidad_ejecutora_responsable WHERE fk_prb012_unidad_ejecutora='$idUnidadEjecutora'"
			);
		
		    $eliminar = $this->_db->query(
            "DELETE FROM pr_b012_unidad_ejecutora WHERE pk_num_unidad_ejecutora='$idUnidadEjecutora'"
			);
				
             $error=$eliminar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idUnidadEjecutora;
            }
    }
	

}
