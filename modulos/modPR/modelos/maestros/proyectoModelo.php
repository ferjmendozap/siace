<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |         Liduvica Bastardo                 |         liduvica@hotmail.com       |        0424-9080200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class proyectoModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarProyecto($idProyecto)
    {
        $tipoCuenta = $this->_db->query("
          SELECT
               *
          FROM
            pr_b005_proyecto 
          WHERE
            pk_num_proyecto='$idProyecto'
        ");
		
	
        $tipoCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCuenta->fetch();
    }
	
	
	
	 public function metMostrarProyectoSelect($idSubPrograma)
    {
        $cuenta = $this->_db->query("
          SELECT
               *
          FROM
            pr_b005_proyecto 
          WHERE
            fk_prb010_num_subprograma=$idSubPrograma");
			
		
		$cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
		
    }
	

    public function metListarProyecto()
    {
        $tipoCuenta = $this->_db->query(
            "SELECT * FROM pr_b005_proyecto"
        );
        $tipoCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCuenta->fetchAll();
    }


    public function metObtenerUltimaProyecto()
    {
	 
       $antepresupuesto = $this->_db->query(
            "SELECT ind_cod_proyecto FROM pr_b005_proyecto ORDER BY ind_cod_proyecto DESC LIMIT 1"
        );
		
        $antepresupuesto->setFetchMode(PDO::FETCH_UNIQUE);
		$registro=$antepresupuesto->fetch();
        return $registro['ind_cod_proyecto'];
    }




    public function metCrearProyecto($nombre,$status,$subprograma,$numproyecto,$flags_acentralizada)
    {
	
	   $registro=$this->metObtenerUltimaProyecto();
			$registro=(int) $registro+1;
			$mun="0";
            for($i=0;$i<(2-strlen($registro));$i++){
                $mun.="0";
            }
            $registro=$mun.$registro;
			
	
	
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    pr_b005_proyecto
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), ind_cod_proyecto=:ind_cod_proyecto,
                    num_estatus=:num_estatus ,ind_descripcion=:ind_descripcion, fk_prb010_num_subprograma=:fk_prb010_num_subprograma, 	
					num_cod_proyecto=:num_cod_proyecto, num_flags_acentralizada=:num_flags_acentralizada
                ");
            $nuevoRegistro->execute(array(
                'ind_cod_proyecto'=>$registro,
                'num_estatus'=>$status,
                'ind_descripcion'=>$nombre,
				'fk_prb010_num_subprograma'=>$subprograma,
				'num_cod_proyecto'=>$numproyecto,
				'num_flags_acentralizada'=>$flags_acentralizada,
               
            ));
            $idRegistro= $this->_db->lastInsertId();
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }

    public function metModificarProyecto($nombre,$status,$subprograma,$numproyecto, $flags_acentralizada,$idProyecto)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        pr_b005_proyecto
                      SET
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), 
                        num_estatus=:num_estatus ,ind_descripcion=:ind_descripcion, fk_prb010_num_subprograma=:fk_prb010_num_subprograma,
					    num_cod_proyecto=:num_cod_proyecto, num_flags_acentralizada=:num_flags_acentralizada   
                      WHERE
                        pk_num_proyecto='$idProyecto'
            ");
            $nuevoRegistro->execute(array(
                
                'num_estatus'=>$status,
                'ind_descripcion'=>$nombre,
				'fk_prb010_num_subprograma'=>$subprograma,
				'num_cod_proyecto'=>$numproyecto,
				'num_flags_acentralizada'=>$flags_acentralizada,
            ));
			
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idProyecto;
            }
    }

    public function metEliminarProyecto($idProyecto)
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                DELETE FROM  pr_b005_proyecto WHERE pk_num_proyecto=:pk_num_proyecto
            ");
            $elimar->execute(array(
                'pk_num_proyecto'=>$idProyecto
            ));
            $error=$elimar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idProyecto;
            }

    }

}
