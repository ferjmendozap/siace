<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |         Liduvica Bastardo                 |         liduvica@hotmail.com       |        0424-9080200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class programaModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarPrograma($idPrograma)
    {
        $tipoCuenta = $this->_db->query("
          SELECT
               *
          FROM
            pr_b009_programa 
          WHERE
            pk_num_programa='$idPrograma'
        ");
		
	
        $tipoCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCuenta->fetch();
    }

    public function metListarPrograma()
    {
        $tipoCuenta = $this->_db->query(
            "SELECT * FROM pr_b009_programa"
        );
        $tipoCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCuenta->fetchAll();
    }


    public function metObtenerUltimaPrograma()
    {
	 
       $antepresupuesto = $this->_db->query(
            "SELECT ind_cod_programa FROM pr_b009_programa ORDER BY ind_cod_programa DESC LIMIT 1"
        );
		
        $antepresupuesto->setFetchMode(PDO::FETCH_UNIQUE);
		$registro=$antepresupuesto->fetch();
        return $registro['ind_cod_programa'];
    }




    public function metCrearPrograma($nombre,$status,$sector,$numprograma)
    {
	
	   $registro=$this->metObtenerUltimaPrograma();
			$registro=(int) $registro+1;
			$mun="0";
            for($i=0;$i<(2-strlen($registro));$i++){
                $mun.="0";
            }
            $registro=$mun.$registro;
			
	
	
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    pr_b009_programa
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), ind_cod_programa=:ind_cod_programa,
                    num_estatus=:num_estatus ,ind_descripcion=:ind_descripcion, fk_prb008_num_sector=:fk_prb008_num_sector,  num_cod_programa=:num_cod_programa 
                ");
            $nuevoRegistro->execute(array(
                'ind_cod_programa'=>$registro,
                'num_estatus'=>$status,
                'ind_descripcion'=>$nombre,
				'fk_prb008_num_sector'=>$sector,
				'num_cod_programa'=>$numprograma,
               
            ));
            $idRegistro= $this->_db->lastInsertId();
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }

    public function metModificarPrograma($nombre,$status,$sector,$numprograma,$idPrograma)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        pr_b009_programa
                      SET
                       fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), 
                       num_estatus=:num_estatus ,ind_descripcion=:ind_descripcion, fk_prb008_num_sector=:fk_prb008_num_sector,num_cod_programa=:num_cod_programa 
                      WHERE
                        pk_num_programa='$idPrograma'
            ");
            $nuevoRegistro->execute(array(
                
                'num_estatus'=>$status,
                'ind_descripcion'=>$nombre,
				'fk_prb008_num_sector'=>$sector,
				'num_cod_programa'=>$numprograma,
            ));
			
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idPrograma;
            }
    }


    public function metEliminarPrograma($idPrograma)
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                DELETE FROM  pr_b009_programa WHERE pk_num_programa=:pk_num_programa
            ");
            $elimar->execute(array(
                'pk_num_programa'=>$idPrograma
            ));
            $error=$elimar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idPrograma;
            }

    }

}
