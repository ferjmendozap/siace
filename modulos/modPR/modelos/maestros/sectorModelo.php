<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |         Liduvica Bastardo                 |         liduvica@hotmail.com       |        0424-9080200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class sectorModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarSector($idSector)
    {
        $tipoCuenta = $this->_db->query("
          SELECT
               *
          FROM
            pr_b008_sector 
          WHERE
            pk_num_sector='$idSector'
        ");
		
	
        $tipoCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCuenta->fetch();
    }

    public function metListarSector()
    {
        $tipoCuenta = $this->_db->query(
            "SELECT * FROM pr_b008_sector"
        );
        $tipoCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCuenta->fetchAll();
    }


    public function metObtenerUltimaSector()
    {
	 
       $antepresupuesto = $this->_db->query(
            "SELECT ind_cod_sector FROM pr_b008_sector ORDER BY ind_cod_sector DESC LIMIT 1"
        );
		
        $antepresupuesto->setFetchMode(PDO::FETCH_UNIQUE);
		$registro=$antepresupuesto->fetch();
        return $registro['ind_cod_sector'];
    }




    public function metCrearSector($nombre,$status)
    {
	
	   $registro=$this->metObtenerUltimaSector();
			$registro=(int) $registro+1;
			$mun="0";
            for($i=0;$i<(2-strlen($registro));$i++){
                $mun.="0";
            }
            $registro=$mun.$registro;
			
	
	
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    pr_b008_sector
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), ind_cod_sector=:ind_cod_sector,
                    num_estatus=:num_estatus ,ind_descripcion=:ind_descripcion 
                ");
            $nuevoRegistro->execute(array(
                'ind_cod_sector'=>$registro,
                'num_estatus'=>$status,
                'ind_descripcion'=>$nombre,
               
            ));
            $idRegistro= $this->_db->lastInsertId();
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }

    public function metModificarSector($nombre,$status,$idSector)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        pr_b008_sector
                      SET
                       fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), 
                       num_estatus=:num_estatus ,ind_descripcion=:ind_descripcion 
                      WHERE
                        pk_num_sector='$idSector'
            ");
            $nuevoRegistro->execute(array(
                
                'num_estatus'=>$status,
                'ind_descripcion'=>$nombre,
            ));
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idSector;
            }
    }

    public function metEliminarSector($idSector)
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                DELETE FROM  pr_b008_sector WHERE pk_num_sector=:pk_num_sector
            ");
            $elimar->execute(array(
                'pk_num_sector'=>$idSector
            ));
            $error=$elimar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idSector;
            }

    }

}
