<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PARTIDA: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO                   |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                 lbastardo@contraloriadebolivar.gob.ve   |         0424-9080200           |
 * |   |                                           |                                        |                                |
 * |___|___________________________________________|________________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        01-09-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class partidaModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarPartida($idPartida)
    {
        /*$partida = $this->_db->query("
         SELECT * FROM pr_b002_partida_presupuestaria ppp
		 INNER JOIN a018_seguridad_usuario ass ON (ppp.fk_a018_num_seguridad_usuario=ass.pk_num_seguridad_usuario)
		 WHERE ppp.pk_num_partida_presupuestaria='$idPartida'
        ");*/
		
		$partida = $this->_db->query("
         SELECT ppp.*,  cpc.pk_num_cuenta, cpc.cod_cuenta, cpc.ind_descripcion, cpc_onco.cod_cuenta as cod_cuenta_onco, cpc_onco.ind_descripcion as ind_descripcion_onco,  
		 cpc_gasto.cod_cuenta as cod_cuenta_gasto, cpc_gasto.ind_descripcion as ind_descripcion_gasto
		 
		 FROM pr_b002_partida_presupuestaria ppp
		 
		 LEFT JOIN cb_b004_plan_cuenta cpc ON (ppp.fk_cbb004_num_plan_cuenta_pub20=cpc.pk_num_cuenta)
         LEFT JOIN cb_b004_plan_cuenta cpc_onco ON (ppp.fk_cbb004_num_plan_cuenta_onco=cpc_onco.pk_num_cuenta)
		 LEFT JOIN cb_b004_plan_cuenta cpc_gasto ON (ppp.fk_cbb004_num_plan_cuenta_gastopub20=cpc_gasto.pk_num_cuenta)
		 
		 INNER JOIN a018_seguridad_usuario ass ON (ppp.fk_a018_num_seguridad_usuario=ass.pk_num_seguridad_usuario)
		 WHERE ppp.pk_num_partida_presupuestaria='$idPartida'
        ");
        $partida->setFetchMode(PDO::FETCH_ASSOC);
        return $partida->fetch();
    }


    public function metCuentaListar()
    {
       
		
		 $cuenta =  $this->_db->query(
            "SELECT amd.cod_detalle, amd.ind_nombre_detalle FROM a005_miscelaneo_maestro  amm
             INNER JOIN a006_miscelaneo_detalle amd ON (amm.pk_num_miscelaneo_maestro = amd.fk_a005_num_miscelaneo_maestro)
             WHERE amm.ind_nombre_maestro='Cuenta Presupuestaria'"
        );
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
    }
	
	
	 public function metAespecifica()
    {
        $aesp =  $this->_db->query(
            "SELECT * FROM  pr_b006_aespecifica"
        );
        $aesp->setFetchMode(PDO::FETCH_ASSOC);
        return $aesp->fetchAll();
    }
	
	
	
	 public function metCuentaContableListar()
    {
        $cuenta =  $this->_db->query(
            "SELECT * FROM  cb_b004_plan_cuenta"
        );
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
    }
	
	 public function metCuentaContablePub20Listar()
    {
        $cuenta =  $this->_db->query(
            "SELECT * FROM  cb_b004_plan_cuenta WHERE num_flag_tipo_cuenta=1"
        );
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
    }


     public function metCuentaContableOncoListar()
    {
        $cuenta =  $this->_db->query(
            "SELECT * FROM  cb_b004_plan_cuenta WHERE num_flag_tipo_cuenta=2"
        );
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
    }

	

    public function metListarPartida()
    {
        $partida = $this->_db->query(
            "SELECT * FROM pr_b002_partida_presupuestaria ORDER BY fk_prb001_num_tipo_cuenta,ind_partida,ind_generica,ind_especifica,ind_subespecifica"
        );
        $partida->setFetchMode(PDO::FETCH_ASSOC);
        return $partida->fetchAll();
    }
	
	
	public function metListarTipoDependenciaInt()
    {
        $dependencia = $this->_db->query(
            "SELECT 
		a004.*,
		rh_c006.pk_num_cargo,
		rh_c006.ind_nombre_cargo AS cargodepen,
		  CONCAT(ind_nombre1,'  ',ind_apellido1)  AS nombre_apellidos
		 FROM a004_dependencia AS a004
		INNER JOIN
		a003_persona a003 ON a003.pk_num_persona = a004.fk_a003_num_persona_responsable
		LEFT JOIN
			rh_c006_tipo_cargo rh_c006 ON rh_c006.pk_num_cargo = a003.pk_num_persona"
       );
	   
	   //var_dump($dependencia);
        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $dependencia->fetchAll();
    }
	
	
	 public function metListarResponsable()
    {
      /*  $tipoParticular = $this->_db->query(
            "SELECT 
			a003.*,
			CONCAT(ind_nombre1,'  ',ind_apellido1)  AS nombre_empleado,
			a006.ind_nombre_detalle,
			a004.pk_num_dependencia,
			a004.ind_dependencia
			 FROM a003_persona  a003
			LEFT JOIN
            a006_miscelaneo_detalle a006 ON a006.pk_num_miscelaneo_detalle = a003.fk_a006_num_miscelaneo_det_tipopersona
			 LEFT JOIN
            a004_dependencia  a004 ON a004.fk_a003_num_persona_responsable =  a003.pk_num_persona
			WHERE cod_detalle='EMP' GROUP BY a003.pk_num_persona
			"
        );*/
		
		$tipoParticular = $this->_db->query(
            "SELECT 
			vl_rh.*,
			rh_c063.ind_descripcion_cargo AS cargoempleado,
			rh_c063.pk_num_puestos,
			a004.ind_dependencia,
			a004.pk_num_dependencia,
		  CONCAT(vl_rh.ind_nombre1,'   ',vl_rh.ind_apellido1)  AS nombre_empleado
			FROM
			a004_dependencia a004 
 			INNER JOIN
			vl_rh_persona_empleado_datos_laborales vl_rh ON vl_rh.pk_num_persona = a004.fk_a003_num_persona_responsable 
			INNER JOIN
			rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = vl_rh.fk_rhc063_num_puestos_cargo  
			 WHERE vl_rh.ind_estatus='1' GROUP BY vl_rh.pk_num_persona");
		
		
		
        $tipoParticular->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoParticular->fetchAll();
    }
	
	 public function metListaCC()
    {
        $centroCosto = $this->_db->query(
            "SELECT * FROM a023_centro_costo"
        );
        $centroCosto->setFetchMode(PDO::FETCH_ASSOC);
        return $centroCosto->fetchAll();
    }

	
	
	 public function metListarPartidas_egresos()
    {
	  
        $partida = $this->_db->query(
            "SELECT @rownum:=@rownum+1 AS id, REPLACE(cod_partida,'.','_') AS partidacod, p.* FROM pr_b002_partida_presupuestaria p, (SELECT @rownum:=0) i WHERE fk_prb001_num_tipo_cuenta='4'  ORDER BY fk_prb001_num_tipo_cuenta,ind_partida,ind_generica,ind_especifica,ind_subespecifica"
        );
        $partida->setFetchMode(PDO::FETCH_ASSOC);
        return $partida->fetchAll();
    }
	
	
	 public function metListarPartidaDetalle()
    {
        $partida = $this->_db->query(
            "SELECT * FROM pr_b002_partida_presupuestaria where ind_tipo='D'"
        );
        $partida->setFetchMode(PDO::FETCH_ASSOC);
        return $partida->fetchAll();
    }
	

    public function metCrearPartida($num_tipo_cuenta, $partida, $generica, $especifica,$subespecifica, $numeral, $codPartida,$denominacion, $ind_tipo,$nivel,$cuenta,
	 $cuentaonco, $cuentagasto, $status)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    pr_b002_partida_presupuestaria
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), cod_partida=:cod_partida,
					ind_partida=:ind_partida, ind_generica=:ind_generica, ind_especifica=:ind_especifica, ind_subespecifica=:ind_subespecifica,                   
					ind_numeral=:ind_numeral, num_estatus=:num_estatus ,ind_denominacion=:ind_denominacion , ind_tipo=:ind_tipo, num_nivel=:num_nivel,      
					fk_prb001_num_tipo_cuenta=:fk_prb001_num_tipo_cuenta,	fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20, 
					fk_cbb004_num_plan_cuenta_onco=:fk_cbb004_num_plan_cuenta_onco,
					fk_cbb004_num_plan_cuenta_gastopub20=:fk_cbb004_num_plan_cuenta_gastopub20
                ");
            $nuevoRegistro->execute(array(
			    'fk_prb001_num_tipo_cuenta'=>$num_tipo_cuenta,
				'ind_partida'=>$partida,
				'ind_generica'=>$generica,
				'ind_especifica'=>$especifica,
				'ind_subespecifica'=>$subespecifica,
				'ind_numeral'=>$numeral,
                'cod_partida'=>$codPartida,
                'num_estatus'=>$status,
                'ind_denominacion'=>$denominacion,
				'ind_tipo'=>$ind_tipo,
				'num_nivel'=>$nivel,
				'fk_cbb004_num_plan_cuenta_pub20'=>$cuenta,
				'fk_cbb004_num_plan_cuenta_onco'=>$cuentaonco,
				'fk_cbb004_num_plan_cuenta_gastopub20'=>$cuentagasto,
            ));
            $idRegistro= $this->_db->lastInsertId();
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }

    public function metModificarPartida($num_tipo_cuenta, $partida, $generica, $especifica,$subespecifica, $numeral, $codPartida,$denominacion, $ind_tipo,$nivel,$cuenta,
	$cuentaonco, $cuentagasto, $status, $idPartida)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                         pr_b002_partida_presupuestaria
                      SET
                       fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), cod_partida=:cod_partida,
					   ind_partida=:ind_partida, ind_generica=:ind_generica, ind_especifica=:ind_especifica, ind_subespecifica=:ind_subespecifica,                   
					   ind_numeral=:ind_numeral,
                       num_estatus=:num_estatus ,ind_denominacion=:ind_denominacion , ind_tipo=:ind_tipo, num_nivel=:num_nivel,  
					   fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20, 
					   fk_cbb004_num_plan_cuenta_onco=:fk_cbb004_num_plan_cuenta_onco,
					   fk_cbb004_num_plan_cuenta_gastopub20=:fk_cbb004_num_plan_cuenta_gastopub20,
					   fk_prb001_num_tipo_cuenta=:fk_prb001_num_tipo_cuenta
                      WHERE
                        pk_num_partida_presupuestaria='$idPartida'
            ");
			
			
	
			
            $nuevoRegistro->execute(array(
                'fk_prb001_num_tipo_cuenta'=>$num_tipo_cuenta,
				'ind_partida'=>$partida,
				'ind_generica'=>$generica,
				'ind_especifica'=>$especifica,
				'ind_subespecifica'=>$subespecifica,
				'ind_numeral'=>$numeral,
                'cod_partida'=>$codPartida,
                'num_estatus'=>$status,
                'ind_denominacion'=>$denominacion,
				'ind_tipo'=>$ind_tipo,
				'num_nivel'=>$nivel,
				'fk_cbb004_num_plan_cuenta_pub20'=>$cuenta,
				'fk_cbb004_num_plan_cuenta_onco'=>$cuentaonco,
				'fk_cbb004_num_plan_cuenta_gastopub20'=>$cuentagasto,
            ));
			
			
			//var_dump($nuevoRegistro);
			//exit;
			
			
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idPartida;
            }
    }

    public function metEliminarPartida($idPartida)
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                DELETE FROM pr_b002_partida_presupuestaria WHERE pk_num_partida_presupuestaria=:pk_num_partida_presupuestaria
            ");
            $elimar->execute(array(
                'pk_num_partida_presupuestaria'=>$idPartida
            ));
            $error=$elimar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idPartida;
            }

    }

}
