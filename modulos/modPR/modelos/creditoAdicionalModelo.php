<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * Antepresupuesto: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO                   |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                 lbastardo@contraloriadebolivar.gob.ve   |         0424-9080200           |
 * |   |                                           |                                        |                                |
 * |___|___________________________________________|________________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        01-09-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class creditoAdicionalModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarCreditoAdicional($idCredito)
    {
    	
		$credito = $this->_db->query("
         SELECT pca.*, pbp.pk_num_presupuesto, pbp.ind_cod_presupuesto, pbp.ind_tipo_presupuesto  FROM pr_d004_credito_adicional pca 
		 INNER JOIN pr_b004_presupuesto  pbp ON pca.fk_prb004_num_presupuesto=pbp.pk_num_presupuesto
		 INNER JOIN a018_seguridad_usuario asu ON asu.pk_num_seguridad_usuario=pca.fk_a018_num_seguridad_usuario
		 WHERE pca.pk_num_credito_adicional='$idCredito'
        ");
		
	
        $credito->setFetchMode(PDO::FETCH_ASSOC);
        return $credito->fetch();
    }

    //AGREGADO PARA LISTAR PARTIR PARTIDAS LIGADAS A UN ANTEPROYECTO
	public function metMostrarCreditoAdicionalDetalle($idCredito,$status)
    {
	
      if ($status=='AP') {	
	     $credito = $this->_db->query(
             
		  "SELECT
              pbp.ind_tipo_presupuesto,pbp.pk_num_presupuesto, ppd.fk_pr006_num_aespecifica, pca.*, pcad.*,pbd.*,ppd.pk_num_presupuesto_det, ppd.fk_prb002_num_partida_presupuestaria, ppd.num_monto_ajustado,ppd.num_monto_compromiso
            FROM
              pr_d004_credito_adicional pca

              INNER JOIN pr_d005_credito_adicional_det pcad ON pca.pk_num_credito_adicional = pcad.fk_prd004_num_credito_adicional
			   
			  INNER JOIN pr_c002_presupuesto_det ppd ON pcad.fk_prc002_num_presupuesto_det = ppd.pk_num_presupuesto_det
			  
			  INNER JOIN pr_b004_presupuesto pbp ON ppd.fk_prb004_num_presupuesto = pbp.pk_num_presupuesto
			   

              INNER JOIN pr_b002_partida_presupuestaria pbd ON ppd.fk_prb002_num_partida_presupuestaria = pbd.pk_num_partida_presupuestaria

             WHERE pca.pk_num_credito_adicional='$idCredito' and  pcad.num_monto_credito>0

            ORDER by pbd.pk_num_partida_presupuestaria ASC
          "
		  
        );}
		
		else {
        $credito = $this->_db->query(
          
		   "SELECT
              pbp.ind_tipo_presupuesto, pbp.pk_num_presupuesto, pca.*, pcad.*,pbd.*,ppd.pk_num_presupuesto_det,ppd.fk_prb002_num_partida_presupuestaria,
			  ppd.num_monto_ajustado,ppd.num_monto_compromiso, ppd.fk_pr006_num_aespecifica
            FROM
              pr_d004_credito_adicional pca

              INNER JOIN pr_d005_credito_adicional_det pcad ON pca.pk_num_credito_adicional = pcad.fk_prd004_num_credito_adicional
			   
			  INNER JOIN pr_c002_presupuesto_det ppd ON pcad.fk_prc002_num_presupuesto_det = ppd.pk_num_presupuesto_det
			  
			   INNER JOIN pr_b004_presupuesto pbp ON ppd.fk_prb004_num_presupuesto = pbp.pk_num_presupuesto

              INNER JOIN pr_b002_partida_presupuestaria pbd ON ppd.fk_prb002_num_partida_presupuestaria = pbd.pk_num_partida_presupuestaria

             WHERE pca.pk_num_credito_adicional='$idCredito'

            ORDER by pbd.pk_num_partida_presupuestaria ASC
          "
		  
		  
        );}
		
		
        $credito->setFetchMode(PDO::FETCH_ASSOC);
        return $credito->fetchAll();
    }
	


    public function metListarCreditoAdicional($status = false)
    {
	  if ($status) {
            $status = "WHERE ind_estado='$status'";
        }
	
        $creditoa = $this->_db->query(
            "SELECT * FROM pr_d004_credito_adicional $status"
        );
		
		
        $creditoa->setFetchMode(PDO::FETCH_ASSOC);
        return $creditoa->fetchAll();
    }
	
	
	 public function metPresupuestoListar()
    {
        $cuenta =  $this->_db->query(
            "SELECT * FROM pr_b004_presupuesto"
        );
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
    }
	
	
	 public function metIdPresupuesto()
    {
	    $year_actual=date("Y"); 
        $id =  $this->_db->query(
            "SELECT
               pk_num_presupuesto, ind_cod_presupuesto, ind_tipo_presupuesto
            FROM
               pr_b004_presupuesto
            WHERE fec_anio = '$year_actual' AND ind_estado='AP'
          "
        );
		
				
        $id->setFetchMode(PDO::FETCH_ASSOC);
        return $id->fetchAll();
    }
		
	
	 public function metSelectPresupuesto()
    {
	    $year_actual=date("Y"); 
        $ano =  $this->_db->query(
            "SELECT
              pba.pk_num_presupuesto, pba.ind_cod_presupuesto, pba.ind_cod_presupuesto, pca.*
            FROM
               pr_b004_presupuesto pba
               INNER JOIN pr_c002_presupuesto_det pca ON pba.pk_num_presupuesto = pca.fk_prb004_num_presupuesto
               INNER JOIN pr_b002_partida_presupuestaria pbp ON pbp.pk_num_partida_presupuestaria = pca.fk_prb002_num_partida_presupuestaria
            WHERE pba.fec_anio = '$year_actual' AND pba.ind_estado='AP'
            ORDER by pca.fk_prb002_num_partida_presupuestaria ASC
          "
        );
				
        $ano->setFetchMode(PDO::FETCH_ASSOC);
        return $ano->fetchAll();
    }
	
	

    public function metCrearCreditoAdicional($fk_prb004_num_presupuesto, $fec_anio,  $ind_numero_oficio, $fec_oficio, $ind_numero_decreto, $fec_decreto, $num_monto_total, $motivo, $partidas=false, $monto=false, $fec_credito_adicional)
    {

        $this->_db->beginTransaction();
	        $codigo=count($this->metListarCreditoAdicional())+1;
            $mun="0";
            for($i=0;$i<(3-strlen($codigo));$i++){
                $mun.="0";
            }
            $codigo=$mun.$codigo;
			
			
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    pr_d004_credito_adicional
                  SET
                    fk_prb004_num_presupuesto=:fk_prb004_num_presupuesto,fk_rhb001_num_empleado_creado='$this->atIdUsuario',   
					fk_rhb001_num_empleado_aprobado=:fk_rhb001_num_empleado_aprobado,fec_anio=:fec_anio, 
					ind_numero_oficio=:ind_numero_oficio,fec_oficio=:fec_oficio,ind_numero_decreto=:ind_numero_decreto,fec_decreto=:fec_decreto,
					num_monto_total=:num_monto_total,   
					ind_motivo=:ind_motivo,fec_creado=CURDATE(),fec_aprobado=:fec_aprobado,fec_credito_adicional=:fec_credito_adicional,ind_estado=:ind_estado,   
					fec_ultima_modificacion=NOW(),fk_a018_num_seguridad_usuario='$this->atIdUsuario',
					ind_cod_credito_adicional=:ind_cod_credito_adicional
					
                ");
            $nuevoRegistro->execute(array(
			    'fk_prb004_num_presupuesto'=>$fk_prb004_num_presupuesto,
			    'ind_cod_credito_adicional'=>$codigo,
				'fec_anio'=>$fec_anio,
				'ind_numero_oficio'=>$ind_numero_oficio,
				'fec_oficio'=>$fec_oficio,
				'ind_numero_decreto'=>$ind_numero_decreto,
				'fec_decreto'=>$fec_decreto,
                'num_monto_total'=>$num_monto_total,
				'ind_motivo'=>$motivo,
				'fec_aprobado'=>'0000-00-00',
                'ind_estado'=>'PR',
				'fk_rhb001_num_empleado_aprobado'=>0,
				'fec_credito_adicional'=>$fec_credito_adicional,
				
				
            ));		
			
				
            $idRegistro=$this->_db->lastInsertId();
			
		$cont=0;	
		if(isset($partidas) && $partidas!=false){	
		 for($i=0;$i<count($partidas);$i++) {
		  
		  $id=$partidas[$i];
		 
		   if ($id>0)
		   {
		        
		       $monto[$i]=$this->FormatoMonto_modelo($monto[$i]); 
			   
			   $nuevaPartida=$this->_db->prepare("
                  INSERT INTO
                    pr_d005_credito_adicional_det
                  SET
                    fk_prd004_num_credito_adicional=:fk_prd004_num_credito_adicional,
                    fk_prc002_num_presupuesto_det=:fk_prc002_num_presupuesto_det,  
					num_monto_credito=:num_monto_credito, 
					fec_ultima_modificacion=NOW(),fk_a018_num_seguridad_usuario='$this->atIdUsuario'	
               ");
			
              $nuevaPartida->execute(array(
			    'fk_prd004_num_credito_adicional'=>$idRegistro,
                'fk_prc002_num_presupuesto_det'=>$partidas[$i],
				'num_monto_credito'=> $monto[$i]
              ));	
			
		   } //$id 
	     }	// for
        } //fin $partidas
		
		
		
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }



    public function metModificarCreditoAdicional( $fec_anio,  $ind_numero_oficio, $fec_oficio, $ind_numero_decreto, $fec_decreto, $num_monto_total, $motivo, $partidas=false, $monto=false, $fec_credito_adicional, $idCreditoAdicional)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                         pr_d004_credito_adicional
                      SET
					      fk_rhb001_num_empleado_creado='$this->atIdUsuario',   
					      fec_anio=:fec_anio, 
					      ind_numero_oficio=:ind_numero_oficio,fec_oficio=:fec_oficio,ind_numero_decreto=:ind_numero_decreto,fec_decreto=:fec_decreto,
						  fec_credito_adicional=:fec_credito_adicional,
					      num_monto_total=:num_monto_total,   
					      ind_motivo=:ind_motivo,  
					      fec_ultima_modificacion=NOW(),fk_a018_num_seguridad_usuario='$this->atIdUsuario'
					      
					 WHERE 
					    pk_num_credito_adicional='$idCreditoAdicional' 
            ");
            $nuevoRegistro->execute(array(
				'fec_anio'=>$fec_anio,
				'ind_numero_oficio'=>$ind_numero_oficio,
				'fec_oficio'=>$fec_oficio,
				'ind_numero_decreto'=>$ind_numero_decreto,
				'fec_decreto'=>$fec_decreto,
                'num_monto_total'=>$num_monto_total,
				'ind_motivo'=>$motivo,
				'fec_credito_adicional'=>$fec_credito_adicional,
            ));
			
			
		
		  $eliminarDet = $this->_db->query(
            "DELETE FROM pr_d005_credito_adicional_det WHERE fk_prd004_num_credito_adicional='$idCreditoAdicional'"
        );	
			
        if(isset($partidas) && $partidas!=false){	
		 for($i=0;$i<count($partidas);$i++) {
		  
		  $id=$partidas[$i];
		 
		 
		   if ($id>0)
		   {    
		       $monto[$i]=$this->FormatoMonto_modelo($monto[$i]); 
			   
			   $nuevaPartida=$this->_db->prepare("
                  INSERT INTO
                    pr_d005_credito_adicional_det
                  SET
                    fk_prd004_num_credito_adicional=:fk_prd004_num_credito_adicional,
                    fk_prc002_num_presupuesto_det=:fk_prc002_num_presupuesto_det,  
					num_monto_credito=:num_monto_credito, 
					fec_ultima_modificacion=NOW(),fk_a018_num_seguridad_usuario='$this->atIdUsuario'	
               ");
			
              $nuevaPartida->execute(array(
			    'fk_prd004_num_credito_adicional'=>$idCreditoAdicional,
                'fk_prc002_num_presupuesto_det'=>$partidas[$i],
				'num_monto_credito'=> $monto[$i],
				
              ));	
			
		   } //$id 
	     }	// for
        } //fin $partidas
		
		  
           $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idCreditoAdicional;
            }
    }



    public function metEliminarCreditoAdicional($idCreditoAdicional)
    {
	   
        $this->_db->beginTransaction();
		
		   $eliminarAntePresupuestoDet = $this->_db->query(
            "DELETE FROM pr_d005_credito_adicional_det WHERE fk_prd004_num_credito_adicional='$idCreditoAdicional'"
			);
		
		  
			
		    $eliminar = $this->_db->query(
            "DELETE FROM pr_d004_credito_adicional WHERE pk_num_credito_adicional='$idCreditoAdicional'"
			);
			
			
             $error=$eliminar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idCreditoAdicional;
            }
    }
	
	 public function metObtenerEstadoCreditoAdicional($idCreditoAdicional)
    {
	 
       $antepresupuesto = $this->_db->query(
            "SELECT ind_estado FROM pr_d004_credito_adicional  WHERE pk_num_credito_adicional='$idCreditoAdicional'"
        );
		
        $antepresupuesto->setFetchMode(PDO::FETCH_UNIQUE);
		$registro=$antepresupuesto->fetch();
        return $registro['ind_estado'];
    }
	

	
	public function metAccionesCreditoAdicional($idCreditoAdicional, $status)
    {
        $this->_db->beginTransaction();
        if ($status=='AP') {
           $aprobarregistro = $this->_db->prepare("
                 	
				  UPDATE
                     pr_d004_credito_adicional
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    fk_rhb001_num_empleado_aprobado='$this->atIdUsuario', fec_aprobado=NOW(), ind_estado=:ind_estado
                  WHERE
                    pk_num_credito_adicional='$idCreditoAdicional'
            ");
       
	   
            $aprobarregistro->execute(array(
              'ind_estado' => $status
           ));
		
					
			 $actualizarpresupuesto=$this->_db->query("
                UPDATE pr_c002_presupuesto_det ppd
                INNER JOIN pr_d005_credito_adicional_det pcad ON (ppd.pk_num_presupuesto_det=pcad.fk_prc002_num_presupuesto_det AND pcad.num_monto_credito>0)
                INNER JOIN pr_d004_credito_adicional pca
                    ON (pcad.fk_prd004_num_credito_adicional=pca.pk_num_credito_adicional)
                INNER JOIN pr_b004_presupuesto ppdd
                    ON (ppd.fk_prb004_num_presupuesto=ppdd.pk_num_presupuesto)
                SET  ppd.num_monto_ajustado=pcad.num_monto_credito+ppd.num_monto_ajustado, ppd.num_monto_credito=pcad.num_monto_credito+ppd.num_monto_credito
                WHERE  pca.pk_num_credito_adicional='$idCreditoAdicional'
            ");	
			
		 $eliminarDet = $this->_db->query(
                "DELETE FROM pr_d005_credito_adicional_det WHERE fk_prd004_num_credito_adicional='$idCreditoAdicional' AND num_monto_credito<=0" 
			 );
					
	}	
	
	
	
	  if ($status=='RE'){
	   
	     $estado=$this->metObtenerEstadoCreditoAdicional($idCreditoAdicional);
		  
		  if ($estado=='RV')
		      $status='PR';
		  elseif ($estado=='AP')	  
		      $status='RV';
		  
		   
	      $aprobarregistro = $this->_db->prepare("
                   UPDATE
                     pr_d004_credito_adicional
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), ind_estado=:ind_estado
                  WHERE
                    pk_num_credito_adicional='$idCreditoAdicional'
            ");
			
	  
	   
		$aprobarregistro->execute(array(
            'ind_estado' => $status,
            ));
	
	 }
	
	
	if ($status=='RV') {
           $aprobarregistro = $this->_db->prepare("
                 	
				 UPDATE
                     pr_d004_credito_adicional
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    fk_rhb001_num_empleado_revisado='$this->atIdUsuario', fec_revisado=NOW(), ind_estado=:ind_estado
                  WHERE
                    pk_num_credito_adicional='$idCreditoAdicional'
            ");
       
	   
            $aprobarregistro->execute(array(
              'ind_estado' => $status
           ));
		
			
	   }
	
	 if ($status=='AN') {
           $aprobarregistro = $this->_db->prepare("
                 	
				  UPDATE
                     pr_d004_credito_adicional
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    ind_estado=:ind_estado
                  WHERE
                    pk_num_credito_adicional='$idCreditoAdicional'
            ");
       
	   
            $aprobarregistro->execute(array(
              'ind_estado' => $status
           ));
		
			
	   }
       
			
            $error =   $aprobarregistro->errorInfo();
			
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idCreditoAdicional;
            }
    }
	
	
	
	public function metListarPresupuesto($status = false)
    {
	  if ($status) {
            $status = "WHERE ind_estado='$status'";
        }
	
        $ajuste = $this->_db->query(
            "SELECT * FROM pr_b004_presupuesto $status"
        );
        $ajuste->setFetchMode(PDO::FETCH_ASSOC);
        return $ajuste->fetchAll();
    }
	
	
	public function FormatoMonto_modelo($monto_f)
    {
       if(!isset($monto_f)){
                $monto_f=0;
            }
			else {
			     $monto_f=str_replace(".", "", $monto_f);
			     $monto_f=str_replace(",", ".", $monto_f);
				 
			}
			 return $monto_f;
    }	
	
	
	
	
}
