<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Antepresupuesto
 * Antepresupuesto: Reformulación Modelo
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO                   |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                 lbastardo@contraloriadebolivar.gob.ve   |         0424-9080200           |
 * |   |                                           |                                        |                                |
 * |___|___________________________________________|________________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        01-09-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class reformulacionModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarReformulacion($idReformulacion)
    {
        $antepresupuesto = $this->_db->query("
         SELECT pdr.*, pbp.pk_num_presupuesto, pbp.ind_cod_presupuesto, pbp.ind_tipo_presupuesto, asu.ind_usuario FROM pr_d006_reformulacion_presupuesto pdr
		 
		    INNER JOIN pr_b004_presupuesto pbp ON pdr.fk_prb004_num_presupuesto=pbp.pk_num_presupuesto
			INNER JOIN a018_seguridad_usuario asu ON asu.pk_num_seguridad_usuario=pdr.fk_a018_num_seguridad_usuario
		 
		  WHERE pdr.pk_num_reformulacion_presupuesto='$idReformulacion'
		
        ");
		
        $antepresupuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $antepresupuesto->fetch();
    }

    //-----------------------------------------------------------------
	public function metMostrarReformulacionDetalle($idReformulacion)
    {
        $partida = $this->_db->query(
            "SELECT
              *
            FROM
			   
               pr_d006_reformulacion_presupuesto pba
			   
			   INNER JOIN pr_b004_presupuesto pbps
               ON pba.fk_prb004_num_presupuesto = pbps.pk_num_presupuesto
               INNER JOIN pr_d007_reformulacion_presupuesto_det pca
               ON pba.pk_num_reformulacion_presupuesto = pca.fk_prd006_num_reformulacion_presupuesto
               INNER JOIN pr_b002_partida_presupuestaria pbp
               ON pbp.pk_num_partida_presupuestaria = pca.fk_prb002_num_partida_presupuestaria

            WHERE pba.pk_num_reformulacion_presupuesto='$idReformulacion'
            ORDER by pca.fk_prb002_num_partida_presupuestaria ASC
          "
        );
		
		//var_dump($partida);
		//exit;
		
        $partida->setFetchMode(PDO::FETCH_ASSOC);
        return $partida->fetchAll();
    }
	//-------------------------------------------------------------------------------------------------------

     //AGREGADO PARA LISTAR PARTIR PARTIDAS LIGADAS A UN ANTEPROYECTO
	public function metMostrarReformulacionTitulo()
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_b002_partida_presupuestaria 
            WHERE ind_tipo='T'
            ORDER by cod_partida ASC
          "
        );
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
	//-------------------------------------------------------------------------------------------------------

      //AGREGADO PARA LISTAR PARTIR PARTIDAS LIGADAS A UN ANTEPROYECTO
	public function metMostrarPresupuestoTituloEs($id)
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_b002_partida_presupuestaria 
            WHERE ind_tipo='T' and pk_num_partida_presupuestaria='$id'
            ORDER by cod_partida ASC
          "
        );
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
	//-------------------------------------------------------------------------------------------------------


    public function metMostrarReformulacionAespecificaEs($id)
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_b006_aespecifica 
            WHERE  pk_num_aespecifica='$id'
            ORDER by pk_num_aespecifica ASC
          "
        );
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
	
	//-------------------------------------------------------------------------------------------------------

	public function metExisteAespecificaEs($id,$idReformulacion)
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_d007_reformulacion_presupuesto_det 
            WHERE  fk_pr_b006_aespecifica='$id' AND fk_prd006_num_reformulacion_presupuesto='$idReformulacion'
            
          "
        );
	
		
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
	
	//-------------------------------------------------------------------------------------------------------------
    
	
    public function metIdPresupuesto()
    {
	    $year_actual=date("Y"); 
        $id =  $this->_db->query(
            "SELECT
               pk_num_presupuesto, ind_cod_presupuesto
            FROM
               pr_b004_presupuesto
            WHERE fec_anio = $year_actual AND ind_estado='AP' 
          "
        );
  }
  
  
    public function metReformulacionListar()
    {
        $cuenta =  $this->_db->query(
            "SELECT * FROM  pr_d006_reformulacion_presupuesto"
        );
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
    }
	

    public function metListarReformulacion($status = false)
    {
	  if ($status) {
            $status = "WHERE ind_estado='$status'";
        }
	
        $antepresupuesto = $this->_db->query(
            "SELECT * FROM pr_d006_reformulacion_presupuesto $status"
        );
		
		
        $antepresupuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $antepresupuesto->fetchAll();
    }
	
      //BUSCAR PRESUPUESTO
	public function metBuscarPresupuesto($cod_presupuesto)
    {
        $titulo = $this->_db->query(
            "SELECT
              pk_num_presupuesto
            FROM
                pr_b004_presupuesto
            WHERE ind_cod_presupuesto= $cod_presupuesto
          ");
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }

     public function metPresupuestoListar()
    {
	    $anio_actual=date("Y"); 
        $cuenta =  $this->_db->query(
            "SELECT * FROM pr_b004_presupuesto WHERE fec_anio=$anio_actual"
        );
		
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
    }
	
	
	
	public function metPresupuestoListarTodos()
    {
	  
        $cuenta =  $this->_db->query(
            "SELECT * FROM pr_b004_presupuesto"
        );
		
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
    }
	
	
	public function metListarPresupuestoAnio($anio)
    {
        $cuenta =  $this->_db->query(
            "SELECT * FROM pr_b004_presupuesto WHERE fec_anio=$anio"
        );
		
		//var_dump($cuenta);
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
    }
 
 

//-------------------------------------------------------------------------------------------------------

      //AGREGADO PARA LISTAR PARTIR PARTIDAS LIGADAS A UN ANTEPROYECTO
	public function metMostrarPartidasTituloEs($id)
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_b002_partida_presupuestaria 
            WHERE ind_tipo='T' and pk_num_partida_presupuestaria='$id'
            ORDER by cod_partida ASC
          "
        );
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
	//-------------------------------------------------------------------------------------------------------
	
	public function metMostrarAntepresupuestoAespecificaEs($id)
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_b006_aespecifica 
            WHERE  pk_num_aespecifica='$id'
            ORDER by pk_num_aespecifica ASC
          "
        );
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
	
	//-------------------------------------------------------------------------------------------------------
	
    public function metCrearReformulacion($fk_prb004_num_presupuesto, $fec_anio, $fec_mes, $ind_numero_gaceta, $fec_gaceta, $ind_numero_resolucion, $fec_resolucion,     $num_monto_reformulacion, $ind_descripcion, $partidas=false, $monto=false, $aespecifica=false)
    {
	    
        $this->_db->beginTransaction();
		
            $codigo=count($this->metListarReformulacion())+1;
            $mun="0";
            for($i=0;$i<(3-strlen($codigo));$i++){
                $mun.="0";
            }
            $codigo=$mun.$codigo;
			
			
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    pr_d006_reformulacion_presupuesto
                  SET
                    fk_prb004_num_presupuesto=:fk_prb004_num_presupuesto,fk_rhb001_num_empleado_creado='$this->atIdUsuario',   
					fk_rhb001_num_empleado_aprobado=:fk_rhb001_num_empleado_aprobado,fec_anio=:fec_anio, fec_mes=:fec_mes, 
					ind_numero_gaceta=:ind_numero_gaceta,fec_gaceta=:fec_gaceta,ind_numero_resolucion=:ind_numero_resolucion,fec_resolucion=:fec_resolucion,
					num_monto_reformulacion=:num_monto_reformulacion,   
					ind_descripcion=:ind_descripcion,fec_creado=CURDATE(),fec_aprobado=:fec_aprobado,fec_reformulacion=CURDATE(),ind_estado=:ind_estado,   
					fec_ultima_modificacion=NOW(),fk_a018_num_seguridad_usuario='$this->atIdUsuario',
					cod_reformulacion=:cod_reformulacion
                ");
            $nuevoRegistro->execute(array(
			    'fk_prb004_num_presupuesto'=>$fk_prb004_num_presupuesto,
			    'cod_reformulacion'=>$codigo,
				'fec_anio'=>$fec_anio,
				'fec_mes'=>$fec_mes,
				'ind_numero_gaceta'=>$ind_numero_gaceta,
				'fec_gaceta'=>$fec_gaceta,
				'ind_numero_resolucion'=>$ind_numero_resolucion,
				'fec_resolucion'=>$fec_resolucion,
                'num_monto_reformulacion'=>$num_monto_reformulacion,
				'ind_descripcion'=>$ind_descripcion,
				'fec_aprobado'=>'0000-00-00',
                'ind_estado'=>'PR',
				'fk_rhb001_num_empleado_aprobado'=>0	
            ));		
            $idRegistro=$this->_db->lastInsertId();
			
		   
		
		$cont=0;
		if(isset($partidas) && $partidas!=false){	
		  for($i=0;$i<count($partidas);$i++) {
		  
		 $id=$partidas[$i];
		  $indicador=0;
		  //$indicador=count($this->metMostrarPartidasTituloEs($id));
		  
		   $indicador=count($this->metMostrarAntepresupuestoAespecificaEs($id));
		   
		   $indicador=$indicador+count($this->metMostrarPartidasTituloEs($id));
		   
		 if ($indicador<=0)
		 {
		  
		   //$monto[$i]=$this->FormatoMonto_modelo($monto[$i]); 
		   $monto[$i]=0;
		   
		    
		    if(isset($aespecifica[$cont]))
			{
			  if ($aespecifica[$cont]>99)
			     $ae= $aespecifica[$cont]/100;
			  else	 
			     $ae= $aespecifica[$cont];
			}   
			else  
			  $ae=0;
			  

			 $cont=$cont+1;
			 
			
			$titulo = $this->_db->query("SELECT * FROM pr_c002_presupuesto_det WHERE fk_prb004_num_presupuesto='$fk_prb004_num_presupuesto' AND fk_prb002_num_partida_presupuestaria='$partidas[$i]'");
			
					  
                  $titulo->setFetchMode(PDO::FETCH_ASSOC);
                  $existe=count($titulo->fetchAll());
				  
 			 
		if ($existe==0)
	   {
		 
			$nuevaPartida=$this->_db->prepare("
                  INSERT INTO
                    pr_d007_reformulacion_presupuesto_det
                  SET
                    fk_prd006_num_reformulacion_presupuesto=:fk_prd006_num_reformulacion_presupuesto,
                    fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,  
					num_monto=:num_monto, ind_estado=:ind_estado,fec_ultima_modificacion=NOW(),fk_a018_num_seguridad_usuario='$this->atIdUsuario',
					fk_prc002_num_presupuesto_det=:fk_prc002_num_presupuesto_det, fk_pr_b006_aespecifica=:fk_pr_b006_aespecifica
					
             ");
			
             $nuevaPartida->execute(array(
			    'fk_prd006_num_reformulacion_presupuesto'=>$idRegistro,
                'fk_prb002_num_partida_presupuestaria'=>$partidas[$i],
				'num_monto'=> $monto[$i],
				'ind_estado'=>'PR',
				'fk_prc002_num_presupuesto_det'=>1,
				'fk_pr_b006_aespecifica'=>0,
            ));
		//	}//fin if monto
			
			}//if existe
			else 
			{
			    $id=-3;
			}
			
		   }//fin if indicador
		  }	
      }  
		    if($id==-3)
	             $idReformulacion=-3;
		    else 
			     $idReformulacion=$idRegistro;	 
		
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idReformulacion;
            }
    }


    public function metModificarReformulacion($fk_prb004_num_presupuesto, $fec_anio, $fec_mes, $ind_numero_gaceta, $fec_gaceta, $ind_numero_resolucion, $fec_resolucion, $num_monto_reformulacion, $ind_descripcion, $partidas=false, $monto=false, $aespecifica=false, $idReformulacion)
    {
	
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                          pr_d006_reformulacion_presupuesto
                      SET
					    fec_anio=:fec_anio, fec_mes=:fec_mes, 
					    ind_numero_gaceta=:ind_numero_gaceta,fec_gaceta=:fec_gaceta,ind_numero_resolucion=:ind_numero_resolucion,fec_resolucion=:fec_resolucion,
					    num_monto_reformulacion=:num_monto_reformulacion,   
					    ind_descripcion=:ind_descripcion,
					    fec_ultima_modificacion=NOW(),fk_a018_num_seguridad_usuario='$this->atIdUsuario'
					   
					  WHERE 
					    pk_num_reformulacion_presupuesto='$idReformulacion' 
            ");
			
            $nuevoRegistro->execute(array(
			    
				'fec_anio'=>$fec_anio,
				'fec_mes'=>$fec_mes,
				'ind_numero_gaceta'=>$ind_numero_gaceta,
				'fec_gaceta'=>$fec_gaceta,
				'ind_numero_resolucion'=>$ind_numero_resolucion,
				'fec_resolucion'=>$fec_resolucion,
                'num_monto_reformulacion'=>$num_monto_reformulacion,
				'ind_descripcion'=>$ind_descripcion,
				
             ));
			
			
		  $eliminarAntePresupuestoDet = $this->_db->query(
            "DELETE FROM pr_d007_reformulacion_presupuesto_det WHERE fk_prd006_num_reformulacion_presupuesto ='$idReformulacion'"
        );	
			
		  $cont=0;	
		  
          if(isset($partidas) && $partidas!=false){	
		  for($i=0;$i<count($partidas);$i++) { 
		  $id=$partidas[$i];
		  $indicador=0;
		  $existe=0;
		 
		  
		   $indicador=count($this->metMostrarAntepresupuestoAespecificaEs($id));
		   $indicador=$indicador+count($this->metMostrarPartidasTituloEs($id));
		  
		  if ($indicador<=0)
		  {
		    $monto[$i]=$this->FormatoMonto_modelo($monto[$i]); 
			
			if(isset($aespecifica[$cont]))
			{
			  if ($aespecifica[$cont]<99)
				  $ae=$aespecifica[$cont];
			  else 
				  $ae= $aespecifica[$cont]/100;
			}	  
			else  
			  $ae=0;
			  
			 $cont=$cont+1;
			 
											  
				  $titulo = $this->_db->query("SELECT * FROM pr_c002_presupuesto_det WHERE fk_prb004_num_presupuesto='$fk_prb004_num_presupuesto' AND fk_prb002_num_partida_presupuestaria='$partidas[$i]'");							  
                  $titulo->setFetchMode(PDO::FETCH_ASSOC);
                  $existe=count($titulo->fetchAll());
 			 
		    if ($existe==0)
		    {
				$nuevaPartida=$this->_db->prepare("
					  INSERT INTO
						pr_d007_reformulacion_presupuesto_det
					  SET
						fk_prd006_num_reformulacion_presupuesto=:fk_prd006_num_reformulacion_presupuesto,
						fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,  
						num_monto=:num_monto, ind_estado=:ind_estado,fec_ultima_modificacion=NOW(),fk_a018_num_seguridad_usuario='$this->atIdUsuario',
						fk_prc002_num_presupuesto_det=:fk_prc002_num_presupuesto_det, fk_pr_b006_aespecifica=:fk_pr_b006_aespecifica
						
				");
				
				
				$nuevaPartida->execute(array(
					'fk_prd006_num_reformulacion_presupuesto'=>$idReformulacion,
					'fk_prb002_num_partida_presupuestaria'=>$partidas[$i],
					'num_monto'=> $monto[$i],
					'ind_estado'=>'PR',
					'fk_pr_b006_aespecifica'=> $ae,
					'fk_prc002_num_presupuesto_det'=>1,
				));
		    }//if existe
			else 
			{
			    $id=-3;
			}
		  	
		  }	 
		 }
       }  
	   
	    if($id==-3)
	       $idReformulacion=-3;
		
			  
           $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idReformulacion;
            }
    }

   

    public function metEliminarReformulacion($idReformulacion)
    {
        $this->_db->beginTransaction();
		
		   $eliminarAntePresupuestoDet = $this->_db->query(
            "DELETE FROM pr_d007_reformulacion_presupuesto_det WHERE fk_prd006_num_reformulacion_presupuesto='$idReformulacion'"
			);
		
			
		    $eliminar = $this->_db->query(
            "DELETE FROM pr_d006_reformulacion_presupuesto WHERE pk_num_reformulacion_presupuesto='$idReformulacion'"
			);
		
			
             $error=$eliminar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idReformulacion;
            }
    }
	

	
	
	public function metAccionesReformulacion($idReformulacion, $status)
    {
        $this->_db->beginTransaction();
        if ($status=='AP') {
            $aprobarregistro = $this->_db->prepare("
                  UPDATE
                    pr_d006_reformulacion_presupuesto
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    fk_rhb001_num_empleado_aprobado='$this->atIdUsuario', fec_aprobado=CURDATE(), ind_estado=:ind_estado
                  WHERE
                    pk_num_reformulacion_presupuesto='$idReformulacion'");
			
			
						
			$actualizarpresupuesto=$this->_db->query("
			INSERT INTO pr_c002_presupuesto_det (fk_prb004_num_presupuesto, fk_prb002_num_partida_presupuestaria,num_flags_anexa, num_flags_reformulacion, num_monto_aprobado, num_monto_ajustado,num_monto_compromiso, num_monto_causado,num_monto_pagado,num_monto_incremento, num_monto_disminucion, num_monto_credito,           
					fk_pr006_num_aespecifica,fk_a018_num_seguridad_usuario, fec_ultima_modificacion)
			SELECT  fk_prb004_num_presupuesto, fk_prb002_num_partida_presupuestaria,0, 1, num_monto, num_monto,0,0,0,0,0,0,prpd.fk_pr_b006_aespecifica,'$this->atIdUsuario',NOW()
			FROM pr_d007_reformulacion_presupuesto_det	prpd
			INNER JOIN  pr_d006_reformulacion_presupuesto prp ON (prp.pk_num_reformulacion_presupuesto=prpd.fk_prd006_num_reformulacion_presupuesto)
		    WHERE
                pk_num_reformulacion_presupuesto='$idReformulacion'
            ");	
			
			
				
       }else if ($status=='AN'){
	   
	      $aprobarregistro = $this->_db->prepare("
                  UPDATE
                    pr_d006_reformulacion_presupuesto
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), ind_estado=:ind_estado
                  WHERE
                    pk_num_reformulacion_presupuesto='$idReformulacion'");
	        
	   }
	   
	   
        $aprobarregistro->execute(array(
            'ind_estado' => $status,
        ));
		
	
            $error = $aprobarregistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idReformulacion;
            }
  		
    }
	
	
	

	public function FormatoMonto_modelo($monto_f)
    {
       if(!isset($monto_f)){
                $monto_f=0;
            }
			else {
			     $monto_f=str_replace(".", "", $monto_f);
			     $monto_f=str_replace(",", ".", $monto_f);
				 
			}
			 return $monto_f;
    }	
		
	
}
