<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Antepresupuesto
 * 
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO                   |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                 lbastardo@contraloriadebolivar.gob.ve   |         0424-9080200           |
 * |   |                                           |                                        |                                |
 * |___|___________________________________________|________________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        01-09-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class cierremesModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarCierreMes($idCierreMes)
    {
        $cierre = $this->_db->query("
         SELECT * FROM pr_d001_ejecucion_presupuestaria pep
		 INNER JOIN pr_c002_presupuesto_det pcpd ON (pcpd.pk_num_presupuesto_det = pep.fk_prc002_num_presupuesto_det)
		 INNER JOIN pr_b004_presupuesto pcp ON pcp.pk_num_presupuesto =pcpd.fk_prb004_num_presupuesto
		 WHERE pep.pk_num_ejecucion_presupuestaria='$idCierreMes'
        ");
        $cierre->setFetchMode(PDO::FETCH_ASSOC);
        return $cierre->fetch();
    }
	

    //AGREGADO PARA LISTAR  PARTIDAS LIGADAS A UN ANTEPROYECTO
	public function metMostrarPresupuestoDetalle($idPresupuesto)
    {
        $partida = $this->_db->query(
            "SELECT
              *
            FROM
               pr_b004_presupuesto pba
               INNER JOIN pr_c002_presupuesto_det pca ON pba.pk_num_presupuesto = pca.fk_prb004_num_presupuesto
               INNER JOIN pr_b002_partida_presupuestaria pbp ON pbp.pk_num_partida_presupuestaria = pca.fk_prb002_num_partida_presupuestaria
            WHERE pba.pk_num_presupuesto='$idPresupuesto'
            ORDER by pca.fk_prb002_num_partida_presupuestaria ASC
          "
        );
        $partida->setFetchMode(PDO::FETCH_ASSOC);
        return $partida->fetchAll();
    }
	//-------------------------------------------------------------------------------------------------------

     //AGREGADO PARA LISTAR PARTIR PARTIDAS LIGADAS A UN ANTEPROYECTO
	public function metMostrarPresupuestoTitulo()
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_b002_partida_presupuestaria 
            WHERE ind_tipo='T'
            ORDER by cod_partida ASC
          "
        );
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
	//-------------------------------------------------------------------------------------------------------

      //AGREGADO PARA LISTAR PARTIR PARTIDAS LIGADAS A UN ANTEPROYECTO
	public function metMostrarPresupuestoTituloEs($id)
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_b002_partida_presupuestaria 
            WHERE ind_tipo='T' and pk_num_partida_presupuestaria='$id'
            ORDER by cod_partida ASC
          "
        );
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
	//-------------------------------------------------------------------------------------------------------
  
    public function metCuentaListar()
    {
        $cuenta =  $this->_db->query(
            "SELECT * FROM  pr_b001_tipo_cuenta"
        );
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
		
		 
    }
	

    public function metListarCierreMes($status = false)
    {
	  if ($status) {
            $status = "WHERE ind_estatus='$status'";
        }
	
        $cierremes = $this->_db->query(
            "SELECT *,sum(num_disponibilidad_presupuestaria) as num_disponibilidad_presupuestaria_sum ,
			sum(num_disponibilidad_financiera) as num_disponibilidad_financiera_sum
			FROM pr_d001_ejecucion_presupuestaria pdep
			INNER JOIN pr_c002_presupuesto_det ppd ON (pdep.fk_prc002_num_presupuesto_det=ppd.pk_num_presupuesto_det)
			INNER JOIN pr_b004_presupuesto pbp ON (pbp.pk_num_presupuesto=ppd.fk_prb004_num_presupuesto)
			GROUP BY pbp.pk_num_presupuesto,pdep.fec_anio,pdep.fec_mes"
        );
		
		
        $cierremes->setFetchMode(PDO::FETCH_ASSOC);
        return $cierremes->fetchAll();
    }
	
	
	public function metPresupuestoListar()
    {
        $cuenta =  $this->_db->query(
            "SELECT * FROM pr_b004_presupuesto"
        );
		
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
    }
	
	
	public function metBuscarCierreMensual($fec_anio, $fec_mes, $presupuesto)
    {
        $cuenta =  $this->_db->query(
            "SELECT * FROM  pr_d001_ejecucion_presupuestaria pdep
             INNER JOIN pr_c002_presupuesto_det ppd ON (pdep.fk_prc002_num_presupuesto_det=ppd.pk_num_presupuesto_det)
             INNER JOIN pr_b004_presupuesto pbp ON (pbp.pk_num_presupuesto=ppd.fk_prb004_num_presupuesto) WHERE pdep.fec_anio='$fec_anio' and  pdep.fec_mes= '$fec_mes' and   
			 pbp.pk_num_presupuesto= '$presupuesto'"
        );
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
    }
	

    public function metMostrarCierreMensual($idCierremes)
    {
        $cuenta =  $this->_db->query(
            "SELECT * FROM  pr_d001_ejecucion_presupuestaria pdep
             INNER JOIN pr_c002_presupuesto_det ppd ON (pdep.fk_prc002_num_presupuesto_det=ppd.pk_num_presupuesto_det)
             INNER JOIN pr_b004_presupuesto pbp ON (pbp.pk_num_presupuesto=ppd.fk_prb004_num_presupuesto) WHERE pdep.pk_num_ejecucion_presupuestaria='$idCierremes'"
        );
		
		
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetch();
    }
	

    public function metCrearCierreMes($fec_anio, $fec_mes, $presupuesto)
    { 
	     $generar='';
	     $codigo=0;
	     $codigo=count($this->metBuscarCierreMensual($fec_anio, $fec_mes, $presupuesto));
		 $codigo2=1;
		 
		 //----------------------------
		 $numero_mes=substr($fec_mes,0, 1);
		 
		 if ($numero_mes > 0)
		   $fec_mes_anterior=substr($fec_mes,0, 2);
		 else  
		   $fec_mes_anterior=substr($fec_mes,1, 1);
		   
		
		 $fec_mes_a=$fec_mes_anterior-1;
		
		 if ($fec_mes_a <= 10)
		    $fec_mes_anterior='0'.$fec_mes_a;
		   
		 
		 
		 if ($fec_mes!='01')
		   $codigo2=count($this->metBuscarCierreMensual($fec_anio, $fec_mes_anterior, $presupuesto));
		 
		 //------------------------------
		 
		
        $this->_db->beginTransaction();
		
		if ($codigo==0 && $codigo2>0){
		
		$fec_mes_anterior=substr($fec_mes,1, 1);
		
		$fec_mes_a=$fec_mes_anterior-1;
		
		if ($fec_mes_a < 10)
		   $fec_mes_anterior='0'.$fec_mes_a;
		  
	     if ($fec_mes=='01')
			   $generar = $this->_db->prepare("INSERT INTO pr_d001_ejecucion_presupuestaria  (fk_prc002_num_presupuesto_det, fec_anio,fec_mes,
						num_disponibilidad_presupuestaria_inicial,num_disponibilidad_financiera_inicial,num_incremento,num_disminucion,num_credito,num_presupuesto_ajustado,
						num_compromiso, num_causado, num_pagado, num_disponibilidad_presupuestaria,num_disponibilidad_financiera,num_variacion,fec_ultima_modificacion,
						fk_a018_num_seguridad_usuario)
					SELECT pk_num_presupuesto_det, '$fec_anio', '$fec_mes',
						num_monto_aprobado, num_monto_aprobado,
						num_monto_incremento,num_monto_disminucion,num_monto_credito,num_monto_ajustado,num_monto_compromiso,
						num_monto_causado, num_monto_pagado, num_monto_ajustado-num_monto_compromiso, num_monto_ajustado-num_monto_pagado,0,NOW(),
					   '$this->atIdUsuario'
					FROM pr_c002_presupuesto_det WHERE fk_prb004_num_presupuesto='$presupuesto'");
	      else
				  $generar = $this->_db->prepare("INSERT INTO pr_d001_ejecucion_presupuestaria  (fk_prc002_num_presupuesto_det, fec_anio,fec_mes,
						num_disponibilidad_presupuestaria_inicial,num_disponibilidad_financiera_inicial,num_incremento,num_disminucion,num_credito,num_presupuesto_ajustado,
						num_compromiso, num_causado, num_pagado, num_disponibilidad_presupuestaria,num_disponibilidad_financiera,num_variacion,fec_ultima_modificacion,
						fk_a018_num_seguridad_usuario)
				  SELECT pk_num_presupuesto_det, '$fec_anio', '$fec_mes',
						(SELECT num_disponibilidad_presupuestaria FROM pr_d001_ejecucion_presupuestaria  WHERE fec_anio='$fec_anio' and fec_mes='$fec_mes_anterior' and                
						fk_prc002_num_presupuesto_det=pk_num_presupuesto_det),
						(SELECT num_disponibilidad_financiera FROM pr_d001_ejecucion_presupuestaria  WHERE fec_anio='$fec_anio' and fec_mes='$fec_mes_anterior' and     
						fk_prc002_num_presupuesto_det=pk_num_presupuesto_det),
						num_monto_incremento,num_monto_disminucion,num_monto_credito,num_monto_ajustado,num_monto_compromiso,
						num_monto_causado, num_monto_pagado, num_monto_ajustado-num_monto_compromiso, num_monto_ajustado-num_monto_pagado,0,NOW(),
						 '$this->atIdUsuario'
				   FROM pr_c002_presupuesto_det WHERE fk_prb004_num_presupuesto='$presupuesto'");
				
			
		
		 // $generar = $this->_db->prepare("CALL siace.generar_cierremensual('".$fec_anio."','".$fec_mes."','".$presupuesto."','".$this->atIdUsuario."')");
		 
		 
		  $generar->execute();
		} 
		  
		 
			if ($generar)
			{
              $idRegistro=1;
			} 
		    else
			{ 
			  $idRegistro=0;
			}
			
			if ($codigo2<=0)
			   $idRegistro=2;
			  
            //$error = $generar->errorInfo();
			
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
			   
                $this->_db->commit();
                return $idRegistro;
            }
    }


   
    public function metCrearPreCierreMes($fec_anio, $fec_mes, $presupuesto)
    { 
	     $generar='';
		 $temporal='';
	     $codigo=0;
	     $codigo=count($this->metBuscarCierreMensual($fec_anio, $fec_mes, $presupuesto));
		 $codigo2=1;
		 
		 //----------------------------
		 $numero_mes=substr($fec_mes,0, 1);
		 
		 if ($numero_mes > 0)
		   $fec_mes_anterior=substr($fec_mes,0, 2);
		 else  
		   $fec_mes_anterior=substr($fec_mes,1, 1);
		   
		
		 $fec_mes_a=$fec_mes_anterior-1;
		
		 if ($fec_mes_a <= 10)
		    $fec_mes_anterior='0'.$fec_mes_a;
		   
		 
		 
		 if ($fec_mes!='01')
		   $codigo2=count($this->metBuscarCierreMensual($fec_anio, $fec_mes_anterior, $presupuesto));
		 
		 //------------------------------
		 
		
        $this->_db->beginTransaction();
		
		if ($codigo==0 && $codigo2>0){
		
		$fec_mes_anterior=substr($fec_mes,1, 1);
		
		$fec_mes_a=$fec_mes_anterior-1;
		
		if ($fec_mes_a < 10)
		   $fec_mes_anterior='0'.$fec_mes_a;
		   
		   
		   
		   $temporal = $this->_db->prepare("DROP TABLE pr_d001_ejecucion_presupuestaria_pre");
		   $temporal->execute();
		   $temporal = $this->_db->prepare("CREATE TABLE pr_d001_ejecucion_presupuestaria_pre LIKE pr_d001_ejecucion_presupuestaria"); 
		   $temporal->execute();
		   $temporal = $this->_db->prepare("INSERT INTO pr_d001_ejecucion_presupuestaria_pre SELECT * FROM pr_d001_ejecucion_presupuestaria"); 
		   $temporal->execute();
		  
		  
	     if ($fec_mes=='01')
			   $generar = $this->_db->prepare("INSERT INTO pr_d001_ejecucion_presupuestaria_pre  (fk_prc002_num_presupuesto_det, fec_anio,fec_mes,
						num_disponibilidad_presupuestaria_inicial,num_disponibilidad_financiera_inicial,num_incremento,num_disminucion,num_credito,num_presupuesto_ajustado,
						num_compromiso, num_causado, num_pagado, num_disponibilidad_presupuestaria,num_disponibilidad_financiera,num_variacion,fec_ultima_modificacion,
						fk_a018_num_seguridad_usuario)
					SELECT pk_num_presupuesto_det, '$fec_anio', '$fec_mes',
						num_monto_aprobado, num_monto_aprobado,
						num_monto_incremento,num_monto_disminucion,num_monto_credito,num_monto_ajustado,num_monto_compromiso,
						num_monto_causado, num_monto_pagado, num_monto_ajustado-num_monto_compromiso, num_monto_ajustado-num_monto_pagado,0,NOW(),
					   '$this->atIdUsuario'
					FROM pr_c002_presupuesto_det WHERE fk_prb004_num_presupuesto='$presupuesto'");
	      else
				  $generar = $this->_db->prepare("INSERT INTO pr_d001_ejecucion_presupuestaria_pre  (fk_prc002_num_presupuesto_det, fec_anio,fec_mes,
						num_disponibilidad_presupuestaria_inicial,num_disponibilidad_financiera_inicial,num_incremento,num_disminucion,num_credito,num_presupuesto_ajustado,
						num_compromiso, num_causado, num_pagado, num_disponibilidad_presupuestaria,num_disponibilidad_financiera,num_variacion,fec_ultima_modificacion,
						fk_a018_num_seguridad_usuario)
				  SELECT pk_num_presupuesto_det, '$fec_anio', '$fec_mes',
						(SELECT num_disponibilidad_presupuestaria FROM pr_d001_ejecucion_presupuestaria  WHERE fec_anio='$fec_anio' and fec_mes='$fec_mes_anterior' and                
						fk_prc002_num_presupuesto_det=pk_num_presupuesto_det),
						(SELECT num_disponibilidad_financiera FROM pr_d001_ejecucion_presupuestaria  WHERE fec_anio='$fec_anio' and fec_mes='$fec_mes_anterior' and     
						fk_prc002_num_presupuesto_det=pk_num_presupuesto_det),
						num_monto_incremento,num_monto_disminucion,num_monto_credito,num_monto_ajustado,num_monto_compromiso,
						num_monto_causado, num_monto_pagado, num_monto_ajustado-num_monto_compromiso, num_monto_ajustado-num_monto_pagado,0,NOW(),
						 '$this->atIdUsuario'
				   FROM pr_c002_presupuesto_det WHERE fk_prb004_num_presupuesto='$presupuesto'");
				
		
		 // $generar = $this->_db->prepare("CALL siace.generar_cierremensual('".$fec_anio."','".$fec_mes."','".$presupuesto."','".$this->atIdUsuario."')");
		 
		 
		  $generar->execute();
		} 
		  
		 
			if ($generar)
			{
              $idRegistro=1;
			} 
		    else
			{ 
			  $idRegistro=0;
			}
			
			if ($codigo2<=0)
			   $idRegistro=2;
			  
            //$error = $generar->errorInfo();
			
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
			   
                $this->_db->commit();
                return $idRegistro;
            }
    }
   


    public function metModificarAntepresupuesto($fec_anio, $fec_antepresupuesto,$fec_inicio, $dec_fin, $ind_numero_gaceta, $fec_gaceta, $ind_numero_decreto, $fec_decreto,$num_monto_presupuestado, $num_monto_generado, $partidas=false, $monto=false, $idAntepresupuesto)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                         pr_b003_antepresupuesto
                      SET
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),fec_anio=:fec_anio,fec_antepresupuesto=:fec_antepresupuesto,    
						fec_inicio=:fec_inicio, dec_fin=:dec_fin, ind_numero_gaceta=:ind_numero_gaceta, fec_gaceta=:fec_gaceta,ind_numero_decreto=:ind_numero_decreto,  
					    fec_decreto=:fec_decreto,  num_monto_presupuestado=:num_monto_presupuestado, num_monto_generado=:num_monto_generado   
					 WHERE 
					    pk_num_antepresupuesto='$idAntepresupuesto' 
            ");
            $nuevoRegistro->execute(array(
			    'fec_anio'=>$fec_anio,
				'fec_antepresupuesto'=>$fec_antepresupuesto,
				'fec_inicio'=>$fec_inicio,
				'dec_fin'=>$dec_fin,
				'ind_numero_gaceta'=>$ind_numero_gaceta,
				'fec_gaceta'=>$fec_gaceta,
				'ind_numero_decreto'=>$ind_numero_decreto,
				'fec_decreto'=>$fec_decreto,
                'num_monto_presupuestado'=>$num_monto_presupuestado,
				'num_monto_generado'=>$num_monto_generado,
               
            ));
			
			
		  $eliminarAntePresupuestoDet = $this->_db->query(
            "DELETE FROM pr_c001_antepresupuesto_det WHERE fk_prb003_num_antepresupuesto='$idAntepresupuesto'"
        );	
			
          if(isset($partidas) && $partidas!=false){	
		  for($i=0;$i<count($partidas);$i++) { 
		  $id=$partidas[$i];
		  $indicador=0;
		  $indicador=count($this->metMostrarAntepresupuestoTituloEs($id));
		  if ($indicador<=0)
		  {
			$nuevaPartida=$this->_db->prepare("
                  INSERT INTO
                    pr_c001_antepresupuesto_det
                  SET
                    fk_prb003_num_antepresupuesto=:fk_prb003_num_antepresupuesto,
                    fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,  
					num_monto_presupuestado=:num_monto_presupuestado
            ");
			
            $nuevaPartida->execute(array(
			    'fk_prb003_num_antepresupuesto'=>$idAntepresupuesto,
                'fk_prb002_num_partida_presupuestaria'=> $partidas[$i],
				'num_monto_presupuestado'=> $monto[$id]
            ));
		  }	
		 }
       }  
			  
           $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idAntepresupuesto;
            }
    }



    public function metEliminarAntepresupuesto($idAntepresupuesto)
    {
        $this->_db->beginTransaction();
		
		   $eliminarAntePresupuestoDet = $this->_db->query(
            "DELETE FROM pr_c001_antepresupuesto_det WHERE fk_prb003_num_antepresupuesto='$idAntepresupuesto'"
			);
		
		  
			
		    $eliminar = $this->_db->query(
            "DELETE FROM pr_b003_antepresupuesto WHERE pk_num_antepresupuesto='$idAntepresupuesto'"
			);
		
			
             $error=$eliminar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idAntepresupuesto;
            }
    }
	

	public function metAccionesAntepresupuesto($idAntepresupuesto, $status)
    {
        $this->_db->beginTransaction();
        if ($status=='AP') {
            $aprobarregistro = $this->_db->prepare("
                  UPDATE
                    pr_b003_antepresupuesto
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    fk_rhb001_num_empleado_aprobado_por='$this->atIdUsuario', fec_aprobado=NOW(), ind_estatus=:ind_estatus
                  WHERE
                    pk_num_antepresupuesto='$idAntepresupuesto'
            ");
       }else {
	  
            $aprobarregistro = $this->_db->prepare("
                  UPDATE
                    pr_b003_antepresupuesto
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    fec_generado=NOW(), ind_estatus=:ind_estatus
                  WHERE
                    pk_num_antepresupuesto='$idAntepresupuesto'
            ");
      
	   
	   }
	   
        $aprobarregistro->execute(array(
            'ind_estatus' => $status
        ));
		
		
		    $codigo=count($this->metListarPresupuesto())+1;
            $mun="0";
            for($i=0;$i<(3-strlen($codigo));$i++){
                $mun.="0";
            }
		     $codigo=$mun.$codigo;
		
	    if ($status=='GE') {
		    $generar = $this->_db->prepare("CALL siace.generar_presupuesto('".$idAntepresupuesto."','".$codigo."')");
			
			$generar->execute();
		
	    }	
	
	
            $error =  $aprobarregistro->errorInfo();
			
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idAntepresupuesto;
            }
    }
	

	
	
	
}
