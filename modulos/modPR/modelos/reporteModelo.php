<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Presupuesto
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           Liduvica Bastardo            |          liduvica@hotmail.com       |         04249080200            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-12-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/

class reporteModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener("idUsuario");
    }


    public function metListarPresupuestoAnio($anio)
    {
        $cuenta =  $this->_db->query(
            "SELECT * FROM pr_b004_presupuesto WHERE fec_anio=$anio"
        );
		
		//var_dump($cuenta);
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
    }
 
 
    public function metListarAntePresupuestoAnio($anio)
    {
        $cuenta =  $this->_db->query(
            "SELECT * FROM pr_b003_antepresupuesto WHERE fec_anio=$anio"
        );
		
		//var_dump($cuenta);
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
    }
	
	
	public function metListarPresupuestoReformulacion($presupuesto)
    {
        $cuenta =  $this->_db->query(
            "SELECT * FROM pr_d006_reformulacion_presupuesto WHERE fk_prb004_num_presupuesto=$presupuesto and ind_estado='AP'"
        );
		
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
    }


	public function metListarPresupuestoAjuste($presupuesto)
    {
        $cuenta =  $this->_db->query("
         SELECT DISTINCT pap.* FROM pr_d002_ajuste_presupuestario pap 
		 INNER JOIN  pr_d003_ajuste_presupuestario_det pdap ON pap.pk_num_ajuste_presupuestario = pdap.fk_prd002_num_ajuste_presupuestario
		 INNER JOIN  pr_c002_presupuesto_det ppd ON pdap.fk_prc002_num_presupuesto_det = ppd.pk_num_presupuesto_det
		 INNER JOIN pr_b004_presupuesto  pbp ON ppd.fk_prb004_num_presupuesto=pbp.pk_num_presupuesto
		 WHERE ppd.fk_prb004_num_presupuesto='$presupuesto' and pap.ind_estado='AP'
        ");
		
        $cuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $cuenta->fetchAll();
    }



    public function metListarPartida($cod_partida)
    {
	
	     if($cod_partida!="error"){
            $complemento1=" AND cod_partida like '%".$cod_partida."%'";
        }else{
            $complemento1=" ";
        }
	
        $partida = $this->_db->query(
            "SELECT * FROM pr_b002_partida_presupuestaria  WHERE 1 $complemento1 
			 ORDER BY fk_prb001_num_tipo_cuenta,ind_partida,ind_generica,ind_especifica,ind_subespecifica"
        );
		
		/*var_dump($partida);
		exit;*/
		
        $partida->setFetchMode(PDO::FETCH_ASSOC);
        return $partida->fetchAll();
    }
	
	
	public function metListarOrganismoInt($idorganismo)
    {
        $organismoint= $this->_db->query("SELECT * FROM a001_organismo WHERE ind_tipo_organismo='I' AND pk_num_organismo='".$idorganismo."'");
        $organismoint->setFetchMode(PDO::FETCH_ASSOC);
        return $organismoint->fetchAll();
		
    }
	
	
    public function getReportePresupuesto($idpresupuesto,$fec_anio,$organismo)
    {
        if($idpresupuesto!="error"){
            $complemento1=" AND pbp.pk_num_presupuesto='".$idpresupuesto."'";
        }else{
            $complemento1=" ";
        }
		
		 if($fec_anio!="error"){
            $complemento2=" AND pbp.fec_anio='".$fec_anio."'";
        }else{
            $complemento2=" ";
        }
		
		 if($organismo!="error"){
            $complemento3=" AND pbp.fk_a001_num_organismo='".$organismo."'";
        }else{
            $complemento3=" ";
        }


        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            " SELECT *, pbip.ind_tipo_presupuesto, pbip.ind_tipo_categoria  FROM pr_b004_presupuesto pbp
			  INNER JOIN pr_b016_indice_presupuestario pbip ON (pbp.fk_prb016_indice_presupuestario=pbip.pk_num_indice_presupuestario)
			  INNER JOIN pr_c002_presupuesto_det pcpd ON (pbp.pk_num_presupuesto=pcpd.fk_prb004_num_presupuesto)
			  INNER JOIN pr_b002_partida_presupuestaria ppp ON (ppp.pk_num_partida_presupuestaria=pcpd.fk_prb002_num_partida_presupuestaria)
			WHERE 1 $complemento1 $complemento2 $complemento3"
        );
		
		
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetchAll();
    }


    
	    public function getReportePresupuestoAntepresupuesto($idpresupuesto,$fec_anio,$organismo)
    {
        if($idpresupuesto!="error"){
            $complemento1=" AND pbp.pk_num_presupuesto='".$idpresupuesto."'";
        }else{
            $complemento1=" ";
        }
		
		 if($fec_anio!="error"){
            $complemento2=" AND pbp.fec_anio='".$fec_anio."'";
        }else{
            $complemento2=" ";
        }
		
		 if($organismo!="error"){
            $complemento3=" AND fk_a001_num_organismo='".$organismo."'";
        }else{
            $complemento3=" ";
        }
      
		
		
	     $pruebaPost = $this->_db->query(
           "SELECT DISTINCT ppp.cod_partida, ppp.ind_denominacion, pcpd.num_monto_presupuestado, pcpd.num_monto_aprobado, pbp.ind_tipo_presupuesto
 FROM pr_b003_antepresupuesto pbpa
			  INNER JOIN pr_c001_antepresupuesto_det pcpd ON (pbpa.pk_num_antepresupuesto=pcpd.fk_prb003_num_antepresupuesto)
			  INNER JOIN pr_b004_presupuesto pbp ON(pbp.fk_prb003_num_antepresupuesto=pbpa.pk_num_antepresupuesto)
			  INNER JOIN pr_c002_presupuesto_det pcp ON (pbp.pk_num_presupuesto=pcp.fk_prb004_num_presupuesto)

			  INNER JOIN pr_b002_partida_presupuestaria ppp ON (ppp.pk_num_partida_presupuestaria=pcpd.fk_prb002_num_partida_presupuestaria)
			WHERE 1 $complemento1 $complemento2
UNION
 SELECT ppp.cod_partida, ppp.ind_denominacion, 0, pcp.num_monto_aprobado,'' FROM pr_b004_presupuesto pbp

  INNER JOIN pr_b003_antepresupuesto pbpa ON (pbpa.pk_num_antepresupuesto=pbp.fk_prb003_num_antepresupuesto)

  INNER JOIN pr_c002_presupuesto_det pcp ON (pbp.pk_num_presupuesto=pcp.fk_prb004_num_presupuesto)
  INNER JOIN pr_b002_partida_presupuestaria ppp ON (ppp.pk_num_partida_presupuestaria=pcp.fk_prb002_num_partida_presupuestaria)
  WHERE 1 $complemento1 $complemento2 AND ppp.cod_partida NOT IN(SELECT DISTINCT ppp.cod_partida
 FROM pr_b003_antepresupuesto pbpa
			  INNER JOIN pr_c001_antepresupuesto_det pcpd ON (pbpa.pk_num_antepresupuesto=pcpd.fk_prb003_num_antepresupuesto)
			  INNER JOIN pr_b004_presupuesto pbp ON(pbp.fk_prb003_num_antepresupuesto=pbpa.pk_num_antepresupuesto)
			  INNER JOIN pr_c002_presupuesto_det pcp ON (pbp.pk_num_presupuesto=pcp.fk_prb004_num_presupuesto)

			  INNER JOIN pr_b002_partida_presupuestaria ppp ON (ppp.pk_num_partida_presupuestaria=pcpd.fk_prb002_num_partida_presupuestaria)
			WHERE 1  $complemento1 $complemento2)"
        );
		

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetchAll();
    }


	
    public function metAespecifica()
    {
        $aesp =  $this->_db->query(
            "SELECT * FROM  pr_b006_aespecifica"
        );
        $aesp->setFetchMode(PDO::FETCH_ASSOC);
        return $aesp->fetchAll();
    } 
	 
	 
	 public function metExisteAespecificaEs($id,$idAntepresupuesto)
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_c001_antepresupuesto_det 
            WHERE  fk_pr_b006_aespecifica='$id' AND fk_prb003_num_antepresupuesto='$idAntepresupuesto'
            
          "
        );
	
		
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }


     public function metExisteAespecificaEs2($id,$idPresupuesto)
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_c002_presupuesto_det 
            WHERE  fk_pr006_num_aespecifica='$id' AND fk_prb004_num_presupuesto='$idPresupuesto'
          "
        );
		
		
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
   
   
     public function metExisteAespecificaEs3($id,$idPresupuesto)
    {
        $titulo = $this->_db->query(
            "SELECT
              *
            FROM
                pr_c002_presupuesto_det pcpd
				INNER JOIN  pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
            WHERE  fk_pr006_num_aespecifica='$id' AND fk_prb004_num_presupuesto='$idPresupuesto'
            
          "
        );
		
		//var_dump($titulo);
		//exit;
		
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
   
     
	 
	public function metExisteAespecificaEs4($id,$idPresupuesto)
    {
        $titulo = $this->_db->query(
        
		  "SELECT
              *
            FROM
                pr_c002_presupuesto_det pcpd
				INNER JOIN pr_d001_ejecucion_presupuestaria pdep ON (pdep.fk_prc002_num_presupuesto_det = pcpd.pk_num_presupuesto_det)
            WHERE  pcpd.fk_pr006_num_aespecifica='$id' AND pcpd.fk_prb004_num_presupuesto='$idPresupuesto'
          "
        );
		
        $titulo->setFetchMode(PDO::FETCH_ASSOC);
        return $titulo->fetchAll();
    }
   


     public function getReporteAntePresupuesto($idpresupuesto,$fec_anio,$organismo)
    {
        if($idpresupuesto!="error"){
            $complemento1=" AND pk_num_antepresupuesto='".$idpresupuesto."'";
        }else{
            $complemento1=" ";
        }
			
		
		 if($fec_anio!="error"){
            $complemento2=" AND fec_anio='".$fec_anio."'";
        }else{
            $complemento2=" ";
        }
		
		if($organismo!="error"){
            $complemento3=" AND fk_a001_num_organismo='".$organismo."'";
        }else{
            $complemento3=" ";
        }
		

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            " SELECT * FROM pr_b003_antepresupuesto pbp
			  INNER JOIN pr_c001_antepresupuesto_det pcpd ON (pbp.pk_num_antepresupuesto=pcpd.fk_prb003_num_antepresupuesto)
			  INNER JOIN pr_b002_partida_presupuestaria ppp ON (ppp.pk_num_partida_presupuestaria=pcpd.fk_prb002_num_partida_presupuestaria)
			WHERE  1 $complemento1 $complemento2 $complemento3"
        );
		

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
		
		
        return $pruebaPost->fetchAll();
    }



     public function getReporteAntePresupuestoAprobado($idpresupuesto,$fec_anio)
    {
        if($idpresupuesto!="error"){
            $complemento1=" AND pk_num_antepresupuesto='".$idpresupuesto."'";
        }else{
            $complemento1=" ";
        }
			
		
		 if($fec_anio!="error"){
            $complemento2=" AND fec_anio='".$fec_anio."'";
        }else{
            $complemento2=" ";
        }
		

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            " SELECT * FROM pr_b003_antepresupuesto pbp
			  INNER JOIN pr_c001_antepresupuesto_det pcpd ON (pbp.pk_num_antepresupuesto=pcpd.fk_prb003_num_antepresupuesto)
			  INNER JOIN pr_b002_partida_presupuestaria ppp ON (ppp.pk_num_partida_presupuestaria=pcpd.fk_prb002_num_partida_presupuestaria)
			WHERE  1 $complemento1 $complemento2"
			
        );
		

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetchAll();
    }


     public function getReporteReformulacion($idpresupuesto,$idreformulacion)
    {
	
        if($idpresupuesto!="error"){
            $complemento1=" AND fk_prb004_num_presupuesto='".$idpresupuesto."'";
        }else{
            $complemento1=" ";
        }
		
		 if($idreformulacion!="error"){
            $complemento2="AND pk_num_reformulacion_presupuesto='".$idreformulacion."'";
        }else{
            $complemento2=" ";
        }
		
    
	    $pruebaPost = $this->_db->query(
            " SELECT * FROM pr_d006_reformulacion_presupuesto
			   WHERE 1 $complemento1 $complemento2 AND ind_estado='AP' "
			 );
			 

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetchAll();
    }



     public function getReporteReformulacionDetalle($idreformulacion)
    {

	   
		if($idreformulacion!="error"){
            $complemento1=" AND prp.pk_num_reformulacion_presupuesto='".$idreformulacion."'";
        }else{
            $complemento1=" ";
        }

		 $pruebaPost = $this->_db->query(
          "SELECT * , pbip.ind_tipo_categoria FROM  pr_d006_reformulacion_presupuesto prp
              INNER JOIN pr_b004_presupuesto pbp  ON (pbp.pk_num_presupuesto=prp.fk_prb004_num_presupuesto)
			  INNER JOIN pr_b016_indice_presupuestario pbip ON (pbp.fk_prb016_indice_presupuestario=pbip.pk_num_indice_presupuestario)
              INNER JOIN pr_d007_reformulacion_presupuesto_det prpd ON (prpd.fk_prd006_num_reformulacion_presupuesto=prp.pk_num_reformulacion_presupuesto)
              INNER JOIN pr_b002_partida_presupuestaria ppp ON (ppp.pk_num_partida_presupuestaria=prpd.fk_prb002_num_partida_presupuestaria)
		   WHERE 1 $complemento1 "
        );
		
	
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados

		
        return $pruebaPost->fetchAll();
    }

    

     public function getReporteAjuste($idpresupuesto,$idajuste, $tipoajuste,$organismo)
    {
        if($idpresupuesto!="error"){
            $complemento1=" AND pap.fk_prb004_num_presupuesto='".$idpresupuesto."'";
        }else{
            $complemento1=" ";
        }
		
		 if($idajuste!="error"){
            $complemento2=" AND pap.pk_num_ajuste_presupuestario='".$idajuste."'";
        }else{
            $complemento2=" ";
        }
		
		 if($tipoajuste!="error"){
            $complemento3=" AND pap.fk_a006_num_miscelaneo_det_tipo_ajuste='".$tipoajuste."'";
        }else{
            $complemento3=" ";
        }
		
		if($organismo!="error"){
            $complemento4=" AND pbp.fk_a001_num_organismo='".$organismo."'";
        }else{
            $complemento4=" ";
        }

 
	    $pruebaPost = $this->_db->query(
            " SELECT * FROM pr_d002_ajuste_presupuestario  pap
			   INNER JOIN pr_b004_presupuesto pbp ON(pap.fk_prb004_num_presupuesto=pbp.pk_num_presupuesto)
			   INNER JOIN a006_miscelaneo_detalle amd ON (amd.pk_num_miscelaneo_detalle=pap.fk_a006_num_miscelaneo_det_tipo_ajuste)
			  WHERE pap.ind_estado ='AP'  $complemento1 $complemento2 $complemento3 $complemento4"
			 );
			
			
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetchAll();
    }

     
	public function getReporteAjusteDetalle($idajuste)
    {
       
		if($idajuste!="error"){
            $complemento1=" AND pap.pk_num_ajuste_presupuestario='".$idajuste."'";
        }else{
            $complemento1=" ";
        }


        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            " SELECT pap.*, papd.*, ppp.*,pba.ind_cod_aespecifica, pbp.ind_cod_presupuesto FROM pr_d002_ajuste_presupuestario  pap  
				INNER JOIN pr_d003_ajuste_presupuestario_det papd ON (pap.pk_num_ajuste_presupuestario=papd.fk_prd002_num_ajuste_presupuestario)
			    INNER JOIN pr_c002_presupuesto_det pcpd ON (papd.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
				INNER JOIN pr_b004_presupuesto pbp ON(pcpd.fk_prb004_num_presupuesto=pbp.pk_num_presupuesto)
			    INNER JOIN pr_b002_partida_presupuestaria ppp ON (ppp.pk_num_partida_presupuestaria=pcpd.fk_prb002_num_partida_presupuestaria)
				LEFT JOIN pr_b006_aespecifica pba ON (pba.pk_num_aespecifica=pcpd.fk_pr006_num_aespecifica)
             WHERE 1 $complemento1 "
        );
		
		
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetchAll();
    }

    
   public function getReportePreCierremes($idpresupuesto,$fec_anio,$fec_mes)
    {
        if($fec_anio!="error"){
		    
			//list($anio, $mes) = split('[.]', $periodo); 
            $complemento1=" AND pep.fec_anio='".$fec_anio."'";
        }else{
            $complemento1="";
        }
		
		 if($fec_mes!="error"){
		 $complemento2=" AND pep.fec_mes='".$fec_mes."' ";
        }else{
            $complemento2="";
        }
		
		 if($idpresupuesto!="error"){
            $complemento3=" AND pbp.pk_num_presupuesto='".$idpresupuesto."'";
        }else{
            $complemento3="";
        }


		// REPORTE DE CIERRE DE MES REALIZADO POR ANA HURTADO
		
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            "SELECT 
			            pep.*,
						pbp.ind_cod_presupuesto, pbp.ind_tipo_presupuesto,
						pcpd.num_monto_aprobado, pcpd.fk_pr006_num_aespecifica,
							pnpp.cod_partida,
							pnpp.ind_denominacion FROM pr_d001_ejecucion_presupuestaria_pre pep
			  INNER JOIN pr_c002_presupuesto_det pcpd ON (pep.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
			  INNER JOIN pr_b004_presupuesto pbp ON (pbp.pk_num_presupuesto=pcpd.fk_prb004_num_presupuesto)
			  INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria) 
			  WHERE 1 $complemento1 $complemento2 $complemento3  ORDER BY  pnpp.cod_partida, pk_num_ejecucion_presupuestaria, pbp.pk_num_presupuesto"
        );
		
		
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetchAll();
    }
	
	
	public function getReporteCierremes($idpresupuesto,$fec_anio,$fec_mes)
    {
        if($fec_anio!="error"){
		    
			//list($anio, $mes) = split('[.]', $periodo); 
            $complemento1=" AND pep.fec_anio='".$fec_anio."'";
        }else{
            $complemento1="";
        }
		
		 if($fec_mes!="error"){
		 $complemento2=" AND pep.fec_mes='".$fec_mes."' ";
        }else{
            $complemento2="";
        }
		
		 if($idpresupuesto!="error"){
            $complemento3=" AND pbp.pk_num_presupuesto='".$idpresupuesto."'";
        }else{
            $complemento3="";
        }


		// REPORTE DE CIERRE DE MES REALIZADO POR ANA HURTADO
		
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            "SELECT 
			            pep.*,
						pbp.ind_cod_presupuesto, pbp.ind_tipo_presupuesto,
						pcpd.num_monto_aprobado, pcpd.fk_pr006_num_aespecifica,
							pnpp.cod_partida,
							pnpp.ind_denominacion FROM pr_d001_ejecucion_presupuestaria pep
			  INNER JOIN pr_c002_presupuesto_det pcpd ON (pep.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
			  INNER JOIN pr_b004_presupuesto pbp ON (pbp.pk_num_presupuesto=pcpd.fk_prb004_num_presupuesto)
			  INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria) 
			  WHERE 1 $complemento1 $complemento2 $complemento3  ORDER BY  pnpp.cod_partida, pk_num_ejecucion_presupuestaria, pbp.pk_num_presupuesto"
        );
		
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetchAll();
    }
	
	
	public function getReporteCierremes2($ejecucion)
    {
        if($ejecucion!="error"){
		    
			//list($anio, $mes) = split('[.]', $periodo); 
            $complemento1=" AND pep.pk_num_ejecucion_presupuestaria='".$ejecucion."'";
        }else{
            $complemento1="";
        }
		
		// REPORTE DE CIERRE DE MES REALIZADO POR ANA HURTADO
		
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            "SELECT 
			            pep.*, pbp.pk_num_presupuesto,
						pbp.ind_cod_presupuesto, pbp.ind_tipo_presupuesto,
						pcpd.num_monto_aprobado, pcpd.fk_pr006_num_aespecifica,
							pnpp.cod_partida,
							pnpp.ind_denominacion FROM pr_d001_ejecucion_presupuestaria pep
			  INNER JOIN pr_c002_presupuesto_det pcpd ON (pep.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
			  INNER JOIN pr_b004_presupuesto pbp ON (pbp.pk_num_presupuesto=pcpd.fk_prb004_num_presupuesto)
			  INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria) 
			  WHERE 1 $complemento1"
        );
		

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetchAll();
    }
	
	

    public function getReporteEjecucion($idpresupuesto, $fec_anio, $organismo)
    {
		
		//list($ano, $mes) = split('[.]', $periodo);
		  
		 if($idpresupuesto!="error"){
            $complemento1=" AND pbp.pk_num_presupuesto='".$idpresupuesto."'";
        }else{
            $complemento1=" ";
        }
		
		
		 if($fec_anio!="error"){
            $complemento2=" AND pbp.fec_anio='".$fec_anio."'";
        }else{
            $complemento2=" ";
        }
		
		
		if($organismo!="error"){
            $complemento3=" AND pbp.fk_a001_num_organismo='".$organismo."'";
        }else{
            $complemento3=" ";
        }
		
		  
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("
           
		  SELECT pbip.ind_tipo_categoria, pbp.ind_cod_presupuesto, pbp.ind_tipo_presupuesto, pbp.num_monto_aprobado AS monto_aprobado, pcpd.num_monto_aprobado, pnpp.cod_partida,pnpp.ind_denominacion,pcpd.*, @num:=pcpd.pk_num_presupuesto_det,

			(SELECT
                SUM(pded.num_monto)
              FROM
               pr_c002_presupuesto_det pcpd
            INNER JOIN  pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
            WHERE  ind_tipo_distribucion='CO' AND ind_estado='AC' AND pded.fk_prc002_num_presupuesto_det=@num)
            AS compromiso_dist,
			
			(SELECT
                SUM(pded.num_monto)
              FROM
               pr_c002_presupuesto_det pcpd
            INNER JOIN  pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
            WHERE  ind_tipo_distribucion='CA' AND ind_estado='AC' AND pded.fk_prc002_num_presupuesto_det=@num)
            AS causado_dist,
			
			(SELECT
                SUM(pded.num_monto)
              FROM
               pr_c002_presupuesto_det pcpd
             INNER JOIN  pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
             WHERE  ind_tipo_distribucion='PA' AND ind_estado='AC' AND pded.fk_prc002_num_presupuesto_det=@num)
             AS pagado_dist
			
			
	      FROM pr_c002_presupuesto_det pcpd
			    INNER JOIN pr_b004_presupuesto pbp ON (pbp.pk_num_presupuesto=pcpd.fk_prb004_num_presupuesto)
				INNER JOIN pr_b016_indice_presupuestario pbip ON (pbp.fk_prb016_indice_presupuestario=pbip.pk_num_indice_presupuestario)
			    INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria)
	      WHERE 1 $complemento1 $complemento2 $complemento3 ORDER BY cod_partida ASC"

          );
		  
	
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetchAll();
    }

   
	public function getReporteEjecucionxPartida($idpresupuesto, $fec_anio, $cod_partida,$organismo)
    {
		
		//list($ano, $mes) = split('[.]', $periodo);
		  
		 if($idpresupuesto!="error"){
            $complemento1=" AND pbp.pk_num_presupuesto='".$idpresupuesto."'";
        }else{
            $complemento1=" ";
        }
		
		
		 if($fec_anio!="error"){
            $complemento2=" AND pbp.fec_anio='".$fec_anio."'";
        }else{
            $complemento2=" ";
        }
		
		
		 if($cod_partida!="error"){
            $complemento3=" AND pnpp.cod_partida like '%".$cod_partida."%'";
        }else{
            $complemento3=" ";
        }
		
		if($organismo!="error"){
            $complemento4=" AND pbp.fk_a001_num_organismo='".$organismo."'";
        }else{
            $complemento4=" ";
        }
        
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("
           
		  SELECT pbp.ind_cod_presupuesto,pcpd.num_monto_aprobado, pnpp.cod_partida,pnpp.ind_denominacion,pcpd.*, @num:=pcpd.pk_num_presupuesto_det,

			(SELECT
                SUM(pded.num_monto)
              FROM
               pr_c002_presupuesto_det pcpd
            INNER JOIN  pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
            WHERE  ind_tipo_distribucion='CO' AND ind_estado='AC' AND pded.fk_prc002_num_presupuesto_det=@num)
            AS compromiso_dist,
			
			(SELECT
                SUM(pded.num_monto)
              FROM
               pr_c002_presupuesto_det pcpd
            INNER JOIN  pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
            WHERE  ind_tipo_distribucion='CA' AND ind_estado='AC' AND pded.fk_prc002_num_presupuesto_det=@num)
            AS causado_dist,
			
			(SELECT
                SUM(pded.num_monto)
              FROM
               pr_c002_presupuesto_det pcpd
             INNER JOIN  pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
             WHERE  ind_tipo_distribucion='PA' AND ind_estado='AC' AND pded.fk_prc002_num_presupuesto_det=@num)
             AS pagado_dist
			
	      FROM pr_c002_presupuesto_det pcpd
			    INNER JOIN pr_b004_presupuesto pbp ON (pbp.pk_num_presupuesto=pcpd.fk_prb004_num_presupuesto)
			    INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria)
	      WHERE 1 $complemento1 $complemento2  $complemento3 $complemento4 ORDER BY cod_partida ASC"

          );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetchAll();
    }


     public function getReporteCompromiso($idpresupuesto, $fec_anio, $organismo)
    {
		
		//list($ano, $mes) = split('[.]', $periodo);
		  
		 if($idpresupuesto!="error"){
            $complemento1=" AND pbp.pk_num_presupuesto='".$idpresupuesto."'";
        }else{
            $complemento1=" ";
        }
		
		
		 if($fec_anio!="error"){
            $complemento2=" AND pbp.fec_anio='".$fec_anio."'";
        }else{
            $complemento2=" ";
        }
		
		
		if($organismo!="error"){
            $complemento3=" AND pbp.fk_a001_num_organismo='".$organismo."'";
        }else{
            $complemento3=" ";
        }
		
		$pruebaPost = $this->_db->query("
           SELECT pbp.ind_cod_presupuesto, pbp.ind_tipo_presupuesto, pcpd.num_monto_aprobado, pnpp.cod_partida,pnpp.ind_denominacion,pcpd.*, @num:=pcpd.pk_num_presupuesto_det,

			(SELECT
              SUM(pded.num_monto)
              FROM
               pr_c002_presupuesto_det pcpd
            INNER JOIN  pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
            WHERE  ind_tipo_distribucion='CO' AND ind_estado='AC' AND pded.fk_prc002_num_presupuesto_det=@num)
            AS compromiso_dist

	      FROM pr_c002_presupuesto_det pcpd
			    INNER JOIN pr_b004_presupuesto pbp ON (pbp.pk_num_presupuesto=pcpd.fk_prb004_num_presupuesto)
			    INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria)
	 
			WHERE 1  $complemento1 $complemento2 $complemento3 ORDER by cod_partida ASC"
			
        );
	

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetchAll();
    }

  

     public function getReporteincremento($idpresupuesto, $fec_anio)
    {
		
		  
		if($idpresupuesto!="error"){
            $complemento1=" AND pbp.pk_num_presupuesto='".$idpresupuesto."'";
        }else{
            $complemento1=" ";
        }
		
		
		if($fec_anio!="error"){
            $complemento2=" AND pbp.fec_anio='".$fec_anio."'";
        }else{
            $complemento2=" ";
        }
		

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("
            SELECT pbp.ind_cod_presupuesto,pcpd.num_monto_aprobado, pnpp.cod_partida,pnpp.ind_denominacion,pcpd.* FROM pr_c002_presupuesto_det pcpd
			  INNER JOIN pr_b004_presupuesto pbp ON (pbp.pk_num_presupuesto=pcpd.fk_prb004_num_presupuesto)
			  INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria)
			WHERE 1  $complemento1 $complemento2 ORDER by cod_partida ASC"
			
        );
		
	
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetchAll();
    }


    public function getReporteCausado($idpresupuesto, $fec_anio,$organismo)
    {
		
		//list($ano, $mes) = split('[.]', $periodo);
		  
		 if($idpresupuesto!="error"){
            $complemento1=" AND pbp.pk_num_presupuesto='".$idpresupuesto."'";
        }else{
            $complemento1=" ";
        }
		
		
		 if($fec_anio!="error"){
            $complemento2=" AND pbp.fec_anio='".$fec_anio."'";
        }else{
            $complemento2=" ";
        }
		
		if($organismo!="error"){
            $complemento3=" AND pbp.fk_a001_num_organismo='".$organismo."'";
        }else{
            $complemento3=" ";
        }
		

		$pruebaPost = $this->_db->query("
           SELECT pbp.ind_cod_presupuesto,pbp.ind_tipo_presupuesto, pcpd.num_monto_aprobado, pnpp.cod_partida,pnpp.ind_denominacion,pcpd.*, @num:=pcpd.pk_num_presupuesto_det,

			(SELECT
              SUM(pded.num_monto)
              FROM
               pr_c002_presupuesto_det pcpd
            INNER JOIN  pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
            WHERE  ind_tipo_distribucion='CA' AND ind_estado='AC' AND pded.fk_prc002_num_presupuesto_det=@num)
            AS causado_dist

	

	      FROM pr_c002_presupuesto_det pcpd
			    INNER JOIN pr_b004_presupuesto pbp ON (pbp.pk_num_presupuesto=pcpd.fk_prb004_num_presupuesto)
			    INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria)
	 
			WHERE 1  $complemento1 $complemento2 $complemento3 ORDER by cod_partida ASC"
			
        );
		
		

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetchAll();
    }


     public function getReportePagado($idpresupuesto, $fec_anio)
    {
		
		  
		 if($idpresupuesto!="error"){
            $complemento1=" AND pbp.pk_num_presupuesto='".$idpresupuesto."'";
        }else{
            $complemento1=" ";
        }
		
		
		 if($fec_anio!="error"){
            $complemento2=" AND pbp.fec_anio='".$fec_anio."'";
        }else{
            $complemento2=" ";
        }

		
		
		
		$pruebaPost = $this->_db->query("
           SELECT pbp.ind_cod_presupuesto,pbp.ind_tipo_presupuesto,pcpd.num_monto_aprobado, pnpp.cod_partida,pnpp.ind_denominacion,pcpd.*, @num:=pcpd.pk_num_presupuesto_det,

			(SELECT
              SUM(pded.num_monto)
              FROM
               pr_c002_presupuesto_det pcpd
            INNER JOIN  pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
            WHERE  ind_tipo_distribucion='PA' AND ind_estado='AC' AND pded.fk_prc002_num_presupuesto_det=@num)
            AS pagado_dist


	      FROM pr_c002_presupuesto_det pcpd
			    INNER JOIN pr_b004_presupuesto pbp ON (pbp.pk_num_presupuesto=pcpd.fk_prb004_num_presupuesto)
			    INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria)
	 
			WHERE 1  $complemento1 $complemento2  ORDER by cod_partida ASC"
			
        );
		

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetchAll();
    }



     public function getReportePartidasTitulo()
    {
		

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("
            SELECT
              *
            FROM
               pr_b002_partida_presupuestaria 
            WHERE ind_tipo='T'
            ORDER by cod_partida ASC"
			
        );
		

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetchAll();
    }



    public function getReporteMontosTituloAntepresupuesto($antepresupuesto,$partida,$aespecifica=false)
    {
		
		if($antepresupuesto!="error"){
            $complemento1=" AND pcpd.fk_prb003_num_antepresupuesto='".$antepresupuesto."'";
        }else{
            $complemento1=" ";
        }
		
		if($partida!="error"){
		$partida=$partida.'%';
            $complemento2=" AND pnpp.cod_partida like '".$partida."'";
        }else{
            $complemento2=" ";
        }
		
		if($aespecifica!="error"){
            $complemento3=" AND fk_pr_b006_aespecifica='".$aespecifica."'";
        }else{
            $complemento3=" ";
        }

      	
		 #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("
           
		SELECT
              SUM(num_monto_presupuestado) AS suma_presupuestado
            FROM
               pr_c001_antepresupuesto_det pcpd
			   INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria)
			   WHERE 1 $complemento1 $complemento2 $complemento3
			"	   
        );
		
		
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_UNIQUE);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
		$registro=$pruebaPost->fetch();
		
		
        return $registro;
    }

	
	
	public function getReporteMontosTituloPresupuesto($presupuesto,$partida,$aespecifica=false)
    {
		
		if($presupuesto!="error"){
            $complemento1=" AND pcpd.fk_prb004_num_presupuesto='".$presupuesto."'";
        }else{
            $complemento1=" ";
        }
		
		if($partida!="error"){
		$partida=$partida.'%';
            $complemento2=" AND pnpp.cod_partida like '".$partida."'";
        }else{
            $complemento2=" ";
        }
		
	if($aespecifica and $aespecifica!="error"){
            $complemento3=" AND pcpd.fk_pr006_num_aespecifica='".$aespecifica."'";
        }else{
            $complemento3=" ";
        }

      
      	
		 #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("
           
		SELECT
              SUM(num_monto_aprobado) AS suma_presupuestado
            FROM
               pr_c002_presupuesto_det pcpd
			   INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria)
			   WHERE 1 $complemento1 $complemento2 $complemento3
			"	   
        );
		
		
		
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_UNIQUE);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
		$registro=$pruebaPost->fetch();
		
		
		
        return $registro;
    }

	
	
	
	 public function getReporteMontosTituloPresupuestoAntepresupuesto($presupuesto,$partida,$aespecifica=false)
    {
        if($presupuesto!="error"){
            $complemento1=" AND pbp.pk_num_presupuesto='".$presupuesto."'";
        }else{
            $complemento1=" ";
        }
		
		
	
	/*	 if($presupuesto!="error"){
            $complemento1=" AND pcpd.fk_prb004_num_presupuesto='".$presupuesto."'";
        }else{
            $complemento1=" ";
        }*/
		
		if($partida!="error"){
		$partida=$partida.'%';
            $complemento2=" AND ppp.cod_partida like '".$partida."'";
        }else{
            $complemento2=" ";
        }
		

$pruebaPost = $this->_db->query("SELECT SUM(presupuestado) as suma FROM(
 SELECT DISTINCT  pcpd.num_monto_presupuestado as presupuestado
 FROM pr_b003_antepresupuesto pbpa
			  INNER JOIN pr_c001_antepresupuesto_det pcpd ON (pbpa.pk_num_antepresupuesto=pcpd.fk_prb003_num_antepresupuesto)
			  INNER JOIN pr_b004_presupuesto pbp ON(pbp.fk_prb003_num_antepresupuesto=pbpa.pk_num_antepresupuesto)
			  INNER JOIN pr_c002_presupuesto_det pcp ON (pbp.pk_num_presupuesto=pcp.fk_prb004_num_presupuesto)

			  INNER JOIN pr_b002_partida_presupuestaria ppp ON (ppp.pk_num_partida_presupuestaria=pcpd.fk_prb002_num_partida_presupuestaria)
			WHERE 1 $complemento1 $complemento2
UNION
(
 SELECT  0 as presupuestado FROM pr_b004_presupuesto pbp

  INNER JOIN pr_b003_antepresupuesto pbpa ON (pbpa.pk_num_antepresupuesto=pbp.fk_prb003_num_antepresupuesto)

  INNER JOIN pr_c002_presupuesto_det pcp ON (pbp.pk_num_presupuesto=pcp.fk_prb004_num_presupuesto)
  INNER JOIN pr_b002_partida_presupuestaria ppp ON (ppp.pk_num_partida_presupuestaria=pcp.fk_prb002_num_partida_presupuestaria)
  WHERE 1 $complemento1 $complemento2 AND ppp.cod_partida NOT IN(SELECT DISTINCT ppp.cod_partida
 FROM pr_b003_antepresupuesto pbpa
			  INNER JOIN pr_c001_antepresupuesto_det pcpd ON (pbpa.pk_num_antepresupuesto=pcpd.fk_prb003_num_antepresupuesto)
			  INNER JOIN pr_b004_presupuesto pbp ON(pbp.fk_prb003_num_antepresupuesto=pbpa.pk_num_antepresupuesto)
			  INNER JOIN pr_c002_presupuesto_det pcp ON (pbp.pk_num_presupuesto=pcp.fk_prb004_num_presupuesto)

			  INNER JOIN pr_b002_partida_presupuestaria ppp ON (ppp.pk_num_partida_presupuestaria=pcpd.fk_prb002_num_partida_presupuestaria)
			WHERE 1 $complemento1 $complemento2)
)) as suma_presupuestado


UNION
(
SELECT SUM(aprobado) as suma FROM(
 SELECT DISTINCT  pcpd.num_monto_aprobado as aprobado
 FROM pr_b003_antepresupuesto pbpa
			  INNER JOIN pr_c001_antepresupuesto_det pcpd ON (pbpa.pk_num_antepresupuesto=pcpd.fk_prb003_num_antepresupuesto)
			  INNER JOIN pr_b004_presupuesto pbp ON(pbp.fk_prb003_num_antepresupuesto=pbpa.pk_num_antepresupuesto)
			  INNER JOIN pr_c002_presupuesto_det pcp ON (pbp.pk_num_presupuesto=pcp.fk_prb004_num_presupuesto)

			  INNER JOIN pr_b002_partida_presupuestaria ppp ON (ppp.pk_num_partida_presupuestaria=pcpd.fk_prb002_num_partida_presupuestaria)

			WHERE 1 $complemento1 $complemento2
UNION
(
 SELECT  pcp.num_monto_aprobado as aprobado FROM pr_b004_presupuesto pbp

  INNER JOIN pr_b003_antepresupuesto pbpa ON (pbpa.pk_num_antepresupuesto=pbp.fk_prb003_num_antepresupuesto)

  INNER JOIN pr_c002_presupuesto_det pcp ON (pbp.pk_num_presupuesto=pcp.fk_prb004_num_presupuesto)
  INNER JOIN pr_b002_partida_presupuestaria ppp ON (ppp.pk_num_partida_presupuestaria=pcp.fk_prb002_num_partida_presupuestaria)
  WHERE 1 $complemento1 $complemento2 AND ppp.cod_partida NOT IN(SELECT DISTINCT ppp.cod_partida
 FROM pr_b003_antepresupuesto pbpa
			  INNER JOIN pr_c001_antepresupuesto_det pcpd ON (pbpa.pk_num_antepresupuesto=pcpd.fk_prb003_num_antepresupuesto)
			  INNER JOIN pr_b004_presupuesto pbp ON(pbp.fk_prb003_num_antepresupuesto=pbpa.pk_num_antepresupuesto)
			  INNER JOIN pr_c002_presupuesto_det pcp ON (pbp.pk_num_presupuesto=pcp.fk_prb004_num_presupuesto)

			  INNER JOIN pr_b002_partida_presupuestaria ppp ON (ppp.pk_num_partida_presupuestaria=pcpd.fk_prb002_num_partida_presupuestaria)
			WHERE 1 $complemento1 $complemento2)
) )as suma_aprobado )");


        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
            return $pruebaPost->fetchAll();
    }
	
	
	//---------------------------------------------
    public function getReporteMontosTituloEjecucionEspecifica1($presupuesto,$partida,$aespecifica=false, $desde, $hasta)
    {
				  
		/* if($idpresupuesto!="error"){
            $complemento1=" AND pbp.pk_num_presupuesto='".$idpresupuesto."'";
        }else{
            $complemento1=" ";
        }*/
		
		if($presupuesto!="error"){
            $complemento1=" AND pbp.fk_prb004_num_presupuesto='".$presupuesto."'";
        }else{
            $complemento1=" ";
        }
		
		if($partida!="error"){
		$partida=$partida.'%';
            $complemento2=" AND pnpp.cod_partida like '".$partida."'";
        }else{
            $complemento2=" ";
        }
		
	    if($aespecifica and $aespecifica!="error"){
            $complemento3=" AND pcpd.fk_pr006_num_aespecifica='".$aespecifica."'";
        }else{
            $complemento3=" ";
        }
        if($desde!="error" and $hasta!='error'){
            $complemento4=" AND cp_d009_orden_pago.fec_orden_pago >= '".$desde."' AND cp_d009_orden_pago.fec_orden_pago <= '".$hasta."'";
        }else{
            $complemento4=" ";
        }

		  		
		$pruebaPost = $this->_db->query("
		
		
            SELECT DISTINCT pbp.ind_cod_presupuesto,pcpd.num_monto_aprobado, pnpp.cod_partida,pnpp.ind_denominacion,pcpd.*, pbp.ind_tipo_presupuesto FROM pr_c002_presupuesto_det pcpd
			  INNER JOIN pr_b004_presupuesto pbp ON (pbp.pk_num_presupuesto=pcpd.fk_prb004_num_presupuesto)
			  INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria)
			  INNER JOIN  pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det AND pded.ind_tipo_distribucion='CO' AND pded.ind_estado='AC')
              INNER JOIN  cp_d001_obligacion cdp ON (pded.fk_cpd001_num_obligacion=cdp.pk_num_obligacion)
              LEFT JOIN cp_d009_orden_pago ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cdp.pk_num_obligacion
			WHERE 1  $complemento1 $complemento2 $complemento3 $complemento4 ORDER by cod_partida ASC"
			
        );

		//var_dump($pruebaPost);
		//exit;
		

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetchAll();
    }
    //--------------------------------------------------
	
	
	public function getReporteMontosTituloEjecucionEspecifica($presupuesto,$partida,$aespecifica=false, $estado=false ,$desde, $hasta)
    {
		
		//list($ano, $mes) = split('[.]', $periodo);
		  
        if($presupuesto!="error"){
            $complemento1=" AND pbp.pk_num_presupuesto='".$presupuesto."'";
        }else{
            $complemento1=" ";
        }

        if($partida!="error"){
		    $partida=$partida.'%';
            $complemento2=" AND pnpp.cod_partida like '".$partida."'";
        }else{
            $complemento2=" ";
        }
		
		
		  if($aespecifica and $aespecifica!="error"){
            $complemento3=" AND pcpd.fk_pr006_num_aespecifica='".$aespecifica."'";
        }else{
            $complemento3=" ";
        }

        if($desde!="error" and $hasta!='error'){
            $complemento4=" AND cp_d009_orden_pago.fec_orden_pago >= '".$desde."' AND cp_d009_orden_pago.fec_orden_pago <= '".$hasta."'";
        }else{
            $complemento4=" ";
        }
	   		
		$pruebaPost = $this->_db->query("
		
		    
            SELECT SUM(pcpd.num_monto_aprobado) AS suma_aprobado , SUM(pded.num_monto) AS suma_monto_distribucion , SUM(cdp.num_monto_obligacion) AS suma_monto_obligacion
			FROM pr_c002_presupuesto_det pcpd
			  INNER JOIN pr_b004_presupuesto pbp ON (pbp.pk_num_presupuesto=pcpd.fk_prb004_num_presupuesto)
			  INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria)
			  INNER JOIN  pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det AND pded.ind_tipo_distribucion='".$estado."' AND pded.ind_estado='AC')
              INNER JOIN  cp_d001_obligacion cdp ON (pded.fk_cpd001_num_obligacion=cdp.pk_num_obligacion)
              LEFT JOIN cp_d009_orden_pago ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cdp.pk_num_obligacion
			WHERE 1  $complemento1 $complemento2 $complemento3 $complemento4 ORDER by cod_partida ASC"
			
        );

		//var_dump($pruebaPost);
		//exit;

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetch();
    
    //--------------------------------------------------
	
}
	
	
      public function getReporteMontosTitulo($presupuesto,$partida, $aespecifica=false)
    {
		
		
		if($presupuesto!="error"){
            $complemento1=" AND pcpd.fk_prb004_num_presupuesto='".$presupuesto."'";
        }else{
            $complemento1=" ";
        }
		
		if($partida!="error"){
		$partida=$partida.'%';
            $complemento2=" AND pnpp.cod_partida like '".$partida."'";
        }else{
            $complemento2=" ";
        }
		
		if($aespecifica and $aespecifica!="error"){
            $complemento3=" AND pcpd.fk_pr006_num_aespecifica='".$aespecifica."'";
        }else{
            $complemento3=" ";
        }

      	
		 #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("
           
		SELECT
              SUM(num_monto_aprobado) AS suma_aprobado, SUM(num_monto_ajustado) AS suma_ajustado, SUM(num_monto_compromiso) AS suma_compromiso,
			  SUM(num_monto_causado) AS suma_causado, SUM(num_monto_pagado) AS suma_pagado, SUM(num_monto_incremento) AS suma_incremento,
			  SUM(num_monto_disminucion) AS suma_disminucion,SUM(num_monto_credito) AS suma_credito,


         (SELECT
              SUM(num_monto)
            FROM
               pr_c002_presupuesto_det pcpd
			   INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria)
			   INNER JOIN  pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
			   WHERE  1 $complemento1 $complemento2 $complemento3 AND ind_tipo_distribucion='CO' AND ind_estado='AC') AS suma_compromiso_dist,

         (SELECT
              SUM(num_monto)
            FROM
               pr_c002_presupuesto_det pcpd
			   INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria)
			   INNER JOIN  pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
			   WHERE  1  $complemento1 $complemento2 $complemento3 AND ind_tipo_distribucion='CA' AND ind_estado='AC') AS suma_causado_dist,


         (SELECT
              SUM(num_monto)
            FROM
               pr_c002_presupuesto_det pcpd
			   INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria)
			   INNER JOIN  pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
			   WHERE  1  $complemento1 $complemento2 $complemento3 AND ind_tipo_distribucion='PA' AND ind_estado='AC') AS suma_pagado_dist


            FROM
               pr_c002_presupuesto_det pcpd
			   INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria)
			   WHERE 1 $complemento1 $complemento2 $complemento3
			"
		   
        );


		//var_dump($pruebaPost);
		//exit;
		
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_UNIQUE);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
		$registro=$pruebaPost->fetch();
		
		
        return $registro;
    }

    public function getReporteMontosTituloCierre($presupuesto,$partida,$fec_anio,$fec_mes,$aespecifica)
    {
		
		
		if($presupuesto!="error"){
            $complemento1=" AND pcpd.fk_prb004_num_presupuesto='".$presupuesto."'";
        }else{
            $complemento1=" ";
        }
		
		if($partida!="error"){
		$partida=$partida.'%';
            $complemento2=" AND pnpp.cod_partida like '".$partida."'";
        }else{
            $complemento2=" ";
        }

         if($fec_anio!="error"){
		    
			//list($anio, $mes) = split('[.]', $periodo); 
            $complemento3=" AND pep.fec_anio='".$fec_anio."'";
        }else{
            $complemento3="";
        }
		
		 if($fec_mes!="error"){
		 $complemento4=" AND pep.fec_mes='".$fec_mes."' ";
        }else{
            $complemento4="";
        }
		
		if($aespecifica!="error"){
            $complemento5=" AND pcpd.fk_pr006_num_aespecifica='".$aespecifica."'";
        }else{
            $complemento5=" ";
        }
		
		
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("
            SELECT 
              SUM(num_disponibilidad_presupuestaria_inicial) AS suma_presupuesto_inicial, SUM(num_disponibilidad_financiera_inicial) AS suma_financiero_inicial,       
			  SUM(num_incremento) AS suma_incremento,
			   SUM(num_disminucion) AS suma_disminucion, SUM(num_monto_ajustado) AS suma_ajustado,SUM(num_compromiso) AS suma_compromiso,
			  SUM(num_causado) AS suma_causado, SUM(num_pagado) AS suma_pagado
            FROM
			   pr_d001_ejecucion_presupuestaria pep
               INNER JOIN pr_c002_presupuesto_det pcpd ON (pcpd.pk_num_presupuesto_det=pep.fk_prc002_num_presupuesto_det)
			   INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria)
			   WHERE 1 $complemento1 $complemento2 $complemento3 $complemento4 $complemento5
			   "
		   
        );
		
		//var_dump($pruebaPost);
		//exit;
		

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_UNIQUE);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
		$registro=$pruebaPost->fetch();
		
		
        return $registro;
    }

    
	public function getReporteMontosTituloPreCierre($presupuesto,$partida,$fec_anio,$fec_mes,$aespecifica)
    {
		
		
		if($presupuesto!="error"){
            $complemento1=" AND pcpd.fk_prb004_num_presupuesto='".$presupuesto."'";
        }else{
            $complemento1=" ";
        }
		
		if($partida!="error"){
		$partida=$partida.'%';
            $complemento2=" AND pnpp.cod_partida like '".$partida."'";
        }else{
            $complemento2=" ";
        }

         if($fec_anio!="error"){
		    
			//list($anio, $mes) = split('[.]', $periodo); 
            $complemento3=" AND pep.fec_anio='".$fec_anio."'";
        }else{
            $complemento3="";
        }
		
		 if($fec_mes!="error"){
		 $complemento4=" AND pep.fec_mes='".$fec_mes."' ";
        }else{
            $complemento4="";
        }
		
		if($aespecifica!="error"){
            $complemento5=" AND pcpd.fk_pr006_num_aespecifica='".$aespecifica."'";
        }else{
            $complemento5=" ";
        }
		
		
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("
            SELECT 
              SUM(num_disponibilidad_presupuestaria_inicial) AS suma_presupuesto_inicial, SUM(num_disponibilidad_financiera_inicial) AS suma_financiero_inicial,       
			  SUM(num_incremento) AS suma_incremento,
			   SUM(num_disminucion) AS suma_disminucion, SUM(num_monto_ajustado) AS suma_ajustado,SUM(num_compromiso) AS suma_compromiso,
			  SUM(num_causado) AS suma_causado, SUM(num_pagado) AS suma_pagado
            FROM
			   pr_d001_ejecucion_presupuestaria_pre pep
               INNER JOIN pr_c002_presupuesto_det pcpd ON (pcpd.pk_num_presupuesto_det=pep.fk_prc002_num_presupuesto_det)
			   INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria)
			   WHERE 1 $complemento1 $complemento2 $complemento3 $complemento4 $complemento5
			   "
		   
        );
		
		//var_dump($pruebaPost);
		//exit;
		

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_UNIQUE);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
		$registro=$pruebaPost->fetch();
		
		
        return $registro;
    }
	
	public function getReporteMontosTituloProyeccion($presupuesto,$partida,$fec_mes)
    {
		
		
		if($presupuesto!="error"){
            $complemento1=" AND pcpd.fk_prb004_num_presupuesto='".$presupuesto."'";
        }else{
            $complemento1=" ";
        }
		
		if($partida!="error"){
		$partida=$partida.'%';
            $complemento2=" AND pnpp.cod_partida like '".$partida."'";
        }else{
            $complemento2=" ";
        }

         
		 if($fec_mes!="error"){
		 $complemento3=" AND pep.fec_mes='".$fec_mes."' ";
        }else{
            $complemento3="";
        }
		
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("
            SELECT 
              SUM(num_disponibilidad_presupuestaria_inicial) AS suma_presupuesto_inicial, SUM(num_disponibilidad_financiera_inicial) AS suma_financiero_inicial,       
			  SUM(num_incremento) AS suma_incremento,
			   SUM(num_disminucion) AS suma_disminucion, SUM(num_monto_ajustado) AS suma_ajustado,SUM(num_compromiso) AS suma_compromiso,
			  SUM(num_causado) AS suma_causado, SUM(num_pagado) AS suma_pagado
            FROM
			   pr_d001_ejecucion_presupuestaria pep
               INNER JOIN pr_c002_presupuesto_det pcpd ON (pcpd.pk_num_presupuesto_det=pep.fk_prc002_num_presupuesto_det)
			   INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria)
			   WHERE 1 $complemento1 $complemento2  $complemento3
			   "
		   
        );
		

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_UNIQUE);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
		$registro=$pruebaPost->fetch();
		
       // return $registro['suma_aprobado'];
		
        return $registro;
    }

    public function getReporteProyeccion($idpresupuesto,$fec_anio,$fec_mes, $cod_partida,$opt)
    {
	
	
		 if($idpresupuesto!="error"){
            $complemento1=" AND pbp.pk_num_presupuesto='".$idpresupuesto."'";
        }else{
            $complemento1="";
        }



        if($fec_anio!="error"){
		 $complemento2=" AND pep.fec_anio='".$fec_anio."'";
        }else{
            $complemento2="";
        }

		
		 if($cod_partida!="error"){
            $complemento3=" AND pnpp.cod_partida like '%".$cod_partida."%'";
        }else{
            $complemento3=" ";
        }
		
		
		//--------------------------------------------------------------
		
		    $mes = $this->_db->query(
            "SELECT MAX(fec_mes) as fec_mes
            FROM pr_d001_ejecucion_presupuestaria pep
            INNER JOIN pr_c002_presupuesto_det pcpd ON (pep.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
             WHERE 1 AND pcpd.fk_prb004_num_presupuesto='".$idpresupuesto."' AND pep.fec_anio='".$fec_anio."' LIMIT 1"
        );
	
		
		
        $mes->setFetchMode(PDO::FETCH_UNIQUE);
		$registro=$mes->fetch();
        $mes1=$registro['fec_mes'];
		
		
		//var_dump($mes1);
		
		
		//---------------------------------------------------------------
		
		
		
		if ($opt==1)
		  $complemento4= "AND pep.fec_mes='".$mes1."'";
		else
		{ 
		  $complemento4= "AND pep.fec_mes='".$fec_mes."'";
		  //var_dump($complemento4);
		 // exit;
        }
		
        #ejecuto la consulta a la base de datos
		
		 
	
		$pruebaPost = $this->_db->query(
			
            "	
			SELECT 
			 pep.*,     pep.fec_mes AS fec_mes_ant,
						pbp.ind_cod_presupuesto, 
						pbp.ind_tipo_presupuesto,
						pcpd.num_monto_aprobado, 
						pnpp.cod_partida,
						pnpp.ind_denominacion,
						pcpd.fk_pr006_num_aespecifica,
						pcpd.pk_num_presupuesto_det
						FROM pr_d001_ejecucion_presupuestaria pep
			  INNER JOIN pr_c002_presupuesto_det pcpd ON (pep.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
			  INNER JOIN pr_b004_presupuesto pbp ON (pbp.pk_num_presupuesto=pcpd.fk_prb004_num_presupuesto)
			  INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria) 
			  WHERE 1 $complemento1 $complemento2 $complemento3 $complemento4 ORDER BY  pnpp.cod_partida, pk_num_ejecucion_presupuestaria, pbp.pk_num_presupuesto"
        );
		
		
		
		#devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetchAll();
    }
	
	//---------------------------------------------
	 public function getReporteEjecucionEspecifica($idpresupuesto, $fec_anio, $estado)
    {
				  
		 if($idpresupuesto!="error"){
            $complemento1=" AND pbp.pk_num_presupuesto='".$idpresupuesto."'";
        }else{
            $complemento1=" ";
        }
		
		
		 if($fec_anio!="error"){
            $complemento2=" AND pbp.fec_anio='".$fec_anio."'";
        }else{
            $complemento2=" ";
        }



	   		
		$pruebaPost = $this->_db->query("
            SELECT DISTINCT 
             pnpp.ind_partida,
             pnpp.ind_generica,
             pnpp.ind_especifica,
             pnpp.ind_subespecifica,
             pbp.ind_cod_presupuesto,pcpd.num_monto_aprobado, pnpp.cod_partida,pnpp.ind_denominacion,pcpd.*, pbp.ind_tipo_presupuesto
 FROM pr_c002_presupuesto_det pcpd
			  INNER JOIN pr_b004_presupuesto pbp ON (pbp.pk_num_presupuesto=pcpd.fk_prb004_num_presupuesto)
			  INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria)
			  INNER JOIN  pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det AND pded.ind_tipo_distribucion='".$estado."' AND pded.ind_estado='AC')
              INNER JOIN  cp_d001_obligacion cdp ON (pded.fk_cpd001_num_obligacion=cdp.pk_num_obligacion)
			WHERE 1  $complemento1 $complemento2   ORDER by cod_partida ASC"
			
        );
		
		//var_dump($pruebaPost);
		//exit;
		

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetchAll();
    }
    //--------------------------------------------------
	

	//---------------------------------------------
	 public function getReporteEjecucionEspecificaDetalle($idpresupuesto, $fec_anio, $partida, $estado=false, $desde, $hasta)
    {
		
		//list($ano, $mes) = split('[.]', $periodo);
		  
		 if($idpresupuesto!="error"){
            $complemento1=" AND pbp.pk_num_presupuesto='".$idpresupuesto."'";
        }else{
            $complemento1=" ";
        }
		
		
		 if($fec_anio!="error"){
            $complemento2=" AND pbp.fec_anio='".$fec_anio."'";
        }else{
            $complemento2=" ";
        }
		
		 if($partida!="error"){
            $complemento3=" AND pnpp.cod_partida='".$partida."'";
        }else{
            $complemento3=" ";
        }
        if($desde!="error" and $hasta!='error'){
            $complemento4=" AND cp_d009_orden_pago.fec_orden_pago >= '".$desde."' AND cp_d009_orden_pago.fec_orden_pago <= '".$hasta."'";
        }else{
            $complemento4=" ";
        }
	   		
		$pruebaPost = $this->_db->query("
            SELECT pbp.ind_cod_presupuesto,pcpd.num_monto_aprobado,
             pnpp.ind_partida,
             pnpp.ind_generica,
             pnpp.ind_especifica,
             pnpp.ind_subespecifica,
             pnpp.cod_partida,
             pnpp.ind_denominacion,
             pcpd.*, pded.num_monto, 
             cdp.ind_comentarios, 
             cdp.num_monto_obligacion, 
             cp_d009_orden_pago.ind_num_orden, 
             lg_b019_orden.ind_orden,
             lg_b019_orden.ind_descripcion,
             lg_b019_orden.fec_revision,
             lg_b019_orden.ind_tipo_orden,
             cp_d009_orden_pago.fec_orden_pago
			FROM pr_c002_presupuesto_det pcpd
			  INNER JOIN pr_b004_presupuesto pbp ON (pbp.pk_num_presupuesto=pcpd.fk_prb004_num_presupuesto)
			  INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria)
			  INNER JOIN pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det AND pded.ind_tipo_distribucion='".$estado."' AND pded.ind_estado='AC')
              LEFT JOIN cp_d001_obligacion cdp ON (pded.fk_cpd001_num_obligacion=cdp.pk_num_obligacion)
              LEFT JOIN lg_b019_orden ON pded.fk_lgb019_num_orden = lg_b019_orden.pk_num_orden
              LEFT JOIN cp_d009_orden_pago ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cdp.pk_num_obligacion

			WHERE 1  $complemento1 $complemento2 $complemento3 $complemento4  ORDER by cod_partida ASC"
			
        );
		
		//var_dump($pruebaPost);
		//exit;
		

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
		
        return $pruebaPost->fetchAll();
    }
    //--------------------------------------------------
	
}
