-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 21, 2017 at 04:11 PM
-- Server version: 5.7.17-0ubuntu0.16.04.1
-- PHP Version: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `SIACE`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `w_rh_operacionesAsignacionUtiles` (IN `txt_estatus` VARCHAR(2), IN `id_asignacion_utiles` INT, IN `id_empleado` INT, IN `flag_estatus` INT, IN `fec_operacion` DATE, IN `txt_observaciones` MEDIUMTEXT, IN `usuario` INT)  BEGIN

  DECLARE N INT;

	IF NOT EXISTS ( SELECT t.fk_rhc093_utiles_asignacion FROM rh_c096_operaciones_asignaciones_utiles AS t WHERE t.fk_rhc093_utiles_asignacion = id_asignacion_utiles) THEN

		INSERT INTO rh_c096_operaciones_asignaciones_utiles(ind_estado, fk_rhc093_utiles_asignacion , fk_rhb001_num_empleado, num_flag_estatus, fec_operacion,txt_observaciones,fk_a018_num_seguridad_usuario)
		VALUES ( txt_estatus,id_asignacion_utiles,id_empleado,flag_estatus,fec_operacion,txt_observaciones,usuario);
  ELSE

		SET N = (SELECT u_rh_ultimoRegistroAsignacionUtiles(id_asignacion_utiles));

		update rh_c096_operaciones_asignaciones_utiles SET num_flag_estatus=0 WHERE pk_num_operaciones_asignaciones_utiles=N;

		INSERT INTO rh_c096_operaciones_asignaciones_utiles(ind_estado, fk_rhc093_utiles_asignacion, fk_rhb001_num_empleado, num_flag_estatus, fec_operacion,txt_observaciones,fk_a018_num_seguridad_usuario)
		VALUES ( txt_estatus,id_asignacion_utiles,id_empleado,flag_estatus,fec_operacion,txt_observaciones,usuario);

  END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `w_rh_operacionesBeneficioUtiles` (IN `txt_estatus` VARCHAR(2), IN `id_beneficio_pension` INT, IN `id_empleado` INT, IN `flag_estatus` INT, IN `fec_operacion` DATE, IN `txt_observaciones` MEDIUMTEXT, IN `usuario` INT)  BEGIN

  DECLARE N INT;

	IF NOT EXISTS ( SELECT t.fk_rhc091_num_utiles_beneficio FROM rh_c095_operaciones_beneficios_utiles AS t WHERE t.fk_rhc091_num_utiles_beneficio = id_beneficio_pension) THEN

		INSERT INTO rh_c095_operaciones_beneficios_utiles(ind_estado, fk_rhc091_num_utiles_beneficio, fk_rhb001_num_empleado, num_flag_estatus, fec_operacion,txt_observaciones,fk_a018_num_seguridad_usuario)
		VALUES ( txt_estatus,id_beneficio_pension,id_empleado,flag_estatus,fec_operacion,txt_observaciones,usuario);
  ELSE

		SET N = (SELECT u_rh_ultimoRegistroBeneficioUtiles(id_beneficio_pension));

		update rh_c095_operaciones_beneficios_utiles SET num_flag_estatus=0 WHERE pk_num_operaciones_beneficios_utiles=N;

		INSERT INTO rh_c095_operaciones_beneficios_utiles(ind_estado, fk_rhc091_num_utiles_beneficio, fk_rhb001_num_empleado, num_flag_estatus, fec_operacion,txt_observaciones,fk_a018_num_seguridad_usuario)
		VALUES ( txt_estatus,id_beneficio_pension,id_empleado,flag_estatus,fec_operacion,txt_observaciones,usuario);

  END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `w_rh_operacionesCeseReingreso` (IN `txt_estatus` VARCHAR(2), IN `id_cese_reingreso` INT, IN `id_empleado` INT, IN `flag_estatus` INT, IN `fec_operacion` DATE, IN `txt_observaciones` MEDIUMTEXT, IN `usuario` INT)  BEGIN

  DECLARE N INT;

	IF NOT EXISTS ( SELECT t.fk_rhc077_num_empleado_cese_reingreso FROM rh_c049_operaciones_cese_reingreso AS t WHERE t.fk_rhc077_num_empleado_cese_reingreso = id_cese_reingreso) THEN


		INSERT INTO rh_c049_operaciones_cese_reingreso(ind_estado, fk_rhc077_num_empleado_cese_reingreso, fk_rhb0001_num_empleado, num_flag_estatus, fec_operacion,txt_observaciones,fk_a018_num_seguridad_usuario)
		VALUES ( txt_estatus,id_cese_reingreso,id_empleado,flag_estatus,fec_operacion,txt_observaciones,usuario);


  ELSE


		SET N = (SELECT u_rh_ultimoRegistroCeseReingreso(id_cese_reingreso));



		update rh_c049_operaciones_cese_reingreso SET num_flag_estatus=0 WHERE pk_num_operaciones_cr=N;


		INSERT INTO rh_c049_operaciones_cese_reingreso(ind_estado, fk_rhc077_num_empleado_cese_reingreso, fk_rhb0001_num_empleado, num_flag_estatus, fec_operacion,txt_observaciones,fk_a018_num_seguridad_usuario)
		VALUES ( txt_estatus,id_cese_reingreso,id_empleado,flag_estatus,fec_operacion,txt_observaciones,usuario);



  END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `w_rh_operacionesProcesoPension` (IN `txt_estatus` VARCHAR(2), IN `id_proceso_pension` INT, IN `id_empleado` INT, IN `flag_estatus` INT, IN `fec_operacion` DATE, IN `txt_observaciones` MEDIUMTEXT, IN `usuario` INT)  BEGIN

  DECLARE N INT;

	IF NOT EXISTS ( SELECT t.fk_rhb003_num_pension FROM rh_c086_operaciones_pension AS t WHERE t.fk_rhb003_num_pension = id_proceso_pension) THEN


		INSERT INTO rh_c086_operaciones_pension(ind_estado, fk_rhb003_num_pension, fk_rhb0001_num_empleado, num_flag_estatus, fec_operacion,txt_observaciones,fk_a018_num_seguridad_usuario)
		VALUES ( txt_estatus,id_proceso_pension,id_empleado,flag_estatus,fec_operacion,txt_observaciones,usuario);


  ELSE


		SET N = (SELECT u_rh_ultimoRegistroProcesoPension(id_proceso_pension));


		update rh_c086_operaciones_pension SET num_flag_estatus=0 WHERE pk_num_operaciones_pension=N;


		INSERT INTO rh_c086_operaciones_pension(ind_estado, fk_rhb003_num_pension, fk_rhb0001_num_empleado, num_flag_estatus, fec_operacion,txt_observaciones,fk_a018_num_seguridad_usuario)
		VALUES ( txt_estatus,id_proceso_pension,id_empleado,flag_estatus,fec_operacion,txt_observaciones,usuario);



  END IF;

END$$

--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `u_pr_generar_cierremensual` (`fec_anio` VARCHAR(4), `fec_mes` VARCHAR(4), `presupuesto` INT, `usuario` INT) RETURNS INT(11) BEGIN
SET @anio=fec_anio;
 SET @mes=fec_mes;
 SET @presupuesto=presupuesto;
 SET @usuario=usuario;


 IF (@mes="01") THEN
	INSERT INTO pr_d001_ejecucion_presupuestaria  (fk_prc002_num_presupuesto_det, fec_anio,fec_mes,
num_disponibilidad_presupuestaria_inicial,num_disponibilidad_financiera_inicial,num_incremento,num_disminucion,num_credito,num_presupuesto_ajustado,
          num_compromiso, num_causado, num_pagado, num_disponibilidad_presupuestaria,num_disponibilidad_financiera,num_variacion,fec_ultima_modificacion,
          fk_a018_num_seguridad_usuario)
	SELECT pk_num_presupuesto_det, @anio, @mes,				num_monto_aprobado, num_monto_aprobado,
          num_monto_incremento,num_monto_disminucion,num_monto_credito,num_monto_ajustado,num_monto_compromiso, num_monto_causado, num_monto_pagado, num_monto_ajustado-num_monto_compromiso, num_monto_ajustado-num_monto_pagado,0,NOW(), @usuario	FROM pr_c002_presupuesto_det WHERE fk_prb004_num_presupuesto=@presupuesto;
  ELSE
INSERT INTO pr_d001_ejecucion_presupuestaria  (fk_prc002_num_presupuesto_det, fec_anio,fec_mes,
num_disponibilidad_presupuestaria_inicial,num_disponibilidad_financiera_inicial,num_incremento,num_disminucion,num_credito,num_presupuesto_ajustado,
          num_compromiso, num_causado, num_pagado, num_disponibilidad_presupuestaria,num_disponibilidad_financiera,num_variacion,fec_ultima_modificacion,
          fk_a018_num_seguridad_usuario)
	SELECT pk_num_presupuesto_det, @anio, @mes,
					(SELECT num_disponibilidad_presupuestaria FROM pr_d001_ejecucion_presupuestaria  WHERE fec_anio=@anio and CAST(fec_mes AS UNSIGNED)=@mes-1  and fk_prc002_num_presupuesto_det=pk_num_presupuesto_det),
          (SELECT num_disponibilidad_financiera FROM pr_d001_ejecucion_presupuestaria  WHERE fec_anio=@anio and CAST(fec_mes AS UNSIGNED)=@mes-1  and fk_prc002_num_presupuesto_det=pk_num_presupuesto_det),
           num_monto_incremento,num_monto_disminucion,num_monto_credito,num_monto_ajustado,num_monto_compromiso, num_monto_causado, num_monto_pagado, num_monto_ajustado-num_monto_compromiso, num_monto_ajustado-num_monto_pagado,0,NOW(),@usuario	FROM pr_c002_presupuesto_det WHERE fk_prb004_num_presupuesto=@presupuesto;
 END IF;

RETURN 1;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `u_pr_generar_presupuesto` (`idAntepresupuesto` INT, `codPresupuesto` VARCHAR(4), `usuario` INT) RETURNS INT(11) BEGIN
SET @codPresu=codPresupuesto;

insert into pr_b004_presupuesto (fk_rhb001_empleado_creado,fk_rhb001_empleado_aprobado,ind_cod_presupuesto, fec_anio,fec_presupuesto,fec_inicio,fec_fin,
ind_numero_gaceta,fec_gaceta,ind_numero_decreto, fecha_decreto,num_monto_aprobado,num_monto_ajustado,ind_estado,fec_ultima_modificacion,
fk_a018_num_seguridad_usuario,fk_a018_seguridad_usuario, ind_tipo_presupuesto, fk_a001_num_organismo,
fk_prb008_num_sector, fk_prb009_num_programa, fk_prb010_num_subprograma, fk_prb005_num_proyecto, fk_prb011_num_actividad, fk_prb006_aespecifica,
fk_prb007_acentralizada, fk_prb012_unidad_ejecutora,fk_prb003_num_antepresupuesto,fk_prb016_indice_presupuestario)

 select fk_rhb001_num_empleado_creado_por,usuario, @codPresu,fec_anio,CURDATE(),fec_inicio,dec_fin,
ind_numero_gaceta, fec_gaceta ,ind_numero_decreto, fec_decreto, num_monto_generado,num_monto_generado,'AP',NOW(),usuario,usuario,ind_tipo_presupuesto,fk_a001_num_organismo,
fk_prb008_num_sector, fk_prb009_num_programa, fk_prb010_num_subprograma, fk_prb005_num_proyecto, fk_prb011_num_actividad, fk_prb006_aespecifica,
fk_prb007_acentralizada, fk_prb012_unidad_ejecutora, pk_num_antepresupuesto,fk_prb016_indice_presupuestario from pr_b003_antepresupuesto where pk_num_antepresupuesto = idAntepresupuesto;

SET @presupuestoid = LAST_INSERT_ID();
insert into pr_c002_presupuesto_det (fk_prb004_num_presupuesto, fk_prb002_num_partida_presupuestaria, num_flags_anexa,num_flags_reformulacion,
num_monto_aprobado,num_monto_ajustado, num_monto_compromiso, num_monto_causado, num_monto_pagado,num_monto_incremento,num_monto_disminucion,
num_monto_credito,fk_pr006_num_aespecifica,fk_a018_num_seguridad_usuario,fec_ultima_modificacion)

select @presupuestoid, fk_prb002_num_partida_presupuestaria,if(num_monto_presupuestado<1,1,0),0,num_monto_aprobado,num_monto_aprobado,0,0,0,0,0,0,fk_pr_b006_aespecifica,usuario,NOW()
from pr_c001_antepresupuesto_det where fk_prb003_num_antepresupuesto = idAntepresupuesto;
RETURN 1;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `u_rh_ultimoRegistroAsignacionUtiles` (`id_asignacion_utiles` INT) RETURNS INT(11) BEGIN
	 SET @registro = (SELECT 	pk_num_operaciones_asignaciones_utiles
		FROM rh_c096_operaciones_asignaciones_utiles
		WHERE fk_rhc093_utiles_asignacion=id_asignacion_utiles
		AND num_flag_estatus=1
    ORDER BY pk_num_operaciones_asignaciones_utiles   DESC);

	RETURN @registro;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `u_rh_ultimoRegistroBeneficioUtiles` (`id_beneficio_utiles` INT) RETURNS INT(11) BEGIN
	 SET @registro = (SELECT 	pk_num_operaciones_beneficios_utiles
		FROM rh_c095_operaciones_beneficios_utiles
		WHERE fk_rhc091_num_utiles_beneficio=id_beneficio_utiles
		AND num_flag_estatus=1
    ORDER BY pk_num_operaciones_beneficios_utiles  DESC);

	RETURN @registro;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `u_rh_ultimoRegistroCeseReingreso` (`id_empleado_cese_reingreso` INT) RETURNS INT(11) BEGIN
 SET @registro = (SELECT pk_num_operaciones_cr
		FROM rh_c049_operaciones_cese_reingreso
		WHERE fk_rhc077_num_empleado_cese_reingreso=id_empleado_cese_reingreso
		AND num_flag_estatus=1
    ORDER BY pk_num_operaciones_cr DESC);

	RETURN @registro;

END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `u_rh_ultimoRegistroProcesoPension` (`id_proceso_pension` INT) RETURNS INT(11) BEGIN
 SET @registro = (SELECT pk_num_operaciones_pension
		FROM rh_c086_operaciones_pension
		WHERE fk_rhb003_num_pension=id_proceso_pension
		AND num_flag_estatus=1
    ORDER BY pk_num_operaciones_pension  DESC);

	RETURN @registro;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `a042_denuncia_remitida`
--

CREATE TABLE `a042_denuncia_remitida` (
  `pk_num_denuncia_remitida` int(11) NOT NULL,
  `fk_dnb001_num_denuncia` int(11) NOT NULL,
  `fk_pfb001_num_planificacion` int(11) DEFAULT NULL,
  `ind_observacion` text COLLATE utf8_spanish2_ci,
  `ind_estatus` varchar(2) COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'PE',
  `fec_ultima_modificacion` datetime NOT NULL,
  `fk_a018_num_seguridad_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dn_b001_denuncia`
--

CREATE TABLE `dn_b001_denuncia` (
  `pk_num_denuncia` int(11) NOT NULL,
  `cod_solicitud` varchar(9) COLLATE utf8_spanish2_ci NOT NULL,
  `ind_correlativo` varchar(12) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `ind_num_denuncia` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fec_anio` year(4) NOT NULL,
  `fk_a023_num_centro_costo` int(11) NOT NULL,
  `ind_propuesta` varchar(200) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `ind_descripcion_denuncia` longtext COLLATE utf8_spanish2_ci NOT NULL,
  `ind_motivo_origen` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `ind_estatus_solicitud` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `ind_estatus_tramite` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `num_duracion` int(11) DEFAULT NULL,
  `fec_inicio` date DEFAULT NULL,
  `fec_termino_real` date DEFAULT NULL,
  `fec_elaborado` datetime NOT NULL,
  `fec_revisado` date DEFAULT NULL,
  `fec_aprobado` date DEFAULT NULL,
  `fec_anulado` date DEFAULT NULL,
  `fec_recepcion` date NOT NULL,
  `fk_a018_num_seguridad_usuario` int(11) NOT NULL,
  `fec_ultima_modificacion` datetime NOT NULL,
  `fk_a039_num_ente` int(11) NOT NULL,
  `fk_rhb001_num_empleado_elabora` int(11) NOT NULL,
  `fk_rhb001_num_empleado_revisa` int(11) DEFAULT NULL,
  `fk_rhb001_num_empleado_aprueba` int(11) DEFAULT NULL,
  `fk_rhb001_num_empleado_anula` int(11) DEFAULT NULL,
  `ind_especificacion_tipo_denuncia` text COLLATE utf8_spanish2_ci,
  `fk_a006_pk_num_miscelaneo_detalle_tipo_denuncia` int(11) NOT NULL,
  `ind_especificacion_origen_denuncia` text COLLATE utf8_spanish2_ci,
  `fk_a006_pk_num_miscelaneo_detalle_origen` int(11) NOT NULL,
  `fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion` int(11) NOT NULL,
  `fk_a039_num_ente_recursos` int(11) DEFAULT NULL,
  `fk_a013_num_sector` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `dn_b002_prorroga`
--

CREATE TABLE `dn_b002_prorroga` (
  `pk_num_prorroga_detalle` int(11) NOT NULL,
  `fec_anio` year(4) NOT NULL,
  `num_secuencia` int(4) NOT NULL,
  `num_prorroga` int(4) NOT NULL,
  `ind_motivo` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `fec_preparacion` datetime NOT NULL,
  `fec_registro` datetime NOT NULL,
  `fec_revision` datetime NOT NULL,
  `fec_aprobacion` datetime NOT NULL,
  `fec_anulado` datetime DEFAULT NULL,
  `ind_estatus` varchar(2) COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'PR' COMMENT 'PR:En Preparación, RV:Revisado, AP:Aprobado, AN:Anulado',
  `fk_dnb001_num_denuncia` int(11) NOT NULL,
  `fk_rhb001_num_empleado_elabora` int(11) NOT NULL,
  `fk_rhb001_num_empleado_revisa` int(11) NOT NULL,
  `fk_rhb001_num_empleado_aprueba` int(11) NOT NULL,
  `fk_rhb001_num_empleado_anula` int(11) NOT NULL,
  `fk_dnd001_num_detalle_dc` int(11) NOT NULL,
  `fec_ultima_modificacion` datetime NOT NULL,
  `fk_a018_num_seguridad_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dn_c001_fase`
--

CREATE TABLE `dn_c001_fase` (
  `pk_num_fase` int(11) NOT NULL,
  `cod_fase` varchar(3) COLLATE utf8_spanish2_ci NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipo_actuacion` int(11) NOT NULL,
  `ind_descripcion` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `num_estatus` int(1) NOT NULL,
  `fk_a018_num_seguridad_usuario` int(11) NOT NULL,
  `fec_ultima_modificacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `dn_c002_actividad`
--

CREATE TABLE `dn_c002_actividad` (
  `pk_num_actividad` int(11) NOT NULL,
  `cod_actividad` varchar(7) COLLATE utf8_spanish2_ci NOT NULL,
  `ind_tipo_actuacion` int(11) NOT NULL,
  `ind_descripcion` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `ind_comentarios` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `num_duracion` int(4) NOT NULL,
  `num_flag_auto_archivo` int(1) NOT NULL DEFAULT '0',
  `num_flag_no_afecto_plan` int(1) NOT NULL DEFAULT '0',
  `num_estatus` int(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` int(11) NOT NULL,
  `fec_ultima_modificacion` datetime NOT NULL,
  `fk_dnc001_num_fase` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `dn_c003_tipo_resultado`
--

CREATE TABLE `dn_c003_tipo_resultado` (
  `pk_num_tipo_resultado` int(11) NOT NULL,
  `ind_resultado` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipo_actuacion` int(11) NOT NULL,
  `fec_ultima_modificacion` datetime NOT NULL,
  `fk_a018_num_seguridad_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `dn_d001_actividades`
--

CREATE TABLE `dn_d001_actividades` (
  `pk_num_detalle_dc` int(11) NOT NULL,
  `num_secuencia` int(3) NOT NULL,
  `fec_anio` year(4) NOT NULL,
  `fec_inicio` date NOT NULL,
  `fec_inicio_real` date NOT NULL,
  `fec_registro_ejecucion` datetime DEFAULT NULL,
  `fec_registro_termino` datetime DEFAULT NULL,
  `fec_termino` date NOT NULL,
  `fec_termino_real` date NOT NULL,
  `fec_cierre` date NOT NULL,
  `fec_termino_cierre` date NOT NULL,
  `num_dias_cierre` int(4) NOT NULL,
  `num_prorroga` int(4) NOT NULL,
  `num_duracion` int(4) NOT NULL,
  `ind_observaciones` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `ind_estatus` varchar(2) COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'PR' COMMENT 'PR:En Preparación, RV:Revisado, EJ:En Ejecucion, AN:Anulado, CO:Completado',
  `fk_a018_num_seguridad_usuario` int(11) NOT NULL,
  `fec_ultima_modificacion` datetime NOT NULL,
  `fk_dnc002_num_actividad` int(11) NOT NULL,
  `fk_dnb001_num_denuncia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dn_e001_denunciantes`
--

CREATE TABLE `dn_e001_denunciantes` (
  `pk_num_actuacion_persona` int(11) NOT NULL,
  `fk_dnb001_num_denuncia` int(11) NOT NULL,
  `fk_a003_num_persona` int(11) NOT NULL,
  `fec_ultima_modificacion` datetime NOT NULL,
  `fk_a018_num_seguridad_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `dn_e002_prueba`
--

CREATE TABLE `dn_e002_prueba` (
  `pk_num_pruebas` int(11) NOT NULL,
  `num_cantidad` int(11) NOT NULL,
  `ind_numero_documento` varchar(25) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `ind_descripcion` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `fk_dnb001_num_denuncia` int(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` int(11) NOT NULL,
  `fec_ultima_modificacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `dn_e003_designacion`
--

CREATE TABLE `dn_e003_designacion` (
  `pk_num_designacion` int(11) NOT NULL,
  `num_coordinador` int(1) NOT NULL DEFAULT '0',
  `fec_designacion` date DEFAULT NULL,
  `fec_ultima_modificacion` datetime NOT NULL,
  `fk_a018_num_seguridad_usuario` int(11) NOT NULL,
  `fk_rhb001_num_empleado` int(11) NOT NULL,
  `fk_dnb001_num_denuncia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `dn_e004_valoracion`
--

CREATE TABLE `dn_e004_valoracion` (
  `pk_num_valoracion` int(11) NOT NULL,
  `ind_antecedente_expediente` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `ind_analisis_hechos_expediente` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `ind_valoracion_expediente` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `ind_conclusion_valoracion` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `ind_manifiesto` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `ind_recomendaciones` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `fec_valoracion` date NOT NULL,
  `num_tipo_instancia` int(11) NOT NULL,
  `fk_a004_num_dependencia_control` int(11) DEFAULT NULL,
  `fec_ultima_modificacion` datetime NOT NULL,
  `fk_a018_num_seguridad_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=COMPACT;


-- --------------------------------------------------------

--
-- Table structure for table `dn_e006_receptores`
--

CREATE TABLE `dn_e006_receptores` (
  `pk_receptor` int(11) NOT NULL,
  `fk_dnb001_num_denuncia` int(11) NOT NULL,
  `fk_rhb001_num_empleado` int(11) NOT NULL,
  `fec_ultima_modificacion` datetime NOT NULL,
  `fk_a018_num_seguridad_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `a042_denuncia_remitida`
--
ALTER TABLE `a042_denuncia_remitida`
  ADD PRIMARY KEY (`pk_num_denuncia_remitida`);

--
-- Indexes for table `dn_b001_denuncia`
--
ALTER TABLE `dn_b001_denuncia`
  ADD PRIMARY KEY (`pk_num_denuncia`),
  ADD KEY `fk_dn_b006_denuncia_a001_organismo1_idx` (`fk_a039_num_ente`),
  ADD KEY `fk_dn_b006_denuncia_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_elabora`),
  ADD KEY `fk_dn_b006_denuncia_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_revisa`),
  ADD KEY `fk_dn_b006_denuncia_rh_b001_empleado3_idx` (`fk_rhb001_num_empleado_aprueba`),
  ADD KEY `fk_dn_b006_denuncia_rh_b001_empleado4_idx` (`fk_rhb001_num_empleado_anula`),
  ADD KEY `fk_dn_b006_denuncia_dn_b004_tipo_denuncia1_idx` (`fk_a006_pk_num_miscelaneo_detalle_tipo_denuncia`),
  ADD KEY `fk_dn_b006_denuncia_dn_b003_origen1_idx` (`fk_a006_pk_num_miscelaneo_detalle_origen`),
  ADD KEY `fk_dn_b006_denuncia_a013_sector1_idx` (`fk_a013_num_sector`),
  ADD KEY `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario`);

--
-- Indexes for table `dn_b002_prorroga`
--
ALTER TABLE `dn_b002_prorroga`
  ADD PRIMARY KEY (`pk_num_prorroga_detalle`),
  ADD KEY `fk_dn_b005_prorroga_dn_b006_denuncia1_idx` (`fk_dnb001_num_denuncia`),
  ADD KEY `fk_dn_b005_prorroga_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_elabora`),
  ADD KEY `fk_dn_b005_prorroga_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_revisa`),
  ADD KEY `fk_dn_b005_prorroga_rh_b001_empleado3_idx` (`fk_rhb001_num_empleado_aprueba`),
  ADD KEY `fk_dn_b005_prorroga_rh_b001_empleado4_idx` (`fk_rhb001_num_empleado_anula`),
  ADD KEY `fk_dn_b005_prorroga_dn_d001_actividades1_idx` (`fk_dnd001_num_detalle_dc`),
  ADD KEY `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario`);

--
-- Indexes for table `dn_c001_fase`
--
ALTER TABLE `dn_c001_fase`
  ADD PRIMARY KEY (`pk_num_fase`),
  ADD UNIQUE KEY `cod_fase` (`cod_fase`,`fk_a006_num_miscelaneo_detalle_tipo_actuacion`) USING BTREE,
  ADD KEY `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario`);

--
-- Indexes for table `dn_c002_actividad`
--
ALTER TABLE `dn_c002_actividad`
  ADD PRIMARY KEY (`pk_num_actividad`),
  ADD KEY `fk_dn_a003_actividad_dn_a002_fase1_idx` (`fk_dnc001_num_fase`),
  ADD KEY `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario`);

--
-- Indexes for table `dn_c003_tipo_resultado`
--
ALTER TABLE `dn_c003_tipo_resultado`
  ADD PRIMARY KEY (`pk_num_tipo_resultado`),
  ADD UNIQUE KEY `resultado_UNIQUE` (`ind_resultado`),
  ADD KEY `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario`);

--
-- Indexes for table `dn_d001_actividades`
--
ALTER TABLE `dn_d001_actividades`
  ADD PRIMARY KEY (`pk_num_detalle_dc`),
  ADD KEY `fk_dn_d001_actividades_dn_a003_actividad1_idx` (`fk_dnc002_num_actividad`),
  ADD KEY `fk_dn_d001_actividades_dn_b006_denuncia1_idx` (`fk_dnb001_num_denuncia`),
  ADD KEY `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario`);

--
-- Indexes for table `dn_e001_denunciantes`
--
ALTER TABLE `dn_e001_denunciantes`
  ADD PRIMARY KEY (`pk_num_actuacion_persona`),
  ADD KEY `fk_dn_d003_actuacion_persona_dn_b006_denuncia1_idx` (`fk_dnb001_num_denuncia`),
  ADD KEY `fk_dn_d003_actuacion_persona_a003_persona1_idx` (`fk_a003_num_persona`),
  ADD KEY `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario`);

--
-- Indexes for table `dn_e002_prueba`
--
ALTER TABLE `dn_e002_prueba`
  ADD PRIMARY KEY (`pk_num_pruebas`),
  ADD KEY `dc_denuncia` (`fk_dnb001_num_denuncia`),
  ADD KEY `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario`);

--
-- Indexes for table `dn_e003_designacion`
--
ALTER TABLE `dn_e003_designacion`
  ADD PRIMARY KEY (`pk_num_designacion`),
  ADD KEY `fk_dn_c003_designacion_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado`),
  ADD KEY `fk_dn_c003_designacion_dn_b006_denuncia1_idx` (`fk_dnb001_num_denuncia`),
  ADD KEY `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario`);

--
-- Indexes for table `dn_e004_valoracion`
--
ALTER TABLE `dn_e004_valoracion`
  ADD PRIMARY KEY (`pk_num_valoracion`),
  ADD KEY `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario`);

--
-- Indexes for table `dn_e005_resultado`
--
ALTER TABLE `dn_e005_resultado`
  ADD PRIMARY KEY (`pk_num_resultado`),
  ADD KEY `fk_dn_c001_resultado_dn_b006_denuncia1_idx` (`fk_dnb001_num_denuncia`),
  ADD KEY `fk_dn_c001_resultado_dn_b001_tipo_resultado1_idx` (`fk_dnc003_num_tipo_resultado`),
  ADD KEY `fk_dn_c001_resultado_dn_b002_asignacion_actuacion1_idx` (`fk_a004_num_dependencia`),
  ADD KEY `fk_a018_num_seguridad_usuario_idx` (` fk_rhb001_num_empleado_elabora`);

--
-- Indexes for table `dn_e006_receptores`
--
ALTER TABLE `dn_e006_receptores`
  ADD PRIMARY KEY (`pk_receptor`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `a042_denuncia_remitida`
--
ALTER TABLE `a042_denuncia_remitida`
  MODIFY `pk_num_denuncia_remitida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dn_b001_denuncia`
--
ALTER TABLE `dn_b001_denuncia`
  MODIFY `pk_num_denuncia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `dn_b002_prorroga`
--
ALTER TABLE `dn_b002_prorroga`
  MODIFY `pk_num_prorroga_detalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `dn_c001_fase`
--
ALTER TABLE `dn_c001_fase`
  MODIFY `pk_num_fase` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `dn_c002_actividad`
--
ALTER TABLE `dn_c002_actividad`
  MODIFY `pk_num_actividad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `dn_c003_tipo_resultado`
--
ALTER TABLE `dn_c003_tipo_resultado`
  MODIFY `pk_num_tipo_resultado` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dn_d001_actividades`
--
ALTER TABLE `dn_d001_actividades`
  MODIFY `pk_num_detalle_dc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `dn_e001_denunciantes`
--
ALTER TABLE `dn_e001_denunciantes`
  MODIFY `pk_num_actuacion_persona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `dn_e002_prueba`
--
ALTER TABLE `dn_e002_prueba`
  MODIFY `pk_num_pruebas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `dn_e003_designacion`
--
ALTER TABLE `dn_e003_designacion`
  MODIFY `pk_num_designacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `dn_e004_valoracion`
--
ALTER TABLE `dn_e004_valoracion`
  MODIFY `pk_num_valoracion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dn_e005_resultado`
--
ALTER TABLE `dn_e005_resultado`
  MODIFY `pk_num_resultado` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dn_e006_receptores`
--
ALTER TABLE `dn_e006_receptores`
  MODIFY `pk_receptor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;