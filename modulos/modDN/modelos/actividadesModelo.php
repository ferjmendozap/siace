<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: DENUNCIAS
 * PROCESO: REGISTRO DE LAS ACTIVIDADES ASOCIADAS A LAS DENUNCIAS / QUEJAS / RECLAMOS
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Guidmar Espinooza                |  dtecnica.conmumat@gmail.com       |         0414-1913443           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        12-12-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_MODELO.'miscelaneoModelo.php';
class actividadesModelo extends Modelo
{
    public $atMiscelaneoModelo;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo=new miscelaneoModelo();
    }
    public function metListarActividades()
    {
        $listar = $this->_db->query(
            "SELECT *, 
              dn_c001_fase.ind_descripcion AS fase,
              dn_c002_actividad.ind_descripcion AS actividad,
              a006_miscelaneo_detalle.pk_num_miscelaneo_detalle AS pkTipoActuacion,
              a006_miscelaneo_detalle.ind_nombre_detalle AS tipoActuacion
            FROM `dn_c002_actividad` 
            INNER JOIN dn_c001_fase ON dn_c001_fase.pk_num_fase=dn_c002_actividad.fk_dnc001_num_fase
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle=dn_c001_fase.fk_a006_num_miscelaneo_detalle_tipo_actuacion
            ORDER BY dn_c002_actividad.cod_actividad ASC
           ");

        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metMostrarActividad($idActividad)
    {
        $consulta =  $this->_db->query(
            "SELECT *, 
              dn_c002_actividad.num_flag_auto_archivo as ind_auto_archivo,
              dn_c002_actividad.num_flag_no_afecto_plan as ind_afecto_plan,
              dn_c001_fase.ind_descripcion AS fase,
              dn_c002_actividad.ind_descripcion AS actividad,
              a006_miscelaneo_detalle.pk_num_miscelaneo_detalle AS pkTipoActuacion,
              a006_miscelaneo_detalle.ind_nombre_detalle AS tipoActuacion
            FROM `dn_c002_actividad` 
            INNER JOIN dn_c001_fase ON dn_c001_fase.pk_num_fase=dn_c002_actividad.fk_dnc001_num_fase
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle=dn_c001_fase.fk_a006_num_miscelaneo_detalle_tipo_actuacion
            INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = dn_c002_actividad.fk_a018_num_seguridad_usuario
            WHERE
              pk_num_actividad=$idActividad 
            ORDER BY dn_c002_actividad.cod_actividad ASC"
        );

        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetch();
    }

    public function metNuevaActividad($registro)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "INSERT INTO
              dn_c002_actividad
              SET
                cod_actividad=:cod_actividad,
                ind_tipo_actuacion=:ind_tipo_actuacion,
                ind_descripcion=:ind_descripcion,
                ind_comentarios=:ind_comentarios,
                num_duracion=:num_duracion,
                num_flag_auto_archivo=:num_flag_auto_archivo,
                num_flag_no_afecto_plan=:num_flag_no_afecto_plan,
                num_estatus=:num_estatus,
                fk_dnc001_num_fase=:fk_dnc002_num_fase,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
               ");
        $NuevoRegistro->execute(array(
            ':cod_actividad' => $registro['hd_cod_actividad'],
            ':ind_tipo_actuacion' => $registro['fk_a006_num_miscelaneo_detalle_tipo_actuacion'],
            ':ind_descripcion' => $registro['txt_descripcion_actividad'],
            ':ind_comentarios' => $registro['txt_comentarios_actividad'],
            ':num_duracion' => $registro['num_duracion_actividad'],
            ':num_flag_auto_archivo' => $registro['ind_auto_archivo'],
            ':num_flag_no_afecto_plan' => $registro['ind_afecto_plan'],
            ':num_estatus' => $registro['num_estatus'],
            ':fk_dnc002_num_fase' => $registro['pk_num_fase']
        ));

        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarActividad($idActividad,$registro)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
              dn_c002_actividad
              SET
                
                ind_descripcion=:ind_descripcion,
                ind_comentarios=:ind_comentarios,
                num_duracion=:num_duracion,
                num_flag_auto_archivo=:num_flag_auto_archivo,
                num_flag_no_afecto_plan=:num_flag_no_afecto_plan,
                num_estatus=:num_estatus,
                
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
              WHERE
                pk_num_actividad='$idActividad'
               ");
        $NuevoRegistro->execute(array(

            ':ind_descripcion' => $registro['txt_descripcion_actividad'],
            ':ind_comentarios' => $registro['txt_comentarios_actividad'],
            ':num_duracion' => $registro['num_duracion_actividad'],
            ':num_flag_auto_archivo' => $registro['ind_auto_archivo'],
            ':num_flag_no_afecto_plan' => $registro['ind_afecto_plan'],
            ':num_estatus' => $registro['num_estatus']

        ));
        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metEliminarActividad($idActividad)
    {
        $this->_db->beginTransaction();
        $elimar=$this->_db->prepare("
                DELETE FROM
                  dn_c002_actividad
                WHERE
                  pk_num_actividad=:pk_num_actividad
            ");
        $elimar->execute(array(
            'pk_num_actividad'=>$idActividad
        ));
        $error=$elimar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idActividad;
        }
    }
}