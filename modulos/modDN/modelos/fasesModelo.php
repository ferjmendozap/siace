<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Denuncias
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Guidmar Espinoza     | dtecnica.conmumat@gmail.com          | 0414-1913443
 * |
 * |_____________________________________________________________________________________
*****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';


class fasesModelo extends Modelo
{
    public $atMiscelaneoModelo;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo=new miscelaneoModelo();
    }
    public function metListarFase($lista = false)
    {
        $listar = $this->_db->query(
            "SELECT *, fk_a006_num_miscelaneo_detalle_tipo_actuacion AS pkTipoActuacion,
                dn_c001_fase.ind_descripcion as indFase,
                a006_miscelaneo_detalle.ind_nombre_detalle AS tipoActuacion
             FROM dn_c001_fase 
             INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = dn_c001_fase.fk_a006_num_miscelaneo_detalle_tipo_actuacion
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metFiltarFase($w = false)
    {
        if($w){
            $where=$w;
        }
        else $where='';
        $listar = $this->_db->query(
            "SELECT *, fk_a006_num_miscelaneo_detalle_tipo_actuacion AS pkTipoActuacion,
                dn_c001_fase.ind_descripcion as indFase 
             FROM dn_c001_fase 
             INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = dn_c001_fase.fk_a006_num_miscelaneo_detalle_tipo_actuacion
             $where");

        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }
    
    public function metMostrarFase($idFase)
    {
        $consulta =  $this->_db->query(
            "SELECT
              *,
              dn_c001_fase.ind_descripcion AS fase,
              a018_seguridad_usuario.ind_usuario,
              a006_miscelaneo_detalle.ind_nombre_detalle AS tipoActuacion
              
            FROM
              dn_c001_fase
            INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = dn_c001_fase.fk_a018_num_seguridad_usuario
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = dn_c001_fase.fk_a006_num_miscelaneo_detalle_tipo_actuacion
            WHERE
              pk_num_fase=$idFase "
        );
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetch();
    }

    public function metNuevaFase($registro)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "INSERT INTO
              dn_c001_fase
              SET
               	cod_fase=:cod_fase,
                fk_a006_num_miscelaneo_detalle_tipo_actuacion=:fk_a006_num_miscelaneo_detalle_tipo_actuacion,
                ind_descripcion=:ind_descripcion,
                num_estatus=:num_estatus,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
               ");
        $NuevoRegistro->execute(array(
            ':cod_fase' => $registro['cod_fase'],
            ':fk_a006_num_miscelaneo_detalle_tipo_actuacion' => $registro['fk_a006_num_miscelaneo_detalle_tipo_actuacion'],
            ':ind_descripcion' => $registro['ind_descripcion'],
            ':num_estatus' => $registro['num_estatus']
        ));
        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }



    }

    public function metModificarFase($idFase,$registro)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
              dn_c001_fase
              SET
                fk_a006_num_miscelaneo_detalle_tipo_actuacion=:fk_a006_num_miscelaneo_detalle_tipo_actuacion,
                ind_descripcion=:ind_descripcion,
                num_estatus=:num_estatus,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
              WHERE
                pk_num_fase='$idFase'
               ");
        $NuevoRegistro->execute(array(
            ':fk_a006_num_miscelaneo_detalle_tipo_actuacion' => $registro['fk_a006_num_miscelaneo_detalle_tipo_actuacion'],
            ':ind_descripcion' => $registro['ind_descripcion'],
            ':num_estatus' => $registro['num_estatus']
        ));
        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metEliminarFase($idFase)
    {
        $this->_db->beginTransaction();
        $elimar=$this->_db->prepare("
                DELETE FROM
                  dn_c001_fase
                WHERE
                  pk_num_fase=:pk_num_fase
            ");
        $elimar->execute(array(
            'pk_num_fase'=>$idFase
        ));
        $error=$elimar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idFase;
        }
    }

    public function metJsonFase($idTipoActuacion)
    {
        $tipoActuacion = $this->_db->query(
            "SELECT
               *
            FROM
                dn_c001_fase
            WHERE fk_a006_num_miscelaneo_detalle_tipo_actuacion='$idTipoActuacion' AND num_estatus=1");
        $tipoActuacion->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoActuacion->fetchAll();
    }
}