<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/


class reportesModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    public function metListarSolicitudesReporte($filtro)
    {//Se listan las solicitudes existentes
        $Solicitud =  $this->_db->query(
            "SELECT *, a039_ente.ind_nombre_ente as ente, 
                act.ind_nombre_detalle as tipoActuacion, 
                estado.ind_nombre_detalle as estado,
                estadoTra.ind_nombre_detalle as estadoTra,
                a013_sector.pk_num_sector,
                a013_sector.ind_sector,
                a012_parroquia.pk_num_parroquia, 
                a012_parroquia.ind_parroquia,
                a011_municipio.pk_num_municipio,
                a011_municipio.ind_municipio, 
                a004_dependencia.pk_num_dependencia,
                usuario.ind_usuario as nombre_usuario,
                elabora.ind_usuario as nombre_elabora,
                rechaza.ind_usuario as nombre_rechaza,
                aprueba.ind_usuario as nombre_aprueba,
                dn_b001_denuncia.fec_ultima_modificacion as fec_modificacion,
                dn_b001_denuncia.fec_anulado as fec_anula_tramite,
                dn_b001_denuncia.fec_aprobado as fec_aprueba_tramite,
                origen.ind_nombre_detalle as origenDenuncia,
                tipoD.ind_nombre_detalle as tipoDenuncia,
                recursos.ind_nombre_ente as origenRecursos
             FROM dn_b001_denuncia 
             LEFT JOIN a039_ente 
             ON dn_b001_denuncia.fk_a039_num_ente = a039_ente.pk_num_ente 
             LEFT JOIN a039_ente AS recursos
             ON dn_b001_denuncia.fk_a039_num_ente_recursos = recursos.pk_num_ente
             LEFT JOIN a006_miscelaneo_detalle as estado
             ON estado.fk_a005_num_miscelaneo_maestro= 
             (SELECT pk_num_miscelaneo_maestro FROM a005_miscelaneo_maestro WHERE cod_maestro='DN-EST-SOL') 
             and estado.cod_detalle = dn_b001_denuncia.ind_estatus_solicitud 
             LEFT JOIN a006_miscelaneo_detalle as estadoTra
             ON estadoTra.fk_a005_num_miscelaneo_maestro= 
             (SELECT pk_num_miscelaneo_maestro FROM a005_miscelaneo_maestro WHERE cod_maestro='DN-EST-TRA') 
             and estadoTra.cod_detalle = dn_b001_denuncia.ind_estatus_tramite 
             LEFT JOIN a006_miscelaneo_detalle as act
             ON act.pk_num_miscelaneo_detalle = dn_b001_denuncia.fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion
             LEFT JOIN a006_miscelaneo_detalle as origen
             ON origen.pk_num_miscelaneo_detalle = dn_b001_denuncia.fk_a006_pk_num_miscelaneo_detalle_origen
             LEFT JOIN a006_miscelaneo_detalle as tipoD
             ON tipoD.pk_num_miscelaneo_detalle = dn_b001_denuncia.fk_a006_pk_num_miscelaneo_detalle_tipo_denuncia
             LEFT JOIN a013_sector 
             ON dn_b001_denuncia.fk_a013_num_sector=a013_sector.pk_num_sector
             LEFT JOIN a012_parroquia 
             ON a013_sector.fk_a012_num_parroquia=a012_parroquia.pk_num_parroquia
             LEFT JOIN a011_municipio
             ON a011_municipio.pk_num_municipio=a012_parroquia.fk_a011_num_municipio
             LEFT JOIN a023_centro_costo 
             ON a023_centro_costo.pk_num_centro_costo=dn_b001_denuncia.fk_a023_num_centro_costo
             LEFT JOIN a004_dependencia
             ON a004_dependencia.pk_num_dependencia=a023_centro_costo.fk_a004_num_dependencia
             LEFT JOIN a018_seguridad_usuario as usuario ON usuario.pk_num_seguridad_usuario = dn_b001_denuncia.fk_a018_num_seguridad_usuario
             LEFT JOIN a018_seguridad_usuario as elabora ON elabora.pk_num_seguridad_usuario = dn_b001_denuncia.fk_rhb001_num_empleado_elabora
             LEFT JOIN a018_seguridad_usuario as rechaza ON rechaza.pk_num_seguridad_usuario = dn_b001_denuncia.fk_rhb001_num_empleado_anula
             LEFT JOIN a018_seguridad_usuario as aprueba ON aprueba.pk_num_seguridad_usuario = dn_b001_denuncia.fk_rhb001_num_empleado_aprueba
             WHERE 1 $filtro"
        );

        $Solicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $Solicitud->fetchAll();
    }
}

?>