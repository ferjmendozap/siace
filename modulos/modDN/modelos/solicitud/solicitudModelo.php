<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Denuncias
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Guidmar Espinoza     | dtecnica.conmumat@gmail.com          | 0414-1913443
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once RUTA_MODELO . 'municipioModelo.php';
require_once RUTA_MODELO . 'parroquiaModelo.php';
require_once RUTA_MODELO . 'sectorModelo.php';
require_once RUTA_Modulo. 'modDN' . DS . 'modelos'  . DS .'funcionesGeneralesDNModelo.php';
require_once RUTA_Modulo. 'modCD' . DS . 'modelos'  . DS .'dependenciaModelo.php';

class solicitudModelo extends Modelo
{
    public $atIdUsuario;
    public $atMiscelaneoModelo;
    public $atFuncGnrles;//Funciones generales
    private $atIdContraloria;
    public $idSolicitud;
    public $criterio;
    public $atMunicipioModelo;
    public $atSectorModelo;
    public $atParroquiaModelo;
    public $atDependenciaModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atParroquiaModelo = new parroquiaModelo();
        $this->atMunicipioModelo = new municipioModelo();
        $this->atSectorModelo = new sectorModelo();
        $this->atDependenciaModelo= new dependenciaModelo();
        $this->atFuncGnrles= new funcionesGeneralesDNModelo();
    }

    public function metListarSolicitudes()
    {//Se listan las solicitudes existentes
        $listaSolicitudes =  $this->_db->query(
            "SELECT *, a039_ente.ind_nombre_ente , act.ind_nombre_detalle as tipoActuacion, estado.ind_nombre_detalle as estado
             FROM dn_b001_denuncia 
             LEFT JOIN a039_ente 
             ON dn_b001_denuncia.fk_a039_num_ente = a039_ente.pk_num_ente 
             LEFT JOIN a006_miscelaneo_detalle as estado
             ON estado.fk_a005_num_miscelaneo_maestro= 
             (SELECT pk_num_miscelaneo_maestro FROM a005_miscelaneo_maestro WHERE cod_maestro='DN-EST-SOL') 
             and estado.cod_detalle = dn_b001_denuncia.ind_estatus_solicitud 
             LEFT JOIN a006_miscelaneo_detalle as act
             ON act.pk_num_miscelaneo_detalle = dn_b001_denuncia.fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion
             WHERE  ind_estatus_tramite !='VA' AND ind_estatus_tramite !='CE' AND ind_estatus_tramite !='AN' AND ind_estatus_tramite !='AA' AND ind_estatus_tramite !='RM'
             ORDER BY cod_solicitud"
        );
        $listaSolicitudes->setFetchMode(PDO::FETCH_ASSOC);
        return $listaSolicitudes->fetchAll();
    }

    public function metMostrarSolicitud($idSolicitud)
    {//Se listan las solicitudes existentes
        $Solicitud =  $this->_db->query(
            "SELECT *, a039_ente.ind_nombre_ente , 
                act.ind_nombre_detalle as tipoActuacion, 
                estado.ind_nombre_detalle as estado, 
                a013_sector.pk_num_sector, 
                a012_parroquia.pk_num_parroquia, 
                a011_municipio.pk_num_municipio, 
                a004_dependencia.pk_num_dependencia,
                usuario.ind_usuario as nombre_usuario,
                elabora.ind_usuario as nombre_elabora,
                rechaza.ind_usuario as nombre_rechaza,
                aprueba.ind_usuario as nombre_aprueba,
                dn_b001_denuncia.fec_ultima_modificacion as fec_modificacion,
                dn_b001_denuncia.fec_anulado as fec_anula_tramite,
                dn_b001_denuncia.fec_aprobado as fec_aprueba_tramite,
                origen.ind_nombre_detalle as origenDenuncia,
                tipoD.ind_nombre_detalle as tipoDenuncia,
                recursos.ind_nombre_ente as origenRecursos
             FROM dn_b001_denuncia 
             LEFT JOIN a039_ente 
             ON dn_b001_denuncia.fk_a039_num_ente = a039_ente.pk_num_ente 
             LEFT JOIN a039_ente AS recursos
             ON dn_b001_denuncia.fk_a039_num_ente_recursos = recursos.pk_num_ente
             LEFT JOIN a006_miscelaneo_detalle as estado
             ON estado.fk_a005_num_miscelaneo_maestro= 
             (SELECT pk_num_miscelaneo_maestro FROM a005_miscelaneo_maestro WHERE cod_maestro='DN-EST-SOL') 
             and estado.cod_detalle = dn_b001_denuncia.ind_estatus_solicitud 
             LEFT JOIN a006_miscelaneo_detalle as act
             ON act.pk_num_miscelaneo_detalle = dn_b001_denuncia.fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion
             LEFT JOIN a006_miscelaneo_detalle as origen
             ON origen.pk_num_miscelaneo_detalle = dn_b001_denuncia.fk_a006_pk_num_miscelaneo_detalle_origen
             LEFT JOIN a006_miscelaneo_detalle as tipoD
             ON tipoD.pk_num_miscelaneo_detalle = dn_b001_denuncia.fk_a006_pk_num_miscelaneo_detalle_tipo_denuncia
             LEFT JOIN a013_sector 
             ON dn_b001_denuncia.fk_a013_num_sector=a013_sector.pk_num_sector
             LEFT JOIN a012_parroquia 
             ON a013_sector.fk_a012_num_parroquia=a012_parroquia.pk_num_parroquia
             LEFT JOIN a011_municipio
             ON a011_municipio.pk_num_municipio=a012_parroquia.fk_a011_num_municipio
             LEFT JOIN a023_centro_costo 
             ON a023_centro_costo.pk_num_centro_costo=dn_b001_denuncia.fk_a023_num_centro_costo
             LEFT JOIN a004_dependencia
             ON a004_dependencia.pk_num_dependencia=a023_centro_costo.fk_a004_num_dependencia
             LEFT JOIN a018_seguridad_usuario as usuario ON usuario.pk_num_seguridad_usuario = dn_b001_denuncia.fk_a018_num_seguridad_usuario
             LEFT JOIN a018_seguridad_usuario as elabora ON elabora.pk_num_seguridad_usuario = dn_b001_denuncia.fk_rhb001_num_empleado_elabora
             LEFT JOIN a018_seguridad_usuario as rechaza ON rechaza.pk_num_seguridad_usuario = dn_b001_denuncia.fk_rhb001_num_empleado_anula
             LEFT JOIN a018_seguridad_usuario as aprueba ON aprueba.pk_num_seguridad_usuario = dn_b001_denuncia.fk_rhb001_num_empleado_aprueba
             WHERE dn_b001_denuncia.pk_num_denuncia='$idSolicitud'"
        );

        $Solicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $Solicitud->fetch();
    }

    public function metMostrarDenunciantes($idSolicitud){
        $Denunciantes=$this->_db->query(
            "SELECT *, 
                a003_persona.pk_num_persona as codigo,
                a003_persona.ind_cedula_documento as cedula,
                CONCAT(if(a003_persona.ind_nombre1 is NULL, '',a003_persona.ind_nombre1 ),' ',if(a003_persona.ind_nombre2 is NULL, '',a003_persona.ind_nombre2 ),' ',if(a003_persona.ind_apellido1 is NULL, '',a003_persona.ind_apellido1 ),' ',if(a003_persona.ind_apellido2 is NULL, '',a003_persona.ind_apellido2 )) AS nombre,
                a036_persona_direccion.ind_direccion as direccion,
                a007_persona_telefono.ind_telefono as telefono
             FROM `dn_e001_denunciantes`
             INNER JOIN a003_persona ON dn_e001_denunciantes.fk_a003_num_persona=a003_persona.pk_num_persona
             LEFT JOIN a036_persona_direccion ON a003_persona.pk_num_persona=a036_persona_direccion.fk_a003_num_persona
             LEFT JOIN a007_persona_telefono ON a003_persona.pk_num_persona=a007_persona_telefono.fk_a003_num_persona
             WHERE dn_e001_denunciantes.fk_dnb001_num_denuncia='$idSolicitud'"
        );
        $Denunciantes->setFetchMode(PDO::FETCH_ASSOC);

        return $Denunciantes->fetchAll();
    }

    public function metMostrarReceptores($idSolicitud){
        $Receptores=$this->_db->query(
            "SELECT *, a004_dependencia.ind_dependencia, 
                dn_e006_receptores.fk_rhb001_num_empleado as codigo,
                a003_persona.ind_cedula_documento as cedula, 
                CONCAT(if(a003_persona.ind_nombre1 is NULL, '',a003_persona.ind_nombre1 ),' ',if(a003_persona.ind_nombre2 is NULL, '',a003_persona.ind_nombre2 ),' ',if(a003_persona.ind_apellido1 is NULL, '',a003_persona.ind_apellido1 ),' ',if(a003_persona.ind_apellido2 is NULL, '',a003_persona.ind_apellido2 )) AS nombre
                 FROM `dn_e006_receptores` 
                 INNER JOIN a003_persona ON dn_e006_receptores.fk_rhb001_num_empleado=pk_num_persona 
                 LEFT JOIN rh_b001_empleado ON rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona 
                 LEFT JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado=pk_num_empleado 
                 LEFT JOIN a004_dependencia ON pk_num_dependencia=fk_a004_num_dependencia 
                 WHERE dn_e006_receptores.fk_dnb001_num_denuncia='$idSolicitud'"
        );

        return $Receptores->fetchAll();
    }

    public function metMostrarPruebas($idSolicitud){
        $Pruebas=$this->_db->query(
            "SELECT 
              num_cantidad,
              ind_numero_documento,
              ind_descripcion,
              fk_dnb001_num_denuncia,
              pk_num_pruebas
             FROM dn_e002_prueba 
             WHERE fk_dnb001_num_denuncia='$idSolicitud'"
        );
        $Pruebas->setFetchMode(PDO::FETCH_ASSOC);

        return $Pruebas->fetchAll();
    }

    public function metCrearSolicitud($params)
    {# Permite registrar una Solicitud

        $this->_db->beginTransaction();
        $fec_recibido=$this->atFuncGnrles->metFormateaFechaMsql($params['fe_recibido']);
        if($params['t']=='nuevo'){
           $sql_solicitud = $this->_db->prepare("
              INSERT INTO dn_b001_denuncia SET
              cod_solicitud=:cod_solicitud,
              fec_anio=NOW(),
              ind_propuesta=:ind_propuesta,
              ind_descripcion_denuncia=:ind_descripcion_denuncia,
              ind_motivo_origen=:ind_motivo_origen,
              ind_estatus_solicitud=:ind_estatus_solicitud,
              ind_estatus_tramite='EP',
              fk_a039_num_ente=:fk_a039_num_ente,      
              fk_a006_pk_num_miscelaneo_detalle_tipo_denuncia=:fk_a006_pk_num_miscelaneo_detalle_tipo_denuncia,
              fk_a006_pk_num_miscelaneo_detalle_origen=:fk_a006_pk_num_miscelaneo_detalle_origen,
              fk_a013_num_sector=:fk_a013_num_sector,
              ind_especificacion_origen_denuncia =:ind_especificacion_origen_denuncia,
              ind_especificacion_tipo_denuncia=:ind_especificacion_tipo_denuncia,
              fk_a039_num_ente_recursos =:fk_a039_num_ente_recursos,
              fk_a023_num_centro_costo =:fk_a023_num_centro_costo,
              fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion=:fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion,
              fk_rhb001_num_empleado_elabora='$this->atIdUsuario',
              fec_elaborado=NOW(),
              fec_recepcion='$fec_recibido',
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
              ");

           $sql_solicitud->execute(array(
            'cod_solicitud'=>$params['cod_solicitud'],
            'ind_propuesta'=>$params['ind_propuesta'],
            'ind_descripcion_denuncia'=>$params['ind_descripcion_denuncia'],
            'ind_motivo_origen'=>$params['ind_motivo_origen'],
            'ind_estatus_solicitud'=>$params['ind_estatus'],
            'fk_a039_num_ente'=>$params['id_ente'],
            'fk_a006_pk_num_miscelaneo_detalle_tipo_denuncia'=>$params['idtipo_denuncia'],
            'fk_a006_pk_num_miscelaneo_detalle_origen'=>$params['idorigen_denuncia'],
            'fk_a013_num_sector'=>$params['num_sector'],
            'ind_especificacion_origen_denuncia' =>$params['ind_especificacion_origen_denuncia'],
            'ind_especificacion_tipo_denuncia'=>$params['ind_especificacion_tipo_denuncia'],
            'fk_a039_num_ente_recursos'=>$params['id_ente_recursos'],
            'fk_a023_num_centro_costo'=>$params['id_centro_costo'],
            'fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion'=>$params['tipoActuacion']
        ));
            $idDenuncia = $this->_db->lastInsertId();

        }else{

            $idSolicitud=$params['idSolicitud'];
            $sql_solicitud = $this->_db->prepare("
              UPDATE
              dn_b001_denuncia 
              SET
              ind_propuesta=:ind_propuesta,
              fec_recepcion=:fec_recepcion,
              ind_descripcion_denuncia=:ind_descripcion_denuncia,
              ind_motivo_origen=:ind_motivo_origen,
              fk_a006_pk_num_miscelaneo_detalle_tipo_denuncia=:fk_a006_pk_num_miscelaneo_detalle_tipo_denuncia,
              ind_especificacion_tipo_denuncia=:ind_especificacion_tipo_denuncia,
              fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion=:fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion,
              fk_a039_num_ente_recursos=:fk_a039_num_ente_recursos,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
              WHERE pk_num_denuncia='$idSolicitud'
              ");

            $sql_solicitud->execute(array(
                'ind_propuesta'=>$params['ind_propuesta'],
                'fec_recepcion'=>$fec_recibido,
                'ind_descripcion_denuncia'=>$params['ind_descripcion_denuncia'],
                'ind_motivo_origen'=>$params['ind_motivo_origen'],
                'fk_a006_pk_num_miscelaneo_detalle_tipo_denuncia'=>$params['idtipo_denuncia'],
                'ind_especificacion_tipo_denuncia'=>$params['ind_especificacion_tipo_denuncia'],
                'fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion'=>$params['tipoActuacion'],
                'fk_a039_num_ente_recursos'=>$params['id_ente_recursos']
            ));

            if (isset($params['denunciantes'])) {
                $delete_denunciante = $this->_db->prepare(
                    "DELETE FROM dn_e001_denunciantes
                     WHERE fk_dnb001_num_denuncia=:fk_dnb001_num_denuncia"
                );
                $delete_denunciante -> execute(array(
                    'fk_dnb001_num_denuncia'=>$idSolicitud
                ));
            }
            if (isset($params['prueba'])) {
                $delete_prueba = $this->_db->prepare(
                    "DELETE FROM dn_e002_prueba
                     WHERE fk_dnb001_num_denuncia=:fk_dnb001_num_denuncia"
                );
                $delete_prueba -> execute(array(
                    'fk_dnb001_num_denuncia'=>$idSolicitud
                ));
            }
            if (isset($params['recibido'])) {
                $delete_recibido = $this->_db->prepare(
                    "DELETE FROM dn_e006_receptores
                     WHERE fk_dnb001_num_denuncia=:fk_dnb001_num_denuncia"
                );
                $delete_recibido -> execute(array(
                    'fk_dnb001_num_denuncia'=>$idSolicitud
                ));
            }
            $idDenuncia=$idSolicitud;
        }

        ## Denunciantes


            $sql_denunciantes = $this->_db->prepare("
              INSERT INTO
              dn_e001_denunciantes
              SET
              fk_dnb001_num_denuncia=:fk_dnb001_num_denuncia,
              fk_a003_num_persona=:fk_a003_num_persona,
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              ");
            if (isset($params['denunciantes'])) {
                $detalle1 = $params['denunciantes'];
                foreach($detalle1['idPersona'] as $den=> $valor) {

                    $sql_denunciantes->execute(array(
                        'fk_dnb001_num_denuncia' => $idDenuncia,
                        'fk_a003_num_persona' => $valor
                    ));
                }
            }
        $idD=$idDenuncia;

        ## Receptores


        $sql_receptores = $this->_db->prepare("
              INSERT INTO
              dn_e006_receptores
              SET
              fk_dnb001_num_denuncia=:fk_dnb001_num_denuncia,
              fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              ");

        if (isset($params['recibido'])) {
            $det = $params['recibido'];
            foreach($det['idPersona'] as $rec=> $valor){
                $sql_receptores->execute(array(
                    'fk_dnb001_num_denuncia' => $idD,
                    'fk_rhb001_num_empleado' => $valor
                ));
            }
        }
        $idD2=$idD;

        ##  Pruebas
        $sql_pruebas = $this->_db->prepare("
          INSERT INTO
            dn_e002_prueba
          SET
              fk_dnb001_num_denuncia=:fk_dnb001_num_denuncia,
              num_cantidad=:num_cantidad,
              ind_numero_documento=:ind_numero_documento,
              ind_descripcion=:ind_descripcion, 
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
          ");

        if(isset($params['prueba'])) {
            $detalle2 = $params['prueba'];

            for($i=1; $i<=count($detalle2['secuencia']['ind_prueba']); $i++){
                $sql_pruebas->execute(array(
                    'fk_dnb001_num_denuncia' => $idD2,
                    'num_cantidad'=>$detalle2['secuencia']['num_cantidad_prueba'][$i],
                    'ind_numero_documento' =>$detalle2['secuencia']['ind_numero_documento'][$i],
                    'ind_descripcion' => $detalle2['secuencia']['ind_prueba'][$i]
                ));
            }
        }

        $fallaTansaccion = $sql_solicitud->errorInfo();
        $fallaTansaccion2 = $sql_denunciantes->errorInfo();
        $fallaTansaccion3 = $sql_pruebas->errorInfo();
        $fallaTansaccion4 = $sql_receptores->errorInfo();
        if(isset($delete_denunciante)){
            $fallaDelete = $delete_denunciante->errorInfo();
            if (!empty($fallaDelete[1]) && !empty($fallaDelete[2])){
                $this->_db->rollBack();
                return $fallaDelete;
            }
        }
        if(isset($delete_prueba)){
            $fallaDelete2 = $delete_prueba->errorInfo();
            if (!empty($fallaDelete2[1]) && !empty($fallaDelete2[2])){
                $this->_db->rollBack();
                return $fallaDelete2;
            }
        }

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }elseif (!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion2;
        }elseif (!empty($fallaTansaccion3[1]) && !empty($fallaTansaccion3[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion3;
        }elseif (!empty($fallaTansaccion4[1]) && !empty($fallaTansaccion4[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion4;
        }else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metJsonRechazar($idSolicitud){
        $this->_db->beginTransaction();
        $rechazo=$this->_db->prepare(
            "UPDATE dn_b001_denuncia
              SET 
              ind_estatus_solicitud=:ind_estatus_solicitud,
              fec_anulado=NOW(),
              fec_ultima_modificacion=NOW(),
              fk_rhb001_num_empleado_anula='$this->atIdUsuario',
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             WHERE pk_num_denuncia='$idSolicitud'
        ");

        $rechazo->execute(array(
            'ind_estatus_solicitud'=>'RE'
        ));

        #commit — Consigna una transacción

        $fallaTansaccion = $rechazo->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idSolicitud;
        }
    }

    public function metListarPersona()
    {
        $menu = $this->_db->query("
            SELECT * FROM a003_persona 
            WHERE fk_a006_num_miscelaneo_det_tipopersona= 
                (SELECT pk_num_miscelaneo_detalle 
                    FROM a006_miscelaneo_detalle 
                    WHERE cod_detalle='PART' 
                    and a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=
                        (SELECT pk_num_miscelaneo_maestro 
                            FROM a005_miscelaneo_maestro
                            WHERE cod_maestro='TIP_PERSON') )");

        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();

    }

    public function metListarEmpleados()
    {
        $menu = $this->_db->query("
              SELECT *, a004_dependencia.ind_dependencia 
              FROM rh_b001_empleado 
              INNER JOIN a003_persona ON fk_a003_num_persona=pk_num_persona 
              INNER JOIN rh_c076_empleado_organizacion ON fk_rhb001_num_empleado=pk_num_empleado 
              INNER JOIN a004_dependencia ON pk_num_dependencia=fk_a004_num_dependencia 
              WHERE rh_b001_empleado.num_estatus=1 ");
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();

    }

    public function metCrearPersona( $cedula,$nombre1,$apellido1,$direccion,$email,$telefono,$estatus )
    {
        $this->_db->beginTransaction();
        $nuevoRegistro = $this->_db->prepare("
                  INSERT INTO
                    a003_persona
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fec_ultima_modificacion=NOW(),
                    ind_cedula_documento=:ind_cedula_documento,
                    ind_nombre1=:ind_nombre1,
                    ind_apellido1=:ind_apellido1,
                    ind_email=:ind_email,
                    num_estatus=1,
                    ind_tipo_persona='N'
                ");

        $nuevoRegistro->execute(array(
            'ind_cedula_documento'=> $cedula,
            'ind_nombre1'=> $nombre1,
            'ind_apellido1'=> $apellido1,
            'ind_email'=> $email
        ));

        $idRegistro = $this->_db->lastInsertId();
        $registroDireccion = $this->_db->prepare("
                  INSERT INTO
                    a036_persona_direccion
                  SET
                    fk_a003_num_persona ='$idRegistro', ind_direccion=:ind_direccion
                ");
        $registroDireccion->execute(array(
            'ind_direccion' => $direccion

        ));

        $registroTelefono = $this->_db->prepare("
                  INSERT INTO
                    a007_persona_telefono
                  SET
                    fk_a003_num_persona ='$idRegistro', ind_telefono=:ind_telefono
                ");
        $registroTelefono->execute(array(
            'ind_telefono' => $telefono

        ));


        $error = $nuevoRegistro->errorInfo();
        $error1 = $registroDireccion->errorInfo();
        $error2 = $registroTelefono->errorInfo();

        if (!empty($error[1]) || !empty($error[2]) || !empty($error[3]) || !empty($error1[1]) || !empty($error1[2]) || !empty($error1[3])
            || !empty($error2[1]) || !empty($error2[2]) || !empty($error2[3])) {
            $this->_db->rollBack();
            return $error = array_merge($error, $error1, $error2);
        } else {
            $this->_db->commit();
            return $idRegistro;
        }
    }
}