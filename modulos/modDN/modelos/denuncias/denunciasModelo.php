<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Denuncias
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Guidmar Espinoza     | dtecnica.conmumat@gmail.com          | 0414-1913443
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once RUTA_MODELO . 'municipioModelo.php';
require_once RUTA_MODELO . 'parroquiaModelo.php';
require_once RUTA_MODELO . 'sectorModelo.php';
require_once RUTA_Modulo. 'modDN' . DS . 'modelos'  . DS .'funcionesGeneralesDNModelo.php';

class denunciasModelo extends Modelo
{
    private $atIdUsuario;
    public $atMiscelaneoModelo;
    public $atFuncGnrles;//Funciones generales
    private $atIdContraloria;
    public $idDenuncia;
    public $criterio;
    public $atMunicipioModelo;
    public $atSectorModelo;
    public $atParroquiaModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atParroquiaModelo = new parroquiaModelo();
        $this->atMunicipioModelo = new municipioModelo();
        $this->atSectorModelo = new sectorModelo();
        $this->atFuncGnrles = new funcionesGeneralesDNModelo();
    }

    public function metListarDenuncias()
    {//Se listan las denuncias existentes
        $listaDenuncias =  $this->_db->query(
            "SELECT *,
                  fk_a001_num_organismo
             FROM dn_b001_denuncia
                  INNER JOIN a039_ente
                  ON dn_b001_denuncia.fk_a001_num_organismo = a039_ente.pk_num_ente
             ORDER BY ind_num_denuncia"
        );
        $listaDenuncias->setFetchMode(PDO::FETCH_ASSOC);
        return $listaDenuncias->fetchAll();
    }

     /**
     * Lista las actividades de denuncias.
     */
    public function metListaActividades($params){

        $sql_query="SELECT dn_c002_actividad.*,dn_c001_fase.pk_num_fase,
                    dn_c001_fase.ind_descripcion as nombre_fase,
                    dn_c001_fase.cod_fase,
                    dn_c002_actividad.ind_descripcion AS nombre_actividad,
                    dn_c002_actividad.cod_actividad AS cod_act
                FROM dn_c002_actividad
                INNER JOIN dn_c001_fase ON dn_c002_actividad.fk_dnc001_num_fase=dn_c001_fase.pk_num_fase AND dn_c001_fase.fk_a006_num_miscelaneo_detalle_tipo_actuacion=
                (SELECT pk_num_miscelaneo_detalle FROM a006_miscelaneo_detalle WHERE fk_a005_num_miscelaneo_maestro=(SELECT pk_num_miscelaneo_maestro FROM a005_miscelaneo_maestro WHERE cod_maestro='TACT') 
                and a006_miscelaneo_detalle.cod_detalle='".$params."')
                WHERE dn_c002_actividad.num_estatus=1 
                ORDER BY cod_fase, cod_actividad";

        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }

    public function metActividadesAsignadas($idTramite){
        $sql_query="SELECT dn_d001_actividades.*,dn_c001_fase.pk_num_fase,
                    dn_c001_fase.ind_descripcion as nombre_fase,
                    dn_c001_fase.cod_fase,
                    dn_c002_actividad.ind_descripcion AS nombre_actividad,
                    dn_c002_actividad.cod_actividad AS cod_act,
                    dn_c002_actividad.num_flag_auto_archivo,
                    dn_c002_actividad.num_flag_no_afecto_plan,
                    estado.ind_nombre_detalle as estadoAct
                FROM dn_d001_actividades
                INNER JOIN dn_c002_actividad ON dn_c002_actividad.pk_num_actividad=dn_d001_actividades.fk_dnc002_num_actividad
                INNER JOIN dn_c001_fase ON dn_c002_actividad.fk_dnc001_num_fase=dn_c001_fase.pk_num_fase AND dn_c001_fase.fk_a006_num_miscelaneo_detalle_tipo_actuacion=
                (SELECT pk_num_miscelaneo_detalle FROM a006_miscelaneo_detalle WHERE fk_a005_num_miscelaneo_maestro=(SELECT pk_num_miscelaneo_maestro FROM a005_miscelaneo_maestro WHERE cod_maestro='TACT') 
                and a006_miscelaneo_detalle.pk_num_miscelaneo_detalle=dn_c001_fase.fk_a006_num_miscelaneo_detalle_tipo_actuacion)
                LEFT JOIN a006_miscelaneo_detalle as estado
                 ON estado.fk_a005_num_miscelaneo_maestro= 
                 (SELECT pk_num_miscelaneo_maestro FROM a005_miscelaneo_maestro WHERE cod_maestro='DN-EST-ACT') 
                 and estado.cod_detalle = dn_d001_actividades.ind_estatus
                WHERE dn_c002_actividad.num_estatus=1 and dn_d001_actividades.fk_dnb001_num_denuncia=".$idTramite."
                ORDER BY cod_fase, dn_c002_actividad.cod_actividad";

        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }

    public function metCrearDenuncia($params)
    {# Permite registrar una Solicitud
        $fec_inicio=$this->atFuncGnrles->metFormateaFechaMsql(trim($params['fe_inicio']));
        $this->_db->beginTransaction();

        if($params['acc']=='nuevo'){
            $Pk=$params['idSolicitud'];
            $estatusS='TR';
            $estatusT='GN';
            $estatusA='PR';
        }elseif($params['acc']=='aprobar'){
            $Pk=$params['idDenuncia'];
            $estatusS='TR';
            $estatusT='AP';
            $estatusA='PE';
        }elseif($params['acc']=='anular'){
            $Pk=$params['idDenuncia'];
            $estatusS='TR';
            $estatusT='AN';
            $estatusA='PR';
        }else{
            $Pk=$params['idSolicitud'];
            $estatusS='TR';
            $estatusT='GN';
            $estatusA='PR';
        }

            $sql_solicitud = $this->_db->prepare("
              UPDATE
              dn_b001_denuncia 
              SET
              ind_num_denuncia=:ind_num_denuncia,
              fec_inicio='$fec_inicio',
              num_duracion=:num_duracion,
              fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion=:fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion,
              fk_rhb001_num_empleado_aprueba='$this->atIdUsuario',
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              ind_estatus_solicitud='$estatusS',
              ind_estatus_tramite='$estatusT',
              fec_ultima_modificacion=NOW()
              WHERE pk_num_denuncia=$Pk
              ");

            $sql_solicitud->execute(array(
                'ind_num_denuncia'=>trim($params['codDenuncia']),
                'num_duracion'=>$params['num_duracion'],
                'fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion'=>$params['tActuacion']
            ));


        #### Prueba
        if($params['acc']=='nuevo' or $params['acc']=='modificar') {
            if (isset($params['prueba'])) {
                $delete_prueba = $this->_db->prepare(
                    "DELETE FROM dn_e002_prueba
                     WHERE fk_dnb001_num_denuncia=:fk_dnb001_num_denuncia"
                );
                $delete_prueba->execute(array(
                    'fk_dnb001_num_denuncia' => $Pk
                ));
            }
            $idDenuncia = $Pk;

            ##  Pruebas
            $sql_pruebas = $this->_db->prepare("
          INSERT INTO
            dn_e002_prueba
          SET
              fk_dnb001_num_denuncia=:fk_dnb001_num_denuncia,
              num_cantidad=:num_cantidad,
              ind_numero_documento=:ind_numero_documento,
              ind_descripcion=:ind_descripcion, 
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
          ");

            if (isset($params['prueba'])) {
                $detalle2 = $params['prueba'];

                for ($i = 1; $i <= count($detalle2['secuencia']['num_cantidad_prueba']); $i++) {
                    $sql_pruebas->execute(array(
                        'fk_dnb001_num_denuncia' => $idDenuncia,
                        'num_cantidad' => $detalle2['secuencia']['num_cantidad_prueba'][$i],
                        'ind_numero_documento' => trim($detalle2['secuencia']['ind_numero_documento'][$i]),
                        'ind_descripcion' => trim($detalle2['secuencia']['ind_prueba'][$i])
                    ));
                }
            }
            $Pk=$idDenuncia;
        }
        if($params['acc']!='nuevo') {
    ##### Auditores designados
            if (isset($params['auditor'])) {
                $delete_prueba = $this->_db->prepare(
                    "DELETE FROM dn_e003_designacion
                     WHERE fk_dnb001_num_denuncia=:fk_dnb001_num_denuncia"
                );
                $delete_prueba->execute(array(
                    'fk_dnb001_num_denuncia' => $Pk
                ));
            }
            $idDenuncia = $Pk;

        }

    #### Auditores
        $sql_auditores = $this->_db->prepare("
              INSERT INTO
              dn_e003_designacion
              SET
              fec_designacion=NOW(),
              fk_dnb001_num_denuncia=:fk_dnb001_num_denuncia,
              fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
              num_coordinador=:num_coordinador,
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              ");
        if (isset($params['auditor'])) {
            $detalle1 = $params['auditor'];
            foreach($detalle1['idPersona'] as $den=> $valor) {
                if(isset($params['auditor']['coordinador']) AND $params['auditor']['coordinador']==$valor){
                    $flag=1;
                }else{
                    $flag=0;
                }
                $sql_auditores->execute(array(
                    'fk_dnb001_num_denuncia' => $Pk,
                    'fk_rhb001_num_empleado' => $valor,
                    'num_coordinador'=>$flag
                ));
            }
        }



    #### Actividades
        if (isset($params['actividad'])) {
            $delete_prueba = $this->_db->prepare(
                "DELETE FROM dn_d001_actividades
                     WHERE fk_dnb001_num_denuncia=:fk_dnb001_num_denuncia"
            );
            $delete_prueba->execute(array(
                'fk_dnb001_num_denuncia' => $Pk
            ));
        }
        $year=date('Y');
        $sql_actividades = $this->_db->prepare("
              INSERT INTO
              dn_d001_actividades
              SET
              num_secuencia=:num_secuencia,
              num_duracion=:num_duracion,
              fec_termino=:fec_termino,
              fec_inicio=:fec_inicio,
              fk_dnc002_num_actividad=:fk_dnc002_num_actividad,
              fk_dnb001_num_denuncia=:fk_dnb001_num_denuncia,
              fec_anio=$year,
              ind_estatus='$estatusA',
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              ");

        if (isset($params['actividad'])) {
            $detalle3=$params['actividad'];
            $sec=1;

            for($i=0; $i<count($detalle3['secuencia']['duracion_actividad']); $i++){
                $sql_actividades->execute(array(
                    'num_secuencia'=>$sec,
                    'num_duracion'=>$detalle3['secuencia']['duracion_actividad'][$i],
                    'fec_termino'=>trim($detalle3['secuencia']['fecha_fin'][$i]),
                    'fec_inicio'=>trim($detalle3['secuencia']['fecha_inicio'][$i]),
                    'fk_dnc002_num_actividad'=>$detalle3['secuencia']['pkActividad'][$i],
                    'fk_dnb001_num_denuncia' => $Pk
                ));
                $sec=$sec+1;
            }
        }
        $fallaTansaccion = $sql_solicitud->errorInfo();
        if(isset($sql_pruebas)){
            $fallaTansaccion2 = $sql_pruebas->errorInfo();
        }
        $fallaTansaccion3 = $sql_actividades->errorInfo();
        if(isset($sql_auditores)){
            $fallaTansaccion4 = $sql_auditores->errorInfo();
        }


        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }elseif (!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion2;
        }elseif (!empty($fallaTansaccion3[1]) && !empty($fallaTansaccion3[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion3;
        }elseif (!empty($fallaTansaccion4[1]) && !empty($fallaTansaccion4[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion4;
        }else {
            $idRegistro = $Pk;
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metMostrarAuditores($idTramite){
        $Receptores=$this->_db->query(
            "SELECT *, 
                a003_persona.pk_num_persona as codigo,
                a003_persona.ind_cedula_documento as cedula,
                CONCAT(if(a003_persona.ind_nombre1 is NULL, '',a003_persona.ind_nombre1 ),' ',if(a003_persona.ind_nombre2 is NULL, '',a003_persona.ind_nombre2 ),' ',if(a003_persona.ind_apellido1 is NULL, '',a003_persona.ind_apellido1 ),' ',if(a003_persona.ind_apellido2 is NULL, '',a003_persona.ind_apellido2 )) AS nombre
             FROM `dn_e003_designacion`
             INNER JOIN a003_persona ON dn_e003_designacion.fk_rhb001_num_empleado=pk_num_persona
             INNER JOIN rh_b001_empleado ON rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona 
             INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado 
             INNER JOIN a004_dependencia ON pk_num_dependencia=fk_a004_num_dependencia 
             WHERE dn_e003_designacion.fk_dnb001_num_denuncia='$idTramite'"
        );
        $Receptores->setFetchMode(PDO::FETCH_ASSOC);

        return $Receptores->fetchAll();
    }


}