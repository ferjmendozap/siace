<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Denuncias
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Guidmar Espinoza     | dtecnica.conmumat@gmail.com          | 0414-1913443
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once RUTA_MODELO . 'municipioModelo.php';
require_once RUTA_MODELO . 'parroquiaModelo.php';
require_once RUTA_MODELO . 'sectorModelo.php';
require_once RUTA_Modulo. 'modDN' . DS . 'modelos'  . DS .'funcionesGeneralesDNModelo.php';
require_once RUTA_Modulo. 'modCD' . DS . 'modelos'  . DS .'dependenciaModelo.php';

class ejecucionModelo extends Modelo
{
    public $atIdUsuario;
    public $atMiscelaneoModelo;
    public $atFuncGnrles;//Funciones generales
    private $atIdContraloria;
    public $idSolicitud;
    public $criterio;
    public $atMunicipioModelo;
    public $atSectorModelo;
    public $atParroquiaModelo;
    public $atDependenciaModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atParroquiaModelo = new parroquiaModelo();
        $this->atMunicipioModelo = new municipioModelo();
        $this->atSectorModelo = new sectorModelo();
        $this->atDependenciaModelo= new dependenciaModelo();
        $this->atFuncGnrles= new funcionesGeneralesDNModelo();
    }

    public function metListarTramites()
    {//Se listan las solicitudes existentes
        $listaTramites =  $this->_db->query(
            "SELECT *, a039_ente.ind_nombre_ente , act.ind_nombre_detalle as tipoActuacion, estado.ind_nombre_detalle as estado, dn_c002_actividad.ind_descripcion as Actividad, dn_c001_fase.ind_descripcion as Fase
             FROM dn_b001_denuncia 
             INNER JOIN dn_d001_actividades ON dn_d001_actividades.fk_dnb001_num_denuncia=dn_b001_denuncia.pk_num_denuncia
             INNER JOIN dn_c002_actividad ON dn_d001_actividades.fk_dnc002_num_actividad=dn_c002_actividad.pk_num_actividad
             INNER JOIN dn_c001_fase ON dn_c002_actividad.fk_dnc001_num_fase=dn_c001_fase.pk_num_fase
             LEFT JOIN a039_ente ON dn_b001_denuncia.fk_a039_num_ente = a039_ente.pk_num_ente 
             LEFT JOIN a006_miscelaneo_detalle as estado ON estado.fk_a005_num_miscelaneo_maestro= (SELECT pk_num_miscelaneo_maestro FROM a005_miscelaneo_maestro WHERE cod_maestro='DN-EST-ACT') and estado.cod_detalle = dn_d001_actividades.ind_estatus 
             LEFT JOIN a006_miscelaneo_detalle as act ON act.pk_num_miscelaneo_detalle = dn_b001_denuncia.fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion 
             WHERE ind_num_denuncia!='' AND dn_d001_actividades.ind_estatus!='TE' AND (dn_b001_denuncia.ind_estatus_tramite='AP' OR dn_b001_denuncia.ind_estatus_tramite='EJ')
             GROUP BY dn_b001_denuncia.ind_num_denuncia
             ORDER BY dn_b001_denuncia.ind_num_denuncia, dn_d001_actividades.num_secuencia"
        );
        $listaTramites->setFetchMode(PDO::FETCH_ASSOC);
        return $listaTramites->fetchAll();
    }

    public function metMostarActividad($idActividad){
        $sql_actividad =  $this->_db->query(
            "SELECT dn_d001_actividades.*, dn_c001_fase.pk_num_fase,
                    dn_c001_fase.ind_descripcion as nombre_fase,
                    dn_c001_fase.cod_fase,
                    dn_c002_actividad.ind_descripcion AS nombre_actividad,
                    dn_c002_actividad.cod_actividad AS cod_act,
                    estado.ind_nombre_detalle as estatus ,
                    dn_c002_actividad.num_flag_auto_archivo,
                    dn_c002_actividad.num_flag_no_afecto_plan
                FROM dn_d001_actividades
                INNER JOIN dn_c002_actividad ON dn_c002_actividad.pk_num_actividad=dn_d001_actividades.fk_dnc002_num_actividad
                INNER JOIN dn_c001_fase ON dn_c002_actividad.fk_dnc001_num_fase=dn_c001_fase.pk_num_fase
                LEFT JOIN a006_miscelaneo_detalle as estado ON estado.fk_a005_num_miscelaneo_maestro= (SELECT pk_num_miscelaneo_maestro FROM a005_miscelaneo_maestro WHERE cod_maestro='DN-EST-ACT') and estado.cod_detalle = dn_d001_actividades.ind_estatus 
                WHERE  dn_d001_actividades.pk_num_detalle_dc=".$idActividad."
                ORDER BY dn_d001_actividades.num_secuencia");
        $sql_actividad->setFetchMode(PDO::FETCH_ASSOC);
        return $sql_actividad->fetch();
    }

    public function metBuscarActividadesTerminadas($idDenuncia)
    {//Se listan las denuncias existentes
        $listaDenuncias =  $this->_db->query(
            "SELECT COUNT(*) AS cantidad
             FROM dn_d001_actividades
             WHERE fk_dnb001_num_denuncia = '$idDenuncia' AND ind_estatus = 'EJ'"
        );
        $listaDenuncias->setFetchMode(PDO::FETCH_ASSOC);
        return $listaDenuncias->fetch();
    }



    public function metTerminarDenuncia($idDenuncia,$fec){
        $this->_db->beginTransaction();

        $sql_denuncia = $this->_db->prepare("
                  UPDATE dn_b001_denuncia
                  SET 
                  fec_termino_real=:fec_termino_real,
                  ind_estatus_tramite=:ind_estatus_tramite,
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                  fec_ultima_modificacion=NOW()
                   WHERE pk_num_denuncia=$idDenuncia
                ");

        $sql_denuncia->execute(array(
            'fec_termino_real'=>$fec,
            'ind_estatus_tramite'=>'TE'
        ));

        $fallaTansaccion = $sql_denuncia->errorInfo();
        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else {
            $this->_db->commit();
            return $idDenuncia;
        }
    }


    public function metEjecutarActividad($params){
        $this->_db->beginTransaction();
        #### Actividades
        $pk=$params['idActividad'];
        $idTramite=$params['idDenuncia'];
        if($params['nuevo_estatus']!='EJ'){
            $num_dias_cierre=$this->atFuncGnrles->metDiferenciaDias($params['fec_inicio_real'], $params['fec_termino_act']);
            if($params['nuevo_estatus']=='TE'){
                $sql_actividades = $this->_db->prepare("
                  UPDATE dn_d001_actividades 
                  SET 
                  fec_registro_termino=NOW(),
                  fec_termino_real=:fec_termino_real,
                  ind_observaciones=:ind_observaciones,
                  ind_estatus=:ind_estatus,
                  num_dias_cierre=:num_dias_cierre,
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                  fec_ultima_modificacion=NOW()
                   WHERE pk_num_detalle_dc=$pk
              ");

              $sql_actividades->execute(array(
                  'fec_termino_real'=>$params['fec_termino_act'],
                  'ind_observaciones'=>$params['ind_observacion'],
                  'ind_estatus'=>'TE',
                  'num_dias_cierre'=>$num_dias_cierre
                ));
                $sql_denuncia = $this->_db->prepare("
                  UPDATE dn_b001_denuncia
                  SET 
                  fec_termino_real=:fec_termino_real,
                  ind_estatus_tramite=:ind_estatus_tramite,
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                  fec_ultima_modificacion=NOW()
                   WHERE pk_num_denuncia=$idTramite
                ");


                $sql_denuncia->execute(array(
                    'fec_termino_real'=>$params['fec_termino_act'],
                    'ind_estatus_tramite'=>'TE'
                ));

            }else{
                $sql_actividades = $this->_db->prepare("
                  UPDATE dn_d001_actividades 
                  SET 
                  fec_registro_termino=NOW(),
                  fec_termino_real=:fec_termino_real,
                  fec_cierre=NOW(),
                  fec_termino_cierre=:fec_termino_cierre,
                  num_dias_cierre=:num_dias_cierre,
                  ind_observaciones=:ind_observaciones,
                  ind_estatus=:ind_estatus,
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                  fec_ultima_modificacion=NOW()
                   WHERE pk_num_detalle_dc=$pk
              ");

                $sql_actividades->execute(array(
                    'fec_termino_real'=>$params['fec_termino_act'],
                    'fec_termino_cierre'=>$params['fec_termino_act'],
                    'num_dias_cierre'=>$num_dias_cierre,
                    'ind_observaciones'=>$params['ind_observacion'],
                    'ind_estatus'=>$params['nuevo_estatus']
                ));

                $idTramite=$params['idDenuncia'];
                if($params['nuevo_estatus']=='VA'){
                    $sql_valoracion = $this->_db->prepare("
                      INSERT INTO dn_e004_valoracion
                      SET 
                        ind_analisis_hechos_expediente=:ind_analisis_hechos_expediente,
                        ind_antecedente_expediente=:ind_antecedente_expediente,
                        ind_conclusion_valoracion=:ind_conclusion_valoracion,
                        ind_manifiesto=:ind_manifiesto,
                        ind_recomendaciones=:ind_recomendaciones,
                        ind_valoracion_expediente=:ind_valoracion_expediente,
                        fec_valoracion=NOW(),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                        fec_ultima_modificacion=NOW()
                    ");

                    $sql_valoracion->execute(array(
                        'ind_analisis_hechos_expediente'=>trim($params['ind_analisis_hechos_expediente']),
                        'ind_antecedente_expediente'=>trim($params['ind_antecedente_expediente']),
                        'ind_conclusion_valoracion'=>trim($params['ind_conclusion_valoracion']),
                        'ind_manifiesto'=>trim($params['ind_manifiesto']),
                        'ind_recomendaciones'=>trim($params['ind_recomendaciones']),
                        'ind_valoracion_expediente'=>trim($params['ind_valoracion_expediente'])
                    ));
                }elseif ($params['nuevo_estatus']=='RM'){
                    $sql_valoracion = $this->_db->prepare("
                      INSERT INTO dn_e004_valoracion
                      SET 
                        ind_analisis_hechos_expediente=:ind_analisis_hechos_expediente,
                        ind_antecedente_expediente=:ind_antecedente_expediente,
                        ind_conclusion_valoracion=:ind_conclusion_valoracion,
                        ind_manifiesto=:ind_manifiesto,
                        ind_recomendaciones=:ind_recomendaciones,
                        ind_valoracion_expediente=:ind_valoracion_expediente,
                        fk_a004_num_dependencia_control=:fk_a004_num_dependencia_control,
                        num_tipo_instancia=:num_tipo_instancia,
                        fec_valoracion=NOW(),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                        fec_ultima_modificacion=NOW()
                    ");

                    $sql_valoracion->execute(array(
                        'ind_analisis_hechos_expediente'=>trim($params['ind_analisis_hechos_expediente']),
                        'ind_antecedente_expediente'=>trim($params['ind_antecedente_expediente']),
                        'ind_conclusion_valoracion'=>trim($params['ind_conclusion_valoracion']),
                        'ind_manifiesto'=>trim($params['ind_manifiesto']),
                        'ind_recomendaciones'=>trim($params['ind_recomendaciones']),
                        'ind_valoracion_expediente'=>trim($params['ind_valoracion_expediente']),
                        'fk_a004_num_dependencia_control'=>$params['instancia'],
                        'num_tipo_instancia'=>$params['tipoInstancia']
                    ));
                    $sql_remitir = $this->_db->prepare("
                      INSERT INTO a042_denuncia_remitida
                      SET 
                        fk_dnb001_num_denuncia=:fk_dnb001_num_denuncia,
                        ind_estatus='PE',
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                        fec_ultima_modificacion=NOW()
                    ");

                    $sql_remitir->execute(array(
                        'fk_dnb001_num_denuncia'=>$idTramite
                    ));

                    $fallaTansaccion = $sql_remitir->errorInfo();
                    $fallaTansaccion2 = $sql_valoracion->errorInfo();
                    if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
                        $this->_db->rollBack();
                        return $fallaTansaccion;
                    }elseif (!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])){
                        $this->_db->rollBack();
                        return $fallaTansaccion2;
                    }else {
                        $idRegistro = $pk;
                        $this->_db->commit();
                        return $idRegistro;
                    }

                }

                /*
                $idTramite=$params['idDenuncia'];
                $sql_denuncia = $this->_db->prepare("
                  UPDATE dn_b001_denuncia
                  SET 
                  fec_termino_real=:fec_termino_real,
                  ind_estatus_tramite=:ind_estatus_tramite,
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                  fec_ultima_modificacion=NOW()
                   WHERE pk_num_denuncia=$idTramite
                ");

                $sql_denuncia->execute(array(
                    'fec_termino_real'=>$params['fec_termino_act'],
                    'ind_estatus_tramite'=>$params['nuevo_estatus']
                ));
                */
            }
        }else{
            $sql_actividades = $this->_db->prepare("
              UPDATE dn_d001_actividades
              SET 
              fec_registro_ejecucion=NOW(),
              fec_inicio_real=:fec_inicio_real,
              ind_observaciones=:ind_observaciones,
              ind_estatus=:ind_estatus,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
               WHERE pk_num_detalle_dc=$pk
              ");
            $sql_actividades->execute(array(
                  'fec_inicio_real'=>$params['fe_ejecucion'],
                  'ind_observaciones'=>$params['ind_observacion'],
                  'ind_estatus'=>$params['nuevo_estatus']
            ));

            if($params['secuencia']==1){
                $sql_denuncia = $this->_db->prepare("
                  UPDATE dn_b001_denuncia
                  SET 
                  fec_inicio=:fec_inicio,
                  ind_estatus_tramite=:ind_estatus_tramite,
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                  fec_ultima_modificacion=NOW()
                   WHERE pk_num_denuncia=$idTramite
                ");

                $sql_denuncia->execute(array(
                    'fec_inicio'=>$params['fe_ejecucion'],
                    'ind_estatus_tramite'=>'EJ'
                ));
            }

        }
        if(isset($sql_denuncia)){
            $fallaTansaccion2 = $sql_denuncia->errorInfo();
            if (!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])){
                $this->_db->rollBack();
                return $fallaTansaccion2;
            }else {
                $idRegistro = $pk;
                $this->_db->commit();
                return $idRegistro;
            }
        }
        $fallaTansaccion = $sql_actividades->errorInfo();
        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else {
            $idRegistro = $pk;
            $this->_db->commit();
            return $idRegistro;
        }
    }

}