<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL MUNICIPIO MATURÍN DEL ESTADO MONAGAS.
 * MODULO: Denuncias
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Guidmar Espinoza     | dtecnica.conmumat@gmail.com          | 0414-1913443
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class scriptCargaModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
    }

    public function metBusquedaSimple($tabla, $innerJoin, $where)
    {
        $busquedaSimple = $this->_db->query("
            SELECT
              *
            FROM
              $tabla
              $innerJoin
            WHERE
              $where
        ");
        $busquedaSimple->setFetchMode(PDO::FETCH_ASSOC);
        return $busquedaSimple->fetch();
    }

    public function metMostrarTipoAct($tipoAct)
    {
        $tipoActuacion = $this->_db->query("
            SELECT
           a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
            FROM
              a006_miscelaneo_detalle
            INNER JOIN a005_miscelaneo_maestro
            ON a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro = a005_miscelaneo_maestro.pk_num_miscelaneo_maestro
            AND a005_miscelaneo_maestro.cod_maestro='TACT'
            WHERE
              a006_miscelaneo_detalle.cod_detalle='$tipoAct'
        ");
        $tipoActuacion->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoActuacion->fetch();
    }
    #### DN
    public function metDNFases($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                dn_c001_fase
              SET
                cod_fase=:cod_fase,
                fk_a006_num_miscelaneo_detalle_tipo_actuacion=:fk_a006_num_miscelaneo_detalle_tipo_actuacion,
                ind_descripcion=:ind_descripcion,
                num_estatus=1,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario
            ");

        $registroDetalle = $this->_db->prepare(
            "INSERT INTO
                dn_c002_actividad
              SET
                cod_actividad=:cod_actividad,
                ind_descripcion=:ind_descripcion,
                ind_comentarios=:ind_comentarios,
                num_duracion=:num_duracion,
                num_flag_auto_archivo=:num_flag_auto_archivo,
                num_flag_no_afecto_plan=:num_flag_no_afecto_plan,
                num_estatus=:num_estatus,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fk_dnc001_num_fase=:fk_dnc001_num_fase,
                ind_tipo_actuacion=:ind_tipo_actuacion
            
            ");


        foreach ($arrays as $array) {
            $idTipoActuacion = $this->metMostrarTipoAct($array['tipo_actuacion']);
            $busqueda = $this->metBusquedaSimple('dn_c001_fase', false, "cod_fase = '" . $array['cod_fase'] . "' and fk_a006_num_miscelaneo_detalle_tipo_actuacion='".$idTipoActuacion['pk_num_miscelaneo_detalle']."'");
            if (!$busqueda) {
                $registro->execute(array(
                    'fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    'cod_fase'=>$array['cod_fase'],
                    'fk_a006_num_miscelaneo_detalle_tipo_actuacion'=>$idTipoActuacion['pk_num_miscelaneo_detalle'],
                    'ind_descripcion'=>$array['ind_descripcion']
                ));
                $idFase = $this->_db->lastInsertId();

                if (isset($array['Det'])) {
                    foreach ($array['Det'] as $arrayDet) {

                        $registroDetalle->execute(array(
                            'cod_actividad'=>$arrayDet['cod_actividad'],
                            'ind_descripcion'=>$arrayDet['ind_descripcion'],
                            'ind_comentarios'=>$arrayDet['ind_comentarios'],
                            'num_duracion'=>$arrayDet['num_duracion'],
                            'num_flag_auto_archivo'=>$arrayDet['num_flag_auto_archivo'],
                            'num_flag_no_afecto_plan'=>$arrayDet['num_flag_no_afecto_plan'],
                            'num_estatus'=>$arrayDet['num_estatus'],
                            'fk_a018_num_seguridad_usuario'=>$arrayDet['fk_a018_num_seguridad_usuario'],
                            'fk_dnc001_num_fase'=>$idFase,
                            'ind_tipo_actuacion'=>$idTipoActuacion['pk_num_miscelaneo_detalle']
                        ));
                    }
                }
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }
}
