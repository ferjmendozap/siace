<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Denuncias
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Guidmar Espinoza     | dtecnica.conmumat@gmail.com          | 0414-1913443
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/


require_once RUTA_Modulo. 'modDN' . DS . 'modelos'  . DS .'funcionesGeneralesDNModelo.php';
require_once RUTA_Modulo. 'modCD' . DS . 'modelos'  . DS .'dependenciaModelo.php';

class prorrogaModelo extends Modelo
{
    public $atIdUsuario;
    public $atFuncGnrles;//Funciones generales
    private $atIdContraloria;
    public $idSolicitud;
    public $criterio;
    public $atDependenciaModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atDependenciaModelo= new dependenciaModelo();
        $this->atFuncGnrles= new funcionesGeneralesDNModelo();
    }

    public function metListarTramitesEnEjecucion()
    {//Se listan las solicitudes existentes
        $listaTramites =  $this->_db->query(
            "SELECT *, a039_ente.ind_nombre_ente , act.ind_nombre_detalle as tipoActuacion, estado.ind_nombre_detalle as estado, dn_c002_actividad.ind_descripcion as Actividad, dn_c001_fase.ind_descripcion as Fase
             FROM dn_b001_denuncia 
             INNER JOIN dn_d001_actividades ON dn_d001_actividades.fk_dnb001_num_denuncia=dn_b001_denuncia.pk_num_denuncia
             INNER JOIN dn_c002_actividad ON dn_d001_actividades.fk_dnc002_num_actividad=dn_c002_actividad.pk_num_actividad
             INNER JOIN dn_c001_fase ON dn_c002_actividad.fk_dnc001_num_fase=dn_c001_fase.pk_num_fase
             LEFT JOIN a039_ente ON dn_b001_denuncia.fk_a039_num_ente = a039_ente.pk_num_ente 
             LEFT JOIN a006_miscelaneo_detalle as estado ON estado.fk_a005_num_miscelaneo_maestro= (SELECT pk_num_miscelaneo_maestro FROM a005_miscelaneo_maestro WHERE cod_maestro='DN-EST-ACT') and estado.cod_detalle = dn_d001_actividades.ind_estatus 
             LEFT JOIN a006_miscelaneo_detalle as act ON act.pk_num_miscelaneo_detalle = dn_b001_denuncia.fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion 
             WHERE ind_num_denuncia!='' AND dn_d001_actividades.ind_estatus='EJ' AND dn_b001_denuncia.ind_estatus_tramite='EJ'
             GROUP BY dn_b001_denuncia.ind_num_denuncia
             ORDER BY dn_b001_denuncia.ind_num_denuncia, dn_d001_actividades.num_secuencia"
        );
        $listaTramites->setFetchMode(PDO::FETCH_ASSOC);
        return $listaTramites->fetchAll();
    }

    public function metMostrarProrroga($idProrroga){
        $sql_prorroga =  $this->_db->query(
            "SELECT * , estado.ind_nombre_detalle as estatus
                FROM dn_b002_prorroga
                LEFT JOIN a006_miscelaneo_detalle as estado
                 ON estado.fk_a005_num_miscelaneo_maestro= 
                 (SELECT pk_num_miscelaneo_maestro FROM a005_miscelaneo_maestro WHERE cod_maestro='DN-EST-PRO') 
                 and estado.cod_detalle = dn_b002_prorroga.ind_estatus 
                WHERE  pk_num_prorroga_detalle=".$idProrroga."
                ");
        $sql_prorroga->setFetchMode(PDO::FETCH_ASSOC);
        return $sql_prorroga->fetch();
    }

    public function metAcumulado($idDenuncia){
        $sql_acumulado =  $this->_db->query(
            "SELECT sum(num_prorroga) as acm
                FROM dn_b002_prorroga
                WHERE  fk_dnb001_num_denuncia=".$idDenuncia." 
                and ind_estatus='AP'
                ");
        $sql_acumulado->setFetchMode(PDO::FETCH_ASSOC);
        return $sql_acumulado->fetch();
    }

    public function metAcumuladoActividad($idActividad){
        $sql_acumulado =  $this->_db->query(
            "SELECT num_prorroga
                FROM dn_d001_actividades
                WHERE pk_num_detalle_dc=$idActividad;
                ");
        $sql_acumulado->setFetchMode(PDO::FETCH_ASSOC);
        return $sql_acumulado->fetch();
    }

    public function metListarProrrogasTramite($idDenuncia){
        $sql_prorroga =  $this->_db->query(
            "SELECT * , estado.ind_nombre_detalle as estatus, dn_c002_actividad.ind_descripcion as Actividad,
             dn_c001_fase.ind_descripcion as Fase ,
              dn_b002_prorroga.num_secuencia as secuencia, dn_b002_prorroga.num_prorroga as dias
             FROM dn_b002_prorroga 
             INNER JOIN dn_d001_actividades ON dn_d001_actividades.fk_dnb001_num_denuncia=dn_b002_prorroga.fk_dnb001_num_denuncia 
                 and dn_d001_actividades.pk_num_detalle_dc=dn_b002_prorroga.fk_dnd001_num_detalle_dc 
             INNER JOIN dn_c002_actividad ON dn_d001_actividades.fk_dnc002_num_actividad=dn_c002_actividad.pk_num_actividad 
             INNER JOIN dn_c001_fase ON dn_c002_actividad.fk_dnc001_num_fase=dn_c001_fase.pk_num_fase 
             LEFT JOIN a006_miscelaneo_detalle as estado 
                 ON estado.fk_a005_num_miscelaneo_maestro= 
                 (SELECT pk_num_miscelaneo_maestro FROM a005_miscelaneo_maestro WHERE cod_maestro='DN-EST-PRO') 
                 and estado.cod_detalle = dn_b002_prorroga.ind_estatus 
             WHERE dn_b002_prorroga.fk_dnb001_num_denuncia=".$idDenuncia."
             ORDER BY dn_b002_prorroga.num_secuencia");
        $sql_prorroga->setFetchMode(PDO::FETCH_ASSOC);
        return $sql_prorroga->fetchAll();
    }

    public function metMaxProrroga($idActividad)
    {
        $sql_Max =  $this->_db->query(
            "SELECT num_secuencia as secuencia, pk_num_prorroga_detalle, ind_estatus as estadoPro
              FROM dn_b002_prorroga 
              WHERE fk_dnd001_num_detalle_dc=".$idActividad." 
              ORDER BY num_secuencia DESC LIMIT 1 
                ");
        $sql_Max->setFetchMode(PDO::FETCH_ASSOC);
        return $sql_Max->fetch();
    }

    public function metPlanificacion($idTramite){
        $sql_query="SELECT dn_d001_actividades.*,dn_c001_fase.pk_num_fase,
                    dn_c001_fase.ind_descripcion as nombre_fase,
                    dn_c001_fase.cod_fase,
                    dn_c002_actividad.ind_descripcion AS nombre_actividad,
                    dn_c002_actividad.cod_actividad AS cod_act,
                    dn_c002_actividad.num_flag_auto_archivo,
                    dn_c002_actividad.num_flag_no_afecto_plan,
                    (SELECT sum(num_prorroga) FROM dn_b002_prorroga WHERE fk_dnd001_num_detalle_dc=dn_d001_actividades.pk_num_detalle_dc and ind_estatus!='AN') as num_prorroga
                FROM dn_d001_actividades
                INNER JOIN dn_c002_actividad ON dn_c002_actividad.pk_num_actividad=dn_d001_actividades.fk_dnc002_num_actividad
                INNER JOIN dn_c001_fase ON dn_c002_actividad.fk_dnc001_num_fase=dn_c001_fase.pk_num_fase AND dn_c001_fase.fk_a006_num_miscelaneo_detalle_tipo_actuacion=
                (SELECT pk_num_miscelaneo_detalle FROM a006_miscelaneo_detalle WHERE fk_a005_num_miscelaneo_maestro=(SELECT pk_num_miscelaneo_maestro FROM a005_miscelaneo_maestro WHERE cod_maestro='TACT') 
                and a006_miscelaneo_detalle.pk_num_miscelaneo_detalle=dn_c001_fase.fk_a006_num_miscelaneo_detalle_tipo_actuacion)
                WHERE dn_c002_actividad.num_estatus=1 and dn_d001_actividades.fk_dnb001_num_denuncia=".$idTramite."
                ORDER BY cod_fase, dn_c002_actividad.cod_actividad";
        $result=$this->_db->query($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }

    public function metCrearProrroga($params)
    {# Permite registrar una Solicitud

        $this->_db->beginTransaction();

        if($params['accion']=='nuevo'){
            $estatus ='PR';
        }elseif($params['accion']=='revisar') {
            $estatus ='RV';
        }elseif($params['accion']=='conformar'){
            $estatus ='CO';
        }elseif($params['accion']=='aprobar'){
            $estatus ='AP';
        }else{
            $estatus ='AN';
        }
        $y=date('Y');

        $sql_secuencia = $this->_db->query("
              SELECT max(num_secuencia) as sec
              FROM dn_b002_prorroga 
              WHERE  fk_dnd001_num_detalle_dc =".$params['idActividad']."
              and fk_dnb001_num_denuncia=".$params['idDenuncia']);
        $sql_secuencia->setFetchMode(PDO::FETCH_ASSOC);
        $sec= $sql_secuencia->fetch();

        #### Prorroga
        if($params['accion']=='nuevo') {
            if($sec['sec']=='NULL'){
                $secuencia=1;
            }else{
                $secuencia=$sec['sec']+1;
            }
            $sql_prorroga = $this->_db->prepare("
              INSERT INTO
                dn_b002_prorroga
              SET
                  fec_anio='$y',
                  num_secuencia=:num_secuencia,
                  num_prorroga=:num_prorroga,
                  ind_motivo=:ind_motivo,
                  fec_preparacion=NOW(),
                  ind_estatus='$estatus',
                  fk_dnb001_num_denuncia=:fk_dnb001_num_denuncia,
                  fk_rhb001_num_empleado_elabora='$this->atIdUsuario',
                  fk_dnd001_num_detalle_dc=:fk_dnd001_num_detalle_dc,                  
                  fec_ultima_modificacion=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              ");

            $sql_prorroga->execute(array(
                'num_secuencia' => $secuencia,
                'num_prorroga' => $params['diasSolicitados'],
                'ind_motivo' => trim($params['ind_justificacion']),
                'fk_dnb001_num_denuncia' => $params['idDenuncia'],
                'fk_dnd001_num_detalle_dc' => $params['idActividad']
            ));
            $id = $this->_db->lastInsertId();
        }elseif($params['accion']=='modificar') {
            if($sec['sec']=='NULL'){
                $secuencia=1;
            }else{
                $secuencia=$sec['sec']+1;
            }
            $sql_prorroga = $this->_db->prepare("
              UPDATE
                dn_b002_prorroga
              SET
                  num_prorroga=:num_prorroga,
                  ind_motivo=:ind_motivo,              
                  fec_ultima_modificacion=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              WHERE 
                  pk_num_prorroga_detalle=".$params['idProrroga']);

            $sql_prorroga->execute(array(
                'num_prorroga' => $params['diasSolicitados'],
                'ind_motivo' => trim($params['ind_justificacion'])
            ));
            $id=$params['idProrroga'];
        }elseif($params['accion']=='anular') {
            if ($params['estatus']=='RV'){
            $es='PR';
            }else{
                $es='AN';
            }
            $sql_prorroga = $this->_db->prepare("
              UPDATE
                dn_b002_prorroga
              SET
                  ind_estatus=:ind_estatus,   
                  fec_ultima_modificacion=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              WHERE 
                  pk_num_prorroga_detalle=".$params['idProrroga']);

            $sql_prorroga->execute(array(
                'ind_estatus' => $es
            ));
            $id=$params['idProrroga'];
        }elseif($params['accion']=='revisar' or $params['accion']=='conformar') {
            $sql_prorroga = $this->_db->prepare("
              UPDATE
                dn_b002_prorroga
              SET
                  ind_estatus=:ind_estatus,    
                  fec_ultima_modificacion=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              WHERE 
                  pk_num_prorroga_detalle=".$params['idProrroga']);

            $sql_prorroga->execute(array(
                'ind_estatus' => $estatus
            ));
            $id=$params['idProrroga'];
        }elseif($params['accion']=='aprobar') {
            $sql_prorroga = $this->_db->prepare("
              UPDATE
                dn_b002_prorroga
              SET
                  ind_estatus=:ind_estatus,    
                  fec_ultima_modificacion=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              WHERE 
                  pk_num_prorroga_detalle=" . $params['idProrroga']);

            $sql_prorroga->execute(array(
                'ind_estatus' => $estatus
            ));
            $ap=$this->metAcumuladoActividad($params['idActividad']);
            $d=$params['diasSolicitados'];
            $acumulado=$ap['num_prorroga']+$d;
            $sql_prorroga2 = $this->_db->prepare("
              UPDATE
                dn_d001_actividades
              SET
                  num_prorroga=:num_prorroga,    
                  fec_ultima_modificacion=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              WHERE 
                  pk_num_detalle_dc=".$params['idActividad']);

            $sql_prorroga2->execute(array(
                'num_prorroga' => $acumulado
            ));

            $id=$params['idProrroga'];
        }

            if(isset($sql_prorroga)){
        $fallaTansaccion = $sql_prorroga->errorInfo();
        }

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else {
            $idRegistro = $id;
            $this->_db->commit();
            return $idRegistro;
        }

    }
}