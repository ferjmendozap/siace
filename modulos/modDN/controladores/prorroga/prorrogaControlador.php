<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Denuncias
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Guidmar Espinoza     | dtecnica.conmumat@gmail.com          | 0414-1913443
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class prorrogaControlador extends Controlador
{
    private $atSolicitudModelo;
    private $atMiscelaneoModelo;
    private $atDependenciaModelo;
    private $atFunGn;
    private $atProrrogaModelo;
    private $atDenunciasModelo;
    private $atIdUsuario;
    private $atEjecucionModelo;

    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atProrrogaModelo = $this->metCargarModelo('prorroga', 'prorroga');
        $this->atDenunciasModelo = $this->metCargarModelo('denuncias', 'denuncias');
        $this->atSolicitudModelo = $this->metCargarModelo('solicitud', 'solicitud');
        $this->atEjecucionModelo = $this->metCargarModelo('ejecucion', 'ejecucion');
        $this->atFunGn = $this->metCargarModelo('funcionesGeneralesDN');
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('prorrogaPost', 1);
        $this->atVista->metRenderizar('listadoProrrogas');
    }

    public function metCrearModificar()
    {
            $complementosJs = array(
                'jquery-validation/dist/jquery.validate.min',
                'jquery-validation/dist/additional-methods.min',
                'wizard/jquery.bootstrap.wizard.min',
                'bootstrap-datepicker/bootstrap-datepicker',
                'select2/select2.min',
            );
            $complementoCss = array(
                'bootstrap-datepicker/datepicker',
                'select2/select201ef',
                'wizard/wizardfa6c'
            );
            $js = array('Aplicacion/appFunciones',
                'modRH/modRHFunciones',
                'materialSiace/core/demo/DemoFormWizard');
            $this->atVista->metCargarCssComplemento($complementoCss);
            $this->atVista->metCargarJsComplemento($complementosJs);
            $this->atVista->metCargarJs($js);

            $valido = $this->metObtenerInt('valido');
            $idActividad = $this->metObtenerInt('idActividad');
            $idProrroga = $this->metObtenerInt('idProrroga');
            $ver = $this->metObtenerInt('ver');
            $idDenuncia = $this->metObtenerInt('idDenuncia');

            $accion = $this->metObtenerTexto('accion');

        if ($valido == 1) {

                $ind = $this->metValidarFormArrayDatos('form', 'int',  array('idProrroga'));
                $texto = $this->metValidarFormArrayDatos('form', 'txt');
                $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');

                if ($alphaNum != null && $ind == null && $texto == null) {
                    $validacion = $alphaNum;
                } elseif ($alphaNum == null && $ind != null && $texto == null) {
                    $validacion = $ind;
                } elseif ($alphaNum == null && $ind == null && $texto != null) {
                    $validacion = $texto;
                } elseif ($alphaNum == null && $ind != null && $texto != null) {
                    $validacion = array_merge($texto, $ind);
                } elseif ($alphaNum != null && $ind == null && $texto != null) {
                    $validacion = array_merge($texto, $alphaNum);
                } elseif ($alphaNum != null && $ind != null && $texto == null) {
                    $validacion = array_merge($ind, $alphaNum);
                } else {
                    $validacion = array_merge($alphaNum, $ind, $texto);
                }
                if (in_array('error', $validacion)) {
                    $validacion['status'] = 'error';
                    echo json_encode($validacion);
                    exit;
                }

                if (!isset($validacion['ind_estatus'])) {
                    $validacion['ind_estatus'] = 'PE';
                }
                    $validacion['accion'] = $accion;
                    $id = $this->atProrrogaModelo->metCrearProrroga($validacion);
                    //$validacion['status'] = $validacion['accion'];

                if (is_array($id)) {
                    foreach ($validacion as $titulo => $valor) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                    $validacion['status'] = 'errorSQL';
                    echo json_encode($validacion);
                    exit;
                }

                $validacion['idProrroga'] = $id;

                echo json_encode($validacion);
                exit;
            }

        if ($idProrroga != 0) {
            $this->atVista->assign('formP', $this->atProrrogaModelo->metMostrarProrroga($idProrroga));

            $this->atVista->assign('idProrroga', $idProrroga);
            $this->atVista->assign('ver', $ver);
        }

        $this->atVista->assign('acumulado', $this->atProrrogaModelo->metAcumulado($idDenuncia));
        $result=$this->atProrrogaModelo->metPlanificacion($idDenuncia);
        $fec_inicio_real='';
        if(count($result)>0) {
            foreach ($result as $fila) {
                if($fila['fec_inicio_real']=='' or $fila['fec_inicio_real']=='00-00-0000' or $fila['fec_inicio_real']=='0000-00-00'){
                    if($fec_inicio_real==''){
                        $fec_inicio_real=$fila['fec_inicio'];
                    }else{
                        $fec_inicio_real=$fec_inicio_real;
                    }

                }else{
                    $fec_inicio_real=$fila['fec_inicio_real'];
                }
                $fila['fec_inicio'] = $this->atFunGn->metFormateaFecha($fila['fec_inicio']);

                $duracionT=$fila['num_duracion'] + $fila['num_prorroga'];

                    $fecha_fin_actividad=$this->atFunGn->metSumarDiasHabiles($fec_inicio_real, (int) $duracionT);

                $fila['fec_inicio_real'] = $this->atFunGn->metFormateaFecha($fec_inicio_real);
                $fila['fec_termino'] = $this->atFunGn->metFormateaFecha($fila['fec_termino']);
                if($fila['fec_termino_real']=='0000-00-00' or $fila['fec_termino_real']=='' or $fila['fec_termino_real']=='00-00-0000'){
                    $fila['fec_termino_real'] = $this->atFunGn->metFormateaFecha($fecha_fin_actividad);
                    $fec_inicio_real=$this->atFunGn->metSumarDiasHabiles($fecha_fin_actividad, 2);
                } else{
                    $fila['fec_termino_real'] = $this->atFunGn->metFormateaFecha($fila['fec_termino_real']);
                    $fec_inicio_real=$this->atFunGn->metSumarDiasHabiles($fila['fec_termino_real'], 2);
                }
                $fila['num_flag_no_afecto_plan'] =$fila['num_flag_no_afecto_plan'];
                $fila['num_duracion'] =$fila['num_duracion'];
                $fila['num_prorroga'] =$fila['num_prorroga'];
                $fila['num_flag_auto_archivo'] =$fila['num_flag_auto_archivo'];
                $fila['pk_num_detalle_dc'] =$fila['pk_num_detalle_dc'];
                $fila['ind_estatus'] =$fila['ind_estatus'];
                $arrData[]=$fila;
            }
        }

        $arrData=$this->atFunGn->metAgrupaArray($arrData,'nombre_fase','cod_fase');
        $this->atVista->assign('listaActividades', $arrData);
        $this->atVista->assign('idDenuncia', $idDenuncia);
        $this->atVista->assign('actividad', $this->atEjecucionModelo->metMostarActividad($idActividad));
        $actividad=$this->atEjecucionModelo->metMostarActividad($idActividad);

        $this->atVista->assign('selectDependenciaInt', $this->atSolicitudModelo->atDependenciaModelo->metListarTipoDependenciaInt());
        $this->atVista->assign('selectDependenciaExt', $this->atSolicitudModelo->atDependenciaModelo->metListarTipoDependencia());

        if($actividad['fec_inicio_real']!='0000-00-00'){
            $num_dias_cierre=$this->atFunGn->metDiferenciaDias($actividad['fec_inicio_real'], date('Y-m-d'));
            $this->atVista->assign('dias_cierre', $num_dias_cierre);
        }else{
            $this->atVista->assign('dias_cierre', 0);
        }

        if($actividad['fec_termino_real']=='0000-00-00'){
            $fec_termino_real=$this->atFunGn->metSumarDiasHabiles($actividad['fec_inicio_real'], $actividad['num_duracion']);
            $this->atVista->assign('fec_termino_real', $fec_termino_real);
        }else{
            $this->atVista->assign('fec_termino_real', $actividad['fec_termino_real']);
        }
        $tipoActuacion = $this->metObtenerTexto('tipoActuacion');
        $pk_actuacion = $this->atFunGn->metMiscelaneo('TACT', ' and a006_miscelaneo_detalle.cod_detalle="'.$tipoActuacion.'"');
        $this->atVista->assign('tramite', $pk_actuacion['ind_nombre_detalle']);
        $this->atVista->assign('accion', $accion);
        $this->atVista->assign('selectEnte', $this->atFunGn->metEnteSujetoControl());
        $this->atVista->assign('formDB', $this->atSolicitudModelo->metMostrarSolicitud($idDenuncia));
        $this->atVista->assign('sesion', $this->atSolicitudModelo->atIdUsuario);
        $this->atVista->assign('listaProrroga', $this->atProrrogaModelo->metListarProrrogasTramite($idDenuncia));
        $this->atVista->metRenderizar('prorroga', 'modales');
    }

    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #capturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "
             SELECT *, 
                 dn_d001_actividades.ind_estatus as estadoAct,
                 a039_ente.ind_nombre_ente , 
                 act.ind_nombre_detalle as tipoActuacion,
                 act.cod_detalle as tipoAct, 
                 estado.ind_nombre_detalle as estado, 
                 dn_c002_actividad.ind_descripcion as Actividad, 
                 dn_c001_fase.ind_descripcion as Fase,
                 CONCAT(dn_c001_fase.ind_descripcion,'-',dn_c002_actividad.ind_descripcion) as FA,
                 Prorroga.pk_num_prorroga_detalle,
                 Prorroga.num_secuencia as secuenciaPro,
                 Prorroga.ind_estatus as estadoProrroga,
                 Prorroga.pk_num_prorroga_detalle as pk_num_prorroga
             FROM dn_b001_denuncia 
             INNER JOIN dn_d001_actividades 
                  ON dn_d001_actividades.fk_dnb001_num_denuncia=dn_b001_denuncia.pk_num_denuncia
             INNER JOIN dn_c002_actividad 
                  ON dn_d001_actividades.fk_dnc002_num_actividad=dn_c002_actividad.pk_num_actividad
             INNER JOIN dn_c001_fase 
                  ON dn_c002_actividad.fk_dnc001_num_fase=dn_c001_fase.pk_num_fase
             LEFT JOIN a039_ente 
                  ON dn_b001_denuncia.fk_a039_num_ente = a039_ente.pk_num_ente 
             LEFT JOIN a006_miscelaneo_detalle as estado 
                  ON estado.fk_a005_num_miscelaneo_maestro= 
                  (SELECT pk_num_miscelaneo_maestro FROM a005_miscelaneo_maestro WHERE cod_maestro='DN-EST-ACT') and estado.cod_detalle = dn_d001_actividades.ind_estatus 
             LEFT JOIN a006_miscelaneo_detalle as act 
                  ON act.pk_num_miscelaneo_detalle = dn_b001_denuncia.fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion
             LEFT JOIN dn_b002_prorroga as Prorroga 
                  ON
                   Prorroga.fk_dnd001_num_detalle_dc=dn_d001_actividades.pk_num_detalle_dc
                   and Prorroga.fk_dnb001_num_denuncia=dn_d001_actividades.fk_dnb001_num_denuncia
                   and Prorroga.num_secuencia=
                   (SELECT MAX(num_secuencia) FROM dn_b002_prorroga
                   WHERE dn_b002_prorroga.fk_dnb001_num_denuncia=dn_d001_actividades.fk_dnb001_num_denuncia
                   AND dn_b002_prorroga.fk_dnd001_num_detalle_dc=dn_d001_actividades.pk_num_detalle_dc)
 					 
             WHERE ind_num_denuncia!='' 
                  AND dn_d001_actividades.ind_estatus='EJ' 
                  AND dn_b001_denuncia.ind_estatus_tramite='EJ'
                ";

        if ($busqueda['value']) {
            #concateno la busqueda si existe, Nota: esto es obligatorio
            $sql .="
                AND 
                    ( 
                      dn_c002_actividad.ind_descripcion LIKE '%$busqueda[value]%' OR 
                      act.ind_nombre_detalle LIKE '%$busqueda[value]%' OR 
                      a039_ente.ind_nombre_ente LIKE '%$busqueda[value]%' OR
                      dn_c001_fase.ind_descripcion LIKE '%$busqueda[value]%'
                    )
            ";
        }

        $sql .="  GROUP BY Prorroga.fk_dnd001_num_detalle_dc ";

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('ind_num_denuncia','tipoActuacion', 'ind_nombre_ente', 'FA');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_prorroga_detalle';
        #construyo el listado de botones
        $camposExtra = array('pk_num_denuncia','tipoAct','pk_num_detalle_dc', 'estadoProrroga','pk_num_prorroga');

        if (in_array('DN-01-03-01-N',$rol)) {
            $campos['boton']['Nueva'] = array("
                <button accion='nueva' title='Crear Prorroga'
                        class='nueva logsUsuario btn ink-reaction btn-raised btn-xs btn-info'
                        descipcion='El Usuario ha creado una nueva la Prorroga Nro.pk_num_detalle_dc del tramite pk_num_denuncia'
                        idActividad='pk_num_detalle_dc' titulo='Crear Prorroga'
                        idDenuncia='pk_num_denuncia'
                        tActuacion='tipoAct'
                        estadoPro='N'
                        data-toggle='modal' data-target='#formModal' 
                        id='nueva'>
                    <i class='md md-create'></i>
                </button>
                ",
                'if( $i["estadoProrroga"] =="AP" OR $i["estadoProrroga"] =="AN" OR $i["estadoProrroga"] =="NULL" OR $i["estadoProrroga"] =="") { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Nueva'] = false;
        }

        if (in_array('DN-01-03-02-M',$rol)) {
            $campos['boton']['Modificar'] = array("
                <button accion='modificar' title='Modificar Prorroga'
                        class='modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        descipcion='El Usuario ha modificado la Prorroga Nro. pk_num_detalle_dc del tramite pk_num_denuncia'
                        idActividad='pk_num_detalle_dc' titulo='Modificar Prorroga'
                        idDenuncia='pk_num_denuncia'
                        idProrroga='pk_num_prorroga'
                        estadoPro='estadoProrroga'
                        tActuacion='tipoAct'
                        data-toggle='modal' data-target='#formModal' id='modificar'>
                    <i class='fa fa-edit'></i>
                </button>
                ",
                'if( $i["estadoProrroga"] =="PR" ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Modificar'] = false;
        }

        if (in_array('DN-01-03-03-R',$rol)) {
            $campos['boton']['Revisar'] = array("
                <button accion='revisar' title='Revisar Prorroga'
                        class='revisar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        descipcion='El Usuario ha Revisado la Prorroga Nro. pk_num_detalle_dc del tramite pk_num_denuncia'
                        idActividad='pk_num_detalle_dc' titulo='Revisar Prorroga'
                        idDenuncia='pk_num_denuncia'
                        idProrroga='pk_num_prorroga'
                        estadoPro='estadoProrroga'
                        tActuacion='tipoAct'
                        data-toggle='modal' data-target='#formModal' id='revisar'>
                    <i class='icm icm-rating'></i>
                </button>
                ",
                'if( $i["estadoProrroga"] =="PR" ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Revisar'] = false;
        }

        if (in_array('DN-01-03-04-C',$rol)) {
            $campos['boton']['Conformar'] = array("
                <button accion='conformar' title='Conformar Prorroga'
                        class='conformar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        descipcion='El Usuario ha Conformar la Prorroga Nro.pk_num_detalle_dc del tramite pk_num_denuncia'
                        idActividad='pk_num_detalle_dc' titulo='Conformar Prorroga'
                        idDenuncia='pk_num_denuncia'
                        idProrroga='pk_num_prorroga'
                        estadoPro='estadoProrroga'
                        tActuacion='tipoAct'
                        data-toggle='modal' data-target='#formModal' id='conformar'>
                    <i class='icm icm-rating2'></i>
                </button>
                ",
                'if( $i["estadoProrroga"] =="RV" ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Conformar'] = false;
        }

        if (in_array('DN-01-03-05-A',$rol)) {
            $campos['boton']['Aprobar'] = array("
                <button accion='Aprobar' title='Aprobar Prorroga'
                        class='aprobar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        descipcion='El Usuario ha Modificado la Prorroga Nro. pk_num_detalle_dc del tramite pk_num_denuncia'
                        idActividad='pk_num_detalle_dc' titulo='Aprobar Prorroga'
                        idDenuncia='pk_num_denuncia'
                        idProrroga='pk_num_prorroga'
                        estadoPro='estadoProrroga'
                        tActuacion='tipoAct'
                        data-toggle='modal' data-target='#formModal' id='aprobar'>
                    <i class='icm icm-rating3'></i>
                </button>
                ",
                'if( $i["estadoProrroga"] =="CO" ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Aprobar'] = false;
        }
        
        if (in_array('DN-01-03-06-V',$rol)) {
            $campos['boton']['Ver'] = "
            <button accion='ver' title='Consultar Prorrogas'
                    class='ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning'
                    descipcion='El Usuario ha Consultado la Prorroga Nro. pk_num_detalle_dc del tramite pk_num_denuncia'
                    idActividad='pk_num_detalle_dc' titulo='Consultar Prorroga'
                    idDenuncia='pk_num_denuncia'
                    idProrroga='pk_num_prorroga'
                    estadoPro='estadoProrroga'
                    tActuacion='tipoAct'
                    data-toggle='modal' data-target='#formModal' id='ver'>
                <i class='icm icm-eye2'></i>
            </button>
            ";
        } else {
            $campos['boton']['Ver'] = false;
        }




        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,$camposExtra);

    }

}