<?php
class reportesControlador extends Controlador
{
    public $atReporte;
    public $atSolicitudModelo;
    public $atFnGnModelo;
    public $atDenunciasModelo;
    private $atFPDF;

    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        $this->atReporte = $this->metCargarModelo('reportes', 'reportes');
        $this->atSolicitudModelo = $this->metCargarModelo('solicitud', 'solicitud');
        $this->atFnGnModelo = $this->metCargarModelo('funcionesGeneralesDN');
        $this->atDenunciasModelo = $this->metCargarModelo('denuncias', 'denuncias');
        Session::metAcceso();
        #se carga la Libreria.
        $this->metObtenerLibreria('headerReportes','modDN');
        $this->atFPDF = new pdf('L','mm','letter');
    }

    public function metIndex()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $estadoContraloria = $this->atFnGnModelo->metParametros('DEFAULTESTADO');
        $this->atVista->assign('ente',$this->atFnGnModelo->metEnteSujetoControl());
        $this->atVista->assign('tramite',$this->atSolicitudModelo->atMiscelaneoModelo->metMostrarSelect('TACT'));
        $this->atVista->assign('estadoSolicitud',$this->atSolicitudModelo->atMiscelaneoModelo->metMostrarSelect('DN-EST-SOL'));
        $this->atVista->assign('estadoTramite',$this->atSolicitudModelo->atMiscelaneoModelo->metMostrarSelect('DN-EST-TRA'));
        $this->atVista->assign('tipoDenuncia',$this->atSolicitudModelo->atMiscelaneoModelo->metMostrarSelect('DN-01-03'));
        $this->atVista->assign('origen',$this->atSolicitudModelo->atMiscelaneoModelo->metMostrarSelect('DN-01-02'));
        $this->atVista->assign('listadoMunicipio', $this->atSolicitudModelo->atMunicipioModelo->metJsonMunicipio($estadoContraloria['ind_valor_parametro']));
        $this->atVista->metRenderizar('reporteSolicitudes');
    }

    public function metGenerarReporte($municipio,$parroquia,$sector,$tramite,$estadoSolicitud,$estadoTramite,$origen,$tipoDenuncia,$desde,$hasta)
    {
        $filtro = "";
        if ($municipio!='no') {
            $filtro .= " AND a011_municipio.pk_num_municipio ='$municipio' ";
        }
        if ($parroquia!='no') {
            $filtro .= " AND a013_sector.fk_a012_num_parroquia ='$parroquia' ";
        }
        if ($sector!='no') {
            $filtro .= " AND dn_b001_denuncia.fk_a013_num_sector ='$sector' ";
        }
        if ($tramite!='no') {
            $filtro .= " AND dn_b001_denuncia.fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion ='$tramite' ";
        }
        if ($estadoSolicitud!='no') {
            $filtro .= " AND dn_b001_denuncia.ind_estatus_solicitud ='$estadoSolicitud' ";
        }
        if ($estadoTramite!='no') {
            $filtro .= " AND dn_b001_denuncia.ind_estatus_tramite ='$estadoTramite' ";
        }
        if ($origen!='no') {
            $filtro .= " AND dn_b001_denuncia.fk_a006_pk_num_miscelaneo_detalle_origen ='$origen' ";
        }
        if ($tipoDenuncia!='no') {
            $filtro .= " AND dn_b001_denuncia.fk_a006_pk_num_miscelaneo_detalle_tipo_denuncia ='$tipoDenuncia' ";
        }
        if ($desde!='no') {
            $filtro .= " AND dn_b001_denuncia.fec_inicio >='$desde' ";
        }
        if ($hasta!='no') {
            $filtro .= " AND dn_b001_denuncia.fec_inicio <='$hasta' ";
        }


        $solicitudes = $this->atReporte->metListarSolicitudesReporte($filtro);
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 1, 1);
        $this->atFPDF->SetAutoPageBreak(5, 35);
        $this->atFPDF->AddPage();
        $i=0;
        foreach ($solicitudes as $p) {
            if($i==0){
                $this->atFPDF->SetFillColor(255, 255, 255);
                $i=1;
            }else{
                $this->atFPDF->SetFillColor(200, 200, 200);
                $i=0;
            }
            $this->atFPDF->SetDrawColor(255, 255, 255);

            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetFont('Arial', '', 6);

            $this->atFPDF->Row(array($p['cod_solicitud'],
                $p['ind_num_denuncia'],
                utf8_decode($p['ente']),
                utf8_decode($p['tipoActuacion']),
                utf8_decode($p['estado']),
                utf8_decode($p['estadoTra']),
                utf8_decode($p['ind_municipio']),
                utf8_decode($p['ind_parroquia']),
                utf8_decode($p['ind_sector'])));
            $this->atFPDF->Ln(1);
        }


        #salida del pdf
        $this->atFPDF->Output();
    }


}


?>