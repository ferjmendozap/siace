<?php
class reportePlanificacionControlador extends Controlador
{
    public $atReporte;
    public $atSolicitudModelo;
    public $atFnGnModelo;
    public $atDenunciasModelo;
    private $atFPDF;
    public $atEjecucionModelo;

    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        $this->atReporte = $this->metCargarModelo('reportes', 'reportes');
        $this->atSolicitudModelo = $this->metCargarModelo('solicitud', 'solicitud');
        $this->atFnGnModelo = $this->metCargarModelo('funcionesGeneralesDN');
        $this->atDenunciasModelo = $this->metCargarModelo('denuncias', 'denuncias');
        $this->atEjecucionModelo = $this->metCargarModelo('ejecucion', 'ejecucion');
        $this->atProrrogaModelo = $this->metCargarModelo('prorroga', 'prorroga');

        Session::metAcceso();
        #se carga la Libreria.
        $this->metObtenerLibreria('headerGeneral','modDN');
        $this->atFPDF = new pdf('p','mm','letter');
    }

    public function metIndex()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado', $this->atReporte->metListarSolicitudesReporte(' AND dn_b001_denuncia.ind_estatus_solicitud="TR"'));
        $this->atVista->metRenderizar('listadoPlanificaciones');
    }

    public function metImprimirPlan()
    {
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 5, 5);
        $this->atFPDF->SetAutoPageBreak(5, 5);
        $this->atFPDF->AddPage();
        //	Cabecera de página
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(15, 5); $this->atFPDF->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->atFPDF->SetXY(15, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCIÓN DE ATENCIÓN AL CIUDADANO'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 12);
        $this->atFPDF->SetY(20); $this->atFPDF->Cell(210, 5, utf8_decode('PLANIFICACIÓN ACTUACIÓN'), 0, 1, 'C');
        $this->atFPDF->Ln(8);

        $where=' AND dn_b001_denuncia.pk_num_denuncia='.$_GET['tramite'];
        $datos=$this->atReporte-> metListarSolicitudesReporte($where);

        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetY(30); $this->atFPDF->Cell(20, 5, 'TRAMITE NRO.: ', 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 7);
        $this->atFPDF->SetXY(40,30); $this->atFPDF->Cell(20, 5, $_GET['cod'], 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetY(35); $this->atFPDF->Cell(20, 5, 'ENTE/ORGANISMO', 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 7);
        $this->atFPDF->SetXY(40,35); $this->atFPDF->Cell(20, 5, utf8_decode($datos[0]['ente']), 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetY(40); $this->atFPDF->Cell(20, 5, 'TIPO DE TRAMITE', 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 7);
        $this->atFPDF->SetXY(40,40); $this->atFPDF->Cell(20, 5, utf8_decode($datos[0]['tipoActuacion']), 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetY(45); $this->atFPDF->Cell(20, 5, 'ESTADO DEL TRAMITE', 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 7);
        $this->atFPDF->SetXY(40,45); $this->atFPDF->Cell(20, 5, utf8_decode($datos[0]['estadoTra']), 0, 1, 'L');

        $result=$this->atDenunciasModelo->metActividadesAsignadas($_GET['tramite']);
        $fec_inicio_real='';
        if(count($result)>0) {
            foreach ($result as $fila) {

                /////   Semaforo de las actividades   /////
                if($fila['ind_estatus']=='PE' or $fila['ind_estatus']=='EJ'){
                    list($a,$m,$d)=explode('-',$fila['fec_inicio']);
                    if($a==date('Y')){
                        if(($d>date('d') and $m>=date('m')) or ($d==date('d') and $m>date('m')) or ($d<date('d') and $m>date('m'))){
                            $fila['semaforo']='alert alert-warning';
                        }elseif(($d<date('d') and $m<=date('m')) or ($d==date('d') and $m<date('m'))){
                            $fila['semaforo']='alert alert-danger';
                        }else{
                            $fila['semaforo']='alert alert-warning';
                        }
                    }elseif($a<date('Y')){
                        $fila['semaforo']='alert alert-danger';
                    }else{
                        $fila['semaforo']='alert alert-warning';
                    }
                }else{
                    $fila['semaforo']='alert alert-success';
                }

                if($fila['fec_inicio_real']=='' or $fila['fec_inicio_real']=='00-00-0000' or $fila['fec_inicio_real']=='0000-00-00'){
                    if($fec_inicio_real==''){
                        $fec_inicio_real=$fila['fec_inicio'];
                    }else{
                        $fec_inicio_real=$fec_inicio_real;
                    }

                }else{
                    $fec_inicio_real=$fila['fec_inicio_real'];
                }

                $duracionT=$fila['num_duracion'] + $fila['num_prorroga'];
                $fila['nombre_fase'] =$fila['nombre_fase'];
                $fila['cod_fase'] =$fila['cod_fase'];
                $fecha_fin_actividad=$this->atFnGnModelo->metSumarDiasHabiles($fec_inicio_real, (int) $duracionT);
                $fila['fec_inicio'] = $this->atFnGnModelo->metFormateaFecha($fila['fec_inicio']);
                $fila['fec_inicio_real'] = $this->atFnGnModelo->metFormateaFecha($fec_inicio_real);
                $fila['fec_termino'] = $this->atFnGnModelo->metFormateaFecha($fila['fec_termino']);
                if($fila['fec_termino_real']=='0000-00-00' or $fila['fec_termino_real']=='' or $fila['fec_termino_real']=='00-00-0000'){
                    $fila['fec_termino_real'] = $this->atFnGnModelo->metFormateaFecha($fecha_fin_actividad);
                    $fec_inicio_real=$this->atFnGnModelo->metSumarDiasHabiles($fecha_fin_actividad, 2);
                } else{
                    $fila['fec_termino_real'] = $this->atFnGnModelo->metFormateaFecha($fila['fec_termino_real']);
                    $fec_inicio_real=$this->atFnGnModelo->metSumarDiasHabiles($fila['fec_termino_real'], 2);
                }
                $fila['num_flag_no_afecto_plan'] =$fila['num_flag_no_afecto_plan'];
                $fila['num_duracion'] =$fila['num_duracion'];
                $fila['num_prorroga'] =$fila['num_prorroga'];
                $fila['num_flag_auto_archivo'] =$fila['num_flag_auto_archivo'];
                $fila['pk_num_detalle_dc'] =$fila['pk_num_detalle_dc'];
                $fila['estadoAct'] =$fila['estadoAct'];

                $arrData[]=$fila;
            }
        }

        $arrData=$this->atFnGnModelo->metAgrupaArray($arrData,'nombre_fase','cod_fase');


        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetWidths(array(30, 50, 10, 20, 20, 10, 20, 20, 10, 10));
        $this->atFPDF->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C','C', 'C', 'C'));


        foreach ($arrData as $key=>$f) {


            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->SetFont('Arial', 'B', 6);
            $this->atFPDF->Cell(200,5, utf8_decode($key),1, 1 , 'L', true);
            $this->atFPDF->Row(array('Est.',
                'Actividad',
                utf8_decode('Días'),
                'Inicio',
                'Fin',
                'Prorr.',
                'Inicio Real',
                'Fin Real',
                'A.P.',
                'A.A.'));
            foreach ($f as $act) {

                if($act['num_flag_no_afecto_plan']==1){
                    $chek=' X ';
                }else{
                    $chek=' ';
                }

                if($act['num_flag_auto_archivo']==1){
                    $chek2=' X ';
                }else{
                    $chek2=' ';
                }

                $this->atFPDF->SetDrawColor(0, 0, 0);
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->SetFillColor(250, 250, 250);

                $this->atFPDF->SetFont('Arial', '', 6);

                $this->atFPDF->Row(array(utf8_decode($act['estadoAct']),
                    utf8_decode($act['cod_act'].' - '.$act['nombre_actividad']),
                    utf8_decode($act['num_duracion']),
                    utf8_decode($act['fec_inicio']),
                    utf8_decode($act['fec_termino']),
                    utf8_decode($act['num_prorroga']),
                    utf8_decode($act['fec_inicio_real']),
                    utf8_decode($act['fec_termino_real']),
                    $chek,
                    $chek2));

            }
            $this->atFPDF->Ln(3);
        }


        #salida del pdf
        $this->atFPDF->Output();
    }

}


?>