<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Denuncias
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Guidmar Espinoza     | dtecnica.conmumat@gmail.com          | 0414-1913443
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class solicitudControlador extends Controlador
{
    private $atSolicitudModelo;
    private $atMiscelaneoModelo;
    private $atDependenciaModelo;
    private $atFunGn;
    private $atFPDF;
    private $atMunicipioModelo;
    private $atPersonaModelo;

    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atSolicitudModelo = $this->metCargarModelo('solicitud', 'solicitud');
        $this->atFunGn = $this->metCargarModelo('funcionesGeneralesDN');
        $this->atPersonaModelo = $this->metCargarModelo('persona','maestros','modCV');
        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->metObtenerLibreria('headerGeneral','modDN');
        $this->atFPDF = new pdf('p','mm','Letter');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('denunciasPost', 1);
        //$this->atVista->assign('listado', $this->atSolicitudModelo->metListarSolicitudes());
        $this->atVista->metRenderizar('listadoSolicitudes');

    }

    public function metCrearModificar()
    {
            $complementosJs = array(
                'jquery-validation/dist/jquery.validate.min',
                'jquery-validation/dist/additional-methods.min',
                'wizard/jquery.bootstrap.wizard.min',
                'bootstrap-datepicker/bootstrap-datepicker',
                'select2/select2.min',
            );
            $complementoCss = array(
                'bootstrap-datepicker/datepicker',
                'select2/select201ef',
                'wizard/wizardfa6c'
            );
            $js = array('Aplicacion/appFunciones',
                'modRH/modRHFunciones',
                'materialSiace/core/demo/DemoFormWizard');
            $this->atVista->metCargarCssComplemento($complementoCss);
            $this->atVista->metCargarJsComplemento($complementosJs);
            $this->atVista->metCargarJs($js);

            $estadoContraloria = $this->atFunGn->metParametros('DEFAULTESTADO');
            $valido = $this->metObtenerInt('valido');
            $idSolicitud = $this->metObtenerInt('idSolicitud');
            $ver = $this->metObtenerInt('ver');
            $codSolicitud='';

        if ($valido == 1) {
            $annio = date('Y');
            $cod = $this->atFunGn->metGeneraCodigo('dn_b001_denuncia', 'cod_solicitud', 4, 'fec_anio', $annio);
            $codSolicitud=$cod.'-'.$annio;

                $Excceccion = array('ind_especificacion_origen_denuncia', 'ind_especificacion_tipo_denuncia', 'tipoActuacionTexto', 'ind_propuesta');
                $ind = $this->metValidarFormArrayDatos('form', 'int',array('id_dependencia'));
                $texto = $this->metValidarFormArrayDatos('form', 'txt', $Excceccion);
                $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');

                if ($alphaNum != null && $ind == null && $texto == null) {
                    $validacion = $alphaNum;
                } elseif ($alphaNum == null && $ind != null && $texto == null) {
                    $validacion = $ind;
                } elseif ($alphaNum == null && $ind == null && $texto != null) {
                    $validacion = $texto;
                } elseif ($alphaNum == null && $ind != null && $texto != null) {
                    $validacion = array_merge($texto, $ind);
                } elseif ($alphaNum != null && $ind == null && $texto != null) {
                    $validacion = array_merge($texto, $alphaNum);
                } elseif ($alphaNum != null && $ind != null && $texto == null) {
                    $validacion = array_merge($ind, $alphaNum);
                } else {
                    $validacion = array_merge($alphaNum, $ind, $texto);
                }
                if (in_array('error', $validacion)) {
                    $validacion['status'] = 'error';
                    echo json_encode($validacion);
                    exit;
                }
                if (!isset($validacion['recibido'])){
                    $validacion['status']="errorRec";
                    echo json_encode($validacion);
                    exit;
                }
                if (!isset($validacion['denunciantes'])){
                    $validacion['status']="errorPer";
                    echo json_encode($validacion);
                    exit;
                }
                if (!isset($validacion['prueba'])){
                    $validacion['status']="errorPrueba";
                    echo json_encode($validacion);
                    exit;
                }
                if (!isset($validacion['ind_estatus'])) {
                    $validacion['ind_estatus'] = 'PE';
                }

                if ($_POST['pk_num_denuncia']=='') {
                    $validacion['cod_solicitud']=$codSolicitud;
                    $validacion['t']='nuevo';
                    $id = $this->atSolicitudModelo->metCrearSolicitud($validacion);
                    $validacion['status'] = 'nuevo';
                } else {
                    $validacion['idSolicitud'] =$_POST['pk_num_denuncia'];
                    $validacion['t']='modificar';
                    $id = $this->atSolicitudModelo->metCrearSolicitud($validacion);
                    $validacion['status'] = 'modificar';
                }

                if (is_array($id)) {
                    foreach ($validacion as $titulo => $valor) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                    $validacion['status'] = 'errorSQL';
                    echo json_encode($validacion);
                    exit;
                }

                $validacion['idSolicitud'] = $id;

                echo json_encode($validacion);
                exit;
            }

        if ($idSolicitud != 0) {

            $this->atVista->assign('formDB', $this->atSolicitudModelo->metMostrarSolicitud($idSolicitud));
            $this->atVista->assign('formTab', $this->atSolicitudModelo->metMostrarReceptores($idSolicitud));
            $this->atVista->assign('formTab2', $this->atSolicitudModelo->metMostrarDenunciantes($idSolicitud));
            $this->atVista->assign('formTab3', $this->atSolicitudModelo->metMostrarPruebas($idSolicitud));

            $this->atVista->assign('idSolicitud', $idSolicitud);
            $this->atVista->assign('ver', $ver);
        }

            $hora = date("h:m:s A");
            $sesion=$this->atIdUsuario;

            $ci = $this->atFunGn->metConsultaDato('a003_persona','ind_cedula_documento','pk_num_persona='.$sesion);
            $nombre = $this->atFunGn->metConsultaDato('a003_persona','ind_nombre1, ind_apellido1','pk_num_persona='.$sesion);
            $dependencia=$this->atFunGn->metBuscarPredeterminado();
            $this->atVista->assign('ci',$ci['ind_cedula_documento']);
            $this->atVista->assign('predeterminado',$this->atFunGn->metBuscarPredeterminado());
            $this->atVista->assign('nombre',$nombre['ind_nombre1'].' '.$nombre['ind_apellido1']);
            $this->atVista->assign('dependencia',$dependencia['ind_dependencia']);
            $this->atVista->assign('origen',$this->atSolicitudModelo->atMiscelaneoModelo->metMostrarSelect('DN-01-02'));
            $this->atVista->assign('tipoActuacion',$this->atSolicitudModelo->atMiscelaneoModelo->metMostrarSelect('TACT'));
            $this->atVista->assign('tipo',$this->atSolicitudModelo->atMiscelaneoModelo->metMostrarSelect('DN-01-03'));
            $this->atVista->assign('listadoMunicipio', $this->atSolicitudModelo->atMunicipioModelo->metJsonMunicipio($estadoContraloria['ind_valor_parametro']));
            $this->atVista->assign('selectEnte', $this->atFunGn->metEnteSujetoControl());
            $this->atVista->assign('selectDependencia', $this->atFunGn->metListaDependenciasInt());
            $this->atVista->assign('listadoContraloria', $this->atFunGn->metContraloria(1));
            $this->atVista->assign('listadoEmpleados', $this->atSolicitudModelo->metListarEmpleados());
            $this->atVista->assign('codSolicitud', $codSolicitud);
            $this->atVista->assign('sesion', $this->atSolicitudModelo->atIdUsuario);
            $this->atVista->assign('fecha_actual', $this->atFunGn->metFormateaFecha(date("Y-m-d")));
            $this->atVista->assign('hora_actual', $hora);
            $this->atVista->metRenderizar('CrearModificar', 'modales');
    }

    public function metRechazarSolicitud(){
        $idSolicitud = $this->metObtenerInt('idSolicitud');
        $rechazo=$this->atSolicitudModelo->metJsonRechazar($idSolicitud);

        echo json_encode($rechazo);
        exit;
    }

    public function metJsonSector()
    {
        $idParroquia = $this->metObtenerInt('idParroquia');
        $sector = $this->atFunGn->metJsonSector($idParroquia);
        echo json_encode($sector);
        exit;
    }

    public function metJsonCentroCosto()
    {
        $idDependencia = $this->metObtenerInt('id_dependencia');
        $centro = $this->atFunGn->metJsonCentroCosto($idDependencia);
        echo json_encode($centro);
        exit;
    }

    public function metPersonas()
    {
        $idPersona = $this->metObtenerTexto('idPersona');
        $tipo = $this->metObtenerTexto('tipo');
        $cant= $this->metObtenerInt('cargar');
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );

        if(!$idPersona){
            $js[] = 'materialSiace/core/demo/DemoTableDynamic';
            $this->atVista->metCargarCssComplemento($complementosCss);
            $this->atVista->metCargarJs($js);
        }
        $this->atVista->assign('idPersona',$idPersona);
        $this->atVista->assign('tipo',$tipo);
        $this->atVista->assign('cant',$cant);
        if ($idPersona=='EMP')
            $this->atVista->assign('listado',$this->atSolicitudModelo->metListarEmpleados());
        else
            $this->atVista->assign('listado',$this->atSolicitudModelo->metListarPersona());

        $this->atVista->metRenderizar('listadoPersonas','modales');

    }

    public function metCrearModificarPersona()
    {
        $complementoJs =array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementoJs);

        $valido=$this->metObtenerInt('valido');


        if($valido==1){

            $excepcion=array('num_estatus');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
            $ind=$this->metValidarFormArrayDatos('form','int',$excepcion);
            $formula=$this->metValidarFormArrayDatos('form','formula');

            if ($alphaNum != null && $ind == null && $formula == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $formula == null) {
                $validacion = $ind;
            } elseif ($alphaNum != null && $ind != null && $formula == null) {
                $validacion = array_merge($alphaNum, $ind);
            } elseif ($alphaNum != null && $formula != null && $ind == null) {
                $validacion = array_merge($alphaNum, $formula);
            } elseif ($ind != null && $formula != null && $alphaNum == null) {
                $validacion = array_merge($ind, $formula);
            } else {
                $validacion = array_merge($ind, $formula, $alphaNum);
            }
            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }

            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            }

                $id=$this->atPersonaModelo->metCrearPersona($validacion['ind_cedula_documento'],$validacion['ind_nombre1'],$validacion['ind_apellido1'],
                $validacion['ind_direccion'],$validacion['ind_email'],$validacion['ind_cedula_documento'],$validacion['ind_telefono'],$validacion['num_estatus']);
                $validacion['status']='nuevo';


            if(is_array($id)){

                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['idPersona']=$id;
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idPersona']=$id;

            echo json_encode($validacion);
            exit;
        }

        $this->atVista->metRenderizar('CrearModificarPersona','modales');
    }

    public function metJsonOrigen()
    {
        $origen = $this->metObtenerInt('origen');
        $valor='DN-01-02';
        $valor2=' and pk_num_miscelaneo_detalle='.$origen;
        $idOrigen = $this->atFunGn->metMiscelaneo($valor, $valor2);
        $tabla='a039_ente';
        $cadena_campo=' pk_num_ente, ind_nombre_ente, num_ente_padre ';
        if($idOrigen['cod_detalle']=='001'){
            $where=" fk_a038_num_categoria_ente=
            (SELECT pk_num_categoria_ente FROM `a038_categoria_ente` where ind_categoria_ente like '%Consejo%' and ind_categoria_ente like '%Comunal%')";
        }elseif ($idOrigen['cod_detalle']=='002'){
            $where=" fk_a037_num_tipo_ente=
            (SELECT pk_num_tipo_ente FROM `a037_tipo_ente` where ind_tipo_ente like '%public%' or ind_tipo_ente like '%organ%')" ;
        }else{
            $where='';
        }
        if($where!=''){
            $origen = $this->atFunGn->metConsultaTabla($tabla, $cadena_campo, $where);
        }else{
            $origen='';
        }

        echo json_encode($origen);
        exit;
    }

    public function metJsonTipoActuacionTexto($tipoActuacion)
    {
        $tipoActuacionTexto = $this->atFunGn->metConsultaDato('a006_miscelaneo_detalle','cod_detalle','pk_num_miscelaneo_detalle='.$tipoActuacion);
        echo json_encode(array("tipoActuacionTexto"=>$tipoActuacionTexto['cod_detalle']));
    }

    public function metImprimirActa()
    {
        $idSolicitud = $_GET['idSolicitud'];

        $datos= $this->atSolicitudModelo->metMostrarSolicitud($idSolicitud);
        $receptores=$this->atSolicitudModelo->metMostrarReceptores($idSolicitud);
        $denunciantes=$this->atSolicitudModelo->metMostrarDenunciantes($idSolicitud);
        $pruebas= $this->atSolicitudModelo->metMostrarPruebas($idSolicitud);

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 5, 5);
        $this->atFPDF->SetAutoPageBreak(5, 5);
        $this->atFPDF->AddPage();

        //	Cabecera de página
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(15, 5); $this->atFPDF->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->atFPDF->SetXY(15, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCIÓN DE ATENCIÓN AL CIUDADANO'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 12);
        $this->atFPDF->SetY(20); $this->atFPDF->Cell(200, 5, utf8_decode('RECEPCIÓN DE DENUNCIA'), 0, 1, 'C');
        $this->atFPDF->Ln(8);

        $this->atFPDF->Ln(1);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetWidths(array(205));
        $this->atFPDF->SetAligns(array('C'));
        $this->atFPDF->Row(array(
            'DATOS DE LA DENUNCIA'));

        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetWidths(array(35, 35, 90, 45));
        $this->atFPDF->SetAligns(array('C', 'C','C', 'C'));
        $this->atFPDF->Row(array(utf8_decode('N° DENUNCIA'),
            'TIPO DE SOLICITUD',
            'LUGAR',
            utf8_decode('FECHA DE RECEPCIÓN')));
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->Row(array(utf8_decode($datos['cod_solicitud']),
            utf8_decode($datos['tipoActuacion']),
            utf8_decode(APP_ORGANISMO),
            $datos['fec_recepcion']));

        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetWidths(array(60, 75, 70));
        $this->atFPDF->SetAligns(array('C', 'C', 'C'));
        $this->atFPDF->Row(array(
            'MUNICIPIO',
            'PARROQUIA',
            'SECTOR'));
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->Row(array(utf8_decode($datos['ind_municipio']),
            utf8_decode($datos['ind_parroquia']),
            utf8_decode($datos['ind_sector'])));

        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetWidths(array(103, 102));
        $this->atFPDF->SetAligns(array('C', 'C'));
        $this->atFPDF->Row(array(
            'ORIGEN DE LA DENUNCIA',
            'TIPO DE DENUNCIA'));
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetWidths(array(103, 102));
        $this->atFPDF->SetAligns(array('C', 'C'));
        $this->atFPDF->Row(array(
            utf8_decode($datos['origenDenuncia']).' '. utf8_decode($datos['ind_especificacion_origen_denuncia']),
            utf8_decode($datos['tipoDenuncia']).' '.utf8_decode($datos['ind_especificacion_tipo_denuncia'])));

        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetWidths(array(103, 102));
        $this->atFPDF->SetAligns(array('C', 'C'));
        $this->atFPDF->Row(array(
            utf8_decode('ÓRGANO/ENTE DENUNCIADO'),
            'ORIGEN DE LOS RECURSOS'));
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetWidths(array(103, 102));
        $this->atFPDF->SetAligns(array('C', 'C'));
        $this->atFPDF->Row(array(
            utf8_decode($datos['ind_nombre_ente']),
            utf8_decode($datos['origenRecursos'])));


        $this->atFPDF->Ln(5);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetWidths(array(205));
        $this->atFPDF->SetAligns(array('C'));
        $this->atFPDF->Row(array(
            'DENUNCIANTES'));

        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetWidths(array(55, 30, 50, 30, 40));
        $this->atFPDF->SetAligns(array('C', 'C', 'C', 'C', 'C'));
        $this->atFPDF->Row(array(
            'NOMBRES Y APELLIDOS',
            utf8_decode('CÉDULA DE IDENTIDAD'),
            utf8_decode('DIRECCIÓN'),
            utf8_decode('TELÉFONO'),
            utf8_decode('CORREO ELECTRÓNICO')));
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 8);
        $ciudadanos='';
        $residencia='';
        foreach ($denunciantes as $d) {
            $ciudadanos=$ciudadanos.$d['nombre'].', ';
            $residencia=$residencia.$d['direccion'].' / ';
            $this->atFPDF->Row(array(utf8_decode($d['nombre']),
                utf8_decode($d['cedula']),
                utf8_decode($d['direccion']),
                utf8_decode($d['telefono']),
                utf8_decode($d['ind_email'])));

        }

        $this->atFPDF->Ln(5);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetWidths(array(205));
        $this->atFPDF->SetAligns(array('C'));
        $this->atFPDF->Row(array(
            'RECAUDOS CONSIGNADOS'));

        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetWidths(array(35, 40, 70, 60));
        $this->atFPDF->SetAligns(array('C', 'C', 'C', 'C'));
        $this->atFPDF->Row(array(
            utf8_decode('N°'),
            utf8_decode('N° DOCUMENTO'),
            utf8_decode('DESCRIPCIÓN'),
            utf8_decode('CANTIDAD')));
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 8);
        $i=1;
        $pru='';
        foreach ($pruebas as $p) {
            $pru=$pru.utf8_decode($p['ind_descripcion']).' ('.$p['num_cantidad'].'), ';
            $this->atFPDF->Row(array($i,
                utf8_decode($p['ind_numero_documento']),
                utf8_decode($p['ind_descripcion']),
                utf8_decode($p['num_cantidad'])));
            $i=$i+1;
        }

        $this->atFPDF->Ln(5);

        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetWidths(array(205));
        $this->atFPDF->SetAligns(array('L'));
        $this->atFPDF->Row(array(
            utf8_decode('DESCRIPCIÓN')));
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetWidths(array(205));
        $this->atFPDF->SetAligns(array('L'));
        $this->atFPDF->Row(array(
            utf8_decode($datos['ind_descripcion_denuncia'])));

        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetWidths(array(205));
        $this->atFPDF->SetAligns(array('L'));
        $this->atFPDF->Row(array(
            utf8_decode('MOTIVO')));
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetWidths(array(205));
        $this->atFPDF->SetAligns(array('L'));
        $this->atFPDF->Row(array(
            utf8_decode($datos['ind_motivo_origen'])));

        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetWidths(array(205));
        $this->atFPDF->SetAligns(array('L'));
        $this->atFPDF->Row(array(
            utf8_decode('PROPUESTA')));
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetWidths(array(205));
        $this->atFPDF->SetAligns(array('L'));
        $this->atFPDF->Row(array(
            utf8_decode($datos['ind_propuesta'])));
        //	Muestro el contenido del pdf.
        $this->atFPDF->AddPage();
        //	Cabecera de página
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(15, 5); $this->atFPDF->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->atFPDF->SetXY(15, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCIÓN DE ATENCIÓN AL CIUDADANO'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 12);
        $this->atFPDF->SetY(20); $this->atFPDF->Cell(200, 5, utf8_decode('ACTA DE LA DENUNCIA'), 0, 1, 'C');


        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 10);
        list($f,$h)=explode(' ',$datos['fec_elaborado']);
        list($y,$m,$d)=explode('-',$f);
        $meses=array('ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE');
        $texto= utf8_decode('
En la Ciudad de Maturín, a los '.$d.' días del mes de '.$meses[$m-1].' del año '.$y.' siendo las '.$h.'.'.' Reunidos en la Dirección de Atención Ciudadano y Control Comunitario, el (los) ciudadanos (s): '.$ciudadanos.
            ' representante (s) de: '. $datos['origenDenuncia'].' '. $datos['ind_especificacion_origen_denuncia'].
            ', residenciado (s) en: '. $residencia.
            '
            
En virtud de su derecho y Decisión de participar en el control de la gestión pública de manera corresponsable de conformidad con lo establecido en el articulo 62 de la constitución de la República Bolivariana de Venezuela y Conforme a lo dispuesto en el articulo 6 de la Ley organica de la Contraloria General de la República y del Sistema Nacional de Control Fiscal, y según lo pautado en materia de denuncias N: 01-00-055 de fecha 21/06/2000 emanado de la Contraloria General de la República denuncia (mos) lo siguiente: '.
            $datos['ind_motivo_origen'].
            '
            
Conforme a los datos suministrados por el (los) denunciante (s) ante la CONTRALORÍA DEL ESTADO SUCRE; y en su concordancia con el artículo 15 de las normas para fomentar la participación ciudadana según Resolución N: 01-00-000225 de fecha 20/08/2007 emanada de la Contraloría General de la República, publicada en la Gaceta Oficial Nro: 38.750 de fecha 20/08/2007, la Dirección de Atención y Control Comunitario determinara al ciudadano, si la misma sera tramitada por esta organización ó si será remitida a otraentidad u Organismo que tenga atribuida la competencia de resolverla.'.
            '
            
Se levanta la presente acta en original y copia de conformidad con lo previsto en el artículo 20 de las Normas para Fomentar la Participación Ciudadana antes identificada, emitida por la Controlaría General de la República; de las cuales una queda en poder del denunciante y la otra en el expediente de la Denuncia.'.
            '
            
Documentos consignados por el denunciante que soportan la denuncia: '.$pru.
            '
            
Se leyo y en prueba de conformidad firman.');
        $this->atFPDF->SetY(25); $this->atFPDF->MultiCell(200, 5, $texto, 0,  'J',false);

        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 10);
        $this->atFPDF->Ln();
        $this->atFPDF->Cell(100, 5, 'Por la '.utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->atFPDF->Ln();
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetWidths(array(90, 60, 50));

        $this->atFPDF->SetAligns(array('C', 'C', 'C'));
        $this->atFPDF->Row(array(
            'NOMBRES Y APELLIDOS',
            utf8_decode('CÉDULA DE IDENTIDAD'),
            'FIRMA'));

        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 10);
        foreach ($receptores as $d) {

            $this->atFPDF->Cell(90,7, utf8_decode($d['nombre']),1, 0 , 'L', false);
            $this->atFPDF->Cell(60,7, utf8_decode($d['cedula']),1, 0 , 'C', false );
            $this->atFPDF->Cell(50,7, '',1, 0 , 'L', false );
            $this->atFPDF->Ln();
        }
        $this->atFPDF->Ln();
        $this->atFPDF->Cell(100, 5, 'Denunciante (s)', 0, 1, 'L');
        $this->atFPDF->Ln();
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetWidths(array(90, 60, 50));
        $this->atFPDF->SetAligns(array('C', 'C', 'C'));
        $this->atFPDF->Row(array(
            'NOMBRES Y APELLIDOS',
            utf8_decode('CÉDULA DE IDENTIDAD'),
            'FIRMA'));

        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 10);
        foreach ($denunciantes as $d) {
            $this->atFPDF->Cell(90,7, utf8_decode($d['nombre']),1, 0 , 'L', false);
            $this->atFPDF->Cell(60,7, utf8_decode($d['cedula']),1, 0 , 'C', false );
            $this->atFPDF->Cell(50,7, '',1, 0 , 'L', false );
            $this->atFPDF->Ln();
        }




        $this->atFPDF->Output();

    }

    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #capturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT *, 
                  a039_ente.ind_nombre_ente , 
                  act.ind_nombre_detalle as tipoActuacion, 
                  act.cod_detalle,
                  estado.ind_nombre_detalle as estado
                FROM dn_b001_denuncia 
                LEFT JOIN a039_ente 
                  ON dn_b001_denuncia.fk_a039_num_ente = a039_ente.pk_num_ente 
                LEFT JOIN a006_miscelaneo_detalle as estado
                  ON estado.fk_a005_num_miscelaneo_maestro= 
                    (SELECT 
                      pk_num_miscelaneo_maestro 
                    FROM a005_miscelaneo_maestro 
                    WHERE cod_maestro='DN-EST-SOL') 
                    and estado.cod_detalle = dn_b001_denuncia.ind_estatus_solicitud 
                LEFT JOIN a006_miscelaneo_detalle as act
                  ON act.pk_num_miscelaneo_detalle = dn_b001_denuncia.fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion
                WHERE  1
                ";

        if ($busqueda['value']) {
            #concateno la busqueda si existe, Nota: esto es obligatorio
            $sql .="
                AND 
                    ( 
                      dn_b001_denuncia.cod_solicitud LIKE '%$busqueda[value]%' OR 
                      act.ind_nombre_detalle LIKE '%$busqueda[value]%' OR 
                      a039_ente.ind_nombre_ente LIKE '%$busqueda[value]%'
                    )
            ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('cod_solicitud','tipoActuacion', 'ind_nombre_ente', 'estado');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_denuncia';
        #construyo el listado de botones
        $camposExtra = array(
            'cod_solicitud',
            'cod_detalle');
        if (in_array('DN-01-01-02-M',$rol)) {
            $campos['boton']['Modificar'] = array("
                <button accion='modificar' title='Editar'
                   class='modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                   descipcion='El Usuario ha Modificado la Solicitud Nro. cod_solicitud'
                   idSolicitud='pk_num_denuncia' titulo='Modificar Solicitud'
                   data-toggle='modal' data-target='#formModal' 
                   id='modificar'>
                   <i class='fa fa-edit'></i>
                </button>
                ",
                'if( $i["ind_estatus_solicitud"] =="PE") { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Modificar'] = false;
        }

        if (in_array('DN-01-01-03-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button accion="ver" title="Consultar"
                        class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                        descipcion="El Usuario ha Consultado la Solicitud Nro. cod_solicitud"
                        idSolicitud="pk_num_denuncia" titulo="Consultar Solicitud"
                        data-toggle="modal" data-target="#formModal" id="ver">
                    <i class="icm icm-eye2"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }


        if (in_array('DN-01-01-04-R',$rol)) {
            $campos['boton']['Rechazar'] = array("
                <button accion='rechazar' title='Rechazar'
                        class='rechazar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger'
                        descipcion='El Usuario ha Rechazado la Solicitud Nro. cod_solicitud'
                        idSolicitud='pk_num_denuncia' titulo='Rechazar Solicitud'
                        mensaje='Estas seguro que desea RECHAZAR la Solicitud!!' id='rechazar'>
                    <i class='md md-clear'></i>
                </button>
                ",
                'if( $i["ind_estatus_solicitud"] =="PE" ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Rechazar'] = false;
        }

        if (in_array('DN-01-01-05-P',$rol)) {
            $campos['boton']['Planificar'] = array("
                <button accion='planificar' title='Planificar'
                        class='planificar logsUsuario btn ink-reaction btn-raised btn-xs btn-success'
                        descipcion='El Usuario ha Generado la Planificación de la Solicitud Nro. cod_solicitud'
                        idSolicitud='pk_num_denuncia' titulo='Generar Planificación'
                        data-toggle='modal' data-target='#formModal'
                        tipoActuacion='cod_detalle'
                        id='planificar'>
                    <i class='icm icm-clipboard2'></i>
                </button>
            ",
                'if( $i["ind_estatus_solicitud"] =="PE" ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Planificar'] = false;
        }

        if (in_array('DN-01-01-06-A',$rol)) {
            $campos['boton']['Aprobar'] = array("
                <button accion='aprobar' title='Aprobar'
                        class='aprobar logsUsuario btn btn-xs ink-reaction btn-primary'
                        descipcion='El Usuario ha Aprobado la Planificación de la Solicitud Nro. cod_solicitud'
                        idDenuncia='pk_num_denuncia' titulo='Aprobar Planificación'
                        data-toggle='modal' data-target='#formModal'
                        tipoActuacion='cod_detalle'
                        id='aprobar'>
                    <i class='icm icm-rating3'></i>
                </button>
            ",
                'if( $i["ind_estatus_tramite"]=="GN") { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Aprobar'] = false;
        }

        if (in_array('DN-01-01-07-I',$rol)) {
            $campos['boton']['Imprimir'] = "
            <button accion='imprimir' title='Imprimir Acta'
                    class='imprimir logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                    descipcion='El Usuario ha Impreso el Acta de la Solicitud Nro. cod_solicitud'
                    idDenuncia='pk_num_denuncia' titulo='Imprimir Acta'
                    idSolicitud='pk_num_denuncia'
                    data-toggle='modal' data-target='#formModal'
                    tipoActuacion='cod_detalle'
                    id='acta'
                    >
                <i class='fa fa-print'></i>
            </button>
            ";
        } else {
            $campos['boton']['Imprimir'] = false;
        }


        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,$camposExtra);

    }
}