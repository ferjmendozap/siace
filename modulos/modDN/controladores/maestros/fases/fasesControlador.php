<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: PLANIFICACIÓN FISCAL
 * PROCESO: INGRESO Y MANTENIMIENTO DE TIPOS PROCESO FISCAL
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 3 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |         0426-5144382           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #3                      |        08-08-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class fasesControlador extends Controlador
{
    public $atFases;
    public function __construct()
    {
        parent::__construct();
        $this->atFases=$this->metCargarModelo('fases');
    }

    public function metIndex()
    {
        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('dataBD',$this->atFases->metListarFase());
        $this->atVista->metRenderizar('listado');

    }

    public function metCrearModificar()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min',
            'inputmask/jquery.inputmask.bundle.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        
        $valido=$this->metObtenerInt('valido');
        $idFase=$this->metObtenerInt('idFase');
        $ver=$this->metObtenerInt('ver');

        if ($valido == 1){
            $ind=$this->metValidarFormArrayDatos('form','int');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');

            if ($alphaNum != null && $ind == null) {
                $registro = $alphaNum;
            } elseif ($alphaNum == null && $ind != null) {
                $registro = $ind;
            } else {
                $registro = array_merge($ind, $alphaNum);
            }
            if (in_array('error', $registro)) {
                $registro['status'] = 'error';
                echo json_encode($registro);
                exit;
            }
            if ($idFase == 0) {

                $registro['status'] = 'nuevo';
                $id = $this->atFases->metNuevaFase($registro);
                $registro['idFase'] = $id;
            } else {
                $registro['status'] = 'modificar';
                $id = $this->atFases->metModificarFase($idFase,$registro);
                $registro['idFase'] = $id;
            }

            if (is_array($id)) {
                foreach ($registro as $titulo => $valor) {
                    if(!is_array($registro[$titulo])){
                        if (strpos($id[2], $registro[$titulo])) {
                            $registro[$titulo] = 'error';
                        }
                    }
                }
                $registro['status'] = 'errorSQL';
                echo json_encode($registro);
                exit;
            }
            $idNuevo['status'] = $registro['status'];
            echo json_encode($idNuevo);
            exit;
        }
        if($idFase!=0){
            $this->atVista->assign('dataBD',$this->atFases->metMostrarFase($idFase));
            $this->atVista->assign('idFase',$idFase);
        }

        $this->atVista->assign('tipoActuacion',$this->atFases->atMiscelaneoModelo->metMostrarSelect('TACT'));
        $this->atVista->assign('ver',$ver);
        $this->atVista->assign('idFase',$idFase);
        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    public function metEliminar()
    {
        $idFase = $this->metObtenerInt('idFase');
        if($idFase!=0){
            $id=$this->atFases->metEliminarFase($idFase);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero la Fase se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idFase'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }


}