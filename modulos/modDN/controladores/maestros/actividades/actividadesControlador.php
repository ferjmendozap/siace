<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: DENUNCIAS
 * PROCESO: REGISTRO DE LAS ACTIVIDADES ASOCIADAS A LAS DENUNCIAS / QUEJAS / RECLAMOS
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Guidmar Espinooza                |  dtecnica.conmumat@gmail.com       |         0414-1913443           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        12-12-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class actividadesControlador extends Controlador
{
    private $atActividad;
    public $atFaseModelo;
    private $atFunGn;
    public function __construct()
    {
        parent::__construct();
        $this->atActividad=$this->metCargarModelo('actividades');
        $this->atFaseModelo=$this->metCargarModelo('fases');
        $this->atFunGn = $this->metCargarModelo('funcionesGeneralesDN');
    }

    public function metIndex()
    {
        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('dataBD',$this->atActividad->metListarActividades());
        $this->atVista->metRenderizar('listado');

    }

    public function metCrearModificar()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min',
            'inputmask/jquery.inputmask.bundle.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido=$this->metObtenerInt('valido');
        $idActividad=$this->metObtenerInt('idActividad');
        $ver=$this->metObtenerInt('ver');

        if ($valido == 1){
            $exepcion=array('ind_auto_archivo','ind_afecto_plan','num_estatus');
            $ind=$this->metValidarFormArrayDatos('form','int', $exepcion);

            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');

            if(!isset($ind['num_estatus'])){
                $ind['num_estatus']="0";
            }
            if(!isset($ind['ind_auto_archivo'])){
                $ind['ind_auto_archivo']="0";
            }
            if(!isset($ind['ind_afecto_plan'])){
                $ind['ind_afecto_plan']="0";
            }
            if ($alphaNum != null && $ind == null) {
                $registro = $alphaNum;
            } elseif ($alphaNum == null && $ind != null) {
                $registro = $ind;
            } else {
                $registro = array_merge($ind, $alphaNum);
            }
            if (in_array('error', $registro)) {
                $registro['status'] = 'error';
                echo json_encode($registro);
                exit;
            }
            if ($idActividad == 0) {

                $registro['status'] = 'nuevo';
                $id = $this->atActividad->metNuevaActividad($registro);
                $registro['idActividad'] = $id;
            } else {
                $registro['status'] = 'modificar';
                $id = $this->atActividad->metModificarActividad($idActividad,$registro);
                $registro['idActividad'] = $id;
            }

            if (is_array($id)) {
                foreach ($registro as $titulo => $valor) {
                    if(!is_array($registro[$titulo])){
                        if (strpos($id[2], $registro[$titulo])) {
                            $registro[$titulo] = 'error';
                        }
                    }
                }
                $registro['status'] = 'errorSQL';
                echo json_encode($registro);
                exit;
            }
            $idNuevo['status'] = $registro['status'];
            echo json_encode($idNuevo);
            exit;
        }
        if($idActividad!=0){
            $this->atVista->assign('formDB',$this->atActividad->metMostrarActividad($idActividad));
            $this->atVista->assign('idFase',$this->atFaseModelo->metListarFase());
            $this->atVista->assign('idActividad',$idActividad);
        }

        $this->atVista->assign('tipoActuacion',$this->atActividad->atMiscelaneoModelo->metMostrarSelect('TACT'));
        $this->atVista->assign('ver',$ver);
        $this->atVista->assign('idActividad',$idActividad);
        $this->atVista->metRenderizar('CrearModificar','modales');

    }
// genera el codigo de la actividad asociado al codigo de la fase seleccionada
    public function metCrearCodigo(){
        $idFase = $this->metObtenerInt('idFase');
        $correlativo = $this->atFunGn->metGeneraCodigo('dn_c002_actividad', 'cod_actividad', 2 , 'fk_dnc001_num_fase', $idFase);
        $codActividad=$idFase.$correlativo;
        echo $codActividad;
        exit;
    }
    public function metEliminar()
    {
        $idActividad = $this->metObtenerInt('idActividad');
        if($idActividad!=0){
            $id=$this->atActividad->metEliminarActividad($idActividad);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Proceso se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idActividad'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    public function metJsonFase()
    {
        $idTipoActuacion = $this->metObtenerInt('idTipoActuacion');
        $fase = $this->atFaseModelo->metJsonFase($idTipoActuacion);
        echo json_encode($fase);
        exit;
    }

}