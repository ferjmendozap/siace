<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL MUNICIPIO MATURÍN DEL ESTADO MONAGAS.
 * MODULO: Denuncias
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Guidmar Espinoza     | dtecnica.conmumat@gmail.com          | 0414-1913443
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once 'dataDN.php';

class scriptCargaControlador extends Controlador
{
    use dataDN;
    private $atScriptCarga;

    public function __construct()
    {
        parent::__construct();
        $this->atScriptCarga = $this->metCargarModelo('scriptCarga','scriptCarga');
    }

    public function metIndex()
    {
        ### DN ###
        $this->metDN();

        echo "TERMINADO";
    }

    public function metDN()
    {
        echo 'INICIO CARGA DE MAESTROS MODULO DN<br><br>';
        echo '-----Carga Fases y Actividades-----<br><br>';
        $this->atScriptCarga->metDNFases($this->metDataFases());
    }

}
