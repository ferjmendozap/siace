<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL MUNICIPIO MATURÍN DEL ESTADO MONAGAS.
 * MODULO: Denuncias
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Guidmar Espinoza     | dtecnica.conmumat@gmail.com          | 0414-1913443
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
trait dataDN
{
    #maestros relacionados a obligaciones
    public function metDataFases()
    {
        $dn_c001_fase = array(
            array('cod_fase' => '01','tipo_actuacion' => 'D','ind_descripcion' => 'INDAGACIÓN','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-09 13:39:53',
                'Det' => array(
                    array('cod_actividad' => '1001','ind_descripcion' => 'ACTIVIDAD 1','ind_comentarios' => 'ACTIVIDAD 1 PRUEBA SISTEMA','num_duracion' => '3','num_flag_auto_archivo' => '0','num_flag_no_afecto_plan' => '0','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-09 14:23:17')
                )
            ),
            array('cod_fase' => '02','tipo_actuacion' => 'D','ind_descripcion' => 'VALORACIÓN','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-09 13:50:52',
                'Det' => array(
                    array('cod_actividad' => '1201','ind_descripcion' => 'ACTIVIDAD 2 ','ind_comentarios' => 'ACTIVIDAD 2 PRUEBA DENUNCIA','num_duracion' => '3','num_flag_auto_archivo' => '0','num_flag_no_afecto_plan' => '0','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-09 14:41:47')
                )
            ),
            array('cod_fase' => '03','tipo_actuacion' => 'D','ind_descripcion' => 'ARCHIVO','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-09 14:02:10',
                'Det' => array(
                    array('cod_actividad' => '1401','ind_descripcion' => 'ACTIVIDAD 3','ind_comentarios' => 'ACTIVIDAD 3','num_duracion' => '4','num_flag_auto_archivo' => '1','num_flag_no_afecto_plan' => '0','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-09 14:48:33'),
                    array('cod_actividad' => '1402','ind_descripcion' => 'ACTIVIDAD 4','ind_comentarios' => 'ACTIVIDAD 4','num_duracion' => '2','num_flag_auto_archivo' => '1','num_flag_no_afecto_plan' => '1','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-09 14:59:29')
                )
            ),
            array('cod_fase' => '01','tipo_actuacion' => 'R','ind_descripcion' => 'INDAGACION','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-09 15:01:46',
            'Det' => array(
                array('cod_actividad' => '1701','ind_descripcion' => 'ACTIVIDAD 1 RECLAMO','ind_comentarios' => 'ACTIVIDAD 1 RECLAMO','num_duracion' => '3','num_flag_auto_archivo' => '1','num_flag_no_afecto_plan' => '1','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-09 15:32:08')
                )
            ),
            array('cod_fase' => '02','tipo_actuacion' => 'R','ind_descripcion' => 'VALORACION','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-09 15:03:06',
                'Det' => array(
                    array('cod_actividad' => '1801','ind_descripcion' => 'VALORACION RECLAMO','ind_comentarios' => 'VALORACION RECLAMO','num_duracion' => '2','num_flag_auto_archivo' => '0','num_flag_no_afecto_plan' => '0','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-24 10:15:20')
                )
            ),
            array('cod_fase' => '03','tipo_actuacion' => 'R','ind_descripcion' => 'RESPUESTA','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-09 15:03:27',
                'Det' => array(
                    array('cod_actividad' => '1901','ind_descripcion' => 'RESPUESTA RECLAMO','ind_comentarios' => 'RESPUESTA RECLAMO','num_duracion' => '2','num_flag_auto_archivo' => '1','num_flag_no_afecto_plan' => '0','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-24 10:15:51')
                )
            ),
            array('cod_fase' => '01','tipo_actuacion' => 'P','ind_descripcion' => 'INDAGACIÓN','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-24 10:12:13',
                'Det' => array(
                    array('cod_actividad' => '2301','ind_descripcion' => 'RESPUESTA DE LA PETICIÓN','ind_comentarios' => 'RESPUESTA DE LA PETICIÓN','num_duracion' => '1','num_flag_auto_archivo' => '1','num_flag_no_afecto_plan' => '0','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-24 10:17:26')
                )
            ),
            array('cod_fase' => '02','tipo_actuacion' => 'P','ind_descripcion' => 'RESPUESTA','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-24 10:12:39',
                'Det' => array(
                    array('cod_actividad' => '2201','ind_descripcion' => 'INDAGACIÓN DE LA PETICIÓN','ind_comentarios' => 'INDAGACIÓN DE LA PETICIÓN','num_duracion' => '3','num_flag_auto_archivo' => '0','num_flag_no_afecto_plan' => '0','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-24 10:16:45')
                )
            ),
            array('cod_fase' => '01','tipo_actuacion' => 'S','ind_descripcion' => 'INDAGACIÓN','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-24 10:13:10',
                'Det' => array(
                    array('cod_actividad' => '2401','ind_descripcion' => 'INDAGACIÓN DE LA SUGERENCIA','ind_comentarios' => 'INDAGACIÓN DE LA SUGERENCIA','num_duracion' => '4','num_flag_auto_archivo' => '1','num_flag_no_afecto_plan' => '0','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-24 10:18:12')
                )
            ),
            array('cod_fase' => '02','tipo_actuacion' => 'S','ind_descripcion' => 'RESPUESTA','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-24 10:13:29',
                'Det' => array(
                    array('cod_actividad' => '2501','ind_descripcion' => 'RESPUESTA DE LA SUGERENCIA','ind_comentarios' => 'RESPUESTA DE LA SUGERENCIA','num_duracion' => '2','num_flag_auto_archivo' => '1','num_flag_no_afecto_plan' => '0','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-24 10:18:41')
                )
            ),
            array('cod_fase' => '01','tipo_actuacion' => 'Q','ind_descripcion' => 'INDAGACIÓN','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-24 10:13:56',
                'Det' => array(
                    array('cod_actividad' => '2601','ind_descripcion' => 'INDAGACIÓN DE LA QUEJA','ind_comentarios' => 'INDAGACIÓN DE LA QUEJA','num_duracion' => '5','num_flag_auto_archivo' => '0','num_flag_no_afecto_plan' => '0','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-24 10:19:12')
                )
            ),
            array('cod_fase' => '02','tipo_actuacion' => 'Q','ind_descripcion' => 'RESPUESTA','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-24 10:14:14',
                'Det' => array(
                    array('cod_actividad' => '2701','ind_descripcion' => 'RESPUESTA DE LA QUEJA','ind_comentarios' => 'RESPUESTA DE LA QUEJA','num_duracion' => '3','num_flag_auto_archivo' => '1','num_flag_no_afecto_plan' => '0','num_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fec_ultima_modificacion' => '2017-01-24 10:19:39')
                )
            )
        );

        return $dn_c001_fase;
    }


}