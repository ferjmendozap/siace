INSERT INTO `a005_miscelaneo_maestro` (`pk_num_miscelaneo_maestro`, `ind_nombre_maestro`, `ind_descripcion`, `num_estatus`, `fk_a015_num_seguridad_aplicacion`, `fk_a018_num_seguridad_usuario`, `cod_maestro`, `fec_ultima_modificacion`) VALUES
(121, 'CODIFICACION', 'ESTRUCTURA DEL CODIGO APLICADO POR CADA CONTRALORÍA PARA LAS DENUNCIAS, QUEJAS, RECLAMOS O SUGERENCI', 1, 19, 1, 'DN-COD-01', '2016-11-24 09:47:20'),
(122, 'ORIGEN DENUNCIA', 'ORIGEN DE LAS DENUNCIAS', 1, 19, 1, 'DN-01-02', '2016-11-28 10:15:57'),
(123, 'TIPO DENUNCIA', 'TIPO DE DENUNCIA', 1, 19, 1, 'DN-01-03', '2016-11-28 10:44:58'),
(127, 'TIPOACTUACION', 'TIPOACTUACION', 1, 19, 1, 'TACT', '2016-11-30 14:28:44'),
(130, 'ESTATUS SOLICITUDES', 'ESTATUS DE LAS SOLICITUDE DE DENUNCIAS', 0, 19, 1, 'DN-EST-SOL', '2017-01-13 15:50:12');


INSERT INTO `a006_miscelaneo_detalle` (`pk_num_miscelaneo_detalle`, `cod_detalle`, `ind_nombre_detalle`, `num_estatus`, `fk_a005_num_miscelaneo_maestro`) VALUES
(786, 'D', 'DACCC-D', 1, 121),
(787, 'P', 'DACCC-P', 1, 121),
(788, 'Q', 'DACCC-Q', 1, 121),
(789, 'R', 'DACCC-R', 1, 121),
(790, 'S', 'DACCC-S', 1, 121),
(791, '001', 'CONSEJO COMUNAL', 1, 122),
(792, '002', 'ÓRGANO O ENTE PÚBLICO', 1, 122),
(793, '003', 'OTRO', 1, 122),
(794, '001', 'OBRAS', 1, 123),
(795, '002', 'BIENES', 1, 123),
(796, '003', 'SERVICIOS', 1, 123),
(797, '004', 'OTROS', 0, 123),
(805, '01', 'DENUNCIA', 1, 127),
(806, '02', 'RECLAMO', 1, 127),
(817, 'P', 'PETICIÓN', 1, 127),
(818, 'S', 'SUGERENCIA', 1, 127),
(819, 'Q', 'QUEJA', 1, 127),
(820, 'PE', 'PENDIENTE', 1, 130),
(821, 'AN', 'ANULADA', 0, 130),
(822, 'AP', 'APROBADA', 0, 130);

INSERT INTO `a035_parametros` (`pk_num_parametros`, `ind_descripcion`, `ind_explicacion`, `ind_parametro_clave`, `ind_valor_parametro`, `num_estatus`, `fec_ultima_modificacion`, `fk_a015_num_seguridad_aplicacion`, `fk_a018_num_seguridad_usuario`, `fk_a006_num_miscelaneo_detalle`) VALUES
(43, 'Cantidad de digitos del correlativo', 'Cantidad de digitos del correlativo aplicado en los codigos de las quejas, denuncias, reclamos, peticiones y sugerencias', 'DIGITOS', '4', 1, '2016-11-24 10:24:22', 19, 1, 2);

