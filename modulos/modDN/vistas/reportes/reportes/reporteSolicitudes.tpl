<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Tramites</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12">
                <div class="contain-lg col-lg-4">
                    <div class="contain-lg col-lg-12">
                        <div class="contain-lg col-lg-4">
                            <label class="checkbox-styled">
                                <input type="checkbox" id="checkMun" class="form-control">
                                <span></span>
                            </label>
                            <span>Municipio:</span>
                        </div>
                        <div class="col-lg-8 contain-lg">
                            <div class="form-group" id="municipioError">
                                <select id="municipio" disabled class="form-control select2">
                                    <option value="">...</option>
                                    {foreach item=mun from=$listadoMunicipio}
                                        <option value="{$mun.pk_num_municipio}">{$mun.ind_municipio}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="contain-lg col-lg-4">
                    <div class="contain-lg col-lg-12">
                        <div class="contain-lg col-lg-4">
                            <label class="checkbox-styled">
                                <input type="checkbox" id="checkPar" class="form-control">
                                <span></span>
                            </label>
                            <span>Parroquia:</span>
                        </div>

                        <div class="col-lg-8 contain-lg">
                            <div class="form-group" id="parroquiaError">
                                <select id="parroquia" disabled class="form-control select2" style="height: 27px;" >
                                    <option value="">...</option>
                                    {foreach item=fila from=$listadoParroquia}
                                        <option value="{$fila.pk_num_parroquia}">{$fila.ind_parroquia}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="contain-lg col-lg-4">
                    <div class="contain-lg col-lg-12">
                        <div class="contain-lg col-lg-4">
                            <label class="checkbox-styled">
                                <input type="checkbox" id="checkSec" class="form-control">
                                <span></span>
                            </label>
                            <span>Sector:</span>
                        </div>
                        <div class="col-lg-8 contain-lg">
                            <select  id="sector" disabled class="form-control select2" style="height: 27px;" >
                                <option value="">...</option>
                                {foreach item=fila from=$listadoSector}
                                    <option value="{$fila.pk_num_sector}">{$fila.ind_sector}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="contain-lg col-lg-4">
                    <div class="contain-lg col-lg-12">
                        <div class="contain-lg col-lg-6">
                            <label class="checkbox-styled">
                                <input type="checkbox" id="checkTramite" class="form-control">
                                <span></span>
                            </label>
                            <span>Tipo de Tramite:</span>
                        </div>
                        <div class="col-lg-6 contain-lg">
                            <div class="form-group" id="tramiteError">
                                <select id="tramite" disabled class="form-control select2">
                                    <option value="">...</option>
                                    {foreach item=t from=$tramite}
                                        <option value="{$t.pk_num_miscelaneo_detalle}">{$t.ind_nombre_detalle}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="contain-lg col-lg-4">
                    <div class="contain-lg col-lg-12">
                        <div class="contain-lg col-lg-6">
                            <label class="checkbox-styled">
                                <input type="checkbox" id="checkES" class="form-control">
                                <span></span>
                            </label>
                            <span>Estado Solicitud:</span>
                        </div>

                        <div class="col-lg-6 contain-lg">
                                <select id="estadoSolicitud" disabled class="form-control select2" style="height: 27px;" >
                                    <option value="">...</option>
                                    {foreach item=fila from=$estadoSolicitud}
                                        <option value="{$fila.cod_detalle}">{$fila.ind_nombre_detalle}</option>
                                    {/foreach}
                                </select>
                        </div>
                    </div>
                </div>
                <div class="contain-lg col-lg-4">
                    <div class="contain-lg col-lg-12">
                        <div class="contain-lg col-lg-6">
                            <label class="checkbox-styled">
                                <input type="checkbox" id="checkET" class="form-control">
                                <span></span>
                            </label>
                            <span>Estado Tramite:</span>
                        </div>
                        <div class="col-lg-6 contain-lg">
                            <select  id="estadoTramite"  disabled class="form-control select2" style="height: 27px;" >
                                <option value="">...</option>
                                {foreach item=fila from=$estadoTramite}
                                    <option value="{$fila.cod_detalle}">{$fila.ind_nombre_detalle}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="contain-lg col-lg-4">
                    <div class="contain-lg col-lg-12">
                        <div class="contain-lg col-lg-7">
                            <label class="checkbox-styled">
                                <input type="checkbox" id="checkOrigen" class="form-control">
                                <span></span>
                            </label>
                            <span>Origen de la Denuncia:</span>


                        </div>
                        <div class="col-lg-5 contain-lg">
                            <div class="form-group" id="tramiteError">
                                <select id="origen" disabled class="form-control select2">
                                    <option value="">...</option>
                                    {foreach item=t from=$origen}
                                        <option value="{$t.pk_num_miscelaneo_detalle}">{$t.ind_nombre_detalle}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="contain-lg col-lg-4">
                    <div class="contain-lg col-lg-12">
                        <div class="contain-lg col-lg-6">
                            <label class="checkbox-styled">
                                <input type="checkbox" id="checkTDenuncia" class="form-control">
                                <span></span>
                            </label>
                            <span>Tipo de Denuncia:</span>
                        </div>

                        <div class="col-lg-6 contain-lg">
                            <select id="tipoDenuncia" disabled class="form-control select2" style="height: 27px;" >
                                <option value="">...</option>
                                {foreach item=fila from=$tipoDenuncia}
                                    <option value="{$fila.pk_num_miscelaneo_detalle}">{$fila.ind_nombre_detalle}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="contain-lg col-lg-4">
                    <div class="contain-lg col-lg-12">
                        <div class="contain-lg col-lg-4">
                            <label class="checkbox-styled">
                                <input type="checkbox" id="checkFecha" class="form-control">
                                <span></span>
                            </label>
                            <span>Fecha:</span>
                        </div>
                        <div class="col-lg-4 contain-lg">
                            <div class="form-group" id="desdeError">
                                <input type="text" disabled name="desde" id="desde" value="{date('Y-m')}-01" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-4 contain-lg">
                            <div class="form-group" id="hastaError">
                                <input type="text" disabled name="hasta" id="hasta" value="{date('Y-m-d')}" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12" align="center">
                <button id="accion" class="btn btn-primary ink-reaction btn-raised">
                    Buscar
                </button>
            </div>
        </div>

        <div id="respuestaPdf">

        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('#formulario').submit(function () {
            return false;
        });

        $('#accion').click(function () {
            var url2 = '{$_Parametros.url}modDN/reportes/reportesCONTROL/generarReporteMET/';

            if($('#checkMun').attr('checked')=="checked" && $('#municipio').val().length>0){
                var municipio = $('#municipio').val();
            } else {
                municipio = 'no';
            }
            if($('#checkPar').attr('checked')=="checked" && $('#parroquia').val().length>0){
                var parroquia = $('#parroquia').val();
            } else {
                parroquia = 'no';
            }
            if($('#checkSec').attr('checked')=="checked" && $('#sector').val().length>0){
                var sector = $('#sector').val();
            } else {
                sector = 'no';
            }
            if($('#checkTramite').attr('checked')=="checked" && $('#tramite').val().length>0){
                var tramite = $('#tramite').val();
            } else {
                tramite = 'no';
            }
            if($('#checkES').attr('checked')=="checked" && $('#estadoSolicitud').val().length>0){
                var estadoSolicitud = $('#estadoSolicitud').val();
            } else {
                estadoSolicitud = 'no';
            }
            if($('#checkET').attr('checked')=="checked" && $('#estadoTramite').val().length>0){
                var estadoTramite = $('#estadoTramite').val();
            } else {
                estadoTramite = 'no';
            }

            if($('#checkOrigen').attr('checked')=="checked" && $('#origen').val().length>0){
                var origen = $('#origen').val();
            } else {
                origen = 'no';
            }

            if($('#checkTDenuncia').attr('checked')=="checked" && $('#tipoDenuncia').val().length>0){
                var tipoDenuncia = $('#tipoDenuncia').val();
            } else {
                tipoDenuncia = 'no';
            }

            if($('#checkFecha').attr('checked')=="checked" && $('#desde').val().length>0){
                var desde = $('#desde').val();
                var hasta = $('#hasta').val();
            } else {
                desde = 'no';
                hasta = 'no';
            }

            $('#respuestaPdf').html('<iframe frameborder="0" src="' + url2 +municipio+'/'+parroquia+'/'+sector+'/'+tramite+'/'+estadoSolicitud+'/'+estadoTramite+'/'+origen+'/'+tipoDenuncia+'/'+desde+'/'+hasta+'" width="100%" height="540px"></iframe>');
        });

        $('#checkMun').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#municipio').attr('disabled',false);
            } else {
                $('#municipio').attr('disabled','disabled');
            }
        });

        $('#checkPar').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#parroquia').attr('disabled',false);
            } else {
                $('#parroquia').attr('disabled','disabled');
            }
        });

        $('#checkSec').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#sector').attr('disabled',false);
            } else {
                $('#sector').attr('disabled','disabled');
            }
        });

        $('#checkTramite').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#tramite').attr('disabled',false);
            } else {
                $('#tramite').attr('disabled','disabled');
            }
        });

        $('#checkES').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#estadoSolicitud').attr('disabled',false);
            } else {
                $('#estadoSolicitud').attr('disabled','disabled');
            }
        });

        $('#checkET').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#estadoTramite').attr('disabled',false);
            } else {
                $('#estadoTramite').attr('disabled','disabled');
            }
        });

        $('#checkTDenuncia').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#tipoDenuncia').attr('disabled',false);
            } else {
                $('#tipoDenuncia').attr('disabled','disabled');
            }
        });

        $('#checkOrigen').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#origen').attr('disabled',false);
            } else {
                $('#origen').attr('disabled','disabled');
            }
        });

        $('#checkFecha').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#desde').attr('disabled',false);
                $('#desde').attr('readonly','readonly');
                $('#hasta').attr('disabled',false);
                $('#hasta').attr('readonly','readonly');
            } else {
                $('#desde').attr('disabled','disabled');
                $('#hasta').attr('disabled','disabled');
            }
        });


        $('#checkEstado').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#estado').attr('disabled',false);
            } else {
                $('#estado').attr('disabled','disabled');
            }
        });

        $('#checkBuscar').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#buscar').attr('disabled',false);
            } else {
                $('#buscar').attr('disabled','disabled');
                $('#buscar').val('');
            }
        });

        $('#desde').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#hasta').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });

        $('#municipio').change(function(){
            $.post('{$_Parametros.url}sector/jsonParroquiaMET', { idMunicipio: $(this).val() },function(dato){
                $('#parroquia').html('');
                $('#parroquia').append('<option value="">...</option>');
                $('#s2id_parroquia .select2-chosen').html('Seleccione la Parroquia');
                for (var i =0; i < dato.length; i=i+1) {
                    $('#parroquia').append('<option value="'+dato[i]['pk_num_parroquia']+'">'+dato[i]['ind_parroquia']+'</option>');
                }
            },'json');
        });
        $('#municipio').ready(function(){
            if ($("#municipio").length > 0) {
                var idParroquia=$('#parroquia').attr('parroquia');
                $.post('{$_Parametros.url}sector/jsonParroquiaMET', { idMunicipio: $("#municipio").val() },function(dato){
                    $('#parroquia').html('');
                    $('#parroquia').append('<option value="">...</option>');
                    $('#s2id_parroquia .select2-chosen').html('Seleccione la Parroquia');
                    for (var i =0; i < dato.length; i=i+1) {
                        if(dato[i]['pk_num_parroquia']==idParroquia){
                            $('#parroquia').append('<option selected value="'+dato[i]['pk_num_parroquia']+'">'+dato[i]['ind_parroquia']+'</option>');
                            $('#s2id_v .select2-chosen').html(dato[i]['ind_parroquia']);
                        } else {
                            $('#parroquia').append('<option value="'+dato[i]['pk_num_parroquia']+'">'+dato[i]['ind_parroquia']+'</option>');
                        }
                    }
                },'json');
            }
        });
        $('#parroquia').change(function(){
            $.post('{$_Parametros.url}modDN/denuncias/denunciasCONTROL/jsonSectorMET', { idParroquia: $(this).val() },function(dato){
                $('#sector').html('');
                $('#sector').append('<option value="">...</option>');
                $('#s2id_sector .select2-chosen').html('Seleccione el Sector');
                for (var i =0; i < dato.length; i=i+1) {
                    $('#sector').append('<option value="'+dato[i]['pk_num_sector']+'">'+dato[i]['ind_sector']+'</option>');
                }
            },'json');
        });
        $('#parroquia').ready(function(){
            if ($("#parroquia").length > 0) {
                var num_sector=$('#sector').attr('sector');

                $.post('{$_Parametros.url}modDN/denuncias/denunciasCONTROL/jsonSectorMET', { idParroquia: $('#parroquia').attr('parroquia') },function(dato){
                    $('#sector').html('');
                    $('#sector').append('<option value="">...</option>');
                    $('#s2id_sector .select2-chosen').html('Seleccione el Sector');

                    for (var i =0; i < dato.length; i=i+1) {
                        if(dato[i]['pk_num_sector']==num_sector){
                            $('#sector').append('<option selected value="'+dato[i]['pk_num_sector']+'">'+dato[i]['ind_sector']+'</option>');
                            $('#s2id_sector .select2-chosen').html(dato[i]['ind_sector']);
                        } else {
                            $('#sector').append('<option value="'+dato[i]['pk_num_sector']+'">'+dato[i]['ind_sector']+'</option>');
                        }
                    }
                },'json');
            }
        });

    });
</script>