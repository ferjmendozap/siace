<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Planificaciones</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">

                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th style="width: 15%">Tramite Nro.</th>
                            <th style="width: 10%">Tipo de Solicitud</th>
                            <th style="width: 35%">Ente</th>
                            <th style="width: 15%">Estatus Tramite</th>
                            <th style="width: 10%">Acción</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$cont=0}
                        {foreach item=post from=$listado}
                            {$cont=$cont+1}
                            <tr id="idSolicitud{$post.pk_num_denuncia}">
                                <td>{$post.ind_num_denuncia}</td>
                                <td>{$post.tipoActuacion}
                                <input type="hidden" name="form[txt][tipoActuacion]" id="tipoActuacion" value="{$post.cod_detalle}"/>
                                </td>
                                <td>{$post.ind_nombre_ente}</td>
                                <td>{$post.estadoTra}</td>
                                <td>
                                    <button accion="acta" title="Imprimir Acta"
                                            class="acta logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                            descipcion="El Usuario ha Impreso el Acta de la Solicitud Nro. {$post.cod_solicitud}"
                                            titulo="Imprimir Acta"
                                            cod="{$post.ind_num_denuncia}"
                                            idSolicitud="{$post.pk_num_denuncia}"
                                            data-toggle="modal" data-target="#formModal"
                                            tipoActuacion="{$post.cod_detalle}"
                                            id="acta" >
                                        <i class="fa fa-print"></i>
                                    </button>
                                    <button accion="plan" title="Imprimir Planificación"
                                            class="plan logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                                            descipcion="El Usuario ha Impreso la planificación de la Solicitud Nro. {$post.cod_solicitud}"
                                            titulo="Imprimir Planificación"
                                            idSolicitud="{$post.pk_num_denuncia}"
                                            cod="{$post.ind_num_denuncia}"
                                            data-toggle="modal" data-target="#formModal"
                                            tipoActuacion="{$post.cod_detalle}"
                                            id="plan"  >
                                        <i class="fa fa-clipboard"></i>
                                    </button>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var $url = '{$_Parametros.url}modDN/solicitud/solicitudCONTROL/CrearModificarMET';


        $('#datatable1 tbody').on( 'click', '.acta', function () {
            var idSolicitud = $(this).attr('idSolicitud');
            var cod = $(this).attr('cod');
            var url='{$_Parametros.url}modDN/solicitud/solicitudCONTROL/imprimirActaMET/?idSolicitud='+idSolicitud+'&cod='+cod;
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( url,{  },function(dato){
                $('#modalAncho').css("width", "90%");
                $('#ContenidoModal').html('<iframe frameborder="0" src="'+url+'" width="100%" height="640px"></iframe>');

            });
        });

        $('#datatable1 tbody').on( 'click', '.plan', function () {
            var idSolicitud = $(this).attr('idSolicitud');
            var cod = $(this).attr('cod');
            var url='{$_Parametros.url}modDN/reportes/reportePlanificacionCONTROL/imprimirPlanMET/?tramite='+idSolicitud+'&cod='+cod;
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( url,{  },function(dato){
                $('#modalAncho').css("width", "90%");
                $('#ContenidoModal').html('<iframe frameborder="0" src="'+url+'" width="100%" height="640px"></iframe>');

            });
        });

    });
</script>