<style type="text/css">
    {literal}
    .fondo-total{background-color:#bbb;}
    .fondo-Subtotal{background-color:#bcd;}

    {/literal}
</style>
    <div class="modal-body" style="padding:0;">
        <div class="row">
            <div class="col-lg-12">
                <div class="card"><br/>
                    <div class="col-sm-12">
                        <!--Código de la  Solicitud-->
                        <div class="col-sm-4">
                            <div class="form-group"
                                 id="idSolicitud" style="margin-top: -10px;">
                                <label for="cod_denuncia"><i
                                            class="md md-markunread-mailbox"></i> Solicitud Nro.  {$formDB.cod_solicitud} </label>

                            </div>
                        </div>
                        <!--Código de la  Denuncia-->
                        <div class="col-sm-4">
                            <div class="form-group"
                                 id="divDenuncia" style="margin-top: -10px;">
                                <label for="cod_denuncia"><i
                                            class="md md-markunread-mailbox"></i> Tramite Nro.  {if isset($formDB.ind_num_denuncia) and $formDB.ind_num_denuncia!='' and $formDB.ind_num_denuncia!='NULL'}{$formDB.ind_num_denuncia}{else}{$codDenuncia}{/if}</label>

                            </div>
                        </div>

                        <!--Estado de la Denuncia-->
                        <div class="col-sm-4">
                            <div class="form-group"
                                 id="id_enteError" style="margin-top: -10px;" >
                                <label for="ind_estado" ><i
                                            class="md md-info-outline"></i>Estado:  {if isset($formDB.estado)} {$formDB.estado} {/if}</label>
                            </div>
                        </div>

                    </div>
                    <div class="card-body">
                        <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                            <form id="formAjax" action="{$_Parametros.url}modDN/denuncias/denunciasCONTROL/CrearModificarMET" class="form floating-label form-validation" class="form" role="form" method="post">

                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">INFORMACIÓN GENERAL</span></a></li>
                                    <li><a id="tab_2" href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">DENUNCIANTES</span></a></li>
                                    <li><a id="tab_3" href="#tab3" data-toggle="tab"><span class="step">3</span> <span class="title">PRUEBAS</span></a></li>
                                    <li><a id="tab_4" href="#tab4" data-toggle="tab"><span class="step">4</span> <span class="title">FUNCIONARIOS ASIGNADOS</span></a></li>
                                    <li><a id="tab_5" href="#tab5" data-toggle="tab"><span class="step">5</span> <span class="title">ACTIVIDADES</span></a></li>
                                </ul>
                            </div>

                            <div class="tab-content clearfix">

                                <!--INFORMACIÓN DE LA DENUNCIA-->
                                <div class="tab-pane active" id="tab1">

                                    <input type="hidden" value="1" name="valido"/>
                                    <input type="hidden" value="{if isset($idDenuncia)}{$idDenuncia}{/if}" id="idDenuncia" name="form[int][idDenuncia]" />
                                    <input type="hidden" value="{if isset($formDB.ind_estatus)}{$formDB.ind_estatus}{else}PE{/if}" id="ind_estatus" name="form[txt][ind_estatus]"/>
                                    <input type="hidden" value="{$idSolicitud}" id="idSolicitud" name="form[int][idSolicitud]" />
                                    <input type="hidden" value="{if isset($formDB.ind_num_denuncia) and $formDB.ind_num_denuncia!='' and $formDB.ind_num_denuncia!='NULL'}{$formDB.ind_num_denuncia}{else}{$codDenuncia}{/if}" id="codDenuncia" name="form[txt][codDenuncia]" />
                                    <input type="hidden" value="{$tipoActuacionTexto}" id="tipoActuacionTexto" name="form[alphaNum][tipoActuacionTexto]" />
                                    <input type="hidden" value="{$acc}" id="acc" name="form[txt][acc]" />
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <div class="card" >
                                                <div class="card-head card-head-xs style-primary text-center">
                                                    <header class="text-center">INFORMACIÓN DE LA SOLICITUD</header>

                                                </div> <br/>
                                                <div class="card-body" style="padding:0;">

                                                    <div class="col-sm-12">
                                                        <!--organismo ejecutante-->

                                                        <div class="col-sm-6">
                                                            <div class="form-group" id="id_contraloriaError" style="margin-bottom: 20px;">
                                                                <label for="contraloria"><i
                                                                            class="md md-account-balance"></i> Lugar</label>
                                                                <select id="id_contraloria" name="form[int][id_contraloria]" class="form-control select2" style="height: 27px;" disabled="disabled">
                                                                    {foreach item=fila from=$listadoContraloria}
                                                                        <option value="{$fila.pk_num_organismo}" selected>{$fila.ind_descripcion_empresa}</option>
                                                                    {/foreach}
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!--Fecha registro-->
                                                        <div class="col-sm-6">

                                                            <div class="form-group" style="margin-bottom: 20px;">

                                                                <label for="fe_registro"
                                                                       class="control-label" > <i class="md md-access-time"></i> Fecha y Hora de la Solicitud:</label>

                                                                <div class="col-sm-4">
                                                                    <input type="text"
                                                                           name="form[txt][fe_registro]"
                                                                           class="form-control" id="fe_registro"
                                                                           value="{if isset($formDB.fec_elaborado)}{($formDB.fec_elaborado)|date_format:"%d-%m-%Y"}{else}{$fecha_actual}{/if}"
                                                                           readonly >
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <input type="text"
                                                                           name="form[txt][fe_hora_registro]"
                                                                           class="form-control" id="fe_hora_registro"
                                                                           value="{if isset($formDB.fec_elaborado)}{($formDB.fec_elaborado)|date_format:"%H:%M:%S"}{else}{$hora_actual}{/if}"
                                                                           readonly >
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <!--dependencia interna de la contraloría-->

                                                        <div class="col-sm-6">
                                                            <div class="form-group" id="id_dependenciaError"
                                                                 style="margin-bottom: 10px;">
                                                                <label for="id_dependencia"><i
                                                                            class="fa fa-sitemap"></i> Dependencia:</label>
                                                                <select id="id_dependencia" name="form[int][id_dependencia]" class="form-control select2" style="height: 27px;" disabled >
                                                                    <option value="">Seleccione..</option>
                                                                    {foreach item=fila from=$selectDependencia}
                                                                        {if $fila.pk_num_dependencia eq $formDB.fk_a004_num_dependencia}
                                                                            <option value="{$fila.pk_num_dependencia}" selected> {$fila.ind_dependencia}</option>
                                                                        {else}
                                                                            <option value="{$fila.pk_num_dependencia}"  > {$fila.ind_dependencia}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!--centro de costo-->
                                                        <div class="col-sm-6">
                                                            <div class="form-group" id="id_centro_costoError"  style="margin-bottom: 10px;">
                                                                <label for="id_centro_costo"><i
                                                                            class="fa fa-tag"></i> Centro de Costo:</label>
                                                                <select centro="{$formDB.fk_a023_num_centro_costo}"id="id_centro_costo" name="form[int][id_centro_costo]" class="form-control select2" style="height: 27px;" disabled >
                                                                    <option value=""></option>
                                                                    {foreach item=fila from=$selectCentroCosto}
                                                                        {if $fila.pk_num_centro_costo eq $formDB.fk_a023_num_centro_costo}
                                                                            <option value="{$fila.pk_num_centro_costo}" selected>{$fila.ind_descripcion_centro_costo}</option>
                                                                        {else}
                                                                            <option value="{$fila.pk_num_centro_costo}">{$fila.ind_descripcion_centro_costo}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <!--organismo o ente denunciado-->

                                                        <div class="col-sm-6">
                                                            <div class="form-group"
                                                                 id="id_enteError" style="margin-bottom: 10px;">
                                                                <label for="id_ente"><i
                                                                            class="md md-domain"></i> Organismo/Ente Denunciado</label>
                                                                <select id="id_ente" name="form[int][id_ente]" class="form-control select2" style="height: 27px;" disabled >
                                                                    <option value="">Seleccione..</option>
                                                                    {foreach item=fila from=$selectEnte}
                                                                        {if $fila.pk_num_ente eq $formDB.fk_a039_num_ente}
                                                                            <option value="{$fila.pk_num_ente}" selected>{if $fila.num_ente_padre!=0}  - {/if} {$fila.ind_nombre_ente} </option>
                                                                        {else}
                                                                            <option value="{$fila.pk_num_ente}">{if $fila.num_ente_padre!=0}  -  {/if} {$fila.ind_nombre_ente}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!--origen de los fondos o recursos -->
                                                        <div class="col-sm-6">

                                                            <div class="form-group"
                                                                 id="id_ente_recursos" style="margin-bottom: 10px;">
                                                                <label for="id_ente_recursos"><i
                                                                            class="md md-local-atm"></i> Origen de los Recursos</label>
                                                                <select id="id_ente_recursos" name="form[int][id_ente_recursos]" class="form-control select2" style="height: 27px;" disabled >
                                                                    <option value="">Seleccione..</option>
                                                                    {foreach item=fila from=$selectEnte}
                                                                        {if $fila.pk_num_ente eq $formDB.fk_a039_num_ente_recursos}
                                                                            <option value="{$fila.pk_num_ente}" selected>{if $fila.num_ente_padre!=0}  - {/if} {$fila.ind_nombre_ente} </option>
                                                                        {else}
                                                                            <option value="{$fila.pk_num_ente}">{if $fila.num_ente_padre!=0}  -  {/if} {$fila.ind_nombre_ente}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <!--MUNICIPIO:-->
                                                        <div class="col-sm-6">
                                                            <div class="form-group" id="idMunicipioError" style="margin-bottom: 10px;">
                                                                <label for="idMunicipio"><i
                                                                            class="md md-map"></i> Municipio</label>
                                                                <select id="idMunicipio" name="form[int][idMunicipio]" disabled
                                                                        class="form-control select2-list select2">
                                                                    <option value="">Seleccione el Municipio</option>
                                                                    {foreach item=i from=$listadoMunicipio}
                                                                        {if $i.pk_num_municipio==$formDB.fk_a011_num_municipio}
                                                                            <option value="{$i.pk_num_municipio}" selected>{$i.ind_municipio}</option>
                                                                        {else}
                                                                            <option value="{$i.pk_num_municipio}">{$i.ind_municipio}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <!--Parroquia:-->

                                                        <div class="col-sm-6">
                                                            <div class="form-group" id="idParroquiaError" style="margin-bottom: 10px;">
                                                                <label for="idParroquia"><i
                                                                            class="md md-map"></i> Parroquia</label>
                                                                <select parroquia="{$formDB.fk_a012_num_parroquia}"id="idParroquia" name="form[int][idParroquia]" class="form-control select2" style="height: 27px;" disabled >
                                                                    <option value="">Seleccione..</option>
                                                                    {foreach item=fila from=$listadoParroquia}
                                                                        {if $formDB.fk_a012_num_parroquia==$fila.pk_num_parroquia}
                                                                            <option value="{$fila.pk_num_parroquia}" selected>{$fila.ind_parroquia}</option>
                                                                        {else}
                                                                            <option value="{$fila.pk_num_parroquia}">{$fila.ind_parroquia}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-sm-12">

                                                        <!--Sector:-->
                                                        <div class="col-sm-6">
                                                            <div class="form-group" id="num_sectorError" style="margin-bottom: 10px;">
                                                                <label for="num_sector"><i
                                                                            class="md md-my-location"></i> Sector</label>
                                                                <select sector="{$formDB.pk_num_sector}" id="num_sector" name="form[int][num_sector]" class="form-control select2" style="height: 27px;" disabled >
                                                                    <option value="">Seleccione..</option>
                                                                    {foreach item=fila from=$listadoSector}
                                                                        {if $formDB.pk_num_sector ==$fila.pk_num_sector}
                                                                            <option value="{$fila.pk_num_sector}" selected>{$fila.ind_sector}</option>
                                                                        {else}
                                                                            <option value="{$fila.pk_num_sector}">{$fila.ind_sector}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <!--Duracion no afecta -->
                                                        <div class="col-sm-6">
                                                            <div class="form-group" id="tActuacionError" style="margin-bottom: 10px;">
                                                                <label for="tActuacion"><i
                                                                            class="md md-beenhere"></i> Tipo de Actuación:</label>
                                                                <select id="tActuacion" name="form[int][tActuacion]" class="form-control select2" style="height: 27px;"
                                                                        >
                                                                    <option value="">Seleccione..</option>
                                                                    {foreach item=fila from=$tipoActuacion}
                                                                        {if isset($fila.pk_num_miscelaneo_detalle) and $fila.pk_num_miscelaneo_detalle==$formDB.fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion}
                                                                            <option value="{$fila.pk_num_miscelaneo_detalle}" selected="selected">{$fila.ind_nombre_detalle}</option>
                                                                        {else}
                                                                            <option value="{$fila.pk_num_miscelaneo_detalle}">{$fila.ind_nombre_detalle}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <!--origen de la actuacion:-->
                                                        <div class="col-sm-6">
                                                            <div class="form-group" id="idorigen_denunciaError" style="margin-bottom: 10px;">
                                                                <label for="idorigen_denuncia"><i
                                                                            class="fa fa-street-view"></i> Origen de la Denuncia</label>
                                                                <select id="idorigen_denuncia" name="form[int][idorigen_denuncia]" class="form-control select2" style="height: 27px;"  disabled >
                                                                    <option value="">Seleccione..</option>
                                                                    {foreach item=orig from=$origen}
                                                                        {if isset($formDB.fk_a006_pk_num_miscelaneo_detalle_origen) and $formDB.fk_a006_pk_num_miscelaneo_detalle_origen == $orig.pk_num_miscelaneo_detalle}
                                                                            <option value="{$orig.pk_num_miscelaneo_detalle}" selected>{$orig.ind_nombre_detalle}</option>
                                                                        {else}
                                                                            <option value="{$orig.pk_num_miscelaneo_detalle}">{$orig.ind_nombre_detalle}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!--Prorroga-->
                                                        <div class="col-sm-6">
                                                            <div class="form-group" id="ind_especificacion_origen_denuncia" style="margin-bottom: 10px;">
                                                                <label for="esporigen_denuncia"><i
                                                                            class="fa fa-edit"></i> Especifique</label>
                                                                <input type="text" id="ind_especificacion_origen_denuncia" class="form-control" maxlength="250"
                                                                       name="form[txt][ind_especificacion_origen_denuncia]" value="{if isset($formDB.ind_especificacion_origen_denuncia)}{$formDB.ind_especificacion_origen_denuncia}{/if}"
                                                                         readonly >
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="form-group" id="idtipo_denunciaError" style="margin-bottom: 10px;">
                                                                <label for="idtipo_denuncia"><i
                                                                            class="fa fa-street-view"></i> Tipo de Denuncia</label>
                                                                <select id="idtipo_denuncia" name="form[int][idtipo_denuncia]" class="form-control select2" style="height: 27px;" disabled >
                                                                    <option value="">Seleccione..</option>
                                                                    {foreach item=tip from=$tipo}
                                                                        {if isset($formDB.fk_a006_pk_num_miscelaneo_detalle_tipo_denuncia) and $formDB.fk_a006_pk_num_miscelaneo_detalle_tipo_denuncia == $tip.pk_num_miscelaneo_detalle}
                                                                            <option value="{$tip.pk_num_miscelaneo_detalle}" selected>{$tip.ind_nombre_detalle}</option>
                                                                        {else}
                                                                            <option value="{$tip.pk_num_miscelaneo_detalle}">{$tip.ind_nombre_detalle}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group" id="ind_especificacion_tipo_denuncia" style="margin-bottom: 10px;">
                                                                <label for="esptipo_denuncia"><i
                                                                            class="fa fa-edit"></i> Especifique</label>
                                                                <input type="text" id="ind_especificacion_tipo_denuncia" class="form-control" maxlength="250"
                                                                       id="ind_especificacion_tipo_denuncia" name="form[txt][ind_especificacion_tipo_denuncia]" value="{if isset($formDB.ind_especificacion_tipo_denuncia)}{$formDB.ind_especificacion_tipo_denuncia}{/if}"
                                                                        readonly >
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-sm-12">
                                                        <!---descripcion-->
                                                        <div class="col-sm-2">
                                                            <label for="ind_descripcion_denuncia" class="control-label" style="margin-bottom: 10px;"><i
                                                                        class="fa fa-edit"></i>Descripción:</label>
                                                        </div>
                                                        <div class="col-sm-10">
                                                            <div style="margin-left: 10px; " id="ind_descripcion_denunciaError" >
                                                                    <textarea id="ind_descripcion_denuncia" class="form-control" rows="2" name="form[txt][ind_descripcion_denuncia]"
                                                                             readonly >{if isset($formDB.ind_descripcion_denuncia)}{$formDB.ind_descripcion_denuncia}{/if}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <!---Motivo-->
                                                        <div class="col-sm-2 ">
                                                            <label for="ind_motivo_origen" class="control-label" style="margin-bottom: 10px;"><i
                                                                        class="fa fa-edit"></i>Motivo:</label>
                                                        </div>
                                                        <div class="col-sm-10">
                                                            <div style="margin-left: 10px;" id="ind_motivo_origenError" >
                                                                    <textarea id="ind_motivo_origen" class="form-control" rows="2" name="form[txt][ind_motivo_origen]"
                                                                            readonly >{if isset($formDB.ind_motivo_origen)}{$formDB.ind_motivo_origen}{/if}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <!---propuesta-->
                                                        <div class="col-sm-2">
                                                            <label for="ind_propuesta" class="control-label" style="margin-bottom: 10px;"><i
                                                                        class="fa fa-edit"></i>Propuesta:</label>
                                                        </div>
                                                        <div class="col-sm-10">
                                                            <div style="margin-left: 10px; margin-bottom: 10px" id="ind_propuestaError" >
                                                                    <textarea id="ind_propuesta" class="form-control" rows="2" name="form[txt][ind_propuesta]"
                                                                            readonly >{if isset($formDB.ind_propuesta)}{$formDB.ind_propuesta}{/if}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-12">
                                                        <div class="col-lg-8">
                                                            <div class="card" >
                                                            <div class="card-head card-head-xs style-primary text-center">
                                                                <header class="text-center">RECIBIDO POR</header>
                                                                <br/>
                                                            </div>

                                                            <div class="card-body no-padding">
                                                                <div class="table-responsive no-margin">
                                                                    <table class="table table-striped no-margin" id="contenidoTabla4">
                                                                        <thead>
                                                                        <tr>
                                                                            <th class="text-rigth" width="30%">Documento de identidad</th>
                                                                            <th width="40%">Nombre completo</th>
                                                                            <th width="30%">Dependencia</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody id="RecibidoPor">

                                                                        {if isset($formTab)}
                                                                            {$n=1}
                                                                            {$cant=count($formTab)}
                                                                            {foreach item=dn from=$formTab}
                                                                                <tr class="idRecibido{$dn.pk_num_receptor}">
                                                                                    <input type="hidden" value="{$dn.fk_rhb001_num_empleado}" name="form[int][recibido][idPersona][{$n}]"/>
                                                                                    <input type="hidden" value="{$dn.cedula}" name="form[txt][recibido][ind_cedula_documento][{$n}]"/>
                                                                                    <input type="hidden" value="{$dn.nombre}" name="form[txt][recibido][persona][{$n}]"/>
                                                                                    <td width="30%">
                                                                                        {$dn.cedula}
                                                                                    </td>
                                                                                    <td width="40%">
                                                                                        {$dn.nombre}
                                                                                    </td>
                                                                                    <td width="30%">
                                                                                        {$dn.ind_dependencia}
                                                                                    </td>
                                                                                </tr>
                                                                                {$n=$n+1}
                                                                            {/foreach}
                                                                        {/if}
                                                                        </tbody>

                                                                    </table>
                                                                </div>
                                                            </div>



                                                        </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <div class="card" >
                                                            <div class="card-head card-head-xs style-primary text-center">
                                                                <header class="text-center">FECHA DE RECEPCIÓN</header>
                                                                <br/>
                                                            </div>

                                                            <div class="card-body no-padding">
                                                                <div class="table-responsive no-margin">
                                                                    <div class="input-group " style="margin-bottom: 30px;">
                                                                        <input type="text"
                                                                           style="text-align: center"
                                                                           name="form[txt][fe_recibido]"
                                                                           class="form-control" id="fe_recibido"
                                                                           value="{if isset($formDB.fec_recepcion)} {($formDB.fec_recepcion)|date_format:"%d-%m-%Y"}{else}{$fecha_actual}{/if}"
                                                                           readonly >
                                                                     </div>
                                                                </div>
                                                            </div>



                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="card" >
                                                <div class="card-head card-head-xs style-primary text-center">
                                                    <header class="text-center">PLANIFICACIÓN</header>
                                                    <br/>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="col-sm-12 form-group ">
                                                        <div class="input-group date" >
                                                        <label for="fe_inicio"
                                                               class="control-label" style="margin-top: 20px;"> Fecha de Inicio:</label>
                                                            <input type="text" style="text-align: center"
                                                                   name="form[txt][fe_inicio]"
                                                                   class="form-control"
                                                                   id="datepicker"
                                                                   onchange="metRecalcular()"
                                                                   value="{if isset($formDB.fec_inicio)}{$formDB.fec_inicio|date_format:"%d-%m-%Y"}{else}{$fecha_actual}{/if}"
                                                                   size="40%"
                                                            {if $acc=='aprobar'}readonly{/if}>
                                                            <span class="input-group-addon"><i
                                                                        class="fa fa-calendar"></i></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <!--fecha termino de la denuncia-->
                                                    <div class="col-sm-12 form-group" >
                                                        <label for="fec_termino"
                                                               class="control-label" style="margin-bottom: 20px;"> Fecha de termino:</label>

                                                        <input type="text" style="text-align: center"
                                                               name="form[txt][fec_termino]"
                                                               id="fec_termino"
                                                               class="form-control" id="fec_termino"
                                                               value="{if isset($fec_termino)} {$fec_termino|date_format:"%d-%m-%Y"} {else}{$fecha_termino}{/if}"
                                                               readonly size="40%">
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <!--fecha termino actuacion-->
                                                    <div class="col-sm-12 form-group">
                                                        <label for="duracion_afecta"
                                                               class="control-label" style="margin-bottom: 20px"> Duración Afecta:</label>

                                                        <input type="text" style="text-align: center"
                                                               name="form[txt][num_duracion]"
                                                               class="form-control" id="num_duracion"
                                                               value="{if isset($formDB.num_duracion)} {$formDB.num_duracion}{else}{$dias_actuacion}{/if}"
                                                               readonly size="40%">
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <!--Duracion no afecta -->
                                                    <div class="col-sm-12 form-group" >
                                                        <label for="duracion_no_afecta"
                                                               class="control-label" style="margin-bottom: 20px"> Duración No Afecta:</label>

                                                        <input type="text" style="text-align: center"
                                                               name="num_duracion_no_afecta"
                                                               class="form-control" id="num_duracion_no_afecta"
                                                               value="{if isset($formDB.num_duracion_no_afecta)} {$formDB.num_duracion_no_afecta}{else}0{/if}"
                                                               readonly size="40%">
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <!--Prorroga -->
                                                    <div class="col-sm-12 form-group">
                                                            <label for="prorroga"
                                                                   class="control-label" style="margin-bottom: 20px"> Prorroga:</label>

                                                            <input type="text" style="text-align: center"
                                                                   name="num_prorroga"
                                                                   class="form-control" id="num_prorroga"
                                                                   value="{if isset($formDB.num_prorroga)} {$formDB.num_prorroga}{else}0{/if}"
                                                                   readonly size="40%">
                                                    </div>

                                                </div>

                                                <div class="col-sm-12">
                                                    <!--Duracion no afecta-->
                                                    <div class="col-sm-12 form-group">
                                                        <label for="duracion_t"
                                                               class="control-label" style="margin-bottom: 20px"> Duración Total:</label>

                                                        <input type="text" style="text-align: center"
                                                               name="form[txt][duracion_total]"
                                                               class="form-control" id="duracion_total"
                                                               value="{if isset($formDB.duracion_total)} {$formDB.duracion_total}{else}{$dias_actuacion}{/if}"
                                                               readonly size="40%">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--cierre INFORMACIÓN DE LA DENUNCIA-->


                                <!--PANEL DENUNCIANTES-->
                                <div class="tab-pane" id="tab2">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-head card-head-xs style-primary text-center">
                                                <header class="text-center">
                                                    DENUNCIANTES</header>
                                            </div>
                                            <div class="card-body">
                                                <div class="col-lg-12">
                                                    <div class="card">
                                                        <div class="card-body no-padding">
                                                            <div class="table-responsive no-margin">
                                                                <table class="table table-striped no-margin" id="contenidoTabla3">
                                                                    <thead>
                                                                    <tr>
                                                                        <th class="text-rigth" width="30%">Documento de identidad</th>
                                                                        <th width="60%">Nombre completo</th>

                                                                    </tr>
                                                                    </thead>
                                                                    <tbody id="Denunciantes">
                                                                    {if isset($formTab2)}
                                                                        {$n=count($formTab2)}
                                                                        {foreach item=dn from=$formTab2}
                                                                            <tr class="idPersona{$dn.pk_num_actuacion_persona}">
                                                                                <input type="hidden" value="{$dn.fk_a003_num_persona}" name="form[int][denunciantes][idPersona][{$n}]"/>
                                                                                <input type="hidden" value="{$dn.cedula}" name="form[txt][denunciantes][ind_cedula_documento][{$n}]"/>
                                                                                <input type="hidden" value="{$dn.nombre}" name="form[txt][denunciantes][persona][{$n}]"/>
                                                                                <td width="30%">
                                                                                    {$dn.cedula}
                                                                                </td>
                                                                                <td width="60%">
                                                                                    {$dn.nombre}
                                                                                </td>

                                                                            </tr>
                                                                            {$n=$n-1}
                                                                        {/foreach}
                                                                    {/if}
                                                                    </tbody>

                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!---cierre PANEL DENUNCIANTES-->

                                <!---PANEL DE PRUEBAS-->
                                <div class="tab-pane" id="tab3">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-head card-head-xs style-primary text-center">
                                                <header class="text-center">
                                                    DOCUMENTOS CONSIGNADOS / PRUEBAS</header>
                                            </div>
                                            <div class="card">
                                                <div class="card-body no-padding">
                                                    <div class="table-responsive no-margin">

                                                        <table class="table table-striped no-margin" id="contenidoTabla">

                                                            <thead>
                                                            <tr>
                                                                <th class="text-center">#</th>
                                                                <th>Descripción *</th>
                                                                <th>Numeración</th>
                                                                <th>Cantidad *</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            {if isset($formTab3)}
                                                                {$FilaNro=1}
                                                                {foreach item=opciones from=$formTab3}

                                                                    <tr>
                                                                        <td class="text-right" style="vertical-align: middle;">
                                                                            {$FilaNro}
                                                                        </td>
                                                                        <td style="vertical-align: middle;">
                                                                            <div class="col-sm-10">
                                                                                <div class="form-group floating-label" id="ind_prueba{$FilaNro}Error">
                                                                                    <input type="text" class="form-control" value="{$opciones.ind_descripcion}" name="form[txt][prueba][secuencia][ind_prueba][{$FilaNro}]" id="ind_prueba{$FilaNro}"
                                                                                            {if (isset($ver) and $ver==1) or $acc=='aprobar'} readonly {/if}>
                                                                                    <label for="ind_prueba{$FilaNro}"><i class="md md-insert-comment"></i> Descripción </label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td style="vertical-align: middle;">
                                                                            <div class="col-sm-6">
                                                                                <div class="form-group floating-label" id="ind_numero_documento{$opciones.FilaNro}Error">
                                                                                    <input type="text" class="form-control" value="{$opciones.ind_numero_documento}" name="form[txt][prueba][secuencia][ind_numero_documento][{$FilaNro}]" id="ind_numero_documento{$FilaNro}"
                                                                                            {if (isset($ver) and $ver==1) or $acc=='aprobar'} readonly {/if}>
                                                                                    <label for="ind_numero_documento{$FilaNro}"><i class="md md-insert-comment"></i> Número </label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td style="vertical-align: middle;">
                                                                            <div class="col-sm-6">
                                                                                <div class="form-group floating-label" id="num_cantidad_prueba{$FilaNro}Error">
                                                                                    <input type="text" class="form-control" value="{$opciones.num_cantidad}" name="form[txt][prueba][secuencia][num_cantidad_prueba][{$FilaNro}]" id="num_cantidad_prueba{$FilaNro}"
                                                                                            {if (isset($ver) and $ver==1) or $acc=='aprobar'} readonly {/if}>
                                                                                    <label for="num_cantidad_prueba{$FilaNro}"><i class="md md-insert-comment"></i> Cantidad </label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td  class="text-center" style="vertical-align: middle;">
                                                                            <button class="btn ink-reaction btn-raised btn-xs btn-danger delete" {if (isset($ver) and $ver==1) or $acc=='aprobar'} disabled {/if}>
                                                                                <i class="md md-delete"></i>
                                                                            </button>
                                                                        </td>

                                                                    </tr>
                                                                    {$FilaNro=$FilaNro+1}
                                                                {/foreach}
                                                            {/if}
                                                            </tbody>

                                                            <tfoot>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <div class="col-sm-offset-4 col-sm-6">
                                                                        <button type="button" id="nuevoDoc" class="btn btn-info ink-reaction btn-raised"
                                                                                {if (isset($ver) and $ver==1) or $acc=='aprobar'} disabled {/if}>
                                                                            <i class="md md-add"></i> Insertar Nuevo Documento
                                                                        </button>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!---cierre PANEL DE PRUEBAS-->

                                <!--PANEL AUDITORES-->
                                <div class="tab-pane" id="tab4">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-head card-head-xs style-primary text-center">
                                                <header class="text-center">
                                                    AUDITORES</header>
                                            </div>
                                            <div class="card-body">
                                                <div class="col-lg-12">
                                                    <div class="card">
                                                        <div class="card-body no-padding">
                                                            <div class="table-responsive no-margin">
                                                                <table class="table table-striped no-margin" id="contenidoTabla2">
                                                                    <thead>
                                                                    <tr>
                                                                        <th class="text-rigth" width="20%">Documento de identidad</th>
                                                                        <th width="30%">Nombre completo</th>
                                                                        <th width="30%">Dependencia</th>
                                                                        <th width="10%">Coordinador</th>
                                                                        <th width="10%"></th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody id="Auditores">
                                                                    {if isset($formAuditor)}
                                                                        {$n=1}
                                                                        {$cant=count($formTab)}
                                                                        {foreach item=auditor from=$formAuditor}
                                                                            <tr class="idPersona">
                                                                                <input type="hidden" value="{$auditor.codigo}" name="form[txt][auditor][idPersona][{$n}]" idPersona="{$auditor.codigo}"/>
                                                                                <input type="hidden" value="{$auditor.cedula}" name="form[txt][auditor][ind_cedula_documento][{$n}]"/>
                                                                                <input type="hidden" value="{$auditor.nombre}" name="form[txt][auditor][persona][{$n}]"/>
                                                                                <td width="20%">
                                                                                    {$auditor.cedula}
                                                                                </td>
                                                                                <td width="30%">
                                                                                    {$auditor.nombre}
                                                                                </td>
                                                                                <td width="30%">
                                                                                    {$auditor.ind_dependencia}
                                                                                </td>
                                                                                <td width="10%" align="center">
                                                                                    <div class="radio radio-styled">
                                                                                        <label>
                                                                                            <input type="radio" value="{$auditor.fk_a003_num_persona}" id="coordinador"
                                                                                                   name="form[txt][auditor][coordinador]"
                                                                                            {if $auditor.num_coordinador==1} checked {/if}
                                                                                            {if $acc=='aprobar'}readonly{/if}>
                                                                                            <span></span>
                                                                                        </label>
                                                                                    </div>
                                                                                </td>
                                                                                <td width="10%">
                                                                                    <button class="eliminarAuditor btn ink-reaction btn-raised btn-xs btn-danger delete"
                                                                                            borrar="idAuditor{$auditor.pk_num_actuacion_persona}" {if (isset($ver) and $ver==1) or $acc=='aprobar'} disabled {/if}>
                                                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                                                    </button>
                                                                                </td>
                                                                            </tr>
                                                                            {$n=$n+1}
                                                                        {/foreach}
                                                                    {/if}
                                                                    </tbody>
                                                                    <tfoot>
                                                                    <tr>
                                                                        <td colspan="5">
                                                                            <div class="col-sm-offset-3 col-sm-6">
                                                                                <button
                                                                                        type="button"
                                                                                        class="btn btn-info ink-reaction btn-raised"
                                                                                        id="agregarAuditor"
                                                                                        data-toggle="modal" data-target="#formModal2"
                                                                                        data-keyboard="false" data-backdrop="static"
                                                                                        tipo="AU"
                                                                                        rows="{$cant}"
                                                                                        titulo="Insertar Funcionario"
                                                                                        url="{$_Parametros.url}modDN/solicitud/solicitudCONTROL/personasMET/MODAL"
                                                                                        {if (isset($ver) and $ver==1) or $acc=='aprobar'} disabled {/if}
                                                                                ><i class="md md-add"></i> Insertar Funcionario
                                                                                </button>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    </tfoot>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!---cierre PANEL AUDITORES-->

                                <!--PANEL DE ACTIVIDADES-->
                                <div class="tab-pane" id="tab5">
                                    <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-head card-head-xs style-primary text-center">
                                                        <header class="text-center">Actividades</header>
                                                    </div>
                                                    <div class="card-body">
                                                        {*<div class="col-lg-12">*}
                                                        <div class="table-responsive">
                                                            <table id="tb_actividades" class="table no-margin table-condensed table-bordered" style="width: 1120px; display:block">
                                                                <thead>
                                                                <tr>
                                                                    <th class="text-center" style="width: 2%;">Est.</th>
                                                                    <th class="text-center" style="width: 41%;">Actividad</th>
                                                                    <th class="text-center" style="width: 4%;">Días</th>
                                                                    <th class="text-center" style="width: 8%;">Inicio</th>
                                                                    <th class="text-center" style="width: 8%;">Fin</th>
                                                                    <th class="text-center" style="width: 3%;">Prorr.</th>
                                                                    <th class="text-center" style="width: 8%;">Inicio Real</th>
                                                                    <th class="text-center" style="width: 8%;">Fin Real</th>
                                                                    <th class="text-center" style="width: 3%;">A.P.</th>
                                                                    <th class="text-center" style="width: 3%;">A.A.</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody  id="actividades" style="height: 400px; width: 100%; display:inline-block; overflow: auto">
                                                                {$SubTotal_dias=0}
                                                                {$SubTotal_diasProrroga=0}
                                                                {$Total_dias=0}
                                                                {$Totaldias_prorroga=0}
                                                                {$totalDiasNoAfecta=0}
                                                                {$cuenta_fase=0}
                                                                {$filasubtotal=0}
                                                                {if $listaActividades|count > 0}
                                                                    {$contador=0}
                                                                    {foreach key=nombre_fase item=fases from=$listaActividades}

                                                                        <tr class="info">
                                                                            <td width="560" colspan="10" class="">
                                                                                {$nombre_fase}
                                                                            </td>
                                                                        </tr>
                                                                        {foreach item=fila from=$fases}

                                                                            {if $fila.num_flag_no_afecto_plan ==0}
                                                                                {$SubTotal_dias=$SubTotal_dias+$fila.num_dias_duracion_actividad}
                                                                                {$SubTotal_diasProrroga=$SubTotal_diasProrroga+$fila.num_dias_prorroga_actividad}
                                                                                {$afecta_plan=1}
                                                                                {$no_afecta_plan='md md-check'}
                                                                            {else}
                                                                                {$afecta_plan=0}
                                                                                {$no_afecta_plan=''}
                                                                                {$totalDiasNoAfecta=$totalDiasNoAfecta+$fila.num_dias_duracion_actividad}
                                                                            {/if}
                                                                            {if $fila.num_flag_auto_archivo==1}
                                                                                {$auto_archivo='md md-check'}
                                                                            {else}
                                                                                {$auto_archivo=0}
                                                                            {/if}

                                                                            <tr id="tr_actividad_{$fila.pk_num_actividad}">

                                                                                     <td style="width: 2%;">
                                                                                         <button class="btn btn-xs" disabled><i class="md md-delete" style="color: #ffffff;"></i></button>
                                                                                     </td>

                                                                                <td style="width: 41%;" id="td_descActividad_{$fila.pk_num_actividad}">
                                                                                    <span id="spn_descActividad_{$fila.pk_num_actividad}">{$fila.cod_act}  {$fila.nombre_actividad}</span>
                                                                                    <input type="hidden" id="hdd_dataActividad{$contador}" name="form[txt][actividad][secuencia][pkActividad][{$contador}]" value="{$fila.pk_num_actividad}"/>
                                                                                </td>
                                                                                <td id="afecta_plan_{$afecta_plan}" style="width: 4%;">
                                                                                    <input type="number" onkeypress="return soloNumeros(event)"  cuenta_fase="{$cuenta_fase}" class="form-control text-center" id="txt_duracion_actividad{$contador}" name="form[txt][actividad][secuencia][duracion_actividad][{$contador}]" size="2" min="1" max="99" value="{$fila.num_dias_duracion_actividad}"
                                                                                   onchange="metRecalcularFechas({$contador})" {if $acc=='aprobar' or (isset($ver) and $ver==1)}readonly{/if}/></td>

                                                                                <td id="td_fecha_inicio{$contador}" style="width: 8%;" class="text-center">{$fila.fec_inicio_actividad}
                                                                                    <input type="hidden" id="txt_fecha_inicio{$contador}" name="form[txt][actividad][secuencia][fecha_inicio][{$contador}]"  value="{if isset($fila.input_fec_inicio_actividad)}{$fila.input_fec_inicio_actividad}{else}{$fila.fec_inicio_actividad|date_format:"%Y-%m-%d"}{/if}" /></td>
                                                                                <td id="td_fecha_fin{$contador}" style="width: 8%;" class="text-center">{$fila.fec_culmina_actividad}
                                                                                    <input type="hidden" id="txt_fecha_fin{$contador}" name="form[txt][actividad][secuencia][fecha_fin][{$contador}]"  value="{if isset($fila.input_fec_culmina_actividad)}{$fila.input_fec_culmina_actividad}{else}{$fila.fec_culmina_actividad|date_format:"%Y-%m-%d"}{/if}" /></td>
                                                                                <td style="width: 3%;" class="text-center">{$fila.num_dias_prorroga_actividad}</td>
                                                                                <td id="td_fecha_inicio_real{$contador}" style="width: 8%;" class="text-center">{$fila.fec_inicio_real_actividad}</td>
                                                                                <td id="td_fecha_fin_real{$contador}"style="width: 8%;" class="text-center">{$fila.fec_culmina_real_actividad}</td>
                                                                                <td id="td_no_afecta{$contador}" valor="{$afecta_plan}" style="width: 3%;" class="text-center"><i class="{$no_afecta_plan}"></i></td>
                                                                                <td id="td_auto_archivo{$contador}"style="width: 3%;" class="text-center"><i class="{$auto_archivo}"></i></td>

                                                                            </tr>
                                                                            {$contador=$contador+1}
                                                                            {$fecha_fin_plan="'{$fila.fec_culmina_real_actividad}'"}
                                                                    {/foreach}

                                                                        <tr id="tr_subtotal_" class="fondo-Subtotal">
                                                                            <td style="width: 2%;" align="center"></td>
                                                                            <td style="width: 41%;" id="td_1">Total dias Fase:</td>
                                                                            <td id="td_Subtotal{$filasubtotal}" name="td_Subtotal" class="text-center" style="width: 4%;">{$SubTotal_dias}</td>
                                                                            <td style="width: 8%;"></td>
                                                                            <td style="width: 8%;"></td>
                                                                            <td class="text-center" style="width: 3%;">{$SubTotal_diasProrroga}</td>
                                                                            <td style="width: 8%;"></td>
                                                                            <td style="width: 8%;"></td>
                                                                            <td style="width: 3%;"></td>
                                                                            <td style="width: 3%;"></td>
                                                                        </tr>


                                                                        {$Total_dias=$Total_dias+$SubTotal_dias}
                                                                        {$Totaldias_prorroga=$Totaldias_prorroga+$SubTotal_diasProrroga}
                                                                        {$SubTotal_dias=0}
                                                                        {$SubTotal_diasProrroga=0}
                                                                        {$cuenta_fase=$cuenta_fase+1}
                                                                        {$filasubtotal=$filasubtotal+1}
                                                                    {/foreach}
                                                                    <tr id="tr_total_" class="fondo-total">
                                                                        <td style="width: 2%;" align="center"><input type="hidden" id="numero" name="numero" value="{$contador}"/></td>
                                                                        <td style="width: 41%;" id="td_1">Total dias de afectación:</td>
                                                                        <td id="td_total" class="text-center" style="width: 4%;">{$Total_dias}</td>
                                                                        <td style="width: 8%;"></td>
                                                                        <td style="width: 8%;"></td>
                                                                        <td class="text-center" style="width: 3%;">{$Totaldias_prorroga}</td>
                                                                        <td style="width: 8%;"></td>
                                                                        <td style="width: 8%;"></td>
                                                                        <td style="width: 3%;"></td>
                                                                        <td style="width: 3%;"></td>
                                                                    </tr>
                                                                    {$totalGeneralDias=$Total_dias+$totalDiasNoAfecta}
                                                                {/if}
                                                                </tbody>
                                                                <tfoot>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                        {*</div>*}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <!--cierre PANEL DE ACTIVIDADES-->

                                <ul class="pager wizard">
                                    <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                                    <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                                    <li class="next last"><a class="btn-raised" href="javascript:void(0);">&Uacute;ltimo</a></li>
                                    <li class="next"><a class="btn-raised" href="javascript:void(0);">Pr&oacute;ximo</a></li>
                                </ul>

                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group floating-label">
                                        <label for="preparado">Preparado por:</label>
                                    </div>
                                    <div class="form-group text-left">
                                        <label id="lbl_preparadopor" class="help-block text-left" style="margin-top: 10px;">{if isset($formDB.nombre_elabora)} {$formDB.nombre_elabora}{/if}</label>
                                    </div>
                                    <div class="form-group text-left">
                                        <label id="f_preparadopor" class="help-block text-left" style="margin-top: 10px;">{if isset($formDB.fec_elaborado)} {$formDB.fec_elaborado}{/if}</label>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group floating-label">
                                        <label for="rechazado">Rechazado por:</label>
                                    </div>
                                    <div class="form-group text-left">
                                        <label id="lbl_rechazadopor" class="help-block text-left" style="margin-top: 10px;">{if isset($formDB.nombre_anula)} {$formDB.nombre_anula}{/if}</label>
                                    </div>
                                    <div class="form-group">
                                        <label id="f_rechazadopor" class="help-block" style="margin-top: 10px;">{if isset($formDB.fec_anula_tramite)} {$formDB.fec_anula_tramite}{/if}</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group floating-label">
                                        <label for="aprobado">Aprobado por:</label>
                                    </div>
                                    <div class="form-group text-left">
                                        <label id="lbl_aprobadopor" class="help-block text-left" style="margin-top: 10px;">{if isset($formDB.nombre_aprueba)} {$formDB.nombre_aprueba}{/if}</label>
                                    </div>
                                    <div class="form-group">
                                        <label id="f_aprobadopor" class="help-block" style="margin-top: 10px;">{if isset($formDB.fec_aprueba_tramite)} {$formDB.fec_aprueba_tramite}{/if}</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group floating-label">
                                        <label for="ultmodificacion"><i class="fa fa-calendar"></i>Última modificación:</label>
                                    </div>
                                    <div class="form-group text-left">
                                        <label id="lbl_fehareg" class="help-block text-left" style="margin-top: 10px;">{if isset($formDB.nombre_usuario)} {$formDB.nombre_usuario}{/if}</label>
                                    </div>
                                    <div class="form-group">
                                        <label id="f_fehareg" class="help-block" style="margin-top: 10px;">{if isset($formDB.fec_modificacion)} {$formDB.fec_modificacion }{/if}</label>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <span class="clearfix"></span>
    <div class="modal-footer">
        <button type="button" class="btn btn-default logsUsuarioModal " descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal">
            <i class="glyphicon glyphicon-floppy-remove"></i> {if !isset($ver) or $ver==0}  Cancelar {else} Cerrar {/if}
        </button>

        {if $acc!='aprobar'}
            <button type="button" id="btn_guarda" class="btn btn-primary logsUsuarioModal">
                <i class="icm icm-clipboard2"></i> Generar Planificacion
            </button>
        {else}
            <button type="button" id="btn_aprueba" class="btn btn-primary logsUsuarioModal">
                <i class="icm icm-rating3"></i> Aprobar
            </button>
            <button type="button" id="btn_anula" class="btn btn-danger logsUsuarioModal">
                <i class="icm icm-blocked"></i> Anular
            </button>
        {/if}
    </div>

<script type="text/javascript">
    var mAsignarAuditor="",mQuitarAuditor="";
    $(document).ready(function () {
        //Activar el wizard
        var app = new AppFunciones();
        app.metWizard();
        // ancho de la Modal
        $('#modalAncho').css("width", "90%"); $('#ContenidoModal').css("padding-top", "0");
        //Complementos
        $('.select2').select2({ allowClear: true });
        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "dd-mm-yyyy",language: 'es'});

        $("#formAjax").submit(function(){ return false; });
        // anular accion del Formulario

        $("#formAjax").keypress(function(e){
            if (e.which == 13){ return false; }else{ return true; }
        });

        $('#agregarAuditor').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            var cargar=$('#contenidoTabla2 >tbody >tr').length;
            $.post($(this).attr('url'), { cargar: cargar ,idPersona: 'EMP', tipo: 'AUD' }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#contenidoTabla2').on('click', '.delete', function () {
            $(this).parent('td').parent('tr').remove();
        });

        $('#tActuacion').change(function(){
            $.post('{$_Parametros.url}modDN/denuncias/denunciasCONTROL/jsonCodigoMET', { tipoActuacion: $(this).val() },function(dato){
                var nuevoCodigo='<label for="cod_denuncia"><i  class="md md-markunread-mailbox"></i>' +
                                ' Tramite Nro.  '+dato['codigo']+'</label>';
                $('#divDenuncia').html('');
                $('#divDenuncia').append(nuevoCodigo);
                $('#codDenuncia').val(dato['codigo']);
                $('#tipoActuacionTexto').val(dato['tipoActuacionTexto']);
                metRecalcular();
            },'json');

        });



        $('#idMunicipio').change(function(){
            $.post('{$_Parametros.url}sector/jsonParroquiaMET', { idMunicipio: $(this).val() },function(dato){
                $('#idParroquia').html('');
                $('#idParroquia').append('<option value="">Seleccione la Parroquia</option>');
                $('#s2id_idParroquia .select2-chosen').html('Seleccione la Parroquia');
                for (var i =0; i < dato.length; i=i+1) {
                    $('#idParroquia').append('<option value="'+dato[i]['pk_num_parroquia']+'">'+dato[i]['ind_parroquia']+'</option>');
                }
            },'json');
        });
        $('#idMunicipio').ready(function(){
            if ($("#idMunicipio").length > 0) {
                var idParroquia=$('#idParroquia').attr('parroquia');
                $.post('{$_Parametros.url}sector/jsonParroquiaMET', { idMunicipio: $("#idMunicipio").val() },function(dato){
                    $('#idParroquia').html('');
                    $('#idParroquia').append('<option value="">Seleccione la Parroquia</option>');
                    $('#s2id_idParroquia .select2-chosen').html('Seleccione la Parroquia');
                    for (var i =0; i < dato.length; i=i+1) {
                        if(dato[i]['pk_num_parroquia']==idParroquia){
                            $('#idParroquia').append('<option selected value="'+dato[i]['pk_num_parroquia']+'">'+dato[i]['ind_parroquia']+'</option>');
                            $('#s2id_idParroquia .select2-chosen').html(dato[i]['ind_parroquia']);
                        } else {
                            $('#idParroquia').append('<option value="'+dato[i]['pk_num_parroquia']+'">'+dato[i]['ind_parroquia']+'</option>');
                        }
                    }
                },'json');
            }
        });
        $('#idParroquia').change(function(){
            $.post('{$_Parametros.url}modDN/denuncias/denunciasCONTROL/jsonSectorMET', { idParroquia: $(this).val() },function(dato){
                $('#num_sector').html('');
                $('#num_sector').append('<option value="">Seleccione el Sector</option>');
                $('#s2id_num_sector .select2-chosen').html('Seleccione el Sector');
                for (var i =0; i < dato.length; i=i+1) {
                    $('#num_sector').append('<option value="'+dato[i]['pk_num_sector']+'">'+dato[i]['ind_sector']+'</option>');
                }
            },'json');
        });
        $('#idParroquia').ready(function(){
            if ($("#idMunicipio").length > 0) {
                var num_sector=$('#num_sector').attr('sector');

                $.post('{$_Parametros.url}modDN/denuncias/denunciasCONTROL/jsonSectorMET', { idParroquia: $('#idParroquia').attr('parroquia') },function(dato){
                    $('#num_sector').html('');
                    $('#num_sector').append('<option value="">Seleccione el Sector</option>');
                    $('#s2id_num_sector .select2-chosen').html('Seleccione el Sector');
                    for (var i =0; i < dato.length; i=i+1) {
                        if(dato[i]['pk_num_sector']==num_sector){
                            $('#num_sector').append('<option selected value="'+dato[i]['pk_num_sector']+'">'+dato[i]['ind_sector']+'</option>');
                            $('#s2id_num_sector .select2-chosen').html(dato[i]['ind_sector']);
                        } else {
                            $('#num_sector').append('<option value="'+dato[i]['pk_num_sector']+'">'+dato[i]['ind_sector']+'</option>');
                        }
                    }
                },'json');
            }
        });

        $('#id_dependencia').change(function(){
            $.post('{$_Parametros.url}modDN/solicitud/solicitudCONTROL/jsonCentroCostoMET', { id_dependencia: $("#id_dependencia").val() },function(dato){

                $('#id_centro_costo').html('');
                $('#id_centro_costo').append('<option value="">Seleccione el Centro de Costo</option>');
                $('#s2id_id_centro_costo .select2-chosen').html('Seleccione el Centro de Costo');
                for (var i =0; i < dato.length; i=i+1) {
                    $('#id_centro_costo').append('<option value="'+dato[i]['pk_num_centro_costo']+'">'+dato[i]['ind_descripcion_centro_costo']+'</option>');
                }
            },'json');
        });
        $('#id_dependencia').ready(function(){
            var id_centro_costo=$('#id_centro_costo').attr('centro');
            $.post('{$_Parametros.url}modDN/solicitud/solicitudCONTROL/jsonCentroCostoMET', { id_dependencia: $("#id_dependencia").val() },function(dato){
                $('#id_centro_costo').html('');
                $('#id_centro_costo').append('<option value="">Seleccione el Sector</option>');
                $('#s2id_id_centro_costo .select2-chosen').html('Seleccione el Sector');
                for (var i =0; i < dato.length; i=i+1) {
                    if(dato[i]['pk_num_centro_costo']==id_centro_costo){
                        $('#id_centro_costo').append('<option selected value="'+dato[i]['pk_num_centro_costo']+'">'+dato[i]['ind_descripcion_centro_costo']+'</option>');
                        $('#s2id_id_centro_costo .select2-chosen').html(dato[i]['ind_descripcion_centro_costo']);
                    } else {
                        $('#id_centro_costo').append('<option value="'+dato[i]['pk_num_centro_costo']+'">'+dato[i]['ind_descripcion_centro_costo']+'</option>');
                    }
                }
            },'json');

        });

        $('#btn_guarda').click(function () {
            /*Guardar datos general de la planificación*/

            swal({
                title: "Confirmación de proceso",
                text: "Se van a ingresar los datos actuales para la nueva planificación",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false
            }, function () {
                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });
                $("#id_contraloria").attr({ disabled: false });
                $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (result) {
                    $("#id_contraloria").attr({ disabled: true });
                    if (result['status'] == 'error') {
                        app.metValidarError(result, 'Atención. Los campos marcados con X en rojo son obligatorios');
                    } else if (result['status'] == 'errorSql') {
                        swal("¡Atención!", result['mensaje'], "error");
                    }else if(result['status']=='errorPer'){
                        swal("Error!", 'Disculpa. No se han seleccionado Auditores', "error");
                    } else if (result['idDenuncia']) {
                        $("#idDenuncia").val(result['idDenuncia']);
                        $("#cod_denuncia").html(result['cod_denuncia']);
                        swal("Información del proceso", result['mensaje'], "success");
                        app.metActualizarRegistroTablaJson('dataTablaJson','El tramite fue registrado satisfactoriamente.','cerrarModal','ContenidoModal');
                    }
                }, 'json');
            });
        });

        $('#btn_anula').click(function () {
            /*Guardar datos general de la planificación*/
            $('#acc').val('anular');
            swal({
                title: "Confirmación de proceso",
                text: "Desea Anular la planificación",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr("boton"),
                closeOnConfirm: false
            }, function () {
                $("#id_contraloria").attr({ disabled: false });
                $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (result) {
                    $("#id_contraloria").attr({ disabled: true });
                    if (result['Error'] == 'error') {
                        app.metValidarError(result, 'Atención. Los campos marcados con X en rojo son obligatorios');
                    } else if (result['Error'] == 'errorSql') {
                        swal("¡Atención!", result['mensaje'], "error");
                    } else if (result['idDenuncia']) {
                        $("#idDenuncia").val(result['idDenuncia']);
                        $("#cod_denuncia").html(result['cod_denuncia']);
                        swal("Información del proceso", result['mensaje'], "success");
                        app.metActualizarRegistroTablaJson('dataTablaJson','El tramite fue anulado satisfactoriamente.','cerrarModal','ContenidoModal');
                    }
                }, 'json');
            });
        });

        $('#btn_aprueba').click(function () {
            /*Guardar datos general de la planificación*/

            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $("#id_contraloria").attr({ disabled: false });
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (result) {
                $("#id_contraloria").attr({ disabled: true });
                if (result['Error'] == 'error') {
                    app.metValidarError(result, 'Atención. Los campos marcados con X en rojo son obligatorios');
                } else if (result['Error'] == 'errorSql') {
                    swal("¡Atención!", result['mensaje'], "error");
                } else if (result['idDenuncia']) {
                    $("#idDenuncia").val(result['idDenuncia']);
                    $("#cod_denuncia").html(result['cod_denuncia']);
                    swal("Información del proceso", result['mensaje'], "success");
                    app.metActualizarRegistroTablaJson('dataTablaJson','El tramite fue aprobado satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            }, 'json');
        });


        //***************************** Llenado campo pruebas ********************************************/

        $('#contenidoTabla  tbody').on('keyup change', 'tr td .floating-label input' ,function (e) {
            var input = $(e.currentTarget);
            if ($.trim(input.val()) !== '') {
                input.addClass('dirty').removeClass('static');
            } else {
                input.removeClass('dirty').removeClass('static');
            }
        });
        $('#contenidoTabla').on('click', '.delete', function () {
            $(this).parent('td').parent('tr').remove();
        });

        $("#nuevoDoc").click(function() {

             var idtabla= $("#contenidoTabla");
             var tr= $("#contenidoTabla > tbody > tr");
             var numTr=tr.length;
             var nuevoTr=numTr+1;

             //var numero=nuevoTr+1;
             var pruebas='<div class="form-group floating-label" id="ind_prueba'+nuevoTr+'Error">'+
             '<input type="text" class="form-control" value="" name="form[txt][prueba][secuencia][ind_prueba]['+nuevoTr+']" id="ind_prueba'+nuevoTr+'">'+
             '<label for="ind_prueba'+nuevoTr+'"><i class="md md-insert-comment"></i> Descripción</label>'+
             '</div>';
             var numDoc='<div class="form-group floating-label" id="ind_numero_documento'+nuevoTr+'Error">'+
             '<input type="text" class="form-control" value="" name="form[txt][prueba][secuencia][ind_numero_documento]['+nuevoTr+']" id="ind_numero_documento'+nuevoTr+'">'+
             '<label for="ind_numero_documento'+nuevoTr+'"><i class="md md-insert-comment"></i> Número</label>'+
             '</div>';
             var cantidad='<div class="form-group floating-label" id="num_cantidad_prueba'+nuevoTr+'Error">'+
             '<input type="number" onkeypress="return soloNumeros(event)"  class="form-control" value="" name="form[txt][prueba][secuencia][num_cantidad_prueba]['+nuevoTr+']" id="num_cantidad_prueba'+nuevoTr+'">'+
             '<label for="num_cantidad_prueba'+nuevoTr+'"><i class="md md-insert-comment"></i> Cant.</label>'+
             '</div>';
             idtabla.append('<tr>'+
             '<td class="text-right" style="vertical-align: middle;">'+nuevoTr+'</td>'+
             '<td style="vertical-align: middle;"><div class="col-sm-10">'+pruebas+'</div></td>'+
             '<td style="vertical-align: middle;"><div class="col-sm-6">'+numDoc+'</div></td>'+
             '<td style="vertical-align: middle;"><div class="col-sm-6">'+cantidad+'</div></td>'+
             '<td  class="text-center" style="vertical-align: middle;"><button class="eliminarPrueba btn ink-reaction btn-raised btn-xs btn-danger delete"><i class="md md-delete"></i></button></td>'+
             '</tr>');
        });
    });

    function metRecalcular() {
        var f_inicio=$('#datepicker').val();
        var idDenuncia=$('#idDenuncia').val();
        var tipoActuacion=$('#tipoActuacionTexto').val();
        var opcion='modificar';
        var contador=0;

        var url='{$_Parametros.url}modDN/denuncias/denunciasCONTROL/recalcularMET';
        $.post(url,{ tipoActuacion:tipoActuacion, opcion:opcion, f_inicio: f_inicio },function(dato){
            $('#actividades').html('');
            var idtabla= $("#actividades");
            var cadena='';
            var totalDiasNoAfecta=0;
            var filasubtotal=0;
            var Total_dias=0;
            var Totaldias_prorroga=0;
            var SubTotal_dias=0;
            var SubTotal_diasProrroga=0;
            var totalGeneralDias=0;
            var fecha_fin_plan='';
            var lineas=0;
            var cuenta_fase=0;
            $.each(dato['listaActividades'], function (nombre_fases, fases){

                cadena=cadena+"<tr class='info'><td width='560' colspan='10' class=''>"+nombre_fases+"</td></tr>";
                var contador=0;
                $.each(dato['listaActividades'][nombre_fases], function (fila) {

                    if (dato['listaActividades'][nombre_fases][contador]['num_flag_no_afecto_plan'] == 0){
                        SubTotal_dias = SubTotal_dias + parseInt(dato['listaActividades'][nombre_fases][contador]['num_dias_duracion_actividad']);
                        SubTotal_diasProrroga = SubTotal_diasProrroga + parseInt(dato['listaActividades'][nombre_fases][contador]['num_dias_prorroga_actividad']);
                        var afecta_plan = 1;
                        var no_afecta_plan = 'md md-check';
                    }else{
                        var afecta_plan=0;
                        var no_afecta_plan='';
                        totalDiasNoAfecta=totalDiasNoAfecta+dato['listaActividades'][nombre_fases][contador]['num_dias_duracion_actividad'];
                    }

                    if (dato['listaActividades'][nombre_fases][contador]['num_flag_auto_archivo']==1){
                        var auto_archivo='md md-check';
                    }else{
                        var auto_archivo=0;
                    }
                    var num=dato['listaActividades'][nombre_fases][contador]['pk_num_actividad'];
                    cadena=cadena+'<tr id="tr_actividad_'+lineas+'">' +
                    '<td style="width: 2%;">' +
                    '<button class="btn btn-xs" disabled><i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
                            '<td style="width: 41%;" id="td_descActividad_'+dato['listaActividades'][nombre_fases][contador]['pk_num_actividad']+'">' +
                    '<span id="spn_descActividad_'+dato['listaActividades'][nombre_fases][contador]['pk_num_actividad']+'">'+dato['listaActividades'][nombre_fases][contador]['cod_act']+'  '+dato['listaActividades'][nombre_fases][contador]['nombre_actividad']+'</span>' +
                            '<input type="hidden" id="hdd_dataActividad'+lineas+'" name="form[txt][actividad][secuencia][pkActividad]['+lineas+']" value="'+num+'"/>'+
                            '</td>'+
                            '<td id="afecta_plan_'+lineas+'" style="width: 4%;">' +
                            '<input type="number" onkeypress="return soloNumeros(event)"  cuenta_fase="'+cuenta_fase+'" class="form-control text-center" id="txt_duracion_actividad'+lineas+'" name="form[txt][actividad][secuencia][duracion_actividad]['+lineas+']" size="2" min="1" max="99" value="'+dato['listaActividades'][nombre_fases][contador]['num_dias_duracion_actividad']+'" ' +
                            ' onkeyup="metRecalcularFechas('+lineas+')" onchange="metRecalcularFechas('+lineas+')" /></td>'+
                            '<td id="td_fecha_inicio'+lineas+'" style="width: 8%;" class="text-center">'+dato['listaActividades'][nombre_fases][contador]['fec_inicio_actividad']+
                            '<input type="hidden" id="txt_fecha_inicio'+lineas+'" name="form[txt][actividad][secuencia][fecha_inicio]['+lineas+']"  value="'+dato['listaActividades'][nombre_fases][contador]['input_fecha_inicial']+'" /></td>' +
                            '<td id="td_fecha_fin'+lineas+'" style="width: 8%;" class="text-center">'+dato['listaActividades'][nombre_fases][contador]['fec_culmina_actividad']+
                            '<input type="hidden" id="txt_fecha_fin'+lineas+'" name="form[txt][actividad][secuencia][fecha_fin]['+lineas+']"  value="'+dato['listaActividades'][nombre_fases][contador]['input_fecha_fin_actividad']+'" /></td>'+
                            '<td style="width: 3%;" class="text-center">'+dato['listaActividades'][nombre_fases][contador]['num_dias_prorroga_actividad']+'</td>'+
                            '<td id="td_fecha_inicio_real'+lineas+'" style="width: 8%;" class="text-center">'+dato['listaActividades'][nombre_fases][contador]['fec_inicio_real_actividad']+'</td>'+
                            '<td id="td_fecha_fin_real'+lineas+'"style="width: 8%;" class="text-center">'+dato['listaActividades'][nombre_fases][contador]['fec_culmina_real_actividad']+'</td>'+
                            '<td id="td_no_afecta'+lineas+'" valor="'+afecta_plan+'" style="width: 3%;" class="text-center"><i class="'+no_afecta_plan+'"></i></td>'+
                            '<td id="td_auto_archivo'+lineas+'"style="width: 3%;" class="text-center"><i class="'+auto_archivo+'"></i></td>'+
                            '</tr>';

                    fecha_fin_plan=dato['listaActividades'][nombre_fases][contador]['fec_culmina_real_actividad'];
                    contador=contador+1;
                    lineas=lineas+1;

                });

                cadena=cadena+'<tr id="tr_subtotal_" class="fondo-Subtotal">'+
                        '<td style="width: 2%;" align="center"></td>'+
                        '<td style="width: 41%;" id="td_1">Total dias Fase:</td>'+
                        '<td id="td_Subtotal'+filasubtotal+'"   name="td_Subtotal" class="text-center" style="width: 4%;">'+SubTotal_dias+'</td>'+
                            '<td style="width: 8%;"></td>'+
                            '<td style="width: 8%;"></td>'+
                            '<td class="text-center" style="width: 3%;">'+SubTotal_diasProrroga+'</td>'+
                            '<td style="width: 8%;"></td>'+
                            '<td style="width: 8%;"></td>'+
                            '<td style="width: 3%;"></td>'+
                            '<td style="width: 3%;"></td>'+
                            '</tr>';
                Total_dias=Total_dias+SubTotal_dias;
                Totaldias_prorroga=Totaldias_prorroga+SubTotal_diasProrroga;
                SubTotal_dias=0;
                SubTotal_diasProrroga=0;
                filasubtotal=filasubtotal+1;
                cuenta_fase=cuenta_fase+1;
            });
           cadena=cadena+'<tr id="tr_total_"  class="fondo-total">'+
                    '<td style="width: 2%;" align="center"><input type="hidden" id="numero" name="numero" value="'+lineas+'"/></td>'+
                    '<td style="width: 41%;" id="td_1">Total dias de afectación:</td>'+
                    '<td id="td_total" class="text-center" style="width: 4%;">'+Total_dias+'</td>'+
                    '<td style="width: 8%;"></td>'+
                    '<td style="width: 8%;"></td>'+
                    '<td class="text-center" style="width: 3%;">'+Totaldias_prorroga+'</td>'+
                    '<td style="width: 8%;"></td>'+
                    '<td style="width: 8%;"></td>'+
                    '<td style="width: 3%;"></td>'+
                    '<td style="width: 3%;"></td>'+
                    '</tr>';
                    totalGeneralDias=Total_dias+totalDiasNoAfecta;
            cadena=cadena+totalGeneralDias+'</tbody>';

            $('#fec_termino').val(fecha_fin_plan);
            idtabla.append(cadena);
        }, 'json');
    }

    function metRecalcularFechas(num){
        var n=$("#numero").val();
        var d= '';
        var f= '';

        var j=1;
        var suma=0;
        var fila='';
        var total=0;
        for(var i=0; i<n; i++){
            var fase =$("#txt_duracion_actividad" + i).attr('cuenta_fase');
            var noAfecta=$("#td_no_afecta" + i).attr('valor');
            var dias=$("#txt_duracion_actividad" + i).val();
            if(fase==j){
                if(noAfecta==1){
                    suma=parseInt(suma)+parseInt(dias);
                }
            }else{
                suma=dias;
            }
            if(noAfecta==1){
                total=parseInt(total)+parseInt(dias);
            }
            j=fase;
            $("#td_Subtotal"+j).html('');
            fila='<td id="td_Subtotal'+j+'"   name="td_Subtotal" class="text-center" style="width: 4%;">'+suma+'</td>';
            $("#td_Subtotal"+j).append(fila);
        }

        $("#td_total").html('');
        var tdTotal='<td id="td_total" class="text-center" style="width: 4%;">'+total+'</td>';
        $("#td_total").append(tdTotal);
        $("#num_duracion").val(total);
        $("#duracion_total").val(total);

        for(var i=num; i<n; i++){
            d=d+$("#txt_duracion_actividad"+i).val()+'/';
            f=f+$("#txt_fecha_inicio"+i).val()+'/';
        }
        var url='{$_Parametros.url}modDN/denuncias/denunciasCONTROL/recalcularFechasMET';
        $.post(url,{ num:num, tr:n, dias:d, fechas: f },function(dato){

            for(var i=0; i<=dato['j']; i++){
                $("#td_fecha_fin"+dato['filas'][i]['linea']).html('');
                var fin='<td id="td_fecha_fin'+dato['filas'][i]['linea']+'" style="width: 8%;" class="text-center">'+dato['filas'][i]['fec_culmina_actividad']+' ' +
                        '<input type="hidden" id="txt_fecha_fin'+dato['filas'][i]['linea']+'" name="form[txt][actividad][secuencia][fecha_fin]['+i+']"  value="'+dato['filas'][i]['input_fecha_fin_actividad']+'" /></td>';
                $("#td_fecha_fin"+dato['filas'][i]['linea']).append(fin);
                $("#td_fecha_inicio"+dato['filas'][i]['linea']).html('');
                var inicio='<td id="td_fecha_inicio'+dato['filas'][i]['linea']+'" style="width: 8%;" class="text-center">'+dato['filas'][i]['fec_inicio']+
                        '<input type="hidden" id="txt_fecha_inicio'+dato['filas'][i]['linea']+'" name="form[txt][actividad][secuencia][fecha_inicio]['+i+']"  value="'+dato['filas'][i]['input_fecha_inicial']+'" /> </td>';
                $("#td_fecha_inicio"+dato['filas'][i]['linea']).append(inicio);
            }
        }, 'json');
    }

    function soloNumeros(e){
        var key = e.keyCode || e.which;
        var tecla = String.fromCharCode(key).toLowerCase();
        var numeros = "0123456789";

        if(numeros.indexOf(tecla)==-1){
            return false;
        }
    }
</script>