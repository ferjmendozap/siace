<form action="{$_Parametros.url}modDN/maestros/actividades/actividadesCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" id="hd_cod_actividad" name="form[int][hd_cod_actividad]" value="{if isset($formDB.cod_actividad)}{$formDB.cod_actividad}{/if}" />

        {if isset($idActividad) }
            <input type="hidden" value="{$idActividad}" id="idActividad" name="idActividad"/>
            <input type="hidden" value="{$idFase}" id="idFase" name="idFase"/>

        {/if}
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group floating-label" id="cod_actividadError">
                    <input type="text" disabled class="form-control" value="{if isset($formDB.cod_actividad)}{$formDB.cod_actividad}{/if}" name="form[int][cod_actividad]" id="cod_actividad">
                    <label for="cod_actividad"><i class="fa fa-code"></i> Cod. Actividad</label>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="form-group floating-label" id="fk_a006_num_miscelaneo_detalle_tipo_actuacionError">
                    <select id="idTipoActuacion" name="form[int][fk_a006_num_miscelaneo_detalle_tipo_actuacion]" class="form-control select2"
                            {if ($ver==1) or ($idActividad!=0)} disabled {/if}>
                        <option value=""></option>
                        {foreach item=i from=$tipoActuacion}
                            {if isset($formDB.fk_a006_num_miscelaneo_detalle_tipo_actuacion) and $formDB.fk_a006_num_miscelaneo_detalle_tipo_actuacion == $i.pk_num_miscelaneo_detalle}
                                <option value="{$i.pk_num_miscelaneo_detalle}" selected >{$i.ind_nombre_detalle}</option>
                            {else}
                                <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="fk_a006_num_miscelaneo_detalle_tipo_actuacion"><i class="icm icm-cog3"></i> Tipo de Actuación</label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
            <div class="form-group floating-label" id="pk_num_faseError">
                <select name="form[int][pk_num_fase]" id="pk_num_fase" class="form-control select2"
                        {if ($ver==1) or ($idActividad!=0)} disabled {/if}>
                    <option value="">&nbsp;</option>
                    {foreach item=fila from=$idFase}
                        {if $fila.pk_num_fase == $formDB.pk_num_fase}
                            <option selected value="{$fila.pk_num_fase}">{$fila.indFase}</option>
                        {else}
                            <option value="{$fila.pk_num_fase}">{$fila.indFase}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="pk_num_fase"><i class="icm icm-cog3"></i> Fase</label>
            </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group floating-label" id="txt_descripcion_actividadError">
                    <input  {if ($ver==1) } disabled {/if} type="text" class="form-control" value="{if isset($formDB.actividad)}{$formDB.actividad}{/if}" name="form[alphaNum][txt_descripcion_actividad]" id="txt_descripcion_actividad">
                    <label for="txt_descripcion_actividad"><i class="icm icm-cog3"></i> Nombre actividad</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group floating-label" id="txt_comentarios_actividadError">
                    <textarea  {if ($ver==1) } disabled {/if} class="form-control" rows="2" name="form[alphaNum][txt_comentarios_actividad]" id="txt_comentarios_actividad">{if isset($formDB.ind_comentarios)}{$formDB.ind_comentarios}{/if}</textarea>
                    <label for="txt_comentarios_actividad"><i class="icm icm-cog3"></i> Descripción</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group floating-label" id="num_duracion_actividadError">
                    <input  {if ($ver==1) } disabled {/if} type="text" class="form-control" value="{if isset($formDB.num_duracion)}{$formDB.num_duracion}{/if}" name="form[alphaNum][num_duracion_actividad]" id="num_duracion_actividad">
                    <label for="num_duracion_actividad"><i class="md md-av-timer"></i> Duración</label>
                </div>
            </div>

            <div class="col-sm-9">
                <div class="checkbox checkbox-styled">
                    <label class="checkbox-inline">
                        <input  {if ($ver==1) } disabled {/if} type="checkbox" {if isset($formDB.ind_auto_archivo) and $formDB.ind_auto_archivo==1} checked="checked" {/if} value="1" name="form[int][ind_auto_archivo]" id="ind_auto_archivo">
                        <span>Auto de archivo</span>
                    </label>
                    <label class="checkbox-inline">
                        <input  {if ($ver==1) } disabled {/if} type="checkbox" {if isset($formDB.ind_afecto_plan) and $formDB.ind_afecto_plan==1} checked="checked" {/if} value="1" name="form[int][ind_afecto_plan]" id="ind_afecto_plan">
                        <span>No afecta planificación</span>
                    </label>
                    <label class="checkbox-inline">
                        <input  {if ($ver==1) } disabled {/if} type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==0} checked="" {else} checked="checked" {/if}  value="1" name="form[int][num_estatus]" id="num_estatus">
                        <span>Estatus</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group floating-label">
                    <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fk_a018_num_seguridad_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                    <label for="ind_usuario"><i class="md md-person"></i> Último Usuario</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="cod_faseError">
                    <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                    <label for="cod_actividad"><i class="fa fa-calendar"></i> Última Modificación</label>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">Cancelar</button>

        {if !$ver==1 }
            <button type="button" class="btn btn-primary ink-reaction logsUsuarioModal" id="accion">
                {if $idActividad!=0}
                    <i class="fa fa-edit"></i>&nbsp;Modificar
                {else}
                    <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
                {/if}
            </button>
        {/if}


    </div>
</form>
<script type="text/javascript">

    function mValidaCampNum(evt){
        var nav4 = window.Event ? true : false;var key = nav4 ? evt.which : evt.keyCode;return (key < 13 || (key >= 48 && key <= 57));
    }
    function validarError(dato,mensaje){
        for (var item in dato) {
            if(dato[item]=='error'){
                $('#'+item+'Error').removeClass('has-success has-feedback');
                $('#'+item+'Error').removeClass('has-error has-feedback');
                $('#'+item+'Error').addClass('has-error has-feedback');
                $('#'+item+'Error').append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                swal("Error!", mensaje, "error");
            }else{
                $('#'+item+'Error').removeClass('has-error has-feedback');
                $('#'+item+'Error').addClass('has-success has-feedback');
                $('#'+item+'Icono').removeClass('glyphicon glyphicon-remove form-control-feedback');
                $('#'+item+'Icono').addClass('glyphicon glyphicon-ok form-control-feedback');
            }
        }
    }
    $(document).ready(function() {
        $("#formAjax").keypress(function(e){
            if (e.which == 13){ return false; }else{ return true; }
        });

        $('#num_duracion_actividad').keypress(function(event){
            return mValidaCampNum(event);
        });


        //crea el codigo de la actividad
        $('#pk_num_fase').change(function(){
            $("#cod_actividad").html("");
            if($(this).val()){
                $('#idFase').val($(this).val());
                $ruta="{$_Parametros.url}modDN/maestros/actividades/actividadesCONTROL/crearCodigoMET";
                $.post($ruta,$( "#formAjax" ).serialize(), function (dato) {
                    $('#cod_actividad').val(dato);
                    $('#hd_cod_actividad').val(dato);
                });
            }
        });
        /// Complementos
        var app = new  AppFunciones();
        $('.select2').select2({ allowClear: true });

        // ancho de la Modal
        $('#modalAncho').css( "width", "60%" );

        // anular accion del Formulario
        $("#formAjax").submit(function(){
            return false;
        });

        $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

        //Guardado y modales
        $('#accion').click(function(){

            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                var arrayCheck = '';
                var arrayMostrarOrden = ['hd_cod_actividad','pk_num_fase','txt_descripcion_actividad','ind_auto_archivo','ind_afecto_plan','ind_estatus'];
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTabla(dato,dato['pk_num_actividad'],'idActividad',arrayCheck,arrayMostrarOrden,'La Actividad fue modificada satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTabla(dato,dato['idActividad'],'idActividad',arrayCheck,arrayMostrarOrden,'La Actividad fue guardada satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });

        $('#idTipoActuacion').change(function(){
            $.post('{$_Parametros.url}modDN/maestros/actividades/actividadesCONTROL/jsonFaseMET', { idTipoActuacion: $(this).val() },function(dato){

                $('#pk_num_fase').html('');
                $('#pk_num_fase').append('<option value="">Seleccione la Fase</option>');
                $('#s2pk_num_fase .select2-chosen').html('Seleccione la Fase');
                for (var i =0; i < dato.length; i=i+1) {
                    $('#pk_num_fase').append('<option value="'+dato[i]['pk_num_fase']+'">'+dato[i]['ind_descripcion']+'</option>');
                }
            },'json');
        });


        $('#idTipoActuacion').ready(function(){
            $.post('{$_Parametros.url}modDN/maestros/actividades/actividadesCONTROL/jsonFaseMET', { idTipoActuacion: $("#idTipoActuacion").val() },function(dato){
                $('#pk_num_fase').html('');
                $('#pk_num_fase').append('<option value="">Seleccione la Fase</option>');
                $('#s2pk_num_fase .select2-chosen').html('Seleccione  la Fase');
                for (var i =0; i < dato.length; i=i+1) {
                    if(dato[i]['pk_num_fase']==pk_num_fase){
                        $('#pk_num_fase').append('<option selected value="'+dato[i]['pk_num_fase']+'">'+dato[i]['ind_descripcion']+'</option>');
                        $('#s2idProceso .select2-chosen').html(dato[i]['ind_descripcion']);
                    } else {
                        $('#pk_num_fase').append('<option value="'+dato[i]['pk_num_fase']+'">'+dato[i]['ind_descripcion']+'</option>');
                    }
                }
            },'json');
        });
    });
</script>