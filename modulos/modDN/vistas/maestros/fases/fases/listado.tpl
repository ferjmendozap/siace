
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">LISTADO DE FASES</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Descripcion</th>
                                <th>Estado</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                        {$grupo=0}
                        {foreach item=i from=$dataBD}
                            {if ($grupo != $i.pkTipoActuacion)}
                                {$grupo = $i.pkTipoActuacion}
                                <tr style="font-weight:bold; background-color:#C7C7C7;">
                                    <td>{$i.tipoActuacion}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            {/if}
                            <tr id="idFase{$i.pk_num_fase}">
                                <td>{$i.cod_fase}</td>
                                <td>{$i.indFase}</td>
                                <td><i class="{if $i.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                <td>
                                        <button class="modificar btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idFase="{$i.pk_num_fase}" title="Editar"
                                                descipcion="El Usuario ha Modificado una Fase" titulo="<i class='fa fa-edit'></i> Editar Fase">
                                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                                        </button>

                                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idFase="{$i.pk_num_fase}" title="Consultar"
                                                descipcion="El Usuario esta viendo un Fase" titulo="<i class='md md-remove-red-eye'></i> Consultar Fase">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idFase="{$i.pk_num_fase}" title="Eliminar"  boton="si, Eliminar"
                                            descipcion="El usuario ha eliminado una Fase" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Fase?">
                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                    </button>

                                </td>
                            </tr>
                        {/foreach}
                        </tbody>

   <tfoot>
                        <tr>
                            <th colspan="4"><button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                                    descipcion="el Usuario ha creado un post de fase" data-toggle="modal"
                                                    data-target="#formModal" titulo="<i class='md md-create'></i>&nbsp;Registrar Nueva Fase" id="nuevo"
                                                    data-keyboard="false" data-backdrop="static">
                                    <i class="md md-create"></i>&nbsp;Nueva Fase
                                </button>
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var url='{$_Parametros.url}modDN/maestros/fases/fasesCONTROL/crearModificarMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idFase:0 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idFase: $(this).attr('idFase')},function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( url,{ idFase: $(this).attr('idFase') , ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idFase=$(this).attr('idFase');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modDN/maestros/fases/fasesCONTROL/eliminarMET';
                $.post($url, { idFase: idFase },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idFase'+dato['idFase'])).html('');
                        swal("Eliminado!", "La Fase ha sido eliminada satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });

    });
</script>