<style type="text/css">
    {literal}
    .fondo-total{background-color:#bbb;}
    .fondo-Subtotal{background-color:#bcd;}

    {/literal}
</style>
    <div class="modal-body" style="padding:0;">
        <div class="row">
            <div class="col-lg-12">
                    <div class="card-body">
                        <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                            <form id="formAjax" action="{$_Parametros.url}modDN/prorroga/prorrogaCONTROL/CrearModificarMET" class="form floating-label form-validation" class="form" role="form" method="post">
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">INFORMACIÓN GENERAL</span></a></li>
                                    <li><a id="tab_2" href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">PLANIFICACIÓN</span></a></li>
                                    <li><a id="tab_3" href="#tab3" data-toggle="tab"><span class="step">3</span> <span class="title">PRORROGAS</span></a></li>
                                </ul>
                            </div>

                            <div class="tab-content clearfix">

                                <!--INFORMACIÓN DE LA DENUNCIA-->
                                <div class="tab-pane active" id="tab1">

                                    <input type="hidden" value="1" name="valido"/>
                                    <input type="hidden" value="{$idDenuncia}" id="idDenuncia" name="form[int][idDenuncia]" />
                                    <input type="hidden" value="{$idProrroga}" id="idProrroga" name="form[int][idProrroga]" />
                                    <input type="hidden" value="{$accion}" id="accion" name="accion" />
                                    <input type="hidden" value="{$actividad.pk_num_detalle_dc}" id="idActividad" name="form[int][idActividad]" />
                                    <input type="hidden" value="{$actividad.ind_estatus}" id="ind_estatus" name="form[txt][ind_estatus]"/>
                                    <input type="hidden" value="{if $actividad.ind_estatus=='PE'}EJ{elseif $actividad.ind_estatus=='EJ'}TE{/if}" id="nuevo_estatus" name="form[txt][nuevo_estatus]"/>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="card" >
                                                <div class="card-head card-head-xs style-primary text-center">
                                                    <header class="text-center">INFORMACIÓN DE LA ACTIVIDAD</header>

                                                </div> <br/>
                                                <div class="card-body" style="padding:0;">
                                                    <div class="col-sm-12" style="margin-bottom: 10px">
                                                        <!--Código de la  Denuncia-->
                                                        <div class="col-sm-4">
                                                            <div class="form-group"
                                                                 id="id_enteError" style="margin-top: -10px;">
                                                                <label for="cod_denuncia"><i
                                                                            class="md md-markunread-mailbox"></i>  Tramite Nro.  </label>
                                                                <div class="col-sm-12">
                                                                    <input type="text"
                                                                           name="codDenuncia"
                                                                           class="form-control" id="codDenuncia"
                                                                           value="{$formDB.ind_num_denuncia}"
                                                                           readonly />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--tipo de tramite-->
                                                        <div class="col-sm-4">
                                                            <div class="form-group"
                                                                 id="tramiteError" style="margin-top: -10px;" >
                                                                <label for="tramite" ><i
                                                                            class="md md-info-outline"></i>  Tipo de tramite:</label>
                                                                <div class="col-sm-12">
                                                                    <input type="text"
                                                                           name="tramite"
                                                                           class="form-control" id="tramite"
                                                                           value="{$tramite}"
                                                                           readonly >
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--Estado de la Denuncia-->
                                                        <div class="col-sm-4">
                                                            <div class="form-group"
                                                                 id="id_enteError" style="margin-top: -10px;" >
                                                                <label for="ind_estado" ><i
                                                                            class="md md-info-outline"></i>  Estado de la Actividad:  </label>
                                                                <div class="col-sm-12">
                                                                    <input type="text"
                                                                           name="estado"
                                                                           class="form-control" id="estado"
                                                                           value="{$actividad.estatus} "
                                                                           readonly >
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-sm-12" style="margin-bottom: 10px">
                                                        <!--organismo ejecutante-->

                                                        <div class="col-sm-4">
                                                            <div class="form-group" style="margin-bottom: 20px;">

                                                                <label for="fec_inicio"
                                                                       class="control-label" > <i class="md md-event-note"></i>  Fecha de inicio:</label>

                                                                <div class="col-sm-12">
                                                                    <input type="text"
                                                                           name="form[txt][fe_inicio]"
                                                                           class="form-control" id="fe_inicio"
                                                                           value="{if isset($actividad.fec_inicio_real)} {($actividad.fec_inicio_real)|date_format:"%d-%m-%Y"}{/if}"
                                                                           readonly >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--Fecha registro-->
                                                        <div class="col-sm-4">

                                                            <div class="form-group" style="margin-bottom: 20px;">

                                                                <label for="fec_termino"
                                                                       class="control-label" > <i class="md md-event-available"></i>  Fecha de culminacion:</label>

                                                                <div class="col-sm-12">
                                                                    <input type="text"
                                                                           name="form[txt][fec_termino]"
                                                                           class="form-control" id="fec_termino"
                                                                           value="{if ($fec_termino_real!='0000-00-00')} {($fec_termino_real)|date_format:"%d-%m-%Y"}{/if}"
                                                                           readonly >
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="col-sm-4">

                                                            <div class="form-group" style="margin-bottom: 20px;">

                                                                <label for="num_duracion"
                                                                       class="control-label" > <i class="md md-access-time"></i>  Duración Programada:</label>

                                                                <div class="col-sm-12">
                                                                    <input type="text"
                                                                           name="form[int][num_duracion]"
                                                                           class="form-control" id="num_duracion"
                                                                           value="{$actividad.num_duracion}"
                                                                           readonly >
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12" style="margin-bottom: 10px;">
                                                        <!---descripcion-->
                                                        <div class="form-group" style="margin-bottom: 20px;">
                                                            <label for="ind_actividad" class="control-label" style="margin-bottom: 10px;"><i
                                                                        class="md md-format-indent-increase"></i>  Fase - Actividad:</label>

                                                            <div style="margin-left: 10px; " id="ind_actividadError" >
                                                                    <textarea id="ind_actividad" maxlength="250" class="form-control" rows="2" name="ind_actividad"
                                                                              readonly >{$actividad.nombre_fase} - {$actividad.nombre_actividad}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12" style="margin-bottom: 10px">
                                                        <!--organismo o ente denunciado-->


                                                        <div class="form-group"
                                                             id="id_enteError" style="margin-bottom: 10px;">
                                                            <label for="id_ente"><i
                                                                        class="md md-domain"></i>  Organismo/Ente Denunciado</label>
                                                            <select id="id_ente" name="form[int][id_ente]" class="form-control select2" style="height: 27px;" disabled >
                                                                <option value="">Seleccione..</option>
                                                                {foreach item=fila from=$selectEnte}
                                                                    {if $fila.pk_num_ente eq $formDB.fk_a039_num_ente}
                                                                        <option value="{$fila.pk_num_ente}" selected>{if $fila.num_ente_padre!=0}  - {/if} {$fila.ind_nombre_ente} </option>
                                                                    {else}
                                                                        <option value="{$fila.pk_num_ente}">{if $fila.num_ente_padre!=0}  -  {/if} {$fila.ind_nombre_ente}</option>
                                                                    {/if}
                                                                {/foreach}
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <!--origen de los fondos o recursos -->
                                                    <div class="col-sm-12" style="margin-bottom: 10px">

                                                        <div class="form-group"
                                                             id="id_ente_recursos" style="margin-bottom: 10px;">
                                                            <label for="id_ente_recursos"><i
                                                                        class="md md-local-atm"></i>  Origen de los Recursos</label>
                                                            <select id="id_ente_recursos" name="form[int][id_ente_recursos]" class="form-control select2" style="height: 27px;" disabled >
                                                                <option value="">Seleccione..</option>
                                                                {foreach item=fila from=$selectEnte}
                                                                    {if $fila.pk_num_ente eq $formDB.fk_a039_num_ente_recursos}
                                                                        <option value="{$fila.pk_num_ente}" selected>{if $fila.num_ente_padre!=0}  - {/if} {$fila.ind_nombre_ente} </option>
                                                                    {else}
                                                                        <option value="{$fila.pk_num_ente}">{if $fila.num_ente_padre!=0}  -  {/if} {$fila.ind_nombre_ente}</option>
                                                                    {/if}
                                                                {/foreach}
                                                            </select>
                                                        </div>

                                                    </div>


                                                    <div class="col-sm-12" style="margin-bottom: 10px;">
                                                        <!---descripcion-->
                                                        <div class="form-group">
                                                            <label for="ind_observacion" class="control-label" style="margin-bottom: 10px;"><i
                                                                        class="fa fa-edit"></i>  Observación:</label>
                                                            <div style="margin-left: 10px; " id="ind_observacion" >
                                                                    <textarea id="ind_observacion" maxlength="250" class="form-control" rows="2" name="ind_observacion"
                                                                            {if !isset($ver)} readonly {/if}>{if isset($actividad.ind_observaciones)}{$actividad.ind_observaciones}{/if}</textarea>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="card" >
                                                <div class="card-head card-head-xs style-primary text-center">
                                                    <header class="text-center">INFORMACIÓN DE LA PRORROGA</header>

                                                </div> <br/>
                                                <div class="col-sm-6" style="margin-bottom: 10px;">
                                                    <!---descripcion-->
                                                    <div class="form-group">
                                                        <label for="diasSolicitados" class="control-label" style="margin-bottom: 10px;"><i
                                                                    class="md md-alarm-add"></i>  Días de Prorroga Solicitados</label>
                                                        <div style="margin-left: 10px; " id="diasSolicitadosError" >
                                                                    <input id="diasSolicitados" type="number" align="center" class="form-control" name="form[int][diasSolicitados]"
                                                                            {if $accion=='ver' and $accion!='nuevo' and $accion!='modificar'} readonly {/if} style="align-content: center"
                                                                    value="{if isset($formP.num_prorroga)}{$formP.num_prorroga}{else}0{/if}"/>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-sm-6" style="margin-bottom: 10px;">
                                                    <!---descripcion-->
                                                    <div class="form-group">
                                                        <label for="diasAcumulados" class="control-label" style="margin-bottom: 10px;"><i
                                                                    class="md md-timelapse"></i>  Días de Prorrogas Acumulados</label>
                                                        <div style="margin-left: 10px; " id="diasAcumuladosDiv" >
                                                            <input id="diasAcumulados" type="number" align="center" class="form-control" name="diasAcumulados"
                                                                   value="{if isset($acumulado.acm)}{$acumulado.acm}{else}0{/if}"
                                                                    readonly />
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-sm-12" style="margin-bottom: 10px;">
                                                    <!---descripcion-->
                                                    <div class="form-group">
                                                        <label for="ind_justificacion" class="control-label" style="margin-bottom: 10px;"><i
                                                                    class="fa fa-edit"></i>  Justificación:</label>
                                                        <div style="margin-left: 10px; " id="ind_justificacionError" >
                                                                    <textarea id="ind_justificacion" maxlength="250" class="form-control" rows="2" name="form[txt][ind_justificacion]"
                                                                            {if $accion=='ver' and $accion!='nuevo' and $accion!='modificar'} readonly {/if}>{if isset($formP.ind_motivo)}{$formP.ind_motivo}{/if}</textarea>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-sm-12" style="margin-bottom: 10px;">
                                                    <!---descripcion-->
                                                    <div class="form-group">
                                                        <label for="estatus" class="control-label" style="margin-bottom: 10px;"><i
                                                                    class="md md-info-outline"></i>  Estatus</label>
                                                        <div style="margin-left: 10px; " id="estatusError" >
                                                            <input id="indestatus" type="text" align="center" class="form-control" name="form[txt][indestatus]"
                                                                   value="{if isset($formP.estatus)}{$formP.estatus}{else}En Preparación{/if}"
                                                                   readonly />
                                                            <input id="estatus" type="hidden" align="center" class="form-control" name="form[txt][estatus]"
                                                                   value="{if isset($formP.ind_estatus)}{$formP.ind_estatus}{else}PE{/if}"
                                                                   readonly />
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--cierre INFORMACIÓN DE LA DENUNCIA-->

                                <!--PANEL DE ACTIVIDADES-->
                                <div class="tab-pane" id="tab2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-head card-head-xs style-primary text-center">
                                                        <header class="text-center">PLANIFICACIÓN</header>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="table-responsive">
                                                            <table id="tb_actividades" class="table no-margin table-condensed table-bordered" style="width: 1120px; display:block">
                                                                <thead>
                                                                <tr>
                                                                    <th class="text-center" style="width: 2%;">Est.</th>
                                                                    <th class="text-center" style="width: 41%;">Actividad</th>
                                                                    <th class="text-center" style="width: 4%;">Días</th>
                                                                    <th class="text-center" style="width: 8%;">Inicio</th>
                                                                    <th class="text-center" style="width: 8%;">Fin</th>
                                                                    <th class="text-center" style="width: 3%;">Prorr.</th>
                                                                    <th class="text-center" style="width: 8%;">Inicio Real</th>
                                                                    <th class="text-center" style="width: 8%;">Fin Real</th>
                                                                    <th class="text-center" style="width: 3%;">A.P.</th>
                                                                    <th class="text-center" style="width: 3%;">A.A.</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody  id="actividades" style="height: 400px; width: 100%; display:inline-block; overflow: auto">
                                                                {$SubTotal_dias=0}
                                                                {$SubTotal_diasProrroga=0}
                                                                {$Total_dias=0}
                                                                {$Totaldias_prorroga=0}
                                                                {$totalDiasNoAfecta=0}
                                                                {$cuenta_fase=0}
                                                                {$ifilasubtotal=0}
                                                                {if $listaActividades|count > 0}
                                                                    {$contador=0}
                                                                    {foreach key=nombre_fase item=fases from=$listaActividades}
                                                                        <tr class="info">
                                                                            <td width="560" colspan="10" class="">
                                                                                {$nombre_fase}
                                                                            </td>
                                                                        </tr>
                                                                        {foreach item=fila from=$fases}
                                                                            {if $fila.num_flag_no_afecto_plan ==0}
                                                                                {$SubTotal_dias=$SubTotal_dias+$fila.num_duracion}
                                                                                {$SubTotal_diasProrroga=$SubTotal_diasProrroga+$fila.num_prorroga}
                                                                                {$afecta_plan=1}
                                                                                {$no_afecta_plan='md md-check'}
                                                                            {else}
                                                                                {$afecta_plan=0}
                                                                                {$no_afecta_plan=''}
                                                                                {$totalDiasNoAfecta=$totalDiasNoAfecta+$fila.num_duracion}
                                                                            {/if}
                                                                            {if $fila.num_flag_auto_archivo==1}
                                                                                {$auto_archivo='md md-check'}
                                                                            {else}
                                                                                {$auto_archivo=0}
                                                                            {/if}

                                                                            <tr id="tr_actividad_{$fila.pk_num_detalle_dc}" {if isset($fila.semaforo)}class="{$fila.semaforo}"{/if} role="alert" >

                                                                                <td style="width: 2%;">
                                                                                    {if ($fila.ind_estatus=='PE')}
                                                                                        <button title="PENDIENTE"
                                                                                                class="btn ink-reaction btn-floating-action btn-xs btn-danger">
                                                                                            <i class="icm icm-busy2" ></i>
                                                                                        </button>
                                                                                    {elseif ($fila.ind_estatus=='EJ')}
                                                                                        <button title="EN EJECUCIÓN"
                                                                                                class="btn ink-reaction btn-floating-action btn-xs btn-info">
                                                                                            <i class="icm icm-spinner" ></i>
                                                                                        </button>

                                                                                    {else}
                                                                                        <button title="TERMINADA"
                                                                                                class="btn ink-reaction btn-floating-action btn-xs btn-success">
                                                                                            <i class="icm icm-checkmark3" ></i>
                                                                                        </button>

                                                                                    {/if}
                                                                                </td>
                                                                                <td style="width: 41%;" id="td_descActividad_{$fila.pk_num_detalle_dc}" registro="{$contador}">
                                                                                    <span id="spn_descActividad_{$fila.pk_num_detalle_dc}">{$fila.cod_act}  {$fila.nombre_actividad}</span>
                                                                                    <input type="hidden" id="hdd_dataActividad{$contador}" name="form[txt][actividad][secuencia][pkActividad][{$contador}]" value="{$fila.pk_num_detalle_dc}"/>
                                                                                </td>
                                                                                <td id="afecta_plan_{$afecta_plan}" style="width: 4%; text-align: center">
                                                                                    {$fila.num_duracion}
                                                                                    <input type="hidden" cuenta_fase="{$cuenta_fase}" id="txt_duracion_actividad{$contador}" name="form[txt][actividad][secuencia][txt_duracion_actividad][{$contador}]" value="{$fila.num_duracion}"/>
                                                                                </td>
                                                                                <td id="td_fecha_inicio{$contador}" style="width: 8%;" class="text-center">{$fila.fec_inicio}
                                                                                    <input type="hidden" id="txt_fecha_inicio{$contador}" name="form[txt][actividad][secuencia][txt_fecha_inicio][{$contador}]" value="{$fila.fec_inicio}"/>
                                                                                </td>
                                                                                <td id="td_fecha_fin{$contador}" style="width: 8%;" class="text-center">{$fila.fec_termino}
                                                                                </td>
                                                                                <td id="td_duracion_prorroga{$contador}" style="width: 3%;" class="text-center">{if $fila.num_prorroga==0 or !isset($fila.num_prorroga)}0{else}{$fila.num_prorroga}{/if}</td>
                                                                                <td id="td_fecha_inicio_real{$contador}" style="width: 8%;" class="text-center">{$fila.fec_inicio_real}</td>
                                                                                <td id="td_fecha_fin_real{$contador}" style="width: 8%;" class="text-center">{$fila.fec_termino_real}</td>
                                                                                <td id="td_no_afecta{$contador}" valor="{$afecta_plan}" style="width: 3%;" class="text-center"><i class="{$no_afecta_plan}"></i></td>
                                                                                <td id="td_auto_archivo{$contador}" style="width: 3%;" class="text-center"><i class="{$auto_archivo}"></i></td>
                                                                            </tr>
                                                                            {$contador=$contador+1}
                                                                            {$fecha_fin_plan="'{$fila.fec_termino_real}'"}
                                                                        {/foreach}

                                                                        <tr id="tr_subtotal_" class="fondo-Subtotal">
                                                                            <td style="width: 2%;" align="center"></td>
                                                                            <td style="width: 41%;" id="td_1">Total dias Fase:</td>
                                                                            <td id="td_Subtotal{$ifilasubtotal}" name="td_Subtotal" class="text-center" style="width: 4%;">{$SubTotal_dias}</td>
                                                                            <td style="width: 8%;"></td>
                                                                            <td style="width: 8%;"></td>
                                                                            <td id="td_SubtotalProrroga{$ifilasubtotal}" name="td_SubtotalProrroga" class="text-center" style="width: 3%;">{$SubTotal_diasProrroga}</td>
                                                                            <td style="width: 8%;"></td>
                                                                            <td style="width: 8%;"></td>
                                                                            <td style="width: 3%;"></td>
                                                                            <td style="width: 3%;"></td>
                                                                        </tr>

                                                                        {$Total_dias=$Total_dias+$SubTotal_dias}
                                                                        {$Totaldias_prorroga=$Totaldias_prorroga+$SubTotal_diasProrroga}
                                                                        {$SubTotal_dias=0}
                                                                        {$SubTotal_diasProrroga=0}
                                                                        {$ifilasubtotal=$ifilasubtotal+1}
                                                                        {$cuenta_fase=$cuenta_fase+1}
                                                                    {/foreach}
                                                                    <tr id="tr_total_" class="fondo-total">
                                                                        <td style="width: 2%;" align="center"><input type="hidden" id="numero" name="numero" value="{$contador}"/></td>
                                                                        <td style="width: 41%;" id="td_1">Total dias de afectación:</td>
                                                                        <td id="td_total" class="text-center" style="width: 4%;">{$Total_dias}</td>
                                                                        <td style="width: 8%;"></td>
                                                                        <td style="width: 8%;"></td>
                                                                        <td id="td_totalProrroga" class="text-center" style="width: 3%;">{$Totaldias_prorroga}</td>
                                                                        <td style="width: 8%;"></td>
                                                                        <td style="width: 8%;"></td>
                                                                        <td style="width: 3%;"></td>
                                                                        <td style="width: 3%;"></td>
                                                                    </tr>
                                                                    {$totalGeneralDias=$Total_dias+$totalDiasNoAfecta}
                                                                {/if}
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <!--cierre PANEL DE ACTIVIDADES-->

                                <!---PANEL DE PRORROGAS REALIZADAS-->
                                <div class="tab-pane" id="tab3">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="card-head card-head-xs style-primary text-center">
                                                    <header class="text-center">
                                                        PRORROGAS DE LA ACTUACIÓN</header>
                                                </div>
                                                <div class="card-body no-padding">
                                                    <div class="table-responsive no-margin">

                                                        <table class="table table-striped no-margin" id="contenidoTabla">

                                                            <thead>
                                                            <tr>
                                                                <th class="text-center">#</th>
                                                                <th>FASE - ACTIVIDAD</th>
                                                                <th>CANT. DÍAS</th>
                                                                <th>JUSTIFICACIÓN</th>
                                                                <th>ESTATUS</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            {if isset($listaProrroga)}
                                                                {$FilaNro=1}
                                                                {foreach item=lista from=$listaProrroga}

                                                                    <tr {if $lista.pk_num_prorroga_detalle==$idProrroga}class="alert alert-warning" {/if}>
                                                                        <td class="text-right" style="vertical-align: middle;">
                                                                            {$lista.secuencia}
                                                                        </td>
                                                                        <td style="vertical-align: middle;">
                                                                            <div class="col-sm-10">
                                                                                <label >{$lista.Fase} - {$lista.Actividad} </label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="text-center" style="vertical-align: middle;">
                                                                            <div class="col-sm-4">
                                                                                <label > {$lista.dias} </label>
                                                                            </div>
                                                                        </td>
                                                                        <td   style="vertical-align: middle;">
                                                                            <div class="col-sm-10">
                                                                                <label > {$lista.ind_motivo} </label>
                                                                            </div>
                                                                        </td>
                                                                        <td   style="vertical-align: middle;">
                                                                            <div class="col-sm-10">
                                                                                <label > {$lista.estatus} </label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    {$FilaNro=$FilaNro+1}
                                                                {/foreach}
                                                            {/if}
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!---cierre PANEL DE PRORROGAS REALIZADAS-->

                                <ul class="pager wizard">
                                    <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                                    <li class="previous"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
                                    <li class="next last"><a class="btn-raised" href="javascript:void(0);">&Uacute;ltimo</a></li>
                                    <li class="next"><a class="btn-raised" href="javascript:void(0);">Pr&oacute;ximo</a></li>
                                </ul>

                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group floating-label">
                                        <label for="preparado">Preparado por:</label>
                                    </div>
                                    <div class="form-group text-left">
                                        <label id="lbl_preparadopor" class="help-block text-left" style="margin-top: 10px;">{if isset($formDB.nombre_elabora)} {$formDB.nombre_elabora}{/if}</label>
                                    </div>
                                    <div class="form-group text-left">
                                        <label id="f_preparadopor" class="help-block text-left" style="margin-top: 10px;">{if isset($formDB.fec_elaborado)} {$formDB.fec_elaborado}{/if}</label>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group floating-label">
                                        <label for="rechazado">Rechazado por:</label>
                                    </div>
                                    <div class="form-group text-left">
                                        <label id="lbl_rechazadopor" class="help-block text-left" style="margin-top: 10px;">{if isset($formDB.nombre_anula)} {$formDB.nombre_anula}{/if}</label>
                                    </div>
                                    <div class="form-group">
                                        <label id="f_rechazadopor" class="help-block" style="margin-top: 10px;">{if isset($formDB.fec_anula_tramite)} {$formDB.fec_anula_tramite}{/if}</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group floating-label">
                                        <label for="aprobado">Aprobado por:</label>
                                    </div>
                                    <div class="form-group text-left">
                                        <label id="lbl_aprobadopor" class="help-block text-left" style="margin-top: 10px;">{if isset($formDB.nombre_aprueba)} {$formDB.nombre_aprueba}{/if}</label>
                                    </div>
                                    <div class="form-group">
                                        <label id="f_aprobadopor" class="help-block" style="margin-top: 10px;">{if isset($formDB.fec_aprueba_tramite)} {$formDB.fec_aprueba_tramite}{/if}</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group floating-label">
                                        <label for="ultmodificacion"><i class="fa fa-calendar"></i>Última modificación:</label>
                                    </div>
                                    <div class="form-group text-left">
                                        <label id="lbl_fehareg" class="help-block text-left" style="margin-top: 10px;">{if isset($formDB.nombre_usuario)} {$formDB.nombre_usuario}{/if}</label>
                                    </div>
                                    <div class="form-group">
                                        <label id="f_fehareg" class="help-block" style="margin-top: 10px;">{if isset($formDB.fec_modificacion)} {$formDB.fec_modificacion }{/if}</label>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <span class="clearfix"></span>
    <div class="modal-footer">
        <button type="button" class="btn btn-default logsUsuarioModal " descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal">
            <i class="glyphicon glyphicon-floppy-remove"></i> {if !isset($ver) or $ver==0}  Cancelar {else} Cerrar {/if}
        </button>

        {if $accion=='nuevo'}
            <button type="button" id="btn_guarda" class="btn btn-info ink-reaction logsUsuarioModal">
                <i class="glyphicon glyphicon-floppy-disk"></i> Guardar
            </button>
        {elseif $accion=='modificar'}
            <button type="button" id="btn_guarda" class="btn btn-primary ink-reaction logsUsuarioModal">
                <i class="fa fa-edit""></i> Modificar
            </button>
        {elseif $accion=='revisar'}
            <button type="button" id="btn_guarda" class="btn btn-primary ink-reaction logsUsuarioModal">
                <i class="icm icm-rating"></i> Revisar
            </button>
            <button type="button" id="btn_anula" class="btn btn-danger logsUsuarioModal"
                    titulo="Anular Prorroga"
                    mensaje="Desea anular la prorroga?">
                <i class="icm icm-blocked"></i> Anular
            </button>
        {elseif $accion=='conformar'}
            <button type="button" id="btn_guarda" class="btn btn-primary ink-reaction logsUsuarioModal">
                <i class="icm icm-rating2"></i> Conformar
            </button>
            <button type="button" id="btn_anula" class="btn btn-danger logsUsuarioModal"
                    titulo="Anular Prorroga"
                    mensaje="Desea anular la prorroga?">
                <i class="icm icm-blocked"></i> Anular
            </button>
        {elseif $accion=='aprobar'}
            <button type="button" id="btn_guarda" class="btn btn-primary ink-reaction logsUsuarioModal">
                <i class="icm icm-rating3"></i> Aprobar
            </button>
            <button type="button" id="btn_anula" class="btn btn-danger logsUsuarioModal"
                    titulo="Anular Prorroga"
                    mensaje="Desea anular la prorroga?">
                <i class="icm icm-blocked"></i> Anular
            </button>
        {/if}
    </div>

<script type="text/javascript">
    var mAsignarAuditor="",mQuitarAuditor="";
    $(document).ready(function () {
        //Activar el wizard
        var app = new AppFunciones();
        app.metWizard();
        // ancho de la Modal
        $('#modalAncho').css("width", "90%"); $('#ContenidoModal').css("padding-top", "0");
        //Complementos
        $('.select2').select2({ allowClear: true });
        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "dd-mm-yyyy",language: 'es'});

        $("#formAjax").submit(function(){ return false; });
        // anular accion del Formulario

        $("#formAjax").keypress(function(e){
            if (e.which == 13){ return false; }else{ return true; }
        });


        $('#contenidoTabla2').on('click', '.delete', function () {
            $(this).parent('td').parent('tr').remove();
        });

        //***************************** Llenado campo pruebas ********************************************/

        $('#contenidoTabla  tbody').on('keyup change', 'tr td .floating-label input' ,function (e) {
            var input = $(e.currentTarget);
            if ($.trim(input.val()) !== '') {
                input.addClass('dirty').removeClass('static');
            } else {
                input.removeClass('dirty').removeClass('static');
            }
        });
        $('#contenidoTabla').on('click', '.delete', function () {
            $(this).parent('td').parent('tr').remove();
        });

        $('#diasSolicitados').change(function(){

            var solicitado=$(this).val();
            var acumulado=$("#diasAcumulados").val();
            var idActividad=$("#idActividad").val();
            var n=$("#td_descActividad_"+idActividad).attr('registro');
            var imp= parseInt(solicitado)+parseInt(acumulado);

            $("#td_duracion_prorroga"+n).html('');
            $("#td_duracion_prorroga"+n).append(imp);

            var f=$("#fe_inicio").val()+'/';
            var tDias= parseInt(solicitado)+parseInt(acumulado)+parseInt($("#num_duracion").val());

            var num=$("#numero").val();
            var j=1;
            var suma=0;

            var total=0;
            for(var i=0; i<num; i++){
                var fase =$("#txt_duracion_actividad" + i).attr('cuenta_fase');
                var dp=$("#td_duracion_prorroga" + i).html();
                var noAfecta=$("#td_no_afecta" + i).attr('valor');
                //var dias= parseInt(solicitado)+parseInt(acumulado);
                if(fase==j){
                    if(noAfecta==1){

                        suma=parseInt(suma)+parseInt(dp);
                    }
                }else{
                    suma=dp;
                }
                if(noAfecta==1){
                    total=parseInt(total)+parseInt(dp);
                }
                j=fase;
                $("#td_SubtotalProrroga"+j).html('');
                $("#td_SubtotalProrroga"+j).append(suma);
            }

            $("#td_totalProrroga").html('');
            $("#td_totalProrroga").append(total);


            var d=tDias+'/';
            var m=parseInt(n)+parseInt(1);
            for(var i=m; i<num; i++){
                d=d+$("#txt_duracion_actividad"+i).val()+'/';
                f=f+$("#txt_fecha_inicio"+i).val()+'/';
            }
            var url='{$_Parametros.url}modDN/denuncias/denunciasCONTROL/recalcularFechasMET';
            $.post(url,{ num:n, tr:num, dias:d, fechas: f },function(dato){

                for(var i=0; i<=dato['j']; i++){
                    $("#td_fecha_fin_real"+dato['filas'][i]['linea']).html('');
                    var fin=dato['filas'][i]['fec_culmina_real_actividad'];
                    $("#td_fecha_fin_real"+dato['filas'][i]['linea']).append(fin);
                    $("#td_fecha_inicio_real"+dato['filas'][i]['linea']).html('');
                    var inicio=dato['filas'][i]['fec_inicio_real_actividad'];
                    $("#td_fecha_inicio_real"+dato['filas'][i]['linea']).append(inicio);
                }
            }, 'json');
        });


        $('#btn_guarda').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else {
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'Proceso Satisfactorio', 'cerrarModal', 'ContenidoModal');
                }
            },'json');
        });


        $('#btn_anula').click(function(){
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                $("#accion").val('anular');
                $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                    if(dato['status']=='error'){
                        app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                    }else if(dato['status']=='errorSQL'){
                        app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                    }else {
                        app.metActualizarRegistroTablaJson('dataTablaJson', 'Proceso Satisfactorio', 'cerrarModal', 'ContenidoModal');
                    }
                },'json');
            });
        });



    });
</script>