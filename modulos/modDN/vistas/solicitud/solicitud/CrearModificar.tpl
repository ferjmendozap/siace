<div class="modal-body" style="padding:0;">
   <div class="row">
       <div class="col-lg-12">
                        <!--Código de la  Solicitud-->
            <div class="table-responsive no-margin">
                <table class="table table-striped ">
                    <tr>
                        <td align="center">
                            <h4><i class="md md-markunread-mailbox"></i>   Solicitud Nro.  {if isset($formDB.cod_solicitud)}{$formDB.cod_solicitud}{else}{$codSolicitud}{/if}</h4>
                        </td>
                        <td align="center">
                            <h4><i class="md md-info-outline"></i>   Estado:  {if isset($formDB.estado)} {$formDB.estado} {else} En Preparación {/if}</h4>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="card">
                <div class="card-body">
                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                         <form id="formAjaxTab1" action="{$_Parametros.url}modDN/solicitud/solicitudCONTROL/CrearModificarMET" class="form floating-label form-validate"  role="form" method="post" novalidate="novalidate">
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">INFORMACIÓN GENERAL</span></a></li>
                                    <li><a href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">INFORMACIÓN DE LA RECEPCIÓN</span></a></li>
                                    <li><a href="#tab3" data-toggle="tab"><span class="step">3</span> <span class="title">DENUNCIANTES</span></a></li>
                                    <li><a href="#tab4" data-toggle="tab"><span class="step">4</span> <span class="title">DOCUMENTOS CONSIGNADOS / PRUEBAS</span></a></li>
                                </ul>
                            </div>
                            <div class="tab-content clearfix">

                                <!--INFORMACIÓN DE LA SOLICITUD-->
                                <div class="tab-pane active" id="tab1">

                                        <input type="hidden" value="1" name="valido"/>
                                        <input type="hidden" value="{if isset($formDB.ind_estatus)}{$formDB.ind_estatus}{else}PE{/if}" id="ind_estatus" name="form[txt][ind_estatus]"/>
                                        <input type="hidden" value="{if isset($idSolicitud)}{$idSolicitud}{/if}" id="pk_num_denuncia" name="pk_num_denuncia" />
                                        <input type="hidden" value="{if isset($formDB.estado)} {$formDB.estado} {else} PENDIENTE {/if}" id="estatus" name="form[txt][estatus]"/>
                                        <input type="hidden" value="{if isset($tipoActuacionTexto)}{$tipoActuacionTexto}{/if}" id="tipoActuacionTexto" name="form[txt][tipoActuacionTexto]" />

                                    <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card" style="padding:0;">
                                                    <div class="card-head card-head-xs style-primary text-center">
                                                        <header class="text-center">INFORMACIÓN GENERAL</header>

                                                    </div> <br/>
                                                    <div class="card-body" style="padding:0;">

                                                        <div class="col-sm-12">
                                                            <!--organismo ejecutante-->

                                                            <div class="col-sm-6">
                                                                <div class="form-group" id="id_contraloriaError" style="margin-bottom: 20px;">
                                                                    <label for="contraloria"><i
                                                                                class="md md-account-balance"></i> Lugar</label>
                                                                    <select id="id_contraloria" name="form[int][id_contraloria]" class="form-control select2" style="height: 27px;" disabled="disabled">
                                                                        {foreach item=fila from=$listadoContraloria}
                                                                            <option value="{$fila.pk_num_organismo}" selected>{$fila.ind_descripcion_empresa}</option>
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!--Fecha registro-->
                                                            <div class="col-sm-6">

                                                                    <div class="form-group" style="margin-bottom: 20px;">

                                                                        <label for="fe_registro"
                                                                               class="control-label" > <i class="md md-access-time"></i> Fecha y Hora de la Solicitud:</label>

                                                                        <div class="col-sm-3">
                                                                            <input type="text"
                                                                                   name="form[txt][fe_registro]"
                                                                                   class="form-control" id="fe_registro"
                                                                                   value="{if isset($formDB.fec_elaborado)} {($formDB.fec_elaborado)|date_format:"%d-%m-%Y"}{else}{$fecha_actual}{/if}"
                                                                                   readonly >
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <input type="text"
                                                                                   name="form[txt][fe_hora_registro]"
                                                                                   class="form-control" id="fe_hora_registro"
                                                                                   value="{if isset($formDB.fec_elaborado)} {($formDB.fec_elaborado)|date_format:"%H:%M:%S"} {else}{$hora_actual}{/if}"
                                                                                   readonly >
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <!--dependencia interna de la contraloría-->

                                                            <div class="col-sm-6">
                                                                <div class="form-group" id="id_dependenciaError"
                                                                     style="margin-bottom: 10px;">
                                                                    <label for="id_dependencia"><i
                                                                                class="fa fa-sitemap"></i> Dependencia:</label>
                                                                    <select id="id_dependencia" name="form[int][id_dependencia]" class="form-control select2" style="height: 27px;" {if (isset($ver) and $ver==1) OR isset($idSolicitud)} disabled {/if}>
                                                                        <option value="">Seleccione..</option>
                                                                        {foreach item=fila from=$selectDependencia}
                                                                            {if ($fila.pk_num_dependencia eq $formDB.fk_a004_num_dependencia) OR ($predeterminado.fk_a004_num_dependencia==$fila.pk_num_dependencia)}
                                                                                <option value="{$fila.pk_num_dependencia}" selected> {$fila.ind_dependencia}</option>
                                                                            {else}
                                                                                <option value="{$fila.pk_num_dependencia}"  > {$fila.ind_dependencia}</option>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!--centro de costo-->
                                                            <div class="col-sm-6">
                                                                <div class="form-group" id="id_centro_costoError"  style="margin-bottom: 10px;">
                                                                    <label for="id_centro_costo"><i
                                                                                class="fa fa-tag"></i> Centro de Costo:</label>
                                                                    <select centro="{$formDB.fk_a023_num_centro_costo}"id="id_centro_costo" name="form[int][id_centro_costo]" class="form-control select2" style="height: 27px;" {if (isset($ver) and $ver==1) OR isset($idSolicitud)} disabled {/if}>
                                                                        <option value=""></option>
                                                                        {foreach item=fila from=$selectCentroCosto}
                                                                            {if $fila.pk_num_centro_costo eq $formDB.fk_a023_num_centro_costo}
                                                                                <option value="{$fila.pk_num_centro_costo}" selected>{$fila.ind_descripcion_centro_costo}</option>
                                                                            {else}
                                                                                <option value="{$fila.pk_num_centro_costo}">{$fila.ind_descripcion_centro_costo}</option>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>




                                                        <div class="col-sm-12">
                                                            <!--organismo o ente denunciado-->

                                                            <div class="col-sm-6">
                                                                <div class="form-group"
                                                                     id="id_enteError" style="margin-bottom: 10px;">
                                                                    <label for="id_ente"><i
                                                                                class="md md-domain"></i> Organismo/Ente Denunciado</label>
                                                                    <select id="id_ente" name="form[int][id_ente]" class="form-control select2" style="height: 27px;" {if (isset($ver) and $ver==1) OR isset($idSolicitud)} disabled {/if}>
                                                                        <option value="">Seleccione..</option>
                                                                        {foreach item=fila from=$selectEnte}
                                                                            {if $fila.pk_num_ente eq $formDB.fk_a039_num_ente}
                                                                                <option value="{$fila.pk_num_ente}" selected>{if $fila.num_ente_padre!=0}  - {/if} {$fila.ind_nombre_ente} </option>
                                                                            {else}
                                                                                <option value="{$fila.pk_num_ente}">{if $fila.num_ente_padre!=0}  -  {/if} {$fila.ind_nombre_ente}</option>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!--origen de los fondos o recursos -->
                                                            <div class="col-sm-6">

                                                                <div class="form-group"
                                                                     id="id_ente_recursos" style="margin-bottom: 10px;">
                                                                    <label for="id_ente_recursos"><i
                                                                                class="md md-local-atm"></i> Origen de los Recursos</label>
                                                                    <select id="id_ente_recursos" name="form[int][id_ente_recursos]" class="form-control select2" style="height: 27px;" {if isset($ver) and $ver==1} disabled {/if}>
                                                                        <option value="">Seleccione..</option>
                                                                        {foreach item=fila from=$selectEnte}
                                                                            {if $fila.pk_num_ente eq $formDB.fk_a039_num_ente_recursos}
                                                                                <option value="{$fila.pk_num_ente}" selected>{if $fila.num_ente_padre!=0}  - {/if} {$fila.ind_nombre_ente} </option>
                                                                            {else}
                                                                                <option value="{$fila.pk_num_ente}">{if $fila.num_ente_padre!=0}  -  {/if} {$fila.ind_nombre_ente}</option>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <!--MUNICIPIO:-->
                                                            <div class="col-sm-6">
                                                                <div class="form-group" id="idMunicipioError" style="margin-bottom: 10px;">
                                                                    <label for="idMunicipio"><i
                                                                                class="md md-map"></i> Municipio</label>
                                                                    <select id="idMunicipio" name="form[int][idMunicipio]" {if (isset($ver) and $ver==1) OR isset($idSolicitud)} disabled {/if}
                                                                            class="form-control select2-list select2">
                                                                        <option value="">Seleccione el Municipio</option>
                                                                        {foreach item=i from=$listadoMunicipio}
                                                                            {if $i.pk_num_municipio==$formDB.fk_a011_num_municipio}
                                                                                <option value="{$i.pk_num_municipio}" selected>{$i.ind_municipio}</option>
                                                                            {else}
                                                                                <option value="{$i.pk_num_municipio}">{$i.ind_municipio}</option>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <!--Parroquia:-->

                                                            <div class="col-sm-6">
                                                                <div class="form-group" id="idParroquiaError" style="margin-bottom: 10px;">
                                                                    <label for="idParroquia"><i
                                                                                class="md md-map"></i> Parroquia</label>
                                                                    <select parroquia="{$formDB.fk_a012_num_parroquia}"id="idParroquia" name="form[int][idParroquia]" class="form-control select2" style="height: 27px;" {if (isset($ver) and $ver==1) OR isset($idSolicitud)} disabled {/if}>
                                                                        <option value="">Seleccione..</option>
                                                                        {foreach item=fila from=$listadoParroquia}
                                                                            {if $formDB.fk_a012_num_parroquia==$fila.pk_num_parroquia}
                                                                                <option value="{$fila.pk_num_parroquia}" selected>{$fila.ind_parroquia}</option>
                                                                            {else}
                                                                                <option value="{$fila.pk_num_parroquia}">{$fila.ind_parroquia}</option>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="col-sm-12">

                                                            <!--Sector:-->
                                                            <div class="col-sm-6">
                                                                <div class="form-group" id="num_sectorError" style="margin-bottom: 10px;">
                                                                    <label for="num_sector"><i
                                                                                class="md md-my-location"></i> Sector</label>
                                                                    <select sector="{$formDB.pk_num_sector}" id="num_sector" name="form[int][num_sector]" class="form-control select2" style="height: 27px;" {if (isset($ver) and $ver==1) OR isset($idSolicitud)} disabled {/if}>
                                                                        <option value="">Seleccione..</option>
                                                                        {foreach item=fila from=$listadoSector}
                                                                            {if $formDB.pk_num_sector ==$fila.pk_num_sector}
                                                                                <option value="{$fila.pk_num_sector}" selected>{$fila.ind_sector}</option>
                                                                            {else}
                                                                                <option value="{$fila.pk_num_sector}">{$fila.ind_sector}</option>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <!--Duracion no afecta -->
                                                            <div class="col-sm-6">
                                                                <div class="form-group" id="tipoActuacionError" style="margin-bottom: 10px;">
                                                                    <label for="tipoActuacion"><i
                                                                                class="md md-beenhere"></i> Tipo de Actuación:</label>
                                                                    <select id="tipoActuacion" name="form[int][tipoActuacion]" class="form-control select2" style="height: 27px;" {if isset($ver) and $ver==1} disabled {/if}>
                                                                        <option value="">Seleccione..</option>
                                                                        {foreach item=fila from=$tipoActuacion}
                                                                            {if isset($fila.pk_num_miscelaneo_detalle) and $fila.pk_num_miscelaneo_detalle==$formDB.fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion}
                                                                                <option value="{$fila.pk_num_miscelaneo_detalle}" selected>{$fila.ind_nombre_detalle}</option>
                                                                            {else}
                                                                                <option value="{$fila.pk_num_miscelaneo_detalle}">{$fila.ind_nombre_detalle}</option>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <!--origen de la actuacion:-->
                                                            <div class="col-sm-6">
                                                                <div class="form-group" id="idorigen_denunciaError" style="margin-bottom: 10px;">
                                                                    <label for="idorigen_denuncia"><i
                                                                                class="fa fa-street-view"></i> Origen de la Denuncia</label>
                                                                    <select id="idorigen_denuncia" name="form[int][idorigen_denuncia]" class="form-control select2" style="height: 27px;" {if (isset($ver) and $ver==1) OR isset($idSolicitud)} disabled {/if}>
                                                                        <option value="">Seleccione..</option>
                                                                        {foreach item=orig from=$origen}
                                                                            {if isset($formDB.fk_a006_pk_num_miscelaneo_detalle_origen) and $formDB.fk_a006_pk_num_miscelaneo_detalle_origen == $orig.pk_num_miscelaneo_detalle}
                                                                                <option value="{$orig.pk_num_miscelaneo_detalle}" selected>{$orig.ind_nombre_detalle}</option>
                                                                            {else}
                                                                                <option value="{$orig.pk_num_miscelaneo_detalle}">{$orig.ind_nombre_detalle}</option>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!--Prorroga-->
                                                            <div class="col-sm-6">
                                                                <div class="form-group" id="div_ind_especificacion_origen_denuncia" style="margin-bottom: 10px;">
                                                                    <label for="esporigen_denuncia"><i
                                                                                class="fa fa-edit"></i> Especifique</label>
                                                                    <input type="text" id="ind_especificacion_origen_denuncia" class="form-control" maxlength="250"
                                                                            name="form[txt][ind_especificacion_origen_denuncia]" value="{if isset($formDB.ind_especificacion_origen_denuncia)} {$formDB.ind_especificacion_origen_denuncia} {/if}"
                                                                            {if (isset($ver) and $ver==1) OR isset($idSolicitud)} readonly {/if}>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="col-sm-6">
                                                                <div class="form-group" id="idtipo_denunciaError" style="margin-bottom: 10px;">
                                                                    <label for="idtipo_denuncia"><i
                                                                                class="fa fa-street-view"></i> Tipo de Denuncia</label>
                                                                    <select id="idtipo_denuncia" name="form[int][idtipo_denuncia]" class="form-control select2" style="height: 27px;" {if (isset($ver) and $ver==1)} disabled {/if}>
                                                                        <option value="">Seleccione..</option>
                                                                        {foreach item=tip from=$tipo}
                                                                            {if isset($formDB.fk_a006_pk_num_miscelaneo_detalle_tipo_denuncia) and $formDB.fk_a006_pk_num_miscelaneo_detalle_tipo_denuncia == $tip.pk_num_miscelaneo_detalle}
                                                                                <option value="{$tip.pk_num_miscelaneo_detalle}" selected>{$tip.ind_nombre_detalle}</option>
                                                                            {else}
                                                                                <option value="{$tip.pk_num_miscelaneo_detalle}">{$tip.ind_nombre_detalle}</option>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group" id="ind_especificacion_tipo_denuncia" style="margin-bottom: 10px;">
                                                                    <label for="esptipo_denuncia"><i
                                                                                class="fa fa-edit"></i> Especifique</label>
                                                                    <input type="text" id="ind_especificacion_tipo_denuncia" class="form-control" maxlength="250"
                                                                           id="ind_especificacion_tipo_denuncia" name="form[txt][ind_especificacion_tipo_denuncia]" value="{if isset($formDB.ind_especificacion_tipo_denuncia)} {$formDB.ind_especificacion_tipo_denuncia} {/if}"
                                                                            {if isset($ver) and $ver==1} readonly {/if}>
                                                                </div>
                                                            </div>

                                                        </div>


                                                        <div class="col-sm-12">
                                                        <!---descripcion-->
                                                            <div class="col-sm-2">
                                                                <label for="ind_descripcion_denuncia" class="control-label" style="margin-bottom: 10px;"><i
                                                                            class="fa fa-edit"></i>Descripción:</label>
                                                            </div>
                                                            <div class="col-sm-10">
                                                                <div style="margin-left: 10px; " id="ind_descripcion_denunciaError" >
                                                                    <textarea id="ind_descripcion_denuncia" maxlength="250" class="form-control" rows="2" name="form[txt][ind_descripcion_denuncia]"
                                                                            {if isset($ver) and $ver==1} readonly {/if}>{if isset($formDB.ind_descripcion_denuncia)} {$formDB.ind_descripcion_denuncia} {/if}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12">
                                                        <!---Motivo-->
                                                            <div class="col-sm-2 ">
                                                                <label for="ind_motivo_origen" class="control-label" style="margin-bottom: 10px;"><i
                                                                            class="fa fa-edit"></i>Motivo:</label>
                                                            </div>
                                                            <div class="col-sm-10">
                                                                <div style="margin-left: 10px;" id="ind_motivo_origenError" >
                                                                    <textarea id="ind_motivo_origen" maxlength="255" class="form-control" rows="2" name="form[txt][ind_motivo_origen]"
                                                                            {if isset($ver) and $ver==1} readonly {/if}>{if isset($formDB.ind_motivo_origen)} {$formDB.ind_motivo_origen} {/if}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <!---propuesta-->
                                                            <div class="col-sm-2">
                                                                <label for="ind_propuesta" class="control-label" style="margin-bottom: 10px;"><i
                                                                            class="fa fa-edit"></i>Propuesta:</label>
                                                            </div>
                                                            <div class="col-sm-10">
                                                                <div style="margin-left: 10px; margin-bottom: 10px" id="ind_propuestaError" >
                                                                    <textarea id="ind_propuesta" maxlength="200" class="form-control" rows="2" name="form[txt][ind_propuesta]"
                                                                            {if isset($ver) and $ver==1} readonly {/if}>{if isset($formDB.ind_propuesta)} {$formDB.ind_propuesta} {/if}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                <!--cierer INFORMACIÓN DE LA SOLICITUD-->

                                <!--PANEL FUNCIONARIOS RECEPTORES-->
                                <div class="tab-pane" id="tab2">
                                    <div class="row">

                                                <div class="col-lg-9">
                                                    <div class="card">
                                                        <div class="card-body no-padding">
                                                            <div class="table-responsive no-margin">
                                                                <div class="card-head card-head-xs style-primary text-center">
                                                                    <header class="text-center">
                                                                        RECIBIDO POR</header>
                                                                </div>
                                                                <table class="table table-striped no-margin" id="contenidoTabla2">
                                                                    <thead>
                                                                    <tr>
                                                                        <th class="text-rigth" width="10%">Documento de identidad</th>
                                                                        <th width="40%">Nombre completo</th>
                                                                        <th width="40%">Dependencia</th>
                                                                        <th width="10%"></th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody id="RecibidoPor">
                                                                    {if !isset($ver) }
                                                                        {$cant=1}
                                                                    <tr class="idRecibido{$sesion}">
                                                                        <input type="hidden" value="{$sesion}" name="form[int][recibido][idPersona][0]"/>
                                                                        <input type="hidden" value="{$ci}" name="form[txt][recibido][ind_cedula_documento][0]"/>
                                                                        <input type="hidden" value="{$nombre}" name="form[txt][recibido][persona][0]"/>
                                                                        <td width="20%">
                                                                           {$ci}
                                                                        </td>
                                                                        <td width="35%">
                                                                            {$nombre}
                                                                        </td>
                                                                        <td width="35%">
                                                                            {$dependencia}
                                                                        </td>
                                                                        <td width="10%">
                                                                            <button class="eliminarRecibido btn ink-reaction btn-raised btn-xs btn-danger delete"
                                                                                    borrar="idRecibido{$sesion}" {if isset($ver) and $ver==1} disabled {/if}>
                                                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                    {/if}
                                                                    {if isset($formTab)}
                                                                        {$n=1}
                                                                        {$cant=count($formTab)}
                                                                        {foreach item=dn from=$formTab}
                                                                            <tr class="idRecibido{$dn.pk_num_receptor}">
                                                                                <input type="hidden" value="{$dn.codigo}" name="form[int][recibido][idPersona][{$n}]"/>
                                                                                <input type="hidden" value="{$dn.cedula}" name="form[txt][recibido][ind_cedula_documento][{$n}]"/>
                                                                                <input type="hidden" value="{$dn.nombre}" name="form[txt][recibido][persona][{$n}]"/>
                                                                                <td width="20%">
                                                                                    {$dn.cedula}
                                                                                </td>
                                                                                <td width="35%">
                                                                                    {$dn.nombre}
                                                                                </td>
                                                                                <td width="35%">
                                                                                    {$dn.ind_dependencia}
                                                                                </td>
                                                                                <td width="10%">
                                                                                    <button class="eliminarRecibido btn ink-reaction btn-raised btn-xs btn-danger delete"
                                                                                            borrar="idRecibido{$dn.pk_num_receptor}" {if isset($ver) and $ver==1} disabled {/if}>
                                                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                                                    </button>
                                                                                </td>
                                                                            </tr>
                                                                            {$n=$n+1}
                                                                        {/foreach}
                                                                    {/if}
                                                                    </tbody>
                                                                    <tfoot>
                                                                    <tr>
                                                                        <td colspan="3">
                                                                            <div class="col-sm-offset-3 col-sm-6">
                                                                                <button
                                                                                        type="button"
                                                                                        class="btn btn-info ink-reaction btn-raised"
                                                                                        id="agregarRecibido"
                                                                                        data-toggle="modal" data-target="#formModal2"
                                                                                        data-keyboard="false" data-backdrop="static"
                                                                                        rows="{$cant}"
                                                                                        titulo="Insertar Funcionario"
                                                                                        url="{$_Parametros.url}modDN/solicitud/solicitudCONTROL/personasMET/MODAL"
                                                                                        {if isset($ver) and $ver==1} disabled {/if}
                                                                                ><i class="md md-add"></i> Insertar Funcionario Receptor
                                                                                </button>
                                                                            </div>
                                                                        </td>

                                                                    </tr>
                                                                    </tfoot>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="card">
                                                        <div class="card-body no-padding">
                                                            <div class="card-head card-head-xs style-primary text-center">
                                                                <header class="text-center">
                                                                    FECHA DE LA RECEPCIÓN</header>
                                                            </div>
                                                            <div class="input-group date" style="margin-bottom: 30px;">
                                                                <div class="col-sm-12">
                                                                    <span class="input-group-addon">
                                                                    <input type="text"
                                                                           style="text-align: center"
                                                                           name="form[txt][fe_recibido]"
                                                                           class="form-control" id="fe_recibido"
                                                                           id="datepicker"
                                                                           value="{if isset($formDB.fec_recepcion)} {($formDB.fec_recepcion)|date_format:"%d-%m-%Y"}{else}{$fecha_actual}{/if}"
                                                                           readonly >
                                                                    <i class="fa fa-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                    </div>
                                </div>
                                <!---cierre FUNCIONARIOS RECEPTORES-->

                                <!--PANEL DENUNCIANTES-->
                                <div class="tab-pane" id="tab3">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-body no-padding">
                                                        <div class="table-responsive no-margin">
                                                            <div class="card-head card-head-xs style-primary text-center">
                                                                <header class="text-center">
                                                                    DENUNCIANTES</header>
                                                            </div>
                                                            <div class="card-body">
                                                                <div class="col-lg-12">
                                                                    <div class="card">
                                                                        <div class="card-body no-padding">
                                                                            <div class="table-responsive no-margin">
                                                                                <table class="table table-striped no-margin" id="contenidoTabla3">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th class="text-rigth" width="30%">Documento de identidad</th>
                                                                                        <th width="60%">Nombre completo</th>
                                                                                        <th width="10%"></th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody id="Persona">
                                                                                    {if isset($formTab2)}
                                                                                        {$n=1}
                                                                                        {$cant=count($formTab2)}
                                                                                        {foreach item=dn from=$formTab2}
                                                                                            <tr class="idPersona">
                                                                                                <input type="hidden" value="{$dn.codigo}" name="form[int][denunciantes][idPersona][{$n}]" class="IdrepInput" idPersona="{$dn.codigo}"/>
                                                                                                <input type="hidden" value="{$dn.cedula}" name="form[txt][denunciantes][ind_cedula_documento][{$n}]" idPersona="{$dn.codigo}"/>
                                                                                                <input type="hidden" value="{$dn.nombre}" name="form[txt][denunciantes][persona][{$n}]"/>
                                                                                                <td width="30%">
                                                                                                    {$dn.cedula}
                                                                                                </td>
                                                                                                <td width="60%">
                                                                                                    {$dn.nombre}
                                                                                                </td>
                                                                                                <td width="10%">
                                                                                                    <button class="eliminarPersona btn ink-reaction btn-raised btn-xs btn-danger delete"
                                                                                                            borrar="idPersona{$dn.fk_a003_num_persona}" {if isset($ver) and $ver==1} disabled {/if}>
                                                                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                                                                    </button>
                                                                                                </td>
                                                                                            </tr>
                                                                                            {$n=$n+1}
                                                                                        {/foreach}
                                                                                    {/if}
                                                                                    </tbody>
                                                                                    <tfoot>
                                                                                    <tr>
                                                                                        <td colspan="1">
                                                                                            <div class="col-sm-offset-3 col-sm-6">
                                                                                                <button
                                                                                                    type="button"
                                                                                                    class="btn btn-info ink-reaction btn-raised"
                                                                                                    id="agregarDenunciante"
                                                                                                    data-toggle="modal" data-target="#formModal2"
                                                                                                    data-keyboard="false" data-backdrop="static"
                                                                                                    rows="{$cant}"
                                                                                                    titulo="Insertar Denunciante"
                                                                                                    url="{$_Parametros.url}modDN/solicitud/solicitudCONTROL/personasMET/MODAL"
                                                                                                    {if isset($ver) and $ver==1} disabled {/if}
                                                                                                ><i class="md md-add"></i> Insertar Denunciante
                                                                                                </button>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td colspan="2">
                                                                                            <div class="col-sm-offset-3 col-sm-6">
                                                                                                <button
                                                                                                        type="button"
                                                                                                        class="btn btn-info ink-reaction btn-raised"
                                                                                                        id="agregarPersona"
                                                                                                        data-toggle="modal" data-target="#formModal2"
                                                                                                        data-keyboard="false" data-backdrop="static"
                                                                                                        titulo="Registrar Persona"
                                                                                                        {if isset($ver) and $ver==1} disabled {/if}

                                                                                                        url="{$_Parametros.url}modDN/solicitud/solicitudCONTROL/crearModificarPersonaMET/MODAL">
                                                                                                    <!--url="{$_Parametros.url}modCV/maestros/personaCONTROL/crearModificarMET/MODAL"-->
                                                                                                <i class="md md-add"></i> Registrar Persona
                                                                                                </button>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    </tfoot>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <!---cierre PANEL DENUNCIANTES-->

                                <!---PANEL DE PRUEBAS-->
                                <div class="tab-pane" id="tab4">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="card">
                                                    <div class="card-body no-padding">
                                                        <div class="table-responsive no-margin">
                                                            <div class="card-head card-head-xs style-primary text-center">
                                                                <header class="text-center">
                                                                    DOCUMENTOS CONSIGNADOS / PRUEBAS</header>
                                                            </div>
                                                            <div class="card">
                                                                <div class="card-body no-padding">
                                                                    <div class="table-responsive no-margin">

                                                                        <table class="table table-striped no-margin" id="contenidoTabla">

                                                                            <thead>
                                                                            <tr>
                                                                                <th class="text-center">#</th>
                                                                                <th>Descripción *</th>
                                                                                <th>Numeración</th>
                                                                                <th>Cantidad *</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            {if isset($formTab3)}
                                                                                {$FilaNro=1}
                                                                                {foreach item=opciones from=$formTab3}

                                                                                    <tr>
                                                                                        <td class="text-right" style="vertical-align: middle;">
                                                                                            {$FilaNro}
                                                                                        </td>
                                                                                        <td style="vertical-align: middle;">
                                                                                            <div class="col-sm-10">
                                                                                                <div class="form-group floating-label" id="ind_prueba{$FilaNro}Error">
                                                                                                    <input type="text" class="form-control" value="{$opciones.ind_descripcion}" name="form[txt][prueba][secuencia][ind_prueba][{$FilaNro}]" id="ind_prueba{$FilaNro}">
                                                                                                    <label for="ind_prueba{$opciones.FilaNro}"><i class="md md-insert-comment"></i> Descripción </label>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="vertical-align: middle;">
                                                                                            <div class="col-sm-6">
                                                                                                <div class="form-group floating-label" id="ind_numero_documento{$FilaNro}Error">
                                                                                                    <input type="text" class="form-control" value="{$opciones.ind_numero_documento}" name="form[txt][prueba][secuencia][ind_numero_documento][{$FilaNro}]" id="ind_numero_documento{$FilaNro}"
                                                                                                            {if isset($ver) and $ver==1} readonly {/if}>
                                                                                                    <label for="ind_numero_documento{$FilaNro}"><i class="md md-insert-comment"></i> Número </label>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="vertical-align: middle;">
                                                                                            <div class="col-sm-6">
                                                                                                <div class="form-group floating-label" id="num_cantidad_prueba{$FilaNro}Error">
                                                                                                     <input type="number" onkeypress="return soloNumeros(event)" class="form-control valida_cantidad_prueba" value="{$opciones.num_cantidad}" name="form[txt][prueba][secuencia][num_cantidad_prueba][{$FilaNro}]" id="num_cantidad_prueba{$FilaNro}"
                                                                                                             {if isset($ver) and $ver==1} readonly {/if}>
                                                                                                     <label for="num_cantidad_prueba{$FilaNro}"><i class="md md-insert-comment"></i> Cantidad </label>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td  class="text-center" style="vertical-align: middle;">
                                                                                            <button class="btn ink-reaction btn-raised btn-xs btn-danger delete" {if isset($ver) and $ver==1} disabled {/if}>
                                                                                                <i class="md md-delete"></i>
                                                                                            </button>
                                                                                        </td>

                                                                                    </tr>
                                                                                    {$FilaNro=$FilaNro+1}
                                                                                {/foreach}
                                                                            {/if}
                                                                            </tbody>
                                                                            <tfoot>
                                                                                <tr>
                                                                                    <td colspan="4">
                                                                                        <div class="col-sm-offset-4 col-sm-6">
                                                                                            <button type="button" id="nuevoCampo" class="btn btn-info ink-reaction btn-raised"
                                                                                                    {if isset($ver) and $ver==1} disabled {/if}>
                                                                                                <i class="md md-add"></i> Insertar Nuevo Documento
                                                                                            </button>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </tfoot>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                </div>
                                <!---cierre PANEL DE PRUEBAS-->

                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group floating-label">
                                        <label for="preparado">Preparado por:</label>
                                    </div>
                                    <div class="form-group text-left">
                                        <label id="lbl_preparadopor" class="help-block text-left" style="margin-top: 10px;">{if isset($formDB.nombre_elabora)} {$formDB.nombre_elabora}{/if}</label>
                                    </div>
                                    <div class="form-group text-left">
                                        <label id="f_preparadopor" class="help-block text-left" style="margin-top: 10px;">{if isset($formDB.fec_elaborado)} {$formDB.fec_elaborado}{/if}</label>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group floating-label">
                                        <label for="rechazado">Rechazado por:</label>
                                    </div>
                                    <div class="form-group text-left">
                                        <label id="lbl_rechazadopor" class="help-block text-left" style="margin-top: 10px;">{if isset($formDB.nombre_anula)} {$formDB.nombre_anula}{/if}</label>
                                    </div>
                                    <div class="form-group">
                                        <label id="f_rechazadopor" class="help-block" style="margin-top: 10px;">{if isset($formDB.fec_anula_tramite)} {$formDB.fec_anula_tramite}{/if}</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group floating-label">
                                        <label for="aprobado">Aprobado por:</label>
                                    </div>
                                    <div class="form-group text-left">
                                        <label id="lbl_aprobadopor" class="help-block text-left" style="margin-top: 10px;">{if isset($formDB.nombre_aprueba)} {$formDB.nombre_aprueba}{/if}</label>
                                    </div>
                                    <div class="form-group">
                                        <label id="f_aprobadopor" class="help-block" style="margin-top: 10px;">{if isset($formDB.fec_aprueba_tramite)} {$formDB.fec_aprueba_tramite}{/if}</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group floating-label">
                                        <label for="ultmodificacion"><i class="fa fa-calendar"></i>Última modificación:</label>
                                    </div>
                                    <div class="form-group text-left">
                                        <label id="lbl_fehareg" class="help-block text-left" style="margin-top: 10px;">{if isset($formDB.nombre_usuario)} {$formDB.nombre_usuario}{/if}</label>
                                    </div>
                                    <div class="form-group">
                                        <label id="f_fehareg" class="help-block" style="margin-top: 10px;">{if isset($formDB.fec_modificacion)} {$formDB.fec_modificacion }{/if}</label>
                                    </div>
                                </div>
                            </div>
                            <ul class="pager wizard">
                                <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                                <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                                <li class="next last"><a class="btn-raised" href="javascript:void(0);">&Uacute;ltimo</a></li>
                                <li class="next"><a class="btn-raised" href="javascript:void(0);">Pr&oacute;ximo</a></li>
                            </ul>
                         </form>
                    </div>
                </div>
            </div>
       </div>
   </div>
</div>
<span class="clearfix"></span>
<div class="modal-footer">
    <button type="button" class="btn btn-default logsUsuarioModal " descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal">
        <i class="glyphicon glyphicon-floppy-remove"></i> {if !isset($ver) or $ver==0}  Cancelar {else} Cerrar {/if}
    </button>
    {if !isset($ver)}
        <button type="button" id="btn_guarda" class="btn btn-primary logsUsuarioModal">
            <i class="glyphicon glyphicon-floppy-disk"></i> Guardar
        </button>
    {elseif $ver==0}
        <button type="button" id="btn_guarda" class="btn btn-primary logsUsuarioModal">
            <i class="glyphicon glyphicon-floppy-disk"></i> Modificar
        </button>
    {/if}
</div>

<script type="text/javascript">

    $(document).ready(function () {
        //Activar el wizard
        var app = new AppFunciones();
        app.metWizard();
        // ancho de la Modal
        $('#modalAncho').css("width", "90%"); $('#ContenidoModal').css("padding-top", "0");
        //Complementos
        $('.select2').select2({ allowClear: true });

        $('.date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: "dd-mm-yyyy",
            language: 'es',
            maxDate:'+0M +0D'});

        $('#tb_auditores_disp').hide(); $('#btnVolver').hide();

        // anular accion del Formulario
        $("#formAjaxTab1").submit(function(){
            return false; });

        $("#formAjaxTab1").keypress(function(e){
            if (e.which == 13){ return false; }else{ return true; }
        })

        $('#agregarDenunciante').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            var cargar=$('#contenidoTabla3 >tbody >tr').length;
            $.post($(this).attr('url'), { cargar: cargar, idPersona: 'PER', tipo: 'PER' }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#agregarPersona').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            var cargar=$('#contenidoTabla3 >tbody >tr').length;
            $.post($(this).attr('url'), { cargar: cargar,idPersona: 'PER' }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#agregarRecibido').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            var cargar=$('#contenidoTabla2 >tbody >tr').length;
            $.post($(this).attr('url'), { cargar: cargar ,idPersona: 'EMP', tipo: 'EMP' }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#contenidoTabla2').on('click', '.delete', function () {
            $(this).parent('td').parent('tr').remove();
        });
        $('#contenidoTabla3').on('click', '.delete', function () {
            $(this).parent('td').parent('tr').remove();
        });

        $('.btn_guardaModal').click(function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal').html($dato);
            });
        });

        $('#btn_guarda').click(function () {
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });

            $.post($("#formAjaxTab1").attr("action"), $( "#formAjaxTab1" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='errorRec'){
                    swal("Error!", 'Disculpa. No se han seleccionado Receptor (es)', "error");
                }else if(dato['status']=='errorPer'){
                    swal("Error!", 'Disculpa. No se han seleccionado Denunciante (s)', "error");
                }else if(dato['status']=='errorPrueba'){
                    swal("Error!", 'Disculpa. No se han ingresado Pruebas (s)', "error");
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'La solicitud fue modificada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTablaJson('dataTablaJson', 'La solicitud fue guardada satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });

        $('#idMunicipio').change(function(){
            $.post('{$_Parametros.url}sector/jsonParroquiaMET', { idMunicipio: $(this).val() },function(dato){
                $('#idParroquia').html('');
                $('#idParroquia').append('<option value="">Seleccione la Parroquia</option>');
                $('#s2id_idParroquia .select2-chosen').html('Seleccione la Parroquia');
                for (var i =0; i < dato.length; i=i+1) {
                    $('#idParroquia').append('<option value="'+dato[i]['pk_num_parroquia']+'">'+dato[i]['ind_parroquia']+'</option>');
                }
            },'json');
        });
        $('#idMunicipio').ready(function(){
            if ($("#idMunicipio").length > 0) {
                var idParroquia=$('#idParroquia').attr('parroquia');
                $.post('{$_Parametros.url}sector/jsonParroquiaMET', { idMunicipio: $("#idMunicipio").val() },function(dato){
                    $('#idParroquia').html('');
                    $('#idParroquia').append('<option value="">Seleccione la Parroquia</option>');
                    $('#s2id_idParroquia .select2-chosen').html('Seleccione la Parroquia');
                    for (var i =0; i < dato.length; i=i+1) {
                        if(dato[i]['pk_num_parroquia']==idParroquia){
                            $('#idParroquia').append('<option selected value="'+dato[i]['pk_num_parroquia']+'">'+dato[i]['ind_parroquia']+'</option>');
                            $('#s2id_idParroquia .select2-chosen').html(dato[i]['ind_parroquia']);
                        } else {
                            $('#idParroquia').append('<option value="'+dato[i]['pk_num_parroquia']+'">'+dato[i]['ind_parroquia']+'</option>');
                        }
                    }
                },'json');
            }
        });
        $('#idParroquia').change(function(){
            $.post('{$_Parametros.url}modDN/denuncias/denunciasCONTROL/jsonSectorMET', { idParroquia: $(this).val() },function(dato){
                $('#num_sector').html('');
                $('#num_sector').append('<option value="">Seleccione el Sector</option>');
                $('#s2id_num_sector .select2-chosen').html('Seleccione el Sector');
                for (var i =0; i < dato.length; i=i+1) {
                    $('#num_sector').append('<option value="'+dato[i]['pk_num_sector']+'">'+dato[i]['ind_sector']+'</option>');
                }
            },'json');
        });
        $('#idParroquia').ready(function(){
            if ($("#idParroquia").length > 0) {
                var num_sector=$('#num_sector').attr('sector');

                $.post('{$_Parametros.url}modDN/denuncias/denunciasCONTROL/jsonSectorMET', { idParroquia: $('#idParroquia').attr('parroquia') },function(dato){
                    $('#num_sector').html('');
                    $('#num_sector').append('<option value="">Seleccione el Sector</option>');
                    $('#s2id_num_sector .select2-chosen').html('Seleccione el Sector');

                    for (var i =0; i < dato.length; i=i+1) {
                        if(dato[i]['pk_num_sector']==num_sector){
                            $('#num_sector').append('<option selected value="'+dato[i]['pk_num_sector']+'">'+dato[i]['ind_sector']+'</option>');
                            $('#s2id_num_sector .select2-chosen').html(dato[i]['ind_sector']);
                        } else {
                            $('#num_sector').append('<option value="'+dato[i]['pk_num_sector']+'">'+dato[i]['ind_sector']+'</option>');
                        }
                    }
                },'json');
            }
        });

        $('#id_dependencia').change(function(){
            $.post('{$_Parametros.url}modDN/solicitud/solicitudCONTROL/jsonCentroCostoMET', { id_dependencia: $("#id_dependencia").val() },function(dato){

                $('#id_centro_costo').html('');
                $('#id_centro_costo').append('<option value="">Seleccione el Centro de Costo</option>');
                $('#s2id_id_centro_costo .select2-chosen').html('Seleccione el Centro de Costo');
                for (var i =0; i < dato.length; i=i+1) {
                    $('#id_centro_costo').append('<option value="'+dato[i]['pk_num_centro_costo']+'">'+dato[i]['ind_descripcion_centro_costo']+'</option>');
                }
            },'json');
        });
        $('#id_dependencia').ready(function(){
                var id_centro_costo=$('#id_centro_costo').attr('centro');
                $.post('{$_Parametros.url}modDN/solicitud/solicitudCONTROL/jsonCentroCostoMET', { id_dependencia: $("#id_dependencia").val() },function(dato){
                    $('#id_centro_costo').html('');
                    $('#id_centro_costo').append('<option value="">Seleccione el Centro de Costo</option>');
                    $('#s2id_id_centro_costo .select2-chosen').html('Seleccione el Centro de Costo');
                    for (var i =0; i < dato.length; i=i+1) {
                        if(dato[i]['pk_num_centro_costo']==id_centro_costo){
                            $('#id_centro_costo').append('<option selected value="'+dato[i]['pk_num_centro_costo']+'">'+dato[i]['ind_descripcion_centro_costo']+'</option>');
                            $('#s2id_id_centro_costo .select2-chosen').html(dato[i]['ind_descripcion_centro_costo']);
                        } else {
                            $('#id_centro_costo').append('<option value="'+dato[i]['pk_num_centro_costo']+'">'+dato[i]['ind_descripcion_centro_costo']+'</option>');
                        }
                    }
                },'json');

        });

        $('#tipoActuacion').change(function(){
            $.post('{$_Parametros.url}modDN/solicitud/solicitudCONTROL/jsonTipoActuacionTextoMET', { tipoActuacion: $(this).val() },function(dato){
                $('#tipoActuacionTexto').val(dato['tipoActuacionTexto']);
            },'json');
        });
        //***************************** Llenado campo pruebas ********************************************/

        $('#contenidoTabla  tbody').on('keyup change', 'tr td .floating-label input' ,function (e) {
            var input = $(e.currentTarget);
            if ($.trim(input.val()) !== '') {
                input.addClass('dirty').removeClass('static');
            } else {
                input.removeClass('dirty').removeClass('static');
            }
        });
        $('#contenidoTabla').on('click', '.delete', function () {
            $(this).parent('td').parent('tr').remove();
        });

        $("#nuevoCampo").click(function() {
            var idtabla= $("#contenidoTabla");
            var tr= $("#contenidoTabla > tbody > tr");
            var numTr=tr.length;
            var nuevoTr=numTr+1;

            //var numero=nuevoTr+1;
            var pruebas='<div class="form-group floating-label" id="ind_prueba'+nuevoTr+'Error">'+
                    '<input type="text" class="form-control" value="" name="form[txt][prueba][secuencia][ind_prueba]['+nuevoTr+']" id="ind_prueba'+nuevoTr+'">'+
                    '<label for="ind_prueba'+nuevoTr+'"><i class="md md-insert-comment"></i> Descripción</label>'+
                    '</div>';
            var numDoc='<div class="form-group floating-label" id="ind_numero_documento'+nuevoTr+'Error">'+
                    '<input type="text" class="form-control" value="" name="form[txt][prueba][secuencia][ind_numero_documento]['+nuevoTr+']" id="ind_numero_documento'+nuevoTr+'">'+
                    '<label for="ind_numero_documento'+nuevoTr+'"><i class="md md-insert-comment"></i> Número</label>'+
                    '</div>';
            var cantidad='<div class="form-group floating-label" id="num_cantidad_prueba'+nuevoTr+'Error">'+
                    '<input type="number" onkeypress="return soloNumeros(event)" class="form-control valida_cantidad_prueba" value="" name="form[txt][prueba][secuencia][num_cantidad_prueba]['+nuevoTr+']" id="num_cantidad_prueba'+nuevoTr+'">'+
                    '<label for="num_cantidad_prueba'+nuevoTr+'"><i class="md md-insert-comment"></i> Cant.</label>'+
                    '</div>';
            idtabla.append('<tr>'+
                    '<td class="text-right" style="vertical-align: middle;">'+nuevoTr+'</td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-10">'+pruebas+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-6">'+numDoc+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-6">'+cantidad+'</div></td>'+
                    '<td  class="text-center" style="vertical-align: middle;"><button class="eliminarPrueba btn ink-reaction btn-raised btn-xs btn-danger delete"><i class="md md-delete"></i></button></td>'+
                    '</tr>');

        });

        $('#idorigen_denuncia').change(function(){

            $.post('{$_Parametros.url}modDN/solicitud/solicitudCONTROL/jsonOrigenMET', { origen: $(this).val() },function(dato){

                $('#div_ind_especificacion_origen_denuncia').html('');
                var cadena='';
                if(dato.length!=0) {
                    cadena='<select id="ind_especificacion_origen_denuncia" name="form[txt][ind_especificacion_origen_denuncia]" class="form-control select2" style="height: 27px;">';
                    cadena=cadena+'<option value="">Seleccione...</option>';

                    $.each(dato,function(index, value){
                        cadena=cadena+'<option value="' + value.ind_nombre_ente+ '">' + value.ind_nombre_ente+ '</option>';
                    });


                    cadena=cadena+'</select>';
                }else{
                    cadena=cadena+'<label for="esporigen_denuncia"><i class="fa fa-edit"></i> Especifique</label>';
                    cadena=cadena+'<input type="text" id="ind_especificacion_origen_denuncia" class="form-control"' +
                            ' maxlength="250" name="form[txt][ind_especificacion_origen_denuncia]" ' +
                            'value=""> ';

                }
                $('#div_ind_especificacion_origen_denuncia').append(cadena);
            },'json');
        });

    });

    function soloNumeros(e){
        var key = e.keyCode || e.which;
        var tecla = String.fromCharCode(key).toLowerCase();
        var numeros = "0123456789";

        if(numeros.indexOf(tecla)==-1){
            return false;
        }
    }



</script>