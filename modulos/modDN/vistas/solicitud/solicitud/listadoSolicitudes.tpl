<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Solicitudes</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th style="width: 10%">Solicitud Nro.</th>
                            <th style="width: 10%">Tipo de Solicitud</th>
                            <th style="width: 40%">Ente</th>
                            <th style="width: 10%">Estatus Solicitud</th>
                            <th style="width: 20%">Acción</th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th colspan="5">
                                {if in_array('DN-01-01-01-N',$_Parametros.perfil) }
                                    <button class="nuevo logsUsuario btn ink-reaction btn-raised btn-info"
                                            descipcion="el Usuario ha creado la Solicitud Nro. " data-toggle="modal"
                                            data-target="#formModal" titulo="Registrar Nueva Solicitud" id="nuevo">
                                        <i class="md md-create"></i> Nueva Solicitud
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var $url = '{$_Parametros.url}modDN/solicitud/solicitudCONTROL/CrearModificarMET';
        var $url2 = '{$_Parametros.url}modDN/denuncias/denunciasCONTROL/CrearModificarMET';

        var app = new AppFunciones();
        var dt = app.dataTable(
            '#dataTablaJson',
            "{$_Parametros.url}modDN/solicitud/solicitudCONTROL/jsonDataTablaMET",
            "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
            [
                { "data": "cod_solicitud" },
                { "data": "tipoActuacion" },
                { "data": "ind_nombre_ente" },
                { "data": "estado"},
                { "orderable": false,"data": "acciones", width:150}
            ]
        );

        $('#nuevo').click(function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url, { idSolicitud: 0 }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url, { idSolicitud: $(this).attr('idSolicitud') }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url, { idSolicitud: $(this).attr('idSolicitud'), ver: 1}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.imprimir', function () {
            var idSolicitud = $(this).attr('idSolicitud');
            var url='{$_Parametros.url}modDN/solicitud/solicitudCONTROL/imprimirActaMET/?idSolicitud='+idSolicitud;
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( url,{  },function(dato){

                $('#ContenidoModal').html('<iframe frameborder="0" src="'+url+'" width="100%" height="640px"></iframe>');

            });
        });

        $('#dataTablaJson tbody').on( 'click', '.rechazar', function () {
            var idSolicitud = $(this).attr('idSolicitud');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modDN/solicitud/solicitudCONTROL/rechazarSolicitudMET';
                $.post($url, { idSolicitud: idSolicitud },function(dato){
                    if(dato==idSolicitud){
                        $(document.getElementById('idSolicitud'+dato['id'])).remove();
                        swal("Solicitud Recahazada!", "La Solicitud fue rechazada satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.planificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url2, { idSolicitud: $(this).attr('idSolicitud'), tipoActuacionTexto: $(this).attr('tipoActuacion'), acc:'nuevo' }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.aprobar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url2, { idDenuncia: $(this).attr('idDenuncia'), tipoActuacionTexto: $(this).attr('tipoActuacion'), acc:$(this).attr('id') }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
    });
</script>