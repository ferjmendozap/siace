<section class="style-default-bright">
    <input type="hidden" id="cant" name="cant" value="{$cant}">
    <input type="hidden" id="tipo" name="tipo" value="{$tipo}">
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th width="6">Check</th>
                            <th width="50">Cedula</th>
                            {if $tipo=='PER'}
                                <th width="130">Nombres y Apellidos</th>
                            {else}
                                <th width="70">Nombres y Apellidos</th>
                                <th width="60">Dependencia</th>
                            {/if}
						</tr>
                        </thead>
                        <tbody>
                        {foreach item=persona from=$listado}
                            <tr id="idPersona{$persona.pk_num_persona}">
                                <td>
                                   <div class="checkbox checkbox-styled">
                                        <label>
                                           <input type="checkbox" class="valores" idPersona="{$persona.pk_num_persona}"
                                                  persona="{$persona.ind_nombre1} {$persona.ind_nombre2} {$persona.ind_apellido1} {$persona.ind_apellido2}"  cedula="{$persona.ind_cedula_documento}"
                                                  {if $tipo!='PER'} dependencia="{$persona.ind_dependencia}" {/if}>

                                        </label>
                                    </div>
                                </td>
								 <td><label>{$persona.ind_cedula_documento}</label></td>
                                 <td><label>{$persona.ind_nombre1} {$persona.ind_nombre2} {$persona.ind_apellido1} {$persona.ind_apellido2}</label></td>
                            {if $tipo!='PER'}
                                <td><label>{$persona.ind_dependencia}</label></td>
                            {/if}
                            </tr>
                        {/foreach}
                        </tbody>

                    </table>

                </div>
            </div>
        </div>
    </div>

</section>
<span class="clearfix"></span>
<div class="modal-footer">
    <button type="button"   title="Cancelar"  class="btn btn-default ink-reaction btn-raised" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar</button>
	<button type="button"  title="Agregar"  class="btn btn-primary ink-reaction btn-raised" id="agregarPersonaSeleccionada"><span class="glyphicon glyphicon-floppy-disk"></span> &nbsp;Agregar</button>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });



        $('#agregarPersonaSeleccionada').click(function () {
            var cant=$("#cant").val();
            var tipo=$("#tipo").val();
            var input = $('.valores');
            if(cant!='') {
                var j = parseInt(cant)+parseInt(1);
            }else{
                var j=1;
            }
            var per='';
            for (i = 0; i < input.length; i++) {
                if (input[i].checked == true) {
                    if(tipo=='PER') {
                        if (($("tr.idPersona" + input[i].getAttribute('idPersona')).parents('tbody')).length != 0){
                            swal({
                                title: input[i].getAttribute('persona') ,
                                text: "Ya se encuentra seleccionado",
                                showConfirmButton: true
                            });

                        }else {
                            per='<tr class="idPersona'+input[i].getAttribute('idPersona')+'">' +

                            '<input type="hidden" value="' + input[i].getAttribute('idPersona') + '"  name="form[int][denunciantes][idPersona][' + j + ']" class="IdrepInput" idPersona="' + input[i].getAttribute('idPersona') + '" />' +
                            '<input type="hidden" value="' + input[i].getAttribute('cedula') + '" name="form[txt][denunciantes][ind_cedula_documento][' + j + ']" class="IdrepInput" idPersona="' + input[i].getAttribute('idPersona') + '" />' +

                            '<input type="hidden" value="' + input[i].getAttribute('persona') + '" name="form[txt][denunciantes][persona][' + j + ']" class="representantInput" persona="' + input[i].getAttribute('persona') + '" />' +


                            '<td width="30%">' + input[i].getAttribute('cedula') + '</td>' +
                            '<td width="60%">' + input[i].getAttribute('persona') + '</td>' +
                            '<td width="10%"><button class="eliminarPersona  btn ink-reaction btn-raised btn-xs btn-danger delete" borrar="idPersona' + input[i].getAttribute('idPersona') + '"><i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
                            '</tr>';
                            $(document.getElementById('Persona')).append(per);
                        }

                    }else if(tipo=='EMP'){
                        if (($("tr.idRecibido" + input[i].getAttribute('idPersona')).parents('tbody')).length != 0){
                            swal({
                                title: input[i].getAttribute('persona') ,
                                text: "Ya se encuentra seleccionado",
                                showConfirmButton: true
                            });
                        }else {
                            per='<tr class="idRecibido'+input[i].getAttribute('idPersona')+'">' +

                            '<input type="hidden" value="' + input[i].getAttribute('idPersona') + '"  name="form[int][recibido][idPersona][' + j + ']" class="IdrepInput" idPersona="' + input[i].getAttribute('idPersona') + '" />' +
                            '<input type="hidden" value="' + input[i].getAttribute('cedula') + '" name="form[txt][recibido][ind_cedula_documento][' + j + ']" class="IdrepInput" idPersona="' + input[i].getAttribute('idPersona') + '" />' +

                            '<input type="hidden" value="' + input[i].getAttribute('persona') + '" name="form[txt][recibido][persona][' + j + ']" class="representantInput" persona="' + input[i].getAttribute('persona') + '" />' +


                            '<td width="20%">' + input[i].getAttribute('cedula') + '</td>' +
                            '<td width="35%">' + input[i].getAttribute('persona') + '</td>' +
                            '<td width="35%">' + input[i].getAttribute('dependencia') + '</td>' +
                            '<td width="10%"><button class="eliminarRecibido  btn ink-reaction btn-raised btn-xs btn-danger delete" borrar="idRecibido' + input[i].getAttribute('idPersona') + '"><i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
                            '</tr>';
                            $(document.getElementById('RecibidoPor')).append(per);
                        }

                    }else{
                        if (($("tr.idPersona" + input[i].getAttribute('idPersona')).parents('tbody')).length != 0){
                            swal({
                                title: input[i].getAttribute('persona') ,
                                text: "Ya se encuentra seleccionado",
                                showConfirmButton: true
                            });
                        }else {

                                per='<tr class="idPersona'+input[i].getAttribute('idPersona')+'">' +

                                '<input type="hidden" value="' + input[i].getAttribute('idPersona') + '"  name="form[txt][auditor][idPersona][' + j + ']" class="IdrepInput" idPersona="' + input[i].getAttribute('idPersona') + '" />' +
                                '<input type="hidden" value="' + input[i].getAttribute('cedula') + '" name="form[txt][auditor][ind_cedula_documento][' + j + ']" class="IdrepInput" idPersona="' + input[i].getAttribute('idPersona') + '" />' +

                                '<input type="hidden" value="' + input[i].getAttribute('persona') + '" name="form[txt][auditor][persona][' + j + ']" class="representantInput" persona="' + input[i].getAttribute('persona') + '" />' +


                                '<td width="20%">' + input[i].getAttribute('cedula') + '</td>' +
                                '<td width="30%">' + input[i].getAttribute('persona') + '</td>' +
                                '<td width="30%">' + input[i].getAttribute('dependencia') + '</td>' +
                                '<td width="10%" align="center"><div class="radio radio-styled"><label>' +
                                    '<input type="radio" value="' + input[i].getAttribute('idPersona')+ '" id="coordinador" name="form[txt][auditor][coordinador]">' +
                                    '<span></span></label></div></td>'+
                                '<td width="10%"><button class="eliminarAuditor  btn ink-reaction btn-raised btn-xs btn-danger delete" borrar="idRecibido' + input[i].getAttribute('idPersona') + '"><i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
                                '</tr>';
                            $(document.getElementById('Auditores')).append(per);
                        }
                    }
                    j=j+1;
                }
            }

            $('#ContenidoModal2').html('');

            $('#cerrarModal2').click();
        });
    });
</script>