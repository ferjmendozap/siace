<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Actividades</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th style="width: 15%">Cod. Tramite.</th>
                            <th style="width: 10%">Tipo de Tramite</th>
                            <th style="width: 20%">Ente</th>
                            <th style="width: 15%">Estatus de la Actividad</th>
                            <th style="width: 25%">Fase - Actividad a Ejecutar</th>
                            <th style="width: 10%">Acción</th>
                        </tr>
                        </thead>


                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var $url = '{$_Parametros.url}modDN/ejecucion/ejecucionCONTROL/ejecutarActividadMET';

        var app = new AppFunciones();
        var dt = app.dataTable(
            '#dataTablaJson',
            "{$_Parametros.url}modDN/ejecucion/ejecucionCONTROL/jsonDataTablaMET",
            "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
            [
                { "data": "ind_num_denuncia" },
                { "data": "tipoActuacion" },
                { "data": "ind_nombre_ente" },
                { "data": "estado"},
                { "data": "FA"},
                { "orderable": false,"data": "acciones", width:150}
            ]
        );

        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url, { idDenuncia: $(this).attr('idDenuncia'), idActividad: $(this).attr('idActividad'), ver: 1, tipoActuacion: $(this).attr('tipo') }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.ejecutar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url, { idDenuncia:  $(this).attr('idDenuncia'), idActividad: $(this).attr('idActividad'), tipoActuacion:  $(this).attr('tipo')  }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
    });
</script>