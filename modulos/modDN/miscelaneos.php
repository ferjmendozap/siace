<?php

/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 4.6.5.2
 */

/**
 * Database `SIACE`
 */

/* `SIACE`.`a005_miscelaneo_maestro` */

$a005_miscelaneo_maestro = array(
    array('ind_nombre_maestro' => 'ORIGEN DENUNCIA','ind_descripcion' => 'ORIGEN DE LAS DENUNCIAS','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '19','cod_maestro' => 'DN-01-02',
        'Det' => array(
            array('cod_detalle' => '001','ind_nombre_detalle' => 'CONSEJO COMUNAL','num_estatus' => '1'),
            array('cod_detalle' => '002','ind_nombre_detalle' => 'ÓRGANO O ENTE PÚBLICO','num_estatus' => '1'),
            array('cod_detalle' => '003','ind_nombre_detalle' => 'OTRO','num_estatus' => '1')
    )),
    array('ind_nombre_maestro' => 'TIPO DENUNCIA','ind_descripcion' => 'TIPO DE DENUNCIA','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '19','cod_maestro' => 'DN-01-03',
        'Det' => array(
            array('cod_detalle' => '001','ind_nombre_detalle' => 'OBRAS','num_estatus' => '1'),
            array('cod_detalle' => '002','ind_nombre_detalle' => 'BIENES','num_estatus' => '1'),
            array('cod_detalle' => '003','ind_nombre_detalle' => 'SERVICIOS','num_estatus' => '1'),
            array('cod_detalle' => '004','ind_nombre_detalle' => 'OTROS','num_estatus' => '0')
    )),
    array('ind_nombre_maestro' => 'CODIFICACION','ind_descripcion' => 'ESTRUCTURA DEL CODIGO APLICADO POR CADA CONTRALORÍA PARA LAS DENUNCIAS, QUEJAS, RECLAMOS O SUGERENCI','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '19','cod_maestro' => 'DN-COD-01',
        'Det' => array(
            array('cod_detalle' => 'D','ind_nombre_detalle' => 'DACCC-D','num_estatus' => '1'),
            array('cod_detalle' => 'P','ind_nombre_detalle' => 'DACCC-P','num_estatus' => '1'),
            array('cod_detalle' => 'Q','ind_nombre_detalle' => 'DACCC-Q','num_estatus' => '1'),
            array('cod_detalle' => 'R','ind_nombre_detalle' => 'DACCC-R','num_estatus' => '1'),
            array('cod_detalle' => 'S','ind_nombre_detalle' => 'DACCC-S','num_estatus' => '1')
    )),

    array('ind_nombre_maestro' => 'ESTATUS SOLICITUDES','ind_descripcion' => 'ESTATUS DE LAS SOLICITUDE DE DENUNCIAS','num_estatus' => '0','fk_a015_num_seguridad_aplicacion' => '19','cod_maestro' => 'DN-EST-SOL',
        'Det' => array(
            array('cod_detalle' => 'PE','ind_nombre_detalle' => 'PENDIENTE','num_estatus' => '1'),
            array('cod_detalle' => 'RE','ind_nombre_detalle' => 'RECHAZADA','num_estatus' => '1'),
            array('cod_detalle' => 'TR','ind_nombre_detalle' => 'TRAMITADA','num_estatus' => '1')
    )),
    array('ind_nombre_maestro' => 'TIPOACTUACION','ind_descripcion' => 'TIPOACTUACION','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '19','cod_maestro' => 'TACT',
        'Det' => array(
            array('cod_detalle' => 'D','ind_nombre_detalle' => 'DENUNCIA','num_estatus' => '1'),
            array('cod_detalle' => 'R','ind_nombre_detalle' => 'RECLAMO','num_estatus' => '1'),
            array('cod_detalle' => 'P','ind_nombre_detalle' => 'PETICIÓN','num_estatus' => '1'),
            array('cod_detalle' => 'S','ind_nombre_detalle' => 'SUGERENCIA','num_estatus' => '1'),
            array('cod_detalle' => 'Q','ind_nombre_detalle' => 'QUEJA','num_estatus' => '1')
    )),
    array('ind_nombre_maestro' => 'ESTATUS TRAMITE','ind_descripcion' => 'ESTATUS DE LOS TRAMITES (D, P, S, R,Q)','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '19','cod_maestro' => 'DN-EST-TRA',
        'Det' => array(
            array('cod_detalle' => 'CE','ind_nombre_detalle' => 'CERRADA','num_estatus' => '1'),
            array('cod_detalle' => 'AA','ind_nombre_detalle' => 'AUTO DE ARCHIVO','num_estatus' => '1'),
            array('cod_detalle' => 'RM','ind_nombre_detalle' => 'REMITIDA','num_estatus' => '1'),
            array('cod_detalle' => 'TE','ind_nombre_detalle' => 'TERMINADA','num_estatus' => '1'),
            array('cod_detalle' => 'EJ','ind_nombre_detalle' => 'EN EJECUCIÓN','num_estatus' => '1'),
            array('cod_detalle' => 'AP','ind_nombre_detalle' => 'APROBADA','num_estatus' => '1'),
            array('cod_detalle' => 'RE','ind_nombre_detalle' => 'REVISADA','num_estatus' => '1'),
            array('cod_detalle' => 'GN','ind_nombre_detalle' => 'GENERADA','num_estatus' => '1'),
            array('cod_detalle' => 'PE','ind_nombre_detalle' => 'PENDIENTE','num_estatus' => '1'),
            array('cod_detalle' => 'EP','ind_nombre_detalle' => 'EN PREPARACIÓN','num_estatus' => '1'),
            array('cod_detalle' => 'AN','ind_nombre_detalle' => 'ANULADO','num_estatus' => '1')
    )),
    array('ind_nombre_maestro' => 'ESTATUS ACTIVIDAD','ind_descripcion' => 'ESTATUS ACTIVIDAD','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '19','cod_maestro' => 'DN-EST-ACT',
        'Det' => array(
            array('cod_detalle' => 'PE','ind_nombre_detalle' => 'PENDIENTE','num_estatus' => '1'),
            array('cod_detalle' => 'EJ','ind_nombre_detalle' => 'EN EJECUCIÓN','num_estatus' => '1'),
            array('cod_detalle' => 'TE','ind_nombre_detalle' => 'TERMINADA','num_estatus' => '1'),
            array('cod_detalle' => 'CE','ind_nombre_detalle' => 'CERRADA','num_estatus' => '1'),
            array('cod_detalle' => 'AA','ind_nombre_detalle' => 'AUTO DE ARCHIVO','num_estatus' => '1'),
            array('cod_detalle' => 'VA','ind_nombre_detalle' => 'VALORADA','num_estatus' => '1'),
            array('cod_detalle' => 'RM','ind_nombre_detalle' => 'REMITIDA','num_estatus' => '1')
        )),
    array('ind_nombre_maestro' => 'ESTATUS PRORROGA','ind_descripcion' => 'ESTATUS PRORROGA','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '19','cod_maestro' => 'DN-EST-PRO',
        'Det' => array(
            array('cod_detalle' => 'PR','ind_nombre_detalle' => 'EN PREPARACIÓN','num_estatus' => '1'),
            array('cod_detalle' => 'RV','ind_nombre_detalle' => 'REVISADA','num_estatus' => '1'),
            array('cod_detalle' => 'CO','ind_nombre_detalle' => 'CONFORMADA','num_estatus' => '1'),
            array('cod_detalle' => 'AP','ind_nombre_detalle' => 'APROBADA','num_estatus' => '1'),
            array('cod_detalle' => 'AN','ind_nombre_detalle' => 'ANULADA','num_estatus' => '1')
        ))

);


/* `SIACE`.`a035_parametros` */
$a035_parametros = array(
array('ind_descripcion' => 'Cantidad de digitos del correlativo','ind_explicacion' => 'Cantidad de digitos del correlativo aplicado en los codigos de las quejas, denuncias, reclamos, peticiones y sugerencias','ind_parametro_clave' => 'DIGITOS','ind_valor_parametro' => '4','num_estatus' => '1','fec_ultima_modificacion' => '2016-11-24 10:24:22','fk_a015_num_seguridad_aplicacion' => '19','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '2')
);