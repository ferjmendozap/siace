<form id="formAjax" class="form floating-label" action="{$_Parametros.url}modCR/reporteGeneralCONTROL/ReporteGeneralMET"
role="form" method="post" target="_blank" novalidate>
	<br/>
	<section class="style-default-bright">
	<br/>
	<div class="col-lg-16">
	<div class="card style-default-bright">
		<div class="card-head"><h2 class="text-primary">&nbsp;Consulta de Consumo de Red: Fechas</h2></div>
	</div>
	</div>
		<div class="section-body contain-lg">
			<br/><br/>
			<table align="center" class="table-responsive">
				<tr>
					<td>
						<div><div class="checkbox checkbox-styled"><label><input type="checkbox" value="1" name="guardar" id="guardar"><span>Guardar Reporte</span></div></div>
						<div class="section-body contain-lg">
							<div class="form-group" style="width:100%">
								<div class="input-daterange input-group" id="demo-date-range">
									<div class="input-group-content">
										<input type="text" class="form-control dirty" name="fechaInicio" id="fechaInicio">
										<label><i class="glyphicon glyphicon-calendar"></i> Rango de fecha</label>
									</div>
									<span class="input-group-addon">a</span>
									<div class="input-group-content">
										<input type="text" class="form-control dirty" name="fechaFin" id="fechaFin">
										<div class="form-control-line"></div>
									</div>
									<button type="submit"  class="btn ink-reaction btn-raised btn-primary" id="btnConsultar" style="margin-top:7px">Consultar</button>&nbsp; &nbsp;
								</div>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</section>
</form>
<script type="text/javascript">
	$(document).ready(function () {
		//validation rules
		$("#formAjax").validate({
			rules:{
				fechaInicio:{
					required:true
				},
				fechaFin:{
					required:true

				}
			},
			messages:{
				fechaInicio:{
					required: "La fecha de inicio es requerida"
				},
				fechaFin:{
					required: "La fecha final es requerida"
				}
			}
		});
	});
</script>
