<form id="formAjax" class="form floating-label"
role="form" method="post" target="_blank" novalidate>
	<br/>
	<section class="style-default-bright">
<br/>
		<div class="col-lg-16">
			<div class="card style-default-bright">
				<div class="card-head"><h2 class="text-primary">&nbsp;Consulta de Consumo de Red: Fechas</h2></div>
			</div>
		</div>
		<div class="section-body contain-lg">
			<table align="center" class="table-responsive">
				<tr>
					<td>
						<div class="section-body contain-lg">
							<div class="form-group" style="width:100%">
								<div class="input-daterange input-group" id="demo-date-range">
									<div class="input-group-content">
										<input type="text" class="form-control dirty" name="fechaInicio" id="fechaInicio">
										<label><i class="glyphicon glyphicon-calendar"></i> Rango de fecha</label>
									</div>
									<span class="input-group-addon">a</span>
									<div class="input-group-content">
										<input type="text" class="form-control dirty" name="fechaFin" id="fechaFin">
										<div class="form-control-line"></div>
									</div>
									<button type="button"  class="btn ink-reaction btn-raised btn-primary" id="btnConsultar"
									style="margin-top:7px"><i class="glyphicon glyphicon-calendar"></i> Consultar</button>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div style="width:90%;margin-left:auto;margin-right:auto;margin-top:30px">
			<div class="card">
				<div class="card-body">
					<div id="contenidoReporteGeneral" class="flot height-6" data-title="Consumo de red" data-color="#9C27B0,#0aa89e"></div>
				</div><!--end .card-body -->
			</div><!--end .card -->
			<em class="text-caption">Consumo de red</em>
		</div>

	</section>
</form>
<script type="text/javascript">
	$('.form-control').datepicker({
		format: 'dd/mm/yyyy',
		language: 'es',
		autoclose: true
	});
	$(document).ready(function () {
		//validation rules
		$("#formAjax").validate({
			rules:{
				fechaInicio:{
					required:true,
					date:true
				},
				fechaFin:{
					required:true,
					date:true
				}
			},
			messages:{
				fechaInicio:{
					required: "La fecha de inicio es requerida",
					date: "Formato de fecha inválido"
				},
				fechaFin:{
					required: "La fecha final es requerida",
					date: "Formato de fecha inválido"
				}
			}
		});
	});
	$(document).ready(function() {
		function mostrarGrafica(_data){
			var chart = $("#contenidoReporteGeneral");
			var labelColor = chart.css('color');
			var bytes = [];
			var fecha;
			var i;
			var temp;
			var options = {
				colors: chart.data('color').split(','),
				series: {
					shadowSize: 0,
					lines: {
						show: true,
						lineWidth: 2
					},
					points: {
						show: true,
						radius: 3,
						lineWidth: 2
					}
				},
				legend: {
					show: false
				},
				xaxis: {
					mode: "time",
					timeformat: "%d/%m/%Y",
					color: 'rgba(0, 0, 0, 0)',
					font: { color: labelColor },
					monthNames: ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
					dayNames: ["domingo", "lunes", "martes", "miercoles", "jueves", "viernes", "sabado"]
				},
				yaxis: {
					font: { color: labelColor }
				},
				grid: {
					borderWidth: 0,
					color: labelColor,
					hoverable: true
				}
			};

			for(i=0;i<_data.bytes.length;i++){
				fecha = new Date(_data.fecha[i]).getTime();
				//alert("La fecha es: "+fecha);
				temp = [fecha,_data.bytes[i]];
				bytes.push(temp);
			}
			var dataset = [{ label:"bytes",data:bytes,last:true,hoverable:true }];
			//$("#aqui").html(bytes);
			$("#contenidoReporteGeneral").html("");
			$.plot($("#contenidoReporteGeneral"), dataset, options);
		}
		$(".form-control").datepicker({
			format: 'dd/mm/yyyy'
		});
		$('#btnConsultar').click(function () {
			$("#contenidoReporteGeneral").html("Cargando...");
			$.ajax({
				url: $("#formAjax").attr('action'),
				dataType: 'json',
				data: $("#formAjax").serialize(),
				method: "POST",
				url: "{$_Parametros.url}modCR/graficaRedCONTROL/GenerarGraficaMET",
				success: mostrarGrafica
			});
		});
		$("<div id='tooltip'></div>").css({
			position: "absolute",
			display: "none",
			border: "1px solid #fdd",
			padding: "2px",
			"background-color": "#fee",
			opacity: 0.80
		}).appendTo("body");
		$("#contenidoReporteGeneral").bind("plothover", function (event, pos, item) {
			if (item) {
				var x = item.datapoint[0].toFixed(2), y = item.datapoint[1].toFixed(2);
				var tiempo = Math.floor(x);
				var d = new Date(tiempo);

				$("#tooltip").html( y + " MB")
						.css({ top: item.pageY-25, left: item.pageX+5 })
						.fadeIn(200);
			} else {
				$("#tooltip").hide();
			}
		});
	});
</script>
