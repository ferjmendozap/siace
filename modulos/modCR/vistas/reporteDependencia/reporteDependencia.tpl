<form id="formAjax" class="form floating-label" action="{$_Parametros.url}modCR/reporteDependenciaCONTROL/ReporteDependenciaMET" role="form" method="post" target="_blank">
	<br/>
	<section class="style-default-bright">
	<div class="col-lg-13">
		<div class="card style-default-bright">
			<div class="card-head"><h2 class="text-primary">&nbsp;Listado de Dependencias</h2></div>
		</div>
	</div>
	<div class="section-body contain-lg">
		<table align="center" class="table-responsive" width="60%">
			<tr>
				<td>
					<div class="section-body contain-lg">
						<div class="form-group" style="width:100%">
							<div class="input-daterange input-group" id="demo-date-range">
								<div class="input-group-content">
									<input type="text" class="form-control dirty" name="fechaInicio" id="fechaInicio">
									<label><i class="glyphicon glyphicon-calendar"></i> Rango de fecha</label>
								</div>
								<span class="input-group-addon">a</span>
								<div class="input-group-content">
									<input type="text" class="form-control dirty" name="fechaFin" id="fechaFin">
									<div class="form-control-line"></div>
								</div>
								<button type="submit" class="btn ink-reaction btn-raised btn-primary" id="btnConsultar" style="margin-top:7px">Consultar</button>
							</div>
						</div>
					</div>
				</td>
			</tr>
		</table>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>
					<input type="checkbox" name="checktodos"/>
					</th>
					<th><i class="md-business"> Dependencia</th>
				</tr>
				</thead>
				<tbody>
				{foreach item=post from=$_PruebaPost}
				<tr id="pk_num_dependencia{$post.pk_num_dependencia}">
				<td><input type="checkbox" name="dependencia[]" value="{$post.pk_num_dependencia}"/></td>
				<td>{$post.ind_dependencia}</td>
				</tr>
			{/foreach}
			</tbody>
		</table>
	</div>
</section>
</form>
<script type="text/javascript">
	$(document).ready(function(){
		//Checkbox
		$("input[name=checktodos]").change(function(){
			$('input[type=checkbox]').each( function() {
				if($("input[name=checktodos]:checked").length == 1){
					this.checked = true;
				} else {
					this.checked = false;
				}
			});
		});
		$("#formAjax").validate({
			rules:{
				fechaInicio:{
					required:true
				},
				fechaFin:{
					required:true
				},
				"dependencia[]":{
					required:true,
					minlength: 1
				}
			},
			messages:{
				fechaInicio:{
					required: "La fecha de inicio es requerida"
				},
				fechaFin:{
					required: "La fecha final es requerida"
				},
				"dependencia[]": "Debe seleccionar al menos una dependencia"
			},
			errorPlacement: function(error, element) {
				if (element.attr("name") == "dependencia[]"){
					alert(error.text());
				}
				else{
					error.insertAfter(element);
				}
			}
		});
	});

	$("#fechaInicio").datepicker({
		format: 'dd/mm/yyyy',
		language: 'es',
		autoclose: true
	});
	$("#fechaFin").datepicker({
		format: 'dd/mm/yyyy',
		language: 'es',
		autoclose: true
	});
</script>