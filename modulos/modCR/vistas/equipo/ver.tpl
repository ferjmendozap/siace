<div class="form floating-label">
	<div class="modal-body" >
		<div class="col-md-6 col-sm-6">
			{foreach item=usuario from=$usuarioEquipo}
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$usuario.ind_nombre1} {$usuario.ind_apellido1}" size="45%" disabled="disabled" disabled="disabled">
					<label for="regular2"><i class="md md-person"></i> Usuario</label>
				</div>
			{/foreach}
			{foreach item=pc from=$equipo}
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$pc.ind_dependencia}" size="45%" disabled="disabled" disabled="disabled">
					<label for="regular2"><i class="md-business"></i> Dependencia</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$pc.ind_nombre_equipo}" size="45%" disabled="disabled" disabled="disabled">
					<label for="regular2"><i class="md-desktop-windows"></i> Equipo</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$pc.ind_direccion_fisica}" size="45%" disabled="disabled" disabled="disabled">
					<label for="regular2"><i class="md-cast"></i> Dirección Mysar</label>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$pc.ind_ip}" size="45%" disabled="disabled" disabled="disabled">
					<label for="regular2"><i class="md-wifi-tethering"></i> Ip</label>
				</div>
			{/foreach}
			{foreach item=ac from=$acceso}
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$ac.fecha}" size="45%" disabled="disabled" disabled="disabled">
					<label for="regular2">Última modificación</label>
				</div>
				<div class="form-group floating-label">
					<input type="text" class="form-control" id="regular2" value="{$ac.ind_nombre1} {$ac.ind_apellido1}" size="45%" disabled="disabled" disabled="disabled">
					<label for="regular2">Último usuario</label>
				</div>
			{/foreach}
	</div>
	<div align="right">
		<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El usuario ha regresado al listado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cerrar</button>
	</div>
</div>