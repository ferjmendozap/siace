<form id="formAjax" action="{$_Parametros.url}modCR/equipoCONTROL/NuevoEquipoMET" class="form floating-label" novalidate>
  <input type="hidden" name="valido" value="1" />
  <div class="modal-body">
    <div class="form-group floating-label">
      <div class="form-group floating-label">
        <div class="input-group">
          <div class="input-group-content">
            <input type="text" class="form-control dirty" id="remitente" name="remitente">
            <input type="hidden" class="form-control" id="pk_num_empleado" name="pk_num_empleado">
            <label for="groupbutton10">Usuario</label>
          </div>
          <div class="input-group-btn">
            <button class="btn btn-default" type="button" onclick="buscarFuncionario()" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Funcionarios"><i class="glyphicon glyphicon-search"></i></button>
          </div>
        </div>
      </div><!--end .form-group -->
    </div>
    <div id="dependencia">
        <div class="form-group floating-label">
          <select id="pk_num_dependencia" name="pk_num_dependencia" id="s2id_single" class="select2-container form-control select2">
            <a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
            <option value="">&nbsp;</option>
            {foreach item=dep from=$dependencia}
              <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
            {/foreach}
          </select>
          <label for="pk_num_dependencia"><i class="md-business"></i> Dependencia</label>
        </div>
    </div>
    <div class="form-group floating-label">
      <input type="text" class="form-control"  name="ind_nombre_equipo" id="ind_nombre_equipo">
      <label for="ind_nombre_equipo"><i class="md-desktop-windows"></i> Equipo</label>
    </div>
    <div class="form-group floating-label">
      <input type="text" class="form-control" name="ind_direccion_fisica" id="ind_direccion_fisica">
      <label for="ind_direccion_fisica"><i class="md-cast"></i> Dirección Mysar</label>
    </div>
    <div class="form-group floating-label">
      <input type="text" class="form-control" name="ind_ip" id="ind_ip">
      <label for="ind_ip"><i class="md-wifi-tethering"></i> Ip</label>
    </div>
    <div align="right">
      <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
      <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
    </div>
</form>
<script type="text/javascript">
  $(document).ready(function () {
    //validation rules
    $("#formAjax").validate({
      rules:{
        pk_num_empleado:{
          required:true
        },
        pk_num_dependencia:{
          required:true
        },
        ind_nombre_equipo:{
          required:true,
          remote:{
            url:"{$_Parametros.url}modCR/equipoCONTROL/VerificarEquipoMET",
            type:"post"
          }
        },
        ind_direccion_fisica:{
          required:true,
          remote:{
            url:"{$_Parametros.url}modCR/equipoCONTROL/VerificarDireccionMysarMET",
            type:"post"
          }
        },
        ind_ip:{
          required:true,
          remote:{
            url:"{$_Parametros.url}modCR/equipoCONTROL/VerificarDireccionIpMET",
            type:"post"
          }
        }
      },
      messages:{
        pk_num_empleado:{
          required: "Este campo es requerido"
        },
        pk_num_dependencia:{
          required: "Este campo es requerido"
        },
        ind_nombre_equipo: {
          required: "Este campo es requerido",
          remote: jQuery.validator.format("Ya se encuentra registrado en el sistema")
        },
        ind_direccion_fisica:{
          required: "Este campo es requerido",
          remote: jQuery.validator.format("Ya se encuentra registrado en el sistema")
        },
        ind_ip:{
          required: "Este campo es requerido",
          remote: jQuery.validator.format("Ya se encuentra registrado en el sistema")
        }
      },
      submitHandler: function(form) {
        $.post($(form).attr('action'), $(form).serialize(),function(dato){
          $(document.getElementById('datatable1')).append('<tr id="pk_num_equipo'+dato['pk_num_equipo']+'"><td>'+dato['empleado']+'</td><td>'+dato['ind_dependencia']+'</td><td>'+dato['ind_nombre_equipo']+'</td><td>'+dato['ind_ip']+'</td>' +
                  '<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha visualizado el equipo" titulo="Visualizar equipo" title="Visualizar equipo" data-toggle="modal" data-target="#formModal" pk_num_equipo="'+dato['pk_num_equipo']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>' +
                  '<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha Modificado el equipo" titulo="Modificar Equipo" data-toggle="modal" data-target="#formModal" pk_num_equipo="'+dato['pk_num_equipo']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>' +
                  '<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado un almacén" titulo="Estas Seguro?" mensaje="¿Estas seguro que desea eliminar el equipo?" boton="si, Eliminar" pk_num_equipo="'+dato['pk_num_equipo']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td></tr>');
          swal("Registro Guardado", "Equipo guardado satisfactoriamente", "success");
          $(document.getElementById('cerrarModal')).click();
          $(document.getElementById('ContenidoModal')).html('');
        },'json');
      }
    });
  });
  var placeholder = "";

  $( ".select2, .select2-multiple" ).select2( { placeholder: placeholder } );
  $( ".select2-allow-clear" ).select2( { allowClear: true, placeholder: placeholder } );

  // @see https://github.com/ivaynberg/select2/commit/6661e3
  function repoFormatResult( repo ) {
    var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

    if ( repo.description ) {
      markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
    }

    markup += "<div class='select2-result-repository__statistics'>" +
            "<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
            "<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
            "<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
            "</div>" +
            "</div></div>";

    return markup;
  }
  function repoFormatSelection( repo ) {
    return repo.full_name;
  }

  $( "button[data-select2-open]" ).click( function() {
    $( "#" + $( this ).data( "select2-open" ) ).select2( "open" );
  });
  
  function cargarDependencia(pkNumEmpleado) {
        $("#dependencia").html("");
        $.post("{$_Parametros.url}modCR/equipoCONTROL/BuscarDependenciaMET",{ pkNumEmpleado:""+pkNumEmpleado }, function (dato) {
            $("#dependencia").html(dato);
        });
    }

  function buscarFuncionario(){
      var $urlFuncionario = '{$_Parametros.url}modCR/equipoCONTROL/ConsultarEmpleadoMET';
      $('#ContenidoModal2').html("");
      $('#formModalLabel2').html('Listado de Empleados');
      $.post($urlFuncionario,"",function($dato){
          $('#ContenidoModal2').html($dato);
      });
  }
</script>
