<form id="formAjax" class="form floating-label" action="{$_Parametros.url}modCR/reporteEquipoCONTROL/ReporteEquipoMET" role="form" method="post" target="_blank">
	<br/>
	<section class="style-default-bright">
		<br/>
			<div class="col-lg-13">
				<div class="card style-default-bright">
					<div class="card-head"><h2 class="text-primary">&nbsp;Listado de Equipos</h2></div>
				</div>
				</div>
					<div class="section-body contain-lg">
						<table align="center" class="table-responsive" width="100%">
							<tr>
								<td>
									<table align="center" class="table-responsive" width="80" align="left">
										<tr>
											<td>
												<div class="form-group floating-label">
													<select id="select2" name="numSitios" class="form-control dirty">
														<option value="">&nbsp;</option>
														<option value="10">10</option>
														<option value="15">15</option>
														<option value="20">20</option>
													</select>
													<label for="select2">N° Sitios</label>
												</div>
											</td>
										</tr>
									</table>
								</td>
								<td>
									<table align="center" class="table-responsive" width="60%">
										<tr>
											<td>
												<div class="section-body contain-lg">
													<div class="form-group" style="width:100%">
														<div class="input-daterange input-group" id="demo-date-range">
															<div class="input-group-content">
																<input type="text" class="form-control dirty" name="fechaInicio" id="fechaInicio">
																<label><i class="glyphicon glyphicon-calendar"></i> Rango de fecha</label>
															</div>
															<span class="input-group-addon">a</span>
																<div class="input-group-content">
																	<input type="text" class="form-control dirty" name="fechaFin" id="fechaFin">
																	<div class="form-control-line"></div>
																</div>
															<button type="submit" class="btn ink-reaction btn-raised btn-primary" id="btnConsultar" style="margin-top:7px">Consultar</button>
														</div>
													</div>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
							<table  class="table table-striped table-hover">
								<thead>
									<tr>
										<th><input type="checkbox" name="checktodos"/></th>
										<th><i class="md md-person"></i> Usuario</th>
										<th><i class="md-business"> Dependencia</th>
										<th><i class="md-desktop-windows"> Equipo</th>
										<th><i class="md-wifi-tethering"></i> Ip</th>
									</tr>
								</thead>
								<tbody>
									{foreach item=post from=$_PruebaPost}
									<tr id="pk_num_equipo{$post.pk_num_equipo}">
										<td><input type="checkbox" name="equipo[]" value="{$post.pk_num_equipo}"/></td>
										<td>{$post.ind_nombre1} {$post.ind_apellido1}</td>
										<td>{$post.ind_dependencia}</td>
										<td>{$post.ind_nombre_equipo}</td>
										<td>{$post.ind_ip}</td>
									</tr>
									{/foreach}
								</tbody>
							</table>
						</div>
					</div>
				</div>
			<div id="contenidoReporteGeneral">
            	<iframe id="miIframe" name="miIframe" width="100%" height="600" scrolling="no" border="0" frameborder="0"></iframe>
            </div>
	</section>
</form>
<script type="text/javascript">
	$(document).ready(function(){
	//Checkbox
		$("input[name=checktodos]").change(function(){
			$('input[type=checkbox]').each( function() {
				if($("input[name=checktodos]:checked").length == 1){
					this.checked = true;
				} else {
					this.checked = false;
				}
			});
		});
		$("#formAjax").validate({
			rules:{
				fechaInicio:{
					required:true
				},
				fechaFin:{
					required:true
				},
				"equipo[]":{
					required:true,
					minlength: 1
				}
			},
			messages:{
				fechaInicio:{
					required: "La fecha de inicio es requerida"
				},
				fechaFin:{
					required: "La fecha final es requerida"
				},
				"equipo[]": "Debe seleccionar al menos un equipo"
			},
			errorPlacement: function(error, element) {
				if (element.attr("name") == "equipo[]"){
					alert(error.text());
				}
				else{
					error.insertAfter(element);
				}
			}
		});
	});

	$("#fechaInicio").datepicker({
		format: 'dd/mm/yyyy',
		language: 'es',
		autoclose: true
	});
	$("#fechaFin").datepicker({
		format: 'dd/mm/yyyy',
		language: 'es',
		autoclose: true
	});
</script>
