<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO SUCRE.
 * MODULO: Consumo de Red
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.sistemas.control@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de equipos
class equipoModelo extends Modelo
{
    // Constructor del modelo
    public function __construct()
    {
        parent::__construct();
    }

    // Método para obtener todos los equipos registrados en la base de datos
    public function metListarEquipos($usuario, $idAplicacion)
    {
        #Se ejecuta la consulta a la base de datos que se encarga de listar los equipos registrados
        $pruebaPost =  $this->_db->query(
            "SELECT a.ind_nombre_equipo, a.ind_direccion_fisica, a.ind_ip, c.ind_dependencia, e.ind_nombre1, e.ind_apellido1,a.pk_num_equipo
		FROM cr_b001_equipo as a LEFT JOIN cr_c002_usuario_equipo as b ON a.pk_num_equipo=b.pk_num_equipo LEFT JOIN rh_b001_empleado as d
		ON b.fk_rhb001_num_empleado=d.pk_num_empleado LEFT JOIN a003_persona as e ON d.fk_a003_num_persona=e.pk_num_persona INNER JOIN a004_dependencia as c
		ON a.fk_a004_num_dependencia=c.pk_num_dependencia LEFT JOIN a019_seguridad_dependencia as f ON c.pk_num_dependencia=f.fk_a004_num_dependencia WHERE f.fk_a018_num_seguridad_usuario=$usuario and f.fk_a015_num_seguridad_aplicacion=$idAplicacion order by c.pk_num_dependencia"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    // Método para obtener los datos de un equipo asociado a una dependencia
    public function metListaEquipos($pkNumDependencia, $usuario, $idAplicacion)
    {
        #Se ejecuta la consulta a la base de datos que se encarga de listar los equipos registrados
        $pruebaPost =  $this->_db->query(
            "SELECT a.ind_nombre_equipo, a.ind_direccion_fisica, a.ind_ip, c.ind_dependencia, e.ind_nombre1, e.ind_apellido1,a.pk_num_equipo
	     FROM cr_b001_equipo as a LEFT JOIN cr_c002_usuario_equipo as b ON a.pk_num_equipo=b.pk_num_equipo LEFT JOIN rh_b001_empleado as d
	     ON b.fk_rhb001_num_empleado=d.pk_num_empleado LEFT JOIN a003_persona as e ON d.fk_a003_num_persona=e.pk_num_persona INNER JOIN a004_dependencia as c
	     ON a.fk_a004_num_dependencia=c.pk_num_dependencia and c.pk_num_dependencia='$pkNumDependencia' LEFT JOIN a019_seguridad_dependencia as f ON c.pk_num_dependencia=f.fk_a004_num_dependencia WHERE f.fk_a018_num_seguridad_usuario=$usuario and f.fk_a015_num_seguridad_aplicacion=$idAplicacion"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    // Método para consultar el equipo que se ha seleccionado
    public function metVerEquipo($pkNumEquipo){
        #ejecuto la consulta a la base de datos
        $gestionEquipo =  $this->_db->query(
            "SELECT d.fk_a003_num_persona,a.ind_dependencia, a.txt_abreviacion,b.ind_nombre_equipo,b.ind_direccion_fisica,b.ind_ip,b.pk_num_equipo,b.fk_a004_num_dependencia,
        e.ind_nombre1, e.ind_apellido1 FROM a004_dependencia as a, cr_b001_equipo as b,  cr_c002_usuario_equipo as c, rh_b001_empleado as d, a003_persona as e
        WHERE a.pk_num_dependencia = b.fk_a004_num_dependencia AND b.pk_num_equipo = '$pkNumEquipo' AND b.pk_num_equipo=c.pk_num_equipo AND c.fk_rhb001_num_empleado=d.pk_num_empleado
        AND d.fk_a003_num_persona=e.pk_num_persona
       "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $gestionEquipo->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetch — Devuelve un unico registro seleccionado
        return $gestionEquipo->fetch();
    }

    // Método para guardar el registro del equipo
    public function metGuardarEquipo($pkNumEmpleado, $pkNumDependencia, $indNombreEquipo, $indDireccionFisica, $indIp, $fechaHora , $usuario){
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $estado = 1;
        // preparo la sentencia
        $nuevoEquipo=$this->_db->prepare(
            "insert into cr_b001_equipo values (null, :fk_a004_num_dependencia, :ind_nombre_equipo, :ind_direccion_fisica, :ind_ip, :num_estado, :fec_fecha_modificacion, :fk_a018_num_seguridad_usuario)"
        );
        #execute — Ejecuta una sentencia preparada
        $nuevoEquipo->execute(array(
            ':fk_a004_num_dependencia' => $pkNumDependencia,
            ':ind_nombre_equipo' => $indNombreEquipo,
            ':ind_direccion_fisica' => $indDireccionFisica,
            ':ind_ip' => $indIp,
            ':num_estado'=> $estado,
            ':fec_fecha_modificacion' => $fechaHora,
            ':fk_a018_num_seguridad_usuario'=> $usuario
        ));
        // Obtengo el id de la base de datos donde se acaba de guardar el equipo
        $pkNumEquipo= $this->_db->lastInsertId();
        // Preparo la sentencia para guardar el usuario del equipo
        $nuevoUsuario=$this->_db->prepare(
            "insert into cr_c002_usuario_equipo values (:pk_num_equipo, :fk_rhb001_num_empleado)"
        );
        // Se ejecuta la sentencia
        $nuevoUsuario->execute(array(
            ':pk_num_equipo' => $pkNumEquipo,
            ':fk_rhb001_num_empleado' => $pkNumEmpleado
        ));
        #commit — Consigna una transacción
        $this->_db->commit();
        // retorna el identificador del equipo
        return $pkNumEquipo;
    }

    // Método para editar equipos
    public function metEditarEquipo($pkNumEmpleado, $pkNumDependencia, $indNombreEquipo, $indDireccionFisica, $indIp, $fechaHora, $usuario, $pkNumEquipo)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "update cr_b001_equipo set fk_a004_num_dependencia='$pkNumDependencia', ind_nombre_equipo='$indNombreEquipo',  ind_direccion_fisica='$indDireccionFisica', ind_ip='$indIp', fec_fecha_modificacion='$fechaHora', fk_a018_num_seguridad_usuario='$usuario' where pk_num_equipo='$pkNumEquipo'"
        );
        $this->_db->query(
            "update cr_c002_usuario_equipo set fk_rhb001_num_empleado='$pkNumEmpleado' where pk_num_equipo='$pkNumEquipo'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }

    public function metInsertarEditar($pkNumPersona, $pkNumDependencia, $indNombreEquipo, $indDireccionFisica, $indIp, $fechaHora, $usuario, $pkNumEquipo)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "update cr_b001_equipo set fk_a004_num_dependencia='$pkNumDependencia', ind_nombre_equipo='$indNombreEquipo',
             ind_direccion_fisica='$indDireccionFisica', ind_ip='$indIp', fec_fecha_modificacion='$fechaHora', fk_a018_num_seguridad_usuario='$usuario' where pk_num_equipo='$pkNumEquipo'"
        );
        $this->_db->query(
            "insert into cr_c002_usuario_equipo (pk_num_equipo, fk_rhb001_num_empleado) values ($pkNumEquipo, $pkNumPersona)"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }

    // Elimina un usuario asignado a un equipo
    public function metEliminarEditar($pkNumEquipo)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from  cr_c002_usuario_equipo " .
            "where pk_num_equipo = '$pkNumEquipo'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }
    // Método para eliminar equipos
    public function metEliminarEquipo($pkNumEquipo)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "delete from  cr_c002_usuario_equipo where pk_num_equipo = '$pkNumEquipo'"
        );
        $this->_db->query(
            "delete from cr_b001_equipo where pk_num_equipo = '$pkNumEquipo'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }

    // Método  para listar las dependencias de la Contraloría
    public function metConsultarSeguridadAlterna($usuario, $idAplicacion)
    {
        $seguridadAlterna = $this->_db->query(
            "select b.pk_num_dependencia, b.ind_dependencia from a019_seguridad_dependencia as a, a004_dependencia as b where a.fk_a004_num_dependencia=b.pk_num_dependencia and a.fk_a018_num_seguridad_usuario=$usuario and a.fk_a015_num_seguridad_aplicacion=$idAplicacion"
        );
        $seguridadAlterna->setFetchMode(PDO::FETCH_ASSOC);
        return $seguridadAlterna->fetchAll();
    }

    // Consulto las dependencias de la contraloría y se las asigno a la variable $dependencia
    public function metEditarDependencia($pkNumEquipo){
        //Consulto la dependencia a la cual pertenece el equipo
        $dependencia = $this->_db->query(
            "SELECT b.fk_a004_num_dependencia, a.ind_dependencia from a004_dependencia as a, cr_b001_equipo as b WHERE
             b.pk_num_equipo='$pkNumEquipo' and b.fk_a004_num_dependencia=a.pk_num_dependencia"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $dependencia->fetchAll();
    }

    // Selecciono el nombre de la dependencia para mostrarla en el listado de equipos
    public function metDependenciaEquipo($pkNumDependencia){
        // Selecciono la dependencia asocida al equipo
        $dependencia = $this->_db->query(
            "SELECT ind_dependencia FROM a004_dependencia where pk_num_dependencia = '$pkNumDependencia'"
        );
        // Asigno el nombre de la dependencia a la variable nombre
        $nombre = $dependencia->fetch();
        //retorno el valor de la posicion 0 de la consulta
        return $nombre[0];
    }

    // Permite consultar el usuario al que está asociado el equipo para ser mostrado en el listado
    public function metUsuarioEquipo($pkNumEmpleado){
        // Selecciono el usuario asociado al equipo
        $usuario = $this->_db->query(
            "SELECT b.ind_nombre1, b.ind_apellido1, a.fk_a003_num_persona from rh_b001_empleado as a, a003_persona as b
             where a.fk_a003_num_persona=b.pk_num_persona and a.pk_num_empleado='$pkNumEmpleado'"
        );
        // Asigno el nombre del usuario a la variable nombre_usuario
        $nombre_usuario = $usuario->fetch();
        //retorno el valor de la posicion 0 de la consulta
        return $nombre_usuario[0].' '.$nombre_usuario[1];
    }

    // Muestra el listado de los empleados de la contraloría
    public function metListarUsuario(){
        $usuario = $this->_db->query(
            "SELECT a.pk_num_empleado, b.ind_nombre1, b.ind_apellido1 from rh_b001_empleado as a, a003_persona as b
             where a.fk_a003_num_persona=b.pk_num_persona order by b.ind_nombre1 asc");
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $usuario->fetchAll();
    }

    // Método que selecciona el usuario del equipo para ser mostrado en la modal de editar
    public function metEditarUsuario($pkNumEquipo){
        //Consulta el usuario de un equipo en específico
        $usuario = $this->_db->query(
            "SELECT d.ind_nombre1, d.ind_apellido1, c.fk_a003_num_persona, d.pk_num_persona FROM cr_b001_equipo as a, cr_c002_usuario_equipo as b,
             rh_b001_empleado as c, a003_persona as d where a.pk_num_equipo='$pkNumEquipo' and a.pk_num_equipo=b.pk_num_equipo and
             c.fk_a003_num_persona=d.pk_num_persona"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $usuario->fetchAll();
    }

    // Método que permite consultar la existencia de un equipo en el sistema
    public function metBuscarEquipo($indNombreEquipo, $pkNumEquipo)
    {
        if($pkNumEquipo == false) {	//Consulta el nombre de un equipo en específico
            $equipo = $this-> _db ->query("SELECT ind_nombre_equipo FROM cr_b001_equipo WHERE ind_nombre_equipo = '$indNombreEquipo'");
            $equipo->setFetchMode(PDO::FETCH_ASSOC);
            $arreglo = $equipo->fetchAll();
            if(count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        } else {	//Consulta el nombre de un equipo en específico respecto a un pk_num_equipo
            $equipo = $this-> _db ->query(
                "SELECT ind_nombre_equipo FROM cr_b001_equipo WHERE ind_nombre_equipo = '$indNombreEquipo' AND pk_num_equipo <> '$pkNumEquipo'"
            );
            $equipo->setFetchMode(PDO::FETCH_ASSOC);
            $arreglo = $equipo->fetchAll();
            if(count($arreglo) > 0) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    // Método que permite consultar la existencia de una dirección mysar dentro del sistema
    public function metBuscarDireccionMysar($direccionMysar, $pkNumEquipo)
    {
        if($pkNumEquipo == false){
            //Consulta la direccion mysar
            $equipo = $this-> _db ->query("SELECT ind_direccion_fisica FROM cr_b001_equipo WHERE ind_direccion_fisica = '$direccionMysar'");
            $equipo->setFetchMode(PDO::FETCH_ASSOC);
            $arreglo = $equipo->fetchAll();
            if(count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            //Consulta la direccion mysar
            $equipo = $this-> _db ->query("SELECT ind_direccion_fisica FROM cr_b001_equipo WHERE ind_direccion_fisica = '$direccionMysar' AND pk_num_equipo <> '$pkNumEquipo'");
            $equipo->setFetchMode(PDO::FETCH_ASSOC);
            $arreglo = $equipo->fetchAll();
            if(count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    // Método que permite consultar la existencia de una dirección ip dentro del sistema
    public function metBuscarDireccionIp($direccionIp,$pkNumEquipo)
    {
        if($pkNumEquipo == false) {
            //Consulta la direccion mysar
            $equipo = $this-> _db ->query("SELECT ind_ip FROM cr_b001_equipo WHERE ind_ip = '$direccionIp'");
            $equipo->setFetchMode(PDO::FETCH_ASSOC);
            $arreglo = $equipo->fetchAll();
            if(count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            //Consulta la direccion mysar
            $equipo = $this-> _db ->query("SELECT ind_ip FROM cr_b001_equipo WHERE ind_ip = '$direccionIp' AND pk_num_equipo <> '$pkNumEquipo'");
            $equipo->setFetchMode(PDO::FETCH_ASSOC);
            $arreglo = $equipo->fetchAll();
            if(count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    // Método que permite obtener el usuario que generó el reporte
    public function metUsuarioReporte($usuario)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_apellido1 from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c
         where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=
         c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerUsuario->fetch();
    }

    public function metVisualizarEquipo($pkNumEquipo){
        #ejecuto la consulta a la base de datos
        $gestionEquipo =  $this->_db->query(
            "SELECT a.ind_dependencia, a.txt_abreviacion,b.ind_nombre_equipo,b.ind_direccion_fisica,b.ind_ip,b.pk_num_equipo,b.fk_a004_num_dependencia,date_format(b.fec_fecha_modificacion,'%d/%m/%Y %H:%m:%S') as fecha FROM a004_dependencia as a, cr_b001_equipo as b WHERE a.pk_num_dependencia = b.fk_a004_num_dependencia AND b.pk_num_equipo = '$pkNumEquipo'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $gestionEquipo->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetch — Devuelve un unico registro seleccionado
        return $gestionEquipo->fetchAll();
    }
    public function metMostrarEquipo($pkNumEquipo){
        #ejecuto la consulta a la base de datos
        $gestionEquipo =  $this->_db->query(
            "SELECT a.ind_dependencia, a.txt_abreviacion,b.ind_nombre_equipo,b.ind_direccion_fisica,b.ind_ip,b.pk_num_equipo,b.fk_a004_num_dependencia,date_format(b.fec_fecha_modificacion,'%d/%m/%Y %H:%m:%S') as fecha FROM a004_dependencia as a, cr_b001_equipo as b WHERE a.pk_num_dependencia = b.fk_a004_num_dependencia AND b.pk_num_equipo = '$pkNumEquipo'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $gestionEquipo->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetch — Devuelve un unico registro seleccionado
        return $gestionEquipo->fetch();
    }
    public function metUsuarioVer($pkNumEquipo){
        #ejecuto la consulta a la base de datos
        $usuarioVer =  $this->_db->query(
            "select b.fk_a003_num_persona, c.ind_nombre1, c.ind_apellido1 from cr_c002_usuario_equipo as a, rh_b001_empleado as b, a003_persona as c where a.fk_rhb001_num_empleado=b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona and a.pk_num_equipo='$pkNumEquipo'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $usuarioVer->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetch — Devuelve un unico registro seleccionado
        return $usuarioVer->fetchAll();
    }
    public function metVerPersona($pk_num_equipo){
        #ejecuto la consulta a la base de datos
        $usuarioVer =  $this->_db->query(
            "select b.pk_num_empleado, c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2 from cr_c002_usuario_equipo as a, rh_b001_empleado as b, a003_persona as c where a.fk_rhb001_num_empleado=b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona and a.pk_num_equipo='$pk_num_equipo'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $usuarioVer->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetch — Devuelve un unico registro seleccionado
        return $usuarioVer->fetch();
    }

    public function metVerAcceso($pkNumEquipo){
        $verAcceso =  $this->_db->query(
            "SELECT date_format(a.fec_fecha_modificacion,'%d/%m/%Y %H:%m:%S') as fecha, d.ind_nombre1, d.ind_apellido1 from cr_b001_equipo as a,a018_seguridad_usuario as b, rh_b001_empleado as c, a003_persona as d where a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona and a.pk_num_equipo='$pkNumEquipo'"
        );
        $verAcceso->setFetchMode(PDO::FETCH_ASSOC);
        return  $verAcceso->fetchAll();
    }

    public function metUsuario($pk_num_equipo){
        $usuarioVer =  $this->_db->query(
            "select * from cr_c002_usuario_equipo where pk_num_equipo='$pk_num_equipo'"
        );
        $usuarioVer->setFetchMode(PDO::FETCH_ASSOC);
        return $usuarioVer->fetch();
    }
	
	// Método que permite buscar la dependencia a la cual pertenece el empleado
	public function metDependenciaUsuario($pkNumEmpleado)
	{
		$dependenciaUsuario =  $this->_db->query(
            "select fk_a004_num_dependencia from vl_rh_persona_empleado_datos_laborales where pk_num_empleado=$pkNumEmpleado"
        );
        $dependenciaUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $dependenciaUsuario->fetch();
	}

    public function metListarEmpleado()
    {
        $funcionario =  $this->_db->query(
            "select a.pk_num_empleado, a.ind_cedula_documento, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, c.ind_dependencia from vl_rh_persona_empleado as a, vl_rh_persona_empleado_datos_laborales as b, a004_dependencia as c where a.ind_estatus_empleado=1 and a.pk_num_empleado=b.pk_num_empleado and b.fk_a004_num_dependencia=c.pk_num_dependencia"
        );
        $funcionario->setFetchMode(PDO::FETCH_ASSOC);
        return $funcionario->fetchAll();
    }

    // Metodo que permite obtener el nombre de un empleado
    public function metConsultarEmpleado($pkNumEmpleado)
    {
        $empleado = $this->_db->query(
            "select ind_nombre1, ind_nombre2, ind_apellido1, ind_apellido2 from vl_rh_persona_empleado where pk_num_empleado=$pkNumEmpleado"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();
    }
}// fin de la clase
?>
