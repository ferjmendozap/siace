<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO SUCRE.
 * MODULO: Consumo de Red
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.sistemas.control@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Clase que contiene las consultas para generar el reporte general de consumo de red
class reporteGeneralModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
    }

    // Método encargado de listar las dependencias por piso
	public function metPiso()
    {
         // Consulto las dependencias de la contraloría y se las asigno a la variable $dependencia
        $piso = $this->_db->query("SELECT * FROM a004_dependencia group by num_piso order by num_piso desc");
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $piso->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $piso->fetchAll();
    }

    // Método que consulta las dependencias ubicadas en un piso en específico
    public function metDependenciaPiso($numPisoPrevio)
    {
        // Consulto las dependencias de la contraloría y se las asigno a la variable $dependencia
        $dependencia = $this->_db->query(
            "SELECT pk_num_dependencia, ind_dependencia FROM a004_dependencia where num_piso='$numPisoPrevio'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $dependencia->fetchAll();
    }

    // Método que consulta los equipos adscritos a una dependencia en específica
    public function metDependenciaEquipos($dependenciaId)
    {
        // Consulto las dependencias de la contraloría y se las asigno a la variable $dependencia
        $dependenciaEquipos = $this->_db->query(
            "SELECT a.ind_nombre_equipo, a.ind_direccion_fisica, a.ind_ip, c.ind_dependencia, e.ind_nombre1, e.ind_apellido1,a.pk_num_equipo
		     FROM cr_b001_equipo as a LEFT JOIN cr_c002_usuario_equipo as b ON a.pk_num_equipo=b.pk_num_equipo LEFT JOIN rh_b001_empleado as d
		     ON b.fk_rhb001_num_empleado=d.pk_num_empleado LEFT JOIN a003_persona as e ON d.fk_a003_num_persona=e.pk_num_persona INNER JOIN a004_dependencia as c
		     ON a.fk_a004_num_dependencia=c.pk_num_dependencia and a.fk_a004_num_dependencia='$dependenciaId'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $dependenciaEquipos->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $dependenciaEquipos->fetchAll();
    }

    // Método que consulta el consumo de red en el rango de fechas dado
    public function metConsumoRed($fechaInicio, $fechaFin)
    {
		$total = $this->_db2->query(
            "select sum(bytes) as total from traffic where date between '$fechaInicio' and '$fechaFin'"
		);
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $total->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return  $total->fetch();
    }

    // Método encargado de consultar el consumo de un equipo en un rango de fecha dado
    public function metConsumoEquipo($fechaInicio, $fechaFin, $direccionMysar)
    {
        $consumoRed = $this->_db2->query("select sum(bytes) as consumo from traffic where ip='$direccionMysar' and date between '$fechaInicio' and '$fechaFin'");
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $consumoRed->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $consumoRed->fetchAll();
    }

    // Método encargado de consultar los sitios más visitados por un equipo en una fecha dada
    public function metSitiosVisitados($fechaInicio, $fechaFin, $direccionMysar, $numSitios)
    {
       //Esta consulta muestra los sitios mas visitados.
	  /* $sitiosVisitados = $this->_db2->query("
            SELECT b.site, count(a.sitesID) as total, a.sitesID
            FROM traffic as a, sites as b
            where a.ip='$direccionMysar' and a.SitesID=b.id and a.date between '$fechaInicio' and '$fechaFin'
            GROUP BY a.sitesID
            HAVING count(*) > 1
            order by total desc LIMIT 0,$numSitios"
        );*/
	
		// Esta consulta muestra los sitios que mas consumieron recursos de red
		$sitiosVisitados = $this->_db2->query("
            SELECT b.site, sum(a.bytes) as total,  a.sitesID
			FROM traffic as a, sites as b
			where a.ip='$direccionMysar' and a.date>='$fechaInicio'and a.date<='$fechaFin' and a.SitesID=b.id
			GROUP BY a.sitesID
			order by total desc LIMIT 0,$numSitios"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $sitiosVisitados->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $sitiosVisitados->fetchAll();
    }

    // Método encargado de identificar el usuario que genera el reprorte
    public function metUsuarioReporte($usuario)
    {
        $obtenerUsuario = $this->_db->query("
        select c.ind_nombre1, c.ind_apellido1 from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c
        where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=
        c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerUsuario->fetch();
    }

    // Método que guarda los datos generales del reporte
    public function metGuardarReporte($fechaInicio, $fechaFin, $fechaReporte, $usuario)
    {
        $this->_db->beginTransaction();
        $guardar=$this->_db->prepare(
            "insert into cr_c006_reporte values (null, :fec_fecha_inicio, :fec_fecha_fin, :fec_fecha_reporte, :fk_a018_num_seguridad_usuario)"
        );
        $guardar->execute(array(
            ':fec_fecha_inicio' => $fechaInicio,
            ':fec_fecha_fin'=> $fechaFin,
            ':fec_fecha_reporte' => $fechaReporte,
            ':fk_a018_num_seguridad_usuario' => $usuario
        ));
        $pkNumReporte= $this->_db->lastInsertId();
        $this->_db->commit();
        return $pkNumReporte;
    }

    // Método que guarda el consumo de red de un equipo asociado a un reporte
    public function metGuardarDescarga($guardarReporte, $pkNumEquipo, $redEquipo)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        // preparo la sentencia
        $guardarDescarga=$this->_db->prepare(
            "insert into cr_c003_descarga_equipo values (null, :fk_crb001_num_equipo, :fk_crc006_num_reporte, :num_descarga_total)"
        );
        #execute — Ejecuta una sentencia preparada
        $guardarDescarga->execute(array(
            ':fk_crc006_num_reporte' => $guardarReporte,
            ':fk_crb001_num_equipo' => $pkNumEquipo,
            ':num_descarga_total' => $redEquipo
        ));
        // Obtengo el id de la base de datos donde se acaba de guardar el equipo
        $pkNumDescarga= $this->_db->lastInsertId();
        // Preparo la sentencia para guardar el usuario del equipo
        #commit — Consigna una transacción
        $this->_db->commit();
        // retorna el identificador del equipo
        return $pkNumDescarga;
    }

    // Método encargado de consultar un sitio en específico
    public function metConsultarSitios($sitio)
    {
        $consultarSitio= $this->_db->query("select pk_num_sitio from cr_c005_sitio where ind_nombre_sitio='$sitio'");
        $consultarSitio->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetch — Devuelve un unico registro seleccionado
        return $consultarSitio->fetch();
    }

    // Método que permite guardar un sitio
    public function metGuardarSitios($sitio)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        // preparo la sentencia
        $guardarSitio=$this->_db->prepare(
            "insert into cr_c005_sitio values (null, :ind_nombre_sitio)"
        );
        #execute — Ejecuta una sentencia preparada
        $guardarSitio->execute(array(
            ':ind_nombre_sitio' => $sitio
        ));
        // Obtengo el id de la base de datos donde se acaba de guardar el equipo
        $this->_db->commit();
        $sitioNuevo = $this->_db->lastInsertId();
        return $sitioNuevo;
    }

    // Método que permite asociar la descarga de un sitio a la descarga de un equipo
    public function metDescargaSitios($descargaReporte, $pkNumSitio)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        // preparo la sentencia
        $descargaGuardar=$this->_db->prepare(
            "insert into cr_c004_descarga_sitio values (null, :fk_crc003_num_descarga, :fk_crc005_num_sitio)"
        );
        #execute — Ejecuta una sentencia preparada
        $descargaGuardar->execute(array(
            ':fk_crc003_num_descarga' => $descargaReporte,
            ':fk_crc005_num_sitio'=> $pkNumSitio
        ));
        // Obtengo el id de la base de datos donde se acaba de guardar el equipo
        $this->_db->commit();
    }
	
	// Método que permite verificar si un piso tiene equipos de trabajo
	public function metVerificarPiso($numPiso)
	{
		$consultarPiso = $this->_db->query(
		 	"select pk_num_dependencia from a004_dependencia as a, cr_b001_equipo as b where a.pk_num_dependencia=b.fk_a004_num_dependencia and a.num_piso=$numPiso"
		);
        $consultarPiso->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetch — Devuelve un unico registro seleccionado
        return $consultarPiso->fetchAll();
	}
	
}
