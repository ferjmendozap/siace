<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO SUCRE.
 * MODULO: Consumo de Red
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.sistemas.control@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Clase que permite visualizar la gráfica de consumo de red
class graficaRedModelo extends Modelo
{
    // Constructor del modelo
    public function __construct()
    {
        parent::__construct();
    }

    // Método que permite obtener el consumo de red
    public function metGenerarGrafica($fechaCalculo)
    {
        $consultarRed = $this->_db2->query(
            "select sum(bytes) as totalDia from traffic where date='$fechaCalculo'"
        );
        $consultarRed->setFetchMode(PDO::FETCH_ASSOC);
        return $consultarRed->fetch();
    }
}// fin de la clase
?>