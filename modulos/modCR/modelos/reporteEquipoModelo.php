<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO SUCRE.
 * MODULO: Consumo de Red
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.sistemas.control@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Clase encargada de generar el reporte de consumo de red por estaciones de trabajo
class reporteEquipoModelo extends reporteGeneralModelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método utilizado para mostrar un determinado equipo
    public function metMostrarEquipo($pkNumEquipo)
    {
        #ejecuto la consulta a la base de datos
        $gestionEquipo =  $this->_db->query("SELECT d.fk_a003_num_persona,a.ind_dependencia, a.txt_abreviacion,b.ind_nombre_equipo,b.ind_direccion_fisica,b.ind_ip,b.pk_num_equipo,b.fk_a004_num_dependencia, e.ind_nombre1, e.ind_apellido1
        FROM a004_dependencia as a, cr_b001_equipo as b,  cr_c002_usuario_equipo as c, rh_b001_empleado as d, a003_persona as e WHERE a.pk_num_dependencia = b.fk_a004_num_dependencia AND b.pk_num_equipo = '$pkNumEquipo' AND b.pk_num_equipo=c.pk_num_equipo AND c.fk_rhb001_num_empleado=d.pk_num_empleado AND d.fk_a003_num_persona=e.pk_num_persona"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $gestionEquipo->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetch — Devuelve un unico registro seleccionado
        return $gestionEquipo->fetch();
    }

    // Método pata obtener el listado general de equipos
    public function metListadoEquipos()
    {
        #Se ejecuta la consulta a la base de datos que se encarga de listar los equipos registrados
        $pruebaPost =  $this->_db->query(
            "SELECT a.ind_nombre_equipo, a.ind_direccion_fisica, a.ind_ip, c.ind_dependencia, e.ind_nombre1, e.ind_apellido1,a.pk_num_equipo
		FROM cr_b001_equipo as a LEFT JOIN cr_c002_usuario_equipo as b ON a.pk_num_equipo=b.pk_num_equipo LEFT JOIN rh_b001_empleado as d
		ON b.fk_rhb001_num_empleado=d.pk_num_empleado LEFT JOIN a003_persona as e ON d.fk_a003_num_persona=e.pk_num_persona INNER JOIN a004_dependencia as c
		ON a.fk_a004_num_dependencia=c.pk_num_dependencia order by c.pk_num_dependencia"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }
}
?>