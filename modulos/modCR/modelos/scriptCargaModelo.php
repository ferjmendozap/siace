<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Consumo de Red. Menú
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Menu
class scriptCargaModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
    }


    public function metBusquedaSimple($tabla, $innerJoin, $where)
    {
        $busquedaSimple = $this->_db->query("
            SELECT
              *
            FROM
              $tabla
              $innerJoin
            WHERE
              $where
        ");
        $busquedaSimple->setFetchMode(PDO::FETCH_ASSOC);
        return $busquedaSimple->fetch();
    }

    #MENU
    public function metCargarMenu($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a027_seguridad_menu
              SET
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario, ind_descripcion=:ind_descripcion,
                fec_ultima_modificacion=NOW(), ind_estatus=1, cod_interno=:cod_interno, cod_padre=:cod_padre,
                fk_a015_num_seguridad_aplicacion=:fk_a015_num_seguridad_aplicacion, ind_icono=:ind_icono, ind_nombre=:ind_nombre,
                ind_rol=:ind_rol, ind_ruta=:ind_ruta, num_nivel=:num_nivel
            ");
        $registroMenuPerfil = $this->_db->prepare(
            "INSERT INTO
                a028_seguridad_menupermiso
              SET
                fk_a017_num_seguridad_perfil=1, fk_a027_num_seguridad_menu=:fk_a027_num_seguridad_menu
            ");
        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a027_seguridad_menu', false, "cod_interno = '" . $array['cod_interno'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':num_nivel' => $array['num_nivel'],
                    ':ind_ruta' => $array['ind_ruta'],
                    ':ind_rol' => $array['ind_rol'],
                    ':ind_nombre' => mb_strtoupper($array['ind_nombre'], 'utf-8'),
                    ':ind_icono' => $array['ind_icono'],
                    ':fk_a015_num_seguridad_aplicacion' => $array['fk_a015_num_seguridad_aplicacion'],
                    ':cod_padre' => mb_strtoupper($array['cod_padre'], 'utf-8'),
                    ':cod_interno' => mb_strtoupper($array['cod_interno'], 'utf-8'),
                    ':ind_descripcion' => mb_strtoupper($array['ind_descripcion'], 'utf-8'),
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                ));
                $idMenu = $this->_db->lastInsertId();
                $registroMenuPerfil->execute(array(
                    ':fk_a027_num_seguridad_menu' => $idMenu
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

}
