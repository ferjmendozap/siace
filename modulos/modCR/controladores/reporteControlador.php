<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO SUCRE.
 * MODULO: Consumo de Red
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.sistemas.control@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
//  Esta clase se encarga de gestionar el reporte de consumo de red guardado en el sistema
class reporteControlador extends Controlador
{
    private $atReporte;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.s
        $this->atReporte = $this->metCargarModelo('reporteGeneral');
        $this->atReporte = $this->metCargarModelo('reporte');
    }

    #Metodo Index del controlador Prueba.
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $datepickerjs[] = 'bootstrap-datepicker/bootstrap-datepicker';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($datepickerjs);
        $this->atVista->metCargarJs($js);
        $validarjs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $this->atVista->metCargarJsComplemento($validarjs);
        $this->atVista->assign('_PruebaPost', $this->atReporte->metMostrarReporte());
        $this->atVista->metRenderizar('listadoReporte');
    }

    // Método que permite eliminar el reporte guardado en el sistema, recibiendo de la vista el id del reporte y enviándolo al modelo para su eliminación.
    public function metEliminarReporte()
    {
        // La Funcion metObtenerInt valida que los datos enviados por post hacia el controlador sean solo Alpha-Numerico
        $pkNumReporte = $_POST['pk_num_reporte'];
        // Se llama al metodo metEliminarEquipo del modelo equipoModelo y se le envía el identificador del equipo a eliminar
        $this->atReporte->metEliminarReporte($pkNumReporte);
        // Se crea un array con el estatus de la operación y el identificador del equipo
        $arrayPost = array(
            'status' => 'OK',
            'pk_num_reporte' => $pkNumReporte
        );
        // Se retorna el array en forma de JSON al listado de equipos en la vista listado.tpl
        echo json_encode($arrayPost);
    }

    // Método que permite realizar la transformación de consumo de red en kb, Mb, Gb y Tb.
    public function metTransformar($valor)
    {
        $datos = array('total_consumo' => 0,'medida' => 0);
        $bytes_total = $valor;
        if($bytes_total >= 1024) {
            $kb=$bytes_total/1024; // regla de tres (La multiplicacion por 1 no se efectua)
            $total_consumo = $kb;
            $kb_prueba = explode(".",$kb);
            $kb_definitivo = $kb_prueba[0];
            $medida = 'Kb';
            if($kb_definitivo >= 1024) {
                $mb=$kb/1024; // regla de tres (La multiplicacion por 1 no se efectua
                $total_consumo = $mb;
                $mb_prueba = explode(".",$mb);
                $mb_definitivo = $mb_prueba[0];
                $medida = 'Mb';
                if($mb_definitivo >= 1024) {
                    $gb = $mb/1024; // regla de tres (La multiplicacion por 1 no se efectua
                    $total_consumo = $gb;
                    $gb_prueba = explode(".",$gb);
                    $gb_definitivo= $gb_prueba[0];
                    $medida ='Gb';
                    if($gb_definitivo >= 1024) {
                        $tb = $gb/1024; // regla de tres (La multiplicacion por 1 no se efectua
                        $total_consumo = $tb;
                        $tb_prueba = explode(".",$tb);
                        $tb_definitivo = $tb_prueba[0];
                        $medida = 'Tb';
                    }// cierre condicional gb
                }// cierre condicional mega
            }//cierre condicional kb
        } else {
            $total_consumo = $bytes_total;
            $medida = 'Bytes';
        }
        $datos['total_consumo'] = $total_consumo;
        $datos['medida'] = $medida;
        return $datos;
    }

    // Método que permite calcular el porcentaje de uso de red
    public function metPorcentajeUso($totalConsumo, $redEquipo)
    {
        $porcentaje = ((100*$redEquipo)/$totalConsumo);
        $porcentajeTotal = number_format($porcentaje,2,',','');
        return $porcentajeTotal;
    }

    // Método que permite Generar el reporte de consumo guardado en el sistema
    public function metGenerarReporte()
    {
        if(isset($_GET['pk_num_reporte'])!='') {
            $pkNumReporte = $_GET['pk_num_reporte'];
        } else {
            echo "<script languaje='javascript' type='text/javascript'>
            alert('Debe recargar el reporte desde el listado guardado'); window.close();</script>";
        }
        $this->metObtenerLibreria('cabeceraEquipo','modCR');
        $pdf=new pdfGeneral('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true,10); // establece el margen inferior del pdf
        $consultarReporte = $this->atReporte->metConsultarReporte($pkNumReporte);
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 220); $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(55, 5,'Fecha del Reporte: '.$consultarReporte['fechaReporte'], 1, 0, 'L', 1);
        $pdf->Cell(70, 5,' Desde: '.$consultarReporte['fechaInicio'].'   Hasta: '.$consultarReporte['fechaFin'], 1, 0, 'L', 1);
        $pdf->Cell(135, 5,utf8_decode('Elaborado por: '.$consultarReporte['ind_nombre1'].' '.$consultarReporte['ind_apellido1']), 1, 0, 'L', 1);
        $pdf->SetFont('Arial', 'B', 6);
        $pdf->Ln();
        $piso = $this->atReporte->metPiso();
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(229, 229, 229); $pdf->SetTextColor(0, 0, 0);
        $totalGeneral = $this->atReporte->metConsumoTotal($pkNumReporte);
        $consumoTotal = $totalGeneral['totalConsumo'];
        $red = $this->metTransformar($consumoTotal);
        $redPrevia = $red['total_consumo'];
        $totalRed = number_format($redPrevia, 2, ',', '');
        foreach($piso as $piso) {
            $numPisoPrevio = $piso['num_piso'];
            $numPiso = $numPisoPrevio - 1;
            $pdf->SetFont('Arial', 'B', 8);
            if ($numPiso == 0) {
                $pdf->Cell(260, 5, utf8_decode('Piso: PB'), 1, 0, 'L', 1);
            } else {
                $pdf->Cell(260, 5, utf8_decode('Piso: ' . $numPiso), 1, 0, 'L', 1);
            }
            $pdf->Ln();
            $dependenciaReporte = $this->atReporte->metDependenciaReporte($numPisoPrevio,  $pkNumReporte);
            foreach($dependenciaReporte as $dependenciaReporte) {
                $dependenciaId=$dependenciaReporte['pk_num_dependencia'];
                $pdf->SetFont('Arial','B',8);
                $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
                $pdf->Cell(260,5,utf8_decode($dependenciaReporte['ind_dependencia']),1,0,'L',1);
                $pdf->Ln();
                $pdf->SetFont('Arial','',8);
                $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(229, 229, 229); $pdf->SetTextColor(0, 0, 0);
                $pdf->Cell(45,5,utf8_decode('Equipo'),1,0,'C',1);
                $pdf->Cell(20,5,utf8_decode('Ip'),1,0,'C',1);
                $pdf->Cell(40,5,utf8_decode('Usuario'),1,0,'L',1);
                $pdf->Cell(25,5,utf8_decode('Descarga'),1,0,'C',1);
                $pdf->Cell(15,5,utf8_decode('Porcentaje'),1,0,'C',1);
                $pdf->Cell(115,5,utf8_decode('Sitios Visitados'),1,0,'C',1);
                $pdf->Ln();
                $equipoReporte=$this->atReporte->metEquipoReporte($dependenciaId, $pkNumReporte);
                foreach($equipoReporte as $equipoReporte) {
                    $pkNumEquipo= $equipoReporte['pk_num_equipo'];
                    // Consulto los sitios visitados para poder ajustar el largo de las celdas
                    $sitiosVisitados= $this->atReporte->metSitiosDescarga($pkNumEquipo ,$pkNumReporte);
                    $totalSitios=count($sitiosVisitados);
                    if($totalSitios==0){
                        $largo=5;
                    } else {
                        $largo=$totalSitios*5;
                    }
                    // imprimo las celdas
                    $pdf->Cell(45,$largo,utf8_decode($equipoReporte['ind_nombre_equipo']),1,0,'L',0);
                    $pdf->Cell(20,$largo,utf8_decode($equipoReporte['ind_ip']),1,0,'L',0);
                    $pdf->Cell(40,$largo,utf8_decode($equipoReporte['ind_nombre1']).' '.utf8_decode($equipoReporte['ind_apellido1']),1,0,'L',0);
                    $consumoDescarga=$this->atReporte->metDescargaEquipo($pkNumEquipo ,$pkNumReporte);
                    $redEquipo=$consumoDescarga['num_descarga_total'];
                    $todo= $this->metTransformar($redEquipo);
                    $total_consumo= $todo['total_consumo'];
                    $total= number_format($total_consumo,2,',','');
                    $pdf->Cell(25,$largo,$total.' '.$todo['medida'],1,0,'R',0);
                    $porcentajeEquipo=$this->metPorcentajeUso($consumoTotal, $redEquipo);
                    $pdf->Cell(15,$largo,$porcentajeEquipo.'%',1,0,'R',0);
                    if($totalSitios == 0) {
                        $pdf->Cell(115,5,'',1,0,'R',0);
                    }
                    $registros =0;
                    foreach($sitiosVisitados as $sitiosVisitados) {
                        if($registros == 0) {
                            $pdf->Cell(115,5,utf8_decode($sitiosVisitados['ind_nombre_sitio']),1,0,'R',0);
                        }
                        if($registros > 0) {
                            if($registros<$totalSitios) {
                                $pdf->Ln();
                            }
                            $pdf->SetX(155);
                            $pdf->Cell(115,5,utf8_decode($sitiosVisitados['ind_nombre_sitio']),1,0,'R',0);
                        }
                        $registros++;
                    }// fin sitios visitados
                    $pdf->Ln();
                }
            }
        }
        $pdf->Ln();
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetFillColor(229, 229, 229);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(85,5,'Total',1,0,'C',1);
        $pdf->Cell(35,5,'Descarga',1,0,'C',1);
        $pdf->Cell(35,5,'Uso',1,0,'C',1);
        $pdf->Ln();
        $pdf->Cell(85,5,'Consumo General de Red',1,0,'C',0);
        $pdf->Cell(35,5,$totalRed.' '.$red['medida'],1,0,'C',0);
        $pdf->Cell(35,5,'100%',1,0,'C',0);
        $pdf->Output();
    }
}
?>