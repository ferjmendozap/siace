<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO SUCRE.
 * MODULO: Consumo de Red
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.sistemas.control@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase que permite visualizar la gráfica de consumo de red
class graficaRedControlador extends Controlador
{
    private $atGrafica;

    public function __construct(){
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.s
        $this->atGrafica = $this->metCargarModelo('graficaRed');
    }

    #Metodo Index del controlador Prueba.
    public function metIndex()
	{
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
		$js = array('materialSiace/core/demo/DemoTableDynamic', 'materialSiace/App', 'materialSiace/core/demo/DemoFormComponents');
		$dateRage[]= 'moment/moment.min';
        $datepickerjs[] = 'bootstrap-datepicker/bootstrap-datepicker';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($datepickerjs);
		$this->atVista->metCargarJsComplemento($dateRage);
		$this->atVista->metCargarJs($js);
        $validarjs= array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
		$flot = array(
			'flot/jquery.flot.min',
			'flot/jquery.flot.time'
		);
		$this->atVista->metCargarJsComplemento($flot);
        $this->atVista->metCargarJsComplemento($validarjs);
        $this->atVista->metRenderizar('graficaRed');
    }

	// Método que permite validar las fechas introducidas
	public function metValidarFecha()
	{
		$fechaAux = explode("/",$_POST['fechaInicio']);
		$fechaInicio = $fechaAux[2].'-'.$fechaAux[1].'-'.$fechaAux[0];
		$fechaAux = explode("/",$_POST['fechaFin']);
		$fechaFin = $fechaAux[2].'-'.$fechaAux[1].'-'.$fechaAux[0];
		if(strtotime($fechaFin) >= strtotime($fechaInicio)){
			echo json_encode(true);
		} else {
			echo json_encode(false);
		}
	}

	// Método que permite realizar la transformación de consumo de red en kb, Mb, Gb y Tb.
	public function metTransformar($valor)
	{
		$datos = array('total_consumo' => 0,'medida' => 0);
		$bytes_total = $valor;
		if($bytes_total>=1024)
		{
			$kb = $bytes_total/1024; // regla de tres (La multiplicacion por 1 no se efectua)
			$total_consumo = $kb;
			$kb_prueba = explode(".",$kb);
			$kb_definitivo = $kb_prueba[0];
			$medida = 'Kb';
			if($kb_definitivo>=1024) {
				$mb=$kb/1024; // regla de tres (La multiplicacion por 1 no se efectua
				$total_consumo=$mb;
				$mb_prueba= explode(".",$mb);
				$mb_definitivo= $mb_prueba[0];
				$medida='Mb';
			}//cierre condicional kb
		} else {
			$total_consumo=$bytes_total;
			$medida='Bytes';
		}
		$datos['total_consumo'] = $total_consumo;
		$datos['medida'] = $medida;
		return $datos;
	}

	// Método que permite generar la gráfica de consumo de red
    public function metGenerarGrafica()
	{
        ini_set("max_execution_time","9000");
		$fechaI = $_POST['fechaInicio'];
        $fechaF = $_POST['fechaFin'];
        $fechaExplodeInicio = explode("/",$fechaI);
        $fechaInicio = $fechaExplodeInicio[2].'-'.$fechaExplodeInicio[1].'-'.$fechaExplodeInicio[0];
        $fechaExplodeFin = explode("/",$fechaF);
        $fechaFin = $fechaExplodeFin[2].'-'.$fechaExplodeFin[1].'-'.$fechaExplodeFin[0];
        $dif = ((strtotime($fechaFin) - strtotime( $fechaInicio)) / 86400);
        $partes = explode("-",  $fechaInicio);
		$bytes = '[';
		$fecha = '[';
        for ($i = 0; $i <= $dif; $i++) {
            $dia = mktime(0, 0, 0, $partes[1], ($partes[2] + $i), $partes[0]);
            $fechaCalculo=date("Y-m-d", $dia);
            $totalRed=$this->atGrafica->metGenerarGrafica($fechaCalculo);
            $totalD = $totalRed['totalDia'];
            if($totalD!='') {
                $totalDia = $totalRed['totalDia'];
				$red = $this->metTransformar($totalDia);
				$redPrevia = $red['total_consumo'];
				$totalRed = number_format($redPrevia, 2, '.', '');
				$bytes .= $totalRed;
				$diaP = $partes[2]+$i;
				if($diaP < 10) {
					$diaP = '0'.$diaP;
				}
				$fecha .= '"'.$partes[0].'-'.$partes[1].'-'.($diaP).'"';
				if($i < $dif) {
					$bytes .= ',';
					$fecha .= ',';
				}
            } else {
                $totalDia = 0;
				$bytes .= $totalDia;
				$diaP = $partes[2]+$i;
				if($diaP < 10) {
					$diaP = '0'.$diaP;
				}
				$fecha .= '"'.$partes[0].'-'.$partes[1].'-'.($diaP).'"';
				if($i < $dif) {
					$bytes .= ',';
					$fecha .= ',';
				}
			}
        }
		$fecha .= ']';
		$bytes .= ']';
		$consumo = '{"bytes":'.$bytes.',"fecha":'.$fecha.'}';
		echo $consumo;
		//print_r($arregloDatos);
        exit();
    }
}
?>
