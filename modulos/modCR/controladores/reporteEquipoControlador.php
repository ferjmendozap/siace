<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO SUCRE.
 * MODULO: Consumo de Red
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.sistemas.control@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// clase encargada de generar el reporte de consumo de red por equipos
class reporteEquipoControlador extends Controlador
{
	private $atBusquedaEquipo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		#se carga el Modelo.
		$this->atBusquedaEquipo = $this->metCargarModelo('reporteGeneral');
		$this->atBusquedaEquipo = $this->metCargarModelo('reporteEquipo');
	}

	#Metodo Index del controlador Prueba.
	public function metIndex()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'bootstrap-datepicker/datepicker'
		);
		$js = array('materialSiace/core/demo/DemoTableDynamic', 'materialSiace/App', 'materialSiace/core/demo/DemoFormComponents');
		$datepickerjs[] = 'bootstrap-datepicker/bootstrap-datepicker';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($datepickerjs);
		$this->atVista->metCargarJs($js);
		$validarjs = array(
		'jquery-validation/dist/jquery.validate.min',
		'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validarjs);
		$this->atVista->assign('_PruebaPost', $this->atBusquedaEquipo->metListadoEquipos());
		$this->atVista->metRenderizar('reporteEquipo');
	}

	// Método que permite realizar la transformación de consumo de red en kb, Mb, Gb y Tb.
	public function metTransformar($valor)
	{
		$datos = array('total_consumo' => 0,'medida' => 0);
		$bytes_total=$valor;
		if($bytes_total>=1024) {
			$kb = $bytes_total/1024; // regla de tres (La multiplicacion por 1 no se efectua)
			$total_consumo = $kb;
			$kb_prueba = explode(".",$kb);
			$kb_definitivo = $kb_prueba[0];
			$medida = 'Kb';
			if($kb_definitivo>=1024) {
				$mb = $kb/1024; // regla de tres (La multiplicacion por 1 no se efectua
				$total_consumo = $mb;
				$mb_prueba = explode(".",$mb);
				$mb_definitivo = $mb_prueba[0];
				$medida = 'Mb';
				if($mb_definitivo>=1024) {
					$gb = $mb/1024; // regla de tres (La multiplicacion por 1 no se efectua
					$total_consumo = $gb;
					$gb_prueba = explode(".",$gb);
					$gb_definitivo = $gb_prueba[0];
					$medida = 'Gb';
					if($gb_definitivo>=1024) {
						$tb = $gb/1024; // regla de tres (La multiplicacion por 1 no se efectua
						$total_consumo = $tb;
						$tb_prueba = explode(".",$tb);
						$tb_definitivo = $tb_prueba[0];
						$medida = 'Tb';
					}// cierre condicional gb
				}// cierre condicional mega
			}//cierre condicional kb
		} else {
			$total_consumo = $bytes_total;
			$medida = 'Bytes';
		}
		$datos['total_consumo'] = $total_consumo;
		$datos['medida'] = $medida;
		return $datos;
	}

	// Método que permite calcular el porcentaje de uso de red
	public function metPorcentajeUso($totalConsumo, $redEquipo)
	{
		$porcentaje = ((100*$redEquipo)/$totalConsumo);
		$porcentajeTotal = number_format($porcentaje,2,',','');
		return $porcentajeTotal;
	}

	// Método que permite generar el reporte de consumo de red por equipos
	public function metReporteEquipo()
	{
		// Debido a la cantidad de registros a procesar se incrementa el tiempo para poder generar el reporte de consumo
		ini_set("max_execution_time","9000");
		$usuario = Session::metObtener('idUsuario');
		$obtenerUsuario = $this->atBusquedaEquipo->metUsuarioReporte($usuario);
		$this->metObtenerLibreria('cabeceraEquipo','modCR');
		$pdf = new pdfReporteEquipos('L','mm','Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true,10); // establece el margen inferior
		if(isset($_POST['fechaInicio'])!=''){
			$fechaI = $_POST['fechaInicio'];
			$fechaF = $_POST['fechaFin'];
		} else {
			echo "<script languaje='javascript' type='text/javascript'>
            alert('Debe cargar el reporte desde formulario de equipos'); window.close();</script>";
		}
		$cantidad = $_POST['numSitios'];
		if($cantidad>0) {
			$numSitios = $cantidad;
		} else {
			$numSitios =30;
		}
		$fechaExplodeInicio = explode("/",$fechaI);
		$fechaInicio = $fechaExplodeInicio[2].'-'.$fechaExplodeInicio[1].'-'.$fechaExplodeInicio[0];
		$fechaExplodeFin = explode("/",$fechaF);
		$fechaFin = $fechaExplodeFin[2].'-'.$fechaExplodeFin[1].'-'.$fechaExplodeFin[0];
		$fechaInicial= $fechaExplodeInicio[0].'/'.$fechaExplodeInicio[1].'/'.$fechaExplodeInicio[2];
		$fechaFinal = $fechaExplodeFin[0].'/'.$fechaExplodeFin[1].'/'.$fechaExplodeFin[2];
		$seleccionados=$_POST['equipo'];
		$pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 220); $pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(115, 5,'Desde: '.$fechaInicial.'   Hasta: '.$fechaFinal, 1, 0, 'L', 1);
		$pdf->Cell(145, 5,utf8_decode('Elaborado por: '.$obtenerUsuario['ind_nombre1'].' '.$obtenerUsuario['ind_apellido1']), 1, 0, 'L', 1);
		$pdf->SetFont('Arial', 'B', 6);
		$pdf->Ln();
		$pdf->SetFont('Arial','',8);
		$pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(229, 229, 229); $pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(30,5,utf8_decode('Dependencia'),1,0,'C',1);
		$pdf->Cell(45,5,utf8_decode('Equipo'),1,0,'C',1);
		$pdf->Cell(20,5,utf8_decode('Ip'),1,0,'C',1);
		$pdf->Cell(40,5,utf8_decode('Usuario'),1,0,'L',1);
		$pdf->Cell(15,5,utf8_decode('Descarga'),1,0,'C',1);
		$pdf->Cell(15,5,utf8_decode('Porcentaje'),1,0,'C',1);
		$pdf->Cell(95,5,utf8_decode('Sitios Visitados'),1,0,'C',1);
		$pdf->Ln();
		$consumoTotal = $this->atBusquedaEquipo->metConsumoRed($fechaInicio, $fechaFin);
		$totalConsumo = $consumoTotal['total'];
		if($totalConsumo>0) {
			$red = $this->metTransformar($totalConsumo);
			$redPrevia = $red['total_consumo'];
			$totalRed = number_format($redPrevia, 2, ',', '');
			$descarga = 0;
			for ($i = 0; $i < count($seleccionados); $i++) {
				$pkNumEquipo = $seleccionados[$i];
				$gestionEquipo = $this->atBusquedaEquipo->metMostrarEquipo($pkNumEquipo);
				$direccionMysar = $gestionEquipo['ind_direccion_fisica'];
				$sitiosVisitados = $this->atBusquedaEquipo->metSitiosVisitados($fechaInicio, $fechaFin, $direccionMysar, $numSitios);
				$totalSitios = count($sitiosVisitados);
				if ($totalSitios == 0) {
					$largo = 5;
				} else {
					$largo = $totalSitios * 5;
				}
				$pdf->Cell(30, $largo, utf8_decode($gestionEquipo['txt_abreviacion']), 1, 0, 'L', 0);
				$pdf->Cell(45, $largo, utf8_decode($gestionEquipo['ind_nombre_equipo']), 1, 0, 'L', 0);
				$pdf->Cell(20, $largo, utf8_decode($gestionEquipo['ind_ip']), 1, 0, 'L', 0);
				$pdf->Cell(40, $largo, utf8_decode($gestionEquipo['ind_nombre1'] . ' ' . $gestionEquipo['ind_apellido1']), 1, 0, 'L', 0);
				$consumoEquipo = $this->atBusquedaEquipo->metConsumoEquipo($fechaInicio, $fechaFin, $direccionMysar);
				foreach ($consumoEquipo as $consumoEquipo) {
					$redEquipo = $consumoEquipo['consumo'];
					$todo = $this->metTransformar($redEquipo);
					$total_consumo = $todo['total_consumo'];
					$total = number_format($total_consumo, 2, ',', '');
					$pdf->Cell(15, $largo, $total . ' ' . $todo['medida'], 1, 0, 'R', 0);
					$porcentajeEquipo = $this->metPorcentajeUso($totalConsumo, $redEquipo);
					$pdf->Cell(15, $largo, $porcentajeEquipo . '%', 1, 0, 'R', 0);
					if ($totalSitios == 0) {
						$pdf->Cell(95, 5, '', 1, 0, 'R', 0);
					}
					$registros = 0;
					foreach ($sitiosVisitados as $sitiosVisitados) {
						if ($registros == 0) {
							$pdf->Cell(95, 5, utf8_decode($sitiosVisitados['site']), 1, 0, 'R', 0);
						}
						if ($registros > 0) {
							if ($registros < $totalSitios) {
								$pdf->Ln();
							}
							$pdf->SetX(175);
							$pdf->Cell(95, 5, utf8_decode($sitiosVisitados['site']), 1, 0, 'R', 0);
						}
						$registros++;
					}
				}
				$pdf->Ln();
				$descarga = $descarga + $redEquipo;
			}
			$redDescarga = $this->metTransformar($descarga);
			$descargaPrevia = $redDescarga['total_consumo'];
			$totalDescarga = number_format($descargaPrevia, 2, ',', '');
			$porcentajeDescarga = $this->metPorcentajeUso($totalConsumo, $descarga);
			$pdf->Ln();
			$pdf->SetDrawColor(0, 0, 0);
			$pdf->SetFillColor(200, 200, 220);
			$pdf->SetTextColor(0, 0, 0);
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Ln();
			$pdf->SetDrawColor(0, 0, 0);
			$pdf->SetFillColor(229, 229, 229);
			$pdf->SetTextColor(0, 0, 0);
			$pdf->Cell(85, 5, 'Total', 1, 0, 'C', 1);
			$pdf->Cell(35, 5, 'Descarga', 1, 0, 'C', 1);
			$pdf->Cell(35, 5, 'Uso', 1, 0, 'C', 1);
			$pdf->Ln();
			$pdf->Cell(85, 5, 'Consumo Total de Equipos Consultados', 1, 0, 'C', 0);
			$pdf->Cell(35, 5, $totalDescarga . ' ' . $redDescarga['medida'], 1, 0, 'C', 0);
			$pdf->Cell(35, 5, $porcentajeDescarga . '%', 1, 0, 'C', 0);
			$pdf->Ln();
			$pdf->Cell(85, 5, 'Consumo General de Red', 1, 0, 'C', 0);
			$pdf->Cell(35, 5, $totalRed . ' ' . $red['medida'], 1, 0, 'C', 0);
			$pdf->Cell(35, 5, '100%', 1, 0, 'C', 0);
		}
		$pdf->Output();
	}

	// Método que permite verificar las fechas del reporte
	public function metVerificarFechas()
	{
		$fechaAux = explode("/",$_POST['fechaInicio']);
		$fechaInicio = $fechaAux[2].'-'.$fechaAux[1].'-'.$fechaAux[0];
		$fechaAux = explode("/",$_POST['fechaFin']);
		$fechaFin = $fechaAux[2].'-'.$fechaAux[1].'-'.$fechaAux[0];
		if(strtotime($fechaFin) >= strtotime($fechaInicio)){
			echo json_encode(true);
		} else {
			echo json_encode(false);
		}
	}
}
?>
