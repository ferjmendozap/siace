<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Consumo de Red. Menú
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Menu
require_once 'dataMenuCR.php';
class scriptCargaCRControlador extends Controlador
{
    use dataMenuCR;

    private $atScriptCarga;

    public function __construct()
    {
        parent::__construct();
        $this->atScriptCarga = $this->metCargarModelo('scriptCarga');
    }

    public function metIndex()
    {
        echo "INICIANDO<br>";

        echo "MENU CONSUMO DE RED<br>";
        $this->atScriptCarga->metCargarMenu($this->metDataMenu());

        echo "TERMINADO";
    }
}
