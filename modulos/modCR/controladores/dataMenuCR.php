<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Consumo de Red. Menú
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Menu
trait dataMenuCR
{

    public function metMenuCR()
    {
       $a027_seguridad_menu = array(
  array('ind_nombre' => 'Equipos','ind_descripcion' => 'Gestión de estaciones de trabajo','cod_interno' => 'CR-01-01','cod_padre' => 'CR-01','num_nivel' => '1','ind_ruta' => 'modCR/equipoCONTROL','ind_rol' => 'CR-01-01','ind_icono' => 'md md-desktop-windows','ind_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fk_a015_num_seguridad_aplicacion' => '13','fec_ultima_modificacion' => '2015-11-13 10:03:33'),
  array('ind_nombre' => 'Rango de Fecha','ind_descripcion' => 'Consulta el consumo de todos los equipos en un rango de fecha dado','cod_interno' => 'CR-01-02-01','cod_padre' => 'CR-01-02','num_nivel' => '2','ind_ruta' => 'modCR/reporteGeneralCONTROL','ind_rol' => 'CR-01-02-01-L','ind_icono' => 'md md-cast-connected','ind_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fk_a015_num_seguridad_aplicacion' => '13','fec_ultima_modificacion' => '2015-11-13 10:03:33'),
  array('ind_nombre' => 'CONSUMO DE RED','ind_descripcion' => 'Gestiona el consumo de red de los equipos','cod_interno' => 'CR-01','cod_padre' => '','num_nivel' => '0','ind_ruta' => '','ind_rol' => 'CR-01-L','ind_icono' => 'md md-language','ind_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fk_a015_num_seguridad_aplicacion' => '13','fec_ultima_modificacion' => '2015-11-14 10:35:00'),
  array('ind_nombre' => 'Consultar Consumo de Red','ind_descripcion' => 'Consulta el consumo de red de los equipos','cod_interno' => 'CR-01-02','cod_padre' => 'CR-01','num_nivel' => '1','ind_ruta' => '','ind_rol' => 'CR-01-02-L','ind_icono' => 'md md-settings-input-antenna','ind_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fk_a015_num_seguridad_aplicacion' => '13','fec_ultima_modificacion' => '2015-11-14 10:35:00'),
  array('ind_nombre' => 'Equipos','ind_descripcion' => 'Consulta el consumo de red por equipos','cod_interno' => 'CR-01-02-02','cod_padre' => 'CR-01-02','num_nivel' => '2','ind_ruta' => 'modCR/reporteEquipoCONTROL','ind_rol' => 'CR-01-02-02-L','ind_icono' => 'md md-cast','ind_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fk_a015_num_seguridad_aplicacion' => '13','fec_ultima_modificacion' => '2016-02-05 15:55:20'),
  array('ind_nombre' => 'Dependencia','ind_descripcion' => 'Consulta el consumo de red por dependencias','cod_interno' => 'CR-01-02-03','cod_padre' => 'CR-01-02','num_nivel' => '2','ind_ruta' => 'modCR/reporteDependenciaCONTROL','ind_rol' => 'CR-01-02-03-L','ind_icono' => 'md md-folder-shared','ind_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fk_a015_num_seguridad_aplicacion' => '13','fec_ultima_modificacion' => '2016-02-05 15:55:31'),
  array('ind_nombre' => 'Reporte de consumo','ind_descripcion' => 'Muestra los reportes guardados del consumo de red','cod_interno' => 'CR-01-03','cod_padre' => 'CR-01','num_nivel' => '1','ind_ruta' => 'modCR/reporteCONTROL','ind_rol' => 'CR-01-03-L','ind_icono' => 'md md-insert-drive-file','ind_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fk_a015_num_seguridad_aplicacion' => '13','fec_ultima_modificacion' => '2015-11-14 10:35:00'),
  array('ind_nombre' => 'Gráfica de Consumo','ind_descripcion' => 'Visualizar gráfica de consumo de red','cod_interno' => 'CR-01-04','cod_padre' => 'CR-01','num_nivel' => '1','ind_ruta' => 'modCR/graficaRedCONTROL','ind_rol' => 'CR-01-04-L','ind_icono' => 'md md-trending-up','ind_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fk_a015_num_seguridad_aplicacion' => '13','fec_ultima_modificacion' => '2015-11-14 10:35:00'),
  array('ind_nombre' => 'Nuevo Equipo','ind_descripcion' => 'Registrar nuevo equipo','cod_interno' => 'CR-01-01-01','cod_padre' => 'CR-01-01','num_nivel' => '9','ind_ruta' => '','ind_rol' => 'CR-01-01-01-N','ind_icono' => 'md md-done','ind_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fk_a015_num_seguridad_aplicacion' => '13','fec_ultima_modificacion' => '2015-12-04 13:25:11'),
  array('ind_nombre' => 'Editar equipo','ind_descripcion' => 'Permite modificar equipos','cod_interno' => 'CR-01-01-02','cod_padre' => 'CR-01-01','num_nivel' => '9','ind_ruta' => '','ind_rol' => 'CR-01-01-02-M','ind_icono' => 'md md-done','ind_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fk_a015_num_seguridad_aplicacion' => '13','fec_ultima_modificacion' => '2015-12-04 13:26:23'),
  array('ind_nombre' => 'Eliminar equipos','ind_descripcion' => 'Permite eliminar equipos','cod_interno' => 'CR-01-01-03','cod_padre' => 'CR-01-01','num_nivel' => '9','ind_ruta' => '','ind_rol' => 'CR-01-01-03-E','ind_icono' => 'md md-done','ind_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fk_a015_num_seguridad_aplicacion' => '13','fec_ultima_modificacion' => '2015-12-04 13:27:15'),
  array('ind_nombre' => 'Visualizar equipos','ind_descripcion' => 'Visualiza los equipos registrados','cod_interno' => 'CR-01-01-04','cod_padre' => 'CR-01-01','num_nivel' => '9','ind_ruta' => '','ind_rol' => 'CR-01-01-04-V','ind_icono' => 'md md-done','ind_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fk_a015_num_seguridad_aplicacion' => '13','fec_ultima_modificacion' => '2015-12-04 13:28:25'),
  array('ind_nombre' => 'Reporte de equipos','ind_descripcion' => 'Permite ver el reporte de los equipos registrads','cod_interno' => 'CR-01-01-05','cod_padre' => 'CR-01-01','num_nivel' => '9','ind_ruta' => '','ind_rol' => 'CR-01-01-05-RE','ind_icono' => 'md md-done','ind_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fk_a015_num_seguridad_aplicacion' => '13','fec_ultima_modificacion' => '2015-12-04 13:29:30'),
  array('ind_nombre' => 'Guardar reporte de consumo','ind_descripcion' => 'Permite guardar el reporte de consumo de red','cod_interno' => 'CR-01-03-01-GR','cod_padre' => 'CR-01-03','num_nivel' => '9','ind_ruta' => '','ind_rol' => 'CR-01-03-01-GR','ind_icono' => 'md md-done','ind_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fk_a015_num_seguridad_aplicacion' => '13','fec_ultima_modificacion' => '2015-12-04 13:31:55'),
  array('ind_nombre' => 'Visualizar reporte de consumo de red','ind_descripcion' => 'Permite visualizar el reporte de consumo de red guardado ','cod_interno' => 'CR-01-03-02','cod_padre' => 'CR-01-03','num_nivel' => '9','ind_ruta' => '','ind_rol' => 'CR-01-03-02-VR','ind_icono' => 'md md-done','ind_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fk_a015_num_seguridad_aplicacion' => '13','fec_ultima_modificacion' => '2015-12-04 13:33:09'),
  array('ind_nombre' => 'Eliminar reporte de consumo de red','ind_descripcion' => 'Permite eliminar el reporte de consumo de red guardado','cod_interno' => 'CR-01-03-03','cod_padre' => 'CR-01-03','num_nivel' => '9','ind_ruta' => '','ind_rol' => 'CR-01-03-03-ER','ind_icono' => 'md md-done','ind_estatus' => '1','fk_a018_num_seguridad_usuario' => '1','fk_a015_num_seguridad_aplicacion' => '13','fec_ultima_modificacion' => '2015-12-04 13:34:14')
);
        return $a027_seguridad_menu;
    }

    public function metDataMenu()
    {
	    $a027_seguridad_menu = array_merge(
            $this->metMenuCR()
        );
        return $a027_seguridad_menu;
    }

}
