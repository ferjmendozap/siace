<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO SUCRE.
 * MODULO: Consumo de Red
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.sistemas.control@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase encargada de generar el consumo de red por dependencias
class reporteDependenciaControlador extends Controlador
{
	private $atBusquedaDependencia;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		#se carga el Modelo.
		$this->atBusquedaDependencia = $this->metCargarModelo('reporteGeneral');
		$this->atBusquedaDependencia = $this->metCargarModelo('reporteDependencia');
	}

	#Metodo Index del controlador Prueba.
	public function metIndex()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'bootstrap-datepicker/datepicker'
		);
		$js = array('materialSiace/core/demo/DemoTableDynamic', 'materialSiace/App', 'materialSiace/core/demo/DemoFormComponents');
		$datepickerjs[] = 'bootstrap-datepicker/bootstrap-datepicker';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($datepickerjs);
		$this->atVista->metCargarJs($js);
		$validarjs = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
        $usuario = Session::metObtener('idUsuario');
        $idAplicacion = 13;
		$this->atVista->metCargarJsComplemento($validarjs);
		$this->atVista->assign('_PruebaPost', $this->atBusquedaDependencia->metListarDependencia($usuario, $idAplicacion));
		$this->atVista->metRenderizar('reporteDependencia');
	}

	// Método que permite realizar la transformación de consumo de red en kb, Mb, Gb y Tb.
	public function metTransformar($valor)
	{
		$datos = array('total_consumo' => 0, 'medida' => 0);
		$bytes_total = $valor;
		if ($bytes_total >= 1024) {
			$kb = $bytes_total / 1024; // regla de tres (La multiplicacion por 1 no se efectua)
			$total_consumo = $kb;
			$kb_prueba = explode(".", $kb);
			$kb_definitivo = $kb_prueba[0];
			$medida = 'Kb';
			if ($kb_definitivo >= 1024) {
				$mb = $kb / 1024; // regla de tres (La multiplicacion por 1 no se efectua
				$total_consumo = $mb;
				$mb_prueba = explode(".", $mb);
				$mb_definitivo = $mb_prueba[0];
				$medida = 'Mb';
				if ($mb_definitivo >= 1024) {
					$gb = $mb / 1024; // regla de tres (La multiplicacion por 1 no se efectua
					$total_consumo = $gb;
					$gb_prueba = explode(".", $gb);
					$gb_definitivo = $gb_prueba[0];
					$medida = 'Gb';
					if ($gb_definitivo >= 1024) {
						$tb = $gb / 1024; // regla de tres (La multiplicacion por 1 no se efectua
						$total_consumo = $tb;
						$tb_prueba = explode(".", $tb);
						$tb_definitivo = $tb_prueba[0];
						$medida = 'Tb';
					}// cierre condicional gb
				}// cierre condicional mega
			}//cierre condicional kb
		} else {
			$total_consumo = $bytes_total;
			$medida = 'Bytes';
		}
		$datos['total_consumo'] = $total_consumo;
		$datos['medida'] = $medida;
		return $datos;
	}

	// Método que permite calcular el porcentaje de uso de red
	public function metPorcentajeUso($totalConsumo, $redEquipo)
	{
		$porcentaje = ((100 * $redEquipo) / $totalConsumo);
		$porcentajeTotal = number_format($porcentaje, 2, ',', '');
		return $porcentajeTotal;
	}

	// Método que permite generar el reporte de consumo de red por dependencias
	public function metReporteDependencia()
	{
		// Debido a la cantidad de registros a procesar se incrementa el tiempo para poder generar el reporte de consumo
		ini_set("max_execution_time", "9000");
		$usuario = Session::metObtener('idUsuario');
		$obtenerUsuario = $this->atBusquedaDependencia->metUsuarioReporte($usuario);
		$this->metObtenerLibreria('cabeceraEquipo', 'modCR');
		$pdf = new pdfDependencia('L', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true,10); // establece el margen inferior
		if(isset($_POST['fechaInicio'])!=''){
			$fechaI = $_POST['fechaInicio'];
			$fechaF = $_POST['fechaFin'];
		} else {
			echo "<script languaje='javascript' type='text/javascript'>
            alert('Debe cargar el reporte desde formulario de dependencias'); window.close();</script>";
		}
		$fechaExplodeInicio = explode("/",$fechaI);
		$fechaInicio = $fechaExplodeInicio[2].'-'.$fechaExplodeInicio[1].'-'.$fechaExplodeInicio[0];
		$fechaExplodeFin = explode("/",$fechaF);
		$fechaFin = $fechaExplodeFin[2].'-'.$fechaExplodeFin[1].'-'.$fechaExplodeFin[0];
		$fechaInicial = $fechaExplodeInicio[0].'/'.$fechaExplodeInicio[1].'/'.$fechaExplodeInicio[2];
		$fechaFinal = $fechaExplodeFin[0].'/'.$fechaExplodeFin[1].'/'.$fechaExplodeFin[2];
		$seleccionados = $_POST['dependencia'];
		$numSitios = 5;
		$pdf->SetDrawColor(0, 0, 0);
		$pdf->SetFillColor(200, 200, 220);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(115, 5, 'Desde: ' . $fechaInicial . '   Hasta: ' . $fechaFinal, 1, 0, 'L', 1);
		$pdf->Cell(145, 5,utf8_decode('Elaborado por: '.$obtenerUsuario['ind_nombre1'].' '.$obtenerUsuario['ind_apellido1']), 1, 0, 'L', 1);
		$pdf->SetFont('Arial', 'B', 8);
		$pdf->SetDrawColor(0, 0, 0);
		$pdf->SetFillColor(229, 229, 229);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Ln();
		$pdf->Cell(115, 5, utf8_decode('Dependencia'), 1, 0, 'C', 1);
		$pdf->Cell(15, 5, utf8_decode('Descarga'), 1, 0, 'C', 1);
		$pdf->Cell(15, 5, utf8_decode('Uso'), 1, 0, 'C', 1);
		$pdf->Cell(115, 5, utf8_decode('Sitios Visitados'), 1, 0, 'C', 1);
		$pdf->Ln();
		$consumoTotal = $this->atBusquedaDependencia->metConsumoRed($fechaInicio, $fechaFin);
		$totalConsumo = $consumoTotal['total'];
		if($totalConsumo>0) {
			$red = $this->metTransformar($totalConsumo);
			$redPrevia = $red['total_consumo'];
			$totalRed = number_format($redPrevia, 2, ',', '');
			$contadorDescarga = 0;
			for ($i = 0; $i < count($seleccionados); $i++) {
				$acumuladorDescarga = 0;
			$arregloSitios = array();
			$indiceSitios = 0;
				$pkNumDependencia = $seleccionados[$i];
				$gestionDependencia = $this->atBusquedaDependencia->metDependenciaListado($pkNumDependencia);
				$dependenciaEquipos = $this->atBusquedaDependencia->metDependenciaEquipos($pkNumDependencia);
				$totalContarEquipo = count($dependenciaEquipos);
				if($totalContarEquipo>0) {
					foreach ($dependenciaEquipos as $dependenciaEquipos) {
						$direccionMysar = $dependenciaEquipos['ind_direccion_fisica'];
						// se procesan los sitios visitados con la finalidad de ajustar las celdas
						$sitiosEquipos = $this->atBusquedaDependencia->metSitiosVisitados($fechaInicio, $fechaFin, $direccionMysar, $numSitios);
						foreach ($sitiosEquipos as $sitiosEquipos) {
							if ($indiceSitios > 0) {
								$band = true;
								for ($t = 0; $t < count($arregloSitios); $t++) {
									if ($arregloSitios[$t]['idSitio'] == $sitiosEquipos['sitesID']) {
										$arregloSitios[$t]['cuentaSitio']++;
										$band = false;
									}
								}
								// si no se encuentra el sitio en el arreglo de sitios entonces se agrega
								if ($band == true) {
									$arregloSitios[$indiceSitios]['idSitio'] = $sitiosEquipos['sitesID'];
									$arregloSitios[$indiceSitios]['nombreSitio'] = $sitiosEquipos['site'];
									$arregloSitios[$indiceSitios]['cuentaSitio'] = 1;
									$indiceSitios++;
								}
							} else {
								$arregloSitios[$indiceSitios]['idSitio'] = $sitiosEquipos['sitesID'];
								$arregloSitios[$indiceSitios]['nombreSitio'] = $sitiosEquipos['site'];
								$arregloSitios[$indiceSitios]['cuentaSitio'] = 1;
								$indiceSitios++;
							}
						}
					}
					// ajusto el largo de las celdas para que se adapten
					$registros = count($arregloSitios);
					if ($registros > 4) {
						$largo = 25;
					}
					if (($registros >= 1) && ($registros <= 4)) {
						$largo = $registros * 5;
					}
					if ($registros == 0) {
						$largo = 5;
					}
					//ordenamiento burbuja
					for ($k = 1; $k < count($arregloSitios); $k++) {
						for ($g = 0; $g < count($arregloSitios) - 1; $g++) {
							if ($arregloSitios[$g]['cuentaSitio'] < $arregloSitios[$g + 1]['cuentaSitio']) {
								$idSitioTemp = $arregloSitios[$g]['idSitio'];
								$nombreSitioTemp = $arregloSitios[$g]['nombreSitio'];
								$cuentaSitioTemp = $arregloSitios[$g]['cuentaSitio'];
								$arregloSitios[$g]['idSitio'] = $arregloSitios[$g + 1]['idSitio'];
								$arregloSitios[$g]['nombreSitio'] = $arregloSitios[$g + 1]['nombreSitio'];
								$arregloSitios[$g]['cuentaSitio'] = $arregloSitios[$g + 1]['cuentaSitio'];
								$arregloSitios[$g + 1]['idSitio'] = $idSitioTemp;
								$arregloSitios[$g + 1]['nombreSitio'] = $nombreSitioTemp;
								$arregloSitios[$g + 1]['cuentaSitio'] = $cuentaSitioTemp;
							}
						}
					}
					//fin procesar sitios visitados
					// proceso el coonsumo de red por equipos
					$consumoEquipo = $this->atBusquedaDependencia->metConsumoEquipo($fechaInicio, $fechaFin, $direccionMysar);
					foreach ($consumoEquipo as $consumoEquipo) {
						$redEquipo = $consumoEquipo['consumo'];
						$acumuladorDescarga = $acumuladorDescarga + $redEquipo;
					}
					$pdf->Cell(115, $largo, utf8_decode($gestionDependencia), 1, 0, 'L', 0);
					$todo = $this->metTransformar($acumuladorDescarga);
					$total_consumo = $todo['total_consumo'];
					$total = number_format($total_consumo, 2, ',', '');
					$pdf->Cell(15, $largo, $total . ' ' . $todo['medida'], 1, 0, 'R', 0);
					$porcentajeEquipo = $this->metPorcentajeUso($totalConsumo, $acumuladorDescarga);
					$pdf->Cell(15, $largo, $porcentajeEquipo . '%', 1, 0, 'R', 0);
					if ($registros == 0) {
						$pdf->Cell(115, 5, '', 1, 0, 'R', 0);
						$pdf->Ln();
					} else {
						// selecciono los primeros 5 sitios del arreglo previamente ordenado
						for ($s = 0; $s < $numSitios; $s++) {
							if ($s > 0) {
								$pdf->SetX(155);
							}
							$pdf->Cell(115, 5, $arregloSitios[$s]['nombreSitio'], 1, 0, 'R', 0);
							$pdf->Ln();
						}
					}
					$contadorDescarga = $contadorDescarga + $acumuladorDescarga;
				}
			}
			$descarga = $this->metTransformar($contadorDescarga);
			$descargaPrevia = $descarga['total_consumo'];
			$descargaTotal = number_format($descargaPrevia, 2, ',', '');
			$porcentajeDependencia = $this->metPorcentajeUso($totalConsumo, $contadorDescarga);
			$pdf->Ln();
			$pdf->SetDrawColor(0, 0, 0);
			$pdf->SetFillColor(200, 200, 220);
			$pdf->SetTextColor(0, 0, 0);
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Ln();
			$pdf->SetDrawColor(0, 0, 0);
			$pdf->SetFillColor(229, 229, 229);
			$pdf->SetTextColor(0, 0, 0);
			$pdf->Cell(85, 5, 'Total', 1, 0, 'C', 1);
			$pdf->Cell(35, 5, 'Descarga', 1, 0, 'C', 1);
			$pdf->Cell(35, 5, 'Uso', 1, 0, 'C', 1);
			$pdf->Ln();
			$pdf->Cell(85, 5, 'Consumo Total de Dependencias Consultadas', 1, 0, 'C', 0);
			$pdf->Cell(35, 5, $descargaTotal . ' ' . $descarga['medida'], 1, 0, 'C', 0);
			$pdf->Cell(35, 5, $porcentajeDependencia . '%', 1, 0, 'C', 0);
			$pdf->Ln();
			$pdf->Cell(85, 5, 'Consumo General de Red', 1, 0, 'C', 0);
			$pdf->Cell(35, 5, $totalRed . ' ' . $red['medida'], 1, 0, 'C', 0);
			$pdf->Cell(35, 5, '100%', 1, 0, 'C', 0);
		}
		$pdf->Output();
	}
	public function metVerificarFechas()
	{
		$fechaAux = explode("/",$_POST['fechaInicio']);
		$fechaInicio = $fechaAux[2].'-'.$fechaAux[1].'-'.$fechaAux[0];
		$fechaAux = explode("/",$_POST['fechaFin']);
		$fechaFin = $fechaAux[2].'-'.$fechaAux[1].'-'.$fechaAux[0];
		if(strtotime($fechaFin) >= strtotime($fechaInicio)) {
			echo json_encode(true);
		} else {
			echo json_encode(false);
		}
	}
}
?>
