<section class="style-default-bright">
    <div class="section-header">
        <h3 class="text-primary">Asignacion de Conceptos</h3>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <form class="form-horizontal" role="form">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="col-sm-6">
                        <div class="col-sm-12">
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <label for="codEmpleado" class="col-sm-6 control-label">Cod. Empleado</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="codEmpleado" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="nomApe" class="col-sm-4 control-label">Nombre y Apellido</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="nomApe" value="" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="col-sm-12">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="tipoNomina" class="col-sm-4 control-label">Tipo de Nomina</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="tipoNomina" value="" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <label for="fecIngreso" class="col-sm-6 control-label">Fecha de Ingreso</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="fecIngreso" value="" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 text-center">
                        <button  data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" type="button" class="btn btn-xs btn-primary" id="selectEmpleado" titulo="seleccionar Empleados">Seleccionar Empleado</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<br>
<div class="row" id="listaConceptoProceso">

</div>
<script type="text/javascript">
    $(document).ready(function () {
        var $url='{$_Parametros.url}listados/listarEmpleados/NMSL';
        $('#selectEmpleado').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ o:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
    });
</script>