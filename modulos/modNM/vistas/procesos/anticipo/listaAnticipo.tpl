<div class="col-sm-offset-1 col-sm-10">
    <div class="card">
        <div class="card-body">
            <span class="clearfix"></span>
            <div class="table-responsive no-margin">
                <table class="table table-striped no-margin" id="conceptoAsignadoEmpleado">
                    <thead>
                        <tr>
                            <th> PERIODO </th>
                            <th> JUSTIFICATIVO </th>
                            <th> MONTO ACUMULADO </th>
                            <th> ANTICIPO </th>
                            <th> TOTAL </th>
                            <th> REPORTE </th>
                        </tr>
                    </thead>
                    <tbody>
                    {if isset($lista)}
                        {foreach item=i from=$lista}
                                <tr id="idAnticipo{$i.pk_num_prestaciones_sociales_anticipo}">
                                    <td> {$i.ind_periodo} </td>
                                    <td> {$i.ind_nombre_detalle} </td>
                                    <td> {$i.num_monto_acumulado} </td>
                                    <td> {$i.num_anticipo} </td>
                                    <td> {$i.num_monto_acumulado - $i.num_anticipo} </td>
                                    <td>
                                        {if $i.num_estatus == 0}
                                            {if in_array('NM-01-01-06-01-02-M',$_Parametros.perfil)}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" idAnticipo="{$i.pk_num_prestaciones_sociales_anticipo}"  title="Editar"
                                                        descipcion="El Usuario es Modificado Un Anticipo" titulo="<i class='fa fa-exchange'></i> Modificar Anticipo de prestaciones Sociales">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        {/if}
                                        {if in_array('NM-01-01-06-01-03-R',$_Parametros.perfil)}
                                            <button class="reporte logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                     data-keyboard="false" data-backdrop="static" idAnticipo="{$i.pk_num_prestaciones_sociales_anticipo}" title="Imprimir"
                                                     descipcion="El Usuario a Abierto un Reporte de Anticipo" titulo="<i class='icm icm-calculate2'></i> Reporte Anticipo periodo: {$i.ind_periodo} ">
                                                <i class="icm icm-print2" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                        {/foreach}
                    {/if}
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="9">
                            {if in_array('NM-01-01-06-01-01-N',$_Parametros.perfil)}
                                <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                        descipcion="el Usuario esta cargando un nuevo anticipo"  titulo="<i class='fa fa-exchange'></i> Agregar Anticipo de prestaciones Sociales" id="nuevo" >
                                    <i class="md md-create"></i>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nuevo Anticipo
                                </button>
                            {/if}
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var url='{$_Parametros.url}modNM/procesos/anticipoCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idAnticipo:0, idEmpleado:$('#codEmpleado').val() },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#conceptoAsignadoEmpleado tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idAnticipo: $(this).attr('idAnticipo'), idEmpleado:$('#codEmpleado').val() },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#conceptoAsignadoEmpleado tbody').on( 'click', '.reporte', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html('<iframe frameborder="0" src="{$_Parametros.url}modNM/procesos/anticipoCONTROL/reporteMET/'+$(this).attr('idAnticipo')+'/'+$('#codEmpleado').val()+'"  width="100%" height="540px"></iframe>');

        });
    });
</script>