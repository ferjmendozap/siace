<form action="{$_Parametros.url}modNM/procesos/anticipoCONTROL/crearModificarMET" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idAnticipo}" name="idAnticipo"/>
        <input type="hidden" value="{$idEmpleado}" name="idEmpleado" id="idEmpleado"/>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group" id="fk_a006_num_miscelaneo_detalle_justificativoError">
                    <label for="fk_a006_num_miscelaneo_detalle_justificativo"><i class="md md-map"></i> Justificativo</label>
                    <select id="fk_a006_num_miscelaneo_detalle_justificativo" name="form[int][fk_a006_num_miscelaneo_detalle_justificativo]" class="form-control select2-list select2">
                        <option value="">Seleccione el Justificativo</option>
                        {if isset($justificativo)}
                            {foreach item=i from=$justificativo}
                                {if isset($formDB.fk_a006_num_miscelaneo_detalle_justificativo)}
                                    {if $i.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_justificativo}
                                        <option selected
                                                value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                    {else}
                                        <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                    {/if}
                                {else}
                                    <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                {/if}
                            {/foreach}
                        {/if}
                    </select>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group control-width-normal" id="fec_anticipoError">
                    <div class="input-group date" id="fec_anticipo">
                        <div class="input-group-content">
                            <input type="text" id="fec_anticipo" class="form-control" name="form[txt][fec_anticipo]" value="{if isset($formDB.fec_anticipo)}{$formDB.fec_anticipo}{/if}">
                            <label>fecha Anticipo</label>
                        </div>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group control-width-normal" id="num_monto_acumuladoError">
                    <div class="input-group-content">
                        <input type="text" class="form-control" disabled value="{if isset($formDB.num_monto_acumulado)}{$formDB.num_monto_acumulado}{else}{$lista.num_monto_acumulado}{/if}">
                        <input type="hidden" id="num_monto_acumulado"  name="form[int][num_monto_acumulado]" value="{if isset($formDB.num_monto_acumulado)}{$formDB.num_monto_acumulado}{else}{$lista.num_monto_acumulado}{/if}">
                        <label>Acumulado</label>
                    </div>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group" id="ind_porcentajeError">
                    <label for="ind_porcentaje"><i class="fa fa-list-alt"></i> Porcentaje</label>
                    <input type="text" class="form-control" value="{if isset($formDB.ind_porcentaje)}{$formDB.ind_porcentaje}{/if}" name="form[int][ind_porcentaje]" id="ind_porcentaje">
                </div>
            </div>
            <div class="col-sm-8">
                <div class="form-group" id="num_anticipoError">
                    <label for="num_anticipo"><i class="fa fa-list-alt"></i> Anticipo</label>
                    <input disabled type="text" class="form-control text-center" value="{if isset($formDB.num_anticipo)}{$formDB.num_anticipo}{/if}" id="num_anticipo">
                    <input type="hidden" value="{if isset($formDB.num_anticipo)}{$formDB.num_anticipo}{/if}" name="form[int][num_anticipo]" id="num_anticipoHiden">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="cod_procesoError">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="cod_proceso"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
    </div>
</form>

<script type="text/javascript">
$(document).ready(function() {
    var app = new  AppFunciones();
    $("#formAjax").submit(function(){
        return false;
    });
    $('#modalAncho').css( "width", "35%" );
    $('#fec_anticipo').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
    $('.select2').select2({ allowClear: true });
    $('#ind_porcentaje').change(function(){
        if($("#ind_porcentaje").val() <= 75) {
            var acumulado = parseFloat($("#num_monto_acumulado").val());
            var porcentaje = parseInt($("#ind_porcentaje").val()) / 100;
            $("#num_anticipo").val(acumulado * porcentaje);
            $("#num_anticipoHiden").val(acumulado * porcentaje);
        }else{
            swal("Error!", 'El porcentaje debe ser menor o igual a 75', "error");
        }
    });
    $('#accion').click(function(){
        swal({
            title: "¡Por favor espere!",
            text: "Se esta procesando su solicitud, puede demorar un poco.",
            timer: 50000000,
            showConfirmButton: false
        });
        $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
            var arrayCheck = [""];
            var arrayMostrarOrden = ['fec_anticipo','fk_a006_num_miscelaneo_detalle_justificativo','num_monto_acumulado','num_anticipo','num_total'];
            if(dato['status']=='error'){
                app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
            }else if(dato['status']=='modificar'){
                app.metActualizarRegistroTabla(dato,dato['idAnticipo'],'idAnticipo',arrayCheck,arrayMostrarOrden,'La aplicacion fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
            }else if(dato['status']=='nuevo'){
                app.metNuevoRegistroTabla(dato,dato['idAnticipo'],'idAnticipo',arrayCheck,arrayMostrarOrden,'La aplicacion fue guardado satisfactoriamente.','cerrarModal','ContenidoModal','conceptoAsignadoEmpleado');
            }
        },'json');
    });


});
</script>