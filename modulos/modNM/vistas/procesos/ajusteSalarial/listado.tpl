<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">
            {if $estatus == 'PR'}
                Ajuste Salarial - Conformar
            {elseif $estatus == 'CO' }
                Ajuste Salarial - Aprobar
            {else}
                Ajuste Salarial - Listado
            {/if}
        </h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Periodo</th>
                            <th>Descripcion</th>
                            <th>Nro. Resolución</th>
                            <th>Nro. Gaceta</th>
                            <th>Estatus</th>
                            <th>Accion</th>
                        </tr>
                        </thead>
                        {if $estatus == 'PR' || $estatus == 'CO' || $estatus == 'AP'}

                        {else}
                            <tfoot>
                            <tr>
                                <th colspan="6">
                                    {if in_array('NM-01-01-05-01-01-N',$_Parametros.perfil)}
                                        <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                                data-toggle="modal" data-target="#formModal" data-keyboard="false"
                                                data-backdrop="static"
                                                descipcion="el Usuario a Registrado un Ajuste Salarial"
                                                titulo="<i class='icm icm-coins'></i> Crear Ajuste Salarial" id="nuevo">
                                            <i class="md md-create"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nuevo Ajuste Salarial
                                        </button>
                                    {/if}
                                </th>
                            </tr>
                            </tfoot>
                        {/if}
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        var url = '{$_Parametros.url}modNM/procesos/ajusteSalarialCONTROL/crearModificarMET';
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modNM/procesos/ajusteSalarialCONTROL/jsonDataTablaMET/{$estatus}",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "fec" },
                    { "data": "ind_descripcion" },
                    { "data": "ind_numero_resolucion" },
                    { "data": "ind_numero_gaceta" },
                    { "data": "ind_estado" },
                    { "orderable": false,"data": "acciones",'width':100 }
                ]
        );
        $('#nuevo').click(function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idAjusteSalarial: 0 }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on('click', '.aprobar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idAjusteSalarial: $(this).attr('idAjusteSalarial'), tipo:'CO' }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on('click', '.conformar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idAjusteSalarial: $(this).attr('idAjusteSalarial'), tipo:'PR' }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on('click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idAjusteSalarial: $(this).attr('idAjusteSalarial') }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on('click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idAjusteSalarial: $(this).attr('idAjusteSalarial'), tipo:'VER' }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

    });
</script>