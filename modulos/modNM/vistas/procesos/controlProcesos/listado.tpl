<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">{if $status=='PR'} Aprobar Procesos {else} Control de Procesos {/if} - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover" url="{$_Parametros.url}modNM/procesos/controlProcesosCONTROL/iniciarModificarPeriodoMET">
                        <thead>
                        <tr>
                            <th>Tipo Nomina</th>
                            <th>Año</th>
                            <th>Mes</th>
                            <th>Tipo de Procesos</th>
                            <th>Estatus</th>
                            <th>Accion</th>
                        </tr>
                        </thead>
                        <!--tbody>
                        {foreach item=i from=$listado}
                            <tr id="idControlProceso{$i.pk_num_proceso_periodo}">
                                <td>{$i.ind_nombre_nomina}</td>
                                <td>{$i.fec_anio}</td>
                                <td>{$i.fec_mes}</td>
                                <td>{$i.ind_nombre_proceso}</td>
                                <td>
                                        {$i.ind_estado}
                                </td>
                                <td align="center">
                                    {if !$status}
                                        {if $i.ind_estado=='PR'}
                                            {if in_array('NM-01-01-03-02-M',$_Parametros.perfil)}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" idControlProceso="{$i.pk_num_proceso_periodo}" title="Editar"
                                                        descipcion="El Usuario a Modificado el Control de Procesos" titulo="Modificar Control de Procesos">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        {/if}
                                        {if $i.ind_estado=='GE'}
                                            {if in_array('NM-01-01-03-04-C',$_Parametros.perfil)}
                                                <button class="cerrar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" idControlProceso="{$i.pk_num_proceso_periodo}" title="Cerrar Proceso"
                                                        descipcion="El Usuario a Modificado el Control de Procesos" titulo="Cerrar Control de Procesos">
                                                    <i class="md md-timer-off" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        {/if}
                                    {elseif $status=='PR'}
                                        {if in_array('NM-01-01-03-01-AP',$_Parametros.perfil)}
                                            <button class="aprobar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idControlProceso="{$i.pk_num_proceso_periodo}" title="Aprobar Proceso"
                                                    descipcion="El Usuario a Aprobado un Control de Procesos" titulo="Aprobar Control de Procesos">
                                                <i class="icm icm-rating3" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    {/if}
                                    {if in_array('NM-01-01-03-03-V',$_Parametros.perfil)}
                                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idControlProceso="{$i.pk_num_proceso_periodo}" title="Ver Proceso"
                                                descipcion="El Usuario esta viendo un Control de Procesos" titulo="<i class='icm icm-calculate2'></i> Ver Control de Procesos">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                        </tbody-->
                        {if !$status}
                        <tfoot>
                            <tr>
                                <th colspan="6">
                                    {if in_array('NM-01-01-03-01-N',$_Parametros.perfil)}
                                        <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                                data-toggle="modal" data-target="#formModal" data-keyboard="false"
                                                data-backdrop="static"
                                                descipcion="el Usuario a Registrado un Sueldo Minimo Nuevo"
                                                titulo="<i class='md md-event-available'></i> Iniciar Periodo" id="nuevo">
                                            <i class="md md-create"></i> Iniciar Nuevo Periodo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </button>
                                    {/if}
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                    {/if}
                </div>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">
    $(document).ready(function () {
        var url=$('#dataTablaJson').attr('url');
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modNM/procesos/controlProcesosCONTROL/jsonDataTablaMET/{$status}",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "ind_nombre_nomina" },
                    { "data": "fec_anio" },
                    { "data": "fec_mes" },
                    { "data": "ind_nombre_proceso" },
                    { "data": "ind_estado" },
                    { "orderable": false,"data": "acciones",'width':100 }
                ]
        );
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idControlProceso:0, status:'pr' },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idControlProceso:$(this).attr('idControlProceso'), status:'pr' },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idControlProceso:$(this).attr('idControlProceso'), ver:1, status:'pr' },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.aprobar', function () {
            console.log('prueba');
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idControlProceso:$(this).attr('idControlProceso'), ver:1, status:'AP' },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.cerrar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idControlProceso:$(this).attr('idControlProceso'), ver:1, status:'CR' },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
    });
</script>