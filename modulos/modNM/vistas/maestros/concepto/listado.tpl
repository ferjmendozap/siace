<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Conceptos - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Cod.</th>
                                <th>Concepto</th>
                                <th>Tipo</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th colspan="3">
                                {if in_array('NM-01-03-03-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario a creado un Concepto"  titulo="<i class='icm icm-calculate2'></i> Crear Concepto" id="nuevo" >
                                        <i class="md md-create"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nuevo Concepto
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var url='{$_Parametros.url}modNM/maestros/conceptoCONTROL/crearModificarMET';
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modNM/maestros/conceptoCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "cod_concepto" },
                    { "data": "ind_descripcion" },
                    { "data": "ind_nombre_detalle" },
                    { "orderable": false,"data": "num_estatus",'width':30 },
                    { "orderable": false,"data": "acciones",'width':100 }
                ]
        );
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idConcepto:0 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post(url,{ idConcepto: $(this).attr('idConcepto') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });

        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modNM/maestros/conceptoCONTROL/verMET',{ idConcepto: $(this).attr('idConcepto') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });

    });
</script>