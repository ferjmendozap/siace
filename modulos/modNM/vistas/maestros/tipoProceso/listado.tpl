<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Tipo de Procesos - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Cod</th>
                                <th>Procesos</th>
                                <th>Adelanto</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th colspan="5">
                                    {if in_array('NM-01-03-01-01-N',$_Parametros.perfil)}
                                        <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                descipcion="el Usuario a creado un Nuevo Proceso de Nomina"  titulo="<i class='icm icm-cog3'></i> Crear Proceso" id="nuevo" >
                                            <i class="md md-create"></i> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nuevo Proceso
                                        </button>
                                    {/if}
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modNM/maestros/tipoProcesoCONTROL/crearModificarMET';
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modNM/maestros/tipoProcesoCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "cod_proceso" },
                    { "data": "ind_nombre_proceso" },
                    { "orderable": false,"data": "num_flag_adelanto",'width':30 },
                    { "orderable": false,"data": "num_estatus",'width':30 },
                    { "orderable": false,"data": "acciones",'width':100 }
                ]
        );

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idProceso:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idProceso: $(this).attr('idProceso')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        
        $('#dataTablaJson tbody').on( 'click', '.eliminar', function () {
            var idProceso=$(this).attr('idProceso');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modNM/maestros/tipoProcesoCONTROL/eliminarMET';
                $.post($url, { idProceso: idProceso },function(dato){
                    if(dato['status']=='ok'){
                        app.metEliminarRegistroJson('dataTablaJson','"El Proceso fue eliminado satisfactoriamente.','cerrarModal','ContenidoModal');
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>