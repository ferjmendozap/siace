<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Tipo de Procesos - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Check</th>
                            <th>Cod</th>
                            <th>Procesos</th>
                            <th>Adelanto</th>
                            <th>Estatus</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=proceso from=$listado}
                            <tr id="idProceso{$proceso.pk_num_tipo_proceso}">
                                <td>
                                    <div class="checkbox checkbox-styled">
                                        <label>
                                            <input type="checkbox" class="valores"
                                                   idProceso="{$proceso.pk_num_tipo_proceso}"
                                                   proceso="{$proceso.ind_nombre_proceso}" cod="{$proceso.cod_proceso}">
                                        </label>
                                    </div>
                                </td>
                                <td><label>{$proceso.cod_proceso}</label></td>
                                <td><label>{$proceso.ind_nombre_proceso}</label></td>
                                <td>
                                    <i class="{if $proceso.num_flag_adelanto==1}md md-check{else}md md-not-interested{/if}"></i>
                                </td>
                                <td>
                                    <i class="{if $proceso.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<span class="clearfix"></span>
<div class="modal-footer">
    <button type="button" class="btn btn-default ink-reaction btn-raised" data-dismiss="modal">Cancelar</button>
    <button type="button" class="btn btn-primary ink-reaction btn-raised" id="agregarProcesoSeleccionado">Agregar
    </button>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#agregarProcesoSeleccionado').click(function () {
            var input = $('.valores');
            for (i = 0; i < input.length; i++) {
                if (input[i].checked == true) {
                    $(document.getElementsByClassName('idProceso' + input[i].getAttribute('idProceso'))).remove();
                    $(document.getElementById('tipoProcesoNomina')).append(
                            '<tr class="idProceso' + input[i].getAttribute('idProceso') + '">' +
                            '<input type="hidden" value="' + input[i].getAttribute('idProceso') + '" name="form[int][fk_nmb003_num_tipo_proceso][]" class="tipoProcesoInput" proceso="' + input[i].getAttribute('proceso') + '" />' +
                            '<td>' + input[i].getAttribute('cod') + '</td>' +
                            '<td>' + input[i].getAttribute('proceso') + '</td>' +
                            '<td><button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idProceso' + input[i].getAttribute('idProceso') + '"><i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
                            '</tr>'
                    );
                }
            }
            $('#ContenidoModal2').html('');
            $('#cerrarModal2').click();
        });
    });
</script>