<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        18-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class perfilConceptoModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metMostrarPerfilConcepto($idTipoNomina)
    {
        $perfilconcepto = $this->_db->query(
            "SELECT
                nm_d001_concepto_perfil_detalle.*,
                a006_miscelaneo_detalle.ind_nombre_detalle,
                nm_b003_tipo_proceso.ind_nombre_proceso,
                nm_b001_tipo_nomina.ind_nombre_nomina,
                nm_b001_tipo_nomina.ind_titulo_boleta,
                nm_b002_concepto.*
            FROM
                nm_d001_concepto_perfil_detalle
                INNER JOIN nm_b001_tipo_nomina ON nm_b001_tipo_nomina.pk_num_tipo_nomina = nm_d001_concepto_perfil_detalle.fk_nmb001_num_tipo_nomina
                INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso
                INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d001_concepto_perfil_detalle.fk_nmb002_num_concepto
                INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
            WHERE
                nm_d001_concepto_perfil_detalle.fk_nmb001_num_tipo_nomina = '$idTipoNomina'
            ORDER by
                a006_miscelaneo_detalle.ind_nombre_detalle, nm_b003_tipo_proceso.ind_nombre_proceso, nm_b002_concepto.cod_concepto ASC
          "
        );
        $perfilconcepto->setFetchMode(PDO::FETCH_ASSOC);
        return $perfilconcepto->fetchAll();
    }

    public function metModificarPerfilConcepto($idNomina,$idProcesos,$idConceptos,$partidas,$debes,$haberes,$debesPub20,$haberesPub20)
    {

        $this->_db->beginTransaction();
        $modificar=$this->_db->prepare("
                  UPDATE
                    nm_d001_concepto_perfil_detalle
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), ind_cod_partida=:ind_cod_partida,
                    ind_cod_cuenta_contable_debe=:ind_cod_cuenta_contable_debe, ind_cod_cuenta_contable_haber=:ind_cod_cuenta_contable_haber,
                    ind_cod_cuenta_contable_debe_pub20=:ind_cod_cuenta_contable_debe_pub20 ,ind_cod_cuenta_contable_haber_pub20=:ind_cod_cuenta_contable_haber_pub20
                  WHERE
                    fk_nmb001_num_tipo_nomina='$idNomina' AND fk_nmb002_num_concepto=:fk_nmb002_num_concepto AND fk_nmb003_num_tipo_proceso=:fk_nmb003_num_tipo_proceso
        ");

        for($i=0;$i<count($idProcesos);$i++){
            $arrayIdConcepto=$idConceptos[$idNomina.$idProcesos[$i]];
            for($ii=0;$ii<count($arrayIdConcepto);$ii++){
                if($arrayIdConcepto[$ii]=='error'){
                    $arrayIdConcepto[$ii]=null;
                }
                if($debes[$idNomina.$idProcesos[$i].$arrayIdConcepto[$ii]]=='error' || $debes[$idNomina.$idProcesos[$i].$arrayIdConcepto[$ii]]==''){
                    $debes[$idNomina.$idProcesos[$i].$arrayIdConcepto[$ii]]=null;
                }
                if($debesPub20[$idNomina.$idProcesos[$i].$arrayIdConcepto[$ii]]=='error' || $debesPub20[$idNomina.$idProcesos[$i].$arrayIdConcepto[$ii]]==''){
                    $debesPub20[$idNomina.$idProcesos[$i].$arrayIdConcepto[$ii]]=null;
                }
                if($haberes[$idNomina.$idProcesos[$i].$arrayIdConcepto[$ii]]=='error' || $haberes[$idNomina.$idProcesos[$i].$arrayIdConcepto[$ii]]==''){
                    $haberes[$idNomina.$idProcesos[$i].$arrayIdConcepto[$ii]]=null;
                }
                if($haberesPub20[$idNomina.$idProcesos[$i].$arrayIdConcepto[$ii]]=='error' || $haberesPub20[$idNomina.$idProcesos[$i].$arrayIdConcepto[$ii]]==''){
                    $haberesPub20[$idNomina.$idProcesos[$i].$arrayIdConcepto[$ii]]=null;
                }
                if($partidas[$idNomina.$idProcesos[$i].$arrayIdConcepto[$ii]]=='error' || $partidas[$idNomina.$idProcesos[$i].$arrayIdConcepto[$ii]]==''){
                    $partidas[$idNomina.$idProcesos[$i].$arrayIdConcepto[$ii]]=null;
                }
                $modificar->execute(array(
                    'fk_nmb002_num_concepto'=>$arrayIdConcepto[$ii],
                    'fk_nmb003_num_tipo_proceso'=>$idProcesos[$i],
                    'ind_cod_partida'=>$partidas[$idNomina.$idProcesos[$i].$arrayIdConcepto[$ii]],
                    'ind_cod_cuenta_contable_debe'=>$debes[$idNomina.$idProcesos[$i].$arrayIdConcepto[$ii]],
                    'ind_cod_cuenta_contable_haber'=>$haberes[$idNomina.$idProcesos[$i].$arrayIdConcepto[$ii]],
                    'ind_cod_cuenta_contable_debe_pub20'=>$debesPub20[$idNomina.$idProcesos[$i].$arrayIdConcepto[$ii]],
                    'ind_cod_cuenta_contable_haber_pub20'=>$haberesPub20[$idNomina.$idProcesos[$i].$arrayIdConcepto[$ii]]
                ));
            }
        }
        $error = $modificar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idNomina;
        }

    }



}
