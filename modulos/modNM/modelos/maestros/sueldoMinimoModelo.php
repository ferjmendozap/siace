<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Mu�oz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        18-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class sueldoMinimoModelo extends Modelo
{
    private $atIdUsuario;
    private $atIdEmpleado;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
    }

    public function metListarSueldoMinimo($estatus)
    {
        if($estatus){
            $sql = " WHERE ind_estado='$estatus'";
        }else{
            $sql ="";
        }
        $sueldoMinimo = $this->_db->query(
            "SELECT * FROM nm_b006_sueldo_minimo $sql"
        );
        $sueldoMinimo->setFetchMode(PDO::FETCH_ASSOC);
        return $sueldoMinimo->fetchAll();
    }

    public function metBuscarSueldoMinimo($idSueldominimo)
    {
        $sueldoMinimo = $this->_db->query(
            "SELECT
                nm_b006_sueldo_minimo.*,
                a018_seguridad_usuario.ind_usuario,
                (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN nm_b006_sueldo_minimo ON rh_b001_empleado.pk_num_empleado = nm_b006_sueldo_minimo.fk_rhb001_num_empleado_preparado
                    WHERE
                        pk_num_sueldo_minimo = '$idSueldominimo'
                ) AS EMPLEADO_PREPARADO,
                (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN nm_b006_sueldo_minimo ON rh_b001_empleado.pk_num_empleado = nm_b006_sueldo_minimo.fk_rhb001_num_empleado_aprobado
                    WHERE
                        pk_num_sueldo_minimo = '$idSueldominimo'
                ) AS EMPLEADO_APROBADO,
                (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN nm_b006_sueldo_minimo ON rh_b001_empleado.pk_num_empleado = nm_b006_sueldo_minimo.fk_rhb001_num_empleado_anulado
                    WHERE
                        pk_num_sueldo_minimo = '$idSueldominimo'
                ) AS EMPLEADO_ANULADO
            FROM
                nm_b006_sueldo_minimo
                INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = nm_b006_sueldo_minimo.fk_a018_num_seguridad_usuario
            WHERE pk_num_sueldo_minimo='$idSueldominimo'"
        );
        $sueldoMinimo->setFetchMode(PDO::FETCH_ASSOC);
        return $sueldoMinimo->fetch();
    }

    public function metCrearSueldoMinimo($descripcion, $numGaceta, $numResolucion, $periodo, $sueldoMinimo)
    {
        $this->_db->beginTransaction();
        $registrarSueldominimo = $this->_db->prepare("
                  INSERT INTO
                    nm_b006_sueldo_minimo
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), fec_preparado=NOW(),
                    fk_rhb001_num_empleado_preparado='$this->atIdEmpleado', ind_descripcion=:ind_descripcion,ind_estado='PR',ind_num_gaceta=:ind_num_gaceta,
                    ind_num_resolucion=:ind_num_resolucion,ind_periodo=:ind_periodo,num_sueldo_minimo=:num_sueldo_minimo
        ");
        $registrarSueldominimo->execute(array(
            'ind_descripcion' => $descripcion,
            'ind_num_gaceta' => $numGaceta,
            'ind_num_resolucion' => $numResolucion,
            'ind_periodo' => $periodo,
            'num_sueldo_minimo' => $sueldoMinimo
        ));
        $idRegistro = $this->_db->lastInsertId();
        $error = $registrarSueldominimo->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarSueldoMinimo($idSueldominimo, $descripcion, $numGaceta, $numResolucion, $periodo, $sueldoMinimo)
    {
        $this->_db->beginTransaction();
        $registrarSueldominimo = $this->_db->prepare("
                  UPDATE
                    nm_b006_sueldo_minimo
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    ind_descripcion=:ind_descripcion,ind_num_gaceta=:ind_num_gaceta, ind_num_resolucion=:ind_num_resolucion,
                    ind_periodo=:ind_periodo,num_sueldo_minimo=:num_sueldo_minimo
                  WHERE
                    pk_num_sueldo_minimo='$idSueldominimo'
        ");
        $registrarSueldominimo->execute(array(
            'ind_descripcion' => $descripcion,
            'ind_num_gaceta' => $numGaceta,
            'ind_num_resolucion' => $numResolucion,
            'ind_periodo' => $periodo,
            'num_sueldo_minimo' => $sueldoMinimo,
        ));
        $error = $registrarSueldominimo->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idSueldominimo;
        }
    }

    public function metAprobarSueldominimo($idSueldominimo,$estatus)
    {
        $this->_db->beginTransaction();

        $registrarSueldominimo = $this->_db->prepare("
                  UPDATE
                    nm_b006_sueldo_minimo
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    ind_estado=:ind_estado, fec_aprobado=NOW(), fk_rhb001_num_empleado_aprobado='$this->atIdEmpleado'
                  WHERE
                    pk_num_sueldo_minimo='$idSueldominimo'
        ");
        $registrarSueldominimo->execute(array(
            'ind_estado' => $estatus
        ));
        $error = $registrarSueldominimo->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idSueldominimo;
        }
    }

    public function metAnularSueldominimo($idSueldominimo,$motivo)
    {
        $this->_db->beginTransaction();

        $registrarSueldominimo = $this->_db->prepare("
                  UPDATE
                    nm_b006_sueldo_minimo
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    ind_estado='AN', fec_anulado=NOW(), fk_rhb001_num_empleado_anulado='$this->atIdEmpleado',
                    ind_motivo_anulado=:ind_motivo_anulado
                  WHERE
                    pk_num_sueldo_minimo='$idSueldominimo'
        ");
        $registrarSueldominimo->execute(array(
            'ind_motivo_anulado'=>$motivo
        ));
        $error = $registrarSueldominimo->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idSueldominimo;
        }
    }

}
