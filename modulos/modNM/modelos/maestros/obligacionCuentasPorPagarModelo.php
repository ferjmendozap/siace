<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Mu�oz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        18-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
class obligacionCuentasPorPagarModelo extends miscelaneoModelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metListaPersona()
    {
        $persona = $this->_db->query(
            "SELECT * FROM a003_persona WHERE num_estatus=1"
        );
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetchAll();
    }

    public function metListarObligacionesCxp()
    {
        $obligacionesCxp = $this->_db->query("
            SELECT
              nm_b005_interfaz_cxp.*,
              cp_b002_tipo_documento.*,
              tipoObligacion.*,
              tipoPago.*,
              proveedor.ind_nombre1,
              proveedor.ind_nombre2,
              proveedor.ind_apellido1,
              proveedor.ind_apellido2,
              pagar.ind_nombre1 AS ind_nombre11,
              pagar.ind_nombre2 AS ind_nombre22,
              pagar.ind_apellido1 AS ind_apellido11,
              pagar.ind_apellido2 AS ind_apellido22
            FROM
              nm_b005_interfaz_cxp
            INNER JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento=nm_b005_interfaz_cxp.fk_cpb002_num_tipo_documento
            INNER JOIN a006_miscelaneo_detalle AS tipoObligacion ON tipoObligacion.pk_num_miscelaneo_detalle=nm_b005_interfaz_cxp.fk_a006_num_miscelaneo_det_tipo_obligacion
            INNER JOIN a006_miscelaneo_detalle AS tipoPago ON tipoPago.pk_num_miscelaneo_detalle=nm_b005_interfaz_cxp.fk_a006_num_miscelaneo_det_tipo_pago
            INNER JOIN a003_persona proveedor ON proveedor.pk_num_persona=nm_b005_interfaz_cxp.fk_a003_num_persona_proveedor
            INNER JOIN a003_persona pagar ON pagar.pk_num_persona=nm_b005_interfaz_cxp.fk_a003_num_persona_pagar
        ");
        $obligacionesCxp->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionesCxp->fetchAll();
    }

    public function metConsultarObligacionesCxp($idObligacionCxP)
    {
        $obligacionesCxp = $this->_db->query("
            SELECT
              nm_b005_interfaz_cxp.*,
              proveedor.ind_nombre1,
              proveedor.ind_nombre2,
              proveedor.ind_apellido1,
              proveedor.ind_apellido2,
              proveedor.ind_documento_fiscal,
              pagar.ind_nombre1 AS ind_nombre11,
              pagar.ind_nombre2 AS ind_nombre22,
              pagar.ind_apellido1 AS ind_apellido11,
              pagar.ind_documento_fiscal AS ind_documento_fiscal11,
              pagar.ind_apellido2 AS ind_apellido22
            FROM
              nm_b005_interfaz_cxp
            INNER JOIN a003_persona proveedor ON proveedor.pk_num_persona=nm_b005_interfaz_cxp.fk_a003_num_persona_proveedor
            INNER JOIN a003_persona pagar ON pagar.pk_num_persona=nm_b005_interfaz_cxp.fk_a003_num_persona_pagar

            WHERE
              pk_num_interfaz_cxp='$idObligacionCxP'
        ");
        $obligacionesCxp->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionesCxp->fetch();
    }

    public function metConsultarObligacionesCxpPartida($idObligacionCxP)
    {
        $obligacionesCxp = $this->_db->query("
            SELECT
              *
            FROM
              nm_c004_interfas_cxp_det
            INNER JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria=nm_c004_interfas_cxp_det.fk_prb002_num_partida_presupuestaria
            WHERE
              fk_nmb005_num_interfaz_cxp='$idObligacionCxP'
        ");
        $obligacionesCxp->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionesCxp->fetchAll();
    }

    public function metConsultarObligacionesCxpCuenta($idObligacionCxP)
    {
        $obligacionesCxp = $this->_db->query("
            SELECT
              *
            FROM
              nm_c004_interfas_cxp_det
            INNER JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta=nm_c004_interfas_cxp_det.fk_000_num_cuenta_contable
            WHERE
              fk_nmb005_num_interfaz_cxp='$idObligacionCxP'
        ");
        $obligacionesCxp->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionesCxp->fetchAll();
    }

    public function metListarCuentas($cuenta)
    {
        if($cuenta == 'cuentaPub20') {
            $where = "AND	num_flag_tipo_cuenta='1'";
        }else {
            $where = "AND	num_flag_tipo_cuenta='2'";
        }
        $obligacionesCxp = $this->_db->query("
            SELECT
              pk_num_cuenta AS pk_num_partida_presupuestaria,
              cod_cuenta AS cod_partida,
              ind_descripcion AS ind_denominacion
            FROM
              cb_b004_plan_cuenta
            WHERE
              cb_b004_plan_cuenta.num_estatus='1'
              $where
        ");
        $obligacionesCxp->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionesCxp->fetchAll();
    }

    public function metNuevaObligacionCxP($form)
    {
        $this->_db->beginTransaction();
        $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    nm_b005_interfaz_cxp
                  SET
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridadusuario='$this->atIdUsuario',
                    fk_a003_num_persona_pagar=:fk_a003_num_persona_pagar,
                    fk_a003_num_persona_proveedor=:fk_a003_num_persona_proveedor,
                    fk_a006_num_miscelaneo_det_tipo_obligacion=:fk_a006_num_miscelaneo_det_tipo_obligacion,
                    fk_a006_num_miscelaneo_det_tipo_pago=:fk_a006_num_miscelaneo_det_tipo_pago,
                    fk_cpb002_num_tipo_documento=:fk_cpb002_num_tipo_documento,
                    fk_000_tipo_servicio=:fk_000_tipo_servicio,
                    fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                    ind_comentario=:ind_comentario,
                    ind_num_cuenta=:ind_num_cuenta,
                    ind_titulo=:ind_titulo,
                    ind_num_factura=:ind_num_factura,
                    num_flag_prq=:num_flag_prq,
                    num_monto=:num_monto
        ");
        $nuevoRegistro->execute(array(
            'fk_a003_num_persona_pagar'=>$form['fk_a003_num_persona_pagar'],
            'fk_a003_num_persona_proveedor'=>$form['fk_a003_num_persona_proveedor'],
            'fk_a006_num_miscelaneo_det_tipo_obligacion'=>$form['fk_a006_num_miscelaneo_det_tipo_obligacion'],
            'fk_a006_num_miscelaneo_det_tipo_pago'=>$form['fk_a006_num_miscelaneo_det_tipo_pago'],
            'fk_cpb002_num_tipo_documento'=>$form['fk_cpb002_num_tipo_documento'],
            'fk_000_tipo_servicio'=>$form['fk_000_tipo_servicio'],
            'fk_rhb001_num_empleado'=>$form['fk_rhb001_num_empleado'],
            'ind_comentario'=>$form['ind_comentario'],
            'ind_num_cuenta'=>$form['ind_num_cuenta'],
            'ind_titulo'=>$form['ind_titulo'],
            'ind_num_factura'=>$form['ind_num_factura'],
            'num_flag_prq'=>$form['num_flag_prq'],
            'num_monto'=>$form['num_monto'],
        ));
        $idRegistro= $this->_db->lastInsertId();

        $det=$this->_db->prepare("
                  INSERT INTO
                    nm_c004_interfas_cxp_det
                  SET
                    fk_nmb005_num_interfaz_cxp='$idRegistro',
                    fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                    fk_000_num_cuenta_contable=:fk_000_num_cuenta_contable
        ");
        $partidaCuenta=$form['cod'];
        $tipo=$form['tipo'];
        for($i=0;$i<count($partidaCuenta);$i++){
            if($tipo[$i]=='P'){
                $partida=$partidaCuenta[$i];
                $cuenta=null;
            }else{
                $partida=null;
                $cuenta=$partidaCuenta[$i];
            }
            $det->execute(array(
                'fk_prb002_num_partida_presupuestaria'=>$partida,
                'fk_000_num_cuenta_contable'=>$cuenta
            ));
        }

        $error = $nuevoRegistro->errorInfo();
        $error1 = $det->errorInfo();

        if(
            (!empty($error[1]) && !empty($error[2])) ||
            (!empty($error1[1]) && !empty($error1[2]))
          ){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarObligacionCxP($idObligacionCxP,$form)
    {
        $this->_db->beginTransaction();
        $nuevoRegistro=$this->_db->prepare("
                  UPDATE
                    nm_b005_interfaz_cxp
                  SET
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridadusuario='$this->atIdUsuario',
                    fk_a003_num_persona_pagar=:fk_a003_num_persona_pagar,
                    fk_a003_num_persona_proveedor=:fk_a003_num_persona_proveedor,
                    fk_a006_num_miscelaneo_det_tipo_obligacion=:fk_a006_num_miscelaneo_det_tipo_obligacion,
                    fk_a006_num_miscelaneo_det_tipo_pago=:fk_a006_num_miscelaneo_det_tipo_pago,
                    fk_cpb002_num_tipo_documento=:fk_cpb002_num_tipo_documento,
                    fk_000_tipo_servicio=:fk_000_tipo_servicio,
                    fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                    ind_comentario=:ind_comentario,
                    ind_num_cuenta=:ind_num_cuenta,
                    ind_titulo=:ind_titulo,
                    ind_num_factura=:ind_num_factura,
                    num_monto=:num_monto,
                    num_flag_prq=:num_flag_prq
                  WHERE
                    pk_num_interfaz_cxp='$idObligacionCxP'
        ");
        $nuevoRegistro->execute(array(
            'fk_a003_num_persona_pagar'=>$form['fk_a003_num_persona_pagar'],
            'fk_a003_num_persona_proveedor'=>$form['fk_a003_num_persona_proveedor'],
            'fk_a006_num_miscelaneo_det_tipo_obligacion'=>$form['fk_a006_num_miscelaneo_det_tipo_obligacion'],
            'fk_a006_num_miscelaneo_det_tipo_pago'=>$form['fk_a006_num_miscelaneo_det_tipo_pago'],
            'fk_cpb002_num_tipo_documento'=>$form['fk_cpb002_num_tipo_documento'],
            'fk_000_tipo_servicio'=>$form['fk_000_tipo_servicio'],
            'fk_rhb001_num_empleado'=>$form['fk_rhb001_num_empleado'],
            'ind_comentario'=>$form['ind_comentario'],
            'ind_num_cuenta'=>$form['ind_num_cuenta'],
            'ind_titulo'=>$form['ind_titulo'],
            'ind_num_factura'=>$form['ind_num_factura'],
            'num_flag_prq'=>$form['num_flag_prq'],
            'num_monto'=>$form['num_monto'],
        ));

        $eliminarDet = $this->_db->query(
            "DELETE FROM nm_c004_interfas_cxp_det WHERE fk_nmb005_num_interfaz_cxp='$idObligacionCxP'"
        );

        $det=$this->_db->prepare("
                  INSERT INTO
                    nm_c004_interfas_cxp_det
                  SET
                    fk_nmb005_num_interfaz_cxp='$idObligacionCxP',
                    fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                    fk_000_num_cuenta_contable=:fk_000_num_cuenta_contable
        ");
        if(isset($form['cod'])) {
            $partidaCuenta = $form['cod'];
            $tipo = $form['tipo'];
            for ($i = 0; $i < count($partidaCuenta); $i++) {
                if ($tipo[$i] == 'P') {
                    $partida = $partidaCuenta[$i];
                    $cuenta = null;
                } else {
                    $partida = null;
                    $cuenta = $partidaCuenta[$i];
                }
                $det->execute(array(
                    'fk_prb002_num_partida_presupuestaria' => $partida,
                    'fk_000_num_cuenta_contable' => $cuenta
                ));
            }
            $error1 = $det->errorInfo();
        }else{
            $error1 = array(1 => null, 2 => null);
        }

        $error = $nuevoRegistro->errorInfo();

        if(
            (!empty($error[1]) && !empty($error[2])) ||
            (!empty($error1[1]) && !empty($error1[2]))
          ){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idObligacionCxP;
        }
    }


}
