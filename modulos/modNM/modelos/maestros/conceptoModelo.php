<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        18-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once RUTA_MODELO . 'miscelaneoModelo.php';

class conceptoModelo extends miscelaneoModelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metMostrarConcepto($idConcepto)
    {
        $concepto = $this->_db->query(
            "SELECT * FROM nm_b002_concepto WHERE pk_num_concepto='$idConcepto'"
        );
        $concepto->setFetchMode(PDO::FETCH_ASSOC);
        return $concepto->fetch();
    }

    public function metMostrarConceptoDetalle($idConcepto)
    {
        $concepto = $this->_db->query(
            "SELECT
              *
            FROM
              nm_d001_concepto_perfil_detalle
            INNER JOIN nm_b001_tipo_nomina ON nm_b001_tipo_nomina.pk_num_tipo_nomina = nm_d001_concepto_perfil_detalle.fk_nmb001_num_tipo_nomina
            INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso

            WHERE fk_nmb002_num_concepto='$idConcepto'
            ORDER by fk_nmb001_num_tipo_nomina ASC
          "
        );
        $concepto->setFetchMode(PDO::FETCH_ASSOC);
        return $concepto->fetchAll();
    }

    public function metListarConcepto($status = false)
    {
        if ($status) {
            $status = 'WHERE nm_b002.num_estatus=1';
        }
        $concepto = $this->_db->query("
            SELECT
              nm_b002.*,
              a006.ind_nombre_detalle,
              a006.cod_detalle
            FROM
              nm_b002_concepto nm_b002
            INNER JOIN
              a006_miscelaneo_detalle a006 ON a006.pk_num_miscelaneo_detalle = nm_b002.fk_a006_num_tipo_concepto
            $status
      ");
        $concepto->setFetchMode(PDO::FETCH_ASSOC);
        return $concepto->fetchAll();
    }


    public function metUltimoConcepto()
    {
        $concepto = $this->_db->query("
            SELECT MAX(cod_concepto) as cod_concepto from nm_b002_concepto
       ");
        $concepto->setFetchMode(PDO::FETCH_ASSOC);
        return $concepto->fetch();

    }

    public function metCrearConcepto(
        $formula = null, $abreviatura, $nombre, $titulo, $tipoConcepto,
        $orden, $nominas = false, $procesos = false, $partida = false, $debe = false, $haber = false,
        $debepub20 = false, $haberPub20 = false, $flagAutomatico, $flagBonificacion, $flagIncidencia, $status,$catces,$reporte
    )
    {
        $this->_db->beginTransaction();
        /*$codigo = count($this->metListarConcepto()) + 1;
        $mun = "0";
        for ($i = 0; $i < (3 - strlen($codigo)); $i++) {
            $mun .= "0";
        }
        $codigo = $mun . $codigo;*/
        $cod = $this->metUltimoConcepto();
        $codigo = $cod['cod_concepto'] + 1;

        $registroConcepto = $this->_db->prepare("
                      INSERT INTO
                        nm_b002_concepto
                      SET
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_moficacion=NOW(), cod_concepto=:cod_concepto,
                        ind_abrebiatura=:ind_abrebiatura, ind_descripcion=:ind_descripcion, num_estatus=:num_estatus,
                        ind_formula=:ind_formula, ind_impresion=:ind_impresion, num_flag_automatico=:num_flag_automatico,
                        num_flag_bono=:num_flag_bono, num_flag_incidencia=:num_flag_incidencia, num_orden_planilla=:num_orden_planilla,
                        fk_a006_num_tipo_concepto=:fk_a006_num_tipo_concepto,ind_reporte=:ind_reporte, num_flag_catces=:num_flag_catces
            ");
        $registroConcepto->execute(array(
            'cod_concepto' => $codigo,
            'ind_abrebiatura' => mb_strtoupper($abreviatura, 'utf-8'),
	    'ind_reporte' => mb_strtoupper($reporte, 'utf-8'),
            'num_estatus' => $status,
            'ind_descripcion' => mb_strtoupper($nombre, 'utf-8'),
            'ind_formula' => $formula,
            'ind_impresion' => $titulo,
            'num_flag_automatico' => $flagAutomatico,
            'num_flag_bono' => $flagBonificacion,
            'num_flag_incidencia' => $flagIncidencia,
            'num_orden_planilla' => $orden,
            'fk_a006_num_tipo_concepto' => $tipoConcepto,
            'num_flag_catces' => $catces

        ));
        $idConcepto = $this->_db->lastInsertId();

        if ($nominas && $procesos) {
            $registroPerfilDetalle = $this->_db->prepare("
                      INSERT INTO
                        nm_d001_concepto_perfil_detalle
                      SET
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                        fk_nmb003_num_tipo_proceso=:fk_nmb003_num_tipo_proceso, fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                        fk_nmb002_num_concepto='$idConcepto',ind_cod_partida=:ind_cod_partida, ind_cod_cuenta_contable_debe=:ind_cod_cuenta_contable_debe,
                        ind_cod_cuenta_contable_haber=:ind_cod_cuenta_contable_haber, ind_cod_cuenta_contable_debe_pub20=:ind_cod_cuenta_contable_debe_pub20,
                        ind_cod_cuenta_contable_haber_pub20=:ind_cod_cuenta_contable_haber_pub20
                ");
            for ($i = 0; $i < count($nominas); $i++) {
                for ($ii = 0; $ii < count($procesos); $ii++) {
                        $id = $nominas[$i] . $procesos[$ii];
                        if (isset($haber[$id])) {
                            $haberInsert = $haber[$id];
                        } else {
                            $haberInsert = null;
                        }
                        if (isset($debe[$id]) && ($debe[$id]!='')) {
                            $debeInsert = $debe[$id];
                        } else {
                            $debeInsert = null;
                        }
                        if (isset($partida[$id]) && ($partida[$id]!='')) {
                            $partidaInsert = $partida[$id];
                        } else {
                            $partidaInsert = null;
                        }


                        if (isset($debepub20[$id]) && ($debepub20[$id]!='')) {
                            $debepub20Insert = $debepub20[$id];
                        } else {
                            $debepub20Insert = null;
                        }

                        if (isset($haberPub20[$id]) && ($haberPub20[$id]!='')) {
                            $haberPub20Insert = $haberPub20[$id];
                        } else {
                            $haberPub20Insert = null;
                        }

                        $registroPerfilDetalle->execute(array(
                            'fk_nmb003_num_tipo_proceso' => $procesos[$ii],
                            'fk_nmb001_num_tipo_nomina' => $nominas[$i],
                            'ind_cod_partida' => $partidaInsert,
                            'ind_cod_cuenta_contable_debe' => $debeInsert,
                            'ind_cod_cuenta_contable_haber' => $haberInsert,
                            'ind_cod_cuenta_contable_debe_pub20' => $debepub20Insert,
                            'ind_cod_cuenta_contable_haber_pub20' => $haberPub20Insert
                        ));
                }
            }
        }
        $error = $registroConcepto->errorInfo();
        if (isset($registroPerfilDetalle)) {
            $error2 = $registroPerfilDetalle->errorInfo();
        } else {
            $error2 = array(1 => null, 2 => null);
        }

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } elseif(!empty($error2[1]) && !empty($error2[2])) {
            $this->_db->rollBack();
            return $error2;
        } else {
            $this->_db->commit();
            return $idConcepto;
        }

    }

    public function metModificarConcepto(
        $formula = null, $abreviatura, $nombre, $titulo, $tipoConcepto,
        $orden, $nominas, $procesos, $partida = false, $debe = false, $haber = false,
        $debepub20 = false, $haberPub20 = false, $flagAutomatico, $flagBonificacion, $flagIncidencia, $status, $catces, $reporte,$idConcepto
    )
    {
        $this->_db->beginTransaction();
        $registroConcepto = $this->_db->prepare("
                      UPDATE
                        nm_b002_concepto
                      SET
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_moficacion=NOW(),
                        ind_descripcion=:ind_descripcion, num_estatus=:num_estatus,
                        ind_formula=:ind_formula, ind_impresion=:ind_impresion, num_flag_automatico=:num_flag_automatico,
                        num_flag_bono=:num_flag_bono, num_flag_incidencia=:num_flag_incidencia, num_orden_planilla=:num_orden_planilla,
                        fk_a006_num_tipo_concepto=:fk_a006_num_tipo_concepto,ind_reporte=:ind_reporte, num_flag_catces=:num_flag_catces
                      WHERE
                        pk_num_concepto = '$idConcepto'
            ");
        $registroConcepto->execute(array(
            'num_estatus' => $status,
            'ind_descripcion' => mb_strtoupper($nombre, 'utf-8'),
	    'ind_reporte' => mb_strtoupper($reporte, 'utf-8'),
            'ind_formula' => $formula,
            'ind_impresion' => $titulo,
            'num_flag_automatico' => $flagAutomatico,
            'num_flag_bono' => $flagBonificacion,
            'num_flag_incidencia' => $flagIncidencia,
            'num_orden_planilla' => $orden,
            'fk_a006_num_tipo_concepto' => $tipoConcepto,
            'num_flag_catces' => $catces
        ));

        $eliminarConceptoDetalle = $this->_db->query(
            "DELETE FROM nm_d001_concepto_perfil_detalle WHERE fk_nmb002_num_concepto='$idConcepto'"
        );

        if ($nominas && $procesos) {
            $registroPerfilDetalle = $this->_db->prepare("
                      INSERT INTO
                        nm_d001_concepto_perfil_detalle
                      SET
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                        fk_nmb003_num_tipo_proceso=:fk_nmb003_num_tipo_proceso, fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                        fk_nmb002_num_concepto='$idConcepto',ind_cod_partida=:ind_cod_partida, ind_cod_cuenta_contable_debe=:ind_cod_cuenta_contable_debe,
                        ind_cod_cuenta_contable_haber=:ind_cod_cuenta_contable_haber, ind_cod_cuenta_contable_debe_pub20=:ind_cod_cuenta_contable_debe_pub20,
                        ind_cod_cuenta_contable_haber_pub20=:ind_cod_cuenta_contable_haber_pub20
                ");
            for ($i = 0; $i < count($nominas); $i++) {
                for ($ii = 0; $ii < count($procesos); $ii++) {
                        $id = $nominas[$i] . $procesos[$ii];
                        if (isset($haber[$id])) {
                            $haberInsert = $haber[$id];
                        } else {
                            $haberInsert = null;
                        }
                        if (isset($debe[$id]) && $debe[$id]!='error') {
                            $debeInsert = $debe[$id];
                        } else {
                            $debeInsert = null;
                        }
                        if (isset($partida[$id]) && $partida[$id]!='error') {
                            $partidaInsert = $partida[$id];
                        } else {
                            $partidaInsert = null;
                        }


                        if (isset($debepub20[$id]) && $debepub20[$id]!='error') {
                            $debepub20Insert = $debepub20[$id];
                        } else {
                            $debepub20Insert = null;
                        }

                        if (isset($haberPub20[$id])) {
                            $haberPub20Insert = $haberPub20[$id];
                        } else {
                            $haberPub20Insert = null;
                        }
                        $registroPerfilDetalle->execute(array(
                            'fk_nmb003_num_tipo_proceso' => $procesos[$ii],
                            'fk_nmb001_num_tipo_nomina' => $nominas[$i],
                            'ind_cod_partida' => $partidaInsert,
                            'ind_cod_cuenta_contable_debe' => $debeInsert,
                            'ind_cod_cuenta_contable_haber' => $haberInsert,
                            'ind_cod_cuenta_contable_debe_pub20' => $debepub20Insert,
                            'ind_cod_cuenta_contable_haber_pub20' => $haberPub20Insert
                        ));
                }
            }
        }
        $error = $registroConcepto->errorInfo();
        if (isset($registroPerfilDetalle)) {
            $error2 = $registroPerfilDetalle->errorInfo();
        } else {
            $error2 = array(1 => null, 2 => null);
        }

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } elseif(!empty($error2[1]) && !empty($error2[2])) {
            $this->_db->rollBack();
            return $error2;
        } else {
            $this->_db->commit();
            return $idConcepto;
        }

    }

}
