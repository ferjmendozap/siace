<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        18-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class acumuladoPrestacionesSocialesModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metPrestacionesSociales($idEmpleado)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              *,
              CONCAT(
                    nm_e002.fec_anio,
                    '-',
                    nm_e002.fec_mes
              ) AS periodo
            FROM
              nm_e002_prestaciones_sociales_calculo AS nm_e002
            WHERE 
              fk_rhb001_num_empleado = '$idEmpleado'
            ORDER BY periodo ASC 
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metPrestacionesSocialesRetroactivo($idEmpleado,$periodo)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              *,
              CONCAT(
                    nm_e002.fec_anio,
                    '-',
                    nm_e002.fec_mes
              ) AS periodo
            FROM
              nm_e001_prestaciones_sociales_calculo_retroactivo AS nm_e002
            WHERE 
              fk_rhb001_num_empleado = '$idEmpleado' AND 
              CONCAT(nm_e002.fec_anio,'-',nm_e002.fec_mes)='$periodo'
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetch();
    }

    public function metPrestacionesSocialesAnticipo($idEmpleado,$periodo)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              *,
              CONCAT(
                    nm_e003.fec_anio,
                    '-',
                    nm_e003.fec_mes
              ) AS periodo
            FROM
              nm_e003_prestaciones_sociales_anticipo AS nm_e003
            WHERE 
              nm_e003.fk_rhb001_num_empleado = '$idEmpleado' AND 
              CONCAT(nm_e003.fec_anio,'-',nm_e003.fec_mes)='$periodo'
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetch();
    }

    public function metPrestacionesSocialesIntereses($idEmpleado,$periodo)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              *,
              CONCAT(
                    nm_e004.fec_anio,
                    '-',
                    nm_e004.fec_mes
              ) AS periodo
            FROM
              nm_e004_prestaciones_sociales_intereses AS nm_e004
            WHERE 
              nm_e004.fk_rhb001_num_empleado = '$idEmpleado' AND 
              CONCAT(nm_e004.fec_anio,'-',nm_e004.fec_mes)='$periodo'
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetch();
    }

    public function metPrestacionesSocialesAcumulado($idEmpleado,$periodo = false, $estado = 1)
    {
        $periodoManual = Session::metObtener('periodoManualNM');
        if($periodo){
            $periodo = "CONCAT(nm_e002.fec_anio,'-',nm_e002.fec_mes)>'$periodoManual' AND CONCAT(nm_e002.fec_anio,'-',nm_e002.fec_mes)<='$periodo'";
        }else{
            $periodo = "CONCAT(nm_e002.fec_anio,'-',nm_e002.fec_mes)='$periodoManual'";
        }
        $obligacionListar = $this->_db->query("
            SELECT
              sum(nm_e002.num_monto_trimestral) as num_monto_trimestral,
              sum(nm_e002.num_monto_anual) as num_monto_anual,
              CONCAT(
                    nm_e002.fec_anio,
                    '-',
                    nm_e002.fec_mes
              ) AS periodo
            FROM
              nm_e002_prestaciones_sociales_calculo AS nm_e002
            WHERE 
              fk_rhb001_num_empleado = '$idEmpleado' AND 
              nm_e002.num_estatus = '$estado' AND 
              $periodo 
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetch();
    }

    public function metPrestacionesSocialesAcumuladoRetroactivo($idEmpleado,$periodo = false, $estado = 1)
    {
        $periodoManual = Session::metObtener('periodoManualNM');
        if($periodo){
            $periodo = "CONCAT(nm_e002.fec_anio,'-',nm_e002.fec_mes)>'$periodoManual' AND CONCAT(nm_e002.fec_anio,'-',nm_e002.fec_mes)<='$periodo'";
        }else{
            $periodo = "CONCAT(nm_e002.fec_anio,'-',nm_e002.fec_mes)='$periodoManual'";
        }
        $obligacionListar = $this->_db->query("
            SELECT
              sum(nm_e002.num_monto_trimestral) as num_monto_trimestral,
              sum(nm_e002.num_monto_anual) as num_monto_anual,
              CONCAT(
                    nm_e002.fec_anio,
                    '-',
                    nm_e002.fec_mes
              ) AS periodo
            FROM
              nm_e001_prestaciones_sociales_calculo_retroactivo AS nm_e002
            WHERE 
              fk_rhb001_num_empleado = '$idEmpleado' AND  
              nm_e002.num_estatus = '$estado' AND
              $periodo
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetch();
    }

    public function metPrestacionesSocialesAcumuladoAnticipo($idEmpleado,$periodo = false, $estado = 1)
    {
        $periodoManual = Session::metObtener('periodoManualNM');
        if($periodo){
            $periodo = "CONCAT(nm_e003.fec_anio,'-',nm_e003.fec_mes)>'$periodoManual' AND CONCAT(nm_e003.fec_anio,'-',nm_e003.fec_mes)<='$periodo'";
        }else{
            $periodo = "CONCAT(nm_e003.fec_anio,'-',nm_e003.fec_mes)='$periodoManual'";
        }
        $obligacionListar = $this->_db->query("
            SELECT
              sum(nm_e003.num_anticipo) as num_anticipo,
              CONCAT(
                    nm_e003.fec_anio,
                    '-',
                    nm_e003.fec_mes
              ) AS periodo
            FROM
              nm_e003_prestaciones_sociales_anticipo AS nm_e003
            WHERE 
              nm_e003.fk_rhb001_num_empleado = '$idEmpleado' AND  
              nm_e003.num_estatus = '$estado' AND
              $periodo
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetch();
    }

    public function metPrestacionesSocialesAcumuladoIntereses($idEmpleado,$periodo = false, $estado = 1)
    {
        $periodoManual = Session::metObtener('periodoManualNM');
        if($periodo){
            $periodo = "CONCAT(nm_e004.fec_anio,'-',nm_e004.fec_mes)>'$periodoManual' AND CONCAT(nm_e004.fec_anio,'-',nm_e004.fec_mes)<='$periodo'";
        }else{
            $periodo = "CONCAT(nm_e004.fec_anio,'-',nm_e004.fec_mes)='$periodoManual'";
        }
        $obligacionListar = $this->_db->query("
            SELECT
              sum(nm_e004.num_monto_interes) as num_monto_interes,
              CONCAT(
                    nm_e004.fec_anio,
                    '-',
                    nm_e004.fec_mes
              ) AS periodo
            FROM
              nm_e004_prestaciones_sociales_intereses AS nm_e004
            WHERE 
              nm_e004.fk_rhb001_num_empleado = '$idEmpleado' AND  
              nm_e004.num_estatus = '$estado' AND
              $periodo
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetch();
    }
}
