<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Fernando Mendoza                 |dt.jefe.ait@cgesucre.gob.ve         |         0424-8942068           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        10-01-2017       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class txtModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }


    /*
    *Agregado por . Fernando Mendoza
    *Fecha: 10Ene18
    *Permite Listar los Periodos de Nomina para generar TXT de Nomina
    */
    public function metGenerarPeriodosTxt()
    {
        $sql ="SELECT
                nm_c002_proceso_periodo.pk_num_proceso_periodo,
                nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo,
                nm_c001_tipo_nomina_periodo.fec_anio,
                nm_c001_tipo_nomina_periodo.fec_mes,
                concat(
                    nm_c001_tipo_nomina_periodo.fec_anio,
                    '-',
                    nm_c001_tipo_nomina_periodo.fec_mes
                ) AS periodo
            FROM
                nm_c002_proceso_periodo
            INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo = nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo
            WHERE nm_c002_proceso_periodo.ind_estado = 'GE'
            GROUP BY periodo";
        $periodosTxt = $this->_db->query($sql);
        $periodosTxt->setFetchMode(PDO::FETCH_ASSOC);
        return $periodosTxt->fetchAll();            

    }    

    public function metProcesosDisponiblesTxt()
    {
        $sql ="SELECT
                nm_b003_tipo_proceso.pk_num_tipo_proceso,
                nm_b003_tipo_proceso.cod_proceso,
                nm_b003_tipo_proceso.ind_nombre_proceso,
                nm_b003_tipo_proceso.num_flag_adelanto,
                nm_b003_tipo_proceso.num_flag_individual,
                nm_b003_tipo_proceso.num_estatus
              FROM
                nm_b003_tipo_proceso
              WHERE nm_b003_tipo_proceso.num_estatus=1";
        $periodosTxt = $this->_db->query($sql);
        $periodosTxt->setFetchMode(PDO::FETCH_ASSOC);
        return $periodosTxt->fetchAll();  
    }

    public function metEmpleadosNomina($idProcesoPeriodo)
    {
        $consulta = $this->_db->query("
            SELECT
              a003_persona.pk_num_persona,
              rh_b001_empleado.pk_num_empleado,
              CONCAT_WS(
                ' ',
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1
               ) AS nombre,
              a003_persona.ind_cedula_documento,
              a003_persona.ind_documento_fiscal,
              rh_c063_puestos.ind_descripcion_cargo,
              rh_c076_empleado_organizacion.fk_a004_num_dependencia,
              rh_c012_empleado_banco.ind_cuenta,
              nm_c001_tipo_nomina_periodo.fec_anio,
              nm_c001_tipo_nomina_periodo.fec_mes,
              a006_miscelaneo_detalle.ind_nombre_detalle AS tipo_cuenta_nombre,
              a006_miscelaneo_detalle.cod_detalle AS tipo_cuenta
            FROM
              rh_b001_empleado
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            INNER JOIN nm_d005_tipo_nomina_empleado_concepto ON rh_b001_empleado.pk_num_empleado = nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado
            INNER JOIN nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
            INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
            INNER JOIN rh_c059_empleado_nivelacion ON rh_c059_empleado_nivelacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
            INNER JOIN rh_c063_puestos ON rh_c063_puestos.pk_num_puestos = rh_c059_empleado_nivelacion.fk_rhc063_num_puestos
            INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
            LEFT JOIN rh_c012_empleado_banco ON rh_b001_empleado.pk_num_empleado = rh_c012_empleado_banco.fk_rhb001_num_empleado
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = rh_c012_empleado_banco.fk_a006_num_miscelaneo_detalle_tipocta
            INNER JOIN nm_b003_tipo_proceso ON nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso = nm_b003_tipo_proceso.pk_num_tipo_proceso
            WHERE
              nm_b003_tipo_proceso.pk_num_tipo_proceso = '$idProcesoPeriodo'
            GROUP BY
              rh_b001_empleado.pk_num_empleado
            ORDER BY
	      rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina, length(a003_persona.ind_cedula_documento), a003_persona.ind_cedula_documento  
              
        ");
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }


    public function metEmpleadosNominaNuevo($anio,$mes,$proceso)
    {

        $consulta = $this->_db->query("
            SELECT
              rh_b001_empleado.pk_num_empleado,
              nm_d005_tipo_nomina_empleado_concepto.pk_num_tiponomina_empleadoconcepto,
              nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo,
              nm_c002_proceso_periodo.fec_desde,
              nm_c002_proceso_periodo.fec_hasta,
              nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto,
              nm_b002_concepto.cod_concepto,
              nm_b002_concepto.ind_descripcion,
              nm_d005_tipo_nomina_empleado_concepto.num_monto,
              nm_c001_tipo_nomina_periodo.fec_anio,
              nm_c001_tipo_nomina_periodo.fec_mes,
              CONCAT_WS(
                '-',
                nm_c001_tipo_nomina_periodo.fec_anio,
                nm_c001_tipo_nomina_periodo.fec_mes
              ) AS PERIODO,
              nm_b003_tipo_proceso.cod_proceso,
              nm_b003_tipo_proceso.ind_nombre_proceso,
              nm_b001_tipo_nomina.cod_tipo_nomina,
              nm_b001_tipo_nomina.ind_nombre_nomina,
              a006_miscelaneo_detalle.cod_detalle,
              nm_d005_tipo_nomina_empleado_concepto.num_cantidad,
              nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado,
              rh_b001_empleado.cod_empleado,
              a003_persona.ind_cedula_documento,
              CONCAT_WS(
                ' ',
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1
              ) AS nombre,
              nm_b001_tipo_nomina.pk_num_tipo_nomina,
              be.ind_cuenta,
              tipo_cuenta.cod_detalle AS tipo_cuenta
            FROM
              nm_d005_tipo_nomina_empleado_concepto
            INNER JOIN nm_c002_proceso_periodo ON nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo = nm_c002_proceso_periodo.pk_num_proceso_periodo
            INNER JOIN nm_b002_concepto ON nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto = nm_b002_concepto.pk_num_concepto
            INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo = nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo
            INNER JOIN nm_b003_tipo_proceso ON nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso = nm_b003_tipo_proceso.pk_num_tipo_proceso
            INNER JOIN nm_b001_tipo_nomina ON nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
            INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado
            INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
            LEFT JOIN rh_c012_empleado_banco AS be ON rh_b001_empleado.pk_num_empleado = be.fk_rhb001_num_empleado
            INNER JOIN a006_miscelaneo_detalle AS tipo_cuenta ON tipo_cuenta.pk_num_miscelaneo_detalle = be.fk_a006_num_miscelaneo_detalle_tipocta
            WHERE
                nm_c001_tipo_nomina_periodo.fec_anio = '$anio'
                AND nm_c001_tipo_nomina_periodo.fec_mes = '$mes'
                AND nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso = '$proceso'
            GROUP BY
              nm_b001_tipo_nomina.pk_num_tipo_nomina, LENGTH(a003_persona.ind_cedula_documento),a003_persona.ind_cedula_documento
              
        ");
        //var_dump($consulta);
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();


    }


    public function metEmpleadoTotalNeto($idProcesoPeriodo,$idEmpleado)
    {

	$neto = $this->_db->query("
		SELECT
		(
			
			(
				SELECT
					SUM(
						nm_d005_tipo_nomina_empleado_concepto.num_monto
					) AS num_monto
				FROM
					nm_d005_tipo_nomina_empleado_concepto
				INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
				INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
				INNER JOIN nm_c002_proceso_periodo ON nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo = nm_c002_proceso_periodo.pk_num_proceso_periodo
				INNER JOIN nm_b003_tipo_proceso ON nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso = nm_b003_tipo_proceso.pk_num_tipo_proceso
				WHERE
					nm_b003_tipo_proceso.pk_num_tipo_proceso = '$idProcesoPeriodo'
				AND nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado = '$idEmpleado'
				AND (
					a006_miscelaneo_detalle.cod_detalle = 'I'
					OR a006_miscelaneo_detalle.cod_detalle = 'T'
					OR a006_miscelaneo_detalle.cod_detalle = 'AS'
					OR a006_miscelaneo_detalle.cod_detalle = 'B'
				)
			) - (
				SELECT
					SUM(
						nm_d005_tipo_nomina_empleado_concepto.num_monto
					) AS num_monto
				FROM
					nm_d005_tipo_nomina_empleado_concepto
				INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
				INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
				INNER JOIN nm_c002_proceso_periodo ON nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo = nm_c002_proceso_periodo.pk_num_proceso_periodo
				INNER JOIN nm_b003_tipo_proceso ON nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso = nm_b003_tipo_proceso.pk_num_tipo_proceso
				WHERE
					nm_b003_tipo_proceso.pk_num_tipo_proceso = '$idProcesoPeriodo'
				AND nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado = '$idEmpleado'
				AND (
					a006_miscelaneo_detalle.cod_detalle = 'D'
					OR a006_miscelaneo_detalle.cod_detalle = 'R'
				)
			)
		) AS total_neto
		FROM
			nm_d005_tipo_nomina_empleado_concepto
		INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
		INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
		INNER JOIN nm_c002_proceso_periodo ON nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo = nm_c002_proceso_periodo.pk_num_proceso_periodo
		INNER JOIN nm_b003_tipo_proceso ON nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso = nm_b003_tipo_proceso.pk_num_tipo_proceso
		WHERE
			nm_b003_tipo_proceso.pk_num_tipo_proceso = '$idProcesoPeriodo'
		AND nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado = '$idEmpleado'
		GROUP BY
			nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado
	");
	$neto->setFetchMode(PDO::FETCH_ASSOC);
        return $neto->fetch();
	

    }	

    public function metEmpleadosAsignaciones($idProcesoPeriodo,$idEmpleado)
    {
        $obligacionListar = $this->_db->query("
            SELECT
	      nm_d005_tipo_nomina_empleado_concepto.num_monto,
              nm_b002_concepto.ind_abrebiatura,
              nm_b002_concepto.cod_concepto,
              nm_b002_concepto.ind_descripcion,
              nm_b002_concepto.ind_impresion
            FROM
              nm_d005_tipo_nomina_empleado_concepto
            INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
            INNER JOIN nm_c002_proceso_periodo ON nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo = nm_c002_proceso_periodo.pk_num_proceso_periodo
            INNER JOIN nm_b003_tipo_proceso ON nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso = nm_b003_tipo_proceso.pk_num_tipo_proceso
            WHERE
              nm_b003_tipo_proceso.pk_num_tipo_proceso = '$idProcesoPeriodo'
            AND nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado = '$idEmpleado'
            AND (
              a006_miscelaneo_detalle.cod_detalle = 'I'
              OR a006_miscelaneo_detalle.cod_detalle = 'T'
              OR a006_miscelaneo_detalle.cod_detalle = 'AS'
              OR a006_miscelaneo_detalle.cod_detalle = 'B'
            )
            ORDER BY
              nm_b002_concepto.ind_abrebiatura ASC
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }


    public function metEmpleadosAsignacionesNuevo($anio,$mes,$idProcesoPeriodo,$idEmpleado)
    {
        $asignaciones = $this->_db->query("
              SELECT
                nm_b002_concepto.cod_concepto,
                nm_b002_concepto.ind_descripcion,
                nm_d005_tipo_nomina_empleado_concepto.num_monto,
                a006_miscelaneo_detalle.cod_detalle,
                nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado,
                nm_b002_concepto.ind_impresion,
                nm_b002_concepto.ind_abrebiatura
              FROM
                nm_d005_tipo_nomina_empleado_concepto
              INNER JOIN nm_c002_proceso_periodo ON nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo = nm_c002_proceso_periodo.pk_num_proceso_periodo
              INNER JOIN nm_b002_concepto ON nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto = nm_b002_concepto.pk_num_concepto
              INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo = nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo
              INNER JOIN nm_b003_tipo_proceso ON nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso = nm_b003_tipo_proceso.pk_num_tipo_proceso
              INNER JOIN nm_b001_tipo_nomina ON nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
              INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
              INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado
              INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
              WHERE
                nm_c001_tipo_nomina_periodo.fec_anio = '$anio'
              AND nm_c001_tipo_nomina_periodo.fec_mes = '$mes'
              AND nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso = '$idProcesoPeriodo'
              AND nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado = '$idEmpleado'
              AND (
                a006_miscelaneo_detalle.cod_detalle = 'I'
                OR a006_miscelaneo_detalle.cod_detalle = 'T'
                OR a006_miscelaneo_detalle.cod_detalle = 'AS'
                OR a006_miscelaneo_detalle.cod_detalle = 'B'
              )
            ORDER BY
              nm_b002_concepto.ind_abrebiatura ASC
        ");
        $asignaciones->setFetchMode(PDO::FETCH_ASSOC);
        return $asignaciones->fetchAll();
    }


    public function metEmpleadosDeducciones($idProcesoPeriodo,$idEmpleado)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              nm_d005_tipo_nomina_empleado_concepto.num_monto,
              nm_b002_concepto.ind_abrebiatura,
              nm_b002_concepto.cod_concepto,
              nm_b002_concepto.ind_descripcion,
              nm_b002_concepto.ind_impresion
            FROM
              nm_d005_tipo_nomina_empleado_concepto
            INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
            INNER JOIN nm_c002_proceso_periodo ON nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo = nm_c002_proceso_periodo.pk_num_proceso_periodo
            INNER JOIN nm_b003_tipo_proceso ON nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso = nm_b003_tipo_proceso.pk_num_tipo_proceso
            WHERE
              nm_b003_tipo_proceso.pk_num_tipo_proceso = '$idProcesoPeriodo'
            AND nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado = '$idEmpleado'
            AND (
              a006_miscelaneo_detalle.cod_detalle = 'D'
              OR a006_miscelaneo_detalle.cod_detalle = 'R'
            )
            ORDER BY
              nm_b002_concepto.ind_abrebiatura ASC
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metEmpleadosDeduccionesNuevo($anio,$mes,$idProcesoPeriodo,$idEmpleado)
    {
        $obligacionListar = $this->_db->query("
              SELECT
                nm_b002_concepto.cod_concepto,
                nm_b002_concepto.ind_descripcion,
                nm_d005_tipo_nomina_empleado_concepto.num_monto,
                a006_miscelaneo_detalle.cod_detalle,
                nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado,
                nm_b002_concepto.ind_impresion,
                nm_b002_concepto.ind_abrebiatura
              FROM
                nm_d005_tipo_nomina_empleado_concepto
              INNER JOIN nm_c002_proceso_periodo ON nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo = nm_c002_proceso_periodo.pk_num_proceso_periodo
              INNER JOIN nm_b002_concepto ON nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto = nm_b002_concepto.pk_num_concepto
              INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo = nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo
              INNER JOIN nm_b003_tipo_proceso ON nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso = nm_b003_tipo_proceso.pk_num_tipo_proceso
              INNER JOIN nm_b001_tipo_nomina ON nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
              INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
              INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado
              INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
              WHERE
                nm_c001_tipo_nomina_periodo.fec_anio = '$anio'
              AND nm_c001_tipo_nomina_periodo.fec_mes = '$mes'
              AND nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso = '$idProcesoPeriodo'
              AND nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado = '$idEmpleado'
              AND (
                a006_miscelaneo_detalle.cod_detalle = 'D'
                OR a006_miscelaneo_detalle.cod_detalle = 'R'
              )
            ORDER BY
              nm_b002_concepto.ind_abrebiatura ASC
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }    


}
