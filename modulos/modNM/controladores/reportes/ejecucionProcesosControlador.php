<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . DS . 'modNM/controladores/FuncionesNomina/funcionesNominaControlador.php';

class ejecucionProcesosControlador extends Controlador
{
    use funcionesNomina {
        funcionesNomina::__construct as private __fnConstruct;
    }

    private $atFPDF;
    private $atEjecucionProcesos;

    public function __construct()
    {
        parent::__construct();
        $this->__fnConstruct();
        $this->atEjecucionProcesos = $this->metCargarModelo('ejecucionProcesos', 'procesos');
        $this->metObtenerLibreria('header', 'modNM');
        $this->atFPDF = new pdfEjecucion('P', 'mm', 'Letter');
    }

    public function metIndex()
    {
        exit;
    }


    public function metVerNomina($idJsonEmpleados, $idTipoNominaPeriodo, $idPeriodoProceso, $tipo)
    {
        $idEmpleado = str_getcsv($idJsonEmpleados, ',');

        if ($idTipoNominaPeriodo && $idPeriodoProceso) {
            $codProceso = $this->atEjecucionProcesos->metObtenerProceso($idPeriodoProceso);
            $this->atFPDF->setDesde(date('d-m-Y', strtotime($codProceso['fec_desde'])));
            $this->atFPDF->setHasta(date('d-m-Y', strtotime($codProceso['fec_hasta'])));
            $this->atFPDF->setNomina(utf8_decode($codProceso['ind_titulo_boleta']));
            $this->atFPDF->setNombreProceso(utf8_decode($codProceso['ind_nombre_proceso']));
            $this->atFPDF->AddPage();

            $totales = 0;
            for ($i = 0; $i < count($idEmpleado); $i++) {
                //if ($i % 2 == 0 && $i != 0) {
                    $this->atFPDF->AddPage();
                //}
                $this->atArgsIdEmpleado = $idEmpleado[$i];

                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->SetWidths(array(23, 30, 110, 30));
                $this->atFPDF->SetAligns(array('L', 'L', 'L', 'L'));
                $this->atFPDF->Row(array(utf8_decode('Código'), utf8_decode('Cédula'), 'Empleado', 'Fecha Ing.'));
                $this->atFPDF->Ln(2);
                $datosEmpleado = $this->atEjecucionProcesos->metObtenerEmpleado($idEmpleado[$i]);
                $this->atFPDF->Row(array(str_pad($datosEmpleado['pk_num_empleado'], 6, "0", STR_PAD_LEFT), utf8_decode($datosEmpleado['ind_cedula_documento']), utf8_decode($datosEmpleado['ind_nombre1'] . ' ' . $datosEmpleado['ind_nombre2'] . ' ' . $datosEmpleado['ind_apellido1'] . ' ' . $datosEmpleado['ind_apellido2']), date('d-m-Y', strtotime($datosEmpleado['fec_ingreso']))));
                $this->atFPDF->Ln(2);
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->SetWidths(array(23, 140));
                $this->atFPDF->SetAligns(array('L', 'L'));
                if($datosEmpleado['fk_nmb001_num_tipo_nomina']==NOJ || $datosEmpleado['fk_nmb001_num_tipo_nomina']==NOP ){
                    if($datosEmpleado['fk_nmb001_num_tipo_nomina']==NOJ){
                        $cargo = 'jubilado';
                    }else{
                        $cargo = 'Pensionado';
                    }
                    $this->atFPDF->Row(array('Cargo:', utf8_decode($cargo)));
                }else{
                    $this->atFPDF->Row(array('Cargo:', utf8_decode($datosEmpleado['ind_descripcion_cargo'])));
                }
                $this->atFPDF->SetWidths(array(55, 120));
                $this->atFPDF->SetAligns(array('L', 'L'));
                $this->atFPDF->Ln(2);
                $this->atFPDF->Ln(2);
                if($datosEmpleado['fk_nmb001_num_tipo_nomina']==NOJ || $datosEmpleado['fk_nmb001_num_tipo_nomina']==NOP ){
                  $this->atFPDF->Row(array('Sueldo Basico Mensual:', number_format($this->metSueldoBasicoJP(1), 2, ',', '.')));  
                }else{
                  $this->atFPDF->Row(array('Sueldo Basico Mensual:', number_format($this->metSueldoBasico(1), 2, ',', '.')));  
                }
                $this->atFPDF->Ln(2);
                //	Imprimo los conceptos
                $this->atFPDF->SetDrawColor(0, 0, 0);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->SetFont('Courier', 'B', 11);
                $y = $this->atFPDF->GetY();
                $this->atFPDF->Rect(10, $y, 195, 0.1);
                $this->atFPDF->Cell(104, 5, 'A S I G N A C I O N E S', 0, 0, 'C');
                $this->atFPDF->Cell(104, 5, 'D E D U C C I O N E S', 0, 1, 'C');
                $y = $this->atFPDF->GetY();
                $this->atFPDF->Rect(10, $y, 195, 0.1);
                $this->atFPDF->Ln(1);
                $y = $this->atFPDF->GetY();
                $yi = $y;
                $yd = $y;
                $contador_conceptos = 0;
                $linead = 0;
                $lineai = 0;
                $asignaciones = 0;
                $deducciones = 0;
                $conceptosEmpleado = $this->atEjecucionProcesos->metObtenerConceptosGeneradosEmpleado($idPeriodoProceso, $idEmpleado[$i]);
                foreach ($conceptosEmpleado AS $conceptoEmpleado) {
                    $this->atFPDF->SetDrawColor(255, 255, 255);
                    $this->atFPDF->SetFillColor(255, 255, 255);
                    $this->atFPDF->SetTextColor(0, 0, 0);
                    $this->atFPDF->SetFont('Courier', 'B', 10);
                    $this->atFPDF->SetWidths(array(65, 30, 1));
                    $this->atFPDF->SetAligns(array('L', 'R', 'R'));
                    if (
                        $conceptoEmpleado['cod_detalle'] == "I" || $conceptoEmpleado['cod_detalle'] == "AS" ||
                        $conceptoEmpleado['cod_detalle'] == "B" || $conceptoEmpleado['cod_detalle'] == "T"
                    ) {
                        $asignaciones = $asignaciones + $conceptoEmpleado['num_monto'];
                        $this->atFPDF->SetXY(10, $yi);
                        /*if(strlen($conceptoEmpleado['ind_impresion'])>27) {
                            $yi += 5;
                        }*/
                        $this->atFPDF->Row(array(utf8_decode(substr($conceptoEmpleado['ind_impresion'],0,29)), number_format($conceptoEmpleado['num_monto'], 2, ',', '.'), ''));
                        $yi += 5;
                        $lineai++;
                    } elseif (
                        $conceptoEmpleado['cod_detalle'] == "R" || $conceptoEmpleado['cod_detalle'] == "D"
                    ) {
                        $deducciones = $deducciones + $conceptoEmpleado['num_monto'];
                        $this->atFPDF->SetXY(107, $yd);
                        /*if(strlen($conceptoEmpleado['ind_impresion'])>27) {
                            $yd += 5;
                        }*/
                        $this->atFPDF->Row(array(utf8_decode(substr($conceptoEmpleado['ind_impresion'],0,29)), number_format($conceptoEmpleado['num_monto'], 2, ',', '.'), ''));
                        $yd += 5;
                        $linead++;
                    }
                }

                if ($lineai > $linead) {
                    $contador_conceptos = $lineai;
                } else {
                    $contador_conceptos = $linead;
                }

                if ($yi > $yd) {
                    $y = $yi + 2;
                } else {
                    $y = $yd + 2;
                }
                $this->atFPDF->SetDrawColor(0, 0, 0);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->Rect(10, $y, 195, 0.1);
                $this->atFPDF->Ln(1);
                $this->atFPDF->SetFont('Courier', 'B', 10);
                $this->atFPDF->SetY($y);
                $this->atFPDF->Cell(70, 5, 'TOTAL ASIGNACIONES', 0, 0, 'L');
                $this->atFPDF->Cell(25, 5, number_format($asignaciones, 2, ',', '.'), 0, 0, 'R');
                $this->atFPDF->Cell(1, 5);
                $this->atFPDF->Cell(70, 5, 'TOTAL DEDUCCIONES', 0, 0, 'L');
                $this->atFPDF->Cell(25, 5, number_format($deducciones, 2, ',', '.'), 0, 1, 'R');
                $this->atFPDF->Cell(97, 5);
                $this->atFPDF->Cell(70, 5, 'TOTAL A PAGAR', 0, 0, 'L');
                $totales = $totales + ($asignaciones - $deducciones);
                $this->atFPDF->Cell(25, 5, number_format($asignaciones - $deducciones, 2, ',', '.'), 0, 1, 'R');

                $this->atFPDF->SetFont('Courier', 'B', 10);
                $this->atFPDF->Ln(5);
                $this->atFPDF->SetX(20);
                $this->atFPDF->Cell(80, 5, '____________________', 0, 1, 'C');
                $this->atFPDF->SetX(20);
                $this->atFPDF->Cell(80, 5, 'RECIBI CONFORME', 0, 0, 'C');
                $this->atFPDF->Ln(4);
                $this->atFPDF->SetX(43.5);
                $this->atFPDF->Cell(50, 5, 'C.I: ' . $datosEmpleado['ind_cedula_documento'], 0, 0, 'L');
                $this->atFPDF->Ln(20);
                if ($tipo == 'verPayRoll') {
                    if ($i % 2 == 0) {
                        $this->atFPDF->SetDrawColor(255, 255, 255);
                        $this->atFPDF->SetFillColor(255, 255, 255);
                        $this->atFPDF->SetTextColor(0, 0, 0);
                        $this->atFPDF->SetFont('Courier', 'B', 12);
                        $this->atFPDF->Cell(190, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
                        $this->atFPDF->SetX(15);
                        $this->atFPDF->SetFont('Courier', 'B', 10);
                        $this->atFPDF->Cell(190, 5, 'Fecha de Emision:' . date('Y-m-d H:i:s'), 0, 1, 'R');

                        $this->atFPDF->Ln(5);
                        $this->atFPDF->SetDrawColor(0, 0, 0);
                        $this->atFPDF->SetFillColor(255, 255, 255);
                        $this->atFPDF->SetTextColor(0, 0, 0);
                        $this->atFPDF->SetFont('Courier', 'B', 11);

                        $this->atFPDF->SetWidths(array(70, 65, 60));
                        $this->atFPDF->SetAligns(array('L', 'L', 'L', 'L'));
                        $this->atFPDF->Row(array('Nomina', 'Nombre del Proceso', 'Periodo'));
                        $this->atFPDF->Row(array(utf8_decode($codProceso['ind_titulo_boleta']), utf8_decode($codProceso['ind_nombre_proceso']), date('d-m-Y', strtotime($codProceso['fec_desde'])) . ' A: ' . date('d-m-Y', strtotime($codProceso['fec_hasta']))));
                        $this->atFPDF->Ln(5);
                    }
                }

            }
            $this->atFPDF->Ln(5);
            if ($tipo != 'verPayRoll') {
                $this->atFPDF->Cell(70, 5, 'TOTAL NETO A PAGAR: ' . number_format($totales, 2, ',', '.'), 0, 0, 'L');
            }
            if (preg_match("/MSIE/i", $_SERVER["HTTP_USER_AGENT"])) {
                header("Content-type: application/PDF");
            } else {
                header("Content-type: application/PDF");
                header("Content-Type: application/pdf");
            }
            $this->atFPDF->Output();
            exit;
        }
    }


}
