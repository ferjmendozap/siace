<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          xxxxxxxxxxxxxx                   |                                    |                                |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |                                       |                         |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . DS . 'modNM/controladores/FuncionesNomina/funcionesNominaControlador.php';

class nominaCuentaControlador extends Controlador
{
    use funcionesNomina {
        funcionesNomina::__construct as private __fnConstruct;
    }

    private $atFPDF;
    private $atEjecucionProcesos;
    private $atConsultasComunes;
    private $atTipoNomina;


    public function __construct()
    {
        parent::__construct();
        $this->__fnConstruct();
        $this->atEjecucionProcesos = $this->metCargarModelo('ejecucionProcesos', 'procesos');
        $this->atConsultasComunes = $this->metCargarModelo('consultasComunes', 'reportes');
        $this->atTipoNomina = $this->metCargarModelo('tipoNomina', 'maestros');
        $this->metObtenerLibreria('headerNominaCuenta', 'modNM');
        $this->atFPDF = new pdfRelacionNominaCuenta('P', 'mm', array(200, 279));
    }

    public function metIndex()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('tipoNomina', $this->atTipoNomina->metListarTipoNomina());
        $this->atVista->metRenderizar('index');
    }


    public function metPdfRelacionNominaCuenta($idProcesoPeriodo)
    {
        $nomina = $this->atEjecucionProcesos->metObtenerProceso($idProcesoPeriodo);

        $empleados = $this->atConsultasComunes->metEmpleadosNomina($idProcesoPeriodo);


        $this->atFPDF->SetTipoNom(utf8_decode($nomina['ind_titulo_boleta']));
        $this->atFPDF->SetProceso(utf8_decode($nomina['ind_nombre_proceso']));
        $this->atFPDF->setPeriodo('MES DE '.$this->metMesLetras($empleados[0]['fec_mes']).' '.$empleados[0]['fec_anio']);

        $this->atFPDF->setDesde(date('d-m-Y', strtotime($nomina['fec_desde'])));
        $this->atFPDF->setHasta(date('d-m-Y', strtotime($nomina['fec_hasta'])));

        $this->atFPDF->setTipoHeader('nomina');

        #### PDF ####
/*
        $this->atFPDF->SetMargins(10, 15, 10);
        $this->atFPDF->AliasNbPages();
*/
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 15, 10);
        $this->atFPDF->SetAutoPageBreak(1, 20);
        $this->atFPDF->AddPage();
        $this->atFPDF->SetFont('Arial', '', 6);


        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetWidths(array(8, 20, 85, 24, 40));
        $this->atFPDF->SetAligns(array('C', 'C', 'L', 'C', 'C'));

        $this->atFPDF->SetFont('Arial', '', 7);
        $c=0;
        $total = 0;
        foreach ($empleados AS $empleado) {
            $c = $c + 1;
            $asignaciones = $this->atConsultasComunes->metEmpleadosAsignaciones($idProcesoPeriodo, $empleado['pk_num_empleado']);
            $deducciones = $this->atConsultasComunes->metEmpleadosDeducciones($idProcesoPeriodo, $empleado['pk_num_empleado']);

            $a = 0;
            foreach ($asignaciones AS $asignacion) {
                $a = $a + $asignacion['num_monto'];
            }

            $d = 0;
            foreach ($deducciones AS $deduccion) {
                $d = $d + $deduccion['num_monto'];
            }


            //SALTAR PAGINA
            $y = $this->atFPDF->GetY();

            if ($y > 300){
                $this->atFPDF->AddPage();
                $this->atFPDF->SetDrawColor(0, 0, 0);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->SetWidths(array(8, 20, 85, 24, 40));
                $this->atFPDF->SetAligns(array('C', 'C', 'L', 'C', 'C'));
            }

            $monto = number_format($a - $d,2,',','.');
            $total = $total + (number_format($a,2,'.','') - number_format($d,2,'.',''));
            $this->atFPDF->Row(array($c,number_format($empleado['ind_cedula_documento'], 0, '', '.') , utf8_decode($empleado['nombre']), $monto, $empleado['ind_cuenta']));
        }
	
        $preparadoPor = $this->atConsultasComunes->metMostrarEmpleado($nomina['fk_rhb001_num_empleado_procesa']);
        $revisadoPor = $this->atConsultasComunes->metMostrarEmpleadoJefe($preparadoPor['pk_num_dependencia']);
        $conformadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) DE RECURSOS HUMANOS ');
        $aprobadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) GENERAL');


        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetAligns(array('C', 'C', 'L', 'C', 'C'));
        $total = number_format($total,2,',','.');
        $this->atFPDF->Row(array('', '', 'TOTAL', $total, ''));
        $this->atFPDF->Ln(15);



        $this->atFPDF->SetDrawColor(255, 255, 255); $this->atFPDF->SetFillColor(255, 255, 255); $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetWidths(array(10,90, 100));
        $this->atFPDF->SetAligns(array('','L', 'L'));
	$this->atFPDF->Ln(6);
        $this->atFPDF->Row(array('','_____________________','_____________________'));
	$this->atFPDF->Row(array('','ELABORADO POR:','CONFORMADO POR:'));
        $this->atFPDF->Row(array('',utf8_decode($preparadoPor['nombre']),utf8_decode($revisadoPor['nombre'])));
        $this->atFPDF->Row(array('',utf8_decode($preparadoPor['ind_descripcion_cargo']),utf8_decode($revisadoPor['ind_descripcion_cargo'])));

        /*$this->atFPDF->Ln(8);
        $this->atFPDF->Row(array('','AUTORIZADO POR:'));$this->atFPDF->Ln(3);
        $this->atFPDF->Row(array('','__________________'));
        $this->atFPDF->Row(array('',utf8_decode($conformadoPor['nombre'])));

        $this->atFPDF->Row(array('',utf8_decode($conformadoPor['ind_descripcion_cargo'])));*/


        $this->atFPDF->Output();
    }
}
