<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . DS . 'modNM/controladores/FuncionesNomina/funcionesNominaControlador.php';

class consolidadoPrestacionesSocialesControlador extends Controlador
{
    use funcionesNomina {
        funcionesNomina::__construct as private __fnConstruct;
    }

    private $atFPDF;
    private $atEjecucionProcesos;
    private $atConsultasComunes;
    private $atTipoNomina;

    public function __construct()
    {
        parent::__construct();
        $this->__fnConstruct();
        $this->atEjecucionProcesos = $this->metCargarModelo('ejecucionProcesos', 'procesos');
        $this->atConsultasComunes = $this->metCargarModelo('consultasComunes', 'reportes');
        $this->atTipoNomina = $this->metCargarModelo('tipoNomina', 'maestros');
        $this->metObtenerLibreria('headerNominaConsolidada', 'modNM');
        $this->atFPDF = new pdfNominaConsolidada("L", "mm", "LEGAL");
    }

    public function metIndex()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('lista', $this->atConsultasComunes->metPrestacionesSocialesAños());
        $this->atVista->metRenderizar('index');
    }


    public function metPdfConsolidadoPrestacionesSociales($fecPrestacionesSociales)
    {
        $empleadoPrestacionesSociales = $this->atConsultasComunes->metConsolidadoPrestacionesSociales($fecPrestacionesSociales);
        $this->atFPDF->setTipoHeader('prestacionesSocialesConsolidado');
        $this->atFPDF->setHasta($fecPrestacionesSociales);
        $n = 1;
        $this->atFPDF->AddPage();
        $totalPrestaciones = 0;
        $totalPago = 0;
        foreach ($empleadoPrestacionesSociales AS $empleadoPrestaciones) {
            if ($this->atFPDF->GetY() <= 152) {
                $prestaciones = ($empleadoPrestaciones['TRIMESTRE1'] + $empleadoPrestaciones['TRIMESTRE2'] + $empleadoPrestaciones['TRIMESTRE3'] + $empleadoPrestaciones['TRIMESTRE4'] + $empleadoPrestaciones['ANUAL']) - $empleadoPrestaciones['ANTICIPO'];
                $totalPrestaciones = $totalPrestaciones + $prestaciones;
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetX(15);
                $this->atFPDF->SetWidths(array(8, 20, 30, 30, 15, 30, 30, 20, 20, 20, 20, 15, 20, 20, 25));
                $this->atFPDF->SetAligns(array('C', 'C', 'L', 'L', 'C', 'C', 'R', 'R', 'R', 'R', 'R', 'C', 'R', 'R', 'R'));
                $this->atFPDF->Row(array(
                    $n++,
                    number_format($empleadoPrestaciones['ind_cedula_documento'], 0, '', '.'),
                    utf8_decode($empleadoPrestaciones['ind_apellido1'] . ' ' . $empleadoPrestaciones['ind_apellido2']),
                    utf8_decode($empleadoPrestaciones['ind_nombre1'] . ' ' . $empleadoPrestaciones['ind_nombre2']),
                    $empleadoPrestaciones['fec_ingreso'],
                    0,#falta Numero Cuenta
                    0,#falta Acumulado
                    number_format($empleadoPrestaciones['TRIMESTRE1'], 2, ',', '.'),
                    number_format($empleadoPrestaciones['TRIMESTRE2'], 2, ',', '.'),
                    number_format($empleadoPrestaciones['TRIMESTRE3'], 2, ',', '.'),
                    number_format($empleadoPrestaciones['TRIMESTRE4'], 2, ',', '.'),
                    $empleadoPrestaciones['num_dias_anual'],
                    number_format($empleadoPrestaciones['ANUAL'], 2, ',', '.'),
                    number_format($empleadoPrestaciones['ANTICIPO'], 2, ',', '.'),
                    number_format($prestaciones, 2, ',', '.')
                ));
            } elseif ($this->atFPDF->GetY() > 152) {
                $this->atFPDF->SetFillColor(200, 200, 200);
                $this->atFPDF->SetX(15);
                $this->atFPDF->SetWidths(array(298, 25));
                $this->atFPDF->SetAligns(array('R', 'R'));
                $this->atFPDF->Row(array('Total', number_format($totalPrestaciones, 2, ',', '.')));
                $totalPago = $totalPago + $totalPrestaciones;
                $totalPrestaciones = 0;
                $this->atFPDF->AddPage();
            }
        }



        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetX(15);
        $this->atFPDF->SetWidths(array(298, 25));
        $this->atFPDF->SetAligns(array('R', 'R'));
        $this->atFPDF->Row(array('Total', number_format($totalPrestaciones, 2, ',', '.')));

        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetX(15);
        $this->atFPDF->SetWidths(array(298, 25));
        $this->atFPDF->SetAligns(array('R', 'R'));
        $this->atFPDF->Row(array('Total Prestaciones', number_format($totalPago + $totalPrestaciones, 2, ',', '.')));

        $this->atFPDF->SetXY(55, 161);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 9);
        $this->atFPDF->Cell(200, 3, '______________________________________________                                     ___________________________________________', 0, 1, 'L');

        $this->atFPDF->SetFont('Arial', '', 9);
        $this->atFPDF->SetXY(35, 160);
        $this->atFPDF->Cell(60, 3, ('Elaborado Por:                                                                                                       Aprobado Por:'), 0, 0, 'L');
        $this->atFPDF->Cell(60, 3, (''), 0, 1, 'L');
        $this->atFPDF->SetXY(60, 166);
        $this->atFPDF->MultiCell(150, 3, utf8_decode('nombre'), 0, 'L');//nombre de quien elabora
        $this->atFPDF->SetXY(170, 166);
        $this->atFPDF->Cell(150, 3, utf8_decode('nombre'), 0, 1, 'L');//nombre de quien revisa
        $this->atFPDF->SetXY(60, 170);
        $this->atFPDF->MultiCell(100, 3, utf8_decode('firmante 1'), 0, 'L');//cargo de quien elabora
        $this->atFPDF->SetXY(170, 170);
        $this->atFPDF->Cell(100, 3, utf8_decode('firmante 2'), 0, 1, 'L');//Cargo de quien revisa

        $this->atFPDF->Output();
    }
}