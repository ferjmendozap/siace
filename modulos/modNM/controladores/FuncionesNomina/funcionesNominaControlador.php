<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        18-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . 'modNM' . DS . 'modelos' . DS . 'FuncionesNomina' . DS . 'funcionesNominaModelo.php';

trait metodosNomina
{

    /***
     * Metodo Para Calcular la Edad.
     *
     *
     * @param $fecInicio
     * @param $fecFin
     * @return array
     */
    public function metCalcularEdad($fecInicio, $fecFin)
    {
        $fecha1 = new DateTime($fecInicio);
        $fecha2 = new DateTime($fecFin);
        $interval = $fecha1->diff($fecha2);
        $edad = array('A' => $interval->format('%y'), 'M' => $interval->format('%m'), 'D' => $interval->format('%d'));
        return $edad;
    }

    /***
     * Metodo Para Calcular la Edad.
     *
     *
     * @param $fecInicio
     * @param $fecFin
     * @return array
     */
    public function metPeriodoAnterior($periodo)
    {
        $periodoAnterior = str_getcsv($periodo, '-');

        if ($periodo == date('Y') . '-01') {
            $año = date('Y') - 1;
            $mes = 12;
        } else {
            $año = $periodoAnterior[0];
            $mes = $periodoAnterior[1] - 1;
        }

        if (strlen($mes) == 1) {
            $mes = '0' . $mes;
        } else {
            $mes = $mes;
        }
        return "$año-$mes";
    }

    protected function metPeriodoProcesosPestaciones($fecIngreso, $periodo)
    {

        $fecIngreso = str_getcsv((string)$fecIngreso, '-');
        if ($fecIngreso[0] <= 1997) {
            if ($fecIngreso[0] < 1997) {
                $fecIngreso = '1997-06-19';
            } else {
                if ($fecIngreso[1] <= 06) {
                    if ($fecIngreso[2] <= 19) {
                        $fecIngreso = '1997-06-19';
                    } else {
                        $fecIngreso = '1997-06' . '-' . $fecIngreso[2];
                    }
                } else {
                    $fecIngreso = '1997-' . $fecIngreso[1] . '-' . $fecIngreso[2];
                }
            }
        } else {
            $fecIngreso = "$fecIngreso[0]-$fecIngreso[1]-$fecIngreso[2]";
        }

        $fecIngreso = str_getcsv($fecIngreso, '-');
        if ($fecIngreso[2] > 15 && $fecIngreso[2] < 31) {
            $periodo1 = $this->metPeriodoAnterior($periodo);
            $periodo2 = $periodo;
            $proceso1 = 'FIN';
            $proceso2 = 'PRQ';
        } else {
            $periodo1 = $periodo;
            $periodo2 = $periodo;
            $proceso1 = 'PRQ';
            $proceso2 = 'FIN';
        }
        $array = array(
            'periodo1' => $periodo1,
            'periodo2' => $periodo2,
            'proceso1' => $proceso1,
            'proceso2' => $proceso2
        );
        return $array;
    }

}

trait funcionesNomina
{
    use metodosNomina;
    #Variables
    public $atMonto;
    public $atCantidad;

    #Constantes
    public $atConstSueldoMinimo;#Constante del Valor del Sueldo Minimo
    public $atEncargaduriaTemporal;#Constante que indica si el empleado esta de encargaduria temporal (09-08-2017)
    public $atDiasPagoBonoFinAnio;#Constante que indica los dias de pago de fin de año
    public $atDiasPagoBonoFinAnioDC;#Constante que indica los dias de pago de fin de año contralor

    #Argumentos
    #Referente al Empleado
    public $atArgsIdEmpleado;#Id Del Empleado
    public $atArgsFecIngreso;#Fecha de Ingreso del Empleado
    public $atArgsFecEgreso;#Fecha de Ingreso del Empleado

    #Referente a la Nomina
    protected $atArgsTipoNomina;
    protected $atArgsPagoMensual;
    public $atArgsCodTipoNomina;
    protected $atArgsTipoProceso;
    public $atArgsCodTipoProceso;
    public $atArgsPeriodoNomina;
    protected $atArgsPrediodoDesde;
    protected $atArgsPrediodoHasta;
    protected $atArgsIdConcepto;
    protected $atArgsProcesoPeriodo;

    #Atributos Privados
    private $atFuncionesModelo;

    public function __construct()
    {
        #Optengo el Modelo Donde se encuentran las Consultas a la DB.
        $this->atFuncionesModelo = new funcionesNominaModelo();
        #SUELDO MINIMO
        $this->atConstSueldoMinimo = $this->atFuncionesModelo->metSueldoMinimo();
        $this->atConstSueldoMinimo = $this->atConstSueldoMinimo['num_sueldo_minimo'];
        #ENCARGADURIA TEMPORAL
        /*$this->atEncargaduriaTemporal = $this->atFuncionesModelo->metEncargaduriaTemporal($this->atArgsIdEmpleado);
        $this->atEncargaduriaTemporal = $this->atEncargaduriaTemporal['cod_detalle'];*/
        #DIAS DE PAGO BONO FIN AÑO
        $this->atDiasPagoBonoFinAnio = Session::metObtener('PAGOFIN');
        $this->atDiasPagoBonoFinAnioDC = Session::metObtener('PAGOFINDC');
    }

    public function metEncargaduriaTemporal()
    {
        $enc= $this->atFuncionesModelo->metEncargaduriaTemporal($this->atArgsIdEmpleado);
        return $enc;
    }


    /***
     * Funcion Para obtener si el funcionario es alto nivel
     * 08-08-2017
     * @return int
     */
    public function metAltoNivel()
    {
        $dato = $this->atFuncionesModelo->metObtenerAltoNivel($this->atArgsIdEmpleado);
        return $dato;
    }

    /***
     * Funcion Para obtener la ultima prima por años de servicio de un empleado asignada a traves de un concepto
     * 08-08-2017
     * @return bool|mixed
     */
    public function metUltimaPrimaAntiguedaConcepto($concepto)
    {
        $dato = $this->atFuncionesModelo->metObtenerUltimaPrimaAntiguedaConcepto($this->atArgsIdEmpleado,$concepto);
        $monto = $dato['num_monto'];
        return $monto;
    }


    /***
     * Funcion Para obtener la ultima prima por años de servicio de un empleado asignada en nomina ya calculada
     * 08-08-2017
     * @return bool|mixed
     */
    public function metUltimaPrimaAntigueda($concepto)
    {
        $dato = $this->atFuncionesModelo->metObtenerUltimaPrimaAntigueda($this->atArgsIdEmpleado,$concepto,$this->atArgsPeriodoNomina);
        return $dato;
    }

    /***
     * Funcion Para obtener Periodo desde Concepto asignado
     * 08-08-2017
     * @return bool|mixed
     */
    public function metUltimoPeriodo($concepto)
    {
        $dato = $this->atFuncionesModelo->metObtenerUltimoPeriodoDesde($this->atArgsIdEmpleado,$concepto);
        $periodo = $dato['fec_periodo_desde'];
        return $periodo;
    }


    /***
     * Funcion Para Obtener la unidad tributaria
     *
     * @return bool|mixed
     */
    public function metUnidadTributaria()
    {
        $unidadTributaria = $this->atFuncionesModelo->metObtenerUnidadTributaria();
        return $unidadTributaria['ind_valor'];
    }

    /***
     * Funcion Para Obtener la unidad tributaria
     *
     * @return bool|mixed
     */
    public function metCantidadDias()
    {
        $dias = $this->atFuncionesModelo->metObtenerCantidadDias($this->atArgsIdEmpleado);
        $totalDias = (int)$dias['num_dias_pago'] - (int)$dias['num_dias_descuento'];
        return $totalDias;
    }


    /***
     * Funcion Para Obtener el Grado de Instruccion
     *
     * @return float|mixed
     */
    public function metGradoInstruccion()
    {
        $gradoInstruccion = $this->atFuncionesModelo->metObtenerGradoInstruccion($this->atArgsIdEmpleado);
        /*if($gradoInstruccion){
            $gradoInstruccion = $gradoInstruccion['cod_detalle'];
        }else{
            $gradoInstruccion = '';
        }*/
        return $gradoInstruccion;
    }

    public function metJerarquia()
    {
        $jerarquia = $this->atFuncionesModelo->metObtenerJerarquia($this->atArgsIdEmpleado);
        if($jerarquia){
            $respuesta['cargo'] = $jerarquia['ind_nombre_cargo'];
            $respuesta['descripcion'] = $jerarquia['ind_descripcion_cargo'];
        }else{
            $respuesta['cargo'] = '';
            $respuesta['descripcion'] = '';
        }
        return $respuesta;
    }

    /***
     *
     * Funcion Para Obtener El Sueldo Basico del Empleado.
     *
     * Atributos a Utilizar.
     *
     * $this->atArgsIdEmpleado:     Empleado a buscar para obtener los años de servicios
     *                              Cargados en el sistema.
     *
     * @return float|mixed
     */
    public function metSueldoBasico($sbCompleto = false)
    {
        $sueldoBasico = $this->atFuncionesModelo->metSueldoBasico($this->atArgsIdEmpleado);
        if($sueldoBasico['num_sueldo_promedio'] <= 0){
            $sueldoBasico = $this->atFuncionesModelo->metSueldoBasicoPaso($this->atArgsIdEmpleado);
        }

        if($sueldoBasico['num_sueldo_promedio'] <= 0){
            $sueldoBasico['num_sueldo_promedio'] = 0;
        }

        if($this->atArgsFecIngreso >= $this->atArgsPrediodoDesde){
            $fechaDesde = $this->atArgsFecIngreso;
        }else{
            $fechaDesde = $this->atArgsPrediodoDesde;
        }

        if($this->atArgsFecEgreso){
            $fechaHasta = $this->atArgsFecEgreso;
        }else{
            $dias = str_getcsv($this->atArgsPrediodoHasta,'-');
            if(isset($dias[2]) && ($dias[2]=='28' || $dias[2]=='29' || $dias[2]=='31')){
               $fechaHasta = $dias[0].'-'.$dias[1].'-30';
            }else{
                $fechaHasta = $this->atArgsPrediodoHasta;
            }
        }

        $fecha = new DateTime($fechaHasta);
        $fecha->add(new DateInterval('P1D'));
        $fechaHasta = $fecha->format('Y-m-d');

        $fecha1 = new DateTime($fechaHasta);
        $fecha2 = new DateTime($fechaDesde);
        $fecha = $fecha1->diff($fecha2);

        if (!$sbCompleto) {
            if (!$this->atArgsPagoMensual) {
                $sueldoBasico = $sueldoBasico['num_sueldo_promedio'] / 2;
                $sueldoBasico = $sueldoBasico/15;
                $sueldoBasico = $sueldoBasico*$fecha->d;
            } else {
                $sueldoBasico = $sueldoBasico['num_sueldo_promedio'];
                $sueldoBasico = $sueldoBasico/30;
                $sueldoBasico = $sueldoBasico*$fecha->d;
            }
        } else {
            $sueldoBasico = $sueldoBasico['num_sueldo_promedio'];
        }
        return $sueldoBasico;
    }


    /***
     *
     * Funcion Para Obtener El Sueldo Basico del Empleado (JUBILADO PENSIONADO).
     *
     * Atributos a Utilizar.
     *
     * $this->atArgsIdEmpleado:     Empleado a buscar para obtener los años de servicios
     *                              Cargados en el sistema.
     * 15-08-2017
     * @return float|mixed
     */
    public function metSueldoBasicoJP($sbCompleto = false)
    {
        $sueldoBasico = $this->atFuncionesModelo->metSueldoBasicoJP($this->atArgsIdEmpleado);

        if (!$sbCompleto) {
            if (!$this->atArgsPagoMensual) {
                $sueldoBasico = $sueldoBasico['num_sueldo'] / 2;
            } else {
                $sueldoBasico = $sueldoBasico['num_sueldo'];
            }
        } else {
            $sueldoBasico = $sueldoBasico['num_sueldo'];
        }

        return $sueldoBasico;

    }

    /***
     *
     * Funcion Para Obtener El Sueldo Basico del Empleado.
     *
     * Atributos a Ustilizar.
     *
     * $this->atArgsIdEmpleado:     Empleado a buscar para obtener los años de servicios
     *                              Cargados en el sistema.
     *
     * @return float|mixed
     */
    public function metDiferenciaSueldoBasico($sbCompleto = false)
    {
        $sueldoBasico = $this->atFuncionesModelo->metSueldoBasico($this->atArgsIdEmpleado);
        if($sueldoBasico['num_sueldo_promedio'] <= 0){
            $sueldoBasico = $this->atFuncionesModelo->metSueldoBasicoPaso($this->atArgsIdEmpleado);
        }

        if($sueldoBasico['num_sueldo_promedio'] <= 0){
            $sueldoBasico['num_sueldo_promedio'] = 0;
        }

        $diferenciaSueldoBasico = $this->atFuncionesModelo->metDiferenciaSueldoBasico($this->atArgsIdEmpleado);
        if($diferenciaSueldoBasico['num_sueldo_promedio'] <= 0){
            $diferenciaSueldoBasico = $this->atFuncionesModelo->metDiferenciaSueldoBasicoPaso($this->atArgsIdEmpleado);
        }

        if($diferenciaSueldoBasico['num_sueldo_promedio'] <= 0){
            $diferenciaSueldoBasico['num_sueldo_promedio'] = 0;
        }

        if($diferenciaSueldoBasico['num_sueldo_promedio']) {
            $diferencia = $diferenciaSueldoBasico['num_sueldo_promedio'] - $sueldoBasico['num_sueldo_promedio'];
            if (!$sbCompleto) {
                if (!$this->atArgsPagoMensual) {
                    $diferencia = $diferencia / 2;
                } else {
                    $diferencia = $diferencia;
                }
            } else {
                $diferencia = $diferencia;
            }
        }else{
            $diferencia=0;
        }
        return $diferencia;
    }

    /***
     * Metodo para Calcular los Lunes Laborados.
     *
     * Atributos a Ustilizar.
     *
     * $this->atArgsFecIngreso: Fecha de Ingreso del Empleado
     * $this->atArgsPrediodoDesde: Fecha de la nomina a Generar desde
     * $this->atArgsPrediodoHasta: Fecha de la nomina a Generar hasta
     *
     * @return int
     */
    public function metLunesLaborados()
    {
        $csvFecha1 = str_getcsv($this->atArgsFecIngreso, '-');
        $csvFecha2 = str_getcsv($this->atArgsPrediodoDesde, '-');
        #Comparo la fecha de ingreso para saber si el funcionario n es nuevo ingreso
        if ($csvFecha1[0] . '-' . $csvFecha1[1] < $csvFecha2[0] . '-' . $csvFecha2[1]) {
            $this->atArgsFecIngreso = $csvFecha2[0] . '-' . $csvFecha2[1] . '-01';
        }
        $dias = array(); #creo un arreglo para devolver los lunes Laborados
        $fecha1 = date($this->atArgsFecIngreso);
        $fecha2 = date($this->atArgsPrediodoHasta);
        $fecha = date("Y-m-d", strtotime($fecha1)); #paso a date para darle formato
        $fechaTime = strtotime($fecha1); #paso a hora unix atArgsFecIngreso
        $arrayFechaIngreso = str_getcsv($this->atArgsFecIngreso, '-');
        $cont = 0;
        /**********************************/
        $aux = explode("-", $fecha);
        $dia = $aux[2];
        $mes = $aux[1];
        $anio = $aux[0];
        $dias_mes['01'] = 31;
        if ($anio%4==0) $dias_mes['02']=29; else $dias_mes['02']=28;
        $dias_mes['03'] = 31;
        $dias_mes['04'] = 30;
        $dias_mes['05'] = 31;
        $dias_mes['06'] = 30;
        $dias_mes['07'] = 31;
        $dias_mes['08'] = 31;
        $dias_mes['09'] = 30;
        $dias_mes['10'] = 31;
        $dias_mes['11'] = 30;
        $dias_mes['12'] = 31;
        $arrayFechaFin = str_getcsv($this->atArgsPrediodoHasta, '-');
        $fecha2 = $arrayFechaFin[0]."-".$arrayFechaFin[1]."-".$dias_mes[$mes];
        /******************************************/
        while ($fecha <= $fecha2) {#verifico que no me haya pasado de la fecha fin
            #Ahora, el Unix timestamp para el primer lunes
            #después de fecha 1:
            if ($cont == 0) {
                if (date("l", mktime(0, 0, 0, $arrayFechaIngreso[1], $arrayFechaIngreso[2], $arrayFechaIngreso[0])) == 'Monday') {
                    $proximo_lunes = $fechaTime;
                } else {
                    $proximo_lunes = strtotime("next Monday", $fechaTime);
                }
            } else {
                $proximo_lunes = strtotime("next Monday", $fechaTime);
            }
            $fechaLunes = date("Y-m-d", $proximo_lunes);
            if ($fechaLunes <= $fecha2) {
                $dias[$fechaLunes] = $fechaLunes;
            } else {
                break;
            }
            $cont++;
            $fechaTime = $proximo_lunes;
            $fecha = date("Y-m-d", $proximo_lunes);
        }
        return count($dias);

    }

    /***
     * Metodo Para ejecutar un Concepto
     *
     * @param $idConcepto
     */
    public function metEjecutarConcepto($abreviatura)
    {
        $formula = $this->atFuncionesModelo->metEjecutarConceptoCodigo($abreviatura);
        if($formula['ind_formula']!=''){
            $monto = eval($formula['ind_formula']);
        } else {
            $monto = 0;
        }
        return $monto;
    }

    /***
     * Funcion Para Obtener Conceptos ya Generados en el Sistema.
     *
     * @param $abrevConcepto
     * @param bool|false $periodo
     * @param bool|false $codProcesos
     * @return mixed
     */
    public function metObtenerConcepto($abrevConcepto, $periodo = false, $codProcesos = false)
    {
        if ($periodo == 1) {
            $periodo = $this->metPeriodoAnterior($this->atArgsPeriodoNomina);
        } elseif ($periodo) {
            $periodo = $periodo;
        } else {
            $periodo = $this->atArgsPeriodoNomina;
        }

        if (!$codProcesos) {
            $codProcesos = $this->atArgsCodTipoProceso;
        }

        $resultado = $this->atFuncionesModelo->metObtenerConceptos($this->atArgsIdEmpleado, $this->atArgsTipoNomina, $abrevConcepto, $codProcesos, $periodo);
        if (isset($resultado['num_monto'])) {
            $monto = (float)$resultado['num_monto'];
        } else {
            $monto = 0;
        }
        return $monto;
    }

    /***
     * Funcion Para Obtener Los años de servicios en la Institucion
     * 09-08-2017
     * Atributos a Utilizar.
     *
     * $this->atArgsFecIngreso:     Fecha de Ingreso del Empleado
     * $this->atArgsPrediodoHasta:  Fecha de la nomina a Generar hasta
     * @return float
     */

    public function metObtenerAniosServiciosInstitucion()
    {
        $añosServicioContraloria = $this->metCalcularEdad($this->atArgsFecIngreso, $this->atArgsPrediodoHasta);
        return $añosServicioContraloria['A'];

    }

    /***
     * Funcion Para Obtener Los años de antiguedad en otras instituciones.
     *
     * Atributos a Ustilizar.
     *
     * $this->atArgsFecIngreso:     Fecha de Ingreso del Empleado
     * $this->atArgsPrediodoHasta:  Fecha de la nomina a Generar hasta

     * @return float
     */
    public function metObtenerAñosAdministracionPublica()
    {
        $fecha = array(
            'sumaDias' => 0,
            'sumaMeses' => 0,
            'sumaAños' => 0,
            'totalDias' => 0,
            'totalMeses' => 0,
            'totalAños' => 0,
        );

        $añosServicioContraloria = $this->metCalcularEdad($this->atArgsFecIngreso, $this->atArgsPrediodoHasta);
        $añosServicioOtrasInstituciones = $this->atFuncionesModelo->metObtenerAñosAdministracionPublica($this->atArgsIdEmpleado);

        if ($añosServicioContraloria['A'] >= 1) {
            for ($i = 0; $i < count($añosServicioOtrasInstituciones); $i++) {
                $añosInstituciones = $this->metCalcularEdad($añosServicioOtrasInstituciones[$i]['fec_ingreso'], $añosServicioOtrasInstituciones[$i]['fec_egreso']);
                $fecha['sumaDias'] += $añosInstituciones['D'];
                $fecha['sumaMeses'] += $añosInstituciones['M'];
                $fecha['sumaAños'] += $añosInstituciones['A'];
            }
        }

        $fecha['sumaDias'] += $añosServicioContraloria['D'];
        $fecha['sumaMeses'] += $añosServicioContraloria['M'];
        $fecha['sumaAños'] += $añosServicioContraloria['A'];

        $fecha['totalMeses'] = $fecha['sumaDias'] / 30;
        $fecha['totalMeses'] += $fecha['sumaMeses'];
        $fecha['totalAños'] += $fecha['totalMeses'] / 12;
        $fecha['totalAños'] += $fecha['sumaAños'];

        return (int)$fecha['totalAños'];
    }

    /***
     * Funcion Para Obtener El Monto Total generados en nominas Por un Tipo de Concepto.
     *
     * Atributos a Ustilizar.
     *
     * $this->atArgsIdEmpleado:     Empleado a buscar para obtener los años de servicios
     *                              Cargados en el sistema.
     * $this->atArgsTipoProceso:
     * $this->atArgsPeriodoNomina:
     *
     * @param $tipo : Tipo de Concepto
     * @param int $flagIncidencia : Si tiene Incidencia
     * @param bool|false $periodo : Periodo de la Nomina
     *
     * @return mixed
     */
    public function metObtenerTipoConceptoCalculados($tipo, $flagIncidencia = 0, $periodo = false, $prestaciones = false)
    {
        if ($periodo) {
            $periodo = $periodo;
        } else {
            $periodo = $this->atArgsTipoNomina;
        }

        if($prestaciones){
            $periodoProceso = $this->metPeriodoProcesosPestaciones($this->atArgsFecIngreso, $this->atArgsPeriodoNomina);
            $monto1 = $this->atFuncionesModelo->metObtenerTipoConceptoCalculados($this->atArgsIdEmpleado, $this->atArgsTipoNomina, $periodoProceso['proceso1'], $periodoProceso['periodo1'], $tipo, $flagIncidencia);
            $monto2 = $this->atFuncionesModelo->metObtenerTipoConceptoCalculados($this->atArgsIdEmpleado, $this->atArgsTipoNomina, $periodoProceso['proceso2'], $periodoProceso['periodo2'], $tipo, $flagIncidencia);
            $total=$monto1['num_monto']+$monto2['num_monto'];
        }else{
            $monto = $this->atFuncionesModelo->metObtenerTipoConceptoCalculados($this->atArgsIdEmpleado, $periodo, $this->atArgsCodTipoProceso, $this->atArgsPeriodoNomina, $tipo, $flagIncidencia);
            $total=$monto['num_monto'];
        }
        return $total;
    }

    /***
     * Funcion Para Obtener Los Dias para el Calculo de la Alicuota Vacacional.
     *
     * Metodos Utilizados
     *
     * $this->metObtenerAñosAdministracionPublica()
     *
     * @param int $flagPrestaciones
     * @return int
     */
    public function metDiasAlicuota($flagPrestaciones = 0)
    {
        $TotalAñosServicio = $this->metObtenerAñosAdministracionPublica();
        $TotalAñosServicio=str_getcsv($TotalAñosServicio,'.');
        $total=$TotalAñosServicio[0];
        if(isset($TotalAñosServicio[1])){
            $strGet=$TotalAñosServicio[1];
        }else{
            $strGet=0;
        }
        if ($total <= 5) {
            if ($total == 5 && $strGet >= 1 && $flagPrestaciones == 1) {
                $añosServicio = 41;
            } else {
                $añosServicio = 40;
            }
        } else if (($total == 6)) {
            if ($strGet >= 1 && $flagPrestaciones == 1) {
                $añosServicio = 42;
            } else {
                $añosServicio = 41;
            }
        } else if (($total == 7)) {
            if ($strGet >= 1 && $flagPrestaciones == 1) {
                $añosServicio = 43;
            } else {
                $añosServicio = 42;
            }
        } else if (($total == 8)) {
            if ($strGet >= 1 && $flagPrestaciones == 1) {
                $añosServicio = 44;
            } else {
                $añosServicio = 43;
            }
        } else if (($total == 9)) {
            if ($strGet >= 1 && $flagPrestaciones == 1) {
                $añosServicio = 45;
            } else {
                $añosServicio = 44;
            }
        } else if (($total == 10)) {
            if ($strGet >= 1 && $flagPrestaciones == 1) {
                $añosServicio = 46;
            } else {
                $añosServicio = 45;
            }
        } else if (($total == 11)) {
            if ($strGet >= 1 && $flagPrestaciones == 1) {
                $añosServicio = 47;
            } else {
                $añosServicio = 46;
            }
        } else if (($total == 12)) {
            if ($strGet >= 1 && $flagPrestaciones == 1) {
                $añosServicio = 48;
            } else {
                $añosServicio = 47;
            }
        } else if (($total == 13)) {
            if ($strGet >= 1 && $flagPrestaciones == 1) {
                $añosServicio = 49;
            } else {
                $añosServicio = 48;
            }
        } else if (($total == 14)) {
            if ($strGet >= 1 && $flagPrestaciones == 1) {
                $añosServicio = 50;
            } else {
                $añosServicio = 49;
            }
        } else if (($total == 15)) {
            if ($strGet >= 1 && $flagPrestaciones == 1) {
                $añosServicio = 51;
            } else {
                $añosServicio = 50;
            }
        } else if (($total == 16)) {
            if ($strGet >= 1 && $flagPrestaciones == 1) {
                $añosServicio = 52;
            } else {
                $añosServicio = 51;
            }
        } else if (($total == 17)) {
            if ($strGet >= 1 && $flagPrestaciones == 1) {
                $añosServicio = 53;
            } else {
                $añosServicio = 52;
            }
        } else if (($total == 18)) {
            if ($strGet >= 1 && $flagPrestaciones == 1) {
                $añosServicio = 54;
            } else {
                $añosServicio = 53;
            }
        } else if (($total == 19)) {
            if ($strGet >= 1 && $flagPrestaciones == 1) {
                $añosServicio = 55;
            } else {
                $añosServicio = 54;
            }
        } else if ($total >= 20) {
            $añosServicio = 55;
        }

        return $añosServicio;
    }

    /***
     * Funcion Para Obtener el sueldo basico para las prestaciones sociales.
     *
     * @return float
     */
    protected function metSueldoPrestaciones()
    {
        $periodoProceso = $this->metPeriodoProcesosPestaciones($this->atArgsFecIngreso, $this->atArgsPeriodoNomina);
        $sbPeriodo1 = $this->metObtenerConcepto('SB', $periodoProceso['periodo1'], $periodoProceso['proceso1']);
        $sbPeriodo2 = $this->metObtenerConcepto('SB', $periodoProceso['periodo2'], $periodoProceso['proceso2']);
        $dsbPeriodo1 = $this->metObtenerConcepto('DSB', $periodoProceso['periodo1'], $periodoProceso['proceso1']);
        $dsbPeriodo2 = $this->metObtenerConcepto('DSB', $periodoProceso['periodo2'], $periodoProceso['proceso2']);
        return (float)($sbPeriodo1 + $sbPeriodo2 + $dsbPeriodo1 + $dsbPeriodo2);
    }

    /***
     * Funcion para Obtener el numero de Hijos que Tenga el Funcionario
     *
     * @return int
     */
    #Mod - 17Ene18
    public function metNroHijos($jug = false)
    {
        $count=0;
        foreach ($this->atFuncionesModelo->metObtenerHijos($this->atArgsIdEmpleado) AS $i){
            $edad = $this->metCalcularEdad($i['fec_nacimiento'],date('Y-m-d'));

            if($jug == 1){
                if($edad['A'] <= 12){
                    $count++;
                }
            }else{
                if($edad['A'] < 18){
                    $count++;
                }elseif ( ($i['num_flag_estudia']==1) &&  ($edad['A'] <26 )){
                    $count++;
                }
            }
        }
        return $count;
    }
    //Método para modificar un mes a letras
    public function metMesLetras($mes){

        if(strlen($mes)==1){
            $mes="0".$mes;
        }
        $metMesLetras=array(
            '01'=>'ENERO',
            '02'=>'FEBRERO',
            '03'=>'MARZO',
            '04'=>'ABRIL',
            '05'=>'MAYO',
            '06'=>'JUNIO',
            '07'=>'JULIO',
            '08'=>'AGOSTO',
            '09'=>'SEPTIEMBRE',
            '10'=>'OCTUBRE',
            '11'=>'NOVIEMBRE',
            '12'=>'DICIEMBRE'
        );

        return $metMesLetras[$mes];
    } // END FUNCTION
}
