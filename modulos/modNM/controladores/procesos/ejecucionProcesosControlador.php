<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . DS . 'modNM/controladores/FuncionesNomina/funcionesNominaControlador.php';

class ejecucionProcesosControlador extends Controlador
{

    use funcionesNomina {
        funcionesNomina::__construct as private __fnConstruct;
    }

    private $atEjecucionProcesos;
    private $atTipoNomina;

    public function __construct()
    {
        parent::__construct();
        $this->__fnConstruct();
        $this->atEjecucionProcesos = $this->metCargarModelo('ejecucionProcesos', 'procesos');
        $this->atTipoNomina = $this->metCargarModelo('tipoNomina', 'maestros');
    }

    public function metIndex()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('tipoNomina', $this->atTipoNomina->metListarTipoNomina(1));
        $this->atVista->metRenderizar('index');
    }

    public function metListadoPersonas()
    {
        $js[] = 'Aplicacion/appFunciones';
        $complementosCss = array(
            'jquery-ui/jquery-ui-theme5e0a',
            'SelectMultipleTabla/css/ui.multiselect',
        );
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'SelectMultipleTabla/js/plugins/localisation/jquery.localisation',
            'SelectMultipleTabla/js/plugins/tmpl/jquery.tmpl.1.1.1',
            'SelectMultipleTabla/js/plugins/blockUI/jquery.blockUI',
            'SelectMultipleTabla/js/ui.multiselect',
        );
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

        $pk_num_tipo_nomina = $this->metObtenerInt('pk_num_tipo_nomina');
        $pk_num_proceso_periodo = $this->metObtenerInt('pk_num_proceso_periodo');
        $ind_cesados = $this->metObtenerInt('ind_cesados');

        if ($pk_num_tipo_nomina && $pk_num_proceso_periodo) {
            $seleccionados = $this->atEjecucionProcesos->metListaEmpleadosEjecucionProcesos($pk_num_proceso_periodo);
            foreach ($seleccionados as $seleccionado) {
                $arraySeleccion[] = $seleccionado['pk_num_empleado'];
            }
            if (!isset($arraySeleccion)) {
                $arraySeleccion = false;
            }
            $listadoEmpleados = $this->atEjecucionProcesos->metListaEmpleadosNomina($pk_num_tipo_nomina, $ind_cesados);
            $codProceso = $this->atEjecucionProcesos->metObtenerProceso($pk_num_proceso_periodo);
            for ($i = 0; $i < count($listadoEmpleados); $i++) {
                if ($codProceso['cod_proceso'] == 'GPA' || $codProceso['cod_proceso'] == 'RPA') {
                    $fecIngreso = str_getcsv($listadoEmpleados[$i]['fec_ingreso'], '-');
                    if ($fecIngreso[0] <= 1997) {
                        if ($fecIngreso[0] < 1997) {
                            $fecIngreso = '1997-06-19';
                        } else {
                            if ($fecIngreso[1] <= 06) {
                                if ($fecIngreso[2] <= 19) {
                                    $fecIngreso = '1997-06-19';
                                } else {
                                    $fecIngreso = '1997-06' . '-' . $fecIngreso[2];
                                }
                            } else {
                                $fecIngreso = '1997-' . $fecIngreso[1] . '-' . $fecIngreso[2];
                            }
                        }
                    } else {
                        $fecIngreso = "$fecIngreso[0]-$fecIngreso[1]-$fecIngreso[2]";
                    }
                    $periodo = strtotime($fecIngreso);
                    $periodo = strtotime('+1 month', $periodo);
                    $periodo = date('Y-m-d', $periodo);
                    $periodoNomina = str_getcsv($codProceso['fec_hasta'], '-');
                    $edad = $this->metCalcularEdad($periodo, $codProceso['fec_hasta']);
                    $periodo = str_getcsv($periodo, '-');
                    $año = $edad['A'] - 1;
                    if ($periodo[1] == $periodoNomina[1]) {
                        if ($año > 0) {
                            $listadoEmpleado[] = $listadoEmpleados[$i];
                        }
                    }
                } else {
                    $listadoEmpleado[] = $listadoEmpleados[$i];
                }
            }
            if (!isset($listadoEmpleado)) {
                $listadoEmpleado = false;
            }
            $this->atVista->assign('listaEmpleadosSeleccionados', $arraySeleccion);
            $this->atVista->assign('listaEmpleados', $listadoEmpleado);
            $this->atVista->assign('pk_num_proceso_periodo', $pk_num_proceso_periodo);
            $this->atVista->assign('pk_num_tipo_nomina', $pk_num_tipo_nomina);
            $this->atVista->metRenderizar('listaEmpleados', 'modales');
        }
    }

    public function metProbarFunciones()
    {
        $prueba = $this->metCargarModelo('concepto', 'maestros');
        $formula = $prueba->metMostrarConcepto('13');
        $this->atArgsIdEmpleado = '1';
        eval($formula['ind_formula']);
        echo $this->atMonto;

    }

    public function metGenerarNomina()
    {
        $valido = $this->metObtenerInt('valido');
        if ($valido == 1) {
            $validacion = $this->metValidarFormArrayDatos('form', 'int');
            $idEmpleados = $validacion['pk_num_empleado'];
            #Obtengo los Datos de la Nomina
            $datosNomina = $this->atEjecucionProcesos->metObtenerDatosNomina($validacion['pk_num_tipo_nomina'], $validacion['pk_num_proceso_periodo']);
            if(
                $datosNomina['ind_estado'] == 'AP' || $datosNomina['ind_estado'] == 'GE'
            ) {
                #Cargo los datos de la Nomina
                $this->atArgsTipoNomina = $datosNomina['pk_num_tipo_nomina'];
                $this->atArgsPagoMensual = $datosNomina['num_flag_pago_mensual'];
                $this->atArgsCodTipoNomina = $datosNomina['cod_tipo_nomina'];
                $this->atArgsTipoProceso = $datosNomina['pk_num_tipo_proceso'];
                $this->atArgsCodTipoProceso = $datosNomina['cod_proceso'];
                $this->atArgsPeriodoNomina = $datosNomina['PERIODO'];
                $this->atArgsPrediodoDesde = $datosNomina['fec_desde'];
                $this->atArgsPrediodoHasta = $datosNomina['fec_hasta'];
                $this->atArgsProcesoPeriodo = $datosNomina['pk_num_proceso_periodo'];

                $LimpiarNominaEmpleado = $this->atEjecucionProcesos->metLimpiarNominaEmpleado($this->atArgsProcesoPeriodo);
                if (is_array($LimpiarNominaEmpleado)) {
                    $validacion['status'] = 'errorSQL';
                    $validacion['mensaje'] = 'Error al Limpiar el empleado de la Nomina';
                    echo json_encode($validacion);
                    exit;
                }
                for ($i = 0; $i < count($idEmpleados); $i++) {
                    #Obtengo los Datos del Empleado
                    $empleado = $this->atEjecucionProcesos->metObtenerEmpleado($idEmpleados[$i]);
                    $empleadoCesado = $this->atEjecucionProcesos->metObtenerEmpleadoCesado($empleado['pk_num_empleado'], $empleado['fec_ingreso']);

                    #Cargo los Argumentos Con los Datos del Empleado
                    $this->atArgsIdEmpleado = $empleado['pk_num_empleado'];
                    $this->atArgsFecIngreso = $empleado['fec_ingreso'];
                    $this->atArgsFecEgreso = $empleadoCesado['fec_fecha'];

                    $conceptos = $this->atEjecucionProcesos->metObtenerConceptos($this->atArgsTipoNomina, $this->atArgsTipoProceso, $this->atArgsIdEmpleado, $this->atArgsPeriodoNomina);
                    foreach ($conceptos as $concepto) {
                        $this->atArgsIdConcepto = $concepto['pk_num_concepto'];
                        $this->atArgsFecIngreso = $empleado['fec_ingreso'];
                        if (strlen($concepto['ind_formula']) < 20) {
                            $this->atMonto = $concepto['num_monto'];
                            $this->atCantidad = $concepto['num_cantidad'];
                        } else {
                            eval($concepto['ind_formula']);
                        }
                        if ($this->atMonto != 0) {
                            $registrarCalculos = $this->atEjecucionProcesos->metRegistrarCalculos($this->atArgsIdEmpleado, $this->atArgsProcesoPeriodo, $this->atArgsIdConcepto, $this->atMonto, $this->atCantidad);
                            if (is_array($registrarCalculos)) {
                                $validacion['status'] = 'errorSQL';
                                $validacion['mensaje'] = 'Error al Registrar el Concepto';
                                echo json_encode($validacion);
                                exit;
                            }
                        }
                        $this->atMonto = 0;
                    }

                    if (
                        $this->atArgsCodTipoProceso == 'FIN' || $this->atArgsCodTipoProceso == 'GPS' ||
                        $this->atArgsCodTipoProceso == 'GPA' || $this->atArgsCodTipoProceso == 'RPA' ||
                        $this->atArgsCodTipoProceso == 'RPS' || $this->atArgsCodTipoProceso == 'RTA'
                    ) {
                        if ($this->atArgsCodTipoProceso == 'FIN' || $this->atArgsCodTipoProceso == 'RTA') {

                            $periodoAnterior = $this->metPeriodoAnterior($this->atArgsPeriodoNomina);
                            $consultaPeriodoAnterior = $this->atEjecucionProcesos->metBuscarPrestacionesSociales($this->atArgsIdEmpleado, $periodoAnterior, $this->atArgsCodTipoProceso);
                            if ($consultaPeriodoAnterior['num_dias_trimestre'] == '5') {
                                if ($consultaPeriodoAnterior['num_monto_trimestral'] == '0.000000') {
                                    $diasTrimestre = 10;
                                } else {
                                    $diasTrimestre = 5;
                                }
                            } elseif ($consultaPeriodoAnterior['num_dias_trimestre'] == '10') {
                                if ($consultaPeriodoAnterior['num_monto_trimestral'] == '0.000000') {
                                    $diasTrimestre = 15;
                                } else {
                                    $diasTrimestre = 5;
                                }
                            } elseif ($consultaPeriodoAnterior['num_dias_trimestre'] == '15') {
                                $diasTrimestre = 5;
                            } else {
                                $diasTrimestre = 5;
                            }
                            $consultaPeriodoActual = $this->atEjecucionProcesos->metBuscarPrestacionesSociales($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina, $this->atArgsCodTipoProceso);

                            $sueldoBasico = $this->metSueldoPrestaciones();
                            $asignaciones = $this->metObtenerTipoConceptoCalculados('AS', 1, false, 1);
                            $sueldoNormal = $sueldoBasico + $asignaciones;
                            $diasAlic = $this->metDiasAlicuota(1);
                            $alicVacacional = (($sueldoNormal / 30) * $diasAlic) / 12;
                            $alicFinAño = ((($sueldoNormal + $alicVacacional) / 30) * 105) / 12;
                            $bonoEspecial = $this->metObtenerConcepto('BESP', $this->atArgsPeriodoNomina, 'BESP');
                            $bonoFiscal = $this->metObtenerConcepto('BFIS', $this->atArgsPeriodoNomina, 'BFIS');
                            $totalRemuneracionMensual = $sueldoNormal + $alicVacacional + $alicFinAño + $bonoEspecial + $bonoFiscal;
                            $totalRemuneracionDiaria = $totalRemuneracionMensual / 30;

                            if (!$consultaPeriodoActual) {
                                $idPrestaciones = $this->atEjecucionProcesos->metRegistrarPrestacionesSociales(
                                    $this->atArgsIdEmpleado, $this->atArgsTipoNomina, $this->atArgsPeriodoNomina,
                                    $sueldoBasico, $asignaciones, $sueldoNormal, $alicVacacional,
                                    $alicFinAño, $bonoEspecial, $bonoFiscal, $totalRemuneracionMensual,
                                    $totalRemuneracionDiaria, $diasTrimestre, $this->atArgsCodTipoProceso
                                );
                            } else {
                                $idPrestaciones = $this->atEjecucionProcesos->metActualizarPrestacionesSociales(
                                    $this->atArgsIdEmpleado, $this->atArgsTipoNomina, $this->atArgsPeriodoNomina,
                                    $sueldoBasico, $asignaciones, $sueldoNormal, $alicVacacional,
                                    $alicFinAño, $bonoEspecial, $bonoFiscal, $totalRemuneracionMensual,
                                    $totalRemuneracionDiaria, $diasTrimestre, $this->atArgsCodTipoProceso,
                                    $consultaPeriodoActual['pk_num_prestaciones_sociales_calculo']
                                );
                            }
                            if (is_array($idPrestaciones)) {
                                $validacion['status'] = 'errorSQL';
                                $validacion['mensaje'] = 'Error al Registrar Las Prestaciones Sociales';
                                echo json_encode($validacion);
                                exit;
                            }

                            $procentaje = $this->atEjecucionProcesos->metUltimosIntereses();
                            $acumuladoPrestaciones = $this->atEjecucionProcesos->metAcumuladoPrestacionesIntereses($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina);
                            $acumuladoPrestacionesRetro = $this->atEjecucionProcesos->metAcumuladoPrestacionesRetroIntereses($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina);
                            $montoAcumulado =
                                ($acumuladoPrestaciones['num_monto_trimestral'] + $acumuladoPrestaciones['num_monto_anual']) +
                                ($acumuladoPrestacionesRetro['num_monto_trimestral'] + $acumuladoPrestacionesRetro['num_monto_anual']);
                            $fecha = new DateTime($this->atArgsPeriodoNomina);
                            $fecha->modify('last day of this month');
                            if (date('Y') % 4 == 0) {
                                $diasAnno = 366;
                            } else {
                                $diasAnno = 365;
                            }
                            $montoProcentaje = ((($montoAcumulado) * ($procentaje['num_activa'] / 100)) / $diasAnno) * $fecha->format('d');

                            $validarInteresMes = $this->atEjecucionProcesos->metBuscarPrestacionesSocialesIntereses($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina);
                            if (!$validarInteresMes) {
                                $idIntereses = $this->atEjecucionProcesos->metRegistrarIntereses($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina, $montoAcumulado, $montoProcentaje, $procentaje['num_activa']);
                            } else {
                                $idIntereses = $this->atEjecucionProcesos->metActualizarIntereses($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina, $montoAcumulado, $montoProcentaje, $procentaje['num_activa'], $validarInteresMes['pk_num_prestaciones_sociales_intereses']);
                            }
                            if (is_array($idIntereses)) {
                                $validacion['status'] = 'errorSQL';
                                $validacion['mensaje'] = 'Error al Registrar Las Prestaciones Sociales';
                                echo json_encode($validacion);
                                exit;
                            }

                        } elseif ($this->atArgsCodTipoProceso == 'GPA' || $this->atArgsCodTipoProceso == 'RPA') {
                            $consultaPeriodoActual = $this->atEjecucionProcesos->metBuscarPrestacionesSociales($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina, $this->atArgsCodTipoProceso);
                            $añosServicioContraloria = $this->metCalcularEdad($this->atArgsFecIngreso, $this->atArgsPrediodoHasta);
                            $años = $añosServicioContraloria['A'];
                            if ($años < 2) {
                                $totalAños = 0;
                            } elseif (($años >= 2 && $años <= 16)) {
                                $años = $años - 1;
                                $totalAños = $años * 2;
                            } elseif ($años > 16) {
                                $totalAños = 30;
                            }

                            if ($totalAños != 0) {
                                $historico = $this->atEjecucionProcesos->metHistoricoPrestacionesSociales($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina, $this->atArgsCodTipoProceso);
                                if ($this->atArgsCodTipoProceso != 'RPA') {
                                    $historicoRetroactivo = $this->atEjecucionProcesos->metHistoricoPrestacionesSociales($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina, 'RPA');
                                    if (isset($historicoRetroactivo['num_remuneracion_diaria'])) {
                                        $montoAnualRPA = $historicoRetroactivo['num_remuneracion_diaria'];
                                    } else {
                                        $montoAnualRPA = 0;
                                    }
                                } else {
                                    $montoAnualRPA = 0;
                                }

                                if (isset($historico['num_remuneracion_diaria'])) {
                                    $montoAnual = $historico['num_remuneracion_diaria'];
                                } else {
                                    $montoAnual = 0;
                                }
                                $montoAnual = ($montoAnual + $montoAnualRPA) * $totalAños;
                                $idPrestacionesSociales = $this->atEjecucionProcesos->metActualizarPrestacionesSocialesAdicionales($this->atArgsIdEmpleado, $this->atArgsTipoNomina, $consultaPeriodoActual['pk_num_prestaciones_sociales_calculo'], $this->atArgsPeriodoNomina, $montoAnual, $totalAños, $this->atArgsCodTipoProceso);
                                if (is_array($idPrestacionesSociales)) {
                                    $validacion['status'] = 'errorSQL';
                                    $validacion['mensaje'] = 'Error al Registrar Las Prestaciones Sociales';
                                    echo json_encode($validacion);
                                    exit;
                                }
                                if ($this->atArgsCodTipoProceso == 'GPA') {
                                    $idConcepto = GPA;
                                } else {
                                    $idConcepto = RPA;
                                }
                                $registrarCalculos = $this->atEjecucionProcesos->metRegistrarCalculos($this->atArgsIdEmpleado, $this->atArgsProcesoPeriodo, $idConcepto, $montoAnual, 1);
                                if (is_array($registrarCalculos)) {
                                    $validacion['status'] = 'errorSQL';
                                    $validacion['mensaje'] = 'Error al Registrar el Concepto';
                                    echo json_encode($validacion);
                                    exit;
                                }

                            }
                        } elseif ($this->atArgsCodTipoProceso == 'GPS' || $this->atArgsCodTipoProceso == 'RPS') {
                            $consultaPeriodoActual = $this->atEjecucionProcesos->metBuscarPrestacionesSociales($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina, $this->atArgsCodTipoProceso);
                            if ($this->atArgsCodTipoProceso == 'GPS') {
                                $consultaPeriodoActualRPS = $this->atEjecucionProcesos->metBuscarPrestacionesSociales($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina, 'RPS');
                                $consultaPeriodoActualRPS['num_remuneracion_diaria'];
                                if (isset($consultaPeriodoActualRPS['num_remuneracion_diaria'])) {
                                    $montoTrimestralRPS = $consultaPeriodoActualRPS['num_remuneracion_diaria'];
                                } else {
                                    $montoTrimestralRPS = 0;
                                }
                            } else {
                                $montoTrimestralRPS = 0;
                            }

                            if (isset($consultaPeriodoActual['num_remuneracion_diaria'])) {
                                $montoTrimestral = $consultaPeriodoActual['num_remuneracion_diaria'];
                            } else {
                                $montoTrimestral = 0;
                            }

                            if ($montoTrimestral != 0) {
                                $montoTrimestral = ($montoTrimestral + $montoTrimestralRPS) * $consultaPeriodoActual['num_dias_trimestre'];
                                $idPrestacionesSociales = $this->atEjecucionProcesos->metActualizarPrestacionesSocialesTrimestre($this->atArgsIdEmpleado, $this->atArgsTipoNomina, $consultaPeriodoActual['pk_num_prestaciones_sociales_calculo'], $this->atArgsPeriodoNomina, $montoTrimestral, $this->atArgsCodTipoProceso);
                                if (is_array($idPrestacionesSociales)) {
                                    $validacion['status'] = 'errorSQL';
                                    $validacion['mensaje'] = 'Error al Registrar Las Prestaciones Sociales';
                                    echo json_encode($validacion);
                                    exit;
                                }
                                if ($this->atArgsCodTipoProceso == 'GPS') {
                                    $idConcepto = GPS;
                                } else {
                                    $idConcepto = RPS;
                                }
                                $registrarCalculos = $this->atEjecucionProcesos->metRegistrarCalculos($this->atArgsIdEmpleado, $this->atArgsProcesoPeriodo, $idConcepto, $montoTrimestral, 1);
                                if (is_array($registrarCalculos)) {
                                    $validacion['status'] = 'errorSQL';
                                    $validacion['mensaje'] = 'Error al Registrar el Concepto';
                                    var_dump($registrarCalculos);
                                    echo json_encode($validacion);
                                    exit;
                                }
                            }
                        }
                    }
                }
                $validacion['status'] = 'generada';
                $cambiarEstadoNomina = $this->atEjecucionProcesos->metCambiarEstadoGenerado($this->atArgsProcesoPeriodo);
                if (is_array($cambiarEstadoNomina)) {
                    $validacion['status'] = 'errorSQL';
                    $validacion['mensaje'] = 'Error al cambiar el estado de la Nomina';
                    echo json_encode($validacion);
                    exit;
                }
                echo json_encode($validacion);
                exit;
            }elseif($datosNomina['ind_estado'] == 'CR'){
                $validacion['status'] = 'error';
                $validacion['mensaje'] = 'Disculpe pero no puede generar una nomina Cerrada';
                echo json_encode($validacion);
                exit;
            }
            var_dump($datosNomina['ind_estado']);
        }
    }
}