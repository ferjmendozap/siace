<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo.'modNM'.DS.'controladores'.DS.'FuncionesNomina'.DS.'funcionesNominaControlador.php';

class asignacionConceptoControlador extends Controlador
{
    use funcionesNomina{
        funcionesNomina::__construct as private __fnConstruct;
    }

    private $atAsignacionConceptos;
    private $atEjecucionProcesos;
    private $atConceptos;

    public function __construct()
    {
        parent::__construct();
        $this->__fnConstruct();
        $this->atAsignacionConceptos = $this->metCargarModelo('asignacionConcepto', 'procesos');
        $this->atEjecucionProcesos = $this->metCargarModelo('ejecucionProcesos', 'procesos');
        $this->atConceptos = $this->metCargarModelo('concepto', 'maestros');
    }

    public function metIndex()
    {
        $this->atVista->metRenderizar('seleccion');
    }

    public function metConceptosEmpleado()
    {
        $idEmpleado = $this->metObtenerInt('idEmpleado');
        $this->atVista->assign('lista', $this->atAsignacionConceptos->metListarConceptosEmpleado($idEmpleado));
        $this->atVista->metRenderizar('listaConceptoProceso', 'modales');
    }
    #Mod - 17Ene18
    public function metEvaluarFormula()
    {
        /*$idConcepto = $this->metObtenerInt('idConcepto');
        $this->atArgsIdEmpleado = $this->metObtenerInt('idEmpleado');
        $concepto = $this->atConceptos->metMostrarConcepto($idConcepto);
        eval($concepto['ind_formula']);
        if(!empty($this->atMonto) && !empty($this->atCantidad)){
            $json=array('monto'=>$this->atMonto,'cantidad'=>$this->atCantidad);
        }else{
            $json=array('monto'=>false,'cantidad'=>false);
        }
        echo json_encode($json);
        exit;*/
        $idConcepto = $this->metObtenerInt('idConcepto');
        $this->atArgsIdEmpleado = $this->metObtenerInt('idEmpleado');
        $idConcepto = $this->metObtenerInt('idConcepto');

        $this->atArgsPrediodoHasta = date('Y-m-d');
        $datosEmpleado = $this->atEjecucionProcesos->metObtenerEmpleado($this->atArgsIdEmpleado);
        $this->atArgsFecIngreso =$datosEmpleado['fec_ingreso'];

        $concepto = $this->atConceptos->metMostrarConcepto($idConcepto);
        $this->atArgsPeriodoNomina = date('Y').'-'.date('m');
        $tipoNomina = $this->atAsignacionConceptos->metBuscarTipoNominaEmpleado($this->atArgsIdEmpleado);
        $this->atArgsTipoNomina = $tipoNomina['fk_nmb001_num_tipo_nomina'];
        eval($concepto['ind_formula']);
        $json=array('monto'=>$this->atMonto,'cantidad'=>$this->atCantidad);
        /*
        if(!empty($this->atMonto) && !empty($this->atCantidad)){
            $json=array('monto'=>$this->atMonto,'cantidad'=>$this->atCantidad);
        }else{
            $json=array('monto'=>'0.00','cantidad'=>'0.00');
        }
        }*/
        echo json_encode($json);
        exit;

    }

    public function metCrearModificar()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $complementosCss = array(
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementosJs = array(
            'select2/select2.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

        $valido = $this->metObtenerInt('valido');
        $idConceptoAsignado = $this->metObtenerInt('idConceptoAsignado');
        $idEmpleado = $this->metObtenerInt('idEmpleado');
        if ($valido == 1) {
            $excepcion = array('num_estatus','fec_periodo_hasta');
            $txt = $this->metValidarFormArrayDatos('form', 'txt',$excepcion);
            $ind = $this->metValidarFormArrayDatos('form', 'int', $excepcion);

            if ($txt != null && $ind == null) {
                $validacion = $txt;
            } elseif ($txt == null && $ind != null) {
                $validacion = $ind;
            } else {
                $validacion = array_merge($txt, $ind);
            }

            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }

            if (!isset($validacion['num_estatus'])) {
                $validacion['num_estatus'] = 0;
            }

            if ($idConceptoAsignado == 0) {
                $id = $this->atAsignacionConceptos->metAsignarConceptoEmpleado($idEmpleado, $validacion['fk_nmb002_num_concepto'], $validacion['num_monto'], $validacion['num_cantidad'], $validacion['fec_periodo_desde'], $validacion['fec_periodo_hasta'], $validacion['num_estatus'],$validacion['fk_nmb003_num_tipo_proceso']);
                $validacion['status'] = 'nuevo';
            } else {
                $id = $this->atAsignacionConceptos->metModificarConceptoEmpleado($idEmpleado, $validacion['fk_nmb002_num_concepto'], $validacion['num_monto'], $validacion['num_cantidad'], $validacion['fec_periodo_desde'], $validacion['fec_periodo_hasta'], $validacion['num_estatus'],$validacion['fk_nmb003_num_tipo_proceso'],$idConceptoAsignado);
                $validacion['status'] = 'modificar';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }

            $validacion['idConceptoAsignado'] = $id;
            echo json_encode($validacion);
            exit;
        }
        $this->atVista->assign('idEmpleado', $idEmpleado);
        $this->atVista->assign('idConceptoAsignado', $idConceptoAsignado);
        if($idConceptoAsignado!=0){
            $this->atVista->assign('formDB', $this->atAsignacionConceptos->metBuscarConceptoAsignado($idEmpleado,$idConceptoAsignado));
            $this->atVista->assign('formDBDet', $this->atAsignacionConceptos->metBuscarConceptoAsignadoProcesos($idEmpleado,$idConceptoAsignado));
        }
        $this->atVista->assign('conceptos', $this->atConceptos->metListarConcepto(1));
        $this->atVista->metRenderizar('crearModificar', 'modales');
    }

    public function metEliminar()
    {
        $idConceptoAsignado = $this->metObtenerInt('idConceptoAsignado');
        $this->atAsignacionConceptos->metEliminarConceptoEmpleado($idConceptoAsignado);
        $arrayMenu = array(
            'status' => 'OK',
            'idConceptoAsignado' => $idConceptoAsignado
        );

        echo json_encode($arrayMenu);
    }
}