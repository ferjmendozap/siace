<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class interfazCxPControlador extends Controlador
{

    private $atInterfazCxP;
    private $atConsultasComunes;
    private $atTipoNomina;
    private $atObligacionModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atInterfazCxP = $this->metCargarModelo('interfazCxP', 'procesos');
        $this->atConsultasComunes = $this->metCargarModelo('consultasComunes', 'reportes');
        $this->atTipoNomina = $this->metCargarModelo('tipoNomina', 'maestros');
        $this->atObligacionModelo = $this->metCargarModelo('obligacion', false, 'modCP');
    }

    public function metIndex()
    {
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->assign('tipoNomina', $this->atTipoNomina->metListarTipoNomina(1));
        $this->atVista->metRenderizar('index');
    }

    public function metListadoObligaciones()
    {
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarCssComplemento($complementosCss);

        $pk_num_tipo_nomina = $this->metObtenerInt('pk_num_tipo_nomina');
        $pk_num_proceso_periodo = $this->metObtenerInt('pk_num_proceso_periodo');
        if ($pk_num_tipo_nomina && $pk_num_proceso_periodo) {
            $datosNomina = $this->atInterfazCxP->metProcesoNomina($pk_num_proceso_periodo);
            $this->atVista->assign('obligaciones', $this->atInterfazCxP->metObligacionListar($pk_num_proceso_periodo));
            $this->atVista->metRenderizar('listadoObligaciones', 'modales');
        }
    }

    public function metDatosObligacion($datos)
    {
        $datosObligacion = array(
            'fk_a003_num_persona_proveedor' => $datos['idPersona'],
            'fk_a003_num_persona_proveedor_a_pagar' => $datos['idPersona'],
            'fk_a023_num_centro_de_costo' => $datos['pk_num_centro_costo'],
            'fk_cpb002_num_tipo_documento' => $datos['documento'],
            'fk_cpb017_num_tipo_servicio' => $datos['pk_num_tipo_servico'],#falta
            'fk_a006_num_miscelaneo_tipo_pago' => $datos['tipoPago'],#Falta
            'fk_cpb014_num_cuenta' => $datos['pk_num_cuenta'],
            'fec_programada' => date('Y-m-d'),
            'fec_factura' => date('Y-m-d'),
            'fec_recepcion' => date('Y-m-d'),
            'fec_vencimiento' => date('Y-m-d'),
            'fec_registro' => date('Y-m-d'),
            'ind_nro_control' => $datos['codigo'],
            'ind_nro_factura' => $datos['codigo'],
            'ind_comentarios' => $datos['ind_comentarios'],
            'ind_comentarios_adicional' => $datos['ind_comentarios'],
            'ind_tipo_descuento' => null,
            'ind_tipo_procedencia' => null,
            'num_flag_afecto_IGV' => 0,
            'num_flag_diferido' => 0,
            'num_flag_pago_diferido' => 0,
            'num_flag_compromiso' => $datos['num_flag_compromiso'],
            'num_flag_presupuesto' =>0,
            'num_flag_obligacion_auto' => 1,
            'num_flag_obligacion_directa' => $datos['num_flag_obligacion_directa'],
            'num_flag_caja_chica' => 0,
            'num_flag_pago_individual' => 0,
            'num_flag_generar_pago' => 0,
            'num_flag_distribucion_manual' => 0,
            'num_flag_adelanto' => 0,
            'num_flag_verificado' => 0,
            'num_flag_cont_pendiente_pub_20' => 0,
            'num_contabilizacion_pendiente' => 0,
            'num_proceso_secuencia' => 1,
            'num_monto_afecto' => 0,
            'num_monto_no_afecto' => 0,
            'num_monto_adelanto' => 0,
            'num_monto_impuesto' => 0,
            'num_monto_pago_parcial' => 0,
            'num_monto_descuento' => 0,
            'fk_nmc002_num_proceso_periodo' => $datos['pk_num_proceso_periodo'],
            'pk_num_tipo_nomina' => $datos['pk_num_tipo_nomina'],
            'pk_num_interfaz_cxp' => $datos['pk_num_interfaz_cxp'],
            'cod_tipo_documento' => $datos['cod_tipo_documento'],
            'proveedor' => $datos['proveedor'],
            'obligacionConglomerar' => $datos['obligacionConglomerar'],
        );

        return $datosObligacion;
    }

    public function metCalcularObligaciones()
    {
        $datosObligaciones = $this->metValidarFormArrayDatos('form','formula');
        $int = $this->metValidarFormArrayDatos('form','int');
        $procesoPeriodo = $datosObligaciones['datosObligaciones']['procesoPeriodo'];

        $datos = $datosObligaciones['datosObligaciones'];
        $partidaCuenta = $datosObligaciones['partidaCuenta'];
        $impuestosRetenciones = $datosObligaciones['impuestosRetenciones'];

        $valido['status'] = 'listo';

        foreach ($datos['fk_a003_num_persona_proveedor'] as $k=>$p) {
            if(!isset($datos['obligacionConglomerar'][$k])){
                $datos['obligacionConglomerar'][$k] = NULL;
            }

            $datosObligacion = array(
                'fk_a003_num_persona_proveedor' => $p,
                'fk_a003_num_persona_proveedor_a_pagar' => $p,
                'fk_a023_num_centro_de_costo' => $datos['fk_a023_num_centro_de_costo'][$k],
                'fk_cpb002_num_tipo_documento' => $datos['fk_cpb002_num_tipo_documento'][$k],
                'fk_cpb017_num_tipo_servicio' => $datos['fk_cpb017_num_tipo_servicio'][$k],
                'fk_a006_num_miscelaneo_tipo_pago' => $datos['fk_a006_num_miscelaneo_tipo_pago'][$k],
                'fk_cpb014_num_cuenta' => $datos['fk_cpb014_num_cuenta'][$k],
                'fec_programada' => date('Y-m-d'),
                'fec_factura' => date('Y-m-d'),
                'fec_recepcion' => date('Y-m-d'),
                'fec_vencimiento' => date('Y-m-d'),
                'fec_registro' => date('Y-m-d'),
                'ind_nro_control' => $datos['ind_nro_control'][$k],
                'ind_nro_factura' => $datos['ind_nro_factura'][$k],
                'ind_comentarios' => $datos['ind_comentarios'][$k],
                'ind_comentarios_adicional' => $datos['ind_comentarios_adicional'][$k],
                'ind_tipo_descuento' => null,
                'ind_tipo_procedencia' => null,
                'num_flag_afecto_IGV' => 0,
                'num_flag_diferido' => 0,
                'num_flag_pago_diferido' => 0,
                'num_flag_compromiso' => $datos['num_flag_compromiso'][$k],
                'num_flag_presupuesto' =>0,
                'num_flag_obligacion_auto' => 1,
                'num_flag_obligacion_directa' => $datos['num_flag_obligacion_directa'][$k],
                'num_flag_caja_chica' => 0,
                'num_flag_pago_individual' => 0,
                'num_flag_generar_pago' => 0,
                'num_flag_distribucion_manual' => 0,
                'num_flag_adelanto' => 0,
                'num_flag_verificado' => 0,
                'num_flag_cont_pendiente_pub_20' => 0,
                'num_contabilizacion_pendiente' => 0,
                'num_proceso_secuencia' => 1,
                'num_monto_afecto' => 0,
                'num_monto_adelanto' => 0,
                'num_monto_impuesto' => 0,
                'num_monto_pago_parcial' => 0,
                'num_monto_descuento' => 0,
                'fk_nmc002_num_proceso_periodo' => $datos['fk_nmc002_num_proceso_periodo'][$k],
                'pk_num_tipo_nomina' => $datos['pk_num_tipo_nomina'][$k],
                'pk_num_interfaz_cxp' => $k,
                'num_monto_obligacion' => $datos['num_monto_obligacion'][$k],
                'num_monto_impuesto_otros' => $datos['num_monto_impuesto_otros'][$k],
                'num_monto_no_afecto' => $datos['num_monto_no_afecto'][$k],
                'obligacionConglomerar' => $datos['obligacionConglomerar'][$k]
            );
            $i=0;
            foreach ($partidaCuenta[$k] as $key=>$item) {
                if($key!='ind_secuencia' AND isset($item)) {
                    $i++;
                    $datosObligacion['partidaCuenta']['ind_secuencia'][$i] = $i;
                    $datosObligacion['partidaCuenta'][$i] = array(
                        'fk_a003_num_persona_proveedor' => $item['fk_a003_num_persona_proveedor'],
                        'fk_a023_num_centro_costo' => $item['fk_a023_num_centro_costo'],
                        'fk_cbb004_num_cuenta' => $item['fk_cbb004_num_cuenta'],
                        'fk_cbb004_num_cuenta_pub20' => $item['fk_cbb004_num_cuenta_pub20'],
                        'fk_prb002_num_partida_presupuestaria' => $item['fk_prb002_num_partida_presupuestaria'],
                        'ind_descripcion' => $item['ind_descripcion'],
                        'num_flag_no_afecto' => 1,
                        'num_monto' => $item['num_monto'],
                    );
                }
            }

            $i=0;
            foreach ($impuestosRetenciones[$k] as $key=>$item) {

                if($key!='ind_secuencia') {
                    $i++;
                    $datosObligacion['impuestosRetenciones']['ind_secuencia'][$i] = $i;
                    $datosObligacion['impuestosRetenciones'][$i] = array(
                        'fk_a003_num_persona_proveedor' => $item['fk_a003_num_persona_proveedor'],
                        'fk_a023_num_centro_costo' => $item['fk_a023_num_centro_costo'],
                        'fk_cbb004_num_cuenta' => $item['fk_cbb004_num_cuenta'],
                        'fk_cbb004_num_cuenta_pub20' => $item['fk_cbb004_num_cuenta_pub20'],
                        'fk_nmb002_num_concepto' => $item['fk_nmb002_num_concepto'],
                        'num_monto_impuesto' => $item['num_monto_impuesto'],
                        'num_monto_afecto' => $item['num_monto_afecto'],
                        'ind_impresion' => $item['ind_impresion']
                    );
                }
            }

            if (!isset($int['calcular'][$k]))
                $int['calcular'][$k] = 0;

            if (!isset($int['conglomerar'][$k]))
                $int['conglomerar'][$k] = 0;

            if (!isset($int['modificar'][$k]))
                $int['modificar'][$k] = 0;

            $nro = '';
            $i=1;
            while($nro=='') {
                $codigo = $procesoPeriodo . '-' . $p . '-' . $i;
                $buscarObligacionCodigo = $this->atInterfazCxP->metBuscarObligacionCodigo2($codigo);
                if($buscarObligacionCodigo) {
                    $i++;
                } else{
                    $nro = $i;
                }
            }
            $datosObligacion['ind_nro_control'] = $procesoPeriodo . '-' . $p . '-' . $nro;
            $datosObligacion['ind_nro_factura'] = $procesoPeriodo . '-' . $p . '-' . $nro;
            //var_dump($datosObligacion['codigo']);

            if($int['calcular'][$k]==1){
                if($int['modificar'][$k]==0 AND $int['conglomerar'][$k]==0){
                    $id = $this->atObligacionModelo->metNuevaObligacion($datosObligacion, 'NM');
                    if (is_array($id)) {
                        $valido['id'] = $id;
                        $valido['datosObligacion'] = $datosObligacion;
                        $valido['status'] = 'errorSQL';
                    } else {
                        $id2 = $this->atInterfazCxP->metNominaObligacion($id, $datosObligacion['fk_nmc002_num_proceso_periodo'], $datosObligacion['pk_num_tipo_nomina']);
                        $valido['id2'] = $id2;
                    }
                } elseif($int['conglomerar'][$k]==1){

                    $datosGenerales = $this->atInterfazCxP->metBuscarDatosObligacion($datosObligacion['obligacionConglomerar']);
                    $cuentas = $this->atInterfazCxP->metBuscarDatosObligacionCuentas($datosObligacion['obligacionConglomerar']);
                    $impuestos = $this->atInterfazCxP->metBuscarDatosObligacionImpuestos($datosObligacion['obligacionConglomerar']);

                    $datosObligacion['num_monto_obligacion'] += $datosGenerales['num_monto_obligacion'];
                    $datosObligacion['num_monto_impuesto_otros'] += $datosGenerales['num_monto_impuesto_otros'];
                    $datosObligacion['num_monto_no_afecto'] += $datosGenerales['num_monto_no_afecto'];

                    $i=count($datosObligacion['partidaCuenta']);
                    foreach ($cuentas as $key=>$item) {
                        $i++;
                        $datosObligacion['partidaCuenta']['ind_secuencia'][$i] = $i;
                        $datosObligacion['partidaCuenta'][$i] = array(
                            'fk_a003_num_persona_proveedor' => $item['fk_a003_num_persona_proveedor'],
                            'fk_a023_num_centro_costo' => $item['fk_a023_num_centro_costo'],
                            'fk_cbb004_num_cuenta' => $item['fk_cbb004_num_cuenta'],
                            'fk_cbb004_num_cuenta_pub20' => $item['fk_cbb004_num_cuenta_pub20'],
                            'fk_prb002_num_partida_presupuestaria' => $item['fk_prb002_num_partida_presupuestaria'],
                            'ind_descripcion' => $item['ind_descripcion'],
                            'num_flag_no_afecto' => 1,
                            'num_monto' => $item['num_monto'],
                        );
                    }

                    if(isset($datosObligacion['impuestosRetenciones'])) {
                        $i=count($datosObligacion['impuestosRetenciones']);
                    } else {
                        $i=0;
                        $datosObligacion['impuestosRetenciones'] = array();
                    }
                    foreach ($impuestos as $key=>$item) {
                        $i++;
                        $datosObligacion['impuestosRetenciones']['ind_secuencia'][$i] = $i;
                        $datosObligacion['impuestosRetenciones'][$i] = array(
                            'fk_a003_num_persona_proveedor' => $item['fk_a003_num_persona_proveedor'],
                            'fk_cbb004_num_cuenta' => $item['fk_cbb004_num_cuenta'],
                            'fk_cbb004_num_cuenta_pub20' => $item['fk_cbb004_num_cuenta_pub20'],
                            'fk_nmb002_num_concepto' => $item['fk_nmb002_num_concepto'],
                            'num_monto_impuesto' => $item['num_monto_impuesto'],
                            'num_monto_afecto' => $item['num_monto_afecto']
                        );
                    }

                    if(isset($datosObligacion['obligacionConglomerar'])) {
                        $id = $this->atInterfazCxP->metModificarObligacion($datosObligacion['obligacionConglomerar'], $datosObligacion);
                        if (is_array($id)) {
                            $valido['id'] = $id;
                            $valido['status'] = 'errorSQL';
                        } else {
                            $id2 = $this->atInterfazCxP->metNominaObligacion($id, $datosObligacion['fk_nmc002_num_proceso_periodo'], $datosObligacion['pk_num_tipo_nomina']);
                            $valido['id2'] = $id2;
                        }
                    }
                } else {
                    $id = $this->atInterfazCxP->metModificarObligacion($datosObligacion['obligacionConglomerar'], $datosObligacion);
                    if (is_array($id)) {
                        $valido['id'] = $id;
                        $valido['status'] = 'errorSQL';
                    } else {
                        $id2 = $this->atInterfazCxP->metNominaObligacion($id, $datosObligacion['fk_nmc002_num_proceso_periodo'], $datosObligacion['pk_num_tipo_nomina']);
                        $valido['id2'] = $id2;
                    }
                }
            }
        }

        echo json_encode($valido);
        exit;
    }

    public function metVerificarObligacion()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idObligacion = $this->metObtenerInt('idObligacion');
        $estado = $this->metObtenerAlphaNumerico('estado');

        if ($valido == 1) {
            $ind = $this->metValidarFormArrayDatos('form', 'int');
            $id = $this->atInterfazCxP->metVerificarPresupuesto($idObligacion);
            $validacion['status'] = 'verificar';
            $validacion['idObligacion'] = $id;
            echo json_encode($validacion);
            exit;
        }
        $this->atVista->assign('obligacionBD', $this->atObligacionModelo->metConsultaObligacion($idObligacion));
        $this->atVista->assign('impuestoBD', $this->atObligacionModelo->metMostrarImpuesto($idObligacion));
        $this->atVista->assign('partidaBD', $this->atObligacionModelo->metMostrarPartidas($idObligacion));
        $this->atVista->assign('listadoCuentas', $this->atObligacionModelo->atCuentasModelo->metCuentaListar());
        $this->atVista->assign('idObligacion', $idObligacion);
        $this->atVista->assign('ver', 1);
        $this->atVista->metRenderizar('verificarObligacion', 'modales');
    }

    public function metPrevisualizarObligaciones()
    {
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $pk_num_tipo_nomina = $this->metObtenerAlphaNumerico('pk_num_tipo_nomina');
        $pk_num_proceso_periodo = $this->metObtenerAlphaNumerico('pk_num_proceso_periodo');
        $datosNomina = $this->atInterfazCxP->metProcesoNomina($pk_num_proceso_periodo);

        $conceptosAsignaciones = $this->atConsultasComunes->metConceptoConsolidadosIngresos($pk_num_proceso_periodo);
        $conceptosDeducciones = $this->atConsultasComunes->metConceptosConsolidadosDeducciones2($pk_num_proceso_periodo);
        $conceptosAportes = $this->atInterfazCxP->metBuscarConceptosPorTipoDocumento($pk_num_proceso_periodo,"'A','I'");

        $datos['datosNomina'] = $datosNomina;
        $datos['pk_num_tipo_nomina'] = $pk_num_tipo_nomina;
        $datos['pk_num_proceso_periodo'] = $pk_num_proceso_periodo;
        $codCentroCosto = Session::metObtener('CENTROCOSTONM');
        $centroCosto = $this->atObligacionModelo->metBuscar('a023_centro_costo', 'ind_abreviatura', $codCentroCosto);
        $cuentaBancaria = $this->atObligacionModelo->metBuscar('cp_b014_cuenta_bancaria', 'ind_num_cuenta', Session::metObtener('CUENTANM'));
        $tipoServicio = $this->atObligacionModelo->metBuscar('cp_b017_tipo_servicio', 'cod_tipo_servicio', Session::metObtener('TIPOSERVNM'));
        $t = $this->atInterfazCxP->metMostrarSelect('TDPLG', '01');
        /****/
        if($datos['pk_num_tipo_nomina']==1){
            $DOC = Session::metObtener('DOCUMENTONM-D');
	    $DET = 'DIRECTORES Y JEFES';
        }
        if($datos['pk_num_tipo_nomina']==2){
            $DOC = Session::metObtener('DOCUMENTONM-E');
	    $DET = 'EMPLEADOS';
        }
        if($datos['pk_num_tipo_nomina']==4){
            $DOC = Session::metObtener('DOCUMENTONM-J');
	    $DET = 'NOMINA DE JUBILADOS';
        }
        if($datos['pk_num_tipo_nomina']==5){
            $DOC = Session::metObtener('DOCUMENTONM-P');
	    $DET = 'NOMINA DE PENSIONADOS';
        }
        if($datos['pk_num_tipo_nomina']==6){
            $DOC = Session::metObtener('DOCUMENTONM-A');
	    $DET = 'NOMINA DEL ALTO NIVEL';
        }
        /****/

        $proveedores1 = $this->atInterfazCxP->metProveedores();
        $prede[0] = $this->atInterfazCxP->metProveedorPredeterminado(
            Session::metObtener('PROVEEDORNM'),
            $DOC,
            Session::metObtener('TIPOSERVNM'),
            Session::metObtener('TIPOPAGONM'),
            Session::metObtener('CUENTANM'),
            "NOMINA DE".$DET." DE LA CONTRALORIA PARA EL PERIODO " . $datosNomina['PERIODO'] . " " . $datosNomina['ind_nombre_proceso']
        );

        $proveedores = array_merge($prede, $proveedores1);

        if($datosNomina['num_flag_individual']==0) {

            foreach ($proveedores as $key=>$p) {

                $cuentaBancaria = $this->atObligacionModelo->metBuscar('cp_b014_cuenta_bancaria', 'ind_num_cuenta', $p['ind_num_cuenta']);

                $buscarObligacionCodigo = $this->atInterfazCxP->metBuscarObligacionCodigo($datos['pk_num_tipo_nomina'], $p['pk_num_persona'], $p['pk_num_tipo_documento'], 1);

                $datos['codigo'] = $datosNomina['procesoPeriodo'] . '-' . $p['pk_num_persona'] . '-' . ($buscarObligacionCodigo['nro'] + 1);

                $datos['idPersona'] = $p['pk_num_persona'];
                $datos['pk_num_centro_costo'] = $centroCosto['pk_num_centro_costo'];
                $datos['documento'] = $p['pk_num_tipo_documento'];
                $datos['pk_num_tipo_servico'] = $p['fk_000_tipo_servicio'];
                $datos['tipoPago'] = $p['fk_a006_num_miscelaneo_det_tipo_pago'];
                $datos['pk_num_cuenta'] = $cuentaBancaria['pk_num_cuenta'];
                $datos['ind_comentarios'] = $p['ind_comentario'];
                $datos['num_flag_compromiso'] = 1;
                $datos['num_flag_obligacion_directa'] = 1;
                $datos['pk_num_centro_costo'] = $centroCosto['pk_num_centro_costo'];
                $datos['pk_num_interfaz_cxp'] = $key;
                $datos['cod_tipo_documento'] = $p['cod_tipo_documento'];
                $datos['proveedor'] = $p['proveedor'];
                $datos['obligacionConglomerar'] = $p['obligacionConglomerar'];

                $datosObligacionP[$key] = $this->metDatosObligacion($datos);
                $datosObligacionP[$key]['idObligacion'] = 0;

                $proveedores[$key] = $datosObligacionP[$key];

                $ii = 0;
                $totalAsignacionesP = 0;
                foreach ($conceptosAsignaciones as $asignacion) {
                    if($key=='0') {
                        $ii++;
                        if($asignacion['debe']=='' OR $asignacion['debe']==0){
                            $asignacion['debe'] = NULL;
                        }
                        if($asignacion['debe20']=='' OR $asignacion['debe20']==0){
                            $asignacion['debe20'] = NULL;
                        }
                        $datosObligacionP[$key]['partidaCuenta']['ind_secuencia'][$ii] = $ii;
                        $datosObligacionP[$key]['partidaCuenta'][$ii] = array(
                            'fk_a003_num_persona_proveedor' => $datos['idPersona'],
                            'fk_a023_num_centro_costo' => $datos['pk_num_centro_costo'],
                            'fk_cbb004_num_cuenta' => $asignacion['debe'],
                            'fk_cbb004_num_cuenta_pub20' => $asignacion['debe20'],
                            'fk_prb002_num_partida_presupuestaria' => $asignacion['pk_num_partida_presupuestaria'],
                            'ind_descripcion' => $asignacion['ind_impresion'],
                            'num_flag_no_afecto' => 1,
                            'num_monto' => $asignacion['num_monto'],
                        );
                        $totalAsignacionesP += $asignacion['num_monto'];
                    }
                }

                $ii = 0;
                $totalDeduccionesP = 0;
                $totalDeduccionesR[$key] = 0;
                $proveedores[$key]['partidaCuenta'] = array();
                $datosObligacionP[$key]['impuestosRetenciones'] = array();

                $datosR['datosNomina'] = $datosNomina;
                $datosR['pk_num_tipo_nomina'] = $pk_num_tipo_nomina;
                $datosR['pk_num_proceso_periodo'] = $pk_num_proceso_periodo;
                $datosR['codigo'] = $datosNomina['procesoPeriodo'] . '-' . $p['pk_num_persona'] . '-' . ($buscarObligacionCodigo['nro'] + 2);

                $datosR['idPersona'] = $p['pk_num_persona'];
                $datosR['pk_num_centro_costo'] = $centroCosto['pk_num_centro_costo'];
                $datosR['documento'] = $p['pk_num_tipo_documento'];
                $datosR['pk_num_tipo_servico'] = $p['fk_000_tipo_servicio'];
                $datosR['tipoPago'] = $p['fk_a006_num_miscelaneo_det_tipo_pago'];
                $datosR['pk_num_cuenta'] = $p['ind_num_cuenta'];
                $datosR['ind_comentarios'] = $p['ind_comentario'].' '.$datosNomina['PERIODO'];
                $datosR['num_flag_compromiso'] = 0;
                $datosR['num_flag_obligacion_directa'] = 1;
                $datosR['pk_num_interfaz_cxp'] = $key;
                $datosR['cod_tipo_documento'] = $p['cod_tipo_documento'];
                $datosR['proveedor'] = $p['proveedor'];
                $datosR['obligacionConglomerar'] = $p['obligacionConglomerar'];

                $datosObligacionR[$key] = $this->metDatosObligacion($datosR);
                $datosObligacionR[$key]['idObligacion'] = 0;
                $datosObligacionR[$key]['partidaCuenta'] = array();

                foreach ($conceptosDeducciones AS $key2 => $value) {
                    $ii++;
                    if($value['haber']=='' OR $value['haber']==0){
                        $value['haber'] = NULL;
                    }
                    if($value['haber20']=='' OR $value['haber20']==0){
                        $value['haber20'] = NULL;
                    }
                    if(isset($datosObligacionP[$key]['impuestosRetenciones'][$ii]['ind_impresion']) AND
                        $datosObligacionP[$key]['impuestosRetenciones'][$ii]['ind_impresion']==$value['ind_impresion']
                    ){
                        $datosObligacionP[$key]['impuestosRetenciones'][$ii]['num_monto'] += $value['num_monto'];
                    } else {
                        $datosObligacionP[$key]['impuestosRetenciones']['ind_secuencia'][$ii] = $ii;

                        $datosObligacionP[$key]['impuestosRetenciones'][$ii] = array(
                            'fk_a003_num_persona_proveedor' => $datosObligacionP[$key]['fk_a003_num_persona_proveedor'],
                            'fk_a023_num_centro_costo' => $datosObligacionP[$key]['fk_a023_num_centro_de_costo'],
                            'fk_cbb004_num_cuenta' => $value['haber'],
                            'fk_cbb004_num_cuenta_pub20' => $value['haber20'],
                            'fk_nmb002_num_concepto' => $value['pk_num_concepto'],
                            'num_monto_impuesto' => $value['num_monto'],
                            'num_monto_afecto' => $value['num_monto'],
                            'ind_impresion' => $value['ind_impresion']
                        );
                    }

                    if($key==$value['pk_num_interfaz_cxp']) {

                        $datosObligacionR[$key]['partidaCuenta']['ind_secuencia'][$ii] = $ii;
                        $datosObligacionR[$key]['partidaCuenta'][$ii] = array(
                            'fk_a003_num_persona_proveedor' => $datosObligacionR[$key]['fk_a003_num_persona_proveedor'],
                            'fk_a023_num_centro_costo' => $datosObligacionR[$key]['fk_a023_num_centro_de_costo'],
                            'fk_cbb004_num_cuenta' => $value['haber'],
                            'fk_cbb004_num_cuenta_pub20' => $value['haber20'],
                            'fk_prb002_num_partida_presupuestaria' => NULL,
                            'ind_descripcion' => $value['ind_impresion'],
                            'num_flag_no_afecto' => 1,
                            'num_monto' => $value['num_monto']
                        );
                        $totalDeduccionesR[$key] += $value['num_monto'];
                    }
                    $totalDeduccionesP += $value['num_monto'];
                }

                $ii = 0;
                $totalAsignacionesA[$key] = 0;

                $datosA['datosNomina'] = $datosNomina;
                $datosA['pk_num_tipo_nomina'] = $pk_num_tipo_nomina;
                $datosA['pk_num_proceso_periodo'] = $pk_num_proceso_periodo;
                $datosA['codigo'] = $datosNomina['procesoPeriodo'] . '-' . $p['pk_num_persona'] . '-' . ($buscarObligacionCodigo['nro'] + 2);

                $datosA['idPersona'] = $p['pk_num_persona'];
                $datosA['pk_num_centro_costo'] = $centroCosto['pk_num_centro_costo'];
                $datosA['documento'] = $p['pk_num_tipo_documento'];
                $datosA['pk_num_tipo_servico'] = $p['fk_000_tipo_servicio'];
                $datosA['tipoPago'] = $p['fk_a006_num_miscelaneo_det_tipo_pago'];
                $datosA['pk_num_cuenta'] = $p['ind_num_cuenta'];
                $datosA['ind_comentarios'] = $p['ind_comentario'].' '.$datosNomina['PERIODO'];
                $datosA['num_flag_compromiso'] = 1;
                $datosA['num_flag_obligacion_directa'] = 1;
                $datosA['pk_num_interfaz_cxp'] = $key;
                $datosA['cod_tipo_documento'] = $p['cod_tipo_documento'];
                $datosA['proveedor'] = $p['proveedor'];
                $datosA['obligacionConglomerar'] = $p['obligacionConglomerar'];

                $datosObligacionA[$key] = $this->metDatosObligacion($datosA);
                $datosObligacionA[$key]['idObligacion'] = 0;
                $datosObligacionA[$key]['partidaCuenta'] = array();

                foreach ($conceptosAportes AS $key2 => $aporte) {
                    if($key==$aporte['pk_num_interfaz_cxp']) {

                        if($aporte['debe']=='' OR $aporte['debe']==0){
                            $aporte['debe'] = NULL;
                        }
                        if($aporte['debe20']=='' OR $aporte['debe20']==0){
                            $aporte['debe20'] = NULL;
                        }

                        $datosObligacionA[$key]['partidaCuenta']['ind_secuencia'][$ii] = $ii;
                        $datosObligacionA[$key]['partidaCuenta'][$ii] = array(
                            'fk_a003_num_persona_proveedor' => $datos['idPersona'],
                            'fk_a023_num_centro_costo' => $datos['pk_num_centro_costo'],
                            'fk_cbb004_num_cuenta' => $aporte['debe'],
                            'fk_cbb004_num_cuenta_pub20' => $aporte['debe20'],
                            'fk_prb002_num_partida_presupuestaria' => $aporte['pk_num_partida_presupuestaria'],
                            'ind_descripcion' => $aporte['ind_impresion'],
                            'num_flag_no_afecto' => 1,
                            'num_monto' => $aporte['num_monto'],
                        );
                        $totalAsignacionesA[$key] += $aporte['num_monto'];
                        $ii++;
                    }
                }

                if($key=='0') {
                    $datosObligacionP[$key]['num_monto_obligacion'] = $totalAsignacionesP-$totalDeduccionesP;
                    $datosObligacionP[$key]['num_monto_impuesto_otros'] = $totalDeduccionesP;
                    $datosObligacionP[$key]['num_monto_no_afecto'] = $totalAsignacionesP;

                    $proveedores[$key] = $datosObligacionP[$key];
                    $proveedores[$key]['partidaCuenta'] = $datosObligacionP[$key]['partidaCuenta'];
                    $proveedores[$key]['impuestosRetenciones'] = $datosObligacionP[$key]['impuestosRetenciones'];
                } else {

                    if($totalDeduccionesR[$key]>0){
                        $datosObligacionR[$key]['num_monto_obligacion'] = $totalDeduccionesR[$key];
                        $datosObligacionR[$key]['num_monto_impuesto_otros'] = 0;
                        $datosObligacionR[$key]['num_monto_no_afecto'] = $totalDeduccionesR[$key];

                        $proveedores[$key] = $datosObligacionR[$key];
                        $proveedores[$key]['partidaCuenta'] = $datosObligacionR[$key]['partidaCuenta'];
                    }
                    if($totalAsignacionesA[$key]>0){
                        $datosObligacionA[$key]['num_monto_obligacion'] = $totalAsignacionesA[$key];
                        $datosObligacionA[$key]['num_monto_impuesto_otros'] = 0;
                        $datosObligacionA[$key]['num_monto_no_afecto'] = $totalAsignacionesA[$key];

                        $proveedores[$key] = $datosObligacionA[$key];
                        $proveedores[$key]['partidaCuenta'] = $datosObligacionA[$key]['partidaCuenta'];
                    }


                }
            }
        } else {
            $empleados = $this->atConsultasComunes->metEmpleadosNomina($pk_num_proceso_periodo);

            $c=0;
            foreach ($empleados as $key=>$e) {
                $c++;
                $buscarObligacionCodigo = $this->atInterfazCxP->metBuscarObligacionCodigo($datos['pk_num_tipo_nomina'], $e['pk_num_persona'], $datosNomina['fk_000_num_tipo_documento'], 1);
                $documento = $this->atObligacionModelo->metBuscar('cp_b002_tipo_documento', 'pk_num_tipo_documento', $datosNomina['fk_000_num_tipo_documento']);
                $datos['codigo'] = $datosNomina['procesoPeriodo'] . '-' . $e['pk_num_persona'] . '-' . ($buscarObligacionCodigo['nro'] + 1);
                $obligacionConglomerar = $this->atInterfazCxP->metBuscarObligacionPersona($e['pk_num_persona']);

                $datos['idPersona'] = $e['pk_num_persona'];
                $datos['pk_num_centro_costo'] = $centroCosto['pk_num_centro_costo'];
                $datos['documento'] = $documento['pk_num_tipo_documento'];
                $datos['pk_num_tipo_servico'] = $tipoServicio['pk_num_tipo_servico'];
                $datos['tipoPago'] = $t['pk_num_miscelaneo_detalle'];
                $datos['pk_num_cuenta'] = $cuentaBancaria['pk_num_cuenta'];
                $datos['ind_comentarios'] = 'PERIODO '.$datosNomina['PERIODO'].' '.$datosNomina['ind_titulo_boleta'].' '.$datosNomina['ind_nombre_proceso'];
                $datos['num_flag_compromiso'] = 1;
                $datos['num_flag_obligacion_directa'] = 0;
                $datos['pk_num_interfaz_cxp'] = $c;
                $datos['cod_tipo_documento'] = $documento['cod_tipo_documento'];
                $datos['proveedor'] = $e['nombre'];

                $datos['obligacionConglomerar'] = $obligacionConglomerar['pk_num_obligacion'];

                $datosObligacionP[$key] = $this->metDatosObligacion($datos);
                $datosObligacionP[$key]['idObligacion'] = 0;

                $asignaciones = $this->atConsultasComunes->metEmpleadosAsignaciones($pk_num_proceso_periodo,$e['pk_num_empleado']);


                $ii = 0;
                $totalAsignacionesP = 0;
                foreach ($asignaciones as $asignacion) {
                    $ii++;
                    if($asignacion['debe']=='' OR $asignacion['debe']==0){
                        $asignacion['debe'] = NULL;
                    }
                    if($asignacion['debe20']=='' OR $asignacion['debe20']==0){
                        $asignacion['debe20'] = NULL;
                    }
                    $datosObligacionP[$key]['partidaCuenta']['ind_secuencia'][$ii] = $ii;
                    $datosObligacionP[$key]['partidaCuenta'][$ii] = array(
                        'fk_a003_num_persona_proveedor' => $datos['idPersona'],
                        'fk_a023_num_centro_costo' => $datos['pk_num_centro_costo'],
                        'fk_cbb004_num_cuenta' => $asignacion['debe'],
                        'fk_cbb004_num_cuenta_pub20' => $asignacion['debe20'],
                        'fk_prb002_num_partida_presupuestaria' => $asignacion['pk_num_partida_presupuestaria'],
                        'ind_descripcion' => $asignacion['ind_impresion'],
                        'num_flag_no_afecto' => 1,
                        'num_monto' => $asignacion['num_monto'],
                    );
                    $totalAsignacionesP += $asignacion['num_monto'];
                }

                $datosObligacionP[$key]['num_monto_obligacion'] = $totalAsignacionesP;
                $datosObligacionP[$key]['num_monto_no_afecto'] = $totalAsignacionesP;
                $proveedores[$key] = $datosObligacionP[$key];

            }

//            var_dump($empleados[0]['pk_num_persona']);
        }

        $this->atVista->assign('datosNomina', $datosNomina);
        $this->atVista->assign('proveedores', $proveedores);
        $this->atVista->metRenderizar('previsualizacion', 'modales');
    }

    public function metTransferirObligacion()
    {
        $idObligacion = $this->metObtenerInt('idObligacion');
        if ($idObligacion) {
            $id = $this->atInterfazCxP->metTransferirObligacion($idObligacion);
            if (is_array($id)) {
                var_dump($id);
                $datosObligacion['status'] = 'errorSQL';
                echo json_encode($datosObligacion);
                exit;
            }
            $validacion['status'] = 'ok';
            echo json_encode($validacion);
            exit;
        }
    }

    public function metJsonDataTabla($procesoNomina=false)
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        if(!$procesoNomina) {
            $procesoNomina = '0';
        }
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                  *,
                  (IF(cp_d001_obligacion.ind_estado='NM',0,1)) AS num_flag_transferido,
                  FORMAT(cp_d001_obligacion.num_monto_obligacion,2,'de_DE') AS num_monto_obligacion,
                  CONCAT_WS(' ',ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2) AS proveedor,
                  cp_d001_obligacion.num_flag_presupuesto AS num_flag_presupuesto2,
                  (IF(cp_d001_obligacion.ind_estado='NM',0,1)) AS num_flag_transferido2
                FROM
                  cp_d001_obligacion
                INNER JOIN nm_d006_nomina_obligacion ON nm_d006_nomina_obligacion.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
                INNER JOIN a003_persona ON a003_persona.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor
                INNER JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = cp_d001_obligacion.fk_cpb002_num_tipo_documento
                INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_d001_obligacion.fk_a006_num_miscelaneo_tipo_pago
                WHERE
                  cp_d001_obligacion.ind_tipo_procedencia='NM' AND
                  nm_d006_nomina_obligacion.fk_nmc002_num_proceso_periodo=$procesoNomina 
                  ";
        if ($busqueda['value']) {
            $sql .= " AND
                        (pk_num_obligacion LIKE '%$busqueda[value]%' OR
                        ind_nombre_detalle LIKE '%$busqueda[value]%' OR
                        fk_a003_num_persona_proveedor LIKE '%$busqueda[value]%' OR
                        ind_nombre1 LIKE '%$busqueda[value]%' OR
                        ind_nombre2 LIKE '%$busqueda[value]%' OR
                        ind_apellido1 LIKE '%$busqueda[value]%' OR
                        ind_apellido2 LIKE '%$busqueda[value]%' OR
                        cod_tipo_documento LIKE '%$busqueda[value]%' OR
                        ind_nro_factura LIKE '%$busqueda[value]%' OR
                        num_monto_obligacion LIKE '%$busqueda[value]%' OR
                        ind_estado LIKE '%$busqueda[value]%' 
                        ) 
                        ";
        }

        $flag = array('num_flag_presupuesto','num_flag_transferido');

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array(
            'pk_num_obligacion',
            'ind_nombre_detalle',
            'fk_a003_num_persona_proveedor',
            'proveedor',
            'cod_tipo_documento',
            'ind_nro_factura',
            'num_flag_presupuesto',
            'num_flag_transferido',
            'num_monto_obligacion',
            'ind_estado',
            );
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_obligacion';
        #construyo el listado de botones

        if (in_array('NM-01-01-07-02-V',$rol)) {
            $campos['boton']['Verificar'] = array("
                    <button class='verificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary' 
                            data-toggle='modal' data-target='#formModal'
                            data-keyboard='false' data-backdrop='static' idObligacion='$clavePrimaria'
                            title='Verificar Presupuesto'>
                        <i class='icm icm-stats2' style='color: #ffffff;'></i>
                    </button>",
                'if( $i["num_flag_presupuesto2"] == "0") { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Verificar'] = false;
        }

        if (in_array('NM-01-01-07-01-T',$rol)) {
            $campos['boton']['Transferir'] = array("
                    <button class='transferir logsUsuario btn ink-reaction btn-raised btn-xs btn-success'
                            idObligacion='$clavePrimaria'
                            titulo='Transferir Obligacion'
                            mensaje='Esta Seguro que desea Transferir la obligacion??'
                            descipcion='el Usuario a transferido una Obligaciones de Nomina a Cuentas Por Pagar'>
                        <i class='icm icm-checkmark3' style='color: #ffffff;'></i>
                    </button>",
                'if( $i["num_flag_presupuesto2"] == 1 AND $i["num_flag_transferido2"] == 0) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Transferir'] = false;
        }

        if (in_array('NM-01-01-07-03-VER',$rol)) {
            $campos['boton']['Ver'] = '
                    <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                            data-keyboard="false" data-backdrop="static" id="ver" idObligacion="'.$clavePrimaria.'" title="Consultar"
                            descipcion="El Usuario esta viendo una Obligacion" titulo="Consultar Obligacion">
                        <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                    </button>'
            ;
        } else {
            $campos['boton']['Ver'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,false,$flag);
    }

}
