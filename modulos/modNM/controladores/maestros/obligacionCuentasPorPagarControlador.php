<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class obligacionCuentasPorPagarControlador extends Controlador
{
    private $atObligacionesCxp;
    private $atDocumento;
    private $atPartida;
    private $atObligacionModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atObligacionesCxp = $this->metCargarModelo('obligacionCuentasPorPagar', 'maestros');
        $this->atDocumento = $this->metCargarModelo('documento', false, 'modCP');
        $this->atPartida = $this->metCargarModelo('partida', 'maestros', 'modPR');
        $this->atObligacionModelo = $this->metCargarModelo('obligacion',false,'modCP');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
#        $this->atVista->assign('listado', $this->atObligacionesCxp->metListarObligacionesCxp());
        $this->atVista->metRenderizar('listado');
    }

    public function metProveedor($proveedor)
    {
        $this->atVista->assign('lista', $this->atObligacionesCxp->metListaPersona());
        $this->atVista->assign('tipoProveedor', $proveedor);
        $this->atVista->metRenderizar('proveedor', 'modales');
    }

    public function metPartidasCuentas($lugar,$tipo,$idCampo=false,$cuenta=false)
    {
        if($tipo=='P'){
            $this->atVista->assign('lista', $this->atPartida->metListarPartida());
        }else{
            $this->atVista->assign('lista', $this->atObligacionesCxp->metListarCuentas($cuenta));
        }
        $this->atVista->assign('lugar', $lugar);
        $this->atVista->assign('tipo', $tipo);
        $this->atVista->assign('idCampo', $idCampo);
        $this->atVista->metRenderizar('partidas', 'modales');
    }

    public function metCrearModificar()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $complementosCss = array(
            'select2/select201ef',
        );
        $complementosJs = array(
            'select2/select2.min',
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $valido = $this->metObtenerInt('valido');
        $idObligacionCxp = $this->metObtenerInt('idObligacionCxp');
        if ($valido == 1) {
            $this->metValidarToken();
            $Excceccion=array('num_flag_pago_mensual','num_estatus','num_flag_prq');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
            $txt=$this->metValidarFormArrayDatos('form','txt');
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
            if ($alphaNum != null && $ind == null && $txt == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt == null) {
                $validacion = $ind;
            } elseif ($alphaNum != null && $ind != null && $txt == null) {
                $validacion = array_merge($alphaNum, $ind);
            } elseif ($alphaNum != null && $txt != null && $ind == null) {
                $validacion = array_merge($alphaNum, $txt);
            } elseif ($ind != null && $txt != null && $alphaNum == null) {
                $validacion = array_merge($ind, $txt);
            } else {
                $validacion = array_merge($ind, $txt, $alphaNum);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            if(!isset($validacion['fk_rhb001_num_empleado'])){
                $validacion['fk_rhb001_num_empleado']=null;
            }
            if(!isset($validacion['ind_num_factura'])){
                $validacion['ind_num_factura']=null;
            }
            if(!isset($validacion['num_monto'])){
                $validacion['num_monto']=null;
            }
            if(!isset($validacion['num_flag_prq'])){
                $validacion['num_flag_prq']=0;
            }

            if ($idObligacionCxp == 0) {
                $id=$this->atObligacionesCxp->metNuevaObligacionCxP($validacion);
                $validacion['status'] = 'nuevo';
            } else {
                $id=$this->atObligacionesCxp->metModificarObligacionCxP($idObligacionCxp,$validacion);
                $validacion['status'] = 'modificar';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idObligacion'] = $id;
            echo json_encode($validacion);
            exit;
        }

        if ($idObligacionCxp != 0) {
            $this->atVista->assign('formDB', $this->atObligacionesCxp->metConsultarObligacionesCxp($idObligacionCxp));
            $this->atVista->assign('formDBDetPartida', $this->atObligacionesCxp->metConsultarObligacionesCxpPartida($idObligacionCxp));
            $this->atVista->assign('formDBDetCuenta', $this->atObligacionesCxp->metConsultarObligacionesCxpCuenta($idObligacionCxp));
        }

        $this->atVista->assign('tipoObligacion', $this->atObligacionesCxp->metMostrarSelect('TPDOB'));
        $this->atVista->assign('tipoPago', $this->atObligacionesCxp->metMostrarSelect('TDPLG'));
        $this->atVista->assign('tipoDocumento', $this->atDocumento->metDocumentoListar());
        $this->atVista->assign('listadoServicio', $this->atObligacionModelo->atServicioModelo->metServiciosListar());
        $this->atVista->assign('listadoCuentas', $this->atObligacionModelo->atCuentasModelo->metCuentaListar());
        $this->atVista->assign('idObligacionCxp', $idObligacionCxp);
        $this->atVista->metRenderizar('CrearModificar', 'modales');
    }

    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
              nm_b005_interfaz_cxp.*,
              cp_b002_tipo_documento.*,
              tipoObligacion.*,
              tipoPago.*,
              concat_ws(' ',proveedor.ind_nombre1,proveedor.ind_nombre2,proveedor.ind_apellido1,proveedor.ind_apellido2) AS proveedor,
              concat_ws(' ',pagar.ind_nombre1,pagar.ind_nombre2,pagar.ind_apellido1,pagar.ind_apellido2) AS pagarA,
              tipoObligacion.ind_nombre_detalle AS tipoObligacion
            FROM
              nm_b005_interfaz_cxp
            INNER JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento=nm_b005_interfaz_cxp.fk_cpb002_num_tipo_documento
            INNER JOIN a006_miscelaneo_detalle AS tipoObligacion ON tipoObligacion.pk_num_miscelaneo_detalle=nm_b005_interfaz_cxp.fk_a006_num_miscelaneo_det_tipo_obligacion
            INNER JOIN a006_miscelaneo_detalle AS tipoPago ON tipoPago.pk_num_miscelaneo_detalle=nm_b005_interfaz_cxp.fk_a006_num_miscelaneo_det_tipo_pago
            INNER JOIN a003_persona proveedor ON proveedor.pk_num_persona=nm_b005_interfaz_cxp.fk_a003_num_persona_proveedor
            INNER JOIN a003_persona pagar ON pagar.pk_num_persona=nm_b005_interfaz_cxp.fk_a003_num_persona_pagar ";
        if ($busqueda['value']) {
            $sql .= " WHERE
                    ( 
                    cp_b002_tipo_documento.cod_tipo_documento LIKE '%$busqueda[value]%' OR
                    proveedor.ind_nombre1 LIKE '%$busqueda[value]%' OR
                    proveedor.ind_nombre2 LIKE '%$busqueda[value]%' OR
                    proveedor.ind_apellido1 LIKE '%$busqueda[value]%' OR
                    proveedor.ind_apellido2 LIKE '%$busqueda[value]%' OR
                    pagar.ind_nombre1 LIKE '%$busqueda[value]%' OR
                    pagar.ind_nombre2 LIKE '%$busqueda[value]%' OR
                    pagar.ind_apellido1 LIKE '%$busqueda[value]%' OR
                    pagar.ind_apellido2 LIKE '%$busqueda[value]%' OR
                    tipoPago.ind_nombre_detalle LIKE '%$busqueda[value]%' OR
                    tipoObligacion.ind_nombre_detalle LIKE '%$busqueda[value]%'
                    )
            ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('cod_tipo_documento','proveedor','pagarA','tipoObligacion','ind_titulo');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_interfaz_cxp';
        #construyo el listado de botones

        if (in_array('NM-01-03-02-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static" idObligacionCxp="'.$clavePrimaria.'"  title="Editar"
                        descipcion="El Usuario a Modificado un Proceso de Nomina" titulo="Modificar Proceso">
                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }


        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }

}
