<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class tipoNominaControlador extends Controlador
{
    private $atTipoNomina;
    private $atDocumento;

    public function __construct()
    {
        parent::__construct();
        $this->atTipoNomina=$this->metCargarModelo('tipoNomina','maestros');
        $this->atDocumento=$this->metCargarModelo('documento', false ,'modCP');
    }

    public function metIndex($lista=false)
    {
        if(!$lista){
            $complementosCss = array(
                'DataTables/jquery.dataTables',
                'DataTables/extensions/dataTables.colVis941e',
                'DataTables/extensions/dataTables.tableTools4029',
            );
            $js[] = 'materialSiace/core/demo/DemoTableDynamic';
            $js[] = 'Aplicacion/appFunciones';
            $this->atVista->metCargarCssComplemento($complementosCss);
            $this->atVista->metCargarJs($js);
        }

        if($lista){
            $this->atVista->assign('listado',$this->atTipoNomina->metListarTipoNomina());
            $this->atVista->metRenderizar('listadoModal','modales');
        }else{
            $this->atVista->metRenderizar('listado');
        }
    }

    public function metCrearModificar()
    {
        $complementosCss = array(
            'select2/select201ef',
        );
        $complementosJs = array(
            'select2/select2.min',
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

        $valido=$this->metObtenerInt('valido');
        $idNomina=$this->metObtenerInt('idNomina');
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('num_flag_pago_mensual','num_estatus','num_estatusDet');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
            if($alphaNum!=null && $ind==null){
                $validacion=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $validacion=$ind;
            }else{
                $validacion=array_merge($alphaNum,$ind);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            }
            if(!isset($validacion['num_flag_pago_mensual'])){
                $validacion['num_flag_pago_mensual']=0;
            }

            if($idNomina==0){
                $id=$this->atTipoNomina->metCrearTipoNomina($validacion['cod_tipo_nomina'],$validacion['ind_nombre_nomina'],$validacion['ind_titulo_boleta'],$validacion['num_flag_pago_mensual'],$validacion['num_estatus'],$validacion['pk_num_tipo_proceso'],$validacion['pk_num_tipo_documento']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atTipoNomina->metModificarTipoNomina($validacion['cod_tipo_nomina'],$validacion['ind_nombre_nomina'],$validacion['ind_titulo_boleta'],$validacion['num_flag_pago_mensual'],$validacion['num_estatus'],$validacion['pk_num_tipo_proceso'],$validacion['pk_num_tipo_documento'],$idNomina);
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idNomina']=$id;

            echo json_encode($validacion);
            exit;
        }

        if($idNomina!=0){
            $this->atVista->assign('formDB',$this->atTipoNomina->metMostrarTipoNomina($idNomina));
            $this->atVista->assign('formDBDet',$this->atTipoNomina->metMostrarDetTipoNomina($idNomina));
            $this->atVista->assign('numero',1);
            $this->atVista->assign('n',0);
            $this->atVista->assign('idNomina',$idNomina);
        }

        $this->atVista->assign('listadoDocumentos',$this->atDocumento->metDocumentoListar());
        $this->atVista->assign('listadoProceso',$this->atTipoNomina->metListarTipoProceso());
        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    public function metEliminar()
    {
        $idNomina = $this->metObtenerInt('idNomina');
        if($idNomina!=0){
            $id=$this->atTipoNomina->metEliminarTipoNomina($idNomina);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el proceso se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idNomina'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT * FROM nm_b001_tipo_nomina ";
        if ($busqueda['value']) {
            $sql .= " WHERE
                    ( 
                    cod_tipo_nomina LIKE '%$busqueda[value]%' OR
                    ind_nombre_nomina LIKE '%$busqueda[value]%'
                    )
                    ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('cod_tipo_nomina','ind_nombre_nomina','num_flag_pago_mensual','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_tipo_nomina';
        #los valores del listado con flag 'md md-check'
        $flags = array('num_flag_pago_mensual');
        #construyo el listado de botones
        if (in_array('NM-01-03-02-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static" idNomina="'.$clavePrimaria.'" title="Editar"
                        descipcion="El Usuario a Modificado un Proceso de Nomina" titulo="Modificar Proceso">
                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('NM-01-03-02-03-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idNomina="'.$clavePrimaria.'" boton="si, Eliminar" title="Eliminar"
                        descipcion="El usuario a eliminado un Proceso" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la el Proceso!!">
                    <i class="md md-delete" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,false,$flags);
    }
}