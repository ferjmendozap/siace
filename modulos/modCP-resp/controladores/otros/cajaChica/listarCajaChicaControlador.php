<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class listarCajaChicaControlador extends Controlador
{
    private $atFPDF;
    private $atListarCajaChicaModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atListarCajaChicaModelo = $this->metCargarModelo('listarCajaChica');
        $this->metObtenerLibreria('cabeceraCP','modCP');
        $this->atFPDF = new pdf('L','mm','Legal');

    }
    public function metIndex($estado=false)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('estado', $estado);
        $listado = $this->atVista->assign('cajaBD',$this->atListarCajaChicaModelo->metListarCajaChica($estado));

        $this->atVista->assign('dependencia', $this->atListarCajaChicaModelo->metListarDependencia());
        $this->atVista->assign('listado', $listado);
        $this->atVista->metRenderizar('listado');

    }

    public function metImpuesto($campo)
    {
        $idTipoServicio = $this->metObtenerInt('idTipoServicio');
        if($campo=='IVA'){
            $consulta = $this->atListarCajaChicaModelo->metBuscarImpuesto($idTipoServicio,'N');
        }else{
            $consulta = $this->atListarCajaChicaModelo->metBuscarImpuesto($idTipoServicio,'I');
        }
            if (!isset($consulta['num_factor_porcentaje'])) {
                $consulta['num_factor_porcentaje'] = 0;
            }
            echo json_encode($consulta);
            exit;
    }

    public function metListaServicio($idCampo)
    {
        $campo['idSelect']=$idCampo;
        $idRegimen = $this->metObtenerInt('idRegimen');
        $campo['id'] = $this->atListarCajaChicaModelo->metListarServicio($idRegimen);
        echo json_encode($campo);
        exit;
    }

    public function metEmpleado ($empleado, $idCampo=false) {
        $this->atVista->assign('lista', $this->atListarCajaChicaModelo->metListaEmpleado());
        $this->atVista->assign('listaEmpleado', $empleado);
        $this->atVista->assign('idCampo', $idCampo);
        $this->atVista->metRenderizar('listadoEmpleado', 'modales');

    }

    public function metConcepto($concepto,$idCampo = false)
    {
        $this->atVista->assign('tr',$this->metObtenerInt('tr'));
        $this->atVista->assign('lista',$this->atListarCajaChicaModelo->metConceptoGastoListar());
        $this->atVista->assign('listaConcepto', $concepto);
        $this->atVista->assign('idCampo', $idCampo);

        $this->atVista->assign('listadoRegimen',$this->atListarCajaChicaModelo->atObligacionModelo->atMiscelaneoModelo->metMostrarSelect('REGFISCAL'));
        $this->atVista->assign('servicios',$this->atListarCajaChicaModelo->atServicioModelo->metServiciosListar());
        $this->atVista->assign('documento',$this->atListarCajaChicaModelo->metDocumentoListar());

        $this->atVista->metRenderizar('listadoConcepto', 'modales');
    }

    public function metBuscarAutorizacion(){
        $idBeneficiario = $this->metObtenerInt('idBeneficiario');
        $resultado = $this->atListarCajaChicaModelo->metListarAutorizacion($idBeneficiario);

        echo json_encode($resultado);
        exit;
    }

    public function metImprimir(){
        $idCajaChica = $_GET['idCajaChica'];
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->AddPage();
        $this->atFPDF->SetAutoPageBreak(true,10);

        //	Cabecera de página
        $this->atFPDF->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5, utf8_decode('CONTRALORIA DEL ESTADO MONAGAS'), 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(300, 5); $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'R');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetXY(300, 10); $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'R');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        $this->atFPDF->Ln(10);
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->Cell(20, 5, utf8_decode('Reporte de Caja Chica'), 0, 0, 'L');
        $this->atFPDF->Ln(5);

        //	imprimo datos generales
        $cajaChica = $this->atListarCajaChicaModelo->metConsultarCajaChica($idCajaChica);
        $this->atFPDF->SetFillColor(245, 245, 245);
        $this->atFPDF->SetFont('Arial', 'B', 6); $this->atFPDF->Cell(20, 5, utf8_decode('Caja Chica: '), 0, 0, 'L');
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 6); $this->atFPDF->Cell(20, 5, utf8_decode($cajaChica['ind_num_caja_chica']), 0, 0, 'L');
        $this->atFPDF->SetFillColor(245, 245, 245);
        $this->atFPDF->SetFont('Arial', 'B', 6); $this->atFPDF->Cell(20, 5, utf8_decode('Clasificación: '), 0, 0, 'L');
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 6); $this->atFPDF->Cell(20, 5, utf8_decode($cajaChica['clasificacion']), 0, 1, 'L');
        $this->atFPDF->SetFillColor(245, 245, 245);
        $this->atFPDF->SetFont('Arial', 'B', 6); $this->atFPDF->Cell(20, 5, utf8_decode('Empleado: '), 0, 0, 'L');
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 6); $this->atFPDF->Cell(20, 5, utf8_decode($cajaChica['nombre_empleado']), 0, 1, 'L');

        //	imprimo cuerpo
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetWidths(array(5, 20, 40, 80, 40, 20, 16, 16, 16, 16, 19, 19, 12, 16));
        $this->atFPDF->SetAligns(array('C', 'C', 'L', 'L', 'L', 'C', 'R', 'R', 'R', 'R', 'C', 'C', 'C', 'R'));
        $this->atFPDF->Row(array('#',
            'Fecha',
            'Concepto',
            utf8_decode('Descripción'),
            'Proveedor',
            'Documento',
            'Monto Afecto',
            'Monto No Afecto',
            'Monto Impuesto',
            'Monto Total',
            'Partida',
            'Cuenta',
            'C.C',
            'Monto Distrib.'
        ));


//	imprimo cuerpo
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 6);

        $reposicionC = $this->atListarCajaChicaModelo->metMostrarDetalleReposicion($idCajaChica);
        $montoAfecto = 0;
        $montoNoAfecto = 0;
        $montoImpuesto = 0;
        $montoDist = 0;
        foreach ($reposicionC AS $reposicion) {
            $montoAfecto = $montoAfecto +  $reposicion['num_monto_afecto'];
            $montoNoAfecto = $montoNoAfecto +  $reposicion['num_monto_no_afecto'];
            $montoImpuesto = $montoImpuesto +  $reposicion['num_monto_impuesto'];
            $montoDist = $montoDist +  $reposicion['num_monto_afecto'];

            $this->atFPDF->Row(array($reposicion['num_secuencia'],
                utf8_decode($reposicion['fec_documento']),
                utf8_decode($reposicion['nombreConcepto']),
                utf8_decode($reposicion['ind_descripcion']),
                utf8_decode($reposicion['persona']),
                $reposicion['documento'].'-'.$reposicion['num_documento'],
                number_format($reposicion['num_monto_afecto'], 2, ',', '.'),
                number_format($reposicion['num_monto_no_afecto'], 2, ',', '.'),
                number_format($reposicion['num_monto_impuesto'], 2, ',', '.'),
                number_format($reposicion['num_monto_pagado'], 2, ',', '.'),
                $reposicion['cod_partida'],
                $reposicion['cod_cuenta'],
                $reposicion['centroCosto'],
                number_format($reposicion['num_monto_afecto'], 2, ',', '.'),

            ));
        }
        //---------------------------------------------------
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(0, 0, 0);
        $y = $this->atFPDF->GetY();
        $this->atFPDF->Rect(10, $y, 335, 0.1, "FD");
        $this->atFPDF->SetY($y+2);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->Row(array('',
            '',
            '',
            '',
            '',
            '',
            number_format($montoAfecto, 2, ',', '.'),
            number_format($montoNoAfecto, 2, ',', '.'),
            number_format($montoImpuesto, 2, ',', '.'),
            number_format($montoAfecto+$montoNoAfecto+$montoImpuesto, 2, ',', '.'),
            '',
            '',
            '',
            number_format($montoDist, 2, ',', '.'),
        ));
//---------------------------------------------------
//	Muestro el contenido del pdf.

        $this->atFPDF->Output();
    }

    public function metRegistrarObligacion($datosCajaChica){
        $obligacion = $this->atListarCajaChicaModelo->metRegistrarObligacion($datosCajaChica);

        echo json_encode($obligacion);
        exit;
    }

    public function metNuevaCajaChica(){

        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $idCajaChica = $this->metObtenerInt('idCajaChica');
        $ver = $this->metObtenerInt('ver');
        $valido = $this->metObtenerInt('valido');
        $estado = $this->metObtenerAlphaNumerico('estado');
        $motivoAnulacion = $this->metObtenerAlphaNumerico('motivoAnulacion');

        if ($valido == 1) {
            if($estado=='PR' ||  $estado=='AN') {

                if ($estado == 'PR') {
                    $año = 2017;
                    $partida = $this->atListarCajaChicaModelo->metMostrarDetalleReposicion($idCajaChica);
                    $respuesta="";
                    foreach ($partida AS $titulo => $valor) {
                        $disponibilidad = $this->atListarCajaChicaModelo->metConsultaDisponibilidad($partida[$titulo]['fk_prb002_num_partida_presupuestaria'], $año);
                        if ($disponibilidad['monto'] > $disponibilidad['montoDisponible']) {
                            $respuesta['status'] = 'error';
                            $respuesta['Mensaje'] = array('titulo' => 'ERROR', 'contenido' => 'Se encontró la partida '.$disponibilidad['cod_partida'].' sin disponibilidad presupuestaria');

                            echo json_encode($respuesta);
                            exit;
                        }
                    }

                    //	actualizo el estado de la caja chica
                    $id = $this->atListarCajaChicaModelo->metAprobarCajaChica($idCajaChica);
                    $registro['Mensaje'] = array('titulo' => 'Aprobado', 'contenido' => 'La Caja Chica fue aprobada Satisfactoriamente');

                } elseif($estado=='AN'){
                    if($motivoAnulacion==''){
                        $validacion['status']='error';
                        $validacion['ind_razon_rechazo']='error';
                        echo json_encode($validacion);
                        exit;
                    }
                $id=$this->atListarCajaChicaModelo->metAnularCajaChica($idCajaChica);
                $registro['Mensaje'] = array('titulo'=>'Anulado','contenido'=>'La Caja Chica fue anulada Satisfactoriamente');
                }


                if (is_array($id)) {
                    $registro['status'] = 'errorSQL';
                    echo json_encode($id);
                    exit;
                }
                $registro['idCajaChica'] = $id;
                $registro['status'] = 'OK';
                echo json_encode($registro);
                exit;
            }

            $ExcepcionAlphaNum = array('ind_razon_rechazo');

            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum', $ExcepcionAlphaNum);
            $txt = $this->metValidarFormArrayDatos('form', 'txt');
            $ind = $this->metValidarFormArrayDatos('form', 'int');

            if ($alphaNum != null && $ind == null && $txt == null) {
                $registro = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt == null) {
                $registro = $ind;
            } elseif ($alphaNum != null && $ind != null && $txt == null) {
                $registro = array_merge($alphaNum, $ind);
            } elseif ($alphaNum != null && $txt != null && $ind == null) {
                $registro = array_merge($alphaNum, $txt);
            } elseif ($ind != null && $txt != null && $alphaNum == null) {
                $registro = array_merge($ind, $txt);
            } else {
                $registro = array_merge($ind, $txt, $alphaNum);
            }
            if (!isset($registro['detalleReposicion']['num_secuencia'])){
                $registro['status'] = 'errorDetalleReposicion';
                echo json_encode($registro);
                exit;
            }
            if (in_array('error', $registro)) {
                $registro['status'] = 'error';
                echo json_encode($registro);
                exit;
            }
            if ($idCajaChica == 0) {
                $porcentajeMontoMaximo = Session::metObtener('REPCC');
                $totalPagado =$registro['num_monto_total'];

                $montoReposicionMax = $totalPagado * $porcentajeMontoMaximo / 100;
                if ($montoReposicionMax > $totalPagado) {
                    $registro['status'] = 'montoExcedido';
                    echo json_encode($totalPagado);
                    exit;
                }else{
                    $id=$this->atListarCajaChicaModelo->metNuevaCajaChica($registro);
                    $registro['status'] = 'nuevo';
                }

            } else {
                $porcentajeMontoMaximo = Session::metObtener('REPCC');
                $totalPagado =$registro['num_monto_total'];

                $montoReposicionMax = $totalPagado * $porcentajeMontoMaximo / 100;
                if ($montoReposicionMax > $totalPagado) {
                    $registro['status'] = 'montoExcedido';
                    echo json_encode($totalPagado);
                    exit;
                }else{
                    $id=$this->atListarCajaChicaModelo->metModificarCajaChica($idCajaChica,$registro);
                    $registro['status'] = 'modificar';
                }
            }
            if (is_array($id)) {
                foreach ($registro as $titulo => $valor) {
                    if(!is_array($registro[$titulo])){
                        if (strpos($id[2], $registro[$titulo])) {
                            $registro[$titulo] = 'error';
                        }
                    }
                }
                $registro['status'] = 'errorSQL';
                $registro['id'] = $id;
                echo json_encode($registro);
                exit;
            }

            $registro['idCajaChica'] = $id;
            echo json_encode($registro);
            exit;
        }

        if ($idCajaChica != 0) {
            $this->atVista->assign('cajaChicaBD', $this->atListarCajaChicaModelo->metConsultarCajaChica($idCajaChica));
            $this->atVista->assign('reposicion', $this->atListarCajaChicaModelo->metMostrarDetalleReposicion($idCajaChica));
        }

        if($estado!=''){
            $this->atVista->assign('estado', $estado);
        }
        $iva = $this->atListarCajaChicaModelo->atObligacionModelo->metBuscar('cp_b015_impuesto','cod_impuesto',Session::metObtener('IVGCODIGO'));
        $cuenta = $this->atListarCajaChicaModelo->atObligacionModelo->metBuscarCuenta($iva['fk_cbb004_num_plan_cuenta']);
        $cuenta20 = $this->atListarCajaChicaModelo->atObligacionModelo->metBuscarCuenta($cuentaIva20 = $iva['fk_cbb004_num_plan_cuenta_pub20']);
        $partidaIva = $this->atListarCajaChicaModelo->atObligacionModelo->metBuscar('pr_b002_partida_presupuestaria','cod_partida',Session::metObtener('IVADEFAULT'));
        $partida = $this->atListarCajaChicaModelo->atObligacionModelo->metBuscarPartidaCuenta($partidaIva['pk_num_partida_presupuestaria']);

        $this->atVista->assign('idCajaChica', $idCajaChica);
        $this->atVista->assign('cuenta', $cuenta);
        $this->atVista->assign('cuenta20', $cuenta20);



        $this->atVista->assign('partida', $partida);
        $this->atVista->assign('dependencia', $this->atListarCajaChicaModelo->metListarDependencia());
        $this->atVista->assign('ver', $ver);
        $this->atVista->assign('autorizacion',$this->atListarCajaChicaModelo->metListarAutorizacion());
        $this->atVista->assign('empleado',$this->atListarCajaChicaModelo->metListarEmpleado());
        $this->atVista->assign('listadoTipoPago',$this->atListarCajaChicaModelo->atObligacionModelo->atMiscelaneoModelo->metMostrarSelect('TDPLG'));
        $this->atVista->assign('clasifGasto',$this->atListarCajaChicaModelo->metClasificacionGastoListar());
        $this->atVista->assign('documento',$this->atListarCajaChicaModelo->metDocumentoListar());
        $this->atVista->assign('conceptoGasto',$this->atListarCajaChicaModelo->metConceptoGastoListar());
        $this->atVista->assign('listadoRegimen',$this->atListarCajaChicaModelo->atObligacionModelo->atMiscelaneoModelo->metMostrarSelect('REGFISCAL'));
        $this->atVista->assign('servicios',$this->atListarCajaChicaModelo->atServicioModelo->metServiciosListar());

        if($estado=='GE'){
            $this->atVista->assign('parametroRevisar', Session::metObtener('REVOBLIG'));
            $datosCajaChica = $this->atListarCajaChicaModelo->metConsultarCajaChica($idCajaChica);
            $datosDetalleCajaChica = $this->atListarCajaChicaModelo->metMostrarDetalleReposicion($datosCajaChica['pk_num_caja_chica']);
            if ($datosDetalleCajaChica != "") {
                $montoAfecto = 0;
                $montoNoAfecto = 0;
                $montoImpuesto = 0;
                $montoRetencion = 0;
                $montoTotalObligacion = 0;
                foreach ($datosDetalleCajaChica AS $reposicion) {
                    $montoAfecto = $montoAfecto + $reposicion['num_monto_afecto'];
                    $montoNoAfecto = $montoNoAfecto + $reposicion['num_monto_no_afecto'];
                    $montoImpuesto = $montoImpuesto + $reposicion['num_monto_impuesto'];
                    $montoRetencion = $montoRetencion + $reposicion['num_monto_retencion'];
                    $montoTotalObligacion = $montoTotalObligacion + $reposicion['num_monto_pagado'];
                }
            }

            $tipoDocumento = $this->atListarCajaChicaModelo->metBuscarTiipoDocumento(Session::metObtener('DOCCHICA'));
            $this->atVista->assign('listadoDocumento', $this->atListarCajaChicaModelo->atObligacionModelo->metDocumentoListar());
            $tipoServicio = $this->atListarCajaChicaModelo->metBuscarTipoServicio(Session::metObtener('TSERVCC'));
            $this->atVista->assign('listadoServicio', $this->atListarCajaChicaModelo->atObligacionModelo->atServicioModelo->metServiciosListar());
            $this->atVista->assign('listadoCuentas', $this->atListarCajaChicaModelo->atObligacionModelo->atCuentasModelo->metCuentaListar());
            $obligacionBD=array(
                'fk_a003_num_persona_proveedor' => $datosCajaChica['pk_persona'],
                'proveedor' => $datosCajaChica['nombre_empleado'],
                'fk_a003_num_persona_proveedor_a_pagar' => $datosCajaChica['pk_persona'],
                'proveedorPagar' => $datosCajaChica['nombrePagar'],
                'documentoFiscal' => $datosCajaChica['documentoFiscal'],
                'fk_a023_num_centro_de_costo' => $datosCajaChica['fk_a023_num_centro_costo'],
                'fk_cpb002_num_tipo_documento' => $tipoDocumento['pk_num_tipo_documento'],
                'ind_nro_control' => '00'.$datosCajaChica['ind_num_caja_chica'],
                'ind_nro_factura' => '00'.$datosCajaChica['ind_num_caja_chica'],
                'ind_comentarios' => $datosCajaChica['ind_descripcion'],
                'ind_comentarios_adicional' => $datosCajaChica['ind_descripcion'],
                'fk_cpb017_num_tipo_servicio' => $tipoServicio['pk_num_tipo_servico'],
                'fk_a006_num_miscelaneo_tipo_pago' => $datosCajaChica['fk_a006_num_miscelaneo_detalle_tipo_pago'],
                'fk_rhb001_num_empleado_ingresa'=> $datosCajaChica['fk_rhb001_num_empleado_prepara'],
                'EMPLEADO_INGRESA'=> $datosCajaChica['EMPLEADO_PREPARA'],
                'fec_preparacion'=> $datosCajaChica['fec_preparacion'],
                'fk_cpb014_num_cuenta'=> $datosCajaChica['ctaBancaria'],
                'num_flag_caja_chica'=> 1,
                'num_flag_pago_individual'=> 0,
                'num_flag_obligacion_auto'=> 0,
                'num_flag_diferido'=> 0,
                'num_flag_afecto_IGV'=> 0,
                'num_flag_compromiso'=> 1,
                'num_flag_presupuesto'=> 1,
                'num_flag_obligacion_directa'=> 0,
                'num_monto_afecto'=> $montoAfecto,
                'num_monto_no_afecto'=> $montoNoAfecto,
                'num_monto_impuesto'=> $montoImpuesto,
                'num_monto_impuesto_otros'=> $montoRetencion,
                'num_monto_obligacion'=> $montoTotalObligacion,
                'num_monto_adelanto'=> 0,
                'num_monto_pago_parcial'=> 0,
                'ind_tipo_procedencia'=> 'CC'
            );
            if($montoRetencion != 0){
                $secuencia=1;
                $impuestoBD[]=array(
                    'ind_secuencia' => $secuencia,
                    'pk_num_obligacion_impuesto' => $montoImpuesto,
                    'num_monto_afecto' => 15
                );
                $this->atVista->assign('impuestoBD',$impuestoBD);
            }
            $ii = 1;
            for ($i = 0; $i < count($datosDetalleCajaChica); $i++) {
                $partidaBD[$ii]= array(
                    'num_secuencia' => $ii,
                    'fk_cbb004_num_cuenta' => $datosDetalleCajaChica[$i]['fk_cbb004_num_plan_cuenta'],
                    'fk_cbb004_num_cuenta_pub20' => $datosDetalleCajaChica[$i]['fk_cbb004_num_plan_cuentaPub20'],
                    'fk_prb002_num_partida_presupuestaria' => $datosDetalleCajaChica[$i]['fk_prb002_num_partida_presupuestaria'],
                    'codigoPartida' => $datosDetalleCajaChica[$i]['cod_partida'],
                    'descripcionPartida' => $datosDetalleCajaChica[$i]['ind_denominacion'],
                    'codigoCuenta' => $datosDetalleCajaChica[$i]['cod_cuenta'],
                    'descripcionCuenta' => $datosDetalleCajaChica[$i]['nombreCuenta'],
                    'codigoCuenta20' => $datosDetalleCajaChica[$i]['cod_cuenta20'],
                    'descripcionCuenta20' => $datosDetalleCajaChica[$i]['nombreCuenta20'],
                    'ind_descripcion' => '',
                    'fk_a023_num_centro_costo' => $datosDetalleCajaChica[$i]['fk_a023_num_centro_costo'],
                    'fk_a003_num_persona_proveedor' => $datosDetalleCajaChica[$i]['pk_num_persona'],
                    'num_monto' =>  $datosDetalleCajaChica[$i]['num_monto_afecto'],
                    'num_monto_afecto' => $datosDetalleCajaChica[$i]['num_monto_afecto'],
                    'num_flag_no_afecto' => 0
                );
                $ii++;
            }

            $this->atVista->assign('partidaBD',$partidaBD);
            $this->atVista->assign('obligacionBD',$obligacionBD);
            $this->atVista->metRenderizar('generarObligacion', 'modales');

        }else{
            $this->atVista->metRenderizar('nuevo', 'modales');
        }
    }
}