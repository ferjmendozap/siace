<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class viaticosControlador extends Controlador
{
    private $atViaticosModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atViaticosModelo = $this->metCargarModelo('viaticos');


    }
    public function metIndex($estado=false)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('estado', $estado);
        if($estado){
            $this->atVista->assign('solicitudBD',$this->atViaticosModelo->metListarViaticos($estado));
        }else{
            $this->atVista->assign('solicitudBD',$this->atViaticosModelo->metListarSolicitudViaticos());
        }
        $this->atVista->metRenderizar('listado');

    }

    public function metConceptosViaticos($parametro = false, $idCampo = false)
    {
        $this->atVista->assign('parametro', $parametro);
        $this->atVista->assign('idBeneficiario', $this->metObtenerInt('idBeneficiario'));
        $this->atVista->assign('idCampo', $idCampo);
        $this->atVista->assign('dataBD', $this->atViaticosModelo->atConceptoViaticosModelo->metListarConceptoViatico());
        $this->atVista->assign('unidadTributaria', $this->atViaticosModelo->metBuscarUnidadTrib());
        $this->atVista->metRenderizar('listadoConcepto', 'modales');
    }

    public function metAccionesViatico()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $idSolViatico = $this->metObtenerInt('idSolViatico');
        $idViatico = $this->metObtenerInt('idViatico');
        $ver = $this->metObtenerInt('ver');
        $estado = $this->metObtenerAlphaNumerico('estado');
        $idBeneficiario = $this->metObtenerInt('idBeneficiario');
        $valido = $this->metObtenerAlphaNumerico('valido');
        $accion = $this->metObtenerAlphaNumerico('accion');

        if ($valido == 1) {

            if($estado == 'RE' || $estado == 'AP' || $estado == 'AN' || $estado == 'RECHAZO'){
                if($estado == 'RE'){
                    $id = $this->atViaticosModelo->metRevisarViatico($idViatico);
                    $validacion['Mensaje'] = array('titulo'=>'Revisado','contenido'=>'El viático fue revisado Satisfactoriamente');
                }elseif($estado == 'AP'){
                    $id = $this->atViaticosModelo->metAprobarViatico($idViatico);
                    $validacion['Mensaje'] = array('titulo'=>'APROBADA','contenido'=>'El viático fue aprobado Satisfactoriamente');
                }
                elseif($estado == 'AN'){
                    $id = $this->atViaticosModelo->metAnularViatico($idViatico);
                    $validacion['Mensaje'] = array('titulo'=>'ANULADO','contenido'=>'El viático fue anulado Satisfactoriamente');
                }elseif($estado == 'RECHAZO'){
                    $id = $this->atViaticosModelo->metRechazarViatico($idViatico);
                    $validacion['Mensaje'] = array('titulo'=>'RECHAZADO','contenido'=>'El viático fue rechazado Satisfactoriamente');
                }
                if (is_array($id)) {
                    foreach ($validacion as $titulo => $valor) {
                        if(is_array($validacion[$titulo])) {
                            if (strpos($id[2], $titulo)) {
                                $validacion[$titulo] = 'error';
                            }
                        }
                    }
                    $validacion['status'] = 'errorSQL';
                    echo json_encode($validacion);
                    exit;
                }
                $validacion['idViatico'] = $id;
                $validacion['status'] = 'OK';
                echo json_encode($validacion);
                exit;
            }
            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');
            $txt = $this->metValidarFormArrayDatos('form', 'txt');
            $ind = $this->metValidarFormArrayDatos('form', 'int');
            $formula = $this->metValidarFormArrayDatos('form', 'formula');

            if ($alphaNum != null && $ind == null && $txt == null && $formula == null) {
                $registro = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt == null && $formula == null) {
                $registro = $ind;
            }elseif ($alphaNum == null && $ind == null && $txt != null && $formula == null) {
                $registro = $txt;
            }elseif ($alphaNum == null && $ind == null && $txt == null && $formula != null) {
                $registro = $formula;
            } elseif ($alphaNum != null && $ind != null && $txt == null && $formula == null) {
                $registro = array_merge($alphaNum, $ind);
            } elseif ($alphaNum != null && $ind == null && $txt != null && $formula == null) {
                $registro = array_merge($alphaNum, $txt);
            } elseif ($alphaNum != null && $ind == null && $txt == null && $formula != null) {
                $registro = array_merge($alphaNum, $formula);
            } elseif ($alphaNum != null && $ind != null && $txt != null && $formula == null) {
                $registro = array_merge($alphaNum, $ind, $txt);
            } elseif ($alphaNum != null && $ind != null && $txt == null && $formula != null) {
                $registro = array_merge($alphaNum, $ind, $formula);
            } else {
                $registro = array_merge($ind, $txt, $alphaNum,$formula);
            }
            if ($idViatico == 0) {
                $registro['status'] = 'nuevo';
                $id = $this->atViaticosModelo->metNuevoViatico($idSolViatico, $registro);
                $registro['idViatico'] = $id;
                $idNuevo = $this->atViaticosModelo->metConsultarViaticos($registro['idViatico']);
            }else {
                if(!isset($registro['conceptoViatico'])){
                    $validacion['status'] = 'errorConceptoViatico';
                    echo json_encode($validacion);
                    exit;
                }
                $registro['status'] = 'modificar';
                $id = $this->atViaticosModelo->metModificarViatico($idViatico, $registro );
                $registro['idViatico'] = $id;
                //$idNuevo = $this->atViaticosModelo->metConsultarViaticos($registro['idViatico']);
            }
            if (is_array($id)) {
                foreach ($registro as $titulo => $valor) {
                    if(!is_array($registro[$titulo])){
                        if (strpos($id[2], $registro[$titulo])) {
                            $registro[$titulo] = 'error';
                        }
                    }
                }
                $registro['status'] = 'errorSQL';
                echo json_encode($id);
                exit;
            }
            $idNuevo['status']=$registro['status'];
            echo json_encode($idNuevo);
            exit;

        }
        if ($idViatico != 0) {
            $this->atVista->assign('viaticoBD', $this->atViaticosModelo->metConsultarViaticos($idViatico));
            $this->atVista->assign('solicitudBD', $this->atViaticosModelo->metConsultarSolicitudViaticos($idSolViatico));
        }

        $this->atVista->assign('idSolViatico', $idSolViatico);
        $this->atVista->assign('idViatico', $idViatico);
        $this->atVista->assign('idBeneficiario', $idBeneficiario);
        $this->atVista->assign('ver', $ver);
        $this->atVista->assign('estado', $estado);

        $this->atVista->assign('unidadTributaria', $this->atViaticosModelo->metBuscarUnidadTrib());
        $this->atVista->assign('solicitudBD', $this->atViaticosModelo->metConsultarSolicitudViaticos($idSolViatico));
        $this->atVista->assign('detalleViaticoBD', $this->atViaticosModelo->metConsultarDetalleViaticos($idViatico));
        $this->atVista->assign('resolucion',Session::metObtener('RESVIAT'));
        if($idBeneficiario) {
            $this->atVista->assign('solicitudDetalleBD', $this->atViaticosModelo->metConsultarDetalleSolViaticos($idSolViatico, $idBeneficiario));
            $datoBusqueda = $this->atViaticosModelo->metBuscarDependenciaCC($idBeneficiario);
            $this->atVista->assign('datoBusqueda',$datoBusqueda );
            $this->atVista->assign('cargo', $this->atViaticosModelo->metBuscarCargo($idBeneficiario));
            if($accion == 'calcular' or $accion == 'editar'){
                $this->atVista->assign('estado',$accion);
                $this->atVista->metRenderizar('calculaViaticosEmpleado', 'modales');
            }elseif($accion == 'copiar') {
                $this->atVista->assign('idBeneficiario', $idBeneficiario);
                $this->atVista->assign('detalleBD', $this->atViaticosModelo->metConsultarDetalleSolViaticos($idSolViatico));
                $this->atVista->metRenderizar('copiarViaticosEmpleado', 'modales');
            }else{
                $solViatico =  $this->atViaticosModelo->metConsultarSolicitudViaticos($idSolViatico);
                $this->atVista->assign('parametroRevisar', Session::metObtener('REVOBLIG'));
                $datosViatico = $this->atViaticosModelo->metConsultarDetalleViaticos($idViatico, $idBeneficiario);
                $datoContableViatico = $this->atViaticosModelo->metConsultarDetalleContableViat($idViatico);
                $centroCosto = $this->atViaticosModelo->metBuscarDependenciaCC($idBeneficiario);

                $this->atVista->assign('listadoDocumento', $this->atViaticosModelo->atListarCajaChicaModelo->atObligacionModelo->metDocumentoListar());
                $this->atVista->assign('listadoServicio', $this->atViaticosModelo->atListarCajaChicaModelo->atObligacionModelo->atServicioModelo->metServiciosListar());
                $this->atVista->assign('listadoTipoPago',$this->atViaticosModelo->atListarCajaChicaModelo->atMiscelaneoModelo->metMostrarSelect('TDPLG'));
                $this->atVista->assign('listadoCuentas', $this->atViaticosModelo->atListarCajaChicaModelo->atObligacionModelo->atCuentasModelo->metCuentaListar());
                $tipoServicio = $this->atViaticosModelo->atListarCajaChicaModelo->metBuscarTipoServicio(Session::metObtener('TIPOSERVNM'));
                $tipoDocumento = $this->atViaticosModelo->atListarCajaChicaModelo->metBuscarTiipoDocumento(Session::metObtener('DOCVIAT'));

                if ($datosViatico != "") {
                    $montoNoAfecto = 0;     $montoTotalObligacion = 0;

                    foreach ($datosViatico AS $i) {
                        $montoNoAfecto = $montoNoAfecto + $i['num_monto_viatico'];
                        $montoTotalObligacion = $montoTotalObligacion + $i['num_monto_total'];
                        $nombreBeneficiario = $i['nombreBeneficiario'];
                        $rif = $i['ind_documento_fiscal'];
                        $fk_a003_persona_beneficiario = $i['fk_a003_persona_beneficiario'];
                        $tipoPago = $i['ind_nombre_detalle'];
                        $numVia = $i['ind_num_viatico'];
                        $nroControl = $i['ind_nro_control'];
                        $nro = date('Y').'-'.$i['ind_num_viatico'];
                        $cantCarat = strlen($nroControl);
                        if($cantCarat == 9){
                            $nro = $nro.'-1';
                        }else if($cantCarat > 9 && $cantCarat < 11){
                            $num = substr($nroControl,-1,1);
                            $valor = $num + 1;
                            $nro = $nro.'-'.$valor;
                        }else if($cantCarat > 11){
                            $num = substr($nroControl,-2,2);
                            $valor = $num + 1;
                            $nro = $nro.'-'.$valor;
                        }

                    }
                }

                $obligacionBD=array(
                    'fk_a003_num_persona_proveedor' => $fk_a003_persona_beneficiario,
                    'proveedor' => $nombreBeneficiario,
                    'fk_a003_num_persona_proveedor_a_pagar' => $fk_a003_persona_beneficiario,
                    'proveedorPagar' => $nombreBeneficiario,
                    'documentoFiscal' => $rif,
                    'fk_a023_num_centro_de_costo' => $centroCosto['pk_num_centro_costo'],
                    'fk_cpb002_num_tipo_documento' => $tipoDocumento['pk_num_tipo_documento'],
                    'ind_comentarios' => $solViatico['ind_motivo'],
                    'ind_comentarios_adicional' => $solViatico['ind_motivo'],
                    'fk_cpb017_num_tipo_servicio' => $tipoServicio['pk_num_tipo_servico'],
                    'ind_nombre_detalle' => $tipoPago,
                    //'fk_cpb014_num_cuenta'=> $datosCajaChica['ctaBancaria'],
                    'num_flag_caja_chica'=> 0,
                    'num_flag_pago_individual'=> 0,
                    'num_flag_obligacion_auto'=> 0,
                    'num_flag_diferido'=> 0,
                    'num_flag_afecto_IGV'=> 0,
                    'num_flag_compromiso'=> 1,
                    'num_flag_presupuesto'=> 1,
                    'num_flag_obligacion_directa'=> 0,
                    'num_monto_no_afecto'=> $montoTotalObligacion,
                    'num_monto_impuesto'=> 0,
                    'num_monto_impuesto_otros'=> 0,
                    'num_monto_afecto'=> 0,
                    'num_monto_obligacion'=> $montoTotalObligacion,
                    'num_monto_adelanto'=> 0,
                    'num_monto_pago_parcial'=> 0,
                    'ind_tipo_procedencia'=> 'VV',
                    'codDoc' => Session::metObtener('DOCVIAT'),
                    'ind_num_viatico' => $numVia,
                    'ind_nro_control' => $nro

                );
                $ii = 1; $monto = 0;
                for ($i = 0; $i < count($datosViatico); $i++) {
                    $monto = $monto + $datosViatico[$i]['num_monto_total'];
                    $partidaBD[$ii]= array(
                        'num_secuencia' => $ii,
                        'fk_cbb004_num_cuenta' => $datosViatico[$i]['fk_cbb004_num_plan_cuenta'],
                        'fk_cbb004_num_cuenta_pub20' => $datosViatico[$i]['fk_cbb004_num_plan_cuenta_pub20'],
                        'fk_prb002_num_partida_presupuestaria' => $datosViatico[$i]['fk_prb002_num_partida_presupuestaria'],
                        'codigoPartida' => $datosViatico[$i]['cod_partida'],
                        'descripcionPartida' => $datosViatico[$i]['ind_denominacion'],
                        'codigoCuenta' => $datosViatico[$i]['cod_cuenta'],
                        'descripcionCuenta' => $datosViatico[$i]['nombreCuenta'],
                        'codigoCuenta20' => $datosViatico[$i]['cod_cuenta20'],
                        'descripcionCuenta20' => $datosViatico[$i]['nombreCuenta20'],
                        'ind_descripcion' => '',
                        'fk_a023_num_centro_costo' => $datoBusqueda['pk_num_centro_costo'],
                        'fk_a003_num_persona_proveedor' => $fk_a003_persona_beneficiario,
                        'num_monto' =>  $monto,
                        'num_monto_afecto' => $monto,
                        'num_flag_no_afecto' => 0
                    );
                    $ii++;
                }

                $this->atVista->assign('partidaBD',$partidaBD);
                $this->atVista->assign('ver',1);
                $this->atVista->assign('obligacionBD',$obligacionBD);
                $this->atVista->metRenderizar('generarObligacion', 'modales');

            }

        }else{
            $this->atVista->assign('datoContableBD', $this->atViaticosModelo->metConsultarDetalleContableViat($idViatico));
            $this->atVista->assign('detalleBD', $this->atViaticosModelo->metConsultarDetalleSolViaticos($idSolViatico));
            $this->atVista->metRenderizar('nuevo', 'modales');
        }
    }

    public function metConceptos()
    {
        $idConcepto = $this->metObtenerInt('pkConcepto');
        $datoConcepto = $this->atViaticosModelo->metBuscarConcepto($idConcepto);
        echo json_encode($datoConcepto);
        exit;
    }

}