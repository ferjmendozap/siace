<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_Modulo.'modCP'.DS.'controladores'.DS.'trait'.DS.'funcionesTrait.php';
class procesosControlador extends Controlador
{
    use funcionesTrait;
    private $atProcesosModelo;
    private $atFPDF;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atProcesosModelo = $this->metCargarModelo('procesos');
        $this->metObtenerLibreria('cabeceraCP', 'modCP');

    }

    public function metIndex($lista=false)
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $complementoCss = array(
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        if ($lista == 'saldosBanco') {
            $this->atVista->assign('listadoCuentas', $this->atProcesosModelo->atCuentasModelo->metCuentaListar());
            $this->atVista->assign('selectBANCOS', $this->atProcesosModelo->atMiscelaneoModelo->metMostrarSelect('BANCOS'));

            $this->atVista->metRenderizar('saldosBanco');

        } elseif ($lista == 'saldosLibro') {
            $this->atVista->assign('listadoCuentas', $this->atProcesosModelo->atCuentasModelo->metCuentaListar());
            $this->atVista->assign('selectBANCOS', $this->atProcesosModelo->atMiscelaneoModelo->metMostrarSelect('BANCOS'));

            $this->atVista->metRenderizar('saldosLibro');

        } else {
            $this->atVista->assign('listadoCuentas', $this->atProcesosModelo->atCuentasModelo->metCuentaListar());
            $this->atVista->assign('selectBANCOS', $this->atProcesosModelo->atMiscelaneoModelo->metMostrarSelect('BANCOS'));

            $this->atVista->metRenderizar('conciliacionBancaria');
        }
    }

    public function metBuscar()
    {
        $idCuenta = $this->metObtenerInt('idCuenta');

        $fSaldo = $_POST['fSaldo'];
        $fconc = $_POST['fconc'];

        if(isset($_POST['periodo'])){
            $periodo = $_POST['periodo'];
        }else{
            $periodo = null;
        }
        if(isset($fSaldo) or isset($fconc)){
            if($fSaldo){
                $fecha = explode("-",$fSaldo);
                $fechaSaldo = $fecha[0].'-'.$fecha[1].'-'.$fecha[2];
            }else{
                $fechaSaldo = "";
            }
            if($fconc){
                $fecha = explode("-",$fconc);
                $fechaConc = $fecha[0].'-'.$fecha[1].'-'.$fecha[2];
            }else{
                $fechaConc = "";
            }
        }
        $id['actual'] = $this->atProcesosModelo->metListar($idCuenta, $fechaSaldo, $fechaConc, $periodo);
        $id['anterior'] = $this->atProcesosModelo->metListarAnterior($idCuenta, $fechaSaldo, $fechaConc, $periodo);
        echo json_encode($id);
        exit;
    }

    public function metCuentasBancarias()
    {
        $id['id'] = $this->atProcesosModelo->metBuscarCuentas($this->metObtenerInt('idBanco'));
        echo json_encode($id);
        exit;
    }

    public function metVerificarFecha()
    {
        $desde = $_POST['desde'];
        $hasta = $_POST['hasta'];

        $fecha1 = explode("-",$desde);
        $fechaDesde = $fecha1[0].'-'.$fecha1[1].'-'.$fecha1[2];

        $fecha = explode("-",$hasta);
        $fechaHasta = $fecha[0].'-'.$fecha[1].'-'.$fecha[2];

        if(strtotime($fechaDesde) >= strtotime($fechaHasta)) {
            echo json_encode(true);
        } else {
            echo json_encode(false);
        }
    }

    public function metAcciones()
    {
        $idTransaccion = $this->metObtenerInt('idTransaccion');
        $estado = $this->metObtenerAlphaNumerico('estado');
        $valido = $this->metObtenerAlphaNumerico('valido');
        $this->atVista->assign('idTransaccion', $idTransaccion);

        if($valido==1){
            $Excepcion = array('fec_fecha','fechaDesde','fechaHasta');
            $ExcepcionAlphaNum = array('ind_descripcion');
            $ExcepcionInt = array('num_bolivares','numero');

            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum',$ExcepcionAlphaNum);
            $txt = $this->metValidarFormArrayDatos('form', 'txt', $Excepcion);
            $ind = $this->metValidarFormArrayDatos('form', 'int',$ExcepcionInt);
            if ($alphaNum != null && $ind == null && $txt == null) {
                $registro = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt == null) {
                $registro = $ind;
            } elseif ($alphaNum != null && $ind != null && $txt == null) {
                $registro = array_merge($alphaNum, $ind);
            } elseif ($alphaNum != null && $txt != null && $ind == null) {
                $registro = array_merge($alphaNum, $txt);
            } elseif ($ind != null && $txt != null && $alphaNum == null) {
                $registro = array_merge($ind, $txt);
            } else {
                $registro = array_merge($ind, $txt, $alphaNum);
            }

            if (in_array('error', $registro)) {
                $registro['status'] = 'error';
                echo json_encode($registro);
                exit;
            }

            if($registro['fechaHasta']!='' and $registro['fechaDesde']!=''){
                $fechaHasta = $registro['fechaHasta'];
                $fecha = explode("-",$fechaHasta);
                $año= $fecha[0]; $mes = $fecha[1];
            }else{
                $año = date('Y');
                $mes = date('m');
            }
        //REGISTRO SEGUN BANCO
            if($estado == 'saldoBanco'){
                $registrar = $this->atProcesosModelo->metNuevoSaldo($registro, 'B', 'MAS DEPOSITO EN TRANSITO',$registro['num_estado_cuenta'], $año,$mes);
                    if ($registro['fec_fecha'] != '' ) {
                        $registrarO = $this->atProcesosModelo->metNuevaDifConciliacion($registro['fec_fecha'], 'a', 'D', $registro['ind_descripcion'], $registro['num_bolivares'], $registrar);
                    }

                //OBTENGO LOS DEPOSITOS EN TRANSITO
                $transaccionesCuenta = $this->atProcesosModelo->metBuscarTransaccionesCuenta($registro['fk_cpb014_num_cuenta_bancaria'], $registro['fechaHasta']);
                    foreach ($transaccionesCuenta as $i) {
                        $proveedor = $this->atProcesosModelo->metBuscarProveedor($i['fk_a003_num_persona_proveedor']);
                        $registrarO = $this->atProcesosModelo->metNuevaDifConciliacion($i['fec_transaccion'], 'c', $i['ind_num_cheque'], $proveedor['proveedor'], $i['num_monto_pago'], $registrar);
                    }

            }

         //REGISTRO SEGUN LIBRO
            if($estado == 'saldoLibro') {

                $estadoCuenta = $this->atProcesosModelo->metBuscarEstadoCuenta($registro['fk_cpb014_num_cuenta_bancaria'],$registro['fechaDesde'],$registro['fechaHasta']);
                if($estadoCuenta['monto'] != null){
                    $saldo = $estadoCuenta['monto'];
                }else{
                    $saldo = 0;
                }
                $registrar = $this->atProcesosModelo->metNuevoSaldo($registro, 'L', $registro['descripcion'], $saldo, $año, $mes);
                foreach ($registro['fec_fecha'] AS $key=>$value) {
                    $registrarO = $this->atProcesosModelo->metNuevaDifConciliacion($value, $registro['ind_carga_abono'], $registro['numero'][$key], $registro['ind_descripcion'][$key], $registro['num_bolivares'][$key], $registrar);

                }
            }

            if (is_array($registrar)) {
                $registro['status'] = 'errorSQL';
                echo json_encode($registro);
                exit;
            }
            $registro['idSaldoBanco'] = $registrar;
            $registro['status'] = 'ok';
            echo json_encode($registro);
            exit;

        }

        if ($estado == 'VER') {
            $this->atVista->assign('estado', $estado);
            $this->atVista->assign('listadoCuentas', $this->atProcesosModelo->atCuentasModelo->metCuentaListar());
            $this->atVista->assign('listadoTipoPago', $this->atProcesosModelo->atMiscelaneoModelo->metMostrarSelect('TDPLG'));
            $this->atVista->metRenderizar('nuevo', 'modales');
        }elseif ($estado == 'ACTUALIZAR') {

            $procesar = $this->atProcesosModelo->metActualizar($idTransaccion);

            if (is_array($procesar)) {
                $registro['status'] = 'errorSQL';
                echo json_encode($registro);
                exit;
            }
            $registro['idTransaccion'] = $procesar;
            $registro['num_flag_conciliacion'] = 1;
            $registro['status'] = 'ok';
            echo json_encode($registro);
            exit;
        }

    }

    public function metReportes($lista= false)
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('listadoCuentas', $this->atProcesosModelo->atCuentasModelo->metCuentaListar());
        $this->atVista->assign('selectBANCOS', $this->atProcesosModelo->atMiscelaneoModelo->metMostrarSelect('BANCOS'));

        if ($lista == 'saldoBanco'){
            $this->atVista->metRenderizar('saldoBancoReportes');
        }else{
            $this->atVista->metRenderizar('saldoLibroReportes');
        }
        $this->atVista->assign('lista', $lista);

    }

    public function metImprimirSaldoBanco()
    {
        $periodo = $_GET['periodo'];
            $fecha = explode("-",$periodo);
            $año = $fecha[0];
            $mes = $fecha[1];

        $idCuenta = $_GET['idCuenta'];

        $cuenta = $this->atProcesosModelo->metCuentaBancaria($idCuenta, $año,$mes);

        $detalleCuenta = $this->atProcesosModelo->metBuscardetalleCuenta($idCuenta,$año,$mes,'banco');
        $detalle = $this->atProcesosModelo->metBuscardetalle($idCuenta,$año,$mes,'banco', $detalleCuenta['diferenciaSaldoMax']);

            if ($detalle['num_estado_cuenta'] == '') {
                //$saldo = $cuenta['SaldoCuenta'] + $field_mast1['SaldoInicial'];
                $saldo = 0;
            } elseif ($detalle['num_estado_cuenta'] != '') {
                $saldo = $detalle['num_estado_cuenta'];
            }

        $this->atFPDF = new pdf('P', 'mm', 'Letter');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->AddPage();
        $this->atFPDF->SetFont('Times', '', 12);

        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 10, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->Cell(50, 5, '', 0, 0, 'C');
        $this->atFPDF->Cell(100, 5, utf8_decode('CONTRALORIA DEL ESTADO MONAGAS'), 0, 1, 'C');
        $this->atFPDF->Cell(50, 5, '', 0, 0, 'C');
        $this->atFPDF->Cell(104, 5, utf8_decode('CONCILIACION BANCARIA'), 0, 1, 'C');
        $this->atFPDF->Cell(50, 5, '', 0, 0, 'C');
        $this->atFPDF->Cell(100, 5, utf8_decode('SEGUN BANCO'), 0, 1, 'C');
        $this->atFPDF->Cell(50, 5, '', 0, 0, 'C');
        $this->atFPDF->Cell(100, 5, utf8_decode("Periodo " . $periodo . ""), 0, 1, 'C');
        $this->atFPDF->Cell(50, 5, '', 0, 0, 'C');
        $this->atFPDF->SetXY(20, 25);
        $this->atFPDF->Cell(25, 5, '   ', 0, 1, 'C');
        $this->atFPDF->Ln(2);

        $this->atFPDF->SetFillColor(202, 202, 202);
        $this->atFPDF->SetFont('Arial', 'B', 7);
        $this->atFPDF->Cell(30, 6, $cuenta['banco'], 0, 1, 'l');

        $this->atFPDF->SetFillColor(202, 202, 202);
        $this->atFPDF->SetFont('Arial', 'B', 7);
        $this->atFPDF->Cell(30, 6, "CUENTA Nro.: " . $cuenta['ind_num_cuenta'] . "", 0, 1, 'l');

        $this->atFPDF->SetFillColor(202, 202, 202);
        $this->atFPDF->SetFont('Arial', 'B', 7);
        $this->atFPDF->Cell(30, 6, "SALDOS SEGUN BANCO.:  " . number_format($saldo, 2, ',', '.') . "", 0, 1, 'l');
        //------------------
        $this->atFPDF->SetFillColor(202, 202, 202);
        $this->atFPDF->SetFont('Arial', 'B', 7);
        $this->atFPDF->Cell(30, 6, $detalle['ind_descripcion'], 0, 1, 'l');

        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 7);
        $this->atFPDF->SetWidths(array(35, 40, 50, 30));
        $this->atFPDF->SetAligns(array('C', 'C', 'C', 'C'));
        $this->atFPDF->Row(array('FECHA', 'Nro.  DEPOSITO', 'BOLIVARES'));

        $transacciones = $this->atProcesosModelo->metBuscarDiferenciaConciliacion($detalleCuenta['diferenciaSaldoMax'],'transacciones');
            $abono = 0;
            $montoD = 0;
            $TotalD = 0;
        foreach ($transacciones AS $trans) {
            $this->atFPDF->Row(
                array($trans['fec_fecha'], $trans['ind_descripcion'], $trans['num_bolivares'])
            );

            $abono = $abono + $trans['num_bolivares'];
            $montoD = $montoD + $trans['num_bolivares'];
            $TotalD = $TotalD + $trans['num_bolivares'];

        }
        $this->atFPDF->Cell(110, 5, 'TOTAL.: Bs.:  ', 0, 0, 'R', 0);
        $this->atFPDF->Cell(20, 5, number_format($TotalD, 2, ',', '.'), 0, 1, 'l');

        //-------
        $this->atFPDF->SetFillColor(202, 202, 202);
        $this->atFPDF->SetFont('Arial', 'B', 7);
        $this->atFPDF->Cell(30, 6, 'MENOS CHEQUES POR PAGAR (TRANSITO)', 0, 1, 'l');

        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 7);
        $this->atFPDF->SetWidths(array(35, 40, 50, 30));
        $this->atFPDF->SetAligns(array('C', 'C', 'C', 'C'));

        $this->atFPDF->Row(array('FECHA', 'Nro. CHEQUE', 'BENEFICIARIO', 'BOLIVARES'));

        // detalles de abono
        $transito = $this->atProcesosModelo->metBuscarDiferenciaConciliacion($detalleCuenta['diferenciaSaldoMax'], 'transito');
        $total = 0;
        $cargo = 0;
        $abono = 0;
        foreach ($transito AS $enTransito) {
            $this->atFPDF->Row(
                array($enTransito['fec_fecha'], $enTransito['ind_num_proceso'], $enTransito['ind_descripcion'], $enTransito['num_bolivares'])
            );

            if ($enTransito['ind_carga_abono'] == 'a') {
                $total = $abono;
            } elseif ($enTransito['ind_carga_abono'] == 'c') {
                $cargo = $cargo + ($enTransito['num_bolivares']);
                $total = $cargo;
            }

        }
        $this->atFPDF->Cell(143, 5, 'TOTAL.: Bs.:  ', 0, 0, 'R', 0);
        $this->atFPDF->Cell(20, 5, number_format($total, 2, ',', '.'), 0, 1, 'l');

        $Total2 = ($abono + ($saldo)) - ($cargo);

        $this->atFPDF->Cell(137, 5, 'SALDOS SEGUN LIBRO CORREGIDOS.:  Bs.:', 0, 0, 'R', 0);
        $this->atFPDF->Cell(20, 5, number_format($Total2, 2, ',', '.'), 0, 1, 'l');

        ////	Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }

    public function metImprimirSaldoLibro()
    {
        $periodo = $_GET['periodo'];
        $fecha = explode("-",$periodo);
        $año = $fecha[0];
        $mes = $fecha[1];

        $idCuenta = $_GET['idCuenta'];

        $cuenta = $this->atProcesosModelo->metCuentaBancaria($idCuenta, $año,$mes);

        $detalleCuenta = $this->atProcesosModelo->metBuscardetalleCuenta($idCuenta,$año,$mes, 'libro');
        $detalle = $this->atProcesosModelo->metBuscardetalle($idCuenta,$año,$mes, 'libro', $detalleCuenta['diferenciaSaldoMax']);
        if ($detalle['num_estado_cuenta'] == '') {
            //$saldo = $cuenta['SaldoCuenta'] + $field_mast1['SaldoInicial'];
            $saldo = 0;
        } elseif ($detalle['num_estado_cuenta'] != '') {
            $saldo = $detalle['num_estado_cuenta'];
        }

        $this->atFPDF = new pdf('P', 'mm', 'Letter');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->AddPage();
        $this->atFPDF->SetFont('Times', '', 12);

        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 10, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->Cell(50, 5, '', 0, 0, 'C');
        $this->atFPDF->Cell(100, 5, utf8_decode('CONTRALORIA DEL ESTADO MONAGAS'), 0, 1, 'C');
        $this->atFPDF->Cell(50, 5, '', 0, 0, 'C');
        $this->atFPDF->Cell(104, 5, utf8_decode('CONCILIACION BANCARIA'), 0, 1, 'C');
        $this->atFPDF->Cell(50, 5, '', 0, 0, 'C');
        $this->atFPDF->Cell(100, 5, utf8_decode('SEGUN LIBRO'), 0, 1, 'C');
        $this->atFPDF->Cell(50, 5, '', 0, 0, 'C');
        $this->atFPDF->Cell(100, 5, utf8_decode("Periodo " . $periodo . ""), 0, 1, 'C');
        $this->atFPDF->Cell(50, 5, '', 0, 0, 'C');
        $this->atFPDF->SetXY(20, 25);
        $this->atFPDF->Cell(25, 5, '   ', 0, 1, 'C');
        $this->atFPDF->Ln(2);

        $this->atFPDF->SetFillColor(202, 202, 202);
        $this->atFPDF->SetFont('Arial', 'B', 7);
        $this->atFPDF->Cell(30, 6, $cuenta['banco'], 0, 1, 'l');

        $this->atFPDF->SetFillColor(202, 202, 202);
        $this->atFPDF->SetFont('Arial', 'B', 7);
        $this->atFPDF->Cell(30, 6, "CUENTA Nro.: " . $cuenta['ind_num_cuenta'] . "", 0, 1, 'l');

        $this->atFPDF->SetFillColor(202, 202, 202);
        $this->atFPDF->SetFont('Arial', 'B', 7);
        $this->atFPDF->Cell(30, 6, "SALDOS SEGUN LIBRO.:  " . number_format($saldo, 2, ',', '.') . "", 0, 1, 'l');

        //------------------
        $Total = 0;
        $cargo = 0;
        $abono = 0;
        $detalleCuenta = $this->atProcesosModelo->metBuscarCuenta($idCuenta,$año,$mes, 'libro');
        foreach ($detalleCuenta AS $detalle) {

            $this->atFPDF->SetFillColor(202, 202, 202);
            $this->atFPDF->SetFont('Arial', 'B', 7);
            $this->atFPDF->Cell(30, 6, $detalle['ind_descripcion'], 0, 1, 'l');

            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', '', 7);
            $this->atFPDF->SetWidths(array(35, 40, 50, 30));
            $this->atFPDF->SetAligns(array('C', 'L', 'C', 'C'));
            $this->atFPDF->Row(array('FECHA', 'NUMERO', 'DESCRIPCION', 'BOLIVARES'));

            $transacciones = $this->atProcesosModelo->metBuscarDiferenciaConciliacion($detalle['pk_num_diferencia_saldo'], 'libro');

            foreach ($transacciones AS $trans) {
                $this->atFPDF->Row(
                    array(
                        $trans['fec_fecha'],
                        $trans['ind_num_proceso'],
                        $trans['ind_descripcion'],
                        number_format($trans['num_bolivares'], 2, ',', '.')
                    ));
                //$Total = $Total + $trans['num_bolivares'];

                if($trans['ind_carga_abono'] == 'a') {
                    $abono = $abono + $trans['num_bolivares'];
                    $Total = $abono;
                }
                elseif($trans['ind_carga_abono'] == 'c'){
                    $cargo = $cargo + $trans['num_bolivares'];
                    $Total = $cargo;
                }
            }
            $this->atFPDF->Cell(137, 5, 'TOTAL.: Bs.:  ', 0, 0, 'R', 0);
            $this->atFPDF->Cell(20, 5, number_format($Total, 2, ',', '.'), 0, 1, 'l');
        }
        $Total2=(($saldo-($cargo))+($abono));

        $this->atFPDF->Cell(137, 5, 'SALDOS SEGUN LIBRO CORREGIDOS .:  Bs.:', 0, 0, 'R', 0);
        $this->atFPDF->Cell(20, 5, number_format($Total2, 2, ',', '.'), 0, 1, 'l');


        ////	Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }

}