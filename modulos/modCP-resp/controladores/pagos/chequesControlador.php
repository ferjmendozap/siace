<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class chequesControlador extends Controlador
{
    private $atChequesModelo;
    private $atFPDF;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atChequesModelo = $this->metCargarModelo('cheques');
        $this->metObtenerLibreria('cabeceraCP', 'modCP');

    }

    public function metIndex($lista = false)
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        if ($lista == 'devolver') {
            $this->atVista->assign('dataBD', $this->atChequesModelo->metListarCheques($lista));
        } elseif ($lista == 'cobrarCheque') {
            $this->atVista->assign('dataBD', $this->atChequesModelo->metListarCheques($lista));
        } elseif ($lista == 'cobrado') {
            $this->atVista->assign('dataBD', $this->atChequesModelo->metListarCheques($lista));
        } else {
            $this->atVista->assign('dataBD', $this->atChequesModelo->metListarCheques());
        }
        $this->atVista->assign('lista', $lista);
        $this->atVista->metRenderizar('listado');

    }

    public function metAcciones()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $idPago = $this->metObtenerInt('idPago');
        $estado = $this->metObtenerAlphaNumerico('estado');
        $this->atVista->assign('pagoChequeBD', $this->atChequesModelo->metConsultarPagoCheque($idPago));
        $this->atVista->assign('idPago', $idPago);

        if ($estado == 'VER') {
            $this->atVista->assign('estado', $estado);
            $this->atVista->assign('listadoCuentas', $this->atChequesModelo->atCuentasModelo->metCuentaListar());
            $this->atVista->assign('listadoTipoPago', $this->atChequesModelo->atMiscelaneoModelo->metMostrarSelect('TDPLG'));
            $this->atVista->metRenderizar('nuevo', 'modales');
        }
        if ($estado == 'PROCESAR') {

            $idPago = $this->metObtenerInt('idPago');
            $accion = $this->metObtenerAlphaNumerico('accion');
            $this->atVista->assign('estado', $estado);
            if ($accion == 'entregar') {
                $estadoEntrega = 'E';
                $procesar = $this->atChequesModelo->metProcesarEstadoEntrega($idPago, $estadoEntrega);
            } elseif ($accion == 'devolver') {
                $estadoEntrega = 'D';
                $procesar = $this->atChequesModelo->metProcesarEstadoEntrega($idPago, $estadoEntrega);
            } elseif ($accion == 'cobrar') {
                $fechaCobranza = $this->metObtenerTexto('fechaCobrado');
                if($fechaCobranza==null){
                    $registro['fec_cobrado'] = 'error';
                    $registro['status'] = 'errorFecha';
                    echo json_encode($registro);
                    exit;
                }

                $fecha = explode("-",$_POST['fechaCobrado']);
                $fechaCobrando = $fecha[0].'-'.$fecha[1].'-'.$fecha[2];

                $procesar = $this->atChequesModelo->metProcesarCobrarCheque($idPago, $fechaCobrando);
            }
            if (is_array($procesar)) {
                $registro['status'] = 'errorSQL';
                echo json_encode($registro);
                exit;
            }
            $registro['idPago'] = $procesar;
            $registro['status'] = 'ok';
            echo json_encode($registro);
            exit;
        }

    }

    public function metReportes($lista= false)
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        if ($lista == 'estadoEntrega'){
            $this->atVista->metRenderizar('estadoEntregaReportes');
        }else{
            $this->atVista->metRenderizar('libroChequesReportes');
        }
        $this->atVista->assign('lista', $lista);
    }

    public function metImprimirLibroCheque()
    {

        $this->atFPDF = new pdf('L', 'mm', 'Letter');

        $cheque = $this->atChequesModelo->metConsultarChequesBanco();

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->AddPage();
        $this->atFPDF->SetAutoPageBreak(true, 10);

        foreach ($cheque AS $ch) {
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetDrawColor(0, 0, 0);
            $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 10, 5, 10, 10);
            $this->atFPDF->SetFont('Arial', '', 8);
            $this->atFPDF->SetXY(20, 5);
            $this->atFPDF->Cell(100, 5, utf8_decode('CONTRALORIA DEL ESTADO MONAGAS'), 0, 1, 'L');
            $this->atFPDF->SetXY(20, 10);
            $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCION DE ADMINISTRACION Y PRESUPUESTO'), 0, 0, 'L');
            $this->atFPDF->SetFont('Arial', '', 8);
            $this->atFPDF->SetXY(225, 5);
            $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'L');
            $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
            $this->atFPDF->SetXY(225, 10);
            $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'L');
            $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo() . ' de {nb}', 0, 1, 'L');
            $this->atFPDF->SetFont('Arial', 'B', 10);
            $this->atFPDF->SetXY(10, 20);
            $this->atFPDF->Cell(260, 5, utf8_decode('LIBRO DE CHEQUES'), 0, 1, 'C', 0);

            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetDrawColor(0, 0, 0);
            $this->atFPDF->SetFillColor(255, 255, 255);

            $this->atFPDF->Ln(5);
            $this->atFPDF->SetFont('Arial', 'B', 8);
            $this->atFPDF->Cell(80, 5, utf8_decode('Cta. Bancaria. ' . $ch['ind_num_cuenta'] . ' ' . $ch['a006_miscelaneo_detalle_banco']), 0, 1, 'L', 0);
            $this->atFPDF->Cell(80, 5, utf8_decode('Cta. Contable. ' . $ch['cod_cuenta']), 0, 1, 'L', 0);
            $this->atFPDF->Ln(2);
            ##
            $this->atFPDF->SetFont('Arial', 'B', 6);
            $this->atFPDF->SetWidths(array(15, 15, 75, 20, 15, 15, 15, 15, 15, 15, 15, 15, 8, 8));
            $this->atFPDF->SetAligns(array('C', 'C', 'L', 'R', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C'));
            $this->atFPDF->Row(array('Nro. Cheque',
                'Fecha',
                'Pagar A',
                'Monto',
                'Voucher Pago',
                utf8_decode('Voucher Anulación'),
                'Estado Documento',
                'Estado Entrega',
                'Fecha Entrega',
                'Fecha Cobranza',
                'Fecha Dif. Original',
                'Pre-Pago',
                'Dif.',
                'Cob.'));
            $this->atFPDF->Ln(1);
//---------------------------------------------------
            $reporte = $this->atChequesModelo->metConsultarChequesReportes($ch['pk_num_cuenta'], $ch['pk_num_miscelaneo_detalle']);
            foreach ($reporte AS $i) {
                ##

                if ($i['num_flag_cobrado'] == 0) {
                    $flagCobrado = 'No';
                } else {
                    $flagCobrado = 'Si';
                }
                if ($i['ind_estado_entrega'] == 'C') {
                    $estadoEntrega = 'Custodia';
                } else {
                    $estadoEntrega = 'Entregado';
                }
                if ($i['ind_estado_pago'] == 'IM') {
                    $estadoDocumento = 'Pagado';
                }elseif($i['ind_estado_pago'] == 'AN') {
                    $estadoDocumento = 'Anulado';
                }else{
                    $estadoDocumento = 'GENERADO';
                }

                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', '', 6);
                $this->atFPDF->Row(array(
                    $i['ind_nro_pago'],
                    $i['fec_pago'],
                    utf8_decode($i['nombreAProveedor']),
                    $i['num_monto_pago'],
                    $i['fk_cbb001_num_voucher_pago'],
                    $i['fk_cbb001_num_voucher_anulacion'],
                    $estadoDocumento,
                    $estadoEntrega,
                    $i['fec_entregado'],
                    $i['fec_cobrado'],
                    $i['fec_diferido'],
                    $i['ind_nro_proceso'],
                    $flagCobrado,
                    $flagCobrado,
                ));
            }
            $this->atFPDF->AddPage();

        }

//---------------------------------------------------
////	Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }

    public function metImprimirChequeEntregados()
    {
        $this->atFPDF = new PDF('P', 'mm', 'Letter');

        $cheque = $this->atChequesModelo->metConsultarChequesBanco();

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->AddPage();
        $this->atFPDF->SetAutoPageBreak(true, 10);

        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 10, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(20, 5);
        $this->atFPDF->Cell(100, 5, utf8_decode('CONTRALORIA DEL ESTADO MONAGAS'), 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10);
        $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCION DE ADMINISTRACION Y PRESUPUESTO'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(165, 5);
        $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetXY(165, 10);
        $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo() . ' de {nb}', 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->SetXY(10, 20);
        $this->atFPDF->Cell(198, 5, utf8_decode('LIBRO DE CHEQUES'), 0, 1, 'C', 0);
        ##
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        ##
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetWidths(array(15, 15, 88, 20, 15, 15, 15, 15));
        $this->atFPDF->SetAligns(array('C', 'C', 'L', 'R', 'C', 'C', 'C', 'C'));
        $this->atFPDF->Row(array('Nro. Cheque',
            'Fecha',
            'Pagar A',
            'Monto',
            'Voucher Pago',
            'Estado Documento',
            'Estado Entrega',
            'Fecha Entrega'));
        $this->atFPDF->Ln(1);

        foreach ($cheque AS $ch) {
            $TotalCuenta=0;
            $TotalCheques= 0;

            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', 'B', 7);
            $this->atFPDF->Cell(50, 5, utf8_decode('Cta. Bancaria. '.$ch['ind_num_cuenta']), 0, 0, 'L', 0);
            $this->atFPDF->Cell(80, 5, utf8_decode($ch['a006_miscelaneo_detalle_banco']), 0, 1, 'L', 0);

            $reporte = $this->atChequesModelo->metConsultarChequesReportes($ch['pk_num_cuenta'], $ch['pk_num_miscelaneo_detalle']);
            foreach ($reporte AS $i) {
                $TotalCheques = $TotalCheques+1;
                $TotalCuenta = $TotalCuenta + $i['num_monto_pago'];

                if ($i['ind_estado_entrega'] == 'C') {
                    $estadoEntrega = 'Custodia';
                } else {
                    $estadoEntrega = 'Entregado';
                }
                if ($i['ind_estado_pago'] == 'IM') {
                    $estadoDocumento = 'Pagado';
                } elseif($i['ind_estado_pago'] == 'AN') {
                    $estadoDocumento = 'Anulado';
                } else{
                    $estadoDocumento = 'GENERADO';
                }
                ##
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', '', 6);
                $this->atFPDF->Row(array(
                    $i['ind_nro_pago'],
                    $i['fec_pago'],
                    utf8_decode($i['nombreAProveedor']),
                    $i['num_monto_pago'],
                    $i['fk_cbb001_num_voucher_pago'],
                    $estadoDocumento,
                    $estadoEntrega,
                    $i['fec_entregado']
                ));
            }
            $this->atFPDF->SetDrawColor(0, 0, 0);
            $this->atFPDF->SetFillColor(0, 0, 0);
            $y =$this->atFPDF->GetY();
            $this->atFPDF->Rect(128, $y, 20, 0.1, 'DF');
            $this->atFPDF->Rect(128, $y + 0.5, 20, 0.1, 'DF');
            $this->atFPDF->SetFont('Arial', 'B', 7);
            $this->atFPDF->Cell(30, 5, utf8_decode('Total Cheques.       ' . $TotalCheques), 0, 0, 'L', 0);
            $this->atFPDF->Cell(88, 5, 'Total Cta. Bancaria. ', 0, 0, 'R', 0);
            $this->atFPDF->Cell(20, 5, $TotalCuenta, 0, 0, 'R', 0);
            $this->atFPDF->Ln(8);
        }
            ##



//---------------------------------------------------
////	Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }

    public function metImprimirChequeCartera()
    {
        $this->atFPDF = new PDF('L', 'mm', 'Letter');

        $cheque = $this->atChequesModelo->metConsultarChequesBanco('cartera');

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->AddPage();
        $this->atFPDF->SetAutoPageBreak(true, 10);

        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 10, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5, utf8_decode('CONTRALORIA DEL ESTADO MONAGAS'), 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCION DE ADMINISTRACION Y PRESUPUESTO'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(225, 5); $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetXY(225, 10); $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->SetXY(10, 20); $this->atFPDF->Cell(260, 5, utf8_decode('CHEQUES EN CARTERA'), 0, 1, 'C', 0);
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(10, 25); $this->atFPDF->Cell(260, 5, utf8_decode('Fecha de Pago del fechaDesde al fechaHasta'), 0, 1, 'C', 0);
        ##
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        ##
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetWidths(array(15, 15, 8, 60, 20, 15, 15, 15, 100));
        $this->atFPDF->SetAligns(array('C', 'C', 'C', 'L', 'R', 'C', 'C', 'C', 'L'));
        $this->atFPDF->Row(array('Nro. Cheque',
            utf8_decode('Fecha Emisión'),
            'Dif. Dias',
            'Pagar A',
            'Monto',
            'Voucher Pago',
            utf8_decode('Voucher Anulación'),
            'Estado Documento',
            'Glosa'));
        $this->atFPDF->Ln(1);

        foreach ($cheque AS $ch) {
            $TotalCuenta=0;
            $TotalCheques= 0;

            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', 'B', 7);
            $this->atFPDF->Cell(50, 5, utf8_decode('Cta. Bancaria. '.$ch['ind_num_cuenta']), 0, 0, 'L', 0);
            $this->atFPDF->Cell(80, 5, utf8_decode($ch['a006_miscelaneo_detalle_banco']), 0, 1, 'L', 0);

            $reporte = $this->atChequesModelo->metConsultarChequesReportes($ch['pk_num_cuenta'], $ch['pk_num_miscelaneo_detalle'],'cartera');
            foreach ($reporte AS $i) {
                $TotalCheques = $TotalCheques+1;
                $TotalCuenta = $TotalCuenta + $i['num_monto_pago'];

                if ($i['ind_estado_pago'] == 'IM') {
                    $estadoDocumento = 'Pagado';
                } elseif($i['ind_estado_pago'] == 'AN') {
                    $estadoDocumento = 'Anulado';
                } else{
                    $estadoDocumento = 'GENERADO';
                }
                ##
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', '', 6);
                $this->atFPDF->Row(array(
                    $i['ind_nro_pago'],
                    $i['fec_pago'],
                    $i['DifDias'],
                    utf8_decode($i['nombreAProveedor']),
                    $i['num_monto_pago'],
                    $i['fk_cbb001_num_voucher_pago'],
                    $i['fk_cbb001_num_voucher_anulacion'],
                    $estadoDocumento,
                    substr(utf8_decode(strtoupper($i['glosa'])), 0, 70)
                ));
            }
            $this->atFPDF->SetDrawColor(0, 0, 0);
            $this->atFPDF->SetFillColor(0, 0, 0);
            $y = $this->atFPDF->GetY();
            $this->atFPDF->Rect(108, $y, 20, 0.1, 'DF');
            $this->atFPDF->Rect(108, $y+0.5, 20, 0.1, 'DF');
            $this->atFPDF->SetFont('Arial', 'B', 6);
            $this->atFPDF->Cell(30, 5, utf8_decode('Total Cheques.       '.$TotalCheques), 0, 0, 'L', 0);
            $this->atFPDF->Cell(68, 5, 'Total Cta. Bancaria. ', 0, 0, 'R', 0);
            $this->atFPDF->Cell(20, 5, number_format($TotalCuenta, 2, ',', '.'), 0, 0, 'R', 0);
        }
        ##



//---------------------------------------------------
////	Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }

    public function metImprimirChequeEstadoEntregados()
    {
        $this->atFPDF = new PDF('L', 'mm', 'Letter');

        $cheque = $this->atChequesModelo->metConsultarChequesBanco('entregados');

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->AddPage();
        $this->atFPDF->SetAutoPageBreak(true, 10);

        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 10, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5, utf8_decode('CONTRALORIA DEL ESTADO MONAGAS'), 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCION DE ADMINISTRACION Y PRESUPUESTO'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(225, 5); $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetXY(225, 10); $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->SetXY(10, 20); $this->atFPDF->Cell(260, 5, utf8_decode('CHEQUES ENTREGADOS'), 0, 1, 'C', 0);
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(10, 25); $this->atFPDF->Cell(260, 5, utf8_decode('Fecha de Pago del fechaDesde al fechaHasta'), 0, 1, 'C', 0);
        ##
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        ##
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetWidths(array(15, 15, 8, 60, 20, 15, 15, 15, 100));
        $this->atFPDF->SetAligns(array('C', 'C', 'C', 'L', 'R', 'C', 'C', 'C', 'L'));
        $this->atFPDF->Row(array('Nro. Cheque',
            utf8_decode('Fecha Emisión'),
            'Dif. Dias',
            'Pagar A',
            'Monto',
            'Voucher Pago',
            utf8_decode('Voucher Anulación'),
            'Estado Documento',
            'Glosa'));
        $this->atFPDF->Ln(1);

        foreach ($cheque AS $ch) {
            $TotalCuenta=0;
            $TotalCheques= 0;

            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', 'B', 7);
            $this->atFPDF->Cell(50, 5, utf8_decode('Cta. Bancaria. '.$ch['ind_num_cuenta']), 0, 0, 'L', 0);
            $this->atFPDF->Cell(80, 5, utf8_decode($ch['a006_miscelaneo_detalle_banco']), 0, 1, 'L', 0);

            $reporte = $this->atChequesModelo->metConsultarChequesReportes($ch['pk_num_cuenta'], $ch['pk_num_miscelaneo_detalle'],'entregados');
            foreach ($reporte AS $i) {
                $TotalCheques = $TotalCheques+1;
                $TotalCuenta = $TotalCuenta + $i['num_monto_pago'];

                if ($i['ind_estado_pago'] == 'IM') {
                    $estadoDocumento = 'Pagado';
                } elseif($i['ind_estado_pago'] == 'AN') {
                    $estadoDocumento = 'Anulado';
                } else{
                    $estadoDocumento = 'GENERADO';
                }
                ##
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', '', 6);
                $this->atFPDF->Row(array(
                    $i['ind_nro_pago'],
                    $i['fec_pago'],
                    $i['DifDias'],
                    utf8_decode($i['nombreAProveedor']),
                    $i['num_monto_pago'],
                    $i['fk_cbb001_num_voucher_pago'],
                    $i['fk_cbb001_num_voucher_anulacion'],
                    $estadoDocumento,
                    substr(utf8_decode(strtoupper($i['glosa'])), 0, 70)
                ));
            }
            $this->atFPDF->SetDrawColor(0, 0, 0);
            $this->atFPDF->SetFillColor(0, 0, 0);
            $y = $this->atFPDF->GetY();
            $this->atFPDF->Rect(108, $y, 20, 0.1, 'DF');
            $this->atFPDF->Rect(108, $y+0.5, 20, 0.1, 'DF');
            $this->atFPDF->SetFont('Arial', 'B', 6);
            $this->atFPDF->Cell(30, 5, utf8_decode('Total Cheques.       '.$TotalCheques), 0, 0, 'L', 0);
            $this->atFPDF->Cell(68, 5, 'Total Cta. Bancaria. ', 0, 0, 'R', 0);
            $this->atFPDF->Cell(20, 5, number_format($TotalCuenta, 2, ',', '.'), 0, 0, 'R', 0);
        }
        ##



//---------------------------------------------------
////	Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }

    public function metImprimirChequeEstadoCobrados()
    {
        $this->atFPDF = new PDF('L', 'mm', 'Letter');

        $cheque = $this->atChequesModelo->metConsultarChequesBanco('cobrados');

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->AddPage();
        $this->atFPDF->SetAutoPageBreak(true, 10);

        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 10, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5, utf8_decode('CONTRALORIA DEL ESTADO MONAGAS'), 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCION DE ADMINISTRACION Y PRESUPUESTO'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(225, 5); $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetXY(225, 10); $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->SetXY(10, 20); $this->atFPDF->Cell(260, 5, utf8_decode('CHEQUES COBRADOS'), 0, 1, 'C', 0);
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(10, 25); $this->atFPDF->Cell(260, 5, utf8_decode('Fecha de Pago del fechaDesde al fechaHasta'), 0, 1, 'C', 0);
        ##
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        ##
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetWidths(array(15, 15, 8, 60, 20, 15, 15, 15, 100));
        $this->atFPDF->SetAligns(array('C', 'C', 'C', 'L', 'R', 'C', 'C', 'C', 'L'));
        $this->atFPDF->Row(array('Nro. Cheque',
            utf8_decode('Fecha Emisión'),
            'Dif. Dias',
            'Pagar A',
            'Monto',
            'Voucher Pago',
            utf8_decode('Voucher Anulación'),
            'Estado Documento',
            'Glosa'));
        $this->atFPDF->Ln(1);

        foreach ($cheque AS $ch) {
            $TotalCuenta=0;
            $TotalCheques= 0;

            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', 'B', 7);
            $this->atFPDF->Cell(50, 5, utf8_decode('Cta. Bancaria. '.$ch['ind_num_cuenta']), 0, 0, 'L', 0);
            $this->atFPDF->Cell(80, 5, utf8_decode($ch['a006_miscelaneo_detalle_banco']), 0, 1, 'L', 0);

            $reporte = $this->atChequesModelo->metConsultarChequesReportes($ch['pk_num_cuenta'], $ch['pk_num_miscelaneo_detalle'],'cobrados');
            foreach ($reporte AS $i) {
                $TotalCheques = $TotalCheques+1;
                $TotalCuenta = $TotalCuenta + $i['num_monto_pago'];

                if ($i['ind_estado_pago'] == 'IM') {
                    $estadoDocumento = 'Pagado';
                } elseif($i['ind_estado_pago'] == 'AN') {
                    $estadoDocumento = 'Anulado';
                } else{
                    $estadoDocumento = 'GENERADO';
                }
                ##
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', '', 6);
                $this->atFPDF->Row(array(
                    $i['ind_nro_pago'],
                    $i['fec_pago'],
                    $i['DifDias'],
                    utf8_decode($i['nombreAProveedor']),
                    $i['num_monto_pago'],
                    $i['fk_cbb001_num_voucher_pago'],
                    $i['fk_cbb001_num_voucher_anulacion'],
                    $estadoDocumento,
                    substr(utf8_decode(strtoupper($i['glosa'])), 0, 70)
                ));
            }
            $this->atFPDF->SetDrawColor(0, 0, 0);
            $this->atFPDF->SetFillColor(0, 0, 0);
            $y = $this->atFPDF->GetY();
            $this->atFPDF->Rect(108, $y, 20, 0.1, 'DF');
            $this->atFPDF->Rect(108, $y+0.5, 20, 0.1, 'DF');
            $this->atFPDF->SetFont('Arial', 'B', 6);
            $this->atFPDF->Cell(30, 5, utf8_decode('Total Cheques.       '.$TotalCheques), 0, 0, 'L', 0);
            $this->atFPDF->Cell(68, 5, 'Total Cta. Bancaria. ', 0, 0, 'R', 0);
            $this->atFPDF->Cell(20, 5, number_format($TotalCuenta, 2, ',', '.'), 0, 0, 'R', 0);
        }
        ##



//---------------------------------------------------
////	Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }

    public function metImprimirChequeEstadoDevueltos()
    {
        $this->atFPDF = new PDF('L', 'mm', 'Letter');

        $cheque = $this->atChequesModelo->metConsultarChequesBanco('devueltos');

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->AddPage();
        $this->atFPDF->SetAutoPageBreak(true, 10);

        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 10, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5, utf8_decode('CONTRALORIA DEL ESTADO MONAGAS'), 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCION DE ADMINISTRACION Y PRESUPUESTO'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(225, 5); $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetXY(225, 10); $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->SetXY(10, 20); $this->atFPDF->Cell(260, 5, utf8_decode('CHEQUES DEVUELTOS'), 0, 1, 'C', 0);
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(10, 25); $this->atFPDF->Cell(260, 5, utf8_decode('Fecha de Pago del fechaDesde al fechaHasta'), 0, 1, 'C', 0);
        ##
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        ##
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetWidths(array(15, 15, 8, 60, 20, 15, 15, 15, 100));
        $this->atFPDF->SetAligns(array('C', 'C', 'C', 'L', 'R', 'C', 'C', 'C', 'L'));
        $this->atFPDF->Row(array('Nro. Cheque',
            utf8_decode('Fecha Emisión'),
            'Dif. Dias',
            'Pagar A',
            'Monto',
            'Voucher Pago',
            utf8_decode('Voucher Anulación'),
            'Estado Documento',
            'Glosa'));
        $this->atFPDF->Ln(1);

        foreach ($cheque AS $ch) {
            $TotalCuenta=0;
            $TotalCheques= 0;

            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', 'B', 7);
            $this->atFPDF->Cell(50, 5, utf8_decode('Cta. Bancaria. '.$ch['ind_num_cuenta']), 0, 0, 'L', 0);
            $this->atFPDF->Cell(80, 5, utf8_decode($ch['a006_miscelaneo_detalle_banco']), 0, 1, 'L', 0);

            $reporte = $this->atChequesModelo->metConsultarChequesReportes($ch['pk_num_cuenta'], $ch['pk_num_miscelaneo_detalle'],'devueltos');
            foreach ($reporte AS $i) {
                $TotalCheques = $TotalCheques+1;
                $TotalCuenta = $TotalCuenta + $i['num_monto_pago'];

                if ($i['ind_estado_pago'] == 'IM') {
                    $estadoDocumento = 'Pagado';
                } elseif($i['ind_estado_pago'] == 'AN') {
                    $estadoDocumento = 'Anulado';
                } else{
                    $estadoDocumento = 'GENERADO';
                }
                ##
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', '', 6);
                $this->atFPDF->Row(array(
                    $i['ind_nro_pago'],
                    $i['fec_pago'],
                    $i['DifDias'],
                    utf8_decode($i['nombreAProveedor']),
                    $i['num_monto_pago'],
                    $i['fk_cbb001_num_voucher_pago'],
                    $i['fk_cbb001_num_voucher_anulacion'],
                    $estadoDocumento,
                    substr(utf8_decode(strtoupper($i['glosa'])), 0, 70)
                ));
            }
            $this->atFPDF->SetDrawColor(0, 0, 0);
            $this->atFPDF->SetFillColor(0, 0, 0);
            $y = $this->atFPDF->GetY();
            $this->atFPDF->Rect(108, $y, 20, 0.1, 'DF');
            $this->atFPDF->Rect(108, $y+0.5, 20, 0.1, 'DF');
            $this->atFPDF->SetFont('Arial', 'B', 6);
            $this->atFPDF->Cell(30, 5, utf8_decode('Total Cheques.       '.$TotalCheques), 0, 0, 'L', 0);
            $this->atFPDF->Cell(68, 5, 'Total Cta. Bancaria. ', 0, 0, 'R', 0);
            $this->atFPDF->Cell(20, 5, number_format($TotalCuenta, 2, ',', '.'), 0, 0, 'R', 0);
        }
        ##



//---------------------------------------------------
////	Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }

    public function metImprimirChequeEstadoProveedor()
    {
        $this->atFPDF = new PDF('L', 'mm', 'Letter');

        $cheque = $this->atChequesModelo->metConsultarChequesBanco('proveedor');

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->AddPage();
        $this->atFPDF->SetAutoPageBreak(true, 10);

        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 10, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5, utf8_decode('CONTRALORIA DEL ESTADO MONAGAS'), 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCION DE ADMINISTRACION Y PRESUPUESTO'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(225, 5); $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetXY(225, 10); $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->SetXY(10, 20); $this->atFPDF->Cell(260, 5, utf8_decode('CHEQUES EN CARTERA POR PROVEEDOR'), 0, 1, 'C', 0);
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(10, 25); $this->atFPDF->Cell(260, 5, utf8_decode('Fecha de Pago del fechaDesde al fechaHasta'), 0, 1, 'C', 0);
        ##
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        ##
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetWidths(array(15, 15, 8, 40, 20, 20, 15, 15, 15, 100));
        $this->atFPDF->SetAligns(array('C', 'C', 'C', 'L', 'C', 'R', 'C', 'C', 'C', 'L'));
        $this->atFPDF->Row(array('Nro. Cheque',
            utf8_decode('Fecha de Pago'),
            'Dif. Dias',
            'Banco',
            'Cta. Bancaria',
            'Monto',
            'Voucher Pago',
            utf8_decode('Voucher Anulación'),
            'Estado Documento',
            'Glosa'));
        $this->atFPDF->Ln(1);

        foreach ($cheque AS $ch) {
            $TotalCuenta=0;
            $TotalCheques= 0;

            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', 'B', 6);
            $this->atFPDF->Cell(25, 5, 'Proveedor.', 0, 0, 'L', 0);
            $this->atFPDF->Cell(80, 5, utf8_decode($ch['nombreAProveedor']), 0, 1, 'L', 0);

            $reporte = $this->atChequesModelo->metConsultarChequesReportes('','','proveedor',$ch['pk_num_persona']);
            foreach ($reporte AS $i) {
                $TotalCheques = $TotalCheques+1;
                $TotalCuenta = $TotalCuenta + $i['num_monto_pago'];

                if ($i['ind_estado_pago'] == 'IM') {
                    $estadoDocumento = 'Pagado';
                } elseif($i['ind_estado_pago'] == 'AN') {
                    $estadoDocumento = 'Anulado';
                } else{
                    $estadoDocumento = 'GENERADO';
                }
                ##
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', '', 6);
                $this->atFPDF->Row(array(
                    $i['ind_nro_pago'],
                    $i['fec_pago'],
                    $i['DifDias'],
                    utf8_decode($i['a006_miscelaneo_detalle_banco']),
                    $i['ind_num_cuenta'],
                    $i['num_monto_pago'],
                    $i['fk_cbb001_num_voucher_pago'],
                    $i['fk_cbb001_num_voucher_anulacion'],
                    $estadoDocumento,
                    substr(utf8_decode(strtoupper($i['glosa'])), 0, 70)
                ));

            }
            $this->atFPDF->SetDrawColor(0, 0, 0);
            $this->atFPDF->SetFillColor(0, 0, 0);
            $y = $this->atFPDF->GetY();
            $this->atFPDF->Rect(108, $y, 20, 0.1, 'DF');
            $this->atFPDF->Rect(108, $y+0.5, 20, 0.1, 'DF');
            $this->atFPDF->SetFont('Arial', 'B', 6);
            $this->atFPDF->Cell(30, 5, utf8_decode('Total Cheques.       '.$TotalCheques), 0, 0, 'L', 0);
            $this->atFPDF->Cell(68, 5, 'Total Cta. Bancaria. ', 0, 0, 'R', 0);
            $this->atFPDF->Cell(20, 5, number_format($TotalCuenta, 2, ',', '.'), 0, 0, 'R', 0);
        }
        ##

//---------------------------------------------------
////	Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }


}