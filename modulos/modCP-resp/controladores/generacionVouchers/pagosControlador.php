<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_Modulo.'modCP'.DS.'controladores'.DS.'trait'.DS.'funcionesTrait.php';
class pagosControlador extends Controlador
{
    use funcionesTrait;
    private $atPagosModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atPagosModelo = $this->metCargarModelo('generacionVouchers');

    }

    public function metIndex()
    {
        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementosJs = array(
            'materialSiace/core/demo/DemoTableDynamic',
        );
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJs($complementosJs);

        $this->atVista->assign('dataBD', $this->atPagosModelo->metListarPagos());
        $this->atVista->metRenderizar('listado');

    }

    public function metGenerarVouchers()
    {
        $idPago = $this->metObtenerInt('idPago');
        $pkObligacion = $this->atPagosModelo->atObligacionModelo->metObligacion($idPago);
        $idObligacion = $pkObligacion['pk_num_obligacion'];

        //Enviar a la vista el Centro de Costo por defecto para los vouchers
        $this->atVista->assign('centroCosto', $this->atPagosModelo->atObligacionModelo->metBuscar('a023_centro_costo','ind_abreviatura',Session::metObtener('CCOSTOVOUCHER')));
        //Enviar a la vista el organismo
        $this->atVista->assign('organismo', $this->atPagosModelo->atObligacionModelo->metOrganismo());
        $this->atVista->assign('libroContable', $this->atPagosModelo->atObligacionModelo->atLibroContableModelo->metListar());
        $this->atVista->assign('voucher', $this->atPagosModelo->atObligacionModelo->atVoucherModelo->metListar());


        $datosOblig = $this->atPagosModelo->atObligacionModelo->metConsultaObligacion($idObligacion);
        $montoImpuesto = $this->atPagosModelo->atObligacionModelo->metBuscarMontoImpuesto($idObligacion);
        $datos = $this->atPagosModelo->metArmarPagoContabilidad($idObligacion,$datosOblig['num_flag_provision']);
        $codVoucher = $datosOblig['fk_cbc003_num_tipo_voucher'];
        if(Session::metObtener('CONTABILIDAD') == 'ONCOP') {
            $contab = 'T';
        }else{
            $contab = 'F';
        }
        $contabilidades = $this->atPagosModelo->atObligacionModelo->metBuscar('cb_b005_contabilidades', 'ind_tipo_contabilidad', $contab);

        $debitos=0; $creditos=0;
        foreach ($datos as $i) {
            if($i['columna']=='Debe'){
                $debitos = $debitos+$i['MontoVoucher'];

                $datoArreglo['Debe'][]=array(
                    'pk_num_cuenta' => $i['pk_num_cuenta'],
                    'cod_cuenta' => $i['cod_cuenta'],
                    'ind_descripcion' => $i['ind_descripcion'],
                    'MontoVoucher' => $i['MontoVoucher']
                );
            }else{
                $creditos = $creditos+$i['MontoVoucher'];
                $datoArreglo['Haber'][]=array(
                    'pk_num_cuenta' => $i['pk_num_cuenta'],
                    'cod_cuenta' => $i['cod_cuenta'],
                    'ind_descripcion' => $i['ind_descripcion'],
                    'MontoVoucher' => $i['MontoVoucher']
                );
            }
        }
        if(!isset($datoArreglo)){
            $datoArreglo=false;
        }
        $this->atVista->assign('datoArreglo',$datoArreglo);
        $this->atVista->assign('datosOblig', $datosOblig);
        $this->atVista->assign('creditos', $creditos);
        $this->atVista->assign('debitos', $debitos);
        $this->atVista->assign('datos', $datos);
        $this->atVista->assign('codVoucher', $codVoucher);
        $this->atVista->assign('idObligacion', $idObligacion);
        $this->atVista->assign('contabilidad', $contabilidades['pk_num_contabilidades']);
        $this->atVista->assign('idPago', $idPago);

        $this->atVista->metRenderizar('vouchersPago', 'modales');
    }

}