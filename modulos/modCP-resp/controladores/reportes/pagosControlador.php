<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_Modulo.'modCP'.DS.'controladores'.DS.'trait'.DS.'funcionesTrait.php';
class pagosControlador extends Controlador
{
    use funcionesTrait;
    private $atPagosModelo;
    private $atFPDF;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atPagosModelo = $this->metCargarModelo('reportes');
        $this->metObtenerLibreria('cabeceraCP', 'modCP');

    }

    public function metIndex($lista=false)
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('listadoCuentas', $this->atPagosModelo->atCuentasModelo->metCuentaListar());
        $this->atVista->assign('selectBANCOS', $this->atPagosModelo->atMiscelaneoModelo->metMostrarSelect('BANCOS'));
        $this->atVista->assign('listadoDocumento', $this->atPagosModelo->atDocumentoModelo->metDocumentoListar());

        if($lista == 'ordenesPago'){
            $this->atVista->metRenderizar('ordenesPagoReportes');
        }else{
            $this->atVista->metRenderizar('pagoProveedoresReportes');
        }
    }
    
    public function metImprimirDetallePresupuestarioContable()
    {
        $proveedor = $_GET['proveedor'];
        $documento = $_GET['documento'];
        $cuenta = $_GET['cuenta'];
        $fdesde = $_GET['fdesde'];
        $fhasta = $_GET['fhasta'];

        $this->atFPDF = new PDF('P', 'mm', 'Letter');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 10);
        $this->atFPDF->SetAutoPageBreak(5, 5);
        $this->atFPDF->AddPage();

        ##
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 10, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', '', 10);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCION DE ADMINISTRACION Y PRESUPUESTO'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 10);
        $this->atFPDF->SetXY(165, 5); $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetXY(165, 10); $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 12);
        $this->atFPDF->SetXY(10, 20); $this->atFPDF->Cell(195, 5, utf8_decode('ORDENES DE PAGO POR PERIODO'), 0, 1, 'C', 0);
        ##
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetWidths(array(20, 20, 20, 110, 30));
        $this->atFPDF->SetAligns(array('C', 'C', 'C', 'L', 'R'));
        $this->atFPDF->Row(array('Nro. Orden',
            'Fecha',
            'Nro. Cheque',
            'Beneficiario',
            'Monto Bs.'));
        $this->atFPDF->Ln(1);
        //if ($_sub == 2) {
            //$this->atFPDF->SetWidths(array(5, 20, 145, 30));
            //$this->atFPDF->SetAligns(array('C', 'C', 'L', 'R'));
        //---------------------------------------------------
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        //---------------------------------------------------
        $datos = $this->atPagosModelo->metConsultaOrdenPago($proveedor,$documento,$cuenta,$fdesde,$fhasta);
        $_sub = 1;  $verDistribucion = ""; $MontoTotal=0;
        foreach ($datos as $i) {
            ##	imprimo orden

            $this->atFPDF->SetFont('Arial', 'B', 8);
            $this->atFPDF->SetWidths(array(20, 20, 20, 110, 30));
            $this->atFPDF->SetAligns(array('C', 'C', 'C', 'L', 'R'));
            $this->atFPDF->Row(array(
                $i['ind_num_orden'],
                $i['fec_orden_pago'],
                $i['ind_cheque_usuario'],
                utf8_decode($i['proveedor']),
                number_format($i['num_monto'],2, ',', '.')
            ));

            $this->atFPDF->SetFont('Arial', '', 8);
            $this->atFPDF->SetWidths(array(5, 20, 145, 30));
            $this->atFPDF->SetAligns(array('C', 'C', 'L', 'R'));
            $this->atFPDF->Row(array('',
                $i['cod_partida'],
                utf8_decode($i['ind_denominacion']),
                number_format($i['num_monto'],2, ',', '.')
            ));

            $MontoTotal += $i['num_monto'];

            $this->atFPDF->Ln(2);
        }

    
        $this->atFPDF->Ln(1);
        //	imprimo total
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(0, 0, 0);
        $y = $this->atFPDF->GetY();
        $this->atFPDF->Rect(10, $y, 200, 0.1, "FD");
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(170, 5, 'Total: ', 0, 0, 'R');
        $this->atFPDF->Cell(30, 5, number_format($MontoTotal, 2, ',', '.'), 0, 0, 'R');
        
        
        ////	Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }

    ##PAGO A PROVEEDORES POR PROCESO
    public function metImprimirPagoProveedoresProceso()
    {
        $proceso = $_GET['proceso'];

        $this->atFPDF = new PDF('P', 'mm', 'Letter');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 10);
        $this->atFPDF->SetAutoPageBreak(5, 5);
        $this->atFPDF->AddPage();

        ##
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 10, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCION DE ADMINISTRACION Y PRESUPUESTO'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(165, 5); $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetXY(165, 10); $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->SetXY(10, 20); $this->atFPDF->Cell(195, 5, utf8_decode('PAGOS A PROVEEDORES X PROCESO'), 0, 1, 'C', 0);
        ##
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetWidths(array(5, 30, 30, 20, 20, 95));
        $this->atFPDF->SetAligns(array('C', 'L', 'L', 'R', 'C', 'L'));
        $this->atFPDF->Row(array('#',
            'Proveedor',
            'Nro. Documento',
            'Monto',
            'Fecha Doc.',
            utf8_decode('Descricpión')
        ));
        $this->atFPDF->Ln(1);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        //---------------------------------------------------
        //	consulto
        $grupo=0; $total= 0;
        $datos = $this->atPagosModelo->metConsultaPago();
        foreach($datos as $dato){
            $this->atFPDF->SetFont('Arial', 'B', 8);
            $this->atFPDF->Cell(50, 5, 'Nro. Proceso NroProceso', 1, 1, 'L');
            $this->atFPDF->Cell(50, 5, 'Cta. Bancaria NroCuenta', 1, 1, 'L');
            ##	imprimo proveedor
            if($grupo != $dato['fk_a003_num_persona_proveedor']){
                $grupo = $dato['fk_a003_num_persona_proveedor'];
                $_sub = 1;
                $i = 0;
                $this->atFPDF->SetFont('Arial', 'B', 6);
                $this->atFPDF->SetWidths(array(65, 20, 20, 65));
                $this->atFPDF->SetAligns(array('L', 'R', 'L', 'L'));
                $this->atFPDF->Row(array(
                    $dato['proveedor'],
                    number_format($dato['num_monto_pago'], 2, ',', '.'),
                    $dato['tipoPago'],
                    'Documento. '.$dato['ind_documento_fiscal']));
            }
            ##	imprimo documento
            $_sub = 2;
            $this->atFPDF->SetFont('Arial', '', 6);
            $this->atFPDF->SetWidths(array(5, 30, 30, 20, 20, 90));
            $this->atFPDF->SetAligns(array('C', 'L', 'L', 'R', 'C', 'L'));

            $this->atFPDF->Row(array(
                ++$i,
                $dato['ind_documento_fiscal'],
                $dato['CodTipoDocumento'].'-NroDocumento',
                number_format($dato['num_monto_pago'], 2, ',', '.'),
                $dato['fec_documento'],
                $dato['ind_comentarios']
            ));

            $this->atFPDF->Ln(2);
            ##
            $total += $dato['num_monto_pago'];

        }



        ////	Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }

    ##PAGO A PROVEEDORES PENDIENTES
    public function metImprimirPagoPendiente()
    {
        $this->atFPDF = new PDF('P', 'mm', 'Letter');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 10);
        $this->atFPDF->SetAutoPageBreak(5, 5);
        $this->atFPDF->AddPage();

        ##
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 10, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCION DE ADMINISTRACION Y PRESUPUESTO'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(165, 5); $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetXY(165, 10); $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->SetXY(10, 20); $this->atFPDF->Cell(195, 5, utf8_decode('ORDENES DE PAGO PENDIENTES'), 0, 1, 'C', 0);
        ##
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetWidths(array(5, 30, 15, 20, 20, 25, 85));
        $this->atFPDF->SetAligns(array('C', 'L', 'C', 'C', 'R', 'L', 'L'));
        $this->atFPDF->Row(array(
            '#',
            'Nro. Documento',
            'Fecha Venc.',
            'Nro. Cuenta',
            'Monto',
            'Tipo de Pago',
            'Comentarios'
        ));
        //---------------------------------------------------
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        //---------------------------------------------------
        //	consulto
        $grupo =0;
        $datos = $this->atPagosModelo->metConsultaPagoPendiente();
        foreach($datos as $dato){
            ##	imprimo proveedor
            if ($grupo != $dato['fk_a003_num_persona_proveedor']) {
                $grupo = $dato['fk_a003_num_persona_proveedor'];
                $_sub = 1;
                $i = 0;
                $this->atFPDF->Ln(3);
                $this->atFPDF->SetFont('Arial', 'B', 6);
                $this->atFPDF->SetWidths(array(70, 20, 25, 65));
                $this->atFPDF->SetAligns(array('L', 'R', 'L', 'L'));
                $this->atFPDF->Row(array(
                    $dato['proveedor'],
                    number_format($dato['montoProveedor'], 2, ',', '.'),
                    '',
                    'Documento. '.$dato['docFiscal']));
            }
        }




        ////	Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }

}