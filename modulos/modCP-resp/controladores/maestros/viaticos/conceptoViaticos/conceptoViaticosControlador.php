<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class conceptoViaticosControlador extends Controlador
{
    private $atConceptoViaticosModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atConceptoViaticosModelo = $this->metCargarModelo('conceptoViaticos');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('dataBD', $this->atConceptoViaticosModelo->metListarConceptoViatico());
        $this->atVista->metRenderizar('listado');
    }
    
    public function metNuevoConceptoViatico()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $ver = $this->metObtenerInt('ver');
        $idConceptoViatico = $this->metObtenerInt('idConceptoViatico');

        if ($valido == 1) {
            $Excepcion = array('num_flag_monto','num_flag_cantidad','num_estatus');
            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');
            $ind = $this->metValidarFormArrayDatos('form', 'int', $Excepcion,true);

            if ($alphaNum != null && $ind == null) {
                $registro = $alphaNum;
            } elseif ($alphaNum == null && $ind != null) {
                $registro = $ind;
            } else {
                $registro = array_merge($ind, $alphaNum);
            }
            if(in_array('datoInvalido', $registro)){
                $registro['num_valor_ut'] = 'error';
                $registro['status'] = 'errorNumerico';
                echo json_encode($registro);
                exit;
            }
            if($registro['num_valor_ut'] == 'error' ){
                $registro['num_valor_ut'] = '0';
            }
            if (in_array('error', $registro)) {
                $registro['status'] = 'error';
                echo json_encode($registro);
                exit;
            }
            if(!isset ($registro['num_flag_monto']) ){
                $registro['num_flag_monto'] = 0;
            }
            if(!isset ($registro['num_flag_cantidad']) ){
                $registro['num_flag_cantidad'] = 0;
            }
            if(!isset ($registro['num_estatus']) ){
                $registro['num_estatus'] = 0;
            }

            if ($idConceptoViatico == 0) {
                $id = $this->atConceptoViaticosModelo->metNuevoConceptoViatico($registro);
                $registro['status'] = 'nuevo';
                $registro['idConceptoViatico'] = $id;
                $idNuevo = $this->atConceptoViaticosModelo->metListarConceptoViatico($registro['idConceptoViatico']);
            } else {
                $id = $this->atConceptoViaticosModelo->metModificarConceptoViatico($idConceptoViatico,$registro);
                    $registro['status'] = 'modificar';
                $registro['idConceptoViatico'] = $id;
                $idNuevo = $this->atConceptoViaticosModelo->metListarConceptoViatico($idConceptoViatico);
            }
            if (is_array($id)) {
                foreach ($registro as $titulo => $valor) {
                    if(!is_array($registro[$titulo])){
                        if (strpos($id[2], $registro[$titulo])) {
                            $registro[$titulo] = 'error';
                        }
                    }
                }
                $registro['status'] = 'errorSQL';
                echo json_encode($id);
                exit;
            }
            $idNuevo['status'] = $registro['status'];
            echo json_encode($idNuevo);
            exit;
        }

        if ($idConceptoViatico != 0) {
            $this->atVista->assign('dataBD', $this->atConceptoViaticosModelo->metConsultarConceptoViatico($idConceptoViatico));
        }
        $this->atVista->assign('ver', $ver);
        $this->atVista->assign('idConceptoViatico', $idConceptoViatico);

        $this->atVista->assign('listadoCategoria',$this->atConceptoViaticosModelo->atMiscelaneoModelo->metMostrarSelect('CATVIAT'));
        $this->atVista->assign('listadoArticulo',$this->atConceptoViaticosModelo->atMiscelaneoModelo->metMostrarSelect('ARTVIAT'));

        $this->atVista->metRenderizar('nuevo', 'modales');
    }

    public function metEliminar()
    {
        $idConceptoViatico = $this->metObtenerInt('idConceptoViatico');
        if($idConceptoViatico!=0){
            $id=$this->atConceptoViaticosModelo->metEliminarConceptoViatico($idConceptoViatico);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Concepto de Viatico se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idConceptoViatico'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }


}