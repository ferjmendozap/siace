<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Deivis Millan        | d.millan@contraloriamonagas.gob.ve   | 0426-9372700
 * | 2 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/


class conceptoGastoControlador extends Controlador
{
    private $atConceptoGastoModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atConceptoGastoModelo = $this->metCargarModelo('conceptoGasto');
    }
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('conceptoGastoBD',$this->atConceptoGastoModelo->metConceptoGastoListar());
        $this->atVista->metRenderizar('listado');
    }
    public function metPartida($partida)
    {
        $this->atVista->assign('lista',$this->atConceptoGastoModelo->atPartidaModelo->metListarPartida());
        $this->atVista->assign('listaPartida', $partida);
        $this->atVista->metRenderizar('listadoPartidas', 'modales');
    }

    public function metNuevoConceptoGasto()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $ver = $this->metObtenerInt('ver');
        $idConceptoGasto = $this->metObtenerInt('idConceptoGasto');

        if ($valido == 1) {
            $Excepcion = array('num_estatus','fk_cbb004_num_plan_cuenta_pub20');
            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');
            $ind = $this->metValidarFormArrayDatos('form', 'int', $Excepcion);

            if ($alphaNum != null && $ind == null) {
                $registro = $alphaNum;
            } elseif ($alphaNum == null && $ind != null) {
                $registro = $ind;
            } else {
                $registro = array_merge($ind, $alphaNum);
            }
            if (in_array('error', $registro)) {
                $registro['status'] = 'error';
                echo json_encode($registro);
                exit;
            }
            if(!isset($registro['num_estatus'])){
                $registro['num_estatus']=0;
            }
            if($registro['fk_cbb004_num_plan_cuenta_pub20']=='0'){
                $registro['fk_cbb004_num_plan_cuenta_pub20']=null;
            }

            if ($idConceptoGasto == 0) {

                $registro['status'] = 'nuevo';
                $id = $this->atConceptoGastoModelo->metNuevoConceptoGasto($registro);
                $registro['idConceptoGasto'] = $id;
                $idNuevo = $this->atConceptoGastoModelo->metConceptoGastoListar($registro['idConceptoGasto']);

            } else {
                $registro['status'] = 'modificar';
                $id = $this->atConceptoGastoModelo->metModificarConceptoGasto($idConceptoGasto, $registro);
                $registro['idConceptoGasto'] = $id;
                $idNuevo = $this->atConceptoGastoModelo->metConceptoGastoListar($idConceptoGasto);
            }

            if (is_array($id)) {
                foreach ($registro as $titulo => $valor) {
                    if(!is_array($registro[$titulo])){
                        if (strpos($id[2], $registro[$titulo])) {
                            $registro[$titulo] = 'error';
                        }
                    }
                }
                $registro['status'] = 'errorSQL';
                echo json_encode($registro);
                exit;
            }
            $idNuevo['status'] = $registro['status'];
            echo json_encode($idNuevo);
            exit;
        }
        if ($idConceptoGasto != 0) {

            $this->atVista->assign('conceptoGastoDetalle', $this->atConceptoGastoModelo->metConceptoGastoConsultaDetalle($idConceptoGasto));
            $this->atVista->assign('conceptoGastoBD', $this->atConceptoGastoModelo->metConceptoGastoConsulta($idConceptoGasto));
            $this->atVista->assign('clasificacion', $this->atConceptoGastoModelo->metMostrarClasif($idConceptoGasto));

        }
        $this->atVista->assign('selectGrupo',$this->atConceptoGastoModelo->atMiscelaneoModelo->metMostrarSelect('GRUPGAST'));
        $this->atVista->assign('selectClasifGasto',$this->atConceptoGastoModelo->atClasificacionGastoModelo->metClasificacionGastoListar());

        $this->atVista->assign('ver', $ver);
        $this->atVista->assign('idConceptoGasto', $idConceptoGasto);
        $this->atVista->metRenderizar('nuevo', 'modales');

    }

    public function metEliminar()
    {
        $idConceptoGasto = $this->metObtenerInt('idConceptoGasto');
        if($idConceptoGasto!=0){
            $id=$this->atConceptoGastoModelo->metEliminarConceptoGasto($idConceptoGasto);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Concepto de Gastos se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idConceptoGasto'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
    public function metEliminarClasif()
    {
        $idConceptoClasifGasto = $this->metObtenerInt('idConceptoClasifGasto');
        if($idConceptoClasifGasto!=0){
            $id=$this->atConceptoGastoModelo->atClasificacionGastoModelo->metEliminarClasif($idConceptoClasifGasto);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Tipo de Docuemento se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idClasifGasto'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }


}