<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/


class documentosTransaccionControlador extends Controlador
{
    private $atDocumentosTransaccionModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atDocumentosTransaccionModelo = $this->metCargarModelo('documentoTransaccion');
    }
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('Documentos',$this->atDocumentosTransaccionModelo->metDocumentoListar());
        $this->atVista->metRenderizar('listado');
    }

    public function metNuevoDocumento()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $ver = $this->metObtenerInt('ver');
        $valido = $this->metObtenerInt('valido');
        $idDocumento = $this->metObtenerInt('idDocumento');

        if ($valido == 1){
            $Excepcion=array('num_estatus');
            $ind=$this->metValidarFormArrayDatos('form','int',$Excepcion);
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');

            if($alphaNum!=null && $ind==null){
                $registro=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $registro=$ind;
            }else{
                $registro=array_merge($alphaNum,$ind);
            }

            if(in_array('error',$registro)){
                $registro['status']='error';
                echo json_encode($registro);
                exit;
            }
            if ($idDocumento === 0) {
                $registro['status'] = 'nuevo';
                $id = $this->atDocumentosTransaccionModelo->metNuevoDocumento($registro);
                $registro['idDocumento'] = $id;
                $idNuevo = $this->atDocumentosTransaccionModelo->metDocumentoListar($registro['idDocumento']);

            } else {
                $registro['status'] = 'modificar';
                 $id = $this->atDocumentosTransaccionModelo->metModificarDocumento($idDocumento,$registro);
                $registro['idDocumento'] = $id;
                $idNuevo = $this->atDocumentosTransaccionModelo->metDocumentoListar($idDocumento);
           }
            if (is_array($id)) {
                foreach ($registro as $titulo => $valor) {
                    if(!is_array($registro[$titulo])){
                        if (strpos($id[2], $registro[$titulo])) {
                            $registro[$titulo] = 'error';
                        }
                    }
                }
                $registro['status'] = 'errorSQL';
                echo json_encode($registro);
                exit;
            }
            $idNuevo['status']=$registro['status'];
            echo json_encode($idNuevo);
            exit;
        }
        if ($idDocumento != 0) {
            $db = $this->atDocumentosTransaccionModelo->metDocumentoConsultaDetalle($idDocumento);
            $this->atVista->assign('dataBD', $db);
        }

        $this->atVista->assign('listadoTransaccion',$this->atDocumentosTransaccionModelo->atMiscelaneoModelo->metMostrarSelect('TIPOTRANS'));

        $this->atVista->assign('ver', $ver);
        $this->atVista->assign('idDocumento',$idDocumento);
        $this->atVista->metRenderizar('nuevo', 'modales');
    }

    public function metEliminar()
    {
        $idDocumento = $this->metObtenerInt('idDocumento');
        if($idDocumento!=0){
            $id=$this->atDocumentosTransaccionModelo->metEliminarDocumento($idDocumento);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Tipo de Docuemento se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idDocumento'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }


}