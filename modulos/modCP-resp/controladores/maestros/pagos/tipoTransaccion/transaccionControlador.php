<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/


class transaccionControlador extends Controlador
{
    private $atTransaccionModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atTransaccionModelo = $this->metCargarModelo('transaccion');
    }
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('transaccionBD',$this->atTransaccionModelo->metTransaccionListar());
        $this->atVista->metRenderizar('listado');
    }
    public function metNuevaTransaccion()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $ver = $this->metObtenerInt('ver');
        $idTransaccion = $this->metObtenerInt('idTransaccion');

        if ($valido == 1) {
            $ExcepcionInt = array('num_flag_voucher', 'num_flag_transaccion','num_estatus','fk_cbb004_num_plan_cuenta','fk_cbb004_num_plan_cuenta_pub20');
            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');
            $ind = $this->metValidarFormArrayDatos('form', 'int', $ExcepcionInt);

            if ($alphaNum != null && $ind == null) {
                $registro = $alphaNum;
            } elseif ($alphaNum == null && $ind != null) {
                $registro = $ind;
            } else {
                $registro = array_merge($ind, $alphaNum);
            }
            if (in_array('error', $registro)) {
                $registro['status'] = 'error';
                echo json_encode($registro);
                exit;
            }
            if(!isset($registro['num_flag_transaccion'])){
                $registro['num_flag_transaccion']=0;
            }
            if(!isset($registro['num_flag_voucher'])){
                $registro['num_flag_voucher']=0;
            }
            if(!isset($registro['num_estatus'])){
                $registro['num_estatus']=0;
            }
            if($registro['fk_cbb004_num_plan_cuenta']=='0'){
                $registro['fk_cbb004_num_plan_cuenta']=null;
            }
            if($registro['fk_cbb004_num_plan_cuenta_pub20']=='0'){
                $registro['fk_cbb004_num_plan_cuenta_pub20']=null;
            }
            if ($idTransaccion == 0) {
                $registro['status'] = 'nuevo';
                $id = $this->atTransaccionModelo->metNuevaTransaccionB($registro);
                $registro['idTransaccion'] = $id;
                $idNuevo = $this->atTransaccionModelo->metTransaccionListar($registro['idTransaccion']);

            } else {
                $registro['status'] = 'modificar';
                $id = $this->atTransaccionModelo->metModificarTransaccionB($idTransaccion, $registro);
                $registro['idTransaccion'] = $id;
                $idNuevo = $this->atTransaccionModelo->metTransaccionListar($idTransaccion);
            }
            if (is_array($id)) {
                foreach ($registro as $titulo => $valor) {
                    if(!is_array($registro[$titulo])){
                        if (strpos($id[2], $registro[$titulo])) {
                            $registro[$titulo] = 'error';
                        }
                    }
                }
                $registro['status'] = 'errorSQL';
                echo json_encode($registro);
                exit;
            }
            $idNuevo['status'] = $registro['status'];
            echo json_encode($idNuevo);
            exit;
        }
        if ($idTransaccion != 0) {
            $db = $this->atTransaccionModelo->metTransaccionConsultaDetalle($idTransaccion);
            $this->atVista->assign('transaccionBD', $db);
        }

        $this->atVista->assign('ver', $ver);
        $this->atVista->assign('idTransaccion',$idTransaccion);
        $this->atVista->assign('selectTIPOTRANS',$this->atTransaccionModelo->atMiscelaneoModelo->metMostrarSelect('TIPOTRANS'));
        $this->atVista->assign('listadoVoucher',$this->atTransaccionModelo->atTipoVoucherModelo->metListar());
        $this->atVista->metRenderizar('nuevo', 'modales');
    }
    public function metEliminarTransaccion()
    {
        $idTransaccion = $this->metObtenerInt('idTransaccion');
        if($idTransaccion!=0){
            $id=$this->atTransaccionModelo->metEliminarTransaccion($idTransaccion);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Tipo de Transaccion Bancaria se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idTransaccion'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
}