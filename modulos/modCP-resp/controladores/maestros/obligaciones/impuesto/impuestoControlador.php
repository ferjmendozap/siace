<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/


class impuestoControlador extends Controlador
{
    private $atImpuestoModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atImpuestoModelo = $this->metCargarModelo('impuesto');
    }
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
    }

    public function metCuenta($cuenta)
    {
        $this->atVista->assign('lista',$this->atImpuestoModelo->metCuentaListar($cuenta));
        $this->atVista->assign('listaCuenta', $cuenta);
        $this->atVista->metRenderizar('listadoCuenta', 'modales');
    }


    public function metNuevoImpuesto()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min',
            'inputmask/jquery.inputmask.bundle.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $ver = $this->metObtenerInt('ver');
        $valido = $this->metObtenerInt('valido');
        $idImpuesto = $this->metObtenerInt('idImpuesto');

        if ($valido == 1){
            $excepcion=array('num_estatus','fk_cbb004_num_plan_cuenta','fk_cbb004_num_plan_cuenta_pub20');
            $ind=$this->metValidarFormArrayDatos('form','int',$excepcion);
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');

            if ($alphaNum != null && $ind == null) {
                $registro = $alphaNum;
            } elseif ($alphaNum == null && $ind != null) {
                $registro = $ind;
            } else {
                $registro = array_merge($ind, $alphaNum);
            }
            if (!isset($registro['num_estatus'])) {
                $registro['num_estatus'] = '0';
            }
            if ($registro['fk_prb002_num_partida_presupuestaria']==0) {
                $registro['fk_prb002_num_partida_presupuestaria'] = null;
            }
            if ($registro['fk_cbb004_num_plan_cuenta']==0) {
                $registro['fk_cbb004_num_plan_cuenta'] = null;
            }
            if ($registro['fk_cbb004_num_plan_cuenta_pub20']==0) {
                $registro['fk_cbb004_num_plan_cuenta_pub20'] = null;
            }
            if (in_array('error', $registro)) {
                $registro['status'] = 'error';
                echo json_encode($registro);
                exit;
            }
            $registro['num_factor_porcentaje'] = str_replace(',','.',$registro['num_factor_porcentaje']);
            if ($idImpuesto == 0) {

                $registro['status'] = 'nuevo';
                $id = $this->atImpuestoModelo->metNuevoImpuesto($registro['cod_impuesto'],$registro['txt_descripcion'],
                    $registro['ind_signo'],$registro['num_factor_porcentaje'],$registro['fk_a006_num_miscelaneo_detalle_flag_provision'],
                    $registro['fk_a006_num_miscelaneo_detalle_flag_imponible'],$registro['fk_a006_num_miscelaneo_detalle_flag_general'],
                    $registro['fk_a006_num_miscelaneo_detalle_tipo_comprobante'],$registro['fk_prb002_num_partida_presupuestaria'],
                    $registro['fk_cbb004_num_plan_cuenta_pub20'],$registro['fk_cbb004_num_plan_cuenta'],
                    $registro['fk_a006_num_miscelaneo_detalle_regimen_fiscal'],$registro['num_estatus']);
                $registro['idImpuesto'] = $id;


            } else {
                $registro['status'] = 'modificar';
                $id = $this->atImpuestoModelo->metModificarImpuesto($idImpuesto,$registro['txt_descripcion'],
                    $registro['ind_signo'],$registro['num_factor_porcentaje'],$registro['fk_a006_num_miscelaneo_detalle_flag_provision'],
                    $registro['fk_a006_num_miscelaneo_detalle_flag_imponible'],$registro['fk_a006_num_miscelaneo_detalle_flag_general'],
                    $registro['fk_a006_num_miscelaneo_detalle_tipo_comprobante'], $registro['fk_prb002_num_partida_presupuestaria'],
                    $registro['fk_cbb004_num_plan_cuenta_pub20'],$registro['fk_cbb004_num_plan_cuenta'],
                    $registro['fk_a006_num_miscelaneo_detalle_regimen_fiscal'],$registro['num_estatus']);
                $registro['idImpuesto'] = $id;

            }

            if (is_array($id)) {
                foreach ($registro as $titulo => $valor) {
                    if(!is_array($registro[$titulo])){
                        if (strpos($id[2], $registro[$titulo])) {
                            $registro[$titulo] = 'error';
                        }
                    }
                }
                $registro['status'] = 'errorSQL';
                echo json_encode($registro);
                exit;
            }
            echo json_encode($registro);
            exit;
        }

        if ($idImpuesto != 0) {
            $db = $this->atImpuestoModelo->metImpuestoConsultaDetalle($idImpuesto);
            $this->atVista->assign('dataBD', $db);
        }

        $this->atVista->assign('ver',$ver);
        $this->atVista->assign('listadoRegimen',$this->atImpuestoModelo->atMiscelaneoModelo->metMostrarSelect('REGFISCAL'));
        $this->atVista->assign('selectPROVDOC',$this->atImpuestoModelo->atMiscelaneoModelo->metMostrarSelect('PROVDOC'));
        $this->atVista->assign('selectIVAIMP',$this->atImpuestoModelo->atMiscelaneoModelo->metMostrarSelect('IVAIMP'));
        $this->atVista->assign('selectCOMPIMP',$this->atImpuestoModelo->atMiscelaneoModelo->metMostrarSelect('COMPIMP'));
        $this->atVista->assign('selectCLASIMP',$this->atImpuestoModelo->atMiscelaneoModelo->metMostrarSelect('CLASIMP'));

        $this->atVista->assign('idImpuesto',$idImpuesto);
        $this->atVista->metRenderizar('nuevo', 'modales');
    }

    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                i.*,
                rf.ind_nombre_detalle AS RegimenFiscal,
                Tipo.ind_nombre_detalle As Tipo,
                Provision.ind_nombre_detalle As Provision,
                imponible.cod_detalle AS imponible,
                imponible.ind_nombre_detalle AS nombreImponible,
                case i.ind_signo
                      when '1' then '<i class=\"md md-add\"></i>'
                      when '2' then '<i class=\"md md-remove\"></i>'
                end as ind_signo
              FROM
              cp_b015_impuesto AS i
              INNER JOIN a006_miscelaneo_detalle AS rf ON i.fk_a006_num_miscelaneo_detalle_regimen_fiscal = rf.pk_num_miscelaneo_detalle
              INNER JOIN a006_miscelaneo_detalle AS Tipo ON i.fk_a006_num_miscelaneo_detalle_tipo_comprobante = Tipo.pk_num_miscelaneo_detalle
              INNER JOIN a006_miscelaneo_detalle AS Provision ON i.fk_a006_num_miscelaneo_detalle_flag_provision = Provision.pk_num_miscelaneo_detalle
              INNER JOIN a006_miscelaneo_detalle as imponible ON imponible.pk_num_miscelaneo_detalle = i.fk_a006_num_miscelaneo_detalle_flag_imponible
            ";
        if ($busqueda['value']) {
            $sql .= " WHERE
                        ( 
                        i.cod_impuesto LIKE '%$busqueda[value]%' OR
                        i.txt_descripcion LIKE '%$busqueda[value]%' OR
                        Tipo.ind_nombre_detalle LIKE '%$busqueda[value]%' OR
                        rf.ind_nombre_detalle LIKE '%$busqueda[value]%' OR
                        i.num_factor_porcentaje LIKE '%$busqueda[value]%' OR
                        Provision.ind_nombre_detalle LIKE '%$busqueda[value]%' OR 
                        imponible.ind_nombre_detalle LIKE '%$busqueda[value]%' 
                        )
                        ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('cod_impuesto','txt_descripcion','Tipo','RegimenFiscal','num_factor_porcentaje','Provision','nombreImponible','ind_signo','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_impuesto';
        #construyo el listado de botones

        if (in_array('CP-01-07-01-02-01-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button class="modificar Impuesto btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static" idImpuesto="'.$clavePrimaria.'" title="Editar"
                        descipcion="El Usuario a Modificado Impuesto" titulo="<i class=\'fa fa-edit\'></i> Editar Impuesto">
                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('CP-01-07-01-02-02-V',$rol)) {
            $campos['boton']['Ver'] = '
               <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static" idImpuesto="'.$clavePrimaria.'" title="Consultar"
                        descipcion="El Usuario esta viendo un Impuesto" titulo="<i class=\'md md-remove-red-eye\'></i> Consultar Impuesto">
                    <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }

}