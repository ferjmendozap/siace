<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Deivis Millan        | d.millan@contraloriamonagas.gob.ve   | 0426-9372700
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class voucherControlador extends Controlador
{
    private $atVoucherModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atVoucherModelo = $this->metCargarModelo('voucher');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);


        $this->atVista->assign('voucherBD', $this->atVoucherModelo->metVoucherListar());
        $this->atVista->metRenderizar('listado');
    }

    public function metNuevoVoucher()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $ver = $this->metObtenerInt('ver');
        $idVoucher = $this->metObtenerInt('idVoucher');

        if ($valido == 1) {
            $Excepcion = array('num_flag_manual','num_estatus');
            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');
            $ind = $this->metValidarFormArrayDatos('form', 'int', $Excepcion);

            if ($alphaNum != null && $ind == null) {
                $registro = $alphaNum;
            } elseif ($alphaNum == null && $ind != null) {
                $registro = $ind;
            } else {
                $registro = array_merge($ind, $alphaNum);
            }
            if (in_array('error', $registro)) {
                $registro['status'] = 'error';
                echo json_encode($registro);
                exit;
            }
            if (!isset($registro['num_estatus'])) {
                $registro['num_estatus'] = 0;
            }
            if (!isset($registro['num_flag_manual'])) {
                $registro['num_flag_manual'] = 0;
            }
            if ($idVoucher == 0) {
                $registro['status'] = 'nuevo';
                $verificar['numero'] = $this->atVoucherModelo->metVoucherVerificar($registro['ind_descripcion']);
                $registro['numero'] = $verificar['numero']['numero'];

                $codigo['cod_voucher'] = $this->atVoucherModelo->nuevoCodigo('cp_b016_tipo_voucher', 'cod_voucher');
                $codigo = $codigo['cod_voucher']['cod_voucher'];
                $codigo = ($codigo + 1);
                $codigo = str_repeat("0", 2 - strlen($codigo)) . $codigo;
                $registro['cod_voucher'] = $codigo;

                $id = $this->atVoucherModelo->metNuevoVoucher($registro['cod_voucher'], $registro['ind_descripcion'], $registro['num_flag_manual'], $registro['num_estatus']);
                $registro['idVoucher'] = $id;
                echo json_encode($registro);
                exit;
            } else {
                $registro['status'] = 'modificar';
                $this->atVoucherModelo->metModificarVoucher($registro['ind_descripcion'], $registro['num_flag_manual'], $registro['num_estatus'],$idVoucher);
                $registro['idVoucher'] = $idVoucher;
                echo json_encode($registro);
                exit;
            }
        }
        if ($idVoucher != 0) {
            $db = $this->atVoucherModelo->metVoucherConsultaDetalle($idVoucher);
            $this->atVista->assign('dataBD', $db);
        }
        $this->atVista->assign('ver', $ver);
        $this->atVista->assign('idVoucher', $idVoucher);
        $this->atVista->metRenderizar('nuevo', 'modales');
    }
    public function metEliminar()
    {
        $idVoucher = $this->metObtenerInt('idVoucher');
        if($idVoucher!=0){
            $id=$this->atVoucherModelo->metEliminarVoucher($idVoucher);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Tipo de Voucher se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idVoucher'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

}