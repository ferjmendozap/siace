<form action="{$_Parametros.url}modCP/maestros/otros/clasificacionDocumentos/clasificacionDocumentosCONTROL/crearModificarMET" class="form" role="form" id="formAjax" method="post">
    <input type="hidden" value="1" name="valido" />
    <input type="hidden" value="{$idClasifDocumentos}" name="idClasifDocumentos"/>

    <div class="modal-body">
        <div class="col-sm-12">
            <div class="col-sm-3">
                <div class="form-group floating-label" id="ind_documento_clasificacionError">
                    <input type="text" class="form-control" maxlength="3" name="form[alphaNum][ind_documento_clasificacion]" id="ind_documento_clasificacion" {if isset($dataBD.ind_documento_clasificacion)} value="{$dataBD.ind_documento_clasificacion}" disabled {/if} >
                    <label for="ind_documento_clasificacion"><i class="md md-border-color"></i> Codigo</label>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group floating-label" id="ind_descripcionError">
                <input type="text" class="form-control" name="form[alphaNum][ind_descripcion]" id="ind_descripcion"  value="{if isset($dataBD.ind_descripcion)}{$dataBD.ind_descripcion}{/if}" {if $ver==1 } disabled{/if}>
                <label for="ind_descripcion"><i class="md md-border-color"></i> Descripcion</label>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($dataBD.num_flag_item) and $dataBD.num_flag_item==1} checked{/if} value="1" name="form[int][num_flag_item]" {if $ver==1 } disabled{/if}>
                    <span>Item</span>
                </label>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($dataBD.num_flag_cliente) and $dataBD.num_flag_cliente==1} checked{/if} value="1" name="form[int][num_flag_cliente]" {if $ver==1 } disabled{/if}>
                    <span>Cliente</span>
                </label>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($dataBD.num_flag_compromiso) and $dataBD.num_flag_compromiso==1} checked{/if} value="1" name="form[int][num_flag_compromiso]" {if $ver==1 } disabled{/if}>
                    <span>Compromiso</span>
                </label>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($dataBD.num_flag_transaccion) and $dataBD.num_flag_transaccion==1} checked{/if} value="1" name="form[int][num_flag_transaccion]" {if $ver==1 } disabled{/if}>
                    <span>Transación del Sistema</span>
                </label>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($dataBD.num_estatus) and $dataBD.num_estatus==0}{else} checked{/if} value="1" name="form[int][num_estatus]" {if $ver==1 } disabled{/if}>
                    <span>Estatus</span>
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 ">
                <div class="col-sm-12">
                    <div class="form-group floating-label">
                        <input type="text" disabled class="form-control disabled"
                               value="{if isset($dataBD.ind_usuario)}{$dataBD.ind_usuario}{/if}"
                               id="ind_usuario">
                        <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled"
                               value="{if isset($dataBD.fec_ultima_modificacion)}{$dataBD.fec_ultima_modificacion}{/if}"
                               id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-footer" >
        <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro"
                data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        {if !$ver==1 }
            <button type="button" class="btn btn-primary ink-reaction logsUsuarioModal" id="accion">
                {if $idClasifDocumentos!=0}
                    <i class="fa fa-edit"></i>&nbsp;Modificar
                {else}
                    <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
                {/if}
            </button>
        {/if}
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        /// Complementos
        var app = new  AppFunciones();
        $('.select2').select2({ allowClear: true });

        // ancho de la Modal
        $('#modalAncho').css( "width", "45%" );

        // anular accion del Formulario
        $("#formAjax").submit(function(){
            return false;
        });

        $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

        //Guardado y modales
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                var arrayCheck = ['num_estatus'];
                var arrayMostrarOrden = ['ind_documento_clasificacion','ind_descripcion','Tipo','RegimenFiscal','num_factor_porcentaje','Provision','nombreImponible','ind_signo','num_estatus'];
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTabla(dato,dato['pk_num_impuesto'],'idClasifDocumentos',arrayCheck,arrayMostrarOrden,'El Impuesto fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTabla(dato,dato['idClasifDocumentos'],'idClasifDocumentos',arrayCheck,arrayMostrarOrden,'El Impuesto fue guardado satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });
    });
</script>