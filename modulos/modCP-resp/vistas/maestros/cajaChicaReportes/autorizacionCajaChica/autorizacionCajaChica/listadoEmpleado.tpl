<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Empleados</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>

                            <th>Persona</th>
                            <th>Nro. Documento</th>
                            <th>Nombre Completo</th>
                            <th>Dependencia</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=empleado from=$lista}
                            <tr id="idempleado{$empleado.pk_num_empleado}">
                                <input type="hidden" value="{$empleado.pk_num_empleado}" class="empleado"
                                       empleado="{$empleado.pk_num_empleado}"
                                       nombre="{$empleado.ind_nombre1} {$empleado.ind_nombre2} {$empleado.ind_apellido1} {$empleado.ind_apellido2}"
                                        >
                                <td><label>{$empleado.pk_num_empleado}</label></td>
                                <td><label>{$empleado.ind_cedula_documento}</label></td>
                                <td><label>{$empleado.ind_nombre1} {$empleado.ind_nombre2} {$empleado.ind_apellido1} {$empleado.ind_apellido2}</label></td>
                                <td><label>{$empleado.ind_dependencia}</label></td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

</div>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatable2').on('click', 'tbody tr', function () {
            var input = $(this).find('input');

            {if $listaEmpleado == 'empleado'}
            $(document.getElementById('fk_rhb001_num_empleado')).val(input.attr('empleado'));
            $(document.getElementById('nombreEmpleado')).val(input.attr('nombre'));

            $('#cerrarModal2').click();

            {/if}

        });
    });
</script>
