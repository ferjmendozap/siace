
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">CLASIFICACION DE GASTOS</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Clasificacion</th>
                            <th>Descripcion</th>
                            <th>Estatus</th>
                            <th>Acción</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=clasifGasto from=$clasifGastoBD}
                        <tr id="idClasifGasto{$clasifGasto.pk_num_clasificacion_gastos}">

                            <td>{$clasifGasto.cod_clasificacion}</td>
                            <td>{$clasifGasto.ind_descripcion}</td>
                            <td><i class="{if $clasifGasto.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                            <td>
                                {if in_array('CP-01-07-03-01-01-M',$_Parametros.perfil)}
                                <button class="modificar ClasifGasto btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                        data-keyboard="false" data-backdrop="static" idClasifGasto="{$clasifGasto.pk_num_clasificacion_gastos}" title="Editar"
                                        descipcion="El Usuario ha Modificado una Clasificacion de Gastos" titulo="<i class='fa fa-edit'></i> Editar Clasificacion de Gastos">
                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                </button>
                                {/if}
                                {if in_array('CP-01-07-03-01-02-V',$_Parametros.perfil)}
                                <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                        data-keyboard="false" data-backdrop="static" idClasifGasto="{$clasifGasto.pk_num_clasificacion_gastos}" title="Consultar"
                                        descipcion="El Usuario esta viendo una Clasificacion de Gastos" titulo="<i class='md md-remove-red-eye'></i> Consultar Clasificacion de Gastos">
                                    <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                </button>
                                {/if}
                                {if in_array('CP-01-07-03-01-03-E',$_Parametros.perfil)}
                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idClasifGasto="{$clasifGasto.pk_num_clasificacion_gastos}" title="Eliminar"  boton="si, Eliminar"
                                            descipcion="El usuario ha eliminado un Tipo de Voucher" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Clasificacion de Gastos!!">
                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                    </button>
                                {/if}
                            </td>
                        </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="4">
                                {if in_array('CP-01-07-03-01-04-N',$_Parametros.perfil)}
                                <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                        descipcion="el Usuario ha creado una Nueva Clasificacion de Gasto"
                                        data-toggle="modal" data-target="#formModal"
                                        titulo="<i class='md md-create'></i>&nbsp;Registrar Nueva Clasificacion de Gasto" id="nuevo"
                                        data-keyboard="false" data-backdrop="static"><i class="md md-create"></i>&nbsp;Nueva Clasificacion de Gasto
                                </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modCP/maestros/cajaChicaReportes/clasificacionGasto/clasificacionGastoCONTROL/NuevaClasifGastoMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idClasifGasto:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.modificar', function () {

            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idClasifGasto: $(this).attr('idClasifGasto')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( $url,{ idClasifGasto: $(this).attr('idClasifGasto') , ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idClasifGasto=$(this).attr('idClasifGasto');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCP/maestros/cajaChicaReportes/clasificacionGasto/clasificacionGastoCONTROL/eliminarMET';
                $.post($url, { idClasifGasto: idClasifGasto },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idClasifGasto'+dato['idClasifGasto'])).html('');
                        swal("Eliminado!", "el Clasificacion de Gastos ha sido eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });

    });
</script>