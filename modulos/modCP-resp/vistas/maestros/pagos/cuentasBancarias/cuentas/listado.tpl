
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Cuentas Bancarias</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Banco</th>
                            <th>Cuenta Bancaria</th>
                            <th>Descripcion</th>
                            <th>Estatus</th>
                            <th>Acción</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th colspan="5">
                                {if in_array('CP-01-07-02-01-04-N',$_Parametros.perfil)}
                                <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                        descipcion="el Usuario ha creado un post de Cuenta Bacaria"
                                        data-toggle="modal" data-target="#formModal"
                                        titulo="<i class='md md-create'></i>&nbsp;Registrar Cuenta Bancaria" id="nuevo"
                                        data-keyboard="false" data-backdrop="static"><i class="md md-create"></i>&nbsp;Nueva Cuenta Bancaria
                                </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modCP/maestros/pagos/cuentasBancarias/cuentasCONTROL/nuevaCuentaMET';

        var app = new AppFunciones();
        var dt = app.dataTable(
            '#dataTablaJson',
            "{$_Parametros.url}modCP/maestros/pagos/cuentasBancarias/cuentasCONTROL/jsonDataTablaMET",
            "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
            [
                { "data": "banco" },
                { "data": "ind_num_cuenta" },
                { "data": "ind_descripcion" },
                { "orderable": false,"data": "num_estatus"},
                { "orderable": false,"data": "acciones"}
            ]
        );
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idCuenta:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idCuenta: $(this).attr('idCuenta')},function($dato){
            $('#ContenidoModal').html($dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( $url,{ idCuenta: $(this).attr('idCuenta') , ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.eliminar', function () {
            var idCuenta = $(this).attr('idCuenta');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCP/maestros/pagos/cuentasBancarias/cuentasCONTROL/eliminarMET';
                $.post($url, { idCuenta: idCuenta },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idCuenta'+dato['idCuenta'])).html('');
                        swal("Eliminado!", "La Cuenta Bancaria ha sido eliminada satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });

    });
</script>