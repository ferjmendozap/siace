
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">CHEQUERAS BANCARIAS </h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Banco.</th>
                            <th>Cta. Bancaria</th>
                            <th>Chequera Activa</th>
                            <th>Accion</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$dataBD}
                            <tr id="idChequera{$i.pk_num_chequera}">
                                <td>{$i.nombreBanco}</td>
                                <td>{$i.ind_num_cuenta}</td>
                                <td><i class="{if $i.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i></td>

                                <td width="120">
                                    {if in_array('CP-01-07-01-03-01-M',$_Parametros.perfil)}
                                    <button class="modificar Chequera btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" idChequera="{$i.pk_num_chequera}" idCuenta="{$i.fk_cpb014_num_cuenta_bancaria}" title="Editar"
                                            descipcion="El Usuario ha Modificado una Chequera" titulo="<i class='fa fa-edit'></i> Editar Chequera">
                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                    </button>
                                    {/if}
                                    {if in_array('CP-01-07-01-03-02-V',$_Parametros.perfil)}
                                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idChequera="{$i.pk_num_chequera}" idCuenta="{$i.fk_cpb014_num_cuenta_bancaria}" title="Consultar"
                                                descipcion="El Usuario esta viendo una Chequera" titulo="<i class='md md-remove-red-eye'></i> Consultar Chequera">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            {if in_array('CP-01-07-01-03-03-E',$_Parametros.perfil)}
                            <th colspan="4"><button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                                    descipcion="el Usuario a creado un post de chequera" data-toggle="modal"
                                                    data-target="#formModal" titulo="<i class='md md-create'></i>&nbsp;Registrar Nueva Chequera" id="nuevo"
                                                    data-keyboard="false" data-backdrop="static">
                                    <i class="md md-create"></i>&nbsp;Nueva Chequera
                                </button></th>
                            {/if}
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {

        var $url='{$_Parametros.url}modCP/maestros/pagos/chequera/chequeraCONTROL/accionesMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idChequera:0 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idChequera: $(this).attr('idChequera'), idCuenta: $(this).attr('idCuenta') },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( $url,{ idChequera: $(this).attr('idChequera') ,idCuenta: $(this).attr('idCuenta'), ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idChequera = $(this).attr('idChequera');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCP/maestros/pagos/cuentasBancarias/cuentasCONTROL/eliminarMET';
                $.post($url, { idChequera: idChequera },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idChequera'+dato['idChequera'])).html('');
                        swal("Eliminado!", "La Cuenta Bancaria ha sido eliminada satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>