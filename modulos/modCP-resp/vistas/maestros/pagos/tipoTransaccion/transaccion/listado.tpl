
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">TIPO DE TRANSACCIONES BANCARIAS</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Tipo</th>
                            <th>Descripcion</th>
                            <th>Vou.</th>
                            <th>Cuenta</th>
                            <th>Estatus</th>
                            <th>Accion</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$grupo=0}
                            {foreach item=transaccion from=$transaccionBD}
                                {if ($grupo != $transaccion.fk_a006_miscelaneo_detalle_tipo_transaccion)}
                                    {$grupo = $transaccion.fk_a006_miscelaneo_detalle_tipo_transaccion}
                                    <tr style="font-weight:bold; background-color:#C7C7C7;">
                                        <td>
                                         {$transaccion.ind_nombre_detalle}
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                {/if}
                                <tr id="idTransaccion{$transaccion.pk_num_banco_tipo_transaccion}">
                                    <td>{$transaccion.cod_tipo_transaccion}</td>
                                    <td>{$transaccion.ind_descripcion}</td>
                                    <td><i class="{if $transaccion.num_flag_voucher==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                    <td>{$transaccion.cod_cuenta}</td>
                                    <td><i class="{if $transaccion.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                    <td>
                                        {if in_array('CP-01-07-02-03-01-M',$_Parametros.perfil)}
                                        <button class="modificar Tipo de Transaccion Bancaria btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idTransaccion="{$transaccion.pk_num_banco_tipo_transaccion}" title="Editar"
                                                descipcion="El Usuario ha Modificado un Tipo de Transaccion Bancaria" titulo="<i class='fa fa-edit'></i> Editar Tipo de Transaccion Bancaria">
                                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                                        </button>
                                        {/if}
                                        {if in_array('CP-01-07-02-03-02-V',$_Parametros.perfil)}
                                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idTransaccion="{$transaccion.pk_num_banco_tipo_transaccion}" title="Consultar"
                                                descipcion="El Usuario esta viendo un Tipo de Transaccion Bancaria" titulo="<i class='md md-remove-red-eye'></i> Consultar Tipo de Transaccion Bancaria">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                                        {/if}
                                        {if in_array('CP-01-07-02-03-03-E',$_Parametros.perfil)}
                                        <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idTransaccion="{$transaccion.pk_num_banco_tipo_transaccion}" title="Eliminar"  boton="si, Eliminar"
                                                descipcion="El usuario ha eliminado un Tipo de Transaccion Bancaria" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Tipo de Transaccion Bancaria!!">
                                            <i class="md md-delete" style="color: #ffffff;"></i>
                                        </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="6">
                                {if in_array('CP-01-07-02-03-04-N',$_Parametros.perfil)}
                                <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                        descipcion="el Usuario ha creado un post de Documento"
                                        data-toggle="modal" data-target="#formModal"
                                        titulo="<i class='md md-create'></i>&nbsp;Registrar Nuevo Tipo de Transaccion" id="nuevo"
                                        data-keyboard="false" data-backdrop="static"><i class="md md-create"></i>&nbsp;Nuevo Tipo de Transaccion
                                </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modCP/maestros/pagos/tipoTransaccion/transaccionCONTROL/nuevaTransaccionMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ $idTransaccion:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.modificar', function () {

            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idTransaccion: $(this).attr('idTransaccion')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( $url,{ idTransaccion: $(this).attr('idTransaccion') , ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idTransaccion=$(this).attr('idTransaccion');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCP/maestros/pagos/tipoTransaccion/transaccionCONTROL/eliminarTransaccionMET';
                $.post($url, { idTransaccion: idTransaccion },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idTransaccion'+dato['idTransaccion'])).html('');
                        swal("Eliminado!", "el Tipo de Transaccion Bancaria ha sido eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });

    });
</script>