
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">TIPOS DE SERVICIOS</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Codigo</th>
                            <th>Descripcion</th>
                            <th>Regimen Fiscal</th>
                            <th>Estatus</th>
                            <th>Acción</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            {if in_array('CP-01-07-01-04-04-N',$_Parametros.perfil)}
                            <th colspan="5"><button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                                    descipcion="el Usuario a creado un post de Tipo de Servicios" data-toggle="modal"
                                                    data-target="#formModal" titulo="<i class='md md-create'></i>&nbsp;Registrar Nuevo Tipo de Servicios" id="nuevo"
                                                    data-keyboard="false" data-backdrop="static">
                                    <i class="md md-create"></i>&nbsp;Nuevo Tipo de Servicio
                                </button></th>
                            {/if}
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modCP/maestros/obligaciones/tiposServicios/serviciosCONTROL/nuevoServicioMET';
        var app = new AppFunciones();
        var dt = app.dataTable(
            '#dataTablaJson',
            "{$_Parametros.url}modCP/maestros/obligaciones/tiposServicios/serviciosCONTROL/jsonDataTablaMET",
            "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
            [
                { "data": "cod_tipo_servicio" },
                { "data": "ind_descripcion" },
                { "data": "RegimenFiscal" },
                { "orderable": false,"data": "num_estatus"},
                { "orderable": false,"data": "acciones"}
            ]
        );
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idServicio:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {

            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idServicio: $(this).attr('idServicio')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( $url,{ idServicio: $(this).attr('idServicio') , ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.eliminar', function () {
            var idServicio=$(this).attr('idServicio');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCP/maestros/obligaciones/tiposServicios/serviciosCONTROL/eliminarServicioMET';
                $.post($url, { idServicio: idServicio },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idServicio'+dato['idServicio'])).html('');
                        swal("Eliminado!", "el Tipo de Servicio ha sido eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });

    });
</script>