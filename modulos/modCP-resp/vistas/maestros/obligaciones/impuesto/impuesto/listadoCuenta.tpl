<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Plan Cuentas</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>cuenta</th>
                            <th>Descripcion</th>
                            <th>Tipo Cuenta</th>
                            <th>Naturaleza</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=cuenta from=$lista}
                            <tr id="idPlanCuenta{$cuenta.pk_num_cuenta}">
                                <input type="hidden" value="{$cuenta.pk_num_cuenta}" class="cuenta"
                                       codigo="{$cuenta.pk_num_cuenta}"
                                       cuenta="{$cuenta.cod_cuenta}" >
                                <td><label>{$cuenta.cod_cuenta}</label></td>
                                <td><label>{$cuenta.ind_descripcion}</label></td>
                                <td><label>{$cuenta.ind_cuenta}</label></td>
                                <td><label>{$cuenta.ind_naturaleza}</label></td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

</div>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatable2').on('click', 'tbody tr', function () {
            var input = $(this).find('input');

            {if $listaCuenta == 'cuenta'}
            $(document.getElementById('fk_cbb004_num_plan_cuenta')).val(input.attr('codigo'));
            $(document.getElementById('indCuenta')).val(input.attr('cuenta'));
            $('#cerrarModal2').click();

            {elseif $listaCuenta == 'cuentaPub20'}
            $(document.getElementById('fk_cbb004_num_plan_cuenta_pub20')).val(input.attr('codigo'));
            $(document.getElementById('indCuentaPub20')).val(input.attr('cuenta'));
            $('#cerrarModal2').click();

            {elseif $listaCuenta == 'cuentaAde'}
            $(document.getElementById('fk_cbb004_num_plan_cuenta_ade')).val(input.attr('codigo'));
            $(document.getElementById('indCuentaAde')).val(input.attr('cuenta'));
            $('#cerrarModal2').click();

            {elseif $listaCuenta == 'cuentaAdePub20'}
            $(document.getElementById('fk_cbb004_num_plan_cuenta_ade_pub20')).val(input.attr('codigo'));
            $(document.getElementById('indCuentaAdePub20')).val(input.attr('cuenta'));
            $('#cerrarModal2').click();
            {/if}


        });
    });
</script>
