
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">CONCEPTO DE VIATICOS</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Concepto</th>
                            <th>Descripción</th>
                            <th>Categoría</th>
                            <th>Valor U.T</th>
                            <th>Estatus</th>
                            <th>Acción</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$dataBD}
                        <tr id="idConceptoViatico{$i.pk_num_concepto_gasto_viatico}">
                                <td>{$i.cod_concepto }</td>
                                <td>{$i.ind_descripcion }</td>
                                <td>{$i.categoria}</td>
                                <td>{$i.num_valor_ut}</td>
                                <td><i class="{if $i.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                <td>
                                    {if in_array('CP-01-07-04-02-01-M',$_Parametros.perfil)}
                                        <button class="modificar tipoespecificaciones btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idConceptoViatico="{$i.pk_num_concepto_gasto_viatico}" title="Editar"
                                                descipcion="El Usuario a Modificado un Concepto de Viatico" titulo="<i class='fa fa-edit'></i> Editar Concepto de Viatico">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                        </button>
                                    {/if}
                                    {if in_array('CP-01-07-04-02-02-V',$_Parametros.perfil)}
                                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idConceptoViatico="{$i.pk_num_concepto_gasto_viatico}" title="Consultar"
                                                    descipcion="El Usuario esta viendo un Concepto de Viatico" titulo="<i class='md md-remove-red-eye'></i> Consultar Concepto de Viatico">
                                                <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                         </button>
                                    {/if}
                                    {if in_array('CP-01-07-04-02-04-E',$_Parametros.perfil)}
                                        <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idConceptoViatico="{$i.pk_num_concepto_gasto_viatico}" title="Eliminar"  boton="si, Eliminar"
                                                descipcion="El usuario ha eliminado un Concepto de Viatico" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Concepto de Viatico!!">
                                            <i class="md md-delete" style="color: #ffffff;"></i>
                                        </button>
                                    {/if}

                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="6">
                                {if in_array('CP-01-07-04-02-03-N',$_Parametros.perfil)}
                                <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                        descipcion="el Usuario ha creado un post de Nuevo Concepto de Viatico"
                                        data-toggle="modal" data-target="#formModal"
                                        titulo="<i class='md md-create'></i>&nbsp;Registrar Concepto de Viatico" id="nuevo"
                                        data-keyboard="false"
                                        data-backdrop="static"><i class="md md-create"></i>&nbsp;Nuevo Concepto de Viatico
                                </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {
        var url='{$_Parametros.url}modCP/maestros/viaticos/conceptoViaticos/conceptoViaticosCONTROL/nuevoConceptoViaticoMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idConceptoViatico:0 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });


        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( url,{ idConceptoViatico: $(this).attr('idConceptoViatico') , ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {

            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idConceptoViatico: $(this).attr('idConceptoViatico')},function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idConceptoViatico=$(this).attr('idConceptoViatico');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var url='{$_Parametros.url}modCP/maestros/viaticos/conceptoViaticos/conceptoViaticosCONTROL/eliminarMET';
                $.post(url, { idConceptoViatico: idConceptoViatico },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idConceptoViatico'+dato['idConceptoViatico'])).html('');
                        swal("Eliminado!", "el Tipo de especificaciones ha sido eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>