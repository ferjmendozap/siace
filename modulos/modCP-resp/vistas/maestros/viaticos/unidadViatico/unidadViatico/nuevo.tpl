<form action="{$_Parametros.url}modCP/maestros/viaticos/unidadViatico/unidadViaticoCONTROL/nuevaUnidadViaticoMET" class="form" role="form" id="formAjax" method="post">
    <input type="hidden" value="1" name="valido" />
    <input type="hidden" value="{$idUnidadViatico}" id="idUnidadViatico" name="idUnidadViatico"/>

    <div class="modal-body">
        <div class="col-lg-12">
            <div class="col-sm-6">
                <div class="form-group floating-label" id="ind_periodoError">
                    <input type="text" class="form-control datePeriodo" value="{if isset($unidadViaBD.ind_periodo)}{$unidadViaBD.ind_periodo}{/if}" name="form[txt][ind_periodo]" id="ind_periodo" {if $ver==1} disabled{/if} >
                    <label for="ind_periodo"><i class="fa fa-calendar"></i> Periodo <b style="color: red;font-weight: bold;">*</b></label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="ind_num_resolucionError">
                    <input type="text" class="form-control" value="{if isset($unidadViaBD.ind_num_resolucion)}{$unidadViaBD.ind_num_resolucion}{/if}" name="form[alphaNum][ind_num_resolucion]" id="ind_num_resolucion" {if $ver==1} disabled{/if} >
                    <label for="ind_num_resolucion"><i class="fa fa-code"></i> N&ordm; Resolucion <b style="color: red;font-weight: bold;">*</b></label>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="col-sm-6">
                <div class="form-group floating-label" id="num_unidad_de_viaticoError">
                    <input type="text" class="form-control" value="{if isset($unidadViaBD.num_unidad_viatico)}{$unidadViaBD.num_unidad_viatico}{/if}" name="form[int][num_unidad_viatico]" id="num_unidad_viatico" {if $ver==1} disabled{/if} >
                    <label for="num_unidad_viatico"  style="font-size: 14px"><i class="icm icm-coins"></i> Monto de la U. de Viatico <b style="color: red;font-weight: bold;">*</b></label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="ind_estadoError">
                    <input type="text" class="form-control" value="{if isset($unidadViaBD.ind_estado) and $unidadViaBD.ind_estado=='AP'}Aprobado{else}En Preparacion{/if}" disabled id="ind_estado">
                    <label for="ind_estado"><i class="fa fa-eye"></i> Estado:</label>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group floating-label" id="ind_descripcionError">
                <textarea id="ind_descripcion" name="form[alphaNum][ind_descripcion]" class="form-control" rows="2" placeholder="" {if $ver==1} disabled{/if} >{if isset($unidadViaBD.ind_descripcion)}{$unidadViaBD.ind_descripcion}{/if}</textarea>
                <label for="ind_descripcion"><i class="md md-comment"></i> Descripcion del Aumento de la U. de Viatico <b style="color: red;font-weight: bold;">*</b></label>
            </div>
        </div>
        <div class="col-sm-12">
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="fk_rhb001_num_empleado_creaError">
                        <input type="text" class="form-control" value="{if isset($unidadViaBD.EMPLEADO_PREPARA)}{$unidadViaBD.EMPLEADO_PREPARA}{/if}" name="form[int][fk_rhb001_num_empleado_crea]" disabled id="fk_rhb001_num_empleado_crea">
                        <label for="fk_rhb001_num_empleado_crea"><i class="md md-person"></i> Creado Por</label>
                    </div>
                </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="fec_preparadoError">
                    <input type="text" class="form-control" value="{if isset($unidadViaBD.fec_preparado)}{$unidadViaBD.fec_preparado}{/if}" disabled name="form[alphaNum][fec_preparado]" id="fec_preparado">
                    <label for="fec_preparado"><i class="md md-today"></i> Fecha de Creacion</label>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group floating-label" id="fk_rhb001_num_empleado_apruebaError">
                    <input type="text" class="form-control" value="{if isset($unidadViaBD.EMPLEADO_APRUEBA)}{$unidadViaBD.EMPLEADO_APRUEBA}{/if}" name="form[int][fk_rhb001_num_empleado_aprueba]" disabled id="fk_rhb001_num_empleado_aprueba">
                    <label for="fk_rhb001_num_empleado_aprueba"><i class="md md-person"></i> Aprobado Por</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="fec_preparadoError">
                    <input type="text" class="form-control" value="{if isset($unidadViaBD.fec_aprobado)}{$unidadViaBD.fec_aprobado}{/if}" disabled name="form[alphaNum][fec_preparado]" id="fec_preparado">
                    <label for="fec_preparado"><i class="md md-today"></i> Fecha de Aprobacion</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5 ">
                <div class="col-sm-12">
                    <div class="form-group floating-label">
                        <input type="text" disabled class="form-control disabled"
                               value="{if isset($unidadViaBD.ind_usuario)}{$unidadViaBD.ind_usuario}{/if}"
                               id="ind_usuario">
                        <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled"
                               value="{if isset($unidadViaBD.fec_ultima_modificacion)}{$unidadViaBD.fec_ultima_modificacion}{/if}"
                               id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>

            <div class="modal-footer" >
                <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro"
                        data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
                {if isset($estado) and $estado == 'PR'}
                    <button type="button" class="btn btn-primary accionesEstado ink-reaction logsUsuarioModal" id="PR">
                        <i class="icm icm-rating3"></i>&nbsp;Aprobar</button>
                {/if}
                {if $ver!=1}
                    <button type="button" class="btn btn-primary ink-reaction logsUsuarioModal" id="accion">
                        {if $idUnidadViatico!=0}
                            <i class="fa fa-edit"></i>&nbsp;Modificar
                        {else}
                            <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
                        {/if}
                    </button>
                {/if}
            </div>
        </div>
</form>
</script>
<script type="text/javascript">
        $(document).ready(function() {
            var app = new  AppFunciones();
            $("#formAjax").submit(function(){
                return false;
            });
            $('.datePeriodo').datepicker({ autoclose: true, todayHighlight: true, minViewMode: 1, format: "yyyy-mm", language:'es'});
            $('#modalAncho').css( "width", "35%" );
            $('#accion').click(function(){
                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });
                $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                    var arrayCheck = ['num_estatus','num_flag_manual'];
                    var arrayMostrarOrden = ['ind_periodo','ind_num_resolucion','num_unidad_viatico','ind_estado'];
                    if(dato['status']=='error'){
                        app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                    }else if(dato['status']=='errorSQL'){
                        app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                    }else if(dato['status']=='modificar'){
                        app.metActualizarRegistroTabla(dato,dato['pk_num_unidad_viatico'],'idUnidadViatico',arrayCheck,arrayMostrarOrden,'La unidad de Viatico fue modificada satisfactoriamente.','cerrarModal','ContenidoModal');
                    }else if(dato['status']=='nuevo'){
                        app.metNuevoRegistroTabla(dato,dato['idUnidadViatico'],'idUnidadViatico',arrayCheck,arrayMostrarOrden,'La unidad de Viatico fue guardada satisfactoriamente.','cerrarModal','ContenidoModal');
                    }
                },'json');
            });

            {if isset($estado)}
            $('.accionesEstado').click(function () {
                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });
                $.post(
                        $("#formAjax").attr("action"),
                        { idUnidadViatico: $('#idUnidadViatico').val(), estado: $(this).attr('id'), valido: 1 },
                        function(dato){
                            if (dato['status'] == 'errorSQL') {
                                app.metValidarError(dato, 'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                            } else if (dato['status'] == 'OK') {
                                $(document.getElementById('idUnidadViatico'+dato['idUnidadViatico'])).remove();
                                swal(dato['Mensaje']['titulo']+'!!', dato['Mensaje']['contenido'], "success");
                                $(document.getElementById('cerrarModal')).click();
                                $(document.getElementById('ContenidoModal')).html('');
                            }
                        },'JSON');
            });
            {/if}
        });
</script>