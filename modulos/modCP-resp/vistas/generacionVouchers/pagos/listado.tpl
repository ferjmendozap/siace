<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">GENERACIÓN DE VOUCHERS DE PAGOS</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>CTA. BANCARIA</th>
                            <th>CHEQUE</th>
                            <th>PROVEEDOR</th>
                            <th>MONTO</th>
                            <th>FECHA DE PAGO</th>
                            <th>TIPO DE PAGO</th>
                            <td>ACCIÓN</td>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$dataBD}
                            <tr id="idPago"{$i.pk_num_pago}>
                                <td>{$i.ind_num_cuenta}</td>
                                <td>{$i.ind_num_pago}</td>
                                <td>{$i.nombreProveedor}</td>
                                <td>{$i.num_monto_pago}</td>
                                <td>{$i.fec_pago}</td>
                                <td>{$i.tipoPago}</td>
                                <td>
                                    <button class="acciones btn ink-reaction btn-raised btn-xs btn-primary-light" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" idPago="{$i.pk_num_pago}" title="Generar Vouchers"
                                            descipcion="El Usuario ha GENERADO un Voucher de Pago" titulo="<i class='icm icm-spinner12'></i> Generar Voucher de Pago">
                                        <i class="icm icm-spinner12" style="color: #ffffff;"></i>
                                    </button>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div></section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modCP/generacionVouchers/pagosCONTROL/generarVouchersMET';

        $('#datatable1 tbody').on( 'click', '.acciones', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idPago: $(this).attr('idPago')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
    });
</script>