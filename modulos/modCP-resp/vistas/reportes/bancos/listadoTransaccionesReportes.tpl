<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="section-header">
                        <h2 class="text-primary">LISTADO DE TRANSACCIONES </h2>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="col-sm-12">
                                <div class="col-sm-4">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkBanco">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 text-right">
                                        <label for="pk_num_tipo_documento"
                                               class="control-label" style="margin-top: 10px;"> Banco:</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group" id="pk_num_banco_tipo_transaccionError" style="margin-top: -10px;">
                                            <select class="form-control select2-list select2" required
                                                    data-placeholder="Seleccione el Tipo de Banco"
                                                    id="BANCO"
                                                    disabled="disabled">
                                                <option value="">Seleccione el Banco</option>
                                                {foreach item=banco from=$selectBANCOS}
                                                    <option value="{$banco.pk_num_miscelaneo_detalle}" >{$banco.ind_nombre_detalle}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="col-sm-3 text-right">
                                        <label for="pk_num_tipo_documento"
                                               class="control-label" style="margin-top: 10px;"> Cta. Bancaria:</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group"
                                             id="pk_num_tipo_documentoError" style="margin-top: -10px;">
                                            <select class="form-control select2" data-placeholder="Seleccione Cuenta Bancaria"
                                                    id="CUENTA"
                                                    disabled="disabled">
                                                <option value="">Seleccione Cuenta Bancaria</option>}
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkFecha" checked>
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2" >
                                        <label for="pago"
                                               class="control-label" style="margin-top: 10px;"> Fecha:</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="desde"
                                               style="text-align: center"
                                               value="{date('Y-m')}-01"
                                               placeholder="Desde"
                                               readonly>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="hasta"
                                               style="text-align: center"
                                               value="{date('Y-m-d')}"
                                               placeholder="Hasta"
                                               readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="col-sm-4">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkTrans">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 text-right">
                                        <label for="pk_num_tipo_documento"
                                               class="control-label" style="margin-top: 10px;"> Tipo de Transacción:</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group" style="margin-top: -10px;">
                                            <select class="form-control select2-list select2" required
                                                    data-placeholder="Seleccione el Tipo de Transaccion"
                                                    id="TRANSACCION"
                                                    disabled="disabled">
                                                <option value="">Seleccione el Tipo de Transaccion</option>
                                                {foreach item=i from=$selectTrans}
                                                    <option value="{$i.pk_num_banco_tipo_transaccion}" >{$i.ind_descripcion}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkPeriodo">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="periodo"
                                               class="control-label" style="margin-top: 10px;"> Periodo:</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center datePeriodo"
                                               id="desdePeriodo"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Desde"
                                               disabled="disabled"
                                               readonly>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center datePeriodo"
                                               id="hastaPeriodo"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Hasta"
                                               disabled="disabled"
                                               readonly>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkTrans">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 text-right">
                                        <label for="pk_num_tipo_documento"
                                               class="control-label" style="margin-top: 10px;"> Estado:</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group" style="margin-top: -10px;">
                                            <select class="form-control select2-list select2" required
                                                    data-placeholder="Seleccione el Tipo de Transaccion"
                                                    id="ESTADO"
                                                    disabled="disabled">
                                                <option value="">Seleccione...</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkProveedor">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 text-right">
                                        <label for="proveedor"
                                               class="control-label" style="margin-top: 10px;"> Persona:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text"
                                               class="form-control" id="codigoProveedor"
                                               value=""
                                               readonly size="9%">
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control"
                                                   id="nombreProveedor"
                                                   value=""
                                                   disabled>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group floating-label">
                                                <button
                                                        class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                        type="button"
                                                        data-toggle="modal"
                                                        data-target="#formModal2"
                                                        titulo="Listado de Personas"
                                                        id="botonPersona"
                                                        disabled="disabled"
                                                        url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/personaMET/persona/">
                                                    <i class="md md-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">

                                </div>

                            </div>

                            <div align="center">
                                <button class="buscar logsUsuario btn ink-reaction btn-raised btn-primary"
                                        >BUSCAR
                                </button>
                            </div>
                        </div>
                    </div>


                    <form action="{$_Parametros.url}modCP/reportes/obligacionesReportesCONTROL/" id="formAjax"
                          class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body" id="resultado" style="height: 560px" >

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<script type="text/javascript">
       $(document).ready(function () {
           var app = new AppFunciones();
           app.metWizard();

           /// Complementos
           $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});
           $('.datePeriodo').datepicker({ autoclose: true, todayHighlight: true, minViewMode: 1, format: "yyyy-mm", language:'es'});


           $('.accionModal').click(function () {
               $('#formModalLabel2').html($(this).attr('titulo'));
               $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                   $('#ContenidoModal2').html($dato);
               });
           });

           //busqueda segun el filtro
           $('.buscar').click(function () {
               var idCuenta = $('#CUENTA').val();
               var desde = $('#desde').val();
               var hasta = $('#hasta').val();

               var url = '{$_Parametros.url}modCP/reportes/bancosCONTROL/imprimirListadoTransaccionesMET?idCuenta='+idCuenta+'&'+'desde='+desde+'&'+'hasta='+hasta;
               $('#resultado').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');

           });

           //habilitar y deshabilitar campos del filtro
           $('#checkProveedor').click(function () {
               if($('#botonPersona').attr('disabled') =='disabled') {
                   $('#botonPersona').attr('disabled', false);
               }else{
                   $('#botonPersona').attr('disabled', true);
                   $(document.getElementById('codigoProveedor')).val("");
                   $(document.getElementById('nombreProveedor')).val("");

               }
           });
           $('#checkBanco').click(function () {
               if(this.checked){
                   $('#BANCO').attr('disabled', false);
                   $('#CUENTA').attr('disabled', false);
               }else{
                   $('#BANCO').attr('disabled', true);
                   $(document.getElementById('BANCO')).val("");
                   $('#CUENTA').attr('disabled', true);
                   $(document.getElementById('CUENTA')).val("");

               }
           });
           $('#checkFecha').click(function () {
               if(this.checked){
                   $('#desde').attr('disabled', false);
                   $('#hasta').attr('disabled', false);
               }else{
                   $('#desde').attr('disabled', true);
                   $(document.getElementById('desde')).val("");
                   $('#hasta').attr('disabled', true);
                   $(document.getElementById('hasta')).val("");

               }

           });
           $('#checkPeriodo').click(function () {
               if(this.checked){
                   $('#desdePeriodo').attr('disabled', false);
                   $('#hastaPeriodo').attr('disabled', false);
               }else{
                   $('#desdePeriodo').attr('disabled', true);
                   $(document.getElementById('desdePeriodo')).val("");
                   $('#hastaPeriodo').attr('disabled', true);
                   $(document.getElementById('hastaPeriodo')).val("");
               }

           });
           $('#checkTrans').click(function () {
               if(this.checked){
                   $('#TRANSACCION').attr('disabled', false);
               }else{
                   $('#TRANSACCION').attr('disabled', true);
                   $(document.getElementById('TRANSACCION')).val("");

               }

           });

           $("#BANCO").change(function(){
               var url = '{$_Parametros.url}modCP/procesos/procesosCONTROL/cuentasBancariasMET/';
               var idBanco = $(this).val();
               $.post(url,{  idBanco: idBanco },
                       function (dato) {
                           if (dato) {
                               $('#CUENTA').html('');
                               $('#CUENTA').append('<option value="">Seleccione...</option>');
                               var id = dato['id'];
                               for (var i = 0; i < id.length; i++) {
                                   $('#CUENTA').append('<option value="' + id[i]['pk_num_cuenta'] + '" selected>' + id[i]['ind_num_cuenta'] + '</option>');
                               }
                           }
                       }, 'json');
           });

       });

</script>