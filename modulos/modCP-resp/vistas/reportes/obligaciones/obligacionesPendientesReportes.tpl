<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="section-header">
                        <h2 class="text-primary">OBLIGACIONES PENDIENTES </h2>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="col-sm-12">
                                <div class="col-sm-5">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkProveedor">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="pk_num_tipo_documento"
                                               class="control-label" style="margin-top: 10px;"> Proveedor:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text"
                                               name="form[int][fk_a003_num_persona_proveedor]"
                                               class="form-control" id="codigoProveedor"
                                               value="{if isset($obligacionBD.fk_a003_num_persona_proveedor)}{$obligacionBD.fk_a003_num_persona_proveedor}{/if}"
                                               readonly size="9%">
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control"
                                                   id="nombreProveedor"
                                                   value="{if isset($obligacionBD.fk_a003_num_persona_proveedor)}{$obligacionBD.proveedor}{/if}"
                                                   disabled>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group floating-label">
                                                <button
                                                        class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                        type="button"
                                                        data-toggle="modal"
                                                        data-target="#formModal2"
                                                        titulo="Listado de Personas"
                                                        id="botonPersona"
                                                        disabled="disabled"
                                                        url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/personaMET/persona/">
                                                    <i class="md md-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkFecha">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="pago"
                                               class="control-label" style="margin-top: 10px;"> Fecha Venc:</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="desde"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Desde"
                                               disabled="disabled"
                                               readonly>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="hasta"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Hasta"
                                               disabled="disabled"
                                               readonly>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkCosto">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="centroCosto"
                                               class="control-label"  style="margin-top: 10px;">C. Costo:</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group" id="centroCostoError">
                                            <input type="text" class="form-control" value="" id="centroCosto" readonly>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group floating-label">
                                            <button
                                                    type="button"
                                                    class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                    data-toggle="modal"
                                                    data-target="#formModal2"
                                                    id="bcentroCosto"
                                                    titulo="Listado Centro de Costo"
                                                    disabled="disabled"
                                                    url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/centroCostoMET">
                                                <i class="md md-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div align="center">
                                <button class="buscar logsUsuario btn ink-reaction btn-raised btn-primary"
                                        >BUSCAR
                                </button>
                            </div>
                        </div>
                    </div>


                    <form action="{$_Parametros.url}modCP/reportes/obligacionesReportesCONTROL/" id="formAjax"
                          class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body" id="resultado" style="height: 560px" >

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<script type="text/javascript">
       $(document).ready(function () {
           var app = new AppFunciones();
           app.metWizard();

           /// Complementos
           $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});

           $('.accionModal').click(function () {
               $('#formModalLabel2').html($(this).attr('titulo'));
               $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                   $('#ContenidoModal2').html($dato);
               });
           });

           //busqueda segun el filtro
           $('.buscar').click(function () {
               var proveedor = $('#codigoProveedor').val();
               var desde = $('#desde').val();
               var hasta = $('#hasta').val();
               var centroCosto = $('#centroCosto').val();

               var url = '{$_Parametros.url}modCP/reportes/obligacionesCONTROL/imprimirObligacionesPendientesMET/?proveedor=' + proveedor + '&' + 'desde=' + desde + '&' + 'hasta=' + hasta + '&' + 'centroCosto=' + centroCosto;
               $('#resultado').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');

           });

           //habilitar y deshabilitar campos del filtro
           $('#checkProveedor').click(function () {
               if($('#botonPersona').attr('disabled') =='disabled') {
                   $('#botonPersona').attr('disabled', false);
               }else{
                   $('#botonPersona').attr('disabled', true);
                   $(document.getElementById('codigoProveedor')).val("");
                   $(document.getElementById('nombreProveedor')).val("");

               }
           });

           $('#checkFecha').click(function () {
               if(this.checked){
                   $('#desde').attr('disabled', false);
                   $('#hasta').attr('disabled', false);
               }else{
                   $('#desde').attr('disabled', true);
                   $(document.getElementById('desde')).val("");
                   $('#hasta').attr('disabled', true);
                   $(document.getElementById('hasta')).val("");

               }
           });

           $('#checkCosto').click(function () {
               if(this.checked){
                   $('#bcentroCosto').attr('disabled', false);
               }else{
                   $('#bcentroCosto').attr('disabled', true);
                   $(document.getElementById('centroCosto')).val("");

               }
           });
       });

</script>