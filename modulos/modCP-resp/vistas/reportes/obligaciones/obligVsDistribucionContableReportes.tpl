<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="section-header">
                        <h2 class="text-primary">OBLIGACIONES VS DISTRIBUCION CONTABLE</h2>
                    </div>
                    <div class="card">
                        <div class="card-body" id="filtro">
                            <div class="col-sm-12">
                                <div class="col-sm-5">
                                    <div class="col-sm-1 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkProveedor">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="pk_num_tipo_documento"
                                               class="control-label" style="margin-top: 10px;"> Proveedor:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text"
                                               name="form[int][fk_a003_num_persona_proveedor]"
                                               class="form-control" id="codigoProveedor"
                                               value="{if isset($obligacionBD.fk_a003_num_persona_proveedor)}{$obligacionBD.fk_a003_num_persona_proveedor}{/if}"
                                               readonly size="9%">
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control"
                                                   id="nombreProveedor"
                                                   value="{if isset($obligacionBD.fk_a003_num_persona_proveedor)}{$obligacionBD.proveedor}{/if}"
                                                   disabled>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group floating-label">
                                                <button
                                                        class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                        type="button"
                                                        data-toggle="modal"
                                                        data-target="#formModal2"
                                                        titulo="Listado de Personas"
                                                        id="botonPersona"
                                                        disabled="disabled"
                                                        url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/personaMET/persona/">
                                                    <i class="md md-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="col-sm-1 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkFecha">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="registro"
                                               class="control-label" style="margin-top: 10px; "> F. Registro:</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="registroDesde"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Desde"
                                               disabled="disabled"
                                               readonly>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="registroHasta"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Hasta"
                                               disabled="disabled"
                                               readonly>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkCosto">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="centroCosto"
                                               class="control-label"  style="margin-top: 10px;">C. Costo:</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group" id="centroCostoError">
                                            <input type="text" class="form-control" value="" id="centroCosto" readonly>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group floating-label">
                                            <button
                                                    type="button"
                                                    class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                    data-toggle="modal"
                                                    data-target="#formModal2"
                                                    id="bcentroCosto"
                                                    titulo="Listado Centro de Costo"
                                                    disabled="disabled"
                                                    url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/centroCostoMET">
                                                <i class="md md-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">

                                <div class="col-sm-5">
                                    <div class="col-sm-1 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkPago">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="pago"
                                               class="control-label" style="margin-top: 10px;"> F. Pago:</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control text-center date"
                                               id="pagoDesde"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Desde"
                                               disabled="disabled"
                                               readonly>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control text-center date"
                                               id="pagoHasta"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Hasta"
                                               disabled="disabled"
                                               readonly>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="col-sm-1 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkCuenta">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="centroCosto"
                                               class="control-label"  style="margin-top: 10px;">Cuenta:</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group" id="fk_cbb004_num_plan_cuentaError">
                                            <input type="text" class="form-control"   value="" id="indCuenta" disabled>
                                            <input type="hidden" id="fk_cbb004_num_plan_cuenta" value="">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group floating-label">
                                            <button
                                                    class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                    type="button"
                                                    id="bcuenta"
                                                    disabled="disabled"
                                                    data-toggle="modal" data-target="#formModal2"
                                                    titulo="Listado de Plan Cuenta"
                                                    url="{$_Parametros.url}modCP/maestros/obligaciones/impuesto/impuestoCONTROL/CuentaMET/cuenta">
                                                <i class="md md-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="check">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="tipoPago"
                                               class="control-label" style="margin-top: 10px;"> Estado:</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <select class="form-control select2-list select2" required
                                                disabled="disabled"
                                                id="tipoPago"
                                                data-placeholder="Seleccione Tipo de Pago">
                                            <option value="">Seleccione...</option>
                                            {foreach item=tipoPago from=$listadoTipoPago}
                                                <option value="{$tipoPago.pk_num_miscelaneo_detalle}">{$tipoPago.ind_nombre_detalle}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div align="center">
                                <button class="buscar logsUsuario btn ink-reaction btn-raised btn-primary"
                                        >BUSCAR
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body" id="resultado" style="height: 560px" >

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<script type="text/javascript">
       $(document).ready(function () {
           var app = new AppFunciones();
           app.metWizard();

           /// Complementos
           $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});

           $('.accionModal').click(function () {
               $('#formModalLabel2').html($(this).attr('titulo'));
               $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                   $('#ContenidoModal2').html($dato);
               });
           });

           //busqueda segun el filtro
           $('.buscar').click(function () {

               var proveedor = $('#codigoProveedor').val();
               var centroCosto = $('#centroCosto').val();
               var registroDesde = $('#registroDesde').val();
               var registroHasta = $('#registroHasta').val();
               var pagoDesde = $('#pagoDesde').val();
               var pagoHasta = $('#pagoHasta').val();
               var cuenta = $('#fk_cbb004_num_plan_cuenta').val();

               var url = '{$_Parametros.url}modCP/reportes/obligacionesCONTROL/imprimirObligacionesContableMET/?proveedor=' + proveedor + '&' + 'centroCosto=' + centroCosto + '&' + 'registroDesde=' + registroDesde + '&' + 'registroHasta=' + registroHasta + '&' + 'pagoDesde=' + pagoDesde+ '&' + 'pagoHasta=' + pagoHasta + '&' + 'cuenta=' + cuenta;
               $('#resultado').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');

           });

           //habilitar y deshabilitar campos del filtro
           $('#checkProveedor').click(function () {
               if(this.checked){
                   $('#botonPersona').attr('disabled', false);
               }else{
                   $('#botonPersona').attr('disabled', true);
                   $(document.getElementById('codigoProveedor')).val("");
                   $(document.getElementById('nombreProveedor')).val("");

               }
           });

           $('#checkFecha').click(function () {
               if(this.checked){
                   $('#registroDesde').attr('disabled', false);
                   $('#registroHasta').attr('disabled', false);
               }else{
                   $('#registroDesde').attr('disabled', true);
                   $(document.getElementById('registroDesde')).val("");
                   $('#registroHasta').attr('disabled', true);
                   $(document.getElementById('registroHasta')).val("");

               }
           });

           $('#checkPago').click(function () {
               if(this.checked){
                   $('#pagoDesde').attr('disabled', false);
                   $('#pagoHasta').attr('disabled', false);
               }else {
                   $('#pagoDesde').attr('disabled', true);
                   $(document.getElementById('pagoDesde')).val("");
                   $('#pagoHasta').attr('disabled', true);
                   $(document.getElementById('pagoHasta')).val("");
               }
           });

           $('#checkCosto').click(function () {
               if(this.checked){
                   $('#bcentroCosto').attr('disabled', false);
               }else{
                   $('#bcentroCosto').attr('disabled', true);
                   $(document.getElementById('centroCosto')).val("");

               }
           });

           $('#checkCuenta').click(function () {
               if(this.checked){
                   $('#bcuenta').attr('disabled', false);
               }else{
                   $('#bcuenta').attr('disabled', true);
                   $(document.getElementById('indCuenta')).val("");
                   $(document.getElementById('fk_cbb004_num_plan_cuenta')).val("");

               }
           });
       });

</script>