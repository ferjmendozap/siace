<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="section-header">
                        <h2 class="text-primary">COMPARACION DE PAGOS Y OBLIGACION </h2>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkProveedor">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="pk_num_tipo_documento"
                                               class="control-label" style="margin-top: 10px;"> Proveedor:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text"
                                               name="form[int][fk_a003_num_persona_proveedor]"
                                               class="form-control" id="codigoProveedor"
                                               value=""
                                               readonly size="9%">
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control"
                                                   id="nombreProveedor"
                                                   value=""
                                                   disabled>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group floating-label">
                                                <button
                                                        class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                        type="button"
                                                        data-toggle="modal"
                                                        data-target="#formModal2"
                                                        titulo="Listado de Personas"
                                                        id="botonPersona"
                                                        disabled="disabled"
                                                        url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/personaMET/persona/">
                                                    <i class="md md-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkFecha">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="pago"
                                               class="control-label" style="margin-top: 10px;"> Periodo:</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="periodo"
                                               style="text-align: center"
                                               value=""
                                               disabled="disabled"
                                               readonly>
                                    </div>
                                </div>
                            </div>

                            <div align="center">
                                <button class="buscarRetencion logsUsuario btn ink-reaction btn-raised btn-primary"
                                        id="buscar" idTipo="pagoOblig">BUSCAR
                                </button>
                            </div>
                        </div>
                    </div>


                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                        <form action="{$_Parametros.url}modCP/pagos/chequesCONTROL" id="formAjax"
                              class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#uno" data-toggle="tab" id="unoPDF" idTipo="pagoOblig" class="buscarRetencion"><span class="step">1</span> <span class="title" style="font-weight:bold;">PAGOS VS. OBLIGACIONES</span></a></li>
                                    <li><a href="#dos" data-toggle="tab" id="dosPDF" idTipo="obligPago" class="buscarRetencion"><span class="step">2</span> <span class="title" style="font-weight:bold;">OBLIGACIONES VS. PAGOS</span></a></li>
                                </ul>
                            </div>
                            <div class="tab-content clearfix">
                                <div class="tab-pane active" id="uno">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body" id="unoLista" style="height: 560px" >

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="dos">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body" id="dosLista" style="height: 560px">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <ul class="pager wizard">
                                <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                                <li class="next"><a class="btn-raised" href="javascript:void(0);" id="siguiente">Último</a></li>
                            </ul>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<script type="text/javascript">
       $(document).ready(function () {
           var app = new AppFunciones();
           app.metWizard();
           /// Complementos
           $('.date').datepicker({ autoclose: true, todayHighlight: true, minViewMode: 1, format: "yyyy-mm", language:'es'});

           ///acciones
           $('.accionModal').click(function () {
               $('#formModalLabel2').html($(this).attr('titulo'));
               $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                   $('#ContenidoModal2').html($dato);
               });
           });

           //busqueda segun el filtro
           $('.buscarRetencion').click(function () {
               var proveedor = $('#codigoProveedor').val();
               var periodo = $('#periodo').val();
               var tipo = $(this).attr('idTipo');
               $('#buscar').attr('idTipo', tipo);

               if(tipo == 'pagoOblig'){
                   var url = '{$_Parametros.url}modCP/reportes/obligacionesCONTROL/imprimirCompracionPagosObligMET/?proveedor=' + proveedor + '&' + 'periodo=' + periodo;
                   $('#unoLista').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');

               }else {
                   var url = '{$_Parametros.url}modCP/reportes/obligacionesCONTROL/imprimirCompracionObligPagosMET/?proveedor=' + proveedor + '&' + 'periodo=' + periodo;
                   $('#dosLista').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');

               }
           });


           //habilitar y deshabilitar campos del filtro
           $('#checkProveedor').click(function () {
               if(this.checked){
                   $('#botonPersona').attr('disabled', false);
               }else{
                   $('#botonPersona').attr('disabled', true);
                   $(document.getElementById('codigoProveedor')).val("");
                   $(document.getElementById('nombreProveedor')).val("");

               }
           });

           $('#checkFecha').click(function () {
               if(this.checked){
                   $('#periodo').attr('disabled', false);
               }else{
                   $('#periodo').attr('disabled', true);
                   $(document.getElementById('periodo')).val("");

               }
           });

       });

</script>