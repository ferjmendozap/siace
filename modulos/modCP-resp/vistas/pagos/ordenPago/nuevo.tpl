<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body ">
                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">

                        <form action="{$_Parametros.url}modCP/pagos/ordenPagoCONTROL/accionesOrdenPagoMET" id="formAjax"
                              class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                            <input type="hidden" value="1" name="valido"/>
                            <input type="hidden" value="{$ordenPagoBD.fk_cbb001_num_voucher_asiento}" id="idVoucherPago" name="idVoucherPago"/>
                            <input type="hidden" value="{$idOrdenPago}" id="idOrdenPago" name="idOrdenPago"/>

                            <div class="tab-content clearfix">
                                <div class="tab-pane active" id="informacionGeneral">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-head card-head-xs style-primary text-center">
                                                    <header class="text-center">Información de la Obligacion</header>
                                                </div>
                                                <div class="card-body" style="padding: 4px;">
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="proveedor"
                                                                       class="control-label" style="margin-top: 10px;"> Proveedor:</label>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <input type="text"
                                                                       id="proveedor"
                                                                       class="form-control" id="proveedor"
                                                                       value="{if isset($ordenPagoBD.pk_num_persona)}{$ordenPagoBD.pk_num_persona}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       id="proveedor"
                                                                       style="font-weight:bold;"
                                                                       value="{if isset($ordenPagoBD.fk_a003_num_persona_proveedor_a_pagar)}{$ordenPagoBD.fk_a003_num_persona_proveedor_a_pagar}{/if}"
                                                                       readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="fk_cpb002_num_tipo_documento"
                                                                       class="control-label" style="margin-top: 10px;"> Tipo de Documento:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <div class="form-group"
                                                                     id="fk_cpb002_num_tipo_documentoError" style="margin-top: -10px;">
                                                                    <select name="form[int][fk_cpb002_num_tipo_documento]"
                                                                            class="form-control select2"
                                                                            data-placeholder="Seleccione la dependencia" {if $estado} disabled {/if}>
                                                                        <option value="">Seleccione...</option>
                                                                        {foreach item=doc from=$documento}
                                                                            {if isset($ordenPagoBD.fk_cpb002_num_tipo_documento) and $ordenPagoBD.fk_cpb002_num_tipo_documento == $doc.pk_num_tipo_documento}
                                                                                <option value="{$doc.pk_num_tipo_documento}" selected>{$doc.ind_descripcion}</option>
                                                                            {else}
                                                                                <option value="{$doc.pk_num_tipo_documento}">{$doc.ind_descripcion}</option>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="ind_nro_control"
                                                                       class="control-label" style="margin-top: 10px;"> Nro Documento:</label>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text"
                                                                       class="form-control" id="ind_nro_control"
                                                                       value="{if isset($ordenPagoBD.ind_nro_control)}{$ordenPagoBD.ind_nro_control}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                        </div>

                                                    </div>


                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-head card-head-xs style-primary text-center">
                                                    <header class="text-center">Información de la Orden</header>
                                                </div>
                                                <div class="card-body" style="padding: 4px;">
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="ind_num_orden"
                                                                       class="control-label" style="margin-top: 10px;"> Nro. Orden:</label>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text"
                                                                       class="form-control" id="ind_num_orden"
                                                                       value="{if isset({$ordenPagoBD.ind_num_orden})}{$ordenPagoBD.ind_num_orden}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">

                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="fec_orden_pago"
                                                                       class="control-label" style="margin-top: 10px;"> F. Orden Pago:</label>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text"
                                                                       id="fec_orden_pago"
                                                                       class="form-control" id="fec_orden_pago"
                                                                       value="{if isset($ordenPagoBD.fec_orden_pago)}{$ordenPagoBD.fec_orden_pago}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="ind_estado"
                                                                       class="control-label" style="margin-top: 10px;"> Estado:</label>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="text"
                                                                       class="form-control" id="ind_estado"
                                                                       value="{if isset($ordenPagoBD.ind_estado)}{$ordenPagoBD.ind_estado}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="fk_a006_num_miscelaneo_tipo_pago"
                                                                       class="control-label" style="margin-top: 10px;"> Tipo de Pago:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <div class="form-group"
                                                                     id="fk_a006_num_miscelaneo_tipo_pagoError" style="margin-top: -10px;">
                                                                    <select name="form[int][fk_a006_num_miscelaneo_tipo_pago]"
                                                                            class="form-control select2"
                                                                            data-placeholder="Seleccione la dependencia" {if $estado} disabled {/if}>
                                                                        <option value="">Seleccione...</option>
                                                                        {foreach item=tipoPago from=$listadoTipoPago}
                                                                            {if isset($ordenPagoBD.fk_a006_num_miscelaneo_tipo_pago) and $ordenPagoBD.fk_a006_num_miscelaneo_tipo_pago == $tipoPago.pk_num_miscelaneo_detalle}
                                                                                <option value="{$tipoPago.pk_num_miscelaneo_detalle}" selected>{$tipoPago.ind_nombre_detalle}</option>
                                                                            {else}
                                                                                <option value="{$tipoPago.pk_num_miscelaneo_detalle}">{$tipoPago.ind_nombre_detalle}</option>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="num_monto"
                                                                       class="control-label" style="margin-top: 10px;"> Total Obliacion:</label>
                                                            </div>
                                                            <div class="col-sm-9">
                                                                <input type="text"
                                                                       id="num_monto"
                                                                       class="form-control" id="num_monto"
                                                                       value="{if isset($ordenPagoBD.num_monto)}{$ordenPagoBD.num_monto}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="pk_num_cuenta"
                                                                       class="control-label" style="margin-top: 10px;"> Cuenta Bancaria:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <div class="form-group"
                                                                     id="pk_num_cuentaError" style="margin-top: -10px;">
                                                                    <select name="form[int][pk_num_cuenta]"
                                                                            class="form-control select2"
                                                                            data-placeholder="Seleccione la dependencia" {if $estado} disabled {/if}>
                                                                        <option value="">Seleccione Cuenta Bancaria</option>}
                                                                        {foreach item=proceso from=$listadoCuentas}
                                                                            {if isset($ordenPagoBD.fk_cpb014_num_cuenta) and $ordenPagoBD.fk_cpb014_num_cuenta == $proceso.pk_num_cuenta }
                                                                                <option value="{$proceso.pk_num_cuenta}" selected >{$proceso.ind_num_cuenta}</option>
                                                                            {else}
                                                                                <option value="{$proceso.pk_num_cuenta}">{$proceso.ind_num_cuenta}</option>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="num_monto_adelanto"
                                                                       class="control-label" style="margin-top: 10px;"> Adelantos (-):</label>
                                                            </div>
                                                            <div class="col-sm-9">
                                                                <input type="text"
                                                                       id="num_monto_adelanto"
                                                                       class="form-control" id="num_monto_adelanto"
                                                                       value="{if isset($ordenPagoBD.num_monto_adelanto)}{$ordenPagoBD.num_monto_adelanto}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-9">
                                                                <div class="form-group floating-label" id="fk_rhb001_num_empleado_conformaError">
                                                                    <input type="text" class="form-control" value="{if isset($ordenPagoBD.EMPLEADO_CONFORMA)}{$ordenPagoBD.EMPLEADO_CONFORMA}{/if}" name="form[int][fk_rhb001_num_empleado_conforma]" disabled id="fk_rhb001_num_empleado_conforma">
                                                                    <label for="fk_rhb001_num_empleado_conforma"><i class="md md-person"></i> Conformado Por</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="form-group floating-label" id="fec_conformadoError">
                                                                    <input type="text" class="form-control" value="{if isset($ordenPagoBD.fec_conformado)}{$ordenPagoBD.fec_conformado}{/if}" disabled name="form[alphaNum][fec_conformado]" id="fec_conformado">
                                                                    <label for="fec_conformado"><i class="md md-today"></i> Fecha</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="ind_num_caja_chica"
                                                                       class="control-label" style="margin-top: 10px;"> Pagos Parciales (-):</label>
                                                            </div>
                                                            <div class="col-sm-9">
                                                                <input type="text"
                                                                       id="ind_num_caja_chica"
                                                                       class="form-control" id="ind_num_caja_chica"
                                                                       value="0.000000"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-9">
                                                                <div class="form-group floating-label" id="fk_rhb001_num_empleado_apruebaError">
                                                                    <input type="text" class="form-control" value="{if isset($ordenPagoBD.EMPLEADO_APRUEBA)}{$ordenPagoBD.EMPLEADO_APRUEBA}{/if}" name="form[int][fk_rhb001_num_empleado_aprueba]" disabled id="fk_rhb001_num_empleado_aprueba">
                                                                    <label for="fk_rhb001_num_empleado_aprueba"><i class="md md-person"></i> Aprobado Por</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="form-group floating-label" id="fec_aprobacionError">
                                                                    <input type="text" class="form-control" value="{if isset($ordenPagoBD.fec_aprobado)}{$ordenPagoBD.fec_aprobado}{/if}" disabled name="form[alphaNum][fec_aprobacion]" id="fec_aprobacion">
                                                                    <label for="fec_aprobacion"><i class="md md-today"></i> Fecha</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="ind_num_caja_chica"
                                                                       class="control-label" style="margin-top: 10px;">Saldo Pendiente (-):</label>
                                                            </div>
                                                            <div class="col-sm-9">
                                                                <input type="text"
                                                                       id="ind_num_caja_chica"
                                                                       class="form-control" id="ind_num_caja_chica"
                                                                       value=""
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group floating-label" id="ind_comentariosError">
                                                            <textarea id="ind_comentarios" name="form[alphaNum][ind_comentarios]" class="form-control" rows="2" placeholder=""  {if $estado} disabled {/if}>{if isset($ordenPagoBD.ind_comentarios)}{$ordenPagoBD.ind_comentarios}{/if}</textarea>
                                                            <label for="ind_comentarios"><i class="md md-comment"></i> Concepto:</label>
                                                        </div>
                                                    </div>
                                                    {if isset($estado) || isset($ordenPagoBD.ind_motivo_anulacion)}
                                                        {if $estado == 'AN' || isset($ordenPagoBD.ind_motivo_anulacion)}
                                                            <div class="col-sm-12">
                                                                <div class="form-group floating-label" id="ind_motivo_anulacionError">
                                                                    <textarea id="ind_motivo_anulacion" name="form[alphaNum][ind_motivo_anulacion]" class="form-control" rows="2" placeholder="" {if isset($ordenPagoBD.ind_motivo_anulacion) and $ordenPagoBD.ind_motivo_anulacion!=''}disabled{/if}>{if isset($ordenPagoBD.ind_motivo_anulacion)}{$ordenPagoBD.ind_motivo_anulacion}{/if}</textarea>
                                                                    <label for="ind_motivo_anulacion"><i class="md md-comment"></i> Motivo Aulacion:</label>
                                                                </div>
                                                            </div>
                                                        {/if}
                                                    {/if}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group floating-label">
                                                    <input type="text" disabled class="form-control disabled"
                                                           value="{if isset($ordenPagoBD.ind_usuario)}{$ordenPagoBD.ind_usuario}{/if}"
                                                           id="ind_usuario">
                                                    <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="col-sm-12">
                                            <div class="form-group floating-label" id="cod_tipo_nominaError">
                                                <input type="text" disabled class="form-control disabled"
                                                       value="{if isset($ordenPagoBD.fec_ultima_modificacion)}{$ordenPagoBD.fec_ultima_modificacion}{/if}"
                                                       id="fec_ultima_modificacion">
                                                <label for="cod_tipo_nomina"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro"
            data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
    </button>

    {if isset($estado)}
        {if $estado == 'AN'}
            <button type="button" class="btn btn-danger accionesEstado logsUsuarioModal" id="ANULAR">
                <i class="icm icm-blocked"></i>&nbsp;Anular</button>
        {/if}
    {/if}
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();
        app.metWizard();
        /// Complementos
        $('.select2').select2({ allowClear: true});
        // anular accion del Formulario
        $("#formAjax").submit(function () {
            return false;
        });

        // ancho de la Modal
        $('#modalAncho').css("width", "75%");

        {if isset($estado)}
            $('.accionesEstado').click(function () {
               var idVoucherPago = $('#idVoucherPago').val();
                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });
                $.post(
                    $("#formAjax").attr("action"),
                    { idOrdenPago: $('#idOrdenPago').val(), idVoucherPago: $('#idVoucherPago').val(),estado: $(this).attr('id'), motivo: $('#ind_motivo_anulacion').val()},
                    function(dato){
                        if (dato['status'] == 'errorSQL') {
                            app.metValidarError(dato, 'Disculpa. Contacta Con la Direccion Tecnica porque hay un ERROR interno');
                        } else if (dato['status'] == 'error') {
                            app.metValidarError(dato, 'Disculpa. el(los) campo(s) seleccionados con una X son Obligatorios');
                        }  else if (dato['status'] == 'OK') {
                            if(idVoucherPago != '') {
                                var tipoVoucher = 'ordenPago'; var tipo = 'ordenPago';
                                $('#tablaPost tbody').html('');
                                $(document.getElementById('idPago' + dato['idPago'])).remove();
                                swal({
                                    type: "success",
                                    title: dato['Mensaje']['titulo'] + '!!',
                                    text: dato['Mensaje']['contenido'],
                                    timer: 2000,
                                    showConfirmButton: false
                                });
                                $(document.getElementById('cerrarModal')).click();
                                setTimeout(function () {
                                    $('#formModal2').modal('toggle');
                                    $('#modalAncho2').css("width", "80%");
                                    $('#formModalLabel2').html('VOUCHERS');
                                    var idPago = dato['idPago'];
                                    $.post('{$_Parametros.url}modCP/pagos/pagosCONTROL/voucherMET', {
                                        idVoucherPago: $('#idVoucherPago').val(),
                                        idOrdenPago: $('#idOrdenPago').val(),
                                        tipoVoucher: tipoVoucher,
                                        tipo: tipo
                                    }, function (dato) {
                                        $('#ContenidoModal2').html(dato);
                                    });
                                }, 2000);
                            }else{
                                $(document.getElementById('idOrdenPago'+dato['idOrdenPago'])).remove();
                                swal(dato['Mensaje']['titulo']+'!!', dato['Mensaje']['contenido'], "success");
                                $(document.getElementById('cerrarModal')).click();
                                $(document.getElementById('ContenidoModal')).html('');
                            }
                        }
                    },'JSON');
            });
        {/if}







    });

</script>