<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">{if isset($lista) and $lista =='prepago'}PREPARACION DEL PREPAGO {else}LISTA DE ORDENES DE PAGO{/if}</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>NRO. ORDEN</th>
                            <th>PAGAR A</th>
                            <th>DOC. FISCAL A</th>
                            <th>N° DOC.</th>
                            <th>FECHA DOC.</th>
                            <th>MONTO</th>
                            <th>ESTADO</th>
                            <td>ACCIÓN</td>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$ordenPagoBD}
                            <tr id="idOrdenPago{$i.pk_num_orden_pago}">
                                <td>{$i.ind_num_orden}</td>
                                <td>{$i.fk_a003_num_persona_proveedor_a_pagar}</td>
                                <td>{$i.ind_documento_fiscal}</td>
                                <td>{$i.ind_nro_control}</td>
                                <td>{$i.fec_documento}</td>
                                <td>{$i.num_monto}</td>
                                <td>{$i.ind_estado}</td>
                                <td>
                                    {if $i.estado =='PE'  && $lista ==''}
                                        {if in_array('CP-01-03-01-03-A',$_Parametros.perfil)}
                                            <button class="acciones btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" id="AN" idOrdenPago="{$i.pk_num_orden_pago}" title="Anular"
                                                    descipcion="El Usuario ha ANULADO un orden pago" titulo="<i class='icm icm-blocked'></i> Anular Orden de Pago">
                                                <i class="icm icm-blocked" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    {/if}
                                    {if isset($lista) && $lista =='prepago'}
                                        {if in_array('CP-01-03-01-02-G',$_Parametros.perfil)}
                                            <button class="acciones btn ink-reaction btn-raised btn-xs btn-primary-light" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" id="GE" idOrdenPago="{$i.pk_num_orden_pago}" title="Generar"
                                                    descipcion="El Usuario ha GENERADO un orden pago" titulo="<i class='icm icm-spinner12'></i> Generar Orden de Pago">
                                                <i class="icm icm-spinner12" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    {/if}
                                    {if in_array('CP-01-03-01-01-V',$_Parametros.perfil)}
                                    <button class="acciones logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static"  id="VER" idOrdenPago="{$i.pk_num_orden_pago}" title="Consultar"
                                            descipcion="El Usuario esta viendo una orden de pago" titulo="<i class='md md-remove-red-eye'></i> Consultar Orden de Pago">
                                        <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                    </button>
                                    {/if}
                                    {if isset($lista) && $lista ==''}
                                    {if in_array('CP-01-03-01-04-I',$_Parametros.perfil)}
                                    <button class="imprimir logsUsuario btn ink-reaction btn-raised btn-xs btn-success" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" idOrdenPago="{$i.pk_num_orden_pago}" title="Imprimir"
                                            descipcion="El Usuario esta Imprimiendo una Orden de Pago" titulo="<i class='md md-print'></i> Imprimir Orden de Pago">
                                        <i class="md md-print" style="color: #ffffff;"></i>
                                    </button>
                                    {/if}
                                </td>
                                {/if}
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div></section>


<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modCP/pagos/ordenPagoCONTROL/accionesOrdenPagoMET';

        $('#datatable1 tbody').on( 'click', '.acciones', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idOrdenPago: $(this).attr('idOrdenPago'), estado: $(this).attr('id') },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.imprimir', function () {
            var idOrdenPago = $(this).attr('idOrdenPago');
            var url='{$_Parametros.url}modCP/pagos/ordenPagoCONTROL/imprimirMET/'+idOrdenPago;
            $('#formModalLabel').html($(this).attr('titulo'));

                $('#ContenidoModal').html('<iframe frameborder="0" src="'+url+'" width="100%" height="540px"></iframe>');

        });
    });
</script>