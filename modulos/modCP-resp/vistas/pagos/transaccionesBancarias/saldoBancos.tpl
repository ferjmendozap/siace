
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary"><h2 class="text-primary">SALDO DE CUENTAS BANCARIAS</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr class="trListaHead">
                            <th scope="col" width="250">Banco</th>
                            <th scope="col" width="150">Nro. Cuenta</th>
                            <th scope="col">Descripcion</th>
                            <th scope="col" width="150">Banco</th>
                            <th scope="col" width="35">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$dataBD}
                            <tr id="idCuenta{$i.pk_num_cuenta}">
                                <td>{$i.banco}</td>
                                <td>{$i.cuentaBancaria}</td>
                                <td>{$i.ind_descripcion}</td>
                                <td>{$i.Monto}</td>
                                <td width="120">
                                    <button class="acciones logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static"  id="TRANSACCIONES" idCuenta="{$i.pk_num_cuenta}" title="Consultar"
                                            descipcion="El Usuario esta viendo una transaccion" titulo="<i class='md md-remove-red-eye'></i> Consultar Transacciones de Cuentas Bancarias">
                                        <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                    </button>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>

<script type="text/javascript">

    $(document).ready(function() {

        var url='{$_Parametros.url}modCP/pagos/transaccionesBancariasCONTROL/accionesMET/';

        $('#datatable1 tbody').on( 'click', '.acciones', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idCuenta: $(this).attr('idCuenta'), estado: $(this).attr('id') },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

    });
</script>