<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">

                        <form action="{$_Parametros.url}modCP/pagos/transaccionesBancariasCONTROL/accionesMET/" id="formAjax"
                              class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                            <input type="hidden" value="1" name="valido"/>
                            <input type="hidden" value="{$idTransaccion}" id="idTransaccion" name="idTransaccion"/>

                            <div class="tab-content clearfix">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body" style="padding: 4px;">
                                                    <div class="card-head card-head-xs style-primary text-center">
                                                        <header class="text-center">Informacion General</header>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="ind_num_transaccion"
                                                                       class="control-label" style="margin-top: 10px;"> # Transaccion:</label>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control"
                                                                       id="ind_num_transaccion"
                                                                       style="font-weight:bold; text-align: center"
                                                                       value="{if isset($transaccionBD.ind_num_transaccion)}{$transaccionBD.ind_num_transaccion}{/if}"
                                                                       readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="ind_periodo_contable"
                                                                       class="control-label" style="margin-top: 10px;"> Periodo:</label>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control"
                                                                       id="ind_periodo_contable"
                                                                       name="form[txt][ind_periodo_contable]"
                                                                       style="font-weight:bold; text-align: center"
                                                                       value="{if isset($transaccionBD.ind_periodo_contable)}{$transaccionBD.ind_periodo_contable}{else}{date('Y-m')}{/if}"
                                                                       readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="fec_transaccion"
                                                                       class="control-label" style="margin-top: 10px;"> F. Transaccion:</label>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control text-center date"
                                                                       id="fec_transaccion"
                                                                       name="form[txt][fec_transaccion]"
                                                                       readonly
                                                                       style="font-weight:bold; text-align: center"
                                                                        {if isset($ver) and $ver==1} disabled {/if} 
                                                                       value="{if isset($transaccionBD.fec_transaccion)}{$transaccionBD.fec_transaccion}{else}{date('Y-m-d')}{/if}" >
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="proveedor"
                                                                       class="control-label" style="margin-top: 10px;"> Estado:</label>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="hidden" class="form-control"
                                                                       id="ind_estado"
                                                                       name="form[alphaNum][ind_estado]"
                                                                       value="{if isset($transaccionBD.ind_estado)}{$transaccionBD.ind_estado}{else}PR{/if}">
                                                                <input type="text" class="form-control"
                                                                       style="font-weight:bold; text-align: center"
                                                                       value="{if isset($transaccionBD.ind_estado)}{$transaccionBD.ind_estado_comp}{/if}"
                                                                       readonly>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="form-group floating-label" id="txt_comentariosError">
                                                            <textarea class="form-control" name="form[txt][txt_comentarios]" id="txt_comentarios" rows="3" {if isset($ver) and $ver==1} disabled {/if} >{if isset($transaccionBD.txt_comentarios)}{$transaccionBD.txt_comentarios}{/if}</textarea>
                                                            <label for="txt_comentarios"><i class="md md-person"></i> Comentarios</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group floating-label" id="txt_comentario_anulacionError">
                                                            <input type="text" class="form-control" value="{if isset($transaccionBD.txt_comentario_anulacion)}{$transaccionBD.txt_comentario_anulacion}{/if}" name="form[txt][txt_comentario_anulacion]" id="txt_comentario_anulacion" {if $estado!='ANULAR' and $estado!='ACTUALIZAR'}disabled{/if}>
                                                            <label for="txt_comentario_anulacion"><i class="md md-person"></i> Razon de Rechazo</label>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="fk_rhb001_num_empleado_creaPorError">
                                                                <input type="text" class="form-control" value="{if isset($transaccionBD.fk_rhb001_num_empleado_crea)}{$transaccionBD.EMPLEADO_PREPARA}{else}{$empleado.nombreEmpleado}{/if}" name="form[int][fk_rhb001_num_empleado_crea]" disabled id="fk_rhb001_num_empleado_crea">
                                                                <label for="fk_rhb001_num_empleado_crea"><i class="md md-person"></i> Preparado Por</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="fec_preparacionError">
                                                                <input type="text" class="form-control" value="{if isset($transaccionBD.fec_preparacion)}{$transaccionBD.fec_preparacion}{else}{date('Y-m-d')}{/if}" disabled name="form[txt][fec_preparacion]" id="fec_preparacion">
                                                                <label for="fec_preparacion"><i class="md md-today"></i> Fecha</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="checkbox checkbox-styled">
                                                            <label>
                                                                <input type="checkbox" {if isset($transaccionBD.num_flag_presupuesto) and $transaccionBD.num_flag_presupuesto==1} checked{/if} value="1" name="form[int][num_flag_presupuesto]" id="num_flag_presupuesto" {if isset($ver) and $ver==1} disabled {/if}  >
                                                                <span>Afecta Presupuesto</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group floating-label">
                                                    <input type="text" disabled class="form-control disabled"
                                                           value="{if isset($transaccionBD.ind_usuario)}{$transaccionBD.ind_usuario}{/if}"
                                                           id="ind_usuario">
                                                    <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="col-sm-12">
                                            <div class="form-group floating-label" id="fec_ultima_modificacionError">
                                                <input type="text" disabled class="form-control disabled"
                                                       value="{if isset($transaccionBD.fec_ultima_modificacion)}{$transaccionBD.fec_ultima_modificacion}{/if}"
                                                       id="fec_ultima_modificacion">
                                                <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-head card-head-xs style-primary" align="center">
                                        <header>Detalles</header>
                                    </div>
                                    <tr class="card-body" style="padding: 4px;">
                                        <div id="imput_nuevo"></div>

                                        <table class="table table-striped table-hover" style="width: 2000px;display:block;overflow: auto;">
                                            <thead style="display: inline-block; width: 2000px;">
                                            <tr>
                                                <th scope="col" width="25">#</th>
                                                <th scope="col" width="100">Tipo</th>
                                                <th scope="col" width="600">Tipo de Transacci&oacute;n</th>
                                                <th scope="col" width="40">I/E</th>
                                                <th scope="col" width="250">Cta. Bancaria</th>
                                                <th scope="col" width="200">Monto</th>
                                                <th scope="col" width="250">Documento</th>
                                                <th scope="col" width="150">Doc. Referencia</th>
                                                <th scope="col" width="75">Persona</th>
                                                <th scope="col" width="75">C.Costo</th>
                                                <th scope="col" width="150">Partida</th>
                                                <th scope="col" width="35">Acciones</th>
                                            </tr>
                                            </thead>
                                            <tbody style="height: 170px;display: inline-block;width: 2000px;overflow: auto;" id="detalleTransaccion" >
                                            {if isset($transaccionDetalleBD)}
                                            {foreach item=i from=$transaccionDetalleBD}
                                            <tr id="codDetalle{$i.num_secuencia}">
                                                <input type="hidden" value="{$i.num_secuencia}" name="form[int][codDetalle][num_secuencia][{$i.num_secuencia}]">
                                                <input type="hidden" id="codDetalle{$i.num_secuencia}fk_cpb006_num_banco_tipo_transaccion" value="{$i.fk_cpb006_num_banco_tipo_transaccion}" name="form[int][codDetalle][{$i.num_secuencia}][fk_cpb006_num_banco_tipo_transaccion]">
                                                <input type="hidden" id="codDetalle{$i.num_secuencia}partida" value="{$i.fk_prb002_num_partida_presupuestaria}" name="form[int][codDetalle][{$i.num_secuencia}][fk_prb002_num_partida_presupuestaria]">'+

                                                <td width="25"><input type="text" class="form-control text-left" id="codDetalle{$i.num_secuencia}num_secuencia" value="{$i.num_secuencia}" disabled ></td>
                                                <td width="100"><input type="text" class="form-control text-left" id="codDetalle{$i.num_secuencia}codTransaccion" value="{$i.cod_tipo_transaccion}" disabled ></td>
                                                <td width="600">
                                                    <input type="text" class="form-control  accionModal" id="codDetalle{$i.num_secuencia}nombreTransaccion" value="{$i.decripcionTransaccion}" {if isset($ver) and $ver==1} disabled {/if}
                                                    data-toggle="modal"
                                                    data-target="#formModal2"
                                                    titulo="Buscar de Tipo Transacciones Bancarias"
                                                    enlace="{$_Parametros.url}modCP/pagos/transaccionesBancariasCONTROL/tipoTransaccionesMET/transacciones/codDetalle{$i.num_secuencia}" >
                                                </td>
                                                <td width="40"><input type="text" class="form-control text-left" id="codDetalle{$i.num_secuencia}tipoTransaccion" value="{$i.cod_detalle}" readonly></td>
                                                <td width="250">
                                                    <select name="form[int][codDetalle][{$i.num_secuencia}][fk_cpb014_num_cuenta]" class="form-control select2" data-placeholder="Seleccione Cuenta Bancaria" {if isset($ver) and $ver==1} disabled {/if}  >
                                                        <option value="">Seleccione Cuenta Bancaria</option>}
                                                        {foreach item=proceso from=$listadoCuentas}
                                                            {if isset($i.pk_num_cuenta) and $i.pk_num_cuenta == $proceso.pk_num_cuenta }
                                                                <option value="{$proceso.pk_num_cuenta}" selected >{$proceso.ind_num_cuenta}</option>
                                                            {else}
                                                                <option value="{$proceso.pk_num_cuenta}">{$proceso.ind_num_cuenta}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                </td>
                                                <td width="200"><input type="text" class="form-control text-left" id="monto{$i.num_secuencia}codTransaccion" value="{number_format($i.num_monto,'2',',','.')}" name="form[int][codDetalle][{$i.num_secuencia}][num_monto]" {if isset($ver) and $ver==1} disabled {/if} ></td>
                                                <td width="250">
                                                    <select class="form-control select2-list select2" id="codReposicion{$i.num_secuencia}fk_cpb002_num_tipo_documento" name="form[int][codDetalle][{$i.num_secuencia}][fk_cpb002_num_tipo_documento]" {if isset($ver) and $ver==1} disabled {/if} >
                                                        <option value="">Seleccione...</option>
                                                        {foreach item=doc from=$documento}
                                                            {if isset($i.fk_cpb002_num_tipo_documento) and $i.fk_cpb002_num_tipo_documento == $doc.pk_num_tipo_documento}
                                                            <option value="{$doc.pk_num_tipo_documento}" selected>{$doc.ind_descripcion}</option>
                                                            {else}
                                                            <option value="{$doc.pk_num_tipo_documento}">{$doc.ind_descripcion}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                </td>
                                                <td width="150">
                                                    <input type="text" class="form-control text-left" id="codDetalle'{$i.num_secuencia}'docReferencia" value="{$i.ind_num_documento_referencia}" name="form[txt][codDetalleAlphaNum][{$i.num_secuencia}][ind_num_documento_referencia]" {if isset($ver) and $ver==1} disabled {/if}>
                                                </td>
                                                <td width="75">
                                                    <input type="text" class="form-control text-left accionModal" id="codDetalle{$i.num_secuencia}persona" value="{$i.fk_a003_num_persona_proveedor}" name="form[int][codDetalle][{$i.num_secuencia}][fk_a003_num_persona_proveedor]" {if isset($ver) and $ver==1} disabled {/if}
                                                           data-toggle="modal"
                                                           data-target="#formModal2"
                                                           titulo="Buscar Persona"
                                                           enlace="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/personaMET/transacciones/codDetalle{$i.num_secuencia}" >
                                                </td>
                                                <td width="75">
                                                    <input type="text" class="form-control text-left accionModal" id="codDetalle{$i.num_secuencia}centroCosto" value="{$i.fk_a023_num_centro_costo}" name="form[int][codDetalle][{$i.num_secuencia}][fk_a023_num_centro_costo]" {if isset($ver) and $ver==1} disabled {/if}
                                                    data-toggle="modal"
                                                    data-target="#formModal2"
                                                    titulo="Buscar Centro de Costo"
                                                    enlace="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/centroCostoMET/transacciones/codDetalle{$i.num_secuencia}" >
                                                </td>
                                                <td width="150">
                                                    <input type="text" class="form-control text-left accionModal" id="codDetalle{$i.num_secuencia}partida" value="{if isset($i.cod_partida)}{$i.cod_partida}{/if}"
                                                    data-toggle="modal"
                                                    data-target="#formModal2"
                                                    titulo="Buscar Partida"
                                                    enlace="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/partidasCuentasMET/PartidasTrans/codDetalle{$i.num_secuencia}" readonly>
                                                </td>
                                                <td width="35" style="text-align: center">
                                                    <button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="{$i.num_secuencia}" {if isset($ver) and $ver==1} disabled {/if} ><i class="md md-delete"></i></button>
                                                </td>
                                            </tr>
                                            {/foreach}
                                            {/if}
                                            </tbody>
                                        </table>

                                        <div class="col-sm-12" align="center">
                                                <div class="col-sm-12" align="center">
                                                    <button href="javascript: void(0);" onclick="agregar();" id="nuevoCampo" idTabla="detalleTransaccion" class="btn btn-info ink-reaction btn-raised" type="button" {if isset($ver) and $ver==1} disabled {/if} >
                                                        <i class="md md-add"></i> Agregar</button>
                                                </div>
                                        </div>
                                </div>
                            </div>
                </div>

                        </form>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro"
            data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>

    {if isset($estado)}
        {if $estado == 'NUEVO'}
            <button type="button" class="btn btn-primary logsUsuarioModal" id="ACCION">PROCESAR</button>
        {elseif $estado == 'MODIFICAR'}
                <button type="button" class="btn btn-primary logsUsuarioModal" id="ACCION">
                    <i class="fa fa-edit"></i>&nbsp;MODIFICAR</button>
        {elseif $estado == 'REVISAR'}
            <button type="button" class="btn btn-primary accionesEstado" id="RV">
                <i class="icm icm-rating"></i>&nbsp;REVISAR</button>
        {elseif $estado == 'ACTUALIZAR'}
            <button type="button" class="btn btn-primary accionesEstado" id="RECHAZAR">
                <i class="icm icm-blocked"></i>&nbsp;&nbsp;RECHAZAR</button>
            <button type="button" class="btn btn-primary accionesEstado" id="AP">
                <i class="icm icm-rotate2"></i>&nbsp;ACTUALIZAR</button>
        {elseif $estado == 'DESACTUALIZAR'}
            <button type="button" class="btn btn-danger accionesEstado" id="AN">
                <i class="icm icm-rotate"></i>&nbsp;DESACTUALIZAR</button>
        {/if}
    {/if}
</div>

<script type="text/javascript">


    function agregar(){
        var sec=$('#detalleTransaccion tr').length + 1;


        $(document.getElementById('detalleTransaccion')).append(
                '<tr id="codDetalle'+sec+'">'+
                '<input type="hidden" value="'+sec+'" name="form[int][codDetalle][num_secuencia]['+sec+']"> '+
                '<input type="hidden" id="codDetalle'+sec+'fk_cpb006_num_banco_tipo_transaccion" value="" name="form[int][codDetalle]['+sec+'][fk_cpb006_num_banco_tipo_transaccion]"> '+
                '<input type="hidden" id="codDetalle'+sec+'partida" value="" name="form[int][codDetalle]['+sec+'][fk_prb002_num_partida_presupuestaria]">'+

                '<td width="25"><input type="text" class="form-control text-right" id="codDetalle'+sec+'num_secuencia" value="'+sec+'" disabled ></td>'+
                    '<td width="100"><input type="text" class="form-control text-right" id="codDetalle'+sec+'codTransaccion" value="" disabled ></td>'+
                    '<td width="600">' +
                        '<input type="text" class="form-control text-right accionModal" id="codDetalle'+sec+'nombreTransaccion" value="" ' +
                        'data-toggle="modal"'+
                        'data-target="#formModal2"'+
                        'titulo="Buscar Tipo de Transacciones Bancarias"'+
                        'enlace="{$_Parametros.url}modCP/pagos/transaccionesBancariasCONTROL/tipoTransaccionesMET/transacciones/codDetalle'+sec+'" >' +
                    '</td>'+
                    '<td width="40"><input type="text" class="form-control text-right" id="codDetalle'+sec+'tipoTransaccion" value="" disabled></td>'+
                    '<td width="250">'+
                        '<select name="form[int][codDetalle]['+sec+'][fk_cpb014_num_cuenta]" class="form-control " data-placeholder="Seleccione Cuenta Bancaria" > ' +

                            '<option value="">Seleccione Cuenta Bancaria</option>}'+
                            {foreach item=proceso from=$listadoCuentas}
                            {if isset($i.pk_num_cuenta) and $i.pk_num_cuenta == $proceso.pk_num_cuenta }
                            '<option value="{$proceso.pk_num_cuenta}" selected >{$proceso.ind_num_cuenta}</option>'+
                            {else}
                            '<option value="{$proceso.pk_num_cuenta}">{$proceso.ind_num_cuenta}</option>'+
                            {/if}
                            {/foreach}
                        '</select>'+
                    '</td>'+
                    '<td width="200"><input type="text" class="form-control text-right" id="monto'+sec+'codTransaccion" value="0" name="form[int][codDetalle]['+sec+'][num_monto]"></td>'+
                    '<td width="250">'+
                        '<select class="form-control " id="codDetalle'+sec+'fk_cpb002_num_tipo_documento" name="form[int][codDetalle]['+sec+'][fk_cpb002_num_tipo_documento]">'+
                            {foreach item=doc from=$documento}
                            {if isset($i.pk_num_cuenta) and $i.pk_num_cuenta == $proceso.pk_num_cuenta }
                            '<option value="{$doc.pk_num_tipo_documento}" selected >{$doc.ind_descripcion}</option>'+
                            {else}
                            '<option value="{$doc.pk_num_tipo_documento}">{$doc.ind_descripcion}</option>'+
                            {/if}
                            {/foreach}
                        '</select>'+
                    '</td>'+
                    '<td width="150"><input type="text" class="form-control text-right" id="docReferencia'+sec+'docReferencia" value="" name="form[txt][codDetalleAlphaNum]['+sec+'][ind_num_documento_referencia]"></td>'+
                    '<td width="75">' +
                        '<input type="text" class="form-control text-right accionModal" id="codDetalle'+sec+'persona" value="" name="form[int][codDetalle]['+sec+'][fk_a003_num_persona_proveedor]"  '+
                        'data-toggle="modal"'+
                        'data-target="#formModal2"'+
                        'titulo="Buscar Persona"'+
                        'enlace="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/personaMET/transacciones/codDetalle'+sec+'" >' +
                    '</td>'+
                    '<td width="75">' +
                        '<input type="text" class="form-control text-right accionModal" id="codDetalle'+sec+'centroCosto" value="" name="form[int][codDetalle]['+sec+'][fk_a023_num_centro_costo]"' +
                        'data-toggle="modal"'+
                        'data-target="#formModal2"'+
                        'titulo="Buscar Centro de Costo"'+
                        'enlace="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/centroCostoMET/transacciones/codDetalle'+sec+'" >' +
                    '</td>'+
                    '<td width="150">' +
                        '<input type="text" class="form-control text-right accionModal" id="codDetalle'+sec+'codigo" value=""  '+
                        'data-toggle="modal"'+
                        'data-target="#formModal2"'+
                        'titulo="Buscar Partida"'+
                        'enlace="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/partidasCuentasMET/PartidasTrans/codDetalle'+sec+'" readonly >' +
                    '</td>'+
                    '<td width="35" style="text-align: center">'+
                    '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="'+sec+'"><i class="md md-delete"></i></button>' +
                    '</td>'+
                '</tr>'
        );
        if($('#num_flag_presupuesto').is(':checked')){
            document.getElementById('codDetalle' + sec + 'codigo').disabled = false;
        }else{
            document.getElementById('codDetalle' + sec + 'codigo').disabled = true;
        }

    }

</script>

<script type="text/javascript">

    $(document).ready(function () {
        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});

        var app = new AppFunciones();
        /// Complementos
        $('.select2').select2({ allowClear: true});
        // anular accion del Formulario
        $("#formAjax").submit(function () {
            return false;
        });

        // ancho de la Modal
        $('#modalAncho').css("width", "88%");

        $('#ACCION').click(function () {
           /* swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });*/
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (dato) {
                var arrayCheck = ['num_flag_presupuesto'];
                var arrayMostrarOrden = ['ind_num_transaccion', 'num_secuencia', 'fec_transaccion', 'decripcionTransaccion', 'num_monto', 'cuentaBancaria', 'ind_periodo_contable', '', 'ind_estado', '', 'txt_comentarios'];
                if (dato['status'] == 'error') {
                    app.metValidarError(dato, 'Disculpa. los campos marcados con X en rojo son obligatorios');
                } else if (dato['status'] == 'errorSQL') {
                    app.metValidarError(dato, 'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                } else if (dato['status'] == 'errorCodDetalle') {
                    swal('ERROR!','Disculpa. Debe ingresar por lo menos una transacción','error');
                }else if (dato['status'] == 'errorDetalle') {
                    swal('ERROR!','Disculpa. Debe ingresar el detalle de la transacción','error');
                } else if (dato['status'] == 'modificar') {
                    app.metActualizarRegistroTabla(dato, dato['idTransaccion'], 'idTransaccion', arrayCheck, arrayMostrarOrden, 'La Transaccion Bancaria fue modificada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                } else if (dato['status'] == 'nuevo') {
                    app.metNuevoRegistroTabla(dato, dato['idTransaccion'], 'idTransaccion', arrayCheck, arrayMostrarOrden, 'La Transaccion Bancaria fue guardada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });

        {if isset($estado)}
        $('.accionesEstado').click(function () {
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post(
                    $("#formAjax").attr("action"),
                    { idTransaccion: $('#idTransaccion').val(), estado: $(this).attr('id'), motivo: $('#txt_comentario_anulacion').val()},
                    function(dato){
                        if (dato['status'] == 'errorSQL') {
                            app.metValidarError(dato, 'Disculpa. Contacta Con la Direccion Tecnica porque hay un ERROR interno');
                        } else if (dato['status'] == 'noDispFinanciera') {
                            swal('Error','Disculpa. No hay Disponibilidad Financiera para Desactualizar la Transaccion Bancaria', 'error');
                        }if (dato['status'] == 'error') {
                            app.metValidarError(dato, 'Disculpa. los campos marcados con X en rojo son obligatorios');
                        } else if (dato['status'] == 'OK') {
                            if(dato['Mensaje']['titulo'] == 'ACTUALIZADA'){
                                if (dato['num_flag_voucher'] == 1) {
                                    //vouchers de provision
                                    var titulo = 'GENERACION DE VOUCHERS DE TRANSACCIONES BANCARIAS - '+dato['contabilidad'];
                                    swal({
                                        type: "success",
                                        title: dato['Mensaje']['titulo'] + '!!',
                                        text: dato['Mensaje']['contenido'],
                                        timer: 2000,
                                        showConfirmButton: false
                                    });
                                    setTimeout(function () {
                                        $('#formModal2').modal('toggle');
                                        $('#modalAncho2').css("width", "80%");
                                        $('#formModalLabel2').html(titulo);
                                        var idTransaccion = dato['idTransaccion'];
                                        $.post('{$_Parametros.url}modCP/generacionVouchers/transaccionesCONTROL/generarVouchersMET', {
                                            idTransaccion: idTransaccion
                                        }, function (dato) {
                                            $('#ContenidoModal2').html(dato);
                                        });
                                    }, 2000);
                                }else{
                                    swal(dato['Mensaje']['titulo']+'!!', dato['Mensaje']['contenido'], "success");
                                }
                            }
                            else if (dato['Mensaje']['titulo'] == 'DESACTUALIZADA') {
                                if (dato['fk_cbb001_num_voucher'] != null) {
                                    swal({
                                        type: "success",
                                        title: dato['Mensaje']['titulo'] + '!!',
                                        text: dato['Mensaje']['contenido'],
                                        timer: 2000,
                                        showConfirmButton: false
                                    });
                                    setTimeout(function () {
                                        $('#formModal2').modal('toggle');
                                        $('#modalAncho2').css("width", "80%");
                                        $('#formModalLabel2').html('VOUCHERS');
                                        var idTransaccion = dato['idTransaccion'];
                                        $.post('{$_Parametros.url}modCP/pagos/transaccionesBancariasCONTROL/vouchersMET', {
                                            idTransaccion: idTransaccion
                                        }, function (dato) {
                                            $('#ContenidoModal2').html(dato);
                                        });
                                    }, 2000);
                                }else{
                                    swal(dato['Mensaje']['titulo']+'!!', dato['Mensaje']['contenido'], "success");
                                }
                            }else{
                                swal(dato['Mensaje']['titulo']+'!!', dato['Mensaje']['contenido'], "success");
                            }
                            $(document.getElementById('idTransaccion'+dato['idTransaccion'])).remove();
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');
                        }
                    },'JSON');
        });
        {/if}

        $('#detalleTransaccion').on('click', '.accionModal', function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('enlace'), { cargar: 0,
                tr: $("#" + $(this).attr('idTabla') + " > tbody > tr").length + 1
            }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

        $('#detalleTransaccion').on('click', '.delete', function () {
            var campo = $(this).attr('id');
            $('#codDetalle' + campo).remove();
        });


    });

</script>
