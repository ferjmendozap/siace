
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary"><h2 class="text-primary">{if $lista =='PR'} REVISAR {elseif $lista == 'RE'}ACTUALIZAR
                {elseif $lista == 'AP'}DESACTUALIZAR {else}LISTA DE {/if}TRANSACCIONES BANCARIAS</h2>
    </div>
        <div class="section-body contain-lg" id="listadoEstado">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>NUMERO</th>
                            <th>#</th>
                            <th>FECHA</th>
                            <th>TRANSACCIÓN</th>
                            <th>MONTO</th>
                            <th>CUENTA BANCARIA</th>
                            <th>PERIODO</th>
                            <th>VOUCHER</th>
                            <th>ESTADO</th>
                            <th>CHEQUE</th>
                            <th>COMENTARIOS</th>
                            <th width="120">ACCIONES</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$dataBD}
                            <tr id="idTransaccion{$i.pk_num_banco_transaccion}">
                                <td>{$i.ind_num_transaccion}</td>
                                <td>{$i.num_secuencia}</td>
                                <td>{$i.fec_transaccion}</td>
                                <td>{$i.decripcionTransaccion}</td>
                                <td>{number_format($i.num_monto,'2',',','.')}</td>
                                <td>{$i.cuentaBancaria}</td>
                                <td>{$i.ind_periodo_contable}</td>
                                <td>{$i.fk_cbb001_num_voucher_pago}</td>
                                <td>{$i.ind_estado}</td>
                                <td>{$i.ind_num_pago}</td>
                                <td>{$i.txt_comentarios}</td>
                                <td width="120">
                                    {if $lista == 'PR' }
                                        <button class="acciones logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" id="REVISAR" idTransaccion="{$i.pk_num_banco_transaccion}" title="Revisar"
                                                descipcion="El Usuario ha Revisado una transaccion" titulo="<i class='icm icm-rating'></i> Revisar Transaccion Bancaria">
                                            <i class="icm icm-rating" style="color: #ffffff;"></i>
                                        </button
                                    {elseif $lista == 'RE' }
                                        <button class="acciones logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" id="ACTUALIZAR" idTransaccion="{$i.pk_num_banco_transaccion}" title="Actualizar"
                                                descipcion="El Usuario ha Actualizar una transaccion" titulo="<i class='icm icm-rotate2'></i> Actualizar Transaccion Bancaria">
                                            <i class="icm icm-rotate2" style="color: #ffffff;"></i>
                                        </button>
                                    {elseif $lista ==  'AP'}
                                        <button class="acciones btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" id="DESACTUALIZAR" idTransaccion="{$i.pk_num_banco_transaccion}" title="Desactualizar"
                                                descipcion="El Usuario ha Desactualizar un Transaccion Bancaria" titulo="<i class='icm icm-rotate'></i> Desactualizar Transaccion Bancaria">
                                            <i class="icm icm-rotate" style="color: #ffffff;"></i>
                                        </button>
                                    {/if}
                                    {if $i.ind_estado != 'REVISADO' AND $i.ind_estado != 'ACTUALIZADO' AND $i.ind_estado != 'CONTABILIZADO' }
                                            <button class="acciones logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static"  id="MODIFICAR" idTransaccion="{$i.pk_num_banco_transaccion}" title="Editar"
                                                    descipcion="El Usuario ha Modificado una transaccion" titulo="<i class='fa fa-edit'></i> Editar Transaccion Bancaria">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                    {/if}
                                    <button class="acciones logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static"  id="VER" idTransaccion="{$i.pk_num_banco_transaccion}" title="Consultar"
                                            descipcion="El Usuario esta viendo una transaccion" titulo="<i class='md md-remove-red-eye'></i> Consultar Transaccion Bancaria">
                                        <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                    </button>
                                    <button class="imprimir logsUsuario btn ink-reaction btn-raised btn-xs btn-success" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" idTransaccion="{$i.pk_num_banco_transaccion}" title="Imprimir"
                                            descipcion="El Usuario esta Imprimiendo una Transaccion Bancaria" titulo="<i class='md md-print'></i> Imprimir Transaccion Bancaria">
                                        <i class="md md-print" style="color: #ffffff;"></i>
                                    </button>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        {if $lista==''}
                        <tr>
                            <th colspan="12">
                                <button class="nuevo logsUsuario btn ink-reaction btn-raised btn-info"
                                        descipcion="el Usuario ha creado un post de Transaccion Bancaria"
                                        data-toggle="modal" data-target="#formModal"
                                        titulo="<i class='md md-create'></i> Registrar Transaccion Bancaria" id="NUEVO"
                                        data-keyboard="false" data-backdrop="static">
                                    <i class="md md-create"></i>&nbsp;Nueva Transaccion Bancaria
                                </button>
                            </th>
                        </tr>
                        {/if}
                        </tfoot>

                    </table>
                </div>
            </div>

<script type="text/javascript">

    $(document).ready(function() {

        var url='{$_Parametros.url}modCP/pagos/transaccionesBancariasCONTROL/accionesMET/';

        $('.nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idTransaccion:0,estado: $(this).attr('id')  },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.acciones', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idTransaccion: $(this).attr('idTransaccion'), estado: $(this).attr('id') },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.imprimir', function () {
            var idTransaccion = $(this).attr('idTransaccion');
            var url='{$_Parametros.url}modCP/pagos/transaccionesBancariasCONTROL/imprimirMET/?idTransaccion='+idTransaccion;
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html('<iframe frameborder="0" src="'+url+'" width="100%" height="540px"></iframe>');

        });

        $('.buscar').click(function(){

            var url='{$_Parametros.url}modCP/pagos/transaccionesBancariasCONTROL/actualizarMET/';
            $.post(url,{ estado: $('#estados').val() },function(dato){
                $('#listadoEstado').html('');
                $('#listadoEstado').html(dato);
               // $('#datatable1 tbody').append(dato);

            });

        });

    });
</script>