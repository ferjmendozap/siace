
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">IMPRIMIR / TRANSFERIR PAGOS</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>PREPAGO</th>
                            <th>BANCO</th>
                            <th>CUENTA BANCARIA</th>
                            <th>TIPO PAGO</th>
                            <th>MONTO A PAGAR</th>
                            <th>FECHA PAGO</th>
                            <th># ORDEN DE PAGO</th>
                            <th>ACCIONES</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$dataBD}
                            <tr id="idPago{$i.pk_num_pago}">
                                <td>{$i.ind_num_pago}</td>
                                <td>{$i.a006_miscelaneo_detalle_banco}</td>
                                <td>{$i.ind_num_cuenta}</td>
                                <td>{$i.a006_miscelaneo_detalle_tipoPago}</td>
                                <td>{$i.num_monto_pago}</td>
                                <td></td>
                                <td>{$i.ind_num_orden}</td>
                                <td>
                                    {if in_array('CP-01-03-03-01-IT',$_Parametros.perfil)}
                                    <button class="imprimirTransferir btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" id="PR" data-backdrop="static" idPago="{$i.pk_num_pago}" title="Imprimir/Transferir"
                                            descipcion="El Usuario " titulo="<i class='icm icm-print3'></i> Imprimir/Transferir">
                                        <i class="icm icm-print3" style="color: #ffffff;"></i>
                                    </button>
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
                <div class="card-head card-head-xs style-primary text-center">
                    <header class="text-center">Documentos a Pagar del Prepago</header>
                </div>
                <div class="table-responsive">
                    <table id="tablaPost" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>PAGAR A</th>
                            <th>PROVEEDOR</th>
                            <th>MONTO TOTAL</th>
                            <th>MONTO RETENIDO</th>
                            <th>NETO A PAGAR</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div></section>


<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modCP/pagos/imprimirTransferirCONTROL/imprimirTransferirMET';

        $('#datatable1 tbody').on( 'click', '.imprimirTransferir', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idPago: $(this).attr('idPago') , ver:1 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', 'tr', function () {
            var idPago=$(this).attr('id');
            $.post($url,{ idPago: idPago , verPago:1 },function(dato){
                $('#tablaPost tbody').html('');
                $('#tablaPost tbody').append(
                    '<tr>'+
                        '<td>'+dato['fk_a003_num_persona_proveedor_a_pagar']+'</td>'+
                        '<td>'+dato['fk_a003_num_persona_proveedor']+'</td>'+
                        '<td>'+dato['montoTotal']+'</td>'+
                        '<td>'+dato['num_monto_retenido']+'</td>'+
                        '<td>'+dato['num_monto_pago']+'</td>'+
                    '</tr>'
                );
            },'json');
        });
        
    });
</script>