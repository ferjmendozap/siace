<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body ">
                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                        <form action="{$_Parametros.url}modCP/pagos/imprimirTransferirCONTROL/imprimirTransferirMET" id="formAjax"
                              class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                            <input type="hidden" value="1" name="valido"/>
                            <input type="hidden" value="{$idPago}" id="idPago" name="idPago"/>
                            <div class="tab-content clearfix">
                                <div class="tab-pane active" id="informacionGeneral">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-head card-head-xs style-primary text-center">
                                                    <header class="text-center">Información del Prepago</header>
                                                </div>
                                                <div class="card-body" style="padding: 4px;">
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="proveedor"
                                                                       class="control-label" style="margin-top: 10px;"> Fecha del Pago:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       id="proveedor"
                                                                       style="font-weight:bold;"
                                                                       value="{if isset($formDB.fec_pago)}{$formDB.fec_pago}{/if}"
                                                                       readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="proveedor"
                                                                       class="control-label" style="margin-top: 10px;"> Nro. Pre-Pago:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       id="proveedor"
                                                                       style="font-weight:bold;"
                                                                       value="{if isset($formDB.ind_num_pago)}{$formDB.ind_num_pago}{/if}"
                                                                       readonly>
                                                            </div>
                                                        </div>
                                                        {if $formDB.a006_miscelaneo_detalle_tipoPago == 'CHEQUE' }
                                                            <div class="col-sm-6" id="ind_cheque_usuarioError">
                                                                <div class="col-sm-3 text-right">
                                                                    <label for="ind_cheque_usuario "
                                                                           class="control-label" style="margin-top: 10px;"> Nro. Cheque:</label>
                                                                </div>
                                                                <div class="col-sm-7">
                                                                    <input type="text" class="form-control"
                                                                           id="ind_cheque_usuario"
                                                                           name="ind_cheque_usuario"
                                                                           style="font-weight:bold;"
                                                                           value="">
                                                                </div>
                                                            </div>
                                                        {/if}
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="pk_num_tipo_documento"
                                                                       class="control-label" style="margin-top: 10px;"> Tipo de Pago:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       id="a006_miscelaneo_detalle_tipoPago"
                                                                       name="a006_miscelaneo_detalle_tipoPago"
                                                                       style="font-weight:bold;"
                                                                       value="{if isset($formDB.a006_miscelaneo_detalle_tipoPago)}{$formDB.a006_miscelaneo_detalle_tipoPago}{/if}"
                                                                       readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="pk_num_tipo_documento"
                                                                       class="control-label" style="margin-top: 10px;"> Monto a Pagar:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       id="proveedor"
                                                                       style="font-weight:bold;"
                                                                       value="{if isset($formDB.num_monto_pago)}{$formDB.num_monto_pago}{/if}"
                                                                       readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-3">
                                                            <label for="pk_num_tipo_documento"
                                                                   class="control-label" style="margin-top: 10px;"> Cta. Bancaria:</label>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control"
                                                                   id="proveedor"
                                                                   style="font-weight:bold;"
                                                                   value="{if isset($formDB.a006_miscelaneo_detalle_banco)}{$formDB.a006_miscelaneo_detalle_banco}{/if}"
                                                                   readonly>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control"
                                                                   id="proveedor"
                                                                   style="font-weight:bold;"
                                                                   value="{if isset($formDB.ind_num_cuenta)}{$formDB.ind_num_cuenta}{/if}"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-head card-head-xs style-primary text-center">
                                                    <header class="text-center">Documentos a Pagar del Prepago</header>
                                                </div>
                                                <div class="table-responsive">
                                                    <table id="datatable1" class="table table-striped table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th>PAGAR A</th>
                                                            <th>PROVEEDOR</th>
                                                            <th>MONTO TOTAL</th>
                                                            <th>MONTO RETENIDO</th>
                                                            <th>NETO A PAGAR</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>{if isset($formDB.fk_a003_num_persona_proveedor_a_pagar)}{$formDB.fk_a003_num_persona_proveedor_a_pagar}{/if}</td>
                                                            <td>{if isset($formDB.fk_a003_num_persona_proveedor)}{$formDB.fk_a003_num_persona_proveedor}{/if}</td>
                                                            <td>{if isset($formDB.montoTotal)}{$formDB.montoTotal}{/if}</td>
                                                            <td>{if isset($formDB.num_monto_retenido)}{$formDB.num_monto_retenido}{/if}</td>
                                                            <td>{if isset($formDB.num_monto_pago)}{$formDB.num_monto_pago}{/if}</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group floating-label">
                                                    <input type="text" disabled class="form-control disabled"
                                                           value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}"
                                                           id="ind_usuario">
                                                    <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="col-sm-12">
                                            <div class="form-group floating-label" id="cod_tipo_nominaError">
                                                <input type="text" disabled class="form-control disabled"
                                                       value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}"
                                                       id="fec_ultima_modificacion">
                                                <label for="cod_tipo_nomina"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro"
            data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
    <button type="button" class="btn btn-primary ink-reaction logsUsuarioModal" id="accion">
            <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();
        // anular accion del Formulario
        $("#formAjax").submit(function () {
            return false;
        });
        // ancho de la Modal
        $('#modalAncho').css("width", "75%");

        //Guardado y modales
        $('#accion').click(function () {
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (dato) {
                if (dato['status'] == 'errorSQL') {
                    app.metValidarError(dato, 'Disculpe. Contacta Con la Direccion Tecnica porque hay un ERROR interno');
                } else if(dato['status']=='errorCheque'){
                    app.metValidarError(dato,'Disculpe. Debe ingresar el número de Cheque');
                }  else if(dato['status']=='errorNroCheque'){
                    app.metValidarError(dato,'Disculpe. El número de Cheque no coincide con el sistema');
                }else if(dato['status']=='errorNroChequeNO'){
                    swal('ERROR','Disculpe. No existe chequera activa para generar el cheque','error');
                }  else if (dato['status'] == 'OK') {
                    //vouchers de pago
                    if(dato['contabilidad'] == 'ONCOP'){
                        var voucher = 'ONCOP';
                    }else{
                        var voucher = 'PUB20';
                    }

                    $('#tablaPost tbody').html('');
                    $(document.getElementById('idPago'+dato['idPago'])).remove();
                    swal({  type: "success", title: dato['Mensaje']['titulo']+'!!',  text: dato['Mensaje']['contenido'],   timer: 2000,   showConfirmButton: false });
                    $(document.getElementById('cerrarModal')).click();
                    setTimeout(function(){
                        $('#formModal2').modal('toggle');
                        $('#modalAncho2').css("width", "80%");
                        $('#formModalLabel2').html('GENERACION DE VOUCHERS DE PAGO - '+dato['contabilidad']);
                        var idPago = dato['idPago'];
                        var tipoPago = dato['tipoPago'];
                        var nroPagoParcial = dato['nroPagoParcial'];
                        $.post('{$_Parametros.url}modCP/pagos/imprimirTransferirCONTROL/vouchersMET',{ idPago:idPago, voucher:voucher, tipoPago:tipoPago, nroPagoParcial:nroPagoParcial }, function (dato) {
                            $('#ContenidoModal2').html(dato);
                        });
                    }, 2000);

                }
            }, 'json');
        });

    });

</script>