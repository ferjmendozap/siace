<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">
            <div class="col-sm-11">Facturación de Logística</div>
        </h2>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="col-sm-12">
                <div class="col-sm-6">
                    <div class="col-sm-2 text-right">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkProveedor">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="pk_num_tipo_documento"
                               class="control-label" style="margin-top: 10px;"> Proveedor:</label>
                    </div>
                    <div class="col-sm-2">
                        <input type="text"
                               name="form[int][fk_a003_num_persona_proveedor]"
                               class="form-control" id="codigoProveedor"
                               value=""
                               readonly size="9%">
                    </div>
                    <div class="col-sm-6">
                        <div class="col-sm-10">
                            <input type="text" class="form-control"
                                   id="nombreProveedor"
                                   value=""
                                   disabled>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group floating-label">
                                <button
                                        class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                        type="button"
                                        data-toggle="modal"
                                        data-target="#formModal2"
                                        titulo="Listado de Personas"
                                        id="botonPersona"
                                        disabled="disabled"
                                        url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/personaMET/persona/">
                                    <i class="md md-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="col-sm-2 text-right">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkBuscar">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="pago"
                               class="control-label" style="margin-top: 10px;"> Buscar:</label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control text-center date"
                               id="buscarF"
                               style="text-align: center"
                               value=""
                                disabled="disabled">
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-6">
                    <div class="col-sm-2 text-right">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkClasif">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="cuenta"
                               class="control-label" style="margin-top: 10px;"> Clasificación:</label>
                    </div>
                    <div class="col-sm-8">
                        <select class="form-control select2"
                                disabled="disabled"
                                id="clasif"
                                data-placeholder="Seleccione Cuenta Bancaria">
                            <option value="">Seleccione...</option>}
                            {foreach item=i from=$clasifDoc}
                                <option value="{$i.ind_documento_clasificacion}">{$i.ind_descripcion}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="col-sm-2 text-right">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkFPago">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label for="cuenta"
                               class="control-label" style="margin-top: 10px;"> Forma de Pago:</label>
                    </div>
                    <div class="col-sm-7">
                        <select class="form-control select2"
                                disabled="disabled"
                                id="pago"
                                data-placeholder="Seleccione Cuenta Bancaria">
                            <option value="">Seleccione...</option>}
                            {foreach item=proceso from=$listadoCuentas}
                                <option value="{$proceso.pk_num_cuenta}">{$proceso.ind_num_cuenta}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
            </div>

            <div align="center">
                <button class="buscar logsUsuario btn ink-reaction btn-raised btn-primary"
                        id="buscar" idTipo="pagoOblig">BUSCAR
                </button>
            </div>
        </div>
    </div>
    <div class="section-body contain-lg">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>DOC. INTERNO</th>
                            <th>FECHA</th>
                            <th>NRO. OC/OS</th>
                            <th>ALMACEN</th>
                            <th>MONTO AFECTO</th>
                            <th>MONTO NO AFECTO</th>
                            <th>IMPUESTOS</th>
                            <th>TOTAL</th>
                            <th>FORMA DE PAGO</th>
                        </tr>
                        </thead>
                        <tbody id="proveedores">
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="9" >
                                <button class="accion Obligacion btn ink-reaction btn-raised btn-primary" data-toggle="modal" data-target="#formModal"
                                    data-keyboard="false" id="factura" data-backdrop="static"  title="Preparar Factura" disabled="disabled"
                                    descipcion="el Usuario ha creado un post de Obligacion" titulo="Preparar Factura" >
                                    <i class="icm icm-spinner12" style="color: #ffffff;"></i> Preparar Factura
                                </button>
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div></section>
<script type="text/javascript">
    $(document).ready(function() {
        ///acciones
        $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });

        //habilitar y deshabilitar campos del filtro
        $('#checkProveedor').click(function () {
            if(this.checked){
                $('#botonPersona').attr('disabled', false);
            }else{
                $('#botonPersona').attr('disabled', true);
                $(document.getElementById('codigoProveedor')).val("");
                $(document.getElementById('nombreProveedor')).val("");

            }
        });
        $('#checkBuscar').click(function () {
            if(this.checked){
                $('#buscarF').attr('disabled', false);
            }else{
                $('#buscarF').attr('disabled', true);
                $(document.getElementById('buscarF')).val("");

            }
        });
        $('#checkClasif').click(function () {
            if(this.checked){
                $('#clasif').attr('disabled', false);
            }else{
                $('#clasif').attr('disabled', true);
                $(document.getElementById('clasif')).val("");

            }
        });
        $('#checkFPago').click(function () {
            if(this.checked){
                $('#pago').attr('disabled', false);
            }else{
                $('#pago').attr('disabled', true);
                $(document.getElementById('pago')).val("");

            }
        });

        //busqueda segun el filtro
        $('.buscar').click(function () {
            var proveedor = $('#codigoProveedor').val();
            var clasificacion = $('#clasif').val();

            if(proveedor == ""){
                swal("Error!", "Debe seleccionar un Proveedor", "error");
            }else {
                var url = '{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/facturacionLgMET/';
                $.post(url,{ proveedor:proveedor,clasificacion:clasificacion },function(dato) {
                    if (dato) {
                        $('#proveedores').html('');
                        for(var i= 0; i < dato.length; i++) {
                            $(document.getElementById('proveedores')).append(
                                    '<tr id="'+dato[i]['pk_num_documento']+'" style=""> '+
                                    '<td>-</td> ' +
                                    '<td>'+dato[i]['fec_documento']+'</td> ' +
                                    '<td>'+dato[i]['ind_orden']+'</td> ' +
                                    '<td>'+dato[i]['cod_almacen']+'</td> ' +
                                    '<td>'+dato[i]['num_monto_afecto']+'</td> ' +
                                    '<td>'+dato[i]['num_monto_no_afecto']+'</td> ' +
                                    '<td>'+dato[i]['num_monto_impuesto']+'</td> ' +
                                    '<td>'+dato[i]['num_monto_total']+'</td> ' +
                                    '<td>'+dato[i]['ind_nombre_detalle']+'</td> ' +
                                    '<td> ' +
                                    '</tr>'
                            );
                        }
                    }
                }, 'json');
            }
        });

        //funcion para seleccionar registro del listado
        $("#datatable1 tbody").on('click', 'tr', function() {
            var idDocumento = $(this).attr('id');
            var cantidad = 0;
            if(idDocumento){
                var style = $(this).attr('style');
                if(style == null || style == ''){
                    $(this).attr('style','background-color:LightGreen;');
                    $(this).attr('class','cambio');
                } else {
                    $(this).attr('style','');
                    $(this).attr('class','');
                }
                $('#datatable1 .cambio').each(function(){
                    cantidad = cantidad+1;
                });
                if(cantidad==0){
                    $('#factura').attr('disabled',true);
                } else {
                    $('#factura').attr('disabled',false);
                }
            }
        });

        $('.accion').on('click', function () {
            var idProveedor = $('#codigoProveedor').val();
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post('{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/facturaLGMET',{
                idProveedor: idProveedor
            },function(dato){
                $('#ContenidoModal').html(dato);
            });
            //TIPO DE DOCUMENTO - busco los tipos de documentos para el proveedor seleccionado
            $.post('{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/listasDocumentoServicioMET/documento', {
                idPersona: idProveedor
            }, function (dato) {
                if(dato.length > 0){
                    $('#fk_cpb002_num_tipo_documento').html('');
                    //muestro el listado de los tipos de documentos del proveedor
                    for(var i=0; i < dato.length; i++){
                        $('#fk_cpb002_num_tipo_documento').append('<option value="'+dato[i]['pk_num_tipo_documento']+'">'+dato[i]['ind_descripcion']+'</option>');
                        //selecciono el primero de la lista por defecto
                        $('#s2id_fk_cpb002_num_tipo_documento .select2-chosen').html(dato[0]['ind_descripcion']);
                    }
                }
            },'json');

            var sec = 1;
            //insertar datos por cada registro seleccionado del listado
            $('#datatable1 .cambio').each(function(){
                var idDocumento = $(this).attr('id');
                $.post('{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/documentoFacturaLGMET/',{
                    idDocumento: idDocumento },function(respuesta) {
                    var dato =  respuesta['documento'];
                    if (dato) {
                        if (dato['num_tipo_documento'] == 'OC'){
                            var clasificacion = 'O. Compra';
                        }else{
                            var clasificacion = 'O. Servicio';
                        }
                        $(document.getElementById('documentos')).append(
                                '<tr id="idDocumento'+dato['fk_lgb019_num_orden']+'" idOrden="'+dato['fk_lgb019_num_orden']+'" class="documentos">' +
                                '<input type="hidden" value="'+sec+'" name="form[int][documentos][ind_secuencia]['+sec+']"> ' +
                                '<input type="hidden" id="codDocumento'+sec+'idOrden" value="' + dato['fk_lgb019_num_orden'] + '" name="form[int][documentos]['+sec+'][fk_lgb019_num_orden]"> ' +
                                '<td width="35"><input type="text" class="form-control text-center" value="'+sec+'" name="form[int][ind_secuencia'+sec+']" disabled="disabled" ></td>' +
                                '<td width="150"><input type="text" class="form-control text-center" id="clasificacion" disabled="disabled" value="'+clasificacion+'"  ></td>' +
                                '<td width="150"><input type="text" class="form-control text-center" id="nroDoc" value="DOC-REF" readonly></td>' +
                                '<td width="150"><input type="text" class="form-control text-center" id="fecha" value="'+dato['fec_documento']+'" readonly></td>' +
                                '<td width="120"><input type="text" class="form-control text-center" id="num_monto_total" value="'+dato['num_monto_total']+'" readonly></td>' +
                                '<td width="120"><input type="text" class="form-control text-center" id="num_monto_afecto" value="'+dato['num_monto_afecto']+'" readonly></td>' +
                                '<td width="120"><input type="text" class="form-control text-center" id="monto_impuesto" value="'+dato['num_monto_impuesto']+'" readonly></td>' +
                                '<td width="120"><input type="text" class="form-control text-center" id="num_monto_no_afecto" value="'+dato['num_monto_no_afecto']+'" readonly></td>' +
                                '<td width="450"><input type="text" class="form-control text-center" id="comentario" value="'+dato['pk_num_orden_detalle']+'" readonly ></td>' +
                                '<td id="' + dato['fk_lgb019_num_orden'] + '" width="35" class="text-center" style="vertical-align: middle;">' +
                                '<input type="hidden" value="' + dato['fk_lgb019_num_orden'] + '" name="form[int][cant]">' +
                                '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="' + dato['fk_lgb019_num_orden'] + '">' +
                                '<i class="md md-delete"></i></button></td>' +
                                '</tr>'
                        );
                        sec = parseFloat(sec) + 1;
                        var tr = $('#partidasCuentas > tbody > tr').length + 1;

//                      $('#partidasCuentas > tbody').html('');
                        var idOrden = $('#documentos tr').attr('idOrden');
                        var tr = $('#partidasCuentas > tbody > tr').length + 1;
                        var secuencia =  tr-1;
                        var partida = $('#codCuentas'+secuencia+'PARTIDA').val();

                        var dato = respuesta['ordenDetalle'];
                        for (var i = 0; i < dato.length; i++) {
                            var persona = $(document.getElementById('codigoProveedor')).val();
                            var flag = dato[i]['num_flag_exonerado'];
                            if (flag == 1) {
                                var flag = 'checked';
                            } else {
                                var flag = '';
                            }
                            if (partida != dato[i]['fk_prb002_partida_presupuestaria']) {
                                partida = dato[i]['fk_prb002_partida_presupuestaria'];

                                $(document.getElementById('partidasCuentas')).append(
                                        '<tr id="codCuentas' + tr + '">' +
                                        '<input type="hidden" class="ajaxPartidas" value="' + tr + '" name="form[int][partidaCuenta][ind_secuencia][' + tr + ']"> ' +
                                        '<input type="hidden" class="ajaxPartidas" id="codCuentas' + tr + 'CUENTA" value="' + dato[i]['fk_cbb004_plan_cuenta_oncop'] + '" name="form[int][partidaCuenta][' + tr + '][fk_cbb004_num_cuenta]"> ' +
                                        '<input type="hidden" class="ajaxPartidas" id="codCuentas' + tr + 'CUENTA20" value="' + dato[i]['fk_cbb004_plan_cuenta_pub_veinte'] + '" name="form[int][partidaCuenta][' + tr + '][fk_cbb004_num_cuenta_pub20]"> ' +
                                        '<input type="hidden" class="ajaxPartidas" id="codCuentas' + tr + 'PARTIDA" value="' + partida + '" name="form[int][partidaCuenta][' + tr + '][fk_prb002_num_partida_presupuestaria]"> ' +
                                        '<input type="hidden" class="ajaxPartidas" id="codCuentas' + tr + 'CC" value="' + dato[i]['fk_a023_num_centro_costo'] + '" name="form[int][partidaCuenta][' + tr + '][fk_a023_num_centro_costo]"> ' +
                                        '<input type="hidden" class="ajaxPartidas" id="codCuentas' + tr + 'PERSONA" value="' + persona + '" name="form[int][partidaCuenta][' + tr + '][fk_a003_num_persona_proveedor]"> ' +
                                        '<input type="hidden" class="ajaxPartidas" id="codCuentas' + tr + 'DESCRIPCION" value="" name="form[int][partidaCuenta][' + tr + '][ind_descripcion]"> ' +
                                        '<td width="35" style="vertical-align: middle;"><input type="text"  class="form-control text-center" value="' + tr + '" readonly ></td>' +
                                        '<td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas' + tr + 'PARTIDA_C" value="' + dato[i]['cod_partida'] + '" readonly></td>' +
                                        '<td width="250" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas' + tr + 'PARTIDA_D" value="' + dato[i]['ind_denominacion'] + '" readonly></td>' +
                                        '<td width="150">' +
                                        '<input type="text" class="form-control text-center" id="codCuentas' + tr + 'CUENTA_C" value="' + dato[i]['cod_cuenta'] + '" readonly>' +
                                        '<input type="text" class="form-control text-center" id="codCuentas' + tr + 'CUENTA20_C" value="' + dato[i]['cod_cuenta20'] + '" readonly>' +
                                        '</td>' +
                                        '<td width="250">' +
                                        '<input type="text" class="form-control text-center" id="codCuentas' + tr + 'CUENTA_D" value="' + dato[i]['ind_descripcion'] + '" readonly>' +
                                        '<input type="text" class="form-control text-center" id="codCuentas' + tr + 'CUENTA20_D" value="' + dato[i]['ind_descripcion20'] + '" readonly>' +
                                        '</td>' +
                                        '<td width="65" style="vertical-align: middle;">' +
                                        '<input type="text" class="form-control text-center accionModal" id="codCuentas' + tr + 'CC_C" value="' + dato[i]['fk_a023_num_centro_costo'] + '" ' +
                                        'data-toggle="modal"' +
                                        'data-target="#formModal2"' +
                                        'titulo="Listado Centro de Costo"' +
                                        'enlace="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/centroCostoMET/partida/codCuentas' + tr + '" readonly>' +
                                        '</td>' +
                                        '<td width="60" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas' + tr + 'PERSONA_C" value="' + persona + '" readonly></td>' +
                                        '<td width="40" style="vertical-align: middle;">' +
                                        '<div class="checkbox checkbox-styled">' +
                                        '<label>' +
                                        '<input type="checkbox" id="codCuentas' + tr + 'FLAG_NO_AFECTO" name="form[int][partidaCuenta][' + tr + '][num_flag_no_afecto]" value="1" ' + flag + '>' +
                                        '<span></span>' +
                                        '</label>' +
                                        '</div>' +
                                        '</td>' +
                                        '<td width="160" style="vertical-align: middle;"><input type="text" class="form-control text-right montoDistribucion ajaxPartidas" id="codCuentas' + dato[i]['fk_prb002_partida_presupuestaria'] + 'monto" idTR="codCuentas' + tr + '" value="' + dato[i]['num_monto_base'] + '" name="form[int][partidaCuenta][' + tr + '][num_monto]"></td>' +
                                        '<td width="70" style="vertical-align: middle;">' +
                                        '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="' + tr + '"><i class="md md-delete"></i></button>' +
                                        '<button class="btn ink-reaction btn-raised btn-xs btn-warning visualizar" id="' + (tr++) + '"><i class="md md-remove-red-eye"></i></button>' +
                                        '</td>' +
                                        '</tr>'
                                );
                            }else{
                                var montoCuenta = $('#codCuentas'+dato[i]['fk_prb002_partida_presupuestaria']+'monto').val();
                                var montoTotal = parseFloat(montoCuenta)+parseFloat( dato[i]['num_monto_base']);
                                $('#codCuentas'+dato[i]['fk_prb002_partida_presupuestaria']+'monto').val(montoTotal.toFixed(2));
                            }
                            actualizarMontos();
                        }
                    }
                }, 'json');
            });


        });
        function actualizarMontos(){
            var sec= $('#documentos tr').length;
            var montoPartidaTotal = 0; var montoImpuestoTotal = 0;
            for (var j = 1; j <= sec; j++) {
                var montoPartida = $('#num_monto_afecto'+j).val();
                var montoImpuesto = $('#monto_impuesto'+j).val();
                montoPartidaTotal = parseFloat(montoPartidaTotal)+parseFloat(montoPartida);
                montoImpuestoTotal = parseFloat(montoImpuestoTotal)+parseFloat(montoImpuesto);
            }
            var montoTotal = parseFloat(montoPartidaTotal)+parseFloat(montoImpuestoTotal);

            $('#num_monto_afecto').val(montoPartidaTotal.toFixed(2));
            $('#num_monto_impuesto').val(montoImpuestoTotal.toFixed(2));
            $('#num_monto_obligacion').val(montoTotal.toFixed(2));
            $('#nomtoTotalPagar').val(montoTotal.toFixed(2));
        }
    });
</script>