<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Impuesto</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Descripcion</th>
                                <th>Tipo</th>
                                <th>Regimen Fiscal</th>
                                <th>%</th>
                                <th>Provision imponible</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                        {foreach item=impuesto from=$lista}
                            <tr id="idImpuesto{$impuesto.pk_num_impuesto}">
                                <input type="hidden" value="{$impuesto.pk_num_impuesto}" class="persona"
                                       idImpuesto="{$impuesto.pk_num_impuesto}"
                                       descripcion="{$impuesto.txt_descripcion}"
                                       idCuenta="{$impuesto.fk_cbb004_num_plan_cuenta}"
                                       idCuenta20="{$impuesto.fk_cbb004_num_plan_cuenta_pub20}"
                                       imponible="{$impuesto.imponible}"
                                       factor="{$impuesto.num_factor_porcentaje}"
                                       sustraendo="{$sustraendo}"
                                       unidadTributaria="{$unidadTributaria.ind_valor}"
                                       tipoComprobante="{$impuesto.Tipo}">
                                <td>{$impuesto.cod_impuesto}</td>
                                <td>{$impuesto.txt_descripcion}</td>
                                <td>{$impuesto.Tipo}</td>
                                <td>{$impuesto.RegimenFiscal}</td>
                                <td>{$impuesto.num_factor_porcentaje}</td>
                                <td>{$impuesto.Provision}</td>
                                <td>{$impuesto.num_estatus}</td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatable2').on('click', 'tbody tr', function () {
            var input = $(this).find('input');
            var montoNoAfecto = parseFloat($(document.getElementById('totalPartidaCuenta')).attr('montoNoAfecto'));
            var montoAfecto = parseFloat($(document.getElementById('totalPartidaCuenta')).attr('montoAfecto'));
            var iva = parseFloat($(document.getElementById('totalImpuesto')).attr('ivaMonto'));
            var imponible = input.attr('imponible');
            var factor = input.attr('factor');
            var tipoComprobante = input.attr('tipoComprobante');
            var porcentaje = parseFloat(input.attr('factor'))/100;
            var monto = 0;
            var sustraendo = '';

            if(imponible=='N'){
                var montoAN = parseFloat(montoAfecto) + parseFloat(montoNoAfecto);
                monto = montoAN;

                var tipoProveedor = $('#tipoProveedor').val();
                if(tipoProveedor != 'J' && tipoComprobante == 'ISLR'){
                    var sustraendo = input.attr('sustraendo');
                    var unidadTributaria = input.attr('unidadTributaria');
                    var Total = (montoAN*porcentaje) - (unidadTributaria * porcentaje * sustraendo);
                }else{
                    var Total = montoAN*porcentaje;
                }
//                alert(montoAfecto+'--'+porcentaje+'--'+Total);

            }else if(imponible=='I'){
                var Total = iva*porcentaje;
                monto = iva;
            }else{
                var Total = montoAN*porcentaje;
                monto = montoAN;
            }
            if(factor == 0){
                monto = 0;
            }

            var montoImpuesto=0;
            $(document.getElementsByClassName('muntoImpuesto')).each(function( titulo, valor ){
                if($(this).val()!=''){
                    if(parseFloat($(this).val())){
                        montoImpuesto = montoImpuesto + parseFloat($(this).val());
                    }
                }
            });
            $('#num_monto_impuesto').val($('#totalImpuesto').attr('ivaMonto'));
            $('#num_monto_impuesto_otros').val(montoImpuesto+Total);
            $('#totalImpuesto').html(montoImpuesto+Total);
            $('#totalImpuesto').attr('monto',montoImpuesto+Total);
            //actualizar Informacion contable
            var impuestos = parseFloat($(document.getElementById('totalImpuesto')).attr('monto'));
            var montoIva = parseFloat($(document.getElementById('totalImpuesto')).attr('ivamonto'));
//            var montoAfecto = parseFloat($(document.getElementById('totalPartidaCuenta')).attr('montoAfecto'));
//            var montoNoAfecto = parseFloat($(document.getElementById('totalPartidaCuenta')).attr('montoNoAfecto'));
            var montoTotalObligacion= (montoAfecto + montoNoAfecto + montoIva ) - impuestos;
            var montoTotalPagar = montoTotalObligacion;
            $(document.getElementById('num_monto_obligacion')).val(montoTotalObligacion);
            $(document.getElementById('nomtoTotalPagar')).val(montoTotalPagar);
            $(document.getElementsByClassName('impuesto{$tr}')).remove();
            $(document.getElementById('contenidoTabla')).append(
                    '<tr id="impuesto{$tr}">' +
                    '<input type="hidden" value="{$tr}" name="form[int][impuestosRetenciones][ind_secuencia][{$tr}]">' +
                    '<input type="hidden" value="'+monto+'" id="num_monto_afecto{$tr}" name="form[int][impuestosRetenciones][{$tr}][num_monto_afecto]">' +
                    '<input type="hidden" value="'+Total+'" id="num_monto_impuesto{$tr}" name="form[int][impuestosRetenciones][{$tr}][num_monto_impuesto]">' +
                    '<input type="hidden" value="'+imponible+'" id="imponible{$tr}">' +
                    '<input type="hidden" value="'+porcentaje+'" id="porcentaje{$tr}">' +
                    '<input type="hidden" value="'+tipoComprobante+'" id="tipoComprobante{$tr}">' +
                    '<input type="hidden" value="'+sustraendo+'" id="sustraendo{$tr}">' +
                    '<input type="hidden" value="'+factor+'" id="factor{$tr}">' +
                    '<input type="hidden" value="'+unidadTributaria+'" id="unidadTributaria{$tr}">' +
                    '<input type="hidden" value="'+input.attr('idCuenta')+'" name="form[int][impuestosRetenciones][{$tr}][fk_cbb004_num_cuenta]">' +
                    '<input type="hidden" value="'+input.attr('idCuenta20')+'" name="form[int][impuestosRetenciones][{$tr}][fk_cbb004_num_cuenta_pub20]">' +
                    '<input type="hidden" value="'+input.attr('idImpuesto')+'" name="form[int][impuestosRetenciones][{$tr}][fk_cpb015_num_impuesto]">' +
                    '<td width="35"><input type="text" style="font-size:11px;" class="form-control text-center" value="{$tr}" readonly ></td>' +
                    '<td width="190"><input type="text" style="font-size:11px;" class="form-control text-center"  value="'+input.attr('descripcion')+'" readonly></td>' +
                    '<td width="100"><input type="text" style="font-size:11px;" class="form-control text-center" id="monto{$tr}"  value="'+monto+'" readonly></td>' +
                    '<td width="100"><input type="text" style="font-size:11px;" class="form-control text-center" id="factor{$tr}" value="'+input.attr('factor')+'" readonly></td>' +
                    '<td width="100"><input type="text" style="font-size:11px;" class="form-control text-center muntoImpuesto" id="montoTotal{$tr}" value="'+Total+'" readonly></td>'+
                    '<td width="35" id="impuesto{$tr}" width="10px" class="text-center" style="vertical-align: middle;">' +
                    '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="{$tr}">' +
                    '<i class="md md-delete"></i></button></td>' +
                    '</tr>'
            );
            if(factor == 0){
                $('#montoTotal{$tr}').attr('readonly',false);
            }
            $('#montoTotal{$tr}').on('change', function () {
                var monto = $(this).val();
                $('#num_monto_impuesto{$tr}').attr('value',monto);
            });
            $('#cerrarModal2').click();
        });
    });
</script>