    <div class="section-header">
        <h2 class="text-primary">Listado de Documentos del Proveedor</h2>
    </div>
    <div id="rootwizard2" class="form-wizard form-wizard-horizontal">

        <form action="" id="formAjax"
              class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">

            <div class="form-wizard-nav">
                <div class="progress">
                    <div class="progress-bar progress-bar-primary"></div>
                </div>
                <ul class="nav nav-justified">
                    <li class="active"><a href="#compras" data-toggle="tab"><span class="step">1</span> <span class="title">ORDEN DE COMPRA</span></a></li>
                    <li><a href="#servicios" data-toggle="tab"><span class="step">2</span> <span class="title">ORDEN DE SERVICIO</span></a></li>
                </ul>
            </div>
            <div class="tab-content clearfix">
                <div class="tab-pane active" id="compras">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="datatable2" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Nro. Orden</th>
                                    <th>Fecha Preparacion</th>
                                    <th>Descripcion</th>
                                    <th>Monto a Pagar</th>
                                    <th>Monto Pendiente</th>
                                    <th>Monto Total</th>
                                    <th>Almacen</th>
                                    <th>Almacen Ingreso</th>
                                    <th>Forma de Pago</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=i from=$documentosBD}
                                    {if $i.ind_tipo_orden == 'OC'}
                                <tr id="idOrden{$i.pk_num_orden}">
                                    <input type="hidden" value="{$i.pk_num_orden}" class="orden"
                                           idOrden="{$i.pk_num_orden}"
                                           almacen="{$i.fk_lgb014_num_almacen}"
                                           tipoOrden="{$i.ind_tipo_orden}"
                                           num_monto_total="{$i.num_monto_total}"
                                           fecha="{$i.fec_creacion}"
                                           num_monto_afecto="{$i.num_monto_afecto}"
                                           num_monto_no_afecto="{$i.num_monto_no_afecto}"
                                           comentario="{$i.ind_comentario}"
                                           impuesto="{$i.num_monto_igv}"
                                            >
                                    <th>{$i.ind_orden}</th>
                                    <th>{$i.fec_creacion}</th>
                                    <th>{$i.ind_descripcion}</th>
                                    <th>{$i.num_monto_total}</th>
                                    <th>{$i.num_monto_pendiente}</th>
                                    <th>{$i.num_monto_pendiente}</th>
                                    <th>{$i.ind_descripcion}</th>
                                    <th>{$i.ind_descripcion}</th>
                                    <th>{$i.ind_nombre_detalle}</th>
                                </tr>
                                    {/if}
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="servicios">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="datatable3" class="table table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>Nro. Orden</th>
                                                <th>Fecha Preparacion</th>
                                                <th>Descripcion</th>
                                                <th>Monto a Pagar</th>
                                                <th>Monto Pendiente</th>
                                                <th>Monto Total</th>
                                                <th>Almacen</th>
                                                <th>Almacen Ingreso</th>
                                                <th>Forma de Pago</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {foreach item=i from=$documentosBD}
                                                {if $i.ind_tipo_orden == 'OS'}
                                                    <tr id="idOrden{$i.pk_num_orden}">
                                                        <input type="hidden" value="{$i.pk_num_orden}" class="orden"
                                                               idOrden="{$i.pk_num_orden}"
                                                               almacen="{$i.fk_lgb014_num_almacen}"
                                                               tipoOrden="{$i.ind_tipo_orden}"
                                                               num_monto_total="{$i.num_monto_total}"
                                                               fecha="{$i.fec_creacion}"
                                                               num_monto_afecto="{$i.num_monto_afecto}"
                                                               num_monto_no_afecto="{$i.num_monto_no_afecto}"
                                                               comentario="{$i.ind_comentario}"
                                                               impuesto="{$i.num_monto_igv}"
                                                                >
                                                        <th>{$i.ind_orden}</th>
                                                        <th>{$i.fec_creacion}</th>
                                                        <th>{$i.ind_descripcion}</th>
                                                        <th>{$i.num_monto_total}</th>
                                                        <th>{$i.num_monto_pendiente}</th>
                                                        <th>{$i.num_monto_pendiente}</th>
                                                        <th>{$i.ind_descripcion}</th>
                                                        <th>{$i.ind_descripcion}</th>
                                                        <th>{$i.ind_nombre_detalle}</th>
                                                    </tr>
                                                {/if}
                                            {/foreach}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ul class="pager wizard">
                <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                <li class="next"><a class="btn-raised" href="javascript:void(0);" id="siguiente">Último</a></li>
            </ul>
        </form>
    </div>

</div>

<script>

    $(document).ready(function() {
       $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatable2').on('click', 'tbody tr', function () {
            var input = $(this).find('input');
            var almacen = input.attr('almacen');
            var idOrden = input.attr('idOrden');
            var tipoOrden = input.attr('tipoOrden');

            if (tipoOrden == 'OC'){
                var clasificacion = 'O. Compra';
            }else{
                var clasificacion = 'O. Servicio';
            }

            if(typeof($(document.getElementById('idDocumento'+ idOrden)).attr('id')) != 'undefined'){
                swal("Error!", 'Documento ya Insertado', "error");
            }else{
                $(document.getElementById('contenidoTablaDoc')).append(
                        '<tr id="idDocumento'+idOrden+'" idOrden="'+idOrden+'" class="documentos">' +
                        '<input type="hidden" value="{$tr}" name="form[int][documentos][ind_secuencia][{$tr}]"> ' +
                        '<input type="hidden" id="codDocumento{$tr}idOrden" value="' + idOrden + '" name="form[int][documentos][{$tr}][fk_lgb019_num_orden]"> ' +
                        '<td width="35"><input type="text" class="form-control text-center" id="ind_secuencia" value="{$tr}" name="form[int][ind_secuencia{$tr}]" disabled="disabled" ></td>' +
                        '<td width="150"><input type="text" class="form-control text-center" id="clasificacion" disabled="disabled" value="'+clasificacion+'"  ></td>' +
                        '<td width="150"><input type="text" class="form-control text-center" id="nroDoc" value="DOC-REF" readonly></td>' +
                        '<td width="150"><input type="text" class="form-control text-center" id="fecha" value="'+input.attr('fecha')+'" readonly></td>' +
                        '<td width="120"><input type="text" class="form-control text-center" id="num_monto_total{$tr}" value="'+input.attr('num_monto_total')+'" readonly></td>' +
                        '<td width="120"><input type="text" class="form-control text-center" id="num_monto_afecto{$tr}" value="'+input.attr('num_monto_afecto')+'" readonly></td>' +
                        '<td width="120"><input type="text" class="form-control text-center" id="monto_impuesto{$tr}" value="'+input.attr('impuesto')+'" readonly></td>' +
                        '<td width="120"><input type="text" class="form-control text-center" id="num_monto_no_afecto{$tr}" value="'+input.attr('num_monto_no_afecto')+'" readonly></td>' +
                        '<td width="450"><input type="text" class="form-control text-center" id="comentario" value="'+input.attr('comentario')+'" readonly ></td>' +
                        '<td id="'+idOrden+'" width="35" class="text-center" style="vertical-align: middle;">' +
                        '<input type="hidden" value="'+idOrden+'" name="form[int][cant]">' +
                        '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="'+idOrden+'">' +
                        '<i class="md md-delete"></i></button></td>' +
                        '</tr>'
                );
                $.post('{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/ajaxDocumentoDetMET', {
                    idOrden: idOrden
                }, function (dato) {
                    var tr = $('#partidasCuentas > tbody > tr').length + 1;
                    var sec =  tr-1;
                    var partida = $('#codCuentas'+sec+'PARTIDA').val();
                    for (var i = 0; i < dato.length; i++) {
                        var persona = $(document.getElementById('codigoProveedor1')).val();
                        var flag = dato[i]['num_flag_exonerado'];
                        if (flag == 1) {
                            var flag = 'checked';
                        } else {
                            var flag = '';
                        }

                        if (partida != dato[i]['fk_prb002_partida_presupuestaria']) {
                            partida = dato[i]['fk_prb002_partida_presupuestaria'];
                            $(document.getElementById('partidasCuentas')).append(
                                    '<tr id="codCuentas' + tr + '">' +
                                    '<input type="hidden" class="ajaxPartidas" value="' + tr + '" name="form[int][partidaCuenta][ind_secuencia][' + tr + ']"> ' +
                                    '<input type="hidden" class="ajaxPartidas" id="codCuentas' + tr + 'CUENTA" value="' + dato[i]['fk_cbb004_plan_cuenta_oncop'] + '" name="form[int][partidaCuenta][' + tr + '][fk_cbb004_num_cuenta]"> ' +
                                    '<input type="hidden" class="ajaxPartidas" id="codCuentas' + tr + 'CUENTA20" value="' + dato[i]['fk_cbb004_plan_cuenta_pub_veinte'] + '" name="form[int][partidaCuenta][' + tr + '][fk_cbb004_num_cuenta_pub20]"> ' +
                                    '<input type="hidden" class="ajaxPartidas" id="codCuentas' + tr + 'PARTIDA" value="' + partida + '" name="form[int][partidaCuenta][' + tr + '][fk_prb002_num_partida_presupuestaria]"> ' +
                                    '<input type="hidden" class="ajaxPartidas" id="codCuentas' + tr + 'CC" value="' + dato[i]['fk_a023_num_centro_costo'] + '" name="form[int][partidaCuenta][' + tr + '][fk_a023_num_centro_costo]"> ' +
                                    '<input type="hidden" class="ajaxPartidas" id="codCuentas' + tr + 'PERSONA" value="' + persona + '" name="form[int][partidaCuenta][' + tr + '][fk_a003_num_persona_proveedor]"> ' +
                                    '<input type="hidden" class="ajaxPartidas" id="codCuentas' + tr + 'DESCRIPCION" value="" name="form[int][partidaCuenta][' + tr + '][ind_descripcion]"> ' +
                                    '<td width="35" style="vertical-align: middle;"><input type="text"  class="form-control text-center" value="' + tr + '" readonly ></td>' +
                                    '<td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas' + tr + 'PARTIDA_C" value="' + dato[i]['cod_partida'] + '" readonly></td>' +
                                    '<td width="250" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas' + tr + 'PARTIDA_D" value="' + dato[i]['ind_denominacion'] + '" readonly></td>' +
                                    '<td width="150">' +
                                    '<input type="text" class="form-control text-center" id="codCuentas' + tr + 'CUENTA_C" value="' + dato[i]['cod_cuenta'] + '" readonly>' +
                                    '<input type="text" class="form-control text-center" id="codCuentas' + tr + 'CUENTA20_C" value="' + dato[i]['cod_cuenta20'] + '" readonly>' +
                                    '</td>' + '<td width="250">' +
                                    '<input type="text" class="form-control text-center" id="codCuentas' + tr + 'CUENTA_D" value="' + dato[i]['ind_descripcion'] + '" readonly>' +
                                    '<input type="text" class="form-control text-center" id="codCuentas' + tr + 'CUENTA20_D" value="' + dato[i]['ind_descripcion20'] + '" readonly>' +
                                    '</td>' +
                                    '<td width="65" style="vertical-align: middle;">' +
                                    '<input type="text" class="form-control text-center accionModal" id="codCuentas' + tr + 'CC_C" value="' + dato[i]['fk_a023_num_centro_costo'] + '" ' +
                                    'data-toggle="modal"' +
                                    'data-target="#formModal2"' +
                                    'titulo="Listado Centro de Costo"' +
                                    'enlace="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/centroCostoMET/partida/codCuentas' + tr + '" readonly>' +
                                    '</td>' +
                                    '<td width="60" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas' + tr + 'PERSONA_C" value="' + persona + '" readonly></td>' +
                                    '<td width="40" style="vertical-align: middle;">' +
                                    '<div class="checkbox checkbox-styled">' +
                                    '<label>' +
                                    '<input type="checkbox" id="codCuentas' + tr + 'FLAG_NO_AFECTO" name="form[int][partidaCuenta][' + tr + '][num_flag_no_afecto]" value="1" ' + flag + '>' +
                                    '<span></span>' +
                                    '</label>' +
                                    '</div>' +
                                    '</td>' +
                                    '<td width="160" style="vertical-align: middle;"><input type="text" class="form-control text-right montoDistribucion ajaxPartidas" id="codCuentas' + dato[i]['fk_prb002_partida_presupuestaria'] + 'monto" idTR="codCuentas' + tr + '" value="' + dato[i]['num_monto_base'] + '" name="form[int][partidaCuenta][' + tr + '][num_monto]"></td>' +
                                    '<td width="70" style="vertical-align: middle;">' +
                                    '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="' + tr + '"><i class="md md-delete"></i></button>' +
                                    '<button class="btn ink-reaction btn-raised btn-xs btn-warning visualizar" id="' + (tr++) + '"><i class="md md-remove-red-eye"></i></button>' +
                                    '</td>' +
                                    '</tr>'
                            );
                        }else{
                            var montoCuenta = $('#codCuentas'+dato[i]['fk_prb002_partida_presupuestaria']+'monto').val();
                            var montoTotal = parseFloat(montoCuenta)+parseFloat( dato[i]['num_monto_base']);
                            $('#codCuentas'+dato[i]['fk_prb002_partida_presupuestaria']+'monto').val(montoTotal.toFixed(2));
                        }
                    }
                    actualizarMontos();
                },'json');
                $('#cerrarModal2').click();
            } //cierre else
        });
    });

    //---
    $(document).ready(function() {
        $('#datatable3').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatable3 tbody tr').click(function() {
            var input = $(this).find('input');
            var almacen = input.attr('almacen');
            var idOrden = input.attr('idOrden');
            var tipoOrden = input.attr('tipoOrden');

            if (tipoOrden == 'OC'){
                var clasificacion = 'O. Compra';
            }else{
                var clasificacion = 'O. Servicio';
            }

            if(typeof($(document.getElementById('idDocumento'+ idOrden)).attr('id')) != 'undefined'){
                swal("Error!", 'Documento ya Insertado', "error");
            }else{
                $(document.getElementById('contenidoTablaDoc')).append(
                        '<tr id="idDocumento'+idOrden+'" idOrden="'+idOrden+'" class="documentos">' +
                        '<input type="hidden" value="{$tr}" name="form[int][documentos][ind_secuencia][{$tr}]"> ' +
                        '<input type="hidden" id="codDocumento{$tr}idOrden" value="' + idOrden + '" name="form[int][documentos][{$tr}][fk_lgb019_num_orden]"> ' +
                        '<td width="35"><input type="text" class="form-control text-center" id="ind_secuencia" value="{$tr}" name="form[int][ind_secuencia{$tr}]" disabled="disabled" ></td>' +
                        '<td width="150"><input type="text" class="form-control text-center" id="clasificacion" disabled="disabled" value="'+clasificacion+'"  ></td>' +
                        '<td width="150"><input type="text" class="form-control text-center" id="nroDoc" value="DOC-REF" readonly></td>' +
                        '<td width="150"><input type="text" class="form-control text-center" id="fecha" value="'+input.attr('fecha')+'" readonly></td>' +
                        '<td width="120"><input type="text" class="form-control text-center" id="num_monto_total{$tr}" value="'+input.attr('num_monto_total')+'" readonly></td>' +
                        '<td width="120"><input type="text" class="form-control text-center" id="num_monto_afecto{$tr}" value="'+input.attr('num_monto_afecto')+'" readonly></td>' +
                        '<td width="120"><input type="text" class="form-control text-center" id="monto_impuesto{$tr}" value="'+input.attr('impuesto')+'" readonly></td>' +
                        '<td width="120"><input type="text" class="form-control text-center" id="num_monto_no_afecto{$tr}" value="'+input.attr('num_monto_no_afecto')+'" readonly></td>' +
                        '<td width="450"><input type="text" class="form-control text-center" id="comentario" value="'+input.attr('comentario')+'" readonly ></td>' +
                        '<td id="'+idOrden+'" width="35" class="text-center" style="vertical-align: middle;">' +
                        '<input type="hidden" value="'+idOrden+'" name="form[int][cant]">' +
                        '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="'+idOrden+'">' +
                        '<i class="md md-delete"></i></button></td>' +
                        '</tr>'
                );

                $.post('{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/ajaxDocumentoDetMET', {
                    idOrden: idOrden
                }, function (dato) {
                    var tr = $('#partidasCuentas > tbody > tr').length + 1;
                    var sec =  tr-1;
                    var partida = $('#codCuentas'+sec+'PARTIDA').val();
                    for (var i = 0; i < dato.length; i++) {
                        var persona = $(document.getElementById('codigoProveedor1')).val();
                        var flag = dato[i]['num_flag_exonerado'];
                        if (flag == 1) {
                            var flag = 'checked';
                        } else {
                            var flag = '';
                        }
                        if (partida != dato[i]['fk_prb002_partida_presupuestaria']) {
                            partida = dato[i]['fk_prb002_partida_presupuestaria'];

                            $(document.getElementById('partidasCuentas')).append(
                                    '<tr id="codCuentas' + tr + '">' +
                                    '<input type="hidden" class="ajaxPartidas" value="' + tr + '" name="form[int][partidaCuenta][ind_secuencia][' + tr + ']"> ' +
                                    '<input type="hidden" class="ajaxPartidas" id="codCuentas' + tr + 'CUENTA" value="' + dato[i]['fk_cbb004_plan_cuenta_oncop'] + '" name="form[int][partidaCuenta][' + tr + '][fk_cbb004_num_cuenta]"> ' +
                                    '<input type="hidden" class="ajaxPartidas" id="codCuentas' + tr + 'CUENTA20" value="' + dato[i]['fk_cbb004_plan_cuenta_pub_veinte'] + '" name="form[int][partidaCuenta][' + tr + '][fk_cbb004_num_cuenta_pub20]"> ' +
                                    '<input type="hidden" class="ajaxPartidas" id="codCuentas' + tr + 'PARTIDA" value="' + partida + '" name="form[int][partidaCuenta][' + tr + '][fk_prb002_num_partida_presupuestaria]"> ' +
                                    '<input type="hidden" class="ajaxPartidas" id="codCuentas' + tr + 'CC" value="' + dato[i]['fk_a023_num_centro_costo'] + '" name="form[int][partidaCuenta][' + tr + '][fk_a023_num_centro_costo]"> ' +
                                    '<input type="hidden" class="ajaxPartidas" id="codCuentas' + tr + 'PERSONA" value="' + persona + '" name="form[int][partidaCuenta][' + tr + '][fk_a003_num_persona_proveedor]"> ' +
                                    '<input type="hidden" class="ajaxPartidas" id="codCuentas' + tr + 'DESCRIPCION" value="" name="form[int][partidaCuenta][' + tr + '][ind_descripcion]"> ' +
                                    '<td width="35" style="vertical-align: middle;"><input type="text"  class="form-control text-center" value="' + tr + '" readonly ></td>' +
                                    '<td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas' + tr + 'PARTIDA_C" value="' + dato[i]['cod_partida'] + '" readonly></td>' +
                                    '<td width="250" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas' + tr + 'PARTIDA_D" value="' + dato[i]['ind_denominacion'] + '" readonly></td>' +
                                    '<td width="150">' +
                                    '<input type="text" class="form-control text-center" id="codCuentas' + tr + 'CUENTA_C" value="' + dato[i]['cod_cuenta'] + '" readonly>' +
                                    '<input type="text" class="form-control text-center" id="codCuentas' + tr + 'CUENTA20_C" value="' + dato[i]['cod_cuenta20'] + '" readonly>' +
                                    '</td>' +
                                    '<td width="250">' +
                                    '<input type="text" class="form-control text-center" id="codCuentas' + tr + 'CUENTA_D" value="' + dato[i]['ind_descripcion'] + '" readonly>' +
                                    '<input type="text" class="form-control text-center" id="codCuentas' + tr + 'CUENTA20_D" value="' + dato[i]['ind_descripcion20'] + '" readonly>' +
                                    '</td>' +
                                    '<td width="65" style="vertical-align: middle;">' +
                                    '<input type="text" class="form-control text-center accionModal" id="codCuentas' + tr + 'CC_C" value="' + dato[i]['fk_a023_num_centro_costo'] + '" ' +
                                    'data-toggle="modal"' +
                                    'data-target="#formModal2"' +
                                    'titulo="Listado Centro de Costo"' +
                                    'enlace="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/centroCostoMET/partida/codCuentas' + tr + '" readonly>' +
                                    '</td>' +
                                    '<td width="60" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas' + tr + 'PERSONA_C" value="' + persona + '" readonly></td>' +
                                    '<td width="40" style="vertical-align: middle;">' +
                                    '<div class="checkbox checkbox-styled">' +
                                    '<label>' +
                                    '<input type="checkbox" id="codCuentas' + tr + 'FLAG_NO_AFECTO" name="form[int][partidaCuenta][' + tr + '][num_flag_no_afecto]" value="1" ' + flag + '>' +
                                    '<span></span>' +
                                    '</label>' +
                                    '</div>' +
                                    '</td>' +
                                    '<td width="160" style="vertical-align: middle;"><input type="text" class="form-control text-right montoDistribucion ajaxPartidas" id="codCuentas' + dato[i]['fk_prb002_partida_presupuestaria'] + 'monto" idTR="codCuentas' + tr + '" value="' + dato[i]['num_monto_base'] + '" name="form[int][partidaCuenta][' + tr + '][num_monto]"></td>' +
                                    '<td width="70" style="vertical-align: middle;">' +
                                    '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="' + tr + '"><i class="md md-delete"></i></button>' +
                                    '<button class="btn ink-reaction btn-raised btn-xs btn-warning visualizar" id="' + (tr++) + '"><i class="md md-remove-red-eye"></i></button>' +
                                    '</td>' +
                                    '</tr>'
                            );
                        }else{
                            var montoCuenta = $('#codCuentas'+dato[i]['fk_prb002_partida_presupuestaria']+'monto').val();
                            var montoTotal = parseFloat(montoCuenta)+parseFloat( dato[i]['num_monto_base']);
                            $('#codCuentas'+dato[i]['fk_prb002_partida_presupuestaria']+'monto').val(montoTotal.toFixed(2));
                        }
                    }
                    actualizarMontos();
                },'json');
                $('#cerrarModal2').click();
            } //cierre else
        });
    });

    function actualizarMontos(){
        var monto=0;
        var montoAfecto = 0;
        var montoNoAfecto = 0;
        $('.montoDistribucion').each(function( titulo, valor ){
            if($(this).val()!=''){
                if(parseFloat($(this).val())){
                    if($('#'+ $(this).attr('idTR') + 'FLAG_NO_AFECTO').is(':checked')){
                        montoNoAfecto = montoNoAfecto + parseFloat($(this).val());
                    }else{
                        montoAfecto = montoAfecto + parseFloat($(this).val());
                    }
                    monto = monto + parseFloat($(this).val());
                }
            }
        });
        var montoImpuesto = parseFloat(montoAfecto) * (parseFloat($('#totalImpuesto').attr('ivaPorcentaje')) / 100);
        $('#num_monto_afecto').val(montoAfecto);
        $('#totalImpuesto').attr('ivaMonto', montoImpuesto);
        if(montoNoAfecto!=0){
            $('#num_monto_no_afecto').val(montoNoAfecto);
        }
        $('#num_monto_impuesto').val(montoImpuesto);
        $('#totalPartidaCuenta').html(monto);
        $('#totalPartidaCuenta').attr('monto',monto);
        $('#totalPartidaCuenta').attr('montoAfecto',montoAfecto);
        $('#totalPartidaCuenta').attr('montoNoAfecto',montoNoAfecto);
    }
</script>
