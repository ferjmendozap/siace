<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Concepto de Gasto</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Concepto</th>
                            <th>Descripcion</th>
                            <th>Partida</th>
                            <th>Cuenta</th>
                            <th>Estado</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=concepto from=$lista}
                        <tr id="idconcepto{$concepto.pk_num_concepto_gasto}">
                                <input type="hidden" value="" class="partida"
                                       fecha="{date('Y-m-d')}"
                                       concepto="{$concepto.ind_descripcion}"
                                       pkConcepto="{$concepto.pk_num_concepto_gasto}"
                                       codCuenta="{$concepto.cod_cuenta}"
                                       codCuenta20="{$concepto.cod_cuenta20}"
                                       idCuenta="{$concepto.pk_num_cuenta}"
                                       idCuenta20="{$concepto.pk_num_cuenta20}"
                                       nombreCuenta="{$concepto.nombreCuenta}"
                                       nombreCuenta20="{$concepto.nombreCuenta20}"
                                       idPartida="{$concepto.pk_num_partida_presupuestaria}"
                                       nombrePartida="{$concepto.ind_denominacion}"
                                       codPartida="{$concepto.cod_partida}"
                                        >
                                <td><label>{$concepto.pk_num_concepto_gasto}</label></td>
                                <td><label>{$concepto.ind_descripcion}</label></td>
                                <td><label>{$concepto.cod_partida}</label></td>
                                <td><label>{$concepto.cod_cuenta}</label></td>
                                <td><i class="{if $concepto.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                        </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

</div>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        $('#datatable2').on('click', 'tbody tr', function () {
            var input = $(this).find('input');

            if('{$listaConcepto}' == 'concepto') {
                var opciones = '';
                var i = 0;

                var id = 0;
                {foreach item=reg from=$listadoRegimen}
                i = i + 1;
                if (i == 1) {
                    var id = {$reg.pk_num_miscelaneo_detalle};
                }
                opciones += '<option value="{$reg.pk_num_miscelaneo_detalle}">{$reg.ind_nombre_detalle}</option>';
                {/foreach}

                $(document.getElementById('reposicion')).append(
                    '<tr id="codReposicion{$tr}">' +

                    '<input type="hidden" class="ajaxReposicion" value="{$tr}" name="form[int][detalleReposicion][num_secuencia][{$tr}]"> ' +
                    '<input type="hidden" class="ajaxReposicion" id="codReposicion{$tr}CUENTA" value="' + input.attr('idCuenta') + '" name="form[int][detalleReposicion][{$tr}][fk_cbb004_num_plan_cuenta]"> ' +
                    '<input type="hidden" class="ajaxReposicion" id="codReposicion{$tr}CUENTA20" value="' + input.attr('idCuenta20') + '" name="form[int][detalleReposicion][{$tr}][fk_cbb004_num_plan_cuentaPub20]"> ' +
                    '<input type="hidden" class="ajaxReposicion" id="codReposicion{$tr}PARTIDA" value="' + input.attr('idPartida') + '" name="form[int][detalleReposicion][{$tr}][fk_prb002_num_partida_presupuestaria]"> ' +
                    '<input type="hidden" class="ajaxReposicion" id="codReposicion{$tr}COD_PARTIDA" value="' + input.attr('codPartida') + '" > ' +
                    '<input type="hidden" class="ajaxReposicion" id="codReposicion{$tr}NOMBRE_PARTIDA" value="' + input.attr('nombrePartida') + '" > ' +
                    '<input type="hidden" class="ajaxReposicion" id="codReposicion{$tr}COD_CUENTA" value="' + input.attr('codCuenta') + '" > ' +
                    '<input type="hidden" class="ajaxReposicion" id="codReposicion{$tr}COD_CUENTA20" value="' + input.attr('codCuenta20') + '" > ' +
                    '<input type="hidden" class="ajaxReposicion" id="codReposicion{$tr}NOMBRE_CUENTA" value="' + input.attr('nombreCuenta') + '" > ' +
                    '<input type="hidden" class="ajaxReposicion" id="codReposicion{$tr}NOMBRE_CUENTA20" value="' + input.attr('nombreCuenta20') + '" > ' +
                    '<input type="hidden" class="ajaxReposicion" id="codReposicion{$tr}CONCEPTO" value="' + input.attr('pkConcepto') + '" name="form[int][detalleReposicion][{$tr}][fk_cpb005_num_concepto_gasto]" > ' +
                    '<input type="hidden" id="codReposicion{$tr}pkPersona" name="form[int][detalleReposicion][{$tr}][fk_rhb001_num_empleado_proveedor]">' +

                    '<td width="50" style="vertical-align: middle;"><input type="text"  class="form-control text-center" value="{$tr}" readonly ></td>' +
                    '<td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codReposicion{$tr}fec_doc" value="{date('Y-m-d')}" readonly></td>' +
                    '<td width="300" style="vertical-align: middle;">' +

                    '<input type="text" class="form-control text-center concepto accionModal2" id="codReposicion{$tr}concepto" value="' + input.attr('concepto') + '" readonly ' +
                    'data-toggle="modal"' +
                    'data-target="#formModal2"' +
                    'titulo="Listado de Concepto"' +
                    'enlace="{$_Parametros.url}modCP/otros/cajaChica/listarCajaChicaCONTROL/conceptoMET/conceptoDet/{$tr}" >' +
                    '</td>' +
                    '<td width="400" style="vertical-align: middle;"><textarea id="codReposicion{$tr}descripcion" class="form-control ajaxReposicion" rows="2" name="form[txt][detallesReposicion][{$tr}][ind_descripcion]"></textarea></td>' +
                    '<td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-center montoPagado ajaxReposicion" idTR="codReposicion{$tr}" id="codReposicion{$tr}montoPagado" value="0" name="form[int][detalleReposicion][{$tr}][num_monto_pagado]"></td>' +

                    '<td width="300"><select class="form-control regimen select2-list select2" id="codReposicion{$tr}tipoImpuesto" idSec="{$tr}" idTR="codReposicion{$tr}" name="form[int][detalleReposicion][{$tr}][fk_a006_num_miscelaneo_regimen_fiscal]" ' +
                    'enlace="{$_Parametros.url}modCP/otros/cajaChica/listarCajaChicaCONTROL/listaServicioMET/codReposicion{$tr}tipoServicio" ' +
                    '> ' +
                    opciones +
                    '</select></td>' +
                    '<td width="300"><select class="form-control select2-list select2 servicio" id="codReposicion{$tr}tipoServicio" idTR="codReposicion{$tr}" name="form[int][detalleReposicion][{$tr}][fk_cpb017_num_tipo_servicio]"> ' +
                    '</select></td>' +

                    '<td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-center montoAfecto" idTR="codReposicion{$tr}" id="codReposicion{$tr}montoAfecto" value="0" name="form[int][detalleReposicion][{$tr}][num_monto_afecto]" ></td>' +
                    '<td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-center montoNoAfecto" idTR="codReposicion{$tr}" id="codReposicion{$tr}montoNoAfecto" value="0" name="form[int][detalleReposicion][{$tr}][num_monto_no_afecto]"></td>' +
                    '<td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-center montoImpuesto" id="codReposicion{$tr}montoImpuesto" value="0" name="form[int][detalleReposicion][{$tr}][num_monto_impuesto]" readonly></td>' +
                    '<td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codReposicion{$tr}montoRetencion" value="0" name="form[int][detalleReposicion][{$tr}][num_monto_retencion]" readonly></td>' +
                    '<td width="100"><select class="form-control select2-list select2" id="codReposicion{$tr}fk_cpb002_num_tipo_documento" name="form[int][detalleReposicion][{$tr}][fk_cpb002_num_tipo_documento]"> ' +
                    '<option value="">Seleccione...</option>' +
                    '{foreach item=doc from=$documento}' +
                    '<option value="{$doc.pk_num_tipo_documento}">{$doc.ind_descripcion}</option>' +
                    '{/foreach}' +
                    '</select>' +
                    '<td width="120" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codReposicion{$tr}num_documento " name="form[int][detalleReposicion][{$tr}][num_documento]">' +
                    '</td>' +
                    '<td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codReposicion{$tr}ind_num_recibo" name="form[int][detalleReposicion][{$tr}][ind_num_recibo]"></td>' +
                    '<td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codReposicion{$tr}rif" name="form[int][detalleReposicion][{$tr}][ind_doc_fiscal]" readonly></td>' +
                    '<td width="350" style="vertical-align: middle;">' +
                    '<input type="text" class="form-control text-center accionModal2" id="codReposicion{$tr}persona"  readonly ' +
                    'data-toggle="modal"' +
                    'data-target="#formModal2"' +
                    'titulo="Listado de Persona"' +
                    'enlace="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/personaMET/personaConcepto/codReposicion{$tr}" >' +
                    '</td>' +
                    '<td width="120" style="vertical-align: middle;">' +
                    '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="{$tr}"><i class="md md-delete"></i></button>' +
                    '</td>' +
                    '</tr>'
                );
                var idTr = $(this).attr('idTR');
                $.post('{$_Parametros.url}modCP/otros/cajaChica/listarCajaChicaCONTROL/listaServicioMET/codReposicion{$tr}tipoServicio', {
                        idRegimen: id
                    },
                    function (dato) {
                        console.log('dato');
                        if (dato) {
                            $('#' + dato['idSelect']).html('');
                            $('#s2id_' + dato['idSelect'] + ' .select2-chosen').html('Seleccione el Servicio');
                            var id = dato['id'];
                            for (var i = 0; i < id.length; i++) {
                                $('#' + dato['idSelect']).append('<option value="' + id[i]['pk_num_tipo_servico'] + '">' + id[i]['ind_descripcion'] + '</option>');

                                $.post('{$_Parametros.url}modCP/otros/cajaChica/listarCajaChicaCONTROL/impuestoMET/IVA', {
                                    idTipoServicio: id[0]['pk_num_tipo_servico']
                                }, function (dato) {
                                    if (dato) {
                                        if (dato['num_factor_porcentaje']) {
                                            $('#totalImpuesto').attr('ivaporcentaje', dato['num_factor_porcentaje']);
                                            $('#totalImpuesto').attr('signo', dato['ind_signo']);
                                            $('#totalImpuesto').attr('regimen', dato['regimen']);
                                            $('#totalImpuesto').attr('imponible', dato['imponible']);
                                        }
                                    }
                                }, 'json');

                            }
                        }
                        //aqui
                        for (var i = 0; i < id.length; i++) {
                            var regimen = id[i]['cod_detalle'];
                            // alert(regimen);
                        }

                        if (regimen == 'M') {
                            document.getElementById(idTr + 'montoPagado').disabled = true;
                        } else if (regimen == 'N') {
                            document.getElementById(idTr + 'montoPagado').disabled = false;
                            document.getElementById(idTr + 'montoAfecto').disabled = true;
                            document.getElementById(idTr + 'montoNoAfecto').disabled = true;
                            document.getElementById(idTr + 'montoImpuesto').disabled = true;
                            document.getElementById(idTr + 'montoRetencion').disabled = true;
                        } else {
                            document.getElementById(idTr + 'montoPagado').disabled = false;
                            document.getElementById(idTr + 'montoAfecto').disabled = false;
                            document.getElementById(idTr + 'montoNoAfecto').disabled = false;
                            document.getElementById(idTr + 'montoImpuesto').disabled = false;
                            document.getElementById(idTr + 'montoRetencion').disabled = false;
                        }
                    }, 'json');
            }else{
                $(document.getElementById('codReposicion{$idCampo}CUENTA')).attr('value',(input.attr('idCuenta')));
                $(document.getElementById('codReposicion{$idCampo}CUENTA20')).attr('value',(input.attr('idCuenta20')));
                $(document.getElementById('codReposicion{$idCampo}PARTIDA')).attr('value',(input.attr('idPartida')));
                $(document.getElementById('codReposicion{$idCampo}COD_PARTIDA')).attr('value',(input.attr('codPartida')));
                $(document.getElementById('codReposicion{$idCampo}NOMBRE_PARTIDA')).attr('value',(input.attr('nombrePartida')));
                $(document.getElementById('codReposicion{$idCampo}COD_CUENTA')).attr('value',(input.attr('codCuenta')));
                $(document.getElementById('codReposicion{$idCampo}COD_CUENTA20')).attr('value',(input.attr('codCuenta20')));
                $(document.getElementById('codReposicion{$idCampo}NOMBRE_CUENTA')).attr('value',(input.attr('nombreCuenta')));
                $(document.getElementById('codReposicion{$idCampo}NOMBRE_CUENTA20')).attr('value',(input.attr('nombreCuenta20')));

                $(document.getElementById('codReposicion{$idCampo}concepto')).attr('value',(input.attr('concepto')));

            }
            $('#cerrarModal2').click();




        });
    });
</script>