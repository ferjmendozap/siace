<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Personas</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Persona</th>
                            <th>Descripcion</th>
                            <th>Emp.</th>
                            <th>Pro.</th>
                            <th>Cli.</th>
                            <th>Otr.</th>
                            <th>Nro. Documento</th>
                            <th>Doc. Fiscal</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=empleado from=$lista}
                            <tr id="idPersona{$empleado.pk_num_empleado}">
                                <input type="hidden" value="{$empleado.pk_num_empleado}" class="empleado"
                                       empleado="{$empleado.pk_num_empleado}"
                                       nombre="{$empleado.ind_nombre1} {$empleado.ind_nombre2} {$empleado.ind_apellido1} {$empleado.ind_apellido2}"
                                       documentoProv="{$empleado.ind_documento_fiscal}"
                                        >

                                <td><label>{$empleado.pk_num_persona}</label></td>
                                <td><label>{$empleado.ind_nombre1} {$empleado.ind_nombre2} {$empleado.ind_apellido1} {$empleado.ind_apellido2}</label></td>
                                <td><label><i class="md md-check"></i></label></td>
                                <td><label><i class="md md-not-interested"></i></label></td>
                                <td><label><i class="md md-not-interested"></i></label></td>
                                <td><label><i class="md md-not-interested"></i></label></td>
                                <td><label>{$empleado.ind_cedula_documento}</label></td>
                                <td><label>{$empleado.ind_documento_fiscal}</label></td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

</div>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatable2 tbody tr').click(function() {
            var input = $(this).find('input');

            if('{$listaEmpleado}' == 'beneficiario') {
                $(document.getElementById('fk_rhb001_num_empleado_beneficiario')).attr('value',(input.attr('empleado')));
                $(document.getElementById('nombreBenef')).attr('value', (input.attr('nombre')) );
                $('#mensaje').remove();
                $.post('{$_Parametros.url}modCP/otros/cajaChica/listarCajaChicaCONTROL/buscarAutorizacionMET/', {
                        idBeneficiario: input.attr('empleado')
                }, function (dato) {
                    if (dato) {
                        if (dato['fk_rhb001_num_empleado']) {
                            $('#montoAutorizado').attr('value', dato['num_monto_autorizado']);
                            document.getElementById("accion").disabled=false;
                        }
                    }else{
                        $('#montoAutorizado').attr('value', 0);
                        $('#mensajeAutorizacion').append(
                                '<div class="card-head card-head-xs btn-danger text-center" id="mensaje">'+
                                '<header class="text-center">El Beneficiario Actual no Tiene Monto Autorizado para Crear una Reposición de Caja Chica </header>'+
                                '</div>'
                        );
                        document.getElementById("accion").disabled=true;
                    }
                }, 'json');
            }
            if('{$listaPersona}' == 'cajaChicaFiltro') {
                $(document.getElementById('pkBeneficiario')).val(input.attr('proveedor'));
                $(document.getElementById('nombreBeneFiltro')).val(input.attr('nombre'));

            }
            $('#cerrarModal2').click();

        });
    });
</script>
