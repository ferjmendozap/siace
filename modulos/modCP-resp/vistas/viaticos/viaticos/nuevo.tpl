<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body ">
                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                        <form action="{$_Parametros.url}modCP/viaticos/viaticosCONTROL/accionesViaticoMET" id="formAjax"
                              class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">

                            <input type="hidden" value="1" name="valido"/>
                            <input type="hidden" value="{$idSolViatico}" id="idSolViatico" name="idSolViatico"/>
                            <input type="hidden" value="{$idViatico}" id="idViatico" name="idViatico"/>
                            <input type="hidden" value="{$solicitudBD.pkSolicitante}" id="pkSolicitante" name="form[int][pkSolicitante]">
                            <input type="hidden" value="{number_format($unidadTributaria.ind_valor,2, ',', '.')}" id="unidadTributaria" >


                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#informacionGeneralViat" data-toggle="tab"><span class="step">1</span> <span class="title">INFORMACION GENERAL</span></a></li>
                                    <li><a href="#beneficiarios" data-toggle="tab"><span class="step">2</span> <span class="title">BENEFICIARIOS</span></a></li>
                                    <li><a href="#distribucion" data-toggle="tab"><span class="step">3</span> <span class="title">DISTRIBUCION</span></a></li>
                                </ul>
                            </div>
                            <div class="tab-content clearfix">
                                <div class="tab-pane active" id="informacionGeneralViat">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-head card-head-xs style-primary text-center">
                                                    <header class="text-center">Beneficiario del Viatico</header>
                                                </div>
                                                <div class="card-body" style="padding: 4px;">
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-6">
                                                                <div class="col-sm-5 text-right">
                                                                    <label for="ind_num_caja_chica"
                                                                           class="control-label" style="margin-top: 10px;"> Nro. Solicitud:</label>
                                                                </div>
                                                                <div class="col-sm-7">
                                                                    <input type="text"
                                                                           id="ind_num_caja_chica"
                                                                           class="form-control"
                                                                           value="{if isset($solicitudBD.cod_interno)}{$solicitudBD.cod_interno}{/if}"
                                                                           disabled
                                                                           readonly>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">

                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-6">
                                                                <div class="col-sm-5 text-right">
                                                                    <label for="ind_num_viatico"
                                                                           class="control-label" style="margin-top: 10px;"> Viático:</label>
                                                                </div>
                                                                <div class="col-sm-7">
                                                                    <input type="text"
                                                                           id="ind_num_viatico"
                                                                           class="form-control"
                                                                           value="{if isset($viaticoBD.ind_num_viatico)}{$viaticoBD.ind_num_viatico}{/if}"
                                                                           disabled
                                                                           readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-6">
                                                                <div class="col-sm-5 text-right">
                                                                    <label for="fec_salida"
                                                                           class="control-label" style="margin-top: 10px;"> Fecha Salida:</label>
                                                                </div>
                                                                <div class="col-sm-7">
                                                                    <input type="text" class="form-control"
                                                                           id="fec_salida"
                                                                           name="form[txt][fec_salida]"
                                                                           value="{$solicitudBD.fec_salida}"
                                                                           readonly>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-sm-5 text-right">
                                                                    <label for="fec_regreso"
                                                                           class="control-label" style="margin-top: 10px;"> Fecha Regreso:</label>
                                                                </div>
                                                                <div class="col-sm-7">
                                                                    <input type="text" class="form-control"
                                                                           id="fec_regreso"
                                                                           name="form[txt][fec_regreso]"
                                                                           value="{$solicitudBD.fec_ingreso}"
                                                                           readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-6">
                                                                <div class="col-sm-5 text-right">
                                                                    <label for="ind_estado"
                                                                           class="control-label" style="margin-top: 10px;"> Estado:</label>
                                                                </div>
                                                                <div class="col-sm-7">
                                                                    <input type="text"
                                                                           id="ind_estado"
                                                                           class="form-control"
                                                                           value="{if isset($viaticoBD.ind_estado)}{$viaticoBD.ind_estado}{/if}"
                                                                           readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-6">
                                                                <div class="col-sm-5 text-right">
                                                                    <label for="num_total_dias"
                                                                           class="control-label"> Total Dias:</label>
                                                                </div>
                                                                <div class="col-sm-7">
                                                                    <input type="text" class="form-control"
                                                                           id="num_total_dias"
                                                                           value="{$solicitudBD.num_total_dias}"
                                                                           readonly>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-sm-3 text-right">
                                                                    <label for="ind_destino"
                                                                           class="control-label"> Destino:</label>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control"
                                                                           id="ind_destino"
                                                                           name="form[alphaNum][ind_destino]"
                                                                           value="{$solicitudBD.ind_destino}"
                                                                           readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-6">
                                                                <div class="col-sm-4">
                                                                    <label for="num_total_dias"
                                                                           class="control-label" style="margin-top: 10px;"> Periodo:</label>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control"
                                                                           id="num_total_dias"
                                                                           name="form[txt][ind_periodo]"
                                                                           value="{date('Y-m')}"
                                                                           readonly>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="ind_motivoError">
                                                                <textarea id="ind_motivo" name="form[alphaNum][ind_motivo]" class="form-control" rows="3" readonly >{$solicitudBD.ind_motivo}</textarea>
                                                                <label for="ind_motivo"><i class="md md-comment"></i> Motivo:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="ind_descripcionError">
                                                                <textarea id="ind_descripcion" name="form[alphaNum][ind_descripcion]" class="form-control" rows="3" readonly >{$resolucion}</textarea>
                                                                <label for="ind_descripcion"><i class="md md-comment"></i> Resolución:</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-6">
                                                                <div class="form-group floating-label" id="fk_rhb001_num_empleado_ingresaError">
                                                                    <input type="text" class="form-control" value="{if isset($viaticoBD.fk_rhb001_num_empleado_ingresa)}{$viaticoBD.EMPLEADO_INGRESA}{/if}" name="form[int][fk_rhb001_num_empleado_ingresa]" disabled id="fk_rhb001_num_empleado_ingresa">
                                                                    <label for="fk_rhb001_num_empleado_ingresa"><i class="md md-person"></i> Ingresado Por</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group floating-label" id="fec_preparacionError">
                                                                    <input type="text" class="form-control" value="{if isset($viaticoBD.fec_preparado)}{$viaticoBD.fec_preparado}{/if}" disabled name="form[alphaNum][fec_preparacion]" id="fec_preparacion">
                                                                    <label for="fec_preparacion"><i class="md md-today"></i> Fecha</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-6">
                                                                <div class="form-group floating-label" id="fk_rhb001_num_empleado_apruebaError">
                                                                    <input type="text" class="form-control" value="{if isset($viaticoBD.fk_rhb001_num_empleado_aprueba)}{$viaticoBD.EMPLEADO_APRUEBA}{/if}" name="form[int][fk_rhb001_num_empleado_aprueba]" disabled id="fk_rhb001_num_empleado_aprueba">
                                                                    <label for="fk_rhb001_num_empleado_aprueba"><i class="md md-person"></i> Aprobado Por</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group floating-label" id="fec_aprobadoError">
                                                                    <input type="text" class="form-control" value="{if isset($viaticoBD.fec_aprobado)}{$viaticoBD.fec_aprobado}{/if}" disabled name="form[alphaNum][fec_aprobado]" id="fec_aprobado">
                                                                    <label for="fec_aprobado"><i class="md md-today"></i> Fecha</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="beneficiarios">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-body" style="padding: 4px;">
                                                <div class="col-sm-12">
                                                    <div class="col-sm-12">
                                                        <div class="card">
                                                            <div class="card-head card-head-xs style-primary" align="center">
                                                                <header>Detalles de los Beneficiarios</header>
                                                            </div>
                                                            <tr class="card-body" style="padding: 4px;">
                                                                <div id="imput_nuevo"></div>

                                                                <table id="beneficiarios" class="table table-striped table-hover">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>Nombre y Apellidos</th>
                                                                        <th>Monto</th>
                                                                        <th>Fecha Inicio</th>
                                                                        <th>Fecha Fin</th>
                                                                        <th>Total Dias</th>
                                                                        <th>Oblig. </th>
                                                                        <th> Acciones</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody id="tbodyBeneficiarios">
                                                                    {$sec = 0}
                                                                    {foreach item=i from=$detalleBD}
                                                                        {$sec = $sec+1}
                                                                        {$pk =$i.pk_num_persona}
                                                                        {$montoViat = 0}
                                                                        {$oblig = ''}
                                                                        {foreach item=j from=$detalleViaticoBD}
                                                                            {if $pk == $j.fk_a003_persona_beneficiario}
                                                                                {$beneficiario = $j.fk_a003_persona_beneficiario}
                                                                                {$montoViat = $montoViat + $j.num_monto_total}
                                                                                {$estadoViatBenef = $j.ind_estado}

                                                                                {if $estadoViatBenef != 'PR'}
                                                                                    {$oblig = $j.ind_nro_registro}
                                                                                {/if}
                                                                            {/if}

                                                                            {$ind_secuencia = $j.ind_secuencia}
                                                                            {$pkConcepto = $j.fk_cpb009_num_concepto_gasto_viatico}
                                                                            {$concepto = $j.concepto}
                                                                            {$num_monto_viatico = $j.num_monto_viatico}
                                                                            {$num_valor_ut = $j.num_valor_ut}
                                                                            {$num_dia = $j.num_dia}
                                                                            {$num_monto_total = $j.num_monto_total}
                                                                        {/foreach}

                                                                            <tr id="idBeneficiarios{$pk}">
                                                                                <td>{$sec}</td>
                                                                                <td>{$i.beneficiario}</td>
                                                                                <td width="100"><input type="text" class="form-control text-center" id="codConcepto{$pk}monto" value="{if isset($montoViat)}{$montoViat}{/if}" readonly></td>
                                                                                <td>{$i.fec_salida_detalle}</td>
                                                                                <td>{$i.fec_ingreso_detalle}</td>
                                                                                <td>{$i.num_total_dias_detalle}</td>
                                                                                <td width="100"><input type="text" class="form-control text-center" id="obligacion{$i.pk_num_persona}" value="{$oblig}" readonly></td>
                                                                                <td>
                                                                                    {if isset($viaticoBD.ind_estado) and $viaticoBD.ind_estado == 'APROBADO'}
                                                                                        <button class="accion btn ink-reaction btn-raised btn-xs btn-primary-light" data-toggle="modal" data-target="#formModal2"
                                                                                                data-keyboard="false" data-backdrop="static" estado="GE" id="generar{$i.pk_num_persona}" idViatico="{$idViatico}" idSolViatico="{$i.fk_rhc045_num_viatico}" idBeneficiario="{$i.pk_num_persona}" title="Generar Obligación"
                                                                                                descipcion="El Usuario ha GENERADO una Obligación" titulo="<i class='icm icm-spinner12'></i> Obligación">
                                                                                            <i class="icm icm-spinner12" style="color: #ffffff;"></i>
                                                                                        </button>
                                                                                    {/if}
                                                                                    {if $idViatico != 0}
                                                                                    <button class="accion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"
                                                                                            data-keyboard="false" data-backdrop="static" estado="editar" idSolViatico="{$i.fk_rhc045_num_viatico}" idViatico="{$idViatico}" idBeneficiario="{$i.pk_num_persona}" title="Editar"
                                                                                            titulo="<i class='fa fa-edit'></i> Editar Conceptos">
                                                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                                                    </button>
                                                                                        <button class="accion logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal2"
                                                                                            data-keyboard="false" data-backdrop="static" estado="calcular" idSolViatico="{$i.fk_rhc045_num_viatico}" idViatico="{$idViatico}" idBeneficiario="{$i.pk_num_persona}" title="Consultar"
                                                                                            titulo="<i class='md md-remove-red-eye'></i> Consultar Conceptos">
                                                                                        <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                                                                    </button>
                                                                                    {else}
                                                                                        <button class="accion logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal2"
                                                                                                data-keyboard="false" data-backdrop="static" estado="calcular" idSolViatico="{$i.fk_rhc045_num_viatico}" idViatico="{$idViatico}" idBeneficiario="{$i.pk_num_persona}" title="CALCULAR"
                                                                                                titulo="<i class='icm icm-calculate2'></i> Calcular Viaticos">
                                                                                            <i class="fa fa-calculator" style="color: #ffffff;"></i>
                                                                                        </button>
                                                                                        <button class="accion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"
                                                                                                data-keyboard="false" data-backdrop="static" estado="copiar" idSolViatico="{$i.fk_rhc045_num_viatico}" idBeneficiario="{$i.pk_num_persona}" title="COPIAR"
                                                                                                titulo="<i class='fa fa-copy'></i> Copiar Viaticos">
                                                                                            <i class="fa fa-copy" style="color: #ffffff;"></i>
                                                                                        </button>
                                                                                    {/if}
                                                                                </td>
                                                                            </tr>
                                                                    {/foreach}
                                                                    </tbody>
                                                                </table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="distribucion">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-head card-head-xs style-primary" align="center">
                                                    <header>Distribución Contable</header>
                                                </div>
                                                <div class="card-body" style="padding: 4px;">
                                                    <table id="" class="table table-striped table-hover" style="width: 100%;display:block;">
                                                        <thead style="display: inline-block;width: 100%;">
                                                        <tr>
                                                            <th width="200">Cuenta </th>
                                                            <th width="900">Descripcion</th>
                                                            <th width="140">Monto</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody style="height: 150px;display: inline-block;width: 100%;overflow: auto;" id="distribucionContableCuentas">
                                                        {if isset($datoContableBD)}
                                                            {$montoViat = 0}  {$grupo=0}
                                                            {foreach item=i from=$datoContableBD}
                                                                <tr id="cuentaContableCuentas{$i.pk_num_partida_presupuestaria}">
                                                                    <td width="200">
                                                                        <input type="text" style="font-size:11px;" class="form-control text-center" id="codCuenta" value="{$i.cod_cuenta}" readonly>
                                                                        <input type="text" style="font-size:11px;" class="form-control text-center" id="codCuenta20" value="{$i.cod_cuenta20}" readonly>
                                                                    </td>
                                                                    <td width="900">
                                                                        <input type="text" style="font-size:11px;" class="form-control text-center" id="nombreCuenta" value="{$i.nombreCuenta}" readonly>
                                                                        <input type="text" style="font-size:11px;" class="form-control text-center" id="nombreCuenta20" value="{$i.nombreCuenta20}" readonly>
                                                                    </td>
                                                                    <td width="150" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center" id="montoAfecto" value="{$i.monto}" readonly></td>
                                                                </tr>
                                                            {/foreach}
                                                        {/if}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-head card-head-xs style-primary" align="center">
                                                    <header>Distribución Presupuestaria</header>
                                                </div>
                                                <div class="card-body" style="padding: 4px;">
                                                    <table id="datatable" class="table table-striped table-hover" style="width: 100%;display:block;" >
                                                        <thead style="display: inline-block;width: 100%;">
                                                        <tr>
                                                            <th width="200">Partida </th>
                                                            <th width="900">Descripción</th>
                                                            <th width="140">Monto</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody style="height: 150px;display: inline-block;width: 100%;overflow: auto;" id="distribucionContablePartidas">
                                                        {if isset($datoContableBD)}
                                                            {foreach item=i from=$datoContableBD}
                                                                <tr id="cuentaContablePartidas">
                                                                    <td width="200"><input type="text" style="font-size:11px;" class="form-control text-center" id="codPartida" value="{$i.cod_partida}" readonly></td>
                                                                    <td width="900"><input type="text" style="font-size:11px;" class="form-control text-center" id="nombrePartida" value="{$i.ind_denominacion}" readonly></td>
                                                                    <td width="150"><input type="text" style="font-size:11px;" class="form-control text-center" id="montoAfecto" value="{$i.monto}" readonly></td>
                                                                </tr>
                                                            {/foreach}
                                                        {/if}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <ul class="pager wizard">
                                    <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                                    <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                                    <li class="next last"><a class="btn-raised" href="javascript:void(0);">Último</a></li>
                                    <li class="next"><a class="btn-raised" href="javascript:void(0);" id="siguiente">Siguiente</a></li>
                                </ul>
                                <div class="row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group floating-label">
                                                    <input type="text" disabled class="form-control disabled"
                                                           value="{if isset($viaticoBD.ind_usuario)}{$viaticoBD.ind_usuario}{/if}"
                                                           id="ind_usuario">
                                                    <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="col-sm-12">
                                            <div class="form-group floating-label" id="cod_tipo_nominaError">
                                                <input type="text" disabled class="form-control disabled"
                                                       value="{if isset($viaticoBD.fec_ultima_modificacion)}{$viaticoBD.fec_ultima_modificacion}{/if}"
                                                       id="fec_ultima_modificacion">
                                                <label for="cod_tipo_nomina"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default logsUsuarioModal"
            descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal">
        <i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar
    </button>
        {if isset($estado)}
            {if ($estado != 'modificar' and $estado != 'PROCESAR')}
                <button type="button" class="btn btn-primary accionesEstado logsUsuarioModal"
                        id="{if $estado == 'PR'}AN{else}RECHAZO{/if}">
                    <i class="icm icm-blocked"></i>&nbsp;
                    {if $estado == 'PR'}Anular{else}Rechazar{/if}</button>
            {/if}
        {if $estado == 'modificar'}
        <button type="button" class="btn btn-primary logsUsuarioModal" id="accion">
            <i class="fa fa-edit"></i>&nbsp;Modificar</button>
        {elseif $estado == 'PR'}
            <button type="button" class="btn btn-primary accionesEstado logsUsuarioModal" id="RE">
                <i class="icm icm-rating"></i>&nbsp;Revisar</button>
        {elseif $estado == 'RE'}
            <button type="button" class="btn btn-primary accionesEstado logsUsuarioModal" id="AP">
                <i class="icm icm-rating3"></i>&nbsp;Aprobar</button>
        {/if}
    {/if}
        {if ($ver != 2 && $ver != 1)}
            <button type="button" class="btn btn-primary logsUsuarioModal" id="accion">
                <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
            </button>
        {/if}

</div>

<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();
        app.metWizard();

        /// Complementos
        $('.select2').select2({ allowClear: true});

        // anular accion del Formulario
        $("#formAjax").submit(function () {
            return false;
        });

        // ancho de la Modal
        $('#modalAncho').css("width", "85%");

        $('#beneficiarios tbody').on('click', '.accion', function () {
            var idBeneficiario = $(this).attr('idBeneficiario');
            var accion = $(this).attr('estado');
            if(accion == 'GE'){
                $('#modalAncho2').css("width", "100%");
            }else {
                $('#modalAncho2').css("width", "80%");
            }
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post('{$_Parametros.url}modCP/viaticos/viaticosCONTROL/accionesViaticoMET',{ idSolViatico: $(this).attr('idSolViatico'), idViatico: $(this).attr('idViatico') ,idBeneficiario: idBeneficiario, accion:accion},function(dato){
                $('#ContenidoModal2').html(dato);

                var sec = 0;
                $(".cantidad"+idBeneficiario).each(function(){
                    sec = sec + 1;
                });
                var TotalTotalViatico = $('#codConcepto'+idBeneficiario+'monto').val();
                $('#totalViatico').html(TotalTotalViatico);
                var totalViatico = 0;
                for(var i = 1; i<=sec; i++) {
                    var unidadTributaria = $('#unidadTributaria').val();
                    var pkConcepto = $('#codConcepto' + i + 'pkConcepto' + idBeneficiario).val();
                    var descripcion = $('#codConcepto' + i + 'descripcion' + idBeneficiario).val();
                    var unidadViatico = $('#codConcepto' + i + 'unidadViatico' + idBeneficiario).val();
                    var monto = $('#codConcepto' + i + 'monto' + idBeneficiario).val();
                    var dias = $('#codConcepto' + i + 'dias' + idBeneficiario).val();
                    var montoTotal = $('#codConcepto' + i + 'montoTotal' + idBeneficiario).val();
                    var pkCuenta = $('#codConcepto' + i + 'pkCuenta' + idBeneficiario).val();
                    var pkCuenta20 = $('#codConcepto' + i + 'pkCuenta20' + idBeneficiario).val();
                    var pkPartida = $('#codConcepto' + i + 'pkPartida' + idBeneficiario).val();

                   $('#codConcepto'+i).remove();


                    $(document.getElementById('contenidoTabla')).append(
                            '<tr id="codConcepto' + i + '">' +
                            '<input type="hidden" class="concepto" idTr="codConcepto' + i + '" id="codConcepto' + i + 'pkConcepto" value="' + pkConcepto + '" > ' +
                            '<input type="hidden" id="codConcepto' + i + 'num_monto_viatico" value="' + pkConcepto + '" > ' +
                            '<input type="hidden" id="codConcepto' + i + 'pkCuenta" value="'+pkCuenta+'" > '+
                            '<input type="hidden" id="codConcepto' + i + 'pkCuenta20" value="'+pkCuenta20+'" > '+
                            '<input type="hidden" id="codConcepto' + i + 'pkPartida" value="'+pkPartida+'" > '+

                            '<td width="10"><input type="text" class="form-control text-center" value="' + i + '" readonly></td>' +
                            '<td width="70"><input type="text" class="form-control text-center" id="codConcepto' + i + 'concepto" value="'+descripcion+'" readonly></td>' +
                            '<td width="20"><input type="text" class="form-control text-center" id="codConcepto' + i + 'unidadViatico" value="'+unidadViatico+'" readonly></td>' +
                            '<td width="20"><input type="text" class="form-control text-center" id="codConcepto' + i + 'unidadTrib" value="' + unidadTributaria + '" readonly></td>' +
                            '<td width="20"><input type="text" class="form-control text-center monto" id="codConcepto' + i + 'monto' + idBeneficiario + '" idBenef="' + idBeneficiario + '" idTr="codConcepto' + i + '" value="' + monto + '" ></td>' +
                            '<td width="20"><input type="text" class="form-control text-center dias" id="codConcepto' + i + 'dias' + idBeneficiario + '" idBenef="' + idBeneficiario + '" idTr="codConcepto' + i + '" value="' + dias + '" ></td>' +
                            '<td width="20"><input type="text" class="form-control text-center montoTotal" id="codConcepto' + sec + 'montoTotal' + idBeneficiario + '" value="' + montoTotal + '" readonly></td>' +
                            '<td width="15" style="text-align: center">' +
                            '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="' + i + '"><i class="md md-delete"></i></button>' +
                            '</td>' +
                            '</tr>'
                    );
                }
                totalViatico = parseFloat(totalViatico) + parseFloat(montoTotal);
                $('#totalViatico').attr('value',totalViatico);
            });

        });

        $('#accion').click(function () {
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (dato) {
                var arrayCheck = [''];
                var arrayMostrarOrden = ['cod_interno', 'solicitante', 'fec_registro_viatico', 'fec_salida', 'fec_regreso', 'ind_destino', 'ind_motivo'];
                if (dato['status'] == 'error') {
                    app.metValidarError(dato, 'Disculpa. los campos marcados con X en rojo son obligatorios');
                } else if (dato['status'] == 'errorSQL') {
                    app.metValidarError(dato, 'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if (dato['status'] == 'errorConceptoViatico') {
                   swal('ERROR','Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor','error');
                } else if (dato['status'] == 'modificar') {
                    app.metActualizarRegistroTabla(dato, dato['pk_num_viaticos'], 'idViatico', arrayCheck, arrayMostrarOrden, 'El viático fue modificado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                } else if (dato['status'] == 'nuevo') {
                    app.metNuevoRegistroTabla(dato, dato['idViatico'], 'idViatico', arrayCheck, arrayMostrarOrden, 'El viático fue guardado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });

        $('.accionesEstado').click(function () {
           swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post(
                    $("#formAjax").attr("action"),
                    { idViatico: $('#idViatico').val(), estado: $(this).attr('id'), valido: 1 },
                    function(dato){
                        if (dato['status'] == 'errorSQL') {
                            app.metValidarError(dato, 'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                        }else if (dato['status'] == 'error') {
                            app.metValidarError(dato, 'Disculpa. el(los) campo(s) seleccionados con una X son Obligatorios');
                        }else if (dato['status'] == 'OK') {
                            $(document.getElementById('idViatico' + dato['idViatico'])).remove();
                            swal(dato['Mensaje']['titulo']+'!!', dato['Mensaje']['contenido'], "success");
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');
                        }
                    },'JSON');
        });

    });

</script>