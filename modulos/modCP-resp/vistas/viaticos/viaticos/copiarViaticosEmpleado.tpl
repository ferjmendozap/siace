<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body ">
                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">

                        <form action="{$_Parametros.url}modCP/viaticos/viaticosCONTROL/nuevoViaticoMET" id="formAjax"
                              class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                            <input type="hidden" value="1" name="valido"/>
                            <input type="hidden" value="{$idViatico}" id="idViatico" name="idViatico"/>
                            <input type="hidden" value="{$idBeneficiario}" id="idBeneficiario" name="idBeneficiario"/>

                            <div class="tab-content clearfix">
                                <div id="datosGenerales">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-head card-head-xs style-primary text-center">
                                                    <header class="text-center">Beneficiarios del Viatico</header>
                                                </div>
                                                <div class="card-body" style="padding: 4px;">
                                                    <div class="col-sm-12">
                                                        {$sec = 1}
                                                        {foreach item=i from=$detalleBD}
                                                            <div class="col-sm-6">
                                                                <div class="col-sm-2">
                                                                    <div class="checkbox checkbox-styled">
                                                                        <label>
                                                                            <input type="checkbox" value="1" {if $idBeneficiario == $i.pk_num_persona} disabled {else} class="beneficiariosCopiar" id="beneficiario" idBenef="{$i.pk_num_persona}" {/if}>
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-10">
                                                                    <div class="col-sm-12">
                                                                        <input type="hidden"
                                                                               id="pkBeneficiario{$i.pk_num_persona}"
                                                                               value="{$i.pk_num_persona}">

                                                                        <input type="text" class="form-control"
                                                                               id="solicitante"
                                                                               value="{if isset($i.beneficiario)}{$i.beneficiario}{/if}"
                                                                                {if $ver==1} disabled {/if}
                                                                               readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        {/foreach}
                                                        <div class="col-sm-6">

                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="concepto">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-body" style="padding: 4px;">
                                                <div class="col-sm-12">
                                                    <div class="col-sm-12">
                                                        <div class="card">
                                                            <div class="card-head card-head-xs style-primary" align="center">
                                                                <header>Conceptos de Viaticos</header>
                                                            </div>
                                                            <tr class="card-body" style="padding: 4px;">
                                                                <div id="imput_nuevo"></div>

                                                                <table class="table table-striped table-hover">
                                                                    <thead>
                                                                    <tr>
                                                                        <th scope="col" width="15">#</th>
                                                                        <th scope="col" width="70">Concepto</th>
                                                                        <th scope="col" width="20">Unid. Viático</th>
                                                                        <th scope="col" width="20">Unid. Tributaria</th>
                                                                        <th scope="col" width="20">Monto Viático</th>
                                                                        <th scope="col" width="20">Dias</th>
                                                                        <th scope="col" width="20">Total Viatico</th>
                                                                        <th scope="col" width="15"></th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody id="contenidoTabla" >

                                                                    </tbody>
                                                                    <tfoot>
                                                                    <tr>
                                                                        <th scope="col" width="15"></th>
                                                                        <th scope="col" width="70"></th>
                                                                        <th scope="col" width="20"></th>
                                                                        <th scope="col" width="20"></th>
                                                                        <th scope="col" width="20"></th>
                                                                        <th width="20"><label>Total: </label></th>
                                                                        <th width="15"><label id="totalViatico" style="font-weight:bold;" class="form-control text-center" </label></th>
                                                                        <th scope="col" width="15"></th>
                                                                    </tr>
                                                                    </tfoot>
                                                                </table>
                                                                <div class="col-sm-12" align="center">
                                                                    <button
                                                                            class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                                            type="button"
                                                                            titulo="Buscar Concepto de Viaticos"
                                                                            id="nuevoConcepto"
                                                                            idTabla="contenidoTabla"
                                                                            data-toggle="modal" data-target="#formModal3"
                                                                            url="{$_Parametros.url}modCP/viaticos/viaticosCONTROL/conceptosViaticosMET/conceptos/">
                                                                        <i class="md md-add"></i> AGREGAR
                                                                    </button>
                                                                </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default logsUsuarioModal"
            descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal">
        <i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar
    </button>
    <button type="button" class="btn btn-primary logsUsuarioModal" idEmpleado="{$solicitudDetalleBD.pk_num_persona}" id="accion2">
        <i class="fa fa-copy"></i>&nbsp;Copiar</button>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();
        app.metWizard();

        /// Complementos
        //$('.select2').select2({ allowClear: true});

        $('.accionModal').click(function () {
            var tr = $("#"+$(this).attr('idTabla')+" > tbody > tr").length + 1;
            var idBeneficiario = $('#pkBeneficiario').val();
            $('#formModalLabel3').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0,tr:tr, idBeneficiario:idBeneficiario }, function ($dato) {
                $('#ContenidoModal3').html($dato);
            });
        });

        $('#accion2').on('click', function () {

            $('.beneficiariosCopiar').each(function( titulo, valor ){
                if(this.checked) {
                    var idEmpleado = $(this).attr('idBenef');
                    var idEmpleadoCopiar = $('#idBeneficiario').val();
                    var montoVia = 0;

                    $(document.getElementById('idBeneficiarios'+idEmpleado)).append(
                            '<input type="hidden" value="' + idEmpleado + '" name="form[formula][conceptoViatico][fk_a003_persona_beneficiario][]"> '
                    );

                    $('.concepto').each(function( titulo, valor ){
                        var campo = $(this).attr('idTr');
                        var pkConcepto = $('#'+campo+'pkConcepto').val();
                        var unidadViatico = $('#'+campo+'unidadViatico').val();
                        var descripcion = $('#'+campo+'concepto').val();
                        var pkCuenta = $('#'+campo+'pkCuenta'+idEmpleadoCopiar).val();
                        var pkCuenta20 = $('#'+campo+'pkCuenta20'+idEmpleadoCopiar).val();
                        var pkPartida = $('#'+campo+'pkPartida'+idEmpleadoCopiar).val();
                        var monto = $('#' + campo + 'monto' + idEmpleadoCopiar).val();
                        var dias = $('#' + campo + 'dias' + idEmpleadoCopiar).val();
                        var montoTotal = $('#' + campo + 'montoTotal' + idEmpleadoCopiar).val();

                        montoVia = parseFloat(montoVia) + parseFloat(montoTotal);

                        $(document.getElementById(campo+'concepto'+idEmpleado)).remove();
                        $(document.getElementById(campo+'descripcion'+idEmpleado)).remove();
                        $(document.getElementById(campo+'unidadViatico'+idEmpleado)).remove();
                        $(document.getElementById(campo+'pkConcepto'+idEmpleado)).remove();
                        $(document.getElementById(campo+'pkCuenta'+idEmpleado)).remove();
                        $(document.getElementById(campo+'pkCuenta20'+idEmpleado)).remove();
                        $(document.getElementById(campo+'pkPartida'+idEmpleado)).remove();
                        $(document.getElementById(campo+'monto')).remove();
                        $(document.getElementById(campo+'dias')).remove();
                        $(document.getElementById(campo+'montoTotal')).remove();


                        $(document.getElementById('idBeneficiarios'+idEmpleado)).append(
                                '<input type="hidden" class="cantidad'+idEmpleado+'"> '+
                                '<input type="hidden" value="'+descripcion+'" id="'+campo+'descripcion'+idEmpleado+'" > '+
                                '<input type="hidden" value="'+unidadViatico+'" id="'+campo+'unidadViatico'+idEmpleado+'" > '+
                                '<input type="hidden" value="'+pkConcepto+'" id="'+campo+'pkConcepto'+idEmpleado+'" name="form[formula][conceptoViatico]['+idEmpleado+'][fk_cpb009_num_concepto_gasto_viatico][]" > '+
                                '<input type="hidden" value="'+monto+'" id="'+campo+'monto'+idEmpleado+'" name="form[formula][conceptoViatico]['+idEmpleado+']['+pkConcepto+'][num_monto_viatico]" > '+
                                '<input type="hidden" value="'+dias+'" id="'+campo+'dias'+idEmpleado+'" name="form[formula][conceptoViatico]['+idEmpleado+']['+pkConcepto+'][num_dia]" > '+
                                '<input type="hidden" value="'+montoTotal+'" id="'+campo+'montoTotal'+idEmpleado+'" name="form[formula][conceptoViatico]['+idEmpleado+']['+pkConcepto+'][num_monto_total]" > '+

                                '<input type="hidden" value="'+pkCuenta+'" id="'+campo+'pkCuenta'+idEmpleado+'" name="form[formula][conceptoViatico]['+idEmpleado+']['+pkConcepto+'][fk_cbb004_num_plan_cuenta]" > '+
                                '<input type="hidden" value="'+pkCuenta20+'" id="'+campo+'pkCuenta20'+idEmpleado+'" name="form[formula][conceptoViatico]['+idEmpleado+']['+pkConcepto+'][fk_cbb004_num_plan_cuenta_pub20]" > '+
                                '<input type="hidden" value="'+pkPartida+'" id="'+campo+'pkPartida'+idEmpleado+'" name="form[formula][conceptoViatico]['+idEmpleado+']['+pkConcepto+'][fk_prb002_num_partida_presupuestaria]" > '
                        );
                    });
                    $('#codConcepto'+idEmpleado+'monto').attr('value',montoVia);
                    $('#cerrarModal2').click();
                }
            });

        });

        // anular accion del Formulario
        $("#formAjax").submit(function () {
            return false;
        });
    });

</script>