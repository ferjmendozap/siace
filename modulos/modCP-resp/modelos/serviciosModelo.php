<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba | y.alcoba@contraloriamonagas.gob.ve        | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
require_once 'impuestoModelo.php';
class serviciosModelo extends Modelo
{
    private $atIdUsuario;
    public $atMiscelaneoModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo=new miscelaneoModelo();
        $this->atImpuestoModelo=new impuestoModelo();
    }

    public function metServiciosListar($idServicio = false)
    {
        if($idServicio){
            $where = "WHERE s.pk_num_tipo_servico = '$idServicio'";
        }else{
            $where = "";
        }
        $servicioListar =  $this->_db->query(
            "SELECT
              s.*,
              rm.ind_nombre_detalle as RegimenFiscal
             FROM
              cp_b017_tipo_servicio AS s
             INNER JOIN
              a006_miscelaneo_detalle AS rm ON rm.pk_num_miscelaneo_detalle=s.fk_a006_num_miscelaneo_regimen_fiscal
              $where
              ORDER BY cod_tipo_servicio ASC
        ");
        $servicioListar->setFetchMode(PDO::FETCH_ASSOC);
        if($idServicio){
            return $servicioListar->fetch();
        }else{
            return $servicioListar->fetchAll();
        }
    }

    public function metServiciosListarEstatus()
    {
        $servicioListar =  $this->_db->query(
            "SELECT
              s.ind_descripcion,
              s.fk_a006_num_miscelaneo_regimen_fiscal,
              s.cod_tipo_servicio,
              s.pk_num_tipo_servico,
              s.num_estatus,
              rm.txt_descripcion as RegimenFiscal
              FROM  cp_b017_tipo_servicio AS s
              INNER JOIN
              a006_miscelaneo_detalle AS rm ON rm.pk_num_miscelaneo_detalle=s.fk_a006_num_miscelaneo_regimen_fiscal
              WHERE s.num_estatus='1' "
        );
        $servicioListar->setFetchMode(PDO::FETCH_ASSOC);
        return $servicioListar->fetchAll();
    }

    public function metServicioConsulta($idServicio)
    {
        $consultaVoucher =  $this->_db->query(
            "SELECT
              s.ind_descripcion,
              s.fk_a006_num_miscelaneo_regimen_fiscal,
              s.cod_tipo_servicio,
              s.pk_num_tipo_servico,
              s.num_estatus,
              rm.ind_nombre_detalle as RegimenFiscal
              FROM  cp_b017_tipo_servicio AS s
              INNER JOIN
              a006_miscelaneo_detalle AS rm ON rm.pk_num_miscelaneo_detalle=s.fk_a006_num_miscelaneo_regimen_fiscal
              WHERE s.pk_num_tipo_servico='$idServicio'"

        );
        $consultaVoucher->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaVoucher->fetch();
    }

    public function metServicioConsultaDetalle($idServicio)
    {
        $consulta =  $this->_db->query(
            "SELECT
              s.ind_descripcion,
              s.fk_a006_num_miscelaneo_regimen_fiscal,
              s.cod_tipo_servicio,
              s.pk_num_tipo_servico,
              s.num_estatus,
              rm.ind_nombre_detalle as RegimenFiscal,
              cp_b020_servicio_impuesto.fk_cpb017_num_tipo_servico,
              cp_b020_servicio_impuesto.fk_cpb015_num_impuesto
              FROM  cp_b017_tipo_servicio AS s
              INNER JOIN
              a006_miscelaneo_detalle AS rm ON rm.pk_num_miscelaneo_detalle=s.fk_a006_num_miscelaneo_regimen_fiscal
              INNER JOIN cp_b020_servicio_impuesto ON cp_b020_servicio_impuesto.fk_cpb017_num_tipo_servico = s.pk_num_tipo_servico
              WHERE s.pk_num_tipo_servico='$idServicio'"

        );
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetch();
    }

    public function metServicioImpuesto($idServicio)
    {
        $consultaVoucher =  $this->_db->query(
            "SELECT
              *
              FROM
              cp_b020_servicio_impuesto
              WHERE
              fk_cpb017_num_tipo_servico='$idServicio'"

        );
        $consultaVoucher->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaVoucher->fetchAll();
    }

    public function metNuevoServicio($form)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "INSERT INTO
             cp_b017_tipo_servicio
              SET
               cod_tipo_servicio=:cod_tipo_servicio,
               ind_descripcion=:ind_descripcion,
               fk_a006_num_miscelaneo_regimen_fiscal=:fk_a006_num_miscelaneo_regimen_fiscal,
               num_estatus=:num_estatus
               ");
        $NuevoRegistro->execute(array(
            ':cod_tipo_servicio' => $form['cod_tipo_servicio'],
            ':ind_descripcion' => $form['ind_descripcion'],
            ':num_estatus' => $form['num_estatus'],
            ':fk_a006_num_miscelaneo_regimen_fiscal' => $form['fk_a006_num_miscelaneo_regimen_fiscal']
        ));
        $idServicio = $this->_db->lastInsertId();
        ## Impuesto
        $nuevo = $this->_db->prepare("
          INSERT INTO
          cp_b020_servicio_impuesto
          SET
          fk_cpb017_num_tipo_servico=:fk_cpb017_num_tipo_servico,
          fk_cpb015_num_impuesto=:fk_cpb015_num_impuesto
          ");
        if(isset($form['nuevo'])) {
            $detalle = $form['nuevo'];
            $secuencia = 1;
            foreach ($detalle['secuencia'] AS $i) {
                $ids = $detalle[$i];
                    $nuevo ->execute(array(
                        ':fk_cpb017_num_tipo_servico' => $idServicio,
                        ':fk_cpb015_num_impuesto' => $ids['pk_num_impuesto']
                    ));
                $secuencia++;
            }
        }
        $fallaTansaccion = $NuevoRegistro->errorInfo();
        $fallaTansaccion2 = $nuevo->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        } elseif (!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])){
        $this->_db->rollBack();
        return $fallaTansaccion2;
        }else{
            $this->_db->commit();
            return $idServicio;
        }
    }

    public function metModificarServicio($idServicio, $form)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
             cp_b017_tipo_servicio
              SET
               ind_descripcion=:ind_descripcion,
               num_estatus=:num_estatus,
               fk_a006_num_miscelaneo_regimen_fiscal=:fk_a006_num_miscelaneo_regimen_fiscal
               WHERE
               pk_num_tipo_servico='$idServicio'
               ");
        $NuevoRegistro->execute(array(
            ':ind_descripcion' => $form['ind_descripcion'],
            ':num_estatus' => $form['num_estatus'],
            ':fk_a006_num_miscelaneo_regimen_fiscal' => $form['fk_a006_num_miscelaneo_regimen_fiscal']
        ));
        #  Impuesto
        $this->_db->query("DELETE FROM cp_b020_servicio_impuesto WHERE fk_cpb017_num_tipo_servico='$idServicio'");
        if(isset($form['nuevo'])) {
            $detalle = $form['nuevo'];
            $secuencia = 1;
            foreach ($detalle['secuencia'] AS $i) {
                $ids = $detalle[$i];
                $nuevo = $this->_db->prepare("
                  INSERT INTO
                  cp_b020_servicio_impuesto
                  SET
                  fk_cpb017_num_tipo_servico=:fk_cpb017_num_tipo_servico,
                  fk_cpb015_num_impuesto=:fk_cpb015_num_impuesto
                  ");
                $nuevo ->execute(array(
                    ':fk_cpb017_num_tipo_servico' => $idServicio,
                    ':fk_cpb015_num_impuesto' => $ids['pk_num_impuesto']
                ));
                $secuencia++;
            }
        }
        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idServicio;
        }

    }

    public function metEliminar($idServicio)
    {
        $this->_db->beginTransaction();
        $elimar=$this->_db->prepare("
                DELETE FROM cp_b017_tipo_servicio WHERE pk_num_tipo_servico=:pk_num_tipo_servico
            ");
        $elimar->execute(array(
            'pk_num_tipo_servico'=>$idServicio
        ));
        $elimarImpuesto=$this->_db->prepare("
            DELETE FROM
            cp_b020_servicio_impuesto
             WHERE
            fk_cpb017_num_tipo_servico=:fk_cpb017_num_tipo_servico
              ");
        $elimarImpuesto->execute(array(
            'fk_cpb017_num_tipo_servico'=>$idServicio
        ));
        $error=$elimar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idServicio;
        }
    }
    }
