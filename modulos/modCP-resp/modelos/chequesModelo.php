<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once 'cuentasModelo.php';

class chequesModelo extends Modelo
{
    private $atIdUsuario;
    public $atMiscelaneoModelo;
    public $atCuentasModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atCuentasModelo = new cuentasModelo();
    }

    public function metListarCheques($lista=false)
    {
        if($lista=='devolver'){
            $where="WHERE cp_d010_pago.ind_estado_entrega='E'
            AND a006_miscelaneo_detalle_tipo_pago.ind_nombre_detalle = 'CHEQUE'
            AND cp_d010_pago.num_flag_cobrado=0";

        }else if ($lista=='cobrarCheque') {
            $where="WHERE cp_d010_pago.ind_estado = 'IM'
            AND cp_d010_pago.ind_estado_entrega= 'E'
            AND a006_miscelaneo_detalle_tipo_pago.ind_nombre_detalle = 'CHEQUE'
            AND cp_d010_pago.num_flag_cobrado=0 ";
        }else{
            $where="WHERE cp_d010_pago.ind_estado_entrega= 'C' AND cp_d010_pago.ind_estado = 'IM'
            AND a006_miscelaneo_detalle_tipo_pago.ind_nombre_detalle = 'CHEQUE' ";
        }
        $listar = $this->_db->query(
            "SELECT
              cp_d010_pago.pk_num_pago,
              cp_d010_pago.fec_pago,
              cp_d010_pago.ind_num_pago,
              cp_d001_obligacion.ind_nro_proceso,
                CONCAT(
                    REPLACE(FORMAT(FLOOR(ROUND(cp_d010_pago.num_monto_pago, 2)), 0), ',', '.'),
                    ',',
                    SUBSTRING_INDEX(FORMAT(ROUND(cp_d010_pago.num_monto_pago,2), 2), '.', -1)
                ) AS num_monto_pago,
              a006_miscelaneo_detalle_banco.ind_nombre_detalle AS a006_miscelaneo_detalle_banco,
              cp_b014_cuenta_bancaria.pk_num_cuenta,
              cp_b014_cuenta_bancaria.ind_num_cuenta,
              cp_d009_orden_pago.ind_num_orden,
             CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1) AS nombreProveedor
            FROM
              cp_d010_pago
            INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
            INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            INNER JOIN cp_b014_cuenta_bancaria ON cp_d001_obligacion.fk_cpb014_num_cuenta = cp_b014_cuenta_bancaria.pk_num_cuenta
            INNER JOIN a006_miscelaneo_detalle AS a006_miscelaneo_detalle_banco ON cp_b014_cuenta_bancaria.fk_a006_num_miscelaneo_detalle_banco = a006_miscelaneo_detalle_banco.pk_num_miscelaneo_detalle
            INNER JOIN a006_miscelaneo_detalle AS a006_miscelaneo_detalle_tipo_pago ON cp_d001_obligacion.fk_a006_num_miscelaneo_tipo_pago = a006_miscelaneo_detalle_tipo_pago.pk_num_miscelaneo_detalle
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor_a_pagar
            $where
            ORDER BY
            cp_b014_cuenta_bancaria.pk_num_cuenta ASC
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metConsultarPagoCheque($idPago)
    {
        $listar = $this->_db->query(
            "SELECT
              cp_d010_pago.*,
               cp_d010_pago.fec_pago as fechaPago,
              (CASE
                        WHEN cp_d010_pago.ind_estado='GE' THEN 'GENERADO'
                        WHEN cp_d010_pago.ind_estado='IM' THEN 'IMPRESO'
                        WHEN cp_d010_pago.ind_estado='AN' THEN 'ANULADO'
                END) AS ind_estado_pago,
              (CASE
                        WHEN cp_d010_pago.ind_estado_entrega='C' THEN 'CUSTODIA'
                        WHEN cp_d010_pago.ind_estado_entrega='E' THEN 'ENTREGADO'
                        WHEN cp_d010_pago.ind_estado_entrega='D' THEN 'DEVUELTO'
                END) AS ind_estado_entrega,
              cp_d001_obligacion.*,
              a018_seguridad_usuario.ind_usuario,
                CONCAT(
                    REPLACE(FORMAT(FLOOR(ROUND(cp_d010_pago.num_monto_pago, 2)), 0), ',', '.'),
                    ',',
                    SUBSTRING_INDEX(FORMAT(ROUND(cp_d010_pago.num_monto_pago,2), 2), '.', -1)
                ) AS num_monto_pago,
                CONCAT(
                    REPLACE(FORMAT(FLOOR(ROUND(cp_d001_obligacion.num_monto_obligacion, 2)), 0), ',', '.'),
                    ',',
                    SUBSTRING_INDEX(FORMAT(ROUND(cp_d001_obligacion.num_monto_obligacion,2), 2), '.', -1)
                ) AS num_monto_obligacion,
                 CONCAT(
                    REPLACE(FORMAT(FLOOR(ROUND(cp_d001_obligacion.num_monto_impuesto_otros, 2)), 0), ',', '.'),
                    ',',
                    SUBSTRING_INDEX(FORMAT(ROUND(cp_d001_obligacion.num_monto_impuesto_otros,2), 2), '.', -1)
                ) AS num_monto_impuesto_otros,
              a006_miscelaneo_detalle_banco.ind_nombre_detalle AS a006_miscelaneo_detalle_banco,
              cp_b014_cuenta_bancaria.pk_num_cuenta,
              cp_b014_cuenta_bancaria.ind_num_cuenta,
              cp_d009_orden_pago.ind_num_orden,
             CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1) AS nombreProveedor,
             (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d010_pago ON cp_d010_pago.fk_rhb001_num_empleado_anula = rh_b001_empleado.pk_num_empleado
                    WHERE
                        pk_num_pago='$idPago'
                ) AS EMPLEADO_ANULA
            FROM
              cp_d010_pago
            INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = cp_d010_pago.fk_a018_num_seguridad_usuario
            INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
            INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            INNER JOIN cp_b014_cuenta_bancaria ON cp_d001_obligacion.fk_cpb014_num_cuenta = cp_b014_cuenta_bancaria.pk_num_cuenta
            INNER JOIN a006_miscelaneo_detalle AS a006_miscelaneo_detalle_banco ON cp_b014_cuenta_bancaria.fk_a006_num_miscelaneo_detalle_banco = a006_miscelaneo_detalle_banco.pk_num_miscelaneo_detalle
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor_a_pagar
           WHERE
              cp_d010_pago.pk_num_pago='$idPago'
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metConsultarChequesBanco($filtro = false)
    {
        if($filtro=='cartera'){
            $where="AND cp_d010_pago.ind_estado != 'GE' AND cp_d010_pago.ind_estado_entrega = 'C'";
            $groupBy ='GROUP BY cp_b014_cuenta_bancaria.pk_num_cuenta';
        }elseif($filtro=='entregados'){
            $where="AND cp_d010_pago.ind_estado != 'GE' AND cp_d010_pago.ind_estado_entrega = 'E'";
            $groupBy ='GROUP BY cp_b014_cuenta_bancaria.pk_num_cuenta';
        }elseif($filtro=='cobrados'){
            $where="AND cp_d010_pago.ind_estado != 'GE' AND cp_d010_pago.ind_estado_entrega = 'E'
            AND cp_d010_pago.num_flag_cobrado = 1";
            $groupBy ='GROUP BY cp_b014_cuenta_bancaria.pk_num_cuenta';
        }elseif($filtro=='devueltos'){
            $where="AND cp_d010_pago.ind_estado != 'GE' AND cp_d010_pago.ind_estado_entrega = 'D'
            AND cp_d010_pago.num_flag_cobrado = 0";
            $groupBy ='GROUP BY cp_b014_cuenta_bancaria.pk_num_cuenta';
        }elseif($filtro=='proveedor'){
            $where="AND cp_d010_pago.ind_estado != 'GE' AND cp_d010_pago.ind_estado_entrega = 'C'";
            $groupBy ='GROUP BY a003_persona.pk_num_persona';
        }else{
            $where = '';
            $groupBy ='GROUP BY cp_b014_cuenta_bancaria.pk_num_cuenta';

        }
        $listar = $this->_db->query(
            "SELECT
              cp_b014_cuenta_bancaria.pk_num_cuenta,
              cp_b014_cuenta_bancaria.ind_num_cuenta,
              cb_b004_plan_cuenta.cod_cuenta,
              a006_miscelaneo_detalle_banco.pk_num_miscelaneo_detalle AS pk_num_miscelaneo_detalle,
              a006_miscelaneo_detalle_banco.ind_nombre_detalle AS a006_miscelaneo_detalle_banco,
              a006_miscelaneo_detalle_tipo_pago.pk_num_miscelaneo_detalle AS a006_miscelaneo_detalle_tipo_pago,
              a006_miscelaneo_detalle_banco.cod_detalle AS codigo_detalle_banco,
              CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1) AS nombreAProveedor,
              a003_persona.pk_num_persona

            FROM
              cp_d010_pago
            INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
            INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            INNER JOIN cp_b014_cuenta_bancaria ON cp_d001_obligacion.fk_cpb014_num_cuenta = cp_b014_cuenta_bancaria.pk_num_cuenta
            INNER JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_b014_cuenta_bancaria.fk_cbb004_num_plan_cuenta
            INNER JOIN a006_miscelaneo_detalle AS a006_miscelaneo_detalle_banco ON cp_b014_cuenta_bancaria.fk_a006_num_miscelaneo_detalle_banco = a006_miscelaneo_detalle_banco.pk_num_miscelaneo_detalle
            INNER JOIN a006_miscelaneo_detalle AS a006_miscelaneo_detalle_tipo_pago ON cp_d001_obligacion.fk_a006_num_miscelaneo_tipo_pago = a006_miscelaneo_detalle_tipo_pago.pk_num_miscelaneo_detalle
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor_a_pagar

            WHERE
            a006_miscelaneo_detalle_tipo_pago.ind_nombre_detalle = 'CHEQUE'
            $where
            $groupBy
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metConsultarChequesReportes($pkCuenta = false, $banco = false, $filtro = false, $persona = false)
    {
        if($filtro=='cartera'){
            $where="AND cp_b014_cuenta_bancaria.pk_num_cuenta = $pkCuenta
            AND a006_miscelaneo_detalle_banco.pk_num_miscelaneo_detalle = $banco
            AND cp_d010_pago.ind_estado != 'GE' AND cp_d010_pago.ind_estado_entrega = 'C'";
        }elseif($filtro=='entregados'){
            $where="AND cp_b014_cuenta_bancaria.pk_num_cuenta = $pkCuenta
            AND a006_miscelaneo_detalle_banco.pk_num_miscelaneo_detalle = $banco
            AND cp_d010_pago.ind_estado != 'GE' AND cp_d010_pago.ind_estado_entrega = 'E'";
        }elseif($filtro=='cobrados'){
            $where="AND cp_b014_cuenta_bancaria.pk_num_cuenta = $pkCuenta
            AND a006_miscelaneo_detalle_banco.pk_num_miscelaneo_detalle = $banco
            AND cp_d010_pago.ind_estado != 'GE' AND cp_d010_pago.ind_estado_entrega = 'E'
            AND cp_d010_pago.num_flag_cobrado = 1";
        }elseif($filtro=='devueltos'){
            $where="AND cp_b014_cuenta_bancaria.pk_num_cuenta = $pkCuenta
            AND a006_miscelaneo_detalle_banco.pk_num_miscelaneo_detalle = $banco
            AND cp_d010_pago.ind_estado != 'GE' AND cp_d010_pago.ind_estado_entrega = 'D'
            AND cp_d010_pago.num_flag_cobrado = 0";
        }elseif($filtro=='proveedor'){
            $where="AND a003_persona.pk_num_persona = $persona
            AND cp_d010_pago.ind_estado != 'GE' AND cp_d010_pago.ind_estado_entrega = 'C'";
        }else{
            $where = "AND cp_b014_cuenta_bancaria.pk_num_cuenta = $pkCuenta
            AND a006_miscelaneo_detalle_banco.pk_num_miscelaneo_detalle = $banco";
        }
        $listar = $this->_db->query(
            "SELECT
              cp_d010_pago.*,
               cp_d010_pago.fec_pago as fechaPago,
              cp_d010_pago.ind_estado as ind_estado_pago,
              cp_d001_obligacion.*,
              a006_miscelaneo_detalle_banco.ind_nombre_detalle AS a006_miscelaneo_detalle_banco,
              a006_miscelaneo_detalle_banco.pk_num_miscelaneo_detalle AS pk_num_miscelaneo_detalle,
              a006_miscelaneo_detalle_banco.cod_detalle AS codigo_detalle_banco,
              cp_b014_cuenta_bancaria.pk_num_cuenta,
              cp_b014_cuenta_bancaria.ind_num_cuenta,
              cp_d009_orden_pago.ind_num_orden,
              CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1) AS nombreAProveedor,
              cb_b004_plan_cuenta.cod_cuenta,
              DATEDIFF(NOW(), cp_d010_pago.fec_pago) AS DifDias,
              cp_d001_obligacion.ind_comentarios AS glosa

            FROM
              cp_d010_pago
            INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
            INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            INNER JOIN cp_b014_cuenta_bancaria ON cp_d001_obligacion.fk_cpb014_num_cuenta = cp_b014_cuenta_bancaria.pk_num_cuenta
            INNER JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_b014_cuenta_bancaria.fk_cbb004_num_plan_cuenta
            INNER JOIN a006_miscelaneo_detalle AS a006_miscelaneo_detalle_banco ON cp_b014_cuenta_bancaria.fk_a006_num_miscelaneo_detalle_banco = a006_miscelaneo_detalle_banco.pk_num_miscelaneo_detalle
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor_a_pagar
            INNER JOIN a006_miscelaneo_detalle AS a006_miscelaneo_detalle_tipo_pago ON cp_d001_obligacion.fk_a006_num_miscelaneo_tipo_pago = a006_miscelaneo_detalle_tipo_pago.pk_num_miscelaneo_detalle

            WHERE
            a006_miscelaneo_detalle_tipo_pago.ind_nombre_detalle = 'CHEQUE'

            $where
            ");
       // var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metProcesarEstadoEntrega($idPago,$estadoEntrega)
    {
        $this->_db->beginTransaction();
        $procesar = $this->_db->prepare(
            "UPDATE
             cp_d010_pago
             SET
              fec_entregado=NOW(),
              ind_estado_entrega=:ind_estado_entrega,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
              WHERE
              pk_num_pago='$idPago'
               ");
        $procesar->execute(array(
            ':ind_estado_entrega' => $estadoEntrega
        ));
        $fallaTansaccion = $procesar->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metSelectFlagPago($idPago)
    {
        $listar = $this->_db->query(
            "SELECT
              cp_d010_pago.num_flag_cobrado
            FROM
              cp_d010_pago
            WHERE
             cp_d010_pago.pk_num_pago = '$idPago'
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metProcesarCobrarCheque($idPago, $fechaCobrado)
    {
        $this->_db->beginTransaction();

        $flagCobrado = $this->metSelectFlagPago($idPago);
        $procesar=$this->_db->prepare(
            "UPDATE
             cp_d010_pago
             SET
              fec_cobrado=:fec_cobrado,
              num_flag_cobrado=:num_flag_cobrado,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
              WHERE
              pk_num_pago='$idPago'
               ");
        $procesar->execute(array(
            ':fec_cobrado' => $fechaCobrado,
            ':num_flag_cobrado' => '1',
        ));

        $fallaTansaccion = $procesar->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }
}
