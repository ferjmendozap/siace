<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO:Cuentas por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'ordenCompra'. DS . 'ordenCompraModelo.php';
require_once RUTA_Modulo . 'modCB' . DS . 'modelos' . DS . 'LibroContableModelo.php';
require_once RUTA_Modulo . 'modCB' . DS . 'modelos' . DS . 'ListaVoucherModelo.php';
require_once 'documentoModelo.php';
require_once 'serviciosModelo.php';
require_once 'cuentasModelo.php';
require_once 'voucherModelo.php';
require_once 'impuestoModelo.php';
require_once 'clasificacionDocumentosModelo.php';
require_once 'trait'.DS.'consultasTrait.php';
require_once 'trait'.DS.'updateTrait.php';

class obligacionModelo extends documentoModelo
{
    use consultasTrait;
    use updateTrait;

    private $atIdUsuario;
    public $atMiscelaneoModelo;
    public $atServicioModelo;
    public $atCuentasModelo;
    public $atVoucherModelo;
    public $atLibroContableModelo;
    public $atListaVoucherModelo;
    public $atImpuestoModelo;
    public $atClasifDocumentosModelo;
    public $atOrdenCompraModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atIdOrganismo = Session::metObtener('app_organismo');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atServicioModelo = new serviciosModelo();
        $this->atCuentasModelo = new cuentasModelo();
        $this->atVoucherModelo = new TipoVoucherModelo();
        $this->atLibroContableModelo = new LibroContableModelo();
        $this->atListaVoucherModelo = new ListaVoucherModelo();
        $this->atImpuestoModelo = new impuestoModelo();
        $this->atClasifDocumentosModelo = new clasificacionDocumentosModelo();
        $this->atOrdenCompraModelo = new ordenCompraModelo();
    }

    public function metOrganismo()
    {
        $organismo = $this->_db->query(
            "SELECT
                pk_num_organismo
            FROM
                a001_organismo
            INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_a001_num_organismo = a001_organismo.pk_num_organismo
            INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
            WHERE
            rh_b001_empleado.pk_num_empleado = '$this->atIdEmpleado'
	    ");
        $organismo->setFetchMode(PDO::FETCH_ASSOC);
        return $organismo->fetch();
    }

    public function metListaPersona()
    {
        $persona = $this->_db->query(
            "SELECT
                *,
                a006_miscelaneo_detalle.ind_nombre_detalle,
                a006_miscelaneo_detalle.cod_detalle AS cod
            FROM
                a003_persona
            LEFT JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = a003_persona.fk_a006_num_miscelaneo_det_tipopersona
            WHERE
            a003_persona.num_estatus=1
            ORDER BY cod  DESC"
        );
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetchAll();
    }

    public function metBuscarUnidadTrib()
    {
        $listar = $this->_db->query(
            "SELECT
                ind_valor
             FROM
                cp_b018_unidad_tributaria
             WHERE
                (SELECT MAX(ind_anio) FROM cp_b018_unidad_tributaria)
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metBuscar($tabla, $consulta, $cod, $like = false)
    {
        if($like){
            $consul = "$consulta LIKE '%$cod%'";
        }else{
            $consul = "$consulta='$cod'";
        }
        $pk = $this->_db->query("
            SELECT
                *
            FROM
                $tabla
            WHERE
                $consul
        ");
        $pk->setFetchMode(PDO::FETCH_ASSOC);
        return $pk->fetch();
    }

    public function metObligacion($pkOrden)
    {
        $pk = $this->_db->query("
            SELECT
                cp_d001_obligacion.pk_num_obligacion
            FROM
                cp_d010_pago
            INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
            INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            WHERE
                cp_d010_pago.pk_num_pago = '$pkOrden'
        ");
        $pk->setFetchMode(PDO::FETCH_ASSOC);
        return $pk->fetch();
    }

    public function metBuscarIva($idServicio)
    {
        $persona = $this->_db->query("
         SELECT
         cp_b015_impuesto.*,
         tipo.cod_detalle,
         a006_miscelaneo_detalle.cod_detalle as tipo
         FROM
         cp_b017_tipo_servicio
         INNER JOIN cp_b020_servicio_impuesto ON cp_b017_tipo_servicio.pk_num_tipo_servico = cp_b020_servicio_impuesto.fk_cpb017_num_tipo_servico
         INNER JOIN cp_b015_impuesto ON cp_b015_impuesto.pk_num_impuesto = cp_b020_servicio_impuesto.fk_cpb015_num_impuesto
         INNER JOIN a006_miscelaneo_detalle as tipo ON tipo.pk_num_miscelaneo_detalle = cp_b015_impuesto.fk_a006_num_miscelaneo_detalle_flag_imponible
         INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b015_impuesto.fk_a006_num_miscelaneo_detalle_tipo_comprobante
         WHERE
         cp_b015_impuesto.num_estatus='1'
         AND cp_b017_tipo_servicio.num_estatus='1'
         and pk_num_tipo_servico='$idServicio'
        ");
        //var_dump($persona);
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetchAll();
    }

    public function metObtenerOrganismo($idEmpleado)
    {
        $organismo = $this->_db->query("
         SELECT
            fk_a001_num_organismo,
            fk_a004_num_dependencia
         FROM
	        rh_c076_empleado_organizacion
         WHERE
	        fk_rhb001_num_empleado='$idEmpleado'
        ");
        $organismo->setFetchMode(PDO::FETCH_ASSOC);
        return $organismo->fetch();
    }

    public function metListaPartidas()
    {
        $persona = $this->_db->query("
            SELECT
              *,
              IF(cb_b004_pub20.pk_num_cuenta,cb_b004_pub20.pk_num_cuenta,'') AS pk_num_cuenta20,
              IF(cb_b004_pub20.cod_cuenta,cb_b004_pub20.cod_cuenta,'') AS cod_cuenta20,
              IF(cb_b004_pub20.ind_descripcion,'',cb_b004_pub20.ind_descripcion) AS ind_descripcion20,
              IF(cb_b004_plan_cuenta.pk_num_cuenta,cb_b004_plan_cuenta.pk_num_cuenta,'') AS pk_num_cuentaOnco,
              IF(cb_b004_plan_cuenta.cod_cuenta,cb_b004_plan_cuenta.cod_cuenta,'') AS cod_cuentaOnco,
              IF(cb_b004_plan_cuenta.ind_descripcion,'',cb_b004_plan_cuenta.ind_descripcion) AS ind_descripcionOnco
            FROM
              pr_b002_partida_presupuestaria
            INNER JOIN pr_c002_presupuesto_det ON pr_c002_presupuesto_det.fk_prb002_num_partida_presupuestaria = pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria
            LEFT JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta= pr_b002_partida_presupuestaria.fk_cbb004_num_plan_cuenta_onco
            LEFT JOIN cb_b004_plan_cuenta AS cb_b004_pub20 ON cb_b004_pub20.pk_num_cuenta = pr_b002_partida_presupuestaria.fk_cbb004_num_plan_cuenta_pub20
            WHERE
              pr_b002_partida_presupuestaria.num_estatus=1
           GROUP BY pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetchAll();
    }

    public function metBuscarPartidaCuenta($idPartida)
    {
        $año = date('Y');
        $persona = $this->_db->query("
            SELECT
              pr_b002_partida_presupuestaria.*,
              pr_c002_presupuesto_det.num_monto_compromiso,
              pr_c002_presupuesto_det.num_monto_ajustado,
              pr_c002_presupuesto_det.pk_num_presupuesto_det
            FROM
              pr_b002_partida_presupuestaria
            INNER JOIN pr_c002_presupuesto_det ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = pr_c002_presupuesto_det.fk_prb002_num_partida_presupuestaria
            INNER JOIN pr_b004_presupuesto ON pr_b004_presupuesto.pk_num_presupuesto = pr_c002_presupuesto_det.fk_prb004_num_presupuesto
            WHERE
              pk_num_partida_presupuestaria='$idPartida' AND
              pr_b004_presupuesto.fec_anio='$año' AND
              pr_b004_presupuesto.ind_estado='AP'
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }

    public function metBuscarCuenta($idCuenta)
    {
        $persona = $this->_db->query("
            SELECT
              *
            FROM
              cb_b004_plan_cuenta
            WHERE
              pk_num_cuenta='$idCuenta'
        ");
        //var_dump($persona);
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }

    public function metListaCuentas($tipoCuenta = false)
    {
        if ($tipoCuenta == 'cuentaOnco') {
            $where = "WHERE	num_flag_tipo_cuenta='2'";
        } elseif ($tipoCuenta == 'cuentaPub20') {
            $where = "WHERE	num_flag_tipo_cuenta='1'";
        } else {
            $where = "";
        }
        $persona = $this->_db->query("
            SELECT
                *
                FROM
              cb_b004_plan_cuenta
            $where
            ORDER BY cod_cuenta ASC
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetchAll();
    }

    public function metListaCC()
    {
        $centroCosto = $this->_db->query(
            "SELECT * FROM a023_centro_costo"
        );
        $centroCosto->setFetchMode(PDO::FETCH_ASSOC);
        return $centroCosto->fetchAll();
    }

    public function metListarDocumentosPersona($idPersona)
    {
        $documento = $this->_db->query("
            SELECT
              cp_b002_tipo_documento.pk_num_tipo_documento,
              cp_b002_tipo_documento.ind_descripcion
            FROM
              lg_e002_proveedor_documento
            INNER JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = lg_e002_proveedor_documento.fk_cpb002_num_tipo_documento
            INNER JOIN lg_b022_proveedor ON lg_b022_proveedor.pk_num_proveedor = lg_e002_proveedor_documento.fk_lgb022_num_proveedor
            WHERE
              fk_a003_num_persona_proveedor='$idPersona'
        ");
        $documento->setFetchMode(PDO::FETCH_ASSOC);
        return $documento->fetchAll();
    }

    public function metListarServicioPersona($idPersona)
    {
        $servicio = $this->_db->query("
            SELECT
              cp_b017_tipo_servicio.pk_num_tipo_servico,
              cp_b017_tipo_servicio.ind_descripcion
            FROM
              lg_e003_proveedor_servicio
            INNER JOIN cp_b017_tipo_servicio ON cp_b017_tipo_servicio.pk_num_tipo_servico = lg_e003_proveedor_servicio.fk_cpb017_num_tipo_servicio
            INNER JOIN lg_b022_proveedor ON lg_b022_proveedor.pk_num_proveedor = lg_e003_proveedor_servicio.fk_lgb022_num_proveedor
            WHERE
              fk_a003_num_persona_proveedor='$idPersona'
        ");
        $servicio->setFetchMode(PDO::FETCH_ASSOC);
        return $servicio->fetchAll();
    }

    public function metListarTipoPagoPersona($idPersona)
    {
        $servicio = $this->_db->query("
            SELECT
              a006_miscelaneo_detalle.pk_num_miscelaneo_detalle,
              a006_miscelaneo_detalle.ind_nombre_detalle
            FROM
              lg_b022_proveedor
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.cod_detalle = lg_b022_proveedor.cod_tipo_pago
            INNER JOIN a005_miscelaneo_maestro ON a005_miscelaneo_maestro.pk_num_miscelaneo_maestro = a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro
            WHERE
              lg_b022_proveedor.fk_a003_num_persona_proveedor = '$idPersona'
              and a005_miscelaneo_maestro.cod_maestro = 'TDPLG'
        ");
        $servicio->setFetchMode(PDO::FETCH_ASSOC);
        return $servicio->fetch();
    }

    public function metObligacionListar($estado = false, $filtro = false, $idObligacion = false)
    {
        if ($idObligacion) {
            $where = "AND cp_d001_obligacion.pk_num_obligacion = '$idObligacion'";
        } else {
            $where = "";
        }
        if ($estado) {
            if ($estado == 'RE' and Session::metObtener('REVOBLIG') == 'N') {
                $estado = " ind_estado='PR' AND";
            } else {
                $estado = " ind_estado='$estado' AND";
            }

        } elseif ($filtro) {
            $estado = $filtro;
        }
        $obligacionListar = $this->_db->query("
            SELECT
                cp_d001_obligacion.*,
                cp_d001_obligacion.ind_estado AS estado,
                cp_b002_tipo_documento.cod_tipo_documento,
                (CASE
                        WHEN cp_d001_obligacion.ind_estado='PR' THEN 'PREPARADO'
                        WHEN cp_d001_obligacion.ind_estado='RE' THEN 'REVISADO'
                        WHEN cp_d001_obligacion.ind_estado='CO' THEN 'CONFORMADO'
                        WHEN cp_d001_obligacion.ind_estado='AP' THEN 'APROBADO'
                        WHEN cp_d001_obligacion.ind_estado='PA' THEN 'PAGADO'
                        WHEN cp_d001_obligacion.ind_estado='AN' THEN 'ANULADO'
                        WHEN cp_d001_obligacion.ind_estado='GE' THEN 'GENERADO'
                END) AS ind_estado,
                 (CASE
                        WHEN cp_d001_obligacion.ind_tipo_procedencia='CP' THEN 'CP'
                        WHEN cp_d001_obligacion.ind_tipo_procedencia='NM' THEN 'NOMINA'
                        WHEN cp_d001_obligacion.ind_tipo_procedencia='VI' THEN 'VIATICOS'
                        WHEN cp_d001_obligacion.ind_tipo_procedencia='CC' THEN 'CAJA CHICA'
                        WHEN cp_d001_obligacion.ind_tipo_procedencia='GC' THEN 'G. CONTRATO'
                END) AS ind_tipo_procedencia,
                CONCAT(
                    REPLACE(FORMAT(FLOOR(ROUND(cp_d001_obligacion.num_monto_obligacion, 2)), 0), ',', '.'),
                    ',',
                    SUBSTRING_INDEX(FORMAT(ROUND(cp_d001_obligacion.num_monto_obligacion,2), 2), '.', -1)
                ) AS num_monto_obligacion,
                CONCAT_WS(' ',a003_persona.ind_nombre1, a003_persona.ind_apellido1) AS fk_a003_num_persona_proveedor,
                a003_persona.ind_tipo_persona
            FROM
              cp_d001_obligacion
              INNER JOIN cp_b002_tipo_documento ON cp_d001_obligacion.fk_cpb002_num_tipo_documento = cp_b002_tipo_documento.pk_num_tipo_documento
              INNER JOIN a003_persona ON cp_d001_obligacion.fk_a003_num_persona_proveedor = a003_persona.pk_num_persona
            WHERE
              $estado ind_estado<>'NM' $where
            ORDER BY cp_d001_obligacion.fec_documento
        ");
            //var_dump($obligacionListar);
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        if ($idObligacion) {
            return $obligacionListar->fetch();
        } else {
            return $obligacionListar->fetchAll();
        }
    }

    public function metMostrarImpuesto($idObligacion)
    {
        $consultaImpuesto = $this->_db->query(
            "SELECT
              cp_d012_obligacion_impuesto.*,
              cp_b015_impuesto.*,
              nm_b002_concepto.*
             FROM
              cp_d012_obligacion_impuesto
            INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d012_obligacion_impuesto.fk_cpd001_num_obligacion
            LEFT JOIN cp_b015_impuesto ON cp_b015_impuesto.pk_num_impuesto = cp_d012_obligacion_impuesto.fk_cpb015_num_impuesto
            LEFT JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto=cp_d012_obligacion_impuesto.fk_nmb002_num_concepto
          WHERE
             cp_d001_obligacion.pk_num_obligacion = '$idObligacion'
          ORDER BY cp_d012_obligacion_impuesto.ind_secuencia ASC
       ");
        $consultaImpuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaImpuesto->fetchAll();
    }

    public function metMostrarPartidas($idObligacion)
    {
        $consultaImpuesto = $this->_db->query(
            "SELECT
              cp_d013_obligacion_cuentas.*,
              pr_b002_partida_presupuestaria.ind_denominacion AS descripcionPartida,
              pr_b002_partida_presupuestaria.cod_partida AS codigoPartida,
              cb_b004_plan_cuenta.ind_descripcion AS descripcionCuenta,
              cb_b004_plan_cuenta.cod_cuenta AS codigoCuenta,
              cb_b004_plan_cuenta20.ind_descripcion AS descripcionCuenta20,
              cb_b004_plan_cuenta20.cod_cuenta AS codigoCuenta20
             FROM
              cp_d013_obligacion_cuentas
            INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d013_obligacion_cuentas.fk_cpd001_num_obligacion
            LEFT JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = cp_d013_obligacion_cuentas.fk_prb002_num_partida_presupuestaria
            LEFT JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_d013_obligacion_cuentas.fk_cbb004_num_cuenta
            LEFT JOIN cb_b004_plan_cuenta AS cb_b004_plan_cuenta20 ON cb_b004_plan_cuenta20.pk_num_cuenta = cp_d013_obligacion_cuentas.fk_cbb004_num_cuenta_pub20

          WHERE
             cp_d001_obligacion.pk_num_obligacion = '$idObligacion'
          ORDER BY cp_d013_obligacion_cuentas.num_secuencia ASC
       ");
        $consultaImpuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaImpuesto->fetchAll();
    }

    public function metMostrarDocumentoObligacion($idObligacion)
    {
        $consultaImpuesto = $this->_db->query(
            "SELECT
                lg_b019_orden.*,
                lg_b014_almacen.ind_descripcion,
                a006_miscelaneo_detalle.ind_nombre_detalle
            FROM
                lg_b019_orden
            INNER JOIN lg_b022_proveedor ON lg_b022_proveedor.pk_num_proveedor = lg_b019_orden.fk_lgb022_num_proveedor
            INNER JOIN lg_b014_almacen ON lg_b014_almacen.pk_num_almacen = lg_b019_orden.fk_lgb014_num_almacen
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = lg_b019_orden.fk_a006_num_miscelaneos_forma_pago
            INNER JOIN cp_d002_documento_obligacion ON lg_b019_orden.pk_num_orden = cp_d002_documento_obligacion.fk_lgb019_num_orden
            WHERE
                cp_d002_documento_obligacion.fk_cpd001_num_obligacion='$idObligacion'
       ");
        $consultaImpuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaImpuesto->fetchAll();
    }

    public function metMostrarDocumentos($proveedor)
    {
        $consultaDocumentos = $this->_db->query(
            "SELECT
                lg_b019_orden.*,
                lg_b014_almacen.ind_descripcion,
                a006_miscelaneo_detalle.ind_nombre_detalle,
                lg_b022_proveedor.pk_num_proveedor
             FROM lg_b019_orden
             INNER JOIN lg_b022_proveedor ON lg_b022_proveedor.pk_num_proveedor = lg_b019_orden.fk_lgb022_num_proveedor INNER JOIN lg_b014_almacen ON lg_b014_almacen.pk_num_almacen = lg_b019_orden.fk_lgb014_num_almacen
             INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = lg_b019_orden.fk_a006_num_miscelaneos_forma_pago
             WHERE
                lg_b022_proveedor.fk_a003_num_persona_proveedor = '$proveedor'
             AND (lg_b019_orden.ind_estado = 'AP' OR lg_b019_orden.ind_estado = 'CO')

            ");
        //var_dump($consultaDocumentos);
        $consultaDocumentos->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaDocumentos->fetchAll();
    }

    public function metMostrarDocumentosDet($idOrden, $montoAfecto = false, $tipoDocumento = false)
    {
        $and = "";
        if($montoAfecto){
            if($tipoDocumento == 'OS'){
                $and = "and lg_c009_orden_detalle.num_monto_base = $montoAfecto";
            }
        }
        $consulta = $this->_db->query(
            "SELECT
                lg_c009_orden_detalle.num_monto_base,
                lg_c009_orden_detalle.fk_prb002_partida_presupuestaria,
                lg_c009_orden_detalle.fk_cbb004_plan_cuenta_oncop,
                lg_c009_orden_detalle.fk_cbb004_plan_cuenta_pub_veinte,
                lg_c009_orden_detalle.fk_a023_num_centro_costo,
                lg_c009_orden_detalle.num_flag_exonerado,
                lg_c009_orden_detalle.num_precio_unitario_iva,
                pr_b002_partida_presupuestaria.cod_partida,
                pr_b002_partida_presupuestaria.ind_denominacion,
                cb_b004_plan_cuenta.cod_cuenta,
                cb_b004_plan_cuenta.ind_descripcion,
                cb_b004_plan_cuenta20.cod_cuenta AS cod_cuenta20,
                cb_b004_plan_cuenta20.ind_descripcion AS ind_descripcion20

            FROM
                lg_b019_orden
            INNER JOIN lg_b014_almacen ON lg_b014_almacen.pk_num_almacen = lg_b019_orden.fk_lgb014_num_almacen
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = lg_b019_orden.fk_a006_num_miscelaneos_forma_pago
            INNER JOIN lg_c009_orden_detalle ON lg_b019_orden.pk_num_orden = lg_c009_orden_detalle.fk_lgb019_num_orden
            LEFT JOIN  pr_b002_partida_presupuestaria ON lg_c009_orden_detalle.fk_prb002_partida_presupuestaria = pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria
            LEFT JOIN cb_b004_plan_cuenta ON lg_c009_orden_detalle.fk_cbb004_plan_cuenta_oncop = cb_b004_plan_cuenta.pk_num_cuenta
            LEFT JOIN cb_b004_plan_cuenta AS cb_b004_plan_cuenta20 ON lg_c009_orden_detalle.fk_cbb004_plan_cuenta_pub_veinte = cb_b004_plan_cuenta20.pk_num_cuenta

            WHERE
                lg_b019_orden.pk_num_orden='$idOrden' $and
            ");
        //var_dump($consulta);
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }

    public function metRechazarObligacion($idObligacion, $tipo, $motivoAnulacion = false, $idOrden = false)
    {
        $this->_db->beginTransaction();
        if ($tipo == 'RECHAZO') {
            $rechazarAnular = $this->metAnularRechazarObligacion($idObligacion);
            $obligacion = $this->metConsultaObligacion($idObligacion);
            if (Session::metObtener('CONTABILIDAD') == 'PUB20') {
                $anularEstadoDistribucion = $this->metAnularEstadoDistribucion($idObligacion, 'CA', false, true);
            }
            $anularEstadoDistribucion = $this->metAnularEstadoDistribucion($idObligacion, 'CO', false, $obligacion['num_flag_compromiso']);

        } else {
            $obligacion = $this->metConsultaObligacion($idObligacion);
            $procedencia = $obligacion['ind_tipo_procedencia'];
            $rechazarLG = $this->atOrdenCompraModelo->metActualizarOrden('PR', $idOrden);
            $rechazarAnular = $this->metAnularRechazarObligacion($idObligacion, $motivoAnulacion,false, $procedencia);
            $anularEstadoDistribucion = $this->metAnularEstadoDistribucion($idObligacion, false, true);
        }

        if ($rechazarAnular) {
            $this->_db->rollBack();
            return $rechazarAnular;
        } elseif ($anularEstadoDistribucion) {
            $this->_db->rollBack();
            return $anularEstadoDistribucion;
        } else {
            $this->_db->commit();
            return $idObligacion;
        }
    }

    public function metRevisarObligacion($idObligacion)
    {
        $this->_db->beginTransaction();
        $revisar = $this->_db->prepare("
                  UPDATE
                    cp_d001_obligacion
                  SET
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fk_rhb001_num_empleado_revisa='$this->atIdEmpleado',
                    fec_ultima_modificacion=NOW(),
                    fec_revision=NOW(),
                    ind_estado='RE'
                  WHERE
                    pk_num_obligacion=:pk_num_obligacion
        ");
        $revisar->execute(array(
            'pk_num_obligacion' => $idObligacion
        ));
        $error = $revisar->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idObligacion;
        }
    }

    public function metConformarObligacion($idObligacion, $tipoContabilidad)
    {
        $this->_db->beginTransaction();
        $conformar = $this->_db->prepare("
              UPDATE
                cp_d001_obligacion
              SET
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fk_rhb001_num_empleado_conforma='$this->atIdEmpleado',
                fec_ultima_modificacion=NOW(),
                fec_conformado=NOW(),
                ind_estado='CO'
              WHERE
                pk_num_obligacion=:pk_num_obligacion
    ");
        $conformar->execute(array(
            'pk_num_obligacion' => $idObligacion
        ));
        $obligacion = $this->metConsultaObligacion($idObligacion);

        $Contrato=$this->metConsultarObligacionGC($idObligacion);

        if ($obligacion['num_flag_compromiso'] == 1 && $obligacion['num_flag_presupuesto'] == 1) {
            $afectarPresupuesto = $this->_db->prepare("
                INSERT INTO
                    pr_d008_estado_distribucion
                SET
                    fec_periodo=NOW() ,
                    num_monto=:num_monto,
                    ind_tipo_distribucion='CO',
                    ind_estado='AC',
                    fk_prc002_num_presupuesto_det=:fk_prc002_num_presupuesto_det,
                    fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
                    fk_gcb001_num_contrato=:fk_gcb001_num_contrato
            ");
            $partidas = $this->metMostrarPartidas($idObligacion);
            foreach ($partidas AS $partida) {
                $idPresupuestoDet = $this->metBuscarPartidaCuenta($partida['fk_prb002_num_partida_presupuestaria']);
                if ($partida['fk_prb002_num_partida_presupuestaria'] != null) {
                    $afectarPresupuesto->execute(array(
                        'fk_cpd001_num_obligacion' => $idObligacion,
                        'num_monto' => $partida['num_monto'],
                        'fk_prc002_num_presupuesto_det' => $idPresupuestoDet['pk_num_presupuesto_det'],
                        'fk_gcb001_num_contrato'=>$Contrato['idContrato']
                    ));
                }
            }
            if ($obligacion['num_monto_impuesto'] != 0) {
                $pkPartida = $this->metBuscar('pr_b002_partida_presupuestaria', 'cod_partida', Session::metObtener('IVADEFAULT'));
                $idPresupuestoDet = $this->metBuscarPartidaCuenta($pkPartida['pk_num_partida_presupuestaria']);
                $afectarPresupuesto->execute(array(
                    'fk_cpd001_num_obligacion' => $idObligacion,
                    'num_monto' => $obligacion['num_monto_impuesto'],
                    'fk_prc002_num_presupuesto_det' => $idPresupuestoDet['pk_num_presupuesto_det'],
                    'fk_gcb001_num_contrato'=> null
                ));
            }


        }elseif ($obligacion['num_flag_compromiso'] == 0 && $obligacion['num_flag_presupuesto'] == 1 && $Contrato['idContrato']==null) {

            $documentos = $this->metMostrarDocumentoObligacion($idObligacion);
            $afectarPresupuesto = $this->_db->prepare("
                UPDATE
                    pr_d008_estado_distribucion
                SET
                    fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
                    ind_estado='AC'
                WHERE
                    fk_lgb019_num_orden=:fk_lgb019_num_orden AND
                    (fk_cpd001_num_obligacion IS NULL OR fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion) AND
                    ind_tipo_distribucion='CO'
            ");
            foreach ($documentos AS $i) {
                $afectarPresupuesto->execute(array(
                    'fk_cpd001_num_obligacion' => $idObligacion,
                    'fk_lgb019_num_orden' => $i['pk_num_orden']
                ));
            }
            /*
            $afectarPresupuesto = $this->_db->prepare("
                INSERT INTO
                    pr_d008_estado_distribucion
                SET
                    fec_periodo=NOW() ,
                    num_monto=:num_monto,
                    ind_tipo_distribucion='CO',
                    ind_estado='AC',
                    fk_prc002_num_presupuesto_det=:fk_prc002_num_presupuesto_det,
                    fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion
            ");
            if ($obligacion['num_monto_impuesto'] != 0) {
                $pkPartida = $this->metBuscar('pr_b002_partida_presupuestaria', 'cod_partida', Session::metObtener('IVADEFAULT'));
                $idPresupuestoDet = $this->metBuscarPartidaCuenta($pkPartida['pk_num_partida_presupuestaria']);
                $afectarPresupuesto->execute(array(
                    'fk_cpd001_num_obligacion' => $idObligacion,
                    'num_monto' => $obligacion['num_monto_impuesto'],
                    'fk_prc002_num_presupuesto_det' => $idPresupuestoDet['pk_num_presupuesto_det']
                ));
            }*/
        }
        //Si es Contabilidad Publicacion 20, realizar el causado.
        if ($tipoContabilidad == 'PUB20') {

            $afectarPresupuesto = $this->_db->prepare("
                 INSERT INTO
                 pr_d008_estado_distribucion
                 SET
                 fec_periodo=NOW() ,
                 num_monto=:num_monto,
                 ind_tipo_distribucion='CA',
                 ind_estado='AC',
                 fk_prc002_num_presupuesto_det=:fk_prc002_num_presupuesto_det,
                 fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
                 fk_gcb001_num_contrato=:fk_gcb001_num_contrato
             ");
            $partidas = $this->metMostrarPartidas($idObligacion);
            foreach ($partidas AS $partida) {
                $idPresupuestoDet = $this->metBuscarPartidaCuenta($partida['fk_prb002_num_partida_presupuestaria']);
                if ($partida['fk_prb002_num_partida_presupuestaria'] != null) {
                    $afectarPresupuesto->execute(array(
                        'fk_cpd001_num_obligacion' => $idObligacion,
                        'num_monto' => $partida['num_monto'],
                        'fk_prc002_num_presupuesto_det' => $idPresupuestoDet['pk_num_presupuesto_det'],
                        'fk_gcb001_num_contrato'=>$Contrato['idContrato']
                    ));
                }
            }
            if ($obligacion['num_monto_impuesto'] != 0) {
                $pkPartida = $this->metBuscar('pr_b002_partida_presupuestaria', 'cod_partida', Session::metObtener('IVADEFAULT'));
                $idPresupuestoDet = $this->metBuscarPartidaCuenta($pkPartida['pk_num_partida_presupuestaria']);
                $afectarPresupuesto->execute(array(
                    'fk_cpd001_num_obligacion' => $idObligacion,
                    'num_monto' => $obligacion['num_monto_impuesto'],
                    'fk_prc002_num_presupuesto_det' => $idPresupuestoDet['pk_num_presupuesto_det'],
                    'fk_gcb001_num_contrato'=>$Contrato['idContrato']
                ));
            }
        }

        $error = $conformar->errorInfo();
/*
        if ($obligacion['num_flag_compromiso'] == 0 && $obligacion['num_flag_presupuesto'] == 1 && $Contrato['idContrato']==null) {
            $error2 = $afectarPresupuesto->errorInfo();
        } else {
            $error2 = array(1 => null, 2 => null);
        }*/

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } elseif (!empty($error2[1]) && !empty($error2[2])) {
            $this->_db->rollBack();
            return $error2;
        } else {
            $this->_db->commit();
            return $idObligacion;
        }

    }

    // función que realiza consulta en la conformación de la obligación
    public function metConsultarObligacionGC($idObligacion)
    {
        $centroCosto = $this->_db->query(
            "SELECT
                fk_gcb001_registro_contrato as idContrato
             FROM
                gc_c001_relacion_obligacion
             WHERE
             fk_cpd001_obligacion='$idObligacion' "

        );
        $centroCosto->setFetchMode(PDO::FETCH_ASSOC);
        return $centroCosto->fetch();
    }

    public function metBuscarDistribucion($idObligacion)
    {
        $listar = $this->_db->query(
            "SELECT
                pk_num_estado_distribucion,
                num_monto,
                fk_prc002_num_presupuesto_det,
                fk_lgb019_num_orden
              FROM
                pr_d008_estado_distribucion
              WHERE
                fk_cpd001_num_obligacion='$idObligacion' AND
                ind_estado='AC' AND
                ind_tipo_distribucion='CO'
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metAprobarObligacion($idObligacion, $tipoContabilidad)
    {
        $this->_db->beginTransaction();
        $aprobar = $this->_db->prepare("
                  UPDATE
                    cp_d001_obligacion
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fk_rhb001_num_empleado_aprueba='$this->atIdEmpleado',
                    fec_ultima_modificacion=NOW(),
                    fec_aprobado=NOW(),
                    ind_estado='AP'
                  WHERE
                    pk_num_obligacion=:pk_num_obligacion
        ");
        $aprobar->execute(array(
            'pk_num_obligacion' => $idObligacion
        ));
        $secuencia = $this->metSecuencia('cp_d009_orden_pago', 'ind_num_orden');
        $datosOblig = $this->metBuscar('cp_d001_obligacion', 'pk_num_obligacion', $idObligacion);
        $ordenPago = $this->_db->prepare("
                  INSERT INTO
                    cp_d009_orden_pago
                  SET
                    fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
                    fk_cpb002_num_tipo_documento=:fk_cpb002_num_tipo_documento,
                    fk_a006_num_miscelaneo_tipo_pago=:fk_a006_num_miscelaneo_tipo_pago,
                    fk_cpb014_num_cuenta = :fk_cpb014_num_cuenta,
                    fk_a018_num_seguridad_usuario = '$this->atIdUsuario',
                    fec_anio = YEAR(NOW()),
                    fec_mes = LPAD( MONTH (NOW()),2,'0'),
                    fec_orden_pago = NOW(),
                    fec_ultima_modificacion = NOW(),
                    ind_num_orden = LPAD( '" . $secuencia['secuencia'] . "',6,'0'),
                    ind_nro_control=:ind_nro_control,
                    num_monto=:num_monto,
                    ind_comentarios=:ind_comentarios
        ");
        $ordenPago->execute(array(
            'fk_cpd001_num_obligacion' => $idObligacion,
            'fk_cpb002_num_tipo_documento' => $datosOblig['fk_cpb002_num_tipo_documento'],
            'fk_a006_num_miscelaneo_tipo_pago' => $datosOblig['fk_a006_num_miscelaneo_tipo_pago'],
            'fk_cpb014_num_cuenta' => $datosOblig['fk_cpb014_num_cuenta'],
            'ind_nro_control' => $datosOblig['ind_nro_control'],
            'num_monto' => $datosOblig['num_monto_obligacion'],
            'ind_comentarios' => $datosOblig['ind_comentarios']
        ));
        $idOrdenPago = $this->_db->lastInsertId();

        $obligacion = $this->metConsultaObligacion($idObligacion);
        if ($tipoContabilidad != 'PUB20') {
            if ($obligacion['num_flag_presupuesto'] == 1) {
                $afectarPresupuesto = $this->_db->prepare("
            INSERT INTO
                pr_d008_estado_distribucion
            SET
                fec_periodo=NOW(),
                num_monto=:num_monto,
                ind_tipo_distribucion='CA',
                ind_estado='AC',
                fk_prc002_num_presupuesto_det=:fk_prc002_num_presupuesto_det,
                fk_prd008_num_estado_distribucion=:fk_prd008_num_estado_distribucion,
                fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
                fk_lgb019_num_orden=:fk_lgb019_num_orden
        ");
                $distribucion = $this->metBuscarDistribucion($idObligacion);
                foreach ($distribucion AS $dist) {
                    $afectarPresupuesto->execute(array(
                        'fk_cpd001_num_obligacion' => $idObligacion,
                        'num_monto' => $dist['num_monto'],
                        'fk_prc002_num_presupuesto_det' => $dist['fk_prc002_num_presupuesto_det'],
                        'fk_prd008_num_estado_distribucion' => $dist['pk_num_estado_distribucion'],
                        'fk_lgb019_num_orden' => $dist['fk_lgb019_num_orden'],
                    ));
                }
            }
        }

        $contabilidades = $this->metArmarOrdenPagoContabilidadFinanciera($idObligacion);
        $ordenPagoContabilidad = $this->_db->prepare("
                  INSERT INTO
                    cp_d014_orden_pago_contabilidad
                  SET
                    fec_anio = YEAR(NOW()),
                    fk_a018_num_seguridad_usuario = '$this->atIdUsuario',
                    fec_ultima_modificacion = NOW(),
                    fk_cbb004_num_cuenta=:fk_cbb004_num_cuenta,
                    fk_cbb004_num_cuenta_pub20=:fk_cbb004_num_cuenta_pub20,
                    num_secuencia=:num_secuencia,
                    num_monto=:num_monto,
                    fk_cpd009_num_orden_pago='$idOrdenPago'
        ");
        foreach ($contabilidades AS $contabilidad) {
            $secuenciaContabilidad = $this->metSecuencia('cp_d014_orden_pago_contabilidad', 'num_secuencia', 'fk_cpd009_num_orden_pago', $idOrdenPago);
            $ordenPagoContabilidad->execute(array(
                'fk_cbb004_num_cuenta' => $contabilidad['pk_num_cuenta'],
                'fk_cbb004_num_cuenta_pub20' => $contabilidad['pk_num_cuenta20'],
                'num_secuencia' => $secuenciaContabilidad['secuencia'],
                'num_monto' => $contabilidad['MontoVoucher']
            ));
        }

        $error = $aprobar->errorInfo();
        $error1 = $ordenPago->errorInfo();
        if ($tipoContabilidad != 'PUB20' and $obligacion['num_flag_presupuesto'] == 1) {
            $error2 = $afectarPresupuesto->errorInfo();
        } else {
            $error2 = array(1 => null, 2 => null);
        }
        $error3 = $ordenPagoContabilidad->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } elseif (!empty($error1[1]) && !empty($error1[2])) {
            $this->_db->rollBack();
            return $error1;
        } elseif (!empty($error2[1]) && !empty($error2[2])) {
            $this->_db->rollBack();
            return $error2;
        } elseif (!empty($error3[1]) && !empty($error3[2])) {
            $this->_db->rollBack();
            var_dump($error3);
            return $error3;
        } else {
            $this->_db->commit();
            return $idObligacion . '-' . str_pad($secuencia['secuencia'], 6, "0", STR_PAD_LEFT);
        }
    }

    public function metBuscarMontoImpuesto($idObligacion)
    {
        $consultaImpuesto = $this->_db->query(
            "SELECT
                ABS(
                    SUM(
                        cp_d012_obligacion_impuesto.num_monto_impuesto
                    )
               ) AS Monto
            FROM
                cp_d012_obligacion_impuesto
                INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d012_obligacion_impuesto.fk_cpd001_num_obligacion

            WHERE
                cp_d001_obligacion.pk_num_obligacion =  '$idObligacion'
       ");
        $consultaImpuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaImpuesto->fetch();
    }

    public function metConsultaObligacion($idObligacion, $ordenP = false)
    {
        if ($ordenP) {
            $and = "and cp_d009_orden_pago.ind_estado!='AN'";
        } else {
            $and = '';
        }
        $obligacion = $this->_db->query(
            "SELECT
              cp_d001_obligacion.*,
              CONCAT_WS(' ',proveedor.ind_nombre1, proveedor.ind_apellido1) AS proveedor,
              proveedor.ind_tipo_persona,
              proveedor.ind_documento_fiscal AS documentoFiscal,
              CONCAT_WS(' ',proveedorPagar.ind_nombre1, proveedorPagar.ind_apellido1) AS proveedorPagar,
              a018_seguridad_usuario.ind_usuario,
              cp_b002_tipo_documento.num_flag_provision,
              cp_b002_tipo_documento.fk_cbc003_num_tipo_voucher,
              cp_b002_tipo_documento.fk_cbc003_num_tipo_voucher_ordPago,
              cp_d009_orden_pago.ind_num_orden,
              (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d001_obligacion ON rh_b001_empleado.pk_num_empleado = cp_d001_obligacion.fk_rhb001_num_empleado_ingresa
                    WHERE
                        pk_num_obligacion = '$idObligacion'
                ) AS EMPLEADO_INGRESA,
                (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d001_obligacion ON rh_b001_empleado.pk_num_empleado = cp_d001_obligacion.fk_rhb001_num_empleado_revisa
                    WHERE
                        pk_num_obligacion = '$idObligacion'
                ) AS EMPLEADO_REVISA,
                (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d001_obligacion ON rh_b001_empleado.pk_num_empleado = cp_d001_obligacion.fk_rhb001_num_empleado_conforma
                    WHERE
                        pk_num_obligacion = '$idObligacion'
                ) AS EMPLEADO_CONFORMA,
                (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d001_obligacion ON rh_b001_empleado.pk_num_empleado = cp_d001_obligacion.fk_rhb001_num_empleado_aprueba
                    WHERE
                        pk_num_obligacion = '$idObligacion'
                ) AS EMPLEADO_APRUEBA,
                (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d001_obligacion ON rh_b001_empleado.pk_num_empleado = cp_d001_obligacion.fk_rhb001_num_empleado_anula
                    WHERE
                        pk_num_obligacion = '$idObligacion'
                ) AS EMPLEADO_ANULA,
                cp_d001_obligacion.ind_tipo_procedencia

              FROM
                cp_d001_obligacion
              INNER JOIN a003_persona AS proveedor ON  proveedor.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor
              INNER JOIN a003_persona AS proveedorPagar on proveedorPagar.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor_a_pagar
              INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = cp_d001_obligacion.fk_a018_num_seguridad_usuario
              INNER JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = cp_d001_obligacion.fk_cpb002_num_tipo_documento
              LEFT JOIN cp_d009_orden_pago ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion

              WHERE pk_num_obligacion='$idObligacion' $and
            ");
        //var_dump($obligacion);
        $obligacion->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacion->fetch();
    }

    public function metNuevaObligacion($form, $procedencia, $distribucionManual = 1)
    {
        $this->_db->beginTransaction();
        $secuencia = $this->metSecuencia('cp_d001_obligacion', 'ind_nro_registro');
        if ($procedencia == 'NM') {
            $estado = 'NM';
        } else {
            $estado = 'PR';
        }
        $NuevoRegistro = $this->_db->prepare("
            INSERT INTO
                cp_d001_obligacion
            SET
                fk_a003_num_persona_proveedor=:fk_a003_num_persona_proveedor,
                fk_a003_num_persona_proveedor_a_pagar=:fk_a003_num_persona_proveedor_a_pagar,
                fk_a023_num_centro_de_costo=:fk_a023_num_centro_de_costo,
                fk_cpb002_num_tipo_documento=:fk_cpb002_num_tipo_documento,
                fk_cpb017_num_tipo_servicio=:fk_cpb017_num_tipo_servicio,
                fk_a006_num_miscelaneo_tipo_pago=:fk_a006_num_miscelaneo_tipo_pago,
                fk_cpb014_num_cuenta=:fk_cpb014_num_cuenta,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fk_rhb001_num_empleado_ingresa='$this->atIdEmpleado',
                fec_programada=:fec_programada,
                fec_factura=:fec_factura,
                fec_recepcion=:fec_recepcion,
                fec_vencimiento=:fec_vencimiento,
                fec_registro=:fec_registro,
                fec_anio=YEAR(NOW()),
                fec_mes=LPAD( MONTH (NOW()),2,'0'),
                fec_documento=NOW(),
                fec_preparacion=NOW(),
                fec_ultima_modificacion=NOW(),
                ind_nro_registro = LPAD( '" . $secuencia['secuencia'] . "',6,'0'),
                ind_nro_control=:ind_nro_control,
                ind_nro_factura=:ind_nro_factura,
                ind_comentarios=:ind_comentarios,
                ind_comentarios_adicional=:ind_comentarios_adicional,
                ind_tipo_descuento=:ind_tipo_descuento,
                ind_tipo_procedencia=:ind_tipo_procedencia,
                ind_estado='$estado',
                num_flag_afecto_IGV=:num_flag_afecto_IGV,
                num_flag_diferido=:num_flag_diferido,
                num_flag_pago_diferido=:num_flag_pago_diferido,
                num_flag_compromiso=:num_flag_compromiso,
                num_flag_presupuesto=:num_flag_presupuesto,
                num_flag_obligacion_auto=:num_flag_obligacion_auto,
                num_flag_obligacion_directa=:num_flag_obligacion_directa,
                num_flag_caja_chica=:num_flag_caja_chica,
                num_flag_pago_individual=:num_flag_pago_individual,
                num_flag_generar_pago=:num_flag_generar_pago,
                num_flag_cont_pendiente_pub_20=:num_flag_cont_pendiente_pub_20,
                num_flag_verificado=:num_flag_verificado,
                num_flag_distribucion_manual=:num_flag_distribucion_manual,
                num_flag_adelanto=:num_flag_adelanto,
                num_monto_impuesto_otros=:num_monto_impuesto_otros,
                num_monto_no_afecto=:num_monto_no_afecto,
                num_monto_afecto=:num_monto_afecto,
                num_monto_adelanto=:num_monto_adelanto,
                num_monto_impuesto=:num_monto_impuesto,
                num_monto_pago_parcial=:num_monto_pago_parcial,
                num_monto_descuento=:num_monto_descuento,
                num_monto_obligacion=:num_monto_obligacion,
                num_proceso_secuencia=:num_proceso_secuencia,
                num_contabilizacion_pendiente=:num_contabilizacion_pendiente
        ");
        $NuevoRegistro->execute(array(
            'fk_a003_num_persona_proveedor' => $form['fk_a003_num_persona_proveedor'],
            'fk_a003_num_persona_proveedor_a_pagar' => $form['fk_a003_num_persona_proveedor_a_pagar'],
            'fk_a023_num_centro_de_costo' => $form['fk_a023_num_centro_de_costo'],
            'fk_cpb002_num_tipo_documento' => $form['fk_cpb002_num_tipo_documento'],
            'fk_cpb017_num_tipo_servicio' => $form['fk_cpb017_num_tipo_servicio'],
            'fk_a006_num_miscelaneo_tipo_pago' => $form['fk_a006_num_miscelaneo_tipo_pago'],
            'fk_cpb014_num_cuenta' => $form['fk_cpb014_num_cuenta'],
            'fec_programada' => $form['fec_programada'],
            'fec_factura' => $form['fec_factura'],
            'fec_recepcion' => $form['fec_recepcion'],
            'fec_vencimiento' => $form['fec_vencimiento'],
            'fec_registro' => $form['fec_registro'],
            'ind_nro_control' => $form['ind_nro_control'],
            'ind_nro_factura' => $form['ind_nro_factura'],
            'ind_comentarios' => $form['ind_comentarios'],
            'ind_comentarios_adicional' => $form['ind_comentarios_adicional'],
            'ind_tipo_descuento' => $form['ind_tipo_descuento'],
            'ind_tipo_procedencia' => $procedencia,#  de donde viene
            'num_flag_afecto_IGV' => $form['num_flag_afecto_IGV'],
            'num_flag_diferido' => $form['num_flag_diferido'],
            'num_flag_pago_diferido' => $form['num_flag_pago_diferido'],
            'num_flag_compromiso' => $form['num_flag_compromiso'],
            'num_flag_presupuesto' => $form['num_flag_presupuesto'],
            'num_flag_obligacion_auto' => $form['num_flag_obligacion_auto'],
            'num_flag_obligacion_directa' => $form['num_flag_obligacion_directa'],
            'num_flag_caja_chica' => $form['num_flag_caja_chica'],
            'num_flag_pago_individual' => $form['num_flag_pago_individual'],
            'num_flag_generar_pago' => 0,
            'num_flag_distribucion_manual' => $distribucionManual,#no se que hace
            'num_flag_adelanto' => 0,#no se que hace
            'num_flag_verificado' => 0,#no se que hace
            'num_flag_cont_pendiente_pub_20' => 0,
            'num_contabilizacion_pendiente' => 0,
            'num_proceso_secuencia' => 1,#no se que hace
            'num_monto_obligacion' => $form['num_monto_obligacion'],
            'num_monto_impuesto_otros' => $form['num_monto_impuesto_otros'],
            'num_monto_no_afecto' => $form['num_monto_no_afecto'],
            'num_monto_afecto' => $form['num_monto_afecto'],
            'num_monto_adelanto' => $form['num_monto_adelanto'],
            'num_monto_impuesto' => $form['num_monto_impuesto'],
            'num_monto_pago_parcial' => $form['num_monto_pago_parcial'],
            'num_monto_descuento' => $form['num_monto_descuento']
        ));
        $idObligacion = $this->_db->lastInsertId();

        #PARTIDAS partidaCuenta
        $registrarCuentas = $this->_db->prepare("
            INSERT INTO
                cp_d013_obligacion_cuentas
            SET
                fk_a003_num_persona_proveedor=:fk_a003_num_persona_proveedor,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                fk_cbb004_num_cuenta=:fk_cbb004_num_cuenta,
                fk_cbb004_num_cuenta_pub20=:fk_cbb004_num_cuenta_pub20,
                fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
                fec_ultima_modificacion=NOW(),
                ind_descripcion=:ind_descripcion,
                num_flag_no_afecto=:num_flag_no_afecto,
                num_monto=:num_monto,
                num_secuencia=:num_secuencia
        ");
        $partidaCuenta = $form['partidaCuenta'];
        $secuencia = 1;
        if(isset($partidaCuenta['ind_secuencia'])){
            foreach ($partidaCuenta['ind_secuencia'] AS $i) {
                $ids = $partidaCuenta[$i];
                if ($ids['ind_descripcion'] == '') {
                    $ids['ind_descripcion'] = $form['ind_comentarios'];
                }
                if (!isset($ids['num_flag_no_afecto'])) {
                    $ids['num_flag_no_afecto'] = 0;
                }
                if ($ids['fk_cbb004_num_cuenta'] == '' OR $ids['fk_cbb004_num_cuenta'] == 0) {
                    $ids['fk_cbb004_num_cuenta'] = null;
                }
                if ($ids['fk_cbb004_num_cuenta_pub20'] == '' OR $ids['fk_cbb004_num_cuenta_pub20'] == 0) {
                    $ids['fk_cbb004_num_cuenta_pub20'] = null;
                }
                if ($ids['fk_prb002_num_partida_presupuestaria'] == '') {
                    $ids['fk_prb002_num_partida_presupuestaria'] = null;
                }
                $registrarCuentas->execute(array(
                    'fk_a003_num_persona_proveedor' => $ids['fk_a003_num_persona_proveedor'],
                    'fk_a023_num_centro_costo' => $ids['fk_a023_num_centro_costo'],
                    'fk_cbb004_num_cuenta' => $ids['fk_cbb004_num_cuenta'],
                    'fk_cbb004_num_cuenta_pub20' => $ids['fk_cbb004_num_cuenta_pub20'],
                    'fk_prb002_num_partida_presupuestaria' => $ids['fk_prb002_num_partida_presupuestaria'],
                    'fk_cpd001_num_obligacion' => $idObligacion,
                    'ind_descripcion' => $ids['ind_descripcion'],
                    'num_flag_no_afecto' => $ids['num_flag_no_afecto'],
                    'num_monto' => $ids['num_monto'],
                    'num_secuencia' => $secuencia
                ));
                $secuencia++;
            }

        }

        #Impuestos impuestosRetenciones
        $registrarImpuestos = $this->_db->prepare("
            INSERT INTO
                cp_d012_obligacion_impuesto
            SET
                fk_a003_num_persona_proveedor=:fk_a003_num_persona_proveedor,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fk_cbb004_num_cuenta=:fk_cbb004_num_cuenta,
                fk_cbb004_num_cuenta_pub20=:fk_cbb004_num_cuenta_pub20,
                fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
                fk_nmb002_num_concepto=:fk_nmb002_num_concepto,
                fk_cpb015_num_impuesto=:fk_cpb015_num_impuesto,
                fec_ultima_modificacion=NOW(),
                ind_secuencia=:ind_secuencia,
                num_monto_impuesto=:num_monto_impuesto,
                num_monto_afecto=:num_monto_afecto
        ");

        if (!isset($form['impuestosRetenciones'])) {
            $impuestoRetenciones = false;
        } else {
            $impuestoRetenciones = $form['impuestosRetenciones'];
        }
        $secuencia = 1;
        if ($impuestoRetenciones) {
            foreach ($impuestoRetenciones['ind_secuencia'] AS $i) {
                $ids = $impuestoRetenciones[$i];
                if (isset($ids['fk_nmb002_num_concepto'])) {
                    $ids['fk_cpb015_num_impuesto'] = null;
                } else {
                    $ids['fk_nmb002_num_concepto'] = null;
                }

                if ($ids['fk_cbb004_num_cuenta'] == '') {
                    $ids['fk_cbb004_num_cuenta'] = null;
                }
                if ($ids['fk_cbb004_num_cuenta_pub20'] == '') {
                    $ids['fk_cbb004_num_cuenta_pub20'] = null;
                }

                $registrarImpuestos->execute(array(
                    'fk_a003_num_persona_proveedor' => $form['fk_a003_num_persona_proveedor'],
                    'fk_cbb004_num_cuenta' => $ids['fk_cbb004_num_cuenta'],
                    'fk_cbb004_num_cuenta_pub20' => $ids['fk_cbb004_num_cuenta_pub20'],
                    'fk_cpd001_num_obligacion' => $idObligacion,
                    'fk_nmb002_num_concepto' => $ids['fk_nmb002_num_concepto'],
                    'fk_cpb015_num_impuesto' => $ids['fk_cpb015_num_impuesto'],
                    'ind_secuencia' => $secuencia,
                    'num_monto_impuesto' => $ids['num_monto_impuesto'],
                    'num_monto_afecto' => $ids['num_monto_afecto']
                ));
                $secuencia++;
            }
        }

        #Documentos
        if (!isset($form['documentos'])) {
            $documentos = false;
        } else {
            $documentos = $form['documentos'];
        }

        $registrarDocumentos = $this->_db->prepare("
            INSERT INTO
                cp_d002_documento_obligacion
            SET
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
              fk_lgb019_num_orden=:fk_lgb019_num_orden,
              fec_ultima_modificacion=NOW()
        ");
        if ($documentos) {
            foreach ($documentos['ind_secuencia'] AS $i) {
                $ids = $documentos[$i];
                $registrarDocumentos->execute(array(
                    'fk_cpd001_num_obligacion' => $idObligacion,
                    'fk_lgb019_num_orden' => $ids['fk_lgb019_num_orden'],
                ));

                if(isset($ids['pk_num_documento'])){
                    $actualizarDocumentos = $this->_db->prepare("
                        UPDATE
                            lg_e005_documento
                        SET
                          ind_estado=:ind_estado
                        WHERE
                           pk_num_documento=:pk_num_documento
                    ");
                    $actualizarDocumentos->execute(array(
                        'ind_estado' => 'GE',
                        'pk_num_documento' => $ids['pk_num_documento']
                    ));
                }
            }
        }


        $fallaTansaccion = $NuevoRegistro->errorInfo();
        $fallaTansaccion2 = $registrarCuentas->errorInfo();
        if ($impuestoRetenciones) {
            $fallaTansaccion3 = $registrarImpuestos->errorInfo();
        } else {
            $fallaTansaccion3 = array(1 => null, 2 => null);
        }
        if ($documentos) {
            $fallaTansaccion4 = $registrarDocumentos->errorInfo();
        } else {
            $fallaTansaccion4 = array(1 => null, 2 => null);
        }

        if ((!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2]))) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } elseif ((!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2]))) {
            $this->_db->rollBack();
            return $fallaTansaccion2;
        } elseif ((!empty($fallaTansaccion3[1]) && !empty($fallaTansaccion3[2]))) {
            $this->_db->rollBack();
            return $fallaTansaccion3;
        } elseif ((!empty($fallaTansaccion4[1]) && !empty($fallaTansaccion4[2]))) {
            $this->_db->rollBack();
            return $fallaTansaccion4;
        } else {
            $this->_db->commit();
            return $idObligacion;
        }
    }

    public function metModificarObligacion($idObligacion, $form, $procedencia, $distribucionManual = 1)
    {
        $this->_db->beginTransaction();
        if ($procedencia == 'NM') {
            $estado = 'NM';
        } else {
            $estado = 'PR';
        }
        $NuevoRegistro = $this->_db->prepare("
            UPDATE
                cp_d001_obligacion
            SET
                fk_a003_num_persona_proveedor=:fk_a003_num_persona_proveedor,
                fk_a003_num_persona_proveedor_a_pagar=:fk_a003_num_persona_proveedor_a_pagar,
                fk_a023_num_centro_de_costo=:fk_a023_num_centro_de_costo,
                fk_cpb002_num_tipo_documento=:fk_cpb002_num_tipo_documento,
                fk_cpb017_num_tipo_servicio=:fk_cpb017_num_tipo_servicio,
                fk_a006_num_miscelaneo_tipo_pago=:fk_a006_num_miscelaneo_tipo_pago,
                fk_cpb014_num_cuenta=:fk_cpb014_num_cuenta,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_programada=:fec_programada,
                fec_factura=:fec_factura,
                fec_recepcion=:fec_recepcion,
                fec_vencimiento=:fec_vencimiento,
                fec_registro=:fec_registro,
                fec_ultima_modificacion=NOW(),
                ind_nro_control=:ind_nro_control,
                ind_nro_factura=:ind_nro_factura,
                ind_comentarios=:ind_comentarios,
                ind_comentarios_adicional=:ind_comentarios_adicional,
                ind_tipo_descuento=:ind_tipo_descuento,
                ind_estado='$estado',
                num_flag_afecto_IGV=:num_flag_afecto_IGV,
                num_flag_diferido=:num_flag_diferido,
                num_flag_pago_diferido=:num_flag_pago_diferido,
                num_flag_compromiso=:num_flag_compromiso,
                num_flag_presupuesto=:num_flag_presupuesto,
                num_flag_obligacion_auto=:num_flag_obligacion_auto,
                num_flag_obligacion_directa=:num_flag_obligacion_directa,
                num_flag_caja_chica=:num_flag_caja_chica,
                num_flag_pago_individual=:num_flag_pago_individual,
                num_flag_generar_pago=:num_flag_generar_pago,
                num_flag_cont_pendiente_pub_20=:num_flag_cont_pendiente_pub_20,
                num_flag_verificado=:num_flag_verificado,
                num_flag_distribucion_manual=:num_flag_distribucion_manual,
                num_flag_adelanto=:num_flag_adelanto,
                num_monto_impuesto_otros=:num_monto_impuesto_otros,
                num_monto_no_afecto=:num_monto_no_afecto,
                num_monto_afecto=:num_monto_afecto,
                num_monto_adelanto=:num_monto_adelanto,
                num_monto_impuesto=:num_monto_impuesto,
                num_monto_pago_parcial=:num_monto_pago_parcial,
                num_monto_descuento=:num_monto_descuento,
                num_monto_obligacion=:num_monto_obligacion,
                num_proceso_secuencia=:num_proceso_secuencia,
                num_contabilizacion_pendiente=:num_contabilizacion_pendiente
            WHERE
              pk_num_obligacion='$idObligacion'
        ");
        $NuevoRegistro->execute(array(
            'fk_a003_num_persona_proveedor' => $form['fk_a003_num_persona_proveedor'],
            'fk_a003_num_persona_proveedor_a_pagar' => $form['fk_a003_num_persona_proveedor_a_pagar'],
            'fk_a023_num_centro_de_costo' => $form['fk_a023_num_centro_de_costo'],
            'fk_cpb002_num_tipo_documento' => $form['fk_cpb002_num_tipo_documento'],
            'fk_cpb017_num_tipo_servicio' => $form['fk_cpb017_num_tipo_servicio'],
            'fk_a006_num_miscelaneo_tipo_pago' => $form['fk_a006_num_miscelaneo_tipo_pago'],
            'fk_cpb014_num_cuenta' => $form['fk_cpb014_num_cuenta'],
            'fec_programada' => $form['fec_programada'],
            'fec_factura' => $form['fec_factura'],
            'fec_recepcion' => $form['fec_recepcion'],
            'fec_vencimiento' => $form['fec_vencimiento'],
            'fec_registro' => $form['fec_registro'],
            'ind_nro_control' => $form['ind_nro_control'],
            'ind_nro_factura' => $form['ind_nro_factura'],
            'ind_comentarios' => $form['ind_comentarios'],
            'ind_comentarios_adicional' => $form['ind_comentarios_adicional'],
            'ind_tipo_descuento' => $form['ind_tipo_descuento'],
            'num_flag_afecto_IGV' => $form['num_flag_afecto_IGV'],
            'num_flag_diferido' => $form['num_flag_diferido'],
            'num_flag_pago_diferido' => $form['num_flag_pago_diferido'],
            'num_flag_compromiso' => $form['num_flag_compromiso'],
            'num_flag_presupuesto' => $form['num_flag_presupuesto'],
            'num_flag_obligacion_auto' => $form['num_flag_obligacion_auto'],
            'num_flag_obligacion_directa' => $form['num_flag_obligacion_directa'],
            'num_flag_caja_chica' => $form['num_flag_caja_chica'],
            'num_flag_pago_individual' => $form['num_flag_pago_individual'],
            'num_flag_generar_pago' => 0,
            'num_flag_distribucion_manual' => $distribucionManual,#no se que hace
            'num_flag_adelanto' => 0,#no se que hace
            'num_flag_verificado' => 0,#no se que hace
            'num_flag_cont_pendiente_pub_20' => 0,
            'num_contabilizacion_pendiente' => 0,
            'num_proceso_secuencia' => 1,#no se que hace
            'num_monto_obligacion' => $form['num_monto_obligacion'],
            'num_monto_impuesto_otros' => $form['num_monto_impuesto_otros'],
            'num_monto_no_afecto' => $form['num_monto_no_afecto'],
            'num_monto_afecto' => $form['num_monto_afecto'],
            'num_monto_adelanto' => $form['num_monto_adelanto'],
            'num_monto_impuesto' => $form['num_monto_impuesto'],
            'num_monto_pago_parcial' => $form['num_monto_pago_parcial'],
            'num_monto_descuento' => $form['num_monto_descuento']
        ));

        #PARTIDAS partidaCuenta
        $partidaCuenta = $form['partidaCuenta'];
        $secuencia = 1;
        $this->_db->query("DELETE FROM cp_d013_obligacion_cuentas WHERE fk_cpd001_num_obligacion='$idObligacion'");
        foreach ($partidaCuenta['ind_secuencia'] AS $i) {
            $ids = $partidaCuenta[$i];
            #if (isset($ids['pk_num_obligacion_cuentas'])) {
            # $titulo = 'UPDATE';
            # $where = "WHERE fk_cpd001_num_obligacion='$idObligacion' AND pk_num_obligacion_cuentas='" . $ids['pk_num_obligacion_cuentas'] . "'";
            # } else {
            $titulo = 'INSERT INTO';
            $where = "";
            #}
            $registrarCuentas = $this->_db->prepare("
                $titulo
                    cp_d013_obligacion_cuentas
                SET
                    fk_a003_num_persona_proveedor=:fk_a003_num_persona_proveedor,
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                    fk_cbb004_num_cuenta=:fk_cbb004_num_cuenta,
                    fk_cbb004_num_cuenta_pub20=:fk_cbb004_num_cuenta_pub20,
                    fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                    fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
                    fec_ultima_modificacion=NOW(),
                    ind_descripcion=:ind_descripcion,
                    num_flag_no_afecto=:num_flag_no_afecto,
                    num_monto=:num_monto,
                    num_secuencia=:num_secuencia
                $where
            ");
            if ($ids['ind_descripcion'] == '') {
                $ids['ind_descripcion'] = $form['ind_comentarios'];
            }
            if (!isset($ids['num_flag_no_afecto'])) {
                $ids['num_flag_no_afecto'] = 0;
            }
            if ($ids['fk_cbb004_num_cuenta'] == '') {
                $ids['fk_cbb004_num_cuenta'] = null;
            }
            if ($ids['fk_cbb004_num_cuenta_pub20'] == '') {
                $ids['fk_cbb004_num_cuenta_pub20'] = null;
            }
            if ($ids['fk_prb002_num_partida_presupuestaria'] == '') {
                $ids['fk_prb002_num_partida_presupuestaria'] = null;
            }
            $registrarCuentas->execute(array(
                'fk_a003_num_persona_proveedor' => $ids['fk_a003_num_persona_proveedor'],
                'fk_a023_num_centro_costo' => $ids['fk_a023_num_centro_costo'],
                'fk_cbb004_num_cuenta' => $ids['fk_cbb004_num_cuenta'],
                'fk_cbb004_num_cuenta_pub20' => $ids['fk_cbb004_num_cuenta_pub20'],
                'fk_prb002_num_partida_presupuestaria' => $ids['fk_prb002_num_partida_presupuestaria'],
                'fk_cpd001_num_obligacion' => $idObligacion,
                'ind_descripcion' => $ids['ind_descripcion'],
                'num_flag_no_afecto' => $ids['num_flag_no_afecto'],
                'num_monto' => $ids['num_monto'],
                'num_secuencia' => $secuencia
            ));
            $secuencia++;
        }

        #Impuestos impuestosRetenciones
        if (!isset($form['impuestosRetenciones'])) {
            $impuestoRetenciones = false;
        } else {
            $impuestoRetenciones = $form['impuestosRetenciones'];
        }

        $secuencia = 1;

        $this->_db->query("DELETE FROM cp_d012_obligacion_impuesto WHERE fk_cpd001_num_obligacion='$idObligacion'");

        if ($impuestoRetenciones) {
            foreach ($impuestoRetenciones['ind_secuencia'] AS $i) {
                $ids = $impuestoRetenciones[$i];

                $registrarImpuestos = $this->_db->prepare("
                INSERT INTO
                    cp_d012_obligacion_impuesto
                SET
                    fk_a003_num_persona_proveedor=:fk_a003_num_persona_proveedor,
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fk_cbb004_num_cuenta=:fk_cbb004_num_cuenta,
                    fk_cbb004_num_cuenta_pub20=:fk_cbb004_num_cuenta_pub20,
                    fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
                    fk_nmb002_num_concepto=:fk_nmb002_num_concepto,
                    fk_cpb015_num_impuesto=:fk_cpb015_num_impuesto,
                    fec_ultima_modificacion=NOW(),
                    ind_secuencia=:ind_secuencia,
                    num_monto_impuesto=:num_monto_impuesto,
                    num_monto_afecto=:num_monto_afecto
            ");
                if (isset($ids['fk_nmb002_num_concepto'])) {
                    $ids['fk_cpb015_num_impuesto'] = null;
                } else {
                    $ids['fk_nmb002_num_concepto'] = null;
                }

                if ($ids['fk_cbb004_num_cuenta'] == '') {
                    $ids['fk_cbb004_num_cuenta'] = null;
                }
                if ($ids['fk_cbb004_num_cuenta_pub20'] == '') {
                    $ids['fk_cbb004_num_cuenta_pub20'] = null;
                }

                $registrarImpuestos->execute(array(
                    'fk_a003_num_persona_proveedor' => $form['fk_a003_num_persona_proveedor'],
                    'fk_cbb004_num_cuenta' => $ids['fk_cbb004_num_cuenta'],
                    'fk_cbb004_num_cuenta_pub20' => $ids['fk_cbb004_num_cuenta_pub20'],
                    'fk_cpd001_num_obligacion' => $idObligacion,
                    'fk_nmb002_num_concepto' => $ids['fk_nmb002_num_concepto'],
                    'fk_cpb015_num_impuesto' => $ids['fk_cpb015_num_impuesto'],
                    'ind_secuencia' => $secuencia,
                    'num_monto_impuesto' => $ids['num_monto_impuesto'],
                    'num_monto_afecto' => $ids['num_monto_afecto']
                ));
                $secuencia++;
            }
        }


        if (!isset($form['documentos'])) {
            $documentos = false;
        } else {
            $documentos = $form['documentos'];
        }

        $registrarDocumentos = $this->_db->prepare("
            INSERT INTO
                cp_d002_documento_obligacion
            SET
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
              fk_lgb019_num_orden=:fk_lgb019_num_orden,
              fec_ultima_modificacion=NOW()
        ");
        if ($documentos) {
            $this->_db->query("DELETE FROM cp_d002_documento_obligacion WHERE fk_cpd001_num_obligacion='$idObligacion'");
            foreach ($documentos['ind_secuencia'] AS $i) {
                $ids = $documentos[$i];
                $registrarDocumentos->execute(array(
                    'fk_cpd001_num_obligacion' => $idObligacion,
                    'fk_lgb019_num_orden' => $ids['fk_lgb019_num_orden'],
                ));
            }
        }

        $fallaTansaccion = $NuevoRegistro->errorInfo();
        $fallaTansaccion2 = $registrarCuentas->errorInfo();
        if ($impuestoRetenciones) {
            $fallaTansaccion3 = $registrarImpuestos->errorInfo();
        } else {
            $fallaTansaccion3 = array(1 => null, 2 => null);
        }
        if ($documentos) {
            $fallaTansaccion4 = $registrarDocumentos->errorInfo();
        } else {
            $fallaTansaccion4 = array(1 => null, 2 => null);
        }

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            var_dump($fallaTansaccion);
            return $fallaTansaccion;
        } elseif (!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])) {
            $this->_db->rollBack();
            var_dump($fallaTansaccion2);
            return $fallaTansaccion2;
        } elseif ((!empty($fallaTansaccion3[1]) && !empty($fallaTansaccion3[2]))) {
            $this->_db->rollBack();
            var_dump($fallaTansaccion3);
            return $fallaTansaccion3;
        } elseif ((!empty($fallaTansaccion4[1]) && !empty($fallaTansaccion4[2]))) {
            $this->_db->rollBack();
            return $fallaTansaccion4;
        } else {
            $this->_db->commit();
            return $idObligacion;
        }
    }

    public function metRetenciones($idObligacion, $tipoProvision)
    {
        if($tipoProvision){
            $and = "and a006_miscelaneo_detalle.cod_detalle = '$tipoProvision'";
        }else{
            $and = '';
        }
        $retencion = $this->_db->query("
              SELECT
                    ABS(SUM(cp_d012_obligacion_impuesto.num_monto_impuesto)) AS montoRetencion
                FROM
                    cp_d001_obligacion
                    INNER JOIN cp_d012_obligacion_impuesto ON cp_d001_obligacion.pk_num_obligacion = cp_d012_obligacion_impuesto.fk_cpd001_num_obligacion
                    INNER JOIN cp_b015_impuesto ON cp_d012_obligacion_impuesto.fk_cpb015_num_impuesto = cp_b015_impuesto.pk_num_impuesto
                    INNER JOIN cb_b004_plan_cuenta CUENTA ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta = CUENTA.pk_num_cuenta
                    LEFT JOIN cb_b004_plan_cuenta CUENTA20 ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = CUENTA20.pk_num_cuenta
                    INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b015_impuesto.fk_a006_num_miscelaneo_detalle_flag_provision
                WHERE
                    pk_num_obligacion = '$idObligacion' $and

        ");
        $retencion->setFetchMode(PDO::FETCH_ASSOC);
        return $retencion->fetch();
    }

    public function metRetencionesNomina($idObligacion, $tipoProceso)
    {
        $retencion = $this->_db->query("
            SELECT
                ABS(
                  SUM(cp_d012_obligacion_impuesto.num_monto_afecto)
                ) AS montoRetencion
            FROM
                cp_d001_obligacion
                INNER JOIN cp_d012_obligacion_impuesto  ON cp_d001_obligacion.pk_num_obligacion=cp_d012_obligacion_impuesto.fk_cpd001_num_obligacion
                INNER JOIN nm_b002_concepto ON cp_d012_obligacion_impuesto.fk_nmb002_num_concepto = nm_b002_concepto.pk_num_concepto
                INNER JOIN nm_d001_concepto_perfil_detalle ON nm_d001_concepto_perfil_detalle.fk_nmb002_num_concepto = nm_b002_concepto.pk_num_concepto
                INNER JOIN cb_b004_plan_cuenta CUENTA ON nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber = CUENTA.cod_cuenta

            WHERE
                pk_num_obligacion='$idObligacion' and nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso = '$tipoProceso'
        ");
        $retencion->setFetchMode(PDO::FETCH_ASSOC);
        return $retencion->fetch();
    }

    //Caja Chica
    public function metActualizarCajaChica($idCajaChica, $idObligacion)
    {
        $this->_db->beginTransaction();
        $actualizarCajaChica = $this->_db->prepare("
              UPDATE
                cp_d004_caja_chica
              SET
                fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
                ind_estado='GE'
              WHERE
                pk_num_caja_chica=:pk_num_caja_chica
        ");
        $actualizarCajaChica->execute(array(
            'pk_num_caja_chica' => $idCajaChica,
            'fk_cpd001_num_obligacion' => $idObligacion
        ));
        $fallaTansaccion = $actualizarCajaChica->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    //Viaticos
    public function metActualizarViatico($idViatico, $beneficiario, $idObligacion)
    {
        $this->_db->beginTransaction();
        $actualizarCajaChica = $this->_db->prepare("
              UPDATE
                cp_c003_viatico_detalle
              SET
                fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
                ind_estado='GE'
              WHERE
                fk_cpb011_num_viatico=:fk_cpb011_num_viatico and fk_a003_persona_beneficiario=:fk_a003_persona_beneficiario
        ");
        $actualizarCajaChica->execute(array(
            'fk_cpb011_num_viatico' => $idViatico,
            'fk_a003_persona_beneficiario' => $beneficiario,
            'fk_cpd001_num_obligacion' => $idObligacion
        ));
        $fallaTansaccion = $actualizarCajaChica->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $beneficiario;
        }
    }

    //facturacion desde logistica
    public function metBuscarFacturacion($proveedor, $clasif = false)
    {
        if($clasif){
            if($clasif == 'SER'){
                $tipoDocumento = 'OS';
            }elseif($clasif == 'ROC' or $clasif == 'RCD'){
                $tipoDocumento = 'OC';
            }else{
                $tipoDocumento = '';
            }
            $where = "and lg_e005_documento.num_tipo_documento='$tipoDocumento' ";
        }else{
            $where = "";
        }
        $registros = $this->_db->query(
            "SELECT
              lg_e005_documento.*,
              lg_b019_orden.ind_orden,
              lg_b014_almacen.cod_almacen,
              a006_miscelaneo_detalle.ind_nombre_detalle
              FROM
                lg_e005_documento
              INNER JOIN lg_b019_orden ON lg_b019_orden.pk_num_orden = lg_e005_documento.fk_lgb019_num_orden
              INNER JOIN lg_b022_proveedor ON lg_b022_proveedor.pk_num_proveedor = lg_b019_orden.fk_lgb022_num_proveedor
              INNER JOIN a003_persona ON a003_persona.pk_num_persona = lg_b022_proveedor.fk_a003_num_persona_proveedor
              INNER JOIN lg_b014_almacen ON lg_b014_almacen.pk_num_almacen = lg_b019_orden.fk_lgb014_num_almacen
              INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = lg_b019_orden.fk_a006_num_miscelaneos_forma_pago
              WHERE
                lg_e005_documento.ind_estado= 'PR' and a003_persona.pk_num_persona = '$proveedor' and lg_e005_documento.num_monto_pendiente > 0
              $where
            ");
        //var_dump($registros);
        $registros->setFetchMode(PDO::FETCH_ASSOC);
        return $registros->fetchAll();
    }

    public function metBuscarProveedorDocumento($idProveedor)
    {
        $registros = $this->_db->query(
            "SELECT
              a003_persona.pk_num_persona,
              CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1) AS proveedor,
              a003_persona.ind_documento_fiscal
             FROM
                a003_persona
             WHERE
                a003_persona.pk_num_persona = '$idProveedor'
            ");
        //var_dump($registros);
        $registros->setFetchMode(PDO::FETCH_ASSOC);
        return $registros->fetch();
    }

    public function metBuscarDocumento($idDocumento)
    {
        $registros = $this->_db->query(
            "SELECT
              lg_e005_documento.*,
              lg_b019_orden.ind_orden,
              lg_b019_orden.ind_comentario,
              lg_b019_orden.ind_tipo_orden,
              lg_b019_orden.num_monto_igv,
              lg_b014_almacen.cod_almacen,
              a006_miscelaneo_detalle.ind_nombre_detalle,
              a003_persona.pk_num_persona,
              CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1) AS proveedor,
              a003_persona.ind_documento_fiscal,
              a023_centro_costo.pk_num_centro_costo
              FROM
                lg_e005_documento
              INNER JOIN lg_b019_orden ON lg_b019_orden.pk_num_orden = lg_e005_documento.fk_lgb019_num_orden
              INNER JOIN a023_centro_costo ON a023_centro_costo.pk_num_centro_costo = lg_b019_orden.fk_a023_num_centro_costo
              INNER JOIN lg_b022_proveedor ON lg_b022_proveedor.pk_num_proveedor = lg_b019_orden.fk_lgb022_num_proveedor
              INNER JOIN a003_persona ON a003_persona.pk_num_persona = lg_b022_proveedor.fk_a003_num_persona_proveedor
              INNER JOIN lg_b014_almacen ON lg_b014_almacen.pk_num_almacen = lg_b019_orden.fk_lgb014_num_almacen
              INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = lg_b019_orden.fk_a006_num_miscelaneos_forma_pago
              WHERE
                pk_num_documento = '$idDocumento'

            ");
        //var_dump($registros);
        $registros->setFetchMode(PDO::FETCH_ASSOC);
        return $registros->fetch();
    }

    //CObtabilidad

    public function metVouchersObligacion($idObligacion, $codVocuher)
    {
        $this->_db->beginTransaction();
        $actualizarPago = $this->_db->prepare("
            UPDATE
                cp_d001_obligacion
            SET
              fk_cbb001_num_voucher_mast=:fk_cbb001_num_voucher_mast
            WHERE
              pk_num_obligacion='$idObligacion'
        ");
        $actualizarPago->execute(array(
            ':fk_cbb001_num_voucher_mast' => $codVocuher
        ));
        $fallaTansaccion = $actualizarPago->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metPeriodo($ind_mes, $fec_anio)
    {
        $sql = $this->_db->query("SELECT *
                                 FROM cb_c005_control_cierre_mensual
                                WHERE ind_mes='$ind_mes' and fec_anio='$fec_anio' and ind_estatus='A' ");
        $sql->setFetchMode(PDO::FETCH_ASSOC);
        $field = $sql->fetch();

        return $field;
    }

    public function metNuevo($data)
    {
        // consulto para verificar si el período a ingresar se encuentra creado y abierto
        $c_periodo = $this->metPeriodo($data['ind_mes'], $data['ind_anio']);

        if (isset($c_periodo['pk_num_control_cierre'])) {

            $this->_db->beginTransaction();

            $tipo_voucher = $this->_db->query("SELECT cod_voucher
                                               FROM cb_c003_tipo_voucher
                                              WHERE pk_num_voucher= '$data[fk_cbc003_num_voucher]' ");
            $tipo_voucher->setFetchMode(PDO::FETCH_ASSOC);
            $f_tipo_voucher = $tipo_voucher->fetch();

            // Busco nro voucher
            $nro_voucher = $this->_db->query("SELECT max(ind_nro_voucher) as nro_max_voucher
                                               from cb_b001_voucher
                                              where ind_mes= '$data[ind_mes]' and
                                                    ind_anio= '$data[ind_anio]' and
                                                    fk_cbb005_num_contabilidades= '$data[fk_cbb005_num_contabilidades]' and
                                                    fk_a001_num_organismo= '$data[fk_a001_num_organismo]' and
                                                    fk_cbc003_num_voucher= '$data[fk_cbc003_num_voucher]' ");
            $nro_voucher->setFetchMode(PDO::FETCH_ASSOC);
            $f_nro_voucher = $nro_voucher->fetch();

            if ($f_nro_voucher['nro_max_voucher'] != "") {
                $codigo = (int)($f_nro_voucher['nro_max_voucher'] + 1);
                $codigo = (string)str_repeat("0", 4 - strlen($codigo)) . $codigo;
                $valor_ind_voucher = $f_tipo_voucher['cod_voucher'] . "-" . $codigo;
            } else {
                $valor_ind_voucher = $f_tipo_voucher['cod_voucher'] . "-0001";
                $codigo = '0001';
            }

            // Busco PK Libro Contabilidad
            $l_contabilidad = $this->_db->query("SELECT pk_num_libro_contabilidad
                                                  FROM cb_b003_libro_contabilidad
                                                 WHERE fk_cbb002_num_libro_contable= '$data[fk_cbb002_num_libro_contable]' AND
                                                       fk_cbb005_num_contabilidades= '$data[fk_cbb005_num_contabilidades]' ");
            $l_contabilidad->setFetchMode(PDO::FETCH_ASSOC);
            $f_l_contabilidad = $l_contabilidad->fetch();

            // Insertamos datos a la tabla
            $sql = "INSERT INTO cb_b001_voucher
                           SET
                            ind_mes= '$data[ind_mes]',
                            ind_anio= '$data[ind_anio]',
                            ind_voucher='$valor_ind_voucher',
                            ind_nro_voucher='$codigo',
                            fk_a003_num_preparado_por= '$data[fk_a003_num_preparado_por]',
                            fec_fecha_preparacion= NOW(),
                            txt_titulo_voucher= '$data[txt_titulo_voucher]',
                            fec_fecha_voucher= '$data[fec_fecha_voucher]',
                            num_flag_transferencia= '$data[num_flag_transferencia]',
                            ind_estatus='MA',
                            fk_cbc002_num_sistema_fuente= '$data[fk_cbc002_num_sistema_fuente]',
                            fk_cbc003_num_voucher= '$data[fk_cbc003_num_voucher]',
                            fk_cbb005_num_contabilidades= '$data[fk_cbb005_num_contabilidades]',
                            fk_a001_num_organismo= '$data[fk_a001_num_organismo]',
                            fk_a004_num_dependencia= '$data[fk_a004_num_dependencia]',
                            fk_cbb003_num_libro_contabilidad= '" . $f_l_contabilidad['pk_num_libro_contabilidad'] . "',
                            fec_ultima_modificacion= NOW(),
                            fk_a018_num_seguridad_usuario= '$this->atIdUsuario' "; //echo $sql."\n\n";
            $db = $this->_db->prepare($sql);
            $db->execute() or die(json_encode(['status' => 'error', 'mensaje' => [$db->errorInfo()[2]]]));
            $id = $this->_db->lastInsertId();

            //cuentas
            if (isset($data['detalle_fk_cbb004_num_cuenta'])) {
                $debitos = 0;
                $creditos = 0;

                for ($i = 0; $i < count($data['detalle_fk_cbb004_num_cuenta']); $i++) {

                    $linea = $i;
                    $linea++;

                    $formato_debitos = Number:: format($data['detalle_num_debe'][$i]);
                    $formato_creditos = Number:: format($data['detalle_num_haber'][$i]);

                    $debitos += $formato_debitos;
                    $creditos += $formato_creditos;

                    ##  registro
                    $sql = "INSERT INTO cb_c001_voucher_det
                                SET
                                    ind_mes = '$data[ind_mes]',
                                    fec_anio = '$data[ind_anio]',
                                    num_linea = '$linea',
                                    num_debe = '$formato_debitos',
                                    num_haber = '$formato_creditos',
                                    ind_descripcion = '" . $data['detalle_ind_descripcion'][$i] . "',
                                    ind_estatus = 'MA',
                                    fk_a023_num_centro_costo = '" . $data['detalle_fk_a023_num_centro_costo'][$i] . "',
                                    fk_cbb001_num_voucher_mast = '$id',
                                    fk_cbb004_num_cuenta = '" . $data['detalle_fk_cbb004_num_cuenta'][$i] . "',
                                    fk_a003_num_persona = '" . $data['detalle_fk_a003_num_persona'][$i] . "',
                                    fk_cbb003_num_libro_contabilidad = '" . $f_l_contabilidad['pk_num_libro_contabilidad'] . "',
                                    fk_cbb005_num_contabilidades= '$data[fk_cbb005_num_contabilidades]',
                                    fec_ultima_modificacion = NOW(),
                                    fk_a018_num_seguridad_usuario = '$this->atIdUsuario' "; //echo $sql."\n\n";
                    $db = $this->_db->prepare($sql);
                    $db->execute() or die(json_encode(['status' => 'error', 'mensaje' => [$db->errorInfo()[2]]]));

                    $formato_creditos = 0;
                    $formato_debitos = 0;
                }
            }

            // actualizo campos
            $sql = "UPDATE cb_b001_voucher
                      SET num_creditos= '$creditos',
                          num_debitos= '$debitos'
                    WHERE pk_num_voucher_mast= '$id' ";
            $db = $this->_db->prepare($sql);
            $db->execute();


            ##  Consigna una transacción
            $this->_db->commit();

            return $id;
        } else {
            $id = "no-periodo";
            return $id;
        }
    }

    public function metVouchersPago($idOrdenPago, $codVocuher,$idObligacion = false)
    {
        $this->_db->beginTransaction();
        if($idOrdenPago){
            $actualizarPago = $this->_db->prepare("
                UPDATE
                    cp_d009_orden_pago
                SET
                  fk_cbb001_num_voucher_asiento=:fk_cbb001_num_voucher_asiento
                WHERE
                  pk_num_orden_pago='$idOrdenPago'
            ");
            $actualizarPago->execute(array(
                ':fk_cbb001_num_voucher_asiento' => $codVocuher
            ));
        }
        $actualizarOblig = $this->_db->prepare("
            UPDATE
                cp_d001_obligacion
            SET
              num_contabilizacion_pendiente=:num_contabilizacion_pendiente
            WHERE
              pk_num_obligacion='$idObligacion'
        ");
        $actualizarOblig->execute(array(
            ':num_contabilizacion_pendiente' => 1
        ));
        if($idOrdenPago) {
            $fallaTansaccion = $actualizarPago->errorInfo();
        }

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metVoucherAnulacion($idOrdenPago, $codVoucher)
    {
        $this->_db->beginTransaction();
        $actualizarPago = $this->_db->prepare("
            UPDATE
                cp_d009_orden_pago
            SET
              fk_cbb001_num_voucher_asiento_anulacion=:fk_cbb001_num_voucher_asiento_anulacion
            WHERE
              pk_num_orden_pago='$idOrdenPago'
        ");
        $actualizarPago->execute(array(
            ':fk_cbb001_num_voucher_asiento_anulacion' => $codVoucher
        ));
        if (Session::metObtener('CONTABILIDAD') == 'PUB20') {
            $actualizarOblig = $this->_db->prepare("
            UPDATE
                cp_d001_obligacion
            SET
              fk_cbb001_num_voucher_mast_anulacion=:fk_cbb001_num_voucher_mast_anulacion
            WHERE
              pk_num_obligacion='$idOrdenPago'
        ");
            $actualizarOblig->execute(array(
                ':fk_cbb001_num_voucher_asiento_anulacion' => $codVoucher
            ));
        }

        $fallaTansaccion = $actualizarPago->errorInfo();
        $fallaTansaccion2 = $actualizarOblig->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metDetalleVoucher($idVoucherObligacion)
    {
        $listar = $this->_db->query(
            "SELECT cb_c001_voucher_det.*,
                cb_b004_plan_cuenta.cod_cuenta,
                proveedor.pk_num_persona,
                CONCAT(proveedor.ind_nombre1,' ',proveedor.ind_apellido1) AS proveedor
            FROM
                cb_c001_voucher_det
            INNER JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cb_c001_voucher_det.fk_cbb004_num_cuenta
            INNER JOIN cb_b001_voucher ON cb_b001_voucher.pk_num_voucher_mast = cb_c001_voucher_det.fk_cbb001_num_voucher_mast
            INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.fk_cbb001_num_voucher_mast = cb_b001_voucher.pk_num_voucher_mast
            INNER JOIN a003_persona AS proveedor ON  proveedor.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor
            WHERE
            cb_c001_voucher_det.fk_cbb001_num_voucher_mast = '$idVoucherObligacion'
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metBuscarVoucher($idObligacion, $contabilidad)
    {
        $listar = $this->_db->query(
            "SELECT
                cb_b001_voucher.*,
                CONCAT(prepara.ind_nombre1,' ',prepara.ind_apellido1) AS personaPrepara,
                cb_b002_libro_contable.ind_descripcion AS libroContable
            FROM
                cb_b001_voucher
            INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.fk_cbb001_num_voucher_mast = cb_b001_voucher.pk_num_voucher_mast
            INNER JOIN a003_persona AS prepara ON  prepara.pk_num_persona = cb_b001_voucher.fk_a003_num_preparado_por
            INNER JOIN cb_b003_libro_contabilidad ON cb_b003_libro_contabilidad.pk_num_libro_contabilidad = cb_b001_voucher.fk_cbb003_num_libro_contabilidad
            INNER JOIN cb_b002_libro_contable ON cb_b002_libro_contable.pk_num_libro_contable = cb_b003_libro_contabilidad.pk_num_libro_contabilidad
            WHERE
            cp_d001_obligacion.pk_num_obligacion = '$idObligacion'
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }
}
