<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once 'autorizacionCajaChicaModelo.php';
require_once 'clasificacionGastoModelo.php';
require_once 'voucherModelo.php';
require_once 'serviciosModelo.php';
#require_once 'documentoModelo.php';
require_once 'obligacionModelo.php';

class listarCajaChicaModelo extends Modelo
{
    private $atIdUsuario;
    public $atIdEmpleado;
    public $atAutorizacionCajaChicaModelo;
    public $atClasificacionGastoModelo;
    public $atVoucherModelo;
    public $atServicioModelo;
    public $atDocumentoModelo;
    public $atObligacionModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');

        $this->atAutorizacionCajaChicaModelo = new autorizacionCajaChicaModelo();
        $this->atClasificacionGastoModelo = new clasificacionGastoModelo();
        $this->atVoucherModelo = new voucherModelo();
        $this->atServicioModelo = new serviciosModelo();
#        $this->atDocumentosModelo = new documentoModelo();
        $this->atObligacionModelo = new obligacionModelo();

    }
    public function metDocumentoListar()
    {
        $documentoListar =  $this->_db->query(
            "SELECT
            d.*,
            rf.ind_nombre_detalle AS regimen,
            cl.ind_nombre_detalle AS clasificacion,
            cb_c003_tipo_voucher.cod_voucher
            FROM
            cp_b002_tipo_documento AS d
            INNER JOIN a006_miscelaneo_detalle AS rf ON rf.pk_num_miscelaneo_detalle = d.fk_a006_num_miscelaneo_detalle_regimen_fiscal
            INNER JOIN a006_miscelaneo_detalle AS cl ON cl.pk_num_miscelaneo_detalle = d.fk_a006_num_miscelaneo_detalle_clasificacion
            INNER JOIN cb_c003_tipo_voucher ON cb_c003_tipo_voucher.pk_num_voucher = d.fk_cbc003_num_tipo_voucher

            ORDER BY cod_tipo_documento ASC
          ");
        //var_dump($documentoListar);
        $documentoListar->setFetchMode(PDO::FETCH_ASSOC);
        return $documentoListar->fetchAll();
    }

    public function metListarServicio($idRegimenFiscal)
    {
        $servicio = $this->_db->query(
            "SELECT
              cp_b017_tipo_servicio.*,
              a006_miscelaneo_detalle.cod_detalle
            FROM
              cp_b017_tipo_servicio
              INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b017_tipo_servicio.fk_a006_num_miscelaneo_regimen_fiscal
            WHERE
              fk_a006_num_miscelaneo_regimen_fiscal = '$idRegimenFiscal'
        ");
        $servicio->setFetchMode(PDO::FETCH_ASSOC);
        return $servicio->fetchAll();
    }

    public function metBuscarImpuesto($idServicio,$imponible)
    {
        $persona = $this->_db->query("
         SELECT
             cp_b015_impuesto.num_factor_porcentaje,
             cp_b015_impuesto.ind_signo,
             regimen.cod_detalle AS regimen,
             imponible.cod_detalle AS imponible
          FROM
            cp_b017_tipo_servicio
          INNER JOIN cp_b020_servicio_impuesto ON cp_b017_tipo_servicio.pk_num_tipo_servico = cp_b020_servicio_impuesto.fk_cpb017_num_tipo_servico
          INNER JOIN cp_b015_impuesto ON cp_b015_impuesto.pk_num_impuesto = cp_b020_servicio_impuesto.fk_cpb015_num_impuesto
          INNER JOIN a006_miscelaneo_detalle AS regimen ON regimen.pk_num_miscelaneo_detalle = cp_b017_tipo_servicio.fk_a006_num_miscelaneo_regimen_fiscal
          INNER JOIN a006_miscelaneo_detalle AS imponible ON imponible.pk_num_miscelaneo_detalle = cp_b015_impuesto.fk_a006_num_miscelaneo_detalle_flag_imponible
          WHERE
            cp_b015_impuesto.num_estatus='1' AND
            cp_b017_tipo_servicio.num_estatus='1' AND
            imponible.cod_detalle='$imponible' AND
            pk_num_tipo_servico='$idServicio'
            LIMIT 1
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();

    }

    public function metListaEmpleado(){
        $empleado = $this->_db->query(
            "SELECT
              rh_b001_empleado.*,
              a003_persona.*
            FROM rh_b001_empleado
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            WHERE rh_b001_empleado.num_estatus=1"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }

    public function metListarDependencia()
    {
        $dependencia = $this->_db->query("
          SELECT
          *
          FROM
          a004_dependencia
          ORDER BY ind_dependencia
          ");

        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $dependencia->fetchAll();
    }

    public function metListarEmpleado(){
        $empleado =  $this->_db->query(
            "SELECT
            a003_persona.*,
            rh_b001_empleado.pk_num_empleado,
            rh_c076_empleado_organizacion.fk_a004_num_dependencia AS dependencia,
            a004_dependencia.ind_dependencia as nombreDependencia,
            CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1) AS nombreEmpleado
            FROM
            rh_b001_empleado
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            LEFT JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
            INNER JOIN a004_dependencia ON a004_dependencia.pk_num_dependencia = rh_c076_empleado_organizacion.fk_a004_num_dependencia

            WHERE
            rh_b001_empleado.pk_num_empleado='$this->atIdEmpleado'
             ");
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();
    }

    public function metListarAutorizacion($idBeneficiario=false){
        if($idBeneficiario) {
            $where = "WHERE fk_rhb001_num_empleado='$idBeneficiario'";
        }else {
            $where = "WHERE fk_rhb001_num_empleado='$this->atIdEmpleado'";
        }
            $autorizacionListar = $this->_db->query(
                "SELECT
            cp_b013_cajachica_autorizacion.fk_rhb001_num_empleado,
            cp_b013_cajachica_autorizacion.num_monto_autorizado
            FROM
            cp_b013_cajachica_autorizacion
            $where
             ");
            $autorizacionListar->setFetchMode(PDO::FETCH_ASSOC);
            return $autorizacionListar->fetch();
    }

    public function metListarCajaChica($estado=false){
        if($estado){
            $where="WHERE cp_d004_caja_chica.ind_estado='$estado'";
        }else{
            $where="";
        }
        $autorizacionListar =  $this->_db->query(
            "SELECT
                cp_d004_caja_chica.*,
                cp_d001_obligacion.ind_nro_factura,
                (CASE
                        WHEN cp_d004_caja_chica.ind_estado='PR' THEN 'PREPARADO'
                        WHEN cp_d004_caja_chica.ind_estado='AP' THEN 'APROBADO'
                        WHEN cp_d004_caja_chica.ind_estado='GE' THEN 'GENERADO'
                        WHEN cp_d004_caja_chica.ind_estado='AN' THEN 'ANULADO'
                END) AS ind_estado
            FROM
            cp_d004_caja_chica
            LEFT JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d004_caja_chica.fk_cpd001_num_obligacion
            $where
            ");
        $autorizacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $autorizacionListar->fetchAll();
    }

    public function metMostrarDetalleReposicion($idCajaChica){
        $autorizacionListar =  $this->_db->query(
            "SELECT
                cp_c002_cajachica_detalle.*,
                (cp_c002_cajachica_detalle.num_monto_afecto + cp_c002_cajachica_detalle.num_monto_no_afecto) as monto,
                cp_c002_cajachica_detalle.ind_descripcion AS descripcionDetalle,
                cp_d004_caja_chica.*,
                a003_persona.pk_num_persona,
                CONCAT_WS(' ',a003_persona.ind_nombre1, a003_persona.ind_apellido1) AS persona,
                a003_persona.ind_documento_fiscal,
                a003_persona.pk_num_persona,
                cb_b004_plan_cuenta.ind_descripcion AS nombreCuenta,
                cb_b004_plan_cuenta.cod_cuenta,
                cb_b004_plan_cuenta20.cod_cuenta AS cod_cuenta20,
                cb_b004_plan_cuenta20.ind_descripcion AS nombreCuenta20,
                pr_b002_partida_presupuestaria.cod_partida,
                pr_b002_partida_presupuestaria.ind_denominacion,
                cp_b005_concepto_gasto.ind_descripcion AS nombreConcepto,
                cp_b002_tipo_documento.cod_tipo_documento AS documento,
                a023_centro_costo.ind_abreviatura AS centroCosto,
                a006_miscelaneo_detalle.cod_detalle as regimen,
                cp_b015_impuesto.num_factor_porcentaje AS ivaPorcentaje
            FROM
                cp_c002_cajachica_detalle
              INNER JOIN cp_d004_caja_chica ON cp_d004_caja_chica.pk_num_caja_chica = cp_c002_cajachica_detalle.fk_cpd004_num_caja_chica
              INNER JOIN a003_persona ON a003_persona.pk_num_persona = cp_c002_cajachica_detalle.fk_rhb001_num_empleado_proveedor
              LEFT JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_c002_cajachica_detalle.fk_cbb004_num_plan_cuenta
              LEFT JOIN cb_b004_plan_cuenta AS cb_b004_plan_cuenta20 ON cb_b004_plan_cuenta20.pk_num_cuenta = cp_c002_cajachica_detalle.fk_cbb004_num_plan_cuentaPub20
              INNER JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = cp_c002_cajachica_detalle.fk_prb002_num_partida_presupuestaria
              INNER JOIN cp_b005_concepto_gasto ON cp_b005_concepto_gasto.pk_num_concepto_gasto = cp_c002_cajachica_detalle.fk_cpb005_num_concepto_gasto
              LEFT JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = cp_c002_cajachica_detalle.fk_cpb002_num_tipo_documento
              INNER JOIN a023_centro_costo ON a023_centro_costo.pk_num_centro_costo = cp_d004_caja_chica.fk_a023_num_centro_costo
              INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_c002_cajachica_detalle.fk_a006_num_miscelaneo_regimen_fiscal
              INNER JOIN cp_b020_servicio_impuesto ON cp_b020_servicio_impuesto.fk_cpb017_num_tipo_servico = cp_c002_cajachica_detalle.fk_cpb017_num_tipo_servicio
              INNER JOIN cp_b015_impuesto ON cp_b015_impuesto.pk_num_impuesto = cp_b020_servicio_impuesto.fk_cpb015_num_impuesto
            WHERE
               cp_c002_cajachica_detalle.fk_cpd004_num_caja_chica = '$idCajaChica'
            ");
        //var_dump($autorizacionListar);
        $autorizacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $autorizacionListar->fetchAll();

    }

    public function metConsultarCajaChica($idCajaChica){
        $autorizacionListar =  $this->_db->query(
            "SELECT
            cp_d004_caja_chica.*,
            a023_centro_costo.pk_num_centro_costo,
            a018_seguridad_usuario.ind_usuario,
            cp_b004_clasificacion_gastos.ind_descripcion AS clasificacion,
            cp_b007_cuenta_bancaria_defecto.fk_cpb014_num_cuenta_bancaria AS ctaBancaria,
            cp_d001_obligacion.ind_nro_factura,
            cp_b002_tipo_documento.cod_tipo_documento,
            (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d004_caja_chica ON rh_b001_empleado.pk_num_empleado = cp_d004_caja_chica.fk_rhb001_num_empleado_prepara
                    WHERE
                        pk_num_caja_chica = '$idCajaChica'
                ) AS EMPLEADO_PREPARA,
            (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d004_caja_chica ON rh_b001_empleado.pk_num_empleado = cp_d004_caja_chica.fk_rhb001_num_empleado_aprueba
                    WHERE
                        pk_num_caja_chica = '$idCajaChica'
                ) AS EMPLEADO_APRUEBA,
            (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d004_caja_chica ON rh_b001_empleado.pk_num_empleado = cp_d004_caja_chica.fk_rhb001_num_empleado_beneficiario

                    WHERE
                        pk_num_caja_chica = '$idCajaChica'
                ) AS nombre_empleado,
                (
                SELECT
                  a003_persona.pk_num_persona
                FROM
                  rh_b001_empleado
                INNER JOIN
                  a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                INNER JOIN
                  cp_d004_caja_chica ON rh_b001_empleado.pk_num_empleado = cp_d004_caja_chica.fk_rhb001_num_empleado_beneficiario
                WHERE
                  pk_num_caja_chica = '$idCajaChica'
                ) AS pk_persona,
                (
                    SELECT
                       ind_documento_fiscal
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d004_caja_chica ON rh_b001_empleado.pk_num_empleado = cp_d004_caja_chica.fk_rhb001_num_empleado_beneficiario

                    WHERE
                        pk_num_caja_chica = '$idCajaChica'
                ) AS documentoFiscal,
            CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1) AS nombrePagar

            FROM
            cp_d004_caja_chica
            INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = cp_d004_caja_chica.fk_a018_num_seguridad_usuario
            INNER JOIN a023_centro_costo ON a023_centro_costo.pk_num_centro_costo = cp_d004_caja_chica.fk_a023_num_centro_costo
            INNER JOIN cp_b004_clasificacion_gastos ON cp_b004_clasificacion_gastos.pk_num_clasificacion_gastos = cp_d004_caja_chica.fk_cpb004_num_clasificacion_gastos
            LEFT JOIN cp_b007_cuenta_bancaria_defecto ON cp_b007_cuenta_bancaria_defecto.fk_a006_num_miscelaneo_detalle_tipo_pago = cp_d004_caja_chica.fk_a006_num_miscelaneo_detalle_tipo_pago
            INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = cp_d004_caja_chica.fk_rhb001_num_empleado_persona_pagar
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            LEFT JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d004_caja_chica.fk_cpd001_num_obligacion
            LEFT JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = cp_d001_obligacion.fk_cpb002_num_tipo_documento
            WHERE
            pk_num_caja_chica='$idCajaChica'
            ");
        //var_dump($autorizacionListar);
        $autorizacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $autorizacionListar->fetch();
    }

    public function metSecuencia($campo,$tabla)
    {
        $secuencia = $this->_db->query(
            "SELECT
                  IF(
                        MAX($campo),
                        MAX($campo)+1,
                        1
                  ) AS secuencia
            FROM $tabla"
        );
        $secuencia->setFetchMode(PDO::FETCH_ASSOC);
        return $secuencia->fetch();
    }

    public function metNuevaCajaChica($form)
    {
        $this->_db->beginTransaction();
        $secuencia=$this->metSecuencia('ind_num_caja_chica','cp_d004_caja_chica');
        $NuevoRegistro=$this->_db->prepare(
            "INSERT INTO
             cp_d004_caja_chica
              SET
              fk_a004_num_dependencia=:fk_a004_num_dependencia,
              fk_rhb001_num_empleado_beneficiario=:fk_rhb001_num_empleado_beneficiario,
              fk_rhb001_num_empleado_persona_pagar=:fk_rhb001_num_empleado_persona_pagar,
              fk_cpb004_num_clasificacion_gastos=:fk_cpb004_num_clasificacion_gastos,
              fk_a006_num_miscelaneo_detalle_tipo_pago=:fk_a006_num_miscelaneo_detalle_tipo_pago,
              fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
              ind_num_caja_chica = LPAD( '".$secuencia['secuencia']."',4,'0'),
              ind_estado='PR',
              ind_descripcion=:ind_descripcion,
              num_flag_caja_chica=:num_flag_caja_chica,
              fec_anio=:fec_anio,
              fec_mes=:fec_mes,
              num_monto_total=:num_monto_total,
              num_monto_adelantos=:num_monto_adelantos,
              num_monto_neto=:num_monto_neto,
              fec_ultima_modificacion=NOW(),
              fec_preparacion=NOW(),
              fec_pago=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fk_rhb001_num_empleado_prepara='$this->atIdEmpleado'
               ");
        $NuevoRegistro->execute(array(
            'fk_a004_num_dependencia' => $form['fk_a004_num_dependencia'],
            'fk_rhb001_num_empleado_beneficiario' => $form['fk_rhb001_num_empleado_beneficiario'],
            'fk_rhb001_num_empleado_persona_pagar' => $form['fk_rhb001_num_empleado_persona_pagar'],
            'fk_cpb004_num_clasificacion_gastos' => $form['fk_cpb004_num_clasificacion_gastos'],
            'fk_a006_num_miscelaneo_detalle_tipo_pago' => $form['fk_a006_num_miscelaneo_detalle_tipo_pago'],
            'fk_a023_num_centro_costo' => $form['fk_a023_num_centro_costo'],
            'ind_descripcion' => $form['ind_descripcion'],
            'num_flag_caja_chica' => 1,
            'fec_anio' => date('Y'),
            'fec_mes' => date('m'),
            'num_monto_total' => $form['num_monto_total'],
            'num_monto_adelantos' => 0,
            'num_monto_neto' => 0
        ));
        $idCajaChica = $this->_db->lastInsertId();

        #detalleReposicion

        $RegistrarReposicion=$this->_db->prepare("
            INSERT INTO
                cp_c002_cajachica_detalle
            SET
                fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
                fk_cbb004_num_plan_cuentaPub20=:fk_cbb004_num_plan_cuentaPub20,
				num_flag_caja_chica=:num_flag_caja_chica,
				fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
				fk_cpb017_num_tipo_servicio=:fk_cpb017_num_tipo_servicio,
				fec_anio=:fec_anio,
				num_secuencia=:num_secuencia,
				fec_fecha=NOW(),
				ind_descripcion=:ind_descripcion,
				num_linea=:num_linea,
				fk_rhb001_num_empleado_proveedor=:fk_rhb001_num_empleado_proveedor,
				fec_documento=NOW(),
				num_monto_afecto=:num_monto_afecto,
				num_monto_no_afecto=:num_monto_no_afecto,
				num_monto_impuesto=:num_monto_impuesto,
				num_monto_retencion=:num_monto_retencion,
				num_monto_pagado=:num_monto_pagado,
				num_documento=:num_documento,
				fec_periodo_registro_compra=:fec_periodo_registro_compra,
				ind_comentarios='comentarios',
				ind_num_recibo=:ind_num_recibo,
				num_flag_no_afecto_IGV=:num_flag_no_afecto_IGV,
				fk_a018_num_seguridad_usuario='$this->atIdUsuario',
				fec_ultima_modificacion=NOW(),
				fk_cpd004_num_caja_chica=:fk_cpd004_num_caja_chica,
				fk_a006_num_miscelaneo_regimen_fiscal=:fk_a006_num_miscelaneo_regimen_fiscal,
				fk_cpb002_num_tipo_documento=:fk_cpb002_num_tipo_documento,
				fk_cpb005_num_concepto_gasto=:fk_cpb005_num_concepto_gasto

               ");

        $detalleReposicion=$form['detalleReposicion'];
        $detallesReposicion=$form['detallesReposicion'];
        $secuencia=1;
        foreach($detalleReposicion['num_secuencia'] AS $i){
            $ids=$detalleReposicion[$i];
            $idsR=$detallesReposicion[$i];

            if($ids['fk_cbb004_num_plan_cuentaPub20'] == "" or $ids['fk_cbb004_num_plan_cuentaPub20'] == "error") {
                $ids['fk_cbb004_num_plan_cuentaPub20'] = null;
            }
            if($ids['fk_cbb004_num_plan_cuenta'] == "" or $ids['fk_cbb004_num_plan_cuenta'] == "error"){
                $ids['fk_cbb004_num_plan_cuenta'] = null;
            }

            $RegistrarReposicion->execute(array(
                'num_secuencia'=>$secuencia,
                'ind_descripcion'=>$idsR['ind_descripcion'],
                'num_monto_pagado'=>$ids['num_monto_pagado'],
                'num_monto_afecto'=>$ids['num_monto_afecto'],
                'num_monto_no_afecto'=>$ids['num_monto_no_afecto'],
                'num_monto_impuesto'=>$ids['num_monto_impuesto'],
                'num_monto_retencion'=>$ids['num_monto_retencion'],
                'fk_rhb001_num_empleado_proveedor'=>$ids['fk_rhb001_num_empleado_proveedor'],
                'num_flag_caja_chica'=> 0,
                'num_documento'=>$ids['num_documento'],
                'ind_num_recibo'=>$ids['ind_num_recibo'],
                'ind_num_recibo'=>$ids['ind_num_recibo'],
                'num_linea'=>1,
                'fec_periodo_registro_compra'=>1,
                'num_flag_no_afecto_IGV'=>0,
                'fk_a006_num_miscelaneo_regimen_fiscal'=>$ids['fk_a006_num_miscelaneo_regimen_fiscal'],
                'fk_cpb017_num_tipo_servicio'=>$ids['fk_cpb017_num_tipo_servicio'],
                'fk_cpb002_num_tipo_documento'=>$ids['fk_cpb002_num_tipo_documento'],
                'fk_cbb004_num_plan_cuenta'=>$ids['fk_cbb004_num_plan_cuenta'],
                'fk_cbb004_num_plan_cuentaPub20'=>$ids['fk_cbb004_num_plan_cuentaPub20'],
                'fk_prb002_num_partida_presupuestaria'=>$ids['fk_prb002_num_partida_presupuestaria'],
                'fk_cpd004_num_caja_chica'=>$idCajaChica,
                'fec_anio' => date('Y'),
                'fk_cpb005_num_concepto_gasto'=>$ids['fk_cpb005_num_concepto_gasto']
            ));
            $secuencia++;
        }

        $fallaTansaccion = $NuevoRegistro->errorInfo();
        $fallaTansaccion2 = $RegistrarReposicion->errorInfo();


        if(
            (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) ||
            (!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2]))
        ){
            $this->_db->rollBack();
            $falla  = array_merge($fallaTansaccion,$fallaTansaccion2);
            return $falla;
        }else{
            $this->_db->commit();
            return $idCajaChica;
        }
    }
    #-----------------

    public function metModificarCajaChica($idCajaChica,$form)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
                cp_d004_caja_chica
              SET
                  fk_a004_num_dependencia=:fk_a004_num_dependencia,
                  fk_rhb001_num_empleado_beneficiario=:fk_rhb001_num_empleado_beneficiario,
                  fk_rhb001_num_empleado_persona_pagar=:fk_rhb001_num_empleado_persona_pagar,
                  fk_cpb004_num_clasificacion_gastos=:fk_cpb004_num_clasificacion_gastos,
                  fk_a006_num_miscelaneo_detalle_tipo_pago=:fk_a006_num_miscelaneo_detalle_tipo_pago,
                  fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                  ind_estado='PR',
                  ind_descripcion=:ind_descripcion,
                  num_flag_caja_chica=:num_flag_caja_chica,
                  fec_anio=:fec_anio,
                  fec_mes=:fec_mes,
                  num_monto_total=:num_monto_total,
                  num_monto_adelantos=:num_monto_adelantos,
                  num_monto_neto=:num_monto_neto,
                  fec_ultima_modificacion=NOW(),
                  fec_pago=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                  fk_rhb001_num_empleado_prepara='$this->atIdEmpleado'
              WHERE
                pk_num_caja_chica='$idCajaChica'

               ");
        $NuevoRegistro->execute(array(
            'fk_a004_num_dependencia' => $form['fk_a004_num_dependencia'],
            'fk_rhb001_num_empleado_beneficiario' => $form['fk_rhb001_num_empleado_beneficiario'],
            'fk_rhb001_num_empleado_persona_pagar' => $form['fk_rhb001_num_empleado_persona_pagar'],
            'fk_cpb004_num_clasificacion_gastos' => $form['fk_cpb004_num_clasificacion_gastos'],
            'fk_a006_num_miscelaneo_detalle_tipo_pago' => $form['fk_a006_num_miscelaneo_detalle_tipo_pago'],
            'fk_a023_num_centro_costo' => $form['fk_a023_num_centro_costo'],
            'ind_descripcion' => $form['ind_descripcion'],
            'num_flag_caja_chica' => 1,
            'fec_anio' => date('Y'),
            'fec_mes' => date('M'),
            'num_monto_total' => $form['num_monto_total'],
            'num_monto_adelantos' => 0,
            'num_monto_neto' => 0
        ));

        #detalleReposicion
        $detalleReposicion=$form['detalleReposicion'];
        $detallesReposicion=$form['detallesReposicion'];
        $secuencia=1;
        foreach($detalleReposicion['num_secuencia'] AS $i){
            $ids=$detalleReposicion[$i];
            $idsR=$detallesReposicion[$i];
            if($ids['fk_cbb004_num_plan_cuentaPub20'] == "" or $ids['fk_cbb004_num_plan_cuentaPub20'] == "error") {
                $ids['fk_cbb004_num_plan_cuentaPub20'] = null;
            }
            if($ids['fk_cbb004_num_plan_cuenta'] == "" or $ids['fk_cbb004_num_plan_cuenta'] == "error"){
                $ids['fk_cbb004_num_plan_cuenta'] = null;
            }
            if(isset($ids['pk_num_caja_chica_detalle'])){
                $titulo='UPDATE';
                $where="WHERE pk_num_caja_chica_detalle ='".$ids['pk_num_caja_chica_detalle']."' ";
            }else{
                $titulo='INSERT INTO';
                $where="";
            }
            $RegistrarReposicion=$this->_db->prepare("
                $titulo
               cp_c002_cajachica_detalle
            SET
                num_secuencia=:num_secuencia,
                fec_documento=NOW(),
                ind_descripcion=:ind_descripcion,
                num_monto_pagado=:num_monto_pagado,
                num_monto_afecto=:num_monto_afecto,
                num_monto_no_afecto=:num_monto_no_afecto,
                num_monto_impuesto=:num_monto_impuesto,
                num_monto_retencion=:num_monto_retencion,
                fk_rhb001_num_empleado_proveedor=:fk_rhb001_num_empleado_proveedor,
                num_flag_caja_chica=:num_flag_caja_chica,
                num_documento=:num_documento,
                ind_num_recibo=:ind_num_recibo,
                fec_fecha=NOW(),
                num_linea=:num_linea,
                fec_periodo_registro_compra=:fec_periodo_registro_compra,
                ind_comentarios='comentarios',
                num_flag_no_afecto_IGV=:num_flag_no_afecto_IGV,
                fk_a006_num_miscelaneo_regimen_fiscal=:fk_a006_num_miscelaneo_regimen_fiscal,
                fk_cpb017_num_tipo_servicio=:fk_cpb017_num_tipo_servicio,
                fk_cpb002_num_tipo_documento=:fk_cpb002_num_tipo_documento,
                fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
                fk_cbb004_num_plan_cuentaPub20=:fk_cbb004_num_plan_cuentaPub20,
                fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                fk_cpd004_num_caja_chica=:fk_cpd004_num_caja_chica,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW(),
                fec_anio=:fec_anio,
                fk_cpb005_num_concepto_gasto=:fk_cpb005_num_concepto_gasto
                $where
               ");

            $RegistrarReposicion->execute(array(
                'num_secuencia'=>$secuencia,
                'ind_descripcion'=>$idsR['ind_descripcion'],
                'num_monto_pagado'=>$ids['num_monto_pagado'],
                'num_monto_afecto'=>$ids['num_monto_afecto'],
                'num_monto_no_afecto'=>$ids['num_monto_no_afecto'],
                'num_monto_impuesto'=>$ids['num_monto_impuesto'],
                'num_monto_retencion'=>$ids['num_monto_retencion'],
                'fk_rhb001_num_empleado_proveedor'=>$ids['fk_rhb001_num_empleado_proveedor'],
                'num_flag_caja_chica'=> 0,
                'num_documento'=>$ids['num_documento'],
                'ind_num_recibo'=>$ids['ind_num_recibo'],
                'num_linea'=>1,
                'fec_periodo_registro_compra'=>1,
                'num_flag_no_afecto_IGV'=>0,
                'fk_a006_num_miscelaneo_regimen_fiscal'=>$ids['fk_a006_num_miscelaneo_regimen_fiscal'],
                'fk_cpb017_num_tipo_servicio'=>$ids['fk_cpb017_num_tipo_servicio'],
                'fk_cpb002_num_tipo_documento'=>$ids['fk_cpb002_num_tipo_documento'],
                'fk_cbb004_num_plan_cuenta'=>$ids['fk_cbb004_num_plan_cuenta'],
                'fk_cbb004_num_plan_cuentaPub20'=>$ids['fk_cbb004_num_plan_cuentaPub20'],
                'fk_prb002_num_partida_presupuestaria'=>$ids['fk_prb002_num_partida_presupuestaria'],
                'fk_cpd004_num_caja_chica'=>$idCajaChica,
                'fec_anio' => date('Y'),
                'fk_cpb005_num_concepto_gasto'=>$ids['fk_cpb005_num_concepto_gasto']
            ));
            $secuencia++;

        }

        $fallaTansaccion = $NuevoRegistro->errorInfo();
        $fallaTansaccion2 = $RegistrarReposicion->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }elseif (!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])){
            $this->_db->rollBack();
            return $fallaTansaccion2;
        }else{
            $this->_db->commit();
            return $idCajaChica;
        }
    }

    public function metConsultaDisponibilidad($idPartida, $anioPresupuest)
    {
        $consultar = $this->_db->query("
              SELECT
                cp_c002_cajachica_detalle.fk_prb002_num_partida_presupuestaria,
                SUM(cp_c002_cajachica_detalle.num_monto_afecto) AS monto,
                (pr_c002_presupuesto_det.num_monto_ajustado - pr_c002_presupuesto_det.num_monto_compromiso) AS montoDisponible,
                pr_b002_partida_presupuestaria.cod_partida

              FROM
              cp_c002_cajachica_detalle
              INNER JOIN pr_b004_presupuesto ON pr_b004_presupuesto.fec_anio = '$anioPresupuest'
              INNER JOIN pr_c002_presupuesto_det ON pr_c002_presupuesto_det.fk_prb004_num_presupuesto = pr_b004_presupuesto.pk_num_presupuesto
                  AND cp_c002_cajachica_detalle.fk_prb002_num_partida_presupuestaria = pr_c002_presupuesto_det.fk_prb002_num_partida_presupuestaria
              INNER JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = cp_c002_cajachica_detalle.fk_prb002_num_partida_presupuestaria
              WHERE
               pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = '$idPartida'
        ");
        $consultar->setFetchMode(PDO::FETCH_ASSOC);
        return $consultar->fetch();
    }

    public function metAprobarCajaChica($idCajaChica)
    {
        $this->_db->beginTransaction();
        $aprobar = $this->_db->prepare("
                  UPDATE
                    cp_d004_caja_chica
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fk_rhb001_num_empleado_aprueba='$this->atIdEmpleado',
                    fec_ultima_modificacion=NOW(),
                    fec_aprobacion=NOW(),
                    ind_estado='AP'
                  WHERE
                    pk_num_caja_chica=:pk_num_caja_chica
        ");
        $aprobar->execute(array(
            'pk_num_caja_chica' => $idCajaChica
        ));
        $error = $aprobar->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idCajaChica;
        }
    }

    public function metAnularCajaChica($idCajaChica)
    {
        $this->_db->beginTransaction();
        $reversar = $this->_db->prepare("
                  UPDATE
                    cp_d004_caja_chica
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fec_ultima_modificacion=NOW(),
                    ind_estado='AN'
                  WHERE
                    pk_num_caja_chica=:pk_num_caja_chica
        ");
        $reversar->execute(array(
            'pk_num_caja_chica' => $idCajaChica
        ));
        $error = $reversar->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idCajaChica;
        }
    }

    public function metBuscarTipoServicio($tipoServicio)
    {
        $servicio = $this->_db->query(
            "SELECT
              cp_b017_tipo_servicio.pk_num_tipo_servico
            FROM
              cp_b017_tipo_servicio
            WHERE
              cod_tipo_servicio = '$tipoServicio'
        ");
        $servicio->setFetchMode(PDO::FETCH_ASSOC);
        return $servicio->fetch();
    }

    public function metBuscarTiipoDocumento($cod)
    {
        $servicio = $this->_db->query(
            "SELECT
              cp_b002_tipo_documento.pk_num_tipo_documento
            FROM
              cp_b002_tipo_documento
            WHERE
              cp_b002_tipo_documento.cod_tipo_documento = '$cod'
        ");
        $servicio->setFetchMode(PDO::FETCH_ASSOC);
        return $servicio->fetch();
    }

    public function metConceptoGastoListar($idConceptoGasto = false)
    {
        if($idConceptoGasto){
            $where = "WHERE cp_b005_concepto_gasto.pk_num_concepto_gasto = '$idConceptoGasto'";
        } else {
            $where = "";
        }
        $impuestoListar =  $this->_db->query(
            "SELECT
              cp_b005_concepto_gasto.*,
              a018.ind_usuario,
              partida.cod_partida,
              partida.pk_num_partida_presupuestaria,
              partida.ind_denominacion,
              cuenta.cod_cuenta,
              cuenta20.cod_cuenta AS cod_cuenta20,
              cuenta.pk_num_cuenta,
              cuenta20.pk_num_cuenta AS pk_num_cuenta20,
              cuenta.ind_descripcion AS nombreCuenta,
              cuenta20.ind_descripcion AS nombreCuenta20,
              a006_miscelaneo_detalle.ind_nombre_detalle,
              a006_miscelaneo_detalle.cod_detalle
             FROM
              cp_b005_concepto_gasto
            INNER JOIN a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario = cp_b005_concepto_gasto.fk_a018_num_seguridad_usuario
            INNER JOIN pr_b002_partida_presupuestaria AS partida ON partida.pk_num_partida_presupuestaria = cp_b005_concepto_gasto.fk_prb002_num_partida_presupuestaria
            LEFT JOIN cb_b004_plan_cuenta AS cuenta ON cuenta.pk_num_cuenta = cp_b005_concepto_gasto.fk_cbb004_num_plan_cuenta
            LEFT JOIN cb_b004_plan_cuenta AS cuenta20 ON cuenta20.pk_num_cuenta = cp_b005_concepto_gasto.fk_cbb004_num_plan_cuenta_pub20
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b005_concepto_gasto.fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo

            ORDER BY fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo ASC

             " );
        $impuestoListar->setFetchMode(PDO::FETCH_ASSOC);
        if($idConceptoGasto){
            return $impuestoListar->fetch();
        } else {
            return $impuestoListar->fetchAll();
        }

    }

    public function metClasificacionGastoListar($idClasifGasto = false)
    {
        if($idClasifGasto){
            $where = "WHERE cp_b004_clasificacion_gastos.pk_num_clasificacion_gastos = '$idClasifGasto'";
        } else {
            $where = "";
        }
        $clasifGastoListar =  $this->_db->query(
            "SELECT
                *
              FROM
              cp_b004_clasificacion_gastos
              $where
              ORDER BY cod_clasificacion ASC
             " );
        $clasifGastoListar->setFetchMode(PDO::FETCH_ASSOC);
        if($idClasifGasto){
            return $clasifGastoListar->fetch();
        } else {
            return $clasifGastoListar->fetchAll();
        }

    }
}
