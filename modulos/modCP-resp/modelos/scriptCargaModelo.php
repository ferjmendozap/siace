<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcobavvv   | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class scriptCargaModelo extends Modelo

{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
    }

    public function metBusquedaSimple($tabla, $innerJoin, $where)
    {
        $busquedaSimple = $this->_db->query("
            SELECT
              *
            FROM
              $tabla
              $innerJoin
            WHERE
              $where
        ");
        $busquedaSimple->setFetchMode(PDO::FETCH_ASSOC);
        return $busquedaSimple->fetch();
    }

    public function metMostrarSelectImpuesto($codImpuesto)
    {
        $impuesto = $this->_db->query("
            SELECT
           cp_b015_impuesto.pk_num_impuesto
            FROM
              cp_b015_impuesto
            WHERE
              cp_b015_impuesto.cod_impuesto='$codImpuesto'
        ");
        $impuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $impuesto->fetch();
    }

    public function metMostrarSelectClasif($codClasif)
    {
        $clasif = $this->_db->query("
            SELECT
           cp_b004_clasificacion_gastos.pk_num_clasificacion_gastos
            FROM
              cp_b004_clasificacion_gastos
            WHERE
              cp_b004_clasificacion_gastos.cod_clasificacion='$codClasif'
        ");
        $clasif->setFetchMode(PDO::FETCH_ASSOC);
        return $clasif->fetch();
    }

    public function metMostrarCuenta($codCuenta)
    {
        $impuesto = $this->_db->query("
            SELECT
           cb_b004_plan_cuenta.pk_num_cuenta
            FROM
              cb_b004_plan_cuenta
            WHERE
              cb_b004_plan_cuenta.cod_cuenta='$codCuenta'
        ");
        $impuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $impuesto->fetch();
    }

    public function metMostrarVocuher($codVoucher)
    {
        $impuesto = $this->_db->query("
            SELECT
           cb_c003_tipo_voucher.pk_num_voucher
            FROM
              cb_c003_tipo_voucher
            WHERE
              cb_c003_tipo_voucher.cod_voucher='$codVoucher'
        ");
        $impuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $impuesto->fetch();
    }

    public function metMostrarPartida($codPartida)
    {
        $impuesto = $this->_db->query("
            SELECT
           pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria
            FROM
              pr_b002_partida_presupuestaria
            WHERE
              pr_b002_partida_presupuestaria.cod_partida='$codPartida'
        ");
        $impuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $impuesto->fetch();
    }

    public function metMostrarSelect($codMaestro, $codDetalle)
    {
        $miscelaneoDetalle = $this->_db->query("
            SELECT
              a006.pk_num_miscelaneo_detalle
            FROM
              a006_miscelaneo_detalle AS a006
            INNER JOIN
              a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
            WHERE
              a005.cod_maestro='$codMaestro' AND
              a006.cod_detalle='$codDetalle'
        ");
        $miscelaneoDetalle->setFetchMode(PDO::FETCH_ASSOC);
        return $miscelaneoDetalle->fetch();
    }

    #### CxP
    public function metCxPImpuesto($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                cp_b015_impuesto
              SET
                num_estatus=1,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                cod_impuesto=:cod_impuesto,
                txt_descripcion=:txt_descripcion,
                ind_signo=:ind_signo,
                num_factor_porcentaje=:num_factor_porcentaje,
                fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
                fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20,
                fk_a006_num_miscelaneo_detalle_regimen_fiscal=:fk_a006_num_miscelaneo_detalle_regimen_fiscal,
                fk_a006_num_miscelaneo_detalle_flag_provision=:fk_a006_num_miscelaneo_detalle_flag_provision,
                fk_a006_num_miscelaneo_detalle_flag_imponible=:fk_a006_num_miscelaneo_detalle_flag_imponible,
                fk_a006_num_miscelaneo_detalle_tipo_comprobante=:fk_a006_num_miscelaneo_detalle_tipo_comprobante,
                fk_a006_num_miscelaneo_detalle_flag_general=:fk_a006_num_miscelaneo_detalle_flag_general
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cp_b015_impuesto', false, "cod_impuesto = '" . $array['cod_impuesto'] . "'");
            if (!$busqueda) {
                $idMiscelaneosDetRegimen = $this->metMostrarSelect('REGFISCAL', $array['cod_detalle_Regimen']);
                $idMiscelaneosDetProvision = $this->metMostrarSelect('PROVDOC', $array['cod_detalle_Provision']);
                $idMiscelaneosDetImponible = $this->metMostrarSelect('IVAIMP', $array['cod_detalle_Imponible']);
                $idMiscelaneosDetComprob = $this->metMostrarSelect('COMPIMP', $array['cod_detalle_Comprob']);
                $idMiscelaneosDetGeneral = $this->metMostrarSelect('CLASIMP', $array['cod_detalle_General']);
                $idCuenta = $this->metMostrarCuenta($array['plan_cuenta']);
                $idCuenta20 = $this->metMostrarCuenta($array['plan_cuenta_pub20']);
                $registro->execute(array(
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':cod_impuesto' => $array['cod_impuesto'],
                    ':txt_descripcion' => $array['txt_descripcion'],
                    ':ind_signo' => $array['ind_signo'],
                    ':num_factor_porcentaje' => $array['num_factor_porcentaje'],
                    ':fk_cbb004_num_plan_cuenta' => $idCuenta['pk_num_cuenta'],
                    ':fk_cbb004_num_plan_cuenta_pub20' => $idCuenta20['pk_num_cuenta'],
                    ':fk_a006_num_miscelaneo_detalle_regimen_fiscal' => $idMiscelaneosDetRegimen['pk_num_miscelaneo_detalle'],
                    ':fk_a006_num_miscelaneo_detalle_flag_provision' => $idMiscelaneosDetProvision['pk_num_miscelaneo_detalle'],
                    ':fk_a006_num_miscelaneo_detalle_flag_imponible' => $idMiscelaneosDetImponible['pk_num_miscelaneo_detalle'],
                    ':fk_a006_num_miscelaneo_detalle_tipo_comprobante' => $idMiscelaneosDetComprob['pk_num_miscelaneo_detalle'],
                    ':fk_a006_num_miscelaneo_detalle_flag_general' => $idMiscelaneosDetGeneral['pk_num_miscelaneo_detalle'],
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCxPTipoServicios($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                cp_b017_tipo_servicio
              SET
                cod_tipo_servicio=:cod_tipo_servicio,
                ind_descripcion=:ind_descripcion,
                fk_a006_num_miscelaneo_regimen_fiscal=:fk_a006_num_miscelaneo_regimen_fiscal,
                num_estatus=1
            ");

        $registroDetalle = $this->_db->prepare(
            "INSERT INTO
                cp_b020_servicio_impuesto
              SET
                fk_cpb015_num_impuesto=:fk_cpb015_num_impuesto,
                fk_cpb017_num_tipo_servico=:fk_cpb017_num_tipo_servico
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cp_b017_tipo_servicio', false, "cod_tipo_servicio = '" . $array['cod_tipo_servicio'] . "'");
            if (!$busqueda) {
                $idMiscelaneosDet = $this->metMostrarSelect('REGFISCAL', $array['cod_detalle']);
                $registro->execute(array(
                    ':cod_tipo_servicio' => $array['cod_tipo_servicio'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':fk_a006_num_miscelaneo_regimen_fiscal' => $idMiscelaneosDet['pk_num_miscelaneo_detalle']
                ));
                $idServicio = $this->_db->lastInsertId();

                if (isset($array['Det'])) {
                    foreach ($array['Det'] as $arrayDet) {
                        $idImpuesto = $this->metMostrarSelectImpuesto($arrayDet['impuesto']);

                        $registroDetalle->execute(array(
                            ':fk_cpb015_num_impuesto' => $idImpuesto['pk_num_impuesto'],
                            ':fk_cpb017_num_tipo_servico' => $idServicio
                        ));
                    }
                }
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCxPDocumentos($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                cp_b002_tipo_documento
              SET
                num_estatus=1,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=1,
                cod_tipo_documento=:cod_tipo_documento,
                num_flag_obligacion=:num_flag_obligacion,
                ind_descripcion=:ind_descripcion,
                fk_a006_num_miscelaneo_detalle_clasificacion=:fk_a006_num_miscelaneo_detalle_clasificacion,
                fk_cbc003_num_tipo_voucher=:fk_cbc003_num_tipo_voucher,
                fk_cbc003_num_tipo_voucher_ordPago=:fk_cbc003_num_tipo_voucher_ordPago,
                num_flag_provision=:num_flag_provision,
                fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
                fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20,
                num_flag_adelanto=:num_flag_adelanto,
                 fk_cbb004_num_plan_cuenta_ade=:fk_cbb004_num_plan_cuenta_ade,
                fk_cbb004_num_plan_cuenta_ade_pub20=:fk_cbb004_num_plan_cuenta_ade_pub20,
                num_flag_fiscal=:num_flag_fiscal,
                num_flag_monto_negativo=:num_flag_monto_negativo,
                fk_a006_num_miscelaneo_detalle_regimen_fiscal=:fk_a006_num_miscelaneo_detalle_regimen_fiscal

            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cp_b002_tipo_documento', false, "cod_tipo_documento = '" . $array['cod_tipo_documento'] . "'");
            if (!$busqueda) {
                $idMiscelaneosDetClasif = $this->metMostrarSelect('CLASDOC', $array['cod_detalle_clasif']);
                $idMiscelaneosDetRegimen = $this->metMostrarSelect('REGFISCAL', $array['cod_detalle_regimen']);
                $idVoucher = $this->metMostrarVocuher($array['fk_cbc003_num_tipo_voucher']);
                $idVoucher20 = $this->metMostrarVocuher($array['fk_cbc003_num_tipo_voucher_ordPago']);
                $idCuenta = $this->metMostrarCuenta($array['plan_cuenta']);
                $idCuenta20 = $this->metMostrarCuenta($array['plan_cuenta_pub20']);
                $idCuentaAde = $this->metMostrarCuenta($array['plan_cuenta_ade']);
                $idCuentaAde20 = $this->metMostrarCuenta($array['plan_cuenta_ade_pub20']);

                $registro->execute(array(
                    ':cod_tipo_documento' => $array['cod_tipo_documento'],
                    ':num_flag_obligacion' => 1,
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':fk_a006_num_miscelaneo_detalle_clasificacion' => $idMiscelaneosDetClasif['pk_num_miscelaneo_detalle'],
                    ':fk_cbc003_num_tipo_voucher' => $idVoucher['pk_num_voucher'],
                    ':fk_cbc003_num_tipo_voucher_ordPago' => $idVoucher20['pk_num_voucher'],
                    ':num_flag_provision' => $array['num_flag_provision'],
                    ':fk_cbb004_num_plan_cuenta' => $idCuenta['pk_num_cuenta'],
                    ':fk_cbb004_num_plan_cuenta_pub20' => $idCuenta20['pk_num_cuenta'],
                    ':num_flag_adelanto' => $array['num_flag_adelanto'],
                    ':fk_cbb004_num_plan_cuenta_ade' => $idCuentaAde['pk_num_cuenta'],
                    ':fk_cbb004_num_plan_cuenta_ade_pub20' => $idCuentaAde20['pk_num_cuenta'],
                    ':num_flag_fiscal' => $array['num_flag_fiscal'],
                    ':num_flag_monto_negativo' => $array['num_flag_monto_negativo'],
                    ':fk_a006_num_miscelaneo_detalle_regimen_fiscal' => $idMiscelaneosDetRegimen['pk_num_miscelaneo_detalle']
                ));
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCxPClasificacion($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                cp_b004_clasificacion_gastos
              SET
                num_estatus=1,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                cod_clasificacion=:cod_clasificacion,
                ind_descripcion=:ind_descripcion,
                fk_a006_num_miscelaneo_detalle_aplicacion=:fk_a006_num_miscelaneo_detalle_aplicacion


            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cp_b004_clasificacion_gastos', false, "cod_clasificacion = '" . $array['cod_clasificacion'] . "'");
            if (!$busqueda) {
                $idMiscelaneosDet = $this->metMostrarSelect('APLICG', $array['cod_detalle']);

                $registro->execute(array(
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':cod_clasificacion' => $array['cod_clasificacion'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':fk_a006_num_miscelaneo_detalle_aplicacion' => $idMiscelaneosDet['pk_num_miscelaneo_detalle']
                ));
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCxPConcepto($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                cp_b005_concepto_gasto
              SET
                num_estatus=1,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=1,

                cod_concepto_gasto=:cod_concepto_gasto,
                ind_descripcion=:ind_descripcion,
                fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
                fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo=:fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo

            ");
        $registroDetalle = $this->_db->prepare(
            "INSERT INTO
                cp_c001_concepto_clasificacion_gasto
              SET
                fk_a018_num_seguridad_usuario=1,
                fec_ultima_modificacion=NOW(),
              
                fk_cpb004_num_clasificacion_gastos=:fk_cpb004_num_clasificacion_gastos,
                fk_cpb005_num_concepto_gasto=:fk_cpb005_num_concepto_gasto
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cp_b005_concepto_gasto', false, "cod_concepto_gasto = '" . $array['cod_concepto_gasto'] . "'");
            if (!$busqueda) {
                $idPartida = $this->metMostrarPartida($array['partida_presupuestaria']);
                $idCuenta = $this->metMostrarCuenta($array['plan_cuenta']);
                $idMiscelaneosDet = $this->metMostrarSelect('GRUPGAST', $array['cod_detalle']);

                $registro->execute(array(
                    ':cod_concepto_gasto' => $array['cod_concepto_gasto'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':fk_prb002_num_partida_presupuestaria' => $idPartida['pk_num_partida_presupuestaria'],
                    ':fk_cbb004_num_plan_cuenta' => $idCuenta['pk_num_cuenta'],
                    ':fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo' => $idMiscelaneosDet['pk_num_miscelaneo_detalle']
                ));
                $idConcepto = $this->_db->lastInsertId();

                if (isset($array['Det'])) {
                    foreach ($array['Det'] as $arrayDet) {
                        $idClasif = $this->metMostrarSelectClasif($arrayDet['clasificacion_gastos']);
                        $registroDetalle->execute(array(
                            ':fk_cpb004_num_clasificacion_gastos' => $idClasif['pk_num_clasificacion_gastos'],
                            ':fk_cpb005_num_concepto_gasto' => $idConcepto
                        ));
                    }
                }
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCxPCuentaBancaria($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                cp_b014_cuenta_bancaria
              SET
                num_estatus=1,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=1,

                fk_a006_num_miscelaneo_detalle_banco=:fk_a006_num_miscelaneo_detalle_banco,
                ind_num_cuenta=:ind_num_cuenta,
                ind_descripcion=:ind_descripcion,
                fk_a006_num_miscelaneo_detalle_tipo_cuenta=:fk_a006_num_miscelaneo_detalle_tipo_cuenta,
                fec_apertura=:fec_apertura,
                fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
                fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20,
                ind_agencia=:ind_agencia,
                ind_distrito=:ind_distrito,
                ind_atencion=:ind_atencion,
                ind_cargo=:ind_cargo,
                num_flag_conciliacion_bancaria=:num_flag_conciliacion_bancaria,
                num_flag_debito_bancario=:num_flag_debito_bancario,
                num_flag_conciliacion_cp=:num_flag_conciliacion_cp

            ");

        $registroDetalle = $this->_db->prepare(
            "INSERT INTO
                cp_b019_cuenta_bancaria_tipo_pago
              SET
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=1,

                fk_cpb014_num_cuenta_bancaria=:fk_cpb014_num_cuenta_bancaria,
                fk_a006_num_miscelaneo_detalle_tipo_pago=:fk_a006_num_miscelaneo_detalle_tipo_pago,
                int_numero_ultimo_cheque=:int_numero_ultimo_cheque

            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cp_b014_cuenta_bancaria', false, "ind_num_cuenta = '" . $array['ind_num_cuenta'] . "'");
            if (!$busqueda) {
                $idCuenta = $this->metMostrarCuenta($array['plan_cuenta']);
                $idCuenta20 = $this->metMostrarCuenta($array['plan_cuenta_pub20']);
                $idMiscelaneosCuenta = $this->metMostrarSelect('BANCOS', $array['cod_detalle_banco']);
                $idMiscelaneosTipoC = $this->metMostrarSelect('TIPCUENTAB', $array['cod_detalle_tipo_cuenta']);

                $registro->execute(array(
                    ':ind_num_cuenta' => $array['ind_num_cuenta'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':fec_apertura' => $array['fec_apertura'],
                    ':ind_agencia' => $array['ind_agencia'],
                    ':ind_distrito' => $array['ind_distrito'],
                    ':ind_atencion' => $array['ind_atencion'],
                    ':ind_cargo' => $array['ind_cargo'],
                    ':num_flag_conciliacion_bancaria' => $array['num_flag_conciliacion_bancaria'],
                    ':num_flag_debito_bancario' => $array['num_flag_debito_bancario'],
                    ':num_flag_conciliacion_cp' => $array['num_flag_conciliacion_cp'],
                    ':fk_cbb004_num_plan_cuenta' => $idCuenta['pk_num_cuenta'],
                    ':fk_cbb004_num_plan_cuenta_pub20' => $idCuenta20['pk_num_cuenta'],
                    ':fk_a006_num_miscelaneo_detalle_banco' => $idMiscelaneosCuenta['pk_num_miscelaneo_detalle'],
                    ':fk_a006_num_miscelaneo_detalle_tipo_cuenta' => $idMiscelaneosTipoC['pk_num_miscelaneo_detalle'],
                ));
                $idCuentaBancaria = $this->_db->lastInsertId();

                if (isset($array['Det'])) {
                    foreach ($array['Det'] as $arrayDet) {
                        $idMiscelaneosTipoPago = $this->metMostrarSelect('TDPLG', $arrayDet['cod_detalle']);

                        $registroDetalle->execute(array(
                            ':fk_cpb014_num_cuenta_bancaria' => $idCuentaBancaria,
                            ':fk_a006_num_miscelaneo_detalle_tipo_pago' => $idMiscelaneosTipoPago['pk_num_miscelaneo_detalle'],
                            ':int_numero_ultimo_cheque' => $arrayDet['int_numero_ultimo_cheque']
                        ));
                    }
                }
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCxPtipoTransaccion($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                cp_b006_banco_tipo_transaccion
              SET
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=1,

                num_estatus=:num_estatus,
                cod_tipo_transaccion=:cod_tipo_transaccion,
                ind_descripcion=:ind_descripcion,
                fk_a006_miscelaneo_detalle_tipo_transaccion=:fk_a006_miscelaneo_detalle_tipo_transaccion,
                num_flag_voucher=:num_flag_voucher,
                num_flag_transaccion=:num_flag_transaccion,
                num_flag_transaccion_planilla=:num_flag_transaccion_planilla,
                fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta

            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cp_b006_banco_tipo_transaccion', false, "cod_tipo_transaccion = '" . $array['cod_tipo_transaccion'] . "'");
            if (!$busqueda) {
                $idCuenta = $this->metMostrarCuenta($array['plan_cuenta']);
                $idMiscelaneosDet = $this->metMostrarSelect('TIPOTRANS', $array['cod_detalle']);

                $registro->execute(array(
                    ':num_estatus' => $array['num_estatus'],
                    ':cod_tipo_transaccion' => $array['cod_tipo_transaccion'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':fk_a006_miscelaneo_detalle_tipo_transaccion' => $idMiscelaneosDet['pk_num_miscelaneo_detalle'],
                    ':num_flag_voucher' => $array['num_flag_voucher'],
                    ':num_flag_transaccion' => $array['num_flag_transaccion'],
                    ':num_flag_transaccion_planilla' => $array['num_flag_transaccion_planilla'],
                    ':fk_cbb004_num_plan_cuenta' => $idCuenta['pk_num_cuenta']
                ));
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }
}
