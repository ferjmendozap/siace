<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once 'cuentasModelo.php';
require_once 'trait'.DS.'consultasTrait.php';
require_once 'trait'.DS.'updateTrait.php';
require_once RUTA_MODELO . 'miscelaneoModelo.php';


class procesosModelo extends Modelo
{
    use consultasTrait;
    use updateTrait;
    private $atIdUsuario;
    public $atCuentasModelo;
    public $atMiscelaneoModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atCuentasModelo = new cuentasModelo();
        $this->atMiscelaneoModelo=new miscelaneoModelo();
    }

    public function metListar($idCuenta, $fSaldo = false, $fconc = false, $periodo = false)
    {
        if($fSaldo and $fconc){
            $where = "AND cp_d011_banco_transaccion.fec_transaccion='$fSaldo' AND cp_d011_banco_transaccion.fec_conciliacion='$fconc'";

        }elseif($fSaldo){
            $where = "AND cp_d011_banco_transaccion.fec_transaccion='$fSaldo'";

        }elseif($fconc){
            $where = "AND cp_d011_banco_transaccion.fec_conciliacion='$fconc'";
        }else{
            $where = "";
        }
        if($periodo){
            $where = "AND cp_d011_banco_transaccion.ind_periodo_contable = '$periodo'";
        }
        $listar = $this->_db->query(
            "SELECT
              cp_d011_banco_transaccion.*,
              cp_b014_cuenta_bancaria.pk_num_cuenta,
              cp_b014_cuenta_bancaria.ind_num_cuenta AS cuentaBancaria,
              cp_b014_cuenta_bancaria.ind_descripcion AS ind_descripcion,
              a006_miscelaneo_detalle.ind_nombre_detalle AS banco,
              cp_b006_banco_tipo_transaccion.cod_tipo_transaccion,
              cp_b006_banco_tipo_transaccion.ind_descripcion AS decripcionTransaccion,
              tipoTransaccion.cod_detalle AS tipoTransaccion,
              cp_d010_pago.ind_num_pago AS ind_num_pago,
               (CASE
                            WHEN cp_d011_banco_transaccion.ind_estado = 'PE' THEN 'PENDIENTE'
                            WHEN cp_d011_banco_transaccion.ind_estado = 'AP' THEN 'ACTUALIZADO'
                            WHEN cp_d011_banco_transaccion.ind_estado = 'CO' THEN 'CONTABILIZADO'
                            WHEN cp_d011_banco_transaccion.ind_estado = 'AN' THEN 'ANULADO'
              END) AS ind_estado
            FROM
              cp_d011_banco_transaccion
            INNER JOIN cp_b006_banco_tipo_transaccion ON cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = cp_d011_banco_transaccion.fk_cpb006_num_banco_tipo_transaccion
            INNER JOIN a006_miscelaneo_detalle AS tipoTransaccion ON tipoTransaccion.pk_num_miscelaneo_detalle = cp_b006_banco_tipo_transaccion.fk_a006_miscelaneo_detalle_tipo_transaccion
            LEFT JOIN cp_d010_pago ON cp_d010_pago.pk_num_pago = cp_d011_banco_transaccion.fk_cpd010_num_pago
            LEFT JOIN cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
            LEFT JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            LEFT JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d001_obligacion.fk_cpb014_num_cuenta or cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b014_cuenta_bancaria.fk_a006_num_miscelaneo_detalle_banco
            LEFT JOIN cp_d006_cuenta_bancaria_balance ON cp_d006_cuenta_bancaria_balance.pk_num_cuenta_balance = cp_b014_cuenta_bancaria.pk_num_cuenta
            WHERE cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria='$idCuenta'
            $where
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metListarAnterior($idCuenta, $fSaldo = false, $fconc = false, $periodo = false)
    {
        if($fSaldo and $fconc){
            $where = "AND cp_d011_banco_transaccion.fec_transaccion='$fSaldo' AND cp_d011_banco_transaccion.fec_conciliacion='$fconc'";

        }elseif($fSaldo){
            $where = "AND cp_d011_banco_transaccion.fec_transaccion='$fSaldo'";

        }elseif($fconc){
            $where = "AND cp_d011_banco_transaccion.fec_conciliacion='$fconc'";
        }else{
            $where = "";
        }
        if($periodo){
            $where = "AND cp_d011_banco_transaccion.num_flag_conciliacion = 0 AND cp_d011_banco_transaccion.ind_periodo_contable < '$periodo'";
        }
        $listar = $this->_db->query(
            "SELECT
              cp_d011_banco_transaccion.*,
              cp_b014_cuenta_bancaria.pk_num_cuenta,
              cp_b014_cuenta_bancaria.ind_num_cuenta AS cuentaBancaria,
              cp_b014_cuenta_bancaria.ind_descripcion AS ind_descripcion,
              a006_miscelaneo_detalle.ind_nombre_detalle AS banco,
              cp_b006_banco_tipo_transaccion.cod_tipo_transaccion,
              cp_b006_banco_tipo_transaccion.ind_descripcion AS decripcionTransaccion,
              tipoTransaccion.cod_detalle AS tipoTransaccion,
              cp_d010_pago.ind_num_pago AS ind_num_pago,
               (CASE
                            WHEN cp_d011_banco_transaccion.ind_estado = 'PE' THEN 'PENDIENTE'
                            WHEN cp_d011_banco_transaccion.ind_estado = 'AP' THEN 'ACTUALIZADO'
                            WHEN cp_d011_banco_transaccion.ind_estado = 'CO' THEN 'CONTABILIZADO'
                            WHEN cp_d011_banco_transaccion.ind_estado = 'AN' THEN 'ANULADO'
              END) AS ind_estado
            FROM
              cp_d011_banco_transaccion
            INNER JOIN cp_b006_banco_tipo_transaccion ON cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = cp_d011_banco_transaccion.fk_cpb006_num_banco_tipo_transaccion
            INNER JOIN a006_miscelaneo_detalle AS tipoTransaccion ON tipoTransaccion.pk_num_miscelaneo_detalle = cp_b006_banco_tipo_transaccion.fk_a006_miscelaneo_detalle_tipo_transaccion
            LEFT JOIN cp_d010_pago ON cp_d010_pago.pk_num_pago = cp_d011_banco_transaccion.fk_cpd010_num_pago
            LEFT JOIN cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
            LEFT JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            LEFT JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d001_obligacion.fk_cpb014_num_cuenta or cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b014_cuenta_bancaria.fk_a006_num_miscelaneo_detalle_banco
            LEFT JOIN cp_d006_cuenta_bancaria_balance ON cp_d006_cuenta_bancaria_balance.pk_num_cuenta_balance = cp_b014_cuenta_bancaria.pk_num_cuenta
            WHERE cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria='$idCuenta'
            $where
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metBuscarCuentas($idBanco)
    {
        $listar = $this->_db->query(
            "SELECT
              *
            FROM
              cp_b014_cuenta_bancaria
            WHERE
             fk_a006_num_miscelaneo_detalle_banco='$idBanco'
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metCuentaBancaria($idCuenta, $mes, $año)
    {
        $listar = $this->_db->query(
            "SELECT
              cp_b014_cuenta_bancaria.*,
              a006_miscelaneo_detalle.ind_nombre_detalle as banco,
              (
                SELECT
                    SUM(num_monto)
                FROM
                    cp_d011_banco_transaccion
                WHERE
                    ind_periodo_contable<= '$mes-$año'
                    AND cp_b014_cuenta_bancaria.pk_num_cuenta='$idCuenta'
              ) AS SaldoCuenta
            FROM
              cp_b014_cuenta_bancaria
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b014_cuenta_bancaria.fk_a006_num_miscelaneo_detalle_banco
            WHERE
             cp_b014_cuenta_bancaria.pk_num_cuenta='$idCuenta'
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metBuscardetalleCuenta($idCuenta, $año, $mes, $tipo)
    {
        if($tipo=='banco'){
            $ind_tipo = "AND ind_tipo_banco_libro = 'B'";
        }elseif($tipo=='libro'){
            $ind_tipo = "AND ind_tipo_banco_libro = 'L'";
        }else{
            $ind_tipo = "";
        }
        $listar = $this->_db->query(
            "SELECT
                MAX(pk_num_diferencia_saldo) AS diferenciaSaldoMax
             FROM
              cp_d007_diferencia_saldos
             WHERE
              fec_anio = '$año' AND
              fec_mes = '$mes' AND
              fk_cpb014_num_cuenta_bancaria = '$idCuenta'
              $ind_tipo
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }
    public function metBuscardetalle($idCuenta, $año, $mes, $tipo, $pk)
    {
        if($tipo=='banco'){
            $ind_tipo = "AND ind_tipo_banco_libro = 'B'";
        }elseif($tipo=='libro'){
            $ind_tipo = "AND ind_tipo_banco_libro = 'L'";
        }else{
            $ind_tipo = "";
        }
        $listar = $this->_db->query(
            "SELECT
               *
             FROM
              cp_d007_diferencia_saldos
             WHERE
              fec_anio = '$año' AND
              fec_mes = '$mes' AND
              fk_cpb014_num_cuenta_bancaria = '$idCuenta' AND 
              pk_num_diferencia_saldo = '$pk'
              
              $ind_tipo
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }
    public function metBuscarCuenta($idCuenta, $año, $mes, $tipo)
    {
        if($tipo=='banco'){
            $ind_tipo = "AND ind_tipo_banco_libro = 'B'";
        }elseif($tipo=='libro'){
            $ind_tipo = "AND ind_tipo_banco_libro = 'L'";
        }else{
            $ind_tipo = "";
        }
        $listar = $this->_db->query(
            "SELECT
                *
             FROM
              cp_d007_diferencia_saldos
             WHERE
              fec_anio = '$año' AND
              fec_mes = '$mes' AND
              fk_cpb014_num_cuenta_bancaria = '$idCuenta'
              $ind_tipo
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metBuscarDiferenciaConciliacion($diferenciaSaldoMax, $tipo = false)
    {
        if($tipo=='transacciones'){
            $where = "AND ind_num_proceso = 'D'";
        }elseif($tipo=='transito'){
            $where = "AND ind_num_proceso != 'D'";
        }else{
            $where = "";
        }

        $listar = $this->_db->query(
            "SELECT
               *
             FROM
              cp_d008_diferencia_conciliacion
             WHERE
              fk_cpd007_num_diferencia_saldo = '$diferenciaSaldoMax'
              $where
            ");
       //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metBuscarTransaccionesCuenta($idCuenta, $fechaHasta)
    {
        $listar = $this->_db->query(
            "SELECT
              cp_d011_banco_transaccion.*,
              cp_d010_pago.ind_num_pago,
              cp_d010_pago.num_monto_pago,
              cp_d010_pago.ind_num_cheque
            FROM
              cp_d011_banco_transaccion
            INNER JOIN cp_d010_pago ON cp_d010_pago.pk_num_pago = cp_d011_banco_transaccion.fk_cpd010_num_pago
            INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
            INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            WHERE
              cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria='$idCuenta'
              AND cp_d011_banco_transaccion.fec_transaccion <= '$fechaHasta 23:59:59' 
              AND cp_d011_banco_transaccion.num_flag_conciliacion=0
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metBuscarProveedor($proveedor)
    {
        $listar = $this->_db->query("
            SELECT
                CONCAT_WS(' ',a003_persona.ind_nombre1, a003_persona.ind_apellido1) AS proveedor
            FROM
                a003_persona
            WHERE
                a003_persona.pk_num_persona = '$proveedor'
        ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metBuscarEstadoCuenta($idCuenta,$desde,$hasta)
    {
        $listar = $this->_db->query(
            "SELECT
            SUM(num_monto) as monto

            FROM
              cp_d011_banco_transaccion
            WHERE
              cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria='$idCuenta'
              AND cp_d011_banco_transaccion.fec_transaccion <= '$hasta 23:59:59'
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metActualizar($idTransaccion)
    {
        $this->_db->beginTransaction();
        $actualizar = $this->_db->prepare(
            "UPDATE
             cp_d011_banco_transaccion
             SET
              num_flag_conciliacion=:num_flag_conciliacion,
              fec_conciliacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
              WHERE
              pk_num_banco_transaccion='$idTransaccion'
               ");
        $actualizar->execute(array(
            ':num_flag_conciliacion' => 1
        ));
        $fallaTansaccion = $actualizar->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metNuevoSaldo($form, $tipo, $descripcion, $estadoCuenta, $año, $mes)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "INSERT INTO
              cp_d007_diferencia_saldos
              SET
               	ind_tipo_banco_libro=:ind_tipo_banco_libro,
                fec_anio=:fec_anio,
                fec_mes=:fec_mes,
                num_estado_cuenta=:num_estado_cuenta,
                fk_cpb014_num_cuenta_bancaria=:fk_cpb014_num_cuenta_bancaria,
                ind_descripcion=:ind_descripcion,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
               ");
        $NuevoRegistro->execute(array(
            ':ind_tipo_banco_libro' => $tipo,
            ':fec_anio' => $año,
            ':fec_mes' => $mes,
            ':num_estado_cuenta' => $estadoCuenta,
            ':fk_cpb014_num_cuenta_bancaria' => $form['fk_cpb014_num_cuenta_bancaria'],
            ':ind_descripcion' => $descripcion
        ));
        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metNuevaDifConciliacion($fecha, $cargaAbono, $numero, $descripcion, $bolivares, $idDiferenciaSaldo)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "INSERT INTO
              cp_d008_diferencia_conciliacion
              SET
                fec_anio=YEAR(NOW()),
                fec_fecha=:fec_fecha,
                ind_carga_abono=:ind_carga_abono,
                ind_num_orden=:ind_num_orden,
                ind_num_proceso=:ind_num_proceso,
                ind_descripcion=:ind_descripcion,
                num_bolivares=:num_bolivares,
                fk_cpd007_num_diferencia_saldo=:fk_cpd007_num_diferencia_saldo,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
               ");
            $NuevoRegistro->execute(array(
                ':fec_fecha' => $fecha,
                ':ind_carga_abono' => $cargaAbono,
                ':ind_num_orden' => '',
                ':ind_num_proceso' => $numero,
                ':ind_descripcion' => $descripcion,
                ':num_bolivares' => $bolivares,
                ':fk_cpd007_num_diferencia_saldo' => $idDiferenciaSaldo
            ));
        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }


}
