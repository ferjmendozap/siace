<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_MODELO.'miscelaneoModelo.php';
require_once RUTA_Modulo . 'modCB' . DS . 'modelos' . DS . 'TipoVoucherModelo.php';

class documentoModelo extends Modelo
{
    private $atIdUsuario;
    public $atMiscelaneoModelo;
    public $atVoucherModelo;
    public function __construct()
    {
        parent::__construct();

        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo=new miscelaneoModelo();
        $this->atTipoVoucherModelo = new TipoVoucherModelo();
    }

    public function metDocumentoListar($idDocumento = false)
    {
        if($idDocumento){
            $and = "AND d.pk_num_tipo_documento = '$idDocumento'";
        }else{
            $and = "";
        }
        $documentoListar =  $this->_db->query(
            "SELECT
            d.*,
            rf.ind_nombre_detalle AS regimen,
            cl.ind_nombre_detalle AS clasificacion,
            cb_c003_tipo_voucher.cod_voucher
            FROM
            cp_b002_tipo_documento AS d
            INNER JOIN a006_miscelaneo_detalle AS rf ON rf.pk_num_miscelaneo_detalle = d.fk_a006_num_miscelaneo_detalle_regimen_fiscal
            INNER JOIN a006_miscelaneo_detalle AS cl ON cl.pk_num_miscelaneo_detalle = d.fk_a006_num_miscelaneo_detalle_clasificacion
            LEFT JOIN cb_c003_tipo_voucher ON cb_c003_tipo_voucher.pk_num_voucher = d.fk_cbc003_num_tipo_voucher
            WHERE d.num_flag_obligacion = 1 $and
            ORDER BY cod_tipo_documento ASC
          ");
        //var_dump($documentoListar);
        $documentoListar->setFetchMode(PDO::FETCH_ASSOC);
        if($idDocumento){
            return $documentoListar->fetch();
        }else{
            return $documentoListar->fetchAll();
        }
    }

    public function metDocumentoConsultaDetalle($idDocumento)
    {
        $consultaDocumento =  $this->_db->query(
            "SELECT
                d.*,
                rf.ind_nombre_detalle AS RegimenFiscal,
                cl.ind_nombre_detalle AS Clasificacion,
                a018.ind_usuario

              FROM
              cp_b002_tipo_documento AS d
              INNER JOIN a018_seguridad_usuario AS a018 ON a018.pk_num_seguridad_usuario = d.fk_a018_num_seguridad_usuario
              INNER JOIN a006_miscelaneo_detalle AS rf ON d.fk_a006_num_miscelaneo_detalle_regimen_fiscal = rf.pk_num_miscelaneo_detalle
              INNER JOIN a006_miscelaneo_detalle AS cl ON d.fk_a006_num_miscelaneo_detalle_clasificacion = cl.pk_num_miscelaneo_detalle
              WHERE
              d.pk_num_tipo_documento='$idDocumento'
              "
        );
        $consultaDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaDocumento->fetch();
    }
    public function metDocumentoConsultaDetalleCuenta($idDocumento)
    {
        $consultaDocumento =  $this->_db->query(
            "SELECT
	            cuenta.cod_cuenta AS cuenta,
	            cuentaAde.cod_cuenta AS cuentaAde,
                cuentaPub20.cod_cuenta AS cuentaPub20,
                cuentaPub20Ade.cod_cuenta AS cuentaPub20Ade
             FROM
	            cp_b002_tipo_documento AS d
	         LEFT JOIN cb_b004_plan_cuenta AS cuenta ON cuenta.pk_num_cuenta = d.fk_cbb004_num_plan_cuenta
	         LEFT JOIN cb_b004_plan_cuenta AS cuentaAde ON cuentaAde.pk_num_cuenta = d.fk_cbb004_num_plan_cuenta_ade
             LEFT JOIN cb_b004_plan_cuenta AS cuentaPub20 ON cuentaPub20.pk_num_cuenta = d.fk_cbb004_num_plan_cuenta_pub20
             LEFT JOIN cb_b004_plan_cuenta AS cuentaPub20Ade ON cuentaPub20Ade.pk_num_cuenta = d.fk_cbb004_num_plan_cuenta_ade_pub20
             WHERE
	            d.pk_num_tipo_documento = '$idDocumento'
        ");
        $consultaDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaDocumento->fetch();
    }

    public function metNuevoDocumento($codDocumento,$descripcion,$flagAdelanto,$flagAutoNomina,$flagFiscal,
                                      $flagProvision,$estatus,$regFiscal,$clasificacion, $voucher,$voucherOrdPago,
                                      $cuenta,$cuentaAde,$cuentaPub20,$cuentaAdePub20)
    {

        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "INSERT INTO
              cp_b002_tipo_documento
              SET
                cod_tipo_documento=:cod_tipo_documento,
                ind_descripcion=:ind_descripcion,
                num_flag_adelanto=:num_flag_adelanto,
                num_flag_auto_nomina=:num_flag_auto_nomina,
                num_flag_fiscal=:num_flag_fiscal,
                num_flag_obligacion=:num_flag_obligacion,
                num_flag_provision=:num_flag_provision,
                num_estatus=:num_estatus,
                fk_a006_num_miscelaneo_detalle_regimen_fiscal=:fk_a006_num_miscelaneo_detalle_regimen_fiscal,
                fk_a006_num_miscelaneo_detalle_clasificacion=:fk_a006_num_miscelaneo_detalle_clasificacion,
                fk_cbc003_num_tipo_voucher=:fk_cbc003_num_tipo_voucher,
                fk_cbc003_num_tipo_voucher_ordPago=:fk_cbc003_num_tipo_voucher_ordPago,
                fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
                fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20,
                fk_cbb004_num_plan_cuenta_ade =:fk_cbb004_num_plan_cuenta_ade,
                fk_cbb004_num_plan_cuenta_ade_pub20=:fk_cbb004_num_plan_cuenta_ade_pub20,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
               ");
        $NuevoRegistro->execute(array(
            ':cod_tipo_documento' => $codDocumento,
            ':ind_descripcion' => $descripcion,
            ':num_flag_adelanto' => $flagAdelanto,
            ':num_flag_auto_nomina' => $flagAutoNomina,
            ':num_flag_fiscal' => $flagFiscal,
            ':num_flag_obligacion' => 1,
            ':num_flag_provision' => $flagProvision,
            ':num_estatus' => $estatus,
            ':fk_a006_num_miscelaneo_detalle_regimen_fiscal' => $regFiscal,
            ':fk_a006_num_miscelaneo_detalle_clasificacion' => $clasificacion,
            ':fk_cbc003_num_tipo_voucher' => $voucher,
            ':fk_cbc003_num_tipo_voucher_ordPago' => $voucherOrdPago,
            ':fk_cbb004_num_plan_cuenta' =>$cuenta,
            ':fk_cbb004_num_plan_cuenta_pub20' =>$cuentaPub20,
            ':fk_cbb004_num_plan_cuenta_ade' =>$cuentaAde,
            ':fk_cbb004_num_plan_cuenta_ade_pub20' =>$cuentaAdePub20
        ));

        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarDocumento($idDocumento,$descripcion,$flagAdelanto,$flagAutoNomina,$flagFiscal,
                                          $flagProvision,$estatus,$regFiscal,$clasificacion, $voucher,$voucherOrdPago,
                                          $cuenta,$cuentaAde,$cuentaPub20,$cuentaAdePub20)
    {
        $this->_db->beginTransaction();
        $modificarDocumento=$this->_db->prepare(
            "UPDATE
              cp_b002_tipo_documento
              SET
               	ind_descripcion=:ind_descripcion,
                num_flag_adelanto=:num_flag_adelanto,
                num_flag_auto_nomina=:num_flag_auto_nomina,
                num_flag_fiscal=:num_flag_fiscal,
                num_flag_provision=:num_flag_provision,
                num_estatus=:num_estatus,
                fk_a006_num_miscelaneo_detalle_regimen_fiscal=:fk_a006_num_miscelaneo_detalle_regimen_fiscal,
                fk_a006_num_miscelaneo_detalle_clasificacion=:fk_a006_num_miscelaneo_detalle_clasificacion,
                fk_cbc003_num_tipo_voucher=:fk_cbc003_num_tipo_voucher,
                fk_cbc003_num_tipo_voucher_ordPago=:fk_cbc003_num_tipo_voucher_ordPago,
                fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
                fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20,
                fk_cbb004_num_plan_cuenta_ade =:fk_cbb004_num_plan_cuenta_ade,
                fk_cbb004_num_plan_cuenta_ade_pub20=:fk_cbb004_num_plan_cuenta_ade_pub20,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                WHERE
                pk_num_tipo_documento='$idDocumento'

               ");
        $modificarDocumento->execute(array(
            ':ind_descripcion' => $descripcion,
            ':num_flag_adelanto' => $flagAdelanto,
            ':num_flag_auto_nomina' => $flagAutoNomina,
            ':num_flag_fiscal' => $flagFiscal,
            ':num_flag_provision' => $flagProvision,
            ':num_estatus' => $estatus,
            ':fk_a006_num_miscelaneo_detalle_regimen_fiscal' => $regFiscal,
            ':fk_a006_num_miscelaneo_detalle_clasificacion' => $clasificacion,
            ':fk_cbc003_num_tipo_voucher' => $voucher,
            ':fk_cbc003_num_tipo_voucher_ordPago' => $voucherOrdPago,
            ':fk_cbb004_num_plan_cuenta' =>$cuenta,
            ':fk_cbb004_num_plan_cuenta_pub20' =>$cuentaPub20,
            ':fk_cbb004_num_plan_cuenta_ade' =>$cuentaAde,
            ':fk_cbb004_num_plan_cuenta_ade_pub20' =>$cuentaAdePub20
        ));
        $fallaTansaccion = $modificarDocumento->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();

        }
    }

    public function metEliminarDocumento($idDocumento)
    {
        $this->_db->beginTransaction();
        $elimar=$this->_db->prepare("
                DELETE FROM
                  cp_b002_tipo_documento
                WHERE
                  pk_num_tipo_documento=:pk_num_tipo_documento
            ");
        $elimar->execute(array(
            'pk_num_tipo_documento'=>$idDocumento
        ));
        $error=$elimar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idDocumento;
        }


    }

}
