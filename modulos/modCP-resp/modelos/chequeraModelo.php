<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_MODELO.'miscelaneoModelo.php';
require_once 'cuentasModelo.php';

class chequeraModelo extends Modelo
{
    private $atIdUsuario;
    public $atMiscelaneoModelo;
    public $atCuentasModelo;

    public function __construct()
    {
        parent::__construct();

        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo=new miscelaneoModelo();
        $this->atCuentasModelo = new cuentasModelo();
    }

    public function metChequeraListar($idChequera = false)
    {
        if($idChequera){
            $where = "WHERE cp_b012_chequera.pk_num_chequera = '$idChequera'";
        }else{
            $where = "";
        }
        $chequeraListar =  $this->_db->query(
            "SELECT
              cp_b012_chequera.*,
              cp_b014_cuenta_bancaria.ind_num_cuenta,
              a006_miscelaneo_detalle.ind_nombre_detalle AS nombreBanco
            FROM
              cp_b012_chequera
            INNER JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_b012_chequera.fk_cpb014_num_cuenta_bancaria
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b014_cuenta_bancaria.fk_a006_num_miscelaneo_detalle_banco
            $where
            GROUP BY cp_b012_chequera.fk_cpb014_num_cuenta_bancaria

          ");
        //var_dump($documentoListar);
        $chequeraListar->setFetchMode(PDO::FETCH_ASSOC);
        if($idChequera){
            return $chequeraListar->fetch();
        }else{
            return $chequeraListar->fetchAll();
        }
    }

    public function metConsulta($idCuenta)
    {
        $consulta =  $this->_db->query(
            "SELECT
              cp_b012_chequera.*,
              cp_b014_cuenta_bancaria.ind_num_cuenta,
              a006_miscelaneo_detalle.pk_num_miscelaneo_detalle,
              a006_miscelaneo_detalle.ind_nombre_detalle AS nombreBanco
            FROM
              cp_b012_chequera
            INNER JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_b012_chequera.fk_cpb014_num_cuenta_bancaria
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b014_cuenta_bancaria.fk_a006_num_miscelaneo_detalle_banco
            WHERE
              cp_b012_chequera.fk_cpb014_num_cuenta_bancaria='$idCuenta'
          "
        );
        //var_dump($consulta);
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }

    public function metNuevo($form)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
             cp_b012_chequera
              SET
               fk_cpb014_num_cuenta_bancaria=:fk_cpb014_num_cuenta_bancaria,
               ind_num_chequera=:ind_num_chequera,
               num_chequera_desde=:num_chequera_desde,
               num_chequera_hasta=:num_chequera_hasta,
               ind_num_cheque=:ind_num_cheque,
               num_estatus=:num_estatus,
               fec_ultima_modificacion=NOW(),
               fk_a018_num_seguridad_usuario='$this->atIdUsuario'
               ");
        $detalle=$form['chequera'];
        $secuencia=1;
        foreach($detalle['num_secuencia'] AS $i) {
            $ids=$detalle[$i];
            if(!isset($ids['num_estatus'])){
                $ids['num_estatus'] = 0;
            }
            $NuevoRegistro->execute(array(
                'fk_cpb014_num_cuenta_bancaria' => $form['fk_cpb014_num_cuenta_bancaria'],
                'ind_num_chequera' => $secuencia,
                'num_chequera_desde' => $ids['num_chequera_desde'],
                'num_chequera_hasta' => $ids['num_chequera_hasta'],
                'ind_num_cheque' => $ids['ind_num_cheque'],
                'num_estatus' => $ids['num_estatus'],
            ));
            $secuencia++;
        }
        $idCorrelativoChequera = $this->_db->lastInsertId();

        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $this->_db->commit();
            return $idCorrelativoChequera;
        }

    }

    public function metModificar($idCuenta, $form)
    {

        $this->_db->beginTransaction();
        $elimar=$this->_db->prepare("
                DELETE FROM cp_b012_chequera WHERE fk_cpb014_num_cuenta_bancaria=:fk_cpb014_num_cuenta_bancaria
            ");
        $elimar->execute(array(
            'fk_cpb014_num_cuenta_bancaria'=>$idCuenta
        ));
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
             cp_b012_chequera
              SET
               fk_cpb014_num_cuenta_bancaria=:fk_cpb014_num_cuenta_bancaria,
               ind_num_chequera=:ind_num_chequera,
               num_chequera_desde=:num_chequera_desde,
               num_chequera_hasta=:num_chequera_hasta,
               ind_num_cheque=:ind_num_cheque,
               num_estatus=:num_estatus,
               fec_ultima_modificacion=NOW(),
               fk_a018_num_seguridad_usuario='$this->atIdUsuario'
               ");
        $detalle=$form['chequera'];
        $secuencia=1;
        foreach($detalle['num_secuencia'] AS $i) {
            $ids=$detalle[$i];
            if(!isset($ids['num_estatus'])){
                $ids['num_estatus'] = 0;
            }
            $NuevoRegistro->execute(array(
                'fk_cpb014_num_cuenta_bancaria' => $form['fk_cpb014_num_cuenta_bancaria'],
                'ind_num_chequera' => $secuencia,
                'num_chequera_desde' => $ids['num_chequera_desde'],
                'num_chequera_hasta' => $ids['num_chequera_hasta'],
                'ind_num_cheque' => $ids['ind_num_cheque'],
                'num_estatus' => $ids['num_estatus'],
            ));
            $secuencia++;
        }
        $idCorrelativoChequera = $this->_db->lastInsertId();

        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $this->_db->commit();
            return $idCorrelativoChequera;
        }




    }

}
