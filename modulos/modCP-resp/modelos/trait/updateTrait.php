<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

trait updateTrait
{
    private function metCondicionRespuesta($error)
    {
        if(!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        }else{
            return false;
        }
    }

    public function metAnularRechazarOrdenPago($idOrdenPago,$motivo = null, $estadoOP = null)
    {
        if($motivo!=null){
            $fec_anulacion='NOW()';
            $ind_estado='AN';
            $fk_anula="'$this->atIdEmpleado'";
        }else{
            $fec_anulacion='null';
            $fk_anula='null';
            if($estadoOP == 'PP'){
                $ind_estado = $estadoOP;
            }else{
                $ind_estado='PE';
            }
        }
        $ordenPago = $this->_db->prepare("
            UPDATE
                cp_d009_orden_pago
            SET
                fec_anulacion= $fec_anulacion,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fk_rhb001_num_empleado_anula=$fk_anula,
                ind_motivo_anulacion='$motivo',
                ind_estado='$ind_estado'
            WHERE
              pk_num_orden_pago=:pk_num_orden_pago
        ");
        $ordenPago->execute(array(
            'pk_num_orden_pago' => $idOrdenPago
        ));
        $error = $ordenPago->errorInfo();
        return $this->metCondicionRespuesta($error);
    }

    public function metAnularRechazarObligacion($idObligacion, $motivo = null, $tipo = false, $procedencia = false)
    {
        if($motivo!=null){
            $fec_anulacion='NOW()';
            $ind_estado='AN';
            $fk_anula="'$this->atIdEmpleado'";
        }else{
            if($tipo=='orden') {
                $fec_anulacion='null';
                $ind_estado='CO';
                $fk_anula='null';
            }else{
                $fec_anulacion='null';
                $ind_estado='PR';
                $fk_anula='null';
            }
        }
        $obligacion = $this->_db->prepare("
            UPDATE
                cp_d001_obligacion
            SET
                fec_anulacion= $fec_anulacion,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fk_rhb001_num_empleado_anula= $fk_anula,
                ind_motivo_anulacion='$motivo',
                ind_estado='$ind_estado'
            WHERE
              pk_num_obligacion=:pk_num_obligacion
        ");
        $obligacion->execute(array(
            'pk_num_obligacion' => $idObligacion
        ));
        if($procedencia!=''){
            $viatico = $this->_db->prepare("
            UPDATE
                cp_c003_viatico_detalle
            SET
                ind_estado='PR'
            WHERE
              fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion
        ");
            $viatico->execute(array(
                'fk_cpd001_num_obligacion' => $idObligacion
            ));
        }
        $error = $obligacion->errorInfo();
        return $this->metCondicionRespuesta($error);
    }

    public function metAnularEstadoDistribucion($idObligacion,$tipo = false, $eliminar = false, $flagCompromiso = false)
    {
        if($tipo){
            $where=" AND ind_tipo_distribucion='$tipo'";
        }
        if($flagCompromiso==0){
            $where = "";
        }
        if(!$eliminar) {
            $obligacion = $this->_db->prepare("
                UPDATE
                    pr_d008_estado_distribucion
                SET
                    ind_estado='AN'
                WHERE
                    ind_estado='AC' AND
                    fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion
                    $where
            ");
            //var_dump($obligacion);
            $obligacion->execute(array(
                'fk_cpd001_num_obligacion' => $idObligacion
            ));
        }else{
            $obligacion = $this->_db->prepare("
                DELETE FROM
                    pr_d008_estado_distribucion
                WHERE
                    fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion
            ");
            $obligacion->execute(array(
                'fk_cpd001_num_obligacion' => $idObligacion
            ));
        }
        $error = $obligacion->errorInfo();
        return $this->metCondicionRespuesta($error);
    }

}
