<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        13-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class unidadTributariaModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarUnidadTributaria($idUnidadTributaria)
    {
        $aplicacion = $this->_db->query("
            SELECT * FROM cp_b018_unidad_tributaria WHERE pk_num_unidad_tributaria ='$idUnidadTributaria'
        ");
        $aplicacion->setFetchMode(PDO::FETCH_ASSOC);

        return $aplicacion->fetch();
    }

    public function metListarUnidadTributaria()
    {
        $pais = $this->_db->query("SELECT * FROM cp_b018_unidad_tributaria");
        $pais->setFetchMode(PDO::FETCH_ASSOC);
        return $pais->fetchAll();
    }

    public function metCrearModificarUnidadTributaria($idUnidadTributaria,$form)
    {
        if($idUnidadTributaria!=0){
            $head="UPDATE";
            $foot="WHERE pk_num_unidad_tributaria ='$idUnidadTributaria' ";
        }else{
            $head="INSERT INTO";
            $foot="";
        }

        $this->_db->beginTransaction();
        $codigo = count($this->metListarUnidadTributaria()) + 1;
        $mun = "0";
        for ($i = 0; $i < (3 - strlen($codigo)); $i++) {
            $mun .= "0";
        }
        $codigo = $mun . $codigo;
        $registro=$this->_db->prepare(
            "$head
                cp_b018_unidad_tributaria
            SET
                ind_anio=:ind_anio, 
                num_secuencia=:num_secuencia, 
                ind_valor=:ind_valor, 
                fec=:fec, 
                txt_gaceta_oficial=:txt_gaceta_oficial, 
                ind_providencia_num=:ind_providencia_num, 
                fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
                fec_ultima=NOW() 
            $foot
            "
        );
        $registro->execute(array(
            ':ind_anio' => date('Y'),
            ':num_secuencia' => $codigo,
            ':ind_valor' => $form['ind_valor'],
            ':fec' => $form['fec'],
            ':txt_gaceta_oficial' => $form['txt_gaceta_oficial'],
            ':ind_providencia_num' => $form['ind_providencia_num']
        ));

        if($idUnidadTributaria!=0){
            $idUnidadTributaria = $idUnidadTributaria;
        }else{
            $idUnidadTributaria = $this->_db->lastInsertId();
        }


        $error = $registro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idUnidadTributaria;
        }
    }

}
