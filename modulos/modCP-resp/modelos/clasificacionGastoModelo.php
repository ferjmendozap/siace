<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO:Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_MODELO.'miscelaneoModelo.php';

class clasificacionGastoModelo extends Modelo
{
    private $atIdUsuario;
    public $atMiscelaneoModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo=new miscelaneoModelo();
    }

    public function metClasificacionGastoListar($idClasifGasto = false)
    {
        if($idClasifGasto){
            $where = "WHERE cp_b004_clasificacion_gastos.pk_num_clasificacion_gastos = '$idClasifGasto'";
        } else {
            $where = "";
        }
        $clasifGastoListar =  $this->_db->query(
            "SELECT
                *
              FROM
              cp_b004_clasificacion_gastos
              $where
              ORDER BY cod_clasificacion ASC
             " );
        $clasifGastoListar->setFetchMode(PDO::FETCH_ASSOC);
        if($idClasifGasto){
            return $clasifGastoListar->fetch();
        } else {
            return $clasifGastoListar->fetchAll();
        }

    }
    public function metConceptoGastoListar()
    {
        $impuestoListar =  $this->_db->query(
            "SELECT
              *
              FROM
              cp_b005_concepto_gasto
              ORDER BY cod_concepto_gasto ASC
             " );
        $impuestoListar->setFetchMode(PDO::FETCH_ASSOC);
        return $impuestoListar->fetchAll();
    }
    public function metMostrarConcepto($idClasifGasto){
        $consultaConceptoGasto = $this->_db->query("
              SELECT
                 *
              FROM
                 cp_c001_concepto_clasificacion_gasto
              INNER JOIN
                 cp_b004_clasificacion_gastos on cp_b004_clasificacion_gastos.pk_num_clasificacion_gastos = cp_c001_concepto_clasificacion_gasto.fk_cpb004_num_clasificacion_gastos
              WHERE
                 cp_b004_clasificacion_gastos.pk_num_clasificacion_gastos = $idClasifGasto
             ");
        $consultaConceptoGasto->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaConceptoGasto->fetchAll();
    }

    public function metClasifGastoConsulta($idClasifGasto)
    {
        $consultaClasifGasto = $this->_db->query(
            "SELECT
              cp_b004_clasificacion_gastos.*,
              a018.ind_usuario
             FROM
              cp_b004_clasificacion_gastos
             INNER JOIN a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario = cp_b004_clasificacion_gastos.fk_a018_num_seguridad_usuario

             WHERE
             cp_b004_clasificacion_gastos.pk_num_clasificacion_gastos = $idClasifGasto"
        );
        $consultaClasifGasto->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaClasifGasto->fetch();
    }
    public function metClasifGastoConsultaDetalle($idClasifGasto)
    {
        $consultaClasifGasto = $this->_db->query(
            "SELECT
              cp_b004_clasificacion_gastos.*,
              a018.ind_usuario,
              concepto.pk_num_concepto_clasificacion_gastos,
              concepto.fk_cpb005_num_concepto_gasto
             FROM
              cp_b004_clasificacion_gastos
             INNER JOIN a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario = cp_b004_clasificacion_gastos.fk_a018_num_seguridad_usuario
             INNER JOIN cp_c001_concepto_clasificacion_gasto AS concepto ON concepto.fk_cpb004_num_clasificacion_gastos = cp_b004_clasificacion_gastos.pk_num_clasificacion_gastos
             WHERE
             cp_b004_clasificacion_gastos.pk_num_clasificacion_gastos = $idClasifGasto"
        );
        $consultaClasifGasto->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaClasifGasto->fetch();
    }



    public function metNuevaClasifGasto($form)
    {
        $this->_db->beginTransaction();
        $nuevaClasifGasto = $this->_db->prepare(
            "INSERT INTO
             cp_b004_clasificacion_gastos
              SET
              cod_clasificacion=:cod_clasificacion,
              ind_descripcion=:ind_descripcion,
              fk_a006_num_miscelaneo_detalle_aplicacion=:fk_a006_num_miscelaneo_detalle_aplicacion,
              num_estatus=:num_estatus,
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
               ");
        $nuevaClasifGasto->execute(array(
            ':cod_clasificacion' => $form['cod_clasificacion'],
            ':ind_descripcion' => $form['ind_descripcion'],
            ':fk_a006_num_miscelaneo_detalle_aplicacion' => $form['fk_a006_num_miscelaneo_detalle_aplicacion'],
            ':num_estatus' => $form['num_estatus']
        ));
        $idClasificacion = $this->_db->lastInsertId();
        #  Concepto de gastos
        $nuevoConcepto =  $this->_db->prepare("
          INSERT INTO
          cp_c001_concepto_clasificacion_gasto
          SET
          fk_cpb004_num_clasificacion_gastos=:fk_cpb004_num_clasificacion_gastos,
          fk_cpb005_num_concepto_gasto=:fk_cpb005_num_concepto_gasto,
          fec_ultima_modificacion=NOW(),
          fk_a018_num_seguridad_usuario='$this->atIdUsuario'
          ");
        if(isset($form['nuevo'])) {
            $detalle = $form['nuevo'];
            $secuencia = 1;
            foreach ($detalle['secuencia'] AS $i) {
                $ids = $detalle[$i];
                $nuevoConcepto->execute(array(
                    ':fk_cpb004_num_clasificacion_gastos'=>$idClasificacion,
                    ':fk_cpb005_num_concepto_gasto'=>$ids['pk_num_concepto_gasto']
                ));
                $secuencia++;
            }
        }
        $fallaTansaccion = $nuevaClasifGasto->errorInfo();
        $fallaTansaccion2 = $nuevoConcepto->errorInfo();
        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } elseif (!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion2;
        } else {
            $this->_db->commit();
            return $idClasificacion;
        }
    }

    public function metModificarClasifGasto($idClasifGasto,$form)
    {
        $this->_db->beginTransaction();
        $nuevaClasifGasto = $this->_db->prepare(
            "UPDATE
             cp_b004_clasificacion_gastos
              SET
              ind_descripcion=:ind_descripcion,
              fk_a006_num_miscelaneo_detalle_aplicacion=:fk_a006_num_miscelaneo_detalle_aplicacion,
              num_estatus=:num_estatus,
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              WHERE
              pk_num_clasificacion_gastos='$idClasifGasto'
               ");
        $nuevaClasifGasto->execute(array(
            ':ind_descripcion' => $form['ind_descripcion'],
            ':fk_a006_num_miscelaneo_detalle_aplicacion' => $form['fk_a006_num_miscelaneo_detalle_aplicacion'],
            ':num_estatus' => $form['num_estatus']
        ));
        #  Concepto de gastos
        $this->_db->query("DELETE FROM cp_c001_concepto_clasificacion_gasto WHERE fk_cpb004_num_clasificacion_gastos='$idClasifGasto'");
        if(isset($form['nuevo'])) {
            $detalle = $form['nuevo'];
            $secuencia = 1;
            foreach ($detalle['secuencia'] AS $i) {
                $ids = $detalle[$i];
                $nuevoConcepto =  $this->_db->prepare("
                  INSERT INTO
                  cp_c001_concepto_clasificacion_gasto
                  SET
                  fk_cpb004_num_clasificacion_gastos=:fk_cpb004_num_clasificacion_gastos,
                  fk_cpb005_num_concepto_gasto=:fk_cpb005_num_concepto_gasto,
                  fec_ultima_modificacion=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");
                $nuevoConcepto->execute(array(
                    ':fk_cpb004_num_clasificacion_gastos'=>$idClasifGasto,
                    ':fk_cpb005_num_concepto_gasto'=>$ids['pk_num_concepto_gasto']
                ));
                $secuencia++;
            }
        }

        $fallaTansaccion = $nuevaClasifGasto->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $this->_db->commit();
            return $idClasifGasto;
        }
    }

    public function metEliminarClasifGasto($idClasifGasto){
        $this->_db->beginTransaction();
        $elimar=$this->_db->prepare("
           DELETE FROM cp_b004_clasificacion_gastos
           WHERE pk_num_clasificacion_gastos=:pk_num_clasificacion_gastos
            ");
        $elimar->execute(array(
            'pk_num_clasificacion_gastos'=>$idClasifGasto
        ));
        $error=$elimar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idClasifGasto;
        }
    }
}
