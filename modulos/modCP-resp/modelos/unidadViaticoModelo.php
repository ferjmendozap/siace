<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class unidadViaticoModelo extends Modelo
{
    private $atIdUsuario;
    public $atIdEmpleado;


    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
    }

    public function metUnidadViaticoModeloListar($estado = false, $idUnidadViatico = false)
    {
        $where = "";
        if ($estado) {
            $where .= "AND ind_estado='$estado'";
        }
        if($idUnidadViatico){
            $where .= "AND cp_b008_unidad_viatico.pk_num_unidad_viatico = '$idUnidadViatico'";
        }
        $Listar = $this->_db->query(
            "SELECT * FROM  cp_b008_unidad_viatico
            WHERE 1
            $where
        ");
        $Listar->setFetchMode(PDO::FETCH_ASSOC);
        if($idUnidadViatico){
            return $Listar->fetch();
        }else{
            return $Listar->fetchAll();
        }
    }

    public function metConsultaUnidadViaticoModelo($idUnidadViatico)
    {
        $consultar = $this->_db->query("
            SELECT
               cp_b008_unidad_viatico.*,
               a018_seguridad_usuario.ind_usuario,
               (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_b008_unidad_viatico ON rh_b001_empleado.pk_num_empleado = cp_b008_unidad_viatico.fk_rhb001_num_empleado_preparado
                    WHERE
                        pk_num_unidad_viatico = '$idUnidadViatico'
                ) AS EMPLEADO_PREPARA,
                (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_b008_unidad_viatico ON rh_b001_empleado.pk_num_empleado = cp_b008_unidad_viatico.fk_rhb001_num_empleado_aprobado
                    WHERE
                        pk_num_unidad_viatico = '$idUnidadViatico'
                ) AS EMPLEADO_APRUEBA
             FROM
               cp_b008_unidad_viatico
            INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = cp_b008_unidad_viatico.fk_a018_num_seguridad_usuario
            WHERE
            pk_num_unidad_viatico='$idUnidadViatico'
             ");
        $consultar->setFetchMode(PDO::FETCH_ASSOC);
        return $consultar->fetch();
    }

    public function metNuevaUnidadViatico($resolucion,$periodo,$numViatico,$descripcion)
    {
        $this->_db->beginTransaction();
        $registrar = $this->_db->prepare("
            INSERT INTO
              cp_b008_unidad_viatico
            SET
              ind_num_resolucion=:ind_num_resolucion,
              ind_periodo=:ind_periodo,
              num_unidad_viatico=:num_unidad_viatico,
              ind_estado='PR',
              ind_descripcion=:ind_descripcion,
              fec_preparado=NOW(),
              fk_rhb001_num_empleado_preparado='$this->atIdEmpleado',
              fec_ultima_modificacion=NOW(),
             fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             ");
        $registrar->execute(array(
            'ind_num_resolucion' => $resolucion,
            'ind_periodo' => $periodo,
            'num_unidad_viatico' => $numViatico,
            'ind_descripcion' => $descripcion
        ));

        $fallaTansaccion = $registrar->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarUnidadViatico($idUnidadViatico,$resolucion,$periodo,$numViatico,$descripcion)
    {
        $this->_db->beginTransaction();
        $registrar = $this->_db->prepare("
            UPDATE
              cp_b008_unidad_viatico
            SET
              ind_num_resolucion=:ind_num_resolucion,
              ind_periodo=:ind_periodo,
              num_unidad_viatico=:num_unidad_viatico,
              ind_estado='PR',
              ind_descripcion=:ind_descripcion,
              fec_preparado=NOW(),
              fk_rhb001_num_empleado_preparado='$this->atIdEmpleado',
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            WHERE
              pk_num_unidad_viatico='$idUnidadViatico'
             ");
        $registrar->execute(array(
            'ind_num_resolucion' => $resolucion,
            'ind_periodo' => $periodo,
            'num_unidad_viatico' => $numViatico,
            'ind_descripcion' => $descripcion
        ));

        $fallaTansaccion = $registrar->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metAprobatUnidadViatico($idUnidadViatico)
    {
        $this->_db->beginTransaction();
        $registrar = $this->_db->prepare("
            UPDATE
              cp_b008_unidad_viatico
            SET
              ind_estado='AP',
              fec_aprobado=NOW(),
              fk_rhb001_num_empleado_aprobado='$this->atIdEmpleado'
            WHERE
              pk_num_unidad_viatico='$idUnidadViatico'
             ");
        $registrar->execute(array(
            'pk_num_unidad_viatico' => $idUnidadViatico
        ));

        $fallaTansaccion = $registrar->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $this->_db->commit();
            return $idUnidadViatico;
        }
    }
}