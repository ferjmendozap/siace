<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once 'trait' . DS . 'updateTrait.php';
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once 'trait'.DS.'consultasTrait.php';
require_once 'cuentasModelo.php';
require_once 'obligacionModelo.php';
require_once RUTA_Modulo . 'modCB' . DS . 'modelos' . DS . 'ListaVoucherModelo.php';

class pagosModelo extends Modelo
{
    use consultasTrait;
    use updateTrait;
    private $atIdUsuario;
    public $atMiscelaneoModelo;
    public $atCuentasModelo;
    public $atObligacionModelo;
    public $atListaVoucherModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atCuentasModelo = new cuentasModelo();
        $this->atObligacionModelo = new obligacionModelo();
        $this->atListaVoucherModelo = new ListaVoucherModelo();
    }

    public function metListarPagos()
    {
        $listar = $this->_db->query(
            "SELECT
              cp_d010_pago.pk_num_pago,
              cp_d010_pago.fk_cbb001_num_voucher_pago,
              cp_b014_cuenta_bancaria.ind_num_cuenta,
              cp_d010_pago.ind_num_pago,
              cb_b001_voucher.ind_voucher,
              cb_b001_voucher.ind_anio,
              cb_b001_voucher.ind_mes,
              CONCAT_WS(' ',a003_persona.ind_nombre1, a003_persona.ind_apellido1) AS fk_a003_num_persona_proveedor_a_pagar,
                (CASE
                        WHEN cp_d010_pago.ind_estado='GE' THEN 'GENERADO'
                        WHEN cp_d010_pago.ind_estado='IM' THEN 'IMPRESO'
                        WHEN cp_d010_pago.ind_estado='AN' THEN 'ANULADO'
                END) AS ind_estado,
                CONCAT(
                    REPLACE(FORMAT(FLOOR(ROUND(cp_d010_pago.num_monto_pago, 2)), 0), ',', '.'),
                    ',',
                    SUBSTRING_INDEX(FORMAT(ROUND(cp_d010_pago.num_monto_pago,2), 2), '.', -1)
              ) AS num_monto_pago,
                cp_d010_pago.num_monto_retenido,
                date_format(cp_d010_pago.fec_pago,'%d-%m-%Y') AS fec_pago,
                cp_d010_pago.ind_num_cheque,
                a006_miscelaneo_detalle_tipo_pago.ind_nombre_detalle AS a006_miscelaneo_detalle_tipo_pago
            FROM
              cp_d010_pago
              INNER JOIN cp_d009_orden_pago ON cp_d010_pago.fk_cpd009_num_orden_pago = cp_d009_orden_pago.pk_num_orden_pago
              INNER JOIN cp_d001_obligacion ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
              INNER JOIN cp_b014_cuenta_bancaria ON cp_d001_obligacion.fk_cpb014_num_cuenta = cp_b014_cuenta_bancaria.pk_num_cuenta
              INNER JOIN a003_persona ON cp_d001_obligacion.fk_a003_num_persona_proveedor_a_pagar = a003_persona.pk_num_persona
              INNER JOIN a006_miscelaneo_detalle AS a006_miscelaneo_detalle_tipo_pago ON cp_d001_obligacion.fk_a006_num_miscelaneo_tipo_pago = a006_miscelaneo_detalle_tipo_pago.pk_num_miscelaneo_detalle
              LEFT JOIN cb_b001_voucher ON cb_b001_voucher.pk_num_voucher_mast = cp_d010_pago.fk_cbb001_num_voucher_pago

             WHERE
              cp_d010_pago.ind_estado = 'IM'
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metConsultarPago($idPago)
    {
        $listar = $this->_db->query(
            "SELECT
              cp_d010_pago.*,
              cp_d010_pago.fec_pago as fechaPago,
              cb_b001_voucher.ind_voucher,
              cb_b001_voucher.pk_num_voucher_mast,
              cb_b001_voucher.ind_anio,
              cb_b001_voucher.ind_mes,
              (CASE
                        WHEN cp_d010_pago.ind_estado='GE' THEN 'GENERADO'
                        WHEN cp_d010_pago.ind_estado='IM' THEN 'IMPRESO'
                        WHEN cp_d010_pago.ind_estado='AN' THEN 'ANULADO'
                END) AS ind_estado_pago,
              (CASE
                        WHEN cp_d010_pago.ind_estado_entrega='C' THEN 'CUSTODIA'
                        WHEN cp_d010_pago.ind_estado_entrega='E' THEN 'ENTREGADO'
                        WHEN cp_d010_pago.ind_estado_entrega='D' THEN 'DEVUELTO'
                END) AS ind_estado_entrega,
              cp_d001_obligacion.*,
              a018_seguridad_usuario.ind_usuario,
                CONCAT(
                    REPLACE(FORMAT(FLOOR(ROUND(cp_d010_pago.num_monto_pago, 2)), 0), ',', '.'),
                    ',',
                    SUBSTRING_INDEX(FORMAT(ROUND(cp_d010_pago.num_monto_pago,2), 2), '.', -1)
                ) AS num_monto_pago,
                CONCAT(
                    REPLACE(FORMAT(FLOOR(ROUND(cp_d001_obligacion.num_monto_obligacion, 2)), 0), ',', '.'),
                    ',',
                    SUBSTRING_INDEX(FORMAT(ROUND(cp_d001_obligacion.num_monto_obligacion,2), 2), '.', -1)
                ) AS num_monto_obligacion,
                 CONCAT(
                    REPLACE(FORMAT(FLOOR(ROUND(cp_d001_obligacion.num_monto_impuesto_otros, 2)), 0), ',', '.'),
                    ',',
                    SUBSTRING_INDEX(FORMAT(ROUND(cp_d001_obligacion.num_monto_impuesto_otros,2), 2), '.', -1)
                ) AS num_monto_impuesto_otros,
              a006_miscelaneo_detalle_banco.ind_nombre_detalle AS a006_miscelaneo_detalle_banco,
              cp_b014_cuenta_bancaria.pk_num_cuenta,
              cp_b014_cuenta_bancaria.ind_num_cuenta,
              cp_d009_orden_pago.ind_num_orden,
              cp_b002_tipo_documento.num_flag_provision,
              a006_miscelaneo_detalle_tipo_pago.pk_num_miscelaneo_detalle AS a006_miscelaneo_detalle_tipo_pago,
              a006_miscelaneo_detalle_tipo_pago.ind_nombre_detalle AS tipo_pago,
             CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1) AS nombreProveedor,
             (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d010_pago ON cp_d010_pago.fk_rhb001_num_empleado_anula = rh_b001_empleado.pk_num_empleado
                    WHERE
                        pk_num_pago='$idPago'
                ) AS EMPLEADO_ANULA,
              (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d010_pago ON cp_d010_pago.fk_rhb001_num_empleado_crea = rh_b001_empleado.pk_num_empleado
                    WHERE
                        pk_num_pago='$idPago'
                ) AS EMPLEADO_GENERA,
              (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.fk_rhb001_num_empleado_conforma = rh_b001_empleado.pk_num_empleado
                    WHERE
                        pk_num_obligacion=cp_d009_orden_pago.fk_cpd001_num_obligacion
                ) AS EMPLEADO_CONFORMA,
                 (
                    SELECT
                      puesto.ind_descripcion_cargo
                    FROM
                      rh_b001_empleado 
                    INNER JOIN a003_persona AS persona ON persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                    INNER JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                    INNER JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
                    INNER JOIN rh_c006_tipo_cargo AS cargo ON cargo.pk_num_cargo = puesto.fk_rhc006_num_cargo
                    INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.fk_rhb001_num_empleado_conforma = rh_b001_empleado.pk_num_empleado
                    WHERE
                      pk_num_obligacion=cp_d009_orden_pago.fk_cpd001_num_obligacion
                ) AS CARGO_CONFORMA,
                cp_d001_obligacion.fec_conformado,
              (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.fk_rhb001_num_empleado_aprueba = rh_b001_empleado.pk_num_empleado
                    WHERE
                        pk_num_obligacion=cp_d009_orden_pago.fk_cpd001_num_obligacion
                ) AS EMPLEADO_APRUEBA,
                (
                    SELECT
                      puesto.ind_descripcion_cargo
                    FROM
                      rh_b001_empleado 
                    INNER JOIN a003_persona AS persona ON persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                    INNER JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                    INNER JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
                    INNER JOIN rh_c006_tipo_cargo AS cargo ON cargo.pk_num_cargo = puesto.fk_rhc006_num_cargo
                    INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.fk_rhb001_num_empleado_aprueba = rh_b001_empleado.pk_num_empleado
                    WHERE
                      pk_num_obligacion=cp_d009_orden_pago.fk_cpd001_num_obligacion
                ) AS CARGO_APRUEBA,
                CONCAT_WS(' ',a003_persona.ind_nombre1, a003_persona.ind_apellido1) AS fk_a003_num_persona_proveedor_a_pagar,
                cp_d001_obligacion.fec_aprobado,
                a003_persona.pk_num_persona,
                cp_d010_pago.num_monto_pago AS pago,
                cp_d010_pago.ind_estado AS estadoPago,
                cp_d010_pago.ind_motivo_anulacion AS motivoAnulacionPago,
                cp_d009_orden_pago.fk_cpd001_num_obligacion,
                cp_d009_orden_pago.ind_estado AS ind_estado_op

            FROM
              cp_d010_pago
            INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = cp_d010_pago.fk_a018_num_seguridad_usuario
            INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
            INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            INNER JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = cp_d001_obligacion.fk_cpb002_num_tipo_documento
            INNER JOIN cp_b014_cuenta_bancaria ON cp_d001_obligacion.fk_cpb014_num_cuenta = cp_b014_cuenta_bancaria.pk_num_cuenta
            INNER JOIN a006_miscelaneo_detalle AS a006_miscelaneo_detalle_banco ON cp_b014_cuenta_bancaria.fk_a006_num_miscelaneo_detalle_banco = a006_miscelaneo_detalle_banco.pk_num_miscelaneo_detalle
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor_a_pagar
            INNER JOIN a006_miscelaneo_detalle AS a006_miscelaneo_detalle_tipo_pago ON cp_d001_obligacion.fk_a006_num_miscelaneo_tipo_pago = a006_miscelaneo_detalle_tipo_pago.pk_num_miscelaneo_detalle
            LEFT JOIN cb_b001_voucher ON cb_b001_voucher.pk_num_voucher_mast = cp_d010_pago.fk_cbb001_num_voucher_pago
           WHERE
              cp_d010_pago.pk_num_pago = '$idPago'
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metBuscarRevisor($codigo)
    {
        $listar = $this->_db->query(
            "
              SELECT
            CONCAT_WS(' ',a003_persona.ind_nombre1, a003_persona.ind_apellido1) AS nombreRevisor,
              puesto.ind_descripcion_cargo
            FROM
              rh_b001_empleado
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            INNER JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
            INNER JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
            WHERE
              rh_b001_empleado.cod_empleado = '$codigo'
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metAnularPago($idPago, $motivo,$cuenta)
    {
        $this->_db->beginTransaction();
        $anulacionPago = $this->_db->prepare("
            UPDATE
                cp_d010_pago
            SET
                fec_anulacion=NOW(),
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fk_rhb001_num_empleado_anula= '$this->atIdEmpleado',
                ind_motivo_anulacion=:ind_motivo_anulacion,
                ind_estado='AN'
            WHERE
              pk_num_pago=:pk_num_pago
        ");
        $anulacionPago->execute(array(
            'pk_num_pago' => $idPago,
            'ind_motivo_anulacion' => $motivo
        ));
        $pago = $this->metConsultarPago($idPago);

        $rechazarOrdenPago = $this->metAnularRechazarOrdenPago($pago['fk_cpd009_num_orden_pago'],false, $pago['ind_estado_op']);
        $anularEstadoDistribucion = $this->metAnularEstadoDistribucion($pago['fk_cpd001_num_obligacion'],'PA',false,true);
        $num_flag_automatico = 1;
        $bancoTransaccion = $this->_db->prepare("
            INSERT INTO
                cp_d011_banco_transaccion
            SET
                fk_cpb006_num_banco_tipo_transaccion =:fk_cpb006_num_banco_tipo_transaccion,
                fk_cpb014_num_cuenta_bancaria =:fk_cpb014_num_cuenta_bancaria,
                fk_cpb002_num_tipo_documento =:fk_cpb002_num_tipo_documento,
                fk_a023_num_centro_costo =:fk_a023_num_centro_costo,
                fk_cpd010_num_pago = '$idPago',
                fk_rhb001_num_empleado_crea = '$this->atIdEmpleado',
                ind_num_transaccion =:ind_num_transaccion,
                ind_periodo_contable =:ind_periodo_contable,
                ind_estado = 'AP',
                fec_anio = NOW(),
                fec_transaccion = NOW(),
                fec_ultima_modificacion=NOW(),
                num_monto=:num_monto,
                num_secuencia=:num_secuencia,
                num_flag_presupuesto=:num_flag_presupuesto,
                num_flag_automatico=:num_flag_automatico,
                txt_comentarios=:txt_comentarios,
                ind_mes =LPAD( MONTH (NOW()),2,'0'),
                fec_preparacion=NOW(),
                fk_a003_num_persona_proveedor =:fk_a003_num_persona_proveedor,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
        ");

        $tipoTransaccion = $this->metBuscarTipoTransaccion(Session::metObtener('TRANSANUL'));
        $secuencia = $this->metSecuencia('cp_d011_banco_transaccion', 'ind_num_transaccion');
        if ($tipoTransaccion['cod_detalle'] == 'E') {
            $signo = -1;
        } else {
            $signo = 1;
        }
        $bancoTransaccion->execute(array(
            'fk_cpb006_num_banco_tipo_transaccion' => $tipoTransaccion['pk_num_banco_tipo_transaccion'],
            'fk_cpb014_num_cuenta_bancaria' => $cuenta,
            'fk_cpb002_num_tipo_documento' => $pago['fk_cpb002_num_tipo_documento'],
            'fk_a023_num_centro_costo' => $pago['fk_a023_num_centro_de_costo'],
            'ind_num_transaccion' => $secuencia['secuencia'],
            'ind_periodo_contable' => date('Y-m'),
            'num_monto' => $pago['pago'] * $signo,
            'num_secuencia' => 1,
            'num_flag_presupuesto' => $pago['num_flag_presupuesto'],
            'num_flag_automatico' => $num_flag_automatico,
            'txt_comentarios' => $pago['ind_comentarios'],
            'fk_a003_num_persona_proveedor' => $pago['pk_num_persona']
        ));
        $anularRetencion = $this->_db->prepare("
            UPDATE
                cp_e001_retenciones
            SET
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                ind_estado='AN'
            WHERE
              fk_cpd010_num_orden_pago=:fk_cpd010_num_orden_pago
        ");
        $anularRetencion->execute(array(
            'fk_cpd010_num_orden_pago' => $pago['fk_cpd009_num_orden_pago']
        ));

        $error1 = $anulacionPago->errorInfo();
        $error2 = $bancoTransaccion->errorInfo();
        $error3 = $anularRetencion->errorInfo();
        if ((!empty($error1[1]) && !empty($error1[2]))) {
            $this->_db->rollBack();
            return $error1;
        } elseif (!empty($error2[1]) && !empty($error2[2])) {
            $this->_db->rollBack();
            return $error2;
        }elseif (!empty($error3[1]) && !empty($error3[2])) {
            $this->_db->rollBack();
            return $error3;
        } elseif ($rechazarOrdenPago) {
            $this->_db->rollBack();
            return $rechazarOrdenPago;
        } elseif ($anularEstadoDistribucion) {
            $this->_db->rollBack();
            return $anularEstadoDistribucion;
        } else {
            $this->_db->commit();
            return $idPago;
        }
    }

    public function metDetalleVoucher($idVoucherPago,$tipo=false){
        if($tipo == 'ordenPago'){
            $innerJoin = "INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.fk_cbb001_num_voucher_asiento = cb_b001_voucher.pk_num_voucher_mast";
        }else{
            $innerJoin = "INNER JOIN cp_d010_pago ON cp_d010_pago.fk_cbb001_num_voucher_pago = cb_b001_voucher.pk_num_voucher_mast
            INNER JOIN cp_d009_orden_pago on cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago";
        }
        $listar = $this->_db->query(
            "SELECT cb_c001_voucher_det.*,
                cb_b004_plan_cuenta.cod_cuenta,
                proveedor.pk_num_persona,
                CONCAT(proveedor.ind_nombre1,' ',proveedor.ind_apellido1) AS proveedor
            FROM
                cb_c001_voucher_det
            INNER JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cb_c001_voucher_det.fk_cbb004_num_cuenta
            INNER JOIN cb_b001_voucher ON cb_b001_voucher.pk_num_voucher_mast = cb_c001_voucher_det.fk_cbb001_num_voucher_mast
            $innerJoin
            INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            INNER JOIN a003_persona AS proveedor ON  proveedor.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor
            WHERE
            cb_c001_voucher_det.fk_cbb001_num_voucher_mast = '$idVoucherPago'
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();

    }

    public function metImpuesto($idObligacion)
    {
        $impuesto = $this->_db->query("
            SELECT
              ABS(SUM(cp_d012_obligacion_impuesto.num_monto_afecto)) AS Monto
            FROM
              cp_d012_obligacion_impuesto
            WHERE
              cp_d012_obligacion_impuesto.fk_cpd001_num_obligacion='$idObligacion'
        ");
        $impuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $impuesto->fetch();
    }


}
