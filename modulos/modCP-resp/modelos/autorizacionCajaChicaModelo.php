<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO:Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class autorizacionCajaChicaModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }
    public function metListarAutorizacionCC($idAutorizacionCC = false)
    {
        if($idAutorizacionCC){
            $where = "WHERE cp_b013_cajachica_autorizacion.pk_num_cajachica_autorizacion = '$idAutorizacionCC'";
        } else {
            $where = "";
        }
        $autorizacionListar =  $this->_db->query(
            "SELECT
            cp_b013_cajachica_autorizacion.*,
            cp_b013_cajachica_autorizacion.num_estatus AS estatus,
            a003_persona.*,
            rh_b001_empleado.pk_num_empleado,
            CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1) AS nombreEmpleado
              FROM
            cp_b013_cajachica_autorizacion
            INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = cp_b013_cajachica_autorizacion.fk_rhb001_num_empleado
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
             " );
        $autorizacionListar->setFetchMode(PDO::FETCH_ASSOC);
        if($idAutorizacionCC){
            return $autorizacionListar->fetch();
        } else {
            return $autorizacionListar->fetchAll();
        }
    }

    public function metConsultaAutorizacionCC($idAutorizacionCC)
    {
        $autorizacion =  $this->_db->query(
            "SELECT
            cp_b013_cajachica_autorizacion.*,
            cp_b013_cajachica_autorizacion.num_estatus AS estatus,
            cp_b013_cajachica_autorizacion.fec_ultima_modificacion AS fecha,
            a003_persona.*,
            rh_b001_empleado.pk_num_empleado,
            a018.ind_usuario,
            CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1) AS nombreEmpleado
              FROM
            cp_b013_cajachica_autorizacion
            INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = cp_b013_cajachica_autorizacion.fk_rhb001_num_empleado
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            INNER JOIN a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario = cp_b013_cajachica_autorizacion.fk_a018_num_seguridad_usuario

            WHERE
            cp_b013_cajachica_autorizacion.pk_num_cajachica_autorizacion = $idAutorizacionCC"
        );
        $autorizacion->setFetchMode(PDO::FETCH_ASSOC);
        return $autorizacion->fetch();


    }
    public function metListarEmpleado()
    {
        $empelado = $this->_db->query("
            SELECT
            a003_persona.*,
            rh_b001_empleado.pk_num_empleado,
            CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1),
            a004_dependencia.ind_dependencia
            FROM
            rh_b001_empleado
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
            INNER JOIN a004_dependencia ON a004_dependencia.pk_num_dependencia = rh_c076_empleado_organizacion.fk_a004_num_dependencia

        ");
        $empelado->setFetchMode(PDO::FETCH_ASSOC);
        return $empelado->fetchAll();
    }

    public function metNuevaAutorizacion($empleado,$monto,$estado)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "INSERT INTO
             cp_b013_cajachica_autorizacion
              SET
              fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
              num_monto_autorizado=:num_monto_autorizado,
              num_estatus=:num_estatus,
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
               ");
        $NuevoRegistro->execute(array(
            ':fk_rhb001_num_empleado' => $empleado,
            ':num_monto_autorizado' => $monto,
            ':num_estatus' => $estado
        ));
        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }
    public function metModificarAutorizacion($idAutorizacionCC,$monto,$estado)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
             cp_b013_cajachica_autorizacion
              SET
              num_monto_autorizado=:num_monto_autorizado,
              num_estatus=:num_estatus,
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              WHERE
              pk_num_cajachica_autorizacion='$idAutorizacionCC'
               ");
        $NuevoRegistro->execute(array(
            ':num_monto_autorizado' => $monto,
            ':num_estatus' => $estado
        ));
        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metEliminarAutorizacion($idAutorizacionCC){
        $this->_db->beginTransaction();
        $elimar=$this->_db->prepare("
           DELETE FROM cp_b013_cajachica_autorizacion
           WHERE
           pk_num_cajachica_autorizacion=:pk_num_cajachica_autorizacion
            ");
        $elimar->execute(array(
            'pk_num_cajachica_autorizacion'=>$idAutorizacionCC
        ));
        $error=$elimar->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idAutorizacionCC;
        }
    }
}
