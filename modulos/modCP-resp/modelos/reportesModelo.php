<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once 'cuentasModelo.php';
#require_once 'transaccionModelo.php';
require_once 'obligacionModelo.php';
require_once 'documentoModelo.php';

require_once 'trait'.DS.'consultasTrait.php';
require_once 'trait'.DS.'updateTrait.php';
require_once RUTA_MODELO . 'miscelaneoModelo.php';


class reportesModelo extends Modelo
{
    use consultasTrait;
    use updateTrait;
    private $atIdUsuario;
    public $atCuentasModelo;
 #   public $atTransaccionModelo;
    public $atDocumentoModelo;
    public $atMiscelaneoModelo;
    public $atObligacionModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atCuentasModelo = new cuentasModelo();
#        $this->atTransaccionModelo = new transaccionModelo();
        $this->atMiscelaneoModelo=new miscelaneoModelo();
        $this->atObligacionModelo = new obligacionModelo();
        $this->atDocumentoModelo = new documentoModelo();
    }

    ## OBLIGACIONES / LISTA DE OBLIGACIONES
    public function metConsultaListaObligaciones($proveedor=false)
    {
        $consulta = $this->_db->query(
            "SELECT 
	          cp_d001_obligacion.*, 
	          CONCAT(proveedor.ind_nombre1, ' ', proveedor.ind_apellido1) AS proveedor,
              cp_b002_tipo_documento.cod_tipo_documento,
	          proveedor.ind_cedula_documento,	          
	          a036_persona_direccion.ind_direccion,
	          a007_persona_telefono.ind_telefono
            FROM  
             cp_d001_obligacion 
            INNER JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = cp_d001_obligacion.fk_cpb002_num_tipo_documento
            INNER JOIN a003_persona AS proveedor ON proveedor.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor 
            INNER JOIN a003_persona AS proveedorPagar on proveedorPagar.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor_a_pagar
            LEFT JOIN a036_persona_direccion ON a036_persona_direccion.fk_a003_num_persona = proveedor.pk_num_persona
            LEFT JOIN a007_persona_telefono ON a007_persona_telefono.fk_a003_num_persona = proveedor.pk_num_persona
            ORDER BY 
	          cp_d001_obligacion.fk_a003_num_persona_proveedor
            ");
        //var_dump($consulta);
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }

    ## OBLIGACIONES / OBLIGACIONES PENDIENTES A PROVEEDORES
    public function metConsultaObligacionesPend($proveedor=false, $desde = false, $hasta = false, $centroCosto = false)
    {
        if($proveedor and $desde and $hasta) {
            $where = "AND cp_d001_obligacion.fk_a003_num_persona_proveedor = '$proveedor' 
            AND cp_d001_obligacion.fec_vencimiento >= '$desde' AND cp_d001_obligacion.fec_vencimiento <= '$hasta'";
        }elseif($proveedor){
            $where = "AND cp_d001_obligacion.fk_a003_num_persona_proveedor = '$proveedor'";
        }
        elseif($desde and $hasta){
            $where = "AND cp_d001_obligacion.fec_vencimiento >= '$desde' AND cp_d001_obligacion.fec_vencimiento <= '$hasta'";
        }else{
            $where = "";
        }
        if($centroCosto) {
            $costo = "AND cp_d001_obligacion.fk_a023_num_centro_de_costo = '$centroCosto'";
        }else{
            $costo = "";
        }
        $consulta = $this->_db->query(
            "SELECT 
	          cp_d001_obligacion.*, 
	          CONCAT(proveedor.ind_nombre1, ' ', proveedor.ind_apellido1) AS proveedor,
              cp_b002_tipo_documento.cod_tipo_documento
            FROM  
             cp_d001_obligacion 
            INNER JOIN a003_persona AS proveedor ON proveedor.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor 
            INNER JOIN a003_persona AS proveedorPagar on proveedorPagar.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor_a_pagar 
            INNER JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = cp_d001_obligacion.fk_cpb002_num_tipo_documento
            WHERE 
              cp_d001_obligacion.ind_estado != 'PA' 
	          AND cp_d001_obligacion.ind_estado != 'AN' 
	          $where
	          $costo
            ORDER BY 
	          cp_d001_obligacion.fk_a003_num_persona_proveedor
            ");
        //var_dump($consulta);
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }

    ## OBLIGACIONES / COMPARACION PAGOS Y OBLIGACIONES
    public function metConsultaPagosOblig($proveedor = false, $año = false, $mes = false)
    {
        $filtro = "";
        if($proveedor){
            $filtro .= "AND cp_d001_obligacion.fk_a003_num_persona_proveedor = '$proveedor' ";
        }
        if($año and  $mes){
            $filtro .= "AND cp_d010_pago.fec_anio = '$año' AND cp_d010_pago.fec_mes = '$mes' ";
        }
        $consulta = $this->_db->query(
            "SELECT 
	          cp_d010_pago.*,
	          cp_d001_obligacion.fk_cpb014_num_cuenta,
	          cp_d001_obligacion.fk_cbb001_num_voucher_mast AS voucherProvision,
	          cp_d001_obligacion.fec_documento,
	          cp_d001_obligacion.num_monto_obligacion,
	          cp_d001_obligacion.num_monto_adelanto,
	          cp_d001_obligacion.num_monto_pago_parcial,
	          cp_d001_obligacion.ind_nro_proceso,
	          cp_b014_cuenta_bancaria.ind_num_cuenta,
	          cp_b014_cuenta_bancaria.ind_descripcion,
	          CONCAT(proveedor.ind_nombre1, ' ', proveedor.ind_apellido1) AS proveedor,
	          CONCAT(proveedorPagar.ind_nombre1, ' ', proveedorPagar.ind_apellido1) AS proveedorPagar,
	          cp_b002_tipo_documento.cod_tipo_documento
	          	          
            FROM  
             cp_d010_pago
             INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
             INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
             INNER JOIN a003_persona AS proveedor ON proveedor.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor
             INNER JOIN a003_persona AS proveedorPagar ON proveedorPagar.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor_a_pagar
             INNER JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d001_obligacion.fk_cpb014_num_cuenta
             INNER JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = cp_d001_obligacion.fk_cpb002_num_tipo_documento
            WHERE 1 $filtro
            ORDER BY 
	          cp_d001_obligacion.fk_a003_num_persona_proveedor
            ");
        //var_dump($consulta);
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }

    public function metConsultaObligPagos($proveedor = false, $año = false, $mes = false)
    {
        $filtro="";
        if($proveedor) {
            $filtro .= "AND cp_d001_obligacion.fk_a003_num_persona_proveedor = '$proveedor' ";
        }
        if($año and  $mes){
            $filtro .= "AND cp_d001_obligacion.fec_anio = '$año' AND cp_d001_obligacion.fec_mes = '$mes' ";
        }
        $consulta = $this->_db->query(
            "SELECT 
	          cp_d001_obligacion.*,
	          cp_b002_tipo_documento.cod_tipo_documento,
	          cp_d010_pago.fec_pago,
	          cp_d010_pago.ind_num_pago,
	          cp_d010_pago.fk_cbb001_num_voucher_pago,
	          cp_b014_cuenta_bancaria.ind_num_cuenta,
	          cp_b014_cuenta_bancaria.ind_descripcion,
	          CONCAT(proveedor.ind_nombre1, ' ', proveedor.ind_apellido1) AS proveedor
	          	          
            FROM  
             cp_d001_obligacion
            INNER JOIN a003_persona AS proveedor ON proveedor.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor
            INNER JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = cp_d001_obligacion.fk_cpb002_num_tipo_documento
            INNER JOIN cp_d009_orden_pago ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            INNER JOIN cp_d010_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
            INNER JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d001_obligacion.fk_cpb014_num_cuenta
            WHERE 1 $filtro                           
            ORDER BY 
	          cp_d001_obligacion.fk_cpb014_num_cuenta, cp_d001_obligacion.fk_a003_num_persona_proveedor
            ");
        //var_dump($consulta);
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }

    ## OBLIGACIONES / REGISTRO DE COMPRAS
    public function metConsultaRetencion($tipoComprobante, $proveedor=false, $desde, $hasta, $diaDesde =  false, $diaHasta = false)
    {
        $filtro="";
        if($tipoComprobante){
            $filtro .= " AND cp_e001_retenciones.ind_tipo_comprobante = '$tipoComprobante'";
            if($tipoComprobante == 'IVA'){
                $filtro .= " AND cp_e001_retenciones.fec_comprobante >= '$diaDesde 00:00:00' and cp_e001_retenciones.fec_comprobante <= '$diaHasta 23:59:59'";
            }
        }
        if($desde and $hasta){
            $filtro .= " AND cp_e001_retenciones.ind_periodo_fiscal >= '$desde' and cp_e001_retenciones.ind_periodo_fiscal <= '$hasta'";
        }
        if($proveedor) {
            $filtro .= " AND cp_d001_obligacion.fk_a003_num_persona_proveedor = '$proveedor'";
        }else{
            $filtro .= "";
        }
        $consulta = $this->_db->query(
            "SELECT
              cp_e001_retenciones.*,
              cp_b015_impuesto.num_factor_porcentaje,
              CONCAT_WS(' ',a003_persona.ind_nombre1, a003_persona.ind_apellido1) AS nombreProveedor,
              cp_d001_obligacion.ind_nro_factura,
              cp_d001_obligacion.num_monto_afecto AS montoAfecto,
              cp_d001_obligacion.num_monto_impuesto AS montoImpuesto
            FROM
              cp_e001_retenciones
            INNER JOIN
              cp_b015_impuesto ON cp_b015_impuesto.pk_num_impuesto = cp_e001_retenciones.fk_b015_num_impuesto
            INNER JOIN
              cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_e001_retenciones.fk_cpd010_num_orden_pago
            INNER JOIN
              cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            INNER JOIN
              a003_persona ON a003_persona.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor
            WHERE 1 $filtro            
            ");
        //var_dump($consulta);
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }

    ## OBLIGACIONES / OBLIGACIONES VS DISTRIBUCION CONTABLE
    public function metConsultaObligacionesContable($proveedor = false, $centroCosto = false,
                                                    $registroDesde = false, $registroHasta = false,
                                                    $pagoDesde = false, $pagoHasta = false, $cuenta = false)
    {   $filtro="";
        if ($proveedor) {
            $filtro .= "AND cp_d001_obligacion.fk_a003_num_persona_proveedor = '$proveedor' ";
        }
        if ($centroCosto) {
            $filtro .= "AND cp_d001_obligacion.fk_a023_num_centro_de_costo = '$centroCosto' ";
        }
        if ($registroDesde and $registroHasta){
            $filtro .= "AND cp_d001_obligacion.fec_registro >= '$registroDesde' 
                      AND cp_d001_obligacion.fec_registro <= '$registroHasta' ";
        }
        if ($pagoDesde and $pagoHasta){
            $filtro .= "AND cp_d001_obligacion.fec_pago >= '$pagoDesde' 
                      AND cp_d001_obligacion.fec_pago <= '$pagoHasta' ";
        }
        if ($cuenta){
            $filtro .= "AND cp_d001_obligacion.fk_cpb014_num_cuenta = '$cuenta' ";
        }
        $consulta = $this->_db->query(
            "SELECT 
	          cp_d001_obligacion.*,
	          cp_b002_tipo_documento.cod_tipo_documento,
	          cp_b014_cuenta_bancaria.ind_num_cuenta,
	          CONCAT(proveedor.ind_nombre1, ' ', proveedor.ind_apellido1) AS proveedor
	          	          
            FROM  
             cp_d001_obligacion
            INNER JOIN a003_persona AS proveedor ON proveedor.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor
            INNER JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = cp_d001_obligacion.fk_cpb002_num_tipo_documento
            INNER JOIN cp_d009_orden_pago ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            INNER JOIN cp_d010_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
            INNER JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d001_obligacion.fk_cpb014_num_cuenta
            WHERE 1 $filtro
            ORDER BY 
	          cp_d001_obligacion.fec_registro
            ");
        //var_dump($consulta);
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }

    ## OBLIGACIONES / DOCUMENTOS EMITIDOS - CHEQUES GIRADOS
    public function metConsultaDocumentosEmitidos($cuenta, $tipoPago = false, $desde = false, $hasta = false)
    {   $filtro="";
        if($cuenta){
            $filtro .= "AND cp_d001_obligacion.fk_cpb014_num_cuenta = '$cuenta' ";
        }
        if($tipoPago){
            $filtro .= "AND cp_d001_obligacion.fk_a006_num_miscelaneo_tipo_pago = '$tipoPago' ";
        }
        if($desde and $hasta){
            $filtro .= "AND cp_d010_pago.fec_pago >= '$desde'
                        AND cp_d010_pago.fec_pago <= '$hasta' ";
        }

        $consulta = $this->_db->query(
            "SELECT 
	          cp_d010_pago.*,
	          cp_d001_obligacion.fk_cpb014_num_cuenta,
	          cp_d001_obligacion.fk_a006_num_miscelaneo_tipo_pago,
	          cp_b014_cuenta_bancaria.ind_descripcion,
	          cp_b014_cuenta_bancaria.ind_num_cuenta,
	          cb_b004_plan_cuenta.cod_cuenta,
	          CONCAT(proveedor.ind_nombre1, ' ', proveedor.ind_apellido1) AS proveedor,
	          (CASE
                        WHEN cp_d010_pago.ind_estado='GE' THEN 'GENERADO'
                        WHEN cp_d010_pago.ind_estado='IM' THEN 'IMPRESO'
                        WHEN cp_d010_pago.ind_estado='AN' THEN 'ANULADO'
              END) AS ind_estado
            FROM  
             cp_d010_pago
            INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
            INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            INNER JOIN a003_persona AS proveedor ON proveedor.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor
            INNER JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d001_obligacion.fk_cpb014_num_cuenta
            LEFT JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_b014_cuenta_bancaria.fk_cbb004_num_plan_cuenta
            WHERE 1 $filtro
            ");
        //var_dump($consulta);
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();

    }

    ## OBLIGACIONES / ESTADO DE CUENTA
    public function metConsultaProveedorEC($proveedor)
    {
        $consulta = $this->_db->query(
            "SELECT 
	          lg_b022_proveedor.*,
	          CONCAT(a003_persona.ind_nombre1, ' ', a003_persona.ind_apellido1) AS proveedor,
	          a003_persona.ind_documento_fiscal,
	          a036_persona_direccion.ind_direccion,
	          a007_persona_telefono.ind_telefono
            FROM  
              lg_b022_proveedor
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = lg_b022_proveedor.fk_a003_num_persona_proveedor
            INNER JOIN a036_persona_direccion ON a036_persona_direccion.fk_a003_num_persona = a003_persona.pk_num_persona
            LEFT JOIN a007_persona_telefono ON a007_persona_telefono.fk_a003_num_persona = a003_persona.pk_num_persona

            WHERE 
              lg_b022_proveedor.pk_num_proveedor = '$proveedor'
            ");
        //var_dump($consulta);
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetch();

    }

    public function metConsultaObligacionesProveedorEC($proveedor, $hasta, $tipoDoc = false, $desde = false)
    {
        $filtro = "";
        if($proveedor){
            $filtro .= "AND cp_d001_obligacion.fk_a003_num_persona_proveedor = '$proveedor' ";
        }
        if($desde){
            $filtro .= "AND cp_d001_obligacion.fec_documento >= '$desde' ";
        }
        if($hasta){
            $filtro .= "AND cp_d001_obligacion.fec_documento <= '$hasta' ";
        }
        if($tipoDoc){
            $filtro .= "AND cp_d001_obligacion.fk_cpb002_num_tipo_documento = '$tipoDoc' ";
        }
        $consulta = $this->_db->query(
            "SELECT 
	          cp_d001_obligacion.*,
	          cp_b002_tipo_documento.cod_tipo_documento
            FROM  
              cp_d001_obligacion
            INNER JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = cp_d001_obligacion.fk_cpb002_num_tipo_documento
            WHERE 1 $filtro
            ORDER BY cp_d001_obligacion.fk_a006_num_miscelaneo_tipo_pago
            ");
        //var_dump($consulta);
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();

    }

    ## BANCOS / LIBRO DE BANCOS
    public function metConsultaSaldoBanco($desde, $idCuenta = false)
    {
        if($idCuenta){
            $where = " AND cp_b014_cuenta_bancaria.pk_num_cuenta = '$idCuenta'";
        }else{
            $where = "";
        }
        $consulta = $this->_db->query(
            "SELECT 
	          cp_b014_cuenta_bancaria.pk_num_cuenta, 
	          cp_b014_cuenta_bancaria.ind_num_cuenta, 
	          a006_miscelaneo_detalle.ind_nombre_detalle,
              (SELECT 
                SUM(cp_d011_banco_transaccion.num_monto)
			  FROM cp_d011_banco_transaccion
			  INNER JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria
			  WHERE
			    cp_d011_banco_transaccion.fec_transaccion < '$desde'
			    $where
              ) as saldoCuenta
              FROM 
                cp_b014_cuenta_bancaria 
              INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b014_cuenta_bancaria.fk_a006_num_miscelaneo_detalle_banco
              INNER JOIN cp_d011_banco_transaccion ON cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria = cp_b014_cuenta_bancaria.pk_num_cuenta
            WHERE  1 
            $where
            GROUP BY cp_b014_cuenta_bancaria.pk_num_cuenta
	        ");
        //var_dump($consulta);
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }

    public function metConsultaSaldoIncialBanco($idCuenta)
    {
        $consulta = $this->_db->query(
            "SELECT 
              cp_d006_cuenta_bancaria_balance.num_saldo_inicial
            FROM 
	          cp_d006_cuenta_bancaria_balance
            WHERE 
            cp_d006_cuenta_bancaria_balance.fk_cpb014_pk_num_cuenta_bancaria = '$idCuenta'
	        ");
        //var_dump($consulta);
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetch();
    }

    public function metConsultaTransaccionesBanco($idCuenta, $desde, $hasta)
    {
        $consulta = $this->_db->query(
            "
            SELECT
              cp_d011_banco_transaccion.*,
              cp_d010_pago.ind_num_pago,
	      cp_d010_pago.ind_cheque_usuario,
              voucherTransaccion.ind_voucher AS ind_voucherTransaccion,
              voucherPago.ind_voucher AS ind_voucherPago,
              CONCAT_WS(' ',a003_persona.ind_nombre1, a003_persona.ind_apellido1) AS persona_proveedor
            FROM
              cp_d011_banco_transaccion
            LEFT JOIN
              cp_d010_pago ON cp_d010_pago.pk_num_pago = cp_d011_banco_transaccion.fk_cpd010_num_pago
            LEFT JOIN
              cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
            LEFT JOIN
              cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            LEFT JOIN cb_b001_voucher AS voucherTransaccion ON voucherTransaccion.pk_num_voucher_mast = cp_d011_banco_transaccion.fk_cbb001_num_voucher
            LEFT JOIN cb_b001_voucher AS voucherPago ON voucherPago.pk_num_voucher_mast = cp_d010_pago.fk_cbb001_num_voucher_pago
            LEFT JOIN a003_persona ON a003_persona.pk_num_persona = cp_d011_banco_transaccion.fk_a003_num_persona_proveedor
            WHERE
              (cp_d011_banco_transaccion.ind_estado = 'AP' OR  cp_d011_banco_transaccion.ind_estado = 'CO') AND cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria = '$idCuenta' AND cp_d011_banco_transaccion.fec_transaccion >= '$desde 00:00:00' 
              AND cp_d011_banco_transaccion.fec_transaccion <= '$hasta 23:59:59'
            ORDER BY
              cp_d011_banco_transaccion.fec_transaccion
	        ");
        //var_dump($consulta);
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }

    public function metConsultaListadoTransacciones($cuenta = false, $desde = false, $hasta = false)
    {
        $filtro="";
        if($cuenta){
            $filtro .= "AND cp_b014_cuenta_bancaria.pk_num_cuenta = '$cuenta' ";
        }
        if($desde and $hasta){
            $filtro .= "AND cp_d011_banco_transaccion.fec_transaccion >= '$desde' AND cp_d011_banco_transaccion.fec_transaccion <= '$hasta'";
        }
        $consulta = $this->_db->query(
            "SELECT 
              cp_d011_banco_transaccion.*,
              cp_b006_banco_tipo_transaccion.cod_tipo_transaccion,
              cp_b006_banco_tipo_transaccion.ind_descripcion,
              cp_b014_cuenta_bancaria.ind_num_cuenta,
              cp_d010_pago.ind_num_pago,
              cp_d010_pago.fk_cbb001_num_voucher_pago
            FROM 
	            cp_d011_banco_transaccion
	        INNER JOIN cp_b006_banco_tipo_transaccion ON cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = cp_d011_banco_transaccion.fk_cpb006_num_banco_tipo_transaccion
	        INNER JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria
	        INNER JOIN cp_d010_pago ON cp_d010_pago.pk_num_pago = cp_d011_banco_transaccion.fk_cpd010_num_pago
	         WHERE 1 $filtro
	        ");
        //var_dump($consulta);
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }


    ## PAGOS / ORDENES DE PAGO
    public function metConsultaOrdenPago($proveedor=false, $documento=false, $cuenta=false, $fdesde=false, $fhasta=false)
    {
        $filtro="";
        if($proveedor){
            $filtro .= "AND cp_d001_obligacion.fk_a003_num_persona_proveedor = '$proveedor' ";
        }
        if($documento){
            $filtro .= "AND cp_d001_obligacion.fk_cpb002_num_tipo_documento = '$documento' ";
        }
        if($cuenta){
            $filtro .= "AND cp_d001_obligacion.fk_cpb014_num_cuenta = '$cuenta' ";
        }
        if($fdesde and $fhasta){
            $filtro .= "AND cp_d009_orden_pago.fec_orden_pago >= '$fdesde' AND cp_d009_orden_pago.fec_orden_pago <= '$fhasta'";
        }

        $consulta = $this->_db->query(
            "SELECT
              cp_d009_orden_pago.*,
              cp_d001_obligacion.ind_nro_pago,
              cp_d010_pago.ind_cheque_usuario,
              cp_d001_obligacion.num_monto_obligacion,
              cp_d013_obligacion_cuentas.num_monto,
              CONCAT_WS(
                ' ',
                a003_persona.ind_nombre1,
                a003_persona.ind_apellido1
              ) AS proveedor,
              pr_b002_partida_presupuestaria.cod_partida,
              pr_b002_partida_presupuestaria.ind_denominacion
            FROM
              cp_d009_orden_pago
            INNER JOIN
              cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            INNER JOIN
              a003_persona ON a003_persona.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor
            LEFT JOIN
              cp_d010_pago ON cp_d010_pago.fk_cpd009_num_orden_pago = cp_d009_orden_pago.pk_num_orden_pago
            LEFT JOIN cp_d013_obligacion_cuentas ON cp_d013_obligacion_cuentas.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
            LEFT JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = cp_d013_obligacion_cuentas.fk_prb002_num_partida_presupuestaria
            WHERE
              cp_d010_pago.ind_estado != 'AN' $filtro
          
	        ");
        //var_dump($consulta);
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }

    public function metConsultaPago()
    {
        $consulta = $this->_db->query(
            "SELECT
              cp_d010_pago.*,
              cp_d001_obligacion.fk_a003_num_persona_proveedor,
              CONCAT(a003_persona.ind_nombre1, ' ', a003_persona.ind_apellido1) AS proveedor,
              a003_persona.ind_documento_fiscal,
              a006_miscelaneo_detalle.ind_nombre_detalle AS tipoPago,
              cp_d001_obligacion.fec_documento,
              cp_d001_obligacion.ind_comentarios,
              a006_miscelaneo_detalle.ind_nombre_detalle AS CodTipoDocumento

            FROM
	            cp_d010_pago
	        INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
	        INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
	        INNER JOIN a003_persona ON a003_persona.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor
	        INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_d001_obligacion.fk_a006_num_miscelaneo_tipo_pago
	        INNER JOIN a006_miscelaneo_detalle AS tipoDocumento ON tipoDocumento.pk_num_miscelaneo_detalle = cp_d001_obligacion.fk_cpb002_num_tipo_documento

	        WHERE
	          cp_d010_pago.ind_estado!='AN'

	        ");
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }

    public function metConsultaPagoPendiente()
    {
        $consulta = $this->_db->query(
            "SELECT
              cp_d009_orden_pago.*,
              CONCAT(a003_persona.ind_nombre1, ' ', a003_persona.ind_apellido1) AS proveedor,
              a003_persona.ind_documento_fiscal AS docFiscal,
              cp_d001_obligacion.fk_a003_num_persona_proveedor,
              (SELECT
                SUM(cp_d001_obligacion.num_monto_obligacion)
                FROM
                cp_d001_obligacion
                WHERE
                cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion

              ) AS montoProveedor

            FROM
	            cp_d009_orden_pago
	        INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
	        INNER JOIN a003_persona ON a003_persona.pk_num_persona = cp_d001_obligacion.fk_a003_num_persona_proveedor
            WHERE
                cp_d009_orden_pago.ind_estado = 'PE'
	        ");
        //var_dump($consulta);
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetchAll();
    }

    public function metTransaccionListar($idTransaccion = false)
    {
        if($idTransaccion){
            $where = "WHERE cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = '$idTransaccion'";
        }else{
            $where = "";
        }
        $transaccionListar =  $this->_db->query(
            "SELECT
              cp_b006_banco_tipo_transaccion.*,
              a006_miscelaneo_detalle.ind_nombre_detalle,
              cb_b004_plan_cuenta.cod_cuenta
            FROM
            cp_b006_banco_tipo_transaccion
             INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b006_banco_tipo_transaccion.fk_a006_miscelaneo_detalle_tipo_transaccion
             LEFT JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_b006_banco_tipo_transaccion.fk_cbb004_num_plan_cuenta
              $where
             ORDER BY cp_b006_banco_tipo_transaccion.fk_a006_miscelaneo_detalle_tipo_transaccion ASC"
        );
        //var_dump($transaccionListar);
        $transaccionListar->setFetchMode(PDO::FETCH_ASSOC);
        if($idTransaccion){
            return $transaccionListar->fetch();
        }else{
            return $transaccionListar->fetchAll();
        }

    }
    
}
