<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class clasificacionDocumentosModelo extends Modelo

{
    private $atIdUsuario;
    public $atIdEmpleado;


    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
    }

    public function metClasificacionDocumentosListar()
    {
        $Listar = $this->_db->query(
            "SELECT
                *
             FROM
               cp_b001_documento_clasificacion

        ");
        $Listar->setFetchMode(PDO::FETCH_ASSOC);
        return $Listar->fetchAll();

    }

    public function metConsultaClasificacionDocumentos($idClasifDocumentos)
    {
        $consultar = $this->_db->query("
            SELECT
               cp_b001_documento_clasificacion.*,
               a018_seguridad_usuario.ind_usuario

             FROM
               cp_b001_documento_clasificacion
            INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = cp_b001_documento_clasificacion.fk_a018_num_seguridad_usuario
            WHERE
            pk_num_documento_clasificacion='$idClasifDocumentos'
             ");
        $consultar->setFetchMode(PDO::FETCH_ASSOC);
        return $consultar->fetch();
    }

    public function metNuevaClasifDocumentos($validacion)
    {
        $this->_db->beginTransaction();
        $registrar = $this->_db->prepare("
            INSERT INTO
              cp_b001_documento_clasificacion
            SET
              ind_documento_clasificacion=:ind_documento_clasificacion,
              ind_descripcion=:ind_descripcion,
              num_flag_item=:num_flag_item,
              num_flag_cliente=:num_flag_cliente,
              num_flag_compromiso=:num_flag_compromiso,
              num_flag_transaccion=:num_flag_transaccion,
              num_estatus=:num_estatus,
              fec_ultima_modificacion=NOW(),
             fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             ");
        $registrar->execute(array(
            'ind_documento_clasificacion' => $validacion['ind_documento_clasificacion'],
            'ind_descripcion' => $validacion['ind_descripcion'],
            'num_flag_item' => $validacion['num_flag_item'],
            'num_flag_cliente' => $validacion['num_flag_cliente'],
            'num_flag_compromiso' => $validacion['num_flag_compromiso'],
            'num_flag_transaccion' => $validacion['num_flag_transaccion'],
            'num_estatus' => $validacion['num_estatus']
        ));

        $fallaTansaccion = $registrar->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarClasifDocumentos($idClasifDocumentos,$validacion)
    {
        $this->_db->beginTransaction();
        $registrar = $this->_db->prepare("
            UPDATE
              cp_b001_documento_clasificacion
            SET
              ind_descripcion=:ind_descripcion,
              num_flag_item=:num_flag_item,
              num_flag_cliente=:num_flag_cliente,
              num_flag_compromiso=:num_flag_compromiso,
              num_flag_transaccion=:num_flag_transaccion,
              num_estatus=:num_estatus,
              fec_ultima_modificacion=NOW(),
             fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            WHERE
              pk_num_documento_clasificacion='$idClasifDocumentos'
             ");
        $registrar->execute(array(
            'ind_descripcion' => $validacion['ind_descripcion'],
            'num_flag_item' => $validacion['num_flag_item'],
            'num_flag_cliente' => $validacion['num_flag_cliente'],
            'num_flag_compromiso' => $validacion['num_flag_compromiso'],
            'num_flag_transaccion' => $validacion['num_flag_transaccion'],
            'num_estatus' => $validacion['num_estatus']
        ));

        $fallaTansaccion = $registrar->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metEliminarClasificacionDocumentos($idClasifDocumentos)
    {
        $this->_db->beginTransaction();
        $elimar=$this->_db->prepare("
                DELETE FROM
                  cp_b001_documento_clasificacion
                WHERE
                  pk_num_documento_clasificacion=:pk_num_documento_clasificacion
            ");
        $elimar->execute(array(
            'pk_num_documento_clasificacion'=>$idClasifDocumentos
        ));
        $error=$elimar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idClasifDocumentos;
        }


    }

}