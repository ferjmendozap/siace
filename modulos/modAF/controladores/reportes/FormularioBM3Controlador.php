<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Activos
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |26-07-2016               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'Fecha.php';
require_once ROOT.'librerias'.DS.'Number.php';

class FormularioBM3Controlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->FormularioBM3 = $this->metCargarModelo('FormularioBM3','reportes');
    }

    /**
     * Método para cargar vista Listado de Registros.
     *
     * @return Response
     */
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-ui-selectable/jquery-ui',
            'bootstrap-datepicker/datepicker',
        );
        $complementoJs = array(
            'mask/jquery.mask',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-ui-selectable/jquery-ui',
            'bootstrap-datepicker/bootstrap-datepicker',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        $filtro = [
            'fperiodo' => date('Y-m'),
        ];
        $this->atVista->assign('filtro', $filtro);
        $this->atVista->metRenderizar('filtro');
    }

    /**
     * Método para cargar vista Generar Acta.
     *
     * @return Response
     */
    public function metFrame()
    {
        ##  vista
        $this->atVista->assign('filtro', serialize($_POST));
        $this->atVista->metRenderizar('frame', 'modales');
    }

    /**
     * Método para imprimir PDF.
     *
     * @return Response
     */
    public function metPdf()
    {
        $filtro = unserialize($_GET["filtro"]);
        $db = new db();
        ##  ----------------------------------
        $this->metObtenerLibreria('plantillaPDF','modAF');
        $parametros = Select::parametros();
        ##  datos de la dependencia default
        $sql = "SELECT
                    d.ind_dependencia,
                    o.ind_descripcion_empresa,
                    CONCAT_WS(' ', p.ind_nombre1, p.ind_nombre2, p.ind_apellido1, p.ind_apellido2) AS persona
                FROM
                    a004_dependencia d
                    INNER JOIN a001_organismo o ON (o.pk_num_organismo = d.fk_a001_num_organismo)
                    INNER JOIN a003_persona p ON (p.pk_num_persona = d.fk_a003_num_persona_responsable)
                WHERE d.pk_num_dependencia = '$parametros[DEPENDAFDEF]'";
        $query_dependencia = $db->query($sql);
        $query_dependencia->setFetchMode(PDO::FETCH_ASSOC);
        $dependencia = $query_dependencia->fetch();
        ##  Obtengo la data a imprimir
        $field = $this->FormularioBM3->metListar($filtro);
        ##  ----------------------------------
        ##  Creo el Reporte
        $pdf = new pdfDefaultL2('L','mm','Letter');
        $pdf->Organismo = 'DIRECCION DE ADMINISTRACIÓN';
        $pdf->Dependencia = 'AREA DE BIENES E INVENTARIO';
        $pdf->heightLine = 5;
        $pdf->Head = "";
        if (count($field))
        {
            $pdf->Head .= "
                \$this->SetFont('Arial','B',8);
                \$this->SetXY(220,12); \$this->Cell(25,5,utf8_decode('FORMULARIO BM-3'),0,1,'L');
                \$this->SetXY(220,17); \$this->Cell(25,5,utf8_decode('Hoja N° ' . \$this->PageNo() . ' / {nb}'),0,1,'L');
                \$this->SetY(35);
                \$this->SetFont('Arial','B',10);
                \$this->Cell(260,5,utf8_decode('RELACIÓN DE BIENES MUEBLES FALTANTES'),0,1,'C');
                \$this->SetY(40);
                
                \$this->SetFont('Arial','B',9); \$this->Cell(20,5,utf8_decode('ESTADO: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(175,5,utf8_decode(\$this->Data['ind_estado']),0,0,'L');
                \$this->SetFont('Arial','B',9); \$this->Cell(30,5,utf8_decode('CONCEPTO: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(50,5,utf8_decode(\$this->Data['movimiento_codigo']),0,1,'L');
                \$this->SetFont('Arial','B',9); \$this->Cell(20,5,utf8_decode('MUNICIPIO: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(175,5,utf8_decode(\$this->Data['ind_municipio']),0,0,'L');
                \$this->SetFont('Arial','B',9); \$this->Cell(30,5,utf8_decode('COMPROBANTE: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(50,5,utf8_decode(\$this->Data['ind_codigo']),0,1,'L');
                \$this->SetFont('Arial','B',9); \$this->Cell(20,5,utf8_decode('CIUDAD: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(175,5,utf8_decode(\$this->Data['ind_ciudad']),0,0,'L');
                \$this->SetFont('Arial','B',9); \$this->Cell(30,5,utf8_decode('FECHA: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(50,5,utf8_decode(Fecha::formatFecha(\$this->Data['fec_fecha'],'Y-m-d','d-m-Y')),0,1,'L');
                \$this->SetFont('Arial','B',9); \$this->Cell(60,5,utf8_decode('DEPENDENCIA O UNIDAD PRIMARIA: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(90,5,utf8_decode(\$this->Data['ind_dependencia']),0,1,'L');
                \$this->SetFont('Arial','B',9); \$this->Cell(65,5,utf8_decode('UNIDAD DE TRABAJO O DEPENDENCIA: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(90,5,utf8_decode(\$this->Data['ind_dependencia']),0,1,'L');
                \$this->Ln(6);
            ";
        }
        $pdf->Head .= "
            \$this->SetFillColor(200,200,200);
            \$this->SetDrawColor(0,0,0);
            \$this->SetFont('Arial','B',8);
            \$this->SetXY(10,65); \$this->Cell(30,6,utf8_decode('Clasificación(Código)'),1,0,'C');
            \$this->SetXY(40,65); \$this->VCell(15,35,utf8_decode('N° Identificación'),1,0,'L');
            \$this->SetXY(55,65); \$this->MultiCell(125,35,'',1,'C');
            \$this->SetXY(55,80); \$this->MultiCell(125,4,utf8_decode('NOMBRE Y DESCRIPCIÓN DE LOS BIENES'),0,'C');
            \$this->SetXY(180,65); \$this->Cell(25,35,'',1,0,'C');
            \$this->SetXY(180,80); \$this->Cell(25,6,utf8_decode('Cantidad'),0,0,'C');
            \$this->SetXY(205,65); \$this->Cell(30,35,'',1,0,'C');
            \$this->SetXY(205,80); \$this->Cell(30,6,utf8_decode('Valor Unitario'),0,0,'C');
            \$this->SetXY(235,65); \$this->Cell(30,35,'',1,0,'C');
            \$this->SetXY(235,80); \$this->Cell(30,6,utf8_decode('Valor Unitario'),0,0,'C');
            \$this->SetXY(10,71); \$this->VCell(10,29,utf8_decode('Grupo'),1,0,'L');
            \$this->SetXY(20,71); \$this->VCell(10,29,utf8_decode('Sub-Grupo'),1,0,'L');
            \$this->SetXY(30,71); \$this->VCell(10,29,utf8_decode('Sección'),1,0,'L');
            \$this->SetY(100);
        ";
        ##
        $pdf->AliasNbPages();

        if (count($field)) $pdf->Data = $field[0];
        ##  
        $Grupo = '';
        foreach ($field as $f) 
        {
            $pdf->Data = $f;
            if ($Grupo != $f['fk_afc030_num_activo_faltante'])
            {
                if ($Grupo != '')
                {
                    $pdf->SetFont('Arial','B',8);
                    $pdf->Cell(100,10,utf8_decode('Total de Bienes: ' . $CantidadBienes),0,0,'L');
                    ## 
                    if ($pdf->GetY() > 160) $pdf->AddPage();
                    $pdf->SetFont('Arial','B',8);
                    $pdf->SetXY(160,180); $pdf->Cell(100,6,utf8_decode('FALTANTES DETERMINADOS POR: ___________________________________________'),0,1,'C');
                    $pdf->SetXY(195,$pdf->GetY()); $pdf->MultiCell(75,4,utf8_decode($f['persona_dependencia']),0,'C');
                    $pdf->SetXY(195,$pdf->GetY()); $pdf->MultiCell(75,4,utf8_decode($f['persona_dependencia_cargo']),0,'C');
                }
                $CantidadBienes = 0;
                $Grupo = $f['fk_afc030_num_activo_faltante'];
                $pdf->AddPage();
            }
            ++$CantidadBienes;
            $digitos_clasificacion = strlen($f['clasificacion_codigo']);
            $pdf->SetFillColor(255,255,255);
            $pdf->SetWidths(array(10,10,10,15,125,25,30,30));
            $pdf->SetAligns(array('C','C','C','C','L','R','R','R'));
            $pdf->SetFont('Arial','',8);
            $pdf->Row(array(substr($f['clasificacion_codigo'],0,2),
                            substr($f['clasificacion_codigo'],2,2),
                            ($digitos_clasificacion > 4 ? substr($f['clasificacion_codigo'],4,3) : ''),
                            utf8_decode($f['cod_codigo_interno']),
                            utf8_decode($f['ind_descripcion']),
                            '1',
                            number_format($f['num_monto'],2,',','.'),
                            number_format($f['num_monto'],2,',','.')
                    ));
        }
        if (count($field)) 
        {
            $pdf->SetFont('Arial','B',8);
            $pdf->Cell(100,10,utf8_decode('Total de Bienes: ' . $CantidadBienes),0,0,'L');
            ##  
            if ($pdf->GetY() > 160) $pdf->AddPage();
            $pdf->SetFont('Arial','B',8);
            $pdf->SetXY(160,180); $pdf->Cell(100,6,utf8_decode('FALTANTES DETERMINADOS POR: ___________________________________________'),0,1,'C');
            $pdf->SetXY(195,$pdf->GetY()); $pdf->MultiCell(75,4,utf8_decode($f['persona_dependencia']),0,'C');
            $pdf->SetXY(195,$pdf->GetY()); $pdf->MultiCell(75,4,utf8_decode($f['persona_dependencia_cargo']),0,'C');   
        }

        ##
        /*$Grupo = '';
        $CantidadBienes = 0;
        $TotalIncorporaciones = 0;
        $TotalDesincorporaciones = 0;
        foreach ($field as $f) {
            $pdf->Data = $f;
            if ($Grupo != $f['fk_a023_num_centro_costo'])
            {
                if ($Grupo != '')
                {
                    $pdf->SetFont('Arial','B',8);
                    $pdf->Cell(100,10,utf8_decode('Total de Bienes: ' . $CantidadBienes),0,0,'L');
                    $pdf->Cell(90,10,utf8_decode('Total: '),0,0,'R');
                    $pdf->Cell(35,10,number_format($TotalIncorporaciones,2,',','.'),0,0,'R');
                    $pdf->Cell(35,10,number_format($TotalDesincorporaciones,2,',','.'),0,0,'R');
                    ## 
                    if ($pdf->GetY() > 160) $pdf->AddPage();
                    $pdf->SetFont('Arial','B',8);
                    $pdf->SetXY(170,180); $pdf->Cell(100,6,utf8_decode('RECIBI CONFORME: ___________________________________________'),0,1,'C');
                    $pdf->SetXY(195,$pdf->GetY()); $pdf->MultiCell(75,4,utf8_decode($f['persona_dependencia']),0,'C');
                    $pdf->SetXY(195,$pdf->GetY()); $pdf->MultiCell(75,4,utf8_decode($f['persona_dependencia_cargo']),0,'C');
                }
                $Grupo = $f['fk_a023_num_centro_costo'];
                $pdf->AddPage();
                ##  
                $CantidadBienes = 0;
                $TotalIncorporaciones = 0;
                $TotalDesincorporaciones = 0;
            }
            if ($f['ind_tipo'] == 'I') 
            {
                $Incorporaciones = $f['num_monto'];
                $Desincorporaciones = 0;
                $TotalIncorporaciones += $Incorporaciones;
            }
            else
            {
                $Incorporaciones = 0;
                $Desincorporaciones = $f['num_monto'];
                $TotalDesincorporaciones += $Desincorporaciones;
            }
            $digitos_clasificacion = strlen($f['clasificacion_codigo']);
            $pdf->SetFillColor(255,255,255);
            $pdf->SetWidths(array(10,10,10,10,10,15,125,35,35));
            $pdf->SetAligns(array('C','C','C','C','C','C','L','R','R'));
            $pdf->SetFont('Arial','',8);
            $pdf->Row(array(substr($f['clasificacion_codigo'],0,2),
                            substr($f['clasificacion_codigo'],2,2),
                            ($digitos_clasificacion > 4 ? substr($f['clasificacion_codigo'],4,3) : ''),
                            utf8_decode($f['movimiento_codigo']),
                            '1',
                            utf8_decode($f['cod_codigo_interno']),
                            utf8_decode($f['ind_descripcion']),
                            number_format($Incorporaciones,2,',','.'),
                            number_format($Desincorporaciones,2,',','.')
                    ));
            ++$CantidadBienes;
        }
        if (count($field)) 
        {
            $pdf->SetFont('Arial','B',8);
            $pdf->Cell(100,10,utf8_decode('Total de Bienes: ' . $CantidadBienes),0,0,'L');
            $pdf->Cell(90,10,utf8_decode('Total: '),0,0,'R');
            $pdf->Cell(35,10,number_format($TotalIncorporaciones,2,',','.'),0,0,'R');
            $pdf->Cell(35,10,number_format($TotalDesincorporaciones,2,',','.'),0,0,'R');
            ##  
            if ($pdf->GetY() > 160) $pdf->AddPage();
            $pdf->SetFont('Arial','B',8);
            $pdf->SetXY(170,180); $pdf->Cell(100,6,utf8_decode('RECIBI CONFORME: ___________________________________________'),0,1,'C');
            $pdf->SetXY(195,$pdf->GetY()); $pdf->MultiCell(75,4,utf8_decode($f['persona_dependencia']),0,'C');
            $pdf->SetXY(195,$pdf->GetY()); $pdf->MultiCell(75,4,utf8_decode($f['persona_dependencia_cargo']),0,'C');   
        }*/
        ##
        $pdf->Output();
    }
}
