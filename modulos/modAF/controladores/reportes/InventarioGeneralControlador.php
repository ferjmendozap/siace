<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Activos
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |01-12-2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'Fecha.php';
require_once ROOT.'librerias'.DS.'Number.php';

class InventarioGeneralControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->InventarioGeneral = $this->metCargarModelo('InventarioGeneral','reportes');
    }

    /**
     * Método para cargar vista Listado de Registros.
     *
     * @return Response
     */
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-ui-selectable/jquery-ui',
        );
        $complementoJs = array(
            'mask/jquery.mask',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-ui-selectable/jquery-ui',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('filtro');
    }

    /**
     * Método para cargar vista Generar Acta.
     *
     * @return Response
     */
    public function metFrame()
    {
        ##  vista
        $this->atVista->assign('filtro', serialize($_POST));
        $this->atVista->metRenderizar('frame', 'modales');
    }

    /**
     * Método para imprimir PDF Transferencia de Activos.
     *
     * @return Response
     */
    public function metPdf()
    {
        $filtro = unserialize($_GET["filtro"]);
        $db = new db();
        ##  ----------------------------------
        $this->metObtenerLibreria('plantillaPDF','modAF');
        $parametros = Select::parametros();
        ##  datos de la dependencia default
        $sql = "SELECT
                    d.ind_dependencia,
                    o.ind_descripcion_empresa,
                    CONCAT_WS(' ', p.ind_nombre1, p.ind_nombre2, p.ind_apellido1, p.ind_apellido2) AS persona
                FROM
                    a004_dependencia d
                    INNER JOIN a001_organismo o ON (o.pk_num_organismo = d.fk_a001_num_organismo)
                    INNER JOIN a003_persona p ON (p.pk_num_persona = d.fk_a003_num_persona_responsable)
                WHERE d.pk_num_dependencia = '$parametros[DEPENDAFDEF]'";
        $query_dependencia = $db->query($sql);
        $query_dependencia->setFetchMode(PDO::FETCH_ASSOC);
        $dependencia = $query_dependencia->fetch();
        ##  Obtengo la data a imprimir
        $field = $this->InventarioGeneral->metListar($filtro);
        ##  ----------------------------------
        ##  Creo el Reporte
        $pdf = new pdfDefaultL('L','mm','Letter');
        $pdf->Organismo = 'DIRECCION DE ADMINISTRACIÓN';
        $pdf->Dependencia = 'AREA DE BIENES E INVENTARIO';
        $pdf->Titulo = 'INVENTARIO GENERAL';
        $pdf->heightLine = 5;
        $pdf->Head = "";
        if (count($field))
        {
            if ($filtro['ffk_a001_num_organismo']) {
                $pdf->Head .= "
                    \$this->SetFont('Arial','B',9); \$this->Cell(40,5,utf8_decode('Organismo: '),0,0,'L');
                    \$this->SetFont('Arial','',9); \$this->Cell(40,5,utf8_decode(\$this->Data['ind_descripcion_empresa']),0,0,'L');
                    \$this->Ln(5);
                ";
            }
            if ($filtro['ffk_a004_num_dependencia']) {
                $pdf->Head .= "
                    \$this->SetFont('Arial','B',9); \$this->Cell(40,5,utf8_decode('Dependencia: '),0,0,'L');
                    \$this->SetFont('Arial','',9); \$this->Cell(40,5,utf8_decode(\$this->Data['ind_dependencia']),0,0,'L');
                    \$this->Ln(5);
                ";
            }
            if ($filtro['ffk_a023_num_centro_costo']) {
                $pdf->Head .= "
                    \$this->SetFont('Arial','B',9); \$this->Cell(40,5,utf8_decode('Centro de Costo: '),0,0,'L');
                    \$this->SetFont('Arial','',9); \$this->Cell(40,5,utf8_decode(\$this->Data['ind_descripcion_centro_costo']),0,0,'L');
                    \$this->Ln(5);
                ";
            }
            if ($filtro['ffk_afc019_num_categoria']) {
                $pdf->Head .= "
                    \$this->SetFont('Arial','B',9); \$this->Cell(40,5,utf8_decode('Categoría: '),0,0,'L');
                    \$this->SetFont('Arial','',9); \$this->Cell(40,5,utf8_decode(\$this->Data['ind_nombre_categoria']),0,0,'L');
                    \$this->Ln(5);
                ";
            }
            if ($filtro['ffk_afc008_num_clasificacion']) {
                $pdf->Head .= "
                    \$this->SetFont('Arial','B',9); \$this->Cell(40,5,utf8_decode('Clasificación: '),0,0,'L');
                    \$this->SetFont('Arial','',9); \$this->Cell(40,5,utf8_decode(\$this->Data['ind_nombre_clasificacion']),0,0,'L');
                    \$this->Ln(5);
                ";
            }
            if ($filtro['ffk_afc009_num_movimiento']) {
                $pdf->Head .= "
                    \$this->SetFont('Arial','B',9); \$this->Cell(40,5,utf8_decode('Tipo de Movimiento: '),0,0,'L');
                    \$this->SetFont('Arial','',9); \$this->Cell(40,5,utf8_decode(\$this->Data['ind_nombre_movimiento']),0,0,'L');
                    \$this->Ln(5);
                ";
            }
            if ($filtro['ffk_afc007_num_situacion']) {
                $pdf->Head .= "
                    \$this->SetFont('Arial','B',9); \$this->Cell(40,5,utf8_decode('Situación: '),0,0,'L');
                    \$this->SetFont('Arial','',9); \$this->Cell(40,5,utf8_decode(\$this->Data['ind_nombre_situacion']),0,0,'L');
                    \$this->Ln(5);
                ";
            }
            if ($filtro['find_estado']) {
                $pdf->Head .= "
                    \$this->SetFont('Arial','B',9); \$this->Cell(40,5,utf8_decode('Estado: '),0,0,'L');
                    \$this->SetFont('Arial','',9); \$this->Cell(40,5,utf8_decode(Select::options('af_estado_activo',\$this->Data['ind_estado'],2)),0,0,'L');
                    \$this->Ln(5);
                ";
            }
            if ($filtro['ffk_a003_num_persona_usuario']) {
                $pdf->Head .= "
                    \$this->SetFont('Arial','B',9); \$this->Cell(40,5,utf8_decode('Usuario: '),0,0,'L');
                    \$this->SetFont('Arial','',9); \$this->Cell(40,5,utf8_decode(\$this->Data['persona_usuario']),0,0,'L');
                    \$this->Ln(5);
                ";
            }
            if ($filtro['ffk_a003_num_persona_responsable']) {
                $pdf->Head .= "
                    \$this->SetFont('Arial','B',9); \$this->Cell(40,5,utf8_decode('Responsable: '),0,0,'L');
                    \$this->SetFont('Arial','',9); \$this->Cell(40,5,utf8_decode(\$this->Data['persona_responsable']),0,0,'L');
                    \$this->Ln(5);
                ";
            }
            if ($filtro['ffk_afb001_num_activo']) {
                $pdf->Head .= "
                    \$this->SetFont('Arial','B',9); \$this->Cell(40,5,utf8_decode('Activo Principal: '),0,0,'L');
                    \$this->SetFont('Arial','',9); \$this->MultiCell(200,5,utf8_decode(\$this->Data['activo_principal']),0,'L');
                ";
            }

        }
        $pdf->Head .= "
            \$this->Ln(3);
            \$this->SetFillColor(200,200,200);
            \$this->SetDrawColor(0,0,0);
            \$this->SetFont('Arial','B',8);
            \$this->SetWidths(array(20,100,90,50));
            \$this->SetAligns(array('C','L','L','L'));
            \$this->Row(array(utf8_decode('CÓDIGO'),
                              utf8_decode('DESCRIPCIÓN'),
                              utf8_decode('CLASIFICACIÓN'),
                              utf8_decode('SERIAL')
                    ));
        ";
        ##
        if (count($field)) $pdf->Data = $field[0];
        $pdf->AliasNbPages();
        $pdf->AddPage();
        ##
        $Grupo = '';
        foreach ($field as $f) {
            $pdf->Data = $f;
            if ($filtro['fgroup'])
            {
                if ($filtro['fgroup'] == 'fk_a001_num_organismo')
                {
                    $Agrupador = $f['fk_a001_num_organismo'];
                    $DetalleGrupo = 'Organismo: ' . $f['ind_descripcion_empresa'];
                }
                elseif ($filtro['fgroup'] == 'fk_a004_num_dependencia')
                {
                    $Agrupador = $f['fk_a004_num_dependencia'];
                    $DetalleGrupo = 'Dependencia: ' . $f['ind_dependencia'];
                }
                elseif ($filtro['fgroup'] == 'fk_a023_num_centro_costo')
                {
                    $Agrupador = $f['fk_a023_num_centro_costo'];
                    $DetalleGrupo = 'Centro de Costo: ' . $f['ind_descripcion_centro_costo'];
                }

                if ($Grupo != $Agrupador)
                {
                    $Grupo = $Agrupador;
                    $pdf->SetFont('Arial','B',8);
                    $pdf->Cell(260,10,utf8_decode($DetalleGrupo),1,1,'L');
                }
            }
            $pdf->SetFillColor(255,255,255);
            $pdf->SetDrawColor(0,0,0);
            $pdf->SetWidths(array(20,100,90,50));
            $pdf->SetAligns(array('C','L','L','L'));
            $pdf->SetFont('Arial','',8);
            $pdf->Row(array(utf8_decode($f['cod_codigo_interno']),
                            utf8_decode($f['ind_descripcion']),
                            utf8_decode($f['ind_nombre_clasificacion']),
                            utf8_decode($f['ind_serie'])
                    ));
        }
        ##
        $pdf->Output();
    }
}
