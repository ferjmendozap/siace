<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Activos
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |26-07-2016               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'Fecha.php';
require_once ROOT.'librerias'.DS.'Number.php';

class FormularioBM4Controlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->FormularioBM4 = $this->metCargarModelo('FormularioBM4','reportes');
    }

    /**
     * Método para cargar vista Listado de Registros.
     *
     * @return Response
     */
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-ui-selectable/jquery-ui',
            'bootstrap-datepicker/datepicker',
        );
        $complementoJs = array(
            'mask/jquery.mask',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-ui-selectable/jquery-ui',
            'bootstrap-datepicker/bootstrap-datepicker',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        $filtro = [
            'fperiodo' => date('Y-m'),
        ];
        $this->atVista->assign('filtro', $filtro);
        $this->atVista->metRenderizar('filtro');
    }

    /**
     * Método para cargar vista Generar Acta.
     *
     * @return Response
     */
    public function metFrame()
    {
        ##  vista
        $this->atVista->assign('filtro', serialize($_POST));
        $this->atVista->metRenderizar('frame', 'modales');
    }

    /**
     * Método para imprimir PDF Transferencia de Activos.
     *
     * @return Response
     */
    public function metPdf()
    {
        $filtro = unserialize($_GET["filtro"]);
        $db = new db();
        list($PeriodoAnio, $PeriodoMes) = explode('-', $filtro['fperiodo']);
        $NombreMes = mb_strtoupper(Fecha::mes_nombre($PeriodoMes));
        ##  ----------------------------------
        $this->metObtenerLibreria('plantillaPDF','modAF');
        $parametros = Select::parametros();
        ##  datos de la dependencia default
        $sql = "SELECT
                    d.ind_dependencia,
                    o.ind_descripcion_empresa,
                    CONCAT_WS(' ', p.ind_nombre1, p.ind_nombre2, p.ind_apellido1, p.ind_apellido2) AS persona,
                    rhc63.ind_descripcion_cargo AS persona_cargo
                FROM
                    a004_dependencia d
                    INNER JOIN a001_organismo o ON (o.pk_num_organismo = d.fk_a001_num_organismo)
                    INNER JOIN a003_persona p ON (p.pk_num_persona = d.fk_a003_num_persona_responsable)
                    LEFT JOIN rh_b001_empleado rhb1 ON (rhb1.fk_a003_num_persona = p.pk_num_persona)
                    LEFT JOIN rh_c005_empleado_laboral rhc5 ON (rhc5.fk_rhb001_num_empleado = rhb1.pk_num_empleado)
                    LEFT JOIN rh_c063_puestos rhc63 ON (rhc63.pk_num_puestos = rhc5.fk_rhc063_num_puestos_cargo)
                WHERE d.pk_num_dependencia = '$parametros[DEPENDAFDEF]'";
        $query_dependencia = $db->query($sql);
        $query_dependencia->setFetchMode(PDO::FETCH_ASSOC);
        $dependencia = $query_dependencia->fetch();
        ##  Obtengo la data a imprimir
        $field = $this->FormularioBM4->metListar($filtro);
        ##  ----------------------------------
        ##  Creo el Reporte
        $pdf = new pdfDefaultL2('L','mm','Letter');
        $pdf->Organismo = 'DIRECCION DE ADMINISTRACIÓN';
        $pdf->Dependencia = 'AREA DE BIENES E INVENTARIO';
        $pdf->heightLine = 5;
        $pdf->Head = "";
        /*if (count($field))
        {*/
            $pdf->Head .= "
                \$this->SetFont('Arial','B',8);
                \$this->SetXY(220,12); \$this->Cell(25,5,utf8_decode('FORMULARIO BM-4'),0,1,'L');
                \$this->SetXY(220,17); \$this->Cell(25,5,utf8_decode('Hoja N° ' . \$this->PageNo() . ' / {nb}'),0,1,'L');
                \$this->SetY(35);
                \$this->SetFont('Arial','B',10); \$this->Cell(260,10,utf8_decode('RESUMEN DE LA CUENTA DE BIENES MUEBLES'),0,1,'C');
                \$this->SetFont('Arial','B',9); \$this->Cell(42,5,utf8_decode('PERIODO DE LA CUENTA: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(50,5,utf8_decode('$NombreMes $PeriodoAnio'),0,1,'L');
                \$this->Ln(6);
            ";
        //}
        if (count($field)) $pdf->Data = $field[0];
        $pdf->AliasNbPages();
        $pdf->AddPage();
        ##  
        $pdf->SetY(55);
        $pdf->SetDrawColor(255,255,255);
        $pdf->SetFillColor(255,255,255);
        $pdf->SetWidths(array(150,50));
        $pdf->SetAligns(array('L','R'));
        $pdf->SetFont('Arial','',10);
        $pdf->Row(array('Existencia anterior:',
                        '0,00'
                ));
        $pdf->Ln(3);
        $pdf->Row(array(utf8_decode('Incorporaciones en el mes de la Cuenta:'),
                        '0,00'
                ));
        $pdf->Ln(3);
        $pdf->Row(array(utf8_decode('Desincorporaciones en el mes de la cuenta por todos los conceptos, con excepción del 60, "Faltantes de Bienes por Investigar":'),
                        '0,00'
                ));
        $pdf->Ln(3);
        $pdf->Row(array(utf8_decode('Desincorporaciones en el mes de la cuenta por el concepto 60, "Faltantes de Bienes por Investigar":'),
                        '0,00'
                ));
        $pdf->Ln(3);
        $pdf->Row(array(utf8_decode('Existencia final:'),
                        '0,00'
                ));
        $pdf->Ln(3);
        ##  
        if ($pdf->GetY() > 160) $pdf->AddPage();
        $pdf->SetFont('Arial','B',8);
        $pdf->SetXY(170,180); $pdf->Cell(100,6,utf8_decode('RESPONSABLE: ___________________________________________'),0,1,'C');
        $pdf->SetXY(195,$pdf->GetY()); $pdf->MultiCell(75,4,utf8_decode($dependencia['persona']),0,'C');
        $pdf->SetXY(195,$pdf->GetY()); $pdf->MultiCell(75,4,utf8_decode($dependencia['persona_cargo']),0,'C');   
        ##
        $pdf->Output();
    }
}
