<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Activos
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |26-07-2016               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'Fecha.php';
require_once ROOT.'librerias'.DS.'Number.php';

class FormularioBM2Controlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->FormularioBM2 = $this->metCargarModelo('FormularioBM2','reportes');
    }

    /**
     * Método para cargar vista Listado de Registros.
     *
     * @return Response
     */
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-ui-selectable/jquery-ui',
            'bootstrap-datepicker/datepicker',
        );
        $complementoJs = array(
            'mask/jquery.mask',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-ui-selectable/jquery-ui',
            'bootstrap-datepicker/bootstrap-datepicker',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        $filtro = [
            'fperiodo' => date('Y-m'),
        ];
        $this->atVista->assign('filtro', $filtro);
        $this->atVista->metRenderizar('filtro');
    }

    /**
     * Método para cargar vista Generar Acta.
     *
     * @return Response
     */
    public function metFrame()
    {
        ##  vista
        $this->atVista->assign('filtro', serialize($_POST));
        $this->atVista->metRenderizar('frame', 'modales');
    }

    /**
     * Método para imprimir PDF Transferencia de Activos.
     *
     * @return Response
     */
    public function metPdf()
    {
        $filtro = unserialize($_GET["filtro"]);
        $db = new db();
        list($PeriodoAnio, $PeriodoMes) = explode('-', $filtro['fperiodo']);
        $NombreMes = mb_strtoupper(Fecha::mes_nombre($PeriodoMes));
        ##  ----------------------------------
        $this->metObtenerLibreria('plantillaPDF','modAF');
        $parametros = Select::parametros();
        ##  datos de la dependencia default
        $sql = "SELECT
                    d.ind_dependencia,
                    o.ind_descripcion_empresa,
                    CONCAT_WS(' ', p.ind_nombre1, p.ind_nombre2, p.ind_apellido1, p.ind_apellido2) AS persona
                FROM
                    a004_dependencia d
                    INNER JOIN a001_organismo o ON (o.pk_num_organismo = d.fk_a001_num_organismo)
                    INNER JOIN a003_persona p ON (p.pk_num_persona = d.fk_a003_num_persona_responsable)
                WHERE d.pk_num_dependencia = '$parametros[DEPENDAFDEF]'";
        $query_dependencia = $db->query($sql);
        $query_dependencia->setFetchMode(PDO::FETCH_ASSOC);
        $dependencia = $query_dependencia->fetch();
        ##  Obtengo la data a imprimir
        $field = $this->FormularioBM2->metListar($filtro);
        ##  ----------------------------------
        ##  Creo el Reporte
        $pdf = new pdfDefaultL2('L','mm','Letter');
        $pdf->Organismo = 'DIRECCION DE ADMINISTRACIÓN';
        $pdf->Dependencia = 'AREA DE BIENES E INVENTARIO';
        $pdf->heightLine = 5;
        $pdf->Head = "";
        if (count($field))
        {
            $pdf->Head .= "
                \$this->SetFont('Arial','B',8);
                \$this->SetXY(220,12); \$this->Cell(25,5,utf8_decode('FORMULARIO BM-2'),0,1,'L');
                \$this->SetXY(220,17); \$this->Cell(25,5,utf8_decode('Hoja N° ' . \$this->PageNo() . ' / {nb}'),0,1,'L');
                \$this->SetY(28);
                \$this->SetFont('Arial','B',10); \$this->Cell(260,5,utf8_decode('RELACION DEL MOVIMIENTO DE BIENES MUEBLES'),0,0,'C');
                \$this->SetY(35);
                \$this->SetFont('Arial','B',9); \$this->Cell(20,5,utf8_decode('ESTADO: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(50,5,utf8_decode(\$this->Data['ind_estado']),0,1,'L');
                \$this->SetFont('Arial','B',9); \$this->Cell(20,5,utf8_decode('DISTRITO: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(50,5,utf8_decode(\$this->Data['ind_estado']),0,1,'L');
                \$this->SetFont('Arial','B',9); \$this->Cell(20,5,utf8_decode('MUNICIPIO: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(50,5,utf8_decode(\$this->Data['ind_municipio']),0,1,'L');
                \$this->SetFont('Arial','B',9); \$this->Cell(38,5,utf8_decode('DIRECCIÓN O LUGAR: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->MultiCell(200,5,utf8_decode(\$this->Data['ind_direccion']),0,'L');
                \$this->SetFont('Arial','B',9); \$this->Cell(60,5,utf8_decode('DEPENDENCIA O UNIDAD PRIMARIA: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(90,5,utf8_decode(\$this->Data['ind_dependencia']),0,0,'L');
                \$this->SetFont('Arial','B',9); \$this->Cell(20,5,utf8_decode('SERVICIO: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(50,5,utf8_decode(mb_strtoupper(\$this->Data['ind_descripcion_centro_costo'])),0,1,'L');
                \$this->SetFont('Arial','B',9); \$this->Cell(65,5,utf8_decode('UNIDAD DE TRABAJO O DEPENDENCIA: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(90,5,utf8_decode(\$this->Data['ind_dependencia']),0,1,'L');
                \$this->SetFont('Arial','B',9); \$this->Cell(42,5,utf8_decode('PERIODO DE LA CUENTA: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(50,5,utf8_decode('$NombreMes $PeriodoAnio'),0,1,'L');
                \$this->Ln(6);
            ";
        }
        $pdf->Head .= "
            \$this->SetFillColor(200,200,200);
            \$this->SetDrawColor(0,0,0);
            \$this->SetFont('Arial','B',8);
            \$this->SetXY(10,70); \$this->Cell(30,6,utf8_decode('Clasificación(Código)'),1,0,'C');
            \$this->SetXY(40,70); \$this->VCell(10,35,utf8_decode('Concepto de Movimiento'),1,0,'L');
            \$this->SetXY(50,70); \$this->VCell(10,35,utf8_decode('Cantidad'),1,0,'L');
            \$this->SetXY(60,70); \$this->VCell(15,35,utf8_decode('N° Identificación'),1,0,'L');
            \$this->SetXY(75,70); \$this->MultiCell(125,35,'',1,'C');
            \$this->SetXY(75,85); \$this->MultiCell(125,4,utf8_decode('NOMBRE Y DESCRIPCIÓN DE LOS BIENES, REFERENCIA DE LOS COMPROBANTES Y DE LOS PRECIOS UNITARIOS'),0,'C');
            \$this->SetXY(200,70); \$this->Cell(35,35,'',1,0,'C');
            \$this->SetXY(200,85); \$this->Cell(35,6,utf8_decode('Incorporaciones Bs.'),0,0,'C');
            \$this->SetXY(235,70); \$this->Cell(35,35,'',1,0,'C');
            \$this->SetXY(235,85); \$this->Cell(35,6,utf8_decode('Desincorporaciones Bs.'),0,0,'C');
            \$this->SetXY(10,76); \$this->VCell(10,29,utf8_decode('Grupo'),1,0,'L');
            \$this->SetXY(20,76); \$this->VCell(10,29,utf8_decode('Sub-Grupo'),1,0,'L');
            \$this->SetXY(30,76); \$this->VCell(10,29,utf8_decode('Sección'),1,0,'L');
            \$this->SetY(105);
        ";
        ##
        if (count($field)) $pdf->Data = $field[0];
        $pdf->AliasNbPages();
        ##
        $Grupo = '';
        $CantidadBienes = 0;
        $TotalIncorporaciones = 0;
        $TotalDesincorporaciones = 0;
        foreach ($field as $f) {
            $pdf->Data = $f;
            if ($Grupo != $f['fk_a023_num_centro_costo'])
            {
                if ($Grupo != '')
                {
                    $pdf->SetFont('Arial','B',8);
                    $pdf->Cell(100,10,utf8_decode('Total de Bienes: ' . $CantidadBienes),0,0,'L');
                    $pdf->Cell(90,10,utf8_decode('Total: '),0,0,'R');
                    $pdf->Cell(35,10,number_format($TotalIncorporaciones,2,',','.'),0,0,'R');
                    $pdf->Cell(35,10,number_format($TotalDesincorporaciones,2,',','.'),0,0,'R');
                    ## 
                    if ($pdf->GetY() > 160) $pdf->AddPage();
                    $pdf->SetFont('Arial','B',8);
                    $pdf->SetXY(170,180); $pdf->Cell(100,6,utf8_decode('RECIBI CONFORME: ___________________________________________'),0,1,'C');
                    $pdf->SetXY(195,$pdf->GetY()); $pdf->MultiCell(75,4,utf8_decode($f['persona_dependencia']),0,'C');
                    $pdf->SetXY(195,$pdf->GetY()); $pdf->MultiCell(75,4,utf8_decode($f['persona_dependencia_cargo']),0,'C');
                }
                $Grupo = $f['fk_a023_num_centro_costo'];
                $pdf->AddPage();
                ##  
                $CantidadBienes = 0;
                $TotalIncorporaciones = 0;
                $TotalDesincorporaciones = 0;
            }
            if ($f['ind_tipo'] == 'I') 
            {
                $Incorporaciones = $f['num_monto'];
                $Desincorporaciones = 0;
                $TotalIncorporaciones += $Incorporaciones;
            }
            else
            {
                $Incorporaciones = 0;
                $Desincorporaciones = $f['num_monto'];
                $TotalDesincorporaciones += $Desincorporaciones;
            }
            $digitos_clasificacion = strlen($f['clasificacion_codigo']);
            $pdf->SetFillColor(255,255,255);
            $pdf->SetWidths(array(10,10,10,10,10,15,125,35,35));
            $pdf->SetAligns(array('C','C','C','C','C','C','L','R','R'));
            $pdf->SetFont('Arial','',8);
            $pdf->Row(array(substr($f['clasificacion_codigo'],0,2),
                            substr($f['clasificacion_codigo'],2,2),
                            ($digitos_clasificacion > 4 ? substr($f['clasificacion_codigo'],4,3) : ''),
                            utf8_decode($f['movimiento_codigo']),
                            '1',
                            utf8_decode($f['cod_codigo_interno']),
                            utf8_decode($f['ind_descripcion']),
                            number_format($Incorporaciones,2,',','.'),
                            number_format($Desincorporaciones,2,',','.')
                    ));
            ++$CantidadBienes;
        }
        if (count($field)) 
        {
            $pdf->SetFont('Arial','B',8);
            $pdf->Cell(100,10,utf8_decode('Total de Bienes: ' . $CantidadBienes),0,0,'L');
            $pdf->Cell(90,10,utf8_decode('Total: '),0,0,'R');
            $pdf->Cell(35,10,number_format($TotalIncorporaciones,2,',','.'),0,0,'R');
            $pdf->Cell(35,10,number_format($TotalDesincorporaciones,2,',','.'),0,0,'R');
            ##  
            if ($pdf->GetY() > 160) $pdf->AddPage();
            $pdf->SetFont('Arial','B',8);
            $pdf->SetXY(170,180); $pdf->Cell(100,6,utf8_decode('RECIBI CONFORME: ___________________________________________'),0,1,'C');
            $pdf->SetXY(195,$pdf->GetY()); $pdf->MultiCell(75,4,utf8_decode($f['persona_dependencia']),0,'C');
            $pdf->SetXY(195,$pdf->GetY()); $pdf->MultiCell(75,4,utf8_decode($f['persona_dependencia_cargo']),0,'C');   
        }
        ##
        $pdf->Output();
    }
}
