<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Acta de Incorporación de Bienes
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |23-02-2017               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'Fecha.php';
require_once ROOT.'librerias'.DS.'Number.php';

class IncorporacionBienesControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->IncorporacionBienes = $this->metCargarModelo('IncorporacionBienes','reportes');
    }

    /**
     * Método para cargar vista Listado de Registros.
     *
     * @return Response
     */
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-ui-selectable/jquery-ui',
            'bootstrap-datepicker/datepicker',
        );
        $complementoJs = array(
            'mask/jquery.mask',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-ui-selectable/jquery-ui',
            'bootstrap-datepicker/bootstrap-datepicker',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        if ($_POST) $filtro = $_POST;
        else {
            $filtro = [
                'fpk_num_organismo' => '',
                'fpk_num_dependencia' => '',
                'fpk_num_centro_costo' => '',
                'ffec_fechad' => date('01-m-Y'),
                'ffec_fechah' => date('d-m-Y'),
            ];
        }
        $data = [
            'titulo' => 'Acta de Incorporación de Bienes',
        ];
        $this->atVista->assign('data', $data);
        $this->atVista->assign('filtro', $filtro);
        $this->atVista->assign('listado', $this->IncorporacionBienes->metListar($filtro));
        $this->atVista->metRenderizar('filtro');
    }

    /**
     * Método para cargar vista.
     *
     * @return Response
     */
    public function metMostrarActaIncorporacion()
    {
        ##  vista
        $this->atVista->assign('data', ['id'=>$_POST['id']]);
        $this->atVista->metRenderizar('frame', 'modales');
    }

    /**
     * Método para imprimir PDF.
     *
     * @return Response
     */
    public function metImprimirActaIncorporacion($id)
    {
        $db = new db();
        ##  ----------------------------------
        $this->metObtenerLibreria('plantillaPDF','modAF');
        $parametros = Select::parametros();
        ##  datos de la dependencia default
        $sql = "SELECT
                    d.ind_dependencia,
                    o.ind_descripcion_empresa,
                    CONCAT_WS(' ', p.ind_nombre1, p.ind_nombre2, p.ind_apellido1, p.ind_apellido2) AS persona
                FROM
                    a004_dependencia d
                    INNER JOIN a001_organismo o ON (o.pk_num_organismo = d.fk_a001_num_organismo)
                    INNER JOIN a003_persona p ON (p.pk_num_persona = d.fk_a003_num_persona_responsable)
                WHERE d.pk_num_dependencia = '$parametros[DEPENDAFDEF]'";
        $query_dependencia = $db->query($sql);
        $query_dependencia->setFetchMode(PDO::FETCH_ASSOC);
        $dependencia = $query_dependencia->fetch();
        ##  ----------------------------------
        ##  Obtengo la data a imprimir
        $field = $this->IncorporacionBienes->metObtenerActa($id);
        list($Anio, $Mes, $Dia) = explode('-', $field['fec_fecha']);
        ##  Creo el Reporte
        $pdf = new pdfDefaultP('P','mm','Letter');
        $pdf->Organismo = 'DIRECCION DE ADMINISTRACIÓN';
        $pdf->Dependencia = 'AREA DE BIENES E INVENTARIO';
        $pdf->Titulo = 'ACTA DE INCORPORACION DE BIENES MUEBLES';
        $pdf->Head = "
            \$this->SetFont('Arial','B',8);
            \$this->SetXY(165,22); \$this->Cell(25,5,utf8_decode('Acta: " . $field['ind_nro_acta'] ."-" . $field['num_anio'] . "'),0,1,'L');
            \$this->SetY(45);
        ";
        $pdf->Foot = "
            \$this->SetFont('Arial','B',8);
            \$this->SetXY(15,265); \$this->Cell(25,5,utf8_decode('REF.: FOR-DSG-003'),0,1,'L');
        ";
        ##
        $pdf->AliasNbPages();
        $pdf->AddPage();
        ##
        $Parrafo1 = "El (la) Suscrito $field[persona_responsable], C.I: N° $field[cedula_responsable] $field[cargo_responsable] de la $field[ind_descripcion_empresa], en cumplimiento al Artículo 8, Titulo 11 de la Ley de Contraloría del Estado Delta Amacuro hace constar por medio de la presente, que los bienes que a continuación se especifican han sido incorporados al Inventario General de esta Institución";
        $pdf->SetFont('Arial','',12);
        $pdf->MultiCell(195,7,utf8_decode($Parrafo1),0,'FJ');
        $pdf->Ln(3);

        $pdf->SetFillColor(240,240,240);
        $pdf->SetDrawColor(0,0,0);
        $pdf->Ln(1);
        $pdf->SetFont('Arial','B',7);
        $pdf->SetWidths(array(25,15,20,75,35,25));
        $pdf->SetAligns(array('C','C','C','L','C','C'));
        $pdf->Row(array(utf8_decode('CLASIFICACIÓN'),
                        utf8_decode('CANTIDAD'),
                        utf8_decode('COD. INTERNO'),
                        utf8_decode('DESCRIPCIÓN'),
                        utf8_decode('UBICACIÓN'),
                        utf8_decode('PRECIO')
                ));
        $pdf->SetFillColor(255,255,255);
        $pdf->SetDrawColor(0,0,0);
        $pdf->SetFont('Arial','',7);
        $pdf->Row(array(utf8_decode($field['cod_clasificacion']),
                        1,
                        utf8_decode($field['cod_activo']),
                        utf8_decode($field['desc_activo']),
                        utf8_decode($field['ind_nombre_ubicacion']),
                        utf8_decode('')
                ));
        $pdf->Ln(15);

        ##  firmas
        if ($pdf->GetY() > 240) $pdf->AddPage();
        $pdf->SetY(240);
        $pdf->SetDrawColor(0,0,0);
        $pdf->Rect(29, $pdf->GetY(), 60, 0.1, "D");
        $pdf->Rect(126, $pdf->GetY(), 60, 0.1, "D");
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(98,5,utf8_decode($field['persona_aprobado']),0,0,'C');
        $pdf->Cell(98,5,utf8_decode($field['persona_conformado']),0,0,'C');
        $pdf->Ln(5);
        $pdf->Cell(98,5,utf8_decode($field['cargo_aprobado']),0,0,'C');
        $pdf->Cell(98,5,utf8_decode($field['cargo_conformado']),0,0,'C');
        ##
        $pdf->Output();
    }
}
