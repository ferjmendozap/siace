<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Activos
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |26-07-2016               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'Fecha.php';
require_once ROOT.'librerias'.DS.'Number.php';

class FormularioBM1Controlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->FormularioBM1 = $this->metCargarModelo('FormularioBM1','reportes');
    }

    /**
     * Método para cargar vista Listado de Registros.
     *
     * @return Response
     */
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-ui-selectable/jquery-ui',
            'bootstrap-datepicker/datepicker',
        );
        $complementoJs = array(
            'mask/jquery.mask',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-ui-selectable/jquery-ui',
            'bootstrap-datepicker/bootstrap-datepicker',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('filtro');
    }

    /**
     * Método para cargar vista Generar Acta.
     *
     * @return Response
     */
    public function metFrame()
    {
        ##  vista
        $this->atVista->assign('filtro', serialize($_POST));
        $this->atVista->metRenderizar('frame', 'modales');
    }

    /**
     * Método para imprimir PDF Transferencia de Activos.
     *
     * @return Response
     */
    public function metPdf()
    {
        $filtro = unserialize($_GET["filtro"]);
        $db = new db();
        ##  ----------------------------------
        $this->metObtenerLibreria('plantillaPDF','modAF');
        $parametros = Select::parametros();
        ##  datos de la dependencia default
        $sql = "SELECT
                    d.ind_dependencia,
                    o.ind_descripcion_empresa,
                    CONCAT_WS(' ', p.ind_nombre1, p.ind_nombre2, p.ind_apellido1, p.ind_apellido2) AS persona
                FROM
                    a004_dependencia d
                    INNER JOIN a001_organismo o ON (o.pk_num_organismo = d.fk_a001_num_organismo)
                    INNER JOIN a003_persona p ON (p.pk_num_persona = d.fk_a003_num_persona_responsable)
                WHERE d.pk_num_dependencia = '$parametros[DEPENDAFDEF]'";
        $query_dependencia = $db->query($sql);
        $query_dependencia->setFetchMode(PDO::FETCH_ASSOC);
        $dependencia = $query_dependencia->fetch();
        ##  Obtengo la data a imprimir
        $field = $this->FormularioBM1->metListar($filtro);
        ##  ----------------------------------
        ##  Creo el Reporte
        $pdf = new pdfDefaultL2('L','mm','Letter');
        $pdf->Organismo = 'DIRECCION DE ADMINISTRACIÓN';
        $pdf->Dependencia = 'AREA DE BIENES E INVENTARIO';
        $pdf->heightLine = 5;
        $pdf->Head = "";
        if (count($field))
        {
            $pdf->Head .= "
                \$this->SetFont('Arial','B',8);
                \$this->SetXY(220,12); \$this->Cell(25,5,utf8_decode('FORMULARIO BM-1'),0,1,'L');
                \$this->SetXY(220,17); \$this->Cell(25,5,utf8_decode('Hoja N° ' . \$this->PageNo() . ' / {nb}'),0,1,'L');
                \$this->SetY(23);
                \$this->SetFont('Arial','B',10); \$this->Cell(260,5,utf8_decode('INVENTARIO DE BIENES MUEBLES'),0,0,'C');
                \$this->SetY(30);
                \$this->SetFont('Arial','B',9); \$this->Cell(50,5,utf8_decode('ENTIDAD PROPIETARIA: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(90,5,utf8_decode(\$this->Data['ind_descripcion_empresa']),0,0,'L');
                \$this->SetX(120);
                \$this->SetFont('Arial','B',9); \$this->Cell(20,5,utf8_decode('SERVICIO: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(50,5,utf8_decode(mb_strtoupper(\$this->Data['ind_dependencia'])),0,0,'L');
                \$this->Ln(5);
                \$this->SetFont('Arial','B',9); \$this->Cell(50,5,utf8_decode('UNIDAD DE TRABAJO O ÁREA: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(50,5,utf8_decode(mb_strtoupper(\$this->Data['ind_nombre_ubicacion'])),0,0,'L');
                \$this->Ln(5);
                \$this->SetFont('Arial','B',9); \$this->Cell(20,5,utf8_decode('ESTADO: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(50,5,utf8_decode(\$this->Data['ind_estado']),0,0,'L');
                \$this->SetFont('Arial','B',9); \$this->Cell(20,5,utf8_decode('MUNICIPIO: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(50,5,utf8_decode(\$this->Data['ind_municipio']),0,0,'L');
                \$this->SetFont('Arial','B',9); \$this->Cell(20,5,utf8_decode('CIUDAD: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(50,5,utf8_decode(\$this->Data['ind_ciudad']),0,0,'L');
                \$this->SetFont('Arial','B',9); \$this->Cell(20,5,utf8_decode('FECHA: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->Cell(25,5,date('d-m-Y'),0,0,'L');
                \$this->Ln(5);
                \$this->SetX(10);
                \$this->SetFont('Arial','B',9); \$this->Cell(50,5,utf8_decode('DIRECCIÓN O LUGAR: '),0,0,'L');
                \$this->SetFont('Arial','',9); \$this->MultiCell(170,5,utf8_decode(\$this->Data['ind_direccion']),0,'L');
            ";
        }
        $pdf->Head .= "
            \$this->Ln(3);
            \$this->SetFillColor(200,200,200);
            \$this->SetDrawColor(0,0,0);
            \$this->SetFont('Arial','B',8);
            \$this->Cell(48,6,utf8_decode('Clasificación'),1,0,'C');
            \$this->Cell(17,12,utf8_decode('Cantidad'),1,0,'C');
            \$this->Cell(35,12,utf8_decode('N° Identificación'),1,0,'C');
            \$this->Cell(100,12,utf8_decode('Nombre y Descripción de los Elementos'),1,0,'C');
            \$this->Cell(30,12,utf8_decode('Valor Unitario Bs.'),1,0,'C');
            \$this->Cell(30,12,utf8_decode('Valor Total Bs.'),1,0,'C');
            \$this->Ln(6);
            \$this->Cell(16,6,utf8_decode('Grupo'),1,0,'C');
            \$this->Cell(16,6,utf8_decode('Sub-Grupo'),1,0,'C');
            \$this->Cell(16,6,utf8_decode('Sección'),1,0,'C');
            \$this->Ln(6);
        ";
        ##
        if (count($field)) $pdf->Data = $field[0];
        $pdf->AliasNbPages();
        ##
        $Grupo = '';
        foreach ($field as $f) {
            $pdf->Data = $f;
            if ($Grupo != $f['fk_a023_num_centro_costo'])
            {
                if ($Grupo != '')
                {
                    if ($pdf->GetY() > 160) $pdf->AddPage();
                    $pdf->SetFont('Arial','B',8);
                    $pdf->SetXY(170,180); $pdf->Cell(100,6,utf8_decode('RECIBI CONFORME: ___________________________________________'),0,1,'C');
                    $pdf->SetXY(195,$pdf->GetY()); $pdf->MultiCell(75,4,utf8_decode($f['persona_dependencia']),0,'C');
                    $pdf->SetXY(195,$pdf->GetY()); $pdf->MultiCell(75,4,utf8_decode($f['persona_dependencia_cargo']),0,'C');
                }
                $Grupo = $f['fk_a023_num_centro_costo'];
                $pdf->AddPage();
            }
            ##
            $digitos_clasificacion = strlen($f['clasificacion_codigo']);
            $pdf->SetFillColor(255,255,255);
            $pdf->SetWidths(array(16,16,16,17,35,100,30,30));
            $pdf->SetAligns(array('C','C','C','C','C','L','R','R'));
            $pdf->SetFont('Arial','',8);
            $pdf->Row(array(substr($f['clasificacion_codigo'],0,2),
                            substr($f['clasificacion_codigo'],2,2),
                            ($digitos_clasificacion > 4 ? substr($f['clasificacion_codigo'],4,3) : ''),
                            '1',
                            utf8_decode($f['cod_codigo_interno']),
                            utf8_decode($f['ind_descripcion']),
                            number_format($f['num_monto'],2,',','.'),
                            number_format($f['num_monto'],2,',','.')
                    ));
        }
        if (count($field)) 
        {
            if ($pdf->GetY() > 160) $pdf->AddPage();
            $pdf->SetFont('Arial','B',8);
            $pdf->SetXY(170,180); $pdf->Cell(100,6,utf8_decode('RECIBI CONFORME: ___________________________________________'),0,1,'C');
            $pdf->SetXY(195,$pdf->GetY()); $pdf->MultiCell(75,4,utf8_decode($f['persona_dependencia']),0,'C');
            $pdf->SetXY(195,$pdf->GetY()); $pdf->MultiCell(75,4,utf8_decode($f['persona_dependencia_cargo']),0,'C');   
        }
        ##
        $pdf->Output();
    }
}
