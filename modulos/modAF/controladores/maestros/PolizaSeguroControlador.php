<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Maestro P�lizas de Seguro
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bol�var                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |13-11-2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'Fecha.php';
require_once ROOT.'librerias'.DS.'Number.php';

class PolizaSeguroControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->PolizaSeguro = $this->metCargarModelo('PolizaSeguro','maestros');
    }

    /**
     * M�todo para cargar vista Listado de Registros.
     *
     * @return Response
     */
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-ui-selectable/jquery-ui',
            'bootstrap-datepicker/datepicker',
        );
        $complementoJs = array(
            'mask/jquery.mask',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-ui-selectable/jquery-ui',
            'bootstrap-datepicker/bootstrap-datepicker',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado', $this->PolizaSeguro->metListar());
        $this->atVista->metRenderizar('listado');
    }

    /**
     * M�todo para cargar vista formulario Nuevo Registro.
     *
     * @return Response
     */
    public function metNuevo()
    {
        ##  valores del formulario
        $form = [
            'metodo' => 'crear',
            'pk_num_poliza_seguro' => null,
            'ind_nombre_poliza_seguro' => null,
            'ind_empresa_aseguradora' => null,
            'ind_agente_seguros' => null,
            'fec_fecha_vencimiento' => null,
            'num_monto_cobertura' => null,
            'num_costo_poliza' => null,
            'num_estatus' => 1,
        ];

        ##  vista
        $this->atVista->assign('form', $form);
        $this->atVista->metRenderizar('formulario', 'modales');
    }

    /**
     * M�todo para cargar vista formulario Editar Registro.
     *
     * @return Response
     */
    public function metEditar()
    {
        ##  valores del formulario
        $form = $this->PolizaSeguro->metObtener($_POST['id']);
        $form['metodo'] = 'modificar';

        ##  vista
        $this->atVista->assign('form', $form);
        $this->atVista->metRenderizar('formulario', 'modales');
    }

    /**
     * M�todo para crear un nuevo registro.
     *
     * @return Void
     */
    public function metCrear()
    {
        $_POST['num_estatus'] = (isset($_POST['num_estatus'])?$_POST['num_estatus']:0);
        $_POST['fec_fecha_vencimiento'] = Fecha::formatFecha($_POST['fec_fecha_vencimiento']);
        $_POST['num_monto_cobertura'] = Number::format($_POST['num_monto_cobertura']);
        $_POST['num_costo_poliza'] = Number::format($_POST['num_costo_poliza']);

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'ind_nombre_poliza_seguro' => 'required|alpha_space|max_len,50',
            'ind_empresa_aseguradora' => 'required|alpha_full|max_len,100',
            'ind_agente_seguros' => 'required|alpha_space|max_len,100',
            'fec_fecha_vencimiento' => 'required|date',
            'num_monto_cobertura' => 'required|float',
            'num_costo_poliza' => 'required|float',
        ]);
        $gump->set_field_name("ind_nombre_poliza_seguro", "Nombre");
        $gump->set_field_name("ind_empresa_aseguradora", "Empresa Aseguradora");
        $gump->set_field_name("ind_agente_seguros", "Agente de Seguros");
        $gump->set_field_name("fec_fecha_vencimiento", "Fecha de Vencimiento");
        $gump->set_field_name("num_monto_cobertura", "Monto Cobertura");
        $gump->set_field_name("num_costo_poliza", "Costo Poliza");

        ##  validaci�n exitosa
        if($validate === TRUE) {
            ##  crear registro
            $id = $this->PolizaSeguro->metCrear($_POST);
            $field = $this->PolizaSeguro->metObtener($id);
            ##  devolver registro creado
            $jsondata = [
                'status' => 'crear',
                'mensaje' => 'Registro creado exitosamente',
                'id' => $id,
                'tr' => [
                    '<td><label>'.$id.'</label></td>',
                    '<td><label>'.$field['ind_nombre_poliza_seguro'].'</label></td>',
                    '<td><i class="'.($field['num_estatus']==1?'md md-check':'md md-not-interested').'"></i></td>',
                    '<td>
                        <div class="btn-group">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Modificar Registro">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'
                            '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Registro!!">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                        </div>
                    </td>',
                ],
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
     * M�todo para actualizar un registro.
     *
     * @return Void
     */
    public function metModificar()
    {
        $_POST['num_estatus'] = (isset($_POST['num_estatus'])?$_POST['num_estatus']:0);
        $_POST['fec_fecha_vencimiento'] = Fecha::formatFecha($_POST['fec_fecha_vencimiento']);
        $_POST['num_monto_cobertura'] = Number::format($_POST['num_monto_cobertura']);
        $_POST['num_costo_poliza'] = Number::format($_POST['num_costo_poliza']);
        $id = $_POST['pk_num_poliza_seguro'];

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'ind_nombre_poliza_seguro' => 'required|alpha_space|max_len,50',
            'ind_empresa_aseguradora' => 'required|alpha_full|max_len,100',
            'ind_agente_seguros' => 'required|alpha_space|max_len,100',
            'fec_fecha_vencimiento' => 'required|date',
            'num_monto_cobertura' => 'required|float',
            'num_costo_poliza' => 'required|float',
        ]);
        $gump->set_field_name("ind_nombre_poliza_seguro", "Nombre");
        $gump->set_field_name("ind_empresa_aseguradora", "Empresa Aseguradora");
        $gump->set_field_name("ind_agente_seguros", "Agente de Seguros");
        $gump->set_field_name("fec_fecha_vencimiento", "Fecha de Vencimiento");
        $gump->set_field_name("num_monto_cobertura", "Monto Cobertura");
        $gump->set_field_name("num_costo_poliza", "Costo Poliza");

        ##  validaci�n exitosa
        if($validate === TRUE) {
            ##  crear registro
            $this->PolizaSeguro->metModificar($_POST);
            $field = $this->PolizaSeguro->metObtener($id);

            ##  devolver registro creado
            $jsondata = [
                'status' => 'modificar',
                'mensaje' => 'Registro actualizado exitosamente',
                'id' => $id,
                'tr' => '
                    <td><label>'.$id.'</label></td>
                    <td><label>'.$field['ind_nombre_poliza_seguro'].'</label></td>
                    <td><i class="'.($field['num_estatus']==1?'md md-check':'md md-not-interested').'"></i></td>
                    <td>
                        <div class="btn-group">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Modificar Registro">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'
                            '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Registro!!">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                        </div>
                    </td>
                ',
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
     * M�todo para eliminar un registro.
     *
     * @return Void
     */
    public function metEliminar()
    {
        $this->PolizaSeguro->metEliminar(['pk_num_poliza_seguro' => $_POST['id']]);

        $jsondata = [
            'status' => 'OK',
            'mensaje' => 'Registro eliminado exitosamente',
            'id' => $_POST['id'],
        ];
        echo json_encode($jsondata);
        exit();
    }
}
