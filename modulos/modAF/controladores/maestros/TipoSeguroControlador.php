<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Maestro Tipos de Seguro
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bol�var                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |13-11-2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'Fecha.php';
require_once ROOT.'librerias'.DS.'Number.php';

class TipoSeguroControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->TipoSeguro = $this->metCargarModelo('TipoSeguro','maestros');
    }

    /**
     * M�todo para cargar vista Listado de Registros.
     *
     * @return Response
     */
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-ui-selectable/jquery-ui',
        );
        $complementoJs = array(
            'mask/jquery.mask',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-ui-selectable/jquery-ui',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado', $this->TipoSeguro->metListar());
        $this->atVista->metRenderizar('listado');
    }

    /**
     * M�todo para cargar vista formulario Nuevo Registro.
     *
     * @return Response
     */
    public function metNuevo()
    {
        ##  valores del formulario
        $form = [
            'metodo' => 'crear',
            'pk_num_tipo_seguro' => null,
            'ind_nombre_tipo_seguro' => null,
            'num_estatus' => 1,
        ];

        ##  vista
        $this->atVista->assign('form', $form);
        $this->atVista->metRenderizar('formulario', 'modales');
    }

    /**
     * M�todo para cargar vista formulario Editar Registro.
     *
     * @return Response
     */
    public function metEditar()
    {
        ##  valores del formulario
        $form = $this->TipoSeguro->metObtener($_POST['id']);
        $form['metodo'] = 'modificar';

        ##  vista
        $this->atVista->assign('form', $form);
        $this->atVista->metRenderizar('formulario', 'modales');
    }

    /**
     * M�todo para crear un nuevo registro.
     *
     * @return Void
     */
    public function metCrear()
    {
        $_POST['num_estatus'] = (isset($_POST['num_estatus'])?$_POST['num_estatus']:0);

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'ind_nombre_tipo_seguro' => 'required|alpha_space|max_len,45',
        ]);
        $gump->set_field_name("ind_nombre_tipo_seguro", "Nombre");

        ##  validaci�n exitosa
        if($validate === TRUE) {
            ##  crear registro
            $id = $this->TipoSeguro->metCrear($_POST);
            $field = $this->TipoSeguro->metObtener($id);
            ##  devolver registro creado
            $jsondata = [
                'status' => 'crear',
                'mensaje' => 'Registro creado exitosamente',
                'id' => $id,
                'tr' => [
                    '<td><label>'.$id.'</label></td>',
                    '<td><label>'.$field['ind_nombre_tipo_seguro'].'</label></td>',
                    '<td><i class="'.($_POST['num_estatus']==1?'md md-check':'md md-not-interested').'"></i></td>',
                    '<td>
                        <div class="btn-group">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Modificar Registro">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'
                            '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Registro!!">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                        </div>
                    </td>',
                ],
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
     * M�todo para actualizar un registro.
     *
     * @return Void
     */
    public function metModificar()
    {
        $_POST['num_estatus'] = (isset($_POST['num_estatus'])?$_POST['num_estatus']:0);
        $id = $_POST['pk_num_tipo_seguro'];

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'ind_nombre_tipo_seguro' => 'required|alpha_space|max_len,45',
        ]);
        $gump->set_field_name("ind_nombre_tipo_seguro", "Nombre");

        ##  validaci�n exitosa
        if($validate === TRUE) {
            ##  crear registro
            $this->TipoSeguro->metModificar($_POST);
            $field = $this->TipoSeguro->metObtener($id);

            ##  devolver registro creado
            $jsondata = [
                'status' => 'modificar',
                'mensaje' => 'Registro actualizado exitosamente',
                'id' => $id,
                'tr' => '
                    <td><label>'.$id.'</label></td>
                    <td><label>'.$field['ind_nombre_tipo_seguro'].'</label></td>
                    <td><i class="'.($_POST['num_estatus']==1?'md md-check':'md md-not-interested').'"></i></td>
                    <td>
                        <div class="btn-group">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Modificar Registro">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'
                            '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Registro!!">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                        </div>
                    </td>
                ',
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
     * M�todo para eliminar un registro.
     *
     * @return Void
     */
    public function metEliminar()
    {
        $this->TipoSeguro->metEliminar(['pk_num_tipo_seguro' => $_POST['id']]);

        $jsondata = [
            'status' => 'OK',
            'mensaje' => 'Registro eliminado exitosamente',
            'id' => $_POST['id'],
        ];
        echo json_encode($jsondata);
        exit();
    }
}
