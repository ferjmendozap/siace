<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Maestro Categoria de Depreciación
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |17-11-2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'Fecha.php';
require_once ROOT.'librerias'.DS.'Number.php';

class CategoriaControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->Categoria = $this->metCargarModelo('Categoria','maestros');
    }

    /**
     * Método para cargar vista Listado de Registros.
     *
     * @return Response
     */
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-ui-selectable/jquery-ui',
        );
        $complementoJs = array(
            'mask/jquery.mask',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-ui-selectable/jquery-ui',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado', $this->Categoria->metListar());
        $this->atVista->metRenderizar('listado');
    }

    /**
     * Método para cargar vista formulario Nuevo Registro.
     *
     * @return Response
     */
    public function metNuevo()
    {
        ##  valores del formulario
        $form = [
            'metodo' => 'crear',
            'pk_num_categoria' => null,
            'ind_codigo' => null,
            'ind_nombre_categoria' => null,
            'fk_cbb004_num_cuenta_historica' => null,
            'num_cuenta_historica' => null,
            'fk_cbb004_num_cuenta_historica_variacion' => null,
            'num_cuenta_historica_variacion' => null,
            'fk_cbb004_num_cuenta_historica_revaluacion' => null,
            'num_cuenta_historica_revaluacion' => null,
            'fk_cbb004_num_cuenta_depreciacion' => null,
            'num_cuenta_depreciacion' => null,
            'fk_cbb004_num_cuenta_depreciacion_variacion' => null,
            'num_cuenta_depreciacion_variacion' => null,
            'fk_cbb004_num_cuenta_depreciacion_revaluacion' => null,
            'num_cuenta_depreciacion_revaluacion' => null,
            'fk_cbb004_num_cuenta_gastos' => null,
            'num_cuenta_gastos' => null,
            'fk_cbb004_num_cuenta_gastos_revaluacion' => null,
            'num_cuenta_gastos_revaluacion' => null,
            'fk_cbb004_num_cuenta_rei' => null,
            'num_cuenta_rei' => null,
            'fk_cbb004_num_cuenta_neto' => null,
            'num_cuenta_neto' => null,
            'fk_cbb004_num_cuenta_resultado' => null,
            'num_cuenta_resultado' => null,
            'num_flag_inventariable' => 0,
            'ind_tipo' => null,
            'ind_grupo' => null,
            'num_estatus' => 1,
            'listado_contabilidad' => [],
        ];

        ##  vista
        $this->atVista->assign('form', $form);
        $this->atVista->metRenderizar('formulario', 'modales');
    }

    /**
     * Método para cargar vista formulario Editar Registro.
     *
     * @return Response
     */
    public function metEditar()
    {
        ##  valores del formulario
        $form = $this->Categoria->metObtener($_POST['id']);
        $form['listado_contabilidad'] = $this->Categoria->metObtenerContabilidades($form['pk_num_categoria']);
        $form['metodo'] = 'modificar';

        ##  vista
        $this->atVista->assign('form', $form);
        $this->atVista->metRenderizar('formulario', 'modales');
    }

    /**
     * Método para crear un nuevo registro.
     *
     * @return Void
     */
    public function metCrear()
    {
        $_POST['num_estatus'] = (isset($_POST['num_estatus'])?$_POST['num_estatus']:0);
        $_POST['num_flag_inventariable'] = (isset($_POST['num_flag_inventariable'])?$_POST['num_flag_inventariable']:0);

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'ind_codigo' => 'required|max_len,15|numeric',
            'fk_a006_num_tipo' => 'required',
            'fk_a006_num_grupo' => 'required',
            'ind_nombre_categoria' => 'required|max_len,100',
            'num_cuenta_historica' => 'required|numeric',
            'num_cuenta_historica_variacion' => 'required|numeric',
            'num_cuenta_historica_revaluacion' => 'required|numeric',
            'num_cuenta_depreciacion' => 'required|numeric',
            'num_cuenta_depreciacion_variacion' => 'required|numeric',
            'num_cuenta_depreciacion_revaluacion' => 'required|numeric',
            'num_cuenta_gastos' => 'required|numeric',
            'num_cuenta_gastos_revaluacion' => 'required|numeric',
            'num_cuenta_neto' => 'required|numeric',
            'num_cuenta_rei' => 'required|numeric',
            'num_cuenta_resultado' => 'required|numeric',
        ]);
        $gump->set_field_name("ind_codigo", "Código");
        $gump->set_field_name("fk_a006_num_tipo", "Tipo de Deprec.");
        $gump->set_field_name("fk_a006_num_grupo", "Grupo Categoría");
        $gump->set_field_name("ind_nombre_categoria", "Nombre");
        $gump->set_field_name("num_cuenta_historica", "Valor Histórico");
        $gump->set_field_name("num_cuenta_historica_variacion", "Adiciones");
        $gump->set_field_name("num_cuenta_historica_revaluacion", "Ajuste x Inflación");
        $gump->set_field_name("num_cuenta_depreciacion", "Para Depreciación");
        $gump->set_field_name("num_cuenta_depreciacion_variacion", "Adiciones");
        $gump->set_field_name("num_cuenta_depreciacion_revaluacion", "Ajuste x Inflación");
        $gump->set_field_name("num_cuenta_gastos", "Depreciación");
        $gump->set_field_name("num_cuenta_gastos_revaluacion", "Ajuste x Inflación");
        $gump->set_field_name("num_cuenta_neto", "Valor Neto");
        $gump->set_field_name("num_cuenta_rei", "R.E.I");
        $gump->set_field_name("num_cuenta_resultado", "Cta. Resultado");

        ##  validación exitosa
        if($validate === TRUE) {
            ##  crear registro
            $id = $this->Categoria->metCrear($_POST);
            $field = $this->Categoria->metObtener($id);
            ##  devolver registro creado
            $jsondata = [
                'status' => 'crear',
                'mensaje' => 'Registro creado exitosamente',
                'id' => $id,
                'tr' => [
                    '<td><label>'.$id.'</label></td>',
                    '<td><label>'.$field['ind_codigo'].'</label></td>',
                    '<td><label>'.$field['ind_nombre_categoria'].'</label></td>',
                    '<td><i class="'.($field['num_estatus']==1?'md md-check':'md md-not-interested').'"></i></td>',
                    '<td>
                        <div class="btn-group">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Modificar Registro">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'
                            '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Registro!!">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                        </div>
                    </td>',
                ],
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
     * Método para actualizar un registro.
     *
     * @return Void
     */
    public function metModificar()
    {
        $_POST['num_estatus'] = (isset($_POST['num_estatus'])?$_POST['num_estatus']:0);
        $_POST['num_flag_inventariable'] = (isset($_POST['num_flag_inventariable'])?$_POST['num_flag_inventariable']:0);
        $id = $_POST['pk_num_categoria'];

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'ind_codigo' => 'required|max_len,15|numeric',
            'fk_a006_num_tipo' => 'required',
            'fk_a006_num_grupo' => 'required',
            'ind_nombre_categoria' => 'required|max_len,100',
            'num_cuenta_historica' => 'required|numeric',
            'num_cuenta_historica_variacion' => 'required|numeric',
            'num_cuenta_historica_revaluacion' => 'required|numeric',
            'num_cuenta_depreciacion' => 'required|numeric',
            'num_cuenta_depreciacion_variacion' => 'required|numeric',
            'num_cuenta_depreciacion_revaluacion' => 'required|numeric',
            'num_cuenta_gastos' => 'required|numeric',
            'num_cuenta_gastos_revaluacion' => 'required|numeric',
            'num_cuenta_neto' => 'required|numeric',
            'num_cuenta_rei' => 'required|numeric',
            'num_cuenta_resultado' => 'required|numeric',
        ]);
        $gump->set_field_name("ind_codigo", "Código");
        $gump->set_field_name("fk_a006_num_tipo", "Tipo de Deprec.");
        $gump->set_field_name("fk_a006_num_grupo", "Grupo Categoría");
        $gump->set_field_name("ind_nombre_categoria", "Nombre");
        $gump->set_field_name("num_cuenta_historica", "Valor Histórico");
        $gump->set_field_name("num_cuenta_historica_variacion", "Adiciones");
        $gump->set_field_name("num_cuenta_historica_revaluacion", "Ajuste x Inflación");
        $gump->set_field_name("num_cuenta_depreciacion", "Para Depreciación");
        $gump->set_field_name("num_cuenta_depreciacion_variacion", "Adiciones");
        $gump->set_field_name("num_cuenta_depreciacion_revaluacion", "Ajuste x Inflación");
        $gump->set_field_name("num_cuenta_gastos", "Depreciación");
        $gump->set_field_name("num_cuenta_gastos_revaluacion", "Ajuste x Inflación");
        $gump->set_field_name("num_cuenta_neto", "Valor Neto");
        $gump->set_field_name("num_cuenta_rei", "R.E.I");
        $gump->set_field_name("num_cuenta_resultado", "Cta. Resultado");

        ##  validación exitosa
        if($validate === TRUE) {
            ##  crear registro
            $this->Categoria->metModificar($_POST);
            $field = $this->Categoria->metObtener($id);

            ##  devolver registro creado
            $jsondata = [
                'status' => 'modificar',
                'mensaje' => 'Registro actualizado exitosamente',
                'id' => $id,
                'tr' => '
                    <td><label>'.$id.'</label></td>
                    <td><label>'.$field['ind_codigo'].'</label></td>
                    <td><label>'.$field['ind_nombre_categoria'].'</label></td>
                    <td><i class="'.($field['num_estatus']==1?'md md-check':'md md-not-interested').'"></i></td>
                    <td>
                        <div class="btn-group">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Modificar Registro">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'
                            '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Registro!!">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                        </div>
                    </td>
                ',
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
     * Método para eliminar un registro.
     *
     * @return Void
     */
    public function metEliminar()
    {
        $this->Categoria->metEliminar(['pk_num_categoria' => $_POST['id']]);

        $jsondata = [
            'status' => 'OK',
            'mensaje' => 'Registro eliminado exitosamente',
            'id' => $_POST['id'],
        ];
        echo json_encode($jsondata);
        exit();
    }

    /**
     * Insertar una linea en una tabla.
     *
     * @return Void
     */
    public function metInsertar()
    {
        $tr = '
        <tr id="contabilidades'.$_POST['numero'].'">
            <td>
                <div class="col-sm-12" style="margin0px; padding:0px;">
                    <div class="form-group form-group-sm floating-label">
                        <input type="hidden" name="contabilidades_pk_num_categoria_contabilidad[]" value="0">
                        <select name="contabilidades_fk_cbb005_num_contabilidades[]" class="form-control input-sm">
                            <option value="">&nbsp;</option>
                            '.Select::lista('cb_b005_contabilidades','pk_num_contabilidades','ind_descripcion').'
                        </select>
                        <label for="contabilidades_fk_cbb005_num_contabilidades" style="padding:0px;">&nbsp;</label>
                    </div>
                </div>
            </td>
            <td>
                <div class="col-sm-12" style="margin0px; padding:0px;">
                    <div class="form-group form-group-sm floating-label">
                        <input type="text" name="contabilidades_num_depreciacion[]" class="form-control input-sm money" style="text-align:right;">
                        <label for="contabilidades_num_depreciacion" style="padding:0px;">&nbsp;</label>
                    </div>
                </div>
            </td>
            <td>
                <button class="btn ink-reaction btn-raised btn-xs btn-danger" style="margin: 0px 0px 55px -10px;" onclick="$(\'#contabilidades'.$_POST['numero'].'\').remove();">
                    <i class="md md-delete" style="color: #ffffff;"></i>
                </button>
            </td>
        </tr>';

        echo $tr;
    }

    /**
     * Insertar una linea en una tabla.
     *
     * @return Void
     */
    public function metObtenerCuenta()
    {
        $db = new db();
        $sql = "SELECT * FROM cb_b004_plan_cuenta WHERE cod_cuenta = '".$_POST['cod_cuenta']."'";
        $row = $db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        $field = $row->fetch();

        echo $field['pk_num_cuenta'];
    }

    /**
     * Método para cargar vista Selector de Registros.
     *
     * @return Void
     */
    public function metSelector()
    {
        $campos = $_POST;
        $this->atVista->assign('campos', $campos);
        $this->atVista->assign('listado', $this->Categoria->metListar());
        $this->atVista->metRenderizar('selector','modales');
    }
}
