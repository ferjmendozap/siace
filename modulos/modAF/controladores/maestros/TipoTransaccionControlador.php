<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Maestro Tipos de Transacción
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |20-11-2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'Fecha.php';
require_once ROOT.'librerias'.DS.'Number.php';

class TipoTransaccionControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->TipoTransaccion = $this->metCargarModelo('TipoTransaccion','maestros');
    }

    /**
     * Método para cargar vista Listado de Registros.
     *
     * @return Response
     */
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-ui-selectable/jquery-ui',
        );
        $complementoJs = array(
            'mask/jquery.mask',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-ui-selectable/jquery-ui',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado', $this->TipoTransaccion->metListar());
        $this->atVista->metRenderizar('listado');
    }

    /**
     * Método para cargar vista formulario Nuevo Registro.
     *
     * @return Response
     */
    public function metNuevo()
    {
        ##  valores del formulario
        $form = [
            'metodo' => 'crear',
            'pk_num_tipo_transaccion' => null,
            'ind_codigo' => null,
            'ind_nombre_transaccion' => null,
            'fk_cbc003_num_voucher' => null,
            'ind_tipo' => null,
            'num_flag_sistema' => 0,
            'num_estatus' => 1,
            'listado_detalle' => [],
        ];

        ##  vista
        $this->atVista->assign('form', $form);
        $this->atVista->metRenderizar('formulario', 'modales');
    }

    /**
     * Método para cargar vista formulario Editar Registro.
     *
     * @return Response
     */
    public function metEditar()
    {
        ##  valores del formulario
        $form = $this->TipoTransaccion->metObtener($_POST['id']);
        $form['listado_detalle'] = $this->TipoTransaccion->metObtenerCuentas($form['pk_num_tipo_transaccion']);
        $form['metodo'] = 'modificar';

        ##  vista
        $this->atVista->assign('form', $form);
        $this->atVista->metRenderizar('formulario', 'modales');
    }

    /**
     * Método para crear un nuevo registro.
     *
     * @return Void
     */
    public function metCrear()
    {
        $_POST['num_estatus'] = (isset($_POST['num_estatus'])?$_POST['num_estatus']:0);
        $_POST['num_flag_sistema'] = (isset($_POST['num_flag_sistema'])?$_POST['num_flag_sistema']:0);

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'ind_codigo' => 'required|max_len,3|alpha_numeric',
            'ind_nombre_transaccion' => 'required|max_len,150',
            'fk_cbc003_num_voucher' => 'required',
            'ind_tipo' => 'required',
        ]);
        $gump->set_field_name("ind_codigo", "Código");
        $gump->set_field_name("ind_nombre_transaccion", "Nombre");
        $gump->set_field_name("fk_cbc003_num_voucher", "Tipo de Voucher");
        $gump->set_field_name("ind_tipo", "Tipo de Transacción");

        ##  validación exitosa
        if($validate === TRUE) {
            ##  crear registro
            $id = $this->TipoTransaccion->metCrear($_POST);
            $field = $this->TipoTransaccion->metObtener($id);
            ##  devolver registro creado
            $jsondata = [
                'status' => 'crear',
                'mensaje' => 'Registro creado exitosamente',
                'id' => $id,
                'tr' => [
                    '<td><label>'.$id.'</label></td>',
                    '<td><label>'.$field['ind_codigo'].'</label></td>',
                    '<td><label>'.$field['ind_nombre_transaccion'].'</label></td>',
                    '<td><label>'.$field['ind_tipo'].'</label></td>',
                    '<td><i class="'.($field['num_estatus']==1?'md md-check':'md md-not-interested').'"></i></td>',
                    '<td>
                        <div class="btn-group">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Modificar Registro">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'
                            '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Registro!!">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                        </div>
                    </td>',
                ],
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
     * Método para actualizar un registro.
     *
     * @return Void
     */
    public function metModificar()
    {
        $id = $_POST['pk_num_tipo_transaccion'];
        $_POST['num_estatus'] = (isset($_POST['num_estatus'])?$_POST['num_estatus']:0);
        $_POST['num_flag_sistema'] = (isset($_POST['num_flag_sistema'])?$_POST['num_flag_sistema']:0);

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'ind_codigo' => 'required|max_len,3|alpha_numeric',
            ##'ind_nombre_transaccion' => 'required|max_len,150',
            'fk_cbc003_num_voucher' => 'required',
            'ind_tipo' => 'required',
        ]);
        $gump->set_field_name("ind_codigo", "Código");
        $gump->set_field_name("ind_nombre_transaccion", "Nombre");
        $gump->set_field_name("fk_cbc003_num_voucher", "Tipo de Voucher");
        $gump->set_field_name("ind_tipo", "Tipo de Transacción");

        ##  validación exitosa
        if($validate === TRUE) {
            ##  crear registro
            $this->TipoTransaccion->metModificar($_POST);
            $field = $this->TipoTransaccion->metObtener($id);

            ##  devolver registro creado
            $jsondata = [
                'status' => 'modificar',
                'mensaje' => 'Registro actualizado exitosamente',
                'id' => $id,
                'tr' => '
                    <td><label>'.$id.'</label></td>
                    <td><label>'.$field['ind_codigo'].'</label></td>
                    <td><label>'.$field['ind_nombre_transaccion'].'</label></td>
                    <td><label>'.$field['ind_tipo'].'</label></td>
                    <td><i class="'.($field['num_estatus']==1?'md md-check':'md md-not-interested').'"></i></td>
                    <td>
                        <div class="btn-group">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Modificar Registro">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'
                            '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Registro!!">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                        </div>
                    </td>
                ',
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
     * Método para eliminar un registro.
     *
     * @return Void
     */
    public function metEliminar()
    {
        $this->TipoTransaccion->metEliminar(['pk_num_tipo_transaccion' => $_POST['id']]);

        $jsondata = [
            'status' => 'OK',
            'mensaje' => 'Registro eliminado exitosamente',
            'id' => $_POST['id'],
        ];
        echo json_encode($jsondata);
        exit();
    }

    /**
     * Insertar una linea en una tabla.
     *
     * @return Void
     */
    public function metInsertar()
    {
        $tr = '
        <tr id="detalle'.$_POST['numero'].'">
            <td>
                <div class="col-sm-10">
                    <div class="form-group form-group-sm floating-label" id="detalle_num_categoria'.$_POST['numero'].'Error">
                        <input type="hidden" name="detalle_pk_num_tipo_transaccion_cuenta[]" value="">
                        <input type="hidden" name="detalle_fk_afc019_num_categoria[]" id="detalle_fk_afc019_num_categoria'.$_POST['numero'].'">
                        <input type="text" name="detalle_num_categoria[]" id="detalle_num_categoria'.$_POST['numero'].'" class="form-control input-sm" maxlength="15" data-id="'.$_POST['numero'].'" onchange="obtenerCategoria($(this));">
                        <label for="detalle_num_categoria">&nbsp;</label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector" style="margin: 0px 0px -45px -10px;"
                            data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Categorias"
                            onclick="selector($(this), \''.BASE_URL.'modAF/maestros/CategoriaCONTROL/selectorMET\', [\'detalle_fk_afc019_num_categoria'.$_POST['numero'].'\',\'detalle_num_categoria'.$_POST['numero'].'\']);">
                        <i class="md md-search"></i>
                    </button>
                </div>
            </td>
            <td>
                <div class="col-sm-12">
                    <div class="form-group form-group-sm floating-label">
                        <select name="detalle_fk_cbb005_num_contabilidades[]" class="form-control input-sm">
                            <option value="">&nbsp;</option>
                            '.Select::lista('cb_b005_contabilidades','pk_num_contabilidades','ind_descripcion').'
                        </select>
                        <label for="detalle_fk_cbb005_num_contabilidades">&nbsp;</label>
                    </div>
                </div>
            </td>
            <td>
                <div class="col-sm-10">
                    <div class="form-group form-group-sm floating-label" id="detalle_num_cuenta'.$_POST['numero'].'Error">
                        <input type="hidden" name="detalle_fk_cbb004_num_cuenta[]" id="detalle_fk_cbb004_num_cuenta'.$_POST['numero'].'">
                        <input type="text" name="detalle_num_cuenta[]" id="detalle_num_cuenta'.$_POST['numero'].'" class="form-control input-sm" maxlength="15" data-id="'.$_POST['numero'].'" onchange="obtenerCuenta($(this));">
                        <label for="detalle_num_cuenta">&nbsp;</label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                            style="margin: 0px 0px -45px -10px;"
                            data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Plan de Cuentas"
                            onclick="selector($(this), \''.BASE_URL.'modCB/PlanCuentaCONTROL/selectorCuentasContablesMET\', [\'detalle_fk_cbb004_num_cuenta'.$_POST['numero'].'\',\'detalle_num_cuenta'.$_POST['numero'].'\'], \'caso=cuenta\');">
                        <i class="md md-search"></i>
                    </button>
                </div>
            </td>
            <td>
                <div class="col-sm-12">
                    <div class="form-group form-group-sm floating-label">
                        <input type="text" name="detalle_ind_descripcion[]" class="form-control input-sm">
                        <label for="detalle_ind_descripcion">&nbsp;</label>
                    </div>
                </div>
            </td>
            <td>
                <div class="col-sm-12">
                    <div class="form-group form-group-sm floating-label">
                        <select name="detalle_ind_signo[]" class="form-control input-sm">
                            <option value="">&nbsp;</option>
                            '.Select::options('signo').'
                        </select>
                        <label for="detalle_ind_signo">&nbsp;</label>
                    </div>
                </div>
            </td>
            <td>
                <button class="btn ink-reaction btn-raised btn-xs btn-danger" onclick="$(\'#detalle'.$_POST['numero'].'\').remove();">
                    <i class="md md-delete" style="color: #ffffff;"></i>
                </button>
            </td>
        </tr>';



        echo $tr;
    }

    /**
     * Insertar una linea en una tabla.
     *
     * @return Void
     */
    public function metObtenerCategoria()
    {
        $db = new db();
        $sql = "SELECT * FROM af_c019_categoria WHERE ind_codigo = '".$_POST['detalle_num_categoria']."'";
        $row = $db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        $field = $row->fetch();

        echo $field['pk_num_categoria'];
    }

    /**
     * Insertar una linea en una tabla.
     *
     * @return Void
     */
    public function metObtenerCuenta()
    {
        $db = new db();
        $sql = "SELECT * FROM cb_b004_plan_cuenta WHERE cod_cuenta = '".$_POST['detalle_num_cuenta']."'";
        $row = $db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        $field = $row->fetch();

        echo $field['pk_num_cuenta'];
    }
}
