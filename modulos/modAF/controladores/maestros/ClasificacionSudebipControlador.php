<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Maestro Clasificación SUDEBIP
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bol�var                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |15-02-2017               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'Fecha.php';
require_once ROOT.'librerias'.DS.'Number.php';

class ClasificacionSudebipControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->ClasificacionSudebip = $this->metCargarModelo('ClasificacionSudebip','maestros');
    }

    /**
     * M�todo para cargar vista Listado de Registros.
     *
     * @return Response
     */
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-ui-selectable/jquery-ui',
        );
        $complementoJs = array(
            'mask/jquery.mask',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-ui-selectable/jquery-ui',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado', $this->ClasificacionSudebip->metListar());
        $this->atVista->metRenderizar('listado');
    }

    /**
     * M�todo para cargar vista formulario Nuevo Registro.
     *
     * @return Response
     */
    public function metNuevo()
    {
        ##  valores del formulario
        $form = [
            'metodo' => 'crear',
            'pk_num_clasificacion' => null,
            'ind_codigo' => null,
            'ind_nombre' => null,
            'num_nivel' => 1,
            'num_estatus' => 1,
        ];

        ##  vista
        $this->atVista->assign('form', $form);
        $this->atVista->metRenderizar('formulario', 'modales');
    }

    /**
     * M�todo para cargar vista formulario Editar Registro.
     *
     * @return Response
     */
    public function metEditar()
    {
        ##  valores del formulario
        $form = $this->ClasificacionSudebip->metObtener($_POST['id']);
        $form['metodo'] = 'modificar';

        ##  vista
        $this->atVista->assign('form', $form);
        $this->atVista->metRenderizar('formulario', 'modales');
    }

    /**
     * M�todo para crear un nuevo registro.
     *
     * @return Void
     */
    public function metCrear()
    {
        $_POST['num_estatus'] = (isset($_POST['num_estatus'])?$_POST['num_estatus']:0);

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'ind_codigo' => 'required|alpha_dash|max_len,10',
            'ind_nombre' => 'required|alpha_space_full|max_len,255',
            'num_nivel' => 'required|integer|max_len,1',
        ]);
        $gump->set_field_name("ind_codigo", "Codigo");
        $gump->set_field_name("ind_nombre", "Nombre");
        $gump->set_field_name("num_nivel", "Nivel");

        ##  validaci�n exitosa
        if($validate === TRUE) {
            ##  crear registro
            $id = $this->ClasificacionSudebip->metCrear($_POST);
            $field = $this->ClasificacionSudebip->metObtener($id);
            ##  devolver registro creado
            $jsondata = [
                'status' => 'crear',
                'mensaje' => 'Registro creado exitosamente',
                'id' => $id,
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
     * M�todo para actualizar un registro.
     *
     * @return Void
     */
    public function metModificar()
    {
        $_POST['num_estatus'] = (isset($_POST['num_estatus'])?$_POST['num_estatus']:0);
        $id = $_POST['pk_num_clasificacion'];

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'ind_codigo' => 'required|alpha_dash|max_len,10',
            'ind_nombre' => 'required|alpha_space_full|max_len,255',
            'num_nivel' => 'required|integer|max_len,1',
        ]);
        $gump->set_field_name("ind_codigo", "Codigo");
        $gump->set_field_name("ind_nombre", "Nombre");
        $gump->set_field_name("num_nivel", "Nivel");

        ##  validaci�n exitosa
        if($validate === TRUE) {
            ##  crear registro
            $this->ClasificacionSudebip->metModificar($_POST);
            $field = $this->ClasificacionSudebip->metObtener($id);

            ##  devolver registro creado
            $jsondata = [
                'status' => 'modificar',
                'mensaje' => 'Registro actualizado exitosamente',
                'id' => $id,
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
     * M�todo para eliminar un registro.
     *
     * @return Void
     */
    public function metEliminar()
    {
        $this->ClasificacionSudebip->metEliminar(['pk_num_clasificacion' => $_POST['id']]);

        $jsondata = [
            'status' => 'OK',
            'mensaje' => 'Registro eliminado exitosamente',
            'id' => $_POST['id'],
        ];
        echo json_encode($jsondata);
        exit();
    }

    /**
     * M�todo para cargar vista Selector de Registros.
     *
     * @return Void
     */
    public function metSelector()
    {
        $campos = $_POST;
        $this->atVista->assign('campos', $campos);
        $this->atVista->assign('listado', $this->ClasificacionSudebip->metListar());
        $this->atVista->metRenderizar('selector','modales');
    }
}
