<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Activos
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |01-12-2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'Fecha.php';
require_once ROOT.'librerias'.DS.'Number.php';

class ActivosControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->Activos = $this->metCargarModelo('Activos','activos');
        $this->ActivosTransferir = $this->metCargarModelo('ActivosTransferir','activos');
        $this->TipoTransaccion = $this->metCargarModelo('TipoTransaccion','maestros');
    }

    /**
     * M�todo para cargar vista Listado de Registros.
     *
     * @return Response
     */
    public function metIndex($lista='listar')
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-ui-selectable/jquery-ui',
            'bootstrap-datepicker/datepicker',
        );
        $complementoJs = array(
            'mask/jquery.mask',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-ui-selectable/jquery-ui',
            'bootstrap-datepicker/bootstrap-datepicker',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        if ($_POST) $filtro = $_POST;
        else {
            $filtro = [
                'fpk_num_organismo' => '',
                'fpk_num_dependencia' => '',
                'fpk_num_centro_costo' => '',
                'fpk_num_categoria' => '',
                'fpk_num_clasificacion' => '', 'fcod_clasificacion' => '', 'find_nombre_clasificacion' => '',
                'fpk_num_clasificacion' => '', 'find_codigo' => '', 'find_nombre' => '',
                'fpk_num_tipo_transaccion' => '',
                'fpk_num_movimiento' => '',
                'fpk_num_ubicacion' => '',
                'fpk_num_situacion' => '',
                'fnum_tipo_activo' => '',
                'find_estado' => (($lista == 'aprobar')?'PE':''),
            ];
        }
        if ($lista == 'generarActa') {
            $data = [
                'lista' => $lista,
                'titulo' => 'Generar Acta de Responsabilidad',
            ];
        }
        if ($lista == 'generarActaP') {
            $data = [
                'lista' => $lista,
                'titulo' => 'Acta de Responsabilidad por Empleado',
            ];

        }
        elseif ($lista == 'aprobar') {
            $data = [
                'lista' => $lista,
                'titulo' => 'Aprobar Alta de Activo',
            ];
        }
        else {
            $data = [
                'lista' => $lista,
                'titulo' => 'Activos',
            ];
        }
        $this->atVista->assign('data', $data);
        $this->atVista->assign('filtro', $filtro);
        $this->atVista->metRenderizar('listado');

    }

    public function metJsonDataTabla($lista='listar')
    {
       #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
      
              $sql = "SELECT
                    a.*,
                    cc.ind_descripcion_centro_costo,
                    d.pk_num_dependencia,
                    d.ind_dependencia,
                    o.pk_num_organismo,
                    o.ind_descripcion_empresa,
                    c.ind_nombre_categoria,
                    u.ind_nombre_ubicacion,
                    cl.pk_num_clasificacion,
                    CONCAT_WS(' ', p1.ind_nombre1, p1.ind_nombre2, p1.ind_apellido1, p1.ind_apellido2) AS persona_usuario,
                    CONCAT_WS(' ', p2.ind_nombre1, p2.ind_nombre2, p2.ind_apellido1, p2.ind_apellido2) AS persona_responsable
                FROM
                    af_b001_activo a
                    INNER JOIN a023_centro_costo cc ON (cc.pk_num_centro_costo = a.fk_a023_num_centro_costo)
                    INNER JOIN a004_dependencia d ON (d.pk_num_dependencia = cc.fk_a004_num_dependencia)
                    INNER JOIN a001_organismo o ON (o.pk_num_organismo = d.fk_a001_num_organismo)
                    INNER JOIN a003_persona p1 ON (p1.pk_num_persona = a.fk_a003_num_persona_usuario)
                    INNER JOIN a003_persona p2 ON (p2.pk_num_persona = a.fk_a003_num_persona_responsable)
                    INNER JOIN af_c019_categoria c ON (c.pk_num_categoria = a.fk_afc019_num_categoria)
                    INNER JOIN af_c008_clasificacion cl ON (cl.pk_num_clasificacion = a.fk_afc008_num_clasificacion)
                    INNER JOIN af_c005_ubicacion u ON (u.pk_num_ubicacion = a.fk_afc005_num_ubicacion)";
        ##  filtro adicional
        $filtrar = "";
        if ($_GET)
        {
            if (!empty($_GET['fpk_num_organismo']))
                $filtrar .= " AND d.fk_a001_num_organismo = '$_GET[fpk_num_organismo]'";
            if (!empty($_GET['fpk_num_dependencia']))
                $filtrar .= " AND cc.fk_a004_num_dependencia = '$_GET[fpk_num_dependencia]'";
            if (!empty($_GET['fpk_num_centro_costo'])) 
                $filtrar .= " AND a.fk_a023_num_centro_costo = '$_GET[fpk_num_centro_costo]'";
            if (!empty($_GET['fpk_num_categoria'])) 
                $filtrar .= " AND a.fk_afc019_num_categoria = '$_GET[fpk_num_categoria]'";
            if (!empty($_GET['fpk_num_clasificacion'])) 
                $filtrar .= " AND a.fk_afc008_num_clasificacion = '$_GET[fpk_num_clasificacion]'";
            if (!empty($_GET['fpk_num_clasificacion'])) 
                $filtrar .= " AND a.fk_afc032_num_clasificacion_sudebip = '$_GET[fpk_num_clasificacion]'";
            if (!empty($_GET['fpk_num_movimiento'])) 
                $filtrar .= " AND a.fk_afc009_num_movimiento = '$_GET[fpk_num_movimiento]'";
            if (!empty($_GET['fpk_num_ubicacion'])) 
                $filtrar .= " AND a.fk_afc005_num_ubicacion = '$_GET[fpk_num_ubicacion]'";
            if (!empty($_GET['fpk_num_tipo_transaccion'])) 
                $filtrar .= " AND a.fk_afc021_num_tipo_transaccion = '$_GET[fpk_num_tipo_transaccion]'";
            if (!empty($_GET['fpk_num_situacion'])) 
                $filtrar .= " AND a.fk_afc007_num_situacion = '$_GET[fpk_num_situacion]'";
            if (!empty($_GET['fnum_tipo_activo'])) 
                $filtrar .= " AND a.fk_a006_num_tipo_activo = '$_GET[fnum_tipo_activo]'";
            if (!empty($_GET['find_estado'])) 
                $filtrar .= " AND a.ind_estado = '$_GET[find_estado]'";
        }
        else
        {
            if ($lista == 'aprobar') $filtrar .= " AND a.ind_estado = 'PE'";
        }
        if ($filtrar)
        {
            $sql .= " WHERE 1 $filtrar";
        }
        elseif ($busqueda['value'])
        {
            #concateno la busqueda si existe, Nota: esto es obligatorio
            $sql .="
                WHERE
                    ( 
                        a.pk_num_activo LIKE '%$busqueda[value]%'
                        OR a.cod_codigo_interno LIKE '%$busqueda[value]%'
                        OR a.ind_descripcion LIKE '%$busqueda[value]%'
                    )
            ";
        }
        ##  ------------------------------------------------------------------
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = [
            'pk_num_activo',
            'cod_codigo_interno',
            'ind_descripcion',
        ];
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_activo';


        #construyo el listado de botones
        if ($lista == 'listar')
        {
            if (in_array('AF-01-01-01-02',$rol))
            {
             $campos['boton']['modificar'] = '
                        <button idRegistro="'.$clavePrimaria.'" id="modificar"
                                titulo="Editar Activo" title="Editar Activo"
                                class="logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static">
                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                        </button>
                    ';
            }
            $campos['boton']['ver'] = '
                    <button idRegistro="'.$clavePrimaria.'" id="ver"
                            titulo="Ver Activo" title="Ver Activo"
                            class="logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static">
                        <i class="fa fa-eye" style="color: #ffffff;"></i>
                    </button>
                ';
        }
        elseif ($lista == 'aprobar')
        {
            $campos['boton']['aprobar'] = '
                    <button idRegistro="'.$clavePrimaria.'" id="aprobar"
                            titulo="Aprobar Alta de Activo" title="Aprobar Alta de Activo"
                            class="logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static">
                        <i class="md md-done-all" style="color: #ffffff;"></i>
                    </button>
                ';
        }
        elseif ($lista == 'generarActa')
        {
            $campos['boton']['GenerarActaResponsabilidad'] = '
                    <button idRegistro="'.$clavePrimaria.'" id="generarActaResponsabilidad"
                            titulo="Generar Acta de Responsabilidad" title="Generar Acta de Responsabilidad"
                            class="logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static">
                        <i class="fa fa-check" style="color: #ffffff;"></i>
                    </button>

                ';
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);
    }

    /**
     * Método para cargar vista formulario Nuevo Registro.
     *
     * @return Response
     */
    public function metForm($opcion = 'nuevo')
    {
        $fk_a004_num_dependencia = Select::parametros('DEPENDAFDEF');
        $fk_a001_num_organismo = Select::field('a004_dependencia', 'fk_a001_num_organismo', ['pk_num_dependencia' => $fk_a004_num_dependencia]);
        $AFTIPOMOV = Select::miscelaneo('AFTIPOMOV', 'I', 2);
        $AFTIPOACT = Select::miscelaneo('AFTIPOACT', 'I', 2);
        $AFTIPOMEJ = Select::miscelaneo('AFTIPOMEJ', 'NA', 2);
        $AFESTCONS = Select::miscelaneo('AFESTCONS', 'B', 2);
        $fec_ingreso = date('Y-m-d');
        $ind_periodo_inicio_depreciacion = substr(Fecha::sumar_meses($fec_ingreso, 1), 0, 7);

        ##  valores del formulario
        if ($opcion == 'nuevo') {
            $form = [
                'metodo' => 'crear',
                'pk_num_activo_fijo' => null,
                'pk_num_activo' => null,
                'fk_a006_num_naturaleza' => Select::miscelaneo('AFGRUPCAT', 'AN', 2),
                'fk_a006_num_origen' => Select::miscelaneo('AFORIGENAC', 'MA', 2),
                'ind_estado' => 'PE',
                'ind_descripcion' => null,
                'fk_a001_num_organismo' => $fk_a001_num_organismo,
                'cod_codigo_interno' => null,
                'cod_codigo_barra' => null,
                'fk_a004_num_dependencia' => null,
                'fk_afc007_num_situacion' => 2,
                'fk_a006_num_tipo_activo' => $AFTIPOACT,
                'fk_a023_num_centro_costo' => null,
                'fk_a006_num_tipo_mejora' => $AFTIPOMEJ,
                'fk_a006_num_estado_conservacion' => $AFESTCONS,
                'fk_afc019_num_categoria' => null,
                'fk_afc017_num_tipo_vehiculo' => null,
                'fk_afc016_num_tipo_seguro' => null,
                'fk_afc008_num_clasificacion' => null,
                'cod_clasificacion' => null,
                'ind_nombre_clasificacion' => null,                
                'fk_afc032_num_clasificacion_sudebip' => null,
                'ind_codigo' => null,
                'ind_nombre' => null,
                'fk_afb001_num_activo' => null,
                'cod_activo' => null,
                'fk_afc009_num_movimiento' => 1,
                'fk_a003_num_persona_usuario' => null,
                'persona_usuario' => '',
                'fk_afc005_num_ubicacion' => null,
                'fk_a003_num_persona_responsable' => null,
                'persona_responsable' => '',
                'num_flag_mantenimiento' => 0,
                'num_flag_activo_tecnologico' => 0,
                'fk_a006_num_marca' => null,
                'ind_modelo' => null,
                'ind_serie' => null,
                'ind_serie_motor' => null,
                'ind_placa' => null,
                'ind_motor' => null,
                'num_asientos' => null,
                'ind_material' => null,
                'ind_dimensiones' => null,
                'ind_nro_parte' => null,
                'fk_a006_num_color' => null,
                'fk_a008_num_pais' => null,
                'num_anio' => null,
                'fec_ingreso' => $fec_ingreso,
                'ind_periodo_inicio_depreciacion' => $ind_periodo_inicio_depreciacion,
                'ind_periodo_inicio_revaluacion' => null,
                'fk_afc018_num_poliza_seguro' => null,
                'num_unidades' => null,
                'fk_lgb004_num_unidad' => null,
                'fk_cbb001_num_voucher_baja' => null,
                'num_flag_depreciacion' => 0,
                'num_flag_voucher_ingreso' => 0,
                'fk_cbb001_num_voucher_ingreso' => null,
                'voucher_ingreso' => null,
                'voucher_baja' => null,
                'voucher_periodo' => null,
                'num_monto' => null,
                'num_valor_mercado' => null,
                'num_monto_referencial' => null,
                'fec_inventario' => null,
                'fk_a003_num_proveedor' => null,
                'proveedor' => null,
                'fk_cpb002_num_tipo_documento' => null,
                'ind_nro_control' => null,
                'fec_fecha_obligacion' => null,
                'ind_nro_factura' => null,
                'fec_factura' => null,
                'ind_nro_orden_compra' => null,
                'fec_orden_compra' => null,
                'ind_nro_guia_remision' => null,
                'fec_guia_remision' => null,
                'ind_nro_documento_almacen' => null,
                'fec_documento_almacen' => null,
                'ind_observaciones' => null,
                'fk_afc021_num_tipo_transaccion' => null,
                'ind_nombre_transaccion' => null,
                'num_flag_voucher_ingreso' => null,
                'AFTIPOMOV' => $AFTIPOMOV,
                'lista_tipo_transaccion_cuentas' => []
            ];
            $disabled = [
                'editar' => '',
                'ver' => '',
            ];
        }
        elseif ($opcion == 'editar' || $opcion == 'ver' || $opcion == 'aprobar') {
            $form = $this->Activos->metObtener($_POST['id']);
            $form['lista_tipo_transaccion_cuentas'] = $this->TipoTransaccion->metObtenerCuentas($form['fk_afc021_num_tipo_transaccion']);
            $form['pk_num_activo_fijo'] = null;
            ##
            if ($opcion == 'editar') {
                $form['metodo'] = 'modificar';
                $disabled = [
                    'editar' => 'disabled',
                    'ver' => '',
                ];
            }
            elseif ($opcion == 'ver') {
                $form['metodo'] = '';
                $disabled = [
                    'editar' => 'disabled',
                    'ver' => 'disabled',
                ];
            }
            elseif ($opcion == 'aprobar') {
                $form['metodo'] = 'aprobar';
                $disabled = [
                    'editar' => 'disabled',
                    'ver' => 'disabled',
                ];
            }
        }
        elseif ($opcion == 'transferir') {
            $datos_pendiente = $this->ActivosTransferir->metObtenerPendiente($_POST['id']);
            $form = [
                'metodo' => 'crear',
                'pk_num_activo_fijo' => $datos_pendiente['pk_num_activo_fijo'],
                'pk_num_activo' => null,
                'fk_a006_num_naturaleza' => Select::miscelaneo('AFGRUPCAT', 'AN', 2),
                'fk_a006_num_origen' => Select::miscelaneo('AFORIGENAC', 'MA', 2),
                'ind_estado' => 'PE',
                'ind_descripcion' => $datos_pendiente['ind_descripcion'],
                'fk_a001_num_organismo' => $datos_pendiente['fk_a001_num_organismo'],
                'cod_codigo_interno' => null,
                'cod_codigo_barra' => null,
                'fk_a004_num_dependencia' => $datos_pendiente['fk_a004_num_dependencia'],
                'fk_afc007_num_situacion' => null,
                'fk_a006_num_tipo_activo' => null,
                'fk_a023_num_centro_costo' => $datos_pendiente['fk_a023_num_centro_costo'],
                'fk_a006_num_tipo_mejora' => null,
                'fk_a006_num_estado_conservacion' => null,
                'fk_afc019_num_categoria' => null,
                'fk_afc017_num_tipo_vehiculo' => null,
                'fk_afc016_num_tipo_seguro' => null,
                'fk_afc008_num_clasificacion' => null,
                'cod_clasificacion' => null,
                'ind_nombre_clasificacion' => null,
                'fk_afc032_num_clasificacion_sudebip' => null,
                'ind_codigo' => null,
                'ind_nombre' => null,
                'fk_afb001_num_activo' => null,
                'cod_activo' => null,
                'fk_afc009_num_movimiento' => null,
                'fk_a003_num_persona_usuario' => null,
                'persona_usuario' => '',
                'fk_afc005_num_ubicacion' => $datos_pendiente['fk_afc005_num_ubicacion'],
                'fk_a003_num_persona_responsable' => null,
                'persona_responsable' => '',
                'num_flag_mantenimiento' => 0,
                'num_flag_activo_tecnologico' => 0,
                'fk_a006_num_marca' => $datos_pendiente['fk_a006_num_marca'],
                'ind_modelo' => $datos_pendiente['ind_modelo'],
                'ind_serie' => $datos_pendiente['ind_nro_serie'],
                'ind_serie_motor' => null,
                'ind_placa' => null,
                'ind_motor' => null,
                'num_asientos' => null,
                'ind_material' => null,
                'ind_dimensiones' => null,
                'ind_nro_parte' => null,
                'fk_a006_num_color' => $datos_pendiente['fk_a006_num_color'],
                'fk_a008_num_pais' => null,
                'num_anio' => substr($datos_pendiente['fec_ingreso'], 0, 4),
                'fec_ingreso' => $datos_pendiente['fec_ingreso'],
                'ind_periodo_inicio_depreciacion' => substr(Fecha::sumar_meses(date('Y-m-d'), 1), 0, 7),
                'ind_periodo_inicio_revaluacion' => null,
                'fk_afc018_num_poliza_seguro' => null,
                'num_unidades' => null,
                'fk_lgb004_num_unidad' => null,
                'fk_cbb001_num_voucher_baja' => null,
                'num_flag_depreciacion' => 0,
                'num_flag_voucher_ingreso' => 0,
                'fk_cbb001_num_voucher_ingreso' => null,
                'voucher_ingreso' => null,
                'voucher_baja' => null,
                'voucher_periodo' => null,
                'num_monto' => $datos_pendiente['num_monto'],
                'num_valor_mercado' => null,
                'num_monto_referencial' => null,
                'fec_inventario' => null,
                'fk_a003_num_proveedor' => $datos_pendiente['fk_a003_num_persona_proveedor'],
                'proveedor' => $datos_pendiente['proveedor'],
                'fk_cpb002_num_tipo_documento' => $datos_pendiente['fk_cpb002_num_tipo_documento'],
                'ind_nro_control' => $datos_pendiente['ind_nro_control'],
                'fec_fecha_obligacion' => $datos_pendiente['fec_documento'],
                'ind_nro_factura' => $datos_pendiente['ind_nro_factura'],
                'fec_factura' => $datos_pendiente['fec_factura'],
                'ind_nro_orden_compra' => $datos_pendiente['ind_nro_orden_compra'],
                'fec_orden_compra' => $datos_pendiente['fec_orden_compra'],
                'ind_nro_guia_remision' => null,
                'fec_guia_remision' => null,
                'ind_nro_documento_almacen' => $datos_pendiente['ind_nro_documento_almacen'],
                'fec_documento_almacen' => $datos_pendiente['fec_documento_almacen'],
                'ind_observaciones' => $datos_pendiente['ind_observaciones'],
                'fk_afc021_num_tipo_transaccion' => $datos_pendiente['fk_afc021_num_tipo_transaccion'],
                'ind_nombre_transaccion' => null,
                'num_flag_voucher_ingreso' => null,
                'AFTIPOMOV' => $AFTIPOMOV,
                'lista_tipo_transaccion_cuentas' => []
            ];
            $disabled = [
                'editar' => '',
                'ver' => '',
            ];
        }

        ##  vista
        $this->atVista->assign('form', $form);
        $this->atVista->assign('disabled', $disabled);
        $this->atVista->metRenderizar('formulario', 'modales');
    }

    /**
     * Método para crear un nuevo registro.
     *
     * @return Void
     */
    public function metCrear()
    {
        $_POST['num_flag_activo_tecnologico'] = (isset($_POST['num_flag_activo_tecnologico'])?$_POST['num_flag_activo_tecnologico']:0);
        $_POST['num_flag_mantenimiento'] = (isset($_POST['num_flag_mantenimiento'])?$_POST['num_flag_mantenimiento']:0);
        $_POST['num_flag_voucher_ingreso'] = (isset($_POST['num_flag_voucher_ingreso'])?$_POST['num_flag_voucher_ingreso']:0);
        $_POST['num_flag_depreciacion'] = (isset($_POST['num_flag_depreciacion'])?$_POST['num_flag_depreciacion']:0);
        $_POST['fec_inventario'] = Fecha::formatFecha($_POST['fec_inventario']);
        $_POST['fec_fecha_obligacion'] = Fecha::formatFecha($_POST['fec_fecha_obligacion']);
        $_POST['fec_factura'] = Fecha::formatFecha($_POST['fec_factura']);
        $_POST['fec_orden_compra'] = Fecha::formatFecha($_POST['fec_orden_compra']);
        $_POST['fec_guia_remision'] = Fecha::formatFecha($_POST['fec_guia_remision']);
        $_POST['fec_documento_almacen'] = Fecha::formatFecha($_POST['fec_documento_almacen']);
        $_POST['fec_ingreso'] = Fecha::formatFecha($_POST['fec_ingreso']);
        $_POST['num_monto'] = Number::format($_POST['num_monto']);
        $_POST['num_valor_mercado'] = Number::format($_POST['num_valor_mercado']);
        $_POST['num_monto_referencial'] = Number::format($_POST['num_monto_referencial']);

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'ind_descripcion' => 'required',
            'fk_a001_num_organismo' => 'required',
            'fk_a004_num_dependencia' => 'required',
            'fk_a023_num_centro_costo' => 'required',
            'fk_afc019_num_categoria' => 'required',
            'fk_afc008_num_clasificacion' => 'required',
            'fk_afc032_num_clasificacion_sudebip' => 'required',
            'fk_afc009_num_movimiento' => 'required',
            'fk_afc005_num_ubicacion' => 'required',
            //'cod_codigo_interno' => 'required',
            'fk_afc007_num_situacion' => 'required',
            'fk_a006_num_tipo_activo' => 'required',
            'fk_a006_num_tipo_mejora' => 'required',
            'fk_a006_num_estado_conservacion' => 'required',
            'fk_a003_num_persona_usuario' => 'required',
            'fk_a003_num_persona_responsable' => 'required',
            'fec_ingreso' => 'required|date',
            'fk_afc021_num_tipo_transaccion' => 'required',
        ]);
        $gump->set_field_name("ind_descripcion", "Descripcion");
        $gump->set_field_name("fk_a001_num_organismo", "Organismo");
        $gump->set_field_name("fk_a004_num_dependencia", "Dependencia");
        $gump->set_field_name("fk_a023_num_centro_costo", "Centro de Costo");
        $gump->set_field_name("fk_afc019_num_categoria", "Categoria");
        $gump->set_field_name("fk_afc008_num_clasificacion", "Clasificacion");
        $gump->set_field_name("fk_afc032_num_clasificacion_sudebip", "ClasificacionSudebip");
        $gump->set_field_name("fk_afc009_num_movimiento", "Tipo de Movimiento");
        $gump->set_field_name("fk_afc005_num_ubicacion", "Ubicacion");
        $gump->set_field_name("fk_afc021_num_tipo_transaccion", "Tipo de Transaccion");
        $gump->set_field_name("fk_afc021_num_tipo_transaccion", "TipoTransaccion");
        $gump->set_field_name("cod_codigo_interno", "Codigo Interno");
        $gump->set_field_name("fk_afc007_num_situacion", "Situacion");
        $gump->set_field_name("fk_a006_num_tipo_activo", "Tipo de Activo");
        $gump->set_field_name("fk_a006_num_tipo_mejora", "Tipo de Mejora");
        $gump->set_field_name("fk_a006_num_estado_conservacion", "Estado Conservacion");
        $gump->set_field_name("fk_a003_num_persona_usuario", "Empleado Usuario");
        $gump->set_field_name("fk_a003_num_persona_responsable", "Empleado Responsable");
        $gump->set_field_name("fec_ingreso", "Fecha Ingreso");

        ##  validación exitosa
        if($validate === TRUE) {

            ##  crear registro
            $id = $this->Activos->metCrear($_POST);
            $field = $this->Activos->metObtener($id);

            ## Parametro para validar modificación de Usuario y Responsable de Activo en el Acta de Responsabilidad
            $perUsuario=$_POST['fk_a003_num_persona_usuario'];              
            $this->Activos->metValidarCodigoAct($perUsuario);

            ##  devolver registro creado
            $jsondata = [
                'status' => 'crear',
                'mensaje' => 'Registro creado exitosamente',
                'id' => $id,
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
     * M�todo para actualizar un registro.
     *
     * @return Void
     */
    public function metModificar()
    {
        
        $_POST['num_flag_activo_tecnologico'] = (isset($_POST['num_flag_activo_tecnologico'])?$_POST['num_flag_activo_tecnologico']:0);
        $_POST['num_flag_mantenimiento'] = (isset($_POST['num_flag_mantenimiento'])?$_POST['num_flag_mantenimiento']:0);
        $_POST['num_flag_voucher_ingreso'] = (isset($_POST['num_flag_voucher_ingreso'])?$_POST['num_flag_voucher_ingreso']:0);
        $_POST['num_flag_depreciacion'] = (isset($_POST['num_flag_depreciacion'])?$_POST['num_flag_depreciacion']:0);
        $_POST['fec_inventario'] = Fecha::formatFecha($_POST['fec_inventario']);
        $_POST['fec_fecha_obligacion'] = Fecha::formatFecha($_POST['fec_fecha_obligacion']);
        $_POST['fec_factura'] = Fecha::formatFecha($_POST['fec_factura']);
        $_POST['fec_orden_compra'] = Fecha::formatFecha($_POST['fec_orden_compra']);
        $_POST['fec_guia_remision'] = Fecha::formatFecha($_POST['fec_guia_remision']);
        $_POST['fec_documento_almacen'] = Fecha::formatFecha($_POST['fec_documento_almacen']);
        $_POST['fec_ingreso'] = Fecha::formatFecha($_POST['fec_ingreso']);
        $_POST['num_monto'] = Number::format($_POST['num_monto']);
        $_POST['num_valor_mercado'] = Number::format($_POST['num_valor_mercado']);
        $_POST['num_monto_referencial'] = Number::format($_POST['num_monto_referencial']);

        ## valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'ind_descripcion' => 'required',
            'fk_a001_num_organismo' => 'required',
            'fk_a004_num_dependencia' => 'required',
            'fk_a023_num_centro_costo' => 'required',
            'fk_afc019_num_categoria' => 'required',
            'fk_afc008_num_clasificacion' => 'required',
            'fk_afc032_num_clasificacion_sudebip' => 'required',
            'fk_afc009_num_movimiento' => 'required',
            'fk_afc005_num_ubicacion' => 'required',
            //'cod_codigo_interno' => 'required',
            'fk_afc007_num_situacion' => 'required',
            'fk_a006_num_tipo_activo' => 'required',
            'fk_a006_num_tipo_mejora' => 'required',
            'fk_a006_num_estado_conservacion' => 'required',
            'fk_a003_num_persona_usuario' => 'required',
            'fk_a003_num_persona_responsable' => 'required',
            'fec_ingreso' => 'required|date',            
            'fk_afc021_num_tipo_transaccion' => 'required',
        ]);
        $gump->set_field_name("ind_descripcion", "Descripcion");
        $gump->set_field_name("fk_a001_num_organismo", "Organismo");
        $gump->set_field_name("fk_a004_num_dependencia", "Dependencia");
        $gump->set_field_name("fk_a023_num_centro_costo", "Centro de Costo");
        $gump->set_field_name("fk_afc019_num_categoria", "Categoria");
        $gump->set_field_name("fk_afc008_num_clasificacion", "Clasificacion");
        $gump->set_field_name("fk_afc032_num_clasificacion_sudebip", "ClasificacionSudebip");
        $gump->set_field_name("fk_afc009_num_movimiento", "Tipo de Movimiento");
        $gump->set_field_name("fk_afc005_num_ubicacion", "Ubicacion");
        $gump->set_field_name("fk_afc021_num_tipo_transaccion", "Tipo de Transaccion");
        $gump->set_field_name("cod_codigo_interno", "Codigo Interno");
        $gump->set_field_name("fk_afc007_num_situacion", "Situacion");
        $gump->set_field_name("fk_a006_num_tipo_activo", "Tipo de Activo");
        $gump->set_field_name("fk_a006_num_tipo_mejora", "Tipo de Mejora");
        $gump->set_field_name("fk_a006_num_estado_conservacion", "Estado Conservacion");
        $gump->set_field_name("fk_a003_num_persona_usuario", "Empleado Usuario");
        $gump->set_field_name("fk_a003_num_persona_responsable", "Empleado Responsable");
        $gump->set_field_name("fec_ingreso", "Fecha Ingreso");

        ##  validaci�n exitosa
        if($validate === TRUE) {

            ## Parametro para validar modificación de Usuario y Responsable de Activo
            $id = $_POST['pk_num_activo'];            
            $numPerUsu= $this->Activos->metObtenerNumPersonaActa($id);
      
            $perUsuario=$_POST['fk_a003_num_persona_usuario'];
            $perResponsable=$_POST['fk_a003_num_persona_responsable'];
            
           if(($numPerUsu['numPerUsuaA']!=$perUsuario) || ($numPerUsu['numPerRespA']!=$perResponsable)){
              
              $this->Activos->metValidarCodigoAct($perUsuario);         
              $this->Activos->metValidarCodigoAct($numPerUsu['numPerUsuaA']);
            }

            ##  crear registro            
            $this->Activos->metModificar($_POST);
            $field = $this->Activos->metObtener($id);

            ##  devolver registro creado
            $jsondata = [
                'status' => 'modificar',
                'mensaje' => 'Registro Modificado satisfactoriamente',
                'id' => $id,
            ];

        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
     * M�todo para actualizar un registro.
     *
     * @return Void
     */
    public function metAprobar()
    {
        ##  actualizar registro
        $id = $_POST['pk_num_activo'];
        $this->Activos->metAprobar($_POST);
        $field = $this->Activos->metObtener($id);

        ##  devolver registro creado
        die(json_encode([
            'status' => 'aprobar',
            'mensaje' => 'Activo aprobado exitosamente',
            'id' => $id,
        ]));
    }

     /**
     * Método para cargar vista Generar Acta.
     *
     * @return Response
     */
    public function metGenerarActaResponsabilidad()
    {
        ##  valores del formulario
        $id = $this->Activos->metGenerarActaResponsabilidad($_POST['id']);

        ##  vista
        $this->atVista->assign('data', ['id'=>$id]);
        $this->atVista->metRenderizar('reporteActaResponsabilidad', 'modales');
    }

    /**
     * Método para cargar vista Generar Acta de Responsabilidad por Funcionario.
     *
     * @return Response
     */
    public function metGenerarActaResponsabilidadPF()
    {
        ##  valores del formulario  
        $id = $this->Activos->metGenerarActaResponsabilidadMPF($_POST['id']);

        $id = $_POST['id'];
        ##  vista
        $this->atVista->assign('data', ['id'=>$id]);
        $this->atVista->metRenderizar('reporteActaResponsabilidadPF', 'modales');
    }

    /**
     * Método para CARGAR AVISO antes de Generar Acta de Responsabilidad por Funcionario

       en el caso de que el Funcionario NO CUENTE CON BIENES O ACTIVOS ASIGNADOS.
     *
     * @return Response
     */
     public function metGenerarActaResponsabilidadCPF()
     {
        ##  valores del formulario
         $id = $this->Activos->metValidarActa($_POST['id']);

       if($id>0){
            $arrayPost =  array('valor' => 1 , 'mensaje' => 'ESTE FUNCIONARIO TIENE BIENES ASIGANDOS');

        }else{
            $arrayPost =  array('valor' => 0 , 'mensaje' => 'ESTE FUNCIONARIO NO TIENE BIENES ASIGANDOS');
        }
      echo json_encode($arrayPost);
     }

    /**
     * Método para cargar vista Generar Acta.
     *
     * @return Response
     */
    public function metMostrarActaResponsabilidad()
    {
        ## vista
        $this->atVista->assign('data', ['id'=>$_POST['id']]);
        $this->atVista->metRenderizar('reporteActaResponsabilidad', 'modales');
    }

    /**
     * Método para imprimir PDF Transferencia de Activos.
     *
     * @return Response
     */
    public function metImprimirActaResponsabilidad($id)
    {
       $db = new db();
        ##  ----------------------------------
        $this->metObtenerLibreria('plantillaPDF','modAF');
        $parametros = Select::parametros();
        ##  datos de la dependencia default
        $sql = "SELECT
                    d.ind_dependencia,
                    o.ind_descripcion_empresa,
                    CONCAT_WS(' ', p.ind_nombre1, p.ind_nombre2, p.ind_apellido1, p.ind_apellido2) AS persona
                FROM
                    a004_dependencia d
                    INNER JOIN a001_organismo o ON (o.pk_num_organismo = d.fk_a001_num_organismo)
                    INNER JOIN a003_persona p ON (p.pk_num_persona = d.fk_a003_num_persona_responsable)
                WHERE d.pk_num_dependencia = '$parametros[DEPENDAFDEF]'";
        $query_dependencia = $db->query($sql);
        $query_dependencia->setFetchMode(PDO::FETCH_ASSOC);
        $dependencia = $query_dependencia->fetch();
        ##  ----------------------------------
        ##  Obtengo la data a imprimir
        $field = $this->Activos->metObtenerActaResponsabilidad($id);
        list($Anio, $Mes, $Dia) = explode('-', $field['fec_fecha']);
        ##  Creo el Reporte
        $pdf = new pdfDefaultP('P','mm','Letter');
        $pdf->Organismo = $dependencia['ind_descripcion_empresa'];
        $pdf->Dependencia = $dependencia['ind_dependencia'];
        $pdf->Titulo = 'ACTA DE RESPONSABILIDAD DE USO';
        $pdf->Head = "
            \$this->SetFont('Arial','B',8);
            \$this->SetXY(165,22); \$this->Cell(25,5,utf8_decode('Acta: " . $field['ind_nro_acta'] ."-" . $field['num_anio'] . "'),0,1,'L');
            \$this->SetY(45);
        ";
        $pdf->Foot = "
            \$this->SetFont('Arial','B',8);
            \$this->SetXY(15,265); \$this->Cell(25,5,utf8_decode('REF.: FOR-DSG-003'),0,1,'L');
        ";
        ##
        $pdf->AliasNbPages();
        $pdf->AddPage();
        ##
        $Parrafo1 = "El (la) Suscrito $field[persona_responsable], titular de la Cédula de Identidad N°: $field[cedula_responsable], quien desempeña el cargo de $field[cargo_responsable] en la $field[ind_descripcion_empresa] y como Responsable Patrimonial Primario, hace costar que el dia de hoy, " . Fecha::dia_semana_nombre($field['fec_fecha']) . " del mes de " . Fecha::mes_nombre($Mes) . " del año " . $Anio . ", se asignan en calidad de uso y custodia al funcionario $field[persona_usuario], titular de la Cédula de Identidad N°: $field[cedula_usuario], el bien o bienes que a continuación se especifican:                                                                             ";
        $pdf->SetFont('Arial','',12);
        $pdf->MultiCell(195,7,utf8_decode($Parrafo1),0,'FJ');
        $pdf->Ln(3);

        $pdf->SetFillColor(240,240,240);
        $pdf->SetDrawColor(0,0,0);
        $pdf->Ln(1);
        $pdf->SetFont('Arial','B',7);
        $pdf->SetWidths(array(25,15,20,75,35,25));
        $pdf->SetAligns(array('C','C','C','L','C','C'));
        $pdf->Row(array(utf8_decode('CLASIFICACIÓN'),
                        utf8_decode('CANTIDAD'),
                        utf8_decode('N° IDENTIFICACIÓN'),
                        utf8_decode('DESCRIPCIÓN'),
                        utf8_decode('MARCA'),
                        utf8_decode('MODELO')
                ));
        $pdf->SetFillColor(255,255,255);
        $pdf->SetDrawColor(0,0,0);
        $pdf->SetFont('Arial','',7);
        $pdf->Row(array(utf8_decode($field['cod_clasificacion']),
                        1,
                        utf8_decode($field['cod_activo']),
                        utf8_decode($field['desc_activo']),
                        utf8_decode($field['ind_modelo']),
                        utf8_decode($field['marca'])
                ));
        $pdf->Ln(3);

        $Parrafo2 = "Yo, $field[persona_usuario] como Responsable Patromonial de Uso declaro: \"Recibido el bien o bienes especificados en la presente acta, en buenas condiciones de uso y conservación, para ser utilizado(s) por mi persona, dentro de las instalaciones de la $field[ind_descripcion_empresa] o fuera de su recinto, cuando sea requerido y autorizado para ello, únicamente para el desempeño de mis funciones como $field[cargo_usuario] adscrito a\" $field[ind_dependencia]. En virtud de la presente asignación, en mi condición de Responsable Patrimonial de Uso y custodio de los bienes descritos como propiedad de este ente de Control Fiscal, me comprometo a utilizarlos de manera responsable y tomar las medidas de resguardo necesarias para evitar el deteriorio acelerado, hurto, robo, extravío o perdida de los mismos. Queda entendido que cualquier daño material que pueda ocurrirle al referido bien mueble; con ocasión de negligencia impericia u omisión en el uso del mismo; queda sujeta a las sanciones administrativas previstas en artículo 91 numeral 2 de la Ley Orgánica de la Contraloría General de la República y del Sistema Nacional de Control Fiscal, Disciplinarias prevista en el artículo 33 numeral 7 de la Ley de Estatuto de Función Pública y Penal prevista en el artículo 53 de la Ley Contra la Corrupción, salvo aquellos daños naturales u hechos fortuitos que se presenten, lo cual deberá ser notificado por escrito ante la $dependencia[ind_dependencia] de ésta Contraloría. Es todo, terminó se leyó y conformes firman.                                                                                  ";
        $pdf->SetFont('Arial','',12);
        $pdf->MultiCell(195,7,utf8_decode($Parrafo2),0,'FJ');

        ##  firmas
        if ($pdf->GetY() > 250) $pdf->AddPage();
        $pdf->SetY(250);
        $pdf->SetDrawColor(0,0,0);
        $pdf->Rect(29, $pdf->GetY(), 60, 0.1, "D");
        $pdf->Rect(126, $pdf->GetY(), 60, 0.1, "D");
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(98,5,utf8_decode($field['persona_responsable']),0,0,'C');
        $pdf->Cell(98,5,utf8_decode($field['persona_usuario']),0,0,'C');
        ##
        $pdf->Output();
    }
     /**
     * Método para imprimir PDF de Actas por Funcionario Transferencia de Activos en GENERAR ACTA.
     *
     * @return Response
     */
    public function metImprimirActaResponsabilidadPF($id)
    {
        $db = new db();
        ##  ----------------------------------
        $this->metObtenerLibreria('plantillaPDF','modAF');

        $persona = $this->Activos->metGetPersonaActa($id);
        $desCargPerUs = $this->Activos->metGetPersonaCargoActa($id);
        $CodDepPers = $this->Activos->metGetCodPersonas($id);   

        $parametros = Select::parametros();
        ##  datos de la dependencia default
        $sql = "SELECT
                    d.ind_dependencia,
                    o.ind_descripcion_empresa,
                    CONCAT_WS(' ', p.ind_nombre1, p.ind_nombre2, p.ind_apellido1, p.ind_apellido2) AS persona
                FROM
                    a004_dependencia d
                    INNER JOIN a001_organismo o ON (o.pk_num_organismo = d.fk_a001_num_organismo)
                    INNER JOIN a003_persona p ON (p.pk_num_persona = d.fk_a003_num_persona_responsable)
                WHERE d.pk_num_dependencia = '$parametros[DEPENDAFDEF]'";
        $query_dependencia = $db->query($sql);
        $query_dependencia->setFetchMode(PDO::FETCH_ASSOC);
        $dependencia = $query_dependencia->fetch();
        ##  ----------------------------------
       
        ##  Obtengo la data a imprimir
       $activo = $this->Activos->metObtenerActivoEmpleado($id);
       $field = $this->Activos->metObtenerActaResponsabilidadPF($id);
       
        ##  Creo el Reporte
        $pdf = new pdfDefaultP('P','mm','Letter');
        $pdf->Organismo = $dependencia['ind_descripcion_empresa'];
        $pdf->Dependencia = 'DIRECCIÓN DE ADMINISTRACIÓN';  ## $dependencia['ind_dependencia'];
        $pdf->Titulo = 'ACTA DE RESPONSABILIDAD DE USO POR FUNCIONARIO';
       
         $pdf->Head = "
            \$this->SetFont('Arial','B',8);
           \$this->SetXY(165,22); \$this->Cell(25,5,utf8_decode('Acta: ".$CodDepPers['codigoDep']."-".$CodDepPers['nro_acta']."-" . date('Y') . "'),0,1,'L');
            \$this->SetY(45);
        ";
        ##
        $pdf->AliasNbPages();
        $pdf->AddPage();
        ##
        
        $Parrafo1 = "En el día de hoy, " . Fecha::dia_semana_nombre(date('Y-m-d')) . " ".date('d')." del mes de ".Fecha::mes_nombre(date('m'))." del año ".date('Y').", reunidos en las instalaciones donde funciona la Contraloría del Estado Sucre, ubicada en la Av. Arismendi, Edificio Palacio Legislativo, Cumaná, Municipio Sucre del Estado Sucre, los ciudadanos: $persona[persona_responsable], titular de la Cédula de Identidad N°: $persona[cedula_responsable], Responsable Patrimonial Primario y $persona[persona_usuario], titular de la Cédula de Identidad N°: $persona[cedula_usuario], Responsable Patrimonial de Uso, quien desempeña el cargo de: $desCargPerUs[cargo_usuario] con el único objeto de hacerle entrega al último de los mencionados; en calidad de uso, guarda y custodia los bienes muebles que a continuación se especifican:                                 ";
               
        $pdf->heightLine = 5;
        $pdf->Head = "";
        $pdf->SetFont('Arial','',12);
        $pdf->MultiCell(195,7,utf8_decode($Parrafo1),0,'FJ');
        $pdf->Ln(1);
       
        $pdf->SetFillColor(240,240,240);
        $pdf->SetDrawColor(0,0,0);
        $pdf->SetFont('Arial','B',7);
        $pdf->SetWidths(array(18,75,35,35,30));
        $pdf->SetAligns(array('C','C','C','C','C'));
        $pdf->Row(array(utf8_decode('CANTIDAD'),
                        utf8_decode('BIEN (ES) MUEBLE (ES)'),
                        utf8_decode('SERIAL (ES) Nº'),
                        utf8_decode('Nº BIEN MUEBLE'),
                        utf8_decode('ESTADO DEL BIEN')
                ));


        $pdf->SetFillColor(255,255,255);
        $pdf->SetDrawColor(0,0,0);
        $pdf->SetFont('Arial','',7);

        //--------Tabla Activos por Funcionarios----//  
        foreach ($activo as $AcF) {
        $pdf->Row(array(1,
                      utf8_decode($AcF['desc_activo']),
                        utf8_decode($AcF['serie']),
                        utf8_decode($AcF['cod_activo']),
                        utf8_decode($AcF['estado'])
               )); 
        }
        $pdf->Ln(3);

        $Parrafo2 = "Los mismos son propiedad de la Contraloría del estado Sucre, tal como se desprende del registro de Bienes e Inventario llevado por este Órgano de Control Fiscal y quedarán bajo su responsabilidad; siendo éste responsable de vigilar, conservar y salvaguardar el bien mueble entregado mediante la presente Acta, conforme a lo establecido en el artículo 55 de la Ley Orgánica de Bienes Públicos. Queda entendido que cualquier daño material que pueda ocurrir a los referidos bienes, con ocasión de negligencia, imprudencia, impericia, falta u omisión en el uso o disposición del mismo, queda sujeto a lo establecido en el artículo 103 de la mencionada Ley; así como también, a lo previsto en el Artículo 33 numeral 7 de la Ley del Estatuto de la Función Pública, Artículo 91 numeral 2 de la Ley Orgánica de la Contraloría General de la República y del Sistema Nacional de Control Fiscal y Artículo 21 de la Ley  Contra la Corrupción; y por tanto, a las sanciones disciplinarias, administrativas y penales que corresponda, salvo por aquellos daños naturales u hechos fortuitos que se presenten, lo cual deberá ser notificado por escrito ante la Dirección de Administración de ésta Contraloría. Es todo, terminó, se leyó y conformes firman.                                                                                  ";

        $pdf->SetFont('Arial','',12);
        $pdf->MultiCell(195,7,utf8_decode($Parrafo2),0,'FJ');

        ##  firmas
        if ($pdf->GetY() > 250) $pdf->AddPage();
        $pdf->SetY(250);
        $pdf->SetDrawColor(0,0,0);
        $pdf->Rect(29, $pdf->GetY(), 60, 0.1, "D");
        $pdf->Rect(126, $pdf->GetY(), 60, 0.1, "D");
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(98,5,utf8_decode($persona['persona_responsable']),0,0,'C');
        #$pdf->Cell(98,5,utf8_decode($codActivo['codigoAct']),0,0,'C');
        $pdf->Cell(98,5,utf8_decode($persona['persona_usuario']),0,0,'C');
        ##
        $pdf->Output();
    }


    /**
     * Método para mostrar el sector de registros.
     *
     * @return Void
     */
    public function metSelector()
    {
        $campos = $_POST;
        $this->atVista->assign('campos', $campos);
        $this->atVista->assign('listado', $this->Activos->metListar());
        $this->atVista->metRenderizar('selector','modales');
    }

    /**
     * Método para obtener las cuentas del tipo de transacción seleccionado.
     *
     * @return Void
     */
    public function metTipoTransaccionCuentas()
    {
        $lista_tipo_transaccion_cuentas = $this->TipoTransaccion->metObtenerCuentas($_POST['fk_afc021_num_tipo_transaccion']);
        foreach ($lista_tipo_transaccion_cuentas as $f) 
        {
            echo '
            <tr>
                <td>
                    <div class="col-sm-12">
                        '.$f['nombre_contabilidad'].'
                    </div>
                </td>
                <td>
                    <div class="col-sm-12">
                        '.$f['num_cuenta'].'
                    </div>
                </td>
                <td>
                    <div class="col-sm-12">
                        '.$f['ind_descripcion'].'
                    </div>
                </td>
                <td>
                    <div class="col-sm-12">
                        '.$f['ind_signo'].'
                    </div>
                </td>
            </tr>';
        }
    }

    /**
     * Método para obtener las cuentas del tipo de transacción seleccionado.
     *
     * @return Void
     */
    public function metTipoTransaccionCuentasContabilidad()
    {
        $lista_tipo_transaccion_cuentas = $this->TipoTransaccion->metObtenerCuentasContabilidad($_POST['fk_afc021_num_tipo_transaccion']);
        foreach ($lista_tipo_transaccion_cuentas as $f) 
        {
            echo '
            <tr>
                <td>
                    <div class="col-sm-12">
                        '.$f['nombre_contabilidad'].'
                    </div>
                </td>
                <td>
                    <div class="col-sm-12">
                        '.$f['num_cuenta'].'
                    </div>
                </td>
                <td>
                    <div class="col-sm-12">
                        '.$f['ind_descripcion'].'
                    </div>
                </td>
                <td>
                    <div class="col-sm-12">
                        '.$f['ind_signo'].'
                    </div>
                </td>
            </tr>';
        }
    }

    /**
     * Método para obtener el periodo de depreciación a partir de la fecha de ingreso.
     *
     * @return Void
     */
    public function metPeriodoDepreciacion()
    {
        $fec_ingreso = Fecha::formatFecha($_POST['fec_ingreso']);
        $ind_periodo_inicio_depreciacion = Fecha::sumar_meses($fec_ingreso, 1);

        die(json_encode([
            'ind_periodo_inicio_depreciacion' => substr($ind_periodo_inicio_depreciacion, 0, 7),
        ]));
    }

    public function metPersona()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-ui-selectable/jquery-ui',
            'bootstrap-datepicker/datepicker',
        );
        $complementoJs = array(
            'mask/jquery.mask',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-ui-selectable/jquery-ui',
            'bootstrap-datepicker/bootstrap-datepicker',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        
        $this->atVista->assign('lista', $this->Activos->metGetPersonas());        
        $this->atVista->metRenderizar('actaListado');        
    }

    public function metFecha()
    {
        if ($lista == 'generarActaP') {
            $data = [
                'lista' => $lista,
                'titulo' => 'Acta de Responsabilidad por Empleado',
            ];

        }
        elseif ($lista == 'aprobar') {
            $data = [
                'lista' => $lista,
                'titulo' => 'Aprobar Alta de Activo',
            ];
        }
        else {
            $data = [
                'lista' => $lista,
                'titulo' => 'Activos',
            ];
        }
    }
}
