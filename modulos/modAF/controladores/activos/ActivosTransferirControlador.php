<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Activos
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |01-12-2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'Fecha.php';

class ActivosTransferirControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->ActivosTransferir = $this->metCargarModelo('ActivosTransferir','activos');
        $this->Activos = $this->metCargarModelo('Activos','activos');
        $this->TipoTransaccion = $this->metCargarModelo('TipoTransaccion','maestros');
    }

    /**
     * Método para cargar vista Listado de Registros.
     *
     * @return Response
     */
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-ui-selectable/jquery-ui',
        );
        $complementoJs = array(
            'mask/jquery.mask',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-ui-selectable/jquery-ui',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        if ($_POST) $filtro = $_POST;
        else {
            $filtro = [
                'fpk_num_organismo' => '',
                'fpk_num_dependencia' => '',
                'fpk_num_centro_costo' => '',
                'fpk_num_categoria' => '',
                'fpk_num_clasificacion' => '', 'fcod_clasificacion' => '', 'find_nombre_clasificacion' => '',
                'fpk_num_movimiento' => '',
                'fpk_num_ubicacion' => '',
                'fpk_num_situacion' => '',
                'fnum_tipo_activo' => '',
            ];
        }
        $this->atVista->assign('filtro', $filtro);
        $this->atVista->assign('listado_pendientes', $this->ActivosTransferir->metListar('PE',$filtro));
        $this->atVista->assign('listado_transferidos', $this->ActivosTransferir->metListar('TR',$filtro));
        $this->atVista->metRenderizar('listado');
    }
}
