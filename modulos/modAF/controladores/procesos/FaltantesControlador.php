<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Faltantes de Activos
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |20-07-2016               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'Fecha.php';
require_once ROOT.'librerias'.DS.'Number.php';

class FaltantesControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->Faltantes = $this->metCargarModelo('Faltantes','procesos');
        $this->Activos = $this->metCargarModelo('Activos','activos');
        $this->TipoMovimiento = $this->metCargarModelo('TipoMovimiento','maestros');
    }

    /**
     * Método para cargar vista Listado de Registros.
     *
     * @return Response
     */
    public function metIndex($lista='listar')
    {
        //die(print_r($_POST));
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-ui-selectable/jquery-ui',
            'bootstrap-datepicker/datepicker',
        );
        $complementoJs = array(
            'mask/jquery.mask',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-ui-selectable/jquery-ui',
            'bootstrap-datepicker/bootstrap-datepicker',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        if ($_POST) $filtro = $_POST;
        else 
        {
            $find_estado = '';
            $find_situacion = '';
            if ($lista == 'revisar') $find_estado = 'PR';
            elseif ($lista == 'aprobar') $find_estado = 'RV';
            elseif ($lista == 'tramitar') {
                $find_estado = 'AP';
                $find_situacion = 'EI';
            }
            $filtro = [
                'fnum_organismo' => '',
                'fnum_dependencia' => '',
                'fnum_centro_costo' => '',
                'find_estado' => $find_estado,
                'find_situacion' => $find_situacion,
                'ffec_fechad' => '',
                'ffec_fechah' => '',
            ];
        }
        if ($lista == 'listar') 
        {
            $data = [
                'lista' => $lista,
                'titulo' => 'Listado',
            ];
        }
        elseif ($lista == 'revisar') 
        {
            $data = [
                'lista' => $lista,
                'titulo' => 'Revisar',
            ];
        }
        elseif ($lista == 'aprobar') 
        {
            $data = [
                'lista' => $lista,
                'titulo' => 'Aprobar',
            ];
        }
        elseif ($lista == 'tramitar')
        {
            $data = [
                'lista' => $lista,
                'titulo' => 'Tramitar Suceso',
            ];
        }
        $this->atVista->assign('data', $data);
        $this->atVista->assign('filtro', $filtro);
        $this->atVista->assign('listado', $this->Faltantes->metListar($filtro));
        $this->atVista->metRenderizar('listado');
    }

    /**
     * Método para cargar vista formulario Nuevo Registro.
     *
     * @return Response
     */
    public function metForm($opcion='nuevo')
    {
        ##  valores del formulario
        if ($opcion == 'nuevo') 
        {
            $form = [
                'metodo' => 'crear',
                'pk_num_activo_faltante' => null,
                'fk_afc009_num_movimiento' => $this->TipoMovimiento->metObtenerPorCodigo('60')['pk_num_movimiento'],
                'fec_fecha' => date('Y-m-d'),
                'ind_codigo' => null,
                'ind_comentarios' => null,
                'fk_a001_num_organismo_externo' => null,
                'fec_fecha_informe' => null,
                'ind_comentarios_informe' => null,
                'ind_situacion' => 'PE',
                'ind_estado' => 'PR',
                'lista_detalle' => [],
            ];
            $disabled = [
                'editar' => '',
                'ver' => '',
                'aprobar' => 'disabled',
                'tramitar' => 'disabled',
            ];
            $show = [
                'tramitar' => 'hidden',
                'ver' => '',
            ];
        }
        else
        {
            $form = $this->Faltantes->metObtener($_POST['id']);
            $form['lista_detalle'] = $this->Faltantes->metObtenerActivos($form['pk_num_activo_faltante']);
            ##
            if ($opcion == 'editar') 
            {
                $form['metodo'] = 'modificar';
                $disabled = [
                    'editar' => 'disabled',
                    'ver' => '',
                    'aprobar' => 'disabled',
                    'tramitar' => 'disabled',
                ];
                $show = [
                    'tramitar' => 'hidden',
                    'ver' => '',
                ];
            }
            elseif ($opcion == 'ver') 
            {
                $form['metodo'] = '';
                $disabled = [
                    'editar' => 'disabled',
                    'ver' => 'disabled',
                    'aprobar' => 'disabled',
                    'tramitar' => 'disabled',
                ];
                $show = [
                    'tramitar' => ($form['ind_situacion'] == 'CO') ? '' : 'hidden',
                    'ver' => 'hidden',
                ];
            }
            elseif ($opcion == 'anular') 
            {
                $form['metodo'] = 'anular';
                $disabled = [
                    'editar' => 'disabled',
                    'ver' => 'disabled',
                    'aprobar' => 'disabled',
                    'tramitar' => 'disabled',
                ];
                $show = [
                    'tramitar' => 'hidden',
                    'ver' => '',
                ];
            }
            elseif ($opcion == 'revisar') 
            {
                $form['metodo'] = 'revisar';
                $disabled = [
                    'editar' => 'disabled',
                    'ver' => 'disabled',
                    'aprobar' => 'disabled',
                    'tramitar' => 'disabled',
                ];
                $show = [
                    'tramitar' => 'hidden',
                    'ver' => '',
                ];
            }
            elseif ($opcion == 'aprobar') 
            {
                $form['metodo'] = 'aprobar';
                $disabled = [
                    'editar' => 'disabled',
                    'ver' => 'disabled',
                    'aprobar' => '',
                    'tramitar' => 'disabled',
                ];
                $show = [
                    'tramitar' => 'hidden',
                    'ver' => '',
                ];
            }
            elseif ($opcion == 'tramitar') 
            {
                $form['metodo'] = 'tramitar';
                $form['fec_fecha_informe'] = date('Y-m-d');
                $disabled = [
                    'editar' => 'disabled',
                    'ver' => 'disabled',
                    'aprobar' => 'disabled',
                    'tramitar' => '',
                ];
                $show = [
                    'tramitar' => '',
                    'ver' => '',
                ];
            }
        }

        ##  vista
        $this->atVista->assign('form', $form);
        $this->atVista->assign('disabled', $disabled);
        $this->atVista->assign('show', $show);
        $this->atVista->metRenderizar('formulario', 'modales');
    }

    /**
     * Método para crear un nuevo registro.
     *
     * @return Void
     */
    public function metCrear()
    {
        $_POST['fec_fecha'] = Fecha::formatFecha($_POST['fec_fecha']);

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'fk_afc009_num_movimiento' => 'required',
            'fec_fecha' => 'required|date',
            'ind_comentarios' => 'required',
            'detalle_fk_afb001_num_activo' => 'required',
        ]);
        $gump->set_field_name("fk_afc009_num_movimiento", "Tipo de Movimiento");
        $gump->set_field_name("fec_fecha", "Fecha de Suceso");
        $gump->set_field_name("ind_comentarios", "Descripción del Suceso");
        $gump->set_field_name("detalle_fk_afb001_num_activo", "Activo (Activos Afectados)");
        ##
        if (isset($_POST['detalle_fk_afb001_num_activo']))
        {
            for ($i=0; $i < count($_POST['detalle_fk_afb001_num_activo']); $i++)
            {
                if (count(array_keys($_POST['detalle_fk_afb001_num_activo'], $_POST['detalle_fk_afb001_num_activo'][$i])) > 1) die(json_encode(['status'=>'error', 'mensaje'=>['No puede repetir los Activos Afectados']]));
            }
        }

        ##  validación exitosa
        if($validate === TRUE)
        {
            ##  crear registro
            $id = $this->Faltantes->metCrear($_POST);
            $field = $this->Faltantes->metObtener($id);
            ##  devolver registro creado
            $jsondata = [
                'status' => 'crear',
                'mensaje' => 'Registro creado exitosamente',
                'id' => $id,
            ];
        }
        else 
        {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
     * Método para actualizar un registro.
     *
     * @return Void
     */
    public function metModificar()
    {
        $_POST['fec_fecha'] = Fecha::formatFecha($_POST['fec_fecha']);

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'fec_fecha' => 'required|date',
            'ind_comentarios' => 'required',
            'detalle_fk_afb001_num_activo' => 'required',
        ]);
        $gump->set_field_name("fec_fecha", "Fecha de Suceso");
        $gump->set_field_name("ind_comentarios", "Descripción del Suceso");
        $gump->set_field_name("detalle_fk_afb001_num_activo", "Activo (Activos Afectados)");
        ##
        if (isset($_POST['detalle_fk_afb001_num_activo']))
        {
            for ($i=0; $i < count($_POST['detalle_fk_afb001_num_activo']); $i++)
            {
                if (count(array_keys($_POST['detalle_fk_afb001_num_activo'], $_POST['detalle_fk_afb001_num_activo'][$i])) > 1) die(json_encode(['status'=>'error', 'mensaje'=>['No puede repetir los Activos Afectados']]));
            }
        }

        ##  validación exitosa
        if($validate === TRUE) 
        {
            ##  crear registro
            $id = $_POST['pk_num_activo_faltante'];
            $this->Faltantes->metModificar($_POST);
            $field = $this->Faltantes->metObtener($id);

            ##  devolver registro creado
            $jsondata = [
                'status' => 'modificar',
                'mensaje' => 'Registro actualizado exitosamente',
                'id' => $id,
            ];
        }
        else 
        {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
     * Método para anular un registro.
     *
     * @return Void
     */
    public function metAnular()
    {
        ##  crear registro
        $this->Faltantes->metAnular($_POST);

        ##  devolver registro creado
        $id = $_POST['pk_num_activo_faltante'];
        $field = $this->Faltantes->metObtener($id);
        $jsondata = [
            'status' => 'anular',
            'mensaje' => 'Registro anulado exitosamente',
            'id' => $id,
        ];

        echo json_encode($jsondata);
        exit();
    }

    /**
     * Método para revisar un registro.
     *
     * @return Void
     */
    public function metRevisar()
    {
        ##  crear registro
        $this->Faltantes->metRevisar($_POST);

        ##  devolver registro creado
        $jsondata = [
            'status' => 'revisar',
            'id' => $_POST['pk_num_activo_faltante'],
            'mensaje' => 'Registro revisado exitosamente',
        ];

        echo json_encode($jsondata);
        exit();
    }

    /**
     * Método para aprobar un registro.
     *
     * @return Void
     */
    public function metAprobar()
    {
        ##  crear registro
        $this->Faltantes->metAprobar($_POST);

        ##  devolver registro creado
        $jsondata = [
            'status' => 'aprobar',
            'id' => $_POST['pk_num_activo_faltante'],
            'mensaje' => 'Registro aprobado exitosamente',
        ];

        echo json_encode($jsondata);
        exit();
    }

    /**
     * Método para tramitar un registro.
     *
     * @return Void
     */
    public function metTramitar()
    {
        $_POST['fec_fecha_informe'] = Fecha::formatFecha($_POST['fec_fecha_informe']);

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'fk_a001_num_organismo_externo' => 'required',
            'fec_fecha_informe' => 'required|date',
            'ind_comentarios_informe' => 'required',
        ]);
        $gump->set_field_name("fk_a001_num_organismo_externo", "Organismo Externo");
        $gump->set_field_name("fec_fecha_informe", "Fecha de Informe");
        $gump->set_field_name("ind_comentarios_informe", "Descripción Resultados");

        ##  validación exitosa
        if($validate === TRUE) 
        {
            ##  crear registro
            $this->Faltantes->metTramitar($_POST);

            ##  devolver registro creado
            $jsondata = [
                'status' => 'tramitar',
                'id' => $_POST['pk_num_activo_faltante'],
                'mensaje' => 'Registro tramitado exitosamente',
            ];
        }
        else 
        {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
     * Insertar una linea en una tabla.
     *
     * @return Void
     */
    public function metInsertar()
    {
        $tr = '
        <tr id="detalle'.$_POST['numero'].'">
            <td>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group form-group-sm" id="detalle_fk_afb001_num_activo'.$_POST['numero'].'Error">
                                <input type="hidden" name="detalle_pk_num_activo_faltante_detalle[]" id="detalle_pk_num_activo_faltante_detalle'.$_POST['numero'].'" value="">
                                <input type="hidden" name="detalle_fk_afb001_num_activo[]" id="detalle_fk_afb001_num_activo'.$_POST['numero'].'" value="">
                                <input type="text" name="detalle_cod_activo[]" id="detalle_cod_activo'.$_POST['numero'].'" value="" class="form-control input-sm" disabled>
                                <label for="detalle_cod_activo'.$_POST['numero'].'">Activo</label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector" style="margin: 0px 0px -45px -10px;"
                                    data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Activos"
                                    onclick="selector($(this), \''.BASE_URL.'modAF/activos/ActivosCONTROL/selectorMET\', [\'detalle_fk_afb001_num_activo'.$_POST['numero'].'\',\'detalle_cod_activo'.$_POST['numero'].'\',\'detalle_desc_activo'.$_POST['numero'].'\',\'detalle_fk_afc005_num_ubicacion'.$_POST['numero'].'\',\'detalle_fk_afc007_num_situacion'.$_POST['numero'].'\'],\'opcion=af_faltantes_detalle\');">
                                <i class="md md-search"></i>
                            </button>
                        </div>
                        <div class="col-sm-9">
                            <div class="form-group form-group-sm" id="detalle_desc_activo'.$_POST['numero'].'Error">
                                <textarea name="detalle_desc_activo[]" id="detalle_desc_activo'.$_POST['numero'].'" class="form-control input-sm" disabled></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm" id="detalle_fk_afc005_num_ubicacion'.$_POST['numero'].'Error">
                                <select name="detalle_fk_afc005_num_ubicacion[]" id="detalle_fk_afc005_num_ubicacion'.$_POST['numero'].'" class="form-control input-sm" disabled>
                                    <option value="">&nbsp;</option>
                                    '.Select::lista('af_c005_ubicacion','pk_num_ubicacion','ind_nombre_ubicacion').'
                                </select>
                                <label for="fk_afc005_num_ubicacion">Ubicaci&oacute;n</label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-group-sm" id="detalle_fk_afc007_num_situacion'.$_POST['numero'].'Error">
                                <select name="detalle_fk_afc007_num_situacion[]" id="detalle_fk_afc007_num_situacion'.$_POST['numero'].'" class="form-control input-sm" disabled>
                                    <option value="">&nbsp;</option>
                                    '.Select::lista('af_c007_situacion','pk_num_situacion','ind_nombre_situacion').'
                                </select>
                                <label for="fk_afc007_num_situacion">Situaci&oacute;n</label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-group-sm" id="detalle_ind_estado'.$_POST['numero'].'Error">
                                <input type="hidden" name="detalle_ind_estado[]" id="detalle_ind_estado'.$_POST['numero'].'" value="PE">
                                <input type="text" name="detalle_estado'.$_POST['numero'].'" value="'.Select::options('af_estado_faltante_detalle', 'PE', 2).'" class="form-control input-sm" disabled>
                                <label for="detalle_estado'.$_POST['numero'].'">Estado</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-sm" id="detalle_ind_comentariosError">
                                <textarea name="detalle_ind_comentarios[]" id="detalle_ind_comentarios'.$_POST['numero'].'" class="form-control input-sm" style="height:35px;"></textarea>
                                <label for="detalle_ind_comentarios">Comentarios</label>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>';

        echo $tr;
    }

    /**
     * Método para cargar vista Imprimir Registro.
     *
     * @return Response
     */
    public function metImprimir()
    {
        ##  valores del formulario
        $form['id'] = $_POST['id'];

        ##  vista
        $this->atVista->assign('form', $form);
        $this->atVista->metRenderizar('reporte', 'modales');
    }

    /**
     * Método para imprimir PDF Transferencia de Activos.
     *
     * @return Response
     */
    public function metPdf($id)
    {
        $this->metObtenerLibreria('plantillaPDF','modAF');
        $parametros = Select::parametros();
        ##  ----------------------------------
        $db = new db();
        ##  Obtengo la data a imprimir
        $field = $this->Faltantes->metObtener($id);
        $field['activos'] = $this->Faltantes->metObtenerActivos($field['pk_num_activo_faltante']);
        ##  dependencia
        $sql = "SELECT
                    a4.ind_dependencia,
                    a1.ind_descripcion_empresa,
                    CONCAT_WS(' ', a3.ind_nombre1, a3.ind_nombre2, a3.ind_apellido1, a3.ind_apellido2) AS persona_dependencia,
                    rhc63.ind_descripcion_cargo AS persona_dependencia_cargo
                FROM
                    a004_dependencia a4
                    INNER JOIN a001_organismo a1 ON (a1.pk_num_organismo = a4.fk_a001_num_organismo)
                    LEFT JOIN a003_persona a3 ON (a3.pk_num_persona = a4.fk_a003_num_persona_responsable)
                    LEFT JOIN rh_b001_empleado rhb1 ON (rhb1.fk_a003_num_persona = a3.pk_num_persona)
                    LEFT JOIN rh_c005_empleado_laboral rhc5 ON (rhc5.fk_rhb001_num_empleado = rhb1.pk_num_empleado)
                    LEFT JOIN rh_c063_puestos rhc63 ON (rhc63.pk_num_puestos = rhc5.fk_rhc063_num_puestos_cargo)
                WHERE a4.pk_num_dependencia = '$parametros[DEPENDAFDEF]'";
        $query_dependencia = $db->query($sql);
        $query_dependencia->setFetchMode(PDO::FETCH_ASSOC);
        $field_dependencia = $query_dependencia->fetch();
        ##  ----------------------------------
        ##  Creo el Reporte
        $pdf = new pdfDefaultL('L','mm','Letter');
        $pdf->Organismo = $field_dependencia['ind_descripcion_empresa'];
        $pdf->Dependencia = $field_dependencia['ind_dependencia'];
        $pdf->Titulo = 'FORMATO FALTANTE DE ACTIVOS';
        $pdf->Head = "
            \$this->SetFont('Arial','B',10);
            \$this->Cell(195,5,utf8_decode('Movimiento: " . $field['ind_codigo'] ."-" . $field['num_anio'] . "'),0,1,'L');
            \$this->MultiCell(195,5,utf8_decode('Comentario: " . $field['ind_comentarios'] . "'),0,'L');
            \$this->Ln(1);
            \$this->SetFillColor(255,255,255);
            \$this->SetDrawColor(0,0,0); \$this->Rect(10, \$this->GetY(), 260, 0.1, 'D');
            \$this->Ln(1);
            \$this->SetDrawColor(255,255,255);
            \$this->SetFont('Arial','B',9);
            \$this->SetWidths(array(10,140,30,40,40));
            \$this->SetAligns(array('C','L','C','C','R'));
            \$this->Row(array(utf8_decode('#'),
                            utf8_decode('Descripción'),
                            utf8_decode('Código Interno'),
                            utf8_decode('# Serie'),
                            utf8_decode('Monto Local')
                    ));
        ";
        ##
        $pdf->AliasNbPages();
        $pdf->AddPage();
        ##
        $i = 0;
        foreach ($field['activos'] as $f) {
            $pdf->SetFillColor(255,255,255);
            $pdf->SetDrawColor(0,0,0); $pdf->Rect(10, $pdf->GetY(), 260, 0.1, "D");
            $pdf->Ln(2);
            $pdf->SetDrawColor(255,255,255);
            $pdf->SetFont('Arial','',9);
            $pdf->Row(array(++$i,
                            utf8_decode($f['desc_activo']),
                            utf8_decode($f['cod_activo']),
                            utf8_decode($f['ind_serie']),
                            number_format($f['num_monto'],2,',','.')
                    ));
        }
        if ($pdf->GetY() > 160) $pdf->AddPage(); else $pdf->SetY(160);
        $pdf->SetDrawColor(0,0,0);
        $pdf->Rect(39, $pdf->GetY(), 70, 0.1, "D");
        $pdf->Rect(170, $pdf->GetY(), 70, 0.1, "D");
        $pdf->SetFont('Arial','B',10);
        //$pdf->Cell(130,5,utf8_decode($f['persona_usuario']),0,0,'C');
        //$pdf->Cell(130,5,utf8_decode($f['persona_responsable']),0,0,'C');
        $pdf->Ln(20);
        $pdf->Rect(104, $pdf->GetY(), 70, 0.1, "D");
        $pdf->Cell(260,5,utf8_decode($field_dependencia['persona_dependencia']),0,0,'C');
        ##
        $pdf->Output();
    }
}
