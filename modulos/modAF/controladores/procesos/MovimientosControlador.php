<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Movimientos de Activos
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |08-03-2016               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'Fecha.php';
require_once ROOT.'librerias'.DS.'Number.php';

class MovimientosControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->Movimientos = $this->metCargarModelo('Movimientos','procesos');
        $this->Activos = $this->metCargarModelo('Activos','activos');
    }

    /**
     * Método para cargar vista Listado de Registros.
     *
     * @return Response
     */
    public function metIndex($lista='listar')
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-ui-selectable/jquery-ui',
        );
        $complementoJs = array(
            'mask/jquery.mask',
            'jquery-ui-selectable/jquery-ui',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        if ($_POST) $filtro = $_POST;
        else {
            if ($lista == 'revisar') $find_estado = 'PR';
            elseif ($lista == 'aprobar') $find_estado = 'RV';
            else $find_estado = '';
            $filtro = [
                'fnum_organismo' => '',
                'find_tipo' => '',
                'fnum_motivo' => '',
                'find_estado' => $find_estado,
                'ffec_fechad' => '',
                'ffec_fechah' => '',
            ];
        }
        if ($lista == 'listar') {
            $data = [
                'lista' => $lista,
                'titulo' => 'Listado',
            ];
        }
        elseif ($lista == 'revisar') {
            $data = [
                'lista' => $lista,
                'titulo' => 'Revisar',
            ];
        }
        elseif ($lista == 'aprobar') {
            $data = [
                'lista' => $lista,
                'titulo' => 'Aprobar',
            ];
        }
        $this->atVista->assign('data', $data);
        $this->atVista->assign('filtro', $filtro);
        $this->atVista->assign('listado', $this->Movimientos->metListar($filtro));
        $this->atVista->metRenderizar('listado');
    }

    /**
     * Método para cargar vista formulario Nuevo Registro.
     *
     * @return Response
     */
    public function metForm($opcion='nuevo')
    {
        ##  valores del formulario
        if ($opcion == 'nuevo') {
            $fk_a004_num_dependencia = Select::parametros('DEPENDAFDEF');
            $fk_a001_num_organismo = Select::field('a004_dependencia', 'fk_a001_num_organismo', ['pk_num_dependencia' => $fk_a004_num_dependencia]);
            $form = [
                'base' => BASE_URL + 'modAF/procesos/MovimientosCONTROL/',
                'metodo' => 'crear',
                'pk_num_movimiento' => null,
                'fk_a001_num_organismo' => $fk_a001_num_organismo,
                'fk_a006_num_tipo_acta' => null,
                'fec_fecha' => date('Y-m-d'),
                'num_anio' => null,
                'ind_nro_acta' => null,
                'ind_tipo' => 'I',
                'fk_a006_num_motivo' => null,
                'ind_comentarios' => null,
                'ind_estado' => 'PR',
                'lista_detalle' => []
            ];
            $disabled = [
                'editar' => '',
                'ver' => '',
            ];
        }
        elseif ($opcion == 'editar' || $opcion == 'ver' || $opcion == 'anular' || $opcion == 'revisar' || $opcion == 'aprobar') {
            $form = $this->Movimientos->metObtener($_POST['id']);
            $form['lista_detalle'] = $this->Movimientos->metObtenerActivos($form['pk_num_movimiento']);
            ##
            if ($opcion == 'editar') {
                $form['metodo'] = 'modificar';
                $disabled = [
                    'editar' => 'disabled',
                    'ver' => '',
                ];
            }
            elseif ($opcion == 'ver') {
                $form['metodo'] = '';
                $disabled = [
                    'editar' => 'disabled',
                    'ver' => 'disabled',
                ];
            }
            elseif ($opcion == 'anular') {
                $form['metodo'] = 'anular';
                $disabled = [
                    'editar' => 'disabled',
                    'ver' => 'disabled',
                ];
            }
            elseif ($opcion == 'revisar') {
                $form['metodo'] = 'revisar';
                $disabled = [
                    'editar' => 'disabled',
                    'ver' => 'disabled',
                ];
            }
            elseif ($opcion == 'aprobar') {
                $form['metodo'] = 'aprobar';
                $disabled = [
                    'editar' => 'disabled',
                    'ver' => 'disabled',
                ];
            }
        }

        ##  vista
        $this->atVista->assign('form', $form);
        $this->atVista->assign('disabled', $disabled);
        $this->atVista->metRenderizar('formulario', 'modales');
    }

    /**
     * Método para crear un nuevo registro.
     *
     * @return Void
     */
    public function metCrear()
    {
        $_POST['fec_fecha'] = Fecha::formatFecha($_POST['fec_fecha']);

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'fk_a001_num_organismo' => 'required',
            'fec_fecha' => 'required|date',
            'ind_tipo' => 'required',
            'fk_a006_num_motivo' => 'required',
            'ind_comentarios' => 'required',
            'detalle_fk_afb001_num_activo' => 'required',
            'detalle_fk_a023_num_centro_costo' => 'required',
            'detalle_fk_afc005_num_ubicacion' => 'required',
            'detalle_fk_a003_num_usuario' => 'required',
            'detalle_fk_a003_num_responsable' => 'required',
        ]);
        $gump->set_field_name("fk_a001_num_organismo", "Organismo");
        $gump->set_field_name("fec_fecha", "Fecha");
        $gump->set_field_name("ind_tipo", "Tipo de Movimiento");
        $gump->set_field_name("fk_a006_num_motivo", "Motivo de Traslado");
        $gump->set_field_name("ind_comentarios", "Comentarios");
        $gump->set_field_name("detalle_fk_afb001_num_activo", "Activo (Activos Afectados)");
        $gump->set_field_name("detalle_fk_a023_num_centro_costo", "Centro de Costo Actual (Activos Afectados)");
        $gump->set_field_name("detalle_fk_afc005_num_ubicacion", "Ubicación Actual (Activos Afectados)");
        $gump->set_field_name("detalle_fk_a003_num_usuario", "Empleado Usuario Actual (Activos Afectados)");
        $gump->set_field_name("detalle_fk_a003_num_responsable", "Empleado Responsable Actual (Activos Afectados)");
        ##
        if (isset($_POST['detalle_fk_afb001_num_activo']))
        {
            for ($i=0; $i < count($_POST['detalle_fk_afb001_num_activo']); $i++)
            {
                if ($i > 0) $j = $i - 1; else $j = 0;
                ##
                $field_activo = $this->Activos->metObtener($_POST['detalle_fk_afb001_num_activo'][$i]);
                $DependenciaAnterior[] = $field_activo['fk_a004_num_dependencia'];
                ##
                if ($_POST['detalle_fk_afb001_num_activo'][$i] == '') die(json_encode(['status'=>'error', 'mensaje'=>["Debe seleccionar el Activo Afectado"]]));
                elseif ($_POST['detalle_fk_a023_num_centro_costo'][$i] == '') die(json_encode(['status'=>'error', 'mensaje'=>["Debe seleccionar el Centro de Costo Actual (Activo Afectado)"]]));
                elseif ($_POST['detalle_fk_afc005_num_ubicacion'][$i] == '') die(json_encode(['status'=>'error', 'mensaje'=>["Debe seleccionar la Ubicación Actual (Activo Afectado)"]]));
                elseif ($_POST['detalle_fk_a003_num_usuario'][$i] == '') die(json_encode(['status'=>'error', 'mensaje'=>["Debe seleccionar el Empleado Usuario Actual (Activo Afectado)"]]));
                elseif ($_POST['detalle_fk_a003_num_responsable'][$i] == '') die(json_encode(['status'=>'error', 'mensaje'=>["Debe seleccionar el Empleado Responsable Actual (Activo Afectado)"]]));
                elseif ($_POST['detalle_fk_afc009_num_movimiento_inc'][$i] == '') die(json_encode(['status'=>'error', 'mensaje'=>["Debe seleccionar el Tipo de Movimiento Actual (Activo Afectado)"]]));
                elseif ($_POST['detalle_fk_afc009_num_movimiento_des'][$i] == '') die(json_encode(['status'=>'error', 'mensaje'=>["Debe seleccionar el Tipo de Movimiento Actual (Activo Afectado)"]]));
                elseif ($_POST['fk_a001_num_organismo'] != $_POST['detalle_fk_a001_num_organismo'][$i] || $_POST['fk_a001_num_organismo'] != $field_activo['fk_a001_num_organismo']) die(json_encode(['status'=>'error', 'mensaje'=>['El Organismo debe coincidir para todos los activos']]));
                elseif (count(array_keys($_POST['detalle_fk_afb001_num_activo'], $_POST['detalle_fk_afb001_num_activo'][$i])) > 1) die(json_encode(['status'=>'error', 'mensaje'=>['No puede repetir los Activos Afectados']]));
                elseif (count(array_keys($_POST['detalle_fk_a004_num_dependencia'], $_POST['detalle_fk_a004_num_dependencia'][$i])) != count($_POST['detalle_fk_a004_num_dependencia'])) die(json_encode(['status'=>'error', 'mensaje'=>['La Dependencia Actual debe coincidir para todos los activos']]));
                elseif (count(array_keys($DependenciaAnterior, $field_activo['fk_a004_num_dependencia'])) != count($DependenciaAnterior)) die(json_encode(['status'=>'error', 'mensaje'=>['La Dependencia Anterior debe coincidir para todos los activos']]));
            }
        }

        ##  validación exitosa
        if($validate === TRUE) {
            ##  crear registro
            $id = $this->Movimientos->metCrear($_POST);
            $field = $this->Movimientos->metObtener($id);
            ##  devolver registro creado
            $jsondata = [
                'status' => 'crear',
                'mensaje' => 'Registro creado exitosamente',
                'id' => $id,
                'tr' => [
                    '<td><label>'.$id.'</label></td>',
                    '<td><label>'.Select::options('af_estado_movimiento', $field['ind_estado'], 2).'</label></td>',
                    '<td><label>'.$field['ind_nro_acta'].$field['num_anio'].'</label></td>',
                    '<td><label>'.Fecha::formatFecha($field['fec_fecha'],'Y-m-d','d-m-Y').'</label></td>',
                    '<td><label>'.$field['ind_comentarios'].'</label></td>',
                    '<td>
                        <div class="btn-group">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="btn ink-reaction btn-raised btn-xs btn-primary" id="modificar"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Modificar Registro">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'
                            <button idRegistro="'.$id.'" class="btn ink-reaction btn-raised btn-xs btn-warning" id="ver"
                                    data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Ver Registro">
                                <i class="fa fa-eye" style="color: #ffffff;"></i>
                            </button>
                            <button idRegistro="'.$id.'" class="btn ink-reaction btn-raised btn-xs btn-primary" id="imprimir"
                                    data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Imprimir Registro">
                                <i class="fa fa-print" style="color: #ffffff;"></i>
                            </button>
                            '.(($_POST['perfilAN'])?'<button idRegistro="'.$id.'" class="btn ink-reaction btn-raised btn-xs btn-danger" id="anular"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Anular Registro"
                                                            title="Anular Movimiento Nro. {$registro.ind_nro_acta}-{$registro.num_anio}">
                                                        <i class="fa fa-thumbs-down" style="color: #ffffff;"></i>
                                                    </button>':'').'
                        </div>
                    </td>',
                ],
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
     * Método para actualizar un registro.
     *
     * @return Void
     */
    public function metModificar()
    {
        $field = $this->Movimientos->metObtener($_POST['pk_num_movimiento']);
        $_POST['fec_fecha'] = Fecha::formatFecha($_POST['fec_fecha']);

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'fec_fecha' => 'required|date',
            'ind_tipo' => 'required',
            'fk_a006_num_motivo' => 'required',
            'ind_comentarios' => 'required',
            'detalle_fk_afb001_num_activo' => 'required',
            'detalle_fk_a023_num_centro_costo' => 'required',
            'detalle_fk_afc005_num_ubicacion' => 'required',
            'detalle_fk_a003_num_usuario' => 'required',
            'detalle_fk_a003_num_responsable' => 'required',
        ]);
        $gump->set_field_name("fec_fecha", "Fecha");
        $gump->set_field_name("ind_tipo", "Tipo de Movimiento");
        $gump->set_field_name("fk_a006_num_motivo", "Motivo de Traslado");
        $gump->set_field_name("ind_comentarios", "Comentarios");
        $gump->set_field_name("detalle_fk_afb001_num_activo", "Organismo (Activos Afectados)");
        $gump->set_field_name("detalle_fk_a023_num_centro_costo", "Centro de Costo Actual (Activos Afectados)");
        $gump->set_field_name("detalle_fk_afc005_num_ubicacion", "Ubicación Actual (Activos Afectados)");
        $gump->set_field_name("detalle_fk_a003_num_usuario", "Empleado Usuario Actual (Activos Afectados)");
        $gump->set_field_name("detalle_fk_a003_num_responsable", "Empleado Responsable Actual (Activos Afectados)");
        ##
        for ($i=0; $i < count($_POST['detalle_fk_afb001_num_activo']); $i++) {
            if ($i > 0) $j = $i - 1; else $j = 0;
            ##
            $field_activo = $this->Activos->metObtener($_POST['detalle_fk_afb001_num_activo'][$i]);
            $DependenciaAnterior[] = $field_activo['fk_a004_num_dependencia'];
            ##
            if ($_POST['detalle_fk_afb001_num_activo'][$i] == '') die(json_encode(['status'=>'error', 'mensaje'=>["Debe seleccionar el Activo Afectado"]]));
            elseif ($_POST['detalle_fk_a023_num_centro_costo'][$i] == '') die(json_encode(['status'=>'error', 'mensaje'=>["Debe seleccionar el Centro de Costo Actual (Activo Afectado)"]]));
            elseif ($_POST['detalle_fk_afc005_num_ubicacion'][$i] == '') die(json_encode(['status'=>'error', 'mensaje'=>["Debe seleccionar la Ubicación Actual (Activo Afectado)"]]));
            elseif ($_POST['detalle_fk_a003_num_usuario'][$i] == '') die(json_encode(['status'=>'error', 'mensaje'=>["Debe seleccionar el Empleado Usuario Actual (Activo Afectado)"]]));
            elseif ($_POST['detalle_fk_a003_num_responsable'][$i] == '') die(json_encode(['status'=>'error', 'mensaje'=>["Debe seleccionar el Empleado Responsable Actual (Activo Afectado)"]]));
            elseif ($_POST['detalle_fk_afc009_num_movimiento_inc'][$i] == '') die(json_encode(['status'=>'error', 'mensaje'=>["Debe seleccionar el Tipo de Movimiento Actual (Activo Afectado)"]]));
            elseif ($_POST['detalle_fk_afc009_num_movimiento_des'][$i] == '') die(json_encode(['status'=>'error', 'mensaje'=>["Debe seleccionar el Tipo de Movimiento Actual (Activo Afectado)"]]));
            elseif ($field['fk_a001_num_organismo'] != $_POST['detalle_fk_a001_num_organismo'][$i] || $field['fk_a001_num_organismo'] != $field_activo['fk_a001_num_organismo']) die(json_encode(['status'=>'error', 'mensaje'=>['El Organismo debe coincidir para todos los activos']]));
            elseif (count(array_keys($_POST['detalle_fk_afb001_num_activo'], $_POST['detalle_fk_afb001_num_activo'][$i])) > 1) die(json_encode(['status'=>'error', 'mensaje'=>['No puede repetir los Activos Afectados']]));
            elseif (count(array_keys($_POST['detalle_fk_a004_num_dependencia'], $_POST['detalle_fk_a004_num_dependencia'][$i])) != count($_POST['detalle_fk_a004_num_dependencia'])) die(json_encode(['status'=>'error', 'mensaje'=>['La Dependencia Actual debe coincidir para todos los activos']]));
            elseif (count(array_keys($DependenciaAnterior, $field_activo['fk_a004_num_dependencia'])) != count($DependenciaAnterior)) die(json_encode(['status'=>'error', 'mensaje'=>['La Dependencia Anterior debe coincidir para todos los activos']]));
        }

        ##  validación exitosa
        if($validate === TRUE) {
            ##  crear registro
            $id = $_POST['pk_num_movimiento'];
            $this->Movimientos->metModificar($_POST);
            $field = $this->Movimientos->metObtener($id);

            ##  devolver registro creado
            $jsondata = [
                'status' => 'modificar',
                'mensaje' => 'Registro actualizado exitosamente',
                'id' => $id,
                'tr' => '
                    <td><label>'.$id.'</label></td>
                    <td><label>'.Select::options('af_estado_movimiento', $field['ind_estado'], 2).'</label></td>
                    <td><label>'.$field['ind_nro_acta'].$field['num_anio'].'</label></td>
                    <td><label>'.Fecha::formatFecha($field['fec_fecha'],'Y-m-d','d-m-Y').'</label></td>
                    <td><label>'.$field['ind_comentarios'].'</label></td>
                    <td>
                        <div class="btn-group">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="btn ink-reaction btn-raised btn-xs btn-primary" id="modificar"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Modificar Registro">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'
                            <button idRegistro="'.$id.'" class="btn ink-reaction btn-raised btn-xs btn-warning" id="ver"
                                    data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Ver Registro">
                                <i class="fa fa-eye" style="color: #ffffff;"></i>
                            </button>
                            <button idRegistro="'.$id.'" class="btn ink-reaction btn-raised btn-xs btn-primary" id="imprimir"
                                    data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Imprimir Registro">
                                <i class="fa fa-print" style="color: #ffffff;"></i>
                            </button>
                            '.(($_POST['perfilAN'])?'<button idRegistro="'.$id.'" class="btn ink-reaction btn-raised btn-xs btn-danger" id="anular"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Anular Registro"
                                                            title="Anular Movimiento Nro. {$registro.ind_nro_acta}-{$registro.num_anio}">
                                                        <i class="fa fa-thumbs-down" style="color: #ffffff;"></i>
                                                    </button>':'').'
                        </div>
                    </td>
                ',
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
     * Método para anular un registro.
     *
     * @return Void
     */
    public function metAnular()
    {
        ##  crear registro
        $this->Movimientos->metAnular($_POST);

        ##  devolver registro creado
        $jsondata = [
            'status' => 'anular',
            'id' => $_POST['pk_num_movimiento'],
            'mensaje' => 'Registro anulado exitosamente',
        ];

        echo json_encode($jsondata);
        exit();
    }

    /**
     * Método para revisar un registro.
     *
     * @return Void
     */
    public function metRevisar()
    {
        ##  crear registro
        $this->Movimientos->metRevisar($_POST);

        ##  devolver registro creado
        $jsondata = [
            'status' => 'revisar',
            'id' => $_POST['pk_num_movimiento'],
            'mensaje' => 'Registro revisado exitosamente',
        ];

        echo json_encode($jsondata);
        exit();
    }

    /**
     * Método para aprobar un registro.
     *
     * @return Void
     */
    public function metAprobar()
    {
        ##  crear registro
        $this->Movimientos->metAprobar($_POST);

        ##  devolver registro creado
        $jsondata = [
            'status' => 'aprobar',
            'id' => $_POST['pk_num_movimiento'],
            'mensaje' => 'Registro aprobado exitosamente',
        ];

        echo json_encode($jsondata);
        exit();
    }

    /**
     * Método para cargar vista Imprimir Registro.
     *
     * @return Response
     */
    public function metImprimir()
    {
        ##  valores del formulario
        $form['id'] = $_POST['id'];

        ##  vista
        $this->atVista->assign('form', $form);
        $this->atVista->metRenderizar('reporte', 'modales');
    }

    /**
     * Método para imprimir PDF Transferencia de Activos.
     *
     * @return Response
     */
    public function metImprimirTransferencia($id)
    {
        $this->metObtenerLibreria('plantillaPDF','modAF');
        $parametros = Select::parametros();
        ##  ----------------------------------
        $db = new db();
        ##  Obtengo la data a imprimir
        $field = $this->Movimientos->metObtener($id);
        $field['activos'] = $this->Movimientos->metObtenerActivos($field['pk_num_movimiento']);
        ##  dependencia
        $sql = "SELECT
                    d.ind_dependencia,
                    o.ind_descripcion_empresa
                FROM
                    a004_dependencia d
                    INNER JOIN a001_organismo o ON (o.pk_num_organismo = d.fk_a001_num_organismo)
                WHERE d.pk_num_dependencia = '$parametros[DEPENDAFDEF]'";
        $query_dependencia = $db->query($sql);
        $query_dependencia->setFetchMode(PDO::FETCH_ASSOC);
        $field_dependencia = $query_dependencia->fetch();
        ##  firma 1
        $sql = "SELECT
                    CONCAT_WS(' ', p.ind_nombre1, p.ind_nombre2, p.ind_apellido1, p.ind_apellido2) AS persona,
                    p.ind_cedula_documento as cedula
                FROM
                    a003_persona p
                    INNER JOIN af_c023_movimiento_detalle md ON (md.fk_a003_num_responsable_ant = p.pk_num_persona)
                WHERE md.fk_afc012_num_movimiento = '$id'
                GROUP BY md.fk_a003_num_responsable_ant";
        $query_firma1 = $db->query($sql);
        $query_firma1->setFetchMode(PDO::FETCH_ASSOC);
        $field_firma1 = $query_firma1->fetch();
        ##  firma 2
        $sql = "SELECT
                    CONCAT_WS(' ', p.ind_nombre1, p.ind_nombre2, p.ind_apellido1, p.ind_apellido2) AS persona,
                    p.ind_cedula_documento as cedula
                FROM
                    a003_persona p
                    INNER JOIN af_c023_movimiento_detalle md ON (md.fk_a003_num_responsable = p.pk_num_persona)
                WHERE md.fk_afc012_num_movimiento = '$id'
                GROUP BY md.fk_a003_num_responsable";
        $query_firma2 = $db->query($sql);
        $query_firma2->setFetchMode(PDO::FETCH_ASSOC);
        $field_firma2 = $query_firma2->fetch();
        ##  firma 3
        $sql = "SELECT
                    CONCAT_WS(' ', p.ind_nombre1, p.ind_nombre2, p.ind_apellido1, p.ind_apellido2) AS persona,
                    p.ind_cedula_documento as cedula
                FROM a003_persona p
                WHERE p.pk_num_persona = '$field[fk_a003_num_preparado_por]'";
        $query_firma3 = $db->query($sql);
        $query_firma3->setFetchMode(PDO::FETCH_ASSOC);
        $field_firma3 = $query_firma3->fetch();
        ##  ----------------------------------
        ##  Creo el Reporte
        $pdf = new pdfDefaultP('P','mm','Letter');
        $pdf->Organismo = $field_dependencia['ind_descripcion_empresa'];
        $pdf->Dependencia = $field_dependencia['ind_dependencia'];
        $pdf->Titulo = 'TRANSFERENCIA DE ACTIVOS FIJOS';
        $pdf->Head = "
            \$this->SetFont('Arial','B',8);
            \$this->Cell(195,5,utf8_decode('Movimiento: " . $field['ind_nro_acta'] ."-" . $field['num_anio'] . "'),0,1,'L');
            \$this->MultiCell(195,5,utf8_decode('Comentario: " . $field['ind_comentarios'] . "'),0,'L');
            \$this->Cell(195,5,utf8_decode('Items a ser transferidos: '),0,1,'L');
        ";
        ##
        $pdf->AliasNbPages();
        $pdf->AddPage();
        ##
        $i = 0;
        foreach ($field['activos'] as $f) {
            $pdf->SetFillColor(255,255,255);
            $pdf->SetDrawColor(0,0,0); $pdf->Rect(10, $pdf->GetY(), 195, 0.1, "D");
            $pdf->Ln(1);
            $pdf->SetDrawColor(255,255,255);
            $pdf->SetFont('Arial','B',9);
            $pdf->SetWidths(array(10,30,95,30,30));
            $pdf->SetAligns(array('C','C','L','C','C'));
            $pdf->Row(array(utf8_decode('#'),
                            utf8_decode('Activo'),
                            utf8_decode('Descripción'),
                            utf8_decode('Código Interno'),
                            utf8_decode('# Serie')
                    ));
            $pdf->SetDrawColor(0,0,0); $pdf->Rect(10, $pdf->GetY(), 195, 0.1, "D");
            $pdf->Ln(2);
            $pdf->SetDrawColor(255,255,255);
            $pdf->SetFont('Arial','B',9);
            $pdf->Row(array(++$i,
                            utf8_decode($f['fk_afb001_num_activo']),
                            utf8_decode($f['desc_activo']),
                            utf8_decode($f['cod_activo']),
                            utf8_decode($f['ind_serie'])
                    ));
            $pdf->Ln(2);
            $pdf->Cell(27,5);
            $pdf->Cell(84,5,utf8_decode('Anterior'),0,0,'L');
            $pdf->Cell(84,5,utf8_decode('Actual'),0,1,'L');
            $pdf->Ln(1);
            $pdf->SetDrawColor(255,255,255);
            $pdf->SetFont('Arial','',8);
            $pdf->SetWidths(array(27,14,70,14,70));
            $pdf->SetAligns(array('L','L','L','L','L'));
            $pdf->Row(array(utf8_decode('C.Costo:'),
                            utf8_decode($f['cod_centro_costo_ant']),
                            utf8_decode($f['centro_costo_ant']),
                            utf8_decode($f['cod_centro_costo']),
                            utf8_decode($f['centro_costo'])
                    ));
            $pdf->Ln(1);
            $pdf->Row(array(utf8_decode('Ubicación:'),
                            utf8_decode($f['cod_ubicacion_ant']),
                            utf8_decode($f['ubicacion_ant']),
                            utf8_decode($f['cod_ubicacion']),
                            utf8_decode($f['ubicacion'])
                    ));
            $pdf->Ln(1);
            $pdf->Row(array(utf8_decode('Dependencia:'),
                            utf8_decode($f['cod_dependencia_ant']),
                            utf8_decode($f['dependencia_ant']),
                            utf8_decode($f['cod_dependencia']),
                            utf8_decode($f['dependencia'])
                    ));
            $pdf->Ln(1);
            $pdf->Row(array(utf8_decode('Emp. Usuario:'),
                            utf8_decode(''),
                            utf8_decode($f['persona_usuario']),
                            utf8_decode(''),
                            utf8_decode($f['persona_usuario_ant'])
                    ));
            $pdf->Ln(1);
            $pdf->Row(array(utf8_decode('Emp. Responsable:'),
                            utf8_decode(''),
                            utf8_decode($f['persona_responsable']),
                            utf8_decode(''),
                            utf8_decode($f['persona_responsable_ant'])
                    ));
            $pdf->Ln(10);
        }
        if ($pdf->GetY() > 230) $pdf->AddPage();
        $pdf->SetY(230);
        $pdf->SetDrawColor(0,0,0);
        $pdf->Rect(29, $pdf->GetY(), 60, 0.1, "D");
        $pdf->Rect(126, $pdf->GetY(), 60, 0.1, "D");
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(98,5,utf8_decode($field_firma2['persona']),0,0,'C');
        $pdf->Cell(98,5,utf8_decode($field_firma1['persona']),0,0,'C');
        $pdf->Ln(20);
        $pdf->Rect(77, $pdf->GetY(), 60, 0.1, "D");
        $pdf->Cell(195,5,utf8_decode($field_firma3['persona']),0,0,'C');
        ##
        $pdf->Output();
    }

    /**
     * Método para imprimir PDF Transferencia de Activos.
     *
     * @return Response
     */
    public function metImprimirActa($id)
    {
        $this->metObtenerLibreria('plantillaPDF','modAF');
        $parametros = Select::parametros();
        ##  ----------------------------------
        $db = new db();
        ##  Obtengo la data a imprimir
        $field = $this->Movimientos->metObtener($id);
        $field['activos'] = $this->Movimientos->metObtenerActivos($field['pk_num_movimiento']);
        ##  dependencia
        $sql = "SELECT
                    d.ind_dependencia,
                    o.ind_descripcion_empresa
                FROM
                    a004_dependencia d
                    INNER JOIN a001_organismo o ON (o.pk_num_organismo = d.fk_a001_num_organismo)
                WHERE d.pk_num_dependencia = '$parametros[DEPENDAFDEF]'";
        $query_dependencia = $db->query($sql);
        $query_dependencia->setFetchMode(PDO::FETCH_ASSOC);
        $field_dependencia = $query_dependencia->fetch();
        ##  firma 1
        $sql = "SELECT
                    CONCAT_WS(' ', p.ind_nombre1, p.ind_nombre2, p.ind_apellido1, p.ind_apellido2) AS persona,
                    p.ind_cedula_documento as cedula
                FROM a003_persona p
                WHERE p.pk_num_persona = '$field[fk_a003_num_aprobado_por]'";
        $query_firma1 = $db->query($sql);
        $query_firma1->setFetchMode(PDO::FETCH_ASSOC);
        $field_firma1 = $query_firma1->fetch();
        ##  firma 2
        $sql = "SELECT
                    CONCAT_WS(' ', p.ind_nombre1, p.ind_nombre2, p.ind_apellido1, p.ind_apellido2) AS persona,
                    p.ind_cedula_documento as cedula
                FROM a003_persona p
                WHERE p.pk_num_persona = '$field[fk_a003_num_revisado_por]'";
        $query_firma2 = $db->query($sql);
        $query_firma2->setFetchMode(PDO::FETCH_ASSOC);
        $field_firma2 = $query_firma2->fetch();
        ##  firma 3
        $sql = "SELECT
                    CONCAT_WS(' ', p.ind_nombre1, p.ind_nombre2, p.ind_apellido1, p.ind_apellido2) AS persona,
                    p.ind_cedula_documento as cedula
                FROM
                    a003_persona p
                    INNER JOIN af_c023_movimiento_detalle md ON (md.fk_a003_num_responsable = p.pk_num_persona)
                WHERE md.fk_afc012_num_movimiento = '$id'
                GROUP BY md.fk_a003_num_responsable";
        $query_firma3 = $db->query($sql);
        $query_firma3->setFetchMode(PDO::FETCH_ASSOC);
        $field_firma3 = $query_firma3->fetch();
        ##  ----------------------------------
        ##  Creo el Reporte
        $pdf = new pdfDefaultP('P','mm','Letter');
        $pdf->Organismo = $field_dependencia['ind_descripcion_empresa'];
        $pdf->Dependencia = $field_dependencia['ind_dependencia'];
        $pdf->Titulo = 'ACTA DE ENTREGA DE BIENES MUEBLES';
        $pdf->Head = "
            \$this->SetFont('Arial','B',8);
            \$this->SetXY(165,22); \$this->Cell(25,5,utf8_decode('Movimiento: " . $field['ind_nro_acta'] ."-" . $field['num_anio'] . "'),0,1,'L');
            \$this->SetY(45);
        ";
        $pdf->Foot = "
            \$this->SetFont('Arial','B',8);
            \$this->SetXY(15,265); \$this->Cell(25,5,utf8_decode('REF.: FOR-DSG-002'),0,1,'L');
        ";
        ##
        $pdf->AliasNbPages();
        $pdf->AddPage();
        ##
        list($Anio, $Mes, $Dia) = explode('-', $field['fec_fecha']);
        $Texto = "En el dia de hoy, " . Fecha::dia_semana_nombre($field['fec_fecha']) . " del mes de " . Fecha::mes_nombre($Mes) . " del año " . $Anio . ", reunidos en las instalaciones donde funciona la " . $field['organismo'] . ", ubicada en " . $field['organismo_direccion'] . ", los ciudadanos: " . $field_firma1['persona'] . ", titular de la cédula de identidad N° " . $field_firma1['cedula'] . ", " . $field_firma1['persona'] . ", titular de la cédula de identidad N° " . $field_firma1['cedula'] . ", " . $field_firma3['persona'] . ", titular de la cédula de identidad N° " . $field_firma3['cedula'] . ", con el único objeto de hacerle entrega al último de los mencionados como Responsable Patrimonial Primario; en calidad de uso, guarda y custodia de los bienes muebles que a continuación se especifican:                                      ";
        $pdf->SetFont('Arial','',12);
        $pdf->MultiCell(195,7,utf8_decode($Texto),0,'FJ');
        $pdf->Ln(3);
        ##
        $pdf->SetFillColor(240,240,240);
        $pdf->SetDrawColor(0,0,0);
        $pdf->Ln(1);
        $pdf->SetFont('Arial','B',7);
        $pdf->SetWidths(array(25,15,20,75,35,25));
        $pdf->SetAligns(array('C','C','C','L','C','C'));
        $pdf->Row(array(utf8_decode('CLASIFICACIÓN'),
                        utf8_decode('CANTIDAD'),
                        utf8_decode('COD. INTERNO'),
                        utf8_decode('DESCRIPCIÓN'),
                        utf8_decode('MARCA'),
                        utf8_decode('MODELO')
                ));
        foreach ($field['activos'] as $f) {
            $pdf->SetFillColor(255,255,255);
            $pdf->SetDrawColor(0,0,0);
            $pdf->SetFont('Arial','',7);
            $pdf->Row(array(utf8_decode($f['cod_clasificacion']),
                            1,
                            utf8_decode($f['cod_activo']),
                            utf8_decode($f['desc_activo']),
                            utf8_decode($f['ind_modelo']),
                            utf8_decode($f['marca'])
                    ));
        }
        $pdf->Ln(3);
        $Texto = "Los mismos son propiedad de este ente de Control Fiscal, tal como se desprende del registro de Bienes e Inventarios llevado ante esta Contraloría, el mismo quedará bajo su responsabilidad absoluta, siendo éste responsable de vigilar, conservar y salvaguardar, los bienes muebles entregados mediante la presente Acta; queda entendido que cualquier daño material que pueda ocurrirle a los referidos bienes muebles; con ocasión de negligencia u omisión en su uso; queda sujeta a las sanciones administrativas previstas en articulo 91 numeral 2 de la Ley Orgánica de la Contraloría General de la República y del Sistema Nacional de Control Fiscal,Disciplinarias prevista en el artículo 33 numeral 7 de la Ley del Estatuto de Función Pública y Penal prevista en el artículo 53 de la Ley Contra la Corrupcion, salvo aquellos daños naturales u hechos fortuitos que se presenten, lo cual deberá ser notificado por escrito ante la " . $field_dependencia['ind_dependencia'] . " de ésta Contraloría. Es todo, terminó se leyó y conformes firman.                                                       ";
        $pdf->SetFont('Arial','',12);
        $pdf->MultiCell(195,7,utf8_decode($Texto),0,'FJ');
        ##
        if ($pdf->GetY() > 230) $pdf->AddPage();
        $pdf->SetY(230);
        $pdf->SetDrawColor(0,0,0);
        $pdf->Rect(29, $pdf->GetY(), 60, 0.1, "D");
        $pdf->Rect(126, $pdf->GetY(), 60, 0.1, "D");
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(98,5,utf8_decode($field_firma2['persona']),0,0,'C');
        $pdf->Cell(98,5,utf8_decode($field_firma1['persona']),0,0,'C');
        $pdf->Ln(20);
        $pdf->Rect(77, $pdf->GetY(), 60, 0.1, "D");
        $pdf->Cell(195,5,utf8_decode($field_firma3['persona']),0,0,'C');
        ##
        $pdf->Output();
    }

    /**
     * Insertar una linea en una tabla.
     *
     * @return Void
     */
    public function metInsertar()
    {
        $tr = '
        <tr id="detalle'.$_POST['numero'].'">
            <td>
                <div class="container-fluid">
                    <div class="row" style="background-color:#DDD; border-radius:5px; padding-top:10px;">
                        <div class="col-sm-2">
                            <div class="form-group form-group-sm" id="detalle_fk_afb001_num_activo'.$_POST['numero'].'Error">
                                <input type="hidden" name="detalle_fk_afb001_num_activo[]" id="detalle_fk_afb001_num_activo'.$_POST['numero'].'" value="">
                                <input type="text" name="detalle_cod_activo[]" id="detalle_cod_activo'.$_POST['numero'].'" value="" class="form-control input-sm" disabled>
                                <label for="detalle_cod_activo'.$_POST['numero'].'">Activo</label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector" style="margin: 0px 0px -45px -10px;"
                                    data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Activos"
                                    onclick="selector($(this), \''.BASE_URL.'modAF/activos/ActivosCONTROL/selectorMET\', [\'detalle_fk_afb001_num_activo'.$_POST['numero'].'\',\'detalle_cod_activo'.$_POST['numero'].'\',\'detalle_desc_activo'.$_POST['numero'].'\',\'detalle_fk_a001_num_organismo_ant'.$_POST['numero'].'\',\'detalle_fk_a004_num_dependencia_ant'.$_POST['numero'].'\',\'detalle_fk_a023_num_centro_costo_ant'.$_POST['numero'].'\',\'detalle_fk_afc005_num_ubicacion_ant'.$_POST['numero'].'\',\'detalle_fk_a003_num_usuario_ant'.$_POST['numero'].'\',\'detalle_persona_usuario_ant'.$_POST['numero'].'\',\'detalle_fk_a003_num_responsable_ant'.$_POST['numero'].'\',\'detalle_persona_responsable_ant'.$_POST['numero'].'\'],\'opcion=af_movimientos\');">
                                <i class="md md-search"></i>
                            </button>
                        </div>
                        <div class="col-sm-9">
                            <div class="form-group form-group-sm" id="detalle_desc_activo'.$_POST['numero'].'Error">
                                <textarea name="detalle_desc_activo[]" id="detalle_desc_activo'.$_POST['numero'].'" class="form-control input-sm" disabled></textarea>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-6"><span class="label label-default">Datos Anteriores</span></div>
                        <div class="col-sm-6"><span class="label label-default">Datos Actuales</span></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <select id="detalle_fk_a001_num_organismo_ant'.$_POST['numero'].'" class="form-control input-sm" disabled>
                                    <option value="">&nbsp;</option>
                                    '.Select::lista('a001_organismo','pk_num_organismo','ind_descripcion_empresa').'
                                </select>
                                <label for="detalle_fk_a001_num_organismo_ant'.$_POST['numero'].'">Organismo</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <select name="detalle_fk_a001_num_organismo[]" id="detalle_fk_a001_num_organismo'.$_POST['numero'].'" class="form-control input-sm" onchange="select(\''.BASE_URL.'select/a004_dependencia\', $(\'#detalle_fk_a004_num_dependencia'.$_POST['numero'].'\'), \'fk_a001_num_organismo=\'+$(this).val(), [\'detalle_fk_a023_num_centro_costo'.$_POST['numero'].'\']);">
                                    <option value="">&nbsp;</option>
                                    '.Select::lista('a001_organismo','pk_num_organismo','ind_descripcion_empresa').'
                                </select>
                                <label for="detalle_fk_a001_num_organismo'.$_POST['numero'].'">Organismo</label>
                                <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <select id="detalle_fk_a004_num_dependencia_ant'.$_POST['numero'].'" class="form-control input-sm" disabled>
                                    <option value="">&nbsp;</option>
                                    '.Select::lista('a004_dependencia','pk_num_dependencia','ind_dependencia').'
                                </select>
                                <label for="detalle_fk_a004_num_dependencia_ant'.$_POST['numero'].'">Dependencia</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <select name="detalle_fk_a004_num_dependencia[]" id="detalle_fk_a004_num_dependencia'.$_POST['numero'].'" class="form-control input-sm" onchange="select(\''.BASE_URL.'select/a023_centro_costo\', $(\'#detalle_fk_a023_num_centro_costo'.$_POST['numero'].'\'), \'fk_a004_num_dependencia=\'+$(this).val());">
                                    <option value="">&nbsp;</option>
                                </select>
                                <label for="detalle_fk_a004_num_dependencia'.$_POST['numero'].'">Dependencia</label>
                                <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <select id="detalle_fk_a023_num_centro_costo_ant'.$_POST['numero'].'" class="form-control input-sm" disabled>
                                    <option value="">&nbsp;</option>
                                    '.Select::lista('a023_centro_costo','pk_num_centro_costo','ind_descripcion_centro_costo').'
                                </select>
                                <label for="detalle_fk_a023_num_centro_costo_ant'.$_POST['numero'].'">Centro de Costo</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <select name="detalle_fk_a023_num_centro_costo[]" id="detalle_fk_a023_num_centro_costo'.$_POST['numero'].'" class="form-control input-sm">
                                    <option value="">&nbsp;</option>
                                </select>
                                <label for="detalle_fk_a023_num_centro_costo'.$_POST['numero'].'">Centro de Costo</label>
                                <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <select id="detalle_fk_afc005_num_ubicacion_ant'.$_POST['numero'].'" class="form-control input-sm" disabled>
                                    <option value="">&nbsp;</option>
                                    '.Select::lista('af_c005_ubicacion','pk_num_ubicacion','ind_nombre_ubicacion').'
                                </select>
                                <label for="detalle_fk_afc005_num_ubicacion_ant'.$_POST['numero'].'">Ubicaci&oacute;n</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <select name="detalle_fk_afc005_num_ubicacion[]" id="detalle_fk_afc005_num_ubicacion'.$_POST['numero'].'" class="form-control input-sm">
                                    <option value="">&nbsp;</option>
                                    '.Select::lista('af_c005_ubicacion','pk_num_ubicacion','ind_nombre_ubicacion').'
                                </select>
                                <label for="detalle_fk_afc005_num_ubicacion'.$_POST['numero'].'">Ubicaci&oacute;n</label>
                                <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group form-group-sm">
                                <input type="hidden" id="detalle_fk_a003_num_usuario_ant'.$_POST['numero'].'">
                                <input type="text" id="detalle_persona_usuario_ant'.$_POST['numero'].'" class="form-control input-sm" disabled>
                                <label for="detalle_persona_usuario_ant'.$_POST['numero'].'">Empleado Usuario</label>
                            </div>
                        </div>
                        <div class="col-sm-5 col-md-offset-1">
                            <div class="form-group form-group-sm">
                                <input type="hidden" name="detalle_fk_a003_num_usuario[]" id="detalle_fk_a003_num_usuario'.$_POST['numero'].'">
                                <input type="text" name="detalle_persona_usuario[]" id="detalle_persona_usuario'.$_POST['numero'].'" class="form-control input-sm" disabled>
                                <label for="detalle_persona_usuario'.$_POST['numero'].'">Empleado Usuario</label>
                                <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector" style="margin: 0px 0px -45px -10px;"
                                    data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Empleados"
                                    onclick="selector($(this), \''.BASE_URL.'modAF/ListasCONTROL/selectorEmpleadosMET\', [\'detalle_fk_a003_num_usuario'.$_POST['numero'].'\',\'detalle_persona_usuario'.$_POST['numero'].'\']);">
                                <i class="md md-search"></i>
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group form-group-sm">
                                <input type="hidden" id="detalle_fk_a003_num_responsable_ant'.$_POST['numero'].'">
                                <input type="text" id="detalle_persona_responsable_ant'.$_POST['numero'].'" class="form-control input-sm" maxlength="15" disabled>
                                <label for="detalle_persona_responsable_ant'.$_POST['numero'].'">Empleado Responsable</label>
                            </div>
                        </div>
                        <div class="col-sm-5 col-md-offset-1">
                            <div class="form-group form-group-sm">
                                <input type="hidden" name="detalle_fk_a003_num_responsable[]" id="detalle_fk_a003_num_responsable'.$_POST['numero'].'">
                                <input type="text" name="detalle_persona_responsable[]" id="detalle_persona_responsable'.$_POST['numero'].'" value="" class="form-control input-sm" maxlength="15" disabled>
                                <label for="detalle_persona_responsable'.$_POST['numero'].'">Empleado Responsable</label>
                                <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector" style="margin: 0px 0px -45px -10px;"
                                    data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Empleados"
                                    onclick="selector($(this), \''.BASE_URL.'modAF/ListasCONTROL/selectorEmpleadosMET\', [\'detalle_fk_a003_num_responsable'.$_POST['numero'].'\',\'detalle_persona_responsable'.$_POST['numero'].'\']);">
                                <i class="md md-search"></i>
                            </button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <select name="detalle_fk_afc009_num_movimiento_inc[]" id="detalle_fk_afc009_num_movimiento_inc'.$_POST['numero'].'" class="form-control input-sm">
                                    <option value="">&nbsp;</option>
                                    '.Select::lista('af_c009_tipo_movimiento','pk_num_movimiento','ind_nombre_movimiento',20).'
                                </select>
                                <label for="detalle_fk_afc009_num_movimiento_inc{$numero}">Tipo de Movimiento</label>
                                <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <select name="detalle_fk_afc009_num_movimiento_des[]" id="detalle_fk_afc009_num_movimiento_des'.$_POST['numero'].'" class="form-control input-sm">
                                    <option value="">&nbsp;</option>
                                    '.Select::lista('af_c009_tipo_movimiento','pk_num_movimiento','ind_nombre_movimiento',2).'
                                </select>
                                <label for="detalle_fk_afc009_num_movimiento_des{$numero}">Tipo de Movimiento</label>
                                <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>';

        echo $tr;
    }

    /**
     * Motivos de Traslado.
     *
     * @return String
     */
    public function metSelectMotivoTraslado()
    {
        echo '<option value="">&nbsp;</option>';
        if ($_POST['ind_tipo'] == 'I')
        {
            echo Select::miscelaneo('AFMOTTRASI');
        }
        else{
            echo Select::miscelaneo('AFMOTTRASE');
        }
    }
}
