<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Generación de Vouchers Transacciones de Baja (Pub.20)
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |10-02-2017               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'Fecha.php';
require_once ROOT.'librerias'.DS.'Number.php';

class VoucherBaja20Controlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->VoucherBaja20 = $this->metCargarModelo('VoucherBaja20','vouchers');
        $this->BajaActivo = $this->metCargarModelo('BajaActivo','procesos');
        $this->TipoTransaccion = $this->metCargarModelo('TipoTransaccion','maestros');
    }

    /**
     * Método para cargar vista Listado de Registros.
     *
     * @return Response
     */
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-ui-selectable/jquery-ui',
            'bootstrap-datepicker/datepicker',
        );
        $complementoJs = array(
            'mask/jquery.mask',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-ui-selectable/jquery-ui',
            'bootstrap-datepicker/bootstrap-datepicker',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado', $this->VoucherBaja20->metListar());
        $this->atVista->metRenderizar('listado');
    }

    /**
     * Método para cargar vista formulario Editar Registro.
     *
     * @return Response
     */
    public function metMostrar()
    {
        ##  valores del formulario
        $field_activo = $this->VoucherBaja20->metObtener($_POST['id']);
        $fk_cbb002_num_libro_contable = Select::field('cb_b002_libro_contable', 'pk_num_libro_contable', ['cod_libro'=>'CO']);
        $fk_cbb005_num_contabilidades = Select::field('cb_b005_contabilidades', 'pk_num_contabilidades', ['ind_tipo_contabilidad'=>'F']);
        $field_cuentas = $this->BajaActivo->metObtenerCuentas($field_activo['pk_num_baja_activo'], $fk_cbb005_num_contabilidades);

        $form = [
            'metodo' => 'generar',
            'pk_num_activo' => $field_activo['pk_num_activo'],
            'pk_num_baja_activo' => $field_activo['pk_num_baja_activo'],
            'fk_a001_num_organismo' => $field_activo['fk_a001_num_organismo'],
            'fk_a004_num_dependencia' => $field_activo['fk_a004_num_dependencia'],
            'fk_cbc003_num_voucher' => Select::field('cb_c003_tipo_voucher', 'pk_num_voucher', ['cod_voucher'=>'FA']),
            'fk_cbc002_num_sistema_fuente' => Select::field('cb_c002_sistema_fuente', 'pk_num_sistema_fuente', ['cod_sistema_fuente'=>'AUTOCONT']),
            'fk_cbb005_num_contabilidades' => $fk_cbb005_num_contabilidades,
            'fk_cbb002_num_libro_contable' => $fk_cbb002_num_libro_contable,
            'fk_cbb003_num_libro_contabilidad' => Select::field('cb_b003_libro_contabilidad', 'pk_num_libro_contabilidad', ['fk_cbb002_num_libro_contable'=>$fk_cbb002_num_libro_contable,'fk_cbb005_num_contabilidades'=>$fk_cbb005_num_contabilidades]),
            'ind_anio' => date('Y'),
            'ind_mes' => date('m'),
            'ind_voucher'=> '',
            'fec_fecha_voucher' => date('Y-m-d'),
            'txt_titulo_voucher'=> $field_activo['ind_descripcion'],
            'fk_a003_num_preparado_por'=> '',
            'preparado_por'=> '',
            'fk_a003_num_aprobado_por'=> '',
            'aprobado_por'=> '',
            'num_monto' => $field_activo['num_monto'],
            'fk_a023_num_centro_costo' => $field_activo['fk_a023_num_centro_costo'],
            'centro_costo' => $field_activo['centro_costo'],
            'fk_a003_num_persona' => $field_activo['fk_a003_num_proveedor'],
            'persona' => $field_activo['ind_documento_fiscal_proveedor'],
            'lista_cuentas' => $field_cuentas,
        ];

        ##  vista
        $this->atVista->assign('form', $form);
        $this->atVista->metRenderizar('formulario', 'modales');
    }

    /**
     * Método para crear un nuevo registro.
     *
     * @return Void
     */
    public function metGenerar()
    {
        $_POST['fec_fecha_voucher'] = Fecha::formatFecha($_POST['fec_fecha_voucher']);
        $_POST['ind_anio'] = substr($_POST['ind_periodo'], 0, 4);
        $_POST['ind_mes'] = substr($_POST['ind_periodo'], 5, 2);

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'fec_fecha_voucher' => 'required|date',
            'txt_titulo_voucher' => 'required',
            'num_creditos' => 'required|equalsfield,num_debitos,Débitos',
        ]);
        $gump->set_field_name("fec_fecha_voucher", "Fecha");
        $gump->set_field_name("txt_titulo_voucher", "Descripción");
        $gump->set_field_name("num_creditos", "Créditos");
        $gump->set_field_name("num_debitos", "Débitos");
        ##  
        if (!$this->VoucherBaja20->metValidarPeriodo($_POST['ind_mes'], $_POST['ind_anio']))
        {
            die(json_encode([
                'status' => 'error',
                'mensaje' => ['El Periodo no se ha Aperturado'],
            ]));
        }

        ##  validación exitosa
        if($validate === TRUE) {
            ##  crear registro
            $id = $this->VoucherBaja20->metGenerar($_POST);
            ##  devolver registro creado
            $jsondata = [
                'status' => 'crear',
                'mensaje' => 'Voucher generado exitosamente',
                'id' => $id,
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate,
                'mensaje' => $gump->get_readable_errors(),
            ];
        }

        echo json_encode($jsondata);
        exit();
    }
}
