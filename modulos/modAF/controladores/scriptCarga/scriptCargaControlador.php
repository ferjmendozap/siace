<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Scripts de Carga
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |29-06-2016               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'Select.php';
require_once 'dataAFMenu.php';
require_once 'dataAFMiscelaneo.php';
require_once 'dataAFParametro.php';
require_once 'dataAF.php';

class scriptCargaControlador extends Controlador
{
    use dataAFMenu;
    use dataAFMiscelaneo;
    use dataAFParametro;
    use dataAF;
    private $atScriptCarga;

    public function __construct()
    {
        parent::__construct();
        $this->atScriptCarga = $this->metCargarModelo('scriptCarga');
    }

    /**
     * Index para cargar todos los maestros.
     *
     * @return Void
     */
    public function metIndex()
    {
        ### Activos Fijos ###
        //  http://localhost/SIACE/scriptCarga
        //  http://localhost/SIACE/modCB/scriptCargaCONTROL
        //  http://localhost/SIACE/modAF/scriptCarga/scriptCargaCONTROL
        //  http://localhost/SIACE/modAF/scriptCarga/scriptCargaCONTROL/AFMenuMET
        $this->metAF();

        echo "TERMINADO";
    }

    /**
     * Método para cargar maestros.
     *
     * @return Void
     */
    public function metAF()
    {
        echo 'INICIO CARGA DE MAESTROS MODULO ACTIVOS FIJOS<br><br>';
        echo '-----Carga Situación del Activo-----<br><br>';
        $this->atScriptCarga->metCargarSituacion($this->metSituacion());
        echo '-----Carga Ubicación del Activo-----<br><br>';
        $this->atScriptCarga->metCargarUbicacion($this->metUbicacion());
        echo '-----Carga Tipos de Seguro-----<br><br>';
        $this->atScriptCarga->metCargarTipoSeguro($this->metTipoSeguro());
        echo '-----Carga Tipos de Vehículos-----<br><br>';
        $this->atScriptCarga->metCargarTipoVehiculo($this->metTipoVehiculo());
        echo '-----Carga Tipos de Movimientos-----<br><br>';
        $this->atScriptCarga->metCargarTipoMovimiento($this->metTipoMovimiento());
        echo '-----Carga clasificación del Activo-----<br><br>';
        $this->atScriptCarga->metCargarClasificacion($this->metClasificacion());
        echo '-----Carga Categorias de Depreciación-----<br><br>';
        $this->atScriptCarga->metCargarCategoria($this->metCategoria());
        echo '-----Carga Tipo de Transacciones-----<br><br>';
        $this->atScriptCarga->metCargarTipoTransaccion($this->metTipoTransaccion());
        echo 'FIN CARGA DE ACTIVOS FIJOS<br><br>';
    }

    /**
     * Método para cargar Menu.
     *
     * @return Void
     */
    public function metAFMenu()
    {
        echo 'INICIO CARGA MENU MODULO ACTIVOS FIJOS<br><br>';
        echo '-----Carga Menu-----<br><br>';
        $this->atScriptCarga->metCargarMenu($this->metDataMenu());
        echo 'FIN CARGA MENU DE ACTIVOS FIJOS<br><br>';
    }
}
