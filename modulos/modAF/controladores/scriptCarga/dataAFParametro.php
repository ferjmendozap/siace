<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Activos Fijos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Edgar Bolívar        | bedgar3000@gmail.com                 | 0424-9201982
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
trait dataAFParametro
{

      /**
      * Menu
      *
      * @return Array
      */
      public function metAFParametro()
      {
            $data = array(
                  array('ind_descripcion' => 'DEPENDENCIA DEFAULT MÓDULO DE ACTIVOS FIJOS', 'ind_explicacion' => 'INFORMACIÓN A SER UTILIZADA EN LOS REPORTES DEL MÓDULO', 'ind_parametro_clave' => 'DEPENDAFDEF', 'ind_valor_parametro' => '12', 'fk_a015_num_seguridad_aplicacion' => '8','cod_detalle' => 'N','cod_maestro' => 'TPPARAMET'),
                  array('ind_descripcion' => 'PERSONA CONFORMACIÓN DEFAULT ACTIVOS FIJOS', 'ind_explicacion' => 'ID DE LA PERSONA POR DEFECTO PARA LAS FIRMAS DE CONFORMACIÓN EN LOS REPORTES DEL MÓDULO', 'ind_parametro_clave' => 'CONFAFDEF', 'ind_valor_parametro' => '1', 'fk_a015_num_seguridad_aplicacion' => '8','cod_detalle' => 'N','cod_maestro' => 'TPPARAMET')
            );

            return $data;
      }
}
