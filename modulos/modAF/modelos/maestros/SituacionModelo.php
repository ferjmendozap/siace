<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Maestro Tipos de Seguro
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |13-11-2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class SituacionModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /**
     * Método para listar los registros de la tabla.
     *
     * @return Array
     */
    public function metListar()
    {
        $db = $this->_db->query(
            "SELECT * FROM af_c007_situacion"
        );
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /**
     * Método para crear un nuevo registro.
     *
     * @param Array
     * @return Integer
     */
    public function metCrear($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $sql = "INSERT INTO af_c007_situacion
                SET
                    ind_nombre_situacion = '$data[ind_nombre_situacion]',
                    num_flag_depreciacion = '$data[num_flag_depreciacion]',
                    num_flag_revaluacion = '$data[num_flag_revaluacion]',
                    num_flag_sistema = '$data[num_flag_sistema]',
                    num_estatus = '$data[num_estatus]',
                    fk_a018_num_seguridad_usuario = '$this->idUsuario',
                    fec_ultima_modificacion = NOW()";
        $db = $this->_db->prepare($sql);
        $db->execute();

        $id = $this->_db->lastInsertId();

        ##  Consigna una transacción
        $this->_db->commit();

        return $id;
    }

    /**
     * Método para obtener un registro de la tabla.
     *
     * @param Integer
     * @return Array
     */
    public function metObtener($id)
    {
        $row = $this->_db->query(
            "SELECT * FROM af_c007_situacion WHERE pk_num_situacion = '$id'"
        );
        $row->setFetchMode(PDO::FETCH_ASSOC);
        return $row->fetch();
    }

    /**
     * Método para actualizar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metModificar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $sql = "UPDATE af_c007_situacion
                SET
                    ind_nombre_situacion = '$data[ind_nombre_situacion]',
                    num_flag_depreciacion = '$data[num_flag_depreciacion]',
                    num_flag_revaluacion = '$data[num_flag_revaluacion]',
                    num_flag_sistema = '$data[num_flag_sistema]',
                    num_estatus = '$data[num_estatus]',
                    fk_a018_num_seguridad_usuario = '$this->idUsuario',
                    fec_ultima_modificacion = NOW()
                WHERE pk_num_situacion = '$data[pk_num_situacion]'";
        $db = $this->_db->prepare($sql);
        $db->execute();

        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para eliminar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metEliminar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $sql = "DELETE FROM af_c007_situacion WHERE pk_num_situacion = '$data[pk_num_situacion]'";
        $db = $this->_db->prepare($sql);
        $db->execute();

        ##  Consigna una transacción
        $this->_db->commit();
    }
}
