<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Maestro Clasificaci�n de Activos
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bol�var                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |24-08-2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class ClasificacionModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /**
     * M�todo para listar los registros de la tabla.
     *
     * @return Array
     */
    public function metListar()
    {
        $db = $this->_db->query(
            "SELECT
                cl.*,
                co.ind_descripcion AS contabilidad
             FROM
                af_c008_clasificacion cl
                INNER JOIN cb_b005_contabilidades co ON (
                    cl.fk_cbb005_num_contabilidad = co.pk_num_contabilidades
                )"
        );
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /**
     * M�todo para crear un nuevo registro.
     *
     * @param Array
     * @return Integer
     */
    public function metCrear($data)
    {
        ##  Inicia una transacci�n
        $this->_db->beginTransaction();

        $sql = "INSERT INTO af_c008_clasificacion
                SET
                    ind_codigo = '$data[ind_codigo]',
                    ind_nombre_clasificacion = '$data[ind_nombre_clasificacion]',
                    fk_cbb005_num_contabilidad = '$data[fk_cbb005_num_contabilidad]',
                    num_nivel = '$data[num_nivel]',
                    num_estatus = '$data[num_estatus]',
                    fk_a018_num_seguridad_usuario = '$this->idUsuario',
                    fec_ultima_modificacion = NOW()";
        $db = $this->_db->prepare($sql);
        $db->execute();

        $id = $this->_db->lastInsertId();

        ##  Consigna una transacci�n
        $this->_db->commit();

        return $id;
    }

    /**
     * M�todo para obtener un registro de la tabla.
     *
     * @param Integer
     * @return Array
     */
    public function metObtener($id)
    {
        $sql = "SELECT
                    cl.*,
                    co.ind_descripcion AS contabilidad
                FROM
                    af_c008_clasificacion cl
                    INNER JOIN cb_b005_contabilidades co ON (
                        cl.fk_cbb005_num_contabilidad = co.pk_num_contabilidades
                    )
                WHERE cl.pk_num_clasificacion = '$id'";
        $row = $this->_db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        return $row->fetch();
    }

    /**
     * M�todo para actualizar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metModificar($data)
    {
        ##  Inicia una transacci�n
        $this->_db->beginTransaction();

        $sql = "UPDATE af_c008_clasificacion
                SET
                    ind_codigo = '$data[ind_codigo]',
                    ind_nombre_clasificacion = '$data[ind_nombre_clasificacion]',
                    fk_cbb005_num_contabilidad = '$data[fk_cbb005_num_contabilidad]',
                    num_nivel = '$data[num_nivel]',
                    num_estatus = '$data[num_estatus]',
                    fk_a018_num_seguridad_usuario = '$this->idUsuario',
                    fec_ultima_modificacion = NOW()
                WHERE pk_num_clasificacion = '$data[pk_num_clasificacion]'";
        $db = $this->_db->prepare($sql);
        $db->execute();

        ##  Consigna una transacci�n
        $this->_db->commit();
    }

    /**
     * M�todo para eliminar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metEliminar($data)
    {
        ##  Inicia una transacci�n
        $this->_db->beginTransaction();

        $sql = "DELETE FROM af_c008_clasificacion WHERE pk_num_clasificacion = '$data[pk_num_clasificacion]'";
        $db = $this->_db->prepare($sql);
        $db->execute();

        ##  Consigna una transacci�n
        $this->_db->commit();
    }
}
