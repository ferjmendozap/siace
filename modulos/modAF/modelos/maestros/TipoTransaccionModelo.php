<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Maestro Tipos de Transacción
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |20-11-2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class TipoTransaccionModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /**
     * Método para listar los registros de la tabla.
     *
     * @return Array
     */
    public function metListar()
    {
        $db = $this->_db->query(
            "SELECT
                tt.*,
                tv.cod_voucher
              FROM
                af_c021_tipo_transaccion tt
                INNER JOIN cb_c003_tipo_voucher tv ON (tv.pk_num_voucher = tt.fk_cbc003_num_voucher)"
        );
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /**
     * Método para obtener un registro de la tabla.
     *
     * @param Integer
     * @return Array
     */
    public function metObtener($id)
    {
        $sql = "SELECT * FROM af_c021_tipo_transaccion WHERE pk_num_tipo_transaccion = '$id'";
        $row = $this->_db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        return $row->fetch();
    }

    /**
     * Método para obtener las cuentas asociadas al registro.
     *
     * @param Integer
     * @return Array
     */
    public function metObtenerCuentas($fk_afc021_num_tipo_transaccion)
    {
        $sql = "SELECT
                  ttc.*,
                  c.ind_codigo AS num_categoria,
                  pc.cod_cuenta AS num_cuenta,
                  pc.ind_descripcion AS nombre_cuenta,
                  ct.ind_descripcion AS nombre_contabilidad
                FROM
                  af_c022_tipo_transaccion_cuenta ttc
                  INNER JOIN cb_b004_plan_cuenta pc ON (pc.pk_num_cuenta = ttc.fk_cbb004_num_cuenta)
                  INNER JOIN af_c019_categoria c ON (c.pk_num_categoria = ttc.fk_afc019_num_categoria)
                  INNER JOIN cb_b005_contabilidades ct ON (ct.pk_num_contabilidades = ttc.fk_cbb005_num_contabilidades)
                WHERE ttc.fk_afc021_num_tipo_transaccion = '$fk_afc021_num_tipo_transaccion'
                ORDER BY num_cuenta";
        $db = $this->_db->query($sql);
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /**
     * Método para obtener las cuentas asociadas al registro.
     *
     * @param Integer
     * @return Array
     */
    public function metObtenerCuentasContabilidad($fk_afc021_num_tipo_transaccion)
    {
    	##	obtengo el tipo de contabilidad por defecto
        $CONTABILIDAD = Select::parametros('CONTABILIDAD');
        if ($CONTABILIDAD == 'PUB20') $tipo_contabilidad = 'F'; else $tipo_contabilidad = 'T';
        $num_contabilidades = Select::field('cb_b005_contabilidades','pk_num_contabilidades',['ind_tipo_contabilidad' => $tipo_contabilidad]);
        ##	
        $sql = "SELECT
                  ttc.*,
                  c.ind_codigo AS num_categoria,
                  pc.cod_cuenta AS num_cuenta,
                  pc.ind_descripcion AS nombre_cuenta,
                  ct.ind_descripcion AS nombre_contabilidad
                FROM
                  af_c022_tipo_transaccion_cuenta ttc
                  INNER JOIN cb_b004_plan_cuenta pc ON (pc.pk_num_cuenta = ttc.fk_cbb004_num_cuenta)
                  INNER JOIN af_c019_categoria c ON (c.pk_num_categoria = ttc.fk_afc019_num_categoria)
                  INNER JOIN cb_b005_contabilidades ct ON (ct.pk_num_contabilidades = ttc.fk_cbb005_num_contabilidades)
                WHERE
                	ttc.fk_afc021_num_tipo_transaccion = '$fk_afc021_num_tipo_transaccion'
                	AND ttc.fk_cbb005_num_contabilidades = '$num_contabilidades'
                ORDER BY num_cuenta";
        $db = $this->_db->query($sql);
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /**
     * Método para crear un nuevo registro.
     *
     * @param Array
     * @return Integer
     */
    public function metCrear($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        //  registro
        $sql = "INSERT INTO af_c021_tipo_transaccion
                SET
                  ind_codigo = '$data[ind_codigo]',
                  ind_nombre_transaccion = '$data[ind_nombre_transaccion]',
                  fk_cbc003_num_voucher = '$data[fk_cbc003_num_voucher]',
                  ind_tipo = '$data[ind_tipo]',
                  num_flag_sistema = '$data[num_flag_sistema]',
                  num_estatus = '$data[num_estatus]',
                  fk_a018_num_seguridad_usuario = '$this->idUsuario',
                  fec_ultima_modificacion = NOW()";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        $id = $this->_db->lastInsertId();

        //  cuentas
        if (isset($data['detalle_fk_afc019_num_categoria'])) {
          for ($i=0; $i < count($data['detalle_fk_afc019_num_categoria']); $i++) {
            ##  registro
            $sql = "INSERT INTO af_c022_tipo_transaccion_cuenta
                    SET
                      fk_afc021_num_tipo_transaccion = '$id',
                      fk_afc019_num_categoria = '".$data['detalle_fk_afc019_num_categoria'][$i]."',
                      fk_cbb004_num_cuenta = '".$data['detalle_fk_cbb004_num_cuenta'][$i]."',
                      fk_cbb005_num_contabilidades = '".$data['detalle_fk_cbb005_num_contabilidades'][$i]."',
                      ind_descripcion = '".$data['detalle_ind_descripcion'][$i]."',
                      ind_signo = '".$data['detalle_ind_signo'][$i]."'";
            $db = $this->_db->prepare($sql);
            $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
          }
        }

        ##  Consigna una transacción
        $this->_db->commit();

        return $id;
    }

    /**
     * Método para actualizar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metModificar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $id = $data['pk_num_tipo_transaccion'];

        //  registro
        $sql = "UPDATE af_c021_tipo_transaccion
                SET
                  ind_codigo = '$data[ind_codigo]',
                  ind_nombre_transaccion = '$data[ind_nombre_transaccion]',
                  fk_cbc003_num_voucher = '$data[fk_cbc003_num_voucher]',
                  ind_tipo = '$data[ind_tipo]',
                  num_flag_sistema = '$data[num_flag_sistema]',
                  num_estatus = '$data[num_estatus]',
                  fk_a018_num_seguridad_usuario = '$this->idUsuario',
                  fec_ultima_modificacion = NOW()
                WHERE pk_num_tipo_transaccion = '$data[pk_num_tipo_transaccion]'";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        //  cuentas 
        if (isset($data['detalle_fk_afc019_num_categoria'])) 
        {
          $sql = "DELETE FROM af_c022_tipo_transaccion_cuenta 
                  WHERE
                    fk_afc021_num_tipo_transaccion = '$id'
                    AND pk_num_tipo_transaccion_cuenta NOT IN (".implode(",",$data['detalle_pk_num_tipo_transaccion_cuenta']).")";
          $db = $this->_db->prepare($sql);
          $db->execute();
          ## 
          for ($i=0; $i < count($data['detalle_fk_afc019_num_categoria']); $i++) {
            ##  registro
            $sql = "REPLACE INTO af_c022_tipo_transaccion_cuenta
                    SET
                      pk_num_tipo_transaccion_cuenta = '".$data['detalle_pk_num_tipo_transaccion_cuenta'][$i]."',
                      fk_afc021_num_tipo_transaccion = '$id',
                      fk_afc019_num_categoria = '".$data['detalle_fk_afc019_num_categoria'][$i]."',
                      fk_cbb004_num_cuenta = '".$data['detalle_fk_cbb004_num_cuenta'][$i]."',
                      fk_cbb005_num_contabilidades = '".$data['detalle_fk_cbb005_num_contabilidades'][$i]."',
                      ind_descripcion = '".$data['detalle_ind_descripcion'][$i]."',
                      ind_signo = '".$data['detalle_ind_signo'][$i]."'";
            $db = $this->_db->prepare($sql);
            $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
          }
        }
        else
        {
          $sql = "DELETE FROM af_c022_tipo_transaccion_cuenta WHERE fk_afc021_num_tipo_transaccion = '$id'";
          $db = $this->_db->prepare($sql);
          $db->execute();
        }

        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para eliminar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metEliminar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $sql = "DELETE FROM af_c021_tipo_transaccion WHERE pk_num_tipo_transaccion = '$data[pk_num_tipo_transaccion]'";
        $db = $this->_db->prepare($sql);
        $db->execute();

        ##  Consigna una transacción
        $this->_db->commit();
    }
}
