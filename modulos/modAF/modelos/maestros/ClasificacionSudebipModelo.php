<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Maestro Clasificación SUDEBIP
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |15-02-2017               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class ClasificacionSudebipModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /**
     * Método para listar los registros de la tabla.
     *
     * @return Array
     */
    public function metListar()
    {
        $db = $this->_db->query(
            "SELECT * FROM af_c032_clasificacion_sudebip"
        );
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /**
     * Método para crear un nuevo registro.
     *
     * @param Array
     * @return Integer
     */
    public function metCrear($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $sql = "INSERT INTO af_c032_clasificacion_sudebip
                SET
                    ind_codigo = '$data[ind_codigo]',
                    ind_nombre = '$data[ind_nombre]',
                    num_nivel = '$data[num_nivel]',
                    num_estatus = '$data[num_estatus]',
                    fk_a018_num_seguridad_usuario = '$this->idUsuario',
                    fec_ultima_modificacion = NOW()";
        $db = $this->_db->prepare($sql);
        $db->execute();

        $id = $this->_db->lastInsertId();

        ##  Consigna una transacción
        $this->_db->commit();

        return $id;
    }

    /**
     * Método para obtener un registro de la tabla.
     *
     * @param Integer
     * @return Array
     */
    public function metObtener($id)
    {
        $sql = "SELECT * FROM af_c032_clasificacion_sudebip WHERE pk_num_clasificacion = '$id'";
        $row = $this->_db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        return $row->fetch();
    }

    /**
     * Método para actualizar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metModificar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $sql = "UPDATE af_c032_clasificacion_sudebip
                SET
                    ind_codigo = '$data[ind_codigo]',
                    ind_nombre = '$data[ind_nombre]',
                    num_nivel = '$data[num_nivel]',
                    num_estatus = '$data[num_estatus]',
                    fk_a018_num_seguridad_usuario = '$this->idUsuario',
                    fec_ultima_modificacion = NOW()
                WHERE pk_num_clasificacion = '$data[pk_num_clasificacion]'";
        $db = $this->_db->prepare($sql);
        $db->execute();

        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para eliminar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metEliminar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $sql = "DELETE FROM af_c032_clasificacion_sudebip WHERE pk_num_clasificacion = '$data[pk_num_clasificacion]'";
        $db = $this->_db->prepare($sql);
        $db->execute();

        ##  Consigna una transacción
        $this->_db->commit();
    }
}
