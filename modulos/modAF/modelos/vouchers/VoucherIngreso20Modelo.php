<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Generación de Vouchers de Ingreso (Pub.20)
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |18-01-2017               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class VoucherIngreso20Modelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /**
     * Método para listar los registros de la tabla.
     *
     * @return Array
     */
    public function metListar()
    {
        $sql = "SELECT
                    a.*,
                    cc.ind_descripcion_centro_costo,
                    cc.fk_a004_num_dependencia,
                    d.ind_dependencia,
                    d.fk_a001_num_organismo,
                    o.ind_descripcion_empresa,
                    c.ind_nombre_categoria,
                    u.ind_nombre_ubicacion,
                    CONCAT_WS(' ', p1.ind_nombre1, p1.ind_nombre2, p1.ind_apellido1, p1.ind_apellido2) AS persona_usuario,
                    CONCAT_WS(' ', p2.ind_nombre1, p2.ind_nombre2, p2.ind_apellido1, p2.ind_apellido2) AS persona_responsable
                FROM af_b001_activo a
                INNER JOIN a023_centro_costo cc ON cc.pk_num_centro_costo = a.fk_a023_num_centro_costo
                INNER JOIN a004_dependencia d ON d.pk_num_dependencia = cc.fk_a004_num_dependencia
                INNER JOIN a001_organismo o ON o.pk_num_organismo = d.fk_a001_num_organismo
                INNER JOIN a003_persona p1 ON p1.pk_num_persona = a.fk_a003_num_persona_usuario
                INNER JOIN a003_persona p2 ON p2.pk_num_persona = a.fk_a003_num_persona_responsable
                INNER JOIN af_c019_categoria c ON c.pk_num_categoria = a.fk_afc019_num_categoria
                INNER JOIN af_c005_ubicacion u ON u.pk_num_ubicacion = a.fk_afc005_num_ubicacion
                WHERE
                	a.num_flag_voucher_ingreso = 1
                	AND a.fk_cbb001_num_voucher_ingreso IS NULL
                	AND a.ind_estado = 'AP'";
        $db = $this->_db->query($sql);
        $db->setFetchMode(PDO::FETCH_ASSOC);

        return $db->fetchAll();
    }

    /**
     * Método para obtener un registro de la tabla.
     *
     * @param Integer
     * @return Array
     */
    public function metObtener($id)
    {
        $sql = "SELECT
                    a.*,
                    cc.ind_descripcion_centro_costo,
                    cc.ind_abreviatura AS centro_costo,
                    cc.fk_a004_num_dependencia,
                    d.ind_dependencia,
                    d.fk_a001_num_organismo,
                    o.ind_descripcion_empresa,
                    c.ind_nombre_categoria,
                    u.ind_nombre_ubicacion,
                    CONCAT_WS(' ', p1.ind_nombre1, p1.ind_nombre2, p1.ind_apellido1, p1.ind_apellido2) AS persona_usuario,
                    CONCAT_WS(' ', p2.ind_nombre1, p2.ind_nombre2, p2.ind_apellido1, p2.ind_apellido2) AS persona_responsable,
                    p3.ind_documento_fiscal AS ind_documento_fiscal_proveedor
                FROM af_b001_activo a
                INNER JOIN a023_centro_costo cc ON cc.pk_num_centro_costo = a.fk_a023_num_centro_costo
                INNER JOIN a004_dependencia d ON d.pk_num_dependencia = cc.fk_a004_num_dependencia
                INNER JOIN a001_organismo o ON o.pk_num_organismo = d.fk_a001_num_organismo
                INNER JOIN a003_persona p1 ON p1.pk_num_persona = a.fk_a003_num_persona_usuario
                INNER JOIN a003_persona p2 ON p2.pk_num_persona = a.fk_a003_num_persona_responsable
                LEFT JOIN a003_persona p3 ON p3.pk_num_persona = a.fk_a003_num_proveedor
                INNER JOIN af_c019_categoria c ON c.pk_num_categoria = a.fk_afc019_num_categoria
                INNER JOIN af_c005_ubicacion u ON u.pk_num_ubicacion = a.fk_afc005_num_ubicacion
                WHERE pk_num_activo = '$id'";
        $row = $this->_db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        return $row->fetch();
    }

    /**
     * Método para crear un nuevo registro.
     *
     * @param Array
     * @return Integer
     */
    public function metGenerar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        ##	nro de voucher
        $voucher = $this->metCodigo($data);
        list($ind_voucher, $ind_nro_voucher) = explode('-', $voucher);

        //  inserto
        $sql = "INSERT INTO cb_b001_voucher
                SET
					ind_mes = '$data[ind_mes]',
					ind_anio = '$data[ind_anio]',
					ind_voucher = '$voucher',
					ind_nro_voucher = '$ind_nro_voucher',
					fec_fecha_voucher = '$data[fec_fecha_voucher]',
					txt_titulo_voucher = '$data[txt_titulo_voucher]',
					num_creditos = '$data[num_creditos]',
					num_debitos = '$data[num_debitos]',
					fk_a001_num_organismo = '$data[fk_a001_num_organismo]',
					fk_a004_num_dependencia = '$data[fk_a004_num_dependencia]',
					fk_cbc003_num_voucher = '$data[fk_cbc003_num_voucher]',
					fk_cbb005_num_contabilidades = '$data[fk_cbb005_num_contabilidades]',
					fk_cbc002_num_sistema_fuente = '$data[fk_cbc002_num_sistema_fuente]',
					fk_cbb003_num_libro_contabilidad = '$data[fk_cbb003_num_libro_contabilidad]',
					fk_a003_num_preparado_por = '$this->idUsuario',
					fec_fecha_preparacion = NOW(),
					fk_a003_num_aprobado_por = '$this->idUsuario',
					fec_fecha_aprobacion = NOW(),
					ind_estatus = 'AP',
					fk_a018_num_seguridad_usuario = '$this->idUsuario',
					fec_ultima_modificacion = NOW()";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        $id = $this->_db->lastInsertId();

        //  detalle
        if (isset($data['detalle_fk_cbb004_num_cuenta'])) 
        {
        	$num_linea = 0;
			for ($i=0; $i < count($data['detalle_fk_cbb004_num_cuenta']); $i++) 
			{
				++$num_linea;

				##  inserto
				$sql = "INSERT INTO cb_c001_voucher_det
						SET
							fk_cbb001_num_voucher_mast = '$id',
							ind_mes = '$data[ind_mes]',
							fec_anio = '$data[ind_anio]',
							num_linea = '$num_linea',
							num_debe = '".$data['detalle_num_debe'][$i]."',
							num_haber = '".$data['detalle_num_haber'][$i]."',
							ind_descripcion = '".$data['detalle_ind_descripcion'][$i]."',
							fk_a003_num_persona = '".$data['detalle_fk_a003_num_persona'][$i]."',
							fk_a023_num_centro_costo = '".$data['detalle_fk_a023_num_centro_costo'][$i]."',
							fk_cbb004_num_cuenta = '".$data['detalle_fk_cbb004_num_cuenta'][$i]."',
							fk_cbb005_num_contabilidades = '$data[fk_cbb005_num_contabilidades]',
							fk_cbb003_num_libro_contabilidad = '$data[fk_cbb003_num_libro_contabilidad]',
							ind_estatus = 'AP',
							fk_a018_num_seguridad_usuario = '$this->idUsuario',
							fec_ultima_modificacion = NOW()";
				$db = $this->_db->prepare($sql);
				$db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
			}
        }

        //  actualizo
        $sql = "UPDATE af_b001_activo SET fk_cbb001_num_voucher_ingreso = '$id' WHERE pk_num_activo = '$data[pk_num_activo]'";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        ##  Consigna una transacción
        $this->_db->commit();

        return $id;
    }

    /**
     * Método para validar el periodo contable.
     *
     * @param String
     * @param String
     * @return Array
     */
    public function metValidarPeriodo($ind_mes, $fec_anio)
    {
        $sql = "SELECT *
        		FROM cb_c005_control_cierre_mensual
        		WHERE
        			ind_mes = '$ind_mes'
        			AND fec_anio = '$fec_anio'
        			AND ind_estatus = 'A'";
        $row = $this->_db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        $field = $row->fetch();

        if (empty($field['pk_num_control_cierre'])) return false; else return true;
    }

    /**
     * Método para obtener el nro de voucher por periodo.
     *
     * @param Array
     * @return Array
     */
    public function metCodigo($data)
    {
        ##	codigo de voucher
        $cod_voucher = Select::field('cb_c003_tipo_voucher', 'cod_voucher', ['pk_num_voucher'=>$data['fk_cbc003_num_voucher']]);

    	##	ultimo nro de voucher generado
        $sql = "SELECT MAX(ind_nro_voucher) AS ind_nro_voucher
        		FROM cb_b001_voucher
        		WHERE
        			ind_mes = '$data[ind_mes]'
        			AND ind_anio = '$data[ind_anio]'
        			AND fk_cbb005_num_contabilidades = '$data[fk_cbb005_num_contabilidades]'
        			AND fk_a001_num_organismo = '$data[fk_a001_num_organismo]'
        			AND fk_cbc003_num_voucher = '$data[fk_cbc003_num_voucher]'";
        $row = $this->_db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        $field = $row->fetch();

        ##	genero siguiente nro
        $codigo = (int) ($field['ind_nro_voucher'] + 1);
        $codigo = (string) str_repeat("0", 4 - strlen($codigo)) . $codigo;
		$ind_voucher = $cod_voucher . "-" . $codigo;

		return $ind_voucher;		
    }
}
