<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Activos
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |01-12-2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class ActivosTransferirModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /**
     * Método para listar los registros de la tabla.
     *
     * @return Array
     */
    public function metListar($ind_estado=null, $filtro=null)
    {
		$filtrar = "";
		/*if (isset($filtro['fpk_num_organismo'])) if ($filtro['fpk_num_organismo'] != "") $filtrar .= " AND d.fk_a001_num_organismo = '$filtro[fpk_num_organismo]'";
		if (isset($filtro['fpk_num_dependencia'])) if ($filtro['fpk_num_dependencia'] != "") $filtrar .= " AND cc.fk_a004_num_dependencia = '$filtro[fpk_num_dependencia]'";
		if (isset($filtro['fpk_num_centro_costo'])) if ($filtro['fpk_num_centro_costo'] != "") $filtrar .= " AND a.fk_a023_num_centro_costo = '$filtro[fpk_num_centro_costo]'";
		if (isset($filtro['fpk_num_categoria'])) if ($filtro['fpk_num_categoria'] != "") $filtrar .= " AND a.fk_afc019_num_categoria = '$filtro[fpk_num_categoria]'";
		if (isset($filtro['fpk_num_clasificacion'])) if ($filtro['fpk_num_clasificacion'] != "") $filtrar .= " AND a.fk_afc008_num_clasificacion = '$filtro[fpk_num_clasificacion]'";
		if (isset($filtro['fpk_num_movimiento'])) if ($filtro['fpk_num_movimiento'] != "") $filtrar .= " AND a.fk_afc009_num_movimiento = '$filtro[fpk_num_movimiento]'";
		if (isset($filtro['fpk_num_ubicacion'])) if ($filtro['fpk_num_ubicacion'] != "") $filtrar .= " AND a.fk_afc005_num_ubicacion = '$filtro[fpk_num_ubicacion]'";
		if (isset($filtro['fpk_num_situacion'])) if ($filtro['fpk_num_situacion'] != "") $filtrar .= " AND a.fk_afc007_num_situacion = '$filtro[fpk_num_situacion]'";
		if (isset($filtro['fnum_tipo_activo'])) if ($filtro['fnum_tipo_activo'] != "") $filtrar .= " AND a.fk_a006_num_tipo_activo = '$filtro[fnum_tipo_activo]'";*/
		if ($ind_estado) $filtrar .= " AND af.ind_estado = '$ind_estado'";

		$sql = "SELECT
					af.*,
					o.ind_orden,
					od.num_secuencia AS num_secuencia_orden,
					c.ind_descripcion,
					a.pk_num_activo,
					a.cod_codigo_interno,
					a.ind_descripcion AS ind_descripcion_activo
				FROM lg_e004_activo_fijo af
				INNER JOIN lg_c009_orden_detalle od ON (od.pk_num_orden_detalle = af.fk_lgc009_num_orden_detalle)
				INNER JOIN lg_b019_orden o ON (o.pk_num_orden = od.fk_lgb019_num_orden)
				INNER JOIN lg_b003_commodity c ON (c.pk_num_commodity = od.fk_lgb003_num_commodity)
				LEFT JOIN af_b001_activo a ON (a.pk_num_activo = af.fk_afb001_num_activo)
				WHERE 1 $filtrar";
		$db = $this->_db->query($sql);
		$db->setFetchMode(PDO::FETCH_ASSOC);
		return $db->fetchAll();
    }

    /**
     * Método para obtener los datos del activo desde logistica.
     *
     * @param Integer
     * @return Array
     */
    public function metObtenerPendiente($id)
    {
        $sql = "SELECT
					af.*,
					c.ind_descripcion,
					cc.fk_a004_num_dependencia,
					d.fk_a001_num_organismo,
					od.num_precio_unitario_iva AS num_monto,
					lgb022.fk_a003_num_persona_proveedor,
					CONCAT_WS(' ',a003.ind_nombre1,a003.ind_nombre2,a003.ind_apellido1,a003.ind_apellido2) AS proveedor,
					cpd001.fk_cpb002_num_tipo_documento,
					cpd001.ind_nro_control,
					cpd001.ind_nro_factura,
					cpd001.fec_factura,
					cpd001.fec_documento,
					CONCAT_WS('-',o.ind_orden,o.fec_anio) AS ind_nro_orden_compra,
					o.fec_creacion AS fec_orden_compra,
					o.ind_descripcion AS ind_observaciones,
					lgd001.ind_num_documento_referencia AS ind_nro_documento_almacen,
					lgd001.ind_nota_entrega_factura,
					lgd001.fec_documento AS fec_documento_almacen
                FROM lg_e004_activo_fijo af
				INNER JOIN lg_c009_orden_detalle od ON (od.pk_num_orden_detalle = af.fk_lgc009_num_orden_detalle)
				INNER JOIN lg_b019_orden o ON (o.pk_num_orden = od.fk_lgb019_num_orden)
				INNER JOIN lg_b003_commodity c ON (c.pk_num_commodity = od.fk_lgb003_num_commodity)
				INNER JOIN a023_centro_costo cc ON (cc.pk_num_centro_costo = af.fk_a023_num_centro_costo)
				INNER JOIN a004_dependencia d ON (d.pk_num_dependencia = cc.fk_a004_num_dependencia)
				INNER JOIN lg_b022_proveedor lgb022 ON (lgb022.pk_num_proveedor = o.fk_lgb022_num_proveedor)
				INNER JOIN a003_persona a003 ON (a003.pk_num_persona = lgb022.fk_a003_num_persona_proveedor)
				INNER JOIN lg_d002_transaccion_detalle lgd002 ON (lgd002.pk_num_transaccion_detalle = af.fk_lgd002_num_transaccion_detalle)
				INNER JOIN lg_d001_transaccion lgd001 ON (lgd001.pk_num_transaccion = lgd002.fk_lgd001_num_transaccion)
				LEFT JOIN cp_d001_obligacion cpd001 ON (cpd001.pk_num_obligacion = af.fk_cpd001_num_obligacion)
                WHERE af.pk_num_activo_fijo = '$id'";
        $row = $this->_db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        return $row->fetch();
    }
}
