<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Activos
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |01-12-2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class ActivosModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /**
     * Método para listar los registros de la tabla.
     *
     * @return Array
     */
    public function metListar($filtro=null)
    {
        $filtrar = "";
        if (!empty($filtro['fpk_num_organismo'])) $filtrar .= " AND d.fk_a001_num_organismo = '$filtro[fpk_num_organismo]'";
        if (!empty($filtro['fpk_num_dependencia'])) $filtrar .= " AND cc.fk_a004_num_dependencia = '$filtro[fpk_num_dependencia]'";
        if (!empty($filtro['fpk_num_centro_costo'])) $filtrar .= " AND a.fk_a023_num_centro_costo = '$filtro[fpk_num_centro_costo]'";
        if (!empty($filtro['fpk_num_categoria'])) $filtrar .= " AND a.fk_afc019_num_categoria = '$filtro[fpk_num_categoria]'";
       
        if (!empty($filtro['fpk_num_clasificacion'])) $filtrar .= " AND a.fk_afc008_num_clasificacion = '$filtro[fpk_num_clasificacion]'";

        if (!empty($filtro['fpk_num_clasificacion'])) $filtrar .= " AND a.fk_afc032_num_clasificacion_sudebip = '$filtro[fpk_num_clasificacion]'";
        if (!empty($filtro['fpk_num_movimiento'])) $filtrar .= " AND a.fk_afc009_num_movimiento = '$filtro[fpk_num_movimiento]'";
        if (!empty($filtro['fpk_num_ubicacion'])) $filtrar .= " AND a.fk_afc005_num_ubicacion = '$filtro[fpk_num_ubicacion]'";
        if (!empty($filtro['fpk_num_situacion'])) $filtrar .= " AND a.fk_afc007_num_situacion = '$filtro[fpk_num_situacion]'";
        if (!empty($filtro['fnum_tipo_activo'])) $filtrar .= " AND a.fk_a006_num_tipo_activo = '$filtro[fnum_tipo_activo]'";
        if (!empty($filtro['find_estado'])) $filtrar .= " AND a.ind_estado = '$filtro[find_estado]'";


        $sql = "SELECT
                    a.*,
                    cc.ind_descripcion_centro_costo,
                    d.pk_num_dependencia,
                    d.ind_dependencia,
                    o.pk_num_organismo,
                    o.ind_descripcion_empresa,
                    c.ind_nombre_categoria,
                    u.ind_nombre_ubicacion,
                    cl.pk_num_clasificacion,
                    CONCAT_WS(' ', p1.ind_nombre1, p1.ind_nombre2, p1.ind_apellido1, p1.ind_apellido2) AS persona_usuario,
                    CONCAT_WS(' ', p2.ind_nombre1, p2.ind_nombre2, p2.ind_apellido1, p2.ind_apellido2) AS persona_responsable
                FROM
                    af_b001_activo a
                    INNER JOIN a023_centro_costo cc ON (cc.pk_num_centro_costo = a.fk_a023_num_centro_costo)
                    INNER JOIN a004_dependencia d ON (d.pk_num_dependencia = cc.fk_a004_num_dependencia)
                    INNER JOIN a001_organismo o ON (o.pk_num_organismo = d.fk_a001_num_organismo)
                    INNER JOIN a003_persona p1 ON (p1.pk_num_persona = a.fk_a003_num_persona_usuario)
                    INNER JOIN a003_persona p2 ON (p2.pk_num_persona = a.fk_a003_num_persona_responsable)
                    INNER JOIN af_c019_categoria c ON (c.pk_num_categoria = a.fk_afc019_num_categoria)
                    INNER JOIN af_c008_clasificacion cl ON (cl.pk_num_clasificacion = a.fk_afc008_num_clasificacion)
                    INNER JOIN af_c005_ubicacion u ON (u.pk_num_ubicacion = a.fk_afc005_num_ubicacion)
                WHERE 1 $filtrar";
        $db = $this->_db->query($sql);
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /**
     * Método para obtener un registro de la tabla.
     *
     * @param Integer
     * @return Array
     */
    public function metObtener($id)
    {
        $sql = "SELECT
                    a.*,
                    cc.fk_a004_num_dependencia,
                    d.fk_a001_num_organismo,
                    cl.ind_codigo AS cod_clasificacion,
                    cl.ind_nombre_clasificacion,
                    cs.ind_codigo AS cod_clasificacion_sudebip,
                    cs.ind_nombre,
                    CONCAT_WS(' ',p1.ind_nombre1,p1.ind_nombre2,p1.ind_apellido1,p1.ind_apellido2) AS persona_usuario,
                    CONCAT_WS(' ',p2.ind_nombre1,p2.ind_nombre2,p2.ind_apellido1,p2.ind_apellido2) AS persona_responsable,
                    a1.cod_codigo_interno AS cod_activo,
                    CONCAT_WS(' ',p1.ind_nombre1,p2.ind_nombre2,p2.ind_apellido1,p2.ind_apellido2) AS proveedor,
                    CONCAT(cbb0011.ind_anio,'-',cbb0011.ind_mes,'-',cbb0011.ind_voucher) AS voucher_ingreso,
                    CONCAT(cbb0012.ind_anio,'-',cbb0012.ind_mes,'-',cbb0012.ind_voucher) AS voucher_baja,
                    CONCAT(cbb0012.ind_anio,'-',cbb0012.ind_mes) AS voucher_periodo
                FROM
                    af_b001_activo a
                    INNER JOIN a023_centro_costo cc ON (cc.pk_num_centro_costo = a.fk_a023_num_centro_costo)
                    INNER JOIN a004_dependencia d ON (d.pk_num_dependencia = cc.fk_a004_num_dependencia)
                    INNER JOIN af_c008_clasificacion cl ON (cl.pk_num_clasificacion = a.fk_afc008_num_clasificacion)
                INNER JOIN af_c032_clasificacion_sudebip cs ON (cs.pk_num_clasificacion = a.fk_afc032_num_clasificacion_sudebip)
                    INNER JOIN a003_persona p1 ON (p1.pk_num_persona = a.fk_a003_num_persona_usuario)
                    INNER JOIN a003_persona p2 ON (p2.pk_num_persona = a.fk_a003_num_persona_responsable)
                    LEFT JOIN a003_persona p3 ON (p3.pk_num_persona = a.fk_a003_num_proveedor)
                    LEFT JOIN af_b001_activo a1 ON (a1.pk_num_activo = a.fk_afb001_num_activo)
                    LEFT JOIN cb_b001_voucher cbb0011 ON (cbb0011.pk_num_voucher_mast = a.fk_cbb001_num_voucher_ingreso)
                    LEFT JOIN cb_b001_voucher cbb0012 ON (cbb0012.pk_num_voucher_mast = a.fk_cbb001_num_voucher_baja)
                WHERE a.pk_num_activo = '$id'";
        $row = $this->_db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        return $row->fetch();
    }

    public function metObtenerPer($id)
    {
        $sql = "SELECT
                    a.*,
                    cc.fk_a004_num_dependencia,
                    d.fk_a001_num_organismo,
                    cl.ind_codigo AS cod_clasificacion,
                    cl.ind_nombre_clasificacion,
                    cs.ind_codigo AS cod_clasificacion_sudebip,
                    cs.ind_nombre,
                    CONCAT_WS(' ',p1.ind_nombre1,p1.ind_nombre2,p1.ind_apellido1,p1.ind_apellido2) AS persona_usuario,
                    CONCAT_WS(' ',p2.ind_nombre1,p2.ind_nombre2,p2.ind_apellido1,p2.ind_apellido2) AS persona_responsable,
                    a1.cod_codigo_interno AS cod_activo,
                    CONCAT_WS(' ',p1.ind_nombre1,p2.ind_nombre2,p2.ind_apellido1,p2.ind_apellido2) AS proveedor,
                    CONCAT(cbb0011.ind_anio,'-',cbb0011.ind_mes,'-',cbb0011.ind_voucher) AS voucher_ingreso,
                    CONCAT(cbb0012.ind_anio,'-',cbb0012.ind_mes,'-',cbb0012.ind_voucher) AS voucher_baja,
                    CONCAT(cbb0012.ind_anio,'-',cbb0012.ind_mes) AS voucher_periodo
                FROM
                    af_b001_activo a
                    INNER JOIN a023_centro_costo cc ON (cc.pk_num_centro_costo = a.fk_a023_num_centro_costo)
                    INNER JOIN a004_dependencia d ON (d.pk_num_dependencia = cc.fk_a004_num_dependencia)
                    INNER JOIN af_c008_clasificacion cl ON (cl.pk_num_clasificacion = a.fk_afc008_num_clasificacion)
                INNER JOIN af_c032_clasificacion_sudebip cs ON (cs.pk_num_clasificacion = a.fk_afc032_num_clasificacion_sudebip)
                    INNER JOIN a003_persona p1 ON (p1.pk_num_persona = a.fk_a003_num_persona_usuario)
                    INNER JOIN a003_persona p2 ON (p2.pk_num_persona = a.fk_a003_num_persona_responsable)
                    LEFT JOIN a003_persona p3 ON (p3.pk_num_persona = a.fk_a003_num_proveedor)
                    LEFT JOIN af_b001_activo a1 ON (a1.pk_num_activo = a.fk_afb001_num_activo)
                    LEFT JOIN cb_b001_voucher cbb0011 ON (cbb0011.pk_num_voucher_mast = a.fk_cbb001_num_voucher_ingreso)
                    LEFT JOIN cb_b001_voucher cbb0012 ON (cbb0012.pk_num_voucher_mast = a.fk_cbb001_num_voucher_baja)
                WHERE a.pk_num_activo = '$id'";
        $row = $this->_db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        return $row->fetch();
    }

    /**
     * Método para crear un nuevo registro.
     *
     * @param Array
     * @return Integer
     */
    public function metCrear($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        ##
        if (trim($data['fk_afc016_num_tipo_seguro'])) $num_tipo_seguro = "fk_afc016_num_tipo_seguro = '$data[fk_afc016_num_tipo_seguro]',"; else $num_tipo_seguro = "";
        if (trim($data['fk_afc017_num_tipo_vehiculo'])) $num_tipo_vehiculo = "fk_afc017_num_tipo_vehiculo = '$data[fk_afc017_num_tipo_vehiculo]',"; else $num_tipo_vehiculo = "";
        if (trim($data['fk_afc018_num_poliza_seguro'])) $num_poliza_seguro = "fk_afc018_num_poliza_seguro = '$data[fk_afc018_num_poliza_seguro]',"; else $num_poliza_seguro = "";

        if (trim($data['fk_afc021_num_tipo_transaccion'])) $num_tipo_transaccion = "fk_afc021_num_tipo_transaccion = '$data[fk_afc021_num_tipo_transaccion]',"; else $num_tipo_transaccion = "";

        if (trim($data['fk_a003_num_proveedor'])) $num_proveedor = "fk_a003_num_proveedor = '$data[fk_a003_num_proveedor]',"; else $num_proveedor = "";
        if (trim($data['fk_a006_num_marca'])) $num_marca = "fk_a006_num_marca = '$data[fk_a006_num_marca]',"; else $num_marca = "";
        if (trim($data['fk_lgb004_num_unidad'])) $num_unidad_medida = "fk_lgb004_num_unidad = '$data[fk_lgb004_num_unidad]',"; else $num_unidad_medida = "";
        if (trim($data['fk_a006_num_color'])) $num_color = "fk_a006_num_color = '$data[fk_a006_num_color]',"; else $num_color = "";
        if (trim($data['fk_a008_num_pais'])) $num_pais = "fk_a008_num_pais = '$data[fk_a008_num_pais]',"; else $num_pais = "";
        if (trim($data['fk_afb001_num_activo'])) $num_activo = "fk_afb001_num_activo = '$data[fk_afb001_num_activo]',"; else $num_activo = "";
        if (trim($data['fk_cbb001_num_voucher_baja'])) $num_voucher_baja = "fk_cbb001_num_voucher_baja = '$data[fk_cbb001_num_voucher_baja]',"; else $num_voucher_baja = "";
        if (trim($data['fk_cbb001_num_voucher_ingreso'])) $num_voucher_ingreso = "fk_cbb001_num_voucher_ingreso = '$data[fk_cbb001_num_voucher_ingreso]',"; else $num_voucher_ingreso = "";
        if (trim($data['fk_cpb002_num_tipo_documento'])) $num_tipo_documento = "fk_cpb002_num_tipo_documento = '$data[fk_cpb002_num_tipo_documento]',"; else $num_tipo_documento = "";

        ## Asignar Numero de Activo Nuevo
        $codPost = "SELECT
                    cl.ind_codigo as codigo_clas
                  FROM
                    af_c008_clasificacion cl 
                    INNER JOIN af_b001_activo a ON (a.fk_afc008_num_clasificacion = cl.pk_num_clasificacion)
                  WHERE a.fk_afc008_num_clasificacion = '$data[fk_afc008_num_clasificacion]'"; 

        $row_validar = $this->_db->query($codPost);
        $row_validar->setFetchMode(PDO::FETCH_ASSOC);
        $field = $row_validar->fetch(); 

##-------------------  Correlativo
        $contador=1;
        $digitos=5;
        $codP = "SELECT
                    cl.ind_codigo as codigo_clas
                  FROM
                    af_c008_clasificacion cl 
                    INNER JOIN af_b001_activo a ON (a.fk_afc008_num_clasificacion = cl.pk_num_clasificacion)
                  WHERE cl.ind_codigo = '$field[codigo_clas]'"; 
        $row_validar = $this->_db->query($codP);
        $row_validar->setFetchMode(PDO::FETCH_ASSOC);
        $num = $row_validar->fetchAll();

        foreach($num AS $key)
        {       
            $contador = $contador+1;
        }
    //--- Recuperar Codigo de Clasificación del Activo
        $codP1 = "SELECT
                    cl.ind_codigo as codigo_clas
                  FROM
                    af_c008_clasificacion cl 
                  WHERE cl.pk_num_clasificacion = '$data[fk_afc008_num_clasificacion]'"; 
        $row_validar = $this->_db->query($codP1);
        $row_validar->setFetchMode(PDO::FETCH_ASSOC);
        $num1 = $row_validar->fetch();

        if($contador==1){
            $correlat = (string) str_repeat("0", $digitos-strlen($contador)).$contador;
            $CodigoAct=$num1['codigo_clas'].$correlat;
            //$CodigoAct="0"."$data[fk_afc008_num_clasificacion]".$correlat;
        }
        else{
            $codPost = "SELECT MAX(cod_codigo_interno) AS ultimo FROM af_b001_activo AS a INNER JOIN af_c008_clasificacion cl ON (cl.pk_num_clasificacion= a.fk_afc008_num_clasificacion ) WHERE a.fk_afc008_num_clasificacion = '$data[fk_afc008_num_clasificacion]'"; 

        $row_validar = $this->_db->query($codPost);
        $row_validar->setFetchMode(PDO::FETCH_ASSOC);
        $field = $row_validar->fetch(); 

        $CodigoAct2 = intval($field['ultimo']) + 1;
        $CodigoAct = "0".$CodigoAct2;

           // $correlat = (string) str_repeat("0", $digitos-strlen($contador)).$contador;
           // $CodigoAct=$field['codigo_clas'].$correlat;
        }

       //--------

              
        ##  activo
        $sql = "INSERT INTO af_b001_activo
                SET
                    fk_afc005_num_ubicacion = '$data[fk_afc005_num_ubicacion]',
                    fk_afc021_num_tipo_transaccion = '$data[fk_afc021_num_tipo_transaccion]',
                    fk_afc007_num_situacion = '$data[fk_afc007_num_situacion]',
                    fk_afc008_num_clasificacion = '$data[fk_afc008_num_clasificacion]',
                    fk_afc009_num_movimiento = '$data[fk_afc009_num_movimiento]',
                    fk_afc019_num_categoria = '$data[fk_afc019_num_categoria]',
                    fk_a003_num_persona_responsable = '$data[fk_a003_num_persona_responsable]',
                    fk_a003_num_persona_usuario = '$data[fk_a003_num_persona_usuario]',
                    fk_a006_num_tipo_activo = '$data[fk_a006_num_tipo_activo]',
                    fk_a006_num_estado_conservacion = '$data[fk_a006_num_estado_conservacion]',
                    fk_a006_num_tipo_mejora = '$data[fk_a006_num_tipo_mejora]',
                    fk_a006_num_naturaleza = '$data[fk_a006_num_naturaleza]',
                    fk_a006_num_origen = '$data[fk_a006_num_origen]',
                    fk_a023_num_centro_costo = '$data[fk_a023_num_centro_costo]',
                    fk_afc032_num_clasificacion_sudebip = '$data[fk_afc032_num_clasificacion_sudebip]',
                    ind_modelo = '$data[ind_modelo]',
                    ind_serie = '$data[ind_serie]',
                    ind_serie_motor = '$data[ind_serie_motor]',
                    ind_placa = '$data[ind_placa]',
                    ind_motor = '$data[ind_motor]',
                    num_asientos = '$data[num_asientos]',
                    num_flag_depreciacion = '$data[num_flag_depreciacion]',
                    num_monto = '$data[num_monto]',
                    num_valor_mercado = '$data[num_valor_mercado]',
                    num_monto_referencial = '$data[num_monto_referencial]',
                    fec_inventario = '$data[fec_inventario]',
                    ind_nro_control = '$data[ind_nro_control]',
                    fec_fecha_obligacion = '$data[fec_fecha_obligacion]',
                    ind_nro_factura = '$data[ind_nro_factura]',
                    fec_factura = '$data[fec_factura]',
                    ind_nro_orden_compra = '$data[ind_nro_orden_compra]',
                    fec_orden_compra = '$data[fec_orden_compra]',
                    ind_nro_guia_remision = '$data[ind_nro_guia_remision]',
                    fec_guia_remision = '$data[fec_guia_remision]',
                    ind_nro_documento_almacen = '$data[ind_nro_documento_almacen]',
                    fec_documento_almacen = '$data[fec_documento_almacen]',
                    ind_material = '$data[ind_material]',
                    ind_dimensiones = '$data[ind_dimensiones]',
                    ind_nro_parte = '$data[ind_nro_parte]',
                    num_anio = '$data[num_anio]',
                    fec_ingreso = '$data[fec_ingreso]',
                    ind_periodo_inicio_depreciacion = '$data[ind_periodo_inicio_depreciacion]',
                    ind_periodo_inicio_revaluacion = '$data[ind_periodo_inicio_revaluacion]',
                    num_unidades = '$data[num_unidades]',
                    num_flag_activo_tecnologico = '$data[num_flag_activo_tecnologico]',
                    num_flag_mantenimiento = '$data[num_flag_mantenimiento]',
                    ind_estado = '$data[ind_estado]',
                    ind_descripcion = '$data[ind_descripcion]',
                    cod_codigo_interno = '$CodigoAct',
                    cod_codigo_barra = '$data[cod_codigo_barra]',
                    ind_observaciones = '$data[ind_observaciones]',
                    num_flag_voucher_ingreso = '$data[num_flag_voucher_ingreso]',
                    $num_poliza_seguro
                    $num_tipo_seguro
                    $num_tipo_vehiculo
                    $num_activo
                    $num_marca
                    $num_unidad_medida
                    $num_voucher_baja
                    $num_voucher_ingreso
                    $num_proveedor
                    $num_tipo_documento
                    $num_color
                    $num_pais
                    fk_a018_num_seguridad_usuario = '$this->idUsuario',
                    fec_ultima_modificacion = NOW()";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        $id = $this->_db->lastInsertId();

        if ($data['pk_num_activo_fijo']) {
            ##  activo (transferencia desde logistica)
            $sql = "UPDATE lg_e004_activo_fijo
                    SET
                        fk_afb001_num_activo = '$id',
                        ind_estado = 'TR',
                        fk_a018_num_seguridad_usuario = '$this->idUsuario',
                        fec_ultima_modificacion = NOW()
                    WHERE pk_num_activo_fijo = '$data[pk_num_activo_fijo]'";
            $db = $this->_db->prepare($sql);
            $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
        }

        ##  Consigna una transacción
        $this->_db->commit();

        return $id;
    }

    /**
     * Método para actualizar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metModificar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $id = $data['pk_num_activo'];

        //  registro
        if (trim($data['fk_afc016_num_tipo_seguro'])) $num_tipo_seguro = "fk_afc016_num_tipo_seguro = '$data[fk_afc016_num_tipo_seguro]',"; else $num_tipo_seguro = "";
        if (trim($data['fk_afc017_num_tipo_vehiculo'])) $num_tipo_vehiculo = "fk_afc017_num_tipo_vehiculo = '$data[fk_afc017_num_tipo_vehiculo]',"; else $num_tipo_vehiculo = "";
        if (trim($data['fk_afc018_num_poliza_seguro'])) $num_poliza_seguro = "fk_afc018_num_poliza_seguro = '$data[fk_afc018_num_poliza_seguro]',"; else $num_poliza_seguro = "";
        if (trim($data['fk_afc021_num_tipo_transaccion'])) $num_tipo_transaccion = "fk_afc021_num_tipo_transaccion = '$data[fk_afc021_num_tipo_transaccion]',"; else $num_tipo_transaccion = "";
        
        if (trim($data['fk_a003_num_proveedor'])) $num_proveedor = "fk_a003_num_proveedor = '$data[fk_a003_num_proveedor]',"; else $num_proveedor = "";
        if (trim($data['fk_a006_num_marca'])) $num_marca = "fk_a006_num_marca = '$data[fk_a006_num_marca]',"; else $num_marca = "";
        if (trim($data['fk_lgb004_num_unidad'])) $num_unidad_medida = "fk_lgb004_num_unidad = '$data[fk_lgb004_num_unidad]',"; else $num_unidad_medida = "";
        if (trim($data['fk_a006_num_color'])) $num_color = "fk_a006_num_color = '$data[fk_a006_num_color]',"; else $num_color = "";
        if (trim($data['fk_a008_num_pais'])) $num_pais = "fk_a008_num_pais = '$data[fk_a008_num_pais]',"; else $num_pais = "";
        if (trim($data['fk_afb001_num_activo'])) $num_activo = "fk_afb001_num_activo = '$data[fk_afb001_num_activo]',"; else $num_activo = "";
        if (trim($data['fk_cbb001_num_voucher_baja'])) $num_voucher_baja = "fk_cbb001_num_voucher_baja = '$data[fk_cbb001_num_voucher_baja]',"; else $num_voucher_baja = "";
        if (trim($data['fk_cbb001_num_voucher_ingreso'])) $num_voucher_ingreso = "fk_cbb001_num_voucher_ingreso = '$data[fk_cbb001_num_voucher_ingreso]',"; else $num_voucher_ingreso = "";
        if (trim($data['fk_cpb002_num_tipo_documento'])) $num_tipo_documento = "fk_cpb002_num_tipo_documento = '$data[fk_cpb002_num_tipo_documento]',"; else $num_tipo_documento = "";

        $codP1 = "SELECT
                    a.cod_codigo_interno as codigo_clas
                  FROM
                    af_b001_activo a 
                  WHERE a.pk_num_activo = '$data[pk_num_activo]'"; 
        $row_validar = $this->_db->query($codP1);
        $row_validar->setFetchMode(PDO::FETCH_ASSOC);
        $num1 = $row_validar->fetch(); 
        $CodigoClas=$num1['codigo_clas'];       

        $sql = "UPDATE af_b001_activo
                SET
                    fk_afc005_num_ubicacion = '$data[fk_afc005_num_ubicacion]',
                    fk_afc021_num_tipo_transaccion = '$data[fk_afc021_num_tipo_transaccion]',
                    fk_afc007_num_situacion = '$data[fk_afc007_num_situacion]',
                    fk_afc008_num_clasificacion = '$data[fk_afc008_num_clasificacion]',                    
                    fk_afc032_num_clasificacion_sudebip = '$data[fk_afc032_num_clasificacion_sudebip]',
                    fk_afc009_num_movimiento = '$data[fk_afc009_num_movimiento]',
                    fk_afc019_num_categoria = '$data[fk_afc019_num_categoria]',
                    fk_a003_num_persona_responsable = '$data[fk_a003_num_persona_responsable]',
                    fk_a003_num_persona_usuario = '$data[fk_a003_num_persona_usuario]',
                    fk_a006_num_tipo_activo = '$data[fk_a006_num_tipo_activo]',
                    fk_a006_num_estado_conservacion = '$data[fk_a006_num_estado_conservacion]',
                    fk_a006_num_tipo_mejora = '$data[fk_a006_num_tipo_mejora]',
                    fk_a023_num_centro_costo = '$data[fk_a023_num_centro_costo]',
                    ind_modelo = '$data[ind_modelo]',
                    ind_serie = '$data[ind_serie]',
                    ind_serie_motor = '$data[ind_serie_motor]',
                    ind_placa = '$data[ind_placa]',
                    ind_motor = '$data[ind_motor]',
                    num_asientos = '$data[num_asientos]',
                    num_flag_depreciacion = '$data[num_flag_depreciacion]',
                    num_monto = '$data[num_monto]',
                    num_valor_mercado = '$data[num_valor_mercado]',
                    num_monto_referencial = '$data[num_monto_referencial]',
                    fec_inventario = '$data[fec_inventario]',
                    ind_nro_control = '$data[ind_nro_control]',
                    fec_fecha_obligacion = '$data[fec_fecha_obligacion]',
                    ind_nro_factura = '$data[ind_nro_factura]',
                    fec_factura = '$data[fec_factura]',
                    ind_nro_orden_compra = '$data[ind_nro_orden_compra]',
                    fec_orden_compra = '$data[fec_orden_compra]',
                    ind_nro_guia_remision = '$data[ind_nro_guia_remision]',
                    fec_guia_remision = '$data[fec_guia_remision]',
                    ind_nro_documento_almacen = '$data[ind_nro_documento_almacen]',
                    fec_documento_almacen = '$data[fec_documento_almacen]',
                    ind_material = '$data[ind_material]',
                    ind_dimensiones = '$data[ind_dimensiones]',
                    ind_nro_parte = '$data[ind_nro_parte]',
                    num_anio = '$data[num_anio]',
                    fec_ingreso = '$data[fec_ingreso]',
                    ind_periodo_inicio_depreciacion = '$data[ind_periodo_inicio_depreciacion]',
                    ind_periodo_inicio_revaluacion = '$data[ind_periodo_inicio_revaluacion]',
                    num_unidades = '$data[num_unidades]',
                    num_flag_activo_tecnologico = '$data[num_flag_activo_tecnologico]',
                    num_flag_mantenimiento = '$data[num_flag_mantenimiento]',
                    ind_descripcion = '$data[ind_descripcion]',
                    cod_codigo_interno = '$CodigoClas',
                    cod_codigo_barra = '$data[cod_codigo_barra]',
                    ind_observaciones = '$data[ind_observaciones]',
                    num_flag_voucher_ingreso = '$data[num_flag_voucher_ingreso]',
                    $num_poliza_seguro
                    $num_tipo_transaccion
                    $num_tipo_seguro
                    $num_tipo_vehiculo
                    $num_activo
                    $num_marca
                    $num_unidad_medida
                    $num_voucher_baja
                    $num_voucher_ingreso
                    $num_proveedor
                    $num_tipo_documento
                    $num_color
                    $num_pais
                    fk_a018_num_seguridad_usuario = '$this->idUsuario',
                    fec_ultima_modificacion = NOW()
                WHERE pk_num_activo = '$data[pk_num_activo]'";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para actualizar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metAprobar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $id = $data['pk_num_activo'];
        $field = $this->metObtener($id);

        $sql = "UPDATE af_b001_activo
                SET
                    ind_estado = 'AP',
                    fk_a018_num_seguridad_usuario = '$this->idUsuario',
                    fec_ultima_modificacion = NOW()
                WHERE pk_num_activo = '$field[pk_num_activo]'";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        ##  usuario responsable
        $sql = "SELECT el.fk_rhc063_num_puestos_cargo
                FROM
                    a003_persona p
                    INNER JOIN rh_b001_empleado e ON (e.fk_a003_num_persona = p.pk_num_persona)
                    INNER JOIN rh_c005_empleado_laboral el ON (el.fk_rhb001_num_empleado = e.pk_num_empleado)
                WHERE p.pk_num_persona = '$field[fk_a003_num_persona_responsable]'";
        $row_responsable = $this->_db->query($sql);
        $row_responsable->setFetchMode(PDO::FETCH_ASSOC);
        $field_responsable = $row_responsable->fetch();

        ##  usuario
        $sql = "SELECT el.fk_rhc063_num_puestos_cargo
                FROM
                    a003_persona p
                    INNER JOIN rh_b001_empleado e ON (e.fk_a003_num_persona = p.pk_num_persona)
                    INNER JOIN rh_c005_empleado_laboral el ON (el.fk_rhb001_num_empleado = e.pk_num_empleado)
                WHERE p.pk_num_persona = '$this->idUsuario'";
        $row_usuario = $this->_db->query($sql);
        $row_usuario->setFetchMode(PDO::FETCH_ASSOC);
        $field_usuario = $row_usuario->fetch();

        ##  conformado
        $conformado_por = Select::parametros('CONFAFDEF');
        $sql = "SELECT el.fk_rhc063_num_puestos_cargo
                FROM
                    a003_persona p
                    INNER JOIN rh_b001_empleado e ON (e.fk_a003_num_persona = p.pk_num_persona)
                    INNER JOIN rh_c005_empleado_laboral el ON (el.fk_rhb001_num_empleado = e.pk_num_empleado)
                WHERE p.pk_num_persona = '$conformado_por'";
        $row_conformado = $this->_db->query($sql);
        $row_conformado->setFetchMode(PDO::FETCH_ASSOC);
        $field_conformado = $row_conformado->fetch();

        ##  acta de incorporación
        $num_anio = date('Y');
        $ind_nro_acta = Number::codigo('af_c027_acta_incorporacion', 'ind_nro_acta', 5, ['num_anio'=>$num_anio]);
        $sql = "INSERT INTO af_c027_acta_incorporacion
                SET
                    fk_afb001_num_activo = '$id',
                    fk_a003_num_responsable = '$field[fk_a003_num_persona_responsable]',
                    fk_rhc063_num_responsable_puesto = '$field_responsable[fk_rhc063_num_puestos_cargo]',
                    fk_a003_num_aprobado_por = '$this->idUsuario',
                    fk_rhc063_num_aprobado_por_puesto = '$field_usuario[fk_rhc063_num_puestos_cargo]',
                    fk_a003_num_conformado_por = '$conformado_por',
                    fk_rhc063_num_conformado_por_puesto = '$field_conformado[fk_rhc063_num_puestos_cargo]',
                    fk_a004_num_dependencia = '$field[fk_a004_num_dependencia]',
                    num_anio = '$num_anio',
                    ind_nro_acta = '$ind_nro_acta',
                    fec_fecha = '$field[fec_ingreso]',
                    fk_a018_num_seguridad_usuario = '$this->idUsuario',
                    fec_ultima_modificacion = NOW()";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        ##  acta de entrega
        $num_anio = date('Y');
        $ind_nro_acta = Number::codigo('af_c028_acta_entrega', 'ind_nro_acta', 5, ['num_anio'=>$num_anio]);
        $sql = "INSERT INTO af_c028_acta_entrega
                SET
                    fk_afb001_num_activo = '$id',
                    fk_a003_num_aprobado_por = '$this->idUsuario',
                    fk_rhc063_num_aprobado_por_puesto = '$field_usuario[fk_rhc063_num_puestos_cargo]',
                    fk_a003_num_conformado_por = '$conformado_por',
                    fk_rhc063_num_conformado_por_puesto = '$field_usuario[fk_rhc063_num_puestos_cargo]',
                    fk_a004_num_dependencia = '$field[fk_a004_num_dependencia]',
                    num_anio = '$num_anio',
                    ind_nro_acta = '$ind_nro_acta',
                    fec_fecha = '$field[fec_ingreso]',
                    fk_a018_num_seguridad_usuario = '$this->idUsuario',
                    fec_ultima_modificacion = NOW()";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        ##  historico
        $fa = $this->metObtener($id);
        $fk_a006_num_motivo = Select::miscelaneo('AFMOTTRASI', '1', 2);
        $sql = "INSERT INTO af_c029_activo_historico
                SET
                    fk_afb001_num_activo = '$id',
                    fk_a023_num_centro_costo = '$fa[fk_a023_num_centro_costo]',
                    fk_afc007_num_situacion = '$fa[fk_afc007_num_situacion]',
                    fk_afc009_num_movimiento = '$fa[fk_afc009_num_movimiento]',
                    fk_afc005_num_ubicacion = '$fa[fk_afc005_num_ubicacion]',
                    fk_a006_num_motivo = '$fk_a006_num_motivo',
                    cod_codigo_interno = '$fa[cod_codigo_interno]',
                    fec_fecha_ingreso = '$fa[fec_ingreso]',
                    ind_tipo = 'I',
                    fec_fecha_transaccion = NOW(),
                    fk_a018_num_seguridad_usuario = '$this->idUsuario',
                    fec_ultima_modificacion = NOW()";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para generar un acta de responsabilidad de uso.
     *
     * @param Array
     * @return Void
     */
    public function metGenerarActaResponsabilidad($id)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $field = $this->metObtener($id);

        ##  usuario responsable
        $sql = "SELECT el.fk_rhc063_num_puestos_cargo
                FROM
                    a003_persona p
                    INNER JOIN rh_b001_empleado e ON (e.fk_a003_num_persona = p.pk_num_persona)
                    INNER JOIN rh_c005_empleado_laboral el ON (el.fk_rhb001_num_empleado = e.pk_num_empleado)
                WHERE p.pk_num_persona = '$field[fk_a003_num_persona_responsable]'";
        $row_responsable = $this->_db->query($sql);
        $row_responsable->setFetchMode(PDO::FETCH_ASSOC);
        $field_responsable = $row_responsable->fetch();

        ##  usuario
        $sql = "SELECT el.fk_rhc063_num_puestos_cargo
                FROM
                    a003_persona p
                    INNER JOIN rh_b001_empleado e ON (e.fk_a003_num_persona = p.pk_num_persona)
                    INNER JOIN rh_c005_empleado_laboral el ON (el.fk_rhb001_num_empleado = e.pk_num_empleado)
                WHERE p.pk_num_persona = '$field[fk_a003_num_persona_usuario]'";
        $row_usuario = $this->_db->query($sql);
        $row_usuario->setFetchMode(PDO::FETCH_ASSOC);
        $field_usuario = $row_usuario->fetch();

        ##  acta de incorporación
        $num_anio = date('Y');
        $ind_nro_acta = Number::codigo('af_c024_acta_responsabilidad_uso', 'ind_nro_acta', 5, ['num_anio'=>$num_anio]);
        $sql = "INSERT INTO af_c024_acta_responsabilidad_uso
                SET
                    fk_afb001_num_activo = '$id',
                    fk_a003_num_responsable = '$field[fk_a003_num_persona_responsable]',
                    fk_rhc063_num_responsable_puesto = '$field_responsable[fk_rhc063_num_puestos_cargo]',
                    fk_a003_num_usuario = '$field[fk_a003_num_persona_responsable]',
                    fk_rhc063_num_usuario_puesto = '$field_responsable[fk_rhc063_num_puestos_cargo]',
                    fk_a004_num_dependencia = '$field[fk_a004_num_dependencia]',
                    fec_fecha = CURDATE(),
                    num_anio = '$num_anio',
                    ind_nro_acta = '$ind_nro_acta',
                    fk_a018_num_seguridad_usuario = '$this->idUsuario',
                    fec_ultima_modificacion = NOW()";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        $id = $this->_db->lastInsertId();

        ##  Consigna una transacción

        $this->_db->commit();

        return $id;
    }

    /**
     * Método para obtener los datos de una acta de responsabilidad.
     *
     * @param Integer
     * @return Array
     */
    public function metObtenerActaResponsabilidad($id)
    {
        $sql = "SELECT
                    aru.*,
                    d.ind_dependencia,
                    o.ind_descripcion_empresa,
                    CONCAT_WS(' ', p1.ind_nombre1, p1.ind_nombre2,p1.ind_apellido1,p1.ind_apellido2) AS persona_responsable,
                    p1.ind_cedula_documento as cedula_responsable,
                    pt1.ind_descripcion_cargo AS cargo_responsable,
                    CONCAT_WS(' ', p2.ind_nombre1,p2.ind_nombre2,p2.ind_apellido1,p2.ind_apellido2) AS persona_usuario,
                    p2.ind_cedula_documento as cedula_usuario,
                    pt2.ind_descripcion_cargo AS cargo_usuario,
					a.cod_codigo_interno AS cod_activo,
					a.ind_descripcion AS desc_activo,
					a.ind_serie,
					a.ind_modelo,
					c.ind_codigo AS cod_clasificacion,
					misc.ind_nombre_detalle AS marca
                FROM
                    af_c024_acta_responsabilidad_uso aru
					INNER JOIN af_b001_activo a ON (a.pk_num_activo = aru.fk_afb001_num_activo)
                    INNER JOIN a004_dependencia d ON (d.pk_num_dependencia = aru.fk_a004_num_dependencia)
                    INNER JOIN a001_organismo o ON (o.pk_num_organismo = d.fk_a001_num_organismo)
                    INNER JOIN a003_persona p1 ON (p1.pk_num_persona = aru.fk_a003_num_responsable)
                    LEFT JOIN rh_c063_puestos pt1 ON (pt1.pk_num_puestos = aru.fk_rhc063_num_responsable_puesto)
                    INNER JOIN a003_persona p2 ON (p2.pk_num_persona = aru.fk_a003_num_usuario)
                    LEFT JOIN rh_c063_puestos pt2 ON (pt2.pk_num_puestos = aru.fk_rhc063_num_usuario_puesto)
					INNER JOIN af_c008_clasificacion c ON (c.pk_num_clasificacion = a.fk_afc008_num_clasificacion)
					LEFT JOIN a006_miscelaneo_detalle misc ON (misc.pk_num_miscelaneo_detalle = a.fk_a006_num_marca)
                WHERE aru.pk_num_acta_responsabilidad_uso = '$id'";
        $row = $this->_db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        return $row->fetch();
    }

/**
     * Método para generar un acta de responsabilidad de uso por FUNCIONARIO.
     *
     * @param Array
     * @return Void
     */
    public function metGenerarActaResponsabilidadMPF($id)
    {  //-- OBTENER ACTA DE RESPONSABILIDAD POR FUNCIONARIO
      
       ## validar que exista el acta este asignada a un funcionario
        $sqlv = "SELECT  act.fk_a003_num_persona_usuario as activoUs
                      FROM af_b001_activo act                 
                      where act.fk_a003_num_persona_usuario='$id'";
        $row_validar = $this->_db->query($sqlv);
        $row_validar->setFetchMode(PDO::FETCH_ASSOC);
        $activoVali = $row_validar->fetch(); 

        if ($activoVali['activoUs']>0) {

        ##  Inicia una transacción
       $this->_db->beginTransaction();

        //$field = $this->metObtener($id);

        ##  Obtener usuario responsable
         $sql = "SELECT  a004_dependencia.ind_codinterno as codigoDep,            
                        af_b001_activo.fk_a003_num_persona_responsable as num_per_responsable
                FROM rh_b001_empleado
                 INNER JOIN af_b001_activo on (rh_b001_empleado.fk_a003_num_persona=af_b001_activo.fk_a003_num_persona_usuario) 
                 INNER JOIN rh_c076_empleado_organizacion on (rh_c076_empleado_organizacion.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado)             
                 INNER JOIN a004_dependencia on (a004_dependencia.pk_num_dependencia=rh_c076_empleado_organizacion.fk_a004_num_dependencia) 
            where rh_b001_empleado.pk_num_empleado='$id'";
         $row_responsable = $this->_db->query($sql);
         $row_responsable->setFetchMode(PDO::FETCH_ASSOC);
         $personaRes = $row_responsable->fetch();

 ##  Obtener Codigos de los Bienes asignados al usuario responsable
        #ejecuto la consulta a la base de datos
        $codigoPost =  $this->_db->query("SELECT  af_b001_activo.cod_codigo_interno as codigoAct
                FROM af_b001_activo
            where af_b001_activo.fk_a003_num_persona_usuario='$id'");        
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $codigoPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        $codigoActi = $codigoPost->fetch();

//-----------------------------          
        ## validar que exista el Nro del acta en la tabla
        $sqlv = "SELECT  aru.ind_nro_acta as acta
                      FROM af_c024_acta_responsabilidad_uso aru                 
                      where aru.fk_a003_num_usuario='$id'";
        $row_validar = $this->_db->query($sqlv);
        $row_validar->setFetchMode(PDO::FETCH_ASSOC);
        $actaValidar = $row_validar->fetch();

        $num_anio = date('Y');

        if ($actaValidar['acta']>0) {

            ## validar que se haya realizado una modificación a la persona Usuario del activo
             $sqlv = "SELECT  aru.fk_afb001_num_activo as num_acta
                         FROM af_c024_acta_responsabilidad_uso aru  
                        where aru.fk_a003_num_usuario='$id' AND aru.fk_afb001_num_activo = 1";
             $row_validar = $this->_db->query($sqlv);
             $row_validar->setFetchMode(PDO::FETCH_ASSOC);
             $actaValModi = $row_validar->fetch(); 

            if ($actaValModi['num_acta']==1) {
            
              ##  Nro de acta de incorporación       
               $ind_nro_acta = Number::codigoARF('af_c024_acta_responsabilidad_uso', 'ind_nro_acta', 3, ['num_anio'=>$num_anio],'fk_a004_num_dependencia', $personaRes['codigoDep']);              

               $sql = "UPDATE af_c024_acta_responsabilidad_uso aru
                 SET
                    fk_afb001_num_activo = '2',
                    fk_a003_num_responsable = '$personaRes[num_per_responsable]',
                    fk_rhc063_num_responsable_puesto = '1',
                    fk_a003_num_usuario = '$id',
                    fk_rhc063_num_usuario_puesto = '2',
                    fk_a004_num_dependencia = '$personaRes[codigoDep]',
                    fec_fecha = CURDATE(),
                    num_anio = '$num_anio',
                    ind_nro_acta = '$ind_nro_acta',
                    fk_a018_num_seguridad_usuario = '$this->idUsuario',
                    fec_ultima_modificacion = NOW()
                    WHERE aru.fk_a003_num_usuario = '$id'";

                $db = $this->_db->prepare($sql);
                $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
                $id = $this->_db->lastInsertId();
           
                ##  Consigna una transacción
                $this->_db->commit();
                return $id;
            }
            else{
                $sql = "UPDATE af_c024_acta_responsabilidad_uso aru
                 SET
                    fk_afb001_num_activo = '2',
                    fk_a003_num_responsable = '$personaRes[num_per_responsable]',
                    fk_rhc063_num_responsable_puesto = '1',
                    fk_a003_num_usuario = '$id',
                    fk_rhc063_num_usuario_puesto = '2',
                    fk_a004_num_dependencia = '$personaRes[codigoDep]',
                    fec_fecha = CURDATE(),
                    num_anio = '$num_anio',
                    ind_nro_acta = '$actaValidar[acta]',
                    fk_a018_num_seguridad_usuario = '$this->idUsuario',
                    fec_ultima_modificacion = NOW()
                    WHERE aru.fk_a003_num_usuario = '$id'";

                $db = $this->_db->prepare($sql);
                $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
                $id = $this->_db->lastInsertId();
           
                ##  Consigna una transacción
                $this->_db->commit();
                return $id;
            }  
        }

       else {  
       ##  acta de incorporación       
        $ind_nro_acta = Number::codigoARF('af_c024_acta_responsabilidad_uso', 'ind_nro_acta', 3, ['num_anio'=>$num_anio],'fk_a004_num_dependencia', $personaRes['codigoDep']);

     ##  acta de incorporación       
        $ind_nro = Number::codigoActCl('af_b001_activo', 'fk_afc008_num_clasificacion', 3, ['num_anio'=>$num_anio],'pk_num_activo', '1');


        $sql = "INSERT INTO af_c024_acta_responsabilidad_uso
                SET
                    fk_afb001_num_activo = '2',
                    fk_a003_num_responsable = '$personaRes[num_per_responsable]',
                    fk_rhc063_num_responsable_puesto = '1',
                    fk_a003_num_usuario = '$id',
                    fk_rhc063_num_usuario_puesto = '1',
                    fk_a004_num_dependencia = '$personaRes[codigoDep]',
                    fec_fecha = CURDATE(),
                    num_anio = '$num_anio',
                    ind_nro_acta = '$ind_nro_acta',
                    fk_a018_num_seguridad_usuario = '$this->idUsuario',
                    fec_ultima_modificacion = NOW()";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
        $id = $this->_db->lastInsertId();
           
        ##  Consigna una transacción
        $this->_db->commit();
        return $id;
        } 
      }
     else { 
        swal("Activo No Asignado", "Este Funcionario no tiene activos asignados", "success");
         }
    }

    /**
     * Método para obtener los datos de una acta de responsabilidad por FUNCIONARIO.
     *
     * @param Integer
     * @return Array
     */
    public function metObtenerActaResponsabilidadPF($id)
    {
        $sql = "SELECT
                    aru.*,
                    d.ind_dependencia,
                    o.ind_descripcion_empresa,
                    CONCAT_WS(' ', p1.ind_nombre1, p1.ind_nombre2,p1.ind_apellido1,p1.ind_apellido2) AS persona_responsable,
                    p1.ind_cedula_documento as cedula_responsable,
                    pt1.ind_descripcion_cargo AS cargo_responsable,
                    CONCAT_WS(' ', p2.ind_nombre1,p2.ind_nombre2,p2.ind_apellido1,p2.ind_apellido2) AS persona_usuario,
                    p2.ind_cedula_documento as cedula_usuario,
                    pt2.ind_descripcion_cargo AS cargo_usuario,
                    a.cod_codigo_interno AS cod_activo,
                    a.ind_descripcion AS desc_activo,
                    a.ind_serie,
                    a.ind_modelo,
                    c.ind_codigo AS cod_clasificacion,
                    misc.ind_nombre_detalle AS marca
                FROM
                    af_c024_acta_responsabilidad_uso aru
                    INNER JOIN af_b001_activo a ON (a.pk_num_activo = aru.fk_afb001_num_activo)
                    INNER JOIN a004_dependencia d ON (d.pk_num_dependencia = aru.fk_a004_num_dependencia)
                    INNER JOIN a001_organismo o ON (o.pk_num_organismo = d.fk_a001_num_organismo)
                    INNER JOIN a003_persona p1 ON (p1.pk_num_persona = aru.fk_a003_num_responsable)
                    LEFT JOIN rh_c063_puestos pt1 ON (pt1.pk_num_puestos = aru.fk_rhc063_num_responsable_puesto)
                    INNER JOIN a003_persona p2 ON (p2.pk_num_persona = aru.fk_a003_num_usuario)
                    LEFT JOIN rh_c063_puestos pt2 ON (pt2.pk_num_puestos = aru.fk_rhc063_num_usuario_puesto)
                    INNER JOIN af_c008_clasificacion c ON (c.pk_num_clasificacion = a.fk_afc008_num_clasificacion)
                    LEFT JOIN a006_miscelaneo_detalle misc ON (misc.pk_num_miscelaneo_detalle = a.fk_a006_num_marca)
                WHERE aru.pk_num_acta_responsabilidad_uso = '$id'";
                
        $row = $this->_db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        return $row->fetch();
    }

    public function metObtenerActivoEmpleado($id=null)
    {
        $filtrar = "";
         $sql =  "SELECT
                    actv.ind_descripcion AS desc_activo,
                    actv.ind_serie AS serie,
                    actv.cod_codigo_interno AS cod_activo,
                    misc.ind_nombre_detalle AS estado
                FROM
                    af_b001_activo actv
                    LEFT JOIN a006_miscelaneo_detalle misc ON (misc.pk_num_miscelaneo_detalle = actv.fk_a006_num_estado_conservacion)
                WHERE actv.fk_a003_num_persona_usuario = '$id'";

         $db = $this->_db->query($sql);
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

//----Consulta para traer los Funcionarios Activos
    public function metGetPersonas()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query("SELECT 
            rh_b001_empleado.fk_a003_num_persona, 
            rh_b001_empleado.pk_num_empleado, 
            a003_persona.ind_nombre1, 
            a003_persona.ind_apellido1, 
            a003_persona.ind_nombre2, 
            a003_persona.ind_apellido2,
            a003_persona.ind_cedula_documento,
            a004_dependencia.ind_dependencia, 
            a003_persona.pk_num_persona,
            rh_c076_empleado_organizacion.fk_a004_num_dependencia,
            af_b001_activo.fk_a003_num_persona_usuario,
            a004_dependencia.ind_codinterno
            FROM rh_b001_empleado 
            INNER JOIN a003_persona on (a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona AND ind_estado_aprobacion='AP') 
            INNER JOIN rh_c076_empleado_organizacion on (rh_c076_empleado_organizacion.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado) 
            INNER JOIN a004_dependencia on (a004_dependencia.pk_num_dependencia=rh_c076_empleado_organizacion.fk_a004_num_dependencia) 
            LEFT JOIN af_b001_activo on (af_b001_activo.fk_a003_num_persona_usuario=rh_b001_empleado.fk_a003_num_persona)
               where rh_b001_empleado.num_estatus='1' GROUP BY pk_num_empleado"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll â€” Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    public function metGetPersonaActa($id)
    {
        #ejecuto la consulta a la base de datos
        
        $pruebaPost =  $this->_db->query("SELECT 
            rh_b001_empleado.fk_a003_num_persona as num_personaU, 
            rh_b001_empleado.pk_num_empleado as num_personaUsuar,             
            CONCAT_WS(' ',a003_persona.ind_nombre1, a003_persona.ind_nombre2, a003_persona.ind_apellido1, a003_persona.ind_apellido2) AS persona_usuario,
            a003_persona.pk_num_persona,
            a003_persona.ind_cedula_documento as cedula_usuario,
            a004_dependencia.ind_dependencia, 
            rh_c076_empleado_organizacion.fk_a004_num_dependencia,

            apr.ind_cedula_documento as cedula_responsable,
            acr.fk_a003_num_persona_responsable as num_per_responsable,
            CONCAT_WS(' ',apr.ind_nombre1, apr.ind_nombre2,apr.ind_apellido1, apr.ind_apellido2) AS persona_responsable

            FROM rh_b001_empleado 
            inner join a003_persona on ( a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona AND ind_estado_aprobacion='AP') 
            INNER JOIN rh_c076_empleado_organizacion on (rh_c076_empleado_organizacion.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado) 
            INNER JOIN a004_dependencia on (a004_dependencia.pk_num_dependencia=rh_c076_empleado_organizacion.fk_a004_num_dependencia) 

            /* Obtener Persona Responsable del Acta */
            INNER JOIN af_b001_activo acr on (rh_b001_empleado.fk_a003_num_persona=acr.fk_a003_num_persona_usuario)
            INNER JOIN a003_persona apr on (acr.fk_a003_num_persona_responsable=apr.pk_num_persona)

            where rh_b001_empleado.pk_num_empleado='$id'");

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        return $pruebaPost->fetch();
     }

     public function metGetPersonaCargoActa($id)
     {
        #ejecuto la consulta a la base de datos

        $pruebaPos =  $this->_db->query("SELECT el.fk_rhc063_num_puestos_cargo as cargo_u,
                                         pt1.ind_descripcion_cargo AS cargo_usuario
            FROM a003_persona p 

            INNER JOIN rh_b001_empleado e ON (e.fk_a003_num_persona = p.pk_num_persona) 
            INNER JOIN rh_c005_empleado_laboral el ON (el.fk_rhb001_num_empleado = e.pk_num_empleado) 
            LEFT JOIN rh_c063_puestos pt1 ON (pt1.pk_num_puestos = el.fk_rhc063_num_puestos_cargo)

            WHERE p.pk_num_persona = '$id'");
        
        #retorno lo consultado al controlador para ser usado.
        return $pruebaPos->fetch();

     }

     //----Consulta para traer el cógido de Funcionario seleccionado
    public function metGetCodPersonas($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query("SELECT 

            a004_dependencia.ind_codinterno as codigoDep,
            aru.ind_nro_acta as nro_acta,
            act.fk_a003_num_persona_usuario as numPerUsuaA,
            act.fk_a003_num_persona_responsable as numPerRespA

            FROM rh_b001_empleado
            INNER JOIN rh_c076_empleado_organizacion on (rh_c076_empleado_organizacion.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado) 
            INNER JOIN af_c024_acta_responsabilidad_uso aru on (aru.fk_a003_num_usuario='$id') 
            INNER JOIN a004_dependencia on (a004_dependencia.pk_num_dependencia=rh_c076_empleado_organizacion.fk_a004_num_dependencia) 
            INNER JOIN af_b001_activo act on (act.pk_num_activo='$id')
            where rh_b001_empleado.pk_num_empleado='$id'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        return $pruebaPost->fetch();
    }

## ---- Consulta para obtener el numero de la persona responsable del acta a modificar
   
     public function metObtenerNumPersonaActa($id)
     {
     #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query("SELECT  act.fk_a003_num_persona_usuario as numPerUsuaA,
                                                  act.fk_a003_num_persona_responsable as numPerRespA
                                 FROM af_b001_activo act                 
                                 where act.pk_num_activo='$id'"
        );        
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        return $pruebaPost->fetch();
     }   

     public function metValidarActa($id)
    {
     #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query("SELECT  act.fk_a003_num_persona_usuario
                      FROM af_b001_activo act                 
                      where act.fk_a003_num_persona_usuario='$id'"
        );        
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        return $pruebaPost->fetch();
    }
## -- Actualizacion en caso de modificacion de persona usuario o responsable de los activos

    public function metValidarCodigoAct($id)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

         $sql = "UPDATE af_c024_acta_responsabilidad_uso aru
                SET
                    fk_afb001_num_activo = '1'
                    WHERE aru.fk_a003_num_usuario = '$id'";
            $db = $this->_db->prepare($sql);
            $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));      
        ##  Consigna una transacción
        $this->_db->commit();
        ##return $id;
    }

}
