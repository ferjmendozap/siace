<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Selector Cuentas Contables
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |16-02-2016               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class ListasModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /**
     * Método para listar los registros de la tabla.
     *
     * @return Array
     */
    public function metListarCuentasContables()
    {
        $db = $this->_db->query(
            "SELECT * FROM cb_b004_plan_cuenta"
        );
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /**
     * Método para listar los registros de la tabla.
     *
     * @return Array
     */
    public function metListarEmpleados()
    {
        $db = $this->_db->query(
            "SELECT
                p.pk_num_persona,
                p.ind_cedula_documento,
                CONCAT_WS(' ', p.ind_nombre1, p.ind_nombre2, p.ind_apellido1, p.ind_apellido2) AS nombre_completo,
                e.num_estatus
             FROM
                a003_persona p
                INNER JOIN rh_b001_empleado e ON (e.fk_a003_num_persona = p.pk_num_persona)
             ORDER BY ind_cedula_documento"
        );
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /**
     * Método para listar los registros de la tabla.
     *
     * @return Array
     */
    public function metListarPersonas()
    {
        $db = $this->_db->query(
            "SELECT
                p.pk_num_persona,
                p.ind_cedula_documento,
                CONCAT(' ', p.ind_nombre1, p.ind_nombre2, p.ind_apellido1, p.ind_apellido2) AS nombre_completo,
                p.num_estatus
             FROM a003_persona p
             ORDER BY ind_cedula_documento"
        );
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }
}
