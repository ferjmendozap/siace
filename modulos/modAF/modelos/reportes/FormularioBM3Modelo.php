<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Activos
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |01-12-2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class FormularioBM3Modelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método para listar los registros de la tabla.
     *
     * @return Array
     */
    public function metListar($filtro=null)
    {
        $filtrar = "";
        if (!empty($filtro['ffk_a001_num_organismo'])) $filtrar .= " AND d.fk_a001_num_organismo = '$filtro[ffk_a001_num_organismo]'";
        if (!empty($filtro['ffk_a004_num_dependencia'])) $filtrar .= " AND cc.fk_a004_num_dependencia = '$filtro[ffk_a004_num_dependencia]'";
        if (!empty($filtro['ffk_a023_num_centro_costo'])) $filtrar .= " AND afc29.fk_a023_num_centro_costo = '$filtro[ffk_a023_num_centro_costo]'";
        if (!empty($filtro['ffec_fechad'])) $filtrar .= " AND afc30.fec_fecha >= '".Fecha::formatFecha($filtro['ffec_fechad'])."'";
        if (!empty($filtro['ffec_fechah'])) $filtrar .= " AND afc30.fec_fecha <= '".Fecha::formatFecha($filtro['ffec_fechah'])."'";
        if (!empty($filtro['find_estado'])) $filtrar .= " AND afc30.ind_estado = '$filtro[find_estado]'";
        if (!empty($filtro['find_situacion'])) $filtrar .= " AND afc30.ind_situacion = '$filtro[find_situacion]'";

        $sql = "SELECT
                    afc31.*,
                    afc30.fec_fecha,
                    afc30.ind_codigo,
                    a9.ind_estado,
                    a11.ind_municipio,
                    a10.ind_ciudad,
                    cc.ind_descripcion_centro_costo,
                    cc.fk_a004_num_dependencia,
                    d.ind_dependencia,
                    d.fk_a001_num_organismo,
                    o.ind_descripcion_empresa,
                    o.ind_direccion,
                    afc8.ind_nombre_clasificacion,
                    afc8.ind_codigo AS clasificacion_codigo,
                    afc9.ind_nombre_movimiento,
                    afc9.ind_codigo AS movimiento_codigo,
                    afb1.num_monto,
                    afb1.cod_codigo_interno,
                    afb1.ind_descripcion,
                    CONCAT_WS(' ', p3.ind_nombre1, p3.ind_nombre2, p3.ind_apellido1, p3.ind_apellido2) AS persona_dependencia,
                    rhc63.ind_descripcion_cargo AS persona_dependencia_cargo
                FROM
                    af_c031_activos_faltantes_detalle afc31
                    INNER JOIN af_c030_activos_faltantes afc30 ON (afc30.pk_num_activo_faltante = afc31.fk_afc030_num_activo_faltante)
                    INNER JOIN af_b001_activo afb1 ON (afb1.pk_num_activo = afc31.fk_afb001_num_activo)
                    INNER JOIN a023_centro_costo cc ON (cc.pk_num_centro_costo = afb1.fk_a023_num_centro_costo)
                    INNER JOIN a004_dependencia d ON (d.pk_num_dependencia = cc.fk_a004_num_dependencia)
                    INNER JOIN a001_organismo o ON (o.pk_num_organismo = d.fk_a001_num_organismo)
                    INNER JOIN af_c009_tipo_movimiento afc9 ON (afc9.pk_num_movimiento = afc30.fk_afc009_num_movimiento)
                    INNER JOIN af_c008_clasificacion afc8 ON (afc8.pk_num_clasificacion = afb1.fk_afc008_num_clasificacion)
                    LEFT JOIN a002_organismo_detalle a2 ON (a2.fk_a001_num_organismo = o.pk_num_organismo)
                    LEFT JOIN a010_ciudad a10 ON (a10.pk_num_ciudad = a2.fk_a010_num_ciudad)
                    LEFT JOIN a014_ciudad_municipio a14 ON (a14.fk_a010_num_ciudad = a10.pk_num_ciudad)
                    LEFT JOIN a011_municipio a11 ON (a11.pk_num_municipio = a14.fk_a011_num_municipio)
                    LEFT JOIN a009_estado a9 ON (a9.pk_num_estado = a10.fk_a009_num_estado)
                    LEFT JOIN a003_persona p3 ON (p3.pk_num_persona = d.fk_a003_num_persona_responsable)
                    LEFT JOIN rh_b001_empleado rhb1 ON (rhb1.fk_a003_num_persona = p3.pk_num_persona)
                    LEFT JOIN rh_c005_empleado_laboral rhc5 ON (rhc5.fk_rhb001_num_empleado = rhb1.pk_num_empleado)
                    LEFT JOIN rh_c063_puestos rhc63 ON (rhc63.pk_num_puestos = rhc5.fk_rhc063_num_puestos_cargo)
                WHERE
                    1 $filtrar
                ORDER BY afc31.fk_afc030_num_activo_faltante, afc31.pk_num_activo_faltante_detalle";
        $db = $this->_db->query($sql);
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }
}
