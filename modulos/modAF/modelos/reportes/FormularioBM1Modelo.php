<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Activos
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |01-12-2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class FormularioBM1Modelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método para listar los registros de la tabla.
     *
     * @return Array
     */
    public function metListar($filtro=null)
    {
        $filtrar = "";
        $order='afb1.ind_descripcion,cc.fk_a004_num_dependencia,a.fk_a023_num_centro_costo,a.pk_num_activo';
        if (isset($filtro['ffk_a001_num_organismo'])) if ($filtro['ffk_a001_num_organismo'] != "") $filtrar .= " AND d.fk_a001_num_organismo = '$filtro[ffk_a001_num_organismo]'";
        if (isset($filtro['ffk_a004_num_dependencia'])) if ($filtro['ffk_a004_num_dependencia'] != "") $filtrar .= " AND cc.fk_a004_num_dependencia = '$filtro[ffk_a004_num_dependencia]'";
        if (isset($filtro['ffk_a023_num_centro_costo'])) if ($filtro['ffk_a023_num_centro_costo'] != "") $filtrar .= " AND a.fk_a023_num_centro_costo = '$filtro[ffk_a023_num_centro_costo]'";
        if (isset($filtro['ffk_afc005_num_ubicacion'])) if ($filtro['ffk_afc005_num_ubicacion'] != "") $filtrar .= " AND a.fk_afc005_num_ubicacion = '$filtro[ffk_afc005_num_ubicacion]'";
        if (isset($filtro['ffk_afc007_num_situacion'])) if ($filtro['ffk_afc007_num_situacion'] != "") $filtrar .= " AND a.fk_afc007_num_situacion = '$filtro[ffk_afc007_num_situacion]'";
        if (isset($filtro['ffk_afc008_num_clasificacion'])) if ($filtro['ffk_afc008_num_clasificacion'] != "") $filtrar .= " AND a.fk_afc008_num_clasificacion = '$filtro[ffk_afc008_num_clasificacion]'";
        if (isset($filtro['ffk_afc009_num_movimiento'])) if ($filtro['ffk_afc009_num_movimiento'] != "") $filtrar .= " AND a.fk_afc009_num_movimiento = '$filtro[ffk_afc009_num_movimiento]'";
        if (isset($filtro['ffk_afc019_num_categoria'])) if ($filtro['ffk_afc019_num_categoria'] != "") $filtrar .= " AND a.fk_afc019_num_categoria = '$filtro[ffk_afc019_num_categoria]'";
        if (isset($filtro['find_estado'])) if ($filtro['find_estado'] != "") $filtrar .= " AND a.ind_estado = '$filtro[find_estado]'";

        $sql = "SELECT
                    a.*,
                    cc.ind_descripcion_centro_costo,
                    cc.fk_a004_num_dependencia,
                    d.ind_dependencia,
                    d.fk_a001_num_organismo,
                    o.ind_descripcion_empresa,
                    o.ind_direccion,
                    afc5.ind_nombre_ubicacion,
                    afc7.ind_nombre_situacion,
                    afc8.ind_nombre_clasificacion,
                    afc8.ind_codigo AS clasificacion_codigo,
                    afc9.ind_nombre_movimiento,
                    afc19.ind_nombre_categoria,
                    afb1.ind_descripcion AS activo_principal,
                    CONCAT_WS(' ', p1.ind_nombre1, p1.ind_nombre2, p1.ind_apellido1, p1.ind_apellido2) AS persona_usuario,
                    CONCAT_WS(' ', p2.ind_nombre1, p2.ind_nombre2, p2.ind_apellido1, p2.ind_apellido2) AS persona_responsable,
                    CONCAT_WS(' ', p3.ind_nombre1, p3.ind_nombre2, p3.ind_apellido1, p3.ind_apellido2) AS persona_dependencia,
                    rhc63.ind_descripcion_cargo AS persona_dependencia_cargo,
                    a9.ind_estado,
                    a11.ind_municipio,
                    a10.ind_ciudad
                FROM
                    af_b001_activo a
                    INNER JOIN a023_centro_costo cc ON (cc.pk_num_centro_costo = a.fk_a023_num_centro_costo)
                    INNER JOIN a004_dependencia d ON (d.pk_num_dependencia = cc.fk_a004_num_dependencia)
                    INNER JOIN a001_organismo o ON (o.pk_num_organismo = d.fk_a001_num_organismo)
                    INNER JOIN a003_persona p1 ON (p1.pk_num_persona = a.fk_a003_num_persona_usuario)
                    INNER JOIN a003_persona p2 ON (p2.pk_num_persona = a.fk_a003_num_persona_responsable)
                    INNER JOIN af_c005_ubicacion afc5 ON (afc5.pk_num_ubicacion = a.fk_afc005_num_ubicacion)
                    INNER JOIN af_c007_situacion afc7 ON (afc7.pk_num_situacion = a.fk_afc007_num_situacion)
                    INNER JOIN af_c008_clasificacion afc8 ON (afc8.pk_num_clasificacion = a.fk_afc008_num_clasificacion)
                    INNER JOIN af_c009_tipo_movimiento afc9 ON (afc9.pk_num_movimiento = a.fk_afc009_num_movimiento)
                    INNER JOIN af_c019_categoria afc19 ON (afc19.pk_num_categoria = a.fk_afc019_num_categoria)
                    LEFT JOIN af_b001_activo afb1 ON (afb1.pk_num_activo = a.fk_afb001_num_activo)
                    LEFT JOIN a003_persona p3 ON (p3.pk_num_persona = d.fk_a003_num_persona_responsable)
                    LEFT JOIN rh_b001_empleado rhb1 ON (rhb1.fk_a003_num_persona = p3.pk_num_persona)
                    LEFT JOIN rh_c005_empleado_laboral rhc5 ON (rhc5.fk_rhb001_num_empleado = rhb1.pk_num_empleado)
                    LEFT JOIN rh_c063_puestos rhc63 ON (rhc63.pk_num_puestos = rhc5.fk_rhc063_num_puestos_cargo)
                    LEFT JOIN a002_organismo_detalle a2 ON (a2.fk_a001_num_organismo = o.pk_num_organismo)
                    LEFT JOIN a010_ciudad a10 ON (a10.pk_num_ciudad = a2.fk_a010_num_ciudad)
                    LEFT JOIN a014_ciudad_municipio a14 ON (a14.fk_a010_num_ciudad = a10.pk_num_ciudad)
                    LEFT JOIN a011_municipio a11 ON (a11.pk_num_municipio = a14.fk_a011_num_municipio)
                    LEFT JOIN a009_estado a9 ON (a9.pk_num_estado = a10.fk_a009_num_estado)
                WHERE 1 $filtrar
                ORDER BY $order";
                
        $db = $this->_db->query($sql);
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }
}
