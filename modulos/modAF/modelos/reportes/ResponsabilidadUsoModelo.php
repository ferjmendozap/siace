<?php
/******************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Acta de Responsabilidad de Uso
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacura001.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |23-02-2017               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 ********************************************************************************************************************************/
class ResponsabilidadUsoModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /**
     * Método para listar los registros de la tabla.
     *
     * @return Array
     */
    public function metListar($filtro=null)
    {
        $filtro['ffec_fechad'] = Fecha::formatFecha($filtro['ffec_fechad']);
        $filtro['ffec_fechah'] = Fecha::formatFecha($filtro['ffec_fechah']);
        $filtrar = "";
        if (!empty($filtro['fpk_num_organismo'])) $filtrar .= " AND a004.fk_a001_num_organismo = '$filtro[fpk_num_organismo]'";
        if (!empty($filtro['fpk_num_dependencia'])) $filtrar .= " AND cc.fk_a004_num_dependencia = 'DIRECCION DE ADMINISTRACION'";
        if (!empty($filtro['fpk_num_centro_costo'])) $filtrar .= " AND afb1.fk_a023_num_centro_costo = '$filtro[fpk_num_centro_costo]'";
        if (!empty($filtro['ffec_fechad'])) $filtrar .= " AND afc24.fec_fecha >= '$filtro[ffec_fechad]'";
        if (!empty($filtro['ffec_fechah'])) $filtrar .= " AND afc24.fec_fecha <= '$filtro[ffec_fechah]'";

        $sql = "SELECT
                    afc24.*,
                    CONCAT_WS(' ', a3_1.ind_nombre1, a3_1.ind_nombre2, a3_1.ind_apellido1, a3_1.ind_apellido2) AS persona_usuario,
                    CONCAT_WS(' ', a3_2.ind_nombre1, a3_2.ind_nombre2, a3_2.ind_apellido1, a3_2.ind_apellido2) AS persona_responsable
                FROM
                	af_c024_acta_responsabilidad_uso afc24

                    INNER JOIN a003_persona a3_1 ON (a3_1.pk_num_persona = afc24.fk_a003_num_usuario)
                    INNER JOIN a003_persona a3_2 ON (a3_2.pk_num_persona = afc24.fk_a003_num_responsable)
                WHERE 1 $filtrar";

        $db = $this->_db->query($sql);
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }
}
