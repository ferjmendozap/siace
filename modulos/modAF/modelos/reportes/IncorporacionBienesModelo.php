<?php
/******************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Acta de Incorporación de Bienes
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacura001.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |23-02-2017               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 ********************************************************************************************************************************/
class IncorporacionBienesModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /**
     * Método para listar los registros de la tabla.
     *
     * @return Array
     */
    public function metListar($filtro=null)
    {
        $filtro['ffec_fechad'] = Fecha::formatFecha($filtro['ffec_fechad']);
        $filtro['ffec_fechah'] = Fecha::formatFecha($filtro['ffec_fechah']);
        $filtrar = "";
        if (!empty($filtro['fpk_num_organismo'])) $filtrar .= " AND a004.fk_a001_num_organismo = '$filtro[fpk_num_organismo]'";
        if (!empty($filtro['fpk_num_dependencia'])) $filtrar .= " AND cc.fk_a004_num_dependencia = '$filtro[fpk_num_dependencia]'";
        if (!empty($filtro['fpk_num_centro_costo'])) $filtrar .= " AND afb1.fk_a023_num_centro_costo = '$filtro[fpk_num_centro_costo]'";
        if (!empty($filtro['ffec_fechad'])) $filtrar .= " AND afc27.fec_fecha >= '$filtro[ffec_fechad]'";
        if (!empty($filtro['ffec_fechah'])) $filtrar .= " AND afc27.fec_fecha <= '$filtro[ffec_fechah]'";

        $sql = "SELECT
                    afc27.*,
                    a23.ind_descripcion_centro_costo,
                    a4.pk_num_dependencia,
                    a4.ind_dependencia,
                    a1.pk_num_organismo,
                    a1.ind_descripcion_empresa,
                    CONCAT_WS(' ', a3_1.ind_nombre1, a3_1.ind_nombre2, a3_1.ind_apellido1, a3_1.ind_apellido2) AS persona_responsable,
                    CONCAT_WS(' ', a3_2.ind_nombre1, a3_2.ind_nombre2, a3_2.ind_apellido1, a3_2.ind_apellido2) AS persona_aprobado,
                    CONCAT_WS(' ', a3_3.ind_nombre1, a3_3.ind_nombre2, a3_3.ind_apellido1, a3_3.ind_apellido2) AS persona_conformado
                FROM
                	af_c027_acta_incorporacion afc27
                    INNER JOIN af_b001_activo afb1 ON (afb1.pk_num_activo = afc27.fk_afb001_num_activo)
                    INNER JOIN a023_centro_costo a23 ON (a23.pk_num_centro_costo = afb1.fk_a023_num_centro_costo)
                    INNER JOIN a004_dependencia a4 ON (a4.pk_num_dependencia = a23.fk_a004_num_dependencia)
                    INNER JOIN a001_organismo a1 ON (a1.pk_num_organismo = a4.fk_a001_num_organismo)
                    INNER JOIN a003_persona a3_1 ON (a3_1.pk_num_persona = afc27.fk_a003_num_responsable)
                    INNER JOIN a003_persona a3_2 ON (a3_2.pk_num_persona = afc27.fk_a003_num_aprobado_por)
                    INNER JOIN a003_persona a3_3 ON (a3_3.pk_num_persona = afc27.fk_a003_num_conformado_por)
                WHERE 1 $filtrar";
        $db = $this->_db->query($sql);
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /**
     * Método para obtener los datos de una acta de responsabilidad.
     *
     * @param Integer
     * @return Array
     */
    public function metObtenerActa($id)
    {
        $sql = "SELECT
                    afc27.*,
                    afb1.cod_codigo_interno AS cod_activo,
                    afb1.ind_descripcion AS desc_activo,
                    a23.ind_descripcion_centro_costo,
                    a4.pk_num_dependencia,
                    a4.ind_dependencia,
                    a1.pk_num_organismo,
                    a1.ind_descripcion_empresa,
                    afc5.ind_nombre_ubicacion,
                    afc8.ind_codigo AS cod_clasificacion,
                    a3_1.ind_cedula_documento as cedula_responsable,
                    rhc63_1.ind_descripcion_cargo AS cargo_responsable,
                    CONCAT_WS(' ', a3_1.ind_nombre1, a3_1.ind_nombre2, a3_1.ind_apellido1, a3_1.ind_apellido2) AS persona_responsable,
                    rhc63_2.ind_descripcion_cargo AS cargo_aprobado,
                    CONCAT_WS(' ', a3_2.ind_nombre1, a3_2.ind_nombre2, a3_2.ind_apellido1, a3_2.ind_apellido2) AS persona_aprobado,
                    rhc63_3.ind_descripcion_cargo AS cargo_conformado,
                    CONCAT_WS(' ', a3_3.ind_nombre1, a3_3.ind_nombre2, a3_3.ind_apellido1, a3_3.ind_apellido2) AS persona_conformado
                FROM
                    af_c027_acta_incorporacion afc27
                    INNER JOIN af_b001_activo afb1 ON (afb1.pk_num_activo = afc27.fk_afb001_num_activo)
                    INNER JOIN a023_centro_costo a23 ON (a23.pk_num_centro_costo = afb1.fk_a023_num_centro_costo)
                    INNER JOIN a004_dependencia a4 ON (a4.pk_num_dependencia = a23.fk_a004_num_dependencia)
                    INNER JOIN a001_organismo a1 ON (a1.pk_num_organismo = a4.fk_a001_num_organismo)
                    INNER JOIN af_c005_ubicacion afc5 ON (afc5.pk_num_ubicacion = afb1.fk_afc005_num_ubicacion)
                    INNER JOIN af_c008_clasificacion afc8 ON (afc8.pk_num_clasificacion = afb1.fk_afc008_num_clasificacion)
                    INNER JOIN a003_persona a3_1 ON (a3_1.pk_num_persona = afc27.fk_a003_num_responsable)
                    INNER JOIN a003_persona a3_2 ON (a3_2.pk_num_persona = afc27.fk_a003_num_aprobado_por)
                    INNER JOIN a003_persona a3_3 ON (a3_3.pk_num_persona = afc27.fk_a003_num_conformado_por)
                    INNER JOIN rh_c063_puestos rhc63_1 ON (rhc63_1.pk_num_puestos = afc27.fk_rhc063_num_responsable_puesto)

                    INNER JOIN rh_b001_empleado rhb1_2 ON (rhb1_2.fk_a003_num_persona = a3_2.pk_num_persona)
                    INNER JOIN rh_c005_empleado_laboral rhc5_2 ON (rhc5_2.fk_rhb001_num_empleado = rhb1_2.pk_num_empleado)
                    INNER JOIN rh_c063_puestos rhc63_2 ON (rhc63_2.pk_num_puestos = rhc5_2.fk_rhc063_num_puestos_cargo)

                    INNER JOIN rh_b001_empleado rhb1_3 ON (rhb1_3.fk_a003_num_persona = a3_3.pk_num_persona)
                    INNER JOIN rh_c005_empleado_laboral rhc5_3 ON (rhc5_3.fk_rhb001_num_empleado = rhb1_3.pk_num_empleado)
                    INNER JOIN rh_c063_puestos rhc63_3 ON (rhc63_3.pk_num_puestos = rhc5_3.fk_rhc063_num_puestos_cargo)
                WHERE afc27.pk_num_acta_incorporacion = '$id'";
        $row = $this->_db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        return $row->fetch();
    }
}
