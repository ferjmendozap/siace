<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Script de Carga Inicial
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |01-12-2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class scriptCargaModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método para obtener el tipo de contabilidad.
     *
     * @return Array
     */
    public function metObtenerContabilidad($codigo)
    {
    	$sql = "SELECT pk_num_contabilidades FROM cb_b005_contabilidades WHERE ind_tipo_contabilidad = '$codigo'";
        $row = $this->_db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        $field = $row->fetch();

        return $field['pk_num_contabilidades'];
    }

    /**
     * Método para obtener cuenta contable.
     *
     * @return Array
     */
    public function metObtenerCuenta($codigo, $contabilidad)
    {
    	$sql = "SELECT pk_num_cuenta
                FROM cb_b004_plan_cuenta
                WHERE
                	cod_cuenta = '$codigo'
                	AND num_flag_tipo_cuenta = '$contabilidad'";
        $row = $this->_db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        $field = $row->fetch();

        return $field['pk_num_cuenta'];
    }

    /**
     * Método para obtener cuenta contable.
     *
     * @return Array
     */
    public function metObtenerTipoVoucher($codigo)
    {
        $sql = "SELECT pk_num_voucher FROM cb_c003_tipo_voucher WHERE cod_voucher = '$codigo'";
        $row = $this->_db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        $field = $row->fetch();

        return $field['pk_num_voucher'];
    }

    /**
     * Método para obtener cuenta contable.
     *
     * @return Array
     */
    public function metObtenerCategoria($codigo)
    {
        $sql = "SELECT pk_num_categoria FROM af_c019_categoria WHERE ind_codigo = '$codigo'";
        $row = $this->_db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        $field = $row->fetch();

        return $field['pk_num_categoria'];
    }

    /**
     * Método para cargar maestro de situación del activo
     *
     * @return Array
     */
    public function metCargarSituacion($array)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();
        ##
        $sql = "DELETE FROM af_c029_activo_historico";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        $sql = "ALTER TABLE af_c029_activo_historico AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        $sql = "DELETE FROM af_c024_acta_responsabilidad_uso";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        $sql = "ALTER TABLE af_c024_acta_responsabilidad_uso AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        $sql = "DELETE FROM af_c023_movimiento_detalle";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        $sql = "ALTER TABLE af_c023_movimiento_detalle AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        $sql = "DELETE FROM af_c028_acta_entrega";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        $sql = "ALTER TABLE af_c028_acta_entrega AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        $sql = "DELETE FROM af_c031_activos_faltantes_detalle";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        $sql = "ALTER TABLE af_c031_activos_faltantes_detalle AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        $sql = "DELETE FROM af_c025_baja_activo";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        $sql = "ALTER TABLE af_c025_baja_activo AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        $sql = "DELETE FROM af_c027_acta_incorporacion";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        $sql = "ALTER TABLE af_c027_acta_incorporacion AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        $sql = "DELETE FROM lg_e004_activo_fijo";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        $sql = "ALTER TABLE lg_e004_activo_fijo AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        $sql = "DELETE FROM af_c030_activos_faltantes";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        $sql = "ALTER TABLE af_c030_activos_faltantes AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        $sql = "DELETE FROM af_b001_activo";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        $sql = "ALTER TABLE af_b001_activo AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##  
        $sql = "DELETE FROM af_c007_situacion";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##  
        $sql = "ALTER TABLE af_c007_situacion AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##  
        foreach ($array as $f) {
            $sql = "INSERT INTO af_c007_situacion
                    SET 
                        ind_nombre_situacion='$f[ind_nombre_situacion]',
                        num_flag_depreciacion='$f[num_flag_depreciacion]',
                        num_flag_revaluacion='$f[num_flag_revaluacion]',
                        num_flag_sistema='$f[num_flag_sistema]',
                        num_estatus='$f[num_estatus]',
                        fec_ultima_modificacion='$f[fec_ultima_modificacion]',
                        fk_a018_num_seguridad_usuario='$f[fk_a018_num_seguridad_usuario]'";
            $db = $this->_db->prepare($sql);
            $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        }
        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para cargar maestro ubicación del activo
     *
     * @return Array
     */
    public function metCargarUbicacion($array)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();
        ##  
        $sql = "DELETE FROM af_c005_ubicacion";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##  
        $sql = "ALTER TABLE af_c005_ubicacion AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        foreach ($array as $f) {
            $sql = "INSERT INTO af_c005_ubicacion
                    SET 
                        ind_nombre_ubicacion='$f[ind_nombre_ubicacion]',
                        ind_codigo='$f[ind_codigo]',
                        num_nivel='$f[num_nivel]',
                        num_estatus='$f[num_estatus]',
                        fec_ultima_modificacion='$f[fec_ultima_modificacion]',
                        fk_a018_num_seguridad_usuario='$f[fk_a018_num_seguridad_usuario]'";
            $db = $this->_db->prepare($sql);
            $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        }
        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para cargar maestro tipos de seguro
     *
     * @return Array
     */
    public function metCargarTipoSeguro($array)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();
        ##  
        $sql = "DELETE FROM af_c016_tipo_seguro";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##  
        $sql = "ALTER TABLE af_c016_tipo_seguro AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        foreach ($array as $f) {
            $sql = "INSERT INTO af_c016_tipo_seguro
                    SET 
                        ind_nombre_tipo_seguro='$f[ind_nombre_tipo_seguro]',
                        num_estatus='$f[num_estatus]',
                        fec_ultima_modificacion='$f[fec_ultima_modificacion]',
                        fk_a018_num_seguridad_usuario='$f[fk_a018_num_seguridad_usuario]'";
            $db = $this->_db->prepare($sql);
            $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        }
        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para cargar maestro tipos de vehiculo
     *
     * @return Array
     */
    public function metCargarTipoVehiculo($array)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();
        ##  
        $sql = "DELETE FROM af_c017_tipo_vehiculo";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##  
        $sql = "ALTER TABLE af_c017_tipo_vehiculo AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        foreach ($array as $f) {
            $sql = "INSERT INTO af_c017_tipo_vehiculo
                    SET 
                        ind_nombre_tipo_vehiculo='$f[ind_nombre_tipo_vehiculo]',
                        num_estatus='$f[num_estatus]',
                        fec_ultima_modificacion='$f[fec_ultima_modificacion]',
                        fk_a018_num_seguridad_usuario='$f[fk_a018_num_seguridad_usuario]'";
            $db = $this->_db->prepare($sql);
            $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        }
        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para cargar maestro tipos de movimiento
     *
     * @return Array
     */
    public function metCargarTipoMovimiento($array)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();
        ##  
        $sql = "DELETE FROM af_c009_tipo_movimiento";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##  
        $sql = "ALTER TABLE af_c009_tipo_movimiento AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        foreach ($array as $f) {
            $sql = "INSERT INTO af_c009_tipo_movimiento
                    SET 
                        fk_a006_num_miscelaneo_detalle_tipo_movimiento='$f[fk_a006_num_miscelaneo_detalle_tipo_movimiento]',
                        ind_nombre_movimiento='$f[ind_nombre_movimiento]',
                        ind_codigo='$f[ind_codigo]',
                        num_movimiento='$f[num_movimiento]',
                        num_incorporacion='$f[num_incorporacion]',
                        num_desincorporacion='$f[num_desincorporacion]',
                        num_estatus='$f[num_estatus]',
                        fec_ultima_modificacion='$f[fec_ultima_modificacion]',
                        fk_a018_num_seguridad_usuario='$f[fk_a018_num_seguridad_usuario]'";
            $db = $this->_db->prepare($sql);
            $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        }
        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para cargar maestro clasificación del activo
     *
     * @return Array
     */
    public function metCargarClasificacion($array)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();
        ##  
        $sql = "DELETE FROM af_c008_clasificacion";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##  
        $sql = "ALTER TABLE af_c008_clasificacion AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        foreach ($array as $f) {
            $sql = "INSERT INTO af_c008_clasificacion
                    SET 
                        ind_codigo='$f[ind_codigo]',
                        ind_nombre_clasificacion='$f[ind_nombre_clasificacion]',
                        fk_cbb005_num_contabilidad='$f[fk_cbb005_num_contabilidad]',
                        num_nivel='$f[num_nivel]',
                        num_estatus='$f[num_estatus]',
                        fec_ultima_modificacion='$f[fec_ultima_modificacion]',
                        fk_a018_num_seguridad_usuario='$f[fk_a018_num_seguridad_usuario]'";
            $db = $this->_db->prepare($sql);
            $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        }
        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para cargar maestro categorias de depreciación
     *
     * @return Array
     */
    public function metCargarCategoria($array)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();
        ##  
        $sql = "DELETE FROM af_c021_tipo_transaccion";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##  
        $sql = "DELETE FROM af_c019_categoria";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##  
        $sql = "ALTER TABLE af_c019_categoria AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##  
        $sql = "ALTER TABLE af_c020_categoria_contabilidad AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##  
        foreach ($array as $f) {
            $sql = "INSERT INTO af_c019_categoria
                    SET 
                        ind_codigo='$f[ind_codigo]',
                        ind_nombre_categoria='$f[ind_nombre_categoria]',
                        fk_a006_num_tipo='$f[fk_a006_num_tipo]',
                        fk_a006_num_grupo='$f[fk_a006_num_grupo]',
                        fk_cbb004_num_cuenta_historica='$f[fk_cbb004_num_cuenta_historica]',
                        fk_cbb004_num_cuenta_historica_variacion='$f[fk_cbb004_num_cuenta_historica_variacion]',
                        fk_cbb004_num_cuenta_historica_revaluacion='$f[fk_cbb004_num_cuenta_historica_revaluacion]',
                        fk_cbb004_num_cuenta_depreciacion='$f[fk_cbb004_num_cuenta_depreciacion]',
                        fk_cbb004_num_cuenta_depreciacion_variacion='$f[fk_cbb004_num_cuenta_depreciacion_variacion]',
                        fk_cbb004_num_cuenta_depreciacion_revaluacion='$f[fk_cbb004_num_cuenta_depreciacion_revaluacion]',
                        fk_cbb004_num_cuenta_gastos='$f[fk_cbb004_num_cuenta_gastos]',
                        fk_cbb004_num_cuenta_gastos_revaluacion='$f[fk_cbb004_num_cuenta_gastos_revaluacion]',
                        fk_cbb004_num_cuenta_neto='$f[fk_cbb004_num_cuenta_neto]',
                        fk_cbb004_num_cuenta_rei='$f[fk_cbb004_num_cuenta_rei]',
                        fk_cbb004_num_cuenta_resultado='$f[fk_cbb004_num_cuenta_resultado]',
                        num_flag_inventariable='$f[num_flag_inventariable]',
                        num_estatus='$f[num_estatus]',
                        fec_ultima_modificacion='$f[fec_ultima_modificacion]',
                        fk_a018_num_seguridad_usuario='$f[fk_a018_num_seguridad_usuario]'";
            $db = $this->_db->prepare($sql);
            $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
            ##  
            $fk_afc019_num_categoria = $this->_db->lastInsertId();
            foreach ($f['Det'] as $fd) {
                $sql = "INSERT INTO af_c020_categoria_contabilidad
                        SET 
                            fk_afc019_num_categoria='$fk_afc019_num_categoria',
                            fk_cbb005_num_contabilidades='$fd[fk_cbb005_num_contabilidades]',
                            num_depreciacion='$fd[num_depreciacion]'";
                $db = $this->_db->prepare($sql);
                $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
            }
        }
        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para cargar maestro categorias de depreciación
     *
     * @return Array
     */
    public function metCargarTipoTransaccion($array)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();
        ##  
        $sql = "DELETE FROM af_c021_tipo_transaccion";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##  
        $sql = "ALTER TABLE af_c021_tipo_transaccion AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##  
        $sql = "ALTER TABLE af_c022_tipo_transaccion_cuenta AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##  
        foreach ($array as $f) {
            $sql = "INSERT INTO af_c021_tipo_transaccion
                    SET 
                        ind_codigo='$f[ind_codigo]',
                        ind_nombre_transaccion='$f[ind_nombre_transaccion]',
                        fk_cbc003_num_voucher='$f[fk_cbc003_num_voucher]',
                        ind_tipo='$f[ind_tipo]',
                        num_flag_sistema='$f[num_flag_sistema]',
                        num_estatus='$f[num_estatus]',
                        fec_ultima_modificacion='$f[fec_ultima_modificacion]',
                        fk_a018_num_seguridad_usuario='$f[fk_a018_num_seguridad_usuario]'";
            $db = $this->_db->prepare($sql);
            $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
            ##  
            $fk_afc021_num_tipo_transaccion = $this->_db->lastInsertId();
            foreach ($f['Det'] as $fd) {
                $sql = "INSERT INTO af_c022_tipo_transaccion_cuenta
                        SET 
                            fk_afc021_num_tipo_transaccion='$fk_afc021_num_tipo_transaccion',
                            fk_afc019_num_categoria='$fd[fk_afc019_num_categoria]',
                            fk_cbb004_num_cuenta='$fd[fk_cbb004_num_cuenta]',
                            fk_cbb005_num_contabilidades='$fd[fk_cbb005_num_contabilidades]',
                            ind_descripcion='$fd[ind_descripcion]',
                            ind_signo='$fd[ind_signo]'";
                $db = $this->_db->prepare($sql);
                $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
            }
        }
        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para cargar Menu
     *
     * @return Array
     */
    public function metCargarMenu($array)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();
        ##  
        $sql = "DELETE FROM a028_seguridad_menupermiso 
                WHERE fk_a027_num_seguridad_menu IN (SELECT pk_num_seguridad_menu FROM a027_seguridad_menu WHERE fk_a015_num_seguridad_aplicacion = 8)";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##  
        $sql = "DELETE FROM a027_seguridad_menu WHERE fk_a015_num_seguridad_aplicacion = 8";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##  
        foreach ($array as $f) {
            $sql = "INSERT INTO a027_seguridad_menu
                    SET 
                        ind_nombre='$f[ind_nombre]',
                        ind_descripcion='$f[ind_descripcion]',
                        cod_interno='$f[cod_interno]',
                        cod_padre='$f[cod_padre]',
                        num_nivel='$f[num_nivel]',
                        ind_ruta='$f[ind_ruta]',
                        ind_rol='$f[ind_rol]',
                        ind_icono='$f[ind_icono]',
                        num_estatus='$f[num_estatus]',
                        fk_a018_num_seguridad_usuario='$f[fk_a018_num_seguridad_usuario]',
                        fk_a015_num_seguridad_aplicacion='$f[fk_a015_num_seguridad_aplicacion]',
                        fec_ultima_modificacion = NOW()";
            $db = $this->_db->prepare($sql);
            $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        }
        ##  
        $sql = "INSERT INTO a028_seguridad_menupermiso (fk_a027_num_seguridad_menu, fk_a017_num_seguridad_perfil)
                SELECT pk_num_seguridad_menu AS fk_a027_num_seguridad_menu, '1' AS fk_a017_num_seguridad_perfil
                FROM a027_seguridad_menu
                WHERE fk_a015_num_seguridad_aplicacion = 8";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##  Consigna una transacción
        $this->_db->commit();
    }
}