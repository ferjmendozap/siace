<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Faltantes de Activos
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |20-07-2016               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class FaltantesModelo extends Modelo
{
    public function __construct()
    {
		parent::__construct();

		$this->idUsuario = Session::metObtener('idUsuario');
    }

    /**
     * Método para listar los registros de la tabla.
     *
     * @return Array
     */
    public function metListar($filtro=null)
    {
		$filtrar = "";
		if (isset($filtro['ffec_fechad'])) if ($filtro['ffec_fechad'] != "") $filtrar .= " AND afc30.fec_fecha >= '".Fecha::formatFecha($filtro['ffec_fechad'])."'";
		if (isset($filtro['ffec_fechah'])) if ($filtro['ffec_fechah'] != "") $filtrar .= " AND afc30.fec_fecha <= '".Fecha::formatFecha($filtro['ffec_fechah'])."'";
        if (isset($filtro['find_estado'])) if ($filtro['find_estado'] != "") $filtrar .= " AND afc30.ind_estado = '$filtro[find_estado]'";
        if (isset($filtro['find_situacion'])) if ($filtro['find_situacion'] != "") $filtrar .= " AND afc30.ind_situacion = '$filtro[find_situacion]'";

		$sql = "SELECT
					afc30.*
				FROM
					af_c030_activos_faltantes afc30
				WHERE
					1 $filtrar";
		$db = $this->_db->query($sql);
		$db->setFetchMode(PDO::FETCH_ASSOC);
		return $db->fetchAll();
    }

    /**
     * Método para obtener un registro de la tabla.
     *
     * @param Integer
     * @return Array
     */
    public function metObtener($id)
    {
        $sql = "SELECT
                    afc30.*
                FROM
                    af_c030_activos_faltantes afc30
                WHERE afc30.pk_num_activo_faltante = '$id'";
        $row = $this->_db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        return $row->fetch();
    }

    /**
     * Método para obtener los activos afectados.
     *
     * @param Integer
     * @return Array
     */
    public function metObtenerActivos($fk_afc030_num_activo_faltante)
    {
        $sql = "SELECT
                    afc31.*,
                    afc30.fk_afc009_num_movimiento,
                    afb1.cod_codigo_interno AS cod_activo,
                    afb1.ind_descripcion AS desc_activo,
                    afb1.ind_serie,
                    afb1.num_monto,
                    afb1.num_valor_mercado,
                    afb1.num_monto_referencial,
                    afb1.fk_afc005_num_ubicacion,
                    afb1.fk_afc007_num_situacion,
                    CONCAT_WS(' ', p1.ind_nombre1, p1.ind_nombre2, p1.ind_apellido1, p1.ind_apellido2) AS persona_usuario,
                    CONCAT_WS(' ', p2.ind_nombre1, p2.ind_nombre2, p2.ind_apellido1, p2.ind_apellido2) AS persona_responsable,
                    CONCAT_WS(' ', p3.ind_nombre1, p3.ind_nombre2, p3.ind_apellido1, p3.ind_apellido2) AS persona_dependencia,
                    rhc63.ind_descripcion_cargo AS persona_dependencia_cargo
                FROM
                    af_c031_activos_faltantes_detalle afc31
                    INNER JOIN af_c030_activos_faltantes afc30 ON (afc30.pk_num_activo_faltante = afc31.fk_afc030_num_activo_faltante)
                    INNER JOIN af_b001_activo afb1 ON (afb1.pk_num_activo = afc31.fk_afb001_num_activo)
                    INNER JOIN a023_centro_costo a23 ON (a23.pk_num_centro_costo = afb1.fk_a023_num_centro_costo)
                    INNER JOIN a004_dependencia a4 ON (a4.pk_num_dependencia = a23.fk_a004_num_dependencia)
                    INNER JOIN a001_organismo a1 ON (a1.pk_num_organismo = a4.fk_a001_num_organismo)
                    INNER JOIN a003_persona p1 ON (p1.pk_num_persona = afb1.fk_a003_num_persona_usuario)
                    INNER JOIN a003_persona p2 ON (p2.pk_num_persona = afb1.fk_a003_num_persona_responsable)
                    LEFT JOIN a003_persona p3 ON (p3.pk_num_persona = a4.fk_a003_num_persona_responsable)
                    LEFT JOIN rh_b001_empleado rhb1 ON (rhb1.fk_a003_num_persona = p3.pk_num_persona)
                    LEFT JOIN rh_c005_empleado_laboral rhc5 ON (rhc5.fk_rhb001_num_empleado = rhb1.pk_num_empleado)
                    LEFT JOIN rh_c063_puestos rhc63 ON (rhc63.pk_num_puestos = rhc5.fk_rhc063_num_puestos_cargo)
                WHERE afc31.fk_afc030_num_activo_faltante = '$fk_afc030_num_activo_faltante'"; 
                //die(json_encode(['status'=>'error', 'mensaje'=>[$sql]]));
        $db = $this->_db->query($sql);
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /**
     * Método para crear un nuevo registro.
     *
     * @param Array
     * @return Integer
     */
    public function metCrear($data)
    {
		##  Inicia una transacción
		$this->_db->beginTransaction();
        ##  
        $sql = "ALTER TABLE af_c030_activos_faltantes AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
        ##  
        $sql = "ALTER TABLE af_c031_activos_faltantes_detalle AUTO_INCREMENT=1";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

		##  datos generales
		$num_anio = substr($data['fec_fecha'], 0, 4);
		$ind_codigo = Number::codigo('af_c030_activos_faltantes', 'ind_codigo', 5, ['num_anio'=>$num_anio]);
		$sql = "INSERT INTO af_c030_activos_faltantes
				SET
					fk_afc009_num_movimiento = '$data[fk_afc009_num_movimiento]',
					fec_fecha = '$data[fec_fecha]',
					num_anio = '$num_anio',
					ind_codigo = '$ind_codigo',
					ind_comentarios = '$data[ind_comentarios]',
					fk_a003_num_preparado_por = '$this->idUsuario',
					fecha_preparado = NOW(),
					ind_situacion = '$data[ind_situacion]',
					ind_estado = '$data[ind_estado]',
                    fec_ultima_modificacion = NOW(),
					fk_a018_num_seguridad_usuario = '$this->idUsuario'";
		$db = $this->_db->prepare($sql);
		$db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
		##
		$id = $this->_db->lastInsertId();
        ##  detalles
        for ($i=0; $i < count($data['detalle_fk_afb001_num_activo']); $i++) 
        {
            $sql = "INSERT INTO af_c031_activos_faltantes_detalle
                    SET
                        pk_num_activo_faltante_detalle = '".$data['detalle_pk_num_activo_faltante_detalle'][$i]."',
                        fk_afc030_num_activo_faltante = '$id',
                        fk_afb001_num_activo = '".$data['detalle_fk_afb001_num_activo'][$i]."',
                        ind_comentarios = '".$data['detalle_ind_comentarios'][$i]."',
                        ind_estado = 'PE'";
            $db = $this->_db->prepare($sql);
            $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
        }

		##  Consigna una transacción
		$this->_db->commit();

		return $id;
    }

    /**
     * Método para actualizar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metModificar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $id = $data['pk_num_activo_faltante'];

        ##  datos generales
        $sql = "UPDATE af_c030_activos_faltantes
				SET
					fec_fecha = '$data[fec_fecha]',
					ind_comentarios = '$data[ind_comentarios]',
                    fec_ultima_modificacion = NOW(),
                    fk_a018_num_seguridad_usuario = '$this->idUsuario'
				WHERE pk_num_activo_faltante = '$data[pk_num_activo_faltante]'";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
        ##
        $id = $data['pk_num_activo_faltante'];
        ##  detalles
        $sql = "DELETE FROM af_c031_activos_faltantes_detalle
                WHERE
                    fk_afc030_num_activo_faltante = '$id'
                    AND fk_afc012_num_movimiento NOT IN (".implode(",",$data['detalle_pk_num_activo_faltante_detalle']).")";
        $db = $this->_db->prepare($sql);
        $db->execute();
        ##  
        for ($i=0; $i < count($data['detalle_fk_afb001_num_activo']); $i++) {
            $sql = "REPLACE INTO af_c031_activos_faltantes_detalle
                    SET
                        pk_num_activo_faltante_detalle = '".$data['detalle_pk_num_activo_faltante_detalle'][$i]."',
                        fk_afc030_num_activo_faltante = '$id',
                        fk_afb001_num_activo = '".$data['detalle_fk_afb001_num_activo'][$i]."',
                        ind_comentarios = '".$data['detalle_ind_comentarios'][$i]."',
                        ind_estado = '".$data['detalle_ind_estado'][$i]."'";
            $db = $this->_db->prepare($sql);
            $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
        }

        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para actualizar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metAnular($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        ##	consulto
        $field = $this->metObtener($data['pk_num_activo_faltante']);

        ##	valido
        if ($field['ind_estado'] == 'AP') $ind_estado = 'RV';
        elseif ($field['ind_estado'] == 'RV') $ind_estado = 'PR';
        elseif ($field['ind_estado'] == 'PR') $ind_estado = 'AN';
        else die(json_encode(['status'=>'error', 'mensaje'=>['Registro no puede ser anulado']]));

        ##  datos generales
        $sql = "UPDATE af_c030_activos_faltantes
				SET
					ind_estado = '$ind_estado',
					fk_a018_num_seguridad_usuario = '$this->idUsuario'
				WHERE pk_num_activo_faltante = '$data[pk_num_activo_faltante]'";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para actualizar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metRevisar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        ##  datos generales
        $sql = "UPDATE af_c030_activos_faltantes
				SET
					ind_estado = 'RV',
					fk_a018_num_seguridad_usuario = '$this->idUsuario'
				WHERE pk_num_activo_faltante = '$data[pk_num_activo_faltante]'";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para actualizar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metAprobar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        ##  
        $num_situacion = Select::field('af_c007_situacion','pk_num_situacion',['ind_codigo' => 'FI']);

        ##  actualizo datos generales del faltante
        $sql = "UPDATE af_c030_activos_faltantes
				SET
					ind_situacion = 'EI',
					ind_estado = 'AP',
					fk_a018_num_seguridad_usuario = '$this->idUsuario'
				WHERE pk_num_activo_faltante = '$data[pk_num_activo_faltante]'";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
        ##  
        for ($i=0; $i < count($data['detalle_fk_afb001_num_activo']); $i++) 
        {
            ##  actualizo detalle del activo
            $sql = "UPDATE af_c031_activos_faltantes_detalle
                    SET ind_estado = 'EI'
                    WHERE pk_num_activo_faltante_detalle = '".$data['detalle_pk_num_activo_faltante_detalle'][$i]."'";
            $db = $this->_db->prepare($sql);
            $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
            ##  actualizo activo
            $sql = "UPDATE af_b001_activo
                    SET
                        fk_afc005_num_ubicacion = '".$data['detalle_fk_afc005_num_ubicacion'][$i]."',
                        fk_afc007_num_situacion = '$num_situacion'
                    WHERE pk_num_activo = '".$data['detalle_fk_afb001_num_activo'][$i]."'";
            $db = $this->_db->prepare($sql);
            $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
        }

        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para actualizar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metTramitar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        ##  datos generales
        $sql = "UPDATE af_c030_activos_faltantes
                SET
                    fk_a001_num_organismo_externo = '$data[fk_a001_num_organismo_externo]',
                    fec_fecha_informe = '$data[fec_fecha_informe]',
                    ind_comentarios_informe = '$data[ind_comentarios_informe]',
                    fk_a018_num_seguridad_usuario = '$this->idUsuario'
                WHERE pk_num_activo_faltante = '$data[pk_num_activo_faltante]'";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        ##  actualizo detalle del activo
        for ($i=0; $i < count($data['detalle_fk_afb001_num_activo']); $i++) 
        {
            ##  actualizo detalle del activo
            $sql = "UPDATE af_c031_activos_faltantes_detalle
                    SET ind_estado = '".$data['detalle_ind_estado'][$i]."'
                    WHERE pk_num_activo_faltante_detalle = '".$data['detalle_pk_num_activo_faltante_detalle'][$i]."'";
            $db = $this->_db->prepare($sql);
            $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
        }

        ##  verifico
        $sql = "SELECT *
                FROM af_c031_activos_faltantes_detalle
                WHERE
                    fk_afc030_num_activo_faltante = '$data[pk_num_activo_faltante]'
                    AND (ind_estado <> 'FP' AND ind_estado <> 'FE')";
        $row = $this->_db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        $field_activos = $row->fetchAll();

        if (!count($field_activos))
        {
            ##  datos generales
            $sql = "UPDATE af_c030_activos_faltantes
                    SET
                        ind_estado = 'TR'
                        AND ind_situacion = 'CO'
                    WHERE pk_num_activo_faltante = '$data[pk_num_activo_faltante]'";
            $db = $this->_db->prepare($sql);
            $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
        }

        ##  Consigna una transacción
        $this->_db->commit();
    }
}
