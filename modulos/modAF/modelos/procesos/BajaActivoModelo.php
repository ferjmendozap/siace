	<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Baja de Activos
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bolívar                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |04-04-2016               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class BajaActivoModelo extends Modelo
{
    public function __construct()
    {
		parent::__construct();

		$this->idUsuario = Session::metObtener('idUsuario');
    }

    /**
     * Método para listar los registros de la tabla.
     *
     * @return Array
     */
    public function metListar($filtro=null)
    {
		$filtrar = "";
		if (isset($filtro['fnum_organismo'])) if ($filtro['fnum_organismo'] != "") $filtrar .= " AND o.pk_num_organismo = '$filtro[fnum_organismo]'";
		if (isset($filtro['fnum_dependencia'])) if ($filtro['fnum_dependencia'] != "") $filtrar .= " AND d.pk_num_dependencia = '$filtro[fnum_dependencia]'";
		if (isset($filtro['fnum_centro_costo'])) if ($filtro['fnum_centro_costo'] != "") $filtrar .= " AND cc.pk_num_centro_costo = '$filtro[fnum_centro_costo]'";
		if (isset($filtro['fnum_motivo'])) if ($filtro['fnum_motivo'] != "") $filtrar .= " AND ba.fk_a006_num_motivo = '$filtro[fnum_motivo]'";
		if (isset($filtro['ffec_fecha_bajad'])) if ($filtro['ffec_fecha_bajad'] != "") $filtrar .= " AND ba.fec_fecha_baja >= '".Fecha::formatFecha($filtro['ffec_fecha_bajad'])."'";
		if (isset($filtro['ffec_fecha_bajah'])) if ($filtro['ffec_fecha_bajah'] != "") $filtrar .= " AND ba.fec_fecha_baja <= '".Fecha::formatFecha($filtro['ffec_fecha_bajah'])."'";
		if (isset($filtro['find_estado'])) if ($filtro['find_estado'] != "") $filtrar .= " AND ba.ind_estado = '$filtro[find_estado]'";

		$sql = "SELECT
					ba.*
				FROM
					af_c025_baja_activo ba
				INNER JOIN af_b001_activo a ON (
					a.pk_num_activo = ba.fk_afb001_num_activo
				)
				INNER JOIN a023_centro_costo cc ON (
					cc.pk_num_centro_costo = a.fk_a023_num_centro_costo
				)
				INNER JOIN a004_dependencia d ON (
					d.pk_num_dependencia = cc.fk_a004_num_dependencia
				)
				INNER JOIN a001_organismo o ON (
					o.pk_num_organismo = d.fk_a001_num_organismo
				)
				WHERE
					1 $filtrar";
		$db = $this->_db->query($sql);
		$db->setFetchMode(PDO::FETCH_ASSOC);
		return $db->fetchAll();
    }

    /**
     * Método para crear un nuevo registro.
     *
     * @param Array
     * @return Integer
     */
    public function metCrear($data)
    {
		##  Inicia una transacción
		$this->_db->beginTransaction();

		##  datos generales
		$sql = "INSERT INTO af_c025_baja_activo
				SET
					fk_afb001_num_activo = '$data[fk_afb001_num_activo]',
					fk_afc021_num_tipo_transaccion = '$data[fk_afc021_num_tipo_transaccion]',
					fk_afc009_num_movimiento = '$data[fk_afc009_num_movimiento]',
					fk_a006_num_motivo = '$data[fk_a006_num_motivo]',
					fec_fecha_baja = '$data[fec_fecha_baja]',
					ind_comentarios = '$data[ind_comentarios]',
					ind_nro_resolucion = '$data[ind_nro_resolucion]',
					fec_fecha = NOW(),
					fk_a003_num_preparado_por = '$this->idUsuario',
					fecha_preparado = NOW(),
					ind_estado = '$data[ind_estado]',
					fk_a018_num_seguridad_usuario = '$this->idUsuario'";
		$db = $this->_db->prepare($sql);
		$db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
		##
		$id = $this->_db->lastInsertId();
		##  cuentas
		$sql = "INSERT INTO af_c026_baja_activo_cuenta (
					pk_num_baja_activo_cuenta,
					fk_afc025_num_baja_activo,
					fk_afc019_num_categoria,
					fk_cbb004_num_cuenta,
					fk_cbb005_num_contabilidades,
					ind_descripcion,
					ind_signo
				)
				SELECT
					CONCAT('$id', pk_num_tipo_transaccion_cuenta) AS pk_num_baja_activo_cuenta,
					'$id' AS fk_afc025_num_baja_activo,
					fk_afc019_num_categoria,
					fk_cbb004_num_cuenta,
					fk_cbb005_num_contabilidades,
					ind_descripcion,
					ind_signo
				FROM af_c022_tipo_transaccion_cuenta
				WHERE fk_afc021_num_tipo_transaccion = '$data[fk_afc021_num_tipo_transaccion]'";
		$db = $this->_db->prepare($sql);
		$db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

		##  Consigna una transacción
		$this->_db->commit();

		return $id;
    }

    /**
     * Método para actualizar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metModificar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $id = $data['pk_num_baja_activo'];

        ##  datos generales
        $sql = "UPDATE af_c025_baja_activo
				SET
					fk_afc021_num_tipo_transaccion = '$data[fk_afc021_num_tipo_transaccion]',
					fk_afc009_num_movimiento = '$data[fk_afc009_num_movimiento]',
					fk_a006_num_motivo = '$data[fk_a006_num_motivo]',
					fec_fecha_baja = '$data[fec_fecha_baja]',
					ind_comentarios = '$data[ind_comentarios]',
					ind_nro_resolucion = '$data[ind_nro_resolucion]',
					fk_a018_num_seguridad_usuario = '$this->idUsuario'
				WHERE pk_num_baja_activo = '$data[pk_num_baja_activo]'";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
		##  cuentas
        $db = $this->_db->prepare("DELETE FROM af_c026_baja_activo_cuenta WHERE fk_afc025_num_baja_activo = '$data[pk_num_baja_activo]'");
        $db->execute();
		$sql = "INSERT INTO af_c026_baja_activo_cuenta (
					pk_num_baja_activo_cuenta,
					fk_afc025_num_baja_activo,
					fk_afc019_num_categoria,
					fk_cbb004_num_cuenta,
					fk_cbb005_num_contabilidades,
					ind_descripcion,
					ind_signo
				)
				SELECT
					CONCAT('$id', pk_num_tipo_transaccion_cuenta) AS fk_afc025_num_baja_activo_cuenta,
					'$data[pk_num_baja_activo]' AS fk_afc025_num_baja_activo,
					fk_afc019_num_categoria,
					fk_cbb004_num_cuenta,
					fk_cbb005_num_contabilidades,
					ind_descripcion,
					ind_signo
				FROM af_c022_tipo_transaccion_cuenta
				WHERE fk_afc021_num_tipo_transaccion = '$data[fk_afc021_num_tipo_transaccion]'";
		$db = $this->_db->prepare($sql);
		$db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para actualizar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metAnular($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        ##	consulto
        $field = $this->metObtener($data['pk_num_baja_activo']);

        ##	valido
        if ($field['ind_estado'] == 'AP') $ind_estado = 'RV';
        elseif ($field['ind_estado'] == 'RV') $ind_estado = 'PR';
        elseif ($field['ind_estado'] == 'PR') $ind_estado = 'AN';
        else die(json_encode(['status'=>'error', 'mensaje'=>['Registro no puede ser anulado']]));

        ##  datos generales
        $sql = "UPDATE af_c025_baja_activo
				SET
					ind_estado = '$ind_estado',
					fk_a018_num_seguridad_usuario = '$this->idUsuario'
				WHERE pk_num_baja_activo = '$data[pk_num_baja_activo]'";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para actualizar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metRevisar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        ##  datos generales
        $sql = "UPDATE af_c025_baja_activo
				SET
					ind_estado = 'RV',
					fk_a018_num_seguridad_usuario = '$this->idUsuario'
				WHERE pk_num_baja_activo = '$data[pk_num_baja_activo]'";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para actualizar un registro.
     *
     * @param Array
     * @return Void
     */
    public function metAprobar($data, $fa)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        ##  datos generales
        $sql = "UPDATE af_c025_baja_activo
				SET
					ind_estado = 'AP',
					fk_a018_num_seguridad_usuario = '$this->idUsuario'
				WHERE pk_num_baja_activo = '$data[pk_num_baja_activo]'";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        ##  historico
        $fb = $this->metObtener($data['pk_num_baja_activo']);
        $sql = "INSERT INTO af_c029_activo_historico
                SET
                    fk_afb001_num_activo = '$fb[fk_afb001_num_activo]',
                    fk_a023_num_centro_costo = '$fa[fk_a023_num_centro_costo]',
                    fk_afc007_num_situacion = '$fa[fk_afc007_num_situacion]',
                    fk_afc009_num_movimiento = '$fb[fk_afc009_num_movimiento]',
                    fk_afc005_num_ubicacion = '$fa[fk_afc005_num_ubicacion]',
                    fk_a006_num_motivo = '$fb[fk_a006_num_motivo]',
                    cod_codigo_interno = '$fa[cod_codigo_interno]',
                    fec_fecha_ingreso = '$fa[fec_ingreso]',
                    fec_fecha_transaccion = '$fb[fec_fecha_baja]',
                    fk_a018_num_seguridad_usuario = '$this->idUsuario',
                    fec_ultima_modificacion = NOW()";
        $db = $this->_db->prepare($sql);
        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
     * Método para obtener un registro de la tabla.
     *
     * @param Integer
     * @return Array
     */
    public function metObtener($id)
    {
		$sql = "SELECT
					ba.*,
					a.cod_codigo_interno AS cod_activo,
					a.ind_descripcion AS desc_activo,
                    a.num_monto,
                    a.ind_serie,
					o.ind_descripcion_empresa,
	                d.ind_dependencia,
	                cc.ind_descripcion_centro_costo,
	                c.ind_nombre_categoria,
	                u.ind_nombre_ubicacion,
                	CONCAT(pr.ind_nombre1, ' ', pr.ind_nombre2, ' ', pr.ind_apellido1, ' ', pr.ind_apellido2) AS persona_responsable,
					CONCAT(p1.ind_nombre1, ' ', p1.ind_nombre2, ' ', p1.ind_apellido1, ' ', p1.ind_apellido2) AS preparado_por
				FROM
					af_c025_baja_activo ba
				INNER JOIN af_b001_activo a ON (
					a.pk_num_activo = ba.fk_afb001_num_activo
				)
				LEFT JOIN a003_persona p1 ON (
					p1.pk_num_persona = ba.fk_a003_num_preparado_por
				)
				INNER JOIN a023_centro_costo cc ON (
					cc.pk_num_centro_costo = a.fk_a023_num_centro_costo
				)
				INNER JOIN a004_dependencia d ON (
					d.pk_num_dependencia = cc.fk_a004_num_dependencia
				)
				INNER JOIN a001_organismo o ON (
					o.pk_num_organismo = d.fk_a001_num_organismo
				)
                INNER JOIN af_c019_categoria c ON (
                	c.pk_num_categoria = a.fk_afc019_num_categoria
                )
                INNER JOIN af_c005_ubicacion u ON (
                	u.pk_num_ubicacion = a.fk_afc005_num_ubicacion
                )
                INNER JOIN a003_persona pr ON (
                	pr.pk_num_persona = a.fk_a003_num_persona_responsable
                )
				WHERE ba.pk_num_baja_activo = '$id'";
		$row = $this->_db->query($sql);
		$row->setFetchMode(PDO::FETCH_ASSOC);
		return $row->fetch();
    }

    /**
     * Método para obtener las cuentas asociadas al registro.
     *
     * @param Integer
     * @return Array
     */
    public function metObtenerCuentas($fk_afc025_num_baja_activo, $fk_cbb005_num_contabilidades = NULL)
    {
        $filtro = '';
        if ($fk_cbb005_num_contabilidades) 
        {
            $filtro .= "AND bac.fk_cbb005_num_contabilidades = '$fk_cbb005_num_contabilidades'";
        }

        $sql = "SELECT
                    bac.*,
                    c.ind_codigo AS num_categoria,
                    pc.cod_cuenta AS num_cuenta,
                    pc.ind_descripcion AS nombre_cuenta,
                    ct.ind_descripcion AS nombre_contabilidad
                FROM
                    af_c026_baja_activo_cuenta bac
                    INNER JOIN cb_b004_plan_cuenta pc ON (pc.pk_num_cuenta = bac.fk_cbb004_num_cuenta)
                    INNER JOIN af_c019_categoria c ON (c.pk_num_categoria = bac.fk_afc019_num_categoria)
                    INNER JOIN cb_b005_contabilidades ct ON (ct.pk_num_contabilidades = bac.fk_cbb005_num_contabilidades)
                WHERE
                    bac.fk_afc025_num_baja_activo = '$fk_afc025_num_baja_activo'
                    $filtro";
        $db = $this->_db->query($sql);
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }
}
