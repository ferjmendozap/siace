<section class="style-default-bright">
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="card card-underline">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="" class="table table-striped table-hover datatable1">
                                <thead>
                                    <tr>
                                        <th width="50">Id.</th>
                                        <th width="100">C&eacute;dula</th>
                                        <th>Nombre Completo</th>
                                        <th width="75">Estatus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {foreach item=registro from=$listado}
                                    <tr id="id{$registro.pk_num_persona}" onclick="seleccionar(['{$campos.campo1}','{$campos.campo2}'], ['{$registro.pk_num_persona}','{$registro.nombre_completo}']);">
                                        <td><label>{$registro.pk_num_persona}</label></td>
                                        <td><label>{$registro.ind_cedula_documento}</label></td>
                                        <td><label>{$registro.nombre_completo}</label></td>
                                        <td><i class="{if $registro.ind_estado=='A'}md md-check{else}md md-not-interested{/if}"></i></td>
                                    </tr>
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        inicializar();
    });
</script>