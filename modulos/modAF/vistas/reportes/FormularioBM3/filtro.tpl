<form id="formAjax" class="form" role="form" method="post">
    <div class="card card-underline">
        <div class="card-head">
            <div class="col-sm-12">
                <h2 class="text-primary">Relación de Bienes Muebles Faltantes</h2>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group form-group-sm">
                        <select name="ffk_a001_num_organismo" id="ffk_a001_num_organismo" class="form-control input-sm" onchange="select('{$_Parametros.url}select/a004_dependencia', $('#ffk_a004_num_dependencia'), 'fk_a001_num_organismo='+$(this).val(), ['ffk_a023_num_centro_costo']);">
                            <option value="">&nbsp;</option>
                            {Select::lista('a001_organismo','pk_num_organismo','ind_descripcion_empresa')}
                        </select>
                        <label for="ffk_a001_num_organismo">Organismo</label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group form-group-sm">
                        <select name="find_situacion" id="find_situacion" class="form-control input-sm">
                            <option value="">&nbsp;</option>
                            {Select::options('af_situacion_faltante')}
                        </select>
                        <label for="find_situacion">Situaci&oacute;n</label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group form-group-sm" id="ffk_afc009_num_movimientoError">
                        <select name="ffk_afc009_num_movimiento" id="ffk_afc009_num_movimiento" class="form-control input-sm">
                            <option value="">&nbsp;</option>
                            {Select::options('af_estado_faltante')}
                        </select>
                        <label for="find_situacion">Estado</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group form-group-sm">
                        <select name="ffk_a004_num_dependencia" id="ffk_a004_num_dependencia" class="form-control input-sm" onchange="select('{$_Parametros.url}select/a023_centro_costo', $('#ffk_a023_num_centro_costo'), 'fk_a004_num_dependencia='+$(this).val());">
                            <option value="">&nbsp;</option>
                            {Select::lista('a004_dependencia','pk_num_dependencia','ind_dependencia','',0,['fk_a001_num_organismo'=>''])}
                        </select>
                        <label for="ffk_a004_num_dependencia">Dependencia</label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group form-group-sm">
                        <input type="text" name="ffec_fechad" id="ffec_fechad" class="form-control input-sm date" data-provide="datepicker" data-date-format="dd-mm-yyyy" maxlength="10">
                        <label for="ffec_fechad">Desde</label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group form-group-sm">
                        <input type="text" name="ffec_fechah" id="ffec_fechah" class="form-control input-sm date" data-provide="datepicker" data-date-format="dd-mm-yyyy" maxlength="10">
                        <label for="ffec_fechah">Hasta</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group form-group-sm">
                        <select name="ffk_a023_num_centro_costo" id="ffk_a023_num_centro_costo" class="form-control input-sm">
                            <option value="">&nbsp;</option>
                            {Select::lista('a023_centro_costo','pk_num_centro_costo','ind_descripcion_centro_costo','',0,['fk_a004_num_dependencia'=>''])}
                        </select>
                        <label for="ffk_a023_num_centro_costo">Centro de Costo</label>
                    </div>
                </div>
            </div>

            <center>
                <div class="btn-group">
                    <button type="button" id="imprimir" class="btn ink-reaction btn-raised btn-info btn-sm">
                        Buscar
                    </button>
                    <button type="button" id="imprimir_lanzador" class="btn ink-reaction btn-raised btn-info btn-sm hidden"
                            data-toggle="modal"
                            data-target="#formModal"
                            data-keyboard="false"
                            data-backdrop="static" titulo="Formulario BM-3">
                    </button>
                </div>
            </center>
        </div>
    </div>
</form>

<script type="text/javascript">

    $(document).ready(function() {
        //  base
        var $base = '{$_Parametros.url}modAF/reportes/FormularioBM3CONTROL/';

        //  mostrar pdf
        $('#imprimir').click(function() {
            $('#imprimir_lanzador').click();
        });
        $('#imprimir_lanzador').click(function() {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'FrameMET', $('#formAjax').serialize(), function($dato) {
                $('#ContenidoModal').html($dato);
            });
        });
    });
</script>
