<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Tipo de Movimientos</h2>
    </div>

    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="card card-underline">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover datatable1">
                                <thead>
                                    <tr>
                                        <th width="50">Id.</th>
                                        <th width="150">Tipo de Movimiento</th>
                                        <th>Descripcion</th>
                                        <th width="35">Mov.</th>
                                        <th width="35">Inc.</th>
                                        <th width="35">Des.</th>
                                        <th width="75">Estatus</th>
                                        <th width="100" style="text-align: center;">Accion</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {foreach item=registro from=$listado}
                                        <tr id="id{$registro.pk_num_movimiento}">
                                            <td><label>{$registro.pk_num_movimiento}</label></td>
                                            <td><label>{$registro.tipo_movimiento}</label></td>
                                            <td><label>{$registro.ind_nombre_movimiento}</label></td>
                                            <td><i class="{if $registro.num_movimiento==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                            <td><i class="{if $registro.num_incorporacion==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                            <td><i class="{if $registro.num_desincorporacion==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                            <td><i class="{if $registro.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                            <td>
                                                {if in_array('AF-01-90-01-01-02',$_Parametros.perfil)}
                                                    <button idRegistro="{$registro.pk_num_movimiento}" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Tipo de Movimiento" title="Editar Tipo de Movimiento">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                                {if in_array('AF-01-90-01-01-03',$_Parametros.perfil)}
                                                    <button idRegistro="{$registro.pk_num_movimiento}" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Registro!!" title="Eliminar Tipo de Movimiento">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                            </td>
                                        </tr>
                                    {/foreach}
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="8">
                                            {if in_array('AF-01-90-01-01-01',$_Parametros.perfil)}
                                                <button id="nuevo" class="logsUsuario btn ink-reaction btn-raised btn-info btn-sm"
                                                        data-toggle="modal"
                                                        data-target="#formModal"
                                                        data-keyboard="false"
                                                        data-backdrop="static" titulo="Nuevo Tipo de Movimiento">
                                                    <i class="md md-create"></i> Nuevo Tipo de Movimiento
                                                </button>
                                            {/if}
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {
        //  base
        var $base = '{$_Parametros.url}modAF/maestros/TipoMovimientoCONTROL/';

        //  nuevo registro
        $('#nuevo').click(function() {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'nuevoMET', '', function($dato) {
                $('#ContenidoModal').html($dato);
            });
        });

        $('.datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'editarMET', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('.datatable1 tbody').on( 'click', '.eliminar', function () {
            var id = $(this).attr('idRegistro');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                $.post($base+'eliminarMET', { id: id }, function(dato){
                    //  mensaje
                    var table = $('.datatable1').DataTable();
                    table.row('#id'+id).remove().draw(false);
                    swal("Registro Eliminado!", dato['mensaje'], "success");
                    $('#cerrar').click();
                },'json');
            });
        });
    });
</script>
