<form action="{$_Parametros.url}modAF/maestros/CategoriaCONTROL/{$form.metodo}MET" id="formAjax" class="form" role="form" method="post" autocomplete="off">
    <input type="hidden" name="pk_num_categoria" id="pk_num_categoria" value="{$form.pk_num_categoria}">
    <div class="modal-body">
        <div class="col-sm-5">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-sm floating-label" id="ind_codigoError">
                        <input type="text" name="ind_codigo" id="ind_codigo" value="{$form.ind_codigo}" class="form-control input-sm" maxlength="15">
                        <label for="ind_codigo">C&oacute;digo</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group form-group-sm floating-label" id="ind_nombre_categoriaError">
                        <input type="text" name="ind_nombre_categoria" id="ind_nombre_categoria" value="{$form.ind_nombre_categoria}" class="form-control input-sm" maxlength="100">
                        <label for="ind_nombre_categoria">Nombre</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-9">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" name="num_flag_inventariable" id="num_flag_inventariable" value="1" {if $form.num_flag_inventariable==1}checked{/if}>
                            <span>Categor&iacute;a Inventariable</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" name="num_estatus" id="num_estatus" value="1" {if $form.num_estatus==1}checked{/if}>
                            <span>Estatus</span>
                        </label>
                    </div>
                </div>
            </div>
            <h3>Contabilidades V&aacute;lidas</h3>
            <div class="card card-underline">
                <div class="card-head">
                    <div class="tools">
                        <button id="insertar" class="btn ink-reaction btn-raised btn-info btn-xs" data-lista="contabilidades">
                            <i class="md md-add"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover" style="min-width: 400px;">
                            <thead>
                                <tr>
                                    <th width="60%">Contabilidad</th>
                                    <th width="60%">Depreciaci&oacute;n (%)</th>
                                    <th width="25">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody id="lista_contabilidades">
                                {$numero = 0}
                                {foreach item=registro from=$form.listado_contabilidad}
                                    {$numero=$numero+1}
                                    <tr id="contabilidades{$numero}">
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-sm floating-label">
                                                    <input type="hidden" name="contabilidades_pk_num_categoria_contabilidad[]" value="{$registro.pk_num_categoria_contabilidad}">
                                                    <select name="contabilidades_fk_cbb005_num_contabilidades[]" class="form-control input-sm">
                                                        <option value="">&nbsp;</option>
                                                        {Select::lista('cb_b005_contabilidades','pk_num_contabilidades','ind_descripcion',$registro.fk_cbb005_num_contabilidades)}
                                                    </select>
                                                    <label for="contabilidades_fk_cbb005_num_contabilidades" style="padding:0px;">&nbsp;</label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-sm floating-label">
                                                    <input type="text" name="contabilidades_num_depreciacion[]" value="{number_format($registro.num_depreciacion,2,',','.')}" class="form-control input-sm money" style="text-align:right;">
                                                    <label for="contabilidades_num_depreciacion" style="padding:0px;">&nbsp;</label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <button class="btn ink-reaction btn-raised btn-xs btn-danger" style="margin: 0px 0px 55px -10px;" onclick="$('#contabilidades{$numero}').remove();">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-7">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-sm floating-label" id="fk_a006_num_tipoError">
                        <select name="fk_a006_num_tipo" id="fk_a006_num_tipo" class="form-control input-sm">
                            <option value="">&nbsp;</option>
                            {Select::miscelaneo('AFTIPODEPR',$form.fk_a006_num_tipo,0)}
                        </select>
                        <label for="fk_a006_num_tipo">Tipo de Deprec.</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-group-sm floating-label" id="fk_a006_num_grupoError">
                        <select name="fk_a006_num_grupo" id="fk_a006_num_grupo" class="form-control input-sm">
                            <option value="">&nbsp;</option>
                            {Select::miscelaneo('AFGRUPCAT',$form.fk_a006_num_grupo,0)}
                        </select>
                        <label for="fk_a006_num_grupo">Grupo Categor&iacute;a</label>
                    </div>
                </div>
            </div>
            <h4>Cuentas Contable Activos</h4>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group form-group-sm" id="num_cuenta_historicaError">
                        <input type="hidden" name="fk_cbb004_num_cuenta_historica" id="fk_cbb004_num_cuenta_historica" value="{$form.fk_cbb004_num_cuenta_historica}">
                        <input type="text" name="num_cuenta_historica" id="num_cuenta_historica" value="{$form.num_cuenta_historica}" class="form-control input-sm cuenta" maxlength="15">
                        <label for="num_cuenta_historica">Valor Hist&oacute;rico</label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                            style="margin: 0px 0px -45px -10px;"
                            data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Plan de Cuentas"
                            onclick="selector($(this), '{$_Parametros.url}modCB/PlanCuentaCONTROL/selectorCuentasContablesMET', ['fk_cbb004_num_cuenta_historica','num_cuenta_historica'], 'caso=cuenta');">
                        <i class="md md-search"></i>
                    </button>
                </div>
                <div class="col-sm-3">
                    <div class="form-group form-group-sm" id="num_cuenta_historica_variacionError">
                        <input type="hidden" name="fk_cbb004_num_cuenta_historica_variacion" id="fk_cbb004_num_cuenta_historica_variacion" value="{$form.fk_cbb004_num_cuenta_historica_variacion}">
                        <input type="text" name="num_cuenta_historica_variacion" id="num_cuenta_historica_variacion" value="{$form.num_cuenta_historica_variacion}" class="form-control input-sm cuenta" maxlength="15">
                        <label for="num_cuenta_historica_variacion">Adiciones</label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                            style="margin: 0px 0px -45px -10px;"
                            data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Plan de Cuentas"
                            onclick="selector($(this), '{$_Parametros.url}modCB/PlanCuentaCONTROL/selectorCuentasContablesMET', ['fk_cbb004_num_cuenta_historica_variacion','num_cuenta_historica_variacion'], 'caso=cuenta');">
                        <i class="md md-search"></i>
                    </button>
                </div>
                <div class="col-sm-3">
                    <div class="form-group form-group-sm" id="num_cuenta_historica_revaluacionError">
                        <input type="hidden" name="fk_cbb004_num_cuenta_historica_revaluacion" id="fk_cbb004_num_cuenta_historica_revaluacion" value="{$form.fk_cbb004_num_cuenta_historica_revaluacion}">
                        <input type="text" name="num_cuenta_historica_revaluacion" id="num_cuenta_historica_revaluacion" value="{$form.num_cuenta_historica_revaluacion}" class="form-control input-sm cuenta" maxlength="15">
                        <label for="num_cuenta_historica_revaluacion">Ajuste x Inflaci&oacute;n</label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                            style="margin: 0px 0px -45px -10px;"
                            data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Plan de Cuentas"
                            onclick="selector($(this), '{$_Parametros.url}modCB/PlanCuentaCONTROL/selectorCuentasContablesMET', ['fk_cbb004_num_cuenta_historica_revaluacion','num_cuenta_historica_revaluacion'], 'caso=cuenta');">
                        <i class="md md-search"></i>
                    </button>
                </div>
            </div>
            <h4>Cuentas Contable Depreciaci&oacute;n</h4>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group form-group-sm" id="num_cuenta_depreciacionError">
                        <input type="hidden" name="fk_cbb004_num_cuenta_depreciacion" id="fk_cbb004_num_cuenta_depreciacion" value="{$form.fk_cbb004_num_cuenta_depreciacion}">
                        <input type="text" name="num_cuenta_depreciacion" id="num_cuenta_depreciacion" value="{$form.num_cuenta_depreciacion}" class="form-control input-sm cuenta" maxlength="15">
                        <label for="num_cuenta_depreciacion">Para Depreciaci&oacute;n</label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                            style="margin: 0px 0px -45px -10px;"
                            data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Plan de Cuentas"
                            onclick="selector($(this), '{$_Parametros.url}modCB/PlanCuentaCONTROL/selectorCuentasContablesMET', ['fk_cbb004_num_cuenta_depreciacion','num_cuenta_depreciacion'], 'caso=cuenta');">
                        <i class="md md-search"></i>
                    </button>
                </div>
                <div class="col-sm-3">
                    <div class="form-group form-group-sm" id="num_cuenta_depreciacion_variacionError">
                        <input type="hidden" name="fk_cbb004_num_cuenta_depreciacion_variacion" id="fk_cbb004_num_cuenta_depreciacion_variacion" value="{$form.fk_cbb004_num_cuenta_depreciacion_variacion}">
                        <input type="text" name="num_cuenta_depreciacion_variacion" id="num_cuenta_depreciacion_variacion" value="{$form.num_cuenta_depreciacion_variacion}" class="form-control input-sm cuenta" maxlength="15">
                        <label for="num_cuenta_depreciacion_variacion">Adiciones</label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                            style="margin: 0px 0px -45px -10px;"
                            data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Plan de Cuentas"
                            onclick="selector($(this), '{$_Parametros.url}modCB/PlanCuentaCONTROL/selectorCuentasContablesMET', ['fk_cbb004_num_cuenta_depreciacion_variacion','num_cuenta_depreciacion_variacion'], 'caso=cuenta');">
                        <i class="md md-search"></i>
                    </button>
                </div>
                <div class="col-sm-3">
                    <div class="form-group form-group-sm" id="num_cuenta_depreciacion_revaluacionError">
                        <input type="hidden" name="fk_cbb004_num_cuenta_depreciacion_revaluacion" id="fk_cbb004_num_cuenta_depreciacion_revaluacion" value="{$form.fk_cbb004_num_cuenta_depreciacion_revaluacion}">
                        <input type="text" name="num_cuenta_depreciacion_revaluacion" id="num_cuenta_depreciacion_revaluacion" value="{$form.num_cuenta_depreciacion_revaluacion}" class="form-control input-sm cuenta" maxlength="15">
                        <label for="num_cuenta_depreciacion_revaluacion">Ajuste x Inflaci&oacute;n</label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                            style="margin: 0px 0px -45px -10px;"
                            data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Plan de Cuentas"
                            onclick="selector($(this), '{$_Parametros.url}modCB/PlanCuentaCONTROL/selectorCuentasContablesMET', ['fk_cbb004_num_cuenta_depreciacion_revaluacion','num_cuenta_depreciacion_revaluacion'], 'caso=cuenta');">
                        <i class="md md-search"></i>
                    </button>
                </div>
            </div>
            <h4>Cuentas de Gasto</h4>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group form-group-sm" id="num_cuenta_gastosError">
                        <input type="hidden" name="fk_cbb004_num_cuenta_gastos" id="fk_cbb004_num_cuenta_gastos" value="{$form.fk_cbb004_num_cuenta_gastos}">
                        <input type="text" name="num_cuenta_gastos" id="num_cuenta_gastos" value="{$form.num_cuenta_gastos}" class="form-control input-sm cuenta" maxlength="15">
                        <label for="num_cuenta_gastos">Depreciaci&oacute;n</label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                            style="margin: 0px 0px -45px -10px;"
                            data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Plan de Cuentas"
                            onclick="selector($(this), '{$_Parametros.url}modCB/PlanCuentaCONTROL/selectorCuentasContablesMET', ['fk_cbb004_num_cuenta_gastos','num_cuenta_gastos'], 'caso=cuenta');">
                        <i class="md md-search"></i>
                    </button>
                </div>
                <div class="col-sm-3">
                    <div class="form-group form-group-sm" id="num_cuenta_gastos_revaluacionError">
                        <input type="hidden" name="fk_cbb004_num_cuenta_gastos_revaluacion" id="fk_cbb004_num_cuenta_gastos_revaluacion" value="{$form.fk_cbb004_num_cuenta_gastos_revaluacion}">
                        <input type="text" name="num_cuenta_gastos_revaluacion" id="num_cuenta_gastos_revaluacion" value="{$form.num_cuenta_gastos_revaluacion}" class="form-control input-sm cuenta" maxlength="15">
                        <label for="num_cuenta_gastos_revaluacion">Ajuste x Inflaci&oacute;n</label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                            style="margin: 0px 0px -45px -10px;"
                            data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Plan de Cuentas"
                            onclick="selector($(this), '{$_Parametros.url}modCB/PlanCuentaCONTROL/selectorCuentasContablesMET', ['fk_cbb004_num_cuenta_gastos_revaluacion','num_cuenta_gastos_revaluacion'], 'caso=cuenta');">
                        <i class="md md-search"></i>
                    </button>
                </div>
            </div>
            <h4>Otras Cuentas Contable</h4>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group form-group-sm" id="num_cuenta_reiError">
                        <input type="hidden" name="fk_cbb004_num_cuenta_rei" id="fk_cbb004_num_cuenta_rei" value="{$form.fk_cbb004_num_cuenta_rei}">
                        <input type="text" name="num_cuenta_rei" id="num_cuenta_rei" value="{$form.num_cuenta_rei}" class="form-control input-sm cuenta" maxlength="15">
                        <label for="num_cuenta_rei">R.E.I</label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                            style="margin: 0px 0px -45px -10px;"
                            data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Plan de Cuentas"
                            onclick="selector($(this), '{$_Parametros.url}modCB/PlanCuentaCONTROL/selectorCuentasContablesMET', ['fk_cbb004_num_cuenta_rei','num_cuenta_rei'], 'caso=cuenta');">
                        <i class="md md-search"></i>
                    </button>
                </div>
                <div class="col-sm-3">
                    <div class="form-group form-group-sm" id="num_cuenta_netoError">
                        <input type="hidden" name="fk_cbb004_num_cuenta_neto" id="fk_cbb004_num_cuenta_neto" value="{$form.fk_cbb004_num_cuenta_neto}">
                        <input type="text" name="num_cuenta_neto" id="num_cuenta_neto" value="{$form.num_cuenta_neto}" class="form-control input-sm cuenta" maxlength="15">
                        <label for="num_cuenta_neto">Valor Neto</label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                            style="margin: 0px 0px -45px -10px;"
                            data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Plan de Cuentas"
                            onclick="selector($(this), '{$_Parametros.url}modCB/PlanCuentaCONTROL/selectorCuentasContablesMET', ['fk_cbb004_num_cuenta_neto','num_cuenta_neto'], 'caso=cuenta');">
                        <i class="md md-search"></i>
                    </button>
                </div>
                <div class="col-sm-3">
                    <div class="form-group form-group-sm" id="num_cuenta_resultadoError">
                        <input type="hidden" name="fk_cbb004_num_cuenta_resultado" id="fk_cbb004_num_cuenta_resultado" value="{$form.fk_cbb004_num_cuenta_resultado}">
                        <input type="text" name="num_cuenta_resultado" id="num_cuenta_resultado" value="{$form.num_cuenta_resultado}" class="form-control input-sm cuenta" maxlength="15">
                        <label for="num_cuenta_resultado">Cta. Resultado</label>
                    </div>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                            style="margin: 0px 0px -45px -10px;"
                            data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Plan de Cuentas"
                            onclick="selector($(this), '{$_Parametros.url}modCB/PlanCuentaCONTROL/selectorCuentasContablesMET', ['fk_cbb004_num_cuenta_resultado','num_cuenta_resultado'], 'caso=cuenta');">
                        <i class="md md-search"></i>
                    </button>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
            <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
        </button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        //  tama�o de ventana
        $('#modalAncho').css("width", "90%");

        //  bloqueo formulario
        $("#formAjax").submit(function() {
            return false;
        });

        //  envio formulario
        $('#accion').click(function() {
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize()+"&perfilM={in_array('AF-01-90-01-05-02',$_Parametros.perfil)}&perfilE={in_array('AF-01-90-01-05-03',$_Parametros.perfil)}", function(dato) {
                if (dato['status'] == 'error') {
                    for (var i in dato.input) {
                        $(document.getElementById(dato.input[i].field+'Error')).removeClass('has-error has-feedback');
                        $(document.getElementById(dato.input[i].field+'Error')).addClass('has-error has-feedback');
                    }
                    //  mensaje
                    swal("Error!", dato['mensaje'][0], "error");
                } else {
                    //  mensaje
                    swal("Exito!", dato['mensaje'], "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    //  actualizo tabla
                    if (dato['status'] == 'crear') {
                        /*var table = $('.datatable1').DataTable();
                        table.row.add(dato.tr).draw(false);
                        $('.datatable1 tbody tr').not('[id]').attr('id','id'+dato['id']);*/
                    }
                    else if (dato['status'] == 'modificar') {
                        //$('#id'+dato['id']).html(dato['tr']);
                    }
                    cargarUrl("{$_Parametros.url}modAF/maestros/CategoriaCONTROL/");
                }
            }, 'json');
        });

        //  insertar linea
        $('#insertar').click(function() {
            var tbody = $('#lista_' + $(this).data('lista'));
            var numero = $('#lista_' + $(this).data('lista') + ' tr').length + 1;

            $.post("{$_Parametros.url}modAF/maestros/CategoriaCONTROL/insertarMET", "numero="+numero, function(dato) {
                tbody.append(dato);
            }, '');
        });

        //  obtener el id de la cuenta
        $('.cuenta').change(function() {
            var pk_num_cuenta = $('#fk_cbb004_' + $(this).attr('id'));
            var input = $(this).attr('id');
            var cod_cuenta = $(this);
            $.post("{$_Parametros.url}modAF/maestros/CategoriaCONTROL/ObtenerCuentaMET", "cod_cuenta="+$(this).val(), function(dato) {
                if (dato == '') {
                    $(document.getElementById(input+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(input+'Error')).addClass('has-error has-feedback');
                    cod_cuenta.val('');
                }
                else {
                    $(document.getElementById(input+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(input+'Error')).removeClass('has-error has-feedback');
                }
                pk_num_cuenta.val(dato);
            }, '');
        });

        //
        $('.listado_cuentas').click(function() {
            alert($(this).data('field'));
        });

    });
</script>
