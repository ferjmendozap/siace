<form action="{$_Parametros.url}modAF/maestros/TipoSeguroCONTROL/{$form.metodo}MET" id="formAjax" class="form" role="form" method="post">
    <input type="hidden" name="pk_num_tipo_seguro" id="pk_num_tipo_seguro" value="{$form.pk_num_tipo_seguro}">
    <div class="modal-body">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group form-group-sm floating-label" id="ind_nombre_tipo_seguroError">
                        <input type="text" name="ind_nombre_tipo_seguro" id="ind_nombre_tipo_seguro" value="{$form.ind_nombre_tipo_seguro}" class="form-control" maxlength="45">
                        <label for="ind_nombre_tipo_seguro">Nombre</label>
                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" name="num_estatus" id="num_estatus" value="1" {if $form.num_estatus==1}checked{/if}>
                            <span>Estatus</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
            <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
        </button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        //  tama�o de ventana
        $('#modalAncho').css("width", "50%");

        //  bloqueo formulario
        $("#formAjax").submit(function() {
            return false;
        });

        //  envio formulario
        $('#accion').click(function() {
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize()+"&perfilM={in_array('AF-01-90-02-01-02',$_Parametros.perfil)}&perfilE={in_array('AF-01-90-02-01-03',$_Parametros.perfil)}", function(dato) {
                if (dato['status'] == 'error') {
                    for (var i in dato.input) {
                        $(document.getElementById(dato.input[i].field+'Error')).removeClass('has-error has-feedback');
                        $(document.getElementById(dato.input[i].field+'Error')).addClass('has-error has-feedback');
                    }
                    //  mensaje
                    swal("Error!", dato['mensaje'][0], "error");
                } else {
                    //  mensaje
                    swal("Exito!", dato['mensaje'], "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    //  actualizo tabla
                    if (dato['status'] == 'crear') {
                        /*var table = $('.datatable1').DataTable();
                        table.row.add(dato.tr).draw(false);
                        $('.datatable1 tbody tr').not('[id]').attr('id','id'+dato['id']);*/
                    }
                    else if (dato['status'] == 'modificar') {
                        //$('#id'+dato['id']).html(dato['tr']);
                    }
                    cargarUrl("{$_Parametros.url}modAF/maestros/TipoSeguroCONTROL");
                }
            }, 'json');
        });
    });
</script>
