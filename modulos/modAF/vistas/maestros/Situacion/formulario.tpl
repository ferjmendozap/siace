<form action="{$_Parametros.url}modAF/maestros/SituacionCONTROL/{$form.metodo}MET" id="formAjax" class="form" role="form" method="post">
    <input type="hidden" name="pk_num_situacion" id="pk_num_situacion" value="{$form.pk_num_situacion}">
    <div class="modal-body">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group form-group-sm floating-label" id="ind_nombre_situacionError">
                        <input type="text" name="ind_nombre_situacion" id="ind_nombre_situacion" value="{$form.ind_nombre_situacion}" class="form-control" maxlength="45">
                        <label for="ind_nombre_situacion">Nombre</label>
                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" name="num_flag_depreciacion" id="num_flag_depreciacion" value="1" {if $form.num_flag_depreciacion==1}checked{/if} {if $form.num_flag_sistema==1}disabled{/if}>
                            <span>Depreciaci&oacute;n</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" name="num_flag_revaluacion" id="num_flag_revaluacion" value="1" {if $form.num_flag_revaluacion==1}checked{/if} {if $form.num_flag_sistema==1}disabled{/if}>
                            <span>Revaluaci&oacute;n</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" name="num_flag_sistema" id="num_flag_sistema" value="1" {if $form.num_flag_sistema==1}checked{/if} {if $form.num_flag_sistema==1}disabled{/if}>
                            <span>Del sistema</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" name="num_estatus" id="num_estatus" value="1" {if $form.num_estatus==1}checked{/if}>
                            <span>Estatus</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
            <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
        </button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        //  tama�o de ventana
        $('#modalAncho').css("width", "50%");

        //  bloqueo formulario
        $("#formAjax").submit(function() {
            return false;
        });

        //  envio formulario
        $('#accion').click(function() {
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function(dato) {
                if (dato['status'] == 'error') {
                    for (var i in dato.input) {
                        $(document.getElementById(dato.input[i].field+'Error')).removeClass('has-error has-feedback');
                        $(document.getElementById(dato.input[i].field+'Error')).addClass('has-error has-feedback');
                    }
                    //  mensaje
                    swal("Error!", dato['mensaje'][0], "error");
                } else {
                    //  mensaje
                    swal("Exito!", dato['mensaje'], "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    //  actualizo tabla
                    if (dato['status'] == 'crear') {
                    }
                    else if (dato['status'] == 'modificar') {
                    }
                    cargarUrl("{$_Parametros.url}modAF/maestros/SituacionCONTROL");
                }
            }, 'json');
        });
    });
</script>
