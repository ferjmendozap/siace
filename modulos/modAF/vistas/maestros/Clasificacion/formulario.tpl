<form action="{$_Parametros.url}modAF/maestros/ClasificacionCONTROL/{$form.metodo}MET" id="formAjax" class="form" role="form" method="post">
    <input type="hidden" name="pk_num_clasificacion" id="pk_num_clasificacion" value="{$form.pk_num_clasificacion}">
    <div class="modal-body">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-sm floating-label" id="ind_codigoError">
                        <input type="text" name="ind_codigo" id="ind_codigo" value="{$form.ind_codigo}" class="form-control input-sm" maxlength="10">
                        <label for="ind_codigo">C&oacute;digo</label>
                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group form-group-sm floating-label" id="fk_cbb005_num_contabilidadError">
                        <select name="fk_cbb005_num_contabilidad" id="fk_cbb005_num_contabilidad" class="form-control input-sm">
                            <option value="">&nbsp;</option>
                            {Select::lista('cb_b005_contabilidades','pk_num_contabilidades','ind_descripcion',$form.fk_cbb005_num_contabilidad)}
                        </select>
                        <label for="fk_cbb005_num_contabilidad">Contabilidad</label>
                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group form-group-sm floating-label" id="num_nivelError">
                        <input type="text" name="num_nivel" id="num_nivel" value="{$form.num_nivel}" class="form-control input-sm" maxlength="1">
                        <label for="num_nivel">Nivel</label>
                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group form-group-sm floating-label" id="ind_nombre_clasificacionError">
                        <input type="text" name="ind_nombre_clasificacion" id="ind_nombre_clasificacion" value="{$form.ind_nombre_clasificacion}" class="form-control input-sm" maxlength="255">
                        <label for="ind_nombre_clasificacion">Nombre</label>
                        <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" name="num_estatus" id="num_estatus" value="1" {if $form.num_estatus==1}checked{/if}>
                            <span>Estatus</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
            <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
        </button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        //  tama�o de ventana
        $('#modalAncho').css("width", "50%");

        //  bloqueo formulario
        $("#formAjax").submit(function() {
            return false;
        });

        //  envio formulario
        $('#accion').click(function() {
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize()+"&perfilM={in_array('AF-01-90-01-02-02',$_Parametros.perfil)}&perfilE={in_array('AF-01-90-01-02-03',$_Parametros.perfil)}", function(dato) {
                if (dato['status'] == 'error') {
                    for (var i in dato.input) {
                        $(document.getElementById(dato.input[i].field+'Error')).removeClass('has-error has-feedback');
                        $(document.getElementById(dato.input[i].field+'Error')).addClass('has-error has-feedback');
                    }
                    //  mensaje
                    swal("Error!", dato['mensaje'][0], "error");
                } else {
                    //  mensaje
                    swal("Exito!", dato['mensaje'], "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    //  actualizo tabla
                    if (dato['status'] == 'crear') {
                        /*var table = $('.datatable1').DataTable();
                        table.row.add(dato.tr).draw(false);
                        $('.datatable1 tbody tr').not('[id]').attr('id','id'+dato['id']);*/
                    }
                    else if (dato['status'] == 'modificar') {
                        //$('#id'+dato['id']).html(dato['tr']);
                    }
                    cargarUrl("{$_Parametros.url}modAF/maestros/ClasificacionCONTROL/");
                }
            }, 'json');
        });
    });
</script>
