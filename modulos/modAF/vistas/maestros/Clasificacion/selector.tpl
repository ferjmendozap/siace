<section class="style-default-bright">
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="card card-underline">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="tabla_clasificacion" class="table table-striped table-hover datatable1">
                                <thead>
                                    <tr>
                                        <th width="50">Id.</th>
                                        <th width="100">C&oacute;digo</th>
                                        <th>Descripcion</th>
                                        <th width="125">Contabilidad</th>
                                        <th width="75">Nivel</th>
                                        <th width="75">Estatus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {foreach item=registro from=$listado}
                                    <tr id="id{$registro.pk_num_clasificacion}" onclick="seleccionar(['{$campos.campo1}','{$campos.campo2}','{$campos.campo3}'], ['{$registro.pk_num_clasificacion}','{$registro.ind_codigo}','{$registro.ind_nombre_clasificacion|escape}']);">
                                        <td><label>{$registro.pk_num_clasificacion}</label></td>
                                        <td><label>{$registro.ind_codigo}</label></td>
                                        <td><label>{$registro.ind_nombre_clasificacion|escape}</label></td>
                                        <td><label>{$registro.contabilidad}</label></td>
                                        <td><label>{$registro.num_nivel}</label></td>
                                        <td><i class="{if $registro.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                    </tr>
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        inicializar();
    });
</script>
