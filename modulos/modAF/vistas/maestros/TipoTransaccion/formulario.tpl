<form action="{$_Parametros.url}modAF/maestros/TipoTransaccionCONTROL/{$form.metodo}MET" id="formAjax" class="form" role="form" method="post" autocomplete="off">
    <input type="hidden" name="pk_num_tipo_transaccion" id="pk_num_tipo_transaccion" value="{$form.pk_num_tipo_transaccion}">
    <div class="modal-body">
        <div class="col-sm-5">
            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group form-group-sm floating-label" id="ind_codigoError">
                        <input type="text" name="ind_codigo" id="ind_codigo" value="{$form.ind_codigo}" class="form-control input-sm" maxlength="3">
                        <label for="ind_codigo">C&oacute;digo</label>
                    </div>
                </div>
                <div class="col-sm-6 col-md-offset-1">
                    <div class="form-group form-group-sm floating-label" id="ind_tipoError">
                        <select name="ind_tipo" id="ind_tipo" class="form-control input-sm">
                            <option value="">&nbsp;</option>
                            {Select::options('af_tipo_transaccion',$form.ind_tipo,0)}
                        </select>
                        <label for="ind_tipo">Tipo de Transacci&oacute;n</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group form-group-sm floating-label" id="ind_nombre_transaccionError">
                        <input type="text" name="ind_nombre_transaccion" id="ind_nombre_transaccion" value="{$form.ind_nombre_transaccion}" class="form-control input-sm" maxlength="150">
                        <label for="ind_nombre_transaccion">Nombre</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group form-group-sm floating-label" id="fk_cbc003_num_voucherError">
                        <select name="fk_cbc003_num_voucher" id="fk_cbc003_num_voucher" class="form-control input-sm">
                            <option value="">&nbsp;</option>
                            {Select::lista('cb_c003_tipo_voucher','pk_num_voucher','ind_descripcion',$form.fk_cbc003_num_voucher,0,[],'cod_voucher')}
                        </select>
                        <label for="fk_cbc003_num_voucher">Tipo de Voucher</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-9">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" name="num_flag_sistema" id="num_flag_sistema" value="1" {if $form.num_flag_sistema==1}checked{/if}>
                            <span>Del Sistema</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" name="num_estatus" id="num_estatus" value="1" {if $form.num_estatus==1}checked{/if}>
                            <span>Estatus</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-7">
            <div class="card card-underline">
                <div class="card-head">
                    <div class="tools">
                        <button id="insertar" class="btn ink-reaction btn-raised btn-info btn-xs" data-lista="detalle">
                            <i class="md md-add"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-condensed" style="min-width: 1200px;">
                            <thead>
                                <tr>
                                    <th width="200">Categoria</th>
                                    <th width="175">Contabilidad</th>
                                    <th width="200">Cuenta</th>
                                    <th style="text-align:left;">Descripci&oacute;n</th>
                                    <th width="100">Signo</th>
                                    <th width="25">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody id="lista_detalle">
                                {$numero = 0}
                                {foreach item=registro from=$form.listado_detalle}
                                    {$numero=$numero+1}
                                    <tr id="detalle{$numero}">
                                        <td>
                                            <div class="col-sm-10">
                                                <div class="form-group form-group-sm floating-label" id="detalle_num_categoria{$numero}Error">
                                                    <input type="hidden" name="detalle_pk_num_tipo_transaccion_cuenta[]" value="{$registro.pk_num_tipo_transaccion_cuenta}">
                                                    <input type="hidden" name="detalle_fk_afc019_num_categoria[]" id="detalle_fk_afc019_num_categoria{$numero}" value="{$registro.fk_afc019_num_categoria}">
                                                    <input type="text" name="detalle_num_categoria[]" id="detalle_num_categoria{$numero}" value="{$registro.num_categoria}" class="form-control input-sm" maxlength="15" data-id="{$numero}" onchange="obtenerCategoria($(this));">
                                                    <label for="detalle_num_categoria">&nbsp;</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-1">
                                                <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector" style="margin: 0px 0px -45px -10px;"
                                                        data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Categorias"
                                                        onclick="selector($(this), 'modAF/maestros/CategoriaCONTROL/selectorMET', ['detalle_fk_afc019_num_categoria{$numero}','detalle_num_categoria{$numero}']);">
                                                    <i class="md md-search"></i>
                                                </button>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-sm floating-label">
                                                    <select name="detalle_fk_cbb005_num_contabilidades[]" class="form-control input-sm">
                                                        <option value="">&nbsp;</option>
                                                        {Select::lista('cb_b005_contabilidades','pk_num_contabilidades','ind_descripcion',$registro.fk_cbb005_num_contabilidades)}
                                                    </select>
                                                    <label for="detalle_fk_cbb005_num_contabilidades">&nbsp;</label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="col-sm-10">
                                                <div class="form-group form-group-sm floating-label" id="detalle_num_cuenta{$numero}Error">
                                                    <input type="hidden" name="detalle_fk_cbb004_num_cuenta[]" id="detalle_fk_cbb004_num_cuenta{$numero}" value="{$registro.fk_cbb004_num_cuenta}">
                                                    <input type="text" name="detalle_num_cuenta[]" id="detalle_num_cuenta{$numero}" value="{$registro.num_cuenta}" class="form-control input-sm" maxlength="15" data-id="{$numero}" onchange="obtenerCuenta($(this));">
                                                    <label for="detalle_num_cuenta">&nbsp;</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-1">
                                                <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                                                        style="margin: 0px 0px -45px -10px;"
                                                        data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Plan de Cuentas"
                                                        onclick="selector($(this), '{$_Parametros.url}modCB/PlanCuentaCONTROL/selectorCuentasContablesMET', ['detalle_fk_cbb004_num_cuenta{$numero}','detalle_num_cuenta{$numero}'], 'caso=cuenta');">
                                                    <i class="md md-search"></i>
                                                </button>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-sm floating-label">
                                                    <input type="text" name="detalle_ind_descripcion[]" value="{$registro.ind_descripcion}" class="form-control input-sm">
                                                    <label for="detalle_ind_descripcion">&nbsp;</label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-sm floating-label">
                                                    <select name="detalle_ind_signo[]" class="form-control input-sm">
                                                        <option value="">&nbsp;</option>
                                                        {Select::options('signo', $registro.ind_signo)}
                                                    </select>
                                                    <label for="detalle_ind_signo">&nbsp;</label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <button class="btn ink-reaction btn-raised btn-xs btn-danger" style="margin: 55px 0px 55px -10px;" onclick="$('#detalle{$numero}').remove();">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
            <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
        </button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        //  tamaño de ventana
        $('#modalAncho').css("width", "70%");

        //  bloqueo formulario
        $("#formAjax").submit(function() {
            return false;
        });

        //  envio formulario
        $('#accion').click(function() {
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize()+"&perfilM={in_array('AF-01-90-01-06-02',$_Parametros.perfil)}&perfilE={in_array('AF-01-90-01-06-03',$_Parametros.perfil)}", function(dato) {
                if (dato['status'] == 'error') {
                    for (var i in dato.input) {
                        $(document.getElementById(dato.input[i].field+'Error')).removeClass('has-error has-feedback');
                        $(document.getElementById(dato.input[i].field+'Error')).addClass('has-error has-feedback');
                    }
                    //  mensaje
                    swal("Error!", dato['mensaje'][0], "error");
                } else {
                    //  mensaje
                    swal("Exito!", dato['mensaje'], "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    //  actualizo tabla
                    if (dato['status'] == 'crear') {
                        /*var table = $('.datatable1').DataTable();
                        table.row.add(dato.tr).draw(false);
                        $('.datatable1 tbody tr').not('[id]').attr('id','id'+dato['id']);*/
                    }
                    else if (dato['status'] == 'modificar') {
                        //$('#id'+dato['id']).html(dato['tr']);
                    }
                    cargarUrl("{$_Parametros.url}modAF/maestros/TipoTransaccionCONTROL/");
                }
            }, 'json');
        });

        //  insertar linea
        $('#insertar').click(function() {
            var tbody = $('#lista_' + $(this).data('lista'));
            var numero = $('#lista_' + $(this).data('lista') + ' tr').length + 1;

            $.post("{$_Parametros.url}modAF/maestros/TipoTransaccionCONTROL/insertarMET", "numero="+numero, function(dato) {
                tbody.append(dato);
            }, '');
        });

        //
        $('.listado_cuentas').click(function() {
            alert($(this).data('field'));
        });

    });

    function obtenerCategoria(detalle_num_categoria) {
        var detalle_fk_afc019_num_categoria = $('#detalle_fk_afc019_num_categoria' + detalle_num_categoria.data('id'));
        var input = detalle_num_categoria.attr('id');

        $.post("{$_Parametros.url}modAF/maestros/TipoTransaccionCONTROL/ObtenerCategoriaMET", "detalle_num_categoria="+detalle_num_categoria.val(), function(dato) {
            if (dato == '') {
                $(document.getElementById(input+'Error')).removeClass('has-error has-feedback');
                $(document.getElementById(input+'Error')).addClass('has-error has-feedback');
            }
            else {
                $(document.getElementById(input+'Error')).addClass('has-error has-feedback');
                $(document.getElementById(input+'Error')).removeClass('has-error has-feedback');
            }
            detalle_fk_afc019_num_categoria.val(dato);
        }, '');
    }

    function obtenerCuenta(detalle_num_cuenta) {
        var detalle_fk_cbb004_num_cuenta = $('#detalle_fk_cbb004_num_cuenta' + detalle_num_cuenta.data('id'));
        var input = detalle_num_cuenta.attr('id');

        $.post("{$_Parametros.url}modAF/maestros/TipoTransaccionCONTROL/ObtenerCuentaMET", "detalle_num_cuenta="+detalle_num_cuenta.val(), function(dato) {
            if (dato == '') {
                $(document.getElementById(input+'Error')).removeClass('has-error has-feedback');
                $(document.getElementById(input+'Error')).addClass('has-error has-feedback');
            }
            else {
                $(document.getElementById(input+'Error')).addClass('has-error has-feedback');
                $(document.getElementById(input+'Error')).removeClass('has-error has-feedback');
            }
            detalle_fk_cbb004_num_cuenta.val(dato);
        }, '');
    }
</script>
