<section class="style-default-bright">
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ingresos" aria-controls="ingresos" role="tab" data-toggle="tab">Transferencia de Activos Fijos</a></li>
                    <li role="presentation"><a href="#transferidos" aria-controls="transferidos" role="tab" data-toggle="tab">Acta de Entrega de Bienes Muebles</a></li>
                </ul>
                <br>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="ingresos">
                        <div class="card card-underline">
                            <div class="card-body">
                                <iframe id="tab1" src="{$_Parametros.url}modAF/procesos/MovimientosCONTROL/ImprimirTransferenciaMET/{$form.id}" style="width:100%; height:500px;"></iframe>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="transferidos">
                        <div class="card card-underline">
                            <div class="card-body">
                                <iframe id="tab2" src="{$_Parametros.url}modAF/procesos/MovimientosCONTROL/ImprimirActaMET/{$form.id}" style="width:100%; height:500px;"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        //  base
        var $base = '{$_Parametros.url}modAF/procesos/MovimientosCONTROL/';

        /*
        HEIGHT AUTOMATICO PARA LOS IFRAMES SEGUN RESOLUCIÓN
        if (window.innerHeight){
            //navegadores basados en mozilla
            espacio_iframe = window.innerHeight - 410
        }else{
            if (document.body.clientHeight) {
                //Navegadores basados en IExplorer, es que no tengo innerheight
                espacio_iframe = document.body.clientHeight - 110
            }else{
                //otros navegadores
                espacio_iframe = 478
            }
        }
        $('#tab1').css('height', espacio_iframe);
        $('#tab2').css('height', espacio_iframe);
        */
    });
</script>
