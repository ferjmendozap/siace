<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary pull-left">Movimientos de Activos - {$data.titulo}</h2>
        <a data-toggle="offcanvas" title="Filtro" class="btn ink-reaction btn-floating-action btn-sm btn-primary pull-right" href="#offcanvas-filtro"><i class="fa fa-filter"></i></a>
    </div>

    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="card card-underline">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover datatable1">
                                <thead>
                                    <tr>
                                        <th width="50">Id.</th>
                                        <th width="100">Estado</th>
                                        <th width="100">N&uacute;mero</th>
                                        <th width="100">Fecha</th>
                                        <th>Comentarios</th>
                                        <th width="100">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {foreach item=registro from=$listado}
                                        <tr id="id{$registro.pk_num_movimiento}">
                                            <td><label>{$registro.pk_num_movimiento}</label></td>
                                            <td><label>{Select::options('af_estado_movimiento', $registro.ind_estado, 2)}</label></td>
                                            <td><label>{$registro.ind_nro_acta}-{$registro.num_anio}</label></td>
                                            <td><label>{Fecha::formatFecha($registro.fec_fecha,'Y-m-d','d-m-Y')}</label></td>
                                            <td><label>{$registro.ind_comentarios}</label></td>
                                            <td nowrap="true">
                                                {if $data.lista == 'listar'}
                                                    {if in_array('AF-01-02-01-01-02',$_Parametros.perfil) && $registro.ind_estado == 'PR'}
                                                        <button idRegistro="{$registro.pk_num_movimiento}" class="btn ink-reaction btn-raised btn-xs btn-primary" id="modificar"
                                                                data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Movimiento" title="Editar Movimiento">
                                                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                        </button>
                                                    {/if}
                                                    <button idRegistro="{$registro.pk_num_movimiento}" class="btn ink-reaction btn-raised btn-xs btn-warning" id="ver"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Ver Movimiento" title="Ver Movimiento">
                                                        <i class="icm icm-eye2" style="color: #ffffff;"></i>
                                                    </button>
                                                    <button idRegistro="{$registro.pk_num_movimiento}" class="btn ink-reaction btn-raised btn-xs btn-info" id="imprimir"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Imprimir Movimiento" title="Imprimir Movimiento">
                                                        <i class="fa fa-print" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                                {if $data.lista == 'revisar' && $registro.ind_estado == 'PR'}
                                                    <button idRegistro="{$registro.pk_num_movimiento}" class="btn ink-reaction btn-raised btn-xs btn-primary" id="revisar"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Revisar Movimiento"
                                                            title="Revisar Movimiento Nro. {$registro.ind_nro_acta}-{$registro.num_anio}">
                                                        <i class="icm icm-rating3" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                                {if $data.lista == 'aprobar' && $registro.ind_estado == 'RV'}
                                                    <button idRegistro="{$registro.pk_num_movimiento}" class="btn ink-reaction btn-raised btn-xs btn-primary" id="aprobar"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Aprobar Movimiento"
                                                            title="Aprobar Movimiento Nro. {$registro.ind_nro_acta}-{$registro.num_anio}">
                                                        <i class="icm icm-rating3" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                                {if $registro.ind_estado != 'AN' && $registro.ind_estado != 'AP'}
                                                    {if in_array('AF-01-02-01-01-03',$_Parametros.perfil)}
                                                        <button idRegistro="{$registro.pk_num_movimiento}" class="btn ink-reaction btn-raised btn-xs btn-danger" id="anular"
                                                                data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Anular Movimiento"
                                                                title="Anular Movimiento Nro. {$registro.ind_nro_acta}-{$registro.num_anio}">
                                                            <i class="fa fa-ban" style="color: #ffffff;"></i>
                                                        </button>
                                                    {/if}
                                                {/if}
                                            </td>
                                        </tr>
                                    {/foreach}
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="6">
                                            {if $data.lista == 'listar'}
                                                {if in_array('AF-01-02-01-01-01',$_Parametros.perfil)}
                                                    <button id="nuevo" class="btn ink-reaction btn-raised btn-info btn-sm"
                                                            data-toggle="modal"
                                                            data-target="#formModal"
                                                            data-keyboard="false"
                                                            data-backdrop="static" titulo="Nuevo Movimiento">
                                                        <i class="md md-create"></i> Nuevo Movimiento
                                                    </button>
                                                {/if}
                                            {/if}
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="offcanvas">
    <div id="offcanvas-filtro" class="offcanvas-pane width-7">
        <div class="offcanvas-head">
            <header>Filtro</header>
            <div class="offcanvas-tools">
                <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                    <i class="md md-close"></i>
                </a>
            </div>
        </div>

        <div class="offcanvas-body">
            <form action="{$_Parametros.url}modAF/procesos/MovimientosCONTROL/indexMET/{$data.lista}" id="formFiltro" class="form" role="form" method="post" autocomplete="off" onsubmit="return cargarPagina();">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm">
                            <select name="fnum_organismo" id="fnum_organismo" class="form-control input-sm">
                                <option value="">&nbsp;</option>
                                {Select::lista('a001_organismo','pk_num_organismo','ind_descripcion_empresa',{$filtro.fnum_organismo})}
                            </select>
                            <label for="fnum_organismo">Organismo</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm">
                            <select name="find_tipo" id="find_tipo" class="form-control input-sm" onchange="select('{$_Parametros.url}modAF/procesos/MovimientosCONTROL/SelectMotivoTrasladoMET', $('#fnum_motivo'), 'ind_tipo='+$(this).val());">
                                <option value="">&nbsp;</option>
                                {Select::options('af_tipo_movimiento',$filtro.find_tipo)}
                            </select>
                            <label for="find_tipo">Tipo de Movimiento</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm">
                            <select name="fnum_motivo" id="fnum_motivo" class="form-control input-sm">
                                <option value="">&nbsp;</option>
                                {if $filtro.find_tipo == 'I'}
                                    {Select::miscelaneo('AFMOTTRASI',$filtro.fnum_motivo)}
                                {elseif $filtro.find_tipo == 'E'}
                                    {Select::miscelaneo('AFMOTTRASE',$filtro.fnum_motivo)}
                                {/if}
                            </select>
                            <label for="fnum_motivo">Motivo de Traslado</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm">
                            <select name="find_estado" id="find_estado" class="form-control input-sm">
                                {if $data.lista == 'listar'}
                                    <option value="">&nbsp;</option>
                                    {Select::options('af_estado_movimiento',$filtro.find_estado)}
                                {else}
                                    {Select::options('af_estado_movimiento',$filtro.find_estado,1)}
                                {/if}
                            </select>
                            <label for="find_estado">Estado</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group form-group-sm">
                            <input type="text" name="ffec_fechad" id="ffec_fechad" value="{$filtro.ffec_fechad}" class="form-control input-sm date">
                            <label for="ffec_fechad">Desde</label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-group-sm">
                            <input type="text" name="ffec_fechah" id="ffec_fechah" value="{$filtro.ffec_fechah}" class="form-control input-sm date">
                            <label for="ffec_fechah">Hasta</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-xs ink-reaction btn-raised btn-info" id="botonFiltro">
                            Filtrar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        //  base
        $base = '{$_Parametros.url}modAF/procesos/MovimientosCONTROL/';

        //  nuevo registro
        $('#nuevo').click(function() {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'formMET/nuevo', '', function($dato) {
                $('#ContenidoModal').html($dato);
            });
        });

        //  editar registro
        $('.datatable1 tbody').on( 'click', '#modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'formMET/editar', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //  ver registro
        $('.datatable1 tbody').on( 'click', '#ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'formMET/ver', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //  imprimir registro
        $('.datatable1 tbody').on( 'click', '#imprimir', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'imprimirMET', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //  revisar registro
        $('.datatable1 tbody').on( 'click', '#revisar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'formMET/revisar', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //  aprobar registro
        $('.datatable1 tbody').on( 'click', '#aprobar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'formMET/aprobar', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //  anular registro
        $('.datatable1 tbody').on( 'click', '#anular', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'formMET/anular', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
    });
</script>
