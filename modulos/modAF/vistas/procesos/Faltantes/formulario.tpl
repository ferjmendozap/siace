<form action="{$_Parametros.url}modAF/procesos/FaltantesCONTROL/{$form.metodo}MET" id="formAjax" class="form" role="form" method="post">
    <input type="hidden" name="pk_num_activo_faltante" id="pk_num_activo_faltante" value="{$form.pk_num_activo_faltante}">

    <div class="modal-body">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-head card-head-xs style-primary text-center">
                    <header class="text-center">Informaci&oacute;n General</header>
                </div>
                <div class="card-body" style="padding: 4px;">
                    <div class="col-sm-12">
                        <div class="col-sm-4">
                            <div class="form-group form-group-sm floating-label" id="fk_afc009_num_movimientoError">
                                <select name="fk_afc009_num_movimiento" id="fk_afc009_num_movimiento" class="form-control input-sm" {$disabled.editar}>
                                    {Select::lista('af_c009_tipo_movimiento','pk_num_movimiento','ind_nombre_movimiento',$form.fk_afc009_num_movimiento,1)}
                                </select>
                                <label for="fk_afc009_num_movimiento">Tipo de Movimiento</label>
                                <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group form-group-sm floating-label" id="fec_fechaError">
                                <input type="text" name="fec_fecha" id="fec_fecha" value="{Fecha::formatFecha($form.fec_fecha,'Y-m-d','d-m-Y')}" class="form-control input-sm date" maxlength="10" {$disabled.ver}>
                                <label for="fec_fecha">Fecha de Suceso</label>
                                <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group form-group-sm floating-label" id="ind_estadoError">
                                <input type="hidden" name="ind_estado" id="ind_estado" value="{$form.ind_estado}">
                                <input type="text" name="estado" id="estado" value="{Select::options('af_estado_faltante', $form.ind_estado, 2)}" class="form-control input-sm" disabled>
                                <label for="estado">Estado</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group form-group-sm floating-label" id="ind_situacionError">
                                <input type="hidden" name="ind_situacion" id="ind_situacion" value="{$form.ind_situacion}">
                                <input type="text" name="situacion" id="situacion" value="{Select::options('af_situacion_faltante', $form.ind_situacion, 2)}" class="form-control input-sm" disabled>
                                <label for="estado">Situaci&oacute;n</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group form-group-sm" id="ind_codigoError">
                                <input type="text" name="ind_codigo" id="ind_codigo" value="{$form.ind_codigo}" class="form-control input-sm" maxlength="5" disabled>
                                <label for="ind_codigo">Nro. Documento</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <div class="form-group form-group-sm floating-label" id="ind_comentariosError">
                                <textarea name="ind_comentarios" id="ind_comentarios" class="form-control input-sm" style="height:75px;" {$disabled.ver}>{$form.ind_comentarios}</textarea>
                                <label for="ind_comentarios">Descripci&oacute;n del Suceso</label>
                                <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
        <div class="col-sm-12 {$show.tramitar}">
            <div class="card">
                <div class="card-head card-head-xs style-primary text-center">
                    <header class="text-center">Informaci&oacute;n Resultados</header>
                </div>
                <div class="card-body" style="padding: 4px;">
                    <div class="col-sm-12">
                        <div class="col-sm-5">
                            <div class="form-group form-group-sm floating-label" id="fk_a001_num_organismo_externoError">
                                <select name="fk_a001_num_organismo_externo" id="fk_a001_num_organismo_externo" class="form-control input-sm" {$disabled.tramitar}>
                                    <option value="">&nbsp;</option>
                                    {Select::lista('a001_organismo','pk_num_organismo','ind_descripcion_empresa',$form.fk_a001_num_organismo_externo,1)}
                                </select>
                                <label for="fk_a001_num_organismo_externo">Organismo Externo</label>
                                <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                            </div>
                        </div>
                        <div class="col-sm-2 col-md-offset-5">
                            <div class="form-group form-group-sm floating-label" id="fec_fecha_informeError">
                                <input type="text" name="fec_fecha_informe" id="fec_fecha_informe" value="{Fecha::formatFecha($form.fec_fecha_informe,'Y-m-d','d-m-Y')}" class="form-control input-sm date" maxlength="10" {$disabled.tramitar}>
                                <label for="fec_fecha_informe">Fecha de Informe</label>
                                <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <div class="form-group form-group-sm floating-label" id="ind_comentarios_informeError">
                                <textarea name="ind_comentarios_informe" id="ind_comentarios_informe" class="form-control input-sm" style="height:125px;" {$disabled.tramitar}>{$form.ind_comentarios_informe}</textarea>
                                <label for="ind_comentarios_informe">Descripci&oacute;n Resultados</label>
                                <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
        <div class="col-sm-12">
            <div class="card">
                <div class="card-head card-head-xs style-primary text-center">
                    <header class="text-center">Activos Afectados</header>
                    <div class="pull-right" style="margin-right: 10px;">
                        <button id="btInsertar" class="btn ink-reaction btn-raised btn-info btn-xs" data-lista="detalle" onclick="insertar();" {$disabled.ver}>
                            <i class="md md-add"></i>
                        </button>
                        <button id="btQuitar" class="btn ink-reaction btn-raised btn-danger btn-xs" onclick="$('#lista_detalle tr.ui-selected').remove();" {$disabled.ver}>
                            <i class="md md-delete"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body" style="padding: 4px;">
                    <div class="table-responsive" style="height:320px;">
                        <table class="table table-striped table-hover table-condensed seleccionable">
                            <tbody id="lista_detalle">
                                {$numero = 0}
                                {foreach item=registro from=$form.lista_detalle}
                                    {$numero=$numero+1}
                                    <tr id="detalle{$numero}">
                                        <td>
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <div class="form-group form-group-sm" id="detalle_fk_afb001_num_activo{$numero}Error">
                                                            <input type="hidden" name="detalle_pk_num_activo_faltante_detalle[]" id="detalle_pk_num_activo_faltante_detalle{$numero}" value="{$registro.pk_num_activo_faltante_detalle}">
                                                            <input type="hidden" name="detalle_fk_afb001_num_activo[]" id="detalle_fk_afb001_num_activo{$numero}" value="{$registro.fk_afb001_num_activo}">
                                                            <input type="text" name="detalle_cod_activo[]" id="detalle_cod_activo{$numero}" value="{$registro.cod_activo}" class="form-control input-sm" disabled>
                                                            <label for="detalle_cod_activo{$numero}">Activo</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector" style="margin: 0px 0px -45px -10px;"
                                                                data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Activos"
                                                                onclick="selector($(this), '{$_Parametros.url}modAF/activos/ActivosCONTROL/selectorMET', ['detalle_fk_afb001_num_activo{$numero}','detalle_cod_activo{$numero}','detalle_desc_activo{$numero}','detalle_fk_afc005_num_ubicacion{$numero}','detalle_fk_afc007_num_situacion{$numero}'], 'opcion=af_faltantes_detalle');" {$disabled.ver}>
                                                            <i class="md md-search"></i>
                                                        </button>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="form-group form-group-sm" id="detalle_desc_activo{$numero}Error">
                                                            <textarea name="detalle_desc_activo[]" id="detalle_desc_activo{$numero}" class="form-control input-sm" disabled>{$registro.desc_activo}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group form-group-sm" id="detalle_fk_afc005_num_ubicacion{$numero}Error">
                                                            <select name="detalle_fk_afc005_num_ubicacion[]" id="detalle_fk_afc005_num_ubicacion{$numero}" class="form-control input-sm" {$disabled.aprobar}>
                                                                <option value="">&nbsp;</option>
                                                                {Select::lista('af_c005_ubicacion','pk_num_ubicacion','ind_nombre_ubicacion',$registro.fk_afc005_num_ubicacion)}
                                                            </select>
                                                            <label for="fk_afc005_num_ubicacion">Ubicaci&oacute;n</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group form-group-sm" id="detalle_fk_afc007_num_situacion{$numero}Error">
                                                            <select name="detalle_fk_afc007_num_situacion[]" id="detalle_fk_afc007_num_situacion{$numero}" class="form-control input-sm" {$disabled.ver}>
                                                                <option value="">&nbsp;</option>
                                                                {Select::lista('af_c007_situacion','pk_num_situacion','ind_nombre_situacion',$registro.fk_afc007_num_situacion)}
                                                            </select>
                                                            <label for="fk_afc007_num_situacion">Situaci&oacute;n</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group form-group-sm" id="detalle_ind_estado{$numero}Error">
                                                            {if $form.metodo != 'tramitar' || $registro.ind_estado == 'FE' || $registro.ind_estado == 'FP'}
                                                                <input type="hidden" name="detalle_ind_estado[]" id="detalle_ind_estado{$numero}" value="{$registro.ind_estado}">
                                                                <input type="text" name="detalle_estado{$numero}" value="{Select::options('af_estado_faltante_detalle', $registro.ind_estado, 2)}" class="form-control input-sm" disabled>
                                                            {else}
                                                                <select name="detalle_ind_estado[]" id="detalle_ind_estado{$numero}" class="form-control input-sm">
                                                                    {Select::options('af_estado_faltante_detallet',$registro.ind_estado)}
                                                                </select>
                                                            {/if}
                                                            <label for="detalle_estado{$numero}">Estado</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group form-group-sm" id="detalle_ind_comentariosError">
                                                            <textarea name="detalle_ind_comentarios[]" id="detalle_ind_comentarios{$numero}" class="form-control input-sm" style="height:35px;" {$disabled.ver}>{$registro.ind_comentarios}</textarea>
                                                            <label for="detalle_ind_comentarios">Comentarios</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <span class="clearfix"></span>
    <div class="modal-footer {$show.ver}">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if $form.metodo == 'anular'}
            <button type="button" class="btn btn-danger ink-reaction btn-raised logsUsuarioModal" id="accion">
                 <i class="fa fa-ban"></i> Anular
            </button>
        {elseif $form.metodo == 'revisar'}
            <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
                 <i class="icm icm-rating3"></i> Revisar
            </button>
        {elseif $form.metodo == 'aprobar'}
            <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
                 <i class="icm icm-rating3"></i> Aprobar
            </button>
        {elseif $form.metodo == 'tramitar'}
            <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
                 <i class="md md-done-all"></i> Tramitar
            </button>
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
                <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
            </button>
        {/if}
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        //  tamaño de ventana
        $('#modalAncho').css("width", "75%");

        //  bloqueo formulario
        $("#formAjax").submit(function() {
            return false;
        });

        //  envio formulario
        $('#accion').click(function() {
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function(dato) {
                if (dato['status'] == 'error') {
                    for (var i in dato.input) {
                        $(document.getElementById(dato.input[i].field+'Error')).removeClass('has-error has-feedback');
                        $(document.getElementById(dato.input[i].field+'Error')).addClass('has-error has-feedback');
                    }
                    //  mensaje
                    swal("Error!", dato['mensaje'][0], "error");
                } else {
                    //  mensaje
                    swal("Exito!", dato['mensaje'], "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    //  actualizo tabla
                    if (dato['status'] == 'crear') {
                        cargarPagina($('#formFiltro'), "{$_Parametros.url}modAF/procesos/FaltantesCONTROL");
                    }
                    else if (dato['status'] == 'modificar') {
                        cargarPagina($('#formFiltro'), "{$_Parametros.url}modAF/procesos/FaltantesCONTROL");
                    }
                    else if (dato['status'] == 'revisar') {
                        cargarPagina($('#formFiltro'), "{$_Parametros.url}modAF/procesos/FaltantesCONTROL/indexMET/revisar");
                    }
                    else if (dato['status'] == 'aprobar') {
                        cargarPagina($('#formFiltro'), "{$_Parametros.url}modAF/procesos/FaltantesCONTROL/indexMET/aprobar");
                    }
                    else if (dato['status'] == 'tramitar') {
                        cargarPagina($('#formFiltro'), "{$_Parametros.url}modAF/procesos/FaltantesCONTROL/indexMET/tramitar");
                    }
                    else if (dato['status'] == 'anular') {
                        cargarPagina($('#formFiltro'), "{$_Parametros.url}modAF/procesos/FaltantesCONTROL");
                    }
                }
            }, 'json');
        });

        //  cuentas x tipo de transacción
        $('#fk_afc021_num_tipo_transaccion').change(function() {
            $.post("{$_Parametros.url}modAF/activos/ActivosCONTROL/TipoTransaccionCuentasMET", "fk_afc021_num_tipo_transaccion="+$(this).val(), function(dato) {
                $('#lista_tipo_transaccion_cuentas').html(dato);
            });
        });

        inicializar();
    });
</script>
