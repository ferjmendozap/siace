<form action="{$_Parametros.url}modAF/procesos/BajaActivoCONTROL/{$form.metodo}MET" id="formAjax" class="form" role="form" method="post">
    <input type="hidden" name="pk_num_baja_activo" id="pk_num_baja_activo" value="{$form.pk_num_baja_activo}">
    <div class="modal-body">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-head card-head-xs style-primary text-center">
                    <header class="text-center">Informaci&oacute;n General</header>
                </div>
                <div class="card-body" style="padding: 4px;">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group form-group-sm" id="fk_afb001_num_activoError">
                                    <input type="hidden" name="fk_afb001_num_activo" id="fk_afb001_num_activo" value="{$form.fk_afb001_num_activo}">
                                    <input type="text" name="cod_activo" id="cod_activo" value="{$form.cod_activo}" class="form-control input-sm" disabled>
                                    <label for="cod_activo">Activo</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector" style="margin: 0px 0px -45px -10px;"
                                        data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Activos"
                                        onclick="selector($(this), '{$_Parametros.url}modAF/activos/ActivosCONTROL/selectorMET', ['fk_afb001_num_activo','cod_activo','desc_activo','ind_descripcion_empresa','ind_dependencia','ind_descripcion_centro_costo','persona_responsable','ind_nombre_categoria','ind_nombre_ubicacion','ind_nro_factura','num_monto'], 'opcion=af_baja_activos');" {$disabled.editar}>
                                    <i class="md md-search"></i>
                                </button>
                            </div>
                            <div class="col-sm-7">
                                <div class="form-group form-group-sm" id="desc_activoError">
                                    <textarea name="desc_activo" id="desc_activo" class="form-control input-sm" disabled>{$form.desc_activo}</textarea>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group form-group-sm floating-label" id="ind_estadoError">
                                    <input type="hidden" name="ind_estado" id="ind_estado" value="{$form.ind_estado}">
                                    <input type="text" name="estado" id="estado" value="{Select::options('af_estado_baja', $form.ind_estado, 2)}" class="form-control input-sm" disabled>
                                    <label for="estado">Estado</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm">
                                    <input type="text" name="ind_descripcion_empresa" id="ind_descripcion_empresa" value="{$form.ind_descripcion_empresa}" class="form-control input-sm" disabled>
                                    <label for="ind_descripcion_empresa">Organismo</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm">
                                    <input type="text" name="persona_responsable" id="persona_responsable" value="{$form.persona_responsable}" class="form-control input-sm" disabled>
                                    <label for="persona_responsable">Responsable</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group form-group-sm floating-label" id="fk_afc021_num_tipo_transaccionError">
                                    <select name="fk_afc021_num_tipo_transaccion" id="fk_afc021_num_tipo_transaccion" class="form-control input-sm" {$disabled.ver}>
                                        <option value="">&nbsp;</option>
                                        {Select::lista('af_c021_tipo_transaccion','pk_num_tipo_transaccion','ind_nombre_transaccion',$form.fk_afc021_num_tipo_transaccion,0,['ind_tipo'=>'B'],'ind_codigo')}
                                    </select>
                                    <label for="fk_afc021_num_tipo_transaccion">Tipo de Baja</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group form-group-sm">
                                    <input type="text" name="fec_fecha" id="fec_fecha" value="{Fecha::formatFecha($form.fec_fecha,'Y-m-d','d-m-Y')}" class="form-control input-sm" disabled>
                                    <label for="fec_fecha">Fecha</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm">
                                    <input type="text" name="ind_dependencia" id="ind_dependencia" value="{$form.ind_dependencia}" class="form-control input-sm" disabled>
                                    <label for="ind_dependencia">Dependencia</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm">
                                    <input type="text" name="ind_nombre_categoria" id="ind_nombre_categoria" value="{$form.ind_nombre_categoria}" class="form-control input-sm" disabled>
                                    <label for="ind_nombre_categoria">Categor&iacute;a</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group form-group-sm floating-label" id="fk_afc009_num_movimientoError">
                                    <select name="fk_afc009_num_movimiento" id="fk_afc009_num_movimiento" class="form-control input-sm" {$disabled.ver}>
                                        <option value="">&nbsp;</option>
                                        {Select::lista('af_c009_tipo_movimiento','pk_num_movimiento','ind_nombre_movimiento',$form.fk_afc009_num_movimiento,0,['fk_a006_num_miscelaneo_detalle_tipo_movimiento'=>$form.tipo_movimiento])}
                                    </select>
                                    <label for="fk_afc009_num_movimiento">Tipo de Movimiento</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group form-group-sm floating-label" id="fec_fecha_bajaError">
                                    <input type="text" name="fec_fecha_baja" id="fec_fecha_baja" value="{Fecha::formatFecha($form.fec_fecha_baja,'Y-m-d','d-m-Y')}" class="form-control input-sm date" maxlength="10" {$disabled.ver}>
                                    <label for="fec_fecha_baja">Fecha de Baja</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm">
                                    <input type="text" name="ind_descripcion_centro_costo" id="ind_descripcion_centro_costo" value="{$form.ind_descripcion_centro_costo}" class="form-control input-sm" disabled>
                                    <label for="ind_descripcion_centro_costo">Centro de Costo</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm">
                                    <input type="text" name="ind_nombre_ubicacion" id="ind_nombre_ubicacion" value="{$form.ind_nombre_ubicacion}" class="form-control input-sm" disabled>
                                    <label for="ind_nombre_ubicacion">Ubicaci&oacute;n</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group form-group-sm floating-label" id="fk_a006_num_motivoError">
                                    <select name="fk_a006_num_motivo" id="fk_a006_num_motivo" class="form-control input-sm" {$disabled.ver}>
                                        <option value="">&nbsp;</option>
                                        {Select::miscelaneo('AFMOTTRASE',$form.fk_a006_num_motivo)}
                                    </select>
                                    <label for="fk_a006_num_motivo">Motivo Traslado</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group form-group-sm floating-label" id="ind_nro_resolucionError">
                                    <input type="text" name="ind_nro_resolucion" id="ind_nro_resolucion" value="{$form.ind_nro_resolucion}" class="form-control input-sm" maxlength="20" {$disabled.ver}>
                                    <label for="ind_nro_resolucion">Nro. Documento</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-sm floating-label" id="ind_comentariosError">
                                    <textarea name="ind_comentarios" id="ind_comentarios" class="form-control input-sm" style="height:50px;" {$disabled.ver}>{$form.ind_comentarios}</textarea>
                                    <label for="ind_comentarios">Comentarios</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
        <div class="col-sm-12">
            <div class="card">
                <div class="card-head card-head-xs style-primary text-center">
                    <header class="text-center">Distribuci&oacute;n Contable</header>
                </div>
                <div class="card-body" style="padding: 4px;">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm">
                                    <input type="text" name="num_monto" id="num_monto" value="{number_format($form.num_monto,2,',','.')}" class="form-control input-sm" style="text-align: right;" disabled>
                                    <label for="num_monto">Monto</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table id="datatable1" class="table table-condensed">
                                <thead>
                                    <tr>
                                        <th>Contabilidad</th>
                                        <th>Cuenta</th>
                                        <th style="text-align:left;">Descripci&oacute;n</th>
                                        <th>Signo</th>
                                    </tr>
                                </thead>
                                <tbody id="lista_tipo_transaccion_cuentas">
                                    {foreach item=registro from=$form.lista_tipo_transaccion_cuentas}
                                        <tr>
                                            <td>
                                                <div class="col-sm-12">
                                                    {$registro.nombre_contabilidad}
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    {$registro.num_cuenta}
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    {$registro.ind_descripcion}
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    {$registro.ind_signo}
                                                </div>
                                            </td>
                                        </tr>
                                    {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <span class="clearfix"></span>
    <div class="modal-footer">
        {if $form.metodo != ''}
            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal">
                <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
            </button>
            {if $form.metodo == 'anular'}
                <button type="button" class="btn btn-danger ink-reaction btn-raised logsUsuarioModal" id="accion">
                     <i class="fa fa-thumbs-down"></i> Anular
                </button>
            {elseif $form.metodo == 'revisar'}
                <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
                     <i class="fa fa-thumbs-up"></i> Revisar
                </button>
            {elseif $form.metodo == 'aprobar'}
                <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
                     <i class="fa fa-thumbs-up"></i> Aprobar
                </button>
            {else}
                <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
                    <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
                </button>
            {/if}
        {/if}
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        //  tamaño de ventana
        $('#modalAncho').css("width", "75%");

        //  bloqueo formulario
        $("#formAjax").submit(function() {
            return false;
        });

        //  envio formulario
        $('#accion').click(function() {
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize()+"&perfilM={in_array('AF-01-02-02-01-02',$_Parametros.perfil)}&perfilAN={in_array('AF-01-02-02-01-03',$_Parametros.perfil)}", function(dato) {
                if (dato['status'] == 'error') {
                    for (var i in dato.input) {
                        $(document.getElementById(dato.input[i].field+'Error')).removeClass('has-error has-feedback');
                        $(document.getElementById(dato.input[i].field+'Error')).addClass('has-error has-feedback');
                    }
                    //  mensaje
                    swal("Error!", dato['mensaje'][0], "error");
                } else {
                    //  mensaje
                    swal("Exito!", dato['mensaje'], "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    //  actualizo tabla
                    if (dato['status'] == 'crear') {
                        /*var table = $('.datatable1').DataTable();
                        table.row.add(dato.tr).draw(false);
                        $('.datatable1 tbody tr').not('[id]').attr('id','id'+dato['id']);*/
                    }
                    else if (dato['status'] == 'modificar') {
                        //$('#id'+dato['id']).html(dato['tr']);
                    }
                    else if (dato['status'] == 'revisar') {
                        /*var table = $('.datatable1').DataTable();
                        table.row('#id'+dato['id']).remove().draw(false);*/
                        //$('#id'+dato['id']).remove();
                    }
                    else if (dato['status'] == 'aprobar') {
                        /*var table = $('.datatable1').DataTable();
                        table.row('#id'+dato['id']).remove().draw(false);*/
                        //$('#id'+dato['id']).remove();
                    }
                    cargarPagina($('#formFiltro'), "{$_Parametros.url}modAF/procesos/BajaActivoCONTROL/");
                }
            }, 'json');
        });

        //  cuentas x tipo de transacción
        $('#fk_afc021_num_tipo_transaccion').change(function() {
            $.post("{$_Parametros.url}modAF/activos/ActivosCONTROL/TipoTransaccionCuentasContabilidadMET", "fk_afc021_num_tipo_transaccion="+$(this).val(), function(dato) {
                $('#lista_tipo_transaccion_cuentas').html(dato);
            });
        });

        inicializar();
    });
</script>
