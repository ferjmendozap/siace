<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary pull-left">Desincorporaci&oacute;n de Activos - {$data.titulo}</h2>
        <a data-toggle="offcanvas" title="Filtro" class="btn ink-reaction btn-floating-action btn-sm btn-primary pull-right" href="#offcanvas-filtro"><i class="fa fa-filter"></i></a>
    </div>

    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="card card-underline">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover datatable1">
                                <thead>
                                    <tr>
                                        <th width="50">Id.</th>
                                        <th width="125">Estado</th>
                                        <th width="100">Fecha</th>
                                        <th>Comentarios</th>
                                        <th width="100">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {foreach item=registro from=$listado}
                                        <tr id="id{$registro.pk_num_baja_activo}">
                                            <td>
                                                <input type="checkbox" name="idRegistro[]" value="{$registro.pk_num_baja_activo}" class="seleccionable" style="display:none;">
                                                <label>{$registro.pk_num_baja_activo}</label>
                                            </td>
                                            <td><label>{Select::options('af_estado_baja', $registro.ind_estado, 2)}</label></td>
                                            <td><label>{Fecha::formatFecha($registro.fec_fecha_baja,'Y-m-d','d-m-Y')}</label></td>
                                            <td><label>{$registro.ind_comentarios}</label></td>
                                            <td nowrap="true">
                                                {if $data.lista == 'listar'}
                                                    {if in_array('AF-01-02-02-01-02',$_Parametros.perfil) && $registro.ind_estado == 'PR'}
                                                        <button idRegistro="{$registro.pk_num_baja_activo}" class="btn ink-reaction btn-raised btn-xs btn-primary" id="modificar"
                                                                data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Desincorporación" title="Editar Desincorporación">
                                                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                        </button>
                                                    {/if}
                                                    <button idRegistro="{$registro.pk_num_baja_activo}" class="btn ink-reaction btn-raised btn-xs btn-warning" id="ver"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Ver Desincorporación" title="Ver Desincorporación">
                                                        <i class="icm icm-eye2" style="color: #ffffff;"></i>
                                                    </button>
                                                    <button idRegistro="{$registro.pk_num_baja_activo}" class="btn ink-reaction btn-raised btn-xs btn-info" id="imprimir"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Sustento de Movimiento" title="Imprimir Sustento Nro. {$registro.pk_num_baja_activo}">
                                                        <i class="fa fa-print" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                                {if $data.lista == 'revisar' && $registro.ind_estado == 'PR'}
                                                    <button idRegistro="{$registro.pk_num_baja_activo}" class="btn ink-reaction btn-raised btn-xs btn-primary" id="revisar"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Revisar Desincorporación"
                                                            title="Revisar Desincorporación Nro. {$registro.pk_num_baja_activo}">
                                                        <i class="icm icm-rating3" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                                {if $data.lista == 'aprobar' && $registro.ind_estado == 'RV'}
                                                    <button idRegistro="{$registro.pk_num_baja_activo}" class="btn ink-reaction btn-raised btn-xs btn-primary" id="aprobar"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Aprobar Desincorporación"
                                                            title="Aprobar Desincorporación Nro. {$registro.pk_num_baja_activo}">
                                                        <i class="icm icm-rating3" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                                {if $registro.ind_estado != 'AN' && $registro.ind_estado != 'AP'}
                                                    {if in_array('AF-01-02-02-01-03',$_Parametros.perfil)}
                                                        <button idRegistro="{$registro.pk_num_baja_activo}" class="btn ink-reaction btn-raised btn-xs btn-danger" id="anular"
                                                                data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Anular Desincorporación"
                                                                title="Anular Desincorporación Nro. {$registro.pk_num_baja_activo}">
                                                            <i class="fa fa-ban" style="color: #ffffff;"></i>
                                                        </button>
                                                    {/if}
                                                {/if}
                                            </td>
                                        </tr>
                                    {/foreach}
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="5">
                                            {if $data.lista == 'listar'}
                                                {if in_array('AF-01-02-02-01-01',$_Parametros.perfil)}
                                                    <button id="nuevo" class="btn ink-reaction btn-raised btn-info btn-sm"
                                                            data-toggle="modal"
                                                            data-target="#formModal"
                                                            data-keyboard="false"
                                                            data-backdrop="static" titulo="Nueva Desincorporación">
                                                        <i class="md md-create"></i> Nueva Desincorporación
                                                    </button>
                                                {/if}
                                            {/if}
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="offcanvas">
    <div id="offcanvas-filtro" class="offcanvas-pane width-7">
        <div class="offcanvas-head">
            <header>Filtro</header>
            <div class="offcanvas-tools">
                <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                    <i class="md md-close"></i>
                </a>
            </div>
        </div>

        <div class="offcanvas-body">
            <form action="{$_Parametros.url}modAF/procesos/BajaActivoCONTROL/indexMET/{$data.lista}" id="formFiltro" class="form" role="form" method="post" autocomplete="off" onsubmit="return cargarPagina();">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm ">
                            <select name="fnum_organismo" id="fnum_organismo" class="form-control input-sm" onchange="select('{$_Parametros.url}select/a004_dependencia', $('#fnum_dependencia'), 'fk_a001_num_organismo='+$(this).val(), ['fnum_centro_costo']);">
                                <option value="">&nbsp;</option>
                                {Select::lista('a001_organismo','pk_num_organismo','ind_descripcion_empresa',{$filtro.fnum_organismo})}
                            </select>
                            <label for="fnum_organismo">Organismo</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm ">
                            <select name="fnum_dependencia" id="fnum_dependencia" class="form-control input-sm" onchange="select('{$_Parametros.url}select/a023_centro_costo', $('#fnum_centro_costo'), 'fk_a004_num_dependencia='+$(this).val());">
                                <option value="">&nbsp;</option>
                                {Select::lista('a004_dependencia','pk_num_dependencia','ind_dependencia',{$filtro.fnum_dependencia},0,['fk_a001_num_organismo'=>$filtro.fnum_organismo])}
                            </select>
                            <label for="fnum_dependencia">Dependencia</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm ">
                            <select name="fnum_centro_costo" id="fnum_centro_costo" class="form-control input-sm">
                                <option value="">&nbsp;</option>
                                {Select::lista('a023_centro_costo','pk_num_centro_costo','ind_descripcion_centro_costo',{$filtro.fnum_centro_costo},0,['fk_a004_num_dependencia'=>$filtro.fnum_dependencia])}
                            </select>
                            <label for="fnum_centro_costo">Centro de Costo</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm ">
                            <select name="fnum_motivo" id="fnum_motivo" class="form-control input-sm">
                                <option value="">&nbsp;</option>
                                {Select::miscelaneo('AFMOTTRASE',$filtro.fnum_motivo)}
                            </select>
                            <label for="fnum_motivo">Motivo de Traslado</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm ">
                            <select name="find_estado" id="find_estado" class="form-control input-sm">
                                {if $data.lista == 'listar'}
                                    <option value="">&nbsp;</option>
                                    {Select::options('af_estado_baja',$filtro.find_estado)}
                                {else}
                                    {Select::options('af_estado_baja',$filtro.find_estado,1)}
                                {/if}
                            </select>
                            <label for="find_estado">Estado</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group form-group-sm">
                            <input type="text" name="ffec_fecha_bajad" id="ffec_fecha_bajad" value="{$filtro.ffec_fecha_bajad}" class="form-control input-sm date">
                            <label for="ffec_fecha_bajad">Desde</label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-group-sm">
                            <input type="text" name="ffec_fecha_bajah" id="ffec_fecha_bajah" value="{$filtro.ffec_fecha_bajah}" class="form-control input-sm date">
                            <label for="ffec_fecha_bajah">Hasta</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-xs ink-reaction btn-raised btn-info" id="botonFiltro">
                            Filtrar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        //  base
        $base = '{$_Parametros.url}modAF/procesos/BajaActivoCONTROL/';

        //  nuevo registro
        $('#nuevo').click(function() {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'formMET/nuevo', '', function($dato) {
                $('#ContenidoModal').html($dato);
            });
        });

        //  editar registro
        $('.datatable1 tbody').on( 'click', '#modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'formMET/editar', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //  ver registro
        $('.datatable1 tbody').on( 'click', '#ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'formMET/ver', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //  imprimir registro
        $('.datatable1 tbody').on( 'click', '#imprimir', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'imprimirMET', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //  revisar registro
        $('.datatable1 tbody').on( 'click', '#revisar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'formMET/revisar', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //  aprobar registro
        $('.datatable1 tbody').on( 'click', '#aprobar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'formMET/aprobar', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //  anular registro
        $('.datatable1 tbody').on( 'click', '#anular', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'formMET/anular', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
    });
</script>
