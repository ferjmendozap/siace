<section class="style-default-bright">
  <div class="section-header">

    <h2 class="text-primary">&nbsp;Generar Acta de Responsabilidad por Funcionario</h2>

   </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="card card-underline">
                    <div class="card-body">
                        <div class="table-responsive">
                          <table id="dataTablaJson" class="table table-striped table-hover">
                            <thead>
                                     <tr>
                                        <th width="50">Id.</th>
                                        <th width="100">Cédula</th>
                                        <th>Nombre Completo</th>
                                        <th width="75" style="text-align: center;">Acciones</th>
                                     </tr>
                                  </thead>
                          <tbody>
                           {foreach item=persona from=$lista}
                            <tr id="idPersona{$persona.pk_num_persona}">
                                <input type="hidden" value="{$persona.pk_num_persona}" class="persona"
                                       proveedor="{$persona.pk_num_persona}"
                                       nombre="{$persona.ind_nombre1} {$persona.ind_nombre2} {$persona.ind_apellido1} {$persona.ind_apellido2}"
                                       documentoProv="{$persona.ind_documento_fiscal}"
                                        >                                
                                <td><label>{$persona.pk_num_persona}</label></td>
                                <td><label>{$persona.ind_cedula_documento}</label></td>
                                <td><label>{$persona.ind_nombre1} {$persona.ind_nombre2} {$persona.ind_apellido1} {$persona.ind_apellido2}</label></td>
                                        <td nowrap="true">
                                                {if $persona.fk_a003_num_persona_usuario>0}
                                                <button idRegistro="{$persona.pk_num_persona}"  class="logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" id="generarActaResponsabilidadpf" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Acta de Responsabilidad por Funcionario" title="Generar Acta de Responsabilidad">
                                                    <i class="fa fa-check" style="color: #ffffff;"></i>
                                                </button>
                                                {else}
                                                <button idRegistro="{$persona.pk_num_persona}"  class="logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" id="generarActaResponsabilidadpf"  title="Generar Acta de Responsabilidad">
                                                    <i class="fa fa-check" style="color: #ffffff;"></i>
                                                </button>

                                                {/if}
                                        </td>
                            </tr>
                        {/foreach}
                    </tbody>
                    </table>
                </div>
            </div>
           </div>
          </div>
        </div>
    </div>
</section>
<span class="clearfix"></span>

<script type="text/javascript">
  $(document).ready(function () {
    
     $('#dataTablaJson').DataTable({
            "dom":  'lCfrtip',
            "order": [],
         "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "processing": "Procesando ...",
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

      //  base
        var $base = '{$_Parametros.url}modAF/activos/ActivosCONTROL/';  

      // var $titulo = $(this).attr('titulo');


      //  generar acta de responsabilidad por Funcionario
       $('#dataTablaJson tbody').on( 'click', '#generarActaResponsabilidadpf', function () {    

         $('#formModalLabel').html($(this).attr('titulo'));
                         $.post($base+'generarActaResponsabilidadPFMET', { id: $(this).attr('idRegistro')}, function($dato){
                              $('#ContenidoModal').html($dato);
                         }); 
                                       // Grenerar Aviso
          $.post($base+'generarActaResponsabilidadCPFMET', { id: $(this).attr('idRegistro')}, function($dato){                                     
                if($dato['valor']==1)  
                {
                  // $('#generarModalActa').click(function(){}); 
                  // swal("Aviso", $dato['mensaje'], "warning");  
                }
                  else{
                   swal("Aviso", $dato['mensaje'], "warning");  
                  }              
          },'json');
     }); 

    });
   
</script>


