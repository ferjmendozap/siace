<section class="style-default-bright">
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="card card-underline">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="tabla_activos" class="table table-striped table-hover datatable1">
                                <thead>
                                    <tr>
                                        <th width="50">Id.</th>
                                        <th width="100">C&oacute;digo</th>
                                        <th>Descripcion</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {foreach item=registro from=$listado}
                                        {if $campos.opcion == 'af_movimientos'}
                                            <tr id="id{$registro.pk_num_activo}" onclick="seleccionar(['{$campos.campo1}','{$campos.campo2}','{$campos.campo3}','{$campos.campo4}','{$campos.campo5}','{$campos.campo6}','{$campos.campo7}','{$campos.campo8}','{$campos.campo9}','{$campos.campo10}','{$campos.campo11}'], ['{$registro.pk_num_activo}','{$registro.cod_codigo_interno}','{$registro.ind_descripcion|escape}','{$registro.pk_num_organismo}','{$registro.pk_num_dependencia}','{$registro.fk_a023_num_centro_costo}','{$registro.fk_afc005_num_ubicacion}','{$registro.fk_a003_num_persona_usuario}','{$registro.persona_usuario}','{$registro.fk_a003_num_persona_responsable}','{$registro.persona_responsable}']);">
                                        {elseif $campos.opcion == 'af_baja_activos'}
                                            <tr id="id{$registro.pk_num_activo}" onclick="seleccionar(['{$campos.campo1}','{$campos.campo2}','{$campos.campo3}','{$campos.campo4}','{$campos.campo5}','{$campos.campo6}','{$campos.campo7}','{$campos.campo8}','{$campos.campo9}','{$campos.campo10}','{$campos.campo11}'], ['{$registro.pk_num_activo}','{$registro.cod_codigo_interno}','{$registro.ind_descripcion|escape}','{$registro.ind_descripcion_empresa}','{$registro.ind_dependencia}','{$registro.ind_descripcion_centro_costo}','{$registro.persona_responsable}','{$registro.ind_nombre_categoria}','{$registro.ind_nombre_ubicacion}','{$registro.ind_nro_factura}','{number_format($registro.num_monto,2,',','.')}']);">
                                        {elseif $campos.opcion == 'af_faltantes_activos'}
                                            <tr id="id{$registro.pk_num_activo}" onclick="seleccionar(['{$campos.campo1}','{$campos.campo2}','{$campos.campo3}','{$campos.campo4}','{$campos.campo5}','{$campos.campo6}','{$campos.campo7}','{$campos.campo8}','{$campos.campo9}'], ['{$registro.pk_num_activo}','{$registro.cod_codigo_interno}','{$registro.ind_descripcion|escape}','{$registro.ind_descripcion_empresa}','{$registro.ind_dependencia}','{$registro.ind_descripcion_centro_costo}','{$registro.persona_responsable}','{$registro.persona_usuario}','{number_format($registro.num_monto,2,',','.')}']);">
                                        {elseif $campos.opcion == 'af_faltantes_detalle'}
                                            <tr id="id{$registro.pk_num_activo}" onclick="seleccionar(['{$campos.campo1}','{$campos.campo2}','{$campos.campo3}','{$campos.campo4}','{$campos.campo5}'], ['{$registro.pk_num_activo}','{$registro.cod_codigo_interno}','{$registro.ind_descripcion|escape}','{$registro.fk_afc005_num_ubicacion}','{$registro.fk_afc007_num_situacion}']);">
                                        {elseif $campos.opcion == 'descripcion'}
                                            <tr id="id{$registro.pk_num_activo}" onclick="seleccionar(['{$campos.campo1}','{$campos.campo2}','{$campos.campo3}'], ['{$registro.pk_num_activo}','{$registro.cod_codigo_interno}','{$registro.ind_descripcion|escape}']);">
                                        {else}
                                            <tr id="id{$registro.pk_num_activo}" onclick="seleccionar(['{$campos.campo1}','{$campos.campo2}'], ['{$registro.pk_num_activo}','{$registro.cod_codigo_interno}']);">
                                        {/if}
                                            <td><label>{$registro.pk_num_activo}</label></td>
                                            <td><label>{$registro.cod_codigo_interno}</label></td>
                                            <td><label>{$registro.ind_descripcion}</label></td>
                                        </tr>
                                    {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        inicializar();
    });
</script>