<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary pull-left">{$data.titulo}</h2>
        <a data-toggle="offcanvas" title="Filtro" class="btn ink-reaction btn-floating-action btn-sm btn-primary pull-right" href="#offcanvas-filtro"><i class="fa fa-filter"></i></a>
    </div>

    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="card card-underline">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="dataTablaJson" class="table table-striped table-hover">
                                 {if $data.lista == 'generarActaP'}
                                  <thead>
                                     <tr>
                                        <th width="50">Id.</th>
                                        <th width="100">Cedula</th>
                                        <th>Nombre Completo</th>
                                        <th width="75" style="text-align: center;">Acciones</th>
                                     </tr>
                                  </thead>
                                  <tbody>
                                   {foreach item=registro from=$listado}
                                      <tr id="id{$registro.pk_num_empleado}" onclick="seleccionar(['{$listado.campo1}','{$listado.campo2}'], ['{$registro.pk_num_empleado}','{$registro.nombre_completo}']);">
                                        <td><label>{$registro.pk_num_empleado}</label></td>
                                        <td><label>{$registro.ind_cedula_documento}</label></td>
                                        <td><label>{$registro.nombre_completo}</label></td>
                                        <td nowrap="true">
                                                <button idRegistro="{$registro.pk_num_activo}" class="logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" id="generarActaResponsabilidad"
                                                        data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Generar Acta de Responsabilidad" title="Generar Acta de Responsabilidad">
                                                    <i class="fa fa-check" style="color: #ffffff;"></i>
                                                </button>
                                        </td>
                                     </tr>
                                    {/foreach}   
                                </tbody>
                                {else}                               
                                  <thead>
                                     <tr>
                                        <th width="50">Id.</th>
                                        <th width="100">C&oacute;digo</th>
                                        <th>Descripcion</th>
                                        <th width="75" style="text-align: center;">Acciones</th>
                                     </tr>
                                  </thead>

                                   <tbody>
                                   {foreach item=registro from=$listado}
                                    <tr id="id{$registro.pk_num_activo}">
                                        <td><label>{$registro.pk_num_activo}</label></td>
                                        <td><label>{$registro.cod_codigo_interno}</label></td>
                                        <td><label>{$registro.ind_descripcion}</label></td>
                                        <td nowrap="true">
                                            {if $data.lista == 'listar'}
                                                {if in_array('AF-01-01-01-02',$_Parametros.perfil)}
                                                    <button idRegistro="{$registro.pk_num_activo}" class="logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" id="modificar"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Activo" title="Editar Activo">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                                <button idRegistro="{$registro.pk_num_activo}" class="logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" id="ver"
                                                        data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Ver Registro" title="Ver Activo">
                                                    <i class="fa fa-eye" style="color: #ffffff;"></i>
                                                </button>
                                            {elseif $data.lista == 'aprobar'}
                                                <button idRegistro="{$registro.pk_num_activo}" class="logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" id="aprobar"
                                                        data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Aprobar Alta de Activo" title="Aprobar Alta de Activo">
                                                    <i class="md md-done-all" style="color: #ffffff;"></i>
                                                </button>
                                            {elseif $data.lista == 'generarActa'}
                                                <button idRegistro="{$registro.pk_num_activo}" class="logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" id="generarActaResponsabilidad"
                                                        data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Genera Acta de Responsabilidad1" title="Generare Acta de Responsabilidad1">
                                                    <i class="fa fa-check" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}

                                   </tbody>
                                {/if}
                                <tfoot>
                                    <tr>
                                        <th colspan="4">
                                            {if $data.lista == 'listar'}
                                                {if in_array('AF-01-01-01-01',$_Parametros.perfil)}
                                                    <button id="nuevo" class="btn ink-reaction btn-raised btn-info btn-sm"
                                                            data-toggle="modal"
                                                            data-target="#formModal"
                                                            data-keyboard="false"
                                                            data-backdrop="static" titulo="Nuevo Activo">
                                                        <i class="md md-create"></i> Nuevo Activo
                                                    </button>
                                                {/if}
                                            {/if}
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{if $data.lista != 'generarActaP'}
<div class="offcanvas">
    <div id="offcanvas-filtro" class="offcanvas-pane width-7">
        <div class="offcanvas-head">
            <header>Filtro</header>
            <div class="offcanvas-tools">
                <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                    <i class="md md-close"></i>
                </a>
            </div>
        </div>

        <div class="offcanvas-body">
            <form action="{$_Parametros.url}modAF/activos/ActivosCONTROL/indexMET/{$data.lista}" id="formFiltro" class="form" role="form" method="post" autocomplete="off" onsubmit="return cargarPagina();">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm ">
                            <select name="fpk_num_organismo" id="fpk_num_organismo" class="form-control input-sm" onchange="select('{$_Parametros.url}select/a004_dependencia', $('#fpk_num_dependencia'), 'fk_a001_num_organismo='+$(this).val(), ['fpk_num_centro_costo']);">
                                <option value="">&nbsp;</option>
                                {Select::lista('a001_organismo','pk_num_organismo','ind_descripcion_empresa',{$filtro.fpk_num_organismo})}
                            </select>
                            <label for="fpk_num_organismo">Organismo</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm ">
                            <select name="fpk_num_dependencia" id="fpk_num_dependencia" class="form-control input-sm" onchange="select('{$_Parametros.url}select/a023_centro_costo', $('#fpk_num_centro_costo'), 'fk_a004_num_dependencia='+$(this).val());">
                                <option value="">&nbsp;</option>
                                {Select::lista('a004_dependencia','pk_num_dependencia','ind_dependencia',{$filtro.fpk_num_dependencia},0,['fk_a001_num_organismo'=>$filtro.fpk_num_organismo])}
                            </select>
                            <label for="fpk_num_dependencia">Dependencia</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm ">
                            <select name="fpk_num_centro_costo" id="fpk_num_centro_costo" class="form-control input-sm">
                                <option value="">&nbsp;</option>
                                {Select::lista('a023_centro_costo','pk_num_centro_costo','ind_descripcion_centro_costo',{$filtro.fpk_num_centro_costo},0,['fk_a004_num_dependencia'=>$filtro.fpk_num_dependencia])}
                            </select>
                            <label for="fpk_num_centro_costo">Centro de Costo</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm ">
                            <select name="fpk_num_categoria" id="fpk_num_categoria" class="form-control input-sm">
                                <option value="">&nbsp;</option>
                                {Select::lista('af_c019_categoria','pk_num_categoria','ind_nombre_categoria',$filtro.fpk_num_categoria,0,[],'ind_codigo')}
                            </select>
                            <label for="fpk_num_categoria">Categoria</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group form-group-sm">
                            <input type="hidden" name="fpk_num_clasificacion" id="fpk_num_clasificacion" value="{$filtro.fpk_num_clasificacion}">
                            <input type="text" name="fcod_clasificacion" id="fcod_clasificacion" value="{$filtro.fcod_clasificacion}" class="form-control input-sm" readonly>
                            <label for="fcod_clasificacion">Clasificaci&oacute;n</label>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group form-group-sm">
                            <input type="text" name="find_nombre_clasificacion" id="find_nombre_clasificacion" value="{$filtro.find_nombre_clasificacion}" class="form-control input-sm" readonly>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector" style="margin: 0px 0px -45px -10px;"
                                data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Clasificaci&oacute;n"
                                onclick="selector($(this), '{$_Parametros.url}modAF/maestros/ClasificacionCONTROL/selectorMET', ['fpk_num_clasificacion','fcod_clasificacion','find_nombre_clasificacion']);">
                            <i class="md md-search"></i>
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm ">
                            <select name="fpk_num_movimiento" id="fpk_num_movimiento" class="form-control input-sm">
                                <option value="">&nbsp;</option>
                                {Select::lista('af_c009_tipo_movimiento','pk_num_movimiento','ind_nombre_movimiento',$filtro.fpk_num_movimiento)}
                            </select>
                            <label for="fpk_num_movimiento">Tipo de Movimiento</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm ">
                            <select name="fpk_num_ubicacion" id="fpk_num_ubicacion" class="form-control input-sm">
                                <option value="">&nbsp;</option>
                                {Select::lista('af_c005_ubicacion','pk_num_ubicacion','ind_nombre_ubicacion',$filtro.fpk_num_ubicacion)}
                            </select>
                            <label for="fpk_num_ubicacion">Ubicaci&oacute;n</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm ">
                            <select name="fpk_num_situacion" id="fpk_num_situacion" class="form-control input-sm">
                                <option value="">&nbsp;</option>
                                {Select::lista('af_c007_situacion','pk_num_situacion','ind_nombre_situacion',$filtro.fpk_num_situacion)}
                            </select>
                            <label for="fpk_num_situacion">Situaci&oacute;n</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm ">
                            <select name="fnum_tipo_activo" id="fnum_tipo_activo" class="form-control input-sm">
                                <option value="">&nbsp;</option>
                                {Select::miscelaneo('AFTIPOACT',$filtro.fnum_tipo_activo)}
                            </select>
                            <label for="fnum_tipo_activo">Tipo de Activo</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm ">
                            <select name="find_estado" id="find_estado" class="form-control input-sm">
                                {if $data.lista == 'aprobar'}
                                    {Select::options('af_estado_activo',$filtro.find_estado,1)}
                                {else}
                                    <option value="">&nbsp;</option>
                                    {Select::options('af_estado_activo',$filtro.find_estado,0)}
                                {/if}
                            </select>
                            <label for="find_estado">Estado</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-xs ink-reaction btn-raised btn-info" id="botonFiltro">
                            Filtrar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
 {/if}
<script type="text/javascript">

    $(document).ready(function() {
     
        //  base
        var $base = '{$_Parametros.url}modAF/activos/ActivosCONTROL/';
        //  
        var formFiltro = $('#formFiltro').serialize();
        var app = new AppFunciones();

        var dt = app.dataTable(
                '#dataTablaJson',
                $base+"jsonDataTablaMET/{$data.lista}/?"+formFiltro,
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "pk_num_activo" },
                    { "data": "cod_codigo_interno" },
                    { "data": "ind_descripcion" },
                    { "orderable": false,"data": "acciones",'width':100 }
                ]
        ); 
    
        //  nuevo registro
        $('#nuevo').click(function() {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'formMET/nuevo', '', function($dato) {
                $('#ContenidoModal').html($dato);
            });
        });

        //  editar registro
        $('#dataTablaJson tbody').on( 'click', '#modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'formMET/editar', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //  ver registro
        $('#dataTablaJson tbody').on( 'click', '#ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'formMET/ver', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //  aprobar registro
        $('#dataTablaJson tbody').on( 'click', '#aprobar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'formMET/aprobar', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

      //  generar acta de responsabilidad
        $('#dataTablaJson tbody').on( 'click', '#generarActaResponsabilidad', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'generarActaResponsabilidadMET', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });         
   
 });
       
</script>
