<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Generación de Vouchers Transacciones de Baja (ONCO)</h2>
    </div>

    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="card card-underline">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover datatable1">
                                <thead>
                                    <tr>
                                        <th width="50">Id.</th>
                                        <th width="100">Fecha Ingreso</th>
                                        <th width="100">C&oacute;digo</th>
                                        <th>Descripcion</th>
                                        <th width="50" style="text-align: center;">Accion</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {foreach item=registro from=$listado}
                                        <tr id="id{$registro.pk_num_activo}">
                                            <td><label>{$registro.pk_num_activo}</label></td>
                                            <td><label>{Fecha::formatFecha($registro.fec_ingreso,'Y-m-d','d-m-Y')}</label></td>
                                            <td><label>{$registro.cod_codigo_interno}</label></td>
                                            <td><label>{$registro.ind_descripcion}</label></td>
                                            <td>
                                                <button idRegistro="{$registro.pk_num_activo}" class="mostrar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                        data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Voucher Transacción de Baja (ONCO)" title="Generar Voucher">
                                                    <i class="md md-account-balance-wallet" style="color: #ffffff;"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {
        //  base
        var $base = '{$_Parametros.url}modAF/vouchers/VoucherBajaOncoCONTROL/';

        //  mostrar voucher
        $('.datatable1 tbody').on( 'click', '.mostrar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'mostrarMET', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
    });
</script>
