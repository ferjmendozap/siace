<form action="{$_Parametros.url}modAF/vouchers/VoucherIngreso20CONTROL/{$form.metodo}MET" id="formAjax" class="form" role="form" method="post">
    <input type="hidden" name="pk_num_voucher_mast" id="pk_num_voucher_mast" value="{$form.pk_num_voucher_mast}">
    <input type="hidden" name="fk_cbc002_num_sistema_fuente" id="fk_cbc002_num_sistema_fuente" value="{$form.fk_cbc002_num_sistema_fuente}">
    <input type="hidden" name="fk_cbb003_num_libro_contabilidad" id="fk_cbb003_num_libro_contabilidad" value="{$form.fk_cbb003_num_libro_contabilidad}">
    <input type="hidden" name="pk_num_activo" id="pk_num_activo" value="{$form.pk_num_activo}">

    <div class="modal-body">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-head card-head-xs style-primary text-center">
                    <header class="text-center">Informaci&oacute;n General</header>
                </div>
                <div class="card-body" style="padding: 4px;">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-sm floating-label" id="fk_a001_num_organismoError">
                                    <select name="fk_a001_num_organismo" id="fk_a001_num_organismo" class="form-control input-sm">
                                        {Select::lista('a001_organismo','pk_num_organismo','ind_descripcion_empresa',$form.fk_a001_num_organismo,1)}
                                    </select>
                                    <label for="fk_a001_num_organismo">Organismo</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm floating-label" id="fk_cbc003_num_voucherError">
                                    <select name="fk_cbc003_num_voucher" id="fk_cbc003_num_voucher" class="form-control input-sm">
                                        {Select::lista('cb_c003_tipo_voucher','pk_num_voucher','ind_descripcion',$form.fk_cbc003_num_voucher,1)}
                                    </select>
                                    <label for="fk_cbc003_num_voucher">Voucher</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm" id="ind_periodoError">
                                    <input type="text" name="ind_periodo" id="ind_periodo" value="{$form.ind_anio}-{$form.ind_mes}" class="form-control input-sm" readonly>
                                    <label for="ind_periodo">Periodo</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-sm floating-label" id="fk_a004_num_dependenciaError">
                                    <select name="fk_a004_num_dependencia" id="fk_a004_num_dependencia" class="form-control input-sm">
                                        {Select::lista('a004_dependencia','pk_num_dependencia','ind_dependencia',$form.fk_a004_num_dependencia,1)}
                                    </select>
                                    <label for="fk_a004_num_dependencia">Dependencia</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm" id="ind_voucherError">
                                    <input type="text" name="ind_voucher" id="ind_voucher" value="{$form.ind_voucher}" class="form-control input-sm" readonly>
                                    <label for="ind_voucher">Voucher</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm floating-label" id="fec_fecha_voucherError">
                                    <input type="text" name="fec_fecha_voucher" id="fec_fecha_voucher" value="{Fecha::formatFecha($form.fec_fecha_voucher,'Y-m-d','d-m-Y')}" class="form-control input-sm date" maxlength="10" onchange="setPeriodo(this.value);">
                                    <label for="fec_fecha_voucher">Fecha</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm floating-label" id="fk_cbb005_num_contabilidadesError">
                                    <select name="fk_cbb005_num_contabilidades" id="fk_cbb005_num_contabilidades" class="form-control input-sm">
                                        {Select::lista('cb_b005_contabilidades','pk_num_contabilidades','ind_descripcion',$form.fk_cbb005_num_contabilidades,1)}
                                    </select>
                                    <label for="fk_cbb005_num_contabilidades">Contabilidad</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-sm floating-label" id="fk_cbb002_num_libro_contableError">
                                    <select name="fk_cbb002_num_libro_contable" id="fk_cbb002_num_libro_contable" class="form-control input-sm">
                                        {Select::lista('cb_b002_libro_contable','pk_num_libro_contable','ind_descripcion',$form.fk_cbb002_num_libro_contable,1)}
                                    </select>
                                    <label for="fk_cbb002_num_libro_contable">Libro Contable</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-group-sm" id="txt_titulo_voucherError">
                                    <input type="text" name="txt_titulo_voucher" id="txt_titulo_voucher" value="{$form.txt_titulo_voucher}" class="form-control input-sm">
                                    <label for="txt_titulo_voucher">Descripci&oacute;n</label>
                                    <p class="help-block"><span class="text-xs" style="color:red;">*</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-sm" id="fk_a003_num_preparado_porError">
                                    <input type="hidden" name="fk_a003_num_preparado_por" id="fk_a003_num_preparado_por" value="{$form.fk_a003_num_preparado_por}">
                                    <input type="text" name="preparado_por" id="preparado_por" value="{$form.preparado_por}" class="form-control input-sm" disabled>
                                    <label for="preparado_por">Preparado Por</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-group-sm" id="fk_a003_num_aprobado_porError">
                                    <input type="hidden" name="fk_a003_num_aprobado_por" id="fk_a003_num_aprobado_por" value="{$form.fk_a003_num_aprobado_por}">
                                    <input type="text" name="aprobado_por" id="aprobado_por" value="{$form.aprobado_por}" class="form-control input-sm" disabled>
                                    <label for="aprobado_por">Aprobado Por</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
        <div class="col-sm-12">
            <div class="card">
                <div class="card-head card-head-xs style-primary text-center">
                    <header class="text-center">Distribuci&oacute;n Contable</header>
                </div>
                <div class="card-body" style="padding: 4px;">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table id="datatable1" class="table table-condensed" style="min-width: 1000px;">
                                <thead>
                                    <tr>
                                        <th style="width:150px;">Cuenta</th>
                                        <th style="width:150px; text-align: right;">Debe</th>
                                        <th style="width:150px; text-align: right;">Haber</th>
                                        <th style="width:100px; text-align: center;">Persona</th>
                                        <th style="width:100px; text-align: center;">C.C</th>
                                        <th style="text-align:left;">Descripci&oacute;n</th>
                                    </tr>
                                </thead>
                                <tbody id="lista_tipo_transaccion_cuentas">
                                    {$num_creditos = 0}
                                    {$num_debitos = 0}
                                    {foreach item=registro from=$form.lista_cuentas}
                                        {if $registro.ind_signo == '-'}
                                            {$num_debe = 0}
                                            {$num_haber = $form.num_monto * -1}
                                            {$num_debitos = $num_debitos + $num_haber}
                                        {else}
                                            {$num_debe = $form.num_monto}
                                            {$num_haber = 0}
                                            {$num_creditos = $num_creditos + $num_debe}
                                        {/if}
                                        <tr id="detalle{$numero}">
                                            <td>
                                                <input type="hidden" name="detalle_fk_cbb004_num_cuenta[]" value="{$registro.fk_cbb004_num_cuenta}">
                                                {$registro.num_cuenta}
                                            </td>
                                            <td align="right">
                                                <input type="hidden" name="detalle_num_debe[]" value="{abs($num_debe)}">
                                                {number_format($num_debe,2,',','.')}
                                            </td>
                                            <td align="right">
                                                <input type="hidden" name="detalle_num_haber[]" value="{abs($num_haber)}">
                                                {number_format($num_haber,2,',','.')}
                                            </td>
                                            <td align="center">
                                                <input type="hidden" name="detalle_fk_a003_num_persona[]" value="{$registro.fk_a003_num_persona}">
                                                {$form.persona}
                                            </td>
                                            <td align="center">
                                                <input type="hidden" name="detalle_fk_a023_num_centro_costo[]" value="{$registro.fk_a023_num_centro_costo}">
                                                {$form.centro_costo}
                                            </td>
                                            <td>
                                                <input type="hidden" name="detalle_ind_descripcion[]" value="{$registro.ind_descripcion}">
                                                {$registro.ind_descripcion}
                                            </td>
                                        </tr>
                                    {/foreach}
                                </tbody>
                            </table>
                        </div>
                        <input type="hidden" name="num_creditos" id="num_creditos" value="{abs($num_creditos)}">
                        <input type="hidden" name="num_debitos" id="num_debitos" value="{abs($num_debitos)}">
                        <div>
                            <table class="table table-condensed">
                                <thead>
                                    <tr>
                                        <td style="width:150px; font-weight: bold;">Total:</td>
                                        <td style="width:150px; font-weight: bold; text-align: right; color: black;">
                                            {number_format($num_creditos,2,',','.')}
                                        </td>
                                        <td style="width:150px; font-weight: bold; text-align: right; color: red;">
                                            {number_format($num_debitos,2,',','.')}
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <span class="clearfix"></span>
    <div class="modal-footer">
        {if $form.metodo != ''}
            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal">
                <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
            </button>
            <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
                <span class="glyphicon glyphicon-floppy-disk"></span> Generar Voucher
            </button>
        {/if}
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        //  tamaño de ventana
        $('#modalAncho').css("width", "75%");

        //  bloqueo formulario
        $("#formAjax").submit(function() {
            return false;
        });

        //  envio formulario
        $('#accion').click(function() {
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function(dato) {
                if (dato['status'] == 'error') {
                    for (var i in dato.input) {
                        $(document.getElementById(dato.input[i].field+'Error')).removeClass('has-error has-feedback');
                        $(document.getElementById(dato.input[i].field+'Error')).addClass('has-error has-feedback');
                    }
                    //  mensaje
                    swal("Error!", dato['mensaje'][0], "error");
                } else {
                    //  mensaje
                    swal("Exito!", dato['mensaje'], "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    //  actualizo tabla
                    cargarUrl("{$_Parametros.url}modAF/vouchers/VoucherIngreso20CONTROL/");
                }
            }, 'json');
        });

        inicializar();
    });

    /**
     *  Obtener el periodo a partir de la fecha
     *
     *  @param (string) fecha
     */
    function setPeriodo(fecha) {
        var f = fecha.split("-");
        $('#ind_periodo').val(f[2] + '-' + f[1]);
    }
</script>
