<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        08-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
#Mostrar los errores en php.
ini_set('display_errors', 0);
ini_set('error_reporting', E_ALL);
#Establece cuáles errores de PHP son notificados
#E_ALL: Notificar todos los errores de PHP (ver el registro de cambios)
error_reporting(E_ALL);
setlocale(LC_TIME, 'es_VE', 'es_VE.utf-8', 'es_VE.utf8'); # Asi es mucho mas seguro que funciones, ya que no todos los sistemas llaman igual al locale ;)
date_default_timezone_set('America/Caracas');
#lo que hace es mostrar la manera de la forma en la que se separan las carpetas.
#Por ejemplo. En Windows es \ en Linux es /....
define('DS', DIRECTORY_SEPARATOR);
#Define Cuel la Ruta Principal del sistema
define('ROOT', realpath(dirname(__FILE__)) . DS);
#Define cual es la ruta de la direccion de la carpeta de la aplicacion.
define('APP_PATH', ROOT . 'sistema' .DS);
#Define cual es la ruta de la direccion de la carpeta de la Modelo.
define('RUTA_MODELO',ROOT . 'modelos' .DS);
#Define cual es la ruta de la direccion de la carpeta de lod Modulos.
define('RUTA_Modulo',ROOT . 'modulos' .DS);
#Define la ruta para incluir fpdf
define('LIBRERIA_FPDF',ROOT . 'librerias' .DS.'fpdf-V1.7'.DS.'fpdf.php');
#Try es un modelo de excepciones similar al de otros lenguajes de programación
try{
    #Parametros de Configuracion del sistema
    require_once APP_PATH.'config'.DS.'config.php';
    #Se encarga de estableser el modo de conexion a la base de datos.
    require_once APP_PATH.'control'.DS.'db.php';
    #Se encarga en utilizar la url como enrutado para saber
    #cual es el controlador y metodo a usar dentro de la aplicacion
    require_once APP_PATH.'http'.DS.'Solicitud.php';
    #Se encarga de instanciar el controlado y metodo a usar.
    require_once APP_PATH.'http'.DS.'Respuesta.php';
    #Clase que se encarga Para encriptar Con MD5 y SHA-1 Con un token Unico de la aplicacion
    require_once APP_PATH.'control'.DS.'Hash.php';
    #Se encarga de la seguridad de la sesion
    require_once APP_PATH.'control'.DS.'session.php';
    #Controlador Principal donde se aplican las reglas de negocio MVC y las validaciones de las datos por metodo POST
    require_once APP_PATH.'control'.DS.'controlador.php';
    #Modelo Principal Donde se construye la conexión a base de datos y parámetros a usar
    require_once APP_PATH.'control'.DS.'modelo.php';
    #Vista Principal. se encarga de cargar los css, js, plugins y armar las vistas de cada controlador
    require_once APP_PATH.'control'.DS.'vista.php';
    #Iniciar una nueva sesión o reanudar la existente
    Session::metInit();
    #Unifica Toda la Aplicacion MVC. y la ejecuta.
    Respuesta::metRun(new Solicitud);
#catch Visualiza cualquier Excepción  que se genere en el try.
}catch(Exception $e){
    #Muestra el Mensaje Generado por la Excepción
	echo $e->getMessage();
}
