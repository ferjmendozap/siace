<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        17-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class scriptCargaModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
    }

    public function metBusquedaSimple($tabla, $innerJoin, $where)
    {
        $busquedaSimple = $this->_db->query("
            SELECT
              *
            FROM
              $tabla
              $innerJoin
            WHERE
              $where
        ");
        $busquedaSimple->setFetchMode(PDO::FETCH_ASSOC);
        return $busquedaSimple->fetch();
    }

    public function metMostrarSelectImpuesto($codImpuesto)
    {
        $impuesto = $this->_db->query("
            SELECT
           cp_b015_impuesto.pk_num_impuesto
            FROM
              cp_b015_impuesto
            WHERE
              cp_b015_impuesto.cod_impuesto='$codImpuesto'
        ");
        $impuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $impuesto->fetch();
    }

    public function metMostrarCuenta($codCuenta)
    {
        $impuesto = $this->_db->query("
            SELECT
           cb_b004_plan_cuenta.pk_num_cuenta
            FROM
              cb_b004_plan_cuenta
            WHERE
              cb_b004_plan_cuenta.cod_cuenta='$codCuenta'
        ");
        $impuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $impuesto->fetch();
    }

    public function metMostrarPartida($codPartida)
    {
        $impuesto = $this->_db->query("
            SELECT
           pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria
            FROM
              pr_b002_partida_presupuestaria
            WHERE
              pr_b002_partida_presupuestaria.cod_partida='$codPartida'
        ");
        $impuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $impuesto->fetch();
    }

    public function metCargarPais($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a008_pais
              SET
                num_estatus=1,ind_pais=:ind_pais, pk_num_pais=:pk_num_pais
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a008_pais', false, "pk_num_pais = '" . $array['idpaises'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_pais' => $array['idpaises'],
                    ':ind_pais' => $array['nombre']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarEstado($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a009_estado
              SET
                num_estatus=1,fk_a008_num_pais=:fk_a008_num_pais, ind_estado=:ind_estado, pk_num_estado=:pk_num_estado
            ");

        foreach ($arrays as $array) {
            if ($array['idPais'] == '233') {
                $busqueda = $this->metBusquedaSimple('a009_estado', false, "pk_num_estado = '" . $array['idestados'] . "'");
                if (!$busqueda) {
                    $registro->execute(array(
                        ':pk_num_estado' => $array['idestados'],
                        ':ind_estado' => $array['estados'],
                        ':fk_a008_num_pais' => $array['idPais']
                    ));
                }
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarCiudad($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a010_ciudad
              SET
                num_estatus=1, ind_ciudad=:ind_ciudad,
                pk_num_ciudad=:pk_num_ciudad, ind_cod_postal=0, fk_a009_num_estado=:fk_a009_num_estado
            ");

        foreach ($arrays as $array) {
            if ($array['idmunicipio'] == 210) {
                $busqueda = $this->metBusquedaSimple('a010_ciudad', false, "pk_num_ciudad = '1'");
                if (!$busqueda) {
                    $registro->execute(array(
                        ':pk_num_ciudad' => 1,
                        ':ind_ciudad' => $array['municipio'],
                        ':fk_a009_num_estado' => $array['estadoestados']
                    ));
                }
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarMunicipio($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a011_municipio
              SET
                num_estatus=1,fk_a009_num_estado=:fk_a009_num_estado, ind_municipio=:ind_municipio,
                pk_num_municipio=:pk_num_municipio
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a011_municipio', false, "pk_num_municipio = '" . $array['idmunicipio'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_municipio' => $array['idmunicipio'],
                    ':ind_municipio' => $array['municipio'],
                    ':fk_a009_num_estado' => $array['estadoestados']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarParroquia($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a012_parroquia
              SET
                num_estatus=1, fk_a011_num_municipio=:fk_a011_num_municipio, ind_parroquia=:ind_parroquia,
                pk_num_parroquia=:pk_num_parroquia
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a012_parroquia', false, "pk_num_parroquia = '" . $array['idparroquia'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_parroquia' => $array['idparroquia'],
                    ':ind_parroquia' => $array['parroquia'],
                    ':fk_a011_num_municipio' => $array['municipiomunicipio']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarPersona($arrays)
    {
        $this->_db->query(
            "SET FOREIGN_KEY_CHECKS=0;"
        );
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a003_persona
              SET
                fk_a010_num_ciudad=:fk_a010_num_ciudad, ind_cedula_documento=:ind_cedula_documento,
                ind_documento_fiscal=:ind_documento_fiscal, num_estatus=1, ind_nombre1=:ind_nombre1, ind_tipo_persona=:ind_tipo_persona,
                pk_num_persona=:pk_num_persona, ind_apellido1=:ind_apellido1
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a003_persona', false, "pk_num_persona = '" . $array['pk_num_persona'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_persona' => $array['pk_num_persona'],
                    ':fk_a010_num_ciudad' => $array['fk_a010_num_ciudad'],
                    ':ind_cedula_documento' => $array['ind_cedula_documento'],
                    ':ind_documento_fiscal' => $array['ind_documento_fiscal'],
                    ':ind_nombre1' => $array['ind_nombre1'],
                    ':ind_apellido1' => $array['ind_apellido1'],
                    ':ind_tipo_persona' => $array['ind_tipo_persona']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarEmpleado($arrays)
    {
        $this->_db->beginTransaction();
        $registroEmpleado = $this->_db->prepare(
            "INSERT INTO
                rh_b001_empleado
              SET
                fec_ultima_modificacion=NOW(), fk_a003_num_persona=:fk_a003_num_persona, fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                ind_estado_aprobacion='ap', num_estatus=1, pk_num_empleado=:pk_num_empleado
            ");

        $registroUsuario = $this->_db->prepare(
            "INSERT INTO
                a018_seguridad_usuario
              SET
                fec_ultima_modificacion=NOW(), fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                num_estatus=1, fk_rhb001_num_empleado=:fk_rhb001_num_empleado, ind_password=:ind_password,
                ind_template=:ind_template,pk_num_seguridad_usuario=:pk_num_seguridad_usuario,ind_usuario=:ind_usuario
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('rh_b001_empleado', false, "pk_num_empleado = '" . $array['pk_num_empleado'] . "'");
            if (!$busqueda) {
                $registroEmpleado->execute(array(
                    ':pk_num_empleado' => $array['pk_num_empleado'],
                    ':fk_a003_num_persona' => $array['fk_a003_num_persona'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                ));
                $registroUsuario->execute(array(
                    ':pk_num_seguridad_usuario' => $array['pk_num_seguridad_usuario'],
                    ':fk_rhb001_num_empleado' => $array['fk_rhb001_num_empleado'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':ind_password' => $array['ind_password'],
                    ':ind_usuario' => $array['ind_usuario'],
                    ':ind_template' => $array['ind_template']
                ));
            }
        }
        $error = $registroUsuario->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            exit(var_dump($error));
            echo '<br><br>';
            $this->_db->rollBack();
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
        }
        $this->_db->query(
            "SET FOREIGN_KEY_CHECKS=1;"
        );

    }

    public function metCargarAplicacion($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a015_seguridad_aplicacion
              SET
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario, fec_creacion=NOW(), cod_aplicacion=:cod_aplicacion,
                ind_descripcion=:ind_descripcion, ind_nombre_modulo=:ind_nombre_modulo, num_estatus=1,
                pk_num_seguridad_aplicacion=:pk_num_seguridad_aplicacion

            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a015_seguridad_aplicacion', false, "pk_num_seguridad_aplicacion = '" . $array['pk_num_seguridad_aplicacion'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_seguridad_aplicacion' => $array['pk_num_seguridad_aplicacion'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':cod_aplicacion' => $array['cod_aplicacion'],
                    ':ind_descripcion' => mb_strtoupper($array['ind_descripcion'], 'utf-8'),
                    ':ind_nombre_modulo' => mb_strtoupper($array['ind_nombre_modulo'], 'utf-8')
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCargarPerfil($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a017_seguridad_perfil
              SET
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario, ind_descripcion=:ind_descripcion,
                fec_ultima_modificacion=NOW(), num_estatus=1, ind_nombre_perfil=:ind_nombre_perfil,
                pk_num_seguridad_perfil=:pk_num_seguridad_perfil
        ");

        $registroPerfil = $this->_db->prepare(
            "INSERT INTO
                a020_seguridad_usuarioperfil
              SET
                fk_a018_num_seguridad_usuario=1, fk_a017_num_seguridad_perfil=:fk_a017_num_seguridad_perfil
        ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a017_seguridad_perfil', false, "pk_num_seguridad_perfil = '" . $array['pk_num_seguridad_perfil'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_seguridad_perfil' => $array['pk_num_seguridad_perfil'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':ind_nombre_perfil' => $array['ind_nombre_perfil']
                ));
                $registroPerfil->execute(array(
                    ':fk_a017_num_seguridad_perfil' => $array['pk_num_seguridad_perfil']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCargarMenu($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a027_seguridad_menu
              SET
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario, ind_descripcion=:ind_descripcion,
                fec_ultima_modificacion=NOW(), num_estatus=1, cod_interno=:cod_interno, cod_padre=:cod_padre,
                fk_a015_num_seguridad_aplicacion=:fk_a015_num_seguridad_aplicacion, ind_icono=:ind_icono, ind_nombre=:ind_nombre,
                ind_rol=:ind_rol, ind_ruta=:ind_ruta, num_nivel=:num_nivel
            ");
        $registroMenuPerfil = $this->_db->prepare(
            "INSERT INTO
                a028_seguridad_menupermiso
              SET
                fk_a017_num_seguridad_perfil=1, fk_a027_num_seguridad_menu=:fk_a027_num_seguridad_menu
            ");
        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a027_seguridad_menu', false, "cod_interno = '" . $array['cod_interno'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':num_nivel' => $array['num_nivel'],
                    ':ind_ruta' => $array['ind_ruta'],
                    ':ind_rol' => $array['ind_rol'],
                    ':ind_nombre' => mb_strtoupper($array['ind_nombre'], 'utf-8'),
                    ':ind_icono' => $array['ind_icono'],
                    ':fk_a015_num_seguridad_aplicacion' => $array['fk_a015_num_seguridad_aplicacion'],
                    ':cod_padre' => mb_strtoupper($array['cod_padre'], 'utf-8'),
                    ':cod_interno' => mb_strtoupper($array['cod_interno'], 'utf-8'),
                    ':ind_descripcion' => mb_strtoupper($array['ind_descripcion'], 'utf-8'),
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                ));
                $idMenu = $this->_db->lastInsertId();
                $registroMenuPerfil->execute(array(
                    ':fk_a027_num_seguridad_menu' => $idMenu
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCargarMiscelaneos($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a005_miscelaneo_maestro
              SET
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario, ind_descripcion=:ind_descripcion,
                fec_ultima_modificacion=NOW(), num_estatus=1, cod_maestro=:cod_maestro,
                fk_a015_num_seguridad_aplicacion=:fk_a015_num_seguridad_aplicacion, ind_nombre_maestro=:ind_nombre_maestro
            ");

        $registroDetalle = $this->_db->prepare(
            "INSERT INTO
                a006_miscelaneo_detalle
              SET
                cod_detalle=:cod_detalle, fk_a005_num_miscelaneo_maestro=:fk_a005_num_miscelaneo_maestro, num_estatus=1,
                ind_nombre_detalle=:ind_nombre_detalle
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a005_miscelaneo_maestro', false, "cod_maestro = '" . $array['cod_maestro'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':cod_maestro' => $array['cod_maestro'],
                    ':fk_a015_num_seguridad_aplicacion' => $array['fk_a015_num_seguridad_aplicacion'],
                    ':ind_nombre_maestro' => $array['ind_nombre_maestro']
                ));
                $idMiscelanneo = $this->_db->lastInsertId();
                foreach ($array['Det'] as $arrayDet) {
                    $registroDetalle->execute(array(
                        ':fk_a005_num_miscelaneo_maestro' => $idMiscelanneo,
                        ':cod_detalle' => $arrayDet['cod_detalle'],
                        ':ind_nombre_detalle' => $arrayDet['ind_nombre_detalle']
                    ));
                }
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metMostrarSelect($codMaestro, $codDetalle)
    {
        $miscelaneoDetalle = $this->_db->query("
            SELECT
              a006.pk_num_miscelaneo_detalle
            FROM
              a006_miscelaneo_detalle AS a006
            INNER JOIN
              a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
            WHERE
              a005.cod_maestro='$codMaestro' AND
              a006.cod_detalle='$codDetalle'
        ");
        $miscelaneoDetalle->setFetchMode(PDO::FETCH_ASSOC);
        return $miscelaneoDetalle->fetch();
    }

    public function metCargarParametros($arrays)
    {
        $this->_db->beginTransaction();

        $registro = $this->_db->prepare(
            "INSERT INTO
                a035_parametros
              SET
                ind_descripcion=:ind_descripcion,ind_explicacion=:ind_explicacion,
                ind_parametro_clave=:ind_parametro_clave, ind_valor_parametro=:ind_valor_parametro, num_estatus=:num_estatus, fec_ultima_modificacion=:fec_ultima_modificacion,
                fk_a015_num_seguridad_aplicacion=:fk_a015_num_seguridad_aplicacion, fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fk_a006_num_miscelaneo_detalle=:fk_a006_num_miscelaneo_detalle
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a035_parametros', false, "ind_parametro_clave = '" . $array['ind_parametro_clave'] . "'");
            if (!$busqueda) {
                $idMiscelaneosDet = $this->metMostrarSelect('TPPARAMET', $array['cod_detalle']);
                $registro->execute(array(
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':ind_explicacion' => $array['ind_explicacion'],
                    ':ind_parametro_clave' => $array['ind_parametro_clave'],
                    ':ind_valor_parametro' => $array['ind_valor_parametro'],
                    ':num_estatus' => 1,
                    ':fec_ultima_modificacion' => date('Y-m-d'),
                    ':fk_a015_num_seguridad_aplicacion' => $array['fk_a015_num_seguridad_aplicacion'],
                    ':fk_a018_num_seguridad_usuario' => 1,
                    ':fk_a006_num_miscelaneo_detalle' => $idMiscelaneosDet['pk_num_miscelaneo_detalle']
                ));
            }

        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    #### NOMINA
    public function metNominaTipoProceso($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                nm_b003_tipo_proceso
              SET
                num_estatus=1, cod_proceso=:cod_proceso, fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                ind_nombre_proceso=:ind_nombre_proceso, num_flag_adelanto=:num_flag_adelanto
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('nm_b003_tipo_proceso', false, "cod_proceso = '" . $array['cod_proceso'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':cod_proceso' => $array['cod_proceso'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':num_flag_adelanto' => $array['num_flag_adelanto'],
                    ':ind_nombre_proceso' => $array['ind_nombre_proceso']
                ));
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metNominaConceptos($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                nm_b002_concepto
              SET
                num_estatus=1,
                fec_ultima_moficacion=NOW(),
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                cod_concepto=:cod_concepto,
                ind_abrebiatura=:ind_abrebiatura,
                ind_descripcion=:ind_descripcion,
                ind_formula=:ind_formula,
                ind_impresion=:ind_impresion,
                num_flag_automatico=:num_flag_automatico,
                num_flag_bono=:num_flag_bono,
                num_flag_incidencia=:num_flag_incidencia,
                num_orden_planilla=:num_orden_planilla,
                fk_a006_num_tipo_concepto=:fk_a006_num_tipo_concepto
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('nm_b002_concepto', false, "cod_concepto = '" . $array['cod_concepto'] . "'");
            if (!$busqueda) {
                $idMiscelaneosDet = $this->metMostrarSelect('TDCNM', $array['cod_detalle']);
                $registro->execute(array(
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':cod_concepto' => $array['cod_concepto'],
                    ':ind_abrebiatura' => $array['ind_abrebiatura'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':ind_formula' => $array['ind_formula'],
                    ':ind_impresion' => $array['ind_impresion'],
                    ':num_flag_automatico' => $array['num_flag_automatico'],
                    ':num_flag_bono' => $array['num_flag_bono'],
                    ':num_flag_incidencia' => $array['num_flag_incidencia'],
                    ':num_orden_planilla' => $array['num_orden_planilla'],
                    ':fk_a006_num_tipo_concepto' => $idMiscelaneosDet['pk_num_miscelaneo_detalle']
                ));
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #### PRESUPUESTO
    public function metPresupuestoPartidasPresupuestarias($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                pr_b002_partida_presupuestaria
              SET
                fk_prb001_num_tipo_cuenta=:fk_prb001_num_tipo_cuenta,
                cod_partida=:cod_partida,
                ind_partida=:ind_partida,
                ind_generica=:ind_generica,
                ind_especifica=:ind_especifica,
                ind_subespecifica=:ind_subespecifica,
                ind_denominacion=:ind_denominacion,
                num_estatus=:num_estatus,
                ind_tipo=:ind_tipo,
                num_nivel=:num_nivel,
                fk_cbb004_num_plan_cuenta_onco=:fk_cbb004_num_plan_cuenta_onco,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=:fec_ultima_modificacion,
                ind_numeral=:ind_numeral
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('pr_b002_partida_presupuestaria', false, "cod_partida = '" . $array['cod_partida'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    'fk_prb001_num_tipo_cuenta' => $array['fk_prb001_num_tipo_cuenta'],
                    'cod_partida' => $array['cod_partida'],
                    'ind_partida' => $array['ind_partida'],
                    'ind_generica' => $array['ind_generica'],
                    'ind_especifica' => $array['ind_especifica'],
                    'ind_subespecifica' => $array['ind_subespecifica'],
                    'ind_denominacion' => $array['ind_denominacion'],
                    'num_estatus' => $array['num_estatus'],
                    'ind_tipo' => $array['ind_tipo'],
                    'num_nivel' => $array['num_nivel'],
                    'fk_cbb004_num_plan_cuenta_onco' => $array['fk_cbb004_num_plan_cuenta'],
                    'fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    'fec_ultima_modificacion' => $array['fec_ultima_modificacion'],
                    'ind_numeral' => $array['ind_numeral']
                ));
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #### PRESUPUESTO
    public function metCuentasContables($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                cb_b004_plan_cuenta
              SET
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                cod_cuenta=:cod_cuenta,
                ind_grupo=:ind_grupo,
                ind_subgrupo=:ind_subgrupo,
                ind_rubro=:ind_rubro,
                ind_cuenta=:ind_cuenta,
                ind_sub_cuenta1=:ind_sub_cuenta1,
                ind_sub_cuenta2=:ind_sub_cuenta2,
                ind_sub_cuenta3=:ind_sub_cuenta3,
                ind_descripcion=:ind_descripcion,
                num_nivel=:num_nivel,
                num_flag_tipo=:num_flag_tipo,
                ind_tipo_saldo=:ind_tipo_saldo,
                ind_naturaleza=:ind_naturaleza,
                ind_estado=:ind_estado

            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cb_b004_plan_cuenta', false, "cod_cuenta = '" . $array['cod_cuenta'] . "'");
            if (!$busqueda) {
                $idMiscelaneosDet = $this->metMostrarSelect('TCTA', $array['cod_detalle']);
                $registro->execute(array(
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':cod_cuenta' => $array['cod_cuenta'],
                    ':ind_grupo' => $array['ind_grupo'],
                    ':ind_subgrupo' => $array['ind_subgrupo'],
                    ':ind_rubro' => $array['ind_rubro'],
                    ':ind_cuenta' => $array['ind_cuenta'],
                    ':ind_sub_cuenta1' => $array['ind_sub_cuenta1'],
                    ':ind_sub_cuenta2' => $array['ind_sub_cuenta2'],
                    ':ind_sub_cuenta3' => $array['ind_sub_cuenta3'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':num_nivel' => $array['num_nivel'],
                    ':num_flag_tipo' => $array['num_flag_tipo'],
                    //  ':fk_a006_num_miscelaneo_detalle' => $idMiscelaneosDet['pk_num_miscelaneo_detalle'],
                    ':ind_tipo_saldo' => $array['ind_tipo_saldo'],
                    ':ind_naturaleza' => $array['ind_naturaleza'],
                    ':ind_estado' => $array['ind_estado']
                ));
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #### CxP
    public function metCxPImpuesto($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                cp_b015_impuesto
              SET
                num_estatus=1,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                cod_impuesto=:cod_impuesto,
                txt_descripcion=:txt_descripcion,
                ind_signo=:ind_signo,
                num_factor_porcentaje=:num_factor_porcentaje,
                fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
                fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20,
                fk_a006_num_miscelaneo_detalle_regimen_fiscal=:fk_a006_num_miscelaneo_detalle_regimen_fiscal,
                fk_a006_num_miscelaneo_detalle_flag_provision=:fk_a006_num_miscelaneo_detalle_flag_provision,
                fk_a006_num_miscelaneo_detalle_flag_imponible=:fk_a006_num_miscelaneo_detalle_flag_imponible,
                fk_a006_num_miscelaneo_detalle_tipo_comprobante=:fk_a006_num_miscelaneo_detalle_tipo_comprobante,
                fk_a006_num_miscelaneo_detalle_flag_general=:fk_a006_num_miscelaneo_detalle_flag_general
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cp_b015_impuesto', false, "cod_impuesto = '" . $array['cod_impuesto'] . "'");
            if (!$busqueda) {
                $idMiscelaneosDetRegimen = $this->metMostrarSelect('REGFISCAL', $array['cod_detalle_Regimen']);
                $idMiscelaneosDetProvision = $this->metMostrarSelect('PROVDOC', $array['cod_detalle_Provision']);
                $idMiscelaneosDetImponible = $this->metMostrarSelect('IVAIMP', $array['cod_detalle_Imponible']);
                $idMiscelaneosDetComprob = $this->metMostrarSelect('COMPIMP', $array['cod_detalle_Comprob']);
                $idMiscelaneosDetGeneral = $this->metMostrarSelect('CLASIMP', $array['cod_detalle_General']);
                $idCuenta = $this->metMostrarCuenta($array['plan_cuenta']);
                $idCuenta20 = $this->metMostrarCuenta($array['plan_cuenta_pub20']);
                $registro->execute(array(
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':cod_impuesto' => $array['cod_impuesto'],
                    ':txt_descripcion' => $array['txt_descripcion'],
                    ':ind_signo' => $array['ind_signo'],
                    ':num_factor_porcentaje' => $array['num_factor_porcentaje'],
                    ':fk_cbb004_num_plan_cuenta' => $idCuenta['pk_num_cuenta'],
                    ':fk_cbb004_num_plan_cuenta_pub20' => $idCuenta20['pk_num_cuenta'],
                    ':fk_a006_num_miscelaneo_detalle_regimen_fiscal' => $idMiscelaneosDetRegimen['pk_num_miscelaneo_detalle'],
                    ':fk_a006_num_miscelaneo_detalle_flag_provision' => $idMiscelaneosDetProvision['pk_num_miscelaneo_detalle'],
                    ':fk_a006_num_miscelaneo_detalle_flag_imponible' => $idMiscelaneosDetImponible['pk_num_miscelaneo_detalle'],
                    ':fk_a006_num_miscelaneo_detalle_tipo_comprobante' => $idMiscelaneosDetComprob['pk_num_miscelaneo_detalle'],
                    ':fk_a006_num_miscelaneo_detalle_flag_general' => $idMiscelaneosDetGeneral['pk_num_miscelaneo_detalle'],
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCxPTipoServicios($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                cp_b017_tipo_servicio
              SET
                cod_tipo_servicio=:cod_tipo_servicio,
                ind_descripcion=:ind_descripcion,
                fk_a006_num_miscelaneo_regimen_fiscal=:fk_a006_num_miscelaneo_regimen_fiscal,
                num_estatus=1
            ");

        $registroDetalle = $this->_db->prepare(
            "INSERT INTO
                cp_b020_servicio_impuesto
              SET
                fk_cpb015_num_impuesto=:fk_cpb015_num_impuesto,
                fk_cpb017_num_tipo_servico=:fk_cpb017_num_tipo_servico
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cp_b017_tipo_servicio', false, "cod_tipo_servicio = '" . $array['cod_tipo_servicio'] . "'");
            if (!$busqueda) {
                $idMiscelaneosDet = $this->metMostrarSelect('REGFISCAL', $array['cod_detalle']);
                $registro->execute(array(
                    ':cod_tipo_servicio' => $array['cod_tipo_servicio'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':fk_a006_num_miscelaneo_regimen_fiscal' => $idMiscelaneosDet['pk_num_miscelaneo_detalle']
                ));
                $idServicio = $this->_db->lastInsertId();

                if (isset($array['Det'])) {
                    foreach ($array['Det'] as $arrayDet) {
                        $idImpuesto = $this->metMostrarSelectImpuesto($arrayDet['impuesto']);

                        $registroDetalle->execute(array(
                            ':fk_cpb015_num_impuesto' => $idImpuesto['pk_num_impuesto'],
                            ':fk_cpb017_num_tipo_servico' => $idServicio
                        ));
                    }
                }
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCxPDocumentos($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                cp_b002_tipo_documento
              SET
                num_estatus=1,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=1,
                cod_tipo_documento=:cod_tipo_documento,
                cod_fiscal=:cod_fiscal,
                ind_descripcion=:ind_descripcion,
                fk_a006_num_miscelaneo_detalle_clasificacion=:fk_a006_num_miscelaneo_detalle_clasificacion,
                fk_cbb001_num_voucher=:fk_cbb001_num_voucher,
                num_flag_provision=:num_flag_provision,
                fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
                fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20,
                num_flag_adelanto=:num_flag_adelanto,
                 fk_cbb004_num_plan_cuenta_ade=:fk_cbb004_num_plan_cuenta_ade,
                fk_cbb004_num_plan_cuenta_ade_pub20=:fk_cbb004_num_plan_cuenta_ade_pub20,
                num_flag_fiscal=:num_flag_fiscal,
                num_flag_monto_negativo=:num_flag_monto_negativo,
                fk_a006_num_miscelaneo_detalle_regimen_fiscal=:fk_a006_num_miscelaneo_detalle_regimen_fiscal

            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cp_b002_tipo_documento', false, "cod_tipo_documento = '" . $array['cod_tipo_documento'] . "'");
            if (!$busqueda) {
                $idMiscelaneosDetClasif = $this->metMostrarSelect('CLASDOC', $array['cod_detalle_clasif']);
                $idMiscelaneosDetRegimen = $this->metMostrarSelect('REGFISCAL', $array['cod_detalle_regimen']);
                $idCuenta = $this->metMostrarCuenta($array['plan_cuenta']);
                $idCuenta20 = $this->metMostrarCuenta($array['plan_cuenta_pub20']);
                $idCuentaAde = $this->metMostrarCuenta($array['plan_cuenta_ade']);
                $idCuentaAde20 = $this->metMostrarCuenta($array['plan_cuenta_ade_pub20']);

                $registro->execute(array(
                    ':cod_tipo_documento' => $array['cod_tipo_documento'],
                    ':cod_fiscal' => $array['cod_fiscal'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':fk_a006_num_miscelaneo_detalle_clasificacion' => $idMiscelaneosDetClasif['pk_num_miscelaneo_detalle'],
                    ':fk_cbb001_num_voucher' => $array['fk_cbb001_num_voucher'],
                    ':num_flag_provision' => $array['num_flag_provision'],
                    ':fk_cbb004_num_plan_cuenta' => $idCuenta['pk_num_cuenta'],
                    ':fk_cbb004_num_plan_cuenta_pub20' => $idCuenta20['pk_num_cuenta'],
                    ':num_flag_adelanto' => $array['num_flag_adelanto'],
                    ':fk_cbb004_num_plan_cuenta_ade' => $idCuentaAde['pk_num_cuenta'],
                    ':fk_cbb004_num_plan_cuenta_ade_pub20' => $idCuentaAde20['pk_num_cuenta'],
                    ':num_flag_fiscal' => $array['num_flag_fiscal'],
                    ':num_flag_monto_negativo' => $array['num_flag_monto_negativo'],
                    ':fk_a006_num_miscelaneo_detalle_regimen_fiscal' => $idMiscelaneosDetRegimen['pk_num_miscelaneo_detalle']
                ));
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCxPClasificacion($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                cp_b004_clasificacion_gastos
              SET
                num_estatus=1,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                cod_clasificacion=:cod_clasificacion,
                ind_descripcion=:ind_descripcion,
                fk_a006_num_miscelaneo_detalle_aplicacion=:fk_a006_num_miscelaneo_detalle_aplicacion


            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cp_b004_clasificacion_gastos', false, "cod_clasificacion = '" . $array['cod_clasificacion'] . "'");
            if (!$busqueda) {
                $idMiscelaneosDet = $this->metMostrarSelect('APLICG', $array['cod_detalle']);

                $registro->execute(array(
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':cod_clasificacion' => $array['cod_clasificacion'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':fk_a006_num_miscelaneo_detalle_aplicacion' => $idMiscelaneosDet['pk_num_miscelaneo_detalle']
                ));
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCxPConcepto($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                cp_b005_concepto_gasto
              SET
                num_estatus=1,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=1,

                cod_concepto_gasto=:cod_concepto_gasto,
                ind_descripcion=:ind_descripcion,
                fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
                fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo=:fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo

            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cp_b005_concepto_gasto', false, "cod_concepto_gasto = '" . $array['cod_concepto_gasto'] . "'");
            if (!$busqueda) {
                $idPartida = $this->metMostrarPartida($array['partida_presupuestaria']);
                $idCuenta = $this->metMostrarCuenta($array['plan_cuenta']);
                $idMiscelaneosDet = $this->metMostrarSelect('GRUPGAST', $array['cod_detalle']);

                $registro->execute(array(
                    ':cod_concepto_gasto' => $array['cod_concepto_gasto'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':fk_prb002_num_partida_presupuestaria' => $idPartida['pk_num_partida_presupuestaria'],
                    ':fk_cbb004_num_plan_cuenta' => $idCuenta['pk_num_cuenta'],
                    ':fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo' => $idMiscelaneosDet['pk_num_miscelaneo_detalle']
                ));
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCxPCuentaBancaria($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                cp_b014_cuenta_bancaria
              SET
                num_estatus=1,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=1,

                fk_a006_num_miscelaneo_detalle_banco=:fk_a006_num_miscelaneo_detalle_banco,
                ind_num_cuenta=:ind_num_cuenta,
                ind_descripcion=:ind_descripcion,
                fk_a006_num_miscelaneo_detalle_tipo_cuenta=:fk_a006_num_miscelaneo_detalle_tipo_cuenta,
                fec_apertura=:fec_apertura,
                fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
                fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20,
                ind_agencia=:ind_agencia,
                ind_distrito=:ind_distrito,
                ind_atencion=:ind_atencion,
                ind_cargo=:ind_cargo,
                num_flag_conciliacion_bancaria=:num_flag_conciliacion_bancaria,
                num_flag_debito_bancario=:num_flag_debito_bancario,
                num_flag_conciliacion_cp=:num_flag_conciliacion_cp

            ");

        $registroDetalle = $this->_db->prepare(
            "INSERT INTO
                cp_b019_cuenta_bancaria_tipo_pago
              SET
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=1,

                fk_cpb014_num_cuenta_bancaria=:fk_cpb014_num_cuenta_bancaria,
                fk_a006_num_miscelaneo_detalle_tipo_pago=:fk_a006_num_miscelaneo_detalle_tipo_pago,
                int_numero_ultimo_cheque=:int_numero_ultimo_cheque

            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cp_b014_cuenta_bancaria', false, "ind_num_cuenta = '" . $array['ind_num_cuenta'] . "'");
            if (!$busqueda) {
                $idCuenta = $this->metMostrarCuenta($array['plan_cuenta']);
                $idCuenta20 = $this->metMostrarCuenta($array['plan_cuenta_pub20']);
                $idMiscelaneosCuenta = $this->metMostrarSelect('BANCOS', $array['cod_detalle_banco']);
                $idMiscelaneosTipoC = $this->metMostrarSelect('TIPCUENTAB', $array['cod_detalle_tipo_cuenta']);

                $registro->execute(array(
                    ':ind_num_cuenta' => $array['ind_num_cuenta'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':fec_apertura' => $array['fec_apertura'],
                    ':ind_agencia' => $array['ind_agencia'],
                    ':ind_distrito' => $array['ind_distrito'],
                    ':ind_atencion' => $array['ind_atencion'],
                    ':ind_cargo' => $array['ind_cargo'],
                    ':num_flag_conciliacion_bancaria' => $array['num_flag_conciliacion_bancaria'],
                    ':num_flag_debito_bancario' => $array['num_flag_debito_bancario'],
                    ':num_flag_conciliacion_cp' => $array['num_flag_conciliacion_cp'],
                    ':fk_cbb004_num_plan_cuenta' => $idCuenta['pk_num_cuenta'],
                    ':fk_cbb004_num_plan_cuenta_pub20' => $idCuenta20['pk_num_cuenta'],
                    ':fk_a006_num_miscelaneo_detalle_banco' => $idMiscelaneosCuenta['pk_num_miscelaneo_detalle'],
                    ':fk_a006_num_miscelaneo_detalle_tipo_cuenta' => $idMiscelaneosTipoC['pk_num_miscelaneo_detalle'],
                ));
                $idCuentaBancaria = $this->_db->lastInsertId();

                if (isset($array['Det'])) {
                    foreach ($array['Det'] as $arrayDet) {
                        $idMiscelaneosTipoPago = $this->metMostrarSelect('TDPLG', $array['cod_detalle']);

                        $registroDetalle->execute(array(
                            ':fk_cpb014_num_cuenta_bancaria' => $idCuentaBancaria,
                            ':fk_a006_num_miscelaneo_detalle_tipo_pago' => $idMiscelaneosTipoPago['pk_num_miscelaneo_detalle'],
                            ':int_numero_ultimo_cheque' => $arrayDet['int_numero_ultimo_cheque']
                        ));
                    }
                }
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCxPtipoTransaccion($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                cp_b006_banco_tipo_transaccion
              SET
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=1,

                num_estatus=:num_estatus,
                cod_tipo_transaccion=:cod_tipo_transaccion,
                ind_descripcion=:ind_descripcion,
                fk_a006_miscelaneo_detalle_tipo_transaccion=:fk_a006_miscelaneo_detalle_tipo_transaccion,
                num_flag_voucher=:num_flag_voucher,
                num_flag_transaccion=:num_flag_transaccion,
                num_flag_transaccion_planilla=:num_flag_transaccion_planilla,
                fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta

            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cp_b006_banco_tipo_transaccion', false, "cod_tipo_transaccion = '" . $array['cod_tipo_transaccion'] . "'");
            if (!$busqueda) {
                $idCuenta = $this->metMostrarCuenta($array['plan_cuenta']);
                $idMiscelaneosDet = $this->metMostrarSelect('TIPOTRANS', $array['cod_detalle']);

                $registro->execute(array(
                    ':num_estatus' => $array['num_estatus'],
                    ':cod_tipo_transaccion' => $array['cod_tipo_transaccion'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':fk_a006_miscelaneo_detalle_tipo_transaccion' => $idMiscelaneosDet['pk_num_miscelaneo_detalle'],
                    ':num_flag_voucher' => $array['num_flag_voucher'],
                    ':num_flag_transaccion' => $array['num_flag_transaccion'],
                    ':num_flag_transaccion_planilla' => $array['num_flag_transaccion_planilla'],
                    ':fk_cbb004_num_plan_cuenta' => $idCuenta['pk_num_cuenta']
                ));
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            exit(var_dump($error));
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }
}
