<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        17-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once RUTA_MODELO.'estadoModelo.php';
class municipioModelo extends estadoModelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarMunicipio($idMunicipio)
    {
        $municipio = $this->_db->query("
            SELECT
              *
            FROM
              a011_municipio
            INNER JOIN a009_estado on a009_estado.pk_num_estado = a011_municipio.fk_a009_num_estado
            INNER JOIN a008_pais ON a008_pais.pk_num_pais = a009_estado.fk_a008_num_pais
            WHERE a011_municipio.pk_num_municipio='$idMunicipio'
        ");
        $municipio->setFetchMode(PDO::FETCH_ASSOC);

        return $municipio->fetch();
    }

    public function metListarMunicipio()
    {
        $municipio = $this->_db->query(
            "SELECT
              *
            FROM
              a011_municipio
            INNER JOIN a009_estado ON a009_estado.pk_num_estado = a011_municipio.fk_a009_num_estado
            INNER JOIN a008_pais ON a008_pais.pk_num_pais = a009_estado.fk_a008_num_pais
            ");
        $municipio->setFetchMode(PDO::FETCH_ASSOC);
        return $municipio->fetchAll();
    }

    public function metCrearMunicipio($municipio,$idEstado,$status)
    {
        $this->_db->beginTransaction();
        $registroMunicipio=$this->_db->prepare(
            "INSERT INTO
              a011_municipio
            SET
              num_estatus=:num_estatus, fk_a009_num_estado=:fk_a009_num_estado, ind_municipio=:ind_municipio
            "
        );
        $registroMunicipio->execute(array(
            ':ind_municipio' => $municipio,
            ':fk_a009_num_estado' => $idEstado,
            ':num_estatus' => $status
        ));
        $idMunicipio=$this->_db->lastInsertId();
        $error = $registroMunicipio->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idMunicipio;
        }
    }

    public function metModificarMunicipio($municipio,$idEstado,$status,$idMunicipio)
    {
        $this->_db->beginTransaction();
        $registroMunicipio=$this->_db->prepare(
            "UPDATE
              a011_municipio
            SET
              num_estatus=:num_estatus, fk_a009_num_estado=:fk_a009_num_estado, ind_municipio=:ind_municipio
            WHERE
              pk_num_municipio='$idMunicipio'
            "
        );
        $registroMunicipio->execute(array(
            ':ind_municipio' => $municipio,
            ':fk_a009_num_estado' => $idEstado,
            ':num_estatus' => $status
        ));
        $error = $registroMunicipio->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idMunicipio;
        }
    }

    public function metJsonMunicipio($idEstado)
    {
        $municipio = $this->_db->query(
            "SELECT
              *
            FROM
              a011_municipio
            WHERE
              fk_a009_num_estado='$idEstado' AND num_estatus=1
            ");
        $municipio->setFetchMode(PDO::FETCH_ASSOC);
        return $municipio->fetchAll();
    }

}
