<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class seguridadAlternaModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metListarUsuarios($idUsuario = false)
    {
        if($idUsuario){
            $idUsuario = " AND a018_seguridad_usuario.pk_num_seguridad_usuario='$idUsuario'";
        }

        $menu = $this->_db->query("
            SELECT 
              * 
            FROM 
              a018_seguridad_usuario 
              INNER JOIN rh_b001_empleado ON a018_seguridad_usuario.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
              INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
            WHERE 
              a018_seguridad_usuario.num_estatus = '1' $idUsuario
          ");
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
    }

    public function metListaDetalle($idUsuario = false)
    {
        $seguridad = $this->_db->query(
            "SELECT * FROM a019_seguridad_dependencia WHERE fk_a018_num_seguridad_usuario = '$idUsuario' "
        );
        $seguridad->setFetchMode(PDO::FETCH_ASSOC);
        return $seguridad->fetchAll();
    }

    #PERMITE MOSTRAR TODOS LAS DEPENDECIAS
    public function metConsultarDependencias($estatus = true)
    {
        if($estatus){
            $filtro = 'WHERE a004_dependencia.num_estatus=1';
        }else{
            $filtro = '';
        }

        $con = $this->_db->query("
            SELECT
            a004_dependencia.pk_num_dependencia,
            a004_dependencia.ind_dependencia,
            a004_dependencia.ind_codinterno,
            a004_dependencia.ind_codpadre,
            a004_dependencia.ind_nivel,
            a004_dependencia.num_flag_controlfiscal,
            a004_dependencia.num_flag_principal,
            a004_dependencia.num_piso,
            a004_dependencia.num_estatus,
            a004_dependencia.fk_a001_num_organismo,
            a004_dependencia.fk_a003_num_persona_responsable,
            a004_dependencia.num_flag_responsable,
            a004_dependencia.txt_abreviacion,
            a003_persona.ind_nombre1,
            a003_persona.ind_nombre2,
            a003_persona.ind_apellido1,
            a003_persona.ind_apellido2,
            a001_organismo.ind_descripcion_empresa
            FROM
            a004_dependencia
            LEFT JOIN a003_persona ON a003_persona.pk_num_persona = a004_dependencia.fk_a003_num_persona_responsable
            LEFT JOIN rh_b001_empleado ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
            INNER JOIN a001_organismo ON a001_organismo.pk_num_organismo = a004_dependencia.fk_a001_num_organismo
            $filtro
            ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    public function metListarAplicacion()
    {
        $menu = $this->_db->query("SELECT * FROM a015_seguridad_aplicacion WHERE num_estatus='1'");
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
    }

    public function metEliminar($fk_a018_num_seguridad_usuario)
    {
        $eliminarConceptoDetalle = $this->_db->query(
            "DELETE FROM a019_seguridad_dependencia WHERE fk_a018_num_seguridad_usuario='$fk_a018_num_seguridad_usuario'"
        );
    }

    public function metRegistrarSeguridadAlterna($fk_a018_num_seguridad_usuario,$fk_a015_num_seguridad_aplicacion,$fk_a004_num_dependencia)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "INSERT INTO
                a019_seguridad_dependencia
              SET
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fk_a004_num_dependencia=:fk_a004_num_dependencia,
                fk_a015_num_seguridad_aplicacion=:fk_a015_num_seguridad_aplicacion
                
            ");
        $NuevoRegistro->execute(array(
            ':fk_a018_num_seguridad_usuario' => $fk_a018_num_seguridad_usuario,
            ':fk_a015_num_seguridad_aplicacion' => $fk_a015_num_seguridad_aplicacion,
            ':fk_a004_num_dependencia' => $fk_a004_num_dependencia,
        ));
        $error = $NuevoRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

}
