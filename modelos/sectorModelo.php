<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        17-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once RUTA_MODELO.'parroquiaModelo.php';
class sectorModelo extends estadoModelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atParroquiamodelo = new parroquiaModelo();
    }

    public function metMostrarSector($idSector)
    {
        $ciudad = $this->_db->query("
            SELECT
                sector.*,
                parroquia.fk_a011_num_municipio,
                municipio.fk_a009_num_estado,
                estado.fk_a008_num_pais
            FROM
              a013_sector AS sector 
              INNER JOIN a012_parroquia AS parroquia ON parroquia.pk_num_parroquia = sector.fk_a012_num_parroquia
              INNER JOIN a011_municipio AS municipio ON municipio.pk_num_municipio = parroquia.fk_a011_num_municipio
              INNER JOIN a009_estado AS estado ON estado.pk_num_estado = municipio.fk_a009_num_estado
              INNER JOIN a008_pais AS pais ON pais.pk_num_pais = estado.fk_a008_num_pais
            WHERE pk_num_sector='$idSector'
        ");
        $ciudad->setFetchMode(PDO::FETCH_ASSOC);

        return $ciudad->fetch();
    }

    public function metListarSector()
    {
        $ciudad = $this->_db->query(
            "SELECT
               a013_sector.*,
               a012_parroquia.ind_parroquia
            FROM
                a013_sector
            INNER JOIN a012_parroquia ON a012_parroquia.pk_num_parroquia = a013_sector.fk_a012_num_parroquia
            ");
        $ciudad->setFetchMode(PDO::FETCH_ASSOC);
        return $ciudad->fetchAll();
    }

    public function metCrearSector($sector,$idParroquia,$status)
    {
        $this->_db->beginTransaction();
        $registroSector=$this->_db->prepare(
            "INSERT INTO
              a013_sector
            SET
            ind_sector=:ind_sector,
            fk_a012_num_parroquia=:fk_a012_num_parroquia, 
            num_estatus=:num_estatus, 
            fec_ultima_modificacion=NOW(), 
            fk_a018_num_seguridad_usuario=$this->atIdUsuario
            "
        );
        $registroSector->execute(array(
            ':ind_sector' => $sector,
            ':fk_a012_num_parroquia' => $idParroquia,
            ':num_estatus' => $status
        ));
        $idSector=$this->_db->lastInsertId();
        $error = $registroSector->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idSector;
        }
    }

    public function metModificarSector($sector,$idParroquia,$status,$idSector)
    {
        $this->_db->beginTransaction();
        $registroSector=$this->_db->prepare(
            "UPDATE
              a013_sector
            SET
                ind_sector=:ind_sector,
                fk_a012_num_parroquia=:fk_a012_num_parroquia, 
                num_estatus=:num_estatus, 
                fec_ultima_modificacion=NOW(), 
                fk_a018_num_seguridad_usuario=$this->atIdUsuario
              WHERE
              pk_num_sector=:pk_num_sector
            "
        );
        $registroSector->execute(array(
            ':ind_sector' => $sector,
            ':fk_a012_num_parroquia' => $idParroquia,
            ':num_estatus' => $status,
            ':pk_num_sector' => $idSector
        ));
        $error = $registroSector->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idSector;
        }
    }
    public function metJsonParroquia($idMunicipio)
    {
        $ciudad = $this->_db->query(
            "SELECT
              *
            FROM
              a012_parroquia
            WHERE
              fk_a011_num_municipio='$idMunicipio' AND num_estatus=1
            ");
        $ciudad->setFetchMode(PDO::FETCH_ASSOC);
        return $ciudad->fetchAll();
    }

    public function metJsonSector($parroquia)
    {
        $sector = $this->_db->query(
            "SELECT
               a013_sector.*,
               a012_parroquia.ind_parroquia
            FROM
                a013_sector
            INNER JOIN a012_parroquia ON a012_parroquia.pk_num_parroquia = a013_sector.fk_a012_num_parroquia and a012_parroquia.pk_num_parroquia= $parroquia
            ");
        $sector->setFetchMode(PDO::FETCH_ASSOC);
        return $sector->fetchAll();
    }

}
