<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        17-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once RUTA_MODELO.'municipioModelo.php';
class parroquiaModelo extends estadoModelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atMunicipiomodelo = new municipioModelo();
    }

    public function metMostrarParroquia($idParroquia)
    {
        $ciudad = $this->_db->query("
            SELECT
                parroquia.*,
                estado.fk_a008_num_pais,
                municipio.fk_a009_num_estado
            FROM
              a012_parroquia AS parroquia 
              INNER JOIN a011_municipio AS municipio ON municipio.pk_num_municipio = parroquia.fk_a011_num_municipio
              INNER JOIN a009_estado AS estado ON estado.pk_num_estado = municipio.fk_a009_num_estado
              INNER JOIN a008_pais AS pais ON pais.pk_num_pais = estado.fk_a008_num_pais
            WHERE pk_num_parroquia='$idParroquia'
        ");
        $ciudad->setFetchMode(PDO::FETCH_ASSOC);

        return $ciudad->fetch();
    }

    public function metListarParroquia()
    {
        $ciudad = $this->_db->query(
            "SELECT
                *
            FROM
                a012_parroquia
            INNER JOIN a011_municipio ON a011_municipio.pk_num_municipio = a012_parroquia.fk_a011_num_municipio
            ");
        $ciudad->setFetchMode(PDO::FETCH_ASSOC);
        return $ciudad->fetchAll();
    }

    public function metCrearParroquia($parroquia,$idMunicipio,$status)
    {
        $this->_db->beginTransaction();
        $registroParroquia=$this->_db->prepare(
            "INSERT INTO
              a012_parroquia
            SET
              num_estatus=:num_estatus, 
              fk_a011_num_municipio=:fk_a011_num_municipio,
              ind_parroquia=:ind_parroquia
            "
        );
        $registroParroquia->execute(array(
            ':ind_parroquia' => $parroquia,
            ':fk_a011_num_municipio' => $idMunicipio,
            ':num_estatus' => $status
        ));
        $idParroquia=$this->_db->lastInsertId();
        $error = $registroParroquia->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idParroquia;
        }
    }

    public function metModificarParroquia($parroquia,$idMunicipio,$status,$idParroquia)
    {
        $this->_db->beginTransaction();
        $registroParroquia=$this->_db->prepare(
            "UPDATE
              a012_parroquia
            SET
              num_estatus=:num_estatus, 
              fk_a011_num_municipio=:fk_a011_num_municipio,
              ind_parroquia=:ind_parroquia
              WHERE
              pk_num_parroquia=:pk_num_parroquia
            "
        );
        $registroParroquia->execute(array(
            ':ind_parroquia' => $parroquia,
            ':fk_a011_num_municipio' => $idMunicipio,
            ':num_estatus' => $status,
            ':pk_num_parroquia' => $idParroquia
        ));
        $error = $registroParroquia->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idParroquia;
        }
    }

    public function metJsonParroquia($idMunicipio)
    {
        $listarParroquia = $this->_db->query(
            "SELECT
                *
            FROM
                a012_parroquia
            INNER JOIN a011_municipio ON a011_municipio.pk_num_municipio = a012_parroquia.fk_a011_num_municipio and a011_municipio.pk_num_municipio= $idMunicipio
            ");
        $listarParroquia->setFetchMode(PDO::FETCH_ASSOC);
        return $listarParroquia->fetchAll();
    }


}
