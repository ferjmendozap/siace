<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        17-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once RUTA_MODELO.'estadoModelo.php';
require_once RUTA_MODELO.'municipioModelo.php';
class ciudadModelo extends estadoModelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atMunicipioModelo = new municipioModelo();
    }

    public function metMostrarCiudad($idCiudad)
    {
        $ciudad = $this->_db->query("
            SELECT
                a010_ciudad.*,
                a009_estado.fk_a008_num_pais,
                a011_municipio.pk_num_municipio,
                a014_ciudad_municipio.pk_num_ciudad_municipio
            FROM
                a010_ciudad
            INNER JOIN a009_estado ON a010_ciudad.fk_a009_num_estado = a009_estado.pk_num_estado
            INNER JOIN a008_pais ON a009_estado.fk_a008_num_pais = a008_pais.pk_num_pais
            LEFT JOIN a014_ciudad_municipio ON a014_ciudad_municipio.fk_a010_num_ciudad = a010_ciudad.pk_num_ciudad
            LEFT JOIN a011_municipio ON a011_municipio.pk_num_municipio = a014_ciudad_municipio.fk_a011_num_municipio
            WHERE a010_ciudad.pk_num_ciudad='$idCiudad'
        ");
        $ciudad->setFetchMode(PDO::FETCH_ASSOC);

        return $ciudad->fetch();
    }

    public function metListarCiudad()
    {
        $ciudad = $this->_db->query(
            "SELECT
                *
            FROM
                a010_ciudad
            INNER JOIN a009_estado ON a010_ciudad.fk_a009_num_estado = a009_estado.pk_num_estado
            INNER JOIN a008_pais ON a009_estado.fk_a008_num_pais = a008_pais.pk_num_pais
            ");
        $ciudad->setFetchMode(PDO::FETCH_ASSOC);
        return $ciudad->fetchAll();
    }

    public function metCrearCiudad($ciudad,$idEstado,$flag_capital,$codPostal,$status)
    {
        $this->_db->beginTransaction();
        $registroCiudad=$this->_db->prepare(
            "INSERT INTO
              a010_ciudad
            SET
              num_estatus=:num_estatus, fk_a009_num_estado=:fk_a009_num_estado, ind_ciudad=:ind_ciudad,
              ind_cod_postal=:ind_cod_postal,num_flag_capital=:num_flag_capital
            "
        );
        $registroCiudad->execute(array(
            ':ind_ciudad' => $ciudad,
            ':fk_a009_num_estado' => $idEstado,
            ':ind_cod_postal' => $codPostal,
            ':num_flag_capital' => $flag_capital,
            ':num_estatus' => $status
        ));
        $idCiudad=$this->_db->lastInsertId();
        $error = $registroCiudad->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idCiudad;
        }
    }

    public function metModificarCiudad($ciudad,$idEstado,$flag_capital,$codPostal,$status,$idCiudad)
    {
        $this->_db->beginTransaction();
        $registroCiudad=$this->_db->prepare(
            "UPDATE
              a010_ciudad
            SET
              num_estatus=:num_estatus, fk_a009_num_estado=:fk_a009_num_estado, ind_ciudad=:ind_ciudad,
              ind_cod_postal=:ind_cod_postal,num_flag_capital=:num_flag_capital
            WHERE pk_num_ciudad='$idCiudad'
            "
        );
        $registroCiudad->execute(array(
            ':ind_ciudad' => $ciudad,
            ':fk_a009_num_estado' => $idEstado,
            ':ind_cod_postal' => $codPostal,
            ':num_flag_capital' => $flag_capital,
            ':num_estatus' => $status
        ));
        $error = $registroCiudad->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idCiudad;
        }
    }


    public function metCrearCiudadMunicipio($ciudad,$municipio)
    {
        $this->_db->beginTransaction();
        $registroCM=$this->_db->prepare(
            "INSERT INTO a014_ciudad_municipio SET 
              fk_a010_num_ciudad=:fk_a010_num_ciudad,
              fk_a011_num_municipio=:fk_a011_num_municipio
            "
        );
        $registroCM->execute(array(
            ':fk_a010_num_ciudad' => $ciudad,
            ':fk_a011_num_municipio' => $municipio
        ));
        $idCM=$this->_db->lastInsertId();
        $error = $registroCM->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idCM;
        }
    }
    public function metModificarCiudadMunicipio($ciudad,$municipio,$idCM)
    {
        $this->_db->beginTransaction();
        $modificarCM=$this->_db->prepare(
            "UPDATE a014_ciudad_municipio SET 
              fk_a010_num_ciudad=:fk_a010_num_ciudad,
              fk_a011_num_municipio=:fk_a011_num_municipio
              WHERE pk_num_ciudad_municipio=:pk_num_ciudad_municipio
            "
        );
        $modificarCM->execute(array(
            ':fk_a010_num_ciudad' => $ciudad,
            ':fk_a011_num_municipio' => $municipio,
            ':pk_num_ciudad_municipio' => $idCM
        ));
        $error = $modificarCM->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idCM;
        }
    }

    public function metJsonCiudad($idEstado)
    {
        $ciudad = $this->_db->query(
            "SELECT
              *
            FROM
              a010_ciudad
            WHERE
              fk_a009_num_estado='$idEstado' AND num_estatus=1
            ");
        $ciudad->setFetchMode(PDO::FETCH_ASSOC);
        return $ciudad->fetchAll();
    }

    public function metJsonMunicipio($idEstado)
    {
        $ciudad = $this->_db->query(
            "SELECT
              *
            FROM
              a011_municipio
            WHERE
              fk_a009_num_estado='$idEstado' AND num_estatus=1
            ");
        $ciudad->setFetchMode(PDO::FETCH_ASSOC);
        return $ciudad->fetchAll();
    }

}
