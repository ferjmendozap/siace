<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        17-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once RUTA_MODELO.'aplicacionModelo.php';
class menuModelo extends aplicacionModelo
{
    private $atIdUsuario;
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarMenu($idMenu)
    {
        $menu = $this->_db->query(
            "SELECT * FROM a027_seguridad_menu WHERE pk_num_seguridad_menu='$idMenu'"
        );
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetch();
    }

    public function metListarMenu($nivel = false,$codPadre = false)
    {

        if(($nivel===0 && $codPadre===false) || ($nivel && $codPadre===false)) {
            $menu = $this->_db->query("SELECT * FROM a027_seguridad_menu WHERE num_nivel='$nivel' ORDER BY ind_nombre ASC");
        }elseif(($nivel===0 && $codPadre===0) || ($nivel && $codPadre)){
            $menu = $this->_db->query("SELECT * FROM a027_seguridad_menu WHERE num_nivel='$nivel' AND cod_padre='$codPadre' ORDER BY cod_interno ASC");
        }else{
            $menu = $this->_db->query("
              SELECT
                *,
                a027.num_estatus AS num_estatus
              FROM
                a027_seguridad_menu AS a027
              INNER JOIN
                a015_seguridad_aplicacion AS a015 on a027.fk_a015_num_seguridad_aplicacion=a015.pk_num_seguridad_aplicacion
              ORDER BY
                a027.cod_interno ASC
            ");
        }
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
    }
    /*
    *FERNANDO MENDOZA
    *16Ene18
    *Permite obtener el menu para el modulo
    */
    public function metListarMenuModulo($nivel = false,$codPadre = false, $modulo)
    {

        if(($nivel===0 && $codPadre===false) || ($nivel && $codPadre===false)) {
            $menu = $this->_db->query("SELECT * FROM a027_seguridad_menu WHERE num_nivel='$nivel' and a027_seguridad_menu.fk_a015_num_seguridad_aplicacion='$modulo' ORDER BY ind_nombre ASC");
        }elseif(($nivel===0 && $codPadre===0) || ($nivel && $codPadre)){
            $menu = $this->_db->query("SELECT * FROM a027_seguridad_menu WHERE num_nivel='$nivel' AND cod_padre='$codPadre' and a027_seguridad_menu.fk_a015_num_seguridad_aplicacion='$modulo' ORDER BY cod_interno ASC");
        }else{
            $menu = $this->_db->query("
              SELECT
                *,
                a027.num_estatus AS num_estatus
              FROM
                a027_seguridad_menu AS a027
              INNER JOIN
                a015_seguridad_aplicacion AS a015 on a027.fk_a015_num_seguridad_aplicacion=a015.pk_num_seguridad_aplicacion
              WHERE a027.fk_a015_num_seguridad_aplicacion='$modulo' 
              ORDER BY
                a027.cod_interno ASC
            ");
        }
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
    }    

    public function metCrearMenu($codAplicacion, $nombre, $descripcion, $codInterno, $codPadre, $nivel, $ruta, $rol, $codIcono, $estatus)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "INSERT INTO
                a027_seguridad_menu
              SET
                fk_a015_num_seguridad_aplicacion=:fk_a015_num_seguridad_aplicacion, ind_nombre=:ind_nombre,
                ind_descripcion=:ind_descripcion, cod_interno=:cod_interno, cod_padre=:cod_padre, num_nivel=:num_nivel,
                ind_ruta=:ind_ruta, ind_rol=:ind_rol, ind_icono=:ind_icono, num_estatus=:num_estatus, fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            ");
        $NuevoRegistro->execute(array(
            ':fk_a015_num_seguridad_aplicacion' => $codAplicacion,
            ':ind_nombre' => mb_strtoupper($nombre, 'utf-8'),
            ':ind_descripcion' => mb_strtoupper($descripcion, 'utf-8'),
            ':cod_interno' => mb_strtoupper($codInterno, 'utf-8'),
            ':cod_padre' => mb_strtoupper($codPadre, 'utf-8'),
            ':num_nivel' => $nivel,
            ':ind_ruta' => $ruta,
            ':ind_rol' => $rol,
            ':ind_icono' => $codIcono,
            ':num_estatus' => $estatus
        ));
        $error = $NuevoRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metActualizarMenu($idMenu,$codAplicacion, $nombre, $descripcion, $codInterno, $codPadre, $nivel, $ruta, $rol, $codIcono, $estatus)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
              a027_seguridad_menu
            SET
              fk_a015_num_seguridad_aplicacion=:fk_a015_num_aplicacion, ind_nombre=:ind_nombre, fec_ultima_modificacion=NOW(),
              ind_descripcion=:ind_descripcion, cod_interno=:cod_interno, cod_padre=:cod_padre,num_nivel=:num_nivel,
              ind_ruta=:ind_ruta, ind_rol=:ind_rol, ind_icono=:ind_icono, num_estatus=:num_estatus, fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            WHERE
            pk_num_seguridad_menu='$idMenu'
            "
        );
        $NuevoRegistro->execute(array(
            ':fk_a015_num_aplicacion' => $codAplicacion,
            ':ind_nombre' => mb_strtoupper($nombre, 'utf-8'),
            ':ind_descripcion' => mb_strtoupper($descripcion, 'utf-8'),
            ':cod_interno' => mb_strtoupper($codInterno, 'utf-8'),
            ':cod_padre' => mb_strtoupper($codPadre, 'utf-8'),
            ':num_nivel' => $nivel,
            ':ind_ruta' => $ruta,
            ':ind_rol' => $rol,
            ':ind_icono' => $codIcono,
            ':num_estatus' => $estatus
        ));
        $error = $NuevoRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
        }
    }

    public function metEliminarMenu($idMenu)
    {
        $this->_db->beginTransaction();
            $this->_db->query(
            "  delete from a027_seguridad_menu where pk_num_seguridad_menu ='$idMenu'"
            );
        $this->_db->commit();
    }


}
