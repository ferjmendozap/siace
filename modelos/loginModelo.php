<?php

class loginModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
    }

    public function metActualizazStatus($usuario, $ip)
    {
        $this->_db->beginTransaction();
        $ModificarRegistro=$this->_db->prepare(
            "UPDATE a018_seguridad_usuario SET ind_ip=:ind_ip WHERE ind_usuario='$usuario'"
        );

        //prueba
        $ModificarRegistro->execute(array(
            ':ind_ip' => $ip
        ));

        $error = $ModificarRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metValidarUsuario($usuario, $password)
    {
        $password = Hash::metObtenerHash($password);
        $usuario = $this->_db->query(
            "select
              *
            from
              a018_seguridad_usuario
            INNER JOIN
              rh_b001_empleado
                ON rh_b001_empleado.pk_num_empleado = a018_seguridad_usuario.fk_rhb001_num_empleado
            INNER JOIN
              rh_c005_empleado_laboral
                ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
            INNER JOIN
              rh_c063_puestos
                ON rh_c063_puestos.pk_num_puestos = rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo
            INNER JOIN
              a003_persona
                ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            LEFT JOIN 
              a006_miscelaneo_detalle
                ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = a003_persona.fk_a006_num_miscelaneo_detalle_sexo
            where
                ind_usuario = '$usuario'
              and
                ind_password = '$password'
            "
        );
        return $usuario->fetch();
    }
    /**
     * Autor            : Alexis Ontiveros, 11 Ago 2017.
     * Descripción      : Extrae los datos del usuario cuando se loguéa en el LDAP.
     * @param $cedula   : El número de cédula del usuario.
     * @return          : Un array con los datos del usuario ó false.
     */
    public function metBuscaDataUsuario($cedula){
        $usuario = $this->_db->query("
            SELECT * FROM a018_seguridad_usuario
              INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = a018_seguridad_usuario.fk_rhb001_num_empleado
              INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
              LEFT JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = a003_persona.fk_a006_num_miscelaneo_detalle_sexo
            WHERE ind_cedula_documento='".$cedula."'
        ");
        return $usuario->fetch();
    }
}
