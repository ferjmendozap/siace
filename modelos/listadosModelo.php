<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        17-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class listadosModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metListadoEmpleados()
    {
        $aplicacion = $this->_db->query("
                SELECT
                  *,
                  nm_b001_tipo_nomina.ind_nombre_nomina,
                  rh_c005_empleado_laboral.fec_ingreso
                FROM
                  rh_b001_empleado
                INNER JOIN
                  a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                INNER JOIN
                  rh_c005_empleado_laboral ON rh_b001_empleado.pk_num_empleado = rh_c005_empleado_laboral.fk_rhb001_num_empleado
                INNER JOIN
                  rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN
                  nm_b001_tipo_nomina ON nm_b001_tipo_nomina.pk_num_tipo_nomina = rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina
                 ");
        $aplicacion->setFetchMode(PDO::FETCH_ASSOC);

        return $aplicacion->fetchAll();
    }

}
