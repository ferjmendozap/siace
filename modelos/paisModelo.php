<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        13-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class paisModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarPais($idPais)
    {
        $aplicacion = $this->_db->query("
            SELECT * FROM a008_pais WHERE pk_num_pais='$idPais'
        ");
        $aplicacion->setFetchMode(PDO::FETCH_ASSOC);

        return $aplicacion->fetch();
    }

    public function metListarPais($status=false)
    {
        if($status){
            $status='WHERE num_estatus=1';
        }
        $pais = $this->_db->query("SELECT * FROM a008_pais $status ");
        $pais->setFetchMode(PDO::FETCH_ASSOC);
        return $pais->fetchAll();
    }

    public function metCrearPais($pais,$status)
    {
        $this->_db->beginTransaction();
        $registroPais=$this->_db->prepare(
            "INSERT INTO
              a008_pais
            SET
              num_estatus=:num_estatus, ind_pais=:ind_pais
            "
        );
        $registroPais->execute(array(
            ':ind_pais' => $pais,
            ':num_estatus' => $status
        ));
        $idPais=$this->_db->lastInsertId();
        $error = $registroPais->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idPais;
        }
    }

    public function metModificarPais($pais,$status,$idPais)
    {
        $this->_db->beginTransaction();
        $registroPais=$this->_db->prepare(
            "UPDATE
              a008_pais
            SET
              num_estatus=:num_estatus, ind_pais=:ind_pais
            WHERE
              pk_num_pais='$idPais'
            "
        );
        $registroPais->execute(array(
            ':ind_pais' => $pais,
            ':num_estatus' => $status
        ));
        $error = $registroPais->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idPais;
        }
    }

}
