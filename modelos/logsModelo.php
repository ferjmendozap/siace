<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        08-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class logsModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
    }

    public function metObtenerUltimoLogs($idUsuario)
    {
        $ultimoLogs = $this->_db->query(
            "select * from a034_logs_usuarios where fk_a018_num_seguridad_usuario = '".$idUsuario."' ORDER BY pk_num_logs_usuario DESC limit 1"
        );
        $ultimoLogs->setFetchMode(PDO::FETCH_ASSOC);
        return $ultimoLogs->fetch();
    }

    /**
     * @param $idUsuario
     * @param $tipoActividad
     * @param $detalle
     * @param $ip
     * @return array|string
     */
    public function metRegistrarLogs($idUsuario,$tipoActividad,$detalle, $ip)
    {
        $this->_db->beginTransaction();
            $registrarLogs=$this->_db->prepare(
                "INSERT INTO
                    a034_logs_usuarios
                  SET
                    ind_detalle=:ind_detalle, fk_a033_num_tipo_logs=:fk_a033_num_tipo_logs,
                    fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                    ind_ip=:ind_ip, fec_log=NOW()
                ");
            $registrarLogs->execute(array(
                ':fk_a018_num_seguridad_usuario' => $idUsuario,
                ':fk_a033_num_tipo_logs' => $tipoActividad,
                ':ind_detalle' => $detalle,
                ':ind_ip' => $ip
            ));
            $error = $registrarLogs->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $idRegistro= $this->_db->lastInsertId();
                $this->_db->commit();
                return $idRegistro;
            }
    }

}
