<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        14-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once RUTA_MODELO.'paisModelo.php';
class estadoModelo extends paisModelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarEstado($idEstado)
    {
        $estado = $this->_db->query("
            SELECT
              *
            FROM
              a009_estado
            INNER JOIN a008_pais ON a008_pais.pk_num_pais = a009_estado.fk_a008_num_pais
            WHERE pk_num_estado='$idEstado'
        ");
        $estado->setFetchMode(PDO::FETCH_ASSOC);

        return $estado->fetch();
    }

    public function metListarEstado($status=false)
    {
        if($status){
            $status='WHERE num_estatus=1';
        }
        $estado = $this->_db->query(
            "SELECT
              *
            FROM
              a009_estado
            INNER JOIN a008_pais ON a008_pais.pk_num_pais = a009_estado.fk_a008_num_pais
            $status
            ");
        $estado->setFetchMode(PDO::FETCH_ASSOC);
        return $estado->fetchAll();
    }

    public function metCrearEstado($estado,$idPais,$status)
    {
        $this->_db->beginTransaction();
        $registroEstado=$this->_db->prepare(
            "INSERT INTO
              a009_estado
            SET
              num_estatus=:num_estatus, fk_a008_num_pais=:fk_a008_num_pais, ind_estado=:ind_estado
            "
        );
        $registroEstado->execute(array(
            ':ind_estado' => $estado,
            ':fk_a008_num_pais' => $idPais,
            ':num_estatus' => $status
        ));
        $idEstado=$this->_db->lastInsertId();
        $error = $registroEstado->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idEstado;
        }
    }

    public function metModificarEstado($estado,$idPais,$status,$idEstado)
    {
        $this->_db->beginTransaction();
        $registroEstado=$this->_db->prepare(
            "UPDATE
              a009_estado
            SET
              num_estatus=:num_estatus, fk_a008_num_pais=:fk_a008_num_pais, ind_estado=:ind_estado
            WHERE
              pk_num_estado='$idEstado'
            "
        );
        $registroEstado->execute(array(
            ':ind_estado' => $estado,
            ':fk_a008_num_pais' => $idPais,
            ':num_estatus' => $status
        ));
        $error = $registroEstado->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idEstado;
        }
    }

    public function metJsonEstado($idPais)
    {
        $estado = $this->_db->query(
            "SELECT
              *
            FROM
              a009_estado
            WHERE
              fk_a008_num_pais='$idPais' AND num_estatus=1
            ");
        $estado->setFetchMode(PDO::FETCH_ASSOC);
        return $estado->fetchAll();
    }

}
