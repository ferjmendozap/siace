<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Presupuesto
 * PROCESO: Carga Presupuesto
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                |liduvica@hotmail.com                |         0424-9080200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        17-05-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class scriptCargaPresupuestoModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
    }

    public function metBusquedaSimple($tabla, $innerJoin, $where)
    {
        $busquedaSimple = $this->_db->query("
            SELECT
              *
            FROM
              $tabla
              $innerJoin
            WHERE
              $where
        ");
        $busquedaSimple->setFetchMode(PDO::FETCH_ASSOC);
        return $busquedaSimple->fetch();
    }


    
    public function metPresupuestoPartidasPresupuestarias($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                pr_b002_partida_presupuestaria
              SET
                fk_prb001_num_tipo_cuenta=:fk_prb001_num_tipo_cuenta,
                cod_partida=:cod_partida,
                ind_partida=:ind_partida,
                ind_generica=:ind_generica,
                ind_especifica=:ind_especifica,
                ind_subespecifica=:ind_subespecifica,
                ind_denominacion=:ind_denominacion,
                num_estatus=:num_estatus,
                ind_tipo=:ind_tipo,
                num_nivel=:num_nivel,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=:fec_ultima_modificacion,
                ind_numeral=:ind_numeral
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('pr_b002_partida_presupuestaria', false, "cod_partida = '" . $array['cod_partida'] . "'");
            //var_dump($busqueda);
             // exit;
            if (!$busqueda) {
                $registro->execute(array(
                    'fk_prb001_num_tipo_cuenta' => $array['fk_prb001_num_tipo_cuenta'],
                    'cod_partida' => $array['cod_partida'],
                    'ind_partida' => $array['ind_partida'],
                    'ind_generica' => $array['ind_generica'],
                    'ind_especifica' => $array['ind_especifica'],
                    'ind_subespecifica' => $array['ind_subespecifica'],
                    'ind_denominacion' => $array['ind_denominacion'],
                    'num_estatus' => $array['num_estatus'],
                    'ind_tipo' => $array['ind_tipo'],
                    'num_nivel' => $array['num_nivel'],
                    'fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    'fec_ultima_modificacion' => $array['fec_ultima_modificacion'],
                    'ind_numeral' => $array['ind_numeral']
                ));
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    
   
    public function metTipoCuenta($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                pr_b001_tipo_cuenta
              SET
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=:fec_ultima_modificacion, 
                cod_tipo_cuenta=:cod_tipo_cuenta,
                num_estatus=:num_estatus ,
                ind_descripcion=:ind_descripcion 
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('pr_b001_tipo_cuenta', false, "cod_tipo_cuenta = '" . $array['cod_tipo_cuenta'] . "'");
            //var_dump($busqueda);
             //exit;
            if (!$busqueda) {
                $registro->execute(array(
                   'fk_a018_num_seguridad_usuario'=>$array['fk_a018_num_seguridad_usuario'],
                   'fec_ultima_modificacion'=>$array['fec_ultima_modificacion'], 
                   'cod_tipo_cuenta'=>$array['cod_tipo_cuenta'],
                   'num_estatus'=>$array['num_estatus'],
                   'ind_descripcion'=>$array['ind_descripcion'],
                ));
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }



}
