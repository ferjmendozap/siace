<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{
    function Header()
    {
        $this->SetTitle('Procedimiento de Seleccion de Contratistas');
        $this->Image('publico/imagenes/logos/CES.png', 10, 10, 22);
        $this->SetFont('Arial','B',8);
        $this->SetXY(35,12); $this->Cell(100,5,utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'),0,1,'L');
        $this->SetXY(35,16); $this->Cell(100,5,utf8_decode('CONTRALORÍA DEL ESTADO SUCRE'),0,1,'L');
        $this->SetXY(35,20); $this->Cell(100,5,utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'),0,1,'L');
        ## 
        $this->SetXY(222,12); $this->Cell(25,5,utf8_decode('Fecha: ' . date('d-m-Y')),0,1,'L');
        $this->SetXY(222,17); $this->Cell(25,5,utf8_decode('Página: ' . $this->PageNo() . ' de {nb}'),0,1,'L');
        $this->SetXY(40, 10); $this->Cell(100, 20, '', 0, 1, 'L');
        $this->SetXY(42, 10); $this->Cell(20, 28, '', 0, 1, 'L');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."LOGOSNCF.png", 250, 10, 17, 15);
        $this->SetXY(90,30); $this->Cell(100,5,utf8_decode('Procedimiento de Selección de Contratistas'),0,1,'C');
        $this->Cell(195, 5, utf8_decode('Desde: '.DESDE.'  Hasta: '.HASTA), 0, 1, 'L');

        $this->Ln(5);
        $this->SetDrawColor(255, 255, 255);
        $this->SetFillColor(200, 200, 200);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 7);
        $this->SetWidths(array(25, 40, 85, 35,20, 30, 30));
        $this->SetAligns(array('C', 'C', 'C', 'C','C', 'C'));
        $this->Row(array(utf8_decode('Nº de Requerimiento'),
            utf8_decode('Nº de Procedimiento'),
            utf8_decode('Nombre de Procedimiento'),
            utf8_decode('Nº de Acta de inicio'),
            utf8_decode('Fecha de Acta de inicio'),
            utf8_decode('Estado')));
        $this->Ln(1);
        $this->Ln(1);

    }

    //Page footer
    function Footer()
    {
        /*
        $this->SetY(-20);
        $this->Line(10,204.5,345,204.5);
        $this->SetY(-15);
        $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'R');
        */
    }
}
