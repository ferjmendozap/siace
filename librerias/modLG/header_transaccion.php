<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{
    function Header()
    {
        $this->SetFillColor(255, 255, 255);
        $this->SetDrawColor(0, 0, 0);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 5, 5, 20, 12);
        $this->SetFont('Arial', '', 8);
        $this->SetXY(25, 5); $this->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->SetXY(25, 10); $this->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->SetFont('Arial', 'B', 10);
        $this->SetXY(10, 20); $this->Cell(200, 5, utf8_decode('Nota de '.NOTA.' N° '.NRO_INTERNO), 0, 1, 'C', 0);
        $this->Ln(10);

        ##	primera pagina solamente
        $this->SetFont('Arial', '', 8); $this->Cell(20, 5, utf8_decode('Transacción: '), 0, 0, 'L', 1);
        $this->SetFont('Arial', 'B', 8); $this->Cell(75, 5, FEC_DOC.'   '.utf8_decode('NomTransaccion'), 0, 0, 'L');
        $this->Ln(6);

        $this->SetFont('Arial', '', 8); $this->Cell(20, 5, 'Documento: ', 0, 0, 'L', 1);
        if ('NomProveedor' != "") {
            $Documento = 'FechaPreparacion'.'   OC-'.'ReferenciaNroDocumento';
            $this->SetFont('Arial', 'B', 8); $this->Cell(75, 5, $Documento, 0, 0, 'L');
        }



        $this->Ln(6);

        $this->SetFont('Arial', '', 8); $this->Cell(20, 5, 'Referencia', 0, 0, 'L', 1);
        $this->SetFont('Arial', 'B', 8); $this->Cell(75, 5, 'DocumentoReferencia', 0, 0, 'L');

        if ('NomProveedor' != "") {
            $this->SetXY(110, 35);
            $this->SetFont('Arial', '', 8); $this->Cell(15, 5, 'Proveedor: ', 0, 0, 'L', 1);
            $this->SetFont('Arial', 'B', 8); $this->Cell(90, 5, utf8_decode('NomProveedor'), 0, 0, 'L');

            $this->SetXY(110, 41);
            $this->SetFont('Arial', '', 8);
            $this->Cell(15, 5, 'R.I.F.: ', 0, 0, 'L', 1);
            $this->Cell(30, 5, 'DocFiscal', 0, 0, 'L');
            $this->Cell(15, 5, utf8_decode('Teléfono: '), 0, 0, 'L', 1);
            $this->Cell(45, 5, 'Telefono1', 0, 0, 'L');

            $this->SetXY(110, 47);
            $this->SetFont('Arial', '', 8);
            $this->Cell(15, 5, utf8_decode('Dirección: '), 0, 0, 'L', 1);
            $this->Cell(30, 5, utf8_decode('Direccion'), 0, 0, 'L');
        }

        $this->Ln(6);
        $this->SetFont('Arial', '', 8);
        $this->Cell(20, 5, 'Comentarios: ', 0, 0, 'L', 1);
        $this->MultiCell(180, 5, utf8_decode('Comentarios'), 0, 'L');
        $this->Ln(1);


        $this->SetTextColor(0, 0, 0);
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        $this->SetFont('Arial', 'B', 8);
        $this->SetWidths(array(10, 15, 20, 90, 10, 18, 18, 18));
        $this->SetAligns(array('C', 'C', 'C', 'L', 'C', 'R', 'R', 'R'));
        $this->Row(array('#',
            'Codigo Interno',
            'Item',
            utf8_decode('Descripción'),
            'Uni.',
            'Cant. Compra',
            'Cant. Ingreso',
            'Cant. Pendiente'));
        $this->Ln(1);
    }

    //Page footer
    function Footer()
    {

    }
}
