<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{
    function Header()
    {
        $this->SetTitle(utf8_decode('Listado de Movimientos de Almacén'));
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 5, 5, 20, 12);

        $this->SetFont('Arial', '', 8);
        $this->SetXY(25, 5); $this->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->SetXY(25, 10); $this->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->SetXY(180, 5); $this->Cell(15, 5, utf8_decode('Fecha: '), 0, 0, 'R');
        $this->Cell(60, 5, date("d-m-Y"), 0, 1, 'L');
        $this->SetXY(180, 10); $this->Cell(15, 5, utf8_decode('Página: '), 0, 0, 'R');
        $this->Cell(60, 5, $this->PageNo().' de {nb}', 0, 1, 'L');
        $this->SetFont('Arial', 'B', 9);
        $this->SetXY(5, 20); $this->Cell(195, 5, utf8_decode('Movimientos de Almacén Resumido'), 0, 1, 'C', 0, 'reporte_movimientos_de_almacen_pdf.php');
        $this->Cell(195, 5, utf8_decode('Desde: ').FDESDE.utf8_decode(' / Hasta: ').FHASTA, 0, 1, 'C');
        $this->Ln(5);

        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 6);
        $this->SetWidths(array(15, 20, 70, 40, 15, 15, 15, 15));
        $this->SetAligns(array('C', 'C', 'L', 'C', 'R', 'R', 'R', 'R'));
        $this->Row(array(
            'Item',
            'Cod. Interno',
            'Descripcion',
            'Undidad',
            'Stock Inicial',
            'Ingresos',
            'Egresos',
            'Stock Final'));
        $this->Ln(1);
    }

    //Page footer
    function Footer()
    {

        $this->Ln(3);
        $y = $this->GetY();
        $this->Cell(30, 6,'        ______________________________________________                                                                                                               ______________________________________________', 0, 1, 'L');

        $this->SetFont('Arial', '', 7);
        $this->SetXY(10, $y+5);
        $this->Cell(120, 3, ('Elaborado Por:'), 0, 0, 'L');
        $this->Cell(120, 3, ('Revisado Por:'), 0, 1, 'L');
        $this->SetXY(10, $y+8);
        $this->Cell(120, 3, utf8_decode(ELABORADO_POR), 0, 0, 'L');//nombre de quien elabora
        $this->Cell(120, 3, utf8_decode(REVISADO_POR), 0, 1, 'L');//nombre de quien revisa
        $this->SetXY(10, $y+11);
        $this->Cell(120, 3, utf8_decode(ELABORADO_CARGO), 0, 0, 'L');//cargo de quien elabora
        $this->Cell(120, 3, utf8_decode(REVISADO_CARGO), 0, 1, 'L');//Cargo de quien revisa

        $this->SetXY(5, $y+16);
        $this->Cell(30, 6,'        _______________________________________                                                                                                _______________________________________', 0, 1, 'L');
        $this->SetFont('Arial', '', 7);
        $this->SetXY(10, $y+21);
        $this->Cell(120, 3, ('Conformado Por:'), 0, 0, 'L');
        $this->Cell(120, 3, ('Aprobado Por:'), 0, 1, 'L');
        $this->SetXY(10, $y+24);
        $this->Cell(120, 3, utf8_decode(CONFORMADO_POR), 0, 0, 'L');//nombre de quien conforma
        $this->Cell(120, 3, utf8_decode(APROBADO_POR), 0, 1, 'L');//nombre de quien aprueba
        $this->SetXY(10, $y+27);
        $this->Cell(120, 3, utf8_decode(CONFORMADO_CARGO), 0, 0, 'L');//cargo de quien conforma
        $this->Cell(120, 3, utf8_decode(APROBADO_CARGO), 0, 1, 'L');//Cargo de quien aprueba

    }
}
