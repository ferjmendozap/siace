<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT . 'librerias' . DS . 'modLG' . DS . 'tcpdf' . DS . 'tcpdf.php';
//require_once LIBRERIA_FPDF;

class pdf extends TCPDF
{
    function Header()
    {
        $this->SetDrawColor(255, 255, 255);
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('', '', 12);
        $this->AliasNbPages();
//        $this->SetXY(20, 10);

        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 20, 12);
        $this->SetXY(25, 10);
        $this->Cell(110, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->SetXY(25, 15);
        $this->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->SetXY(35, 20);
        $this->Cell(100, 5, utf8_decode('DIVISIÓN DE COMPRAS Y SUMINISTROS' ), 0, 0, 'L');
        $this->SetXY(300, 30);

        $this->SetFont('times', '', 11);
        $this->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'R');
        $this->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->SetAutoPageBreak(true,25);
        $this->Ln(10);
        $this->SetFont('times', 'B', 12);
        $this->SetXY(135, 40);
        $this->Cell(20, 5, utf8_decode('CUADRO COMPARATIVO DE OFERTAS '), 0, 0, 'L');
        $this->Ln(10);
    }

    //Page footer
    function Footer()
    {

        $this->SetXY(10, 180);
        $this->SetFont('times', 'B', 10);

        #Elaborador por y Revisado por Nombres
        $this->Cell(165, 6,'Elaborado Por:', 0, 0, 'C');
        $this->Cell(165, 6,'Revisado Por:', 0, 0, 'C');

        $this->Ln();
        $this->Cell(165, 6,strtoupper(ELABORADO_POR), 0, 0, 'C');
        $this->Cell(165, 6,strtoupper(REVISADO_POR), 0, 0, 'C');
        $this->Ln();
        $this->Cell(165, 6,strtoupper(CARGO_ELABORADO_POR), 0, 0, 'C');
        $this->Cell(165, 6,strtoupper(CARGO_REVISADO_POR), 0, 0, 'C');
    }
}
