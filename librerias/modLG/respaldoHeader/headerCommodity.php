<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{
    function Header()
    {
        $this->SetTitle('Stock de Commodity');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 5, 5, 20, 12); $this->SetFont('Arial', '', 8);

        $this->SetFont('Arial', '', 8);
        $this->SetXY(25, 5); $this->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->SetXY(25, 10); $this->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->SetXY(180, 5); $this->Cell(15, 5, utf8_decode('Fecha: '), 0, 0, 'R');
        $this->Cell(60, 5, date("d-m-Y"), 0, 1, 'L');
        $this->SetXY(185, 10); $this->Cell(15, 5, utf8_decode('Página: '), 0, 0, 'R');
        $this->Cell(60, 5, $this->PageNo().' de {nb}', 0, 1, 'L');
        $this->SetFont('Arial', 'B', 9);
        $this->SetXY(5, 20);
        $this->Cell(195, 5, utf8_decode('Información de Commodities'), 0, 1, 'C', 0);
        $this->Ln(5);
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 6);
        $this->SetWidths(array( 130, 30, 23, 23));
        $this->SetAligns(array('L', 'C', 'C', 'C'));
        $this->Row(array(
            utf8_decode('Descripción'),
            'Und.',
            'Partida',
            'Cta. Gasto'
            ));
        $this->Ln(1);
    }

    //Page footer
    function Footer()
    {
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0, 0, 0);
        $this->Rect(9, 258, 197, 0.1, 'DF');
    }
}
