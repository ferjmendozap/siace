<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{
    function Header()
    {
        $this->Image('publico/imagenes/logos/CES.png', 10, 10, 22);
        $this->SetFont('Arial','B',8);
        $this->SetXY(35,12); $this->Cell(100,5,utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'),0,1,'L');
        $this->SetXY(35,16); $this->Cell(100,5,utf8_decode('CONTRALORIA DEL ESTADO SUCRE'),0,1,'L');
        $this->SetXY(35,20); $this->Cell(100,5,utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'),0,1,'L');
        ## 
        $this->SetXY(220,12); $this->Cell(25,5,utf8_decode('Fecha: ' . date('d-m-Y')),0,1,'L');
        $this->SetXY(220,17); $this->Cell(25,5,utf8_decode('Página: ' . $this->PageNo() . ' de {nb}'),0,1,'L');
        $this->SetXY(40, 10); $this->Cell(100, 20, '', 0, 1, 'L');
        $this->SetXY(42, 10); $this->Cell(20, 28, '', 0, 1, 'L');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."LOGOSNCF.png", 250, 10, 15, 13);
        $this->SetXY(90,30); $this->Cell(100,5,utf8_decode('Consumo por Dependencia'),0,1,'C');
        $this->Ln(5);

        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 6);
        $this->SetWidths(array(20, 100, 20, 20, 20, 90));
        $this->SetAligns(array('C', 'L', 'C', 'R', 'R', 'L'));
        $this->Row(array('Item',
            utf8_decode('Descripción'),
            utf8_decode('Transacción'),
            utf8_decode('Cant'),
            utf8_decode('Monto'),
            utf8_decode('Recibido Por')
        ));
        $this->Ln(1);
    }

    //Page footer
    function Footer()
    {
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        $this->Rect(5, 185, 270, 25, 'DF');
        $this->Rect(91.5, 185, 0.1, 25, 'DF');
        $this->Rect(178, 185, 0.1, 25, 'DF');
        $this->Rect(5, 190, 270, 0.1, 'DF');

        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(5, 185);
        $this->Cell(76.5, 5, utf8_decode('Preparado Por'), 0, 1, 'L', 0);
        $this->SetXY(91.5, 185);
        $this->Cell(76.5, 5, utf8_decode('Revisado Por'), 0, 1, 'L', 0);
        $this->SetXY(178, 185);
        $this->Cell(76, 5, utf8_decode('Conformado Por'), 0, 1, 'L', 0);

        $this->SetXY(5, 190);
        $this->Cell(76.5, 5, utf8_decode('T.S.U. Mariana Salazar'), 0, 1, 'L', 0);
        $this->SetXY(91.5, 190);
        $this->Cell(76.5, 5, utf8_decode('Licda. Yosmar Greham'), 0, 1, 'L', 0);
        $this->SetXY(178, 190);
        $this->Cell(76, 5, utf8_decode('Licda. Rosis Requena'), 0, 1, 'L', 0);

        $this->SetXY(5, 195);
        $this->Cell(76.5, 5, utf8_decode('ASISTENTE DE PRESUPUESTO I'), 0, 1, 'C', 0);
        $this->SetXY(91.5, 195);
        $this->Cell(76.5, 5, utf8_decode('DIRECTORA ADMINISTRACION Y SERVICIOS'), 0, 1, 'C', 0);
        $this->SetXY(178, 195);
        $this->Cell(76, 5, utf8_decode('DIRECTORA GENERAL'), 0, 1, 'C', 0);

    }
}
