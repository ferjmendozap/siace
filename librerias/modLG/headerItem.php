<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{
    function Header()
    {

        $this->SetTitle(utf8_decode('Listado de Items'));
        $this->Image('publico/imagenes/logos/CES.png', 10, 10, 22);
        $this->SetFont('Arial','B',8);
        $this->SetXY(35,12); $this->Cell(100,5,utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'),0,1,'L');
        $this->SetXY(35,16); $this->Cell(100,5,utf8_decode('CONTRALORIA DEL ESTADO SUCRE'),0,1,'L');
        $this->SetXY(35,20); $this->Cell(100,5,utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'),0,1,'L');
        ## 
        $this->SetXY(160,12); $this->Cell(25,5,utf8_decode('Fecha: ' . date('d-m-Y')),0,1,'L');
        $this->SetXY(160,17); $this->Cell(25,5,utf8_decode('Página: ' . $this->PageNo() . ' de {nb}'),0,1,'L');
        $this->SetXY(40, 10); $this->Cell(100, 20, '', 0, 1, 'L');
        $this->SetXY(42, 10); $this->Cell(20, 28, '', 0, 1, 'L');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."LOGOSNCF.png", 190, 10, 15, 13);
        $this->SetXY(80,30); $this->Cell(58,5,utf8_decode('Listado de Items'),0,1,'C');
        $this->Ln(5);
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 6);
        $this->SetWidths(array(15, 20, 50, 15, 31, 18, 13, 13, 13, 15, 15));
        $this->SetAligns(array('C', 'C', 'L', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C'));
        $this->Row(array(
            'Item',
            '# Interno',
            'Descripcion',
            'Und.',
            'Tipo',
            'Linea',
            'Familia',
            'S.Familia',
            'Partida',
            'Cta. Gasto'
        ));
        $this->Ln(1);

    }

    //Page footer
    function Footer()
    {
        /*
        $this->SetY(-20);
        $this->Line(10,204.5,345,204.5);
        $this->SetY(-15);
        $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'R');
        */
    }
}
