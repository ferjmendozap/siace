<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{
    function Header()
    {
        $this->SetTitle('Orden');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 11, 14, 10);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(25, 10); $this->Cell(195, 5, utf8_decode(APP_ORGANISMO), 0, 0, 'L');
        $this->SetXY(25, 13);
        $this->Cell(30, 5, ('R.I.F. '.APP_ORGANISMO_RIF), 0, 0, 'L');
        $this->Cell(45, 5, ('Telefono: '.'0293-4320708/4320658'), 0, 0, 'L');
        $this->Cell(30, 5, ('Fax: '.'0293-4323447'), 0, 0, 'L');
        $this->SetXY(25, 16); $this->Cell(195, 5, utf8_decode(APP_ORGANISMO_DIRECCION), 0, 0, 'L');
        $this->SetXY(25, 19); $this->Cell(195, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->SetFont('Arial', '', 9);
        $this->SetXY(165, 11); $this->Cell(20, 5, ('Nro. Orden: '), 0, 0, 'R');
        $this->SetFont('Arial', 'B', 9);
        $this->Cell(20, 5, NRO_ORDEN);
        $this->SetFont('Arial', '', 9);
        $this->SetXY(165, 15); $this->Cell(20, 5, ('Fecha: '), 0, 0, 'R');
        $this->SetFont('Arial', 'B', 9);
        $this->Cell(20, 5, FECHA_ORDEN);
        $this->SetFont('Arial', '', 9);
        $this->SetXY(165, 19); $this->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'R');
        $this->SetFont('Arial', 'B', 9);
        $this->Cell(20, 5, $this->PageNo().' de {nb}');
        //------
        $this->SetFont('Arial', 'B', 12);
        $this->SetXY(10,30);
        $this->Cell(195, 10, $this->titulo, 0, 1, 'C');

        //	imprimo los datos del proveedor (solamente en la primera hoja)
        $this->Ln(10);
        if ($this->PageNo() == 1) {
            $this->SetXY(10,40);
            $this->Ln(5);
            $this->SetFillColor(245, 245, 245);
            $this->SetFont('Arial', '', 8);
            $this->Cell(30, 5, 'Proveedor: ', 0, 0, 'L', 1);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(95, 5, utf8_decode(PROVEEDOR), 0, 0, 'L');
            $this->SetFont('Arial', '', 8);
            if($this->titulo=="ORDEN DE COMPRA"){
                $this->Cell(30, 5, 'R.I.F: ', 0, 0, 'L', 1);
                $this->Cell(95, 5, PROVEEDOR_RIF, 0, 0, 'L');
            }
            $this->Ln(6);

            $this->Cell(30, 5, utf8_decode('Dirección: '), 0, 0, 'L', 1);
            $this->MultiCell(195, 3, utf8_decode(PROVEEDOR_DIRECCION), 0, 'J');
            $this->SetY($this->GetY());
            $this->Ln(6);

            $this->Cell(30, 5, ('S.N.C.: '), 0, 0, 'L', 1);
            $this->Cell(95, 5, PROVEEDOR_SNC, 0, 0, 'L');
            $this->Ln(6);
            $this->Cell(30, 5, ('Lugar de Entrega: '), 0, 0, 'L', 1);
            $this->Cell(95, 5, PROVEEDOR_ENTREGA, 0, 0, 'L');

//            if($this->titulo=="ORDEN DE COMPRA"){
                $this->Cell(30, 5, utf8_decode('Teléfono: '), 0, 0, 'L', 1);
                $this->Cell(95, 5, PROVEEDOR_TELF, 0, 0, 'L');
//            } else {
//                $this->Cell(30, 5, utf8_decode('Nro. Requisición: '), 0, 0, 'L', 1);
//                $this->Cell(95, 5, NRO_REQUERIMIENTO, 0, 0, 'L');
//            }
            $this->Ln(6);
            $this->Cell(30, 5, ('Email: '), 0, 0, 'L', 1);
            $this->Cell(95, 5, utf8_decode(PROVEEDOR_CORREO), 0, 0, 'L');

//            if($this->titulo=="ORDEN DE COMPRA"){
                $this->Cell(30, 5, ('Modalidad de Pago: '), 0, 0, 'L', 1);
                $this->Cell(95, 5, PROVEEDOR_PAGO, 0, 0, 'L');
//            } else {
//                $this->Cell(30, 5, utf8_decode('Fecha Requisición: '), 0, 0, 'L', 1);
//                $this->Cell(95, 5, FECHA_REQUERIMIENTO, 0, 0, 'L');
//            }
            $this->Ln(6);

            $this->Cell(30, 5, utf8_decode('Descripción: '), 0, 0, 'L', 1);
            $this->MultiCell(165, 5, utf8_decode(OBSERVACIONES), 0, 'L');

            $this->SetY($this->GetY());
        } else $this->Ln(5);
/*
        if (HOJA_ANEXA=='ORDEN DE COMPRA')  {
        }*/
        //------
    }
    //	Pie de página.
    function Footer() {

        $this->SetXY(10, -40);
        $y = $this->GetY();
        $this->SetXY(10, $y);
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        $this->Rect(10, $y, 195, 30, "DF");
        $this->Rect(75, $y, 0.1, 30, "DF");
        $this->Rect(145, $y, 0.1, 30, "DF");
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(10, 240); $this->Cell(65, 5, 'APROBADO POR', 0, 0, 'L');
        $this->SetXY(75, 240); $this->Cell(65, 5, 'CONFORMADO POR', 0, 0, 'L');
        $this->SetXY(145, 240); $this->Cell(65, 5, 'PROVEEDOR', 0, 0, 'L');
        $this->SetXY(10, 244); $this->MultiCell(65, 5, utf8_decode(substr(NOMBRE_APRUEBA, 0, 35)), 0, 'L');
        $this->SetXY(75, 244); $this->MultiCell(65, 5, utf8_decode(substr(NOMBRE_CONFORMA, 0, 35)), 0, 'L');
        $this->SetXY(145, 244); $this->Cell(65, 5, '', 0, 0, 'L');
        $this->SetXY(10, 252); $this->MultiCell(65, 5, utf8_decode(substr(CARGO_APRUEBA, 0, 35)), 0, 'C');
        $this->SetXY(75, 252); $this->MultiCell(65, 5, utf8_decode(substr(CARGO_CONFORMA, 0, 35)), 0, 'C');
        $this->SetXY(145, 252); $this->Cell(65, 5, '', 0, 0, 'L');
        $this->SetFont('Arial', 'B', 6);
        $this->SetXY(10, 265); $this->Cell(65, 5, 'Fecha:             /                 /', 0, 0, 'L');
        $this->SetXY(75, 265); $this->Cell(65, 5, 'Fecha:             /                 /', 0, 0, 'L');
        $this->SetXY(145, 265); $this->Cell(65, 5, 'Fecha:            /                 /', 0, 0, 'L');
    }
}
