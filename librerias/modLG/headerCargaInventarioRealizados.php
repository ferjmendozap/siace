<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{
    function Header()
    {
        $this->SetTitle('InventarioRealizado-'.ANIO.'-'.NUM.'.pdf');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 5, 5, 20, 12);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(25, 10); $this->Cell(190, 5,utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->SetXY(25, 15); $this->Cell(190, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 1, 'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(190, 10, utf8_decode('Cotejamiento de Inventario Nro. '.ANIO.'-'.NUM), 0, 1, 'C');
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(200, 200, 200); $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 6);
        $this->SetWidths(array(15, 80, 25, 20, 20, 20, 20));
        $this->SetAligns(array('C', 'L', 'C','C', 'R', 'C','C'));

        $this->Row(array(
            'ITEM',
            'DESCRIPCION',
            'UND.',
            'COD. INTERNO.',
            'STOCK SISTEMA',
            utf8_decode('STOCK FÍSICO'),
            'DIFERENCIA'
        ));
        $this->Ln(1);
    }

    //Page footer
    function Footer()
    {
        $this->SetFont('Arial', '', 8);
        $this->SetXY(10, 260);
        $this->Cell(190, 5, '_____________________________________________', 0, 1, 'J');
        $this->Cell(190, 5, utf8_decode('Elaborado por:     '.NOM_COMPLETO), 0, 1, 'J');
        $this->Cell(190, 5, utf8_decode('Fecha de Registro: '.FECHA), 0, 1, 'J');

    }
}
