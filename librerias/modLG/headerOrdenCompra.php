<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{
    function Header(
        //    $fdesde,$fhasta
    )
    {
        $this->SetTitle('Stock de Orden Compra');
        $this->Image('publico/imagenes/logos/CES.png', 8, 8, 22);
        $this->SetFont('Arial','B',8);
        $this->SetXY(30,10); $this->Cell(100,5,utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'),0,1,'L');
        $this->SetXY(30,14); $this->Cell(100,5,utf8_decode('CONTRALORIA DEL ESTADO SUCRE'),0,1,'L');
        $this->SetXY(30,18); $this->Cell(100,5,utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'),0,1,'L');
        ## 
        $this->SetXY(163,12); $this->Cell(25,5,utf8_decode('Fecha: ' . date('d-m-Y')),0,1,'L');
        $this->SetXY(163,17); $this->Cell(25,5,utf8_decode('Página: ' . $this->PageNo() . ' de {nb}'),0,1,'L');
        $this->SetXY(40, 10); $this->Cell(100, 20, '', 0, 1, 'L');
        $this->SetXY(42, 10); $this->Cell(20, 28, '', 0, 1, 'L');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."LOGOSNCF.png", 190, 10, 17, 15);
        //$this->SetXY(90,30); $this->Cell(100,5,utf8_decode('Ordenes de Compras'),0,1,'C');
        $this->SetXY(5, 20); $this->Cell(195, 18, utf8_decode('Ordenes de Compras'), 0, 1, 'C');

        $this->SetFont('Arial', 'B', 7);
        $this->SetXY(5, 22); $this->Cell(195, 18, utf8_decode(' Preparado Desde: '.DESDE.' Hasta:  '.HASTA), 0, 1, 'L');
        $this->Ln(2);

        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 7);
        $this->SetWidths(array(17, 93, 20, 13, 13, 15, 15, 20));
        $this->SetAligns(array('C', 'L', 'C', 'R', 'R', 'C', 'C', 'C'));
        $this->Row(array(utf8_decode('Código'),
            utf8_decode('Descripción'),
            'Uni.',
            'Cant. Pedida',
            'Cant. Recibida',
            'Fecha de Entrega',
            'Dias Atrasados',
            'Estado'));
        $this->Ln(1);
    }

    //Page footer
    function Footer()
    {

        $this->Ln(3);
        $y = $this->GetY();
        $this->Cell(30, 6,'        ______________________________________                                                                                                               ', 0, 1, 'L');
        $this->SetXY(125, $y);
        $this->Cell(30, 6,'        ______________________________________                                                                                                               ', 0, 1, 'L');

        $this->SetFont('Arial', '', 7);
        $this->SetXY(10, $y+5);
        $this->Cell(120, 3, ('Elaborado Por:'), 0, 0, 'L');
        $this->Cell(120, 3, ('Revisado Por:'), 0, 1, 'L');
        $this->SetXY(10, $y+8);
        $this->Cell(120, 3, utf8_decode('NomCompleto'), 0, 0, 'L');//nombre de quien elabora
        $this->Cell(120, 3, utf8_decode('NomCompleto'), 0, 1, 'L');//nombre de quien revisa
        $this->SetXY(10, $y+11);
        $this->Cell(120, 3, utf8_decode('DescripCargo'), 0, 0, 'L');//cargo de quien elabora
        $this->Cell(120, 3, utf8_decode('DescripCargo'), 0, 1, 'L');//Cargo de quien revisa

//$pdf->Rect(10, $y+20, 70, 0.1, "DF");
        $this->SetXY(5, $y+16);
        $this->Cell(30, 6,'        _______________________________________                                                                                                _______________________________________', 0, 1, 'L');
        $this->SetFont('Arial', '', 7);
        $this->SetXY(10, $y+21);
        $this->Cell(120, 3, ('Conformado Por:'), 0, 0, 'L');
        $this->Cell(120, 3, ('Aprobado Por:'), 0, 1, 'L');
        $this->SetXY(10, $y+24);
        $this->Cell(120, 3, utf8_decode('NomCompleto'), 0, 0, 'L');//nombre de quien conforma
        $this->Cell(120, 3, utf8_decode('NomCompleto'), 0, 1, 'L');//nombre de quien aprueba
        $this->SetXY(10, $y+27);
        $this->Cell(120, 3, utf8_decode('DescripCargo'), 0, 0, 'L');//cargo de quien conforma
        $this->Cell(120, 3, utf8_decode('DescripCargo'), 0, 1, 'L');//Cargo de quien aprueba
    }
}
