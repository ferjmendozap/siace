<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{
    function Header()
    {
        $this->SetFillColor(255, 255, 255);
        $this->SetDrawColor(0, 0, 0);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 5, 5, 24, 12);
        $this->SetFont('Arial', '', 8);
        $this->SetXY(22, 5); $this->Cell(100, 5, APP_ORGANISMO, 0, 1, 'L');
        $this->SetXY(22, 10); $this->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->SetFont('Arial', 'B', 10);
        $this->SetXY(10, 20); $this->Cell(260, 5, utf8_decode('Ingreso de Activo Fijo N° '.NRO_INTERNO), 0, 1, 'C', 0);
        $this->Ln(10);
        $this->SetFont('Arial', '', 8); $this->Cell(20, 5, utf8_decode('NroOrden: '), 0, 0, 'L', 1);
        $this->SetFont('Arial', 'B', 8); $this->Cell(75, 5, ANIO_ORDEN.'-'.NRO_ORDEN, 0, 0, 'L');
        $this->Ln(6);

        $this->SetFont('Arial', '', 8);
        $this->Cell(20, 5, 'Proveedor: ', 0, 0, 'L', 1);
        $this->Cell(75, 5, utf8_decode(NOM_PROV), 0, 0, 'L');
        $this->Ln(6);

        $this->SetTextColor(0, 0, 0);
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        $this->SetFont('Arial', 'B', 8);
        $this->SetWidths(array(10, 20, 130, 20, 30, 30, 20));
        $this->SetAligns(array('C', 'C', 'L', 'C', 'C', 'C', 'C', 'L'));
        $this->Row(array('#',
            'Commodity',
            'Descripcion',
            'Nro. Serie',
            'Modelo',
            'Marca',
            'Color'));
        $this->Ln(1);
    }

    //Page footer
    function Footer()
    {
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(0, 0, 0);
        $this->Rect(20, 188, 65, 0.1, "DF");
        $this->SetXY(20, 190); $this->Cell(65, 4, 'ALMACEN', 0, 0, 'C');
        $this->Rect(195, 188, 65, 0.1, "DF");
        $this->SetXY(195, 190); $this->Cell(65, 4, 'RECIBI CONFORME', 0, 0, 'C');
    }
}
