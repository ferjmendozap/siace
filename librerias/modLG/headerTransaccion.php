<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{
    function Header()
    {
        $this->SetFillColor(255, 255, 255);
        $this->SetDrawColor(0, 0, 0);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 5, 5, 24, 12);
        $this->SetFont('Arial', '', 8);
        $this->SetXY(25, 5); $this->Cell(100, 5, APP_ORGANISMO, 0, 1, 'L');
        $this->SetXY(25, 10); $this->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->SetFont('Arial', 'B', 10);
        $this->SetXY(10, 20); $this->Cell(200, 5, utf8_decode('Nota de '.NOTA.' N° '.NRO_INTERNO), 0, 1, 'C', 0);
        $this->Ln(10);
        if(NOTA=='Ingreso'){
            $nombCant1 = 'Compra';
            $nombCant2 = 'Ingreso';
        } else {
            if(IC=='Commodity'){
                $this->SetFont('Arial', '', 8); $this->Cell(20, 5, utf8_decode('N.I. Referida: '), 0, 0, 'L', 1);
                $this->SetFont('Arial', 'B', 8); $this->Cell(75, 5, NRO_INTERNO_REFERENCIA, 0, 0, 'L');
                $this->Ln();
            } elseif (IC=='Item'){
                $this->SetFont('Arial', '', 8); $this->Cell(20, 5, utf8_decode('Solicitado Por: '), 0, 0, 'L', 1);
                $this->SetFont('Arial', 'B', 8); $this->Cell(75, 5, DEP_SOLICITANTE, 0, 0, 'L');
                $this->Ln();
            }

            $nombCant1 = 'Requerida';
            $nombCant2 = 'Salida';
        }

        ##	primera pagina solamente
        $this->SetFont('Arial', '', 8); $this->Cell(20, 5, utf8_decode('Transacción: '), 0, 0, 'L', 1);
        $this->SetFont('Arial', 'B', 8); $this->Cell(75, 5, FEC_DOC.'   '.utf8_decode(NOM_TRANSACCION), 0, 0, 'L');
        $this->Ln(6);

        $this->SetFont('Arial', '', 8); $this->Cell(20, 5, 'Documento: ', 0, 0, 'L', 1);
        if (FEC_CREACION1 != "la fecha es incorrecta") {
            $Documento = FEC_CREACION1.' OC-'.NRO_DOC_REF;
        } elseif (FEC_CREACION2 != "la fecha es incorrecta") {
            $Documento = FEC_CREACION2.' RQ-'.NRO_DOC_REF;
        } else {
            $Documento = NRO_DOC_REF;
        }
        $this->SetFont('Arial', 'B', 8); $this->Cell(75, 5, $Documento, 0, 0, 'L');



        $this->Ln(6);

        $this->SetFont('Arial', '', 8); $this->Cell(20, 5, 'Referencia', 0, 0, 'L', 1);
        $this->SetFont('Arial', 'B', 8); $this->Cell(75, 5, NRO_DOC_REF, 0, 0, 'L');

        if (NOM_PROV != "") {
            $this->SetXY(110, 35);
            $this->SetFont('Arial', '', 8); $this->Cell(15, 5, 'Proveedor: ', 0, 0, 'L', 1);
            $this->SetFont('Arial', 'B', 8); $this->Cell(90, 5, utf8_decode(NOM_PROV), 0, 0, 'L');

            $this->SetXY(110, 41);
            $this->SetFont('Arial', '', 8);
            $this->Cell(15, 5, 'R.I.F.: ', 0, 0, 'L', 1);
            $this->Cell(30, 5, DOC_PROV, 0, 0, 'L');
            $this->Cell(15, 5, utf8_decode('Teléfono: '), 0, 0, 'L', 1);
            $this->Cell(45, 5, TELF1, 0, 0, 'L');

            $this->SetXY(110, 47);
            $this->SetFont('Arial', '', 8);
            $this->Cell(15, 5, utf8_decode('Dirección: '), 0, 0, 'L', 1);
            $this->MultiCell(90, 5, utf8_decode(DIRECCION), 0, 'L');
//            $this->Cell(30, 5, utf8_decode(DIRECCION), 0, 0, 'L');
        }
        $this->Ln();

        $this->SetFont('Arial', '', 8); $this->Cell(20, 5, utf8_decode('Tipo Doc.: '), 0, 0, 'L', 1);
        $this->SetFont('Arial', 'B', 8); $this->Cell(80, 5, utf8_decode(TIPO_DOCUMENTO), 0, 0, 'L');

        $this->SetFont('Arial', '', 8); $this->Cell(15, 5, utf8_decode('Núm. Doc.: '), 0, 0, 'L', 1);
        $this->SetFont('Arial', 'B', 8); $this->Cell(90, 5, utf8_decode(NRO_DOCUMENTO), 0, 0, 'L');


        $this->Ln(6);
        $this->SetFont('Arial', '', 8);
        $this->Cell(20, 5, 'Comentarios: ', 0, 0, 'L', 1);
        $this->MultiCell(180, 5, utf8_decode(COMENTARIO), 0, 'L');
        $this->Ln(1);


        $this->SetTextColor(0, 0, 0);
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        $this->SetFont('Arial', 'B', 8);
        $this->SetWidths(array(10, 25, 20, 70, 20, 18, 18, 18));
        $this->SetAligns(array('C', 'C', 'C', 'L', 'C', 'R', 'R', 'R'));
        $this->Row(array('#',
            'Codigo Interno',
            IC,
            utf8_decode('Descripción'),
            'Unidad '.$nombCant1,
            'Cant. '.$nombCant1,
            'Cant. '.$nombCant2,
            'Cant. Pendiente'));
        $this->Ln(1);
    }

    //Page footer
    function Footer()
    {


        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(0, 0, 0);
        $this->Rect(20, 243, 65, 0.1, "DF");
        if(NOTA!='Ingreso'){
            $this->SetXY(20, 245); $this->Cell(65, 4, utf8_decode('Despachado por:'), 0, 0, 'J');
            $this->SetXY(20, 250); $this->Cell(65, 4, utf8_decode(NOM_DESPACHO), 0, 0, 'C');
            $this->SetXY(20, 255); $this->Cell(65, 4, utf8_decode(CARGO_DESPACHO), 0, 0, 'C');
            $this->Rect(130, 243, 65, 0.1, "DF");
            $this->SetXY(130, 245); $this->Cell(65, 4, utf8_decode('Aprobado por:'), 0, 0, 'J');
            $this->SetXY(130, 250); $this->Cell(65, 4,  utf8_decode(NOM_APRUEBA), 0, 0, 'C');
            $this->SetXY(130, 255); $this->Cell(65, 4, utf8_decode(CARGO_APRUEBA), 0, 0, 'C');
        } else {
            $this->SetXY(20, 245); $this->Cell(65, 4, utf8_decode('Recepcionado por:'), 0, 0, 'J');
            $this->SetXY(20, 250); $this->Cell(65, 4, utf8_decode(NOM_DESPACHO), 0, 0, 'C');
            $this->SetXY(20, 255); $this->Cell(65, 4, utf8_decode(CARGO_DESPACHO), 0, 0, 'C');
        }

    }
}
