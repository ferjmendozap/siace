<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | José A Pereda        | dt.ait.programador2@cgesucre.gob.ve  | 04248040078
 * | * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once LIBRERIA_FPDF;



class pdfReporteVehiculo extends FPDF
{
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPA/logo.png', 10, 10, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('REPORTE DE VEHÍCULOS DE LA CONTRALORÍA'),0,1,'C');
        $this->Ln(4);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}
class pdfReporteInstitucion extends FPDF
{
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPA/logo.png', 10, 10, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('REPORTE DE ENTRADA Y SALIDA DE VEHÍCULOS DE LA INSTITUCIÓN'),0,1,'C');
        $this->Ln(4);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}
class pdfReporteEntradaTaller extends FPDF
{
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPA/logo.png', 10, 10, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('REPORTE DE ENTRADA DE VEHÍCULOS AL TALLER'),0,1,'C');
        $this->Ln(4);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}
class pdfReporteSalidaTaller extends FPDF
{
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPA/logo.png', 10, 10, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('REPORTE DE SALIDA DE VEHÍCULOS AL TALLER'),0,1,'C');
        $this->Ln(4);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}



class pdfReporteChofer  extends FPDF
{
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPA/logo.png', 10, 10, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('REPORTE DE CHOFERES DE LA CONTRALORÍA'),0,1,'C');
        $this->Ln(4);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}

class pdfReporteMantenimiento  extends FPDF
{
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPA/logo.png', 10, 10, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('REPORTE DE MANTENIMIENTO'),0,1,'C');
        $this->Ln(4);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}

class pdfReporteDannio extends FPDF
{
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPA/logo.png', 10, 10, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('REPORTE DE DAÑOS'),0,1,'C');
        $this->Ln(4);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}

class pdfReporteAsignaciones extends FPDF
{
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPA/logo.png', 10, 10, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('REPORTE DE ASIGNACIONES'),0,1,'C');
        $this->Ln(4);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}
class pdfReporteDisponibilidad extends FPDF
{

    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPA/logo.png', 10, 10, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('REPORTE DE DISPONIBILIDAD'),0,1,'C');
        $this->Ln(4);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }


}

class pdfReporteSolicitud extends FPDF
{
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPA/logo.png', 10, 10, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('REPORTE DE SOLICITUDES'),0,1,'C');
        $this->Ln(4);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}