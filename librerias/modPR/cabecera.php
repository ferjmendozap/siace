<?php
require_once LIBRERIA_FPDF;

class pdfReporte extends FPDF
{
     var $organismo;
	 var $logo;
	 var $logo_secundario;
	 
    //Page header
    function Header()
    {
	   
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPR/'.$this->logo, 10, 10, 22);
		 $this->Image('publico/imagenes/modPR/'.$this->logo_secundario, 240, 8, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(150, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(150, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(150, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
      //  $this->Cell(250, 5, utf8_decode('REPORTE DE PRESUPUESTO'),0,1,'C');
        $this->Ln(4);
    }
    
	//Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(175,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}


class pdfReporteAntepresupuesto extends FPDF
{
     var $organismo;
	 var $logo;
	 var $logo_secundario;
	 
    //Page header
    function Header()
    {
	    //$this->Cell(110,69,utf8_decode($this->organismo), 0, 1, 'L');
	
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPR/'.$this->logo, 10, 10, 22);
	    $this->Image('publico/imagenes/modPR/'.$this->logo_secundario, 240, 8, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(150, 5,utf8_decode($this->organismo), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(150, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(150, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('REPORTE DE ANTEPRESUPUESTO'),0,1,'C');
        $this->Ln(4);
    }
	
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(130,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}


class pdfReporteEjecucion extends FPDF
{
      var $organismo;
	  var $logo;
	  var $logo_secundario;  
    //Page header
    function Header()
    {
        $this->Cell(110,69); $this->Image('publico/imagenes/modPR/'.$this->logo, 10, 10, 22);
	    $this->Image('publico/imagenes/modPR/'.$this->logo_secundario, 300, 8, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( $this->organismo), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('REPORTE DE EJECUCIÓN PRESUPUESTARIA'),0,1,'C');
        $this->Ln(4);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}


class pdfReporteEjecucionDetallada extends FPDF
{
      var $organismo;
	  var $logo;
	  var $logo_secundario;  
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPR/'.$this->logo, 10, 10, 22);
		$this->Image('publico/imagenes/modPR/'.$this->logo_secundario, 400, 8, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(300, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(300, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(300, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(400, 5, utf8_decode('REPORTE DE EJECUCIÓN PRESUPUESTARIA DETALLADA'),0,1,'C');
        $this->Ln(4);

        $this->SetDrawColor(255, 255, 255); $this->SetFillColor(255, 255, 255); $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 7);
        $this->Cell(25, 5, utf8_decode('COD. PRESUPUESTO:'), 1, 1, 'L', 0);
        $this->Cell(50, 5, 'DESCRIPCION', 1, 1, 'L', 0);
        $this->Cell(30, 5, 'MONTO APROBADO', 1, 1, 'L', 0);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(310,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}




class pdfReporteCompromiso extends FPDF
{
   var $organismo;
   var $logo;
   var $logo_secundario;  
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPR/'.$this->logo, 10, 10, 22);
	    $this->Image('publico/imagenes/modPR/'.$this->logo_secundario, 300, 8, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('REPORTE DE COMPROMISOS PRESUPUESTARIOS'),0,1,'C');
        $this->Ln(4);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(195,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}



class pdfReporteCausados extends FPDF
{
   var $organismo;
   var $logo;
   var $logo_secundario;  
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPR/'.$this->logo, 10, 10, 22);
		$this->Image('publico/imagenes/modPR/'.$this->logo_secundario, 300, 8, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('REPORTE DE CAUSADOS PRESUPUESTARIOS'),0,1,'C');
        $this->Ln(4);
    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(195,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}



class pdfReportePagados extends FPDF
{
    var $organismo;
    var $logo;
    var $logo_secundario;  
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPR/'.$this->logo, 10, 10, 22);
		$this->Image('publico/imagenes/modPR/'.$this->logo_secundario, 300, 8, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('REPORTE DE PAGADOS PRESUPUESTARIOS'),0,1,'C');
        $this->Ln(4);
    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}


class pdfReporteIncremento extends FPDF
{
    var $organismo;
    var $logo;
    var $logo_secundario;  
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPR/'.$this->logo, 10, 10, 22);
		$this->Image('publico/imagenes/modPR/'.$this->logo_secundario, 300, 8, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(300, 5, utf8_decode('REPORTE DE INCREMENTOS PRESUPUESTARIOS'),0,1,'C');
        $this->Ln(4);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(195,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}


class pdfReporteDisminucion extends FPDF
{
    var $organismo;
    var $logo;
    var $logo_secundario;  
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPR/'.$this->logo, 10, 10, 22);
		$this->Image('publico/imagenes/modPR/'.$this->logo_secundario, 300, 8, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(300, 5, utf8_decode('REPORTE DE DISMINUCIONES PRESUPUESTARIAS'),0,1,'C');
        $this->Ln(4);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(195,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}




class pdfReporteAjuste extends FPDF
{
    var $organismo;
    var $logo;
    var $logo_secundario;  
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPR/logo.jpg', 10, 10, 22);
		$this->Image('publico/imagenes/modPR/'.$this->logo_secundario, 183, 8, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(100, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
		
        $this->Cell(8,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(100, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(200, 5, utf8_decode('REPORTE AJUSTE PRESUPUESTARIO'),0,1,'C');
        $this->Ln(4);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(135,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,8,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
}



class pdfReporteReformulacion extends FPDF
{
   var $organismo;
   var $logo;
   var $logo_secundario;  
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPR/'.$this->logo, 10, 10, 22);
		$this->Image('publico/imagenes/modPR/'.$this->logo_secundario, 183, 8, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(100, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
		
        $this->Cell(8,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(100, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(200, 5, utf8_decode('REPORTE REFORMULACIÓN PRESUPUESTARIO'),0,1,'C');
        $this->Ln(4);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(135,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,8,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
	
	

}


class pdfReporteCierreMes extends FPDF
{
    var $organismo;
    var $logo;
    var $logo_secundario;  
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPR/'.$this->logo, 10, 10, 22);
		$this->Image('publico/imagenes/modPR/'.$this->logo_secundario, 300, 8, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
       
	   /* $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('REPORTE DE CIERRE MES'),0,1,'C');
        $this->Ln(4);
	*/
    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(195,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}



class pdfReporteEjecucionEspecifica extends FPDF
{
    var $organismo;
    var $logo;
    var $logo_secundario;
	var $estado;  
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modPR/'.$this->logo, 10, 10, 22);
		$this->Image('publico/imagenes/modPR/'.$this->logo_secundario, 188, 8, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(140, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(140, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(190, 5, utf8_decode('REPORTE EJECUCIÓN ESPECIFICA'.$this->estado),0,1,'C');
        $this->Ln(4);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(170,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}





/////////////////////////////////////////////////////////////////////////////////////
