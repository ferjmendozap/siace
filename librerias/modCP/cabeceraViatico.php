<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Fernando Mendoza                 |dt.jefe.ait@cgesucre.gob.ve         |         0424-8942068           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        16-02-2018       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{

    function encabezado()
    {
        $this->AddPage();
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 5, 13, 10);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(25, 5);
        $this->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->SetXY(25, 10);
        $this->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 1, 'L');
        $this->SetFillColor(250, 250, 250);
        $this->SetFont('Arial', 'B', 10);
        $this->Ln(5);
	      $this->Cell(200, 5,utf8_decode('Viatico Nº ').Session::metObtener('SolViatico'), 0, 0, 'C');

    }

    //Page footer
    function firma($firma)
    {

      		$this->SetXY(10, 230);
      		$this->SetDrawColor(0, 0, 0); $this->SetFillColor(0, 0, 0); $this->SetTextColor(0, 0, 0);

      		$this->SetTextColor(0, 0, 0);
      		$this->SetDrawColor(0, 0, 0);
      		$this->SetFillColor(255, 255, 255);
      		$this->SetXY(10, 240);
      		$this->Rect(10, 240, 62.66, 25, "D");
      		$this->Rect(72.66, 240, 62.66, 25, "D");
      		$this->Rect(135.32, 240, 62.66, 25, "D");

      		$this->SetFont('Arial', 'B', 8);
      		$this->Cell(62.66, 5, 'PREPARADO POR', 1, 0, 'L');
      		$this->Cell(62.66, 5, 'REVISADO POR', 1, 0, 'L');
      		$this->Cell(62.66, 5, 'APROBADO POR', 1, 0, 'L');

      		##
      		$this->SetFont('Arial', 'B', 6);
      		$this->SetXY(10, 251); $this->MultiCell(62.66, 3,utf8_decode($firma['in_f']), 0, 'L');
      		$this->SetXY(72.66, 251); $this->MultiCell(62.66, 3,utf8_decode($firma['rv_f']), 0, 'L');
      		$this->SetXY(135.32, 251); $this->MultiCell(62.66, 3, utf8_decode($firma['ap_f']), 0, 'L');

      		##
      		$this->SetXY(10, 257); $this->MultiCell(62.66, 3, utf8_decode($firma['in_c']), 0, 'C');
      		$this->SetXY(72.66, 257); $this->MultiCell(62.66, 3, utf8_decode($firma['rv_c']), 0, 'C');
      		$this->SetXY(135.32, 257); $this->MultiCell(62.66, 3, utf8_decode($firma['ap_c']), 0, 'C');
    }
}
