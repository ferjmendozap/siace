<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Gestión de Eventos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once LIBRERIA_FPDF;


class pdfGeneralEvento extends FPDF
{
        
    //Page header
    function Header(){
       $this->SetFont('Arial', 'B', 8);
    }
    function imagenCertificado($ind_certificado){
        $this->Cell(197,69); $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modEV'.DS.'fondo'.DS.$ind_certificado,5, 15,270,187);
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));

        if ($this->heights) {
            $altura1 = $this->heights[0];
            $altura2 = $this->heights[1];
            $altura3 = $this->heights[2];
        } else {
            $altura1 = 5;
            $altura2 = 4.5;
            $altura3 = 5 * $nb;
        }
        $h = $altura3;

        //Issue a page break first if needed
        $this->CheckPageBreak($h);

        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'C';

            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();

            //Draw the border
            //$this->Rect($x, $y, $w, $h, "DF");

            //Print the text
            $this->MultiCell($w, $altura2, $data[$i], 0, $a, 0);

            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }

        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfMensual extends FPDF
{
    var $widths;
    var $aligns;

    function Encabezado($valor1, $valor2, $tipoOpcion, $cadena)
    {
        $this->SetFont('Arial', 'B', 10);
        $this->setXY(10, 35);
        if($tipoOpcion==1){
            $this->Cell(260,5,utf8_decode('EVENTOS REALIZADOS EN EL MES DE '.$valor1.' DEL AÑO '.$valor2),0,0,'C',0);
        }
        if($tipoOpcion==2){
            if($valor1==1){
                $nombreValor = 'I';
            } else if($valor1==2){
                $nombreValor = 'II';
            } else if($valor1==3){
                $nombreValor = 'III';
            } else if($valor1==4){
                $nombreValor = 'IV';
            }
            $this->Cell(260,5,utf8_decode('EVENTOS REALIZADOS EN EL '.$nombreValor.' TRIMESTRE '.$cadena.' DEL AÑO '.$valor2),0,0,'C',0);
        }
        if($tipoOpcion==3){
            if($valor1==1){
                $nombreValor = 'PRIMER';
            } else {
                $nombreValor = 'SEGUNDO';
            }
            $this->Cell(260,5,utf8_decode('EVENTOS REALIZADOS DURANTE EL '.$nombreValor.' SEMESTRE '.$cadena.' DEL AÑO '.$valor2),0,0,'C',0);
        }
        $this->Ln();$this->Ln();
    }

    function Header(){
        
        Session::metAcceso();
        $ind_descripcion_empresa = Session::metObtener('INDORGANISMO');
        $logo = Session::metObtener('INDLOGO');
        $logo_secundario = Session::metObtener('INDLOGOSECUNDARIO');

        $this->SetFont('Arial', 'B', 10);
        $this->Cell(30,10) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS.$logo, 15, 10, 35);
        $this->Cell(200,5,utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'),0,0,'C',0);
        $this->Cell(40,10) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS.$logo_secundario, 240, 10, 20);
        $this->setXY(40, 15); $this->Cell(200,5,utf8_decode($ind_descripcion_empresa),0,0,'C',0);
        $this->SetFont('Arial', 'B', 8);
        $this->Ln();$this->Ln();$this->Ln();
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(229, 229, 229); $this->SetTextColor(0, 0, 0);
        $this->setXY(10, 35);
        $this->Ln();
        $this->Cell(20,5,utf8_decode('N° EVENTO'),1,0,'C',1);
        $this->Cell(55,5,utf8_decode('NOMBRE'),1,0,'C',1);
        $this->Cell(47,5,utf8_decode('LUGAR'),1,0,'C',1);
        $this->Cell(18,5,utf8_decode('FECHA I.'),1,0,'C',1);
        $this->Cell(18,5,utf8_decode('FECHA F.'),1,0,'C',1);
        $this->Cell(55,5,utf8_decode('PONENTE'),1,0,'C',1);
        $this->Cell(15,5,utf8_decode('HORA E.'),1,0,'C',1);
        $this->Cell(30,5,utf8_decode('N° PARTICIPANTES'),1,0,'C',1);
        $this->Ln();
    }
    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom

    }
    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfParticipantes extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){

        Session::metAcceso();
        $ind_descripcion_empresa = Session::metObtener('INDORGANISMO');
        $logo = Session::metObtener('INDLOGO');
        $logo_secundario = Session::metObtener('INDLOGOSECUNDARIO');

        $this->SetFont('Arial', 'B', 10);
        $this->Cell(30,10) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS.$logo, 15, 10, 35);
        $this->Cell(200,5,utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'),0,0,'C',0);
        $this->Cell(40,10) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS.$logo_secundario, 240, 10, 20);
        $this->setXY(40, 15); $this->Cell(200,5,utf8_decode($ind_descripcion_empresa),0,0,'C',0);
        $this->SetFont('Arial', 'B', 12);
        $this->Ln(); $this->Ln(); $this->Ln();
        $this->Cell(260,5,'EVENTOS Y PARTICIPANTES',0,0,'C',0);
        $this->Ln();
    }
    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom

    }
    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfLugares extends FPDF
{
    var $widths;
    var $aligns;

    function Header()
    {
        Session::metAcceso();
        $ind_descripcion_empresa = Session::metObtener('INDORGANISMO');
        $logo = Session::metObtener('INDLOGO');
        $logo_secundario = Session::metObtener('INDLOGOSECUNDARIO');


        $this->SetFont('Arial', 'B', 10);
        $this->Cell(30, 10);
        $this->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . $logo, 15, 10, 35);
        $this->Cell(200, 5, utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'), 0, 0, 'C', 0);
        $this->Cell(40, 10);
        $this->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . $logo_secundario, 240, 10, 20);
        $this->setXY(40, 15);
        $this->Cell(200, 5, utf8_decode($ind_descripcion_empresa), 0, 0, 'C', 0);
        $this->SetFont('Arial', 'B', 12);
        $this->Ln();
        $this->Ln();
        $this->Ln();
        $this->Cell(260, 5, 'EVENTOS', 0, 0, 'C', 0);
        $this->SetFont('Arial', 'B', 8);
        $this->Ln();$this->Ln();$this->Ln();
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(229, 229, 229); $this->SetTextColor(0, 0, 0);
        $this->setXY(10, 35);
        $this->Ln();
        $this->Cell(20,5,utf8_decode('N° EVENTO'),1,0,'C',1);
        $this->Cell(65,5,utf8_decode('NOMBRE'),1,0,'C',1);
        $this->Cell(65,5,utf8_decode('LUGAR'),1,0,'C',1);
        $this->Cell(18,5,utf8_decode('FECHA I.'),1,0,'C',1);
        $this->Cell(18,5,utf8_decode('FECHA F.'),1,0,'C',1);
        $this->Cell(30,5,utf8_decode('DURACION'),1,0,'C',1);
        $this->Cell(40,5,utf8_decode('TIPO DE EVENTO'),1,0,'C',1);
    }

    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom

    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths = $w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns = $a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++)
            $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));
        $h = 5 * $nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            //Draw the border
            $this->Rect($x, $y, $w, $h);
            //Print the text
            $this->MultiCell($w, 5, $data[$i], 0, $a);
            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if ($this->GetY() + $h > $this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w, $txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw =& $this->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;
        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n")
            $nb--;
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ')
                $sep = $i;
            $l += $cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j)
                        $i++;
                } else
                    $i = $sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            } else
                $i++;
        }
        return $nl;
    }
}
class pdfEnte extends FPDF
{
    var $widths;
    var $aligns;

    function Header()
    {
        Session::metAcceso();
        $ind_descripcion_empresa = Session::metObtener('INDORGANISMO');
        $logo = Session::metObtener('INDLOGO');
        $logo_secundario = Session::metObtener('INDLOGOSECUNDARIO');

        $this->SetFont('Arial', 'B', 10);
        $this->Cell(30, 10);
        $this->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . $logo, 15, 10, 35);
        $this->Cell(200, 5, utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'), 0, 0, 'C', 0);
        $this->Cell(40, 10);
        $this->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . $logo_secundario, 240, 10, 20);
        $this->setXY(40, 15);
        $this->Cell(200, 5, utf8_decode($ind_descripcion_empresa), 0, 0, 'C', 0);
        $this->SetFont('Arial', 'B', 12);
        $this->Ln();
        $this->Ln();
        $this->Ln();
        $this->Cell(260, 5, 'EVENTOS POR TIPO DE ENTE', 0, 0, 'C', 0);
        $this->SetFont('Arial', 'B', 8);
        $this->Ln();$this->Ln();$this->Ln();
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(229, 229, 229); $this->SetTextColor(0, 0, 0);
        $this->setXY(10, 35);
        $this->Ln();
        $this->Cell(20,5,utf8_decode('N° EVENTO'),1,0,'C',1);
        $this->Cell(75,5,utf8_decode('NOMBRE'),1,0,'C',1);
        $this->Cell(18,5,utf8_decode('FECHA I.'),1,0,'C',1);
        $this->Cell(18,5,utf8_decode('FECHA F.'),1,0,'C',1);
        $this->Cell(20,5,utf8_decode('DURACION'),1,0,'C',1);
        $this->Cell(50,5,utf8_decode('TIPO DE EVENTO'),1,0,'C',1);
        $this->Cell(50,5,utf8_decode('TIPO DE ENTE'),1,0,'C',1);
    }

    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom

    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths = $w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns = $a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++)
            $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));
        $h = 5 * $nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            //Draw the border
            $this->Rect($x, $y, $w, $h);
            //Print the text
            $this->MultiCell($w, 5, $data[$i], 0, $a);
            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if ($this->GetY() + $h > $this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w, $txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw =& $this->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;
        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n")
            $nb--;
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ')
                $sep = $i;
            $l += $cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j)
                        $i++;
                } else
                    $i = $sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            } else
                $i++;
        }
        return $nl;
    }
}