<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{
    function Header()
    {

        $this->SetTitle('Stock de Items');
        $this->AliasNbPages();
        $this->SetMargins(5, 1, 1);
        $this->SetAutoPageBreak(5, 35);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 5, 5, 10, 10); $this->SetFont('Arial', '', 8);

        $this->SetFont('Arial', '', 8);
        $this->SetXY(15, 5); $this->Cell(100, 5, APP_ORGANISMO, 0, 1, 'L');
        $this->SetXY(15, 10); $this->Cell(100, 5, utf8_decode('DIVISIÓN DE ADMINISTRACIÓN Y PRESUPUESTO'), 0, 0, 'L');
        $this->SetXY(180, 5); $this->Cell(15, 5, utf8_decode('Fecha: '), 0, 0, 'R');
        $this->Cell(60, 5, date("d-m-Y"), 0, 1, 'L');
        $this->SetXY(180, 10); $this->Cell(15, 5, utf8_decode('Página: '), 0, 0, 'R');
        $this->Cell(60, 5, $this->PageNo().' de {nb}', 0, 1, 'L');
        $this->SetFont('Arial', 'B', 9);
        $this->SetXY(5, 20); $this->Cell(195, 5, utf8_decode('Listado de Stock'), 0, 1, 'C', 0);
        $this->Ln(5);

        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 6);
        $this->SetWidths(array(15, 20, 90, 20, 20, 20, 20));
        $this->SetAligns(array('C', 'C', 'L', 'C', 'R', 'R', 'R'));
        $this->Row(array('Item',
            'Cod. Interno',
            'Descripcion',
            'Und.',
            'Stock Actual',
            'Comprometido',
            'Stock Disponible'));
        $this->Ln(1);
    }

    //Page footer
    function Footer()
    {
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        $this->Rect(5, 245, 205, 25, 'DF');
        $this->Rect(73.5, 245, 0.1, 25, 'DF');
        $this->Rect(142, 245, 0.1, 25, 'DF');
        $this->Rect(5, 250, 205, 0.1, 'DF');

        $this->SetFont('Arial', 'B', 8);

        $this->SetXY(5, 245);
        $this->Cell(68.5, 5, utf8_decode('Preparado Por'), 0, 1, 'L', 0);
        $this->SetXY(73.5, 245);
        $this->Cell(68.5, 5, utf8_decode('Revisado Por'), 0, 1, 'L', 0);
        $this->SetXY(142, 245);
        $this->Cell(68, 5, utf8_decode('Conformado Por'), 0, 1, 'L', 0);

        $this->SetXY(5, 250);
        $this->Cell(68.5, 5, utf8_decode("NOMBRE_USUARIO_ACTUAL"), 0, 1, 'L', 0);
        $this->SetXY(73.5, 250);
        $this->Cell(68.5, 5, utf8_decode('Gabriela Guevara'), 0, 1, 'L', 0);
        $this->SetXY(142, 250);
        $this->Cell(68, 5, utf8_decode('Sorielma Salmerón '), 0, 1, 'L', 0);

        $this->SetXY(5, 255);
        $this->Cell(68.5, 5, utf8_decode('cargoPreparadoPor'), 0, 1, 'C', 0);
        $this->SetXY(73.5, 255);
        $this->Cell(68.5, 5, utf8_decode('ANALISTA DE ADMINISTRACIÓN I'), 0, 1, 'C', 0);
        $this->SetXY(142, 255);
        $this->Cell(68, 5, utf8_decode('DIRECTOR DE  ADMINISTRACION Y PRESUPUESTO (E)'), 0, 1, 'C', 0);

    }
}
