<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{
    function Header()
    {

        $this->SetTitle(utf8_decode('Listado de Items'));
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 5, 5, 10, 10);
        $this->SetFont('Arial', '', 8);
        $this->SetXY(15, 5); $this->Cell(100, 5, APP_ORGANISMO, 0, 1, 'L');
        $this->SetXY(15, 10); $this->Cell(100, 5, utf8_decode('DIVISIÓN DE ADMINISTRACIÓN Y PRESUPUESTO'), 0, 0, 'L');
        $this->SetXY(180, 5); $this->Cell(15, 5, utf8_decode('Fecha: '), 0, 0, 'R');
        $this->Cell(60, 5, date("d-m-Y"), 0, 1, 'L');
        $this->SetXY(180, 10); $this->Cell(15, 5, utf8_decode('Página: '), 0, 0, 'R');
        $this->Cell(60, 5, $this->PageNo().' de {nb}', 0, 1, 'L');
        $this->SetFont('Arial', 'B', 9);
        $this->SetXY(5, 20);
        $this->Cell(195, 5, utf8_decode('Listado de Items'), 0, 1, 'C', 0);
        $this->Ln(5);
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 6);
        $this->SetWidths(array(15, 20, 50, 15, 31, 18, 13, 13, 13, 15, 15));
        $this->SetAligns(array('C', 'C', 'L', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C'));
        $this->Row(array(
            'Item',
            '# Interno',
            'Descripcion',
            'Und.',
            'Tipo',
            'Linea',
            'Familia',
            'S.Familia',
            'Partida',
            'Cta. Gasto'
        ));
        $this->Ln(1);

    }

    //Page footer
    function Footer()
    {
        /*
        $this->SetY(-20);
        $this->Line(10,204.5,345,204.5);
        $this->SetY(-15);
        $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'R');
        */
    }
}
