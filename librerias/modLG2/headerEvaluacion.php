<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{
    function Header()
    {
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 5, 5, 10, 10);
        $this->SetFont('Arial', '', 8);
        $this->SetXY(15, 5); $this->Cell(100, 5, APP_ORGANISMO, 0, 1, 'L');
        $this->SetXY(15, 10); $this->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN Y PRESUPUESTO (UNIDAD CONTRATANTE)'), 0, 0, 'L');
        $this->SetXY(230, 5); $this->Cell(15, 5, utf8_decode('Fecha: '), 0, 0, 'R');
        $this->Cell(60, 5, FECHA, 0, 1, 'L');
        $this->SetXY(230, 10); $this->Cell(15, 5, utf8_decode('Página: '), 0, 0, 'R');
        $this->Cell(60, 5, $this->PageNo().' de {nb}', 0, 1, 'L');
        $this->SetFont('Arial', 'B', 9);
        $this->SetXY(5, 20); $this->Cell(260, 5, utf8_decode('MATRIZ FINANCIERA POR RENGLONES Y PRECIOS UNITARIOS '), 0, 1, 'C');
        $this->SetXY(5, 20); $this->Cell(260, 15, utf8_decode('Evaluación Nro. '.NUM_EVAL), 0, 1, 'C');


    }

    //Page footer
    function Footer()
    {
        $this->SetY(200);
        $y=$this->GetY();
        $this->SetXY(20, 195); $this->Cell(80, 4, '______________________________________________', 0, 1, 'C');
        $this->SetXY(20, 200); $this->Cell(80, 4, utf8_decode(NOMBRE_ASIS_A), 0, 1, 'C');
        $this->SetXY(20, 204); $this->Cell(80, 4, utf8_decode(CARGO_ASIS_A), 0, 1, 'C');
        ##
        $this->SetY(200);
        $y=$this->GetY();
        $this->SetXY(150, 195); $this->Cell(80, 4, '______________________________________________', 0, 1, 'C');
        $this->SetXY(150, 200); $this->Cell(80, 4, utf8_decode(NOMBRE_ASIS_B), 0, 1, 'C');
        $this->SetXY(150, 204); $this->Cell(80, 4, utf8_decode(CARGO_ASIS_B), 0, 1, 'C');
        ##
        $this->SetY(200);
        $y=$this->GetY();
        $this->SetXY(260, 195); $this->Cell(80, 4, '______________________________________________', 0, 1, 'C');
        $this->SetXY(260, 200); $this->Cell(80, 4, utf8_decode(NOMBRE_ASIS_C), 0, 1, 'C');
        $this->SetXY(260, 204); $this->Cell(80, 4, utf8_decode(CARGO_ASIS_C), 0, 1, 'C');

    }
}
