<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{
    function Header()
    {

        $this->SetTitle('Procedimiento de Seleccion de Contratistas');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 5, 5, 10, 10);
        $this->SetFont('Arial', '', 8);
        $this->SetXY(15, 5); $this->Cell(100, 5, APP_ORGANISMO, 0, 1, 'L');
        $this->SetXY(15, 10); $this->Cell(100, 5, utf8_decode('Dirección de Administración y Presupuesto'), 0, 0, 'L');
        $this->SetXY(230, 5); $this->Cell(15, 5, utf8_decode('Fecha: '), 0, 0, 'R');
        $this->Cell(60, 5, date("d-m-Y"), 0, 1, 'L');
        $this->SetXY(230, 10); $this->Cell(15, 5, utf8_decode('Página: '), 0, 0, 'R');
        $this->Cell(60, 5, $this->PageNo().' de {nb}', 0, 1, 'L');

        $this->SetFont('Arial', 'B', 9);
        $this->SetXY(5, 20); $this->Cell(270, 5, utf8_decode('Procedimiento de Selección de Contratistas'), 0, 1, 'C');
        $this->Cell(195, 5, utf8_decode('Desde: '.DESDE.'  Hasta: '.HASTA), 0, 1, 'L');

        $this->Ln(5);
        $this->SetDrawColor(255, 255, 255);
        $this->SetFillColor(200, 200, 200);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 7);
        $this->SetWidths(array(25, 40, 85, 35,20, 30, 30));
        $this->SetAligns(array('C', 'C', 'C', 'C','C', 'C'));
        $this->Row(array(utf8_decode('Nº de Requerimiento'),
            utf8_decode('Nº de Procedimiento'),
            utf8_decode('Nombre de Procedimiento'),
            utf8_decode('Nº de Acta de inicio'),
            utf8_decode('Fecha de Acta de inicio'),
            utf8_decode('Estado')));
        $this->Ln(1);
        $this->Ln(1);

    }

    //Page footer
    function Footer()
    {
        /*
        $this->SetY(-20);
        $this->Line(10,204.5,345,204.5);
        $this->SetY(-15);
        $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'R');
        */
    }
}
