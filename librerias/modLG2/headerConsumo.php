<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{
    function Header()
    {
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 5, 5, 10, 10); $this->SetFont('Arial', '', 8);

        $this->SetFont('Arial', '', 8);
        $this->SetXY(15, 5); $this->Cell(100, 5, APP_ORGANISMO, 0, 1, 'L');
        $this->SetXY(15, 10); $this->Cell(100, 5, utf8_decode('DIVISIÓN DE ADMINISTRACIÓN Y PRESUPUESTO'), 0, 0, 'L');

        $this->SetXY(230, 5); $this->Cell(15, 5, utf8_decode('Fecha: '), 0, 0, 'R');
        $this->Cell(60, 5, date("d-m-Y"), 0, 1, 'L');
        $this->SetXY(230, 10); $this->Cell(15, 5, utf8_decode('Página: '), 0, 0, 'R');
        $this->Cell(60, 5, $this->PageNo().' de {nb}', 0, 1, 'L');

        $this->SetFont('Arial', 'B', 9);
        $this->SetXY(5, 20); $this->Cell(270, 5, utf8_decode('Consumo por Dependencia'), 0, 1, 'C');
        $this->Ln(5);

        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 6);
        $this->SetWidths(array(20, 100, 20, 20, 20, 90));
        $this->SetAligns(array('C', 'L', 'C', 'R', 'R', 'L'));
        $this->Row(array('Item',
            utf8_decode('Descripción'),
            utf8_decode('Transacción'),
            utf8_decode('Cant'),
            utf8_decode('Monto'),
            utf8_decode('Recibido Por')
        ));
        $this->Ln(1);
    }

    //Page footer
    function Footer()
    {
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        $this->Rect(5, 185, 270, 25, 'DF');
        $this->Rect(91.5, 185, 0.1, 25, 'DF');
        $this->Rect(178, 185, 0.1, 25, 'DF');
        $this->Rect(5, 190, 270, 0.1, 'DF');

        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(5, 185);
        $this->Cell(76.5, 5, utf8_decode('Preparado Por'), 0, 1, 'L', 0);
        $this->SetXY(91.5, 185);
        $this->Cell(76.5, 5, utf8_decode('Revisado Por'), 0, 1, 'L', 0);
        $this->SetXY(178, 185);
        $this->Cell(76, 5, utf8_decode('Conformado Por'), 0, 1, 'L', 0);

        $this->SetXY(5, 190);
        $this->Cell(76.5, 5, utf8_decode('T.S.U. Mariana Salazar'), 0, 1, 'L', 0);
        $this->SetXY(91.5, 190);
        $this->Cell(76.5, 5, utf8_decode('Licda. Yosmar Greham'), 0, 1, 'L', 0);
        $this->SetXY(178, 190);
        $this->Cell(76, 5, utf8_decode('Licda. Rosis Requena'), 0, 1, 'L', 0);

        $this->SetXY(5, 195);
        $this->Cell(76.5, 5, utf8_decode('ASISTENTE DE PRESUPUESTO I'), 0, 1, 'C', 0);
        $this->SetXY(91.5, 195);
        $this->Cell(76.5, 5, utf8_decode('DIRECTORA ADMINISTRACION Y SERVICIOS'), 0, 1, 'C', 0);
        $this->SetXY(178, 195);
        $this->Cell(76, 5, utf8_decode('DIRECTORA GENERAL'), 0, 1, 'C', 0);

    }
}
