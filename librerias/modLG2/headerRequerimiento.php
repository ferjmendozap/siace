<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdf extends FPDF
{

    function Header()
    {
        $this->SetTitle(utf8_decode('Ubicacion por Almacen'));
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 5, 5, 12, 12);
        $this->SetFont('Arial', '', 8);
        $this->SetXY(20, 5);
        $this->Cell(100, 5, APP_ORGANISMO, 0, 1, 'L');
        $this->SetXY(20, 10);
        $this->Cell(100, 5, utf8_decode("DIRECCIÓN DE ADMINISTRACIÓN"));
        $this->Ln(10);

        $this->SetFillColor(250, 250, 250);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(200, 5, utf8_decode('Requerimiento Nº '.COD_REQ), 0, 0, 'C');
        $this->Ln(10);
    }

    function primeraPagina()
    {
        $this->SetFillColor(250, 250, 250);
        $this->SetFont('Arial', '', 8);
        $this->Cell(25, 5, 'Dependencia: ', 0, 0, 'L', 1);
        $this->Cell(175, 5, utf8_decode(DEPENDENCIA), 0, 0, 'L');
        $this->Ln(6);
        $this->Cell(25, 5, 'Centro de Costos: ', 0, 0, 'L', 1);
        $this->Cell(70, 5, utf8_decode(CENTRO_COSTO), 0, 0, 'L');
        $this->Ln(6);
        $this->Cell(25, 5, utf8_decode('Clasificación: '), 0, 0, 'L', 1);
        $this->Cell(75, 5, utf8_decode(CLASIFICACION), 0, 0, 'L');
        $this->Cell(25, 5, 'Dirigido A: ', 0, 0, 'L', 1);
        $this->Cell(70, 5, utf8_decode(TIPO), 0, 0, 'L');
        $this->Ln(6);
        $this->Cell(25, 5, 'Prioridad: ', 0, 0, 'L', 1);
        $this->Cell(75, 5, utf8_decode(PRIORIDAD), 0, 0, 'L');
        $this->Cell(25, 5, 'Fecha Requerida: ', 0, 0, 'L', 1);
        $this->Cell(70, 5, FECHA_REQUERIDA, 0, 0, 'L');
        $this->Ln(6);
        $this->Cell(25, 5, 'Estado: ', 0, 0, 'L', 1);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(75, 5, ESTADO, 0, 0, 'L');
        $this->SetFont('Arial', '', 8);
        $this->Ln(6);
        $this->Cell(25, 5, 'Comentarios: ', 0, 0, 'L', 1);
        $this->MultiCell(175, 4, utf8_decode( COMENTARIOS), 0, 'J');
        $this->Ln(5);
    }
    function campos()
    {
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 8);
        $this->SetWidths(array(10, 20, 120, 20, 20));
        $this->SetAligns(array('R', 'C', 'L', 'C', 'R'));
        $this->Row(array('#', IC, 'DESCRIPCION', 'UNI.', 'CANT.'));
        $this->Ln(1);
    }

    //Page footer
    function Footer()
    {

        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(0, 0, 0);
        $this->SetTextColor(0, 0, 0);
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 7);
        $this->SetY(225);
        $this->Rect(20, 223, 65, 0.1, "DF");
        $this->SetXY(20, 225); $this->Cell(20, 4, 'Solicitado Por:', 0, 0, 'R'); $this->Cell(80, 4, NOMBRE_PREPARADO, 0, 1, 'L');
        $this->SetXY(20, 229); $this->Cell(20, 4, 'Cargo:', 0, 0, 'R'); $this->Cell(80, 4, CARGO_PREPARADO, 0, 1, 'L');
        $this->SetXY(20, 233); $this->Cell(20, 4, 'Fecha:', 0, 0, 'R'); $this->Cell(80, 4, FECHA_PREPARADO, 0, 1, 'L');
        ##
        $this->SetY(225);
        $this->Rect(130, 223, 65, 0.1, "DF");
        $this->SetXY(130, 225); $this->Cell(20, 4, 'Revisado Por:', 0, 0, 'R'); $this->Cell(80, 4, NOMBRE_REVISADO, 0, 1, 'L');
        $this->SetXY(130, 229); $this->Cell(20, 4, 'Cargo:', 0, 0, 'R'); $this->Cell(80, 4, CARGO_REVISADO, 0, 1, 'L');
        $this->SetXY(130, 233); $this->Cell(20, 4, 'Fecha:', 0, 0, 'R'); $this->Cell(80, 4, FECHA_REVISADO, 0, 1, 'L');
        ##
        $this->SetY(250);
        $this->Rect(20, 248, 65, 0.1, "DF");
        $this->SetXY(20, 250); $this->Cell(20, 4, 'Conformado Por:', 0, 0, 'R'); $this->Cell(80, 4, NOMBRE_CONFORMADO, 0, 1, 'L');
        $this->SetXY(20, 254); $this->Cell(20, 4, 'Cargo:', 0, 0, 'R'); $this->Cell(80, 4, CARGO_CONFORMADO, 0, 1, 'L');
        $this->SetXY(20, 258); $this->Cell(20, 4, 'Fecha:', 0, 0, 'R'); $this->Cell(80, 4, FECHA_CONFORMADO, 0, 1, 'L');
        ##
        $this->SetY(250);
        $this->Rect(130, 248, 65, 0.1, "DF");
        $this->SetXY(130, 250); $this->Cell(20, 4, 'Aprobado Por:', 0, 0, 'R'); $this->Cell(80, 4, NOMBRE_APROBADO, 0, 1, 'L');
        $this->SetXY(130, 254); $this->Cell(20, 4, 'Cargo:', 0, 0, 'R'); $this->Cell(80, 4, CARGO_APROBADO, 0, 1, 'L');
        $this->SetXY(130, 258); $this->Cell(20, 4, 'Fecha:', 0, 0, 'R'); $this->Cell(80, 4, FECHA_APROBADO, 0, 1, 'L');
    }
}
