<?php
require_once APP_PATH.'control'.DS.'db.php';

class Select
{
    /**
     * Mostrar un lista de valores para un select.
     *
     * @param  String  $tabla
     * @param  String  $id
     * @param  String  $nombre
     * @param  String  $valor
     * @param  Integer  $opt
     * @param  Array   $campos
     * @return String
     */
    public static function lista($tabla, $id, $nombre, $valor=NULL, $opt=0, $campos=array(), $codigo=NULL)
    {
        $whereCampos = '';
        $data = '';
        foreach($campos AS $key => $value)
        {
            if (!$whereCampos) {
                $whereCampos .= "WHERE $key = '$value'";
            } else {
                $whereCampos .= " AND $key = '$value'";
            }
        }
        if ($valor)
        {
            if ($opt == 1)
            {
                if (!$whereCampos) {
                    $whereCampos .= "WHERE $id = '$valor'";
                } else {
                    $whereCampos .= " AND $id = '$valor'";
                }
            }
        }

        $db = new db();
        $query = $db->query("SELECT $id AS id, $nombre AS nombre".($codigo?", $codigo AS codigo":"")." FROM $tabla $whereCampos ORDER BY ".($codigo?"$codigo":"$nombre"));
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $field = $query->fetchAll();

        foreach ($field as $f) {
            $selected = (($valor == $f['id'])?'selected':'');
            $data .= '<option value="'.$f['id'].'" '.$selected.'>'.($codigo?$f['codigo'].' - ':'').$f['nombre'].'</option>';
        }

        return $data;
    }

    /**
     * Mostrar un lista de valores para un select (miscelaneo).
     *
     * @param  String  $cod_maestro
     * @param  String  $valor
     * @param  Integer  $opt
     * @param  Array   $campos
     * @return String
     */
    public static function miscelaneo($cod_maestro, $valor=NULL, $opt=0, $campos=array())
    {
        $db = new db();

        $query_maestro = $db->query("SELECT * FROM a005_miscelaneo_maestro WHERE cod_maestro = '$cod_maestro'");
        $query_maestro->setFetchMode(PDO::FETCH_ASSOC);
        $field_maestro = $query_maestro->fetch();

        $whereCampos = '';
        $data = '';
        foreach($campos AS $key => $value)
        {
            $whereCampos .= " AND $key = '$value'";
        }

        $query = $db->query("SELECT pk_num_miscelaneo_detalle AS id, ind_nombre_detalle AS nombre, cod_detalle AS codigo FROM a006_miscelaneo_detalle WHERE fk_a005_num_miscelaneo_maestro = '$field_maestro[pk_num_miscelaneo_maestro]' $whereCampos ORDER BY nombre");
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $field = $query->fetchAll();

        switch ($opt)
        {
            case 0:
                foreach ($field as $f) {
                    $selected = (($valor == $f['id'])?'selected':'');
                    $data .= '<option value="'.$f['id'].'" '.$selected.'>'.$f['nombre'].'</option>' ;
                }
                break;

            case 2:
                foreach ($field as $f) {
                    if ($valor == $f['codigo']) $data = $f['id'];
                }
                break;

            case 3:
                foreach ($field as $f) {
                    if ($valor == $f['id']) $data = $f['nombre'];
                }
                break;
        }

        return $data;
    }

    /**
     * Mostrar un lista de valores para un select (miscelaneo).
     *
     * @param  String  $cod_maestro
     * @param  String  $valor
     * @param  Integer  $opt
     * @return String
     */
    public static function options($cod_maestro, $valor=NULL, $opt=0)
    {
        switch ($cod_maestro)
        {
            case 'contabilidades':
                $v['F'] = 'Fiscal Publicación';
                $v['T'] = 'Balance General';
                break;

            case 'signo':
                $v['+'] = '+';
                $v['-'] = '-';
                break;

            case 'af_tipo_transaccion':
                $v['B'] = 'Baja';
                $v['A'] = 'Alta';
                break;

            case 'af_estado_activo':
                $v['PE'] = 'Pendiente por Activar';
                $v['AP'] = 'Activado';
                break;

            case 'af_tipo_movimiento':
                $v['I'] = 'Interno';
                $v['E'] = 'Externo';
                break;

            case 'af_estado_movimiento':
                $v['PR'] = 'En Preparación';
                $v['RV'] = 'Revisado';
                $v['AP'] = 'Aprobado';
                $v['AN'] = 'Anulado';
                break;

			case 'nivel_cuenta':
                 $v['1'] = "Grupo";
                 $v['2'] = "Sub-Grupo";
                 $v['3'] = "Rubro";
                 $v['4'] = "Cuenta";
                 $v['5'] = "Sub-Cuenta de Primer Orden";
                 $v['6'] = "Sub-Cuenta de Segundo Orden";
                 $v['7'] = "Sub-Cuenta Anexa";
                 break;

            case 'tipo_registro':
                 $v['AB'] = "Periodo Abierto";
                 $v['AC'] = "Periodo Actual";
                 break;

            case 'af_estado_baja':
                $v['PR'] = 'En Preparación';
                $v['RV'] = 'Revisado';
                $v['AP'] = 'Aprobado';
                $v['AN'] = 'Anulado';
                break;

			case 'ReportBalanceComprobacion':
                 $v['AC'] = "Acumulado";
                 $v['SAC'] = "Saldo Anterior Acumulado";
                 $v['DH'] = "Debe - Haber";
                 $v['DHAC'] = "Debe - Haber Acumulado";
                 break;

            case 'Tipo_Cuenta':
                 $v['1'] = "Publicación 20";
                 $v['2'] = "Oncop";
                 break;

            case 'af_estado_faltante':
                $v['PR'] = 'En Preparación';
                $v['RV'] = 'Revisado';
                $v['AP'] = 'Aprobado';
                $v['TR'] = 'Tramitado';
                $v['AN'] = 'Anulado';
                break;

            case 'af_estado_faltante_detalle':
                $v['PE'] = 'Pendiente';
                $v['EI'] = 'En Investigación';
                $v['FP'] = 'Faltante en Pérdida';
                $v['FE'] = 'Faltante Encontrado';
                break;

            case 'af_estado_faltante_detallet':
                $v['EI'] = 'En Investigación';
                $v['FP'] = 'Faltante en Pérdida';
                $v['FE'] = 'Faltante Encontrado';
                break;

            case 'af_situacion_faltante':
                $v['PE'] = 'Pendiente';
                $v['EI'] = 'En Investigación';
                $v['CO'] = 'Completado';
                break;
        }

        $data = '';
        switch ($opt)
        {
            case 0:
                foreach ($v as $key => $value) {
                    $selected = (($valor == $key)?'selected':'');
                    $data .= '<option value="'.$key.'" '.$selected.'>'.$value.'</option>' ;
                }
                break;

            case 1:
                foreach ($v as $key => $value) {
                    if ($valor == $key) $data .= '<option value="'.$key.'" selected>'.$value.'</option>' ;
                }
                break;

            case 2:
                $data = $v[$valor];
                break;
        }

        return $data;
    }

    /**
     * Obtener parámetros.
     *
     * @param  Array   $campos
     * @return String
     */
    public static function parametros($clave=NULL)
    {
        $db = new db();

        $whereCampos = '';
        $data = '';
        if ($clave) $whereCampos .= " AND ind_parametro_clave = '$clave'";

        $query = $db->query("SELECT ind_parametro_clave AS clave, ind_valor_parametro AS valor FROM a035_parametros WHERE 1 $whereCampos ORDER BY clave");
        $query->setFetchMode(PDO::FETCH_ASSOC);
        if ($clave) {
            $field = $query->fetch();
            $data = $field['valor'];
        } else {
            $field = $query->fetchAll();
            foreach ($field as $f) {
                $key = $f['clave'];
                $data[$key] = $f['valor'];
            }
        }

        return $data;
    }

    /**
     * Obtener el valor de un campo de una tabla.
     *
     * @param  String  $tabla
     * @param  String  $campo
     * @param  Array   $campos
     * @return String
     */
    public static function field($tabla, $campo, $campos)
    {
        $db = new db();

        $whereCampos = '';
        $data = '';
        foreach($campos AS $key => $value)
        {
            $whereCampos .= " AND $key = '$value'";
        }

        $sql = "SELECT $campo AS valor FROM $tabla WHERE 1 $whereCampos LIMIT 0, 1";
        $row = $db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        $field = $row->fetch();

        return $field['valor'];
    }

    /* Mod CB */
    public static function lista2($tabla, $id, $nombre, $id2, $valor=NULL, $opt=0, $campos=array(), $codigo=NULL)
    {
        $whereCampos = '';
        $data = '';
        foreach($campos AS $key => $value)
        {
            if (!$whereCampos) {
                $whereCampos .= "WHERE $key = '$value'";
            } else {
                $whereCampos .= " AND $key = '$value'";
            }
        }
        if ($valor)
        {
            if ($opt == 1)
            {
                if (!$whereCampos) {
                    $whereCampos .= "WHERE $id2 = '$valor'";
                } else {
                    $whereCampos .= " AND $id2 = '$valor'";
                }
            }
        }

        $db = new db();
        $query = $db->query("SELECT $id AS id, $nombre AS nombre".($codigo?", $codigo AS codigo":"")." FROM $tabla $whereCampos ORDER BY ".($codigo?"$codigo":"$nombre"));
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $field = $query->fetchAll();

        foreach ($field as $f) {
            $selected = (($valor == $f['id'])?'selected':'');
            $data .= '<option value="'.$f['id'].'" '.$selected.'>'.($codigo?$f['codigo'].' - ':'').$f['nombre'].'</option>';
        }

        return $data;
    }

    public static function lista3($tabla, $id, $nombre, $id2, $valor=NULL, $opt=0, $campos=array(), $codigo=NULL)
    {
        $whereCampos = '';
        $data = '';
        foreach($campos AS $key => $value)
        {
            if (!$whereCampos) {
                $whereCampos .= "WHERE $key = '$value'";
            } else {
                $whereCampos .= " AND $key = '$value'";
            }
        }
        if ($valor)
        {
            if ($opt == 1)
            {
                if (!$whereCampos) {
                    $whereCampos .= "WHERE $id2 = '$valor'";
                } else {
                    $whereCampos .= " AND $id2 = '$valor'";
                }
            }
        }

        $db = new db();
        $query = $db->query("SELECT distinct($nombre) AS nombre FROM $tabla $whereCampos ORDER BY ".($codigo?"$codigo":"$nombre"));
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $field = $query->fetchAll();

        foreach ($field as $f) {
            $selected = (($valor == $f['nombre'])?'selected':'');
            $data .= '<option value="'.$f['nombre'].'" '.$selected.'>'.($codigo?$f['codigo'].' - ':'').$f['nombre'].'</option>';
        }

        return $data;
    }
}
