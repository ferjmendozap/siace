<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Gestion de Contrato
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | José A Pereda        | dt.ait.programador2@cgesucre.gob.ve  | 04248040078
 * | * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once LIBRERIA_FPDF;



class pdfReporteGestion extends FPDF
{
    //Page header
    function Header()
    {
        $this->Ln(3);
        $this->SetFont('Arial', 'B', 9);
        $this->Cell(0, 5,utf8_decode( 'REPÚBLICA BOLIVARIANA DE VENEZUELA'), 0, 0, 'C');
        $this->Ln(5);
        $this->Cell(0, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'C');

        $this->Image('publico/imagenes/modGC/logo.png', 10, 10, 22);
        $this->Image('publico/imagenes/modGC/logo.png', 180, 10, 22);
        $this->Ln(20);
    }


}


class pdfReporteProyectoContrato extends FPDF
{
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modGC/logo.png', 10, 10, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN TÉCNICA'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('REPORTE DE PROYECTOS CONTRATOS'),0,1,'C');
        $this->Ln(20);


    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}

class pdfReporteContrato extends FPDF
{
    //Page header
    function Header()
    {
        $this->Cell(110,69) ; $this->Image('publico/imagenes/modGC/logo.png', 10, 10, 22);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN TÉCNICA'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('REPORTE DE CONTRATOS'),0,1,'C');
        $this->Ln(24);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}
