<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT . 'librerias' . DS . 'modCD' . DS . 'tcpdf' . DS . 'tcpdf.php';
//require_once LIBRERIA_FPDF;

class pdf extends TCPDF
{
    function Header()
    {


        $this->SetFillColor(255, 255, 255);
        $this->SetDrawColor(0, 0, 0);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 25, 18, 38, 20);
        $this->SetFont('Helvetica', ' ', 11);
        $this->SetXY(80, 12); $this->Cell(60, 22, 'REPÚBLICA BOLIVARIANA DE VENEZUELA', 0, 1, 'C');
        $this->SetXY(80, 12); $this->Cell(60, 30, APP_ORGANISMO, 0, 1, 'C');
        $this->SetXY(60, 29); $this->MultiCell(100, 5,DEPENDENCIA_REMITENTE, 0, 'C');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."LOGOSNCF.jpg", 170, 18, 20, 18);
        $this->Ln(5);

    }

    //Page footer
    function Footer()
    {

        //Position at 1.5 cm from bottom
        $this->SetXY(227,24.5);
        //Arial italic 8

        $this->SetFont('Helvetica','',7);
        //Page number


        $this->SetFont('Helvetica','',6);
        //Page number
      //  $this->SetXY(95, 200); $this->Cell(35, 115, '_________________________________________________________________________________________________________________', 0, 1, 'C', 0);

        $this->SetFont('Helvetica','I',8);
        //Page number
        $this->SetXY(95, 200); $this->Cell(35, 100, '
        Dirección: Avenida Arismendi, Edificio Palacio Legislativo, Cumaná, Edo. Sucre Teléfonos: (0293) 4320708, 4323658. Directo: Tele Fax 4323447 
        ', 0, 1, 'C', 0);

        $this->SetXY(95, 200); $this->Cell(35, 105, '
Correo Electrónico: despacho@cgesucre.gob.ve, www.cgesucre.gob.ve
        ', 0, 1, 'C', 0);
        $this->SetFont('Helvetica','BI',9);
        $this->SetXY(95, 201); $this->Cell(35, 110, '"Todos los Sucrenses Somos Contralores"', 0, 1, 'C', 0);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."52Anv.jpg", 12, 254, 21, 12);

        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
}

class circular extends TCPDF
{
    function Header()
    {


        $this->SetFillColor(255, 255, 255);
        $this->SetDrawColor(0, 0, 0);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 25, 18, 38, 20);
        $this->SetFont('Helvetica', ' ', 11);
        $this->SetXY(80, 12); $this->Cell(60, 22, 'REPÚBLICA BOLIVARIANA DE VENEZUELA', 0, 1, 'C');
        $this->SetXY(80, 12); $this->Cell(60, 30, APP_ORGANISMO, 0, 1, 'C');
        $this->SetXY(63, 30); $this->MultiCell(100, 10,DEPENDENCIA_REMITENTE, 0, 'C');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."LOGOSNCF.jpg", 170, 18, 10, 18);
        $this->Ln(5);

    }

    //Page footer
    function Footer()
    {

        //Position at 1.5 cm from bottom
        $this->SetXY(227,24.5);
        //Arial italic 8

        $this->SetFont('Helvetica','',7);
        //Page number


        $this->SetFont('Helvetica','',6);
        //Page number
     //   $this->SetXY(95, 200); $this->Cell(35, 115, '_________________________________________________________________________________________________________________', 0, 1, 'C', 0);

        $this->SetFont('Helvetica','I',6);
        //Page number
        $this->SetXY(95, 200); $this->Cell(35, 100, '
        Dirección: Avenida Arismendi, Edificio Palacio Legislativo, Cumaná, Edo. Sucre Teléfonos: (0293) 4320708, 4323658. Directo: Tele Fax 4323447 
        ', 0, 1, 'C', 0);

        $this->SetXY(95, 200); $this->Cell(35, 105, '
Correo Electrónico: despacho@cgesucre.gob.ve, www.cgesucre.gob.ve
        ', 0, 1, 'C', 0);
        $this->SetFont('Helvetica','BI',9);
        $this->SetXY(95, 201); $this->Cell(35, 110, '"Todos los Sucrenses Somos Contralores"', 0, 1, 'C', 0);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."52Anv.jpg", 12, 254, 21, 12);

        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
}
