<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                |i.lezama@contraloriamonagas.gob.ve   |        04249371200          |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT . 'librerias' . DS . 'modCD' . DS . 'tcpdf' . DS . 'tcpdf.php';
//require_once LIBRERIA_FPDF;

class pdf extends TCPDF
{
    function Header()
    {


        $this->SetFillColor(255, 255, 255);
        $this->SetDrawColor(0, 0, 0);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES_ESCALA_GRISES.jpg", 90, 18, 35, 20);
        $this->SetFont('Helvetica', ' ', 11);
        $this->SetXY(80, 30); $this->Cell(60, 22, 'REPÚBLICA BOLIVARIANA DE VENEZUELA', 0, 1, 'C');
        $this->SetXY(80, 30); $this->Cell(60, 30, APP_ORGANISMO, 0, 1, 'C');
        $this->SetXY(80, 30); $this->Cell(60, 38,'DESPACHO DEL CONTRALOR', 0, 1, 'C');





    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,24.5);
        //Arial italic 8

        $this->SetFont('Helvetica','',7);
        //Page number


     //   $this->SetXY(95, 200); $this->Cell(35, 115, utf8_decode('_________________________________________________________________________________________________________________'), 0, 1, 'C', 0);

        $this->SetFont('Helvetica','I',8);
        //Page number
        $this->SetXY(48, 252); $this->MultiCell(115, 100, 'Dirección: Avenida Arismendi, Edificio Palacio Legislativo, Cumaná, estado Sucre, Teléfonos: (0293) 4320708, 4323658. Directo: Tele Fax 4323447. Correo Electrónico: despacho@cgesucre.gob.ve, Zona Postal: 6101,  Rif. G20001224-2. Sitio web: www.cgesucre.gob.ve, twitter: @CESucre.
        ',0, 'J');
        $this->SetFont('Helvetica','BI',9);
        $this->SetXY(95, 213); $this->Cell(35, 110, '"Todos los Sucrenses Somos Contralores"', 0, 1, 'C', 0);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."52Anv.jpg", 25, 254, 21, 12);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."SNCF_GRIS.jpg", 167, 254, 17, 18);

        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
}
