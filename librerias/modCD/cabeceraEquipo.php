<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Reporte de Diistribucion de Documentos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867297           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once LIBRERIA_FPDF;







class pdfMemorandum extends FPDF
{

		 var $B=0;
    var $I=0;
    var $U=0;
    var $HREF='';
    var $ALIGN='';


    function WriteHTML($html)
    {
        // Intérprete de HTML
        $html = str_replace("\n",' ',$html);
        $a = preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
        foreach($a as $i=>$e)
        {
            if($i%2==0)
            {
                // Text
                if($this->HREF)
                    $this->PutLink($this->HREF,$e);
                else
                    $this->Write(5,$e);
            }
            else
            {
                // Etiqueta
                if($e[0]=='/')
                    $this->CloseTag(strtoupper(substr($e,1)));
                else
                {
                    // Extraer atributos
                    $a2 = explode(' ',$e);
                    $tag = strtoupper(array_shift($a2));
                    $attr = array();
                    foreach($a2 as $v)
                    {
                        if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                            $attr[strtoupper($a3[1])] = $a3[2];
                    }
                    $this->OpenTag($tag,$attr);
                }
            }
        }
    }

    function OpenTag($tag, $attr)
    {
        // Etiqueta de apertura
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,true);
        if($tag=='A')
            $this->HREF = $attr['HREF'];
        if($tag=='BR')
            $this->Ln(5);
    }

    function CloseTag($tag)
    {
        // Etiqueta de cierre
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,false);
        if($tag=='A')
            $this->HREF = '';
    }

    function SetStyle($tag, $enable)
    {
        // Modificar estilo y escoger la fuente correspondiente
        $this->$tag += ($enable ? 1 : -1);
        $style = '';
        foreach(array('B', 'I', 'U') as $s)
        {
            if($this->$s>0)
                $style .= $s;
        }
        $this->SetFont('',$style);
    }

    function PutLink($URL, $txt)
    {
        // Escribir un hiper-enlace
        $this->SetTextColor(0,0,255);
        $this->SetStyle('U',true);
        $this->Write(5,$txt,$URL);
        $this->SetStyle('U',false);
        $this->SetTextColor(0);
    }


    function Dependencia($dependencia)
    {
        $this->SetXY(73, 10);
      //  $this->Cell(28,5,'DEPENDENCIA:',0,0,'L',0);
        $this->SetFont('Arial', '', 9);
        $this->Cell(100,37,'',0,0,'L',0);
      //  $this->SetFont('Arial', 'B', 10);
     //   $this->SetXY(10, 46);
    }
	
    //Page header
    function Header()
    {


        $this->SetFillColor(255, 255, 255);
        $this->SetDrawColor(0, 0, 0);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 30, 15, 20, 15);
        $this->SetFont('Arial', '', 9);
		$this->SetXY(70, 10); $this->Cell(100, 20, '', 0, 1, 'L');
        $this->SetXY(72, 10); $this->Cell(20, 28, '', 0, 1, 'L');

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,24.5);
        //Arial italic 8

        $this->SetFont('Arial','',6);
        //Page number
        $this->SetXY(95, 200); $this->Cell(35, 110, utf8_decode('Hacia la transparencia, fortalecimiento y consolidación del Sistema Nacional de Control Fiscal'), 0, 1, 'C', 0);

        $this->SetFont('Arial','',6);
        //Page number
        $this->SetXY(95, 200); $this->Cell(35, 115, utf8_decode('_________________________________________________________________________________________________________________'), 0, 1, 'C', 0);

        $this->SetFont('Arial','',6);
        //Page number
        $this->SetXY(95, 200); $this->Cell(35, 120, utf8_decode('
        Dirección: Calle Sucre c/c Monagas, Edificio sede de la CONTRALORÍA DEL ESTADO SUCRE, Maturín, Teléfono:0291-641-0441 - 6432713 
        '), 0, 1, 'C', 0);

        $this->SetXY(95, 200); $this->Cell(35, 125, utf8_decode('
Correo Electrónico: contraloriamonagas@contraloriamonagas.gob.ve, www.contraloriamonagas.gob.ve
        '), 0, 1, 'C', 0);


        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}


class pdfCredencial extends FPDF
{

	var $B=0;
    var $I=0;
    var $U=0;
    var $HREF='';
    var $ALIGN='';

    function WriteHTML($html)
    {
        //HTML parser
        $html=str_replace("\n",' ',$html);
        $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
        foreach($a as $i=>$e)
        {
            if($i%2==0)
            {
                //Text
                if($this->HREF)
                    $this->PutLink($this->HREF,$e);
                elseif($this->ALIGN=='center')
                    $this->Cell(0,5,$e,0,1,'C');
                else
                    $this->Write(5,$e);
            }
            else
            {
                //Tag
                if($e[0]=='/')
                    $this->CloseTag(strtoupper(substr($e,1)));
                else
                {
                    //Extract properties
                    $a2=explode(' ',$e);
                    $tag=strtoupper(array_shift($a2));
                    $prop=array();
                    foreach($a2 as $v)
                    {
                        if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                            $prop[strtoupper($a3[1])]=$a3[2];
                    }
                    $this->OpenTag($tag,$prop);
                }
            }
        }
    }

    function OpenTag($tag,$prop)
    {
        //Opening tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,true);
        if($tag=='A')
            $this->HREF=$prop['HREF'];
        if($tag=='BR')
            $this->Ln(5);
        if($tag=='P')
            $this->ALIGN=$prop['ALIGN'];
        if($tag=='HR')
        {
            if( !empty($prop['WIDTH']) )
                $Width = $prop['WIDTH'];
            else
                $Width = $this->w - $this->lMargin-$this->rMargin;
            $this->Ln(2);
            $x = $this->GetX();
            $y = $this->GetY();
            $this->SetLineWidth(0.4);
            $this->Line($x,$y,$x+$Width,$y);
            $this->SetLineWidth(0.2);
            $this->Ln(2);
        }
    }

    function CloseTag($tag)
    {
        //Closing tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,false);
        if($tag=='A')
            $this->HREF='';
        if($tag=='P')
            $this->ALIGN='';
    }

    function SetStyle($tag,$enable)
    {
        //Modify style and select corresponding font
        $this->$tag+=($enable ? 1 : -1);
        $style='';
        foreach(array('B','I','U') as $s)
            if($this->$s>0)
                $style.=$s;
        $this->SetFont('',$style);
    }

    function PutLink($URL,$txt)
    {
        //Put a hyperlink
        $this->SetTextColor(0,0,255);
        $this->SetStyle('U',true);
        $this->Write(5,$txt,$URL);
        $this->SetStyle('U',false);
        $this->SetTextColor(0);
    }
	
		 function Dependencia($dependencia)
    {
        $this->SetXY(73, 10);
      //  $this->Cell(28,5,'DEPENDENCIA:',0,0,'L',0);
        $this->SetFont('Arial', '', 9);
        $this->Cell(100,37,utf8_decode($dependencia),0,0,'L',0);
      //  $this->SetFont('Arial', 'B', 10);
     //   $this->SetXY(10, 46);
    }
	
    //Page header
    function Header()
    {


        $this->SetFillColor(255, 255, 255);
        $this->SetDrawColor(0, 0, 0);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 30, 15, 20, 15);
        $this->SetFont('Arial', '', 9);
        $this->SetXY(70, 10); $this->Cell(100, 20, '', 0, 1, 'L');
        $this->SetXY(72, 10); $this->Cell(20, 28, '', 0, 1, 'L');

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,24.5);
        //Arial italic 8

        $this->SetFont('Arial','',6);
        //Page number
        $this->SetXY(95, 200); $this->Cell(35, 110, utf8_decode('Hacia la transparencia, fortalecimiento y consolidación del Sistema Nacional de Control Fiscal'), 0, 1, 'C', 0);

        $this->SetFont('Arial','',6);
        //Page number
        $this->SetXY(95, 200); $this->Cell(35, 115, utf8_decode('
        Dirección: Calle Sucre c/c Monagas, Edificio sede de la CONTRALORÍA DEL ESTADO SUCRE, Maturín, Teléfono:0291-641-0441 - 6432713 
        '), 0, 1, 'C', 0);

        $this->SetXY(95, 200); $this->Cell(35, 120, utf8_decode('
Correo Electrónico: contraloriamonagas@contraloriamonagas.gob.ve, www.contraloriamonagas.gob.ve
        '), 0, 1, 'C', 0);


        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}


class pdfPuntoCuenta extends FPDF
{


	var $B=0;
    var $I=0;
    var $U=0;
    var $HREF='';
    var $ALIGN='';

    function WriteHTML($html)
    {
        //HTML parser
        $html=str_replace("\n",' ',$html);
        $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
        foreach($a as $i=>$e)
        {
            if($i%2==0)
            {
                //Text
                if($this->HREF)
                    $this->PutLink($this->HREF,$e);
                elseif($this->ALIGN=='center')
                    $this->Cell(0,5,$e,0,1,'C');
                else
                    $this->Write(5,$e);
            }
            else
            {
                //Tag
                if($e[0]=='/')
                    $this->CloseTag(strtoupper(substr($e,1)));
                else
                {
                    //Extract properties
                    $a2=explode(' ',$e);
                    $tag=strtoupper(array_shift($a2));
                    $prop=array();
                    foreach($a2 as $v)
                    {
                        if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                            $prop[strtoupper($a3[1])]=$a3[2];
                    }
                    $this->OpenTag($tag,$prop);
                }
            }
        }
    }

    function OpenTag($tag,$prop)
    {
        //Opening tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,true);
        if($tag=='A')
            $this->HREF=$prop['HREF'];
        if($tag=='BR')
            $this->Ln(5);
        if($tag=='P')
            $this->ALIGN=$prop['ALIGN'];
        if($tag=='HR')
        {
            if( !empty($prop['WIDTH']) )
                $Width = $prop['WIDTH'];
            else
                $Width = $this->w - $this->lMargin-$this->rMargin;
            $this->Ln(2);
            $x = $this->GetX();
            $y = $this->GetY();
            $this->SetLineWidth(0.4);
            $this->Line($x,$y,$x+$Width,$y);
            $this->SetLineWidth(0.2);
            $this->Ln(2);
        }
    }

    function CloseTag($tag)
    {
        //Closing tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,false);
        if($tag=='A')
            $this->HREF='';
        if($tag=='P')
            $this->ALIGN='';
    }

    function SetStyle($tag,$enable)
    {
        //Modify style and select corresponding font
        $this->$tag+=($enable ? 1 : -1);
        $style='';
        foreach(array('B','I','U') as $s)
            if($this->$s>0)
                $style.=$s;
        $this->SetFont('',$style);
    }

    function PutLink($URL,$txt)
    {
        //Put a hyperlink
        $this->SetTextColor(0,0,255);
        $this->SetStyle('U',true);
        $this->Write(5,$txt,$URL);
        $this->SetStyle('U',false);
        $this->SetTextColor(0);
    }
	
		 function Dependencia($dependencia)
    {
        $this->SetXY(73, 10);
      //  $this->Cell(28,5,'DEPENDENCIA:',0,0,'L',0);
        $this->SetFont('Arial', '', 9);
       // $this->Cell(100,37,utf8_decode($dependencia),0,0,'L',0);
      //  $this->SetFont('Arial', 'B', 10);
     //   $this->SetXY(10, 46);
    }
	
    //Page header
    function Header()
    {
  		$this->SetTextColor(0, 0, 0);
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
       // $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."Logo CEB.jpg", 30, 15, 20, 15);
        $this->SetFont('Arial', '', 9);
	//	$this->SetXY(70, 10); $this->Cell(100, 20, 'REPUBLICA BOLIVARIANA DE VENEZUELA', 0, 1, 'L');
        $this->SetXY(72, 5); $this->Cell(20, 28, APP_ORGANISMO, 0, 1, 'L');
      //	$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."Logo SNCF.jpg", 170, 15, 17, 15);
	   	$this->SetXY(10, 10);
	  	$this->MultiCell(30,35, $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg",$this->GetX(), $this->GetY()+5, 30) ,1,'L');
	 	$this->SetXY(40,'10');
		$this->Cell(115, 18, '', 1, 0, 'C'); 
		$this->SetXY(40,'28');
		$this->Cell(115, 17, ' PUNTO DE CUENTA', 1, 0, 'C'); 
		
		$this->SetXY(155,'10');
		$this->SetFont('Arial', '', 8);
		$this->Cell(47, 5, utf8_decode('CÓDIGO'), 1, 0, 'C'); 
		$this->SetXY(155,'15');
		$this->Cell(47, 5, utf8_decode('FOR-CEDA-001'), 1, 0, 'C');
		$this->SetXY(155,'20');
		$this->Cell(47, 5,  utf8_decode('REVISIÓN'), 1, 0, 'C');
		$this->SetXY(177,'25');
		$this->Cell(25, 5, 'FECHA', 1, 0, 'C');
		$this->SetXY(155,'25');
		$this->Cell(22, 5, 'Nro', 1, 0, 'C');
		$this->SetXY(155,'30');
		$this->Cell(22, 5, '0', 1, 0, 'C');
		$this->Cell(25, 5, '05/2010', 1, 0, 'C');
		
		$this->SetXY(155,'35');
		$this->SetFont('Arial','',7);
		$this->Cell(47, 5, 'PAGINA', 1, 0, 'C');
		$this->SetXY(155,'40');
		$this->Cell(47, 5, ' '.$this->PageNo().'/{nb}', 1, 0, 'C');
    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,24.5);
        //Arial italic 8

        $this->SetFont('Arial','',6);
        //Page number
        $this->SetXY(95, 200); $this->Cell(35, 110, utf8_decode('Hacia la transparencia, fortalecimiento y consolidación del Sistema Nacional de Control Fiscal'), 0, 1, 'C', 0);

        $this->SetFont('Arial','',6);
        //Page number
        $this->SetXY(95, 200); $this->Cell(35, 115, utf8_decode('
        Dirección: Calle Sucre c/c Monagas, Edificio sede de la CONTRALORÍA DEL ESTADO SUCRE, Maturín, Teléfono:0291-641-0441 - 6432713 
        '), 0, 1, 'C', 0);

        $this->SetXY(95, 200); $this->Cell(35, 120, utf8_decode('
Correo Electrónico: contraloriamonagas@contraloriamonagas.gob.ve, www.contraloriamonagas.gob.ve
        '), 0, 1, 'C', 0);


        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}


class pdfOficio extends FPDF
{

		
	var $B=0;
    var $I=0;
    var $U=0;
    var $HREF='';
    var $ALIGN='';

    function WriteHTML($html)
    {
        //HTML parser
        $html=str_replace("\n",' ',$html);
        $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
        foreach($a as $i=>$e)
        {
            if($i%2==0)
            {
                //Text
                if($this->HREF)
                    $this->PutLink($this->HREF,$e);
                elseif($this->ALIGN=='center')
                    $this->Cell(0,5,$e,0,1,'C');
                else
                    $this->Write(5,$e);
            }
            else
            {
                //Tag
                if($e[0]=='/')
                    $this->CloseTag(strtoupper(substr($e,1)));
                else
                {
                    //Extract properties
                    $a2=explode(' ',$e);
                    $tag=strtoupper(array_shift($a2));
                    $prop=array();
                    foreach($a2 as $v)
                    {
                        if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                            $prop[strtoupper($a3[1])]=$a3[2];
                    }
                    $this->OpenTag($tag,$prop);
                }
            }
        }
    }

    function OpenTag($tag,$prop)
    {
        //Opening tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,true);
        if($tag=='A')
            $this->HREF=$prop['HREF'];
        if($tag=='BR')
            $this->Ln(5);
        if($tag=='P')
            $this->ALIGN=$prop['ALIGN'];
        if($tag=='HR')
        {
            if( !empty($prop['WIDTH']) )
                $Width = $prop['WIDTH'];
            else
                $Width = $this->w - $this->lMargin-$this->rMargin;
            $this->Ln(2);
            $x = $this->GetX();
            $y = $this->GetY();
            $this->SetLineWidth(0.4);
            $this->Line($x,$y,$x+$Width,$y);
            $this->SetLineWidth(0.2);
            $this->Ln(2);
        }
    }

    function CloseTag($tag)
    {
        //Closing tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,false);
        if($tag=='A')
            $this->HREF='';
        if($tag=='P')
            $this->ALIGN='';
    }

    function SetStyle($tag,$enable)
    {
        //Modify style and select corresponding font
        $this->$tag+=($enable ? 1 : -1);
        $style='';
        foreach(array('B','I','U') as $s)
            if($this->$s>0)
                $style.=$s;
        $this->SetFont('',$style);
    }

    function PutLink($URL,$txt)
    {
        //Put a hyperlink
        $this->SetTextColor(0,0,255);
        $this->SetStyle('U',true);
        $this->Write(5,$txt,$URL);
        $this->SetStyle('U',false);
        $this->SetTextColor(0);
    }
	
   	 function Dependencia($dependencia)
    {
        $this->SetXY(73, 10);
      //  $this->Cell(28,5,'DEPENDENCIA:',0,0,'L',0);
        $this->SetFont('Arial', '', 9);
    //    $this->Cell(100,37,utf8_decode($dependencia),0,0,'L',0);
      //  $this->SetFont('Arial', 'B', 10);
     //   $this->SetXY(10, 46);
    }
	
    //Page header
    function Header()
    {


        $this->SetFillColor(255, 255, 255);
        $this->SetDrawColor(0, 0, 0);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 30, 15, 20, 15);
        $this->SetFont('Arial', '', 9);
        $this->SetXY(70, 10); $this->Cell(100, 20, '', 0, 1, 'L');
        $this->SetXY(72, 10); $this->Cell(20, 28, '', 0, 1, 'L');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."LOGOSNCF.png", 170, 15, 17, 15);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,24.5);
        //Arial italic 8

        $this->SetFont('Arial','',6);
        //Page number
        $this->SetXY(95, 200); $this->Cell(35, 110, utf8_decode('"Hacia la transparencia, fortalecimiento y consolidación del Sistema Nacional de Control Fiscal"'), 0, 1, 'C', 0);

        $this->SetXY(95, 200); $this->Cell(35, 115, utf8_decode('_________________________________________________________________________________________________________________'), 0, 1, 'C', 0);

        $this->SetFont('Arial','',6);
        //Page number
        $this->SetXY(95, 200); $this->Cell(35, 120, utf8_decode('
        Dirección: Calle Sucre c/c Monagas, Edificio sede de la CONTRALORÍA DEL ESTADO SUCRE, Maturín, Teléfono:0291-641-0441 - 6432713 
        '), 0, 1, 'C', 0);

        $this->SetXY(95, 200); $this->Cell(35, 125 , utf8_decode('
Correo Electrónico: contraloriamonagas@contraloriamonagas.gob.ve, www.contraloriamonagas.gob.ve / Twitter: @CEMonagas01
        '), 0, 1, 'C', 0);

        $this->SetXY(95, 200); $this->Cell(35, 130 , utf8_decode('
Rif. G-20001397-4
        '), 0, 1, 'C', 0);


        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}


class pdfCircular extends FPDF
{
	
	
	var $B=0;
    var $I=0;
    var $U=0;
    var $HREF='';
    var $ALIGN='';

    function WriteHTML($html)
    {
        //HTML parser
        $html=str_replace("\n",' ',$html);
        $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
        foreach($a as $i=>$e)
        {
            if($i%2==0)
            {
                //Text
                if($this->HREF)
                    $this->PutLink($this->HREF,$e);
                elseif($this->ALIGN=='center')
                    $this->Cell(0,5,$e,0,1,'C');
                else
                    $this->Write(5,$e);
            }
            else
            {
                //Tag
                if($e[0]=='/')
                    $this->CloseTag(strtoupper(substr($e,1)));
                else
                {
                    //Extract properties
                    $a2=explode(' ',$e);
                    $tag=strtoupper(array_shift($a2));
                    $prop=array();
                    foreach($a2 as $v)
                    {
                        if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                            $prop[strtoupper($a3[1])]=$a3[2];
                    }
                    $this->OpenTag($tag,$prop);
                }
            }
        }
    }

    function OpenTag($tag,$prop)
    {
        //Opening tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,true);
        if($tag=='A')
            $this->HREF=$prop['HREF'];
        if($tag=='BR')
            $this->Ln(5);
        if($tag=='P')
            $this->ALIGN=$prop['ALIGN'];
        if($tag=='HR')
        {
            if( !empty($prop['WIDTH']) )
                $Width = $prop['WIDTH'];
            else
                $Width = $this->w - $this->lMargin-$this->rMargin;
            $this->Ln(2);
            $x = $this->GetX();
            $y = $this->GetY();
            $this->SetLineWidth(0.4);
            $this->Line($x,$y,$x+$Width,$y);
            $this->SetLineWidth(0.2);
            $this->Ln(2);
        }
    }

    function CloseTag($tag)
    {
        //Closing tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,false);
        if($tag=='A')
            $this->HREF='';
        if($tag=='P')
            $this->ALIGN='';
    }

    function SetStyle($tag,$enable)
    {
        //Modify style and select corresponding font
        $this->$tag+=($enable ? 1 : -1);
        $style='';
        foreach(array('B','I','U') as $s)
            if($this->$s>0)
                $style.=$s;
        $this->SetFont('',$style);
    }

    function PutLink($URL,$txt)
    {
        //Put a hyperlink
        $this->SetTextColor(0,0,255);
        $this->SetStyle('U',true);
        $this->Write(5,$txt,$URL);
        $this->SetStyle('U',false);
        $this->SetTextColor(0);
    }
		
   	 function Dependencia($dependencia)
    {
        $this->SetXY(73, 10);
      //  $this->Cell(28,5,'DEPENDENCIA:',0,0,'L',0);
        $this->SetFont('Arial', '', 9);
    //    $this->Cell(100,37,utf8_decode($dependencia),0,0,'L',0);
      //  $this->SetFont('Arial', 'B', 10);
     //   $this->SetXY(10, 46);
    }
	
    //Page header
    function Header()
    {


        $this->SetFillColor(255, 255, 255);
        $this->SetDrawColor(0, 0, 0);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 30, 15, 20, 15);
        $this->SetFont('Arial', '', 9);
        $this->SetXY(70, 10); $this->Cell(100, 20, '', 0, 1, 'L');
        $this->SetXY(72, 10); $this->Cell(20, 28, '', 0, 1, 'L');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."LOGOSNCF.png", 170, 15, 17, 15);

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,24.5);
        //Arial italic 8

        $this->SetFont('Arial','',6);
        //Page number
        $this->SetXY(95, 200); $this->Cell(35, 110, utf8_decode('Hacia la transparencia, fortalecimiento y consolidación del Sistema Nacional de Control Fiscal'), 0, 1, 'C', 0);

        $this->SetFont('Arial','',6);
        //Page number
        $this->SetXY(95, 200); $this->Cell(35, 115, utf8_decode('
        Dirección: Calle Sucre c/c Monagas, Edificio sede de la CONTRALORÍA DEL ESTADO SUCRE, Maturín, Teléfono:0291-641-0441 - 6432713 
        '), 0, 1, 'C', 0);

        $this->SetXY(95, 200); $this->Cell(35, 120, utf8_decode('
Correo Electrónico: contraloriamonagas@contraloriamonagas.gob.ve, www.contraloriamonagas.gob.ve
        '), 0, 1, 'C', 0);


        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }


}




class pdfReporteentdocumentext extends FPDF
{
    //Page header
    function Header()
    {
       $this->Image('publico/imagenes/logos/CES.png', 10, 10, 22);
        ##
        $this->SetFont('Arial','B',8);
        $this->SetXY(35,12); $this->Cell(100,5,utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'),0,1,'L');
        $this->SetXY(35,17); $this->Cell(100,5,utf8_decode('CONTRALORÍA DEL ESTADO SUCRE'),0,1,'L');
        $this->SetXY(35,22); $this->Cell(100,5,utf8_decode('ÁREA DE CORRESPONDENCIA Y MENSAJERÍA'),0,1,'L');
        ## guia 1er item 1 - 2
        $this->SetXY(222,12); $this->Cell(25,5,utf8_decode('Fecha: ' . date('d-m-Y')),0,1,'L');
        $this->SetXY(222,17); $this->Cell(25,5,utf8_decode('Página: ' . $this->PageNo() . ' de {nb}'),0,1,'L');
        $this->SetXY(40, 10); $this->Cell(100, 20, '', 0, 1, 'L');
        $this->SetXY(42, 10); $this->Cell(20, 28, '', 0, 1, 'L');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."LOGOSNCF.png", 250, 10, 17, 15);
        $this->SetXY(90,30); $this->Cell(100,5,utf8_decode('DOCUMENTOS DE ENTRADA'),0,1,'C');
    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        //$this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}

class pdfReporteentdocumentint extends FPDF
{
  //Page header
    function Header()
    {
        $this->Image('publico/imagenes/logos/CES.png', 10, 10, 22);
        ##
        $this->SetFont('Arial','B',8);
        $this->SetXY(35,12); $this->Cell(100,5,utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'),0,1,'L');
        $this->SetXY(35,17); $this->Cell(100,5,utf8_decode('CONTRALORIA DEL ESTADO SUCRE'),0,1,'L');
        $this->SetXY(35,22); $this->Cell(100,5,utf8_decode('ÁREA DE CORRESPONDENCIA Y MENSAJERÍA'),0,1,'L');
         ## LISTA DOC INTERNOS
        $this->SetXY(222,12); $this->Cell(25,5,utf8_decode('Fecha: ' . date('d-m-Y')),0,1,'L');
        $this->SetXY(222,17); $this->Cell(25,5,utf8_decode('Página: ' . $this->PageNo() . ' de {nb}'),0,1,'L');
        $this->SetXY(40, 10); $this->Cell(100, 20, '', 0, 1, 'L');
        $this->SetXY(42, 10); $this->Cell(20, 28, '', 0, 1, 'L');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."LOGOSNCF.png", 250, 10, 17, 15);
        $this->SetXY(90,30); $this->Cell(100,5,utf8_decode('DOCUMENTOS INTERNOS'),0,1,'C');
    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        //$this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}

		class pdfReporteDistriDocumentint extends FPDF
{
     //Page header
    function Header()
    {
        $this->Image('publico/imagenes/logos/CES.png', 10, 10, 22);
        ##
        $this->SetFont('Arial','B',8);
        $this->SetXY(35,12); $this->Cell(100,5,utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'),0,1,'L');
        $this->SetXY(35,17); $this->Cell(100,5,utf8_decode('CONTRALORIA DEL ESTADO SUCRE'),0,1,'L');
        $this->SetXY(35,22); $this->Cell(100,5,utf8_decode('ÁREA DE CORRESPONDENCIA Y MENSAJERÍA'),0,1,'L');
        ## DOCUMENTOS INTERNOS
        $this->SetXY(222,12); $this->Cell(25,5,utf8_decode('Fecha: ' . date('d-m-Y')),0,1,'L');
        $this->SetXY(222,17); $this->Cell(25,5,utf8_decode('Página: ' . $this->PageNo() . ' de {nb}'),0,1,'L');
        $this->SetXY(40, 10); $this->Cell(100, 20, '', 0, 1, 'L');
        $this->SetXY(42, 10); $this->Cell(20, 28, '', 0, 1, 'L');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."LOGOSNCF.png", 250, 10, 17, 15);
        $this->SetXY(90,30); $this->Cell(100,5,utf8_decode('DISTRIBUCIÓN DOCUMENTOS INTERNOS'),0,1,'C');

            }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        //$this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}

class pdfReportedistribucionext extends FPDF
{
   //Page header
    function Header()
    {
        $this->Image('publico/imagenes/logos/CES.png', 10, 10, 22);
        ##
        $this->SetFont('Arial','B',8);
        $this->SetXY(35,12); $this->Cell(100,5,utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'),0,1,'L');
        $this->SetXY(35,17); $this->Cell(100,5,utf8_decode('CONTRALORIA DEL ESTADO SUCRE'),0,1,'L');
        $this->SetXY(35,22); $this->Cell(100,5,utf8_decode('ÁREA DE CORRESPONDENCIA Y MENSAJERÍA'),0,1,'L');
        ## DOCUMENTOS INTERNOS
        $this->SetXY(222,12); $this->Cell(25,5,utf8_decode('Fecha: ' . date('d-m-Y')),0,1,'L');
        $this->SetXY(222,17); $this->Cell(25,5,utf8_decode('Página: ' . $this->PageNo() . ' de {nb}'),0,1,'L');
        $this->SetXY(40, 10); $this->Cell(100, 20, '', 0, 1, 'L');
        $this->SetXY(42, 10); $this->Cell(20, 28, '', 0, 1, 'L');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."LOGOSNCF.png", 250, 10, 17, 15);
        $this->SetXY(90,30); $this->Cell(100,5,utf8_decode('DISTRIBUCIÓN DOCUMENTOS EXTERNOS'),0,1,'C');

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
       // $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
}	

class pdfReportedistribucionPorDocext extends FPDF
{
    //Page header
    function Header()
    {
        $this->Image('publico/imagenes/logos/CES.png', 10, 10, 22);
        ##
        $this->SetFont('Arial','B',8);
        $this->SetXY(35,12); $this->Cell(100,5,utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'),0,1,'L');
        $this->SetXY(35,17); $this->Cell(100,5,utf8_decode('CONTRALORIA DEL ESTADO SUCRE'),0,1,'L');
        $this->SetXY(35,22); $this->Cell(100,5,utf8_decode('ÁREA DE CORRESPONDENCIA Y MENSAJERÍA'),0,1,'L');
        ## DOCUMENTOS INTERNOS
        $this->SetXY(222,12); $this->Cell(25,5,utf8_decode('Fecha: ' . date('d-m-Y')),0,1,'L');
        $this->SetXY(222,17); $this->Cell(25,5,utf8_decode('Página: ' . $this->PageNo() . ' de {nb}'),0,1,'L');
        $this->SetXY(40, 10); $this->Cell(100, 20, '', 0, 1, 'L');
        $this->SetXY(42, 10); $this->Cell(20, 28, '', 0, 1, 'L');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."LOGOSNCF.png", 250, 10, 17, 15);
        $this->SetXY(90,30); $this->Cell(100,5,utf8_decode('DISTRIBUCIÓN POR DOCUMENTOS EXTERNOS'),0,1,'C');

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
       // $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
}   
	
	class pdfReporteSalidaext extends FPDF
{
    //Page header
        function Header()
        {
        $this->Image('publico/imagenes/logos/CES.png', 10, 10, 22);
        ##
        $this->SetFont('Arial','B',8);
        $this->SetXY(35,12); $this->Cell(100,5,utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'),0,1,'L');
        $this->SetXY(35,17); $this->Cell(100,5,utf8_decode('CONTRALORÍA DEL ESTADO SUCRE'),0,1,'L');
        $this->SetXY(35,22); $this->Cell(100,5,utf8_decode('ÁREA DE CORRESPONDENCIA Y MENSAJERÍA'),0,1,'L');
        ## DOCUMENTOS EXTERNOS
        $this->SetXY(222,12); $this->Cell(25,5,utf8_decode('Fecha: ' . date('d-m-Y')),0,1,'L');
        $this->SetXY(222,17); $this->Cell(25,5,utf8_decode('Página: ' . $this->PageNo() . ' de {nb}'),0,1,'L');
        $this->SetXY(40, 10); $this->Cell(100, 20, '', 0, 1, 'L');
        $this->SetXY(42, 10); $this->Cell(20, 28, '', 0, 1, 'L');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."LOGOSNCF.png", 250, 10, 17, 15);
        $this->SetXY(90,30); $this->Cell(100,5,utf8_decode('DOCUMENTOS DE SALIDA'),0,1,'C');
        }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        // $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
}

class pdfReporteDistriSalidaext extends FPDF
{
    //Page header
    function Header()
    {
        $this->Image('publico/imagenes/logos/CES.png', 10, 10, 22);
        ##
        $this->SetFont('Arial','B',8);
        $this->SetXY(35,12); $this->Cell(100,5,utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'),0,1,'L');
        $this->SetXY(35,17); $this->Cell(100,5,utf8_decode('CONTRALORIA DEL ESTADO SUCRE'),0,1,'L');
        $this->SetXY(35,22); $this->Cell(100,5,utf8_decode('ÁREA DE CORRESPONDENCIA Y MENSAJERÍA'),0,1,'L');
        ## DOCUMENTOS DISTRIBUCIÓN EXTERNOS
        $this->SetXY(222,12); $this->Cell(25,5,utf8_decode('Fecha: ' . date('d-m-Y')),0,1,'L');
        $this->SetXY(222,17); $this->Cell(25,5,utf8_decode('Página: ' . $this->PageNo() . ' de {nb}'),0,1,'L');
        $this->SetXY(40, 10); $this->Cell(100, 20, '', 0, 1, 'L');
        $this->SetXY(42, 10); $this->Cell(20, 28, '', 0, 1, 'L');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."LOGOSNCF.png", 250, 10, 17, 15);
        $this->SetXY(90,30); $this->Cell(100,5,utf8_decode('DISTRIBUCIÓN DOCUMENTOS DE SALIDA'),0,1,'C');

    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        //$this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}

	class pdfReporteDistriPorSalidaext extends FPDF
{
     //Page header
        function Header()
        {
$this->Image('publico/imagenes/logos/CES.png', 10, 10, 22);
        ##
        $this->SetFont('Arial','B',8);
        $this->SetXY(35,12); $this->Cell(100,5,utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'),0,1,'L');
        $this->SetXY(35,17); $this->Cell(100,5,utf8_decode('CONTRALORIA DEL ESTADO SUCRE'),0,1,'L');
        $this->SetXY(35,22); $this->Cell(100,5,utf8_decode('ÁREA DE CORRESPONDENCIA Y MENSAJERÍA'),0,1,'L');
        ## DOCUMENTOS DISTRIBUCIÓN EXTERNOS
        $this->SetXY(222,12); $this->Cell(25,5,utf8_decode('Fecha: ' . date('d-m-Y')),0,1,'L');
        $this->SetXY(222,17); $this->Cell(25,5,utf8_decode('Página: ' . $this->PageNo() . ' de {nb}'),0,1,'L');
        $this->SetXY(40, 10); $this->Cell(100, 20, '', 0, 1, 'L');
        $this->SetXY(42, 10); $this->Cell(20, 28, '', 0, 1, 'L');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."LOGOSNCF.png", 250, 10, 17, 15);
        $this->SetXY(90,30); $this->Cell(100,5,utf8_decode('DISTRIBUCIÓN POR DOCUMENTOS DE SALIDA'),0,1,'C');
        }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        //$this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}

class pdfReporteDistriHistPorSalidaext extends FPDF
{
    //Page header
        function Header()
        {
        $this->Image('publico/imagenes/logos/CES.png', 10, 10, 22);
        ##
        $this->SetFont('Arial','B',8);
        $this->SetXY(35,12); $this->Cell(100,5,utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'),0,1,'L');
        $this->SetXY(35,17); $this->Cell(100,5,utf8_decode('CONTRALORIA DEL ESTADO SUCRE'),0,1,'L');
        $this->SetXY(35,22); $this->Cell(100,5,utf8_decode('ÁREA DE CORRESPONDENCIA Y MENSAJERÍA'),0,1,'L');
        ## DOCUMENTOS DISTRIBUCIÓN EXTERNOS
        $this->SetXY(222,12); $this->Cell(25,5,utf8_decode('Fecha: ' . date('d-m-Y')),0,1,'L');
        $this->SetXY(222,17); $this->Cell(25,5,utf8_decode('Página: ' . $this->PageNo() . ' de {nb}'),0,1,'L');
        $this->SetXY(40, 10); $this->Cell(100, 20, '', 0, 1, 'L');
        $this->SetXY(42, 10); $this->Cell(20, 28, '', 0, 1, 'L');
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."LOGOSNCF.png", 250, 10, 17, 15);
        $this->SetXY(90,30); $this->Cell(100,5,utf8_decode('HISTÓRICO POR DOCUMENTOS DE SALIDA'),0,1,'C');
        }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        //$this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

}

