<?php

class Number
{

    /**
     * Formatear montos.
     *
     * @param  String  Monto a formatear
     * @return Float
     */
    public static function format($monto)
    {
        $nuevoMonto = str_replace(".", "", $monto);
        $nuevoMonto = str_replace(",", ".", $nuevoMonto);

        return $nuevoMonto;
    }

    /**
     * Generar numero correlativo.
     *
     * @param  String  Nombre de la tabla
     * @param  String  Nombre del campo
     * @param  String  Nro. de digitos a generar
     * @param  String  Campos a filtrar
     * @return String
     */
    public static function codigo($tabla, $campo, $digitos, $campos=array())
    {
        $db = new db();

        $whereCampos = '';
        foreach($campos AS $key => $value)
        {
            if (!$whereCampos) {
                $whereCampos .= "WHERE $key = '$value'";
            } else {
                $whereCampos .= " AND $key = '$value'";
            }
        }

        $query = $db->query("SELECT MAX($campo) AS ultimo FROM $tabla $whereCampos");
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $field = $query->fetch();

        $codigo = intval($field['ultimo']) + 1;
        $codigo = (string) str_repeat("0", $digitos-strlen($codigo)).$codigo;

        return $codigo;
    }
    public static function codigoARF($tabla, $campo, $digitos, $campos=array(), $depend, $coddep)
    {
        $db = new db();

        $whereCampos = '';
        foreach($campos AS $key => $value)
        {
            if (!$whereCampos) {
                $whereCampos .= "WHERE $key = '$value'";
            } else {
                $whereCampos .= " AND $key = '$value'";
            }
        }

        $query = $db->query("SELECT MAX($campo) AS ultimo FROM $tabla WHERE $depend = $coddep");
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $field = $query->fetch();

        $codigo = intval($field['ultimo']) + 1;

        $codigo = (string) str_repeat("0", $digitos-strlen($codigo)).$codigo;

        return $codigo;
    }

     
    public static function codigoActCl($tabla, $campo, $digitos, $campos=array(), $activo, $codAct)
    {
        $db = new db();
        $contador='';
        $whereCampos = '';
       /* $codPost = $db->query("SELECT
                    cl.ind_codigo as codigo_clas
                  FROM
                    af_c008_clasificacion cl
                    INNER JOIN af_b001_activo a ON (a.fk_afc008_num_clasificacion = cl.pk_num_clasificacion)
                  WHERE a.fk_afc008_num_clasificacion = '28'"); */
        $codPost = $db->query("SELECT
                    cl.ind_codigo as codigo_clas
                  FROM
                    af_c008_clasificacion cl 
                    INNER JOIN af_b001_activo a ON (a.fk_afc008_num_clasificacion = cl.pk_num_clasificacion)
                  WHERE cl.ind_codigo = '0201001006'"); 

                  ##WHERE a.pk_num_activo = '1'");   
        $codPost->setFetchMode(PDO::FETCH_ASSOC);
        $field = $codPost->fetchAll();
        
        foreach($field AS $key)
        {       
            $contador = $contador+1;
        }

        $codigo = (string) str_repeat("0", $digitos-strlen($contador)).$contador;

        return $codigo;

        ##  Obtener Codigos Acta por su Clasificación 
        #ejecuto la consulta a la base de datos
      /*  $codPost =  $this->_db->query("SELECT
                    cl.ind_codigo as codigo_clas
                  FROM
                    af_c008_clasificacion cl
                    INNER JOIN af_b001_activo a ON (a.fk_afc008_num_clasificacion = cl.pk_num_clasificacion)
                  WHERE a.fk_afc008_num_clasificacion = '30'");   
                  ##WHERE a.pk_num_activo = '1'");   

        #retorno lo consultado al controlador para ser usado.
        $codigoActCla = $codPost->fetch();

        ## $codigoActCla[codigo_clas] */
    }
}