<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Archivo Digital
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | IRVIN LEZAMA   | LONDPL@GMAIL.COM | 04249371200
 * | * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once LIBRERIA_FPDF;


class pdfSolicitud extends FPDF
{
    var $widths;
    var $aligns;

    //Page header
    function Header(){
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(50,69) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS.'CES.jpg', 16, 10, 22);
        $this->SetXY(41, 10); $this->Cell(115, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO'), 0, 0, 'L');
        $this->Cell(12,5,'Fecha: ',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(41, 15); $this->Cell(115, 5, utf8_decode('ARCHIVO'), 0, 0, 'L');
        $this->Cell(90,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(41, 20); $this->Cell(115, 5, '', 0, 0, 'L');
        $this->Cell(9,5,utf8_decode('Año: '),0,0,'L');
        $this->Cell(2,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('LISTADO DE SOLICITUDES'),0,1,'C');
        $this->Ln();
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(200, 200, 200); $this->SetTextColor(0, 0, 0);
        $this->Cell(10, 5,utf8_decode('#'), 1, 0, 'C', 1);
        $this->Cell(15, 5,utf8_decode('Fecha S'), 1, 0, 'C', 1);
        $this->Cell(20, 5,utf8_decode('Dependencia'), 1, 0, 'C', 1);
        $this->Cell(20, 5,utf8_decode('Asignado a'), 1, 0, 'C', 1);
        $this->Cell(30, 5,utf8_decode('Equipo'), 1, 0, 'C', 1);
        $this->Cell(80, 5,utf8_decode('Descripcion'), 1, 0, 'C', 1);
        $this->Cell(30, 5,utf8_decode('Usuario'), 1, 0, 'C', 1);
        $this->Cell(30, 5,utf8_decode('Estatus'), 1, 0, 'C', 1);
        $this->Cell(30, 5,utf8_decode('Fecha'), 1, 0, 'C', 1);
        $this->Ln();
    }
    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(166,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',10);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}

class pdfPlanificacion extends FPDF
{
    var $widths;
    var $aligns;

    //Page header
    function Header(){
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(50,69) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS.'CES.jpg', 16, 10, 22);
        $this->SetXY(41, 10); $this->Cell(115, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO'), 0, 0, 'L');
        $this->Cell(12,5,'Fecha: ',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(41, 15); $this->Cell(115, 5, utf8_decode('ARCHIVO'), 0, 0, 'L');
        $this->Cell(90,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(41, 20); $this->Cell(115, 5, '', 0, 0, 'L');
        $this->Cell(9,5,utf8_decode('Año: '),0,0,'L');
        $this->Cell(2,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('LISTADO DE SOLICITUDES'),0,1,'C');
        $this->Ln();
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(200, 200, 200); $this->SetTextColor(0, 0, 0);
        $this->Cell(10, 5,utf8_decode('#'), 1, 0, 'C', 1);
        $this->Cell(20, 5,utf8_decode('Fecha'), 1, 0, 'C', 1);
        $this->Cell(20, 5,utf8_decode('Dependencia'), 1, 0, 'C', 1);
        $this->Cell(20, 5,utf8_decode('Asignado a'), 1, 0, 'C', 1);
        $this->Cell(20, 5,utf8_decode('Descripcion'), 1, 0, 'C', 1);
        $this->Cell(20, 5,utf8_decode('Tipo'), 1, 0, 'C', 1);
        $this->Cell(80, 5,utf8_decode('Detalles'), 1, 0, 'C', 1);
        $this->Cell(30, 5,utf8_decode('Usuario'), 1, 0, 'C', 1);
        $this->Cell(30, 5,utf8_decode('Estatus'), 1, 0, 'C', 1);
        $this->Ln();
    }
    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(166,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',10);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}


class pdfDuracion extends FPDF
{
    var $widths;
    var $aligns;

    //Page header
    function Header(){
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(50,69) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS.'CES.jpg', 16, 10, 22);
        $this->SetXY(41, 10); $this->Cell(115, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO'), 0, 0, 'L');
        $this->Cell(12,5,'Fecha: ',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(41, 15); $this->Cell(115, 5, utf8_decode('ARCHIVO'), 0, 0, 'L');
        $this->Cell(90,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(41, 20); $this->Cell(115, 5, '', 0, 0, 'L');
        $this->Cell(9,5,utf8_decode('Año: '),0,0,'L');
        $this->Cell(2,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('Solicitudes'),0,1,'C');
        $this->Ln();
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(200, 200, 200); $this->SetTextColor(0, 0, 0);
        $this->Cell(18, 5,utf8_decode('Fecha'), 1, 0, 'C', 1);
        $this->Cell(18, 5,utf8_decode('Usuario'), 1, 0, 'C', 1);
        $this->Cell(50, 5,utf8_decode('Detalles'), 1, 0, 'C', 1);
        $this->Cell(18, 5,utf8_decode('Aprobacion'), 1, 0, 'C', 1);
        $this->Cell(18, 5,utf8_decode('Revision'), 1, 0, 'C', 1);
        $this->Cell(18, 5,utf8_decode('Asignacion'), 1, 0, 'C', 1);
        $this->Cell(18, 5,utf8_decode('Ejecucion'), 1, 0, 'C', 1);
        $this->Cell(18, 5,utf8_decode('Culminacion'), 1, 0, 'C', 1);
        $this->Cell(18, 5,utf8_decode('Evaluacion'), 1, 0, 'C', 1);
        $this->Cell(18, 5,utf8_decode('Cierre'), 1, 0, 'C', 1);
        $this->Cell(18, 5,utf8_decode('Estatus'), 1, 0, 'C', 1);
        $this->Ln();



    }
    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(166,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',10);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}

class pdfEjecucion extends FPDF
{
    var $widths;
    var $aligns;

    //Page header
    function Header(){
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(50,69) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS.'CES.jpg', 16, 10, 22);
        $this->SetXY(41, 10); $this->Cell(115, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO'), 0, 0, 'L');
        $this->Cell(12,5,'Fecha: ',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(41, 15); $this->Cell(115, 5, utf8_decode('ARCHIVO'), 0, 0, 'L');
        $this->Cell(90,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(41, 20); $this->Cell(115, 5, '', 0, 0, 'L');
        $this->Cell(9,5,utf8_decode('Año: '),0,0,'L');
        $this->Cell(2,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('HISTORICO DE SOLICITUDES'),0,1,'C');
        $this->Ln();
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(200, 200, 200); $this->SetTextColor(0, 0, 0);
        $this->Cell(40, 5,utf8_decode('Fecha'), 1, 0, 'C', 1);
        $this->Cell(40, 5,utf8_decode('Usuario'), 1, 0, 'C', 1);
        $this->Cell(40, 5,utf8_decode('Dependencia'), 1, 0, 'C', 1);
        $this->Cell(40, 5,utf8_decode('Detalles'), 1, 0, 'C', 1);
        $this->Cell(40, 5,utf8_decode('Estatus'), 1, 0, 'C', 1);

        $this->Ln();



    }
    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(166,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',10);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}


