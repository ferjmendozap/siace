<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once LIBRERIA_FPDF;

/**
*Cabecera de reporte Balance Anterior Acumulado
*/
class ReporteBalanceSaldoAnteriorAcumulado extends FPDF
{
    //Page header
    function Header()
    {

        $this->Image('publico/imagenes/logos/CES.jpg', 10, 10, 10, 10);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(20, 10); $this->Cell(146, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
                              $this->Cell(10,5,'Fecha:',0,0,''); $this->Cell(10,5,date('d/m/Y'),0,1,'');
        $this->SetXY(20, 15); $this->Cell(145, 5, utf8_decode('Dirección de Administración'), 0, 0, 'L');
                              $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(19, 20); $this->Cell(150, 5, '', 0, 0, 'L');
                              $this->Cell(7,5,utf8_decode('Año:'),0,0,'L'); $this->Cell(6,5,date('Y'),0,1,'L');

        $valor_p= explode("-", $_POST['periodo']);
            $fano=  $valor_p[0];
            $fmes=  $valor_p[1];

        switch ($fmes) {
            case "01": $mes = "ENERO"; break;
            case "02": $mes = "FEBRERO";break;
            case "03": $mes = "MARZO";break;
            case "04": $mes = "ABRIL";break;
            case "05": $mes = "MAYO";break;
            case "06": $mes = "JUNIO";break;
            case "07": $mes = "JULIO"; break;
            case "08": $mes = "AGOSTO"; break;
            case "09": $mes = "SEPTIEMBRE"; break;
            case "10": $mes = "OCTUBRE"; break;
            case "11": $mes = "NOVIEMBRE"; break;
            case "12": $mes = "DICIEMBRE"; break;
        }

        $this->SetFont('Arial', 'B', 10);
        $this->Cell(40, 5, '', 0, 0, 'C');
        $this->Cell(100, 5, utf8_decode('BALANCE DE COMPROBACION AL MES DE ').$mes.' DEL '.$fano, 0, 1, 'C');
        $this->SetFont('Arial', '', 10);
        $this->Cell(115, 5, utf8_decode('Saldo Anterior Acumulado'), 0, 1, 'R');

        $this->Ln(3);


        $this->SetFont('Arial', 'B', 7);
        $this->Cell(20, 10,'CUENTA', 1, 0, 'C');
        $this->Cell(60, 10,'DESCRIPCION', 1, 0, 'C');
        $this->Cell(40, 5,'SALDO ANTERIOR', 1, 0, 'C');
        $this->Cell(40, 5,'MOVIMIENTO DEL MES', 1, 0, 'C');
        $this->Cell(40, 5,'SALDO ACTUAL', 1, 1, 'C');
        $this->Cell(20, 0,'', 0, 0, 'C');
        $this->Cell(60, 0,'', 0, 0, 'C');
        $this->Cell(20, 5,'DEBE', 1, 0, 'C');
        $this->Cell(20, 5,'HABER', 1, 0, 'C');
        $this->Cell(20, 5,'DEBE', 1, 0, 'C');
        $this->Cell(20, 5,'HABER', 1, 0, 'C');
        $this->Cell(20, 5,'DEBE', 1, 0, 'C');
        $this->Cell(20, 5,'HABER', 1, 1, 'C');
        $this->Cell(8, 4, '', 0, 1, 'C');
    }

    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(155,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
}

/**
*Cabecera reporte Balance Acumulado
*/
class ReporteBalanceAcumulado extends FPDF
{
    //Page header
    function Header()
    {
        $this->Image('publico/imagenes/logos/CES.jpg', 10, 10, 10, 10);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(20, 10); $this->Cell(146, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
                              $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y'),0,1,'');
        $this->SetXY(20, 15); $this->Cell(145, 5, utf8_decode('Dirección de Administración'), 0, 0, 'L');
                              $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(19, 20); $this->Cell(150, 5, '', 0, 0, 'L');
                              $this->Cell(7,5,utf8_decode('Año:'),0,0,'L');$this->Cell(6,5,date('Y'),0,1,'L');

        $valor_p= explode("-", $_POST['periodo']);
            $fano=  $valor_p[0];
            $fmes=  $valor_p[1];

        switch ($fmes) {
            case "01": $mes = "ENERO"; break;
            case "02": $mes = "FEBRERO";break;
            case "03": $mes = "MARZO";break;
            case "04": $mes = "ABRIL";break;
            case "05": $mes = "MAYO";break;
            case "06": $mes = "JUNIO";break;
            case "07": $mes = "JULIO"; break;
            case "08": $mes = "AGOSTO"; break;
            case "09": $mes = "SEPTIEMBRE"; break;
            case "10": $mes = "OCTUBRE"; break;
            case "11": $mes = "NOVIEMBRE"; break;
            case "12": $mes = "DICIEMBRE"; break;
        }

        $this->SetFont('Arial', 'B', 10);
        $this->Cell(40, 5, '', 0, 0, 'C');
        $this->Cell(100, 5, utf8_decode('BALANCE DE COMPROBACION AL MES DE ').$mes.' DEL '.$fano, 0, 1, 'C');
        $this->SetFont('Arial', '', 10);
        $this->Cell(115, 5, utf8_decode('Comprobación Acumulado'), 0, 1, 'R');

        $this->Ln(3);


        $this->SetFont('Arial', 'B', 7);
        $this->Cell(20, 10,'CUENTA', 1, 0, 'C');
        $this->Cell(100, 10,'DESCRIPCION', 1, 0, 'C');
        $this->Cell(40, 5,'SALDO ACTUAL', 1, 1, 'C');
        $this->Cell(20, 0,'', 0, 0, 'C');
        $this->Cell(100, 0,'', 0, 0, 'C');
        $this->Cell(20, 5,'DEBE', 1, 0, 'C');
        $this->Cell(20, 5,'HABER', 1, 1, 'C');
    }

    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(155,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
}

/**
*Cabecera de reporte Balance DebeHaber
*/
class ReporteBalanceDebeHaber extends FPDF
{
    //Page header
    function Header(){

        $this->Image('publico/imagenes/logos/CES.jpg', 10, 10, 10, 10);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(20, 10); $this->Cell(146, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
                              $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y'),0,1,'');
        $this->SetXY(20, 15); $this->Cell(145, 5, utf8_decode('Dirección de Administración'), 0, 0, 'L');
                              $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(19, 20); $this->Cell(150, 5, '', 0, 0, 'L');
                              $this->Cell(7,5,utf8_decode('Año:'),0,0,'L');$this->Cell(6,5,date('Y'),0,1,'L');

        $valor_p= explode("-", $_POST['periodo']);
            $fano=  $valor_p[0];
            $fmes=  $valor_p[1];

        switch ($fmes) {
            case "01": $mes = "ENERO"; break;
            case "02": $mes = "FEBRERO";break;
            case "03": $mes = "MARZO";break;
            case "04": $mes = "ABRIL";break;
            case "05": $mes = "MAYO";break;
            case "06": $mes = "JUNIO";break;
            case "07": $mes = "JULIO"; break;
            case "08": $mes = "AGOSTO"; break;
            case "09": $mes = "SEPTIEMBRE"; break;
            case "10": $mes = "OCTUBRE"; break;
            case "11": $mes = "NOVIEMBRE"; break;
            case "12": $mes = "DICIEMBRE"; break;
        }

        $this->SetFont('Arial', 'B', 10);
        $this->Cell(40, 5, '', 0, 0, 'C');
        $this->Cell(100, 5, utf8_decode('BALANCE DE COMPROBACION AL MES DE ').$mes.' DEL '.$fano, 0, 1, 'C');
        $this->SetFont('Arial', '', 10);
        $this->Cell(115, 5, utf8_decode('Debe - Haber'), 0, 1, 'R'); ///$this->Cell(25, 5, 'AL   '.date("Y-m"), 0, 1, 'C');
        $this->Ln(3);


        $this->SetFont('Arial', 'B', 7);
        $this->Cell(20, 10,'CUENTA', 1, 0, 'C');
        $this->Cell(60, 10,'DESCRIPCION', 1, 0, 'C');
        $this->Cell(25, 5,'SALDO ANTERIOR', 1, 0, 'C');
        $this->Cell(40, 5,'MOVIMIENTO DEL MES', 1, 0, 'C');
        $this->Cell(25, 5,'NETO MES', 1, 0, 'C');
        $this->Cell(25, 5,'SALDO ACTUAL', 1, 1, 'C');

        $this->Cell(20, 0,'', 0, 0, 'C');
        $this->Cell(60, 0,'', 0, 0, 'C');
        $this->Cell(25, 5,'', 1, 0, 'C');
        $this->Cell(20, 5,'DEBE', 1, 0, 'C');
        $this->Cell(20, 5,'HABER', 1, 0, 'C');
        $this->Cell(25, 5,'', 1, 0, 'C');
        $this->Cell(25, 5,'', 1, 1, 'C');

        $this->Cell(8, 4, '', 0, 1, 'C');
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom

        $this->SetXY(154,13);
        //Arial italic 8
        $this->SetFont('Arial','I',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
}

/**
*Cabecera de reporte Balance DebeHaber Acumulado
*/
class ReporteBalanceDebeHaberAcumulado extends FPDF
{
    //Page header
    function Header(){
        $this->Image('publico/imagenes/logos/CES.jpg', 10, 10, 10, 10);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(20, 10); $this->Cell(146, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
                              $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y'),0,1,'');
        $this->SetXY(20, 15); $this->Cell(145, 5, utf8_decode('Dirección de Administración'), 0, 0, 'L');
                              $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(19, 20); $this->Cell(150, 5, '', 0, 0, 'L');
                              $this->Cell(7,5,utf8_decode('Año:'),0,0,'L');$this->Cell(6,5,date('Y'),0,1,'L');

        $valor_p= explode("-", $_POST['periodo']);
            $fano=  $valor_p[0];
            $fmes=  $valor_p[1];

        switch ($fmes) {
            case "01": $mes = "ENERO"; break;
            case "02": $mes = "FEBRERO";break;
            case "03": $mes = "MARZO";break;
            case "04": $mes = "ABRIL";break;
            case "05": $mes = "MAYO";break;
            case "06": $mes = "JUNIO";break;
            case "07": $mes = "JULIO"; break;
            case "08": $mes = "AGOSTO"; break;
            case "09": $mes = "SEPTIEMBRE"; break;
            case "10": $mes = "OCTUBRE"; break;
            case "11": $mes = "NOVIEMBRE"; break;
            case "12": $mes = "DICIEMBRE"; break;
        }

        $this->SetFont('Arial', 'B', 10);
        $this->Cell(40, 5, '', 0, 0, 'C');
        $this->Cell(100, 5, utf8_decode('BALANCE DE COMPROBACION AL MES DE ').$mes.' DEL '.$fano, 0, 1, 'C');
        $this->SetFont('Arial', '', 10);
        $this->Cell(115, 5, utf8_decode('Debe - Haber Acumulado'), 0, 1, 'R'); ///$this->Cell(25, 5, 'AL   '.date("Y-m"), 0, 1, 'C');
        $this->Ln(3);

        $this->SetFont('Arial', 'B', 6);
        $this->Cell(19, 10,'CUENTA', 1, 0, 'C');
        $this->Cell(48, 10,'DESCRIPCION', 1, 0, 'C');
        $this->Cell(34, 5,'SALDO ANTERIOR', 1, 0, 'C');
        $this->Cell(34, 5,'MOVIMIENTO DEL MES', 1, 0, 'C');
        $this->Cell(34, 5,'SUMAS ACUMULADAS', 1, 0, 'C');
        $this->Cell(34, 5,'SALDO ACTUAL', 1, 1, 'C');
        $this->Cell(19, 0,'', 0, 0, 'C');
        $this->Cell(48, 0,'', 0, 0, 'C');
        $this->Cell(17, 5,'DEBE', 1, 0, 'C');
        $this->Cell(17, 5,'HABER', 1, 0, 'C');
        $this->Cell(17, 5,'DEBE', 1, 0, 'C');
        $this->Cell(17, 5,'HABER', 1, 0, 'C');
        $this->Cell(17, 5,'DEBE', 1, 0, 'C');
        $this->Cell(17, 5,'HABER', 1, 0, 'C');
        $this->Cell(17, 5,'DEBE', 1, 0, 'C');
        $this->Cell(17, 5,'HABER', 1, 1, 'C');
        $this->Cell(8, 4, '', 0, 1, 'C');
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom

        $this->SetXY(154,13);
        //Arial italic 8
        $this->SetFont('Arial','I',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
}

/**
*Cabecera reporte Libro Diario
*/
class ReporteLibroDiario extends FPDF
{
    //Page header
    function Header(){
        $this->Image('publico/imagenes/logos/CES.jpg', 10, 10, 10, 10);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(20, 10); $this->Cell(146, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
                              $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y'),0,1,'');
        $this->SetXY(20, 15); $this->Cell(145, 5, utf8_decode('Dirección de Administración'), 0, 0, 'L');
                              $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(19, 20); $this->Cell(150, 5, '', 0, 0, 'L');
                              $this->Cell(7,5,utf8_decode('Año:'),0,0,'L');$this->Cell(6,5,date('Y'),0,1,'L');

        if($_POST['periodo']!=""){
            $valor_p= explode("-", $_POST['periodo']);
                $fano=  $valor_p[0];
                $fmes=  $valor_p[1];

            switch ($fmes) {
                case "01": $mes = "ENERO"; break;
                case "02": $mes = "FEBRERO";break;
                case "03": $mes = "MARZO";break;
                case "04": $mes = "ABRIL";break;
                case "05": $mes = "MAYO";break;
                case "06": $mes = "JUNIO";break;
                case "07": $mes = "JULIO"; break;
                case "08": $mes = "AGOSTO"; break;
                case "09": $mes = "SEPTIEMBRE"; break;
                case "10": $mes = "OCTUBRE"; break;
                case "11": $mes = "NOVIEMBRE"; break;
                case "12": $mes = "DICIEMBRE"; break;
            }
        }else $fmes=0;

        if($fmes!=0){
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(50, 10, '', 0, 0, 'C');
            $this->Cell(48, 10, utf8_decode('LIBRO DIARIO AL MES DE'), 0, 0, 'C');
            $this->Cell(18, 10, $mes, 0, 0, 'C'); $this->Cell(8, 10, utf8_decode('DE'), 0, 0, 'C');
            $this->Cell(8, 10, $fano, 0, 1, 'C');
        }else{
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(50, 10, '', 0, 0, 'C');
            $this->Cell(35, 10, utf8_decode('LIBRO DIARIO DEL'), 0, 0, 'L');
            $this->Cell(10, 10, date("Y"), 0, 0, 'C'); $this->Cell(16, 10, utf8_decode('A LA FECHA'), 0, 1, 'l');
        }


        $this->SetFont('Arial', 'B', 7);
        $this->Cell(100, 3, '_________________________________________________________________________', 0, 0, 'C');
        $this->Cell(92, 3, '____________________________________________________________________', 0, 1, 'C'); $this->Ln(1);

        $this->Cell(8, 3, 'LIN', 0, 0, 'C');
        $this->Cell(20, 3,'CUENTA', 0, 0, 'C');
        $this->Cell(70, 3, 'DESCRIPCION', 0, 0, 'C');
        $this->Cell(18, 3, 'C.COSTOS', 0, 0, 'R');
        $this->Cell(35, 3, 'DOC. REFERENCIA', 0, 0, 'C');
        $this->Cell(16, 3, 'DEBE', 0, 0, 'R');
        $this->Cell(22, 3, 'HABER', 0, 1, 'R');
        $this->Cell(100, 3, '_________________________________________________________________________', 0, 0, 'C');
        $this->Cell(92, 3, '____________________________________________________________________', 0, 1, 'C');
        $this->Ln(5);
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom

        $this->SetXY(154,13);
        //Arial italic 8
        $this->SetFont('Arial','I',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
}

/**
*Cabecera reporte Libro Mayor
*/
class ReporteLibroMayor extends FPDF
{
    //Page header
    function Header(){
        $this->Image('publico/imagenes/logos/CES.jpg', 10, 10, 10, 10);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(20, 10); $this->Cell(146, 5,utf8_decode('CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
                              $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y'),0,1,'');
        $this->SetXY(20, 15); $this->Cell(145, 5, utf8_decode('Dirección de Administración'), 0, 0, 'L');
                              $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(19, 20); $this->Cell(150, 5, '', 0, 0, 'L');
                              $this->Cell(7,5,utf8_decode('Año:'),0,0,'L');$this->Cell(6,5,date('Y'),0,1,'L');

        if($_POST['periodo']!=""){
            $valor_p= explode("-", $_POST['periodo']);
                $fano=  $valor_p[0];
                $fmes=  $valor_p[1];

            switch ($fmes) {
                case "01": $mes = "ENERO"; break;
                case "02": $mes = "FEBRERO";break;
                case "03": $mes = "MARZO";break;
                case "04": $mes = "ABRIL";break;
                case "05": $mes = "MAYO";break;
                case "06": $mes = "JUNIO";break;
                case "07": $mes = "JULIO"; break;
                case "08": $mes = "AGOSTO"; break;
                case "09": $mes = "SEPTIEMBRE"; break;
                case "10": $mes = "OCTUBRE"; break;
                case "11": $mes = "NOVIEMBRE"; break;
                case "12": $mes = "DICIEMBRE"; break;
            }
        }else $fmes=0;

        //echo $fmes;
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(50, 10, '', 0, 0, 'C');
        $this->Cell(45, 10, utf8_decode('LIBRO MAYOR AL MES DE'), 0, 0, 'C');
        //$this->Cell(22, 10, "DICIEMBRE", 0, 0, 'C');
        $this->Cell(24, 10, $mes, 0, 0, 'C');
        $this->Cell(5, 10, utf8_decode('DE'), 0, 0, 'C');
        $this->Cell(10, 10, $fano, 0, 1, 'C');


        $this->SetFont('Arial', 'B', 7);
        $this->Rect(10,34,195,'','');
        $this->Rect(10,38,195,'','');
        $this->Cell(20, 3, 'VOUCH.', 0, 0, 'C');
        $this->Cell(10, 3,'#', 0, 0, 'C');
        $this->Cell(15, 3,'FECHA', 0, 0, 'C');
        $this->Cell(75, 3, 'CONCEPTO', 0, 0, 'C');
        $this->Cell(25, 3, 'PERS.', 0, 0, 'C');
        $this->Cell(15, 3, '# DOC', 0, 0, 'L');
        $this->Cell(18, 3, 'DEBE', 0, 0, 'C');
        $this->Cell(18, 3, 'HABER', 0, 1, 'C');
        $this->Cell(8, 4, '', 0, 1, 'C');
        $this->Ln(1);
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom

        $this->SetXY(154,13);
        //Arial italic 8
        $this->SetFont('Arial','I',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
}

/**
*Cabecera de reporte Voucher
*/
class ImprimirVoucher extends FPDF
{
    //Page header
    function Header(){

		   foreach($_POST['valores'] as $field){

            $valor_a= explode(" ", $field['fec_fecha_preparacion']);
                $a1=  $valor_a[0];
                $a2=  $valor_a[1];

            $a1= explode("-", $a1);
                $a=  $a1[0];
                $m=  $a1[1];
                $d=  $a1[2];
            $fecha= $d.'-'.$m.'-'.$a;

            $voucher= explode("-", $field['ind_voucher']);
                $cod= $voucher[0];
                $nro= $voucher[1];

            $comprobante= $field['ind_anio'].$field['ind_mes'].'-'.$cod.$nro;
        }

		   $this->Image('publico/imagenes/logos/CES.jpg', 10, 10, 10, 10);
		   $this->SetFont('Arial', 'B', 8);
       $this->SetXY(21, 10);
       $this->Cell(100, 5, ucwords(strtr(strtolower(utf8_decode($field['NombEmpresa'])),"ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÜÚ", "àáâãäåæçèéêëìíîïðñòóôõöøùüú")), 0, 1, 'L');
        ;
    		//$this->SetXY(21, 10); $this->Cell(100, 5, utf8_decode($field['NombEmpresa']), 0, 1, 'L');
    		$this->SetXY(21, 15); $this->Cell(100, 5, utf8_decode('Dirección de Administración'), 0, 0, 'L');
    		$this->SetFont('Arial', '', 8);
    		$this->SetXY(225, 5); $this->Cell(20, 5, utf8_decode('Comprobante: '), 0, 0, 'R');
    		$this->Cell(30, 5, $comprobante, 0, 1, 'L');
    		$this->SetFont('Arial', '', 8);
    		$this->SetXY(225, 10); $this->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'R');
    		$this->Cell(30, 5, $this->PageNo().' de {nb}', 0, 1, 'L');
    		$this->Ln(5);
    		$this->SetFont('Arial', 'B', 10);
    		$this->Cell(260, 5, utf8_decode('Voucher de Contabilidad'), 0, 0, 'C');
    		$this->Ln(5);

    		//	imprimo datos generales
    		$this->SetFont('Arial', 'B', 8); $this->Cell(25, 5, utf8_decode('Fecha Registro: '), 0, 0, 'L');
    		$this->SetFont('Arial', '', 8); $this->Cell(25, 5, $fecha, 0, 1, 'L');
    		$this->SetFont('Arial', 'B', 8); $this->Cell(25, 5, utf8_decode('Usuario: '), 0, 0, 'L');
    		$this->SetFont('Arial', '', 8); $this->Cell(25, 5, $field['Nombre'].' '.$field['Apellido'], 0, 1, 'L');
    		$this->SetFont('Arial', 'B', 8); $this->Cell(25, 5, utf8_decode('C.Costo: '), 0, 0, 'L');
    		//$this->SetFont('Arial', '', 8); $this->Cell(25, 5, $_POST['CodCentroCosto'].' '.$_POST['Abreviatura'], 0, 1, 'L');
            $this->SetFont('Arial', '', 8); $this->Cell(25, 5, '', 0, 1, 'L');
    		$this->SetFont('Arial', 'B', 8); $this->Cell(25, 5, utf8_decode('Descripción: '), 0, 0, 'L');
    		$this->SetFont('Arial', '', 8); $this->MultiCell(250, 5, $field['txt_titulo_voucher'], 0, 'L');
    		$this->Ln(2);

    		//	imprimo cuerpo
    		$this->SetFillColor(255, 255, 255);
    		$this->SetDrawColor(0, 0, 0);
    		$this->SetFont('Arial', 'B', 8);
    		$this->SetWidths(array(20, 60, 30, 60, 40, 25, 25));
    		$this->SetAligns(array('C', 'L', 'C', 'L', 'C', 'R', 'R'));
    		$this->Row(array('Cuenta', 'Nombre', 'Documento', 'Persona', 'C.Costo', 'Debe', 'Haber'));
    }

    //Page footer
    function Footer(){
        /*
        //Position at 1.5 cm from bottom

        $this->SetXY(154,13);
        //Arial italic 8
        $this->SetFont('Arial','I',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
        */
    }
}

/**
*Cabecera de reporte Balance General
*/
class ReporteBalanceGeneral extends FPDF
{
    //Page header
    function Header()
    {

        $this->Image('publico/imagenes/logos/CES.jpg', 10, 10, 10, 10);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(20, 10); $this->Cell(146, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
                              $this->Cell(10,5,'Fecha:',0,0,''); $this->Cell(10,5,date('d/m/Y'),0,1,'');
        $this->SetXY(20, 15); $this->Cell(145, 5, utf8_decode('Dirección de Administración'), 0, 0, 'L');
                              $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(19, 20); $this->Cell(150, 5, '', 0, 0, 'L');
                              $this->Cell(7,5,utf8_decode('Año:'),0,0,'L'); $this->Cell(6,5,date('Y'),0,1,'L');

        $valor_p= explode("-", $_POST['periodo_hasta']);
            $fano=  $valor_p[0];
            $fmes=  $valor_p[1];

        switch ($fmes) {
            case "01": $mes = "ENERO"; break;
            case "02": $mes = "FEBRERO";break;
            case "03": $mes = "MARZO";break;
            case "04": $mes = "ABRIL";break;
            case "05": $mes = "MAYO";break;
            case "06": $mes = "JUNIO";break;
            case "07": $mes = "JULIO"; break;
            case "08": $mes = "AGOSTO"; break;
            case "09": $mes = "SEPTIEMBRE"; break;
            case "10": $mes = "OCTUBRE"; break;
            case "11": $mes = "NOVIEMBRE"; break;
            case "12": $mes = "DICIEMBRE"; break;
        }

        $this->SetFont('Arial', 'B', 10);
        $this->Cell(40, 5, '', 0, 0, 'C');
        $this->Cell(100, 5, utf8_decode('BALANCE GENERAL '), 0, 1, 'C');
        $this->SetFont('Arial', '', 10);
        $this->Cell(40, 5, '', 0, 0, 'C');
        $this->Cell(100, 5, 'Del  '.$_POST['periodo_desde'].'  Al  '.$_POST['periodo_hasta'], 0, 1, 'C');

        $this->Ln(3);
    }

    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(155,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
}

/**
*Cabecera reporte Estado de Resultado
*/
class ReporteEstadoResultado extends FPDF
{
    //Page header
    function Header()
    {

        $this->Image('publico/imagenes/logos/CES.jpg', 10, 10, 10, 10);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(20, 10); $this->Cell(146, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
                              $this->Cell(10,5,'Fecha:',0,0,''); $this->Cell(10,5,date('d/m/Y'),0,1,'');
        $this->SetXY(20, 15); $this->Cell(145, 5, utf8_decode('Dirección de Administración'), 0, 0, 'L');
                              $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        /*$this->SetXY(19, 20); $this->Cell(150, 5, '', 0, 0, 'L');
                              $this->Cell(7,5,utf8_decode('Año:'),0,0,'L'); $this->Cell(6,5,date('Y'),0,1,'L');*/

        $valor_p= explode("-", $_POST['periodo_hasta']);
            $fano=  $valor_p[0];
            $fmes=  $valor_p[1];

        switch ($fmes) {
            case "01": $mes = "ENERO"; break;
            case "02": $mes = "FEBRERO";break;
            case "03": $mes = "MARZO";break;
            case "04": $mes = "ABRIL";break;
            case "05": $mes = "MAYO";break;
            case "06": $mes = "JUNIO";break;
            case "07": $mes = "JULIO"; break;
            case "08": $mes = "AGOSTO"; break;
            case "09": $mes = "SEPTIEMBRE"; break;
            case "10": $mes = "OCTUBRE"; break;
            case "11": $mes = "NOVIEMBRE"; break;
            case "12": $mes = "DICIEMBRE"; break;
        }

        $this->SetFont('Arial', 'B', 10);
        $this->Cell(40, 5, '', 0, 0, 'C');
        $this->Cell(100, 5, utf8_decode('ESTADO DE RESULTADO '), 0, 1, 'C');
        $this->SetFont('Arial', '', 10);
        $this->Cell(40, 5, '', 0, 0, 'C');
        $this->Cell(100, 5, 'Del  '.$_POST['periodo_desde'].'  Al  '.$_POST['periodo_hasta'], 0, 1, 'C');

        $this->Ln(3);
    }

    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(155,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
}

/**
*Cabecera reporte Cierre Anual
*/
class ProcesoCierreAnual extends FPDF
{
   //Page header
    function Header()
    {

        $this->Image('publico/imagenes/logos/CES.jpg', 10, 10, 10, 10);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(20, 10); $this->Cell(146, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
                              $this->Cell(42,5,'Fecha:',0,0,'R'); $this->Cell(10,5,date('d/m/Y'),0,1,'');
        $this->SetXY(20, 15); $this->Cell(145, 5, utf8_decode('Dirección de Administración'), 0, 0, 'L');
                              $this->Cell(43,5,utf8_decode('Página:'),0,1,'R');
        $this->SetFont('Arial', 'B', 10);
        $this->SetY(20); $this->Cell(270, 5, utf8_decode('CIERRE DE CUENTAS'), 0, 1, 'C');
        $this->Ln(5);
        ##  -------------------
        if ($_POST['fk_cbc005_num_control_cierre']) {
            $this->SetFont('Arial', '', 8);
            $this->Cell(30, 5, utf8_decode('PERIODO: '), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(240, 5, utf8_decode($_POST['fk_cbc005_num_control_cierre']), 0, 0, 'L');
            $this->Ln(5);
        }
        if ($_POST['fk_cbb005_num_contabilidades']) {
            $Contabilidad = ConsultarDescripcion::consultar($_POST['fk_cbb005_num_contabilidades'], 'pk_num_contabilidades', 'cb_b005_contabilidades');
            $this->SetFont('Arial', '', 8);
            $this->Cell(30, 5, utf8_decode('CONTABILIDAD: '), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(240, 5, utf8_decode($Contabilidad), 0, 0, 'L');
            $this->Ln(5);
        }
        ##  -------------------
        $this->SetTextColor(0,0,0);
        $this->SetDrawColor(0,0,0);
        $this->SetFillColor(255,255,255);
        $this->Ln(3);
        ##
        $this->SetFont('Arial', 'B', 8);
        $this->SetWidths(array(30,160,35,35));
        $this->SetAligns(array('L','L','R','R'));
        $this->Row(array(utf8_decode('CUENTA'),
                         utf8_decode('DESCRIPCION'),
                         utf8_decode('DEBE'),
                         utf8_decode('HABER')
                         ));
        $this->Ln(1);
    }

    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(155,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
}
