<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Archivo Digital
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once LIBRERIA_FPDF;

class pdfAlmacen extends FPDF
{
    var $widths;
    var $aligns;

    //Page header
    function Header(){
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(50,69) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 16, 10, 22);
        $this->SetXY(41, 10); $this->Cell(115, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(12,5,'Fecha: ',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(41, 15); $this->Cell(115, 5, utf8_decode('ARCHIVO'), 0, 0, 'L');
        $this->Cell(90,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(41, 20); $this->Cell(115, 5, '', 0, 0, 'L');
        $this->Cell(9,5,utf8_decode('Año: '),0,0,'L');
        $this->Cell(2,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('LISTADO DE AlMACÉN'),0,1,'C');
        $this->Ln();
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(200, 200, 200); $this->SetTextColor(0, 0, 0);
        $this->Cell(30, 5,utf8_decode('N° de Almacén'), 1, 0, 'C', 1);
        $this->Cell(65, 5,utf8_decode('DESCRIPCIÓN'), 1, 0, 'C', 1);
        $this->Cell(40, 5,utf8_decode('UBICACIÓN'), 1, 0, 'C', 1);
        $this->Cell(58, 5,utf8_decode('ENCARGADO'), 1, 0, 'C', 1);
        $this->Ln();
    }
    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(166,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',10);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfPasillo extends FPDF
{
    var $widths;
    var $aligns;

    //Page header
    function Header(){
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(50,69) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 16, 10, 22);
        $this->SetXY(41, 10); $this->Cell(115, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(12,5,'Fecha: ',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(41, 15); $this->Cell(115, 5, utf8_decode('ARCHIVO'), 0, 0, 'L');
        $this->Cell(90,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(41, 20); $this->Cell(115, 5, '', 0, 0, 'L');
        $this->Cell(9,5,utf8_decode('Año: '),0,0,'L');
        $this->Cell(2,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('LISTADO DE PASILLOS'),0,1,'C');
        $this->Ln();
    }
    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(166,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',10);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfEstante extends FPDF
{
    var $widths;
    var $aligns;

    //Page header
    function Header(){
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(50,69) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 16, 10, 22);
        $this->SetXY(41, 10); $this->Cell(115, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(12,5,'Fecha: ',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(41, 15); $this->Cell(115, 5, utf8_decode('ARCHIVO'), 0, 0, 'L');
        $this->Cell(90,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(41, 20); $this->Cell(115, 5, '', 0, 0, 'L');
        $this->Cell(9,5,utf8_decode('Año: '),0,0,'L');
        $this->Cell(2,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('LISTADO DE ESTANTES'),0,1,'C');
        $this->Ln();
        $this->SetX(18);
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(200, 200, 200); $this->SetTextColor(0, 0, 0);
        $this->Cell(30, 5,utf8_decode('N° DE ESTANTE'), 1, 0, 'C', 1);
        $this->Cell(45, 5,utf8_decode('ESTANTE'), 1, 0, 'C', 1);
        $this->Cell(40, 5,utf8_decode('PASILLO'), 1, 0, 'C', 1);
        $this->Cell(70, 5,utf8_decode('ALMACÉN'), 1, 0, 'C', 1);
    }
    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(166,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',10);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfDocumento extends FPDF
{
    var $widths;
    var $aligns;

    //Page header
    function Header(){
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(50,69) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 16, 10, 22);
        $this->SetXY(41, 10); $this->Cell(115, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(12,5,'Fecha: ',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(41, 15); $this->Cell(115, 5, utf8_decode('ARCHIVO'), 0, 0, 'L');
        $this->Cell(90,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(41, 20); $this->Cell(115, 5, '', 0, 0, 'L');
        $this->Cell(9,5,utf8_decode('Año: '),0,0,'L');
        $this->Cell(2,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('TIPOS DE DOCUMENTOS'),0,1,'C');
        $this->Ln();
        $this->SetX(18);
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(200, 200, 200); $this->SetTextColor(0, 0, 0);
        $this->Cell(40, 5,utf8_decode('N° DE DOCUMENTO'), 1, 0, 'C', 1);
        $this->Cell(145, 5,utf8_decode('DESCRIPCIÓN'), 1, 0, 'C', 1);
        $this->Ln();
    }
    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(166,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',10);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}

class pdfCaja extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 16, 10, 22);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('ARCHIVO'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('LISTADO DE CAJAS'),0,1,'C');
        $this->Ln();
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(200, 200, 200); $this->SetTextColor(0, 0, 0);
        $this->Cell(75, 5,utf8_decode('NÚMERO-DEPENDENCIA-AÑO'), 1, 0, 'C', 1);
        $this->Cell(40, 5,utf8_decode('TIPO DE DOCUMENTO'), 1, 0, 'C', 1);
        $this->Cell(45, 5,utf8_decode('DESCRIPCIÓN'), 1, 0, 'C', 1);
        $this->Cell(30, 5,utf8_decode('ESTANTE'), 1, 0, 'C', 1);
        $this->Cell(20, 5,utf8_decode('PASILLO'), 1, 0, 'C', 1);
        $this->Cell(51, 5,utf8_decode('ALMACEN'), 1, 0, 'C', 1);
        $this->Ln();
    }
    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}

class pdfCarpeta extends FPDF
{
    var $widths;
    var $aligns;

    //Page header
    function Header(){
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(50,69) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 16, 10, 22);
        $this->SetXY(41, 10); $this->Cell(115, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(30,5,'Fecha: ',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(41, 15); $this->Cell(115, 5, utf8_decode('ARCHIVO'), 0, 0, 'L');
        $this->Cell(100,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(41, 20); $this->Cell(115, 5, '', 0, 0, 'L');
        $this->Cell(20,5,utf8_decode('Año: '),0,0,'L');
        $this->Cell(2,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('LISTADO DE CARPETAS'),0,1,'C');
        $this->Ln();
        $this->SetX(18);
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(200, 200, 200); $this->SetTextColor(0, 0, 0);
        $this->Cell(80, 5,utf8_decode('NÚMERO-DEPENDENCIA-AÑO'), 1, 0, 'C', 1);
        $this->Cell(50, 5,utf8_decode('DESCRIPCIÓN'), 1, 0, 'L', 1);
        $this->Cell(30, 5,utf8_decode('ESTANTE'), 1, 0, 'C', 1);
        $this->Cell(30, 5,utf8_decode('PASILLO'), 1, 0, 'C', 1);
        $this->Cell(51, 5,utf8_decode('ALMACEN'), 1, 0, 'C', 1);
        $this->Ln();        
        $this->SetX(18);
    }
    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(166,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',10);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'C';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
        $this->SetX(18);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}

class pdfRegistro extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('ARCHIVO'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('LISTADO DE DOCUMENTOS REGISTRADOS'),0,1,'C');
        $this->Ln();
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(200, 200, 200); $this->SetTextColor(0, 0, 0);
        $this->Cell(32, 5,utf8_decode('N° DE REGISTRO'), 1, 0, 'C', 1);
        $this->Cell(35, 5,utf8_decode('N° DE DOCUMENTO'), 1, 0, 'C', 1);
        $this->Cell(108, 5,utf8_decode('DESCRIPCIÓN'), 1, 0, 'C', 1);
        $this->Cell(46, 5,utf8_decode('FECHA DEL DOCUMENTO'), 1, 0, 'C', 1);
        $this->Cell(41, 5,utf8_decode('FECHA DEL REGISTRO'), 1, 0, 'C', 1);
        $this->Ln();
    }
    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(248,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfSalida extends FPDF
{
    var $widths;
    var $aligns;

    //Page header
    function Header(){
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(50,69) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 16, 10, 22);
        $this->SetXY(41, 10); $this->Cell(115, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(12,5,'Fecha: ',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(41, 15); $this->Cell(115, 5, utf8_decode('ARCHIVO'), 0, 0, 'L');
        $this->Cell(90,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(41, 20); $this->Cell(115, 5, '', 0, 0, 'L');
        $this->Cell(9,5,utf8_decode('Año: '),0,0,'L');
        $this->Cell(2,5,date('Y'),0,1,'L');
        $this->Ln();
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('SALIDA DE DOCUMENTO'),0,1,'C');
        $this->Ln();
        $this->SetDrawColor(0, 0, 0);$this->SetFillColor(229, 229, 229);$this->SetTextColor(0, 0, 0);
        $this->Cell(12.5,5,utf8_decode('N°'),1,0,'C',1);
        $this->Cell(12.5,5,utf8_decode('Reg.'),1,0,'C',1);
		$this->Cell(20,5,utf8_decode('Estado'),1,0,'C',1);
        $this->Cell(45,5,utf8_decode('Documento'),1,0,'C',1);
        $this->Cell(25,5,utf8_decode('Memorándum'),1,0,'L',1);
        $this->Cell(20,5,utf8_decode('Fecha'),1,0,'C',1);
        $this->Cell(60,5,utf8_decode('Motivo'),1,0,'C',1);
        $this->Ln();
    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(186, 12.5);
        //Arial italic 8
        $this->SetFont('Arial', 'B', 10);
        //Page number
        $this->Cell(0, 10, ' ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}



class pdfEntrada extends FPDF
{
    var $widths;
    var $aligns;

    //Page header
    function Header(){
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(50,69) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 16, 10, 22);
        $this->SetXY(41, 10); $this->Cell(115, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(12,5,'Fecha: ',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(41, 15); $this->Cell(115, 5, utf8_decode('ARCHIVO'), 0, 0, 'L');
        $this->Cell(90,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(41, 20); $this->Cell(115, 5, '', 0, 0, 'L');
        $this->Cell(9,5,utf8_decode('Año: '),0,0,'L');
        $this->Cell(2,5,date('Y'),0,1,'L');
        $this->Ln();
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('ENTRADA DE DOCUMENTO'),0,1,'C');
        $this->Ln();
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(229, 229, 229); $this->SetTextColor(0, 0, 0);
        $this->Cell(12.5,5,utf8_decode('N°'),1,0,'C',1);
        $this->Cell(12.5,5,utf8_decode('REG.'),1,0,'C',1);
		$this->Cell(20,5,utf8_decode('ESTADO'),1,0,'C',1);
        $this->Cell(25,5,utf8_decode('DOCUMENTO'),1,0,'C',1);
        $this->Cell(38,5,utf8_decode('MEMORÁNDUM'),1,0,'C',1);
        $this->Cell(20,5,utf8_decode('SALIDA'),1,0,'C',1);
        $this->Cell(20,5,utf8_decode('ENTRADA'),1,0,'C',1);
        $this->Cell(48,5,utf8_decode('OBSERVACIÓN'),1,0,'C',1);
        $this->Ln();
    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(166, 12.5);
        //Arial italic 8
        $this->SetFont('Arial', 'B', 10);
        //Page number
        $this->Cell(0, 10, ' ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfRegistroDocumento extends FPDF
{
    //Page header
    function Header(){
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(50,69) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 16, 10, 22);
        $this->SetXY(41, 10); $this->Cell(115, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(12,5,'Fecha: ',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(41, 15); $this->Cell(115, 5, utf8_decode('ARCHIVO'), 0, 0, 'L');
        $this->Cell(90,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(41, 20); $this->Cell(115, 5, '', 0, 0, 'L');
        $this->Cell(9,5,utf8_decode('Año: '),0,0,'L');
        $this->Cell(2,5,date('Y'),0,1,'L');
        $this->Ln(10);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('REGISTRO DE DOCUMENTO'),0,1,'C');
        $this->Ln(10);
    }
    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(166, 12.5);
        //Arial italic 8
        $this->SetFont('Arial', 'B', 10);
        //Page number
        $this->Cell(0, 10, ' ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }
}

class pdfVerSalida extends FPDF
{
    var $widths;
    var $aligns;

    //Page header
    function Header()
    {
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(50, 69);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 16, 10, 22);
        $this->SetXY(41, 10);
        $this->Cell(115, 5, utf8_decode('CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(12, 5, 'Fecha: ', 0, 0, '');
        $this->Cell(10, 5, date('d/m/Y h:i:a'), 0, 1, '');
        $this->SetXY(41, 15);
        $this->Cell(115, 5, utf8_decode('ARCHIVO'), 0, 0, 'L');
        $this->Cell(90, 5, utf8_decode('Página:'), 0, 1, '');
        $this->SetXY(41, 20);
        $this->Cell(115, 5, '', 0, 0, 'L');
        $this->Cell(9, 5, utf8_decode('Año: '), 0, 0, 'L');
        $this->Cell(2, 5, date('Y'), 0, 1, 'L');
        $this->Ln();
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('SALIDA DE DOCUMENTO'), 0, 1, 'C');
        $this->Ln();
    }

    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(167, 12.5);
        //Arial italic 8
        $this->SetFont('Arial', 'B', 10);
        //Page number
        $this->Cell(0, 10, ' ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths = $w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns = $a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++)
            $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));
        $h = 5 * $nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            //Draw the border
            $this->Rect($x, $y, $w, $h);
            //Print the text
            $this->MultiCell($w, 5, $data[$i], 0, $a);
            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }
        //Go to the next line
        $this->Ln($h);
    }
}
class pdfCargarRegistro extends FPDF
{
    //Page header
    function Header()
    {
        $this->Ln();
    }
    //Page footer
    function Footer()
    {

    }

}

class pdfRegistroFiltro extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('ARCHIVO DIGITAL'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('FILTRO DE DOCUMENTOS REGISTRADOS'),0,1,'C');
        $this->Ln();
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(200, 200, 200); $this->SetTextColor(0, 0, 0);
        /*
        $this->Cell(32, 5,utf8_decode('N° DE REGISTRO'), 1, 0, 'C', 1);
        $this->Cell(35, 5,utf8_decode('N° DE DOCUMENTO'), 1, 0, 'C', 1);
        $this->Cell(108, 5,utf8_decode('DESCRIPCIÓN'), 1, 0, 'C', 1);
        $this->Cell(46, 5,utf8_decode('FECHA DEL DOCUMENTO'), 1, 0, 'C', 1);
        $this->Cell(41, 5,utf8_decode('FECHA DEL REGISTRO'), 1, 0, 'C', 1);
        $this->Ln();
        */
    }
    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(248,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}