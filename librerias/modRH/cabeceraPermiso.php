<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Consumo de Red
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once LIBRERIA_FPDF;

class pdfGeneral extends FPDF
{
    var $widths;
    var $aligns;
    function Header($a){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modCR'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(250, 5, utf8_decode('LISTADO DE PERMISOS'),0,1,'C');
        $this->Ln();$this->Ln();
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(229, 229, 229); $this->SetTextColor(0, 0, 0);
        $this->Cell(25,5,utf8_decode('N° DE PERMISO'),1,0,'C',1);
        $this->Cell(60,5,utf8_decode('FUNCIONARIO(A)'),1,0,'C',1);
        $this->Cell(40,5,utf8_decode('MOTIVO DE AUSENCIA'),1,0,'C',1);
        $this->Cell(30,5,utf8_decode('TIPO DE AUSENCIA'),1,0,'C',1);
        $this->Cell(20,5,utf8_decode('FECHA'),1,0,'C',1);
        $this->Cell(20,5,utf8_decode('ESTADO'),1,0,'C',1);
        $this->Cell(66,5,utf8_decode('MOTIVO'),1,0,'C',1);
        $this->SetFont('Arial', '', 8);
        $this->Ln();
    }


    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfReporte extends FPDF
{
    var $widths;
    var $aligns;
    var $angle=0;

    function Header(){
        $this->SetFont('Arial', 'B', 10);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 13, 13, 35);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modAD'.DS.'logo.png', 14, 15.5, 35);
        $this->SetXY(10, 9);$this->Cell(42,31,'',1);
        $this->SetFont('Arial', 'B', 14);
        $this->SetXY(60, 16);$this->Cell(42,10,utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'),0);
        $this->SetXY(52, 9);$this->Cell(113,23,'',1);
        $this->SetXY(52, 32);$this->Cell(113,8,'',1);
        $this->SetFont('Arial', 'B', 9);
        $this->SetXY(165, 9);$this->Cell(42,5,utf8_decode('CÓDIGO:'), 0, 0, 'C');
        $this->SetXY(165, 14);$this->Cell(42,2,utf8_decode('FOR-DRRHH-011'), 0, 0, 'C');
        $this->SetXY(165, 17);$this->Cell(42,5,utf8_decode('REVISIÓN'), 1, 0, 'C');
        $this->SetXY(165, 22);$this->Cell(21,5,utf8_decode('N°'), 1, 0, 'C');
        $this->SetXY(165, 27);$this->Cell(21,5,'1', 1, 0, 'C');
        $this->SetXY(186, 22);$this->Cell(21,5,utf8_decode('FECHA'), 1, 0, 'C');
        $this->SetXY(186, 27);$this->Cell(21,5,date('d/m/Y'), 1, 0, 'C');
        $this->SetXY(165, 32);$this->Cell(42,5,utf8_decode('PÁGINA'), 0, 0, 'C');
        $this->SetFont('Arial', 'B', 10);
        $this->SetXY(60, 32);$this->Cell(95,5,utf8_decode('FORMATO'),0, 0, 'C');
        $this->SetXY(60, 37);$this->Cell(95,2,utf8_decode('SOLICITUD DE PERMISO O LICENCIA'),0, 0, 'C');
        $this->SetXY(165, 9);$this->Cell(42,31,'',1);
    }
    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(170,33);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().' DE {nb}',0,0,'C');
    }

    //Funcion me marca de agua
    function RotatedText($x, $y, $txt, $angle)
    {
        //Text rotated around its origin
        $this->Rotate($angle,$x,$y);
        $this->Text($x,$y,$txt);
        $this->Rotate(0);
    }

    function Rotate($angle,$x=-1,$y=-1)
    {
        if($x==-1)
            $x=$this->x;
        if($y==-1)
            $y=$this->y;
        if($this->angle!=0)
            $this->_out('Q');
        $this->angle=$angle;
        if($angle!=0)
        {
            $angle*=M_PI/180;
            $c=cos($angle);
            $s=sin($angle);
            $cx=$x*$this->k;
            $cy=($this->h-$y)*$this->k;
            $this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
        }
    }

    function _endpage()
    {
        if($this->angle!=0)
        {
            $this->angle=0;
            $this->_out('Q');
        }
        parent::_endpage();
    }

    // Funcion para celdas dinámicas
    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data, $altura)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h= $altura;
        //$h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}

class pdfListado extends FPDF
{
    var $widths;
    var $aligns;
    var $angle = 0;

    function Header()
    {
        $this->SetFont('Arial', 'B', 10);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 13, 13, 35);
        //$this->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'modAD' . DS . 'logo.png', 14, 15.5, 35);
        $this->SetXY(10, 9);
        $this->Cell(42, 31, '', 1);
        $this->SetFont('Arial', 'B', 14);
        $this->SetXY(60, 16);
        $this->Cell(42, 10, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0);
        $this->SetXY(52, 9);
        $this->Cell(113, 23, '', 1);
        $this->SetXY(52, 32);
        $this->Cell(113, 8, '', 1);
        $this->SetFont('Arial', 'B', 9);
        /*$this->SetXY(165, 9);$this->Cell(42,5,utf8_decode('CÓDIGO:'), 0, 0, 'C');
        $this->SetXY(165, 14);$this->Cell(42,2,utf8_decode('FOR-DRRHH-011'), 0, 0, 'C');
        $this->SetXY(165, 17);$this->Cell(42,5,utf8_decode('REVISIÓN'), 1, 0, 'C');*/
        $this->SetXY(165, 22);
        $this->Cell(21, 5, utf8_decode('N°'), 1, 0, 'C');
        $this->SetXY(165, 27);
        $this->Cell(21, 5, '1', 1, 0, 'C');
        $this->SetXY(186, 22);
        $this->Cell(21, 5, utf8_decode('FECHA'), 1, 0, 'C');
        $this->SetXY(186, 27);
        $this->Cell(21, 5, date('d/m/Y'), 1, 0, 'C');
        $this->SetXY(165, 32);
        $this->Cell(42, 5, utf8_decode('PÁGINA'), 0, 0, 'C');
        $this->SetFont('Arial', 'B', 10);
        /*$this->SetXY(60, 32);$this->Cell(95,5,utf8_decode('FORMATO'),0, 0, 'C');*/
        $this->SetXY(60, 37);
        $this->Cell(95, 2, utf8_decode('PERMISOS DEL EMPLEADO'), 0, 0, 'C');
        $this->SetXY(165, 9);
        $this->Cell(42, 31, '', 1);
    }

//Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetXY(170, 33);
        //Arial italic 8
        $this->SetFont('Arial', 'B', 8);
        //Page number
        $this->Cell(0, 10, ' ' . $this->PageNo() . ' DE {nb}', 0, 0, 'C');
    }

//Funcion me marca de agua
    function RotatedText($x, $y, $txt, $angle)
    {
        //Text rotated around its origin
        $this->Rotate($angle, $x, $y);
        $this->Text($x, $y, $txt);
        $this->Rotate(0);
    }

    function Rotate($angle, $x = -1, $y = -1)
    {
        if ($x == -1)
            $x = $this->x;
        if ($y == -1)
            $y = $this->y;
        if ($this->angle != 0)
            $this->_out('Q');
        $this->angle = $angle;
        if ($angle != 0) {
            $angle *= M_PI / 180;
            $c = cos($angle);
            $s = sin($angle);
            $cx = $x * $this->k;
            $cy = ($this->h - $y) * $this->k;
            $this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm', $c, $s, -$s, $c, $cx, $cy, -$cx, -$cy));
        }
    }

    function _endpage()
    {
        if ($this->angle != 0) {
            $this->angle = 0;
            $this->_out('Q');
        }
        parent::_endpage();
    }

// Funcion para celdas dinámicas
    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths = $w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns = $a;
    }

    function Row($data, $altura)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++)
            $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));
        $h = $altura;
        //$h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            //Draw the border
            $this->Rect($x, $y, $w, $h);
            //Print the text
            $this->MultiCell($w, 5, $data[$i], 1, $a);
            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if ($this->GetY() + $h > $this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w, $txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw =& $this->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;
        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n")
            $nb--;
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ')
                $sep = $i;
            $l += $cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j)
                        $i++;
                } else
                    $i = $sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            } else
                $i++;
        }
        return $nl;
    }

    function formatHora12($hora, $seg = false)
    {
        list($h, $m, $s) = explode(":", $hora);
        $time = "";
        if ($seg) {
            if ($h >= "01" && $h < 12) $time = "$h:$m:$s am";
            if ($h == 12) $time = "$h:$m:$s pm";
            elseif ($h == "00") $time = "12:$m:$s am";
            elseif ($h > 12) {
                $hh = $h - 12;
                if ($hh < 10) $hh = "0$hh";
                $time = "$hh:$m:$s pm";
            }
        } else {
            if ($h >= "01" && $h < 12) $time = "$h:$m am";
            if ($h == 12) $time = "$h:$m pm";
            elseif ($h == "00") $time = "12:$m am";
            elseif ($h > 12) {
                $hh = $h - 12;
                if ($hh < 10) $hh = "0$hh";
                $time = "$hh:$m pm";
            }
        }
        return $time;
    }
}

?>
