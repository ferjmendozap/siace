<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Recursos Humanos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgeMONAGAS.gob.ve  | 0416-3853790
 * | * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once LIBRERIA_FPDF;

class pdfEmpleado extends FPDF
{
    var $widths;
    var $aligns;
    function Header($valor1){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(136, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(136, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(136, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(200, 5, utf8_decode('LISTADO DE EMPLEADOS'),0,1,'C');
        $this->Ln();
        $this->SetFont('Arial', 'B', 6);
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(229, 229, 229); $this->SetTextColor(0, 0, 0);
		if($valor1==1)
		{
        $this->Cell(10,5,utf8_decode('Código'),1,0,'C',1);
        $this->Cell(58,5,utf8_decode('Nombre Completo'),1,0,'C',1);
        $this->Cell(15,5,utf8_decode('Documento'),1,0,'C',1);
        $this->Cell(50,5,utf8_decode('Cargo'),1,0,'C',1);
        $this->Cell(55,5,utf8_decode('Dependencia'),1,0,'C',1);
        $this->Cell(10,5,utf8_decode('Estado'),1,0,'C',1);
		}
        $this->SetFont('Arial', '', 6);
        $this->Ln();
    }


    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(163,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));

        if ($this->heights) {
            $altura1 = $this->heights[0];
            $altura2 = $this->heights[1];
            $altura3 = $this->heights[2];
        } else {
            $altura1 = 5;
            $altura2 = 4.5;
            $altura3 = 5 * $nb;
        }
        $h = $altura3;

        //Issue a page break first if needed
        $this->CheckPageBreak($h);

        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';

            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();

            //Draw the border
            $this->Rect($x, $y, $w, $h, "DF");

            //Print the text
            $this->MultiCell($w, $altura2, $data[$i], 0, $a);

            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }

        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger){
            $this->AddPage($this->CurOrientation);
			$this->Header(1);
		}
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfCargaFamiliar extends FPDF
{
    var $widths;
    var $aligns;
    function Dependencia($dependencia)
    {
        $this->SetXY(10, 32);
        $this->Cell(28,5,'DEPENDENCIA:',0,0,'L',0);
        $this->SetFont('Arial', '', 10);
        $this->Cell(82,5,utf8_decode($dependencia),0,0,'L',0);
        $this->SetFont('Arial', 'B', 10);
        $this->SetXY(10, 46);
    }

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(136, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(136, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(136, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(200, 5, utf8_decode('RELACIÓN DE CARGA FAMILIAR'),0,1,'C');
        $this->Ln();
        $this->Ln();
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(21,5,utf8_decode('CÉDULA'),0,0,'C',0);
        $this->Cell(82,5,utf8_decode('NOMBRE COMPLETO'),0,0,'L',0);
        $this->Cell(20,5,utf8_decode('PARENTESCO'),0,0,'C',0);
        $this->Cell(35,5,utf8_decode('F. NACIMIENTO'),0,0,'C',0);
        $this->Cell(10,5,utf8_decode('EDAD'),0,0,'C',0);
        $this->Cell(10,5,utf8_decode('SEG.'),0,0,'C',0);
        $this->Cell(10,5,utf8_decode('ESP.'),0,0,'C',0);
        $this->Cell(10,5,utf8_decode('EST.'),0,0,'C',0);
        $y=$this->GetY();
        $this->SetDrawColor(0, 0, 0); $this->Rect(10, $y+5, 198, 0.2);
        $this->Ln();
    }


    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(163,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));

        if ($this->heights) {
            $altura1 = $this->heights[0];
            $altura2 = $this->heights[1];
            $altura3 = $this->heights[2];
        } else {
            $altura1 = 5;
            $altura2 = 4.5;
            $altura3 = 5 * $nb;
        }
        $h = $altura3;

        //Issue a page break first if needed
        $this->CheckPageBreak($h);

        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';

            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();

            //Draw the border
            $this->Rect($x, $y, $w, $h, "DF");

            //Print the text
            $this->MultiCell($w, $altura2, $data[$i], 0, $a);

            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }

        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfConstanciaTrabajo extends FPDF
{
    var $widths;
    var $aligns;

    function Header($a)
    {
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 28, 11, 20, 18);
        $this->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'modRH' . DS . 'LOGOSNCF.png', 175, 11, 20, 18);
        $this->SetFont('Arial', 'B', 12);
        $this->SetTextColor(50, 50, 50);
        $this->SetXY(20, 12); $this->Cell(180, 5, utf8_decode('República Bolivariana de Venezuela'), 0, 1, 'C');
        $this->SetXY(20, 17); $this->Cell(180, 5, utf8_decode('CONTRALORÍA DEL ESTADO SUCRE'), 0, 1, 'C');
        $this->SetXY(20, 22); $this->Cell(180, 5, utf8_decode('Dirección de Recursos Humanos'), 0, 1, 'C');
        $this->SetFont('Arial', 'B', 16);
        $this->SetTextColor(0, 0, 0);
        $this->SetXY(25, 40); $this->Cell(170, 10, 'CONSTANCIA', 0, 1, 'C');
        $this->Ln(10);
    }
}

class pdfAntecedente extends FPDF
{
    var $widths;
    var $aligns;
    var $angle=0;

    function Header(){
        $this->SetDrawColor(0, 0, 0);
        $this->SetTextColor(0, 0, 0);
        ##	cuadros y lineas
        $this->Rect(10, 10, 195, 36, 'D');
        $this->Rect(45, 10, 0.1, 36, 'D');
        $this->Rect(170, 10, 0.1, 36, 'D');
        $this->Rect(45, 35, 160, 0.1, 'D');
        $this->Rect(170, 20, 35, 0.1, 'D');
        $this->Rect(170, 25, 35, 0.1, 'D');
        $this->Rect(170, 30, 35, 0.1, 'D');
        $this->Rect(187.5, 25, 0.1, 10, 'D');
        ##	imprimo membrete
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 17, 15,22,14);
        //$this->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'modRH' . DS . 'logo.png', 17, 15, 22, 14);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(10, 32); $this->MultiCell(35, 4,utf8_decode('CONTRALORÍA DEL ESTADO SUCRE'), 0, 'C');
        ##
        $this->SetFont('Arial', 'B', 11);
        $this->SetXY(40, 18); $this->MultiCell(125, 5,utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 'C');
        ##
        $this->SetFont('Arial', 'B', 10);
        $this->SetXY(40, 36);
        $this->Cell(125, 5, 'FORMATO', 0, 2, 'C');
        $this->Cell(125, 5, utf8_decode('ANTECEDENTES DE SERVICIOS'), 0, 0, 'C');
        ##
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(170, 10); $this->Cell(35, 5, utf8_decode('CODIGO:'), 0, 1, 'C');
        $this->SetXY(170, 15); $this->Cell(35, 5, utf8_decode('FOR-DRRHH-016'), 0, 1, 'C');
        $this->SetXY(170, 20); $this->Cell(35, 5, utf8_decode('REVISIÓN:'), 0, 1, 'C');
        $this->SetXY(170, 25); $this->Cell(17.5, 5, utf8_decode('N°:'), 0, 1, 'C');
        $this->SetXY(187.5, 25); $this->Cell(17.5, 5, utf8_decode('FECHA'), 0, 1, 'C');
        $this->SetXY(170, 30); $this->Cell(17.5, 5, utf8_decode('1 DE 1'), 0, 1, 'C');
        $this->SetXY(187.5, 30); $this->Cell(17.5, 5, utf8_decode('03/05/2013'), 0, 1, 'C');
        $this->SetXY(170, 37); $this->Cell(35, 5, utf8_decode('PAGINA'), 0, 1, 'C');
        $this->SetFont('Arial', '', 8);
        $this->SetXY(170, 41); $this->Cell(35, 5, $this->PageNo().' DE {nb}', 0, 1, 'C');

    }
    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));

        if ($this->heights) {
            $altura1 = $this->heights[0];
            $altura2 = $this->heights[1];
            $altura3 = $this->heights[2];
        } else {
            $altura1 = 5;
            $altura2 = 4.5;
            $altura3 = 5 * $nb;
        }
        $h = $altura3;

        //Issue a page break first if needed
        $this->CheckPageBreak($h);

        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';

            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();

            //Draw the border
            $this->Rect($x, $y, $w, $h, "DF");

            //Print the text
            $this->MultiCell($w, $altura2, $data[$i], 0, $a);

            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }

        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}

class pdfExperienciaLaboral extends FPDF
{
    var $widths;
    var $aligns;

    function Header($pkNumEmpleado, $nombreCompleto){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
       // $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(136, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(136, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(136, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(200, 5, utf8_decode('ANTECEDENTES DE SERVICIO'),0,1,'C');
        $this->Ln();
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(10,5, $pkNumEmpleado,0,0,'L',0);
        $this->Cell(90,5,utf8_decode($nombreCompleto),0,0,'L',0);
        $this->Ln();
        $this->SetFont('Arial', 'B', 6);
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(229, 229, 229); $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell(96,5,utf8_decode('ORGANISMO'),1,0,'L',1);
        $this->Cell(21,5,utf8_decode('FECHA INGRESO'),1,0,'C',1);
        $this->Cell(21,5,utf8_decode('FECHA EGRESO'),1,0,'C',1);
        $this->Cell(20,5,utf8_decode('AÑOS'),1,0,'C',1);
        $this->Cell(20,5,utf8_decode('MESES'),1,0,'C',1);
        $this->Cell(20,5,utf8_decode('DIAS'),1,0,'C',1);
        $this->Ln();
    }
    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(163,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));

        if ($this->heights) {
            $altura1 = $this->heights[0];
            $altura2 = $this->heights[1];
            $altura3 = $this->heights[2];
        } else {
            $altura1 = 5;
            $altura2 = 4.5;
            $altura3 = 5 * $nb;
        }
        $h = $altura3;

        //Issue a page break first if needed
        $this->CheckPageBreak($h);

        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';

            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();

            //Draw the border
            $this->Rect($x, $y, $w, $h, "DF");

            //Print the text
            $this->MultiCell($w, $altura2, $data[$i], 0, $a);

            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }

        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}

class pdfConsolidado extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(136, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(136, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(136, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(200, 5, utf8_decode('ANTECEDENTES DE SERVICIO'),0,1,'C');
        $this->Ln();
        $this->SetFont('Arial', 'B', 6);
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(229, 229, 229); $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell(108,5,'',0,0,'L',0);
        $this->Cell(30,5,utf8_decode('ANTECEDENTES'),1,0,'C',1);
        $this->Cell(30,5,utf8_decode('ORGANISMO'),1,0,'C',1);
        $this->Cell(30,5,utf8_decode('TOTAL'),1,0,'C',1);
        $this->Ln();
        $this->Cell(20,5,utf8_decode('EMPLEADO'),1,0,'C',1);
        $this->Cell(88,5,utf8_decode('NOMBRE COMPLETO'),1,0,'C',1);
        $this->Cell(10,5,utf8_decode('AÑO'),1,0,'C',1);
        $this->Cell(10,5,utf8_decode('MES'),1,0,'C',1);
        $this->Cell(10,5,utf8_decode('DÍA'),1,0,'C',1);
        $this->Cell(10,5,utf8_decode('AÑO'),1,0,'C',1);
        $this->Cell(10,5,utf8_decode('MES'),1,0,'C',1);
        $this->Cell(10,5,utf8_decode('DÍA'),1,0,'C',1);
        $this->Cell(10,5,utf8_decode('AÑO'),1,0,'C',1);
        $this->Cell(10,5,utf8_decode('MES'),1,0,'C',1);
        $this->Cell(10,5,utf8_decode('DÍA'),1,0,'C',1);
    }
    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(163,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));

        if ($this->heights) {
            $altura1 = $this->heights[0];
            $altura2 = $this->heights[1];
            $altura3 = $this->heights[2];
        } else {
            $altura1 = 5;
            $altura2 = 4.5;
            $altura3 = 5 * $nb;
        }
        $h = $altura3;

        //Issue a page break first if needed
        $this->CheckPageBreak($h);

        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';

            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();

            //Draw the border
            $this->Rect($x, $y, $w, $h, "DF");

            //Print the text
            $this->MultiCell($w, $altura2, $data[$i], 0, $a);

            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }

        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfAniversario extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(136, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(136, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(136, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(200, 5, utf8_decode('PERSONAL DE ANIVERSARIO'),0,1,'C');
        $this->Ln();
        $this->SetFont('Arial', 'B', 8);
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(229, 229, 229); $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(20,5,utf8_decode('DOCUMENTO'),0,0,'C',0);
        $this->Cell(75,5,utf8_decode('NOMBRE COMPLETO'),0,0,'C',0);
        $this->Cell(57,5,utf8_decode('CARGO'),0,0,'C',0);
        $this->Cell(25,5,utf8_decode('FECHA INGRESO'),0,0,'C',0);
        $this->Cell(13,5,utf8_decode('TIEMPO'),0,0,'C',0);
        $this->SetDrawColor(0, 0, 0); $this->Rect(10, 40, 197, 0.2);
        $this->Ln();
        $this->Ln();
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(163,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));

        if ($this->heights) {
            $altura1 = $this->heights[0];
            $altura2 = $this->heights[1];
            $altura3 = $this->heights[2];
        } else {
            $altura1 = 5;
            $altura2 = 4.5;
            $altura3 = 5 * $nb;
        }
        $h = $altura3;

        //Issue a page break first if needed
        $this->CheckPageBreak($h);

        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';

            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();

            //Draw the border
            $this->Rect($x, $y, $w, $h, "DF");

            //Print the text
            $this->MultiCell($w, $altura2, $data[$i], 0, $a);

            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }

        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfAniversarioDemostrativo extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(136, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(136, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(136, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(200, 5, utf8_decode('CUADRO DEMOSTRATIVO DE LOS FUNCIONARIOS Y TRABAJADORES CON RECONOCIMIENTO POR AÑOS DE SERVICIOS EN ESTE ORGANISMO'),0,1,'C');
        $this->Ln();
        $this->SetFont('Arial', 'B', 8);
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(229, 229, 229); $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell(20,5,utf8_decode('DOCUMENTO'),0,0,'C',0);
        $this->Cell(56,5,utf8_decode('NOMBRE COMPLETO'),0,0,'C',0);
        $this->Cell(47,5,utf8_decode('CARGO'),0,0,'C',0);
        $this->Cell(14,5,utf8_decode('FECHA'),0,0,'C',0);
        $this->Cell(11,5,utf8_decode('TIEMPO'),0,0,'C',0);
        $this->Cell(8,5,utf8_decode('5'),0,0,'C',0);
        $this->Cell(8,5,utf8_decode('10'),0,0,'C',0);
        $this->Cell(8,5,utf8_decode('15'),0,0,'C',0);
        $this->Cell(8,5,utf8_decode('20'),0,0,'C',0);
        $this->Cell(8,5,utf8_decode('25'),0,0,'C',0);
        $this->Cell(8,5,utf8_decode('30'),0,0,'C',0);
        $this->Ln();
        $this->Cell(20,5,utf8_decode(''),0,0,'C',0);
        $this->Cell(56,5,utf8_decode(''),0,0,'C',0);
        $this->Cell(47,5,utf8_decode(''),0,0,'C',0);
        $this->Cell(14,5,utf8_decode('INGRESO'),0,0,'C',0);
        $this->Cell(11,5,utf8_decode(''),0,0,'C',0);
        $this->Cell(8,5,utf8_decode('AÑOS'),0,0,'C',0);
        $this->Cell(8,5,utf8_decode('AÑOS'),0,0,'C',0);
        $this->Cell(8,5,utf8_decode('AÑOS'),0,0,'C',0);
        $this->Cell(8,5,utf8_decode('AÑOS'),0,0,'C',0);
        $this->Cell(8,5,utf8_decode('AÑOS'),0,0,'C',0);
        $this->Cell(8,5,utf8_decode('AÑOS'),0,0,'C',0);
        $this->SetDrawColor(0, 0, 0); $this->Rect(10, 46, 197, 0.2);
        $this->Ln();
        $this->Ln();
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(163,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));

        if ($this->heights) {
            $altura1 = $this->heights[0];
            $altura2 = $this->heights[1];
            $altura3 = $this->heights[2];
        } else {
            $altura1 = 5;
            $altura2 = 4.5;
            $altura3 = 5 * $nb;
        }
        $h = $altura3;

        //Issue a page break first if needed
        $this->CheckPageBreak($h);

        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';

            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();

            //Draw the border
            $this->Rect($x, $y, $w, $h, "DF");

            //Print the text
            $this->MultiCell($w, $altura2, $data[$i], 0, $a);

            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }

        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfAniversarioJubilacion extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
       // $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(136, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(136, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(136, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(200, 5, utf8_decode('CUADRO DEMOSTRATIVO DE LOS FUNCIONARIOS Y TRABAJADORES CON RECONOCIMIENTO POR AÑOS DE SERVICIOS EN ESTE ORGANISMO'),0,1,'C');
        $this->Ln();
        $this->SetFont('Arial', 'B', 8);
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(229, 229, 229); $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell(20,5,utf8_decode('DOCUMENTO'),0,0,'C',0);
        $this->Cell(65,5,utf8_decode('NOMBRE COMPLETO'),0,0,'C',0);
        $this->Cell(54,5,utf8_decode('CARGO'),0,0,'C',0);
        $this->Cell(14,5,utf8_decode('FECHA'),0,0,'C',0);
        $this->Cell(11,5,utf8_decode('EDAD'),0,0,'C',0);
        $this->Cell(11,5,utf8_decode('TIEMPO'),0,0,'C',0);
        $this->Cell(13,5,utf8_decode('TIEMPO'),0,0,'C',0);
        $this->Cell(11,5,utf8_decode('TIEMPO'),0,0,'C',0);
        $this->Ln();
        $this->Cell(20,5,utf8_decode(''),0,0,'C',0);
        $this->Cell(65,5,utf8_decode(''),0,0,'C',0);
        $this->Cell(54,5,utf8_decode(''),0,0,'C',0);
        $this->Cell(14,5,utf8_decode('INGRESO'),0,0,'C',0);
        $this->Cell(11,5,utf8_decode(''),0,0,'C',0);
        $this->Cell(11,5,utf8_decode('ACTUAL'),0,0,'C',0);
        $this->Cell(13,5,utf8_decode('ADM. PUB.'),0,0,'C',0);
        $this->Cell(11,5,utf8_decode('TOTAL'),0,0,'C',0);
        $this->SetDrawColor(0, 0, 0); $this->Rect(10, 46, 197, 0.2);
        $this->Ln();
        $this->Ln();
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(163,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));

        if ($this->heights) {
            $altura1 = $this->heights[0];
            $altura2 = $this->heights[1];
            $altura3 = $this->heights[2];
        } else {
            $altura1 = 5;
            $altura2 = 4.5;
            $altura3 = 5 * $nb;
        }
        $h = $altura3;

        //Issue a page break first if needed
        $this->CheckPageBreak($h);

        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';

            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();

            //Draw the border
            $this->Rect($x, $y, $w, $h, "DF");

            //Print the text
            $this->MultiCell($w, $altura2, $data[$i], 0, $a);

            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }

        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfReporteCumpleanio extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(136, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(136, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(136, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(200, 5, utf8_decode('PERSONAL DE CUMPLEAÑOS'),0,1,'C');
        $this->Ln();
        $this->SetFont('Arial', 'B', 8);
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(229, 229, 229); $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(20,5,utf8_decode('DOCUMENTO'),0,0,'C',0);
        $this->Cell(102,5,utf8_decode('NOMBRE COMPLETO'),0,0,'C',0);
        $this->Cell(40,5,utf8_decode('NÓMINA'),0,0,'C',0);
        $this->Cell(20,5,utf8_decode('FECHA'),0,0,'C',0);
        $this->Cell(15,5,utf8_decode('EDAD'),0,0,'C',0);
        $this->Ln();
        $this->Cell(20,5,utf8_decode(''),0,0,'C',0);
        $this->Cell(102,5,utf8_decode(''),0,0,'C',0);
        $this->Cell(40,5,utf8_decode(''),0,0,'C',0);
        $this->Cell(20,5,utf8_decode('NACIMIENTO'),0,0,'C',0);
        $this->Cell(15,5,'',0,0,'C',0);
        $this->SetDrawColor(0, 0, 0); $this->Rect(10, 46, 197, 0.2);
        $this->Ln();
        $this->Ln();
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(163,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));

        if ($this->heights) {
            $altura1 = $this->heights[0];
            $altura2 = $this->heights[1];
            $altura3 = $this->heights[2];
        } else {
            $altura1 = 5;
            $altura2 = 4.5;
            $altura3 = 5 * $nb;
        }
        $h = $altura3;

        //Issue a page break first if needed
        $this->CheckPageBreak($h);

        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';

            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();

            //Draw the border
            $this->Rect($x, $y, $w, $h, "DF");

            //Print the text
            $this->MultiCell($w, $altura2, $data[$i], 0, $a);

            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }

        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfReporteCese extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(136, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(136, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(136, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(200, 5, utf8_decode('LISTADO DE CESES/REINGRESO'),0,1,'C');
        $this->Ln();
        $this->SetFont('Arial', 'B', 8);
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(200, 200, 200); $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(10,5,utf8_decode('N°'),1,0,'C',1);
        $this->Cell(20,5,utf8_decode('N° PROCESO'),1,0,'C',1);
        $this->Cell(20,5,utf8_decode('EMPLEADO'),1,0,'C',1);
        $this->Cell(107,5,utf8_decode(' NOMBRE COMPLETO'),1,0,'L',1);
        $this->Cell(20,5,utf8_decode('TIPO'),1,0,'C',1);
        $this->Cell(20,5,utf8_decode('FECHA'),1,0,'C',1);
        $this->Ln();
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(163,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));

        if ($this->heights) {
            $altura1 = $this->heights[0];
            $altura2 = $this->heights[1];
            $altura3 = $this->heights[2];
        } else {
            $altura1 = 5;
            $altura2 = 4.5;
            $altura3 = 5 * $nb;
        }
        $h = $altura3;

        //Issue a page break first if needed
        $this->CheckPageBreak($h);

        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';

            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();

            //Draw the border
            $this->Rect($x, $y, $w, $h, "DF");

            //Print the text
            $this->MultiCell($w, $altura2, $data[$i], 0, $a);

            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }

        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfPermisoEmpleado extends FPDF
{
    var $widths;
    var $aligns;

    function Empleado($nombre, $cargo, $dependencia)
    {
        $this->SetFillColor(240, 240, 240); $this->SetDrawColor(240, 240, 240);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(10, 32);
        $this->Cell(19,5,'EMPLEADO:',0,0,'L',1);
        $this->SetFont('Arial', '', 8);
        $this->Cell(82,5,utf8_decode($nombre),0,0,'L',0);
        $this->SetXY(130, 32);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(22,5,'DEPENDENCIA:',0,0,'L',1);
        $this->SetFont('Arial', '', 8);
        $this->Cell(40,5,utf8_decode($dependencia),0,0,'L',0);
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY(10, 50);
    }

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(198, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(198, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(198, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(258, 5, utf8_decode('PERMISOS DEL EMPLEADO'),0,1,'C');
        $this->Ln();$this->Ln();
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(229, 229, 229);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell(10, 5, utf8_decode('N°'), 0, 0, 'C', 0);
        $this->Cell(20, 5, utf8_decode('N° PERMISO'), 0, 0, 'C', 0);
        $this->Cell(20, 5, utf8_decode('F. REGISTRO'), 0, 0, 'C', 0);
        $this->Cell(18, 5, utf8_decode('DESDE'), 0, 0, 'C', 0);
        $this->Cell(18, 5, utf8_decode('HASTA'), 0, 0, 'C', 0);
        $this->Cell(15, 5, utf8_decode('T. HORAS'), 0, 0, 'C', 0);
        $this->Cell(15, 5, utf8_decode('T. DIAS'), 0, 0, 'C', 0);
        $this->Cell(23, 5, utf8_decode('ESTADO'), 0, 0, 'C', 0);
        $this->Cell(35, 5, utf8_decode('TIPO DE PERMISO'), 0, 0, 'C', 0);
        $this->Cell(38, 5, utf8_decode('TIPO DE AUSENCIA'), 0, 0, 'C', 0);
        $this->Cell(45, 5, utf8_decode('MOTIVO DE AUSENCIA'), 0, 0, 'C', 0);
        $this->Ln();
        $this->SetDrawColor(0, 0, 0); $this->Rect(10, 46, 258, 0.2);
        $this->Ln();
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(226,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));

        if ($this->heights) {
            $altura1 = $this->heights[0];
            $altura2 = $this->heights[1];
            $altura3 = $this->heights[2];
        } else {
            $altura1 = 5;
            $altura2 = 4.5;
            $altura3 = 5 * $nb;
        }
        $h = $altura3;

        //Issue a page break first if needed
        $this->CheckPageBreak($h);

        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';

            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();

            //Draw the border
            $this->Rect($x, $y, $w, $h, "DF");

            //Print the text
            $this->MultiCell($w, $altura2, $data[$i], 0, $a);

            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }

        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfReportePermiso extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(275, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(275, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(275, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(337, 5, utf8_decode('PERMISOS DEL EMPLEADO'),0,1,'C');
        $this->Ln();$this->Ln();
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(229, 229, 229);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(15, 5, 'EMPLEADO', 0, 0, 'C', 0);
        $this->Cell(75, 5, 'NOMBRE', 0, 0, 'C', 0);
        $this->Cell(30, 5, 'FECHA REGISTRO', 0, 0, 'C',0);
        $this->Cell(19, 5, 'DESDE', 0, 0, 'C', 0);
        $this->Cell(19, 5, 'HASTA', 0, 0, 'C', 0);
        $this->Cell(16, 5, 'T. HORAS', 0, 0, 'C', 0);
        $this->Cell(12, 5, 'T. DIAS', 0, 0, 'C', 0);
        $this->Cell(25, 5, 'ESTADO', 0, 0, 'C', 0);
        $this->Cell(40, 5, 'TIPO PERMISO', 0, 0, 'C', 0);
        $this->Cell(76, 5, 'MOTIVO', 0, 1, 'C', 0);
        $this->SetDrawColor(0, 0, 0); $this->Rect(10, 46, 337, 0.2);
        $this->Ln();
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(303,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));

        if ($this->heights) {
            $altura1 = $this->heights[0];
            $altura2 = $this->heights[1];
            $altura3 = $this->heights[2];
        } else {
            $altura1 = 5;
            $altura2 = 4.5;
            $altura3 = 5 * $nb;
        }
        $h = $altura3;

        //Issue a page break first if needed
        $this->CheckPageBreak($h);

        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';

            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();

            //Draw the border
            $this->Rect($x, $y, $w, $h, "DF");

            //Print the text
            $this->MultiCell($w, $altura2, $data[$i], 0, $a);

            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }

        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfControlAsistencia extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(134, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(134, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(134, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('REPORTE DE ASISTENCIAS'),0,1,'C');

    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(303,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));

        if ($this->heights) {
            $altura1 = $this->heights[0];
            $altura2 = $this->heights[1];
            $altura3 = $this->heights[2];
        } else {
            $altura1 = 5;
            $altura2 = 4.5;
            $altura3 = 5 * $nb;
        }
        $h = $altura3;

        //Issue a page break first if needed
        $this->CheckPageBreak($h);

        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';

            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();

            //Draw the border
            $this->Rect($x, $y, $w, $h, "DF");

            //Print the text
            $this->MultiCell($w, $altura2, $data[$i], 0, $a);

            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }

        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfControlPermiso extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(134, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(134, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(134, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('REPORTE DE PERMISOS'),0,1,'C');

    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(303,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));

        if ($this->heights) {
            $altura1 = $this->heights[0];
            $altura2 = $this->heights[1];
            $altura3 = $this->heights[2];
        } else {
            $altura1 = 5;
            $altura2 = 4.5;
            $altura3 = 5 * $nb;
        }
        $h = $altura3;

        //Issue a page break first if needed
        $this->CheckPageBreak($h);

        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';

            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();

            //Draw the border
            $this->Rect($x, $y, $w, $h, "DF");

            //Print the text
            $this->MultiCell($w, $altura2, $data[$i], 0, $a);

            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }

        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfResumenEventos extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(134, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(134, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(134, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(161,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));

        if ($this->heights) {
            $altura1 = $this->heights[0];
            $altura2 = $this->heights[1];
            $altura3 = $this->heights[2];
        } else {
            $altura1 = 5;
            $altura2 = 4.5;
            $altura3 = 5 * $nb;
        }
        $h = $altura3;

        //Issue a page break first if needed
        $this->CheckPageBreak($h);

        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';

            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();

            //Draw the border
            $this->Rect($x, $y, $w, $h, "DF");

            //Print the text
            $this->MultiCell($w, $altura2, $data[$i], 0, $a);

            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }

        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfListadoVacaciones extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(134, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(134, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(134, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('LISTADO DE SOLICITUD DE VACACIONES'),0,1,'C');
        $this->Ln();
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(229, 229, 229);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 8);
        $this->SETX(20);
        $this->Cell(30, 5, 'SOLICITUD', 1, 0, 'C', 0);
        $this->Cell(15, 5, 'TIPO', 1, 0, 'C', 0);
        $this->Cell(30, 5, 'FECHA', 1, 0, 'C',0);
        $this->Cell(30, 5, 'FECHA SALIDA', 1, 0, 'C', 0);
        $this->Cell(30, 5, 'FECHA TERMINO', 1, 0, 'C', 0);
        $this->Cell(15, 5, 'DIAS', 1, 0, 'C', 0);
        $this->Cell(30, 5, utf8_decode('INCORPORACIÓN'), 1, 0, 'C', 0);
        $this->Ln();
        $this->Ln();
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(161,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=3.5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h,"DF");
            //Print the text
            $this->MultiCell($w,3,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfResumenEventosConsolidado extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(197,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(197, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(197, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(197, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(222,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));

        if ($this->heights) {
            $altura1 = $this->heights[0];
            $altura2 = $this->heights[1];
            $altura3 = $this->heights[2];
        } else {
            $altura1 = 5;
            $altura2 = 4.5;
            $altura3 = 5 * $nb;
        }
        $h = $altura3;

        //Issue a page break first if needed
        $this->CheckPageBreak($h);

        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';

            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();

            //Draw the border
            $this->Rect($x, $y, $w, $h, "DF");

            //Print the text
            $this->MultiCell($w, $altura2, $data[$i], 0, $a);

            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }

        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfUtilizacionVacaciones extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(134, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(134, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(134, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('UTILIZACIÓN DE VACACIONES'),0,1,'C');
        $this->Ln();
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(229, 229, 229);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 8);
        $this->SETX(30);
        $this->Cell(30, 5, 'PERIODO', 1, 0, 'C', 0);
        $this->Cell(30, 5, 'DERECHO', 1, 0, 'C', 0);
        $this->Cell(30, 5, 'DIAS SOLICITUD', 1, 0, 'C',0);
        $this->Cell(30, 5, utf8_decode('INTERRUPCIÓN'), 1, 0, 'C', 0);
        $this->Cell(30, 5, 'TOTAL UTILIZADO', 1, 0, 'C', 0);
        $this->Ln();
        $this->Ln();
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(161,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=3.5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h,"DF");
            //Print the text
            $this->MultiCell($w,3,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}

class pdfUtilizacionPendiente extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(134, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(134, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(134, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('VACACIONES PENDIENTES DE DISFRUTE'),0,1,'C');
        $this->Ln();
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(229, 229, 229);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 8);
        $this->SETX(25);
        $this->Cell(30, 5, 'PERIODO', 1, 0, 'C', 0);
        $this->Cell(30, 5, 'DERECHO', 1, 0, 'C', 0);
        $this->Cell(30, 5, 'DIAS SOLICITUD', 1, 0, 'C',0);
        $this->Cell(30, 5, utf8_decode('INTERRUPCIÓN'), 1, 0, 'C', 0);
        $this->Cell(30, 5, 'TOTAL UTILIZADO', 1, 0, 'C', 0);
        $this->Cell(30, 5, 'PENDIENTE', 1, 0, 'C', 0);
        $this->Ln();
        $this->Ln();
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(161,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=3.5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h,"DF");
            //Print the text
            $this->MultiCell($w,3,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfResumenVacacion extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(134, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(134, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(134, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('RESUMEN DE VACACIONES'),0,1,'C');
        $this->Ln();
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(229, 229, 229);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 7);
        $this->Cell(16, 5, '#', 0, 0, 'C', 0);
        $this->Cell(70, 5, 'NOMBRE COMPLETO', 0, 0, 'C', 0);
        $this->Cell(18, 5, 'FECHA', 0, 0, 'C',0);
        $this->Cell(16, 5, 'DERECHO', 0, 0, 'C', 0);
        $this->Cell(18, 5, 'DIAS', 0, 0, 'C', 0);
        $this->Cell(22, 5, utf8_decode('INTERRUMPCIÓN'), 0, 0, 'C', 0);
        $this->Cell(18, 5, 'UTILIZADO', 0, 0, 'C', 0);
        $this->Cell(18, 5, 'PENDIENTES', 0, 0, 'C', 0);
        $this->Ln();
        $this->Cell(16, 5, '', 0, 0, 'C', 0);
        $this->Cell(70, 5, '', 0, 0, 'C', 0);
        $this->Cell(18, 5, 'INGRESO', 0, 0, 'C',0);
        $this->Cell(16, 5, '', 0, 0, 'C', 0);
        $this->Cell(18, 5, 'SOLICITUD', 0, 0, 'C', 0);
        $this->Cell(22, 5, '', 0, 0, 'C', 0);
        $this->Cell(18, 5, '', 0, 0, 'C', 0);
        $this->Cell(18, 5, '', 0, 0, 'C', 0);
        $this->Ln();
        $this->SetDrawColor(0, 0, 0); $this->Rect(10, 46, 196, 0.2);
        $this->Ln();
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(161,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=3.5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h,"DF");
            //Print the text
            $this->MultiCell($w,3,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfResumenGeneralVacacion extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(134, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(134, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(134, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('RESUMEN GENERAL DE VACACIONES'),0,1,'C');
        $this->Ln();
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(229, 229, 229);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 7);
        $this->SETX(25);
        $this->Cell(38, 5, 'PERIODO', 1, 0, 'C', 0);
        $this->Cell(20, 5, 'MES PROG.', 1, 0, 'C', 0);
        $this->Cell(20, 5, 'DERECHO', 1, 0, 'C', 0);
        $this->Cell(22, 5, utf8_decode('DÍAS SOLICITUD'), 1, 0, 'C', 0);
        $this->Cell(25, 5, utf8_decode('INTERRUPCIÓN'), 1, 0, 'C', 0);
        $this->Cell(25, 5, 'TOTAL UTILIZADO', 1, 0, 'C', 0);
        $this->Cell(25, 5, 'PENDIENTES', 1, 0, 'C', 0);
        $this->Ln();
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(161,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=3.5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h,"DF");
            //Print the text
            $this->MultiCell($w,3,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfFuncionarioHcm extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(134, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(134, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(134, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('ESTADO DE CUENTA POR BENEFICIO'),0,1,'C');
        $this->Ln();
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(161,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=3.5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h,"DF");
            //Print the text
            $this->MultiCell($w,3,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfHcmInstitucion extends FPDF
{
    var $widths;
    var $aligns;

    function hcm($nombreDependencia, $ayudaGlobal)
    {
        if($nombreDependencia!=''){
            $simbolo = ' / ';
        } else {
            $simbolo = '';
        }
        if($ayudaGlobal!=''){
            $this->SetXY(10, 30);
            $this->Cell(134, 5, utf8_decode($ayudaGlobal).$simbolo.utf8_decode($nombreDependencia), 0, 0, 'L');
            $this->Ln();$this->Ln();$this->Ln();
        }
    }
    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(134, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(134, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(134, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('CONSUMO POR INSTITUCIÓN'),0,1,'C');
        $this->Ln();
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(25, 5, utf8_decode('N° DE CASOS'),0,0,'C');
        $this->Cell(130, 5, utf8_decode('DESCRIPCIÓN'),0,0,'L');
        $this->Cell(40, 5, utf8_decode('MONTO'),0,1,'R');
        $this->SetDrawColor(0, 0, 0); $this->Rect(10, 40, 196, 0.2);

    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(161,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=3.5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h,"DF");
            //Print the text
            $this->MultiCell($w,3,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfHcmPartidas extends FPDF
{
    var $widths;
    var $aligns;

    function hcm($nombreDependencia, $ayudaGlobal)
    {
        if($nombreDependencia!=''){
            $simbolo = ' / ';
        } else {
            $simbolo = '';
        }
        if($ayudaGlobal!=''){
            $this->SetXY(10, 30);
            $this->Cell(134, 5, utf8_decode($ayudaGlobal).$simbolo.utf8_decode($nombreDependencia), 0, 0, 'L');
            $this->Ln();$this->Ln();$this->Ln();
        }
    }
    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
       // $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(134, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(134, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(134, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('CONSUMO POR PARTIDAS'),0,1,'C');
        $this->Ln();
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(155, 5, utf8_decode('NOMBRE'),0,0,'L');
        $this->Cell(40, 5, utf8_decode('MONTO'),0,1,'R');
        $this->SetDrawColor(0, 0, 0); $this->Rect(10, 40, 196, 0.2);
        $this->Ln();
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(161,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=3.5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h,"DF");
            //Print the text
            $this->MultiCell($w,3,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfUtilesGeneral extends FPDF
{
    var $widths;
    var $aligns;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ;
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 10, 22);
        //$this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(134, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(134, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(134, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
    }

    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(161,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=3.5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h,"DF");
            //Print the text
            $this->MultiCell($w,3,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}

?>
