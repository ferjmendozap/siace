<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Consumo de Red
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once LIBRERIA_FPDF;

class pdfGeneral extends FPDF
{
    var $widths;
    var $aligns;
    function Header($a){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(110,69) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modCR'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(200, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(10,5,'Fecha:',0,0,'');$this->Cell(10,5,date('d/m/Y h:i a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(200, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(10,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(33, 20); $this->Cell(200, 5, '', 0, 0, 'L');
        $this->Cell(7,5,utf8_decode('Año: '),0,0,'L');$this->Cell(5,5,date('Y'),0,1,'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(250, 5, utf8_decode('LISTADO DE JUBILACIONES'),0,1,'C');
        $this->Ln();$this->Ln();
        $this->SetFont('Arial', 'B', 8);
        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(229, 229, 229); $this->SetTextColor(0, 0, 0);
        $this->Cell(25,5,utf8_decode('N° DE PROCESO'),1,0,'C',1);
        $this->Cell(60,5,utf8_decode('DEPENDENCIA'),1,0,'C',1);
        $this->Cell(30,5,utf8_decode('EMPLEADO'),1,0,'C',1);
        $this->Cell(86,5,utf8_decode('NOMBRE COMPLETO'),1,0,'C',1);
        $this->Cell(30,5,utf8_decode('N° DE DOCUMENTO'),1,0,'C',1);
        $this->Cell(30,5,utf8_decode('MONTO'),1,0,'C',1);
        $this->SetFont('Arial', '', 8);
        $this->Ln();
    }


    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(227,12.5);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,10,' '.$this->PageNo().'/{nb}',0,0,'C');
    }
    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
class pdfReporteDetallado extends FPDF
{
    var $widths;
    var $aligns;
    var $angle=0;

    function Header(){
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(50,69) ; $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modAD'.DS.'logo.png', 10, 10, 22);
        $this->SetXY(33, 10); $this->Cell(127, 5,utf8_decode( 'CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->Cell(12,5,'Fecha: ',0,0,'');$this->Cell(10,5,date('d/m/Y h:i:a'),0,1,'');
        $this->SetXY(33, 15); $this->Cell(127, 5, utf8_decode('DIRECCIÓN DE RECURSOS HUMANOS'), 0, 0, 'L');
        $this->Cell(100,5,utf8_decode('Página:'),0,1,'');
        $this->SetXY(41, 20); $this->Cell(119, 5, '', 0, 0, 'L');
        $this->Cell(9,5,utf8_decode('Año: '),0,0,'L');
        $this->Cell(2,5,date('Y'),0,1,'L');
        $this->Ln(2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(195, 5, utf8_decode('Reporte de Jubilación'),0,1,'C');
        $this->Ln(4);
    }
    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(151,11.5);
        //Arial italic 8
        $this->SetFont('Arial','',8);
        //Page number
        $this->Cell(0,12,' '.$this->PageNo().' de {nb}',0,0,'C');
    }
    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'C';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
?>
