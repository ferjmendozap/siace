<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Recursos Humanos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Maikol Isava         | acc.desarrollo@cgesucre.gob.ve       | 0426-1814058
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once LIBRERIA_FPDF;

class pdfViatico extends FPDF
{
    var $widths;
    var $aligns;
    var $angle=0;

    function Header(){
        $this->SetFont('Arial', 'B', 10);
        $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 14, 15.5, 25);

        $this->SetXY(40, 18); $this->Cell(190, 5, utf8_decode('Contraloría del Estado Sucre'), 0, 1, 'L');
        $this->SetXY(40, 23); $this->Cell(190, 5, utf8_decode('Dirección de Recursos Humanos'), 0, 1, 'L');
        $this->SetFillColor(250, 250, 250);
        $this->Ln(10);

    }
    //Page footer
    function Footer(){
        //Position at 1.5 cm from bottom
        $this->SetXY(170,33);
        //Arial italic 8
        $this->SetFont('Arial','B',8);
        //Page number
        $this->Cell(0,470,' '.$this->PageNo().' DE {nb}',0,0,'C');


    }

    //Funcion me marca de agua
    function RotatedText($x, $y, $txt, $angle)
    {
        //Text rotated around its origin
        $this->Rotate($angle,$x,$y);
        $this->Text($x,$y,$txt);
        $this->Rotate(0);
    }

    function Rotate($angle,$x=-1,$y=-1)
    {
        if($x==-1)
            $x=$this->x;
        if($y==-1)
            $y=$this->y;
        if($this->angle!=0)
            $this->_out('Q');
        $this->angle=$angle;
        if($angle!=0)
        {
            $angle*=M_PI/180;
            $c=cos($angle);
            $s=sin($angle);
            $cx=$x*$this->k;
            $cy=($this->h-$y)*$this->k;
            $this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
        }
    }

    function _endpage()
    {
        if($this->angle!=0)
        {
            $this->angle=0;
            $this->_out('Q');
        }
        parent::_endpage();
    }

    // Funcion para celdas dinámicas
    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data, $altura)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h= $altura;
        //$h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }

    function BasicTableViatico($header, $data)
    {
        // Cabecera
        $this->SetFont('Arial', 'B', 8);
        $i = 0;
        foreach($header as $col) {
            if ($i == 0) {
                $this->Cell(20, 7, $col, 1);
            }elseif($i == 1){
                $this->Cell(80, 7, $col, 1);
            }elseif($i == 4){
                $this->Cell(20, 7, $col, 1);
            }else{
                $this->Cell(40, 7, $col, 1);
            }
            $i++;
        }
        $this->Ln();
        // Datos
        $this->SetFont('Arial', '', 8);
        foreach($data as $row)
        {
            $i=0;
            foreach($row as $col) {
                if ($i == 0) {
                    $this->Cell(20, 6, $col, 1);
                }elseif($i == 1){
                    $this->Cell(80, 6, $col, 1);
                }elseif($i == 4){
                    $this->Cell(20, 6, $col, 1);
                }else{
                    $this->Cell(40, 6, $col, 1);
                }
                $i++;
                //$this->Cell(40, 6, $col, 1);
            }
            $this->Ln();
        }
    }


}
?>
