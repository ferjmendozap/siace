<?php
/***********************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Planificación Fiscal
 * PROCESO: Reportes de Planficaciones.
 * DESCRIPCIÓN: Encabezado para los reportes en pdf de auditores por planificación en sus diferentes proceso fiscales:
 *              Actuación fiscal, Valoración preliminar, Potestad investigativa, Valoración jurídica y Procedimientos
 *              administrativos.
 * PROGRAMADORES.______________________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Alexis Ontiveros     | ontiveros.alexis@cmldc.gob.ve        | 0426-514.43.82
 * |___________________________________________________________________________________________________________________
 *
 * VERSION
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        07-08-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 ***********************************************************************************************************************/
require_once LIBRERIA_FPDF;
require_once RUTA_Modulo.'modPF/modelos/funcionesGenerales.php';
class pdfPlanificacionFiscal extends FPDF{
    private $atFuncGnrles;
    public function metDatosContraloria($atIdDependenciaUser){
        $this->atFuncGnrles = new funcionesGenerales();
        //Se definen los datos del encabezado del PDF
        $arrDatos=$this->atFuncGnrles->metBuscaDatosEncabezado($atIdDependenciaUser);
        return $arrDatos;
    }
    function Header(){
        global $atIdDependenciaUser; global $tituloReporte;
        if($atIdDependenciaUser){
            $arrDatos=$this->metDatosContraloria($atIdDependenciaUser);
            if(!$arrDatos){
                $arrDatos['logo_reporte']='publico/imagenes/avatar.jpg';//Coloca la imagen por defecto
                $arrDatos['nombre_contraloria']="El nombre del Organísmo no existe";
                $arrDatos['nombre_dependencia']="";
            }else{
                if(!$arrDatos['logo_reporte']){//Si desde la BD no viene imagen, Entra.
                    $arrDatos['logo_reporte']='publico/imagenes/avatar.jpg';//Coloca la imagen por defecto
                }
            }
            $this->SetTitle(utf8_decode('Planificación Fiscal'));
            $this->AliasNbPages();
            //$this->SetMargins(10, 4, 5);
            $this->SetAutoPageBreak(true, 2);
            $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS.$arrDatos['logo_reporte'], 5, 5, 10, 8); $this->SetFont('Arial', '', 8);
            $this->SetFont('Arial', '', 8);
            $this->SetXY(15, 5); $this->Cell(100, 5, utf8_decode($arrDatos['nombre_contraloria']), 0, 1, 'L');
            $this->SetXY(15, 10); $this->Cell(100, 5, utf8_decode($arrDatos['nombre_dependencia']), 0, 0, 'L');
            $this->SetXY(180, 5); $this->Cell(15, 5, utf8_decode('Fecha: '), 0, 0, 'R');
            $this->Cell(60, 5, date("d-m-Y"), 0, 1, 'L');
            $this->SetXY(180, 10); $this->Cell(15, 5, utf8_decode('Página: '), 0, 0, 'R');
            $this->Cell(60, 5, $this->PageNo().' de {nb}', 0, 1, 'L');
            $this->SetFont('Arial', 'B', 12);
            $this->Cell(195, 10, utf8_decode($tituloReporte), 0, 1, 'C');
            $this->Ln(1);
        }
    }

    /**
     * Pinta las filas de acuerdo al array de datos
     * @param $arrData
     */
    function Row($arrData){
        //Calcula el alto de las filas
        $nb = 0;
        for ($i = 0; $i < count($arrData); $i++) $nb = max($nb, $this->NbLines($this->widths[$i], $arrData[$i]));
        if (isset($this->heights)) {
            $altura1 = $this->heights[0];
            $altura2 = $this->heights[1];
            $altura3 = $this->heights[2];
        } else {
            $altura1 = 5;
            $altura2 = 4.5;
            $altura3 = 5 * $nb;
        }
        $h = $altura3;
        //Emita un salto de página primero si es necesario
        $this->CheckPageBreak($h);
        //Dibuja las celdas de la fila
        for ($i = 0; $i < count($arrData); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Guardar la posición actual
            $x = $this->GetX();
            $y = $this->GetY();
            //Dibuja el borde
            $this->Rect($x, $y, $w, $h, "DF");
            //Imprimir el texto
            $this->MultiCell($w, $altura2, $arrData[$i], 0, $a);
            //Imprimir el texto Colocar la posición a la derecha de la celda
            $this->SetXY($x + $w, $y);
        }
        //Ir a la siguiente línea
        $this->Ln($h);
    }
}