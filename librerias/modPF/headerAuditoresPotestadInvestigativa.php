<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: PLANIFICACION FISCAL
 * PROCESO: REPORTES DEL MODULO
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Idelina Ponte                    |     ponte.idelina@cmldc.gob.ve     |         0412-9045109           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-12-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;
require_once RUTA_Modulo.'modPF/modelos/funcionesGenerales.php';

class pdfAuditoresPotestadInvestigativa extends FPDF
{
    private $atFuncionesGnrles;

    // organismo
    function SetOrganismo($og)
    {
        $this->fid_organismo = $og;
    }

    function getOrganismo()
    {
        return $this->fid_organismo;
    }
    //dependencia
    function SetDependencia($dep)
    {
        $this->fdependencia = $dep;
    }

    function getDependencia()
    {
        return $this->fdependencia;
    }
    //proceso
    function SetTipoProceso($tproceso)
    {
        $this->tproceso = $tproceso;
    }

    function getTipoProceso()
    {
        return $this->tproceso;
    }
    //auditor - nombre
    function SetNombreAuditor($nomauditor)
    {
        $this->nauditor = $nomauditor;
    }

    function getNombreAuditor()
    {
        return $this->nauditor;
    }
     //auditor - cedula
    function SetCedulaAuditor($cedauditor)
    {
        $this->cauditor = $cedauditor;
    }

    function getCedulaAuditor()
    {
        return $this->cauditor;
    }
    //auditor - cargo
    function SetCargoAuditor($cargoauditor)
    {
        $this->cargauditor = $cargoauditor;
    }

    function getCargoAuditor()
    {
        return $this->cargauditor;
    }    
    // tipo de reportes
    function setTipoHeader($var)
    {
        return $this->tipoHeader = $var;
    }

    function getTipoHeader()
    {
        return $this->tipoHeader;
    }

    public function metDatosOrganismo(){
        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atFuncionesGnrles = new funcionesGenerales();
        $datos=$this->atFuncionesGnrles->metBuscaUbicacionOrgUsuario('ind_dependencia,ind_descripcion_empresa,ind_logo',$this->atIdUsuario);
        if(empty($datos[0]['ind_logo'])){
            $datos[0]['ind_logo']='publico/imagenes/avatar.jpg';
        }
        return $datos[0];
    }

    //	Cabecera de página
    function Header()
    {
        $datos=$this->metDatosOrganismo();

        if ($this->getTipoHeader() == 'potestad') {
            $this->SetDrawColor(255, 255, 255);
            $this->SetFillColor(255, 255, 255);
            $this->SetTextColor(0, 0, 0);
            $this->SetFont('Arial', 'B', 6);
            // $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."cmldc.jpg", 10, 5, 10, 10);
            $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS.$datos['ind_logo'], 5, 5, 10, 8);

            $this->SetXY(22, 5);
            $this->Cell(190, 5, $datos['ind_descripcion_empresa'], 0, 1, 'L');
            $this->SetXY(15, 5);
            $this->Cell(230, 5, utf8_decode('Fecha: '), 0, 0, 'R');
            $this->Cell(230, 5, date("d-m-Y"), 0, 1, 'L');
            $this->SetXY(15, 2);
            $this->Cell(230, 2, utf8_decode(' Página: '), 0, 0, 'R');
            $this->Cell(230, 2, $this->PageNo() . ' de {nb}', 0, 1, 'L');
            $this->SetFont('Arial', 'B', 12);
            $this->SetXY(15, 15);
            $this->Cell(240, 10, utf8_decode('Reporte Potestad Investigativa por Auditor'), 0, 1, 'C'); 
            $this->SetFont('Arial', 'BU', 9);
            $this->Cell(30, 10, utf8_decode('INFORMACIÓN GENERAL.'), 0, 0, 'L', 1);  $this->Ln(5);
            $this->SetFont('Arial', 'B', 9);
            $this->SetXY(10,30);
            $this->Cell(25, 10, utf8_decode('AUDITOR: '), 0, 1, 'L');
            $this->SetXY(28,30);
            $this->SetFont('Arial', '', 9);
            $this->Cell(100, 10, utf8_decode($this->getNombreAuditor().' ('.$this->getCargoAuditor().')'), 0, 1, 'L');
            $this->SetXY(10,35);
            $this->SetFont('Arial', 'B', 9);
            $this->Cell(25, 10, utf8_decode('ORGANISMO: '), 0, 1, 'L');
            $this->SetXY(35,35); 
            $this->SetFont('Arial', '', 9);
            $this->Cell(100, 10, strtoupper(utf8_decode($this->getOrganismo().' ('.$this->getDependencia().')')), 0, 1, 'L');                        
            $this->Ln(3);
             $this->SetFont('Arial', 'BU', 9); 
             $this->Cell(30, 5, utf8_decode('Potestades.'), 0, 0, 'L', 1);   
             $this->Ln(7);
             $this->SetFillColor(225, 225, 225);
             $this->SetFont('Arial', 'B', 8); 
             $this->SetWidths(array(10,40,37,80,20, 45));
             $this->SetAligns(array('C','C','C','C','C', 'C'));
             $this->Row(array(utf8_decode('Nº'),
                    utf8_decode('Cod. Potestad'),
                    'Tipo', 
                    'Ente Auditado',
                    'Coordinador',
                    utf8_decode('Estado Proceso')));
        }
    }
    //	Pie de página
    function Footer()
    {

    }
}
?>