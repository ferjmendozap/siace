<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdfEjecucion extends FPDF
{
    //Page header
    function setFechaEmision($var) {
        $this->fechaEmision= $var;
    }

    function getFechaEmision() {
        return $this->fechaEmision;
    }
    function setDesde($var) {
        $this->desde= $var;
    }

    function getDesde() {
        return $this->desde;
    }
    function setHasta($var) {
        $this->hasta= $var;
    }

    function getHasta() {
        return $this->hasta;
    }
    function setNomina($var) {
        $this->nomina= $var;
    }

    function getNomina() {
        return $this->nomina;
    }
    function setPeriodo($var) {
        $this->periodo= $var;
    }

    function getPeriodo() {
        return $this->periodo;
    }

    function setProceso($var) {
        $this->proceso= $var;
    }

    function getProceso() {
        return $this->proceso;
    }


    function setNombreProceso($var) {
        $this->nombreProceso= $var;
    }

    function getNombreProceso() {
        return $this->nombreProceso;
    }

    function Header()
    {
        $this->SetDrawColor(255, 255, 255);
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Courier', 'B', 12);
        $this->SetXY(20, 10);
        $this->Cell(190, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->SetX(180);
        $this->Cell(0, 5, 'Pagina: '.$this->PageNo().'', 0, 1, 'L');
        $this->SetX(15);
        $this->SetFont('Courier', 'B', 10);
        $this->Cell(190, 5, 'Fecha de Emision:'.date('Y-m-d H:i:s'), 0, 1, 'R');

        $this->Ln(5);
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Courier', 'B', 11);

        $this->SetWidths(array(70,65,60));
        $this->SetAligns(array('L','L','L','L'));
        $this->Row(array('Nomina','Nombre del Proceso','Periodo'));
        $this->Row(array(utf8_decode($this->getNomina()), utf8_decode($this->getNombreProceso()), utf8_decode($this->getDesde()).' A: '.utf8_decode($this->getHasta())));
        $this->Ln(5);

    }

    //Page footer
    function Footer()
    {

    }
}
