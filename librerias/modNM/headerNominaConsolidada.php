<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdfNominaConsolidada extends FPDF
{
    function SetTipoNom($tn)
    {
        $this->ftiponom = $tn;
    }

    function getTipoNom()
    {
        return $this->ftiponom;
    }

    function setNumTri($tr)
    {
        $this->fnumtri = $tr;
    }

    function getNumTri()
    {
        return $this->fnumtri;
    }

    function SetProceso($tp)
    {
        $this->ftproceso = $tp;
    }

    function getProceso()
    {
        return $this->ftproceso;
    }

    #Fechas
    function setDesde($var)
    {
        $this->desde = $var;
    }

    function getDesde()
    {
        return $this->desde;
    }

    function setHasta($var)
    {
        $this->hasta = $var;
    }

    function getHasta()
    {
        return $this->hasta;
    }

    function setTipoHeader($var)
    {
        return $this->tipoHeader = $var;
    }

    function getTipoHeader()
    {
        return $this->tipoHeader;
    }
    function setNumAnio($var)
    {
        return $this->numanio = $var;
    }

    function getNumAnio()
    {
        return $this->numanio;
    }

    //	Cabecera de página
    function Header()
    {
        if ($this->getTipoHeader() == 'nomina') {
            $periodo_fecha = "DESDE: " . $this->getDesde() . " HASTA: " . $this->getHasta();
            $this->SetDrawColor(255, 255, 255);
            $this->SetFillColor(255, 255, 255);
            $this->SetTextColor(0, 0, 0);
            $this->SetFont('Arial', 'B', 6);
            $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 5, 5, 10, 10);
            $this->SetXY(15, 5);
            $this->Cell(190, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
            $this->SetXY(15, 10);
            $this->Cell(190, 5, ('DIRECCION DE RECURSOS HUMANOS'), 0, 1, 'L');
            $this->SetXY(15, 5);
            /*$this->Cell(320, 5, utf8_decode('Fecha: '), 0, 0, 'R');
            $this->Cell(350, 5, date("d-m-Y"), 0, 1, 'L');*/
            $this->SetXY(15, 2);
            $this->Cell(320, 2, utf8_decode('Página: '), 0, 0, 'R');
            $this->Cell(350, 2, $this->PageNo() . ' de {nb}', 0, 1, 'L');
            $this->SetXY(250, 5);
            $this->Cell(190, 5, $this->getProceso(), 0, 1, 'L');
            $this->SetXY(250, 8);
            $this->Cell(190, 8, ($periodo_fecha), 0, 1, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(250, 6, utf8_decode('NÓMINA CONSOLIDADA:  ' . $this->getTipoNom()), 0, 1, 'C');
        } elseif ($this->getTipoHeader() == 'conceptos') {
            $periodo_fecha = "DESDE: " . $this->getDesde() . " HASTA: " . $this->getHasta();
            $this->SetDrawColor(255, 255, 255);
            $this->SetFillColor(255, 255, 255);
            $this->SetTextColor(0, 0, 0);
            $this->SetFont('Arial', 'B', 8);
            $this->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 5, 5, 10, 10);
            $this->SetXY(15, 5);
            $this->Cell(190, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
            $this->SetXY(15, 10);
            $this->Cell(190, 5, ('DIRECCION DE RECURSOS HUMANOS'), 0, 1, 'L');
            $this->SetXY(15, 15);
            $this->Cell(190, 5, ('CONCEPTOS CONSOLIDADOS - ' . $this->getTipoNom()), 0, 1, 'L');
            $this->SetXY(15, 20);
            $this->Cell(190, 5, ($periodo_fecha . ' ' . $this->getProceso()), 0, 1, 'L');
            $this->SetXY(15, 25);
            /*$this->Cell(320, 5, utf8_decode('Fecha: '), 0, 0, 'R');
            $this->Cell(350, 5, date("d-m-Y"), 0, 1, 'L');*/
            $this->SetXY(15, 30);
            $this->Cell(320, 2, utf8_decode('Página: '), 0, 0, 'R');
            $this->Cell(350, 2, $this->PageNo() . ' de {nb}', 0, 1, 'L');
            $this->Ln(5);
            $this->SetFont('Arial', '', 10);
            $this->SetWidths(array(30, 70, 30, 30,));
            $this->SetAligns(array('C', 'L', 'R', 'R'));
            $this->Row(array('PARTIDA', 'CONCEPTO', 'ASIGNACIONES', 'DEDUCCIONES'));
            $this->SetDrawColor(0, 0, 0);
            $this->SetFillColor(0, 0, 0);
            $y = $this->GetY();
            $this->Rect(10, $y, 190, 0.1, "DF");
            $this->Ln(2);
        }elseif($this->getTipoHeader() == 'prestacionesSocialesConsolidado'){
            $this->SetLeftMargin(1.0);
            $this->image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 5, 40, 25);
            $this->SetFont('Arial','B',10);
            $this->SetLineWidth(0.00);
            $this->SetXY( 65, 15);
            $this->MultiCell(250 , 4,utf8_decode('REPORTE DE ACUMULADO DE PRESTACIONES SOCIALES,  DEL PERSONAL  DE LA CONTRALORÍA  DEL ESTADO MONAGAS, SEGÚN ART. N° 142,  LITERALES "a y b" DE LA LEY ORGÁNICA DEL TRABAJO, LOS TRABAJADORES Y LAS TRABAJADORAS.'), 0, 'J');

            $this->SetXY( 180, 25);
            $this->MultiCell(210 , 4,utf8_decode('Año  '.$this->getHasta()), 0, 'J');

            $this->SetFont('Arial','B',7);
            $this->SetXY( 15, 35);
            $this->SetFillColor(200,200,200);
            $this->SetWidths(array(8,20,30,30,15,30,30,20,20,20,20,15,20,20,25));
            $this->SetAligns(array('C','C','C','C','C','C','C','C','C','C','C','C','C','C','C'));
            $this->Row(array(utf8_decode('N°'),utf8_decode('CEDULA'),utf8_decode('APELLIDOS'),utf8_decode('NOMBRES')
            ,utf8_decode('FECHA DE INGRESO'),utf8_decode('NUMERO DE CUENTA'),utf8_decode('ACUMULADO AÑO ANTERIOR'),utf8_decode('MONTO -1ER TRIMESTRE')
            ,utf8_decode('MONTO -2DO TRIMESTRE'),utf8_decode('MONTO -3ER TRIMESTRE'),utf8_decode('MONTO -4TO TRIMESTRE')
            ,utf8_decode('NUMERO DE DÍAS'),utf8_decode('MONTO DIAS ADICIONALES'),utf8_decode('MONTO ANTICIPO'),utf8_decode('TOTAL DE PRESTACIONES ACUMULADAS')));

        }elseif($this->getTipoHeader() == 'prestacionesSocialesTrimestralConsolidado'){
            $this->SetLeftMargin(15.0);
            $this->image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 10, 5, 40, 25);
            $this->SetFont('Arial','B',10);
            $this->SetLineWidth(0.00);
            $this->SetXY( 65, 15);
            $this->MultiCell(250 , 4,utf8_decode('REPORTE DE ACUMULADO DE PRESTACIONES SOCIALES,  DEL PERSONAL  DE LA CONTRALORÍA  DEL ESTADO MONAGAS, SEGÚN ART. N° 142,  LITERALES "a y b" DE LA LEY ORGÁNICA DEL TRABAJO, LOS TRABAJADORES Y LAS TRABAJADORAS.'), 0, 'J');

            $this->SetXY( 100, 25);/*
            $this->MultiCell(210 , 4,utf8_decode(' DESDE: ' .$this->getDesde().  ' HASTA: '.
                $this->getHasta() .' DEL: '.$this->getNumAnio()), 0, 'J');*/
            $this->Cell(30, 3, ('TRIMESTRE: '.$this->getNumTri()), 0, 0, 'L');
            $this->Cell(40, 3, ('DESDE: '.$this->getDesde()), 0, 0, 'L');
            $this->Cell(40, 3, ('HASTA: '.$this->getHasta()), 0, 0, 'L');
            $this->Cell(20, 3, ('DEL: '.$this->getNumAnio()), 0, 0, 'L');

            $this->SetFont('Arial','B',9);
            $this->SetXY( 15, 35);
            $this->SetFillColor(200,200,200);
            $this->SetWidths(array(8,20,80,30,50,30,30,30,30));
            $this->SetAligns(array('C','C','C','C','C','C','C','C','C'));
            $this->Row(array(utf8_decode('N°'),utf8_decode('CEDULA'),utf8_decode('APELLIDOS Y  NOMBRES')
            ,utf8_decode('FECHA DE INGRESO'),utf8_decode('NUMERO DE CUENTA'),utf8_decode('DIAS ADICIONALES'),utf8_decode('MONTO DIAS ADICIONALES')
            ,utf8_decode('MONTO TRIMESTRE'),utf8_decode('MONTO ABONAR')));
        }

    }

    //	Pie de página
    function Footer()
    {

    }
}
