<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once LIBRERIA_FPDF;

class pdfRelacionNominaCuenta extends FPDF
{
    //Page header
    function SetTipoNom($tn)
    {
        $this->ftiponom = $tn;
    }

    function getTipoNom()
    {
        return $this->ftiponom;
    }

    function setTipoHeader($var)
    {
        return $this->tipoHeader = $var;
    }

    function getTipoHeader()
    {
        return $this->tipoHeader;
    }

    function setFechaEmision($var) {
        $this->fechaEmision= $var;
    }

    function getFechaEmision() {
        return $this->fechaEmision;
    }
    function setDesde($var) {
        $this->desde= $var;
    }

    function getDesde() {
        return $this->desde;
    }
    function setHasta($var) {
        $this->hasta= $var;
    }

    function getHasta() {
        return $this->hasta;
    }
    function setNomina($var) {
        $this->nomina= $var;
    }

    function getNomina() {
        return $this->nomina;
    }
    function setPeriodo($var) {
        $this->periodo= $var;
    }

    function getPeriodo() {
        return $this->periodo;
    }

    function setProceso($var) {
        $this->proceso= $var;
    }

    function getProceso() {
        return $this->proceso;
    }


    function setNombreProceso($var) {
        $this->nombreProceso= $var;
    }

    function getNombreProceso() {
        return $this->nombreProceso;
    }


    function Header()
    {
        $periodo_fecha = "DESDE: " . $this->getDesde() . " HASTA: " . $this->getHasta();
        $this->SetDrawColor(255, 255, 255);
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0, 0, 0);

        $this->SetDrawColor(255, 255, 255); $this->SetFillColor(255, 255, 255); $this->SetTextColor(0, 0, 0);
        $this->SetFont('Arial', 'B', 8);

        $this->Cell(190, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->Cell(190, 5, ('DIRECCION DE RECURSOS HUMANOS'), 0, 1, 'L');
        $this->Cell(190, 5, ('RELACION DE NOMINA - '.$this->getTipoNom()), 0, 1, 'L');
        $this->Cell(190, 5, ($this->getPeriodo().' '.utf8_decode($this->getProceso())), 0, 1, 'L');

        $this->Cell(190, 5, 'Pagina: '.$this->PageNo().'/{nb}', 0, 1, 'R');
        $this->Ln(5);


        $this->SetDrawColor(0, 0, 0); $this->SetFillColor(255, 255, 255); $this->SetTextColor(0, 0, 0);
        $this->SetWidths(array(8, 20, 85, 24, 40));
        $this->SetAligns(array('C', 'C', 'C', 'C', 'C'));
        $this->Row(array(utf8_decode('Nº'),'CEDULA', 'APELLIDOS Y NOMBRES', 'MONTO Bs.', 'NRO. CUENTA'));

    }


    //Page footer
    function Footer()
    {

    }
}
