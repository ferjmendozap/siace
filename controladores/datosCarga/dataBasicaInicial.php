<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        13-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
trait dataBasicaInicial
{
    public function metDataPersona()
    {
        $a003_persona = array(
            array('pk_num_persona' => '1', 'ind_cedula_documento' => '00000000', 'ind_documento_fiscal' => '00000000-0', 'ind_nombre1' => 'Administrador', 'ind_nombre2' => NULL, 'ind_apellido1' => 'Admin', 'ind_apellido2' => NULL, 'ind_sexo' => NULL, 'ind_det_nacionalidad' => NULL, 'fec_nacimiento' => NULL, 'ind_lugar_nacimiento' => NULL, 'ind_estado_civil' => NULL, 'fec_estado_civil' => NULL, 'ind_direccion_domicilio_fiscal' => 'En el Sistema', 'ind_direccion_residencia' => NULL, 'ind_email' => NULL, 'ind_foto' => NULL, 'ind_tipo_persona' => 'N', 'num_estatus_persona' => 'E', 'fk_a006_num_miscelaneo_det_gruposangre' => NULL, 'fk_a006_num_miscelaneo_det_tipopersona' => NULL, 'fk_a010_num_ciudad' => 1, 'fk_a013_num_sector' => NULL)
        );
        return $a003_persona;
    }

    public function metDataEmpleado()
    {
        $rh_b001_empleado = array(
            array(
                'pk_num_empleado' => '1', 'fk_a003_num_persona' => '1', 'ind_estado_aprobacion' => '', 'num_estatus' => '',
                'fec_ultima_modificacion' => '2015-10-02 00:00:00', 'fk_a018_num_seguridad_usuario' => '1',
                'pk_num_seguridad_usuario' => '1', 'ind_usuario' => 'administrador', 'fk_rhb001_num_empleado' => '1',
                'ind_password' => '2b27201e5c74cca9a2d9b20632ea7da32c303c26', 'ind_template' => 'TemaBasico',
                'ind_ip' => '192.168.0.142'
            )
        );

        return $rh_b001_empleado;
    }

    public function metDataAplicacion()
    {
        $a015_seguridad_aplicacion = array(
            array('pk_num_seguridad_aplicacion' => '1', 'fk_a018_num_seguridad_usuario' => '1', 'cod_aplicacion' => 'AP', 'ind_nombre_modulo' => 'Aplicacion', 'ind_descripcion' => 'Modulo de la aplicacion general', 'fec_creacion' => '2015-09-24', 'num_estatus' => '1'),
            array('pk_num_seguridad_aplicacion' => '2', 'fk_a018_num_seguridad_usuario' => '1', 'cod_aplicacion' => 'CP', 'ind_nombre_modulo' => 'Cuentas Por Pagar', 'ind_descripcion' => 'Operaciones presupuestarias y financieras', 'fec_creacion' => '2015-09-24', 'num_estatus' => '1'),
            array('pk_num_seguridad_aplicacion' => '3', 'fk_a018_num_seguridad_usuario' => '1', 'cod_aplicacion' => 'LG', 'ind_nombre_modulo' => 'Logistica', 'ind_descripcion' => 'Modulo de control de compras e inventario de consumibles', 'fec_creacion' => '2015-09-24', 'num_estatus' => '1'),
            array('pk_num_seguridad_aplicacion' => '4', 'fk_a018_num_seguridad_usuario' => '1', 'cod_aplicacion' => 'CA', 'ind_nombre_modulo' => 'Control De Asistencias', 'ind_descripcion' => 'Modulo De Control De Asistencias', 'fec_creacion' => '2015-09-24', 'num_estatus' => '1'),
            array('pk_num_seguridad_aplicacion' => '5', 'fk_a018_num_seguridad_usuario' => '1', 'cod_aplicacion' => 'NM', 'ind_nombre_modulo' => 'Nomina', 'ind_descripcion' => 'Modulo De Nomina', 'fec_creacion' => '2015-09-24', 'num_estatus' => '1'),
            array('pk_num_seguridad_aplicacion' => '6', 'fk_a018_num_seguridad_usuario' => '1', 'cod_aplicacion' => 'PR', 'ind_nombre_modulo' => 'Presupesto', 'ind_descripcion' => 'Modulo De Presupuesto', 'fec_creacion' => '2015-09-24', 'num_estatus' => '1'),
            array('pk_num_seguridad_aplicacion' => '7', 'fk_a018_num_seguridad_usuario' => '1', 'cod_aplicacion' => 'CD', 'ind_nombre_modulo' => 'Control de Documentos', 'ind_descripcion' => 'Modulo De Control de Documentos', 'fec_creacion' => '2015-09-24', 'num_estatus' => '1'),
            array('pk_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_aplicacion' => 'AF', 'ind_nombre_modulo' => 'Activo Fijo', 'ind_descripcion' => 'Modulo De Activo Fijo', 'fec_creacion' => '2015-09-24', 'num_estatus' => '1'),
            array('pk_num_seguridad_aplicacion' => '9', 'fk_a018_num_seguridad_usuario' => '1', 'cod_aplicacion' => 'PF', 'ind_nombre_modulo' => 'Planificacion Fiscal', 'ind_descripcion' => 'Modulo De Planificacion Fiscal', 'fec_creacion' => '2015-09-24', 'num_estatus' => '1'),
            array('pk_num_seguridad_aplicacion' => '10', 'fk_a018_num_seguridad_usuario' => '1', 'cod_aplicacion' => 'CB', 'ind_nombre_modulo' => 'Contabilidad', 'ind_descripcion' => 'Contabilidad', 'fec_creacion' => '2015-09-24', 'num_estatus' => '1'),
            array('pk_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_aplicacion' => 'RH', 'ind_nombre_modulo' => 'Recursos Humanos', 'ind_descripcion' => 'Recursos Humanos', 'fec_creacion' => '2015-09-24', 'num_estatus' => '1'),
            array('pk_num_seguridad_aplicacion' => '12', 'fk_a018_num_seguridad_usuario' => '1', 'cod_aplicacion' => 'PA', 'ind_nombre_modulo' => 'Parque Automotor', 'ind_descripcion' => 'Parque Automotor', 'fec_creacion' => '2015-09-24', 'num_estatus' => '1'),
            array('pk_num_seguridad_aplicacion' => '13', 'fk_a018_num_seguridad_usuario' => '1', 'cod_aplicacion' => 'CR', 'ind_nombre_modulo' => 'Consumo de Red', 'ind_descripcion' => 'Consumo de Red', 'fec_creacion' => '2015-09-24', 'num_estatus' => '1'),
            array('pk_num_seguridad_aplicacion' => '14', 'fk_a018_num_seguridad_usuario' => '1', 'cod_aplicacion' => 'AD', 'ind_nombre_modulo' => 'Archivo Digital', 'ind_descripcion' => 'Archivo Digital', 'fec_creacion' => '2015-09-24', 'num_estatus' => '1'),
            array('pk_num_seguridad_aplicacion' => '15', 'fk_a018_num_seguridad_usuario' => '1', 'cod_aplicacion' => 'EV', 'ind_nombre_modulo' => 'Eventos', 'ind_descripcion' => 'Eventos', 'fec_creacion' => '2015-09-24', 'num_estatus' => '1'),
            array('pk_num_seguridad_aplicacion' => '16', 'fk_a018_num_seguridad_usuario' => '1', 'cod_aplicacion' => 'GT', 'ind_nombre_modulo' => 'Gestion de Contratos', 'ind_descripcion' => 'Gestion de Contratos', 'fec_creacion' => '2015-09-24', 'num_estatus' => '1'),
            array('pk_num_seguridad_aplicacion' => '17', 'fk_a018_num_seguridad_usuario' => '1', 'cod_aplicacion' => 'PY', 'ind_nombre_modulo' => 'Proyecciones', 'ind_descripcion' => 'Proyecciones', 'fec_creacion' => '2015-09-24', 'num_estatus' => '1'),
            array('pk_num_seguridad_aplicacion' => '18', 'fk_a018_num_seguridad_usuario' => '1', 'cod_aplicacion' => 'CV', 'ind_nombre_modulo' => 'Control de Visitas', 'ind_descripcion' => 'Control de Visitas', 'fec_creacion' => '2015-09-24', 'num_estatus' => '1'),
            array('pk_num_seguridad_aplicacion' => '19', 'fk_a018_num_seguridad_usuario' => '1', 'cod_aplicacion' => 'DN', 'ind_nombre_modulo' => 'Control de Denuncias', 'ind_descripcion' => 'Control de Denuncias', 'fec_creacion' => '2015-09-24', 'num_estatus' => '1'),
            array('pk_num_seguridad_aplicacion' => '20', 'fk_a018_num_seguridad_usuario' => '1', 'cod_aplicacion' => 'IN', 'ind_nombre_modulo' => 'INTRANET', 'ind_descripcion' => 'INTRANET', 'fec_creacion' => '2015-09-24', 'num_estatus' => '1')
        );
        return $a015_seguridad_aplicacion;
    }

    public function metDataPerfil()
    {
        $a017_seguridad_perfil = array(
            array('pk_num_seguridad_perfil' => '1', 'ind_nombre_perfil' => 'Programadores', 'ind_descripcion' => 'Perfil del Programado Tiene acceso a todo el sistema', 'num_estatus' => '1', 'fk_a018_num_seguridad_usuario' => '1', 'fec_ultima_modificacion' => '2015-11-04 10:33:57')
        );
        return $a017_seguridad_perfil;
    }
}
