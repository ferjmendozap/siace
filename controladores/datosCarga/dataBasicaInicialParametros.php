<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        13-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
trait dataBasicaInicialParametros
{
    private function metParametrosNomina()
    {
        $a035_parametros = array(
            array('ind_descripcion' => 'Proveedor de nomina','ind_explicacion' => '','ind_parametro_clave' => 'PROVEEDORNM','ind_valor_parametro' => '1','fk_a015_num_seguridad_aplicacion' => '5','cod_detalle' => 'N','cod_maestro' => 'TPPARAMET'),
            array('ind_descripcion' => 'centro de costo Nomina','ind_explicacion' => '','ind_parametro_clave' => 'CENTROCOSTONM','ind_valor_parametro' => 'DA','fk_a015_num_seguridad_aplicacion' => '5','cod_detalle' => 'T','cod_maestro' => 'TPPARAMET'),
            array('ind_descripcion' => 'tipo de servicio de nomina','ind_explicacion' => '','ind_parametro_clave' => 'TIPOSERVNM','ind_valor_parametro' => 'NING','fk_a015_num_seguridad_aplicacion' => '5','cod_detalle' => 'T','cod_maestro' => 'TPPARAMET'),
            array('ind_descripcion' => 'tipo pago de nomina','ind_explicacion' => '','ind_parametro_clave' => 'TIPOPAGONM','ind_valor_parametro' => '138','fk_a015_num_seguridad_aplicacion' => '5','cod_detalle' => 'N','cod_maestro' => 'TPPARAMET'),
            array('ind_descripcion' => 'numero de cuenta de nomina','ind_explicacion' => '','ind_parametro_clave' => 'CUENTANM','ind_valor_parametro' => '1','fk_a015_num_seguridad_aplicacion' => '5','cod_detalle' => 'N','cod_maestro' => 'TPPARAMET'),
        );
        return $a035_parametros;
    }

    private function metParametrosLG()
    {
        $a035_parametros = array(
            array('ind_descripcion' => 'Puntaje Máximo para el aspecto Cualitativo de la Evaluación','ind_explicacion' => 'Puntaje Máximo para el aspecto Cualitativo de la Evaluación','ind_parametro_clave' => 'PMEC','ind_valor_parametro' => '50','fk_a015_num_seguridad_aplicacion' => '3','cod_detalle' => 'N','cod_maestro' => 'TPPARAMET'
            )
        );
        return $a035_parametros;
    }

    private function metParametrosCP()
    {
        $a035_parametros = array(
            array('ind_descripcion' => 'CODIGO DEL TIPO DE TRANSACCION POR DEFECTO PARA LOS PAGOS EN CUENTAS POR PAGAR','ind_explicacion' => '','ind_parametro_clave' => 'TRANSPAGO','ind_valor_parametro' => 'PAG','fk_a015_num_seguridad_aplicacion' => '2','cod_detalle' => 'T','cod_maestro' => 'TPPARAMET'),
            array('ind_descripcion' => 'CODIGO DEL TIPO DE TRANSACCION POR DEFECTO PARA LA ANULACION DE PAGOS EN CUENTAS POR PAGAR', 'ind_explicacion' => '','ind_parametro_clave' => 'TRANSANUL','ind_valor_parametro' => 'RPA','fk_a015_num_seguridad_aplicacion' => '2','cod_detalle' => 'T','cod_maestro' => 'TPPARAMET'),
            array('ind_descripcion' => 'CENTRO DE COSTO POR DEFCTO PARA LOS VOUCHERS CONTABLES','ind_explicacion' => 'CENTRO DE COSTO POR DEFCTO PARA LOS VOUCHERS CONTABLES','ind_parametro_clave' => 'CCOSTOVOUCHER','ind_valor_parametro' => 'ARIN','num_estatus' => '1','fec_ultima_modificacion' => '2016-11-22 09:16:00','fk_a015_num_seguridad_aplicacion' => '2','cod_detalle' => 'T','cod_maestro' => 'TPPARAMET'),
            array('ind_descripcion' => 'IVGCODIGO','ind_explicacion' => '','ind_parametro_clave' => 'IVGCODIGO','ind_valor_parametro' => 'I01','num_estatus' => '1','fec_ultima_modificacion' => '2016-11-23 10:23:27','fk_a015_num_seguridad_aplicacion' => '2','cod_detalle' => 'T','cod_maestro' => 'TPPARAMET'),
            array('ind_descripcion' => 'CUENTA DEL IVA POR DEFECTO','ind_explicacion' => '','ind_parametro_clave' => 'IVACTADEF','ind_valor_parametro' => '6131701','num_estatus' => '1','fec_ultima_modificacion' => '2016-12-13 12:52:40','fk_a015_num_seguridad_aplicacion' => '2','cod_detalle' => 'T','cod_maestro' => 'TPPARAMET'),
            array('ind_descripcion' => 'Partida del IVA por defecto','ind_explicacion' => '','ind_parametro_clave' => 'IVADEFAULT','ind_valor_parametro' => '403.18.01.00','num_estatus' => '1','fec_ultima_modificacion' => '2016-12-12 15:37:59','fk_a015_num_seguridad_aplicacion' => '2','cod_detalle' => 'T','cod_maestro' => 'TPPARAMET'),
            array('ind_descripcion' => 'TIPO DE SEFVICIO POR DEFECTO DE CAJA CHICA PARA LAS OBLIGACIONES','ind_explicacion' => 'TIPO DE SEFVICIO POR DEFECTO DE CAJA CHICA PARA LAS OBLIGACIONES','ind_parametro_clave' => 'TSERVCC','ind_valor_parametro' => 'NOAFE','num_estatus' => '1','fec_ultima_modificacion' => '2016-12-07 16:41:29','fk_a015_num_seguridad_aplicacion' => '2','cod_detalle' => 'T','cod_maestro' => 'TPPARAMET'),
            array('ind_descripcion' => 'PORCENTAJE MAXIMO PARA REPOSICION POR CAJA CHICA','ind_explicacion' => 'PORCENTAJE MAXIMO PARA REPOSICION POR CAJA CHICA','ind_parametro_clave' => 'REPCC','ind_valor_parametro' => '75','num_estatus' => '1','fec_ultima_modificacion' => '2016-12-08 08:45:33','fk_a015_num_seguridad_aplicacion' => '2','cod_detalle' => 'N','cod_maestro' => 'TPPARAMET'),
            array('ind_descripcion' => 'Parametro para utilizar la opcion de revisado','ind_explicacion' => 'Parametro para utilizar la opcion de revisado','ind_parametro_clave' => 'REVOBLIG','ind_valor_parametro' => 'N','num_estatus' => '1','fec_ultima_modificacion' => '2017-01-12 09:27:59','fk_a015_num_seguridad_aplicacion' => '2','cod_detalle' => 'T','cod_maestro' => 'TPPARAMET'),
            array('ind_descripcion' => 'Parametro para establecer la Contabilidad a ser utilizada','ind_explicacion' => 'Parametro para establecer la Contabilidad a ser utilizada','ind_parametro_clave' => 'CONTABILIDAD','ind_valor_parametro' => 'PUB20','num_estatus' => '1','fec_ultima_modificacion' => '2017-01-23 10:25:08','fk_a015_num_seguridad_aplicacion' => '2','cod_detalle' => 'T','cod_maestro' => 'TPPARAMET')
        );
        return $a035_parametros;

    }

    private function metParametrosAF()
    {
        $a035_parametros = array(
        array('ind_descripcion' => 'DEPENDENCIA DEFAULT MÓDULO DE ACTIVOS FIJOS', 'ind_explicacion' => 'INFORMACIÓN A SER UTILIZADA EN LOS REPORTES DEL MÓDULO', 'ind_parametro_clave' => 'DEPENDAFDEF', 'ind_valor_parametro' => '12', 'fk_a015_num_seguridad_aplicacion' => '8','cod_detalle' => 'N','cod_maestro' => 'TPPARAMET'),
        array('ind_descripcion' => 'PERSONA CONFORMACIÓN DEFAULT ACTIVOS FIJOS', 'ind_explicacion' => 'ID DE LA PERSONA POR DEFECTO PARA LAS FIRMAS DE CONFORMACIÓN EN LOS REPORTES DEL MÓDULO', 'ind_parametro_clave' => 'CONFAFDEF', 'ind_valor_parametro' => '1', 'fk_a015_num_seguridad_aplicacion' => '8','cod_detalle' => 'N','cod_maestro' => 'TPPARAMET')
        );
        return $a035_parametros;
    }

    private function metParametrosIN()
    {
        $a035_parametros = array(
            array('ind_descripcion' => 'ID DEL ORGANISMO EXTERNO CONTRALORIA GENERAL DE LA REPUBLICA','ind_explicacion' => 'GUARDA EL ID (EN BD) DE LA CGR PARA TENER ACCESO DESDE LAS NOTICIAS   ','ind_parametro_clave' => 'IDCGRBD','ind_valor_parametro' => '4','num_estatus' => '1','fec_ultima_modificacion' => '2016-10-27 13:44:09','fk_a015_num_seguridad_aplicacion' => '20','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '2','cod_detalle' => 'N','cod_maestro' => 'TPPARAMET'),
            array('ind_descripcion' => 'INDICA SI EL CHAT ESTARA ACTIVO O NO','ind_explicacion' => 'INDICA SI EL CHAT ESTA ACTIVO O NO. 1: ACTIVO  2:INACTIVO','ind_parametro_clave' => 'ESTADOCHAT','ind_valor_parametro' => '2','num_estatus' => '1','fec_ultima_modificacion' => '2017-01-11 15:37:13','fk_a015_num_seguridad_aplicacion' => '20','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '2','cod_detalle' => 'N','cod_maestro' => 'TPPARAMET')
        );
        return $a035_parametros;

    }

    private function metParametrosDN()
    {
        $a035_parametros = array(
            array('pk_num_parametros' => '43','ind_descripcion' => 'Cantidad de digitos del correlativo','ind_explicacion' => 'Cantidad de digitos del correlativo aplicado en los codigos de las quejas, denuncias, reclamos, peticiones y sugerencias','ind_parametro_clave' => 'DIGITOS','ind_valor_parametro' => '4','num_estatus' => '1','fec_ultima_modificacion' => '2016-11-24 10:24:22','fk_a015_num_seguridad_aplicacion' => '19','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '2','cod_detalle' => 'N','cod_maestro' => 'TPPARAMET')
        );
        return $a035_parametros;
    }

    private function metParametrosRH()
    {
        $a035_parametros = array(
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'INDICA EL AÑO A TOMAR EN CUENTA PARA OBTENER EL VALOR DE LA UNIDAD TRIBUTARIA','ind_explicacion' => '','ind_parametro_clave' => 'UTANIO','ind_valor_parametro' => '2017','num_estatus' => '1','fec_ultima_modificacion' => '2016-08-23 11:03:49','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '1','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'TIEMPO MINIMO NECESARIO PARA PODER DESCONTAR DIA BONO ALIMENTACION','ind_explicacion' => 'TIEMPO MINIMO NECESARIO PARA PODER DESCONTAR DIA BONO ALIMENTACION','ind_parametro_clave' => 'UTDESC','ind_valor_parametro' => '02:00:00','num_estatus' => '1','fec_ultima_modificacion' => '2016-01-21 00:00:00','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '1','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'PORCENTAJE DE LA UNIDAD TRIBUTARIA A PAGAR POR BONO DE ALIMENTACION','ind_explicacion' => '','ind_parametro_clave' => 'UTPORC','ind_valor_parametro' => '338.983050847','num_estatus' => '1','fec_ultima_modificacion' => '2016-08-26 16:22:31','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '1','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'INDICA SI SE VA A PAGAR EL MES COMPLETO O NO. BONO ALIMENTACION','ind_explicacion' => 'INDICA SI SE VA A PAGAR EL MES COMPLETO O NO. BONO ALIMENTACION','ind_parametro_clave' => 'UTPAGOMES','ind_valor_parametro' => 'SI','num_estatus' => '1','fec_ultima_modificacion' => '2016-01-21 00:00:00','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '1','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'EDAD JUBILACION MASCULINO','ind_explicacion' => 'EDAD JUBILACION MASCULINO','ind_parametro_clave' => 'EDADJUBM','ind_valor_parametro' => '60','num_estatus' => '1','fec_ultima_modificacion' => '2016-02-19 16:40:04','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '1','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'MINIMO DE AÑOS REQUERIDOS SI CUMPLE CON TODOS LOS REQUISITOS DE LA JUBILACION','ind_explicacion' => '','ind_parametro_clave' => 'MINSERVSI','ind_valor_parametro' => '25','num_estatus' => '1','fec_ultima_modificacion' => '2016-03-03 09:05:13','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '1','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'EDAD JUBILACION FEMENINO','ind_explicacion' => 'EDAD JUBILACION FEMENINO','ind_parametro_clave' => 'EDADJUBF','ind_valor_parametro' => '55','num_estatus' => '1','fec_ultima_modificacion' => '2016-02-19 16:41:31','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '1','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'MINIMO DE AÑOS REQUERIDOS SI NO CUMPLE CON TODOS LOS REQUISITOS DE LA JUBILACION','ind_explicacion' => 'MINIMO DE AÑOS REQUERIDOS SI NO CUMPLE CON TODOS LOS REQUISITOS DE LA JUBILACION','ind_parametro_clave' => 'MINSERVNO','ind_valor_parametro' => '35','num_estatus' => '1','fec_ultima_modificacion' => '2016-02-19 16:42:21','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '1','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'COEFICIENTE A USAR PARA OBTENER EL MONTO DE LA JUBILACION','ind_explicacion' => '','ind_parametro_clave' => 'COEPORCJUB','ind_valor_parametro' => '2.5','num_estatus' => '1','fec_ultima_modificacion' => '2016-03-02 11:44:11','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '1','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'PORCENTAJE LIMITE CON RESPECTO AL SUELDO BASE DEL CALCULO DE LA JUBILACION','ind_explicacion' => 'PORCENTAJE LIMITE CON RESPECTO AL SUELDO BASE DEL CALCULO DE LA JUBILACION','ind_parametro_clave' => 'PORCLIMITJUB','ind_valor_parametro' => '80','num_estatus' => '1','fec_ultima_modificacion' => '2016-02-19 16:44:34','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '1','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'TIEMPO DE SERVICIO MINIMO PARA OPTAR A LA PENSION X INVALIDEZ','ind_explicacion' => 'TIEMPO DE SERVICIO MINIMO PARA OPTAR A LA PENSION X INVALIDEZ','ind_parametro_clave' => 'PENSERVMIN','ind_valor_parametro' => '3','num_estatus' => '1','fec_ultima_modificacion' => '2016-02-19 16:45:46','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '1','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'Edad máxima de pensión por sobreviviente a hijo estudiante','ind_explicacion' => 'Edad máxima de pensión por sobreviviente a hijo estudiante','ind_parametro_clave' => 'HIBENEST','ind_valor_parametro' => '18','num_estatus' => '1','fec_ultima_modificacion' => '2016-02-19 16:53:23','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '1','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'Edad máximo de hijo beneficiario','ind_explicacion' => '','ind_parametro_clave' => 'HIBEN','ind_valor_parametro' => '14','num_estatus' => '1','fec_ultima_modificacion' => '2016-02-19 17:23:58','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '1','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'PORCENTAJE MINIMO PARA ESTABLECER EL SUELDO DEL PENSIONADO X INVALIDEZ CON RESPECTO AL ULTIMO SUELDO','ind_explicacion' => 'PORCENTAJE MINIMO PARA ESTABLECER EL SUELDO DEL PENSIONADO X INVALIDEZ CON RESPECTO AL ULTIMO SUELDO','ind_parametro_clave' => 'PENPORMIN','ind_valor_parametro' => '50','num_estatus' => '1','fec_ultima_modificacion' => '2016-02-22 16:16:57','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '1','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'PORCENTAJE MAXIMO PARA ESTABLECER EL SUELDO DEL PENSIONADO X INVALIDEZ CON RESPECTO AL ULTIMO SUELDO','ind_explicacion' => 'PORCENTAJE MAXIMO PARA ESTABLECER EL SUELDO DEL PENSIONADO X INVALIDEZ CON RESPECTO AL ULTIMO SUELDO','ind_parametro_clave' => 'PENPORMAX','ind_valor_parametro' => '70','num_estatus' => '1','fec_ultima_modificacion' => '2016-02-22 16:17:39','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '1','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'PORCENTAJE MAXIMO PARA ESTABLECER EL SUELDO DEL PENSIONADO X SOBREVIVIENTE CON RESPECTO A SU JUBILACION','ind_explicacion' => 'PORCENTAJE MAXIMO PARA ESTABLECER EL SUELDO DEL PENSIONADO X SOBREVIVIENTE CON RESPECTO A SU JUBILACION','ind_parametro_clave' => 'PORSOBPORMAX','ind_valor_parametro' => '75','num_estatus' => '1','fec_ultima_modificacion' => '2016-03-02 14:36:23','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '1','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'Apertura del Periodo','ind_explicacion' => NULL,'ind_parametro_clave' => 'PERIODOVAC','ind_valor_parametro' => '1','num_estatus' => '1','fec_ultima_modificacion' => '2016-04-06 00:00:00','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '1','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'Manejo de Vacaciones','ind_explicacion' => '','ind_parametro_clave' => 'MANVAC','ind_valor_parametro' => 'quinquenio','num_estatus' => '1','fec_ultima_modificacion' => '2016-04-06 15:19:25','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '1','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'Código interno de la Dirección de Recursos Humanos','ind_explicacion' => 'Código interno de la Dirección de Recursos Humanos para generar los reportes correspondientes a la dependencia','ind_parametro_clave' => 'RHDEP','ind_valor_parametro' => '0002','num_estatus' => '1','fec_ultima_modificacion' => '2016-03-08 11:36:16','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '1','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'VALOR POR DEFECTO DE NACIONALIDAD','ind_explicacion' => 'VALOR POR DEFECTO DE NACIONALIDAD VENEZOLANO','ind_parametro_clave' => 'DEFAULTNACION','ind_valor_parametro' => '248','num_estatus' => '1','fec_ultima_modificacion' => '2016-09-08 10:17:36','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '2','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'VALOR POR DEFECTO PAIS','ind_explicacion' => 'VALOR POR DEFECTO PAIS','ind_parametro_clave' => 'DEFAULTPAIS','ind_valor_parametro' => '233','num_estatus' => '1','fec_ultima_modificacion' => '2016-09-08 10:32:35','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '2','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'VALOR POR DEFECTO DEL ESTADO','ind_explicacion' => 'VALOR POR DEFECTO DEL ESTADO','ind_parametro_clave' => 'DEFAULTESTADO','ind_valor_parametro' => '4157','num_estatus' => '1','fec_ultima_modificacion' => '2016-09-08 10:35:24','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '2','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'VALOR POR DEFECTO DEL MUNICIPIO','ind_explicacion' => 'VALOR POR DEFECTO DEL MUNICIPIO','ind_parametro_clave' => 'DEFAULTMUNICIPIO','ind_valor_parametro' => '244','num_estatus' => '1','fec_ultima_modificacion' => '2016-09-08 10:36:48','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '2','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'VALOR POR DEFECTO DE LA CIUDAD','ind_explicacion' => 'VALOR POR DEFECTO DE LA CIUDAD','ind_parametro_clave' => 'DEFAULTCIUDAD','ind_valor_parametro' => '2','num_estatus' => '1','fec_ultima_modificacion' => '2016-09-08 10:37:31','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '2','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'VALOR POR DEFECTO DE LA PARROQUIA','ind_explicacion' => 'VALOR POR DEFECTO DE LA PARROQUIA','ind_parametro_clave' => 'DEFAULTPARROQUIA','ind_valor_parametro' => '825','num_estatus' => '1','fec_ultima_modificacion' => '2016-09-20 14:14:59','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '2','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','cod_detalle'=> 'T', 'ind_descripcion' => 'VALOR POR DEFECTO DEL SECTOR','ind_explicacion' => 'VALOR POR DEFECTO DEL SECTOR','ind_parametro_clave' => 'DEFAULTSECTOR','ind_valor_parametro' => '1','num_estatus' => '1','fec_ultima_modificacion' => '2016-09-20 14:15:27','fk_a015_num_seguridad_aplicacion' => '11','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '2','cod_maestro' => 'TPPARAMET')
        );
        return $a035_parametros;
    }

    public function metParametrosPF()
    {
        $a035_parametros = array(
            array('pk_num_parametros' => '','ind_descripcion' => 'Permite establecer el prefijo(código interno ó abreviatura dependencia) para el cód. de las planificaciones fiscales: código interno(2) Abreviatura(1)','ind_explicacion' => 'si el valor es 1, será igual a la abreviatura, si es 2, va a ser igual al código interno','ind_parametro_clave' => 'PF_PREF_CODIGO','ind_valor_parametro' => '2','num_estatus' => '1','fec_ultima_modificacion' => '2017-02-06 10:57:49','fk_a015_num_seguridad_aplicacion' => '9','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '3821','cod_detalle' => 'N','cod_maestro' => 'TPPARAMET'),
            array('pk_num_parametros' => '','ind_descripcion' => 'Permite agregar el indicativo de tipo de procedimiento administrativo en el nro. de control de valoración jurídica y determinación de responsabilidad','ind_explicacion' => 'Si se desea que en el número de control de una Valoración Juridíca y determinación de responsabilidad aparezca el indicativo del tipo de procedimiento, el valor es 1, de lo contrario, si se desea que no aparezca, el valor del parámetro es 2','ind_parametro_clave' => 'PF_TPA_CODIGO','ind_valor_parametro' => '2','num_estatus' => '1','fec_ultima_modificacion' => '2017-04-25 15:15:46','fk_a015_num_seguridad_aplicacion' => '9','fk_a018_num_seguridad_usuario' => '1','fk_a006_num_miscelaneo_detalle' => '3821','cod_detalle' => 'N','cod_maestro' => 'TPPARAMET')
        );
        return $a035_parametros;
    }

    public function metDataParametros()
    {
        $a035_parametros = array_merge(
            $this->metParametrosNomina(),
            $this->metParametrosCP(),
            $this->metParametrosDN(),
            $this->metParametrosRH(),
            $this->metParametrosIN(),
            $this->metParametrosAF(),
            $this->metParametrosLG(),
            $this->metParametrosPF()
        );
        return $a035_parametros;
    }
}
