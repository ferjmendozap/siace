<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        13-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
trait dataBasicaInicialMiscelaneos
{
    public function metMiscelaneosDanielAPyNM()
    {
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--->>Modulo AP<br>';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--->>Modulo NM<br>';
        $a005_miscelaneo_maestro = array(
            array('ind_nombre_maestro' => 'TIPO DE PARAMETROS', 'ind_descripcion' => 'TIPO DE PARAMETRO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '1', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TPPARAMET', 'fec_ultima_modificacion' => '2015-12-12 10:18:39',
                'Det' => array(
                    array('ind_nombre_maestro' => 'TIPO DE PARAMETROS', 'cod_detalle' => 'T', 'ind_nombre_detalle' => 'TEXTO', 'num_estatus' => '1'),
                    array('ind_nombre_maestro' => 'TIPO DE PARAMETROS', 'cod_detalle' => 'N', 'ind_nombre_detalle' => 'NUMERO', 'num_estatus' => '1'),
                    array('ind_nombre_maestro' => 'TIPO DE PARAMETROS', 'cod_detalle' => 'F', 'ind_nombre_detalle' => 'FECHA', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'Tipos De Conceptos', 'ind_descripcion' => 'Tipos de Conceptos de Nomina', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '5', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TDCNM', 'fec_ultima_modificacion' => '2015-11-25 14:53:53',
                'Det' => array(
                    array('ind_nombre_maestro' => 'Tipos De Conceptos', 'cod_detalle' => 'I', 'ind_nombre_detalle' => 'Ingresos', 'num_estatus' => '1'),
                    array('ind_nombre_maestro' => 'Tipos De Conceptos', 'cod_detalle' => 'T', 'ind_nombre_detalle' => 'Totales', 'num_estatus' => '1'),
                    array('ind_nombre_maestro' => 'Tipos De Conceptos', 'cod_detalle' => 'A', 'ind_nombre_detalle' => 'Aportes', 'num_estatus' => '1'),
                    array('ind_nombre_maestro' => 'Tipos De Conceptos', 'cod_detalle' => 'R', 'ind_nombre_detalle' => 'Retenciones', 'num_estatus' => '1'),
                    array('ind_nombre_maestro' => 'Tipos De Conceptos', 'cod_detalle' => 'D', 'ind_nombre_detalle' => 'Descuentos', 'num_estatus' => '1'),
                    array('ind_nombre_maestro' => 'Tipos De Conceptos', 'cod_detalle' => 'AS', 'ind_nombre_detalle' => 'Asignaciones', 'num_estatus' => '1'),
                    array('ind_nombre_maestro' => 'Tipos De Conceptos', 'cod_detalle' => 'B', 'ind_nombre_detalle' => 'Bonificaciones', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'JUSTIFICATIVO ANTICIPO', 'ind_descripcion' => 'JUSTIFICATIVO DE LOS  ANTICIPO DE PRESTACIONES SOCIALES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '5', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'JUSTANT', 'fec_ultima_modificacion' => '2015-12-03 08:30:11',
                'Det' => array(
                    array('ind_nombre_maestro' => 'JUSTIFICATIVO ANTICIPO', 'cod_detalle' => '1', 'ind_nombre_detalle' => 'Construcción adquisición mejora o reparación de vivienda para mí y mi familia', 'num_estatus' => '1'),
                    array('ind_nombre_maestro' => 'JUSTIFICATIVO ANTICIPO', 'cod_detalle' => '2', 'ind_nombre_detalle' => 'La liberación de hipoteca o de cualquier otro gravamen sobre vivienda de mi propiedad', 'num_estatus' => '1'),
                    array('ind_nombre_maestro' => 'JUSTIFICATIVO ANTICIPO', 'cod_detalle' => '3', 'ind_nombre_detalle' => 'Las pensiones escolares para mí mi conyugué hijos o con quien haga vida marital', 'num_estatus' => '1'),
                    array('ind_nombre_maestro' => 'JUSTIFICATIVO ANTICIPO', 'cod_detalle' => '4', 'ind_nombre_detalle' => 'Los gastos por atención médica y hospitalaria de las personas indicadas en el literal anterior', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'Tipo de Obligaciones Nomina', 'ind_descripcion' => 'Tipos de obligaciones en nomina', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '5', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TPDOB', 'fec_ultima_modificacion' => '2016-01-14 14:54:38',
                'Det' => array(
                    array('ind_nombre_maestro' => 'Tipo de Obligaciones Nomina', 'cod_detalle' => 'A', 'ind_nombre_detalle' => 'Aportes', 'num_estatus' => '1'),
                    array('ind_nombre_maestro' => 'Tipo de Obligaciones Nomina', 'cod_detalle' => 'R', 'ind_nombre_detalle' => 'Retenciones', 'num_estatus' => '1'),
                    array('ind_nombre_maestro' => 'Tipo de Obligaciones Nomina', 'cod_detalle' => 'AR', 'ind_nombre_detalle' => 'Aporte y Retenciones', 'num_estatus' => '1')
                )
            ),
        );
        return $a005_miscelaneo_maestro;
    }

    public function metMiscelaneosYohandryCP()
    {
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--->>Modulo CP<br>';
        $a005_miscelaneo_maestro = array(
            array('ind_nombre_maestro' => 'PROVISION ','ind_descripcion' => 'PROVISION','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '2','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'PROVDOC',
                'Det' => array(
                    array('cod_detalle' => 'N','ind_nombre_detalle' => 'Provisión del Documento','num_estatus' => '1'),
                    array('cod_detalle' => 'P','ind_nombre_detalle' => 'Pago del Documento','num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'IMPONIBLE','ind_descripcion' => 'Imponible','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '2','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'IVAIMP',
                'Det' => array(
                    array('cod_detalle' => 'I','ind_nombre_detalle' => 'IGV IVA','num_estatus' => '1'),
                    array('cod_detalle' => 'N','ind_nombre_detalle' => 'Monto Afecto','num_estatus' => '1')

                )
            ),
            array('ind_nombre_maestro' => 'TIPO','ind_descripcion' => 'Tipo','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '2','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'COMPIMP',
                'Det' => array(
                    array('cod_detalle' => 'IVA','ind_nombre_detalle' => 'IVA','num_estatus' => '1'),
                    array('cod_detalle' => 'ISLR','ind_nombre_detalle' => 'ISLR','num_estatus' => '1'),
                    array('cod_detalle' => '1X10','ind_nombre_detalle' => '1X1000','num_estatus' => '1'),
                    array('cod_detalle' => 'DESC','ind_nombre_detalle' => 'DESCUENTO','num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'CLASIFICACION DEL IMPUESTO','ind_descripcion' => 'Clasificación','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '2','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'CLASIMP',
                'Det' => array(
                    array('cod_detalle' => 'A','ind_nombre_detalle' => 'Adicional','num_estatus' => '1'),
                    array('cod_detalle' => 'R','ind_nombre_detalle' => 'Reducido','num_estatus' => '1'),
                    array('cod_detalle' => 'G','ind_nombre_detalle' => 'General','num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'CLASIFICACION DEL DOCUMENTO','ind_descripcion' => 'Clasificación del Documento','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '2','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'CLASDOC',
                'Det' => array(
                    array('cod_detalle' => 'O','ind_nombre_detalle' => 'Obligaciones','num_estatus' => '1'),
                    array('cod_detalle' => 'C','ind_nombre_detalle' => 'Otros de Ctas por Pagar','num_estatus' => '1'),
                    array('cod_detalle' => 'P','ind_nombre_detalle' => 'Prestamos','num_estatus' => '1'),
                    array('cod_detalle' => 'E','ind_nombre_detalle' => 'Otros Externos','num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'REGIMEN FISCAL','ind_descripcion' => 'REGIMEN FISCAL','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '2','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'REGFISCAL',
                'Det' => array(
                    array('cod_detalle' => 'I','ind_nombre_detalle' => 'Registro de Compras','num_estatus' => '1'),
                    array('cod_detalle' => 'M','ind_nombre_detalle' => 'Multiple','num_estatus' => '1'),
                    array('cod_detalle' => 'N','ind_nombre_detalle' => 'Ninguno','num_estatus' => '1'),
                    array('cod_detalle' => 'R','ind_nombre_detalle' => 'Renta','num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'BANCO','ind_descripcion' => 'BANCO','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '2','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'BANCOS',
                'Det' => array(
                    array('cod_detalle' => '0001','ind_nombre_detalle' => 'BANCO DE VENEZUELA','num_estatus' => '1'),
                    array('cod_detalle' => '0002','ind_nombre_detalle' => 'BANCO EXTERIOR','num_estatus' => '1'),
                    array('cod_detalle' => '0003','ind_nombre_detalle' => 'BANCO BANESCO','num_estatus' => '1'),
                    array('cod_detalle' => '0004','ind_nombre_detalle' => 'BANCO ACTIVO','num_estatus' => '1'),
                    array('cod_detalle' => '0005','ind_nombre_detalle' => 'BANCO BANESCO 4187','num_estatus' => '1'),
                    array('cod_detalle' => '0006','ind_nombre_detalle' => 'CAJA CHICA','num_estatus' => '1'),
                    array('cod_detalle' => '0007','ind_nombre_detalle' => 'BANCO DEL TESORO','num_estatus' => '1'),
                    array('cod_detalle' => '0008','ind_nombre_detalle' => 'BANCO FONDO COMUN','num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'TIPO CUENTA BANCARIA','ind_descripcion' => 'TIPO CUENTA BANCARIA','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '2','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'TIPCUENTAB',
                'Det' => array(
                    array('cod_detalle' => 'AH','ind_nombre_detalle' => 'Ahorro','num_estatus' => '1'),
                    array('cod_detalle' => 'CO','ind_nombre_detalle' => 'Corriente','num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE TRANSACCION','ind_descripcion' => 'TIPO DE TRANSACCION','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '2','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'TIPOTRANS',
                'Det' => array(
                    array('cod_detalle' => 'I','ind_nombre_detalle' => 'INGRESO','num_estatus' => '1'),
                    array('cod_detalle' => 'E','ind_nombre_detalle' => 'EGRESO','num_estatus' => '1'),
                    array('cod_detalle' => 'T','ind_nombre_detalle' => 'TRANSACCION','num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'APLICACION DE CLASIFICACION DE GASTOS','ind_descripcion' => 'APLICACION DE CLASIFICACION DE GASTOS','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '2','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'APLICG',
                'Det' => array(
                    array('cod_detalle' => 'CC','ind_nombre_detalle' => 'Caja Chica','num_estatus' => '1'),
                    array('cod_detalle' => 'CR','ind_nombre_detalle' => 'Reporte Gasto','num_estatus' => '1'),
                    array('cod_detalle' => 'AP','ind_nombre_detalle' => 'Adelanto a Proveedores','num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'GRUPO DE CLASIFICACION DE GASTOS','ind_descripcion' => 'GRUPO DE CLASIFICACION DE GASTOS','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '2','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'GRUPGAST',
                'Det' => array(
                    array('cod_detalle' => '001','ind_nombre_detalle' => 'Productos Alimenticios y Agropecuarios','num_estatus' => '1'),
                    array('cod_detalle' => '002','ind_nombre_detalle' => 'Productos de Minas, Canteras y Yacimientos','num_estatus' => '1'),
                    array('cod_detalle' => '003','ind_nombre_detalle' => 'Productos Textiles y Vestuarios','num_estatus' => '1'),
                    array('cod_detalle' => '004','ind_nombre_detalle' => 'Productos de Cuero y Cauchos','num_estatus' => '1'),
                    array('cod_detalle' => '005','ind_nombre_detalle' => 'Productos de Papel Carton e Impresos','num_estatus' => '1'),
                    array('cod_detalle' => '006','ind_nombre_detalle' => 'Productos Quimicos y Derivados','num_estatus' => '1'),
                    array('cod_detalle' => '007','ind_nombre_detalle' => 'Productos Minerales no Metalicos','num_estatus' => '1'),
                    array('cod_detalle' => '008','ind_nombre_detalle' => 'Productos Metalicos','num_estatus' => '1'),
                    array('cod_detalle' => '009','ind_nombre_detalle' => 'Productos de Madera','num_estatus' => '1'),
                    array('cod_detalle' => '010','ind_nombre_detalle' => 'Productos Varios y Utiles Diversos','num_estatus' => '1'),
                    array('cod_detalle' => '011','ind_nombre_detalle' => 'Otros Materiales y Suministros','num_estatus' => '1'),
                    array('cod_detalle' => '012','ind_nombre_detalle' => 'Alquileres de Maquinaria y Equipos','num_estatus' => '1'),
                    array('cod_detalle' => '013','ind_nombre_detalle' => 'Servicios Básicos','num_estatus' => '1'),
                    array('cod_detalle' => '014','ind_nombre_detalle' => 'Servicios de Transporte y Almacenaje','num_estatus' => '1'),
                    array('cod_detalle' => '015','ind_nombre_detalle' => 'Servicios de Información, Impresión y Relaciones Públicas','num_estatus' => '1'),
                    array('cod_detalle' => '016','ind_nombre_detalle' => 'Primas y otros Gastos de Seguros y Comisiones Bancarias','num_estatus' => '1'),
                    array('cod_detalle' => '017','ind_nombre_detalle' => 'Viáticos y Pasajes','num_estatus' => '1'),
                    array('cod_detalle' => '018','ind_nombre_detalle' => 'Servicios Profesionales y Técnicos','num_estatus' => '1'),
                    array('cod_detalle' => '019','ind_nombre_detalle' => 'Conservación y Reparaciones Menores de Maquinaria y Equipos','num_estatus' => '1'),
                    array('cod_detalle' => '020','ind_nombre_detalle' => 'Conservación y Reparaciones Menores de Obras','num_estatus' => '1'),
                    array('cod_detalle' => '021','ind_nombre_detalle' => 'Servicios de Construcciones Temporales','num_estatus' => '1'),
                    array('cod_detalle' => '022','ind_nombre_detalle' => 'Servicios Fiscales','num_estatus' => '1'),
                    array('cod_detalle' => '023','ind_nombre_detalle' => 'Servicios de Diversión, Esparcimiento y Culturales','num_estatus' => '1'),
                    array('cod_detalle' => '024','ind_nombre_detalle' => 'Servicios de Gestión Administrativa Prestados','num_estatus' => '1'),
                    array('cod_detalle' => '025','ind_nombre_detalle' => 'Impuestos Indirectos','num_estatus' => '1'),
                    array('cod_detalle' => '026','ind_nombre_detalle' => 'Otros Servicios no Personales','num_estatus' => '1'),
                    array('cod_detalle' => '027','ind_nombre_detalle' => 'Reparaciones, mejoras y adiciones mayores de maqui','num_estatus' => '1')
                )
            ),

            array('ind_nombre_maestro' => 'TIPO VIATICO','ind_descripcion' => 'TIPO VIATICO','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '2','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'TVIATICO',
                'Det' => array(
                    array('cod_detalle' => '01','ind_nombre_detalle' => 'INTERIOR DEL ESTADO CON TRANSPORTE INSTITUCIONAL','num_estatus' => '1'),
                    array('cod_detalle' => '02','ind_nombre_detalle' => 'INTERIOR DEL ESTADO SIN TRANSPORTE INSTITUCIONAL','num_estatus' => '1'),
                    array('cod_detalle' => '03','ind_nombre_detalle' => 'FUERA DEL ESTADO CON TRANSPORTE INSTITUCIONAL','num_estatus' => '1'),
                    array('cod_detalle' => '04','ind_nombre_detalle' => 'FUERA DEL ESTADO SIN TRANSPORTE INSTITUCIONAL','num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE GASTO','ind_descripcion' => 'TIPO DE GASTO PARA VIATICOS','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '2','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'TGASTOVIAT',
                'Det' => array(
                    array('cod_detalle' => '01','ind_nombre_detalle' => 'Alojamiento','num_estatus' => '1'),
                    array('cod_detalle' => '02','ind_nombre_detalle' => 'Alimentacion','num_estatus' => '1'),
                    array('cod_detalle' => '03','ind_nombre_detalle' => 'Transporte','num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE CUENTA','ind_descripcion' => 'TIPO DE CUENTA','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '2','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'TCTA',
                'Det' => array(
                    array('cod_detalle' => 'BA','ind_nombre_detalle' => 'BALANCE A','num_estatus' => '1'),
                    array('cod_detalle' => 'BP','ind_nombre_detalle' => 'BALANCE PASIVO','num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'CATEGORIAS DE CONCEPTOS DE VIATICOS','ind_descripcion' => 'CATEGORIAS DE CONCEPTOS DE VIATICOS','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '2','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'CATVIAT',
                'Det' => array(
                    array('cod_detalle' => 'PE','ind_nombre_detalle' => 'PERSONAS EXTERNAS','num_estatus' => '1'),
                    array('cod_detalle' => 'MA','ind_nombre_detalle' => 'MAXIMAS AUTORIDADES','num_estatus' => '1'),
                    array('cod_detalle' => 'JE','ind_nombre_detalle' => 'JEFES','num_estatus' => '1'),
                    array('cod_detalle' => 'GE','ind_nombre_detalle' => 'GENERAL','num_estatus' => '1'),
                    array('cod_detalle' => 'FU','ind_nombre_detalle' => 'FUNCIONARIOS','num_estatus' => '1'),
                    array('cod_detalle' => 'DI','ind_nombre_detalle' => 'DIRECTIVOS','num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'ARTICULO DE CONCEPTOS DE VIATICOS','ind_descripcion' => 'ARTICULO DE CONCEPTOS DE VIATICOS','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '2','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'ARTVIAT',
                'Det' => array(
                    array('cod_detalle' => '12','ind_nombre_detalle' => 'ARTICULO 12','num_estatus' => '1'),
                    array('cod_detalle' => '14','ind_nombre_detalle' => 'ARTICULO 14','num_estatus' => '1')
                )
            )
        );
        return $a005_miscelaneo_maestro;
    }

    public function metMiscelaneosFernandoLG()
    {
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--->>Modulo LG<br>';
        $a005_miscelaneo_maestro = array(
            array('ind_nombre_maestro' => 'Forma de Pago', 'ind_descripcion' => 'Forma de Pago', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 3, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'FDPLG',
                'Det' => array(
                    array('cod_detalle' => '001', 'ind_nombre_detalle' => 'CONTADO', 'num_estatus' => '1'),
                    array('cod_detalle' => '002', 'ind_nombre_detalle' => 'CREDITO A 15 DIAS', 'num_estatus' => '1'),
                    array('cod_detalle' => '003', 'ind_nombre_detalle' => 'CREDITO A 30 DIAS', 'num_estatus' => '1'),
                    array('cod_detalle' => '004', 'ind_nombre_detalle' => 'CREDITO A 45 DIAS', 'num_estatus' => '1'),
                    array('cod_detalle' => '005', 'ind_nombre_detalle' => 'CREDITO A 60 DIAS', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'Condición RNC', 'ind_descripcion' => 'Condición RNC', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 3, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'IRCN',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Empresa suspendida por el Art 30 la LCP', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Empresa registrada en el RNC', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Empresa en proceso de descapitalización', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'Suspendido por el Art 139', 'num_estatus' => '1'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'Empresa del Gobierno', 'num_estatus' => '1'),
                    array('cod_detalle' => '6', 'ind_nombre_detalle' => 'CALIFICADA', 'num_estatus' => '1'),
                    array('cod_detalle' => '7', 'ind_nombre_detalle' => 'INSCRITA Y ACTUALIZADA', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'LINEAS', 'ind_descripcion' => 'LINEAS', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 3, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'LINEAS',
                'Det' => array(
                    array('cod_detalle' => '000001', 'ind_nombre_detalle' => 'Materiales y Equipos de Oficina', 'num_estatus' => '1'),
                    array('cod_detalle' => '000002', 'ind_nombre_detalle' => 'Articulos de Limpieza', 'num_estatus' => '1'),
                    array('cod_detalle' => '000003', 'ind_nombre_detalle' => 'Utensilios y Enseres', 'num_estatus' => '1'),
                    array('cod_detalle' => '000004', 'ind_nombre_detalle' => 'Ferreteria Herramientas y Equipos', 'num_estatus' => '1'),
                    array('cod_detalle' => '000005', 'ind_nombre_detalle' => 'Materiales y Productos Plasticos', 'num_estatus' => '1'),
                    array('cod_detalle' => '000006', 'ind_nombre_detalle' => 'Materiales y Accesorios Electricos', 'num_estatus' => '1'),
                    array('cod_detalle' => '000007', 'ind_nombre_detalle' => 'Mobiliario', 'num_estatus' => '1'),
                    array('cod_detalle' => '000008', 'ind_nombre_detalle' => 'Alimentos y Bebidas', 'num_estatus' => '1'),
                    array('cod_detalle' => '000009', 'ind_nombre_detalle' => 'Instalciones Sanitarias', 'num_estatus' => '1'),
                    array('cod_detalle' => '000010', 'ind_nombre_detalle' => 'Productos de Seguridad en el Trabajo', 'num_estatus' => '1'),
                    array('cod_detalle' => '000011', 'ind_nombre_detalle' => 'Productos y Materiales de Construcción', 'num_estatus' => '1'),
                    array('cod_detalle' => '000012', 'ind_nombre_detalle' => 'Tintas Pinturas y Colorantes', 'num_estatus' => '1'),
                    array('cod_detalle' => '000013', 'ind_nombre_detalle' => 'Accesorios Productos Materiales de Vehiculo', 'num_estatus' => '1'),
                    array('cod_detalle' => '000014', 'ind_nombre_detalle' => 'Productos Agricolas y Pecuarios', 'num_estatus' => '1'),
                    array('cod_detalle' => '000016', 'ind_nombre_detalle' => 'Prendas y Accesorios de Vestir', 'num_estatus' => '1'),
                    array('cod_detalle' => '000017', 'ind_nombre_detalle' => 'Equipos y Accesorios de Telecomunicaciones', 'num_estatus' => '1'),
                    array('cod_detalle' => '000018', 'ind_nombre_detalle' => 'Equipo de Computacion', 'num_estatus' => '1'),
                    array('cod_detalle' => '000019', 'ind_nombre_detalle' => 'Cauchos Tripas para Vehiculos', 'num_estatus' => '1'),
                    array('cod_detalle' => '000020', 'ind_nombre_detalle' => 'Servicios', 'num_estatus' => '1'),
                    array('cod_detalle' => '000021', 'ind_nombre_detalle' => 'Condecoraciones', 'num_estatus' => '1'),
                    array('cod_detalle' => '000022', 'ind_nombre_detalle' => 'Activos', 'num_estatus' => '1'),
                    array('cod_detalle' => '000023', 'ind_nombre_detalle' => 'Materiales para Equipos de Computación', 'num_estatus' => '1'),
                    array('cod_detalle' => '000024', 'ind_nombre_detalle' => 'Uniformes', 'num_estatus' => '1'),
                    array('cod_detalle' => '000025', 'ind_nombre_detalle' => 'Material    de Señalamiento', 'num_estatus' => '1'),
                    array('cod_detalle' => '000026', 'ind_nombre_detalle' => 'Imprenta y Reproduccion', 'num_estatus' => '1'),
                    array('cod_detalle' => '000027', 'ind_nombre_detalle' => 'Repuestos y Accesorios para Equipos de Transporte', 'num_estatus' => '1'),
                    array('cod_detalle' => '000028', 'ind_nombre_detalle' => 'Otros Materiales y Suministros', 'num_estatus' => '1'),
                    array('cod_detalle' => '000029', 'ind_nombre_detalle' => 'Productos Minerales No Metalicos', 'num_estatus' => '1'),
                    array('cod_detalle' => '000030', 'ind_nombre_detalle' => 'Productos Metalicos', 'num_estatus' => '1'),
                    array('cod_detalle' => '000031', 'ind_nombre_detalle' => 'Productos Quimicos y Derivados', 'num_estatus' => '1'),
                    array('cod_detalle' => '000032', 'ind_nombre_detalle' => 'Instrumentos Musicales', 'num_estatus' => '1'),
                    array('cod_detalle' => '000033', 'ind_nombre_detalle' => 'Beneficio Socio Economico Empleados', 'num_estatus' => '1'),
                    array('cod_detalle' => '000034', 'ind_nombre_detalle' => 'Otros Activos Reales', 'num_estatus' => '1'),
                    array('cod_detalle' => '000035', 'ind_nombre_detalle' => 'Equipos Materiales y Accesorios Quirurgicos', 'num_estatus' => '1'),
                    array('cod_detalle' => '000036', 'ind_nombre_detalle' => 'Productos Farmaceuticos o Derivados', 'num_estatus' => '1'),
                    array('cod_detalle' => '000037', 'ind_nombre_detalle' => 'Productos de Papel y Cartón', 'num_estatus' => '1'),
                    array('cod_detalle' => '000038', 'ind_nombre_detalle' => 'Otros Productos y Utiles Diversos', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'Tipo de Pago', 'ind_descripcion' => 'Tipo de Pago', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 3, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'TDPLG',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'ABONO EN CUENTA', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'CHEQUE', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'EFECTIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'TRANSFERENCIA', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'CHEQUE GERENCIA', 'num_estatus' => '1'),
                    array('cod_detalle' => '06', 'ind_nombre_detalle' => 'NOTA DE DEBITO', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'Tipo de Medida', 'ind_descripcion' => 'Tipo de Medida', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 3, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'TIPOMED',
                'Det' => array(
                    array('cod_detalle' => 'LO', 'ind_nombre_detalle' => 'LONGITUD', 'num_estatus' => '1'),
                    array('cod_detalle' => 'MA', 'ind_nombre_detalle' => 'MANEJO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PE', 'ind_nombre_detalle' => 'PESO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'TE', 'ind_nombre_detalle' => 'TEMPERATURA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'TI', 'ind_nombre_detalle' => 'TIEMPO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'VO', 'ind_nombre_detalle' => 'VOLUMEN', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE REQUERIMIENTO', 'ind_descripcion' => 'TIPO DE REQUERIMIENTO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 3, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'TIPOREQ',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'ITEMS DE STOCK', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'CARGO DIRECTO', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'ORDEN DE SERVICIO', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE ITEM', 'ind_descripcion' => 'TIPO DE ITEM', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 3, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'TIPOITM',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'Herramientas y Materiales de Ferreteria', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'Mobiliario', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'Productos o Accesorios de Limpieza', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'Equipos o Utiles de Oficina', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'Electrodomestico', 'num_estatus' => '1'),
                    array('cod_detalle' => '06', 'ind_nombre_detalle' => 'Alimentos y Bebidas', 'num_estatus' => '1'),
                    array('cod_detalle' => '07', 'ind_nombre_detalle' => 'Productos o Accesorios de Plasticos', 'num_estatus' => '1'),
                    array('cod_detalle' => '08', 'ind_nombre_detalle' => 'Materiales Electricos', 'num_estatus' => '1'),
                    array('cod_detalle' => '09', 'ind_nombre_detalle' => 'Instalaciones Sanitarias', 'num_estatus' => '1'),
                    array('cod_detalle' => '10', 'ind_nombre_detalle' => 'Materiales y Productos de Construccion', 'num_estatus' => '1'),
                    array('cod_detalle' => '11', 'ind_nombre_detalle' => 'Tinta Pinturas y Colorantes', 'num_estatus' => '1'),
                    array('cod_detalle' => '12', 'ind_nombre_detalle' => 'Vehiculos y Transporte', 'num_estatus' => '1'),
                    array('cod_detalle' => '13', 'ind_nombre_detalle' => 'Productos Agricolas y Pecuarios', 'num_estatus' => '1'),
                    array('cod_detalle' => '14', 'ind_nombre_detalle' => 'Textiles', 'num_estatus' => '1'),
                    array('cod_detalle' => '15', 'ind_nombre_detalle' => 'Equipos de Telecomunicaciones', 'num_estatus' => '1'),
                    array('cod_detalle' => '16', 'ind_nombre_detalle' => 'Productos de Seguridad en el Trabajo', 'num_estatus' => '1'),
                    array('cod_detalle' => '17', 'ind_nombre_detalle' => 'Equipo de Computacion', 'num_estatus' => '1'),
                    array('cod_detalle' => '18', 'ind_nombre_detalle' => 'Cauchos y Tripas para Vehiculos', 'num_estatus' => '1'),
                    array('cod_detalle' => '19', 'ind_nombre_detalle' => 'Servicios', 'num_estatus' => '1'),
                    array('cod_detalle' => '20', 'ind_nombre_detalle' => 'Condecoraciones Ofrentas y Similares', 'num_estatus' => '1'),
                    array('cod_detalle' => '21', 'ind_nombre_detalle' => 'Activo', 'num_estatus' => '1'),
                    array('cod_detalle' => '22', 'ind_nombre_detalle' => 'Materiales para Equipos de Computación', 'num_estatus' => '1'),
                    array('cod_detalle' => '23', 'ind_nombre_detalle' => 'Uniformes', 'num_estatus' => '1'),
                    array('cod_detalle' => '24', 'ind_nombre_detalle' => 'Material de Señalamiento', 'num_estatus' => '1'),
                    array('cod_detalle' => '25', 'ind_nombre_detalle' => 'Otros Materiales y Suministros', 'num_estatus' => '1'),
                    array('cod_detalle' => '26', 'ind_nombre_detalle' => 'Productos Minerales No Metalicos', 'num_estatus' => '1'),
                    array('cod_detalle' => '27', 'ind_nombre_detalle' => 'Productos de Metales no Ferrosos', 'num_estatus' => '1'),
                    array('cod_detalle' => '28', 'ind_nombre_detalle' => 'Productos Quimicos y Lubricantes', 'num_estatus' => '1'),
                    array('cod_detalle' => '29', 'ind_nombre_detalle' => 'Instrumentos Musicales', 'num_estatus' => '1'),
                    array('cod_detalle' => '30', 'ind_nombre_detalle' => 'Productos Quimicos y Derivados', 'num_estatus' => '1'),
                    array('cod_detalle' => '31', 'ind_nombre_detalle' => 'Otros Productos de Industrias Quimicas y Conexo', 'num_estatus' => '1'),
                    array('cod_detalle' => '32', 'ind_nombre_detalle' => 'Productos Metalicos', 'num_estatus' => '1'),
                    array('cod_detalle' => '33', 'ind_nombre_detalle' => 'Beneficio Socio Economico Empleados', 'num_estatus' => '1'),
                    array('cod_detalle' => '34', 'ind_nombre_detalle' => 'Equipos Accesorios y Materiales Quirurgicos', 'num_estatus' => '1'),
                    array('cod_detalle' => '35', 'ind_nombre_detalle' => 'Productos Farmaceuticos o Derivados', 'num_estatus' => '1'),
                    array('cod_detalle' => '36', 'ind_nombre_detalle' => 'Productos de Papel y Cartón', 'num_estatus' => '1'),
                    array('cod_detalle' => '37', 'ind_nombre_detalle' => 'Otros Productos y Utiles Diversos', 'num_estatus' => '1'),
                    array('cod_detalle' => '38', 'ind_nombre_detalle' => 'Utensilios de Cocina', 'num_estatus' => '1'),
                    array('cod_detalle' => '39', 'ind_nombre_detalle' => 'Repuestos y accesorios para otros equipos', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'PRIORIDAD DE REQUERIMIENTOS', 'ind_descripcion' => 'PRIORIDAD DE REQUERIMIENTOS', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 3, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'PRIOR',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Normal', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Urgente', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Muy Urgente, Solicitado por el Contralor', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'ALMACEN', 'ind_descripcion' => 'ALMACENES DISPONIBLES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 3, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'ALM',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Almacen Contraloría', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Almacen Commoditis', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'Tipo de Adjudicacion', 'ind_descripcion' => 'Tipo de Adjudicacion', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 3, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'TIPOADJ',
                'Det' => array(
                    array('cod_detalle' => 'DT', 'ind_nombre_detalle' => 'DIRECTO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'TT', 'ind_nombre_detalle' => 'TOTAL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PR', 'ind_nombre_detalle' => 'PARCIAL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'DE', 'ind_nombre_detalle' => 'DESIERTO', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'Modalidad de Pago', 'ind_descripcion' => 'Modalidad de Pago', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 3, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'MOD',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Adquisición de Bienes', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Prestación de Servicio', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Ejecucion de Obras', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'TIPO TRANSACCION', 'ind_descripcion' => 'TIPO DE TRANSACCION DE LOGISTICA', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 3, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'TIPOTRAN',
                'Det' => array(
                    array('cod_detalle' => 'I', 'ind_nombre_detalle' => 'INGRESO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'E', 'ind_nombre_detalle' => 'EGRESO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'T', 'ind_nombre_detalle' => 'TRANSFERENCIA', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'TIPO ASPECTO EVALUACION', 'ind_descripcion' => 'ASPECTO A EVALUAR CUALITATIVO-CUANTITATIVO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 3, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'TAE',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'Cualitativo', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'Cuantitativo', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'FIRMANTES DEL CONTROL PERCEPTIVO', 'ind_descripcion' => 'FIRMANTES DEL CONTROL PERCEPTIVO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 3, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'FCP',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Área de Bienes', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Unidad Contratante', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Unidad Usuaria', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'Control Previo', 'num_estatus' => '1'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'Área de Almacén', 'num_estatus' => '1')
                )
            )
        );


        return $a005_miscelaneo_maestro;
    }

    public function metMiscelaneosSergioCV()
    {
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--->>Modulo CV<br>';
        $a005_miscelaneo_maestro = array(
            array('ind_nombre_maestro' => 'Motivos de Visita', 'ind_descripcion' => 'Motivos de Visita', 'num_estatus' => 1, 'fk_a015_num_seguridad_aplicacion' => 18, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'CVMOTVIS',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'CURSO', 'num_estatus' => 1),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'ENTREGA DE DOCUMENTOS', 'num_estatus' => 1),
                )
            ),
            array('ind_nombre_maestro' => 'Destino de la Visita', 'ind_descripcion' => 'Hasia donde se dirige la visita', 'num_estatus' => 1, 'fk_a015_num_seguridad_aplicacion' => 18, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'CVDESVIS',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'ADMINISTRACION', 'num_estatus' => 1),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'FUNDICEM', 'num_estatus' => 1),
                )
            ),
        );
        return $a005_miscelaneo_maestro;
    }

    public function metMiscelaneosFernandoMendozaRH()
    {
        echo '---------------->>Modulo RH<br>';
        $a005_miscelaneo_maestro = array(
            array('ind_nombre_maestro' => 'ESTADO CIVIL', 'ind_descripcion' => 'ESTADO CIVIL', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'EDOCIVIL',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'SOLTERO (A)', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'CASADO (A)', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'DIVORCIADO (A)', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'VIUDO (A)', 'num_estatus' => '1'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'CONCUBINO (A)', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO TRABAJADOR', 'ind_descripcion' => 'TIPO DE TRABAJADOR ', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOTRAB',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'ACTIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'INACTIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'JUBILADO', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'PENSIONADO POR INVALIDEZ', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'PENSIONADO SOBREVIVIENTE', 'num_estatus' => '1'),
                    array('cod_detalle' => '06', 'ind_nombre_detalle' => 'CONTRATADO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'GRUPO OCUPACIONAL ', 'ind_descripcion' => 'GRUPO OCUPACIONAL DEL CARGO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'GRUPOCUP',
                'Det' => array(
                    array('cod_detalle' => 'TEC', 'ind_nombre_detalle' => 'TÉCNICO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'ADMI', 'ind_nombre_detalle' => 'ADMINISTRATIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PRO', 'ind_nombre_detalle' => 'PROFESIONAL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GRH', 'ind_nombre_detalle' => 'GRUPO DE RECURSOS HUMANOS', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDT', 'ind_nombre_detalle' => 'GRUPO DIRECCIÓN TÉCNICA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GPPE', 'ind_nombre_detalle' => 'GRUPO DIRECCIÓN DE CONTROL DE LOS PODERES PÚBLICOS ESTADALES', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDCD', 'ind_nombre_detalle' => 'GRUPO DIRECCIÒN DE CONTROL DE LA ADMINISTRACIÓN ESTADAL DESCENTRALALIZADA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'DDC', 'ind_nombre_detalle' => 'GRUPO DESPACHO CONTRALOR', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GUAI', 'ind_nombre_detalle' => 'GRUPO UNIDAD DE AUDITORIA INTERNA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GOAC', 'ind_nombre_detalle' => 'GRUPO OFICINA DE ATENCIÓN AL CIUDADANO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDG', 'ind_nombre_detalle' => 'GRUPO DIRECCIÓN GENERAL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GADM', 'ind_nombre_detalle' => 'GRUPO DIRECCIÓN DE ADMINISTRACIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDSJ', 'ind_nombre_detalle' => 'GRUPO DIRECCIÓN DE SERVICIOS JURÍDICOS', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDRA', 'ind_nombre_detalle' => 'GRUPO DIRECCIÓN DE DETERMINACIÓN DE RESPONSABILIDADES', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDSG', 'ind_nombre_detalle' => 'GRUPO DE SERVICIOS GENERALES', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDCP', 'ind_nombre_detalle' => 'GRUPO DIVISIÓN DE CONTROL PREVIO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDVT', 'ind_nombre_detalle' => 'GRUPO DIVISIÓN TECNICA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDVF', 'ind_nombre_detalle' => 'GRUPO DIVISIÓN DE FISCALIZACIÓN E INVESTIGACIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GUAA', 'ind_nombre_detalle' => 'GRUPO UNIDAD AVERIGUACIONES ADMINISTRATIVA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDE', 'ind_nombre_detalle' => 'GRUPO DIVISIÓN DE EXAMEN', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GSC', 'ind_nombre_detalle' => 'GRUPO SALA DE CONTROL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDCE', 'ind_nombre_detalle' => 'GRUPO DIVISIÓN DE CENTRALIZACIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GJP', 'ind_nombre_detalle' => 'GRUPO DE JEFATURA DE PERSONAL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDFI', 'ind_nombre_detalle' => 'GRUPO DIVISIÓN DE FISCALIZACIÓN E INSPECCIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDI', 'ind_nombre_detalle' => 'GRUPO DIVISIÓN INSPECCIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDAP', 'ind_nombre_detalle' => 'GRUPO DE DIVISION DE ADMINISTRACION Y CONTROL DE PERSONAL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GSA', 'ind_nombre_detalle' => 'GRUPO SALA DE AUDITORIA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDCO', 'ind_nombre_detalle' => 'GRUPO DIVISIÓN DE CONTROL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDBI', 'ind_nombre_detalle' => 'GRUPO DIVISIÓN DE BIENES Y MATERIAS', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDSP', 'ind_nombre_detalle' => 'GRUPO DIRECCIÓN DE SISTEMAS Y PROCEDIMIENTOS', 'num_estatus' => '1'),
                    array('cod_detalle' => 'DA', 'ind_nombre_detalle' => 'DEPARTAMENTO DE ARCHIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GSB', 'ind_nombre_detalle' => 'GRUPO SALA DE BIENES', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'SEXO DE LA PERSONA', 'ind_descripcion' => 'SEXO DE LA PERSONA', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'SEXO',
                'Det' => array(
                    array('cod_detalle' => 'M', 'ind_nombre_detalle' => 'MASCULINO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'F', 'ind_nombre_detalle' => 'FEMENINO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'NACIONALIDADES', 'ind_descripcion' => 'NACIONALIDADES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'NACION',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'VENEZOLANO(A)', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'EXTRANJERO(A)', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'SITUACION DOMICILIO', 'ind_descripcion' => 'TIPO SITUACION DOMICILIO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'SITDOM',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'ALQUILER', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'PROPIA', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'COMPARTIDA', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'CON FAMILIA', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'EN RESIDENCIA', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'GRUPOS SANGUINEOS', 'ind_descripcion' => 'TIPOS DE GRUPOS SANGUINEOS', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'SANGRE',
                'Det' => array(
                    array('cod_detalle' => 'A', 'ind_nombre_detalle' => 'GRUPO A NEGATIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'A', 'ind_nombre_detalle' => 'GRUPO A POSITIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'B', 'ind_nombre_detalle' => 'GRUPO B NEGATIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'B', 'ind_nombre_detalle' => 'GRUPO B POSITIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'O', 'ind_nombre_detalle' => 'GRUPO O NEGATIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'O', 'ind_nombre_detalle' => 'GRUPO O POSITIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'AB', 'ind_nombre_detalle' => 'GRUPO AB NEGATIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'AB', 'ind_nombre_detalle' => 'GRUPO AB POSITIVO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'PARENTESCO', 'ind_descripcion' => 'PARENTESCO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'PARENT',
                'Det' => array(
                    array('cod_detalle' => 'Am', 'ind_nombre_detalle' => 'AMIGO (A)', 'num_estatus' => '1'),
                    array('cod_detalle' => 'CB', 'ind_nombre_detalle' => 'CONCUBINO (A)', 'num_estatus' => '1'),
                    array('cod_detalle' => 'ES', 'ind_nombre_detalle' => 'ESPOSO (A)', 'num_estatus' => '1'),
                    array('cod_detalle' => 'HE', 'ind_nombre_detalle' => 'HERMANO (A)', 'num_estatus' => '1'),
                    array('cod_detalle' => 'HI', 'ind_nombre_detalle' => 'HIJO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'MA', 'ind_nombre_detalle' => 'MADRE', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PA', 'ind_nombre_detalle' => 'PADRE', 'num_estatus' => '1'),
                    array('cod_detalle' => 'SB', 'ind_nombre_detalle' => 'SOBRINO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'SU', 'ind_nombre_detalle' => 'SUEGRO (A)', 'num_estatus' => '1'),
                    array('cod_detalle' => 'TI', 'ind_nombre_detalle' => 'TIO (A)', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO LICENCIA', 'ind_descripcion' => 'TIPO LICENCIA', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOLIC',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'PRIMERA', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'SEGUNDA', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'TERCERA', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'CUARTA', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'QUINTA', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'CATEGORIA CARGO', 'ind_descripcion' => 'CATEGORIA CARGO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'CATCARGO',
                'Det' => array(
                    array('cod_detalle' => 'EMPL', 'ind_nombre_detalle' => 'EMPLEADO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'OBRE', 'ind_nombre_detalle' => 'OBRERO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'GRADO INSTRUCCIÓN', 'ind_descripcion' => 'GRADO INSTRUCCIÓN PERSONA', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'GRADOINST',
                'Det' => array(
                    array('cod_detalle' => 'PRI', 'ind_nombre_detalle' => 'PRIMARIA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'SEC', 'ind_nombre_detalle' => 'SECUNDARIA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'BAC', 'ind_nombre_detalle' => 'BACHILLER', 'num_estatus' => '1'),
                    array('cod_detalle' => 'TSU', 'ind_nombre_detalle' => 'TÉCNICO SUPERIOR UNIVERSITARIO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'UNI', 'ind_nombre_detalle' => 'UNIVERSITARIO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'POS', 'ind_nombre_detalle' => 'POSTGRADO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'MAG', 'ind_nombre_detalle' => 'MAGISTER', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PRE', 'ind_nombre_detalle' => 'PREESCOLAR', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'AREA  DE FORMACIÓN', 'ind_descripcion' => 'TIPOS DE AREA DE FORMACIÓN', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AREA',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'CONTABLES ADMINISTRATIVAS Y ECONOMICAS', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'COMPUTACIÓN E INFORMÁTICA', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'RELACIONES INDUSTRIALES Y RRHH', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'CONSTRUCCION Y DISEÑO', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'NDUSTRIA MECANICA Y ELECTRICIDAD', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'PREGUNTAS DEL CLIMA LABORAL', 'ind_descripcion' => 'AREA CLIMA LABORAL', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AREACLIMA',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'ITEMS', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'LIDERAZGO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'GRADOS ESTUDIOS', 'ind_descripcion' => 'GRADOS ESTUDIOS', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'GRADOEST',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'PRIMER GRADO', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'SEGUNDO GRADO ', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'UTILES ESCOLARES', 'ind_descripcion' => 'UTILES ESCOLARES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'UTILESESC',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'LAPIZ', 'num_estatus' => '1'),
                    array('cod_detalle' => 'CAS', 'ind_nombre_detalle' => 'CUADERNOS', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO TELEFONO', 'ind_descripcion' => 'TIPO TELEFONO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOTELF',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'CASA', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'CELULAR', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'FAX', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE DIRECCIÓN', 'ind_descripcion' => 'TIPO DE DIRECCIÓN', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPODIR',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'DIRECCIÓN FISCAL', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'DIRECCIÓN RESIDENCIAL', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'CLASE DE LICENCIA', 'ind_descripcion' => 'CLASE DE LICENCIA DE CONDUCIR', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'CLASELIC',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'VEHICULO', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'MOTO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE EDUCACION', 'ind_descripcion' => 'TIPO DE EDUCACION', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TEDUCA',
                'Det' => array(
                    array('cod_detalle' => 'PR', 'ind_nombre_detalle' => 'PRIVADA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PU', 'ind_nombre_detalle' => 'PUBLICA', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'AREA DE EXPERIENCIA', 'ind_descripcion' => 'AREA DE EXPERIENCIA', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AREAEXP',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'ADMINISTRACIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'FISCALAUDITORIA', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'TRANSPORTE', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'CONTROL DE CUENTAS BANCARIAS', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'COMPRAS Y LOGISTICA', 'num_estatus' => '1'),
                    array('cod_detalle' => '06', 'ind_nombre_detalle' => 'CREDITOS Y COBRANZAS', 'num_estatus' => '1'),
                    array('cod_detalle' => '07', 'ind_nombre_detalle' => 'COMPUTACION E INFORMATICA', 'num_estatus' => '1'),
                    array('cod_detalle' => '08', 'ind_nombre_detalle' => 'SERVICIOS GENERALES', 'num_estatus' => '1'),
                    array('cod_detalle' => '09', 'ind_nombre_detalle' => 'ADMINISTRACIÓN DE RECURSOS HUMANOS', 'num_estatus' => '1'),
                    array('cod_detalle' => '10', 'ind_nombre_detalle' => 'JUDICIAL', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE CUENTAS BANCARIAS', 'ind_descripcion' => 'TIPO DE CUENTAS BANCARIAS', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOCTA',
                'Det' => array(
                    array('cod_detalle' => 'AH', 'ind_nombre_detalle' => 'AHORRO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'CO', 'ind_nombre_detalle' => 'CORRIENTE', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE APORTE', 'ind_descripcion' => 'TIPO DE APORTE', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOAPO',
                'Det' => array(
                    array('cod_detalle' => 'FI', 'ind_nombre_detalle' => 'FIDEICOMISO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PN', 'ind_nombre_detalle' => 'PAGO DE NOMINA', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPOS DE DOCUMENTOS PERSONALES DEL EMPLEADO', 'ind_descripcion' => 'TIPOS DE DOCUMENTOS PERSONALES DEL EMPLEADO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'DOCUMENTOS',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'COPIA CEDULA IDENTIDAD', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'PARTIDA DE NACIMIENTO', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'ACTA DE MATRIMONIO', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'CARTA DE RESIDENCIA', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'CERTIFICADO DE SALUD', 'num_estatus' => '1'),
                    array('cod_detalle' => '06', 'ind_nombre_detalle' => 'RIF', 'num_estatus' => '1'),
                    array('cod_detalle' => '07', 'ind_nombre_detalle' => 'CONSTANCIA DE BUENA CONDUCTA', 'num_estatus' => '1'),
                    array('cod_detalle' => '08', 'ind_nombre_detalle' => 'COPIA DE TITULO DE BACHILLER', 'num_estatus' => '1'),
                    array('cod_detalle' => '09', 'ind_nombre_detalle' => 'COPIA DEL TITULO UNIVERSITARIO', 'num_estatus' => '1'),
                    array('cod_detalle' => '10', 'ind_nombre_detalle' => 'COPIA CERTIFICADOS CURSOS', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE ENTIDAD', 'ind_descripcion' => 'TIPO DE ENTIDAD', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOENTE',
                'Det' => array(
                    array('cod_detalle' => 'P', 'ind_nombre_detalle' => 'PRIVADA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'G', 'ind_nombre_detalle' => 'GUBERNAMENTAL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'O', 'ind_nombre_detalle' => 'OTRA', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'MERITOS DEL EMPLEADOS', 'ind_descripcion' => 'MERITOS DEL EMPLEADOS', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'MERITO',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'AGRADECIMIENTO', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'FELICITACIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'SOBRESALIENTE', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'NOTABLE', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'HONOR AL MERITO', 'num_estatus' => '1'),
                    array('cod_detalle' => '06', 'ind_nombre_detalle' => 'ANIVERSARIO', 'num_estatus' => '1'),
                    array('cod_detalle' => '07', 'ind_nombre_detalle' => 'RECONOCIMIENTO', 'num_estatus' => '1'),
                    array('cod_detalle' => '08', 'ind_nombre_detalle' => 'BOTON DE HONOR', 'num_estatus' => '1'),
                    array('cod_detalle' => '09', 'ind_nombre_detalle' => 'ORDEN MERITO AL TRABAJO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'DEMERITOS DEL EMPLEADO', 'ind_descripcion' => 'DEMERITOS DEL EMPLEADO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'DEMERITO',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'AMONESTACION', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'FALTA GRAVE', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'LLAMADA DE ATENCION', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'SUSPENSION', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO MERITO DEMERITO', 'ind_descripcion' => 'TIPO MERITO DEMERITO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'MERDEM',
                'Det' => array(
                    array('cod_detalle' => 'ME', 'ind_nombre_detalle' => 'MERITO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'DE', 'ind_nombre_detalle' => 'DEMERITO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'COLORES', 'ind_descripcion' => 'COLRES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'COLOR',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'AMARRILLO', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'AZUL', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'ROJO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE REFERENCIAS ', 'ind_descripcion' => 'TIPO DE REFERENCIAS DEL EMPLEADO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOREF',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'LABORAL', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'PERSONAL', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'NIVEL DE CONOCIMIENTOS', 'ind_descripcion' => 'NIVEL DE CONOCIMIENTOS', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'NIVEL',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'BASICO', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'INTERMEDIO', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'AVANZADO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPOS DE CURSOS', 'ind_descripcion' => 'TIPOS DE CURSOS', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOCURSO',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'CURSO', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'TALLER', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'CONFERENCIA', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'SIMPOSIO', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'CONGRESO', 'num_estatus' => '1'),
                    array('cod_detalle' => '06', 'ind_nombre_detalle' => 'SEMINARIO', 'num_estatus' => '1'),
                    array('cod_detalle' => '07', 'ind_nombre_detalle' => 'CHARLA', 'num_estatus' => '1'),
                    array('cod_detalle' => '08', 'ind_nombre_detalle' => 'ENCUENTRO', 'num_estatus' => '1'),
                    array('cod_detalle' => '09', 'ind_nombre_detalle' => 'PROGRAMA DE FORMACIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => '10', 'ind_nombre_detalle' => 'ENTRENAMIENTO', 'num_estatus' => '1'),
                    array('cod_detalle' => '11', 'ind_nombre_detalle' => 'FORO', 'num_estatus' => '1'),
                    array('cod_detalle' => '12', 'ind_nombre_detalle' => 'JORNADA', 'num_estatus' => '1'),
                    array('cod_detalle' => '13', 'ind_nombre_detalle' => 'INDUCCIÓN', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'COLEGIOS DE PROFESIONALES', 'ind_descripcion' => 'COLEGIOS DE PROFESIONALES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'COLEGIOS',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'CONTADORES', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'MEDICOS', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'INGENIEROS', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'ABOGADOS', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'ECONOMISTAS', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'MOTIVO DE AUSENCIA', 'ind_descripcion' => 'Motivo de ausencia para solicitar permisos en Recursos Humanos', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'MOTAUS',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Matrimonio', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Enfermedad', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Accidente', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'Fallecimiento de familiar', 'num_estatus' => '1'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'Comparecencia ante la autoridad', 'num_estatus' => '1'),
                    array('cod_detalle' => '6', 'ind_nombre_detalle' => 'Licencia de Paternidad', 'num_estatus' => '1'),
                    array('cod_detalle' => '7', 'ind_nombre_detalle' => 'Prenatal', 'num_estatus' => '1'),
                    array('cod_detalle' => '8', 'ind_nombre_detalle' => 'Postnatal', 'num_estatus' => '1'),
                    array('cod_detalle' => '9', 'ind_nombre_detalle' => 'Permiso de lactancia', 'num_estatus' => '1'),
                    array('cod_detalle' => '10', 'ind_nombre_detalle' => 'Asistencia a seminario conferencia o congreso', 'num_estatus' => '1'),
                    array('cod_detalle' => '11', 'ind_nombre_detalle' => 'Beca', 'num_estatus' => '1'),
                    array('cod_detalle' => '12', 'ind_nombre_detalle' => 'Enfermedad de familiar', 'num_estatus' => '1'),
                    array('cod_detalle' => '13', 'ind_nombre_detalle' => 'Accidente de familiar', 'num_estatus' => '1'),
                    array('cod_detalle' => '14', 'ind_nombre_detalle' => 'Estudios', 'num_estatus' => '1'),
                    array('cod_detalle' => '15', 'ind_nombre_detalle' => 'Exámenes', 'num_estatus' => '1'),
                    array('cod_detalle' => '16', 'ind_nombre_detalle' => 'Diligencia', 'num_estatus' => '1'),
                    array('cod_detalle' => '17', 'ind_nombre_detalle' => 'Actividades Deportivas', 'num_estatus' => '1'),
                    array('cod_detalle' => '18', 'ind_nombre_detalle' => 'Asistencia a cursos', 'num_estatus' => '1'),
                    array('cod_detalle' => '19', 'ind_nombre_detalle' => 'Otro', 'num_estatus' => '1'),
                    array('cod_detalle' => '20', 'ind_nombre_detalle' => 'Auditoría', 'num_estatus' => '1'),
                    array('cod_detalle' => '21', 'ind_nombre_detalle' => 'Diligencia CES', 'num_estatus' => '1'),
                    array('cod_detalle' => '22', 'ind_nombre_detalle' => 'Cita médica', 'num_estatus' => '1'),
                    array('cod_detalle' => '23', 'ind_nombre_detalle' => 'Reposo Médico', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE AUSENCIA', 'ind_descripcion' => 'Tipo de ausencia para solicitar permisos en Recursos Humanos', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPAUS',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Inasistencia', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Entrada Tarde ', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Salida Temporal', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'Salida Temprana', 'num_estatus' => '1'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'Permiso', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE ACCIONES', 'ind_descripcion' => 'TIPO DE ACCION DE CONTROL DE NIVELACIONES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOACCION',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'AJUSTE SALARIAL', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'AJUSTE DE ESCALA', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'CAMBIO DE POSICIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'MERITO', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'PROMOCIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => '06', 'ind_nombre_detalle' => 'MOVIMIENTO ENTRE DEPENDENCIAS', 'num_estatus' => '1'),
                    array('cod_detalle' => 'ET', 'ind_nombre_detalle' => 'ENCARGADURIA TEMPORAL', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO CESE REINGRESO', 'ind_descripcion' => 'TIPO CESE REINGRESO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOCR',
                'Det' => array(
                    array('cod_detalle' => 'C', 'ind_nombre_detalle' => 'CESE', 'num_estatus' => '1'),
                    array('cod_detalle' => 'R', 'ind_nombre_detalle' => 'REINGRESO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE SOLICITUD HCM', 'ind_descripcion' => 'tipos de solicitud en el proceso HCM', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'SOLHCM',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Reembolso', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Emisión', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'ESTADO HCM', 'ind_descripcion' => 'Estadoss de las solicitudes de beneficio hcm', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'ESTADOHCM',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Preparado', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Revisado', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Aprobado', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'Anulado', 'num_estatus' => '1'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'Modificado', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE VACACIÓN', 'ind_descripcion' => 'Tipo de vacación ', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPVAC',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'GOCE', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'INTERRUPCIÓN', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'RAMAS MEDICINA', 'ind_descripcion' => 'Ramas de la medicina', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'RAMAS',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Oftalmología', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Hematología', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Cardiología', 'num_estatus' => '1'),
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Preparado', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Enviado a Revisión', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Revisado', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'Conformado', 'num_estatus' => '1'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'Aprobado', 'num_estatus' => '1'),
                    array('cod_detalle' => '6', 'ind_nombre_detalle' => 'Anulado', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'CLASE INSTITUCIÓN', 'ind_descripcion' => 'Clase de institución', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'CLASEINST',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Pública', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Privada', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Sin fines de lucro', 'num_estatus' => '1'),
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Preparado', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Modificado', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Enviado a Revisión', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'Revisado', 'num_estatus' => '1'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'Conformado', 'num_estatus' => '1'),
                    array('cod_detalle' => '6', 'ind_nombre_detalle' => 'Aprobado', 'num_estatus' => '1'),
                    array('cod_detalle' => '7', 'ind_nombre_detalle' => 'Anulado', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE INSTITUCIÓN', 'ind_descripcion' => 'Tipo de institución HCM', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOINST',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Hospital', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Clínica', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Ambulatorio', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'CDI', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'MOTIVO DE PENSIÓN', 'ind_descripcion' => 'MOTIVO DE PENSIÓN DE LA PERSONA', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'MOTIPEN',
                'Det' => array(
                    array('cod_detalle' => 'INV', 'ind_nombre_detalle' => 'INVALIDEZ', 'num_estatus' => '1'),
                    array('cod_detalle' => 'INC', 'ind_nombre_detalle' => 'INCAPACIDAD', 'num_estatus' => '1'),
                    array('cod_detalle' => 'FA', 'ind_nombre_detalle' => 'FALLECIMIENTO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE PENSIÓN', 'ind_descripcion' => 'TIPO DE PENSIÓN DE LA PERSONA', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOPEN',
                'Det' => array(
                    array('cod_detalle' => 'INV', 'ind_nombre_detalle' => 'INVALIDEZ', 'num_estatus' => '1'),
                    array('cod_detalle' => 'SOB', 'ind_nombre_detalle' => 'SOBREVIVIENTE', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'REQUISITOS UTILES', 'ind_descripcion' => 'REQUISITOS PARA OPTAR UTILES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'REQUTILES',
                'Det' => array(
                    array('cod_detalle' => 'CE', 'ind_nombre_detalle' => 'CONSTANCIA DE ESTUDIOS', 'num_estatus' => '1'),
                    array('cod_detalle' => 'CI', 'ind_nombre_detalle' => 'CONSTANCIA DE INSCRIPCIÓN', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO ASIGNACIÓN UTILES', 'ind_descripcion' => 'TIPO DE ASIGNACION DE UTILES BENEFICIO DE UTILES ESCOLARES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOASGUTI',
                'Det' => array(
                    array('cod_detalle' => 'AD', 'ind_nombre_detalle' => 'ASIGNACIÓN DIRECTA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'AF', 'ind_nombre_detalle' => 'ASIGNACIÓN POR FACTURAS', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE PAGO NOMINA', 'ind_descripcion' => 'TIPO DE PAGO NOMINA', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOPAGO',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'ABONO EN CUENTA', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'CHEQUE', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'EFECTIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'TRANSFERENCIA', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'CHEQUE GERENCIA', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'MODALIDAD DE LA CAPACITACIONES', 'ind_descripcion' => 'MODALIDAD DE LA CAPACITACIONES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'MODACAPAC',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'PRESENCIAL', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'SEMIPRESENCIAL', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'A DISTANCIA', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'INTERNET', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'OTROS', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'ORIGEN DE LA CAPACITACION', 'ind_descripcion' => 'ORIGEN DE LAS CAPACITACIONES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'ORIGCAP',
                'Det' => array(
                    array('cod_detalle' => 'EXT', 'ind_nombre_detalle' => 'EXTERNO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'INT', 'ind_nombre_detalle' => 'INTERNO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'PASOS GRADO SALARIAL', 'ind_descripcion' => 'PASOS GRADO SALARIAL', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'PASOS',
                'Det' => array(
                    array('cod_detalle' => 'NA', 'ind_nombre_detalle' => 'NO APLICA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PA', 'ind_nombre_detalle' => 'PASO A', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PB', 'ind_nombre_detalle' => 'PASO B', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PC', 'ind_nombre_detalle' => 'PASO C', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PD', 'ind_nombre_detalle' => 'PASO D', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE RETENCIONES JUDICIALES', 'ind_descripcion' => 'TIPO DE RETENCIONES JUDICIALES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'RJUDICIAL',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'OBLIGACIÓN ALIMENTARIA', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE DESCUENTOS', 'ind_descripcion' => 'TIPO DE DESCUENTOS RETENCIONES JUDICIALES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPODESC',
                'Det' => array(
                    array('cod_detalle' => 'P', 'ind_nombre_detalle' => 'PORCENTUAL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'M', 'ind_nombre_detalle' => 'MONTO', 'num_estatus' => '1'),
                )
            )
        );

        return $a005_miscelaneo_maestro;
    }

    public function metMiscelaneosPA()
    {
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--->>Modulo PA<br>';
        $a005_miscelaneo_maestro = array( array('ind_nombre_maestro' => 'MARCA DE VEHICULOS', 'ind_descripcion' => 'MARCA DE VEHICULOS',  'fk_a015_num_seguridad_aplicacion' => '12', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'MARVEH',
            'Det' => array(
                array('cod_detalle' => '01', 'ind_nombre_detalle' => 'CHEVROLET'),
                array('cod_detalle' => '02', 'ind_nombre_detalle' => 'CHRYSLER'),
                array('cod_detalle' => '03', 'ind_nombre_detalle' => 'TOYOTA'),
                array('cod_detalle' => '04', 'ind_nombre_detalle' => 'FIAT'),
                array('cod_detalle' => '05', 'ind_nombre_detalle' => 'FORD'),
            )
        ),
            array('ind_nombre_maestro' => 'Motivo de Salida', 'ind_descripcion' => 'Motivo de salida de un vehículo',  'fk_a015_num_seguridad_aplicacion' => '12', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'MOTSAL',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Diligencia CE'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Compras'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Jornada OAC'),
                )
            ),
            array('ind_nombre_maestro' => 'Clase de Vehículo', 'ind_descripcion' => 'Clase de Vehículo',  'fk_a015_num_seguridad_aplicacion' => '12', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'CLASEVEH',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Carro'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Moto'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Camioneta'),
                )
            ),
            array('ind_nombre_maestro' => 'Ubicación Pieza', 'ind_descripcion' => 'Ubicación de una Pieza',  'fk_a015_num_seguridad_aplicacion' => '12', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'UBPIEZA',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Caucho Delantero Izquierdo'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Caucho Delantero Derecho'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Caucho Trasero Izquierdo'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'Caucho Trasero Derecho'),
                )
            ),
            array('ind_nombre_maestro' => 'Tipo de Mantenimiento', 'ind_descripcion' => 'Tipo de Mantenimiento',  'fk_a015_num_seguridad_aplicacion' => '12', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOMANT',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Revisión '),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Cambio de Aceite'),
                )
            ),
            array('ind_nombre_maestro' => 'Tipo de Salida', 'ind_descripcion' => 'Tipo de Salida',  'fk_a015_num_seguridad_aplicacion' => '12', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOSAL',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Regional'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Nacional'),
                    array('cod_detalle' => '3 ', 'ind_nombre_detalle' => 'Internacional'),
                )
            ),
            array('ind_nombre_maestro' => 'Estado de la Solicitud', 'ind_descripcion' => 'Estado de la Solicitud',  'fk_a015_num_seguridad_aplicacion' => '12', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'ESTADOSOL',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Preparado'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Verificado'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Conformado'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'Aprobado'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'Asignado'),
                    array('cod_detalle' => '6', 'ind_nombre_detalle' => 'Salida'),
                    array('cod_detalle' => '7 ', 'ind_nombre_detalle' => 'Completado'),
                    array('cod_detalle' => '8', 'ind_nombre_detalle' => 'Anulado'),
                )
            )
        );
        return $a005_miscelaneo_maestro;
    }

    public function metMiscelaneosGC()
    {
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--->>Modulo GC<br>';
        $a005_miscelaneo_maestro = array( array('ind_nombre_maestro' => 'Estatus Contratos GC', 'ind_descripcion' => 'Estatus de los Contratos', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '16', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'PROESTATUS',
            'Det' => array(
                array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Preparado', 'num_estatus' => '1'),
                array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Enviado a Revisión', 'num_estatus' => '1'),
                array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Revisado', 'num_estatus' => '1'),
                array('cod_detalle' => '4', 'ind_nombre_detalle' => 'Conformado', 'num_estatus' => '1'),
                array('cod_detalle' => '5', 'ind_nombre_detalle' => 'Aprobado', 'num_estatus' => '1'),
                array('cod_detalle' => '6', 'ind_nombre_detalle' => 'Anulado', 'num_estatus' => '1'),
            )
        ),
            array('ind_nombre_maestro' => 'Movimiento GC', 'ind_descripcion' => 'Movimientos en Gestión de Contratos', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '16', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'MOVIGC',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Preparado', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Modificado', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Enviado a Revisión', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'Revisado', 'num_estatus' => '1'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'Conformado', 'num_estatus' => '1'),
                    array('cod_detalle' => '6', 'ind_nombre_detalle' => 'Aprobado', 'num_estatus' => '1'),
                    array('cod_detalle' => '7', 'ind_nombre_detalle' => 'Anulado', 'num_estatus' => '1'),
                )
            )
        );
        return $a005_miscelaneo_maestro;
    }

    public function metMiscelaneosCB()
    {
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--->>Modulo CB<br>';
        $a005_miscelaneo_maestro = array(
            array('ind_nombre_maestro' => 'CUENTPUB20', 'ind_descripcion' => 'TIPOS DE CUENTAS PUBLICACION 20', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '10', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'CUENTPUB20',
                'Det' => array(
                    array('cod_detalle' => 'CD', 'ind_nombre_detalle' => 'CUENTAS DEL PATRIMONIO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'CH', 'ind_nombre_detalle' => 'CUENTAS DE LA HACIENDA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'CP', 'ind_nombre_detalle' => 'CUENTAS DEL PRESUPUESTO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'CR', 'ind_nombre_detalle' => 'CUENTAS DEL RESULTADO DEL PRESUPUESTO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'CT', 'ind_nombre_detalle' => 'CUENTAS DEL TESORO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'CUENTONCO', 'ind_descripcion' => 'TIPOS DE CUENTAS CONTABILIDAD', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '10', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'CUENTONCO',
                'Det' => array(
                    array('cod_detalle' => 'BA', 'ind_nombre_detalle' => 'BALANCE ACTIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'BP', 'ind_nombre_detalle' => 'BALANCE PASIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'CO', 'ind_nombre_detalle' => 'CUENTA DE ORDEN', 'num_estatus' => '1'),
                    array('cod_detalle' => 'CP', 'ind_nombre_detalle' => 'CAPITAL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'EG', 'ind_nombre_detalle' => 'EGRESO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'IN', 'ind_nombre_detalle' => 'INGRESO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'OT', 'ind_nombre_detalle' => 'OTROS', 'num_estatus' => '1'),
                )
            ),
        );
        return $a005_miscelaneo_maestro;
    }

    public function metMiscelaneosPR()
    {
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--->>Modulo PR<br>';
        $a005_miscelaneo_maestro = array(
            array('ind_nombre_maestro' => 'Tipo de Ajuste', 'ind_descripcion' => 'Tipo de Ajuste', 'num_estatus' =>'1', 'fk_a015_num_seguridad_aplicacion' =>'6', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TDA',
                'Det' => array(
                    array('cod_detalle' =>'Inc', 'ind_nombre_detalle' => 'Incremento', 'ind_estatus' =>'1'),
                    array('cod_detalle' =>'Dis', 'ind_nombre_detalle' => 'Disminución', 'ind_estatus' => '1'),
                )

            ),

            array('ind_nombre_maestro' => 'Proyecto Presupuestario', 'ind_descripcion' => 'Proyecto Presupuestario', 'num_estatus' =>'1', 'fk_a015_num_seguridad_aplicacion' =>'6',
                'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'PPR',
                'Det' => array(
                    array('cod_detalle' =>'002', 'ind_nombre_detalle' => 'FORTALECIMIENTO DEL CONTROL FISCAL', 'ind_estatus' => '1'),

                )
            ),

            array('ind_nombre_maestro' => 'Accion Especificao', 'ind_descripcion' => 'Accion Especifica', 'num_estatus' =>'1', 'fk_a015_num_seguridad_aplicacion' =>'6',
                'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AES',
                'Det' => array(
                    array('cod_detalle' =>'001', 'ind_nombre_detalle' => 'Fortalecer la participacion ciudadana en el control fiscal sobre la gestion publica tramitar denuncia', 'ind_estatus' => '1'),
                    array('cod_detalle' =>'002', 'ind_nombre_detalle' => 'Apoyo a la gestion del proyecto', 'ind_estatus' => '1'),
                    array('cod_detalle' =>'004', 'ind_nombre_detalle' => 'Prevision y proteccion social', 'ind_estatus' => '1'),

                )
            ),

            array('ind_nombre_maestro' => 'Cuenta Presupuestaria', 'ind_descripcion' => 'Tipo de Cuenta de Presupuesto', 'num_estatus' =>1, 'fk_a015_num_seguridad_aplicacion' =>'6', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TDC',
                'Det' => array(
                    array('cod_detalle' =>'3', 'ind_nombre_detalle' => 'RECURSOS', 'ind_estatus' => '1'),
                    array('cod_detalle' =>'4', 'ind_nombre_detalle' => 'EGRESOS', 'ind_estatus' => '1'),
                )

            ),


            array('ind_nombre_maestro' => 'Tipo de Presupuesto','ind_descripcion' => 'Tipo de Presupuesto','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '6','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'TP',

                'Det' => array(
                    array('cod_detalle' => 'O','ind_nombre_detalle' => 'PROYECTO','num_estatus' => '1'),
                    array('cod_detalle' => 'G','ind_nombre_detalle' => 'PROGRAMA','num_estatus' => '1'),
                )

            ),

            array('ind_nombre_maestro' => 'Tipo de Categoria','ind_descripcion' => 'Tipo de Categoria','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '6','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'TC',

                'Det' => array(
                    array('cod_detalle' => 'O','ind_nombre_detalle' => 'PROYECTO','num_estatus' => '1'),
                    array('cod_detalle' => 'G','ind_nombre_detalle' => 'PROGRAMA','num_estatus' => '1'),
                    array('cod_detalle' => 'C','ind_nombre_detalle' => 'ACCION CENTRALIZADA','num_estatus' => '1'),
                )

            ),

        );
        return $a005_miscelaneo_maestro;
    }

    public function metMiscelaneosCD()
    {
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--->>Modulo CD<br>';
        $a005_miscelaneo_maestro = array(
            array('ind_nombre_maestro' => 'Persona', 'ind_descripcion' => 'Persona', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '7', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'COD_TIPO',
                'Det' => array(
                    array('cod_detalle' => 'NAT', 'ind_nombre_detalle' => 'NATURAL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'JUR', 'ind_nombre_detalle' => 'JURIDICA', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'Tipo Persona', 'ind_descripcion' => 'Tipo Persona', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '7', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIP_PERSON',
                'Det' => array(
                    array('cod_detalle' => 'EMP', 'ind_nombre_detalle' => 'EMPLEADOS', 'num_estatus' => 1),
                    array('cod_detalle' => 'PART', 'ind_nombre_detalle' => 'PARTICULAR', 'num_estatus' => 1),
                    array('cod_detalle' => 'REP', 'ind_nombre_detalle' => 'REPRESENTANTE LEGAL', 'num_estatus' => 1),
                    array('cod_detalle' => 'OTRO', 'ind_nombre_detalle' => 'OTROS', 'num_estatus' => 1),
                )
            ),

        );
        return $a005_miscelaneo_maestro;
    }

    public function metMiscelaneosAF()
    {
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--->>Modulo AF<br>';
        $a005_miscelaneo_maestro = array(
            array('ind_nombre_maestro' => 'TIPO DE MOVIMIENTO ACTIVO FIJO', 'ind_descripcion' => 'TIPO DE MOVIMIENTO ACTIVO FIJO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AFTIPOMOV',
                'Det' => array(
                    array('cod_detalle' => 'I', 'ind_nombre_detalle' => 'INCORPORACIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => 'D', 'ind_nombre_detalle' => 'DESINCORPORACIÓN', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE DEPRECIACIÓN ACTIVO FIJO', 'ind_descripcion' => 'TIPO DE DEPRECIACIÓN ACTIVO FIJO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AFTIPODEPR',
                'Det' => array(
                    array('cod_detalle' => 'LI', 'ind_nombre_detalle' => 'LINEAL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PU', 'ind_nombre_detalle' => 'POR UNIDADES', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'GRUPO CATEGORIA ACTIVO FIJO', 'ind_descripcion' => 'GRUPO CATEGORIA ACTIVO FIJO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AFGRUPCAT',
                'Det' => array(
                    array('cod_detalle' => 'AM', 'ind_nombre_detalle' => 'ACTIVO MENOR', 'num_estatus' => '1'),
                    array('cod_detalle' => 'AN', 'ind_nombre_detalle' => 'ACTIVO NORMAL', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'ORIGEN DEL ACTIVO', 'ind_descripcion' => 'ORIGEN DEL ACTIVO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AFORIGENAC',
                'Det' => array(
                    array('cod_detalle' => 'MA', 'ind_nombre_detalle' => 'MANUAL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'AT', 'ind_nombre_detalle' => 'AUTOMÁTICO', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE ACTIVO', 'ind_descripcion' => 'TIPO DE ACTIVO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AFTIPOACT',
                'Det' => array(
                    array('cod_detalle' => 'I', 'ind_nombre_detalle' => 'INDIVIDUAL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'P', 'ind_nombre_detalle' => 'PRINCIPAL', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE MEJORA DEL ACTIVO FIJO', 'ind_descripcion' => 'TIPO DE MEJORA DEL ACTIVO FIJO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AFTIPOMEJ',
                'Det' => array(
                    array('cod_detalle' => 'NA', 'ind_nombre_detalle' => 'NO APLICABLE', 'num_estatus' => '1'),
                    array('cod_detalle' => 'AD', 'ind_nombre_detalle' => 'ADICIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => 'RV', 'ind_nombre_detalle' => 'REVALUACIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => 'VO', 'ind_nombre_detalle' => 'VOLUNTARIA', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'ESTADO DE ACONSERVACIÓN DEL ACTIVO FIJO', 'ind_descripcion' => 'ESTADO DE ACONSERVACIÓN DEL ACTIVO FIJO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AFESTCONS',
                'Det' => array(
                    array('cod_detalle' => 'B', 'ind_nombre_detalle' => 'BUENO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'M', 'ind_nombre_detalle' => 'MALO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'O', 'ind_nombre_detalle' => 'OBSOLETO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'R', 'ind_nombre_detalle' => 'REGULAR', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'MARCAS', 'ind_descripcion' => 'MARCAS', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'MARCAS',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'SAMSUNG', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'LIFE´S GOOD (LG)', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'GENERAL ELECTRIC', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'PANASONIC', 'num_estatus' => '1'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'PHILLIPS', 'num_estatus' => '1'),
                    array('cod_detalle' => '6', 'ind_nombre_detalle' => 'ADMIRAL', 'num_estatus' => '1'),
                    array('cod_detalle' => '7', 'ind_nombre_detalle' => 'BLACK-DECKER', 'num_estatus' => '1'),
                    array('cod_detalle' => '8', 'ind_nombre_detalle' => 'BOSCH', 'num_estatus' => '1'),
                    array('cod_detalle' => '9', 'ind_nombre_detalle' => 'COMPAQ', 'num_estatus' => '1'),
                    array('cod_detalle' => '10', 'ind_nombre_detalle' => 'CANON', 'num_estatus' => '1'),
                    array('cod_detalle' => '11', 'ind_nombre_detalle' => 'DAEWOO', 'num_estatus' => '1'),
                    array('cod_detalle' => '12', 'ind_nombre_detalle' => 'ELECTROLUX', 'num_estatus' => '1'),
                    array('cod_detalle' => '13', 'ind_nombre_detalle' => 'EPSON', 'num_estatus' => '1'),
                    array('cod_detalle' => '14', 'ind_nombre_detalle' => 'GREE', 'num_estatus' => '1'),
                    array('cod_detalle' => '15', 'ind_nombre_detalle' => 'HEWLETT-PACKARD (HP)', 'num_estatus' => '1'),
                    array('cod_detalle' => '16', 'ind_nombre_detalle' => 'LENOVO', 'num_estatus' => '1'),
                    array('cod_detalle' => '17', 'ind_nombre_detalle' => 'LUFERCA', 'num_estatus' => '1'),
                    array('cod_detalle' => '18', 'ind_nombre_detalle' => 'MABE', 'num_estatus' => '1'),
                    array('cod_detalle' => '19', 'ind_nombre_detalle' => 'OSTER', 'num_estatus' => '1'),
                    array('cod_detalle' => '20', 'ind_nombre_detalle' => 'REGINA', 'num_estatus' => '1'),
                    array('cod_detalle' => '21', 'ind_nombre_detalle' => 'SONY', 'num_estatus' => '1'),
                    array('cod_detalle' => '22', 'ind_nombre_detalle' => 'TEKA', 'num_estatus' => '1'),
                    array('cod_detalle' => '23', 'ind_nombre_detalle' => 'TOSHIBA', 'num_estatus' => '1'),
                    array('cod_detalle' => '24', 'ind_nombre_detalle' => 'XEROX', 'num_estatus' => '1'),
                    array('cod_detalle' => '25', 'ind_nombre_detalle' => 'NEC', 'num_estatus' => '1'),
                    array('cod_detalle' => '26', 'ind_nombre_detalle' => 'PLANTRONICS', 'num_estatus' => '1'),
                    array('cod_detalle' => '27', 'ind_nombre_detalle' => 'HP', 'num_estatus' => '1'),
                    array('cod_detalle' => '28', 'ind_nombre_detalle' => 'FRIGILUX', 'num_estatus' => '1'),
                    array('cod_detalle' => '29', 'ind_nombre_detalle' => 'CASIO', 'num_estatus' => '1'),
                    array('cod_detalle' => '30', 'ind_nombre_detalle' => 'CANYON', 'num_estatus' => '1'),
                    array('cod_detalle' => '31', 'ind_nombre_detalle' => 'TRIPP-LITE', 'num_estatus' => '1'),
                    array('cod_detalle' => '32', 'ind_nombre_detalle' => 'GENIUS', 'num_estatus' => '1'),
                    array('cod_detalle' => '33', 'ind_nombre_detalle' => 'KEYTON', 'num_estatus' => '1'),
                    array('cod_detalle' => '34', 'ind_nombre_detalle' => 'SHARP', 'num_estatus' => '1'),
                    array('cod_detalle' => '35', 'ind_nombre_detalle' => 'CYBERLUX', 'num_estatus' => '1'),
                    array('cod_detalle' => '36', 'ind_nombre_detalle' => 'FORZA', 'num_estatus' => '1'),
                    array('cod_detalle' => '37', 'ind_nombre_detalle' => 'TOPFLO', 'num_estatus' => '1'),
                    array('cod_detalle' => '38', 'ind_nombre_detalle' => 'SMART COOR', 'num_estatus' => '1'),
                    array('cod_detalle' => '39', 'ind_nombre_detalle' => 'TAURUS', 'num_estatus' => '1'),
                    array('cod_detalle' => '40', 'ind_nombre_detalle' => 'LINK NET', 'num_estatus' => '1'),
                    array('cod_detalle' => '41', 'ind_nombre_detalle' => 'CESIO', 'num_estatus' => '1'),
                    array('cod_detalle' => '42', 'ind_nombre_detalle' => 'MICROSOFT', 'num_estatus' => '1'),
                    array('cod_detalle' => '43', 'ind_nombre_detalle' => 'DELL OFFICE', 'num_estatus' => '1'),
                    array('cod_detalle' => '44', 'ind_nombre_detalle' => 'BESTEC', 'num_estatus' => '1'),
                    array('cod_detalle' => '45', 'ind_nombre_detalle' => 'INTEL', 'num_estatus' => '1'),
                    array('cod_detalle' => '46', 'ind_nombre_detalle' => 'SEAGATE', 'num_estatus' => '1'),
                    array('cod_detalle' => '47', 'ind_nombre_detalle' => 'ACER', 'num_estatus' => '1'),
                    array('cod_detalle' => '48', 'ind_nombre_detalle' => 'L & C', 'num_estatus' => '1'),
                    array('cod_detalle' => '49', 'ind_nombre_detalle' => 'KINGSTON', 'num_estatus' => '1'),
                    array('cod_detalle' => '50', 'ind_nombre_detalle' => 'DATA STORAGE', 'num_estatus' => '1'),
                    array('cod_detalle' => '51', 'ind_nombre_detalle' => 'KODE', 'num_estatus' => '1'),
                    array('cod_detalle' => '52', 'ind_nombre_detalle' => 'SUPER TALENT', 'num_estatus' => '1'),
                    array('cod_detalle' => '53', 'ind_nombre_detalle' => 'CITIZEN', 'num_estatus' => '1'),
                    array('cod_detalle' => '54', 'ind_nombre_detalle' => 'HYNIX', 'num_estatus' => '1'),
                    array('cod_detalle' => '55', 'ind_nombre_detalle' => 'WESTERN DIGITAL', 'num_estatus' => '1'),
                    array('cod_detalle' => '56', 'ind_nombre_detalle' => 'EMERALD', 'num_estatus' => '1'),
                    array('cod_detalle' => '57', 'ind_nombre_detalle' => 'AC BEL', 'num_estatus' => '1'),
                    array('cod_detalle' => '58', 'ind_nombre_detalle' => 'ANATEL', 'num_estatus' => '1'),
                    array('cod_detalle' => '59', 'ind_nombre_detalle' => 'LITEON', 'num_estatus' => '1'),
                    array('cod_detalle' => '60', 'ind_nombre_detalle' => 'ITACHI', 'num_estatus' => '1'),
                    array('cod_detalle' => '61', 'ind_nombre_detalle' => 'ASROCK', 'num_estatus' => '1'),
                    array('cod_detalle' => '62', 'ind_nombre_detalle' => 'AIPRO', 'num_estatus' => '1'),
                    array('cod_detalle' => '63', 'ind_nombre_detalle' => 'BENQ', 'num_estatus' => '1'),
                    array('cod_detalle' => '64', 'ind_nombre_detalle' => 'TEMP', 'num_estatus' => '1'),
                    array('cod_detalle' => '65', 'ind_nombre_detalle' => 'FOXCONN', 'num_estatus' => '1'),
                    array('cod_detalle' => '66', 'ind_nombre_detalle' => 'WRITE MASTER', 'num_estatus' => '1'),
                    array('cod_detalle' => '67', 'ind_nombre_detalle' => 'LITE-ON', 'num_estatus' => '1'),
                    array('cod_detalle' => '68', 'ind_nombre_detalle' => 'JENIP', 'num_estatus' => '1'),
                    array('cod_detalle' => '69', 'ind_nombre_detalle' => 'VALUE SERIES', 'num_estatus' => '1'),
                    array('cod_detalle' => '70', 'ind_nombre_detalle' => 'BIO START', 'num_estatus' => '1'),
                    array('cod_detalle' => '71', 'ind_nombre_detalle' => 'PATRIOT', 'num_estatus' => '1'),
                    array('cod_detalle' => '72', 'ind_nombre_detalle' => 'AMD', 'num_estatus' => '1'),
                    array('cod_detalle' => '73', 'ind_nombre_detalle' => 'HIPRO', 'num_estatus' => '1'),
                    array('cod_detalle' => '74', 'ind_nombre_detalle' => 'ADATA', 'num_estatus' => '1'),
                    array('cod_detalle' => '75', 'ind_nombre_detalle' => 'MAXTONE', 'num_estatus' => '1'),
                    array('cod_detalle' => '76', 'ind_nombre_detalle' => 'AEVISION', 'num_estatus' => '1'),
                    array('cod_detalle' => '77', 'ind_nombre_detalle' => 'TALAMO', 'num_estatus' => '1'),
                    array('cod_detalle' => '78', 'ind_nombre_detalle' => 'WASH', 'num_estatus' => '1'),
                    array('cod_detalle' => '79', 'ind_nombre_detalle' => 'HP COMPAQ', 'num_estatus' => '1'),
                    array('cod_detalle' => '80', 'ind_nombre_detalle' => 'C D P', 'num_estatus' => '1'),
                    array('cod_detalle' => '81', 'ind_nombre_detalle' => 'AVTEK', 'num_estatus' => '1'),
                    array('cod_detalle' => '82', 'ind_nombre_detalle' => 'OFITECH', 'num_estatus' => '1'),
                    array('cod_detalle' => '83', 'ind_nombre_detalle' => 'X-ACTO', 'num_estatus' => '1'),
                    array('cod_detalle' => '84', 'ind_nombre_detalle' => 'KW-TRIO', 'num_estatus' => '1'),
                    array('cod_detalle' => '85', 'ind_nombre_detalle' => 'TECH', 'num_estatus' => '1'),
                    array('cod_detalle' => '86', 'ind_nombre_detalle' => 'GOLDEN DREAMS', 'num_estatus' => '1'),
                    array('cod_detalle' => '87', 'ind_nombre_detalle' => 'PREMIER', 'num_estatus' => '1'),
                    array('cod_detalle' => '88', 'ind_nombre_detalle' => 'APC', 'num_estatus' => '1'),
                    array('cod_detalle' => '89', 'ind_nombre_detalle' => 'ENERGIZER', 'num_estatus' => '1'),
                    array('cod_detalle' => '90', 'ind_nombre_detalle' => 'VOSS', 'num_estatus' => '1'),
                    array('cod_detalle' => '91', 'ind_nombre_detalle' => 'SUECO', 'num_estatus' => '1'),
                    array('cod_detalle' => '92', 'ind_nombre_detalle' => 'L G', 'num_estatus' => '1'),
                    array('cod_detalle' => '93', 'ind_nombre_detalle' => 'SIEMENS', 'num_estatus' => '1'),
                    array('cod_detalle' => '94', 'ind_nombre_detalle' => 'RUCOPI', 'num_estatus' => '1'),
                    array('cod_detalle' => '95', 'ind_nombre_detalle' => 'EAGLE', 'num_estatus' => '1'),
                    array('cod_detalle' => '96', 'ind_nombre_detalle' => 'CHICAGO DIGITAL POWER (CDP)', 'num_estatus' => '1'),
                    array('cod_detalle' => '97', 'ind_nombre_detalle' => 'TP LINK', 'num_estatus' => '1'),
                    array('cod_detalle' => '98', 'ind_nombre_detalle' => 'CLON', 'num_estatus' => '1'),
                    array('cod_detalle' => '99', 'ind_nombre_detalle' => 'GEHA', 'num_estatus' => '1'),
                    array('cod_detalle' => '100', 'ind_nombre_detalle' => 'SMART COOK', 'num_estatus' => '1'),
                    array('cod_detalle' => '101', 'ind_nombre_detalle' => 'SONCVICV', 'num_estatus' => '1'),
                    array('cod_detalle' => '102', 'ind_nombre_detalle' => 'TRANSCA', 'num_estatus' => '1'),
                    array('cod_detalle' => '103', 'ind_nombre_detalle' => 'TRAVELER', 'num_estatus' => '1'),
                    array('cod_detalle' => '104', 'ind_nombre_detalle' => 'AUSE', 'num_estatus' => '1'),
                    array('cod_detalle' => '105', 'ind_nombre_detalle' => '3M', 'num_estatus' => '1'),
                    array('cod_detalle' => '106', 'ind_nombre_detalle' => 'AXESS.TEL', 'num_estatus' => '1'),
                    array('cod_detalle' => '107', 'ind_nombre_detalle' => 'D-Link', 'num_estatus' => '1'),
                    array('cod_detalle' => '108', 'ind_nombre_detalle' => 'AIWA', 'num_estatus' => '1'),
                    array('cod_detalle' => '109', 'ind_nombre_detalle' => 'CRAFFT', 'num_estatus' => '1'),
                    array('cod_detalle' => '110', 'ind_nombre_detalle' => 'GEELY', 'num_estatus' => '1'),
                    array('cod_detalle' => '111', 'ind_nombre_detalle' => 'DECTECTO', 'num_estatus' => '1'),
                    array('cod_detalle' => '112', 'ind_nombre_detalle' => 'ELECTROSONIC', 'num_estatus' => '1'),
                    array('cod_detalle' => '113', 'ind_nombre_detalle' => 'RUN', 'num_estatus' => '1'),
                    array('cod_detalle' => '114', 'ind_nombre_detalle' => 'YAMAHA', 'num_estatus' => '1'),
                    array('cod_detalle' => '115', 'ind_nombre_detalle' => 'RIDGID', 'num_estatus' => '1'),
                    array('cod_detalle' => '116', 'ind_nombre_detalle' => 'BLACKBERRY', 'num_estatus' => '1'),
                    array('cod_detalle' => '117', 'ind_nombre_detalle' => 'MOTOROLA', 'num_estatus' => '1'),
                    array('cod_detalle' => '118', 'ind_nombre_detalle' => 'HUAWEL', 'num_estatus' => '1'),
                    array('cod_detalle' => '119', 'ind_nombre_detalle' => 'TOPCON', 'num_estatus' => '1'),
                    array('cod_detalle' => '120', 'ind_nombre_detalle' => 'BMI', 'num_estatus' => '1'),
                    array('cod_detalle' => '121', 'ind_nombre_detalle' => 'ALESSIS', 'num_estatus' => '1'),
                    array('cod_detalle' => '122', 'ind_nombre_detalle' => 'LINCE', 'num_estatus' => '1'),
                    array('cod_detalle' => '123', 'ind_nombre_detalle' => 'BOSTON', 'num_estatus' => '1'),
                    array('cod_detalle' => '124', 'ind_nombre_detalle' => 'ACWELDER', 'num_estatus' => '1'),
                    array('cod_detalle' => '125', 'ind_nombre_detalle' => 'BLACK DECKER', 'num_estatus' => '1'),
                    array('cod_detalle' => '126', 'ind_nombre_detalle' => 'TOP-FLO', 'num_estatus' => '1'),
                    array('cod_detalle' => '127', 'ind_nombre_detalle' => 'TECIVEN', 'num_estatus' => '1'),
                    array('cod_detalle' => '128', 'ind_nombre_detalle' => 'TYCOS', 'num_estatus' => '1'),
                    array('cod_detalle' => '129', 'ind_nombre_detalle' => 'AGUAJET', 'num_estatus' => '1'),
                    array('cod_detalle' => '130', 'ind_nombre_detalle' => 'EON', 'num_estatus' => '1'),
                    array('cod_detalle' => '131', 'ind_nombre_detalle' => 'TRUPPER', 'num_estatus' => '1'),
                    array('cod_detalle' => '132', 'ind_nombre_detalle' => 'DEWALT', 'num_estatus' => '1'),
                    array('cod_detalle' => '133', 'ind_nombre_detalle' => '3COM VASELINE', 'num_estatus' => '1'),
                    array('cod_detalle' => '134', 'ind_nombre_detalle' => 'TAKIMA', 'num_estatus' => '1'),
                    array('cod_detalle' => '135', 'ind_nombre_detalle' => 'KLIP', 'num_estatus' => '1'),
                    array('cod_detalle' => '136', 'ind_nombre_detalle' => 'NEXXT', 'num_estatus' => '1'),
                    array('cod_detalle' => '137', 'ind_nombre_detalle' => 'TARGUS', 'num_estatus' => '1'),
                    array('cod_detalle' => '138', 'ind_nombre_detalle' => 'PROFINA', 'num_estatus' => '1'),
                    array('cod_detalle' => '139', 'ind_nombre_detalle' => 'CHEVROLET', 'num_estatus' => '1'),
                    array('cod_detalle' => '140', 'ind_nombre_detalle' => 'ALADINO', 'num_estatus' => '1'),
                    array('cod_detalle' => '141', 'ind_nombre_detalle' => 'SPEEP', 'num_estatus' => '1'),
                    array('cod_detalle' => '142', 'ind_nombre_detalle' => 'DIPLOMAT', 'num_estatus' => '1'),
                    array('cod_detalle' => '143', 'ind_nombre_detalle' => 'LINKSYS', 'num_estatus' => '1'),
                    array('cod_detalle' => '144', 'ind_nombre_detalle' => 'AMBICO', 'num_estatus' => '1'),
                    array('cod_detalle' => '145', 'ind_nombre_detalle' => 'MANAPLAS', 'num_estatus' => '1'),
                    array('cod_detalle' => '146', 'ind_nombre_detalle' => 'FELLOWES', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'TABLA DE COLORES', 'ind_descripcion' => 'TABLA DE COLORES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'COLORES',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'AMARILLO', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'AZUL', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'ROJO', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'VERDE', 'num_estatus' => '1'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'BLANCO', 'num_estatus' => '1'),
                    array('cod_detalle' => '6', 'ind_nombre_detalle' => 'NEGRO', 'num_estatus' => '1'),
                    array('cod_detalle' => '7', 'ind_nombre_detalle' => 'MARRON', 'num_estatus' => '1'),
                    array('cod_detalle' => '8', 'ind_nombre_detalle' => 'GRIS', 'num_estatus' => '1'),
                    array('cod_detalle' => '9', 'ind_nombre_detalle' => 'NARANJA', 'num_estatus' => '1'),
                    array('cod_detalle' => '10', 'ind_nombre_detalle' => 'VIOLETA', 'num_estatus' => '1'),
                    array('cod_detalle' => '11', 'ind_nombre_detalle' => 'MORADO', 'num_estatus' => '1'),
                    array('cod_detalle' => '12', 'ind_nombre_detalle' => 'BEIGE', 'num_estatus' => '1'),
                    array('cod_detalle' => '13', 'ind_nombre_detalle' => 'PLATEADO', 'num_estatus' => '1'),
                    array('cod_detalle' => '14', 'ind_nombre_detalle' => 'METALIZADO', 'num_estatus' => '1'),
                    array('cod_detalle' => '15', 'ind_nombre_detalle' => 'GRIS/NEGRO', 'num_estatus' => '1'),
                    array('cod_detalle' => '16', 'ind_nombre_detalle' => 'BEIGE//GRIS', 'num_estatus' => '1'),
                    array('cod_detalle' => '17', 'ind_nombre_detalle' => 'DORADO/NEGRO', 'num_estatus' => '1'),
                    array('cod_detalle' => '18', 'ind_nombre_detalle' => 'ROJO/BLANCO', 'num_estatus' => '1'),
                    array('cod_detalle' => '19', 'ind_nombre_detalle' => 'CHAMPAN', 'num_estatus' => '1'),
                    array('cod_detalle' => '20', 'ind_nombre_detalle' => 'VINO TINTO', 'num_estatus' => '1'),
                    array('cod_detalle' => '21', 'ind_nombre_detalle' => 'MULTICOLOR', 'num_estatus' => '1'),
                    array('cod_detalle' => '22', 'ind_nombre_detalle' => 'DORADO', 'num_estatus' => '1'),
                    array('cod_detalle' => '23', 'ind_nombre_detalle' => 'BLANCO/AZUL', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE ACTAS MOVIMIENTOS DE ACTIVO FIJO', 'ind_descripcion' => 'TIPO DE ACTAS MOVIMIENTOS DE ACTIVO FIJO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AFTIPOACTA',
                'Det' => array(
                    array('cod_detalle' => 'AE', 'ind_nombre_detalle' => 'ACTA DE ENTRAEGA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'AA', 'ind_nombre_detalle' => 'ACTA DE ADJUDICACIÓN', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'MOTIVOS DE TRASPASO (INTERNO)', 'ind_descripcion' => 'MOTIVOS DE TRASPASO (INTERNO)', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AFMOTTRASI',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'INGRESO INICIAL', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'TRASLADO ENTRE DEPENDENCIAS', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'PRESTAMO POR MOVIMIENTO DE BIENES', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'INSERVIBILIDAD', 'num_estatus' => '1'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'DETERIORO', 'num_estatus' => '1'),
                    array('cod_detalle' => '6', 'ind_nombre_detalle' => 'OTROS', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'MOTIVOS DE TRASPASO (EXTERNO)', 'ind_descripcion' => 'MOTIVOS DE TRASPASO (EXTERNO)', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '8', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AFMOTTRASE',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'DESINCORPORACION', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'TRASLADO A OTRAS ENTIDADES', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'TRASLADO POR MANTENIMIENTO EXTERNO', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'OTROS', 'num_estatus' => '1')
                )
            )
        );
        return $a005_miscelaneo_maestro;
    }

    public function metMiscelaneosPF()
    {
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--->>Modulo PF<br>';
        $a005_miscelaneo_maestro = array(
            array('ind_nombre_maestro' => 'ORIGEN DE LA ACTUACION', 'ind_descripcion' => 'Cual es el origen de la actuacion fiscal', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 1, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'PFODA',
                'Det' => array(
                    array('cod_detalle' => 'CGR', 'ind_nombre_detalle' => 'Ordenada por la CGR', 'num_estatus' => '1'),
                    array('cod_detalle' => 'DEN', 'ind_nombre_detalle' => 'Denuncia', 'num_estatus' => '1'),
                    array('cod_detalle' => 'OTR', 'ind_nombre_detalle' => 'Otros', 'num_estatus' => '1'),
                    array('cod_detalle' => 'POA', 'ind_nombre_detalle' => 'Plan Operativo Anual (POA)', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'ESTADOS DE LA ACTUACION', 'ind_descripcion' => 'Los distintos estado en que pueda estar la actuación', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 1, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'PFE',
                'Det' => array(
                    array('cod_detalle' => 'PR', 'ind_nombre_detalle' => 'En Preparación', 'num_estatus' => '1'),
                    array('cod_detalle' => 'RV', 'ind_nombre_detalle' => 'En Revisión', 'num_estatus' => '1'),
                    array('cod_detalle' => 'AP', 'ind_nombre_detalle' => 'Aprobado', 'num_estatus' => '1'),
                    array('cod_detalle' => 'CO', 'ind_nombre_detalle' => 'Completado', 'num_estatus' => '1'),
                    array('cod_detalle' => 'CE', 'ind_nombre_detalle' => 'Cerrado', 'num_estatus' => '1'),
                    array('cod_detalle' => 'AN', 'ind_nombre_detalle' => 'Anulado', 'num_estatus' => '1'),
                    array('cod_detalle' => 'TE', 'ind_nombre_detalle' => 'Terminado', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'Tipo Actuación Fiscal', 'ind_descripcion' => 'Tipos de actuaciones fiscales', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 9, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'PFTAF',
                'Det' => array(
                    array('cod_detalle' => 'PFAO', 'ind_nombre_detalle' => 'AUDITORÍA OPERATIVA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PFAS', 'ind_nombre_detalle' => 'AUDITORÍA DE SEGUIMIENTO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PFAB', 'ind_nombre_detalle' => 'AUDITORÍA DE OBRAS', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PFAC', 'ind_nombre_detalle' => 'EXAMEN DE CUENTA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PFAP', 'ind_nombre_detalle' => 'AUDITORÍA OPERATIVA Y DE OBRAS', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PFAD', 'ind_nombre_detalle' => 'AUDITORÍA DE BIENES', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PFAF', 'ind_nombre_detalle' => 'ACCIÓN FISCAL', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'Situación personal externo', 'ind_descripcion' => 'Situación con respecto al cargo y ente del personal externo', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 9, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'PFSPE',
                'Det' => array(
                    array('cod_detalle' => 'PFST', 'ind_nombre_detalle' => 'Titular', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PFSE', 'ind_nombre_detalle' => 'Encargado', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PFSD', 'ind_nombre_detalle' => 'Diputado', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PFSP', 'ind_nombre_detalle' => 'Particular', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PFEP', 'ind_nombre_detalle' => 'Empleado', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'Caracter Social de entes', 'ind_descripcion' => 'Tipo de Caracter Social de un Ente', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 9, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'PFTCSE',
                'Det' => array(
                    array('cod_detalle' => 'NA', 'ind_nombre_detalle' => 'No Aplica', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PTC', 'ind_nombre_detalle' => 'Politico', 'num_estatus' => '1'),
                    array('cod_detalle' => 'CML', 'ind_nombre_detalle' => 'Comunal', 'num_estatus' => '1'),
                    array('cod_detalle' => 'SCL', 'ind_nombre_detalle' => 'Social', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PDS', 'ind_nombre_detalle' => 'Producción Social', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'Indicativos de procesos de planificación fiscal', 'ind_descripcion' => 'Indicativo que identifica el proceso para incluír en los números de control de las planificaciones', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 9, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'PFINDIPROC',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'AF', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'VP', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'PI', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'VJ', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'DR', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'Tipo entes', 'ind_descripcion' => 'Tipos de entes', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 9, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'PFTIPOENTE',
                'Det' => array(
                    array('cod_detalle' => 'FPPA', 'ind_nombre_detalle' => 'Particular', 'num_estatus' => '1'),
                    array('cod_detalle' => 'FPPU', 'ind_nombre_detalle' => 'Público', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PFPR', 'ind_nombre_detalle' => 'Privado', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PFGE', 'ind_nombre_detalle' => 'Gobiernos extranjeros', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PFEI', 'ind_nombre_detalle' => 'Empresa internacional', 'num_estatus' => '1')
                )
            ),
            array('ind_nombre_maestro' => 'Cargos personal externo', 'ind_descripcion' => 'Cargos para el personal externo', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => 9, 'fk_a018_num_seguridad_usuario' => 1, 'cod_maestro' => 'PFCPEXT',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Sin Cargo', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Presidente(a) de la República', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Ministro(a)', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'Director(a) General', 'num_estatus' => '1'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'Director(a)', 'num_estatus' => '1'),
                    array('cod_detalle' => '6', 'ind_nombre_detalle' => 'Presidente(a)', 'num_estatus' => '1'),
                    array('cod_detalle' => '7', 'ind_nombre_detalle' => 'Jefe(a)', 'num_estatus' => '1'),
                    array('cod_detalle' => '8', 'ind_nombre_detalle' => 'Administrador(a)', 'num_estatus' => '1'),
                    array('cod_detalle' => '9', 'ind_nombre_detalle' => 'Gerente', 'num_estatus' => '1'),
                    array('cod_detalle' => '10', 'ind_nombre_detalle' => 'Contralor General de la República', 'num_estatus' => '1'),
                    array('cod_detalle' => '11', 'ind_nombre_detalle' => 'Alcalde', 'num_estatus' => '1')
                )
            )
        );
        return $a005_miscelaneo_maestro;
    }

    public function metMiscelaneosEV()
    {
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--->>Modulo EV<br>';
        $a005_miscelaneo_maestro = array(
            array('ind_nombre_maestro' => 'TIPO DE EVENTO','ind_descripcion' => 'Tipo de evento','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '15','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'TIPEV',
                'Det' => array(
                    array('cod_detalle' => '1','ind_nombre_detalle' => 'CHARLA','num_estatus' => '1'),
                    array('cod_detalle' => '2','ind_nombre_detalle' => 'TALLER','num_estatus' => '1'),
                    array('cod_detalle' => '3','ind_nombre_detalle' => 'CURSO','num_estatus' => '1'),
                    array('cod_detalle' => '4','ind_nombre_detalle' => 'SIMPOSIO','num_estatus' => '1'),
                    array('cod_detalle' => '5','ind_nombre_detalle' => 'SEMINARIO','num_estatus' => '1'),
                    array('cod_detalle' => '6','ind_nombre_detalle' => 'CONFERENCIA','num_estatus' => '1'),
                    array('cod_detalle' => '7','ind_nombre_detalle' => 'RECONOCIMIENTO','num_estatus' => '1'),
                    array('cod_detalle' => '8','ind_nombre_detalle' => 'CELEBRACIÓN','num_estatus' => '1'),
                    array('cod_detalle' => '9','ind_nombre_detalle' => 'JURAMENTACIÓN','num_estatus' => '1'),
                    array('cod_detalle' => '10','ind_nombre_detalle' => 'ACTIVIDAD ESPECIAL','num_estatus' => '1'),
                    array('cod_detalle' => '11','ind_nombre_detalle' => 'OTROS','num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE PERSONA EVENTO','ind_descripcion' => 'Identifica a un ponente o participante','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '15','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'TIPEREV',
                'Det' => array(
                    array('cod_detalle' => '1','ind_nombre_detalle' => 'PONENTE','num_estatus' => '1'),
                    array('cod_detalle' => '2','ind_nombre_detalle' => 'PARTICIPANTE','num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'Nivel de Instrucción','ind_descripcion' => 'Indica el nivel de instrucción académica de una persona','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '15','fk_a018_num_seguridad_usuario' => '1','cod_maestro' => 'NIVELINS',
                'Det' => array(
                    array('cod_detalle' => '1','ind_nombre_detalle' => 'BACHILLER','num_estatus' => '1'),
                    array('cod_detalle' => '2','ind_nombre_detalle' => 'TÉCNICO SUPERIOR UNIVERSITARIO','num_estatus' => '1'),
                    array('cod_detalle' => '3','ind_nombre_detalle' => 'LICENCIADO','num_estatus' => '1'),
                    array('cod_detalle' => '4','ind_nombre_detalle' => 'INGENIERO','num_estatus' => '1'),
                    array('cod_detalle' => '5','ind_nombre_detalle' => 'ESPECIALIZACIÓN','num_estatus' => '1'),
                    array('cod_detalle' => '6','ind_nombre_detalle' => 'MAGISTER','num_estatus' => '1'),
                    array('cod_detalle' => '7','ind_nombre_detalle' => 'ECONOMISTA','num_estatus' => '1'),
                    array('cod_detalle' => '8','ind_nombre_detalle' => 'MÉDICO','num_estatus' => '1'),
                    array('cod_detalle' => '9','ind_nombre_detalle' => 'ABOGADO','num_estatus' => '1'),
                    array('cod_detalle' => '10','ind_nombre_detalle' => 'DOCTORADO','num_estatus' => '1')
                )
            )
        );
        return $a005_miscelaneo_maestro;
    }

    public function metMiscelaneosDN()
    {
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--->>Modulo EV<br>';
        $a005_miscelaneo_maestro = array(
            array('ind_nombre_maestro' => 'ORIGEN DENUNCIA','ind_descripcion' => 'ORIGEN DE LAS DENUNCIAS','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '19','cod_maestro' => 'DN-01-02','fk_a018_num_seguridad_usuario' => '1',
                'Det' => array(
                    array('cod_detalle' => '001','ind_nombre_detalle' => 'CONSEJO COMUNAL','num_estatus' => '1'),
                    array('cod_detalle' => '002','ind_nombre_detalle' => 'ÓRGANO O ENTE PÚBLICO','num_estatus' => '1'),
                    array('cod_detalle' => '003','ind_nombre_detalle' => 'OTRO','num_estatus' => '1')
                )),
            array('ind_nombre_maestro' => 'TIPO DENUNCIA','ind_descripcion' => 'TIPO DE DENUNCIA','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '19','cod_maestro' => 'DN-01-03','fk_a018_num_seguridad_usuario' => '1',
                'Det' => array(
                    array('cod_detalle' => '001','ind_nombre_detalle' => 'OBRAS','num_estatus' => '1'),
                    array('cod_detalle' => '002','ind_nombre_detalle' => 'BIENES','num_estatus' => '1'),
                    array('cod_detalle' => '003','ind_nombre_detalle' => 'SERVICIOS','num_estatus' => '1'),
                    array('cod_detalle' => '004','ind_nombre_detalle' => 'OTROS','num_estatus' => '0')
                )),
            array('ind_nombre_maestro' => 'CODIFICACION','ind_descripcion' => 'ESTRUCTURA DEL CODIGO APLICADO POR CADA CONTRALORÍA PARA LAS DENUNCIAS, QUEJAS, RECLAMOS O SUGERENCI','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '19','cod_maestro' => 'DN-COD-01','fk_a018_num_seguridad_usuario' => '1',
                'Det' => array(
                    array('cod_detalle' => 'D','ind_nombre_detalle' => 'DACCC-D','num_estatus' => '1'),
                    array('cod_detalle' => 'P','ind_nombre_detalle' => 'DACCC-P','num_estatus' => '1'),
                    array('cod_detalle' => 'Q','ind_nombre_detalle' => 'DACCC-Q','num_estatus' => '1'),
                    array('cod_detalle' => 'R','ind_nombre_detalle' => 'DACCC-R','num_estatus' => '1'),
                    array('cod_detalle' => 'S','ind_nombre_detalle' => 'DACCC-S','num_estatus' => '1')
                )),

            array('ind_nombre_maestro' => 'ESTATUS SOLICITUDES','ind_descripcion' => 'ESTATUS DE LAS SOLICITUDE DE DENUNCIAS','num_estatus' => '0','fk_a015_num_seguridad_aplicacion' => '19','cod_maestro' => 'DN-EST-SOL','fk_a018_num_seguridad_usuario' => '1',
                'Det' => array(
                    array('cod_detalle' => 'PE','ind_nombre_detalle' => 'PENDIENTE','num_estatus' => '1'),
                    array('cod_detalle' => 'RE','ind_nombre_detalle' => 'RECHAZADA','num_estatus' => '1'),
                    array('cod_detalle' => 'TR','ind_nombre_detalle' => 'TRAMITADA','num_estatus' => '1')
                )),
            array('ind_nombre_maestro' => 'TIPOACTUACION','ind_descripcion' => 'TIPOACTUACION','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '19','cod_maestro' => 'TACT','fk_a018_num_seguridad_usuario' => '1',
                'Det' => array(
                    array('cod_detalle' => 'D','ind_nombre_detalle' => 'DENUNCIA','num_estatus' => '1'),
                    array('cod_detalle' => 'R','ind_nombre_detalle' => 'RECLAMO','num_estatus' => '1'),
                    array('cod_detalle' => 'P','ind_nombre_detalle' => 'PETICIÓN','num_estatus' => '1'),
                    array('cod_detalle' => 'S','ind_nombre_detalle' => 'SUGERENCIA','num_estatus' => '1'),
                    array('cod_detalle' => 'Q','ind_nombre_detalle' => 'QUEJA','num_estatus' => '1')
                )),
            array('ind_nombre_maestro' => 'ESTATUS TRAMITE','ind_descripcion' => 'ESTATUS DE LOS TRAMITES (D, P, S, R,Q)','num_estatus' => '1','fk_a015_num_seguridad_aplicacion' => '19','cod_maestro' => 'DN-EST-TRA','fk_a018_num_seguridad_usuario' => '1',
                'Det' => array(
                    array('cod_detalle' => 'CE','ind_nombre_detalle' => 'CERRADA','num_estatus' => '1'),
                    array('cod_detalle' => 'AA','ind_nombre_detalle' => 'AUTO DE ARCHIVO','num_estatus' => '1'),
                    array('cod_detalle' => 'RE','ind_nombre_detalle' => 'REMITIDA','num_estatus' => '1'),
                    array('cod_detalle' => 'TE','ind_nombre_detalle' => 'TERMINADA','num_estatus' => '1'),
                    array('cod_detalle' => 'EJ','ind_nombre_detalle' => 'EN EJECUCIÓN','num_estatus' => '1'),
                    array('cod_detalle' => 'AP','ind_nombre_detalle' => 'APROBADA','num_estatus' => '1'),
                    array('cod_detalle' => 'RE','ind_nombre_detalle' => 'REVISADA','num_estatus' => '1'),
                    array('cod_detalle' => 'GN','ind_nombre_detalle' => 'GENERADA','num_estatus' => '1'),
                    array('cod_detalle' => 'PE','ind_nombre_detalle' => 'PENDIENTE','num_estatus' => '1'),
                    array('cod_detalle' => 'EP','ind_nombre_detalle' => 'EN PREPARACIÓN','num_estatus' => '1')
                ))

        );
        return $a005_miscelaneo_maestro;
    }

    public function metDataMiscelaneos()
    {
        $a005_miscelaneo_maestro = array_merge(
            $this->metMiscelaneosDanielAPyNM(),
            $this->metMiscelaneosYohandryCP(),
            $this->metMiscelaneosFernandoLG(),
            $this->metMiscelaneosSergioCV(),
            $this->metMiscelaneosFernandoMendozaRH(),
            $this->metMiscelaneosPA(),
            $this->metMiscelaneosGC(),
            $this->metMiscelaneosPR(),
            $this->metMiscelaneosCD(),
            $this->metMiscelaneosAF(),
            $this->metMiscelaneosPF(),
            $this->metMiscelaneosEV(),
            $this->metMiscelaneosCB(),
            $this->metMiscelaneosDN()
        );
        return $a005_miscelaneo_maestro;
    }
}
