<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * | 2 |          Maikol Isava                     |acc.desarrollo@cgesucre.gob.ve      |         0426-1814058           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |               #2                       |       24 -08-2016      |                    | Modificación para modulo Intranet
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class indexControlador extends Controlador
{
    private $atMenuModelo;
    private $atUsuarioModelo;

	//######################
	//# VAR INTRANET
	//######################
	private $atIntranetModelo;
	private $atEventoModelo;
	//######################
	//# FIN VAR INTRANET
	//######################

    public function __construct()
	{
		parent::__construct();
        Session::metAcceso();
		$this->atMenuModelo= $this->metCargarModelo('menu');
		$this->atUsuarioModelo= $this->metCargarModelo('usuario');

		//######################
		//# MEDELO INTRANET
		//######################
		$this->atIntranetModelo = $this->metCargarModelo('index');
        $this->atEventoModelo = $this->metCargarModelo('gestionEvento', '', 'modEV');
        $this->atIdDetalleOrg = Session::metObtener('IDORGANISMODETALLE');
		//######################
		//# FIN MODELO INTRANET
		//######################

	}

	public function metInicio()
	{
		//#####################################
		//# INICIO INTRANET
		//#####################################
		$js = array(
			'modIN/printThis',
			'modIN/evtcalendar',
			'materialSiace/core/cache/63d0445130d69b2868a8d28c93309746'

		);

		$this->atVista->metCargarJs($js);

		$datosEventos  = $this->atIntranetModelo->metBuscarEventos();
		$cad_array_eventos = '';

		for($i=0; $i<count($datosEventos); $i++)
		{
			$aux = explode("-",$datosEventos[$i]['fec_registro']);
			$hora_inicio = $datosEventos[$i]['hora_inicio'];
			$hora_final = $datosEventos[$i]['hora_final'];
			$horario_inicio = $datosEventos[$i]['horario_inicio'];
			$horario_final = $datosEventos[$i]['horario_final'];
			$cadena_lugar = '';

			if($datosEventos[$i]['ind_estado'] > 0)
			{
				$datosEstado  = $this->atIntranetModelo->metBusquedaUnicaEstado($datosEventos[$i]['ind_estado']);
				$cadena_lugar = $datosEventos[$i]['ind_lugar'].", EDO.".$datosEstado['ind_estado'] ;
				if($datosEventos[$i]['ind_municipio'] > 0)
				{
					$datosMunicipio  = $this->atIntranetModelo->metBusquedUnicaMunicipio($datosEventos[$i]['ind_municipio']);
					$cadena_lugar = $cadena_lugar.", MCPO. ".$datosMunicipio['ind_municipio'];
					if($datosEventos[$i]['ind_parroquia'] > 0)
					{
						$datosParroquia  = $this->atIntranetModelo->metBusquedaUnicaParroquia($datosEventos[$i]['ind_parroquia']);
						$cadena_lugar = $cadena_lugar.", PARROQUIA ".$datosParroquia['ind_parroquia'];
					}
				}

			}
			else
			{
				$cadena_lugar = $datosEventos[$i]['ind_lugar'];
			}

			$cad_array_eventos = $cad_array_eventos.' '.'["'.$datosEventos[$i]['ind_anual'].'","'.$aux[1].'", "'.$aux[2].'","'.$aux[0].'","","'.$hora_inicio." ".$horario_inicio.'","'.$hora_final." ".$horario_final.'","", "'.$datosEventos[$i]['ind_descripcion'].'", "'.$cadena_lugar.'", "<img src=\"'.BASE_URL.'publico/imagenes/modIN/eventos/'.$datosEventos[$i]['ind_ruta_img'].'\" width=\"160\" height=\"140\">"]';
			if(($i+1) < count($datosEventos))
			{
				$cad_array_eventos = $cad_array_eventos.", ";
			}

		}

		$mes = date('m');
		$dia = date('d');

		$mes2 = date('n');
		$mes_nombres = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
		$mes2 = ($mes2-1);
		$nombre_mes = strtoupper($mes_nombres[$mes2]);
		$nombre_mes_titulo = $mes_nombres[$mes2];

		$swCumple = 0;
		$listadoCumpleHoy = $this->atIntranetModelo->metBuscarCumpleanierosHoy($mes, $dia);

		if($listadoCumpleHoy)
		{
			for($i=0; $i < count($listadoCumpleHoy); $i++)
			{
				$listadoCumpleHoy[$i]['ind_nombre1'] = trim(ucfirst(mb_strtolower($listadoCumpleHoy[$i]['ind_nombre1'])));
				$listadoCumpleHoy[$i]['ind_apellido1'] = trim(ucfirst(mb_strtolower($listadoCumpleHoy[$i]['ind_apellido1'])));

				if(strcmp(trim($listadoCumpleHoy[$i]['txt_abreviacion']),'DDRAD') == 0)
				{
					$listadoCumpleHoy[$i]['ind_dependencia'] = 'Determinación';
				}
				else
				{
					if(strcmp(trim($listadoCumpleHoy[$i]['txt_abreviacion']),'OAC') == 0)
					{
						$listadoCumpleHoy[$i]['ind_dependencia'] = 'OAC';
					}
					else
					{
						$listadoCumpleHoy[$i]['ind_dependencia'] = trim(ucfirst(mb_strtolower($listadoCumpleHoy[$i]['ind_dependencia'])));
					}
				}



			}

			if( count($listadoCumpleHoy) >= 2 )
			{
				$swCumple = 2;//mas de 1
			}
			else
			{
				$swCumple = 1;//uno
			}

			$this->atVista->assign('diaCumple', $dia );
			$this->atVista->assign('listadoCumpleHoy', $listadoCumpleHoy );
		}
		else
		{
			$listadoCumpleMes = $this->atIntranetModelo->metBuscarCumpleanierosMes($mes);

			if($listadoCumpleMes)
			{
				$swCumple = 3;//no hay para el dia de hoy
			}

			//no hay cumpleañero $swCumple = 0;

		}
		$this->atVista->assign('nombre_mes', $nombre_mes );
		$this->atVista->assign('nombre_mes_titulo', $nombre_mes_titulo );
		$this->atVista->assign('swCumple', $swCumple );
		$this->atVista->assign('diaCumple', $dia );
		$datosNoticias  = $this->atIntranetModelo->metBuscarNoticias();
		if($datosNoticias)
		{
			$this->atVista->assign('arrayNoticias', $datosNoticias );
		}
		else
		{
			$this->atVista->assign('arrayNoticias', 0 );
		}

		$this->atVista->assign('arrayEventos', $cad_array_eventos );

		//####################################
		//# FIN INTRANET
		//####################################

		$this->atVista->metRenderizar('inicio');
	}

	//#####################################
	//# FUNCIONES INTRANET
	//#####################################
	public function metListadoCumpleanios()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'wizard/wizardfa6c',
			'jquery-validation/dist/site-demo',


		);

		$complementoJs = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min',
			'inputmask/jquery.inputmask.bundle.min',
			'wizard/jquery.bootstrap.wizard.min',



		);

		$js = array('materialSiace/core/demo/DemoTableDynamic',
			'materialSiace/core/demo/DemoFormWizard',
			'materialSiace/core/demo/DemoFormComponents'

		);


		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementoJs);
		$this->atVista->metCargarJs($js);


		$mes = date('m');
		$listadoCumpleMes = $this->atIntranetModelo->metBuscarCumpleanierosMes($mes);

		for($i=0; $i < count($listadoCumpleMes); $i++)
		{
			$listadoCumpleMes[$i]['ind_nombre1'] = trim(ucfirst(strtolower($listadoCumpleMes[$i]['ind_nombre1'])));
			$listadoCumpleMes[$i]['ind_apellido1'] = trim(ucfirst(strtolower($listadoCumpleMes[$i]['ind_apellido1'])));
			$listadoCumpleMes[$i]['ind_dependencia'] = trim(ucfirst(strtolower($listadoCumpleMes[$i]['ind_dependencia'])));
			$aux = explode('-', $listadoCumpleMes[$i]['fec_nacimiento']);
			$listadoCumpleMes[$i]['fec_nacimiento'] = $aux[2];
		}

		$titulo_listado_cumple = $_POST['titulo_listado_cumple'];

		$this->atVista->assign('titulo_listado_cumple', $titulo_listado_cumple);
		$this->atVista->assign('listadoCumpleMes', $listadoCumpleMes);
		$this->atVista->metRenderizar('visualizarCumpleanios', 'modales');

	}

	public function metBuscarNoticia()
	{
		$pk_num_noticia = $_POST['pk_num_noticia'];

		$datosNoticias  = $this->atIntranetModelo->metBusquedaUnicaNoticia($pk_num_noticia);

		$aux = trim($datosNoticias["fec_registro"]);
		$aux2 = explode('-', $aux);
		$fecha = $aux2[2]."-".$aux2[1]."-".$aux2[0];
		$datosNoticias["fec_registro"] = $fecha." ".$datosNoticias["hora_regisro"];

		$this->atVista->assign('datoNoticias', $datosNoticias );
		$this->atVista->metRenderizar('visualizarNoticia', 'modales');
	}

	public function metDescargarConstanciaTrabajo()
	{
		$idEmpleado = Session::metObtener('idEmpleado');
		$datos = $this->atIntranetModelo->metDatosPersonalesUsuario($idEmpleado);
		if($datos)
		{
			for ($i = 0; $i < count($datos); $i++) {
				$datos['ind_nombre1'] = trim(ucfirst(strtolower($datos['ind_nombre1'])));
				$datos['ind_apellido1'] = trim(ucfirst(strtolower($datos['ind_apellido1'])));
			}

			$this->atVista->assign('datosUsuario', $datos );
			$this->atVista->metRenderizar('constanciaTrabajo', 'modales');
		}
		else
		{
			echo "Error: metDatosPersonalesUsuario -> idEmpleado: ".$idEmpleado;
		}


	}

	public function metDescargarCertificados()
	{

		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'wizard/wizardfa6c',
			'jquery-validation/dist/site-demo'
		);

		$complementoJs = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min',
			'inputmask/jquery.inputmask.bundle.min',
			'wizard/jquery.bootstrap.wizard.min'
		);

		$js = array('materialSiace/core/demo/DemoTableDynamic',
			'materialSiace/core/demo/DemoFormWizard',
			'materialSiace/core/demo/DemoFormComponents'
		);


		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementoJs);
		$this->atVista->metCargarJs($js);

		$idEmpleado = Session::metObtener('idEmpleado');
		$datos = $this->atIntranetModelo->metDatosPersonalesUsuario($idEmpleado);
		if($datos)
		{
			for ($i = 0; $i < count($datos); $i++) {
				$datos['ind_nombre1'] = trim(ucfirst(strtolower($datos['ind_nombre1'])));
				$datos['ind_apellido1'] = trim(ucfirst(strtolower($datos['ind_apellido1'])));
			}
			//***********************************************

			$swCertificado = 0;

            $datosCertificadosPer = $this->atIntranetModelo->buscarCertificadoPersona($datos['pk_num_persona']);
			$datosCertificados = $this->atIntranetModelo->metListarCertificados($datos['ind_cedula_documento']);

			if($datosCertificados)
			{
                for ($j = 0; $j < count($datosCertificados); $j++)
                {
                    for ($z = 0; $z < count($datosCertificadosPer); $z++)
                    {
                        if($datosCertificados[$j]["pk_num_evento"] == $datosCertificadosPer[$z]["fk_evb001_num_evento"] && $datosCertificados[$j]["pk_num_persona"] == $datosCertificadosPer[$z]["fk_a003_num_persona"])
                        {
                            $datosCertificados[$j]["num_certificado"] = $datosCertificadosPer[$z]["num_certificado"];
                        }
                    }

                }

                $codPonente = $this->atEventoModelo->metConsultarTipoPersona(1);//PONENTE
                $codParticipante = $this->atEventoModelo->metConsultarTipoPersona(2);//PARTICIPANTE

                $this->atVista->assign('codPonente', $codPonente['pk_num_miscelaneo_detalle'] );
                $this->atVista->assign('codParticipante', $codParticipante['pk_num_miscelaneo_detalle'] );
                $this->atVista->assign('datosCertificados', $datosCertificados );
			}
			else
			{
				$swCertificado = 1;
			}
			//***********************************************

			$this->atVista->assign('swCertificado', $swCertificado);
			$this->atVista->assign('datosUsuario', $datos );
			$this->atVista->metRenderizar('certificados', 'modales');
		}
		else
		{
			echo "Error: metDatosPersonalesUsuario -> idEmpleado: ".$idEmpleado;
		}
	}

	public function metGenerarAbreviatura($instruccionPrevia, $sexo)
	{
		switch($instruccionPrevia){
			case 1:
				$abreviatura = 'BR.';
				break;
			case 2:
				$abreviatura = 'TSU.';
				break;
			case 3:
				if($sexo=='M'){
					$abreviatura = 'LCDO.';
				} else {
					$abreviatura = 'LCDA.';
				}
				break;
			case 4:
				$abreviatura = 'ING.';
				break;
			case 5:
				$abreviatura = 'ESP.';
				break;
			case 6:
				$abreviatura = 'MG.';
				break;
			case 7:
				$abreviatura = 'ECON.';
				break;
			case 8:
				$abreviatura = 'MED.';
				break;
			case 9:
				$abreviatura = 'ABG.';
				break;
			case 10:
				if($sexo=='M'){
					$abreviatura = 'DR.';
				} else {
					$abreviatura = 'DRA.';
				}
				break;
		}
		return $abreviatura;
	}

	public function metBuscarMes($mesFecha)
	{
		switch($mesFecha){
			case 1: $mes="Enero";break;
			case 2: $mes="Febrero";break;
			case 3: $mes="Marzo";break;
			case 4: $mes="Abril";break;
			case 5: $mes="Mayo";break;
			case 6: $mes="Junio";break;
			case 7: $mes="Julio";break;
			case 8: $mes="Agosto";break;
			case 9: $mes="Septiembre";break;
			case 10:$mes="Octubre";break;
			case 11:$mes="Noviembre";break;
			case 12:$mes="Diciembre";break;
		}
		return $mes;
	}

	public function metGenerarCertificado()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');

		$pkNumPersona = $_GET['pk_num_persona'];
		$pkNumEvento = $_GET['pk_num_evento'];
		$verCertificado = $_GET['num_certificado'];

		$this->metObtenerLibreria('cabeceraEvento', 'modEV');

		$pdf = new pdfGeneralEvento('L', 'mm', 'Letter');
		// Obtengo el fondo del certificado
		$verEvento = $this->atEventoModelo->metVerEvento($pkNumEvento);
		//Verifico el tipo de evento
		if(($verEvento['cod_detalle']==1)||($verEvento['cod_detalle']==6)||($verEvento['cod_detalle']==8)||($verEvento['cod_detalle']==9)||($verEvento['cod_detalle']==10))
		{
			$continuacion = 'A LA';
			$continuacionPonente = 'LA';
		}
		else
		{
			$continuacion = 'AL';
			$continuacionPonente = 'EL';
		}

		$explodeDuracion = explode(":", $verEvento['fec_horas_total']);
		$duracionHora = $explodeDuracion[0];
		if($duracionHora>1){
			$duracion = $duracionHora.' horas académicas';
		} else{
			$duracion = $duracionHora.' hora académica';
		}
		// Fecha
		$explodeFecha = explode("/", $verEvento['fecha_registro']);
		$diaFecha = $explodeFecha[0];
		$mesFecha = $explodeFecha[1];
		$buscarMes = $this->metBuscarMes($mesFecha);
		// Consulta del contralor Vigente
		$consultarContralor = $this->atEventoModelo->metConsultarContralor();
        $infoOrganismoCert = $this->atEventoModelo->metConsultarInfoDetalleOrganismo($this->atIdDetalleOrg);
        $ciudadCertificado = ucwords(mb_strtolower($infoOrganismoCert["ind_ciudad"]));
        $instruccionContralor = $this->atEventoModelo->metInstruccionContralor($consultarContralor["pk_num_empleado"]);
        $sexoContralorCert = $this->atEventoModelo->metObtenerSexo($consultarContralor["pk_num_empleado"]);
        $abreviaturaContralor = $this->metGenerarAbreviaturaContralor(trim($instruccionContralor["ind_nombre_nivel_grado"]), $sexoContralorCert["cod_detalle"]);
		//Certificados para los ponentes
		if($verEvento['num_flag_certificado_ponente']>0) { //si tiene ponentes
			$ponenteTipo = $this->atEventoModelo->metConsultarTipoPersona(1);
			$ponente = $this->atEventoModelo->metListarPersonaCertificado($pkNumEvento, $ponenteTipo['pk_num_miscelaneo_detalle'], $pkNumPersona);

			if($ponente)//la persona es un ponente
			{

				$pdf->AliasNbPages();
				$pdf->AddPage();
				$pdf->SetAutoPageBreak(true, 5);
				$pdf->imagenCertificado($verEvento['ind_certificado']);
				$pdf->setXY(105, 79);
				$pdf->SetFont	('Arial', 'B', 8);

				$pdf->Cell(22, 4, $verCertificado, 0, 0, 'C', 0);
				$pdf->Cell(26, 4, str_pad($verEvento['pk_num_evento'], 6, "0", STR_PAD_LEFT) . '-' . $verEvento['anio_registro'], 0, 0, 'C', 0);
				$pdf->Cell(17, 4, $verEvento['fecha_registro'], 0, 0, 'C', 0);
				$pdf->Ln();
				$pdf->Ln();
				$pdf->Ln();
				$pdf->Ln();
				$pdf->SetFont('Arial', 'B', 16);
				$pdf->MultiCell(0, 7, utf8_decode($ponente['ind_nombre1'] . ' ' . $ponente['ind_apellido1']), '', 'C', 0);
				$pdf->Ln(1);
				$pdf->MultiCell(0, 7, "C.I " . number_format($ponente['ind_cedula_documento'], 0, "", "."), '', 'C', 0);
				$pdf->Ln();
				$pdf->SetFont('Arial', '', 11);
				$pdf->MultiCell(0, 7, utf8_decode('POR HABER DICTADO ' . $continuacionPonente . ' ' . $verEvento['ind_nombre_detalle']), '', 'C', 0);
				$pdf->SetFont('Arial', 'BI', 17);
				$pdf->SetTextColor(0,0,160);
				$pdf->MultiCell(0, 7, utf8_decode($verEvento['ind_nombre_evento']), '', 'C', 0);
				$pdf->SetTextColor(0, 0, 0);
				$pdf->SetFont('Arial', '', 10.5);
				$pdf->MultiCell(0, 7, utf8_decode('Duración: ' . $duracion), '', 'C', 0);
				$pdf->MultiCell(0, 7, utf8_decode('Cumaná, ' . $diaFecha . ' de ' . $buscarMes . ' de ' . $verEvento['anio_registro']), '', 'C', 0);
				$pdf->Ln();
				$pdf->Ln();
				$pdf->SetX(95);
				$pdf->SetFont('Arial', 'B', 10);

                $rutaFirmaContralor = $this->atEventoModelo->metConsultarFirmaContralor($consultarContralor["pk_num_empleado"]);
                if ($rutaFirmaContralor['ind_firma_png'])
                {
                    $pdf->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'modEV' . DS . 'firmas' . DS . $rutaFirmaContralor['ind_firma_png'], 115, 155, 50, 30);
                    $pdf->Cell(130, 5, "                                       ", 0, 0, 'C', 0);
                }
                else//Si no tiene firma registrada el contralor
                {
                    $pdf->SetXY(95,170);
                    $pdf->Cell(190 / 2, 5, "_______________________________________", 0, 1, 'C', 0);
                }

                $pdf->SetXY(95,175);
                $pdf->Cell(190 / 2, 5, utf8_decode($abreviaturaContralor." ".$consultarContralor['ind_nombre1'].' '.$consultarContralor['ind_apellido1']), 0, 1, 'C', 0);
                $pdf->SetXY(95,180);
                $pdf->Cell(190 / 2, 4, $infoOrganismoCert['ind_cargo_representante'], '', 0, 'C', 0);

                // CODIGO QR
				$this->metObtenerLibreria('qrlib', 'phpqrcode');
				$nombreFichero = ROOT . 'librerias' . DS . 'modEV' . DS . 'codigoQR' . DS. $pkNumEvento.$ponente['ind_cedula_documento'].'.png';
				if (file_exists($nombreFichero)) {

				} else {
					QRcode::png($verCertificado.' '.$pkNumPersona.' '.$ponente['ind_nombre1'] . ' ' . $ponente['ind_apellido1'].' '.$ponente['ind_cedula_documento'].' '.$verEvento['ind_nombre_evento'], $nombreFichero);
				}
				$pdf->SetXY(95, 90);
				$pdf->Cell(100,100); $pdf->Image(ROOT.'librerias'. DS . 'modEV' . DS . 'codigoQR'. DS. $pkNumEvento.$ponente['ind_cedula_documento'].'.png',5, 174,24,24);
			}
		}

		if($verEvento['num_flag_certificado_participante']>0)//si el evento tiene participantes
		{
            $ponenteTipo2 = $this->atEventoModelo->metConsultarTipoPersona(1);
			$listarPonente = $this->atEventoModelo->metConsultarPonentesFirmantesCert($pkNumEvento, $ponenteTipo2['pk_num_miscelaneo_detalle']);
			$contadorPonente = count($listarPonente);
			$i = 0;

			foreach($listarPonente as $ponente)
			{

                $ponenteNombre[$i] = $ponente['ind_nombre1'].' '.$ponente['ind_nombre2'].' '.$ponente['ind_apellido1'];
                $instruccionPrevia = $ponente['cod_detalle'];
                $pkNumPersonaPonente = $ponente['fk_a003_num_persona'];
                $pkFirmaPonente[] = $ponente['fk_a003_num_persona'];
                $tipoFirmaPonente[] = $ponente['num_firma'];
                $rutaFirmaPonente[] = $ponente['ind_firma_png'];
                $numPersonasPonentes[$i] = $pkNumPersonaPonente;
                $consultarSexo = $this->atEventoModelo->metObtenerSexo($pkNumPersonaPonente);
                if(empty($ponente['cargo_visita']))
                {
                    $cargoVisita = $this->atEventoModelo->metBuscarCargo($pkNumPersonaPonente);
                    $cargoEmpleado[$i] = $cargoVisita["ind_nombre_cargo"];
                }
                else//la persona es un visitante con cargo
                {
                    $cargoEmpleado[$i] = $ponente['cargo_visita'];
                }

                if($consultarSexo['cod_detalle']=='M'){
                    $facilitar[$i] = 'FACILITADOR';
                } else {
                    $facilitar[$i] = 'FACILITADORA';
                }
                $abreviatura[$i] = $this->metGenerarAbreviatura($instruccionPrevia, $consultarSexo['cod_detalle']);
                $i++;
			}

			//*****************************

            $participante = $this->atEventoModelo->metConsultarTipoPersona(2);
			$persona = $this->atEventoModelo->metListarPersonaCertificado($pkNumEvento, trim($participante['pk_num_miscelaneo_detalle']), $pkNumPersona);

			if($persona)//la persona del listado es participante
			{
				$pdf->AliasNbPages();
				$pdf->AddPage();
				$pdf->SetAutoPageBreak(true, 5);
				$pdf->imagenCertificado($verEvento['ind_certificado']);
				$pdf->setXY(105, 79);
				$pdf->SetFont('Arial', 'B', 8);

				$pdf->Cell(22, 4, $verCertificado, 0, 0, 'C', 0);
				$pdf->Cell(26, 4, str_pad($verEvento['pk_num_evento'], 6, "0", STR_PAD_LEFT) . '-' . $verEvento['anio_registro'], 0, 0, 'C', 0);
				$pdf->Cell(17, 4, $verEvento['fecha_registro'], 0, 0, 'C', 0);
				$pdf->Ln();
				$pdf->Ln();
				$pdf->Ln();
				$pdf->Ln();
				$pdf->SetFont('Arial', 'B', 16);
				$pdf->MultiCell(0, 7, utf8_decode($persona['ind_nombre1'] . ' ' . $persona['ind_apellido1']), '', 'C', 0);
				$pdf->Ln(1);
				$pdf->MultiCell(0, 7, "C.I " . number_format($persona['ind_cedula_documento'], 0, "", "."), '', 'C', 0);
				$pdf->Ln();
				$pdf->SetFont('Arial', '', 11);
                if(strcmp("RECONOCIMIENTO", trim($verEvento['ind_nombre_detalle']))!=0)
                {
                    $pdf->MultiCell(0, 7, utf8_decode('POR HABER ASISTIDO ' . $continuacion . ' ' . $verEvento['ind_nombre_detalle']), '', 'C', 0);
                    $cad_duracion_eve = 'Duración: ' . $duracion;
                }
                else
                {
                    $cad_duracion_eve = '';
                }
				$pdf->SetFont('Arial', 'BI', 17);
				$pdf->SetTextColor(0,0,160);
				$pdf->MultiCell(0, 7, utf8_decode($verEvento['ind_nombre_evento']), '', 'C', 0);
				$pdf->SetTextColor(0, 0, 0);
				$pdf->SetFont('Arial', '', 10.5);
                $pdf->MultiCell(0, 7, utf8_decode($cad_duracion_eve), '', 'C', 0);
                $pdf->MultiCell(0, 7, utf8_decode($ciudadCertificado.', ' . $diaFecha . ' de ' . $buscarMes . ' de ' . $verEvento['anio_registro']), '', 'C', 0);
				$pdf->SetFont('Arial', 'B', 10);
				$pdf->Ln();
				$pdf->Ln();
				//==================
                if(strcmp("RECONOCIMIENTO", trim($verEvento['ind_nombre_detalle']))!=0)//EL EVENTO ES DISTINTO A UN RECONOCIMIENTO
                {
                    //NOTA: SOLO SE PERMITEN 3 FIRMAS, EL CONTRALOR FIRMA TODOS LOS CERTIFICADOS
                    if($contadorPonente==1)// 1 SOLO PONENTE
                    {
                        if($pkFirmaPonente[0] > 0)//PONENTE FIRMA
                        {
                            if ($pkFirmaPonente[0] == $consultarContralor["pk_num_empleado"])// 1 FIRMA - EXPONTE CONTRALOR
                            {

                                $pdf->SetFont('Arial', 'B', 10);
                                //=======
                                if ($tipoFirmaPonente[0] == 1)//firma escrita
                                {

                                    $pdf->SetXY(95,170);
                                    $pdf->Cell(190 / 2, 5, "_______________________________________", 0, 1, 'C', 0);
                                }
                                else
                                    if ($tipoFirmaPonente[0] == 2)//firma de imagen
                                    {
                                        $pdf->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'modEV' . DS . 'firmas' . DS . $rutaFirmaPonente[0], 115, 155, 50, 30);
                                    }
                                //======
                                $pdf->SetXY(95,175);
                                $pdf->Cell(190 / 2, 5, utf8_decode($abreviaturaContralor." ". $consultarContralor['ind_nombre1'] . ' ' . $consultarContralor['ind_apellido1']), 0, 1, 'C', 0);
                                $pdf->SetXY(95,180);
                                $pdf->Cell(190 / 2, 4, $infoOrganismoCert['ind_cargo_representante'], '', 0, 'C', 0);
                            }
                            else// PONENTE - NO ES EL CONTRALOR - 2 FIRMAS
                            {

                                $rutaFirmaContralor = $this->atEventoModelo->metConsultarFirmaContralor($consultarContralor["pk_num_empleado"]);

                                if ($rutaFirmaContralor['ind_firma_png'])
                                {
                                    $pdf->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'modEV' . DS . 'firmas' . DS . $rutaFirmaContralor['ind_firma_png'], 50, 155, 50, 30);

                                }
                                else//Si no tiene firma registrada el contralor
                                {
                                    $pdf->SetXY(10,170);
                                    $pdf->Cell(130, 5, "__________________________________", 0, 0, 'C', 0);
                                }

                                if ($tipoFirmaPonente[0] == 1)
                                {
                                    $pdf->SetXY(140,170);
                                    $pdf->Cell(130, 5, "__________________________________", 0, 1, 'C', 0);
                                }
                                else
                                {
                                    if ($tipoFirmaPonente[0] == 2)
                                    {
                                        $pdf->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'modEV' . DS . 'firmas' . DS . $rutaFirmaPonente[0], 180, 155, 50, 30);

                                    }
                                }

                                $pdf->SetXY(10,175);
                                $pdf->Cell(130, 5, utf8_decode($abreviaturaContralor.' ' . $consultarContralor['ind_nombre1'] . ' ' . $consultarContralor['ind_nombre2'] . ' ' . $consultarContralor['ind_apellido1'] . ' ' . $consultarContralor['ind_apellido2']), 0, 0, 'C', 0);
                                $pdf->SetXY(10,180);
                                $pdf->Cell(130, 4, $infoOrganismoCert['ind_cargo_representante'], 0, 0, 'C', 0);
                                //===
                                $pdf->SetXY(140,175);
                                $pdf->Cell(130, 5, utf8_decode($abreviatura[0] . ' ' . $ponenteNombre[0]), 0, 1, 'C', 0);
                                $pdf->SetXY(140,180);
                                $pdf->Cell(130, 4, $facilitar[0], 0, 0, 'C', 0);
                            }
                        }

                    }
                    else
                    {
                        if($contadorPonente==2)//2 PONENTES
                        {
                            if( $numPersonasPonentes[0] == $consultarContralor["pk_num_empleado"] || $numPersonasPonentes[1] == $consultarContralor["pk_num_empleado"])
                            {   //UNO DE LOS PONENTES ES EL CONTRALOR - SOLO 2 FIRMAS

                                if($numPersonasPonentes[0] == $consultarContralor["pk_num_empleado"])
                                {

                                    $cad_facilitar1 = $infoOrganismoCert['ind_cargo_representante'];
                                    $cad_facilitar2 = $facilitar[1];
                                }
                                else
                                {

                                    $cad_facilitar2 = $infoOrganismoCert['ind_cargo_representante'];
                                    $cad_facilitar1 = $facilitar[0];

                                }

                                //====
                                if ($tipoFirmaPonente[0] == 1)
                                {

                                    $pdf->SetXY(10,170);
                                    $pdf->Cell(130, 5, "__________________________________", 0, 0, 'C', 0);

                                }
                                else
                                {
                                    if ($tipoFirmaPonente[0] == 2)
                                    {
                                        $pdf->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'modEV' . DS . 'firmas' . DS . $rutaFirmaPonente[0], 50, 155, 50, 30);

                                    }

                                }

                                if ($tipoFirmaPonente[1] == 1)
                                {

                                    $pdf->SetXY(140,170);
                                    $pdf->Cell(130, 5, "__________________________________", 0, 1, 'C', 0);

                                }
                                else
                                {
                                    if ($tipoFirmaPonente[1] == 2)
                                    {
                                        $pdf->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'modEV' . DS . 'firmas' . DS . $rutaFirmaPonente[1], 180, 155, 50, 30);

                                    }

                                }
                                //====
                                $pdf->SetXY(10,175);
                                $pdf->Cell(130, 5, utf8_decode($abreviatura[0] . $ponenteNombre[0] ), 0, 0, 'C', 0);
                                $pdf->SetXY(10,180);
                                $pdf->Cell(130, 4, $cad_facilitar1, 0, 0, 'C', 0);
                                //===
                                $pdf->SetXY(140,175);
                                $pdf->Cell(130, 5, utf8_decode($abreviatura[1] . ' ' . $ponenteNombre[1]), 0, 1, 'C', 0);
                                $pdf->SetXY(140,180);
                                $pdf->Cell(130, 4, $cad_facilitar2, 0, 0, 'C', 0);

                            }
                            else //NINGUNO DE LOS DOS PONENTES ES CONTRALOR - 3 FIRMAS
                            {
                                $pdf->SetFont('Arial', 'B', 9);
                                //===CONTRALOR
                                $rutaFirmaContralor = $this->atEventoModelo->metConsultarFirmaContralor($consultarContralor["pk_num_empleado"]);

                                if ($rutaFirmaContralor['ind_firma_png'])
                                {
                                    $pdf->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'modEV' . DS . 'firmas' . DS . $rutaFirmaContralor['ind_firma_png'], 40, 155, 50, 30);

                                }
                                else//Si no tiene firma registrada el contralor
                                {
                                    $pdf->SetXY(40,170);
                                    $pdf->Cell(50, 5, "__________________________", 0, 0, 'C', 0);
                                }


                                $pdf->SetXY(10,175);
                                $pdf->Cell(110, 5, utf8_decode($abreviaturaContralor." ". $consultarContralor['ind_nombre1'] . ' ' . $consultarContralor['ind_nombre2'] . ' ' . $consultarContralor['ind_apellido1'] . ' ' . $consultarContralor['ind_apellido2'] ), 0, 0, 'C', 0);
                                $pdf->SetXY(10,180);
                                $pdf->Cell(110, 4, $infoOrganismoCert['ind_cargo_representante'], 0, 0, 'C', 0);

                                //=====PONENTE 1
                                if ($tipoFirmaPonente[0] == 1)
                                {

                                    $pdf->SetXY(120,170);
                                    $pdf->Cell(50, 5, "__________________________", 0, 0, 'C', 0);

                                }
                                else//Si no tiene firma registrada el contralor
                                {
                                    if ($tipoFirmaPonente[0] == 2)
                                    {
                                        $pdf->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'modEV' . DS . 'firmas' . DS . $rutaFirmaPonente[0], 120, 155, 50, 30);

                                    }

                                }

                                $pdf->SetXY(80,175);
                                $pdf->Cell(130, 5, utf8_decode($abreviatura[0] . ' ' . $ponenteNombre[0]), 0, 0, 'C', 0);
                                $pdf->SetXY(80,180);
                                $pdf->Cell(130, 4, $facilitar[0], 0, 0, 'C', 0);


                                //=====PONENTE 2
                                if ($tipoFirmaPonente[1] == 1)
                                {

                                    $pdf->SetXY(200,170);
                                    $pdf->Cell(50, 5, "__________________________", 0, 1, 'C', 0);

                                }
                                else//Si no tiene firma registrada el contralor
                                {
                                    if ($tipoFirmaPonente[1] == 2)
                                    {
                                        $pdf->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'modEV' . DS . 'firmas' . DS . $rutaFirmaPonente[1], 200, 155, 50, 30);

                                    }

                                }

                                $pdf->SetXY(170,175);
                                $pdf->Cell(110, 5, utf8_decode($abreviatura[1] . ' ' . $ponenteNombre[1]), 0, 1, 'C', 0);
                                $pdf->SetXY(170,180);
                                $pdf->Cell(110, 4, $facilitar[1], 0, 0, 'C', 0);

                            }
                        }
                        else //3 FIRMAS
                        {
                            // 3 PONENTES Y UNO ES CONTRALOR
                            $pdf->SetFont('Arial', 'B', 9);

                            if($numPersonasPonentes[0] == $consultarContralor["pk_num_empleado"])
                            {
                                $facilitar[0] = $infoOrganismoCert['ind_cargo_representante'];

                            }
                            else
                            {
                                if($numPersonasPonentes[1] == $consultarContralor["pk_num_empleado"])
                                {
                                    $facilitar[1] = $infoOrganismoCert['ind_cargo_representante'];

                                }
                                else
                                {
                                    $facilitar[2] = $infoOrganismoCert['ind_cargo_representante'];
                                }
                            }

                            if($tipoFirmaPonente[0] == 1)
                            {
                                $pdf->SetXY(40,170);
                                $pdf->Cell(50, 5, "__________________________", 0, 0, 'C', 0);
                            }
                            else
                            {
                                if ($tipoFirmaPonente[0] == 2)
                                {
                                    $pdf->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'modEV' . DS . 'firmas' . DS . $rutaFirmaPonente[0], 40, 155, 50, 30);
                                }
                            }

                            $pdf->SetXY(10,175);
                            $pdf->Cell(110, 5, utf8_decode($abreviatura[0] . ' ' . $ponenteNombre[0]), 0, 0, 'C', 0);
                            $pdf->SetXY(10,180);
                            $pdf->Cell(110, 4, $facilitar[0], 0, 0, 'C', 0);

                            //==

                            if($tipoFirmaPonente[1] == 1)
                            {
                                $pdf->SetXY(119,170);
                                $pdf->Cell(50, 5, "__________________________", 0, 0, 'C', 0);
                            }
                            else
                            {
                                if ($tipoFirmaPonente[1] == 2)
                                {
                                    $pdf->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'modEV' . DS . 'firmas' . DS . $rutaFirmaPonente[1], 119, 155, 50, 30);
                                }
                            }

                            $pdf->SetXY(80,175);
                            $pdf->Cell(130, 5, utf8_decode($abreviatura[1] . ' ' . $ponenteNombre[1]), 0, 0, 'C', 0);
                            $pdf->SetXY(80,180);
                            $pdf->Cell(130, 4, $facilitar[1], 0, 0, 'C', 0);

                            //===

                            if($tipoFirmaPonente[2] == 1)
                            {
                                $pdf->SetXY(200,170);
                                $pdf->Cell(50, 5, "__________________________", 0, 1, 'C', 0);
                            }
                            else
                            {
                                if ($tipoFirmaPonente[2] == 2)
                                {
                                    $pdf->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'modEV' . DS . 'firmas' . DS . $rutaFirmaPonente[2], 200, 155, 50, 30);

                                }
                            }

                            $pdf->SetXY(170,175);
                            $pdf->Cell(110, 5, utf8_decode($abreviatura[2] . ' ' . $ponenteNombre[2]), 0, 1, 'C', 0);
                            $pdf->SetXY(170,180);
                            $pdf->Cell(110, 4, $facilitar[2], 0, 0, 'C', 0);

                        }
                    }
                }
                else
                {
                    //ES UN RECONOCIMIENTO
                    $pdf->SetFont('Arial', 'B', 10);

                    $rutaFirmaContralor = $this->atEventoModelo->metConsultarFirmaContralor($consultarContralor["pk_num_empleado"]);

                    $pdf->Ln();

                    if ($rutaFirmaContralor['ind_firma_png'])
                    {
                        $pdf->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'modEV' . DS . 'firmas' . DS . $rutaFirmaContralor['ind_firma_png'], 115, 155, 50, 30);

                    }
                    else//Si no tiene firma registrada el contralor
                    {
                        $pdf->SetXY(95,170);
                        $pdf->Cell(190 / 2, 5, "_______________________________________", 0, 1, 'C', 0);
                    }

                    $pdf->SetXY(95,175);
                    $pdf->Cell(190 / 2, 5, utf8_decode($abreviaturaContralor." ". $consultarContralor['ind_nombre1'] . ' ' . $consultarContralor['ind_apellido1']), 0, 1, 'C', 0);
                    $pdf->SetXY(95,180);
                    $pdf->Cell(190 / 2, 4, $infoOrganismoCert['ind_cargo_representante'], '', 0, 'C', 0);

                }
				//=======
				// CODIGO QR
				$this->metObtenerLibreria('qrlib', 'phpqrcode');
				$nombreFichero = ROOT . 'librerias' . DS . 'modEV' . DS . 'codigoQR' . DS. $pkNumEvento.$persona['ind_cedula_documento'].'.png';
				if (file_exists($nombreFichero)) {

				} else {
					QRcode::png($verCertificado['num_certificado'].' '.$pkNumPersona.' '.$persona['ind_nombre1'] . ' ' . $persona['ind_apellido1'].' '.$persona['ind_cedula_documento'].' '.$verEvento['ind_nombre_evento'], $nombreFichero);
				}
				$pdf->SetXY(95, 90);
				$pdf->Cell(100,100); $pdf->Image(ROOT.'librerias'. DS . 'modEV' . DS . 'codigoQR'. DS. $pkNumEvento.$persona['ind_cedula_documento'].'.png',10, 150,22,22);
			}
		}
		//*************************
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 5);
		$pdf->SetFont('Arial','B',20);
		$pdf->Ln();
		$pdf->SetXY(10, 15);
		$pdf->Cell(40,5,"CONTENIDO: ",'',0,'C',0);
		$pdf->Ln();
		$verTema = $this->atEventoModelo->metVerTema($pkNumEvento);
		$pdf->Ln();
		$pdf->SetFont('Arial','',12);
		$pdf->SetTextColor(0, 0, 0);
		foreach($verTema as $tema){
			$pdf->MultiCell(0, 7, utf8_decode('.- ' .$tema['ind_tema']), '', 'L', 0);
		}
		$pdf->Output();

	}

    public function metGenerarAbreviaturaContralor($instruccion, $sexo)
    {
        switch($instruccion){
            case "BACHILLER":
                $abreviatura = 'BR.';
                break;
            case "TECNICO SUPERIOR UNIVERSITARIO":
                $abreviatura = 'TSU.';
                break;
            case "LICENCIADO":
                if($sexo=='M'){
                    $abreviatura = 'LCDO.';
                } else {
                    $abreviatura = 'LCDA.';
                }
                break;
            case "INGENIERO":
                $abreviatura = 'ING.';
                break;
            case "MEDICO":
                $abreviatura = 'MED.';
                break;
            case "ABOGADO":
                $abreviatura = 'ABG.';
                break;
            case "DOCTOR":
                if($sexo=='M'){
                    $abreviatura = 'DR.';
                } else {
                    $abreviatura = 'DRA.';
                }
                break;
            default:
                $abreviatura = '';
        }
        return $abreviatura;
    }

	public function metListaChatPublico()
	{
		$idEmpleado = Session::metObtener('idEmpleado');
		$datos_usuario = $this->atIntranetModelo->metDatosPersonalesUsuario($idEmpleado);
		$foto_usuario = $datos_usuario['ind_foto'];

		$dia = date('d');
		$mes = date('m');
		$anio = date('Y');

		$listado = $this->atIntranetModelo->metBuscarMensajesChat($dia, $mes, $anio);

		if($listado)
		{
			$listado_chat ='<input type="hidden" id="foto_usuario" value="'.BASE_URL.'publico/imagenes/modRH/fotos/'.$foto_usuario.'" /><ul class="list-chats">
			<input type="hidden" id="total_chat" value="'.count($listado).'" /><ul class="list-chats">';

			for($i=0; $i<count($listado); $i++)
			{
				$listado_chat = $listado_chat.' <li class="chat-left">
													<div class="chat">
														<div class="chat-avatar"><img class="img-circle width-2" src="'.BASE_URL.'publico/imagenes/modRH/fotos/'.$listado[$i]["ind_foto"].'" alt="" /></div>
														<div class="chat-body">
															'.$listado[$i]["ind_mensaje"].'
															<small>'.$listado[$i]["hora"].'</small>
															<small style="font-weight: bold !important;">'.ucfirst(mb_strtolower($listado[$i]["nombre"])).' '.ucfirst(mb_strtolower($listado[$i]["apellido"])).'</small>
														</div>
													</div>
                            					</li>';

			}

			$listado_chat = $listado_chat .'</ul>';

			echo $listado_chat;

		}
		else//No hay resultado
		{
			echo '<input type="hidden" id="foto_usuario" value="'.BASE_URL.'publico/imagenes/modRH/fotos/'.$foto_usuario.'" /><ul class="list-chats">';
		}


	}

	public  function metInsertarMsgChatPublico()
	{
		$ind_mensaje = $this->metObtenerTexto('mensaje');
		$fec_registro = date('Y-m-d H:i:s');
		$fk_b001_num_empleado = Session::metObtener('idEmpleado');

		$respuesta = $this->atIntranetModelo->metInsertarMensajeChat($ind_mensaje, $fec_registro, $fk_b001_num_empleado, $fec_registro);
		if($respuesta > 0)
		{
			$this->metListaChatPublico();
		}
		else
		{
			echo "Error: ".$respuesta;
		}


	}

	public function metNotificacionChatPublico()
	{
		$contador = $_POST['contador'];

		$fk_b001_num_empleado = Session::metObtener('idEmpleado');
		$total = $this->atIntranetModelo->metContarMensajeHoy($fk_b001_num_empleado);
		if($total)
		{
			if($total['total'] == $contador)//no ha variado la cantidad de mensajes
			{
				echo 0;
			}
			else
			{
				if($contador < $total['total'])//hay mensajes nuevos
				{
					echo $total['total'];
				}
				else//se han borrado mensajes, pero no hay nuevos
				{
					echo 0;
				}

			}

		}
		else//no hay mensajes en bd para la fecha HOY
		{
			echo 0;
		}

	}

	public function metCargarChatModal()
	{
		$this->atVista->metRenderizar('ventanaChat', 'modales');
	}

	public function metTotalMensajesOtros()
	{


		$fk_b001_num_empleado = Session::metObtener('idEmpleado');
		$total = $this->atIntranetModelo->metContarMensajeHoy($fk_b001_num_empleado);
		if($total)
		{
			echo $total['total'];
		}
		else//no hay mensajes en bd para la fecha HOY
		{
			echo 0;
		}

	}

	//####################################
	//# FIN FUNCIONES INTRANET
	//####################################

	public function metIndex()
	{
		if(!Session::metObtener('menu') || !Session::metObtener('perfil')){
			Session::metCrear('menu',$this->metNivelMenu());
			$rol=$this->atUsuarioModelo->metObtenerRol(Session::metObtener('idUsuario'));
			for($i=0;$i<count($rol);$i++){
				$roles[]=$rol[$i]['ind_rol'];
			}
			if(!isset($roles)){
				$roles=null;
			}
			Session::metCrear('perfil',$roles);
		}
		$this->atVista->metRenderizar('inicio','inicio');
	}

	public function metNivelMenu()
	{
		$menu0= $this->atMenuModelo->metListarMenu(0);
		for($i=0;$i<count($menu0);$i++){
			$menu1=$this->atMenuModelo->metListarMenu(1,$menu0[$i]['cod_interno']);
			for($ii=0;$ii<count($menu1);$ii++){
				$menu2=$this->atMenuModelo->metListarMenu(2,$menu1[$ii]['cod_interno']);
				for($iii=0;$iii<count($menu2);$iii++){
					$menu3=$this->atMenuModelo->metListarMenu(3,$menu2[$iii]['cod_interno']);
					for($iv=0;$iv<count($menu3);$iv++){
						$menu4=$this->atMenuModelo->metListarMenu(4,$menu3[$iv]['cod_interno']);
						for($v=0;$v<count($menu4);$v++){
							$menu5=$this->atMenuModelo->metListarMenu(5,$menu4[$v]['cod_interno']);
							for($vi=0;$vi<count($menu5);$vi++){
								$menuArray5[$i][$ii][$iii][$iv][$v][]=$menu5[$vi];
							}
							if(!isset($menuArray5[$i][$ii][$iii][$iv][$v])){
								$menuArray5[$i][$ii][$iii][$iv][$v]=null;
							}
							$menuArray4[$i][$ii][$iii][$iv][]=array(
								'menu5'=>$menuArray5[$i][$ii][$iii][$iv][$v],
								'menu4'=>$menu4[$v]
							);
						}
						if(!isset($menuArray4[$i][$ii][$iii][$iv])){
							$menuArray4[$i][$ii][$iii][$iv]=null;
						}
						$menuArray3[$i][$ii][$iii][]=array(
							'menu4'=>$menuArray4[$i][$ii][$iii][$iv],
							'menu3'=>$menu3[$iv]
						);
					}
					if(!isset($menuArray3[$i][$ii][$iii])){
						$menuArray3[$i][$ii][$iii]=null;
					}
					$menuArray2[$i][$ii][]=array(
						'menu3'=>$menuArray3[$i][$ii][$iii],
						'menu2'=>$menu2[$iii]
					);
				}
				if(!isset($menuArray2[$i][$ii])){
					$menuArray2[$i][$ii]=null;
				}
				$menuArray1[$i][]=array(
					'menu2'=>$menuArray2[$i][$ii],
					'menu1'=>$menu1[$ii]
				);
			}
			if(!isset($menuArray1[$i])){
				$menuArray1[$i]=null;
			}
			$menuArray[]=array(
				'menu1'=>$menuArray1[$i],
				'menu'=>$menu0[$i]
			);
		}
		return $menuArray;

	}



}
