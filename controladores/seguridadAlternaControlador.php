<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class seguridadAlternaControlador extends Controlador
{
    private $atSeguridadAlternaModelo;
    public function __construct()
	{
		parent::__construct();
        Session::metAcceso();
        $this->atSeguridadAlternaModelo=$this->metCargarModelo('seguridadAlterna');
	}

	public function metIndex()
	{
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
	}

    /**
     *
     */
    public function metCrear()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $idUsuario=$this->metObtenerInt('idUsuario');
        $valido=$this->metObtenerInt('valido');
        if($valido==1){
            $form=$this->metValidarFormArrayDatos('form','int');
            $this->atSeguridadAlternaModelo->metEliminar($idUsuario);
            foreach ($form['pk_num_seguridad_aplicacion'] AS $i){
                if(isset($form[$i]['pk_num_dependencia'])) {
                    foreach ($form[$i]['pk_num_dependencia'] AS $ii) {
                        $id = $this->atSeguridadAlternaModelo->metRegistrarSeguridadAlterna($idUsuario,$i,$ii);
                        if (is_array($id)){
                            var_dump($id);
                            exit();
                        }
                    }
                }
            }
            $validacion['status']='nuevo';
            echo json_encode($validacion);
            exit;
        }
        if($idUsuario!=0){
            $listar = $this->atSeguridadAlternaModelo->metListaDetalle($idUsuario);
            $listado = [];
            foreach ($listar AS $i){
                $listado[$i['fk_a015_num_seguridad_aplicacion']][$i['fk_a004_num_dependencia']] = 'ok';
            }
            $this->atVista->assign('listado',$listado);
        }

        $this->atVista->assign('usuario',$this->atSeguridadAlternaModelo->metListarUsuarios($idUsuario));
        $this->atVista->assign('dependencias',$this->atSeguridadAlternaModelo->metConsultarDependencias());
        $this->atVista->assign('aplicaciones',$this->atSeguridadAlternaModelo->metListarAplicacion());
        $this->atVista->assign('idUsuario',$idUsuario);
        $this->atVista->metRenderizar('crear','modales');
    }

    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "
            SELECT 
                *,
                a018_seguridad_usuario.num_estatus
            FROM 
              a018_seguridad_usuario 
              INNER JOIN rh_b001_empleado ON a018_seguridad_usuario.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
              INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
            WHERE
              a018_seguridad_usuario.num_estatus = '1'
        ";
        if ($busqueda['value']) {
            #concateno la busqueda si existe, Nota: esto es obligatorio
            $sql .="
                AND
                    ( 
                      a018_seguridad_usuario.ind_usuario LIKE '%$busqueda[value]%' OR 
                      a003_persona.ind_nombre1 LIKE '%$busqueda[value]%' OR 
                      a003_persona.ind_nombre2 LIKE '%$busqueda[value]%' OR 
                      a003_persona.ind_apellido1 LIKE '%$busqueda[value]%' OR 
                      a003_persona.ind_apellido2 LIKE '%$busqueda[value]%'
                    )
            ";
        }
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('ind_usuario','ind_nombre1','ind_apellido1');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_seguridad_usuario';
        #construyo el listado de botones
        if (in_array('AP-08-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                            data-keyboard="false" data-backdrop="static" idUsuario="'.$clavePrimaria.'" title="Asignar dependencias"
                            descipcion="El Usuario a Modificado el Menu" titulo="Asignar dependencias al Usuario">
                        <i class="fa fa-exchange" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        $campos['boton']['Eliminar'] = false;

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);
    }
	
}
