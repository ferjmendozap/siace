<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class usuarioControlador extends Controlador
{
    private $atUsuarioModelo;

    public function __construct()
	{
		parent::__construct();
        #Session::metAcceso();
        $this->atUsuarioModelo=$this->metCargarModelo('usuario');
	}

	public function metIndex()
	{
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->atUsuarioModelo->metListarUsuarios());
        $this->atVista->metRenderizar('listado');
	}

    public function metCrear()
    {
        $complementosCss = array(
            'multi-select/multi-select555c',
        );
        $complementosJs = array(
            'multi-select/jquery.multi-select',
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

        $idUsuario=$this->metObtenerInt('idUsuario');
        $valido=$this->metObtenerInt('valido');
        if($valido==1){
            $excepcionInt = array('');
            $formInt=$this->metObtenerInt('form','int',$excepcionInt);
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus'] = '0';
            }

            if($validacion['ind_password1']!==$validacion['ind_password2']){
                $validacion['ind_password1']='error';
                $validacion['ind_password2']='error';
            }elseif(strlen($validacion['ind_password1']) <=4){
                $validacion['ind_password1']='error';
                $validacion['ind_password2']='error';
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            if($idUsuario===0){
                $validacion['status']='creacion';
                $id=$this->atUsuarioModelo->metRegistrarUsuario( $validacion['ind_usuario'],  $validacion['ind_password1'], $validacion['ind_template'] ,$validacion['num_estatus'],  $validacion['perfil'],$validacion['fk_rhb001_num_empleado']);
                $validacion['idUsuario']=$id;
                echo json_encode($validacion);
                exit;
            }else{
                $validacion['status']='modificacion';
                if(!isset($validacion['perfil'])){
                    $validacion['perfil']=null;
                }
                $this->atUsuarioModelo->metactualizarUsuario($idUsuario, $validacion['ind_usuario'],  $validacion['ind_password1'], $validacion['ind_template'] ,$validacion['num_estatus'],  $validacion['perfil']);
                $validacion['idUsuario']=$idUsuario;
                echo json_encode($validacion);
                exit;
            }
        }

        if($idUsuario!=0){
            $db=$this->atUsuarioModelo->metMostrarUsuario($idUsuario);
            $dbUsuarioPerfil=$this->atUsuarioModelo->metMostrarUsuarioPerfil($idUsuario);
            if($dbUsuarioPerfil) {
                for ($i = 0; $i < count($dbUsuarioPerfil); $i++) {
                    $dbPerfiles[] = $dbUsuarioPerfil[$i]['fk_a017_num_seguridad_perfil'];
                }
            }else{
                $dbPerfiles=null;
            }
            $this->atVista->assign('formDB',$db);
            $this->atVista->assign('formDBPerfiles',$dbPerfiles);
        }
        $this->atVista->assign('listaPerfil',$this->atUsuarioModelo->metListarPerfil());
        $this->atVista->assign('listaEmpleado',$this->atUsuarioModelo->metEmpleado());
        $this->atVista->assign('idUsuario',$idUsuario);
        $this->atVista->metRenderizar('crear','modales');
    }

    public function metEliminar()
    {
        $idUsuario = $this->metObtenerInt('idUsuario');
        $this->atUsuarioModelo->metEliminarUsuario($idUsuario);
        $arrayPerfil = array(
            'status' => 'OK',
            'idUsuario' => $idUsuario
        );
        echo json_encode($arrayPerfil);
    }

    public function metCambiarPass()
    {
        $valido=$this->metObtenerInt('valido');
        if($valido===1){
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }
            if($validacion['ind_passwordNueva1']!==$validacion['ind_passwordNueva2']){
                $validacion['ind_passwordNueva1']='error';
                $validacion['ind_passwordNueva2']='error';
            }elseif(strlen($validacion['ind_passwordNueva1']) <=3){
                $validacion['ind_passwordNueva1']='error';
                $validacion['ind_passwordNueva2']='error';
            }
            $usuario=$this->atUsuarioModelo->metMostrarUsuario(Session::metObtener('idUsuario'));

            if($usuario['ind_password']!==Hash::metObtenerHash($validacion['ind_passwordVieja'])){
                $validacion['ind_passwordVieja']='error';
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            $Registro=$this->atUsuarioModelo->metActualizarPass(Session::metObtener('idUsuario'),$validacion['ind_passwordNueva1']);
            if(!is_array($Registro)){
                $validacion['status']='modificado';
            }else{
                $validacion['status']='errorSql';
            }
            echo json_encode($validacion);
            exit;
        }
        $this->atVista->metRenderizar('cambiarPass','modales');
    }
	
}
