<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        08-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class logsControlador extends Controlador
{
    private $atLogsModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atLogsModelo = $this->metCargarModelo('logs');
    }

    public function metIndex()
    {
        $this->metRedireccionar();
    }

    public function metCodigoUnicoFormulario()
    {
        $ultimoRegistro= $this->atLogsModelo->metObtenerUltimoLogs(Session::metObtener('idUsuario'));
        $token=array(
            'tokenFormulario'=>Hash::metObtenerHash($ultimoRegistro['fec_log'].$ultimoRegistro['ind_ip']),
            'nombreToken'=>Hash::metObtenerHash('token')
        );
        Session::metCrear('tokenFormulario',Hash::metObtenerHash($ultimoRegistro['fec_log'].$ultimoRegistro['ind_ip']));
        echo json_encode($token);
    }

    public function metRegistrarLogs()
    {
        $this->atLogsModelo->metRegistrarLogs(
            Session::metObtener('idUsuario'),
            $this->metObtenerAlphaNumerico('tipo'),
            $this->metObtenerAlphaNumerico('descripcion'),
            Session::metObtener('ip')
        );
    }



}