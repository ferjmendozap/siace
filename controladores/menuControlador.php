<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        16-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class menuControlador extends Controlador
{
    private $atMenuModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atMenuModelo = $this->metCargarModelo('menu');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
    }

    public function metCrear()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);

        $idMenu = $this->metObtenerInt('idMenu');
        $valido = $this->metObtenerInt('valido');
        if ($valido == 1) {
            $formInt = $this->metObtenerInt('form', 'int');
            $formTxt = $this->metObtenerTexto('form', 'txt');
            $formAlphaNum = $this->metObtenerAlphaNumerico('form', 'alphaNum');
            foreach ($formInt as $tituloInt => $valorInt) {
                if (!empty($formInt[$tituloInt])) {
                    $validacion[$tituloInt] = $valorInt;
                } else {
                    if ($tituloInt != 'num_nivel') {
                        $validacion[$tituloInt] = 'error';
                    } else {
                        $validacion[$tituloInt] = '0';
                    }

                }
            }
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt])) {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    if ($tituloTxt != 'cod_padre' && $tituloTxt != 'ind_ruta') {
                        $validacion[$tituloTxt] = 'error';
                    } else {
                        $validacion[$tituloTxt] = false;
                    }

                }
            }
            foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if (!empty($formAlphaNum[$tituloAlphaNum])) {
                    $validacion[$tituloAlphaNum] = $valorAlphaNum;
                } else {
                    $validacion[$tituloAlphaNum] = 'error';
                }
            }

            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }
            if ($idMenu === 0) {
                $validacion['status'] = 'creacion';
                $id = $this->atMenuModelo->metCrearMenu(
                    $validacion['fk_a015_num_seguridad_aplicacion'], $validacion['ind_nombre'], $validacion['ind_descripcion'],
                    $validacion['cod_interno'], $validacion['cod_padre'], $validacion['num_nivel'], $validacion['ind_ruta'],
                    $validacion['ind_rol'], $validacion['ind_icono'], 1);
                $validacion['idMenu'] = $id;
                echo json_encode($validacion);
                exit;
            } else {
                $validacion['status'] = 'modificacion';
                if (!isset($validacion['num_estatus'])) {
                    $validacion['num_estatus'] = 0;
                }
                $this->atMenuModelo->metActualizarMenu($idMenu,
                    $validacion['fk_a015_num_seguridad_aplicacion'], $validacion['ind_nombre'], $validacion['ind_descripcion'],
                    $validacion['cod_interno'], $validacion['cod_padre'], $validacion['num_nivel'], $validacion['ind_ruta'],
                    $validacion['ind_rol'], $validacion['ind_icono'], $validacion['num_estatus']);
                $validacion['idMenu'] = $idMenu;
                echo json_encode($validacion);
                exit;
            }
        }
        if ($idMenu != 0) {
            $db = $this->atMenuModelo->metMostrarMenu($idMenu);
            $this->atVista->assign('formDB', $db);
        }
        $aplicacion = $this->atMenuModelo->metListarAplicacion();
        $this->atVista->assign('aplicacion', $aplicacion);
        $this->atVista->assign('idMenu', $idMenu);
        $this->atVista->metRenderizar('crear', 'modales');
    }

    public function metEliminar()
    {
        $idMenu = $this->metObtenerInt('idMenu');
        $this->atMenuModelo->metEliminarMenu($idMenu);
        $arrayMenu = array(
            'status' => 'OK',
            'idMenu' => $idMenu
        );

        echo json_encode($arrayMenu);
    }

    public function metIcono()
    {
        $this->atVista->metRenderizar('icono', 'modales');
    }

    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "
            SELECT 
                *,
                a027_seguridad_menu.num_estatus
            FROM 
              a027_seguridad_menu 
              INNER JOIN a015_seguridad_aplicacion ON a027_seguridad_menu.fk_a015_num_seguridad_aplicacion = a015_seguridad_aplicacion.pk_num_seguridad_aplicacion  
        ";
        if ($busqueda['value']) {
            #concateno la busqueda si existe, Nota: esto es obligatorio
            $sql .="
                WHERE
                    ( 
                      a027_seguridad_menu.ind_nombre LIKE '%$busqueda[value]%' OR 
                      a027_seguridad_menu.ind_descripcion LIKE '%$busqueda[value]%' OR 
                      a027_seguridad_menu.cod_interno LIKE '%$busqueda[value]%' OR
                      a027_seguridad_menu.cod_padre LIKE '%$busqueda[value]%'
                    )
            ";
        }
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('ind_nombre','ind_descripcion','cod_interno','cod_padre','num_nivel','ind_rol','ind_icono','cod_aplicacion','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_seguridad_menu';
        #construyo el listado de botones
        if (in_array('AP-02-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                            data-keyboard="false" data-backdrop="static" idMenu="'.$clavePrimaria.'" title="Editar"
                            descipcion="El Usuario a Modificado el Menu" titulo="Modificar Menu">
                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Editar'] = false;
        }
        if (in_array('AP-02-03-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idMenu="'.$clavePrimaria.'"  boton="si, Eliminar" title="Eliminar"
                            descipcion="El usuario a eliminado el Menu" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Menu!!">
                        <i class="md md-delete" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);
    }

}
