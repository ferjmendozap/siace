<?php

require_once 'datosCarga/ubicaciones.php';
require_once 'datosCarga/dataBasicaInicial.php';
require_once 'datosCarga/dataBasicaInicialMenu.php';
require_once 'datosCarga/dataBasicaInicialMiscelaneos.php';
require_once 'datosCarga/dataBasicaInicialParametros.php';
require_once 'datosCarga/dataNomina.php';
require_once 'datosCarga/dataPresupuestoPartidasPresupuestarias.php';
require_once 'datosCarga/dataContabilidadCuentasContables.php';
require_once 'datosCarga/dataCxP.php';

class scriptCargaControlador extends Controlador
{
    use ubicaciones;
    use dataBasicaInicial;
    use dataNomina;
    use dataBasicaInicialMenu;
    use dataBasicaInicialMiscelaneos;
    use dataBasicaInicialParametros;
    use dataPresupuestoPartidasPresupuestarias;
    use dataContabilidadCuentasContables;
    use dataCxP;
    private $atScriptCarga;

    public function __construct()
    {
        parent::__construct();
        $this->atScriptCarga = $this->metCargarModelo('scriptCarga');
    }

    public function metIndex()
    {
        echo "INICIANDO<br>";
        #Cargar las Ubicaciones
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE UBICACIONES<br>';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>PAIS<br>';
        $this->atScriptCarga->metCargarPais($this->metDataPais());
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>ESTADO<br>';
        $this->atScriptCarga->metCargarEstado($this->metDataEstado());
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>MUNICIPIO<br>';
        $this->atScriptCarga->metCargarMunicipio($this->metDataMunicipio());
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>CIUDAD<br>';
        $this->atScriptCarga->metCargarCiudad($this->metDataMunicipio());
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>PARROQUIA<br>';
        $this->atScriptCarga->metCargarParroquia($this->metDataParroquia());
        echo '&nbsp;&nbsp;&nbsp;->>FIN CARGA DE UBICACIONES<br><br>';
        #Cargar dataInicial
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DATA BASICA<br>';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>PERSONA<br>';
        $this->atScriptCarga->metCargarPersona($this->metDataPersona());
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>EMPLEADO<br>';
        $this->atScriptCarga->metCargarEmpleado($this->metDataEmpleado());
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>APLICACION<br>';
        $this->atScriptCarga->metCargarAplicacion($this->metDataAplicacion());
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>PERFIL<br>';
        $this->atScriptCarga->metCargarPerfil($this->metDataPerfil());
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>MENU<br>';
        $this->atScriptCarga->metCargarMenu($this->metDataMenu());
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>MISCELANEOS<br>';
        $this->atScriptCarga->metCargarMiscelaneos($this->metDataMiscelaneos());
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>PARAMETROS<br>';
        $this->atScriptCarga->metCargarParametros($this->metDataParametros());
        echo '&nbsp;&nbsp;&nbsp;->>FIN CARGA DATA BASICA<br><br>';
        ### NOMINA ###
        #$this->metNomina();
        ### CONTABILIDAD ###
        #$this->metContabilidad();
        ### PRESUPUESTO ###
        #$this->metPresupuesto();
        ### CxP ###
        #$this->metCxP();

        echo "TERMINADO";
    }

    public function metNomina()
    {
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE NOMINA<br>';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>TIPO PROCESO<br>';
        $this->atScriptCarga->metNominaTipoProceso($this->metDataProcesos());
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>CONCEPTO<br>';
        $this->atScriptCarga->metNominaConceptos($this->metDataConceptos());
        echo '&nbsp;&nbsp;&nbsp;->>FIN CARGA DE NOMINA<br><br>';
    }

    public function metPresupuesto()
    {
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE PRESUPUESTO<br>';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>PARTIDAS PRESUPUESTARIAS<br>';
        $this->atScriptCarga->metPresupuestoPartidasPresupuestarias($this->metDataPartidasPreupuestarias());
        echo '&nbsp;&nbsp;&nbsp;->>FIN CARGA DE PRESUPUESTO<br><br>';
    }

    public function metContabilidad()
    {
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE CONTABILIDAD<br>';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>CUENTAS CONTABLES<br>';
        $this->atScriptCarga->metCuentasContables($this->metCuentasContables());
        echo '&nbsp;&nbsp;&nbsp;->>FIN CARGA DE CONTABILIDAD<br><br>';
    }

    public function metCxP()
    {
        echo 'INICIO CARGA DE MAESTROS MODULO CxP<br><br>';
        echo '-----Carga Impuesto-----<br><br>';
        $this->atScriptCarga->metCxPImpuesto($this->metDataImpuestos());
        echo '-----Carga Servicios-----<br><br>';
        $this->atScriptCarga->metCxPTipoServicios($this->metDataServicios());
        echo '-----Carga Documentos-----<br><br>';
        $this->atScriptCarga->metCxPDocumentos($this->metDataDocumetos());
        echo '-----Carga Clasificacion de Gatos-----<br><br>';
        $this->atScriptCarga->metCxPClasificacion($this->metDataClasificacionGasto());
        echo '-----Carga Concepto de Gatos-----<br><br>';
        $this->atScriptCarga->metCxPConcepto($this->metDataConceptoGasto());
        echo '-----Carga Cuentas Contables-----<br><br>';
        $this->atScriptCarga->metCxPCuentaBancaria($this->metDataCuentasBancarias());
        echo '-----Carga Tipo Transacciones Bancarias-----<br><br>';
        $this->atScriptCarga->metCxPtipoTransaccion($this->metDataTipoTransaccion());
        echo 'FIN CARGA DE CxP<br><br>';
    }

}
