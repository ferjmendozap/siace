<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        17-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class municipioControlador extends Controlador
{
    private $atMunicipioModelo;
    public function __construct()
	{
		parent::__construct();
        Session::metAcceso();
        $this->atMunicipioModelo=$this->metCargarModelo('municipio');
	}

	public function metIndex()
	{
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
	}

    public function metCrearModificar()
    {
        $js[] = 'Aplicacion/appFunciones';
        $complementosCss = array(
            'select2/select201ef',
        );
        $complementosJs = array(
            'select2/select2.min',
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $idMunicipio=$this->metObtenerInt('idMunicipio');
        $valido=$this->metObtenerInt('valido');
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('num_estatus');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
            if($alphaNum!=null && $ind==null){
                $validacion=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $validacion=$ind;
            }else{
                $validacion=array_merge($alphaNum,$ind);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            }

            if($idMunicipio==0){
                $id=$this->atMunicipioModelo->metCrearMunicipio($validacion['ind_municipio'],$validacion['fk_a009_num_estado'],$validacion['num_estatus']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atMunicipioModelo->metModificarMunicipio($validacion['ind_municipio'],$validacion['fk_a009_num_estado'],$validacion['num_estatus'],$idMunicipio);
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idMunicipio']=$id;

            echo json_encode($validacion);
            exit;

        }

        if($idMunicipio!=0){
            $db=$this->atMunicipioModelo->metMostrarMunicipio($idMunicipio);
            $this->atVista->assign('formDB',$db);
        }

        $this->atVista->assign('idMunicipio',$idMunicipio);
        $this->atVista->assign('listadoPais',$this->atMunicipioModelo->metListarPais(1));
        $this->atVista->metRenderizar('crear','modales');
    }

    public function metJsonMunicipio()
    {
        $idEstado=$this->metObtenerInt('idEstado');
        $estado = $this->atMunicipioModelo->metJsonMunicipio($idEstado);
        echo json_encode($estado);
        exit;
    }

    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "
                    SELECT
                      *,
                      a011_municipio.num_estatus
                    FROM
                      a011_municipio
                    INNER JOIN a009_estado ON a009_estado.pk_num_estado = a011_municipio.fk_a009_num_estado
                    INNER JOIN a008_pais ON a008_pais.pk_num_pais = a009_estado.fk_a008_num_pais
                 ";
        if ($busqueda['value']) {
            #concateno la busqueda si existe, Nota: esto es obligatorio
            $sql .="
                WHERE
                    (
                      ind_pais LIKE '%$busqueda[value]%' OR
                      ind_estado LIKE '%$busqueda[value]%' OR
                      ind_municipio LIKE '%$busqueda[value]%'
                    )
            ";
        }
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('ind_pais','ind_estado','ind_municipio','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_municipio';
        #construyo el listado de botones
        if (in_array('AP-06-03-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                            data-keyboard="false" data-backdrop="static" idMunicipio="'.$clavePrimaria.'" title="Editar"
                            descipcion="El Usuario ha Modificado un Municipio" titulo="<i class=\'md md-map\'></i> Modificar el Municipio">
                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                    </button>

                ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);
    }

}
