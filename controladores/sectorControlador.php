<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        17-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class sectorControlador extends Controlador

{
    private $atSectormodelo;
    private $atParroquiamodelo;
    public function __construct()
	{
		parent::__construct();
        Session::metAcceso();
        $this->atSectormodelo=$this->metCargarModelo('sector');
	}

	public function metIndex()
	{
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
	}

    public function metCrearModificar()
    {
        $js[] = 'Aplicacion/appFunciones';
        $complementosCss = array(
            'select2/select201ef',
        );
        $complementosJs = array(
            'select2/select2.min',
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $idSector=$this->metObtenerInt('idSector');
        $valido=$this->metObtenerInt('valido');
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('num_estatus');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
            if($alphaNum!=null && $ind==null){
                $validacion=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $validacion=$ind;
            }else{
                $validacion=array_merge($alphaNum,$ind);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            }

            if($idSector==0){
                $id=$this->atSectormodelo->metCrearSector($validacion['ind_sector'],$validacion['fk_a012_num_parroquia'],$validacion['num_estatus']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atSectormodelo->metModificarSector($validacion['ind_sector'],$validacion['fk_a012_num_parroquia'],$validacion['num_estatus'],$idSector);
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idSector']=$id;

            echo json_encode($validacion);
            exit;

        }

        if($idSector!=0){
            $db=$this->atSectormodelo->metMostrarSector($idSector);
            $this->atVista->assign('formDB',$db);
        }

        $this->atVista->assign('idSector',$idSector);
        $this->atVista->assign('listadoPais',$this->atSectormodelo->atParroquiamodelo->metListarPais());
        $this->atVista->metRenderizar('crear','modales');
    }

    public function metJsonParroquia()
    {
        $idMunicipio=$this->metObtenerInt('idMunicipio');
        $parroquia = $this->atSectormodelo->metJsonParroquia($idMunicipio);
        echo json_encode($parroquia);
        exit;
    }

    public function metJsonDataTabla()


    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "
                    SELECT
                       a013_sector.*,
                       a013_sector.num_estatus,
                       a012_parroquia.ind_parroquia
                    FROM
                        a013_sector
                    INNER JOIN a012_parroquia ON a012_parroquia.pk_num_parroquia = a013_sector.fk_a012_num_parroquia
                 ";
        if ($busqueda['value']) {
            #concateno la busqueda si existe, Nota: esto es obligatorio
            $sql .="
                WHERE
                    (
                      ind_sector LIKE '%$busqueda[value]%' OR
                      ind_parroquia LIKE '%$busqueda[value]%'
                    )
            ";
        }
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('ind_sector','ind_parroquia','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_sector';
        #construyo el listado de botones
        if (in_array('AP-06-06-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                            data-keyboard="false" data-backdrop="static" idSector="'.$clavePrimaria.'" title="Editar"
                            descipcion="El Usuario a Modificado un Sector" titulo="<i class=\'md md-map\'></i> Modificar el Sector">
                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                    </button>

                ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);
    }
}
