<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class perfilControlador extends Controlador
{
    private $atPerfilModelo;
    public function __construct()
	{
		parent::__construct();
        Session::metAcceso();
        $this->atPerfilModelo=$this->metCargarModelo('perfil');
	}

	public function metIndex()
	{
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->atPerfilModelo->metListarPerfil());
        $this->atVista->metRenderizar('listado');
	}

    public function metCrear()
    {
        $complementosCss = array(
            'jquery-ui/jquery-ui-theme5e0a',
            'SelectMultipleTabla/css/ui.multiselect',
        );
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'SelectMultipleTabla/js/plugins/localisation/jquery.localisation',
            'SelectMultipleTabla/js/plugins/tmpl/jquery.tmpl.1.1.1',
            'SelectMultipleTabla/js/plugins/blockUI/jquery.blockUI',
            'SelectMultipleTabla/js/ui.multiselect',
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);



        $idPerfil=$this->metObtenerInt('idPerfil');
        $valido=$this->metObtenerInt('valido');
        if($valido==1){
            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            if(!isset($validacion['menu'])){
                $validacion['menu']=null;
            }
            if($idPerfil===0){
                $validacion['status']='creacion';
                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }
                $id=$this->atPerfilModelo->metCrearPerfil($validacion['ind_nombre_perfil'], $validacion['ind_descripcion'], $validacion['num_estatus'], $validacion['menu']);
                $validacion['idPerfil']=$id;
                echo json_encode($validacion);
                exit;
            }else{
                $validacion['status']='modificacion';
                $this->atPerfilModelo->metModificarPerfil($idPerfil,$validacion['ind_nombre_perfil'], $validacion['ind_descripcion'], $validacion['num_estatus'], $validacion['menu']);
                $validacion['idPerfil']=$idPerfil;
                echo json_encode($validacion);
                exit;
            }
        }
        if($idPerfil!=0){
            $db=$this->atPerfilModelo->metMostrarPerfil($idPerfil);
            $dbMenu=$this->atPerfilModelo->metMostrarPerfilMenu($idPerfil);
            if($dbMenu) {
                for ($i = 0; $i < count($dbMenu); $i++) {
                    $dbMenus[] = $dbMenu[$i]['fk_a027_num_seguridad_menu'];
                }
            }else{
                $dbMenus=null;
            }
            $this->atVista->assign('formDB',$db);
            $this->atVista->assign('formDBMenu',$dbMenus);
        }
        $this->atVista->assign('listaMenu',$this->atPerfilModelo->metListarMenu());
        $this->atVista->assign('idPerfil',$idPerfil);
        $this->atVista->metRenderizar('crear','modales');
    }

    public function metEliminar()
    {
        $idPerfil = $this->metObtenerInt('idPerfil');
        $this->atPerfilModelo->metEliminarPerfil($idPerfil);
        $arrayPerfil = array(
            'status' => 'OK',
            'idPerfil' => $idPerfil
        );
        echo json_encode($arrayPerfil);
    }
	
}
