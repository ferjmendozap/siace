<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Activos Fijos
 * PROCESO: Cargar selects dependientes
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Edgar Bol�var                              |ebolivar@contradeltamacuro.gob.ve   |0424-9201982                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |01-12-2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'Select.php';

class selectControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
    }

    /**
     * M�todo index.
     *
     * @return Response
     */
    public function metIndex()
    {
    }

    /**
     * Dependencias.
     *
     * @return String
     */
    public function metA004_dependencia()
    {
        echo '<option value="">&nbsp;</option>';
        echo Select::lista('a004_dependencia','pk_num_dependencia','ind_dependencia','',0,['fk_a001_num_organismo'=>$_POST['fk_a001_num_organismo']]);
    }

    /**
     * Dependencias.
     *
     * @return String
     */
    public function metA023_centro_costo()
    {
        echo '<option value="">&nbsp;</option>';
        echo Select::lista('a023_centro_costo','pk_num_centro_costo','ind_descripcion_centro_costo','',0,['fk_a004_num_dependencia'=>$_POST['fk_a004_num_dependencia']]);
    }
}
