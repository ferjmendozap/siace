<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        13-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class aplicacionControlador extends Controlador
{
    private $atAplicacionesModelo;
    public function __construct()
	{
		parent::__construct();
        Session::metAcceso();
        $this->atAplicacionesModelo=$this->metCargarModelo('aplicacion');
	}

	public function metIndex()
	{
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->atAplicacionesModelo->metListarAplicacion());
        $this->atVista->metRenderizar('listado');
	}

    public function metCrear()
    {
        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

        $idAplicacion=$this->metObtenerInt('idAplicacion');
        $valido=$this->metObtenerInt('valido');
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('num_estatus');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
            if($alphaNum!=null && $ind==null){
                $validacion=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $validacion=$ind;
            }else{
                $validacion=array_merge($alphaNum,$ind);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            }

            if($idAplicacion==0){
                $id=$this->atAplicacionesModelo->metCrearAplicacion($validacion['cod_aplicacion'],$validacion['fec_creacion'],$validacion['ind_descripcion'],$validacion['ind_nombre_modulo'],$validacion['num_estatus']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atAplicacionesModelo->metModificarAplicacion($validacion['cod_aplicacion'],$validacion['fec_creacion'],$validacion['ind_descripcion'],$validacion['ind_nombre_modulo'],$validacion['num_estatus'],$idAplicacion);
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idAplicacion']=$id;

            echo json_encode($validacion);
            exit;

        }
        if($idAplicacion!=0){
            $db=$this->atAplicacionesModelo->metMostrarAplicacion($idAplicacion);
            $this->atVista->assign('formDB',$db);
        }
        $this->atVista->assign('idAplicacion',$idAplicacion);
        $this->atVista->metRenderizar('crear','modales');
    }
}
