<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        08-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
#Controlador Principal donde se aplican las reglas de negocio MVC y las validaciones de las datos por metodo POST
abstract class Controlador
{
    protected $atVista;
    protected $atSolicitud;

    public function __construct()
    {
        #Se Instancia el Request
        $this->atSolicitud = new Solicitud();
        #Se Instancia la Vista
        $this->atVista = new Vista(new Solicitud);
    }

    abstract public function metIndex();

    #Funcion que se encarga de validar en el sistema que los formularios sean enviados desde la aplicacion
    #y no via externa
    public function metValidarToken()
    {
        $token = Hash::metObtenerHash('token');
        $tokenHtml = $_POST[$token];
        $tokenSesion = Session::metObtener('tokenFormulario');

        if ($tokenHtml !== $tokenSesion) {
            header('location:' . BASE_URL . 'error/access/5050');
            exit;
        }
    }

    #Funcion que se encarga de Cargar los Modelos Del Sistema
    protected function metCargarModelo($modelo, $carpeta = false, $modulo = false)
    {
        #Nombre del Modelo
        $modelo = $modelo . 'Modelo';
        #Consulto en el Request si existe el modulo
        $moduloSolicitado = $this->atSolicitud->metObtenerModulo();
        #si el modulo no existe
        if ($modulo == 'APP') {
            $moduloSolicitado = false;
            $modulo = false;
        } elseif (!$modulo) {
            $modulo = $moduloSolicitado;
        }
        #si no existe el modulo y si no existe el modulo del request
        if (!$moduloSolicitado && !$modulo) {
            #la ruta va a ser la que se encuentra interna en la aplicacion
            $ruta = 'modelos';
            #si existe el modulo o el modulo del request
        } else {
            #la ruta va a ser donde se encuentre los modelos dentro del modulo
            $ruta = 'modulos' . DS . $modulo . DS . 'modelos';
        }
        #si existe una carpeta enviada por parametros
        if ($carpeta) {
            #a la ruta se le concatena dicha carpeta
            $rutaModelo = ROOT . $ruta . DS . $carpeta . DS . $modelo . '.php';
        } else {
            $rutaModelo = ROOT . $ruta . DS . $modelo . '.php';
        }

        if ($modelo . '.php' != 'parametrosModelo.php' && $modelo . '.php' != 'logsModelo.php' && $modelo . '.php' != 'loginModelo.php' && $modelo . '.php' != 'scriptCargaModelo.php') {
            Session::metAcceso();
        }

        #se empalma la ruta hacia el modelo a usar

        #se consulta si la ruta es legible
        if (is_readable($rutaModelo)) {
            #si es legible se incorpora dentro del documento
            require_once $rutaModelo;
            #se instancia el Modelo a usar
            $modelo = new $modelo;
            #se devuelve el modelo instanciado para ser usado desde el controlador.
            return $modelo;
            #si la ruta no es legible
        } else {
            #Se genera Un Mensaje de Error.
            header('location:' . BASE_URL . 'error/error/403');
            exit;
        }
    }

    #Funcion para Cargar las librerias en el controlador
    protected function metObtenerLibreria($libreria, $carpeta)
    {
        #se establece la ruta hacia la libreria.
        $rutaLibreria = ROOT . 'librerias' . DS . $carpeta . DS . $libreria . '.php';
        #se valida que la ruta sea legible
        if (is_readable($rutaLibreria)) {
            #se incluye el archivo al sistema
            require_once $rutaLibreria;
            #si la ruta no es legible
        } else {
            #se manda un mensaje de error.
            throw new Exception('error de libreria');
        }

    }

    #Funcion para redireccionar a un lugar dentro de la aplicacion
    protected function metRedireccionar($ruta = false)
    {
        if ($ruta) {
            header('location:' . BASE_URL . $ruta);
            exit;
        } else {
            header('location:' . BASE_URL);
            exit;
        }
    }

    #Funcion para validar los datos enviado por post hacia el controlador que sean solo numero
    protected function metObtenerInt($clave, $posicion = false, $soloNumero = false)
    {

        if ((isset($_POST[$clave]) && !empty($_POST[$clave]))) {
            $metodoRecepcion = $_POST;
        } elseif ((isset($_GET[$clave]) && !empty($_GET[$clave]))) {
            $metodoRecepcion = $_GET;
        }

        if ((isset($metodoRecepcion[$clave]) && !empty($metodoRecepcion[$clave]))) {
            if (is_array($metodoRecepcion[$clave])) {
                if ($posicion) {
                    if (isset($metodoRecepcion[$clave][$posicion]) && !empty($metodoRecepcion[$clave][$posicion])) {
                        foreach ($metodoRecepcion[$clave][$posicion] as $titulo => $valor) {
                            if (is_array($metodoRecepcion[$clave][$posicion][$titulo])) {
                                foreach ($metodoRecepcion[$clave][$posicion][$titulo] as $titulo1 => $valor1) {
                                    if($soloNumero){
                                        if(is_numeric($metodoRecepcion[$clave][$posicion][$titulo][$titulo1])){
                                            $metodoRecepcion[$clave][$posicion][$titulo][$titulo1] = $metodoRecepcion[$clave][$posicion][$titulo][$titulo1];
                                        }else{
                                            $metodoRecepcion[$clave][$posicion][$titulo][$titulo1] = 'datoInvalido';
                                        }
                                    }else{
                                        $metodoRecepcion[$clave][$posicion][$titulo][$titulo1] = (preg_replace('/[^0-9 .,]/', '', $metodoRecepcion[$clave][$posicion][$titulo][$titulo1]));
                                    }
                                }
                            } else {
                                if (isset($metodoRecepcion[$clave][$posicion][$titulo]) && !empty($metodoRecepcion[$clave][$posicion][$titulo])) {
                                    if($soloNumero){
                                        if(is_numeric($metodoRecepcion[$clave][$posicion][$titulo])){
                                            $metodoRecepcion[$clave][$posicion][$titulo] = $metodoRecepcion[$clave][$posicion][$titulo];
                                        }else{
                                            $metodoRecepcion[$clave][$posicion][$titulo] = 'datoInvalido';
                                        }
                                    }else{
                                        $metodoRecepcion[$clave][$posicion][$titulo] = (preg_replace('/[^0-9 .,]/', '', $metodoRecepcion[$clave][$posicion][$titulo]));
                                    }
                                }
                            }
                        }
                        return $metodoRecepcion[$clave][$posicion];
                    }
                } else {
                    if (isset($metodoRecepcion[$clave])) {
                        foreach ($metodoRecepcion[$clave] as $titulo => $valor) {
                            $metodoRecepcion[$clave][$titulo] = (preg_replace('/[^0-9 .,]/', '', $metodoRecepcion[$clave][$posicion][$titulo]));
                        }
                        return $metodoRecepcion[$clave];
                    }
                }
            } else {
                $metodoRecepcion[$clave] = filter_input(INPUT_POST, $clave, FILTER_VALIDATE_INT);
                return $metodoRecepcion[$clave];
            }
        }

        return 0;
    }

    #Funcion para validar los datos enviados por post hacia el controlador que sean texto, numero y caracteres especiales.
    #para evitar las inyecciones mediante sql al servidor.
    protected function metObtenerTexto($clave, $posicion = false)
    {
        if (!is_array($clave) && !$posicion && count($_POST[$clave]) === 1) {
            if (isset($_POST[$clave]) && !empty($_POST[$clave])) {
                $_POST[$clave] = (string)htmlspecialchars(preg_replace('/\d[^A-Za-z0-9 ñÑáéíóúÁÉÍÓÚ¿?!¡;:,*+%$#()&._-]/i', '', $_POST[$clave]));
                return $_POST[$clave];
            }
        } else {
            if ($posicion) {
                if (isset($_POST[$clave][$posicion])) {
                    foreach ($_POST[$clave][$posicion] as $titulo => $valor) {
                        if (is_array($_POST[$clave][$posicion][$titulo])) {
                            return $_POST[$clave][$posicion];
                        } else {
                            $_POST[$clave][$posicion][$titulo] = (string)htmlspecialchars(preg_replace('/\d[^A-Za-z0-9 ñÑáéíóúÁÉÍÓÚ¿?!¡;:,*+%$#()&._-]/i', '', $_POST[$clave][$posicion][$titulo]));
                        }
                    }
                    return $_POST[$clave][$posicion];
                }
            } else {
                if (isset($_POST[$clave])) {
                    foreach ($_POST[$clave] as $titulo => $valor) {
                        $_POST[$clave][$titulo] = (string)htmlspecialchars(preg_replace('/\d[^A-Za-z0-9 ñÑáéíóúÁÉÍÓÚ¿?!¡;:,*+%$#()&._-]/i', '', $_POST[$clave][$titulo]));
                    }
                    return $_POST[$clave];
                }
            }
        }
    }

    #Funcion para validar los datos enviados por post hacia el controlador que sea solo Alpha-Numerico
    protected function metObtenerAlphaNumerico($clave, $posicion = false)
    {
        if ((isset($_POST[$clave]) && !empty($_POST[$clave]))) {
            if (is_array($_POST[$clave])) {
                if ($posicion) {
                    if (isset($_POST[$clave][$posicion]) && !empty($_POST[$clave][$posicion])) {
                        foreach ($_POST[$clave][$posicion] as $titulo => $valor) {
                            if (is_array($_POST[$clave][$posicion][$titulo])) {
                                foreach ($_POST[$clave][$posicion][$titulo] as $titulo1 => $valor1) {
                                    $_POST[$clave][$posicion][$titulo][$titulo1] = (string)htmlspecialchars(preg_replace('/[^A-Za-z0-9 ñÑáéíóúÁÉÍÓÚ]/i', '', $_POST[$clave][$posicion][$titulo][$titulo1]));
                                }
                            } else {
                                if (isset($_POST[$clave][$posicion][$titulo]) && !empty($_POST[$clave][$posicion][$titulo])) {
                                    $_POST[$clave][$posicion][$titulo] = (string)htmlspecialchars(preg_replace('/[^A-Za-z0-9 ñÑáéíóúÁÉÍÓÚ]/i', '', $_POST[$clave][$posicion][$titulo]));
                                }
                            }
                        }
                        return $_POST[$clave][$posicion];
                    }
                } else {
                    if (isset($_POST[$clave])) {
                        foreach ($_POST[$clave] as $titulo => $valor) {
                            $_POST[$clave][$titulo] = (string)htmlspecialchars(preg_replace('/[^A-Za-z0-9 ñÑáéíóúÁÉÍÓÚ]/i', '', $_POST[$clave][$titulo]));
                        }
                        return $_POST[$clave];
                    }
                }
            } else {
                $_POST[$clave] = (string)htmlspecialchars(preg_replace('/[^A-Za-z0-9 ñÑáéíóúÁÉÍÓÚ]/i', '', $_POST[$clave]));
                return $_POST[$clave];
            }
        }
    }

    #Funcion para obtener Formulas.
    protected function metObtenerFormulas($clave, $posicion = false)
    {
        if (!is_array($clave) && !$posicion && count($_POST[$clave]) === 1) {
            if (isset($_POST[$clave]) && !empty($_POST[$clave])) {
                $_POST[$clave] = $_POST[$clave];
                return $_POST[$clave];
            }
        } else {
            if ($posicion) {
                if (isset($_POST[$clave][$posicion])) {
                    foreach ($_POST[$clave][$posicion] as $titulo => $valor) {
                        $_POST[$clave][$posicion][$titulo] = $_POST[$clave][$posicion][$titulo];
                    }
                    return $_POST[$clave][$posicion];
                }
            } else {
                if (isset($_POST[$clave])) {
                    foreach ($_POST[$clave] as $titulo => $valor) {
                        $_POST[$clave][$titulo] = $_POST[$clave][$titulo];
                    }
                    return $_POST[$clave];
                }
            }
        }
    }

    #Funcion Para Validar los datos de un array enviado desde el formulario.
    # $form = nombre del arreglo en el formulario
    # $posicionArreglo = nombre de la posicion del arreglo los nombres son int=> Numero, txt=> texto, alphaNum=> AlphaNumerico
    # $exceccion = array de excecciones.
    protected function metValidarFormArrayDatos($form, $posicionArreglo, $exceccion = array(),$condicionUnica = false)
    {
        if ($posicionArreglo == 'int') {
            $formArray = $this->metObtenerInt($form, $posicionArreglo,$condicionUnica);
            $valorExceccion = "0";
        } elseif ($posicionArreglo == 'txt') {
            $formArray = $this->metObtenerTexto($form, $posicionArreglo);
            $valorExceccion = false;
        } elseif ($posicionArreglo == 'alphaNum') {
            $formArray = $this->metObtenerAlphaNumerico($form, $posicionArreglo);
            $valorExceccion = false;
        } elseif ($posicionArreglo == 'formula') {
            $formArray = $this->metObtenerFormulas($form, $posicionArreglo);
            $valorExceccion = false;
        }
        if ($formArray) {
            foreach ($formArray as $titulo => $valor) {
                if (is_array($formArray[$titulo])) {
                    foreach ($formArray[$titulo] as $tituloArray => $valorArray) {
                        if (!empty($formArray[$titulo][$tituloArray]) && $formArray[$titulo][$tituloArray] != '') {
                            $validacion[$titulo][$tituloArray] = $valorArray;
                        } else {
                            if (is_int($tituloArray)) {
                                if (!in_array($titulo, $exceccion)) {
                                    $validacion[$titulo][$tituloArray] = 'error';
                                } else {
                                    $validacion[$titulo][$tituloArray] = $valorExceccion;
                                }
                            } elseif (!in_array($tituloArray, $exceccion)) {
                                $validacion[$titulo][$tituloArray] = 'error';
                            } else {
                                $validacion[$titulo][$tituloArray] = $valorExceccion;
                            }
                        }
                    }
                } else {
                    if (!empty($formArray[$titulo]) && $formArray[$titulo] != '') {
                        $validacion[$titulo] = $valor;
                    } else {
                        if (!in_array($titulo, $exceccion)) {
                            $validacion[$titulo] = 'error';
                        } else {
                            $validacion[$titulo] = $valorExceccion;
                        }
                    }
                }
            }
            if (isset($validacion)) {
                return $validacion;
            }
        }
    }

    /**
     * Metodo para generar el json que se le enviara como respuesta del post de la datatabla
     *
     * #Este Parametro contiene el sql de la consulta de la datatabla
     * @param $sql
     * #Aqui esta el listado que voy a mostrar a la hora de retornar la respuesta
     * @param $listado
     * #Clave primaria requerida Para los botones.
     * @param $clavePrimaria
     * #Atributos extras para los botones en caso de ser requeridos.
     *      Nota: Es Obligatorio Mandarlo en forma de array.
     * @param $datosExtraBotones
     * #Atributos extras para los flag en caso de ser requeridos.
     *      Nota: Es Obligatorio Mandarlo en forma de array.
     * @param $datosExtraBotones
     */
    protected function metDataTabla($sql,$listado,$clavePrimaria,$datosExtraBotones = false,$flagSistema = false)
    {
        #Instancio la clase principal del modelo
        $modelo = new Modelo();
        #Accedo a la base de datos desde el controlador
        $db = $modelo->metAccesoControladorDB();
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        #Parametros enviados desde el datatable
        $pagina = $this->metObtenerFormulas('draw');
        $limiteTabla = $this->metObtenerFormulas('length');
        $inicio = $this->metObtenerFormulas('start');
        $columnas = $this->metObtenerFormulas('columns');
        $orden = $this->metObtenerFormulas('order');
        $busqueda = $this->metObtenerFormulas('search');
        $busqueda = $busqueda['value'];
        $tituloColumna = $columnas[$orden[0]['column']]['data'];
        $ordenColumna = $orden[0]['dir'];
        if ($inicio == 0) {
            $inicio = '0';
        }
        if ($pagina > 1) {
            $inicio = ($inicio / $limiteTabla) + 1;
        }
        if ($inicio != 0) {
            $primerResultado = $limiteTabla * ($inicio - 1);
        } else {
            $primerResultado = 0;
        }

        #concateno el orden de la columna
        $sql .="ORDER BY $tituloColumna $ordenColumna ";
        $resultado = $db->prepare($sql);
        $resultado->execute();
        $numeroTotalRegistros = $resultado->rowCount();
        $arrayResultado['recordsTotal'] = $numeroTotalRegistros;
        $arrayResultado['recordsFiltered'] = $numeroTotalRegistros;
        $arrayResultado['draw'] = $pagina;
        $arrayResultado['length'] = $limiteTabla;
        #concateno el Limite de la consulta
        $sql .= "LIMIT $primerResultado, $limiteTabla ";
        $resultados2 = $db->prepare($sql);
        $resultados2->execute();
        $detalle = array();
        foreach ($resultados2->fetchAll(PDO::FETCH_ASSOC) AS $i) {
            if(isset($i['num_estatus'])){
                if($i['num_estatus']==1){
                    $estado = 'md md-check';
                }else{
                    $estado = 'md md-not-interested';
                }
                $i['num_estatus'] = '<i class="'.$estado.'"></i>';
            }
            if(isset($i['ind_icono'])){
                $i['ind_icono'] = '<i class="'.$i['ind_icono'].'"></i>';
            }
            if($flagSistema){
                foreach ($flagSistema AS $titulo => $valor){
                    if($i[$valor]==1){
                        $i[$valor] = '<i class="md md-check"></i>';
                    }else{
                        $i[$valor] = '<i class="md md-not-interested"></i>';
                    }
                }
            }

            #recorro los datos
            foreach ($listado AS $titulo => $valor){
                if(!is_array($valor)){
                    $detalle[$valor]=$i[$valor];
                }else{
                    $detalle['acciones'] = '';
                    foreach ($listado[$titulo] AS $titulo2 => $valor2){
                        if($titulo == 'boton'){

                            if(is_array($valor2)){
                                $valorArreglo = str_replace("$clavePrimaria", $i[$clavePrimaria], $valor2[0]);
                                $valorEval = str_replace("#botonEval", $valorArreglo, $valor2[1]);

                                eval($valorEval);
                            }else{
                                #sustituyo el parametro del texto de la clave primaria en el boton
                                # por el valor extraido de la base de datos
                                $valor2 = str_replace("$clavePrimaria", $i[$clavePrimaria], $valor2);
                            }
                            if($datosExtraBotones){
                                foreach ($datosExtraBotones AS $datoExtraBoton){
                                    $valor2 = str_replace("$datoExtraBoton", $i[$datoExtraBoton], $valor2);
                                }
                            }
                            $detalle['acciones'] .= "$valor2 ";
                        }
                    }
                }
            }
            $arrayResultado['data'][]=$detalle;
        }
        if (!isset($arrayResultado['data'])) {
            $arrayResultado['data'] = array();
        }
        echo json_encode($arrayResultado);
    }


}
