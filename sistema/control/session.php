<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        08-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

#Se encarga de la seguridad de la sesion
class Session
{
    #Funcion para iniciar una nueva sesión o reanudar la existente
	public static function metInit()
	{
		@session_start();
	}

    #Funcion para Cerrar la Sesion
	public static function metCerrarSesion($clave = false)
	{
        #unset — Destruye una variable especificada
		if($clave){
            $clave = Hash::metObtenerHash($clave);
			if(is_array($clave)){
				for($i =0 ; $i < count($clave); $i++){
					if(isset($_SESSION[$clave[$i]])){
						unset($_SESSION[$clave[$i]]);
					}
				}
			}else{
				if(isset($_SESSION[$clave])){
                    unset($_SESSION[$clave]);
                }
			}
		}else{
            session_unset();
			session_destroy();
		}
	}

    #Funcion Para Crear Una Variable de Sesion

    public static function metNoAccesoSiLogueo()
    {
        if (Session::metObtener('codigoUnico')) {
            header('location:' . BASE_URL);
            exit;
        }
    }

    #Funcion Para Obtener una Variable de sesion

	public static function metObtener($clave)
	{
        $clave = Hash::metObtenerHash($clave);
		if(isset($_SESSION[$clave])){
			return $_SESSION[$clave];
		}
	}

    #Funcion Para no acceder si se encuentra logueado

    public static function metAcceso()
	{
		if (!Session::metObtener('codigoUnico')){
			header ('location:' . BASE_URL. 'login');
			exit;
		}
        Session::metTiempo();
	}

    #Funcion Para Restringir el acceso si no se encuentra logueado
    #lo manda a loguearce

    public static function metTiempo()
    {
        if (!Session::metObtener('tiempo') || !defined('SESSION_TIEMPO')) {
            throw new Exception('No se ha definido el tiempo de sesion');
        }

        if (SESSION_TIEMPO == 0) {
            return;
        }

        if (time() - Session::metObtener('tiempo') > ((SESSION_TIEMPO+0.6) * 60)) {

        } else {
            Session::metCrear('tiempo', time());
        }
    }

    #Funcion para obtener el acceso de usuario

    public static function metCrear($clave, $valor)
    {
        $clave = Hash::metObtenerHash($clave);
        if (!empty($clave)) {
            $_SESSION[$clave] = $valor;
        }
    }

    #Funcion para el acceso estricto

	public static function metObtenerNivel($level)
	{
        $role['admin'] = 3;
        $role['especial'] = 2;
        $role['usuario'] = 1;

		if(!array_key_exists($level, $role)){
			throw new Exception('Error de acceso');
		}else{
			return $role[$level];
		}

    }

    #Funcion para cerrar la session al estar sin inactividad.

    public static function metAccesoEstricto(array $level, $noAdmin = false)
    {
        if(!Session::metObtener('codigoUnico')){
            header('location:' . BASE_URL . 'error/access/5050');
            exit;
        }

        Session::metTiempo();

        if($noAdmin == false){
            if(Session::metObtener('level') == 'admin'){
                return;
            }
        }

        if(count($level)){
            if(in_array(Session::metObtener('level'), $level)){
                return;
            }
        }
        header('location:' . BASE_URL . 'error/access/5050');
    }

    public static function metObtenerIp()
    {
        if(getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown")){
            $ip=getenv("HTTP_CLIENT_IP");
        } elseif(getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown")){
            $ip=getenv("HTTP_X_FORWARDED_FOR");
        }elseif(getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"),"unknown")){
            $ip=getenv("REMOTE_ADDR");
        }elseif(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown")){
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return($ip);
    }

}