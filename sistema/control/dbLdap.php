<?php
/*****************************************************************************************************************************************
 * DEV          :CONTRALORIA DE ESTADOS
 * PROYECTO     :SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO       :APLICACIÓN
 * Descripción  :Permite la conexión a un servidor LDAP y extracción de datos del usuario al loguearse.
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |         0426-514.43.82         |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #3                      |        11-08-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class conexLdap{
    /**
     * Permite establecer la conexión
     * @return bool | identificador de la conexión
     */
    public function metLdapConexion(){
        $conex = ldap_connect(LDAP_HOST, LDAP_PORT) or die ("No ha sido posible conectarse al servidor del ldap.");
        if (!(ldap_set_option($conex, LDAP_OPT_PROTOCOL_VERSION, 3))) {
            return false;
        }else{
            return $conex;
        }
    }
    /**
     * Permite cerrar la conexión
     * @param $conex
     */
    function metLdapCerrar($conex){
        ldap_close($conex);
    }
    /**
     * Permite la validación del usuario que hace login
     * @param $usuario
     * @param $password
     * @param $conex
     * @return bool
     */
    function matLdapVerificaUsuario($usuario,$password,$conex){
        $retornar=false;
        if ($conex) {
            $fuenteData = "uid=".$usuario.",".LDAP_BASE_DN;
            if (@$result=ldap_bind($conex, $fuenteData, $password)){
                $retornar=true;
            }
        }
        return $retornar;
    }
    /**
     * permite extraer el número de cédula del usuario
     * @param $usuario
     * @param $conex
     * @param $campos
     * @return array
     */
    function metLdapBuscaDataUser($usuario,$conex,$campos){
        $retornar='';
        $nro=count($campos);
        if($result=ldap_search($conex,'uid='.$usuario.','.LDAP_BASE_DN,'uid='.$usuario,$campos,0,0)){
            $result=ldap_get_entries($conex,$result);
            for ($i=0; $i<$nro; $i++){
                $retornar[$i]=$result[0][$campos[$i]][0];
            }
        }
        return $retornar;
    }
}