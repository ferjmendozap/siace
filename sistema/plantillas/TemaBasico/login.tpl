<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es-VE">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title>SIACE</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="{$_Parametros.ruta_Complementos}jquery-ui/jquery-ui-theme5e0a.css" rel="stylesheet" />
    <link href="{$_Parametros.ruta_Complementos}bootstrap/bootstrap_v3.3.4.css" rel="stylesheet" />
    <link href="{$_Parametros.ruta_Fonts}font-awesome_v4.3.0.css" rel="stylesheet" />
    <link href="{$_Parametros.ruta_Css}login-v2/animate.min.css" rel="stylesheet" />
    <link href="{$_Parametros.ruta_Css}login-v2/style.min.css" rel="stylesheet" />
    <link href="{$_Parametros.ruta_Css}login-v2/style-responsive.min.css" rel="stylesheet" />
    <link href="{$_Parametros.ruta_Css}login-v2/theme/default.css" rel="stylesheet" id="theme" />
    <!-- ================== END BASE CSS STYLE ================== -->
</head>
<body>
{include file=$_contenido}
</body>
</html>
