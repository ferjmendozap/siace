<!doctype html>
<html>
    <head>
        <title>SIACE</title>
        <!-- BEGIN META -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="your,keywords">
        <meta name="description" content="Short explanation about this website">
        <!-- END META -->
        <!-- BEGIN STYLESHEETS -->
        <link type="text/css" rel="stylesheet" href="{$_Parametros.ruta_Complementos}bootstrap/bootstrap_v3.3.4.css" />
        <link type="text/css" rel="stylesheet" href="{$_Parametros.ruta_Css}MaterialSiace.css" />
        <link type="text/css" rel="stylesheet" href="{$_Parametros.ruta_Fonts}font-awesome_v4.3.0.css" />
        <link type="text/css" rel="stylesheet" href="{$_Parametros.ruta_Fonts}material-design-iconic-font.mine.css" />
        <!-- END STYLESHEETS -->
    </head>

    <body>
        {include file=$_contenido}
    </body>
</html>