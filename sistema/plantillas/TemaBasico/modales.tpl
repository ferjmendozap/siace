<!doctype html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        <title>SIACE</title>
        <script type="text/javascript" src="{$_Parametros.ruta_Js}materialSiace/core/cache/63d0445130d69b2868a8d28c93309746.js"></script>
        <script type="text/javascript" src="{$_Parametros.ruta_Js}materialSiace/core/demo/Demo.js"></script>
        {if isset($_Parametros.css_complemento) && count($_Parametros.css_complemento)}
            {foreach item=css from=$_Parametros.css_complemento}
                <link rel="stylesheet" href="{$css}" type="text/css">
            {/foreach}
        {/if}
        {if isset($_Parametros.js_complemento) && count($_Parametros.js_complemento)}
            {foreach item=js from=$_Parametros.js_complemento}
                <script type="text/javascript" src="{$js}"></script>
            {/foreach}
        {/if}
        {if isset($_Parametros.js) && count($_Parametros.js)}
            {foreach item=js from=$_Parametros.js}
                <script type="text/javascript" src="{$js}"></script>
            {/foreach}
        {/if}
        <script type="text/javascript">
            $(document).ready(function() {
                var url='{$_Parametros.url}logs/CodigoUnicoFormulario';
                $.post(url, { } ,function(dato){
                    $('#formAjax').append('<input type="hidden" value="'+dato["tokenFormulario"]+'" name="'+dato["nombreToken"]+'" />');
                },'json');
            });
        </script>
    </head>

    <body>
        <div class="row">
            <div class="col-lg-12">
                {include file=$_contenido}
            </div>
        </div>

    </body>
</html>