<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO:
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | DANIEL A. MUÑOZ V.   | d.munoz@contraloriamonagas.gob.ve    | 0412-8358676
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

#Se encarga de instanciar el controlador y metodo a usar.
class Respuesta
{
    #Funcion de arranque para Unificar Toda la Aplicacion MVC y la ejecuta.
    #Se usa la clase Request para obtener el modulo, controlador, metodo, argumentos y la ruta de las carpetas.
    public static function metRun(Solicitud $solicitud)
    {
        #Se Obtiene el Modulo del request y se almacena en la variable
        $modulo = $solicitud->metObtenerModulo();
        #Se Obtiene el Controlador del request y se almacena en la variable
        $controlador = $solicitud->metObtenerControlador().'Controlador';
        #Se Obtiene el Metodo del request y se almacena en la variable
        $metodo = 'met'.$solicitud->metObtenerMetodo();
        #Se Obtiene los Argumentos del request y se almacena en la variable
        $argumentos = $solicitud->metObtenerArgumentos();
        #Se Obtiene la ruta de las carpetas del Controlador del request y se almacena en la variable
        $rutaControlador='controladores'.DS.$solicitud->metObtenerRutaControlador();

        #Se valida si el modulo es true o false
        if($modulo){
            #si es true Se establece la ruta para llegar al controlador en el modulo
            $rutaControlador = ROOT.'modulos'.DS.$modulo.DS.$rutaControlador.$controlador.'.php';
        }else{
            #si es false Se establece la ruta para llegar al controlador de la aplicacion
            $rutaControlador = ROOT.'controladores'.DS.$controlador.'.php';
        }

        #is_readable — Indica si un fichero existe y es legible.
        #Se verifica que la ruta del controlador exista
        if(is_readable($rutaControlador)){
            #si existe la ruta del Controlador se incluye en el archivo
            require_once $rutaControlador;

            #una vez incluida la ruta del controlador este mismo se instancia
            $controlador = new $controlador;

            #is_callable — Verificar que los contenidos de una variable puedan ser llamados como una función

            #se verifica que el metodo se pueda llamar dentro de la clase
            if(!is_callable(array($controlador, $metodo))){
                #si el metodo se puede llamar se asigna el metodo del request a la variable metodo
                $metodo = 'metIndex';
            }
            #call_user_func_array — Llama a la llamada de retorno dada por el primer parámetro
            #se verifica que existan los Argumentos
            if(isset($argumentos)){
                #se hace el llamado del controlador, metodo y argumentos para ser utilizado en el sistema.
                call_user_func_array(array($controlador, $metodo), $argumentos);
            }else{
                #se hace el llamado del controlador y metodo para ser utilizado en el sistema.
                call_user_func(array($controlador, $metodo));
            }

          
        #Si la Ruta del Controlador no existe
        }else{
            #Se genera Un Mensaje de Error.
        header ('location:' . BASE_URL. 'error/error/404');
            exit;
        }
    }

}