<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO:
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | DANIEL A. MUÑOZ V.   | d.munoz@contraloriamonagas.gob.ve    | 0412-8358676
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
#Se encarga de utilizar la url como enrutado para saber
#cual es el controlador y metodo a usar dentro de la aplicacion
class Solicitud
{
    private $atModulo;
    private $atControlador;
    private $atMetodo;
    private $atArgumentos;
    private $atRutaControlador;
	
	public function __construct()
	{
        #Comprueba Si existe una Url
		if(isset($_GET['url'])){
            #filter_input — Toma una variable externa concreta por nombre y opcionalmente la filtra
            $url = filter_input(INPUT_GET, 'url', FILTER_SANITIZE_URL);
            #explode — Divide un string en varios string
            $url = explode('/', $url);
            #array_filter — Filtra elementos de un array usando una función de devolución de llamada
            $url = array_filter($url);

            #array_shift() Quita el primer valor del array y lo devuelve, acortando el array un elemento y corriendo
            #el array hacia abajo. Todas la claves del array numéricas serán modificadas para que empiece contando
            #desde cero mientras que los arrays con claves literales no serán modificados.

            #Asigno la primera posicion del arreglo al Modulo
            $this->atModulo = array_shift($url);
            #Se Cuenta el arreglo y se ve si es igual a 0
            if(count($url)===0){
                #Si es Igual a 0 el Modulo se Convierte en el Controlador
                $this->atControlador=$this->atModulo;
                #y el Modulo cambia su valor a Nulo
                $this->atModulo=false;
            #Si el arreglo es Igual a 1 y no tiene el prefijo Controlador
            }elseif(count($url)===1 && (strpos($url[0],'CONTROL')===false)){
                #el Modulo Pasa a ser el Controlador
                $this->atControlador=$this->atModulo;
                #la Primera Posision del arreglo pasa a ser el Metodo a Usar
                $this->atMetodo=str_replace('MET','',array_shift($url));
                #el Modulo cambia su valor a Nulo
                $this->atModulo=false;
            } elseif (count($url) === 2 && (strpos($url[0], 'CONTROL') === false) && (strpos($url[1], 'CONTROL') === false)) {
                #el Modulo Pasa a ser el Controlador
                $this->atControlador = $this->atModulo;
                #la Primera Posision del arreglo pasa a ser el Metodo a Usar
                $this->atMetodo = str_replace('MET', '', array_shift($url));
                #el Modulo cambia su valor a Nulo
                $this->atModulo = false;

            }else{
                #Si el arreglo es Igual a 1 y tiene el prefijo Controlador
                if(count($url)===1 && (strpos($url[0],'CONTROL')===true)){
                    #la primera posicion del arreglo pasa a ser el Controlador y ya el sistema deduce
                    #que se esta Usando un Modulo del sistema
                    $this->atControlador=str_replace('CONTROL','',array_shift($url));
                #Si el arreglo es Igual a 2
                }elseif(count($url)===2 && (strpos($url[0],'CONTROL')===true)){
                    #La primera posicion pasa a ser el Controlador
                    $this->atControlador=str_replace('CONTROL','',array_shift($url));
                    #la siguiente posicion pasa a ser el Metodo
                    $this->atMetodo=str_replace('MET','',array_shift($url));
                #Si el arreglo es mayor a 2
                }else{
                    #Se pasa el arreglo a otra variable para asi no perder la cuenta cuando se van
                    #eliminando posisiones.
                    $contenidoUrl=$url;
                    #se usa el for para recorrer el arreglo. contando las posiciones del mismo
                    for($i=0;$i<count($contenidoUrl);$i++){
                        #Se valida si las posiciones del arreglo tienen el prefijo Controlador
                        if(strpos($contenidoUrl[$i],'CONTROL')!=false){
                            #Si se encuentra el Prefijo Controlador se almacena a la variable correspondiente
                            $this->atControlador=str_replace('CONTROL','',array_shift($url));
                        #Se valida si las posiciones del arreglo tienen el prefijo Metodo
                        }elseif(strpos($contenidoUrl[$i],'MET')!=false){
                            #Si se encuentra el Prefijo Metodo se almacena a la variable correspondiente
                            $this->atMetodo=str_replace('MET','',array_shift($url));
                            #Una vez que se encuentre el metodo se indica que las siguientes posiciones
                            #del arreglo seran contadas como argumentos
                            $argumentos=1;
                        }else{
                            #si no existe la variable argumentos en el recorrido del arreglo las posiciones seran contadas
                            #como la ruta donde se encuentra el controlador.
                            if(!isset($argumentos)){
                                $this->atRutaControlador.=array_shift($url).DS;
                            }
                        }
                    }
                    #al salir del arreglo las posiciones restantes en el seran consideradas como los argumentos.
                }
            }
            $this->atArgumentos = $url;
        }

        #se valida si no existe la ruta del controlador
        if(!$this->atRutaControlador){
            #si no existe el controlador, no tiene ruta por lo cual toma por defecto la ruta de la aplicacion
            $this->atRutaControlador=false;
        }

        #se valida si no existe el controlador
        if(!$this->atControlador){
            #si no existe, se almacena en el controlador el valor por defecto definido en los archivos de configuracion
            $this->atControlador = CONTROLADOR_DEFAULT;
        }

        #se valida si no existe el metodo
        if(!$this->atMetodo){
            #si no existe, se almacena en el metodo el valor por defecto definido en los archivos de configuracion
            $this->atMetodo = METODO_DEFAULT;
        }

        #se valida si no existe los Argumentos
        if(!$this->atArgumentos){
            #si no existe, se carga un arreglo vacio
            $this->atArgumentos = array();
        }
	}

    #Funcion para Obtener el Modulo
    public function metObtenerModulo()
    {
        return $this->atModulo;
    }

    #Funcion para Obtener el Controlador
    public function metObtenerControlador()
    {
        return $this->atControlador;
    }

    #Funcion para Obtener el Metodo
    public function metObtenerMetodo()
    {
        return $this->atMetodo;
    }

    #Funcion para Obtener los Argumentos
    public function metObtenerArgumentos()
    {
        return $this->atArgumentos;
    }

    #Funcion para Obtener la Ruta del Controlador
    public function metObtenerRutaControlador()
    {
        return $this->atRutaControlador;
    }
}