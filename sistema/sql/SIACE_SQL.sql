SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `SIACE` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci ;
USE `SIACE` ;

-- -----------------------------------------------------
-- Table `SIACE`.`a018_seguridad_usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a018_seguridad_usuario` (
  `pk_num_seguridad_usuario` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_usuario` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `ind_password` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_template` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_ip` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_seguridad_usuario`),
  INDEX `fk_a018_seguridad_usuario_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_seguridad_usuario_a018_seguridad_usuario`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 15
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a001_organismo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a001_organismo` (
  `pk_num_organismo` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id unico del organismo',
  `ind_descripcion_empresa` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica el nombre del organismo',
  `ind_numero_registro` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica el numero de registro mercantil del organismo',
  `ind_tomo_registro` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica el numero de tomo del organismo',
  `ind_logo` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'logo del organismo',
  `num_estatus` INT(1) NOT NULL COMMENT '1= activo 0= inactivo, estatus del organismo',
  `ind_tipo_organismo` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'I= Interno E= externo, indica el tipo del organismo',
  `ind_direccion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'direccion del organismo, calle, avenida, edificio, sector, otros',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'fk relacionado con el id del usuario logueado',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'fecha del ultimo registro modificado',
  `fk_a006_num_miscelaneo_detalle_carcater_social` INT(11) NULL DEFAULT NULL COMMENT 'fk relacionado con el id del tipo de caracter social',
  PRIMARY KEY (`pk_num_organismo`),
  INDEX `fk_a001_organismo_a018_seguridad_usuario_idx` USING BTREE (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_a001_organismo_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle_carcater_social` ASC),
  CONSTRAINT `fk_a001_organismo_a018_seguridad_usuario2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 21
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a008_pais`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a008_pais` (
  `pk_num_pais` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_pais` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pk_num_pais`))
ENGINE = InnoDB
AUTO_INCREMENT = 249
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a009_estado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a009_estado` (
  `pk_num_estado` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_estado` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a008_num_pais` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_estado`),
  INDEX `fk_a009_estado_a008_pais` (`fk_a008_num_pais` ASC),
  CONSTRAINT `fk_a009_estado_a008_pais`
    FOREIGN KEY (`fk_a008_num_pais`)
    REFERENCES `SIACE`.`a008_pais` (`pk_num_pais`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4163
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a010_ciudad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a010_ciudad` (
  `pk_num_ciudad` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_ciudad` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_cod_postal` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_capital` INT(11) NULL DEFAULT NULL,
  `fk_a009_num_estado` INT(11) NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pk_num_ciudad`),
  INDEX `fk_a009_num_estado` (`fk_a009_num_estado` ASC),
  CONSTRAINT `a010_ciudad_ibfk_1`
    FOREIGN KEY (`fk_a009_num_estado`)
    REFERENCES `SIACE`.`a009_estado` (`pk_num_estado`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a012_parroquia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a012_parroquia` (
  `pk_num_parroquia` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_parroquia` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a011_num_municipio` INT(11) NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pk_num_parroquia`),
  INDEX `fk_a012_parroquia_a011_municipio` (`fk_a011_num_municipio` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 1126
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a013_sector`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a013_sector` (
  `pk_num_sector` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_sector` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a012_num_parroquia` INT(11) NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_sector`),
  INDEX `fk_a013_sector_a012_parroquia` (`fk_a012_num_parroquia` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a013_sector_a012_parroquia`
    FOREIGN KEY (`fk_a012_num_parroquia`)
    REFERENCES `SIACE`.`a012_parroquia` (`pk_num_parroquia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a018_num_seguridad_usuario`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 17
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a003_persona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a003_persona` (
  `pk_num_persona` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_cedula_documento` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_documento_fiscal` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_nombre1` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_nombre2` VARCHAR(75) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_apellido1` VARCHAR(75) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_apellido2` VARCHAR(75) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a006_num_miscelaneo_detalle_sexo` INT(11) NULL DEFAULT NULL,
  `fk_a006_num_miscelaneo_detalle_nacionalidad` INT(11) NULL DEFAULT NULL,
  `fec_nacimiento` DATE NULL DEFAULT NULL,
  `ind_lugar_nacimiento` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a006_num_miscelaneo_detalle_edocivil` INT(11) NULL DEFAULT NULL,
  `fec_estado_civil` DATE NULL DEFAULT NULL,
  `ind_email` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_foto` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_tipo_persona` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NULL DEFAULT NULL,
  `fk_a006_num_miscelaneo_det_gruposangre` INT(11) NULL DEFAULT NULL,
  `fk_a006_num_miscelaneo_det_tipopersona` INT(11) NULL DEFAULT NULL,
  `fk_a010_num_ciudad` INT(11) NULL DEFAULT NULL,
  `fk_a013_num_sector` INT(11) NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_persona`),
  UNIQUE INDEX `ind_cedula_documento_UNIQUE` (`ind_cedula_documento` ASC),
  UNIQUE INDEX `ind_documento_fiscal_UNIQUE` (`ind_documento_fiscal` ASC),
  INDEX `IDX_9545B9721F89AE30` (`fk_a006_num_miscelaneo_detalle_edocivil` ASC),
  INDEX `fk_a003_persona_a006_miscelaneos_det1_idx` (`fk_a006_num_miscelaneo_det_gruposangre` ASC),
  INDEX `fk_a003_persona_a010_ciudad1_idx` (`fk_a010_num_ciudad` ASC),
  INDEX `fk_a003_persona_a013_sector1_idx` (`fk_a013_num_sector` ASC),
  INDEX `fk_a003_persona_a006_num_miscelaneo_detalle1_idx` USING BTREE (`fk_a006_num_miscelaneo_detalle_nacionalidad` ASC),
  INDEX `fk_a003_persona_a006_num_miscelaneo_detalle2_idx` USING BTREE (`fk_a006_num_miscelaneo_detalle_sexo` ASC),
  INDEX `fk_a003_persona_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a003_persona_a010_ciudad`
    FOREIGN KEY (`fk_a010_num_ciudad`)
    REFERENCES `SIACE`.`a010_ciudad` (`pk_num_ciudad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a003_persona_a013_sector`
    FOREIGN KEY (`fk_a013_num_sector`)
    REFERENCES `SIACE`.`a013_sector` (`pk_num_sector`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a003_persona_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 106
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a002_organismo_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a002_organismo_detalle` (
  `pk_num_organismodet` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id unico del detalle del organismo',
  `ind_cargo_representante` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica el cargo del representante legal del organismo',
  `ind_pagina_web` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica la pagina web del organismo',
  `ind_sujeto_control` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT '0' COMMENT '1= si 0= no, flag que indica si el organismo esta sujeto a control',
  `fk_a001_num_organismo` INT(11) NOT NULL COMMENT 'fk relacionado con el id del organismo',
  `fk_a003_num_persona_representante` INT(11) NOT NULL COMMENT 'fk relacionado con el id del representante legal del organismo',
  `fk_a010_num_ciudad` INT(11) NOT NULL COMMENT 'fk relacionado con el id de la ciudad que pertenece el organismo',
  PRIMARY KEY (`pk_num_organismodet`),
  INDEX `fk_a002_organismo_detalle_a001_organismo_idx` (`fk_a001_num_organismo` ASC),
  INDEX `fk_a002_organismo_detalle_a003_persona1_idx` (`fk_a003_num_persona_representante` ASC),
  INDEX `fk_a002_organismo_detalle_a010_ciudad1_idx` (`fk_a010_num_ciudad` ASC),
  CONSTRAINT `fk_a002_organismo_detalle_a001_organismo`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a002_organismo_detalle_a003_persona`
    FOREIGN KEY (`fk_a003_num_persona_representante`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a002_organismo_detalle_a010_ciudad`
    FOREIGN KEY (`fk_a010_num_ciudad`)
    REFERENCES `SIACE`.`a010_ciudad` (`pk_num_ciudad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 14
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a004_dependencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a004_dependencia` (
  `pk_num_dependencia` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_dependencia` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_codinterno` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_codpadre` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_nivel` INT(11) NOT NULL,
  `num_flag_controlfiscal` INT(1) NOT NULL,
  `num_flag_principal` INT(1) NOT NULL,
  `num_piso` INT(1) NULL DEFAULT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fk_a001_num_organismo` INT(6) NOT NULL,
  `fk_a003_num_persona_responsable` INT(11) NOT NULL,
  `num_flag_responsable` INT(1) NOT NULL,
  `txt_abreviacion` VARCHAR(75) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_dependencia`),
  INDEX `fk_a004_dependencia_a001_organismo` (`fk_a001_num_organismo` ASC),
  INDEX `fk_a004_dependencia_a003_persona1_idx` (`fk_a003_num_persona_responsable` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a004_a018_num_seguridad_usuario`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a004_dependencia_a001_organismo`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a004_dependencia_a003_persona`
    FOREIGN KEY (`fk_a003_num_persona_responsable`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 16
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a015_seguridad_aplicacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a015_seguridad_aplicacion` (
  `pk_num_seguridad_aplicacion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `cod_aplicacion` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_nombre_modulo` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_creacion` DATE NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  PRIMARY KEY (`pk_num_seguridad_aplicacion`),
  UNIQUE INDEX `cod_aplicacion` (`cod_aplicacion` ASC),
  INDEX `fk_a015_seguridad_aplicacion_a018_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a015_seguridad_aplicacion_a018_seguridad_usuario`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 21
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a005_miscelaneo_maestro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a005_miscelaneo_maestro` (
  `pk_num_miscelaneo_maestro` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_nombre_maestro` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a015_num_seguridad_aplicacion` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `cod_maestro` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_miscelaneo_maestro`),
  UNIQUE INDEX `cod_maestro_UNIQUE` (`cod_maestro` ASC),
  UNIQUE INDEX `ind_nombre_maestro_UNIQUE` (`ind_nombre_maestro` ASC),
  INDEX `fk_a005_miscelaneo_maestro_a015_seguridad_aplicacion1_idx` (`fk_a015_num_seguridad_aplicacion` ASC),
  INDEX `fk_a005_miscelaneo_maestro_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a005_miscelaneo_maestro_a015_seguridad_aplicacion`
    FOREIGN KEY (`fk_a015_num_seguridad_aplicacion`)
    REFERENCES `SIACE`.`a015_seguridad_aplicacion` (`pk_num_seguridad_aplicacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a005_miscelaneo_maestro_a018_seguridad_usuario`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 724
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a006_miscelaneo_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a006_miscelaneo_detalle` (
  `pk_num_miscelaneo_detalle` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_detalle` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_nombre_detalle` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a005_num_miscelaneo_maestro` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_miscelaneo_detalle`),
  UNIQUE INDEX `unico` (`ind_nombre_detalle` ASC, `fk_a005_num_miscelaneo_maestro` ASC),
  INDEX `fk_a006_miscelaneos_detalle_a005_miscelaneos_maestro` (`fk_a005_num_miscelaneo_maestro` ASC),
  CONSTRAINT `fk_a006_miscelaneos_detalle_a005_miscelaneo_maestro`
    FOREIGN KEY (`fk_a005_num_miscelaneo_maestro`)
    REFERENCES `SIACE`.`a005_miscelaneo_maestro` (`pk_num_miscelaneo_maestro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4637
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a007_persona_telefono`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a007_persona_telefono` (
  `pk_num_telefono` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_telefono` VARCHAR(13) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipo_telefono` INT(11) NULL DEFAULT '0',
  `fk_a003_num_persona` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_telefono`),
  INDEX `fk_a007_telefono_a003_persona` (`fk_a003_num_persona` ASC),
  CONSTRAINT `fk_a007_telefono_a003_persona`
    FOREIGN KEY (`fk_a003_num_persona`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 60
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a011_municipio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a011_municipio` (
  `pk_num_municipio` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_municipio` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a009_num_estado` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_municipio`),
  INDEX `fk_a011_municipio_a009_estado1_idx` (`fk_a009_num_estado` ASC),
  CONSTRAINT `fk_a011_municipio_a009_estado`
    FOREIGN KEY (`fk_a009_num_estado`)
    REFERENCES `SIACE`.`a009_estado` (`pk_num_estado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 337
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a014_ciudad_municipio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a014_ciudad_municipio` (
  `pk_num_ciudad_municipio` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a010_num_ciudad` INT(11) NOT NULL,
  `fk_a011_num_municipio` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_ciudad_municipio`),
  INDEX `fk_a010_num_ciudad` (`fk_a010_num_ciudad` ASC),
  INDEX `fk_a011_num_municipio` (`fk_a011_num_municipio` ASC),
  CONSTRAINT `a014_ciudad_municipio_ibfk_1`
    FOREIGN KEY (`fk_a010_num_ciudad`)
    REFERENCES `SIACE`.`a010_ciudad` (`pk_num_ciudad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `a014_ciudad_municipio_ibfk_2`
    FOREIGN KEY (`fk_a011_num_municipio`)
    REFERENCES `SIACE`.`a011_municipio` (`pk_num_municipio`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a017_seguridad_perfil`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a017_seguridad_perfil` (
  `pk_num_seguridad_perfil` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_nombre_perfil` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_seguridad_perfil`),
  INDEX `fk_a017_seguridad_perfil_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a017_seguridad_perfil_a018_seguridad_usuario`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a019_seguridad_dependencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a019_seguridad_dependencia` (
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fk_a004_num_dependencia` INT(11) NOT NULL,
  `fk_a015_num_seguridad_aplicacion` INT(11) NOT NULL,
  PRIMARY KEY (`fk_a018_num_seguridad_usuario`, `fk_a004_num_dependencia`, `fk_a015_num_seguridad_aplicacion`),
  INDEX `fk_a019_seguridad_dependencia_a004_dependencia` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_a019_seguridad_dependencia_a015_seguridad_aplicacion` (`fk_a015_num_seguridad_aplicacion` ASC),
  INDEX `fk_a019_seguridad_dependencia_a018_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a019_seguridad_dependencia_a004_dependencia`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a019_seguridad_dependencia_a015_seguridad_aplicacion`
    FOREIGN KEY (`fk_a015_num_seguridad_aplicacion`)
    REFERENCES `SIACE`.`a015_seguridad_aplicacion` (`pk_num_seguridad_aplicacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a019_seguridad_dependencia_a018_seguridad_usuario`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a020_seguridad_usuarioperfil`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a020_seguridad_usuarioperfil` (
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fk_a017_num_seguridad_perfil` INT(11) NOT NULL,
  PRIMARY KEY (`fk_a018_num_seguridad_usuario`, `fk_a017_num_seguridad_perfil`),
  INDEX `fk_a020_seguridad_usuarioperfil_a017_seguridad_perfil` (`fk_a017_num_seguridad_perfil` ASC),
  CONSTRAINT `fk_a020_seguridad_usuarioperfil_a017_seguridad_perfil`
    FOREIGN KEY (`fk_a017_num_seguridad_perfil`)
    REFERENCES `SIACE`.`a017_seguridad_perfil` (`pk_num_seguridad_perfil`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a020_seguridad_usuarioperfil_a018_seguridad_usuario`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a021_dependencia_ext`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a021_dependencia_ext` (
  `pk_num_dependencia_ext` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id unico de cada dependencia',
  `ind_documento_fiscal` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica documento fiscal de la dependencia',
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica el nombre de la dependencia',
  `num_estatus` INT(1) NOT NULL COMMENT '1= activa 0= inactiva, estatus de la dependencia',
  `fk_a001_num_organismo` INT(11) NOT NULL COMMENT 'fk relacionado con el id del organismo el cual pertenece la dependencia',
  PRIMARY KEY (`pk_num_dependencia_ext`),
  INDEX `fk_a021_dependencia_ext_a001_organismo` (`fk_a001_num_organismo` ASC),
  CONSTRAINT `fk_a021_dependencia_ext_a001_organismo`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 18
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a022_dependencia_extdetalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a022_dependencia_extdetalle` (
  `pk_num_dependencia_ext_detalle` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id unico del detalle de la dependencia',
  `ind_cargo_representante` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica el cargo del representante legal de la dependencia externa',
  `ind_direccion` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica la direccion de la dependencia externa',
  `fk_a021_num_dependencia_ext` INT(11) NOT NULL COMMENT 'fk relacionado con el id del numero de la dependencia',
  `fk_a003_num_persona_representante` INT(11) NOT NULL COMMENT 'fk relacionado con el id del representante legal de la dependencia',
  PRIMARY KEY (`pk_num_dependencia_ext_detalle`),
  INDEX `fk_a022_dependencia_extdetalle_a021_dependencia_ext` (`fk_a021_num_dependencia_ext` ASC),
  INDEX `fk_a022_dependencia_extdetalle_a003_persona1_idx` (`fk_a003_num_persona_representante` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 15
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a024_grupo_centro_costo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a024_grupo_centro_costo` (
  `pk_num_grupo_centro_costo` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_grupo_centro_costo` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  PRIMARY KEY (`pk_num_grupo_centro_costo`),
  UNIQUE INDEX `cod_grupo_centrocosto_UNIQUE` (`cod_grupo_centro_costo` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 14
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a025_subgrupo_centro_costo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a025_subgrupo_centro_costo` (
  `pk_num_subgrupo_centro_costo` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a024_num_grupo_centro_costo` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_subgrupo_centro_costo`),
  INDEX `fk_a025_subgrupo_centro_costo_a024_grupo_centro_costo1_idx` (`fk_a024_num_grupo_centro_costo` ASC),
  CONSTRAINT `fk_a025_subgrupo_centro_costo_a024_grupo_centro_costo`
    FOREIGN KEY (`fk_a024_num_grupo_centro_costo`)
    REFERENCES `SIACE`.`a024_grupo_centro_costo` (`pk_num_grupo_centro_costo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 26
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a023_centro_costo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a023_centro_costo` (
  `pk_num_centro_costo` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion_centro_costo` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_abreviatura` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_administrativo` INT(1) NULL DEFAULT NULL,
  `num_flag_ventas` INT(1) NULL DEFAULT NULL,
  `num_flag_financiera` INT(1) NULL DEFAULT NULL,
  `num_flag_produccion` INT(1) NULL DEFAULT NULL,
  `num_flag_centro_ingresos` INT(1) NULL DEFAULT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a003_num_persona` INT(11) NOT NULL,
  `fk_a025_num_subgrupo_centro_costo` INT(11) NOT NULL,
  `fk_a004_num_dependencia` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_centro_costo`),
  INDEX `fk_a023_centro_costo_a003_persona` (`fk_a003_num_persona` ASC),
  INDEX `fk_a023_centro_costo_a025_subgrupo_centrocosto1_idx` (`fk_a025_num_subgrupo_centro_costo` ASC),
  INDEX `fk_a023_centro_costo_a004_dependencia1_idx` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a023_a018_num_seguridad_usuario`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a023_centro_costo_a003_persona`
    FOREIGN KEY (`fk_a003_num_persona`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a023_centro_costo_a004_dependencia`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a023_centro_costo_a025_subgrupo_centrocosto`
    FOREIGN KEY (`fk_a025_num_subgrupo_centro_costo`)
    REFERENCES `SIACE`.`a025_subgrupo_centro_costo` (`pk_num_subgrupo_centro_costo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 46
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a026_banco`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a026_banco` (
  `pk_num_banco` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_nombre_banco` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  PRIMARY KEY (`pk_num_banco`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a027_seguridad_menu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a027_seguridad_menu` (
  `pk_num_seguridad_menu` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_nombre` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `cod_interno` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `cod_padre` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_nivel` INT(11) NOT NULL,
  `ind_ruta` LONGTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_rol` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_icono` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fk_a015_num_seguridad_aplicacion` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_seguridad_menu`),
  INDEX `fk_a027_seguridad_menu_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_a027_seguridad_menu_a015_seguridad_aplicacion1_idx` (`fk_a015_num_seguridad_aplicacion` ASC),
  CONSTRAINT `fk_a027_seguridad_menu_a015_seguridad_aplicacion`
    FOREIGN KEY (`fk_a015_num_seguridad_aplicacion`)
    REFERENCES `SIACE`.`a015_seguridad_aplicacion` (`pk_num_seguridad_aplicacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a027_seguridad_menu_a018_seguridad_usuario`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`))
ENGINE = InnoDB
AUTO_INCREMENT = 5097
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a028_seguridad_menupermiso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a028_seguridad_menupermiso` (
  `fk_a027_num_seguridad_menu` INT(11) NOT NULL,
  `fk_a017_num_seguridad_perfil` INT(11) NOT NULL,
  PRIMARY KEY (`fk_a027_num_seguridad_menu`, `fk_a017_num_seguridad_perfil`),
  INDEX `fk_a028_seguridad_menupermiso_a027_seguridad_menu` (`fk_a027_num_seguridad_menu` ASC),
  INDEX `fk_a028_seguridad_menupermiso_a017_seguridad_perfil` (`fk_a017_num_seguridad_perfil` ASC),
  CONSTRAINT `fk_a028_seguridad_menupermiso_a017_seguridad_perfil`
    FOREIGN KEY (`fk_a017_num_seguridad_perfil`)
    REFERENCES `SIACE`.`a017_seguridad_perfil` (`pk_num_seguridad_perfil`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a029_organismo_telefono`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a029_organismo_telefono` (
  `pk_num_organismo_telefono` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_telefono` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a001_num_organismo` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_organismo_telefono`),
  INDEX `fk_a029_organismo_telefono_a001_organismo` (`fk_a001_num_organismo` ASC),
  CONSTRAINT `fk_a029_organismo_telefono_a001_organismo`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a030_extension_telefono`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a030_extension_telefono` (
  `pk_num_extension_telefono` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_extension` INT(11) NULL DEFAULT NULL,
  `fk_a004_cod_dependencia` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_extension_telefono`),
  INDEX `fk_a030_extension_telefono_a004_dependencia` (`fk_a004_cod_dependencia` ASC),
  CONSTRAINT `fk_a030_extension_telefono_a004_dependencia`
    FOREIGN KEY (`fk_a004_cod_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a032_persona_licencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a032_persona_licencia` (
  `pk_num_licencia` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a003_num_persona` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipolic` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_claselic` INT(11) NOT NULL,
  `fec_expiracion_licencia` DATE NOT NULL,
  PRIMARY KEY (`pk_num_licencia`),
  INDEX `fk_a032_persona_licencia_a003_persona` (`fk_a003_num_persona` ASC),
  INDEX `fk_a032_persona_licencia_a006_miscelaneos_det` (`fk_a006_num_miscelaneo_detalle_tipolic` ASC),
  CONSTRAINT `fk_a032_persona_licencia_a003_persona`
    FOREIGN KEY (`fk_a003_num_persona`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a033_tipo_logs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a033_tipo_logs` (
  `pk_num_tipo_logs` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_tipo_logs` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  PRIMARY KEY (`pk_num_tipo_logs`),
  UNIQUE INDEX `ind_tipo_logs_UNIQUE` (`ind_tipo_logs` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a034_logs_usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a034_logs_usuarios` (
  `pk_num_logs_usuario` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_detalle` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_ip` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_log` DATETIME NOT NULL,
  `fk_a033_num_tipo_logs` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_logs_usuario`),
  INDEX `fk_a034_logs_usuarios_a033_tipo_logs1_idx` (`fk_a033_num_tipo_logs` ASC),
  INDEX `fk_a034_logs_usuarios_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a034_logs_usuarios_a018_seguridad_usuario`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a034_logs_usuarios_a033_tipo_logs`
    FOREIGN KEY (`fk_a033_num_tipo_logs`)
    REFERENCES `SIACE`.`a033_tipo_logs` (`pk_num_tipo_logs`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 252
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a035_parametros`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a035_parametros` (
  `pk_num_parametros` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_explicacion` LONGTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_parametro_clave` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_valor_parametro` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a015_num_seguridad_aplicacion` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_parametros`),
  UNIQUE INDEX `ind_parametro_clave_UNIQUE` (`ind_parametro_clave` ASC),
  INDEX `fk_a034_parametros_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_a034_parametros_a015_seguridad_aplicacion1_idx` (`fk_a015_num_seguridad_aplicacion` ASC),
  INDEX `fk_a035_parametros_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle` ASC),
  CONSTRAINT `fk_a034_parametros_a015_seguridad_aplicacion1`
    FOREIGN KEY (`fk_a015_num_seguridad_aplicacion`)
    REFERENCES `SIACE`.`a015_seguridad_aplicacion` (`pk_num_seguridad_aplicacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a034_parametros_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 103
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a036_persona_direccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a036_persona_direccion` (
  `pk_num_direccion_persona` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a003_num_persona` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_domicilio` INT(11) NULL DEFAULT NULL,
  `fk_a006_num_miscelaneo_detalle_tipodir` INT(11) NULL DEFAULT NULL,
  `fk_a006_num_zona_residencial` INT(11) NOT NULL COMMENT 'Zona residencial(Sector, urbanización, manzana, barrio, entre otros))',
  `ind_zona_residencial` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Detalle de la Zona Residencial',
  `fk_a006_num_ubicacion_inmueble` INT(11) NOT NULL COMMENT 'Ubicación del inmueble (Avenida, calle, carretera, callejon, vereda, entre otros))',
  `ind_ubicacion_inmueble` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Detalle de la Ubicacion',
  `fk_a006_num_tipo_inmueble` INT(11) NOT NULL COMMENT 'Tipo de Inmueble (Casa, quinta, apartamento, local, entre otros))',
  `ind_tipo_inmueble` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Detalle del Tipo de Inmueble',
  `fk_a006_num_tipo_zona` INT(11) NOT NULL COMMENT 'Tipo de zona del inmueble (Rural o urbana)',
  `ind_inmueble` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Número de inmueble (casa, apartamento, local, entre otros)',
  `ind_punto_referencia` INT(11) NOT NULL COMMENT 'Punto de referencia para ubicar el inmueble',
  `ind_zona_postal` INT(11) NULL DEFAULT NULL COMMENT 'Zona Postal a la que pertenece la direccion',
  `ind_direccion` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  PRIMARY KEY (`pk_num_direccion_persona`),
  INDEX `fk_a036_persona_direccion_a003_persona1_idx` (`fk_a003_num_persona` ASC),
  CONSTRAINT `fk_a036_persona_direccion_a003_persona1`
    FOREIGN KEY (`fk_a003_num_persona`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 84
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`a037_tipo_ente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a037_tipo_ente` (
  `pk_num_tipo_ente` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador de registro',
  `ind_tipo_ente` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Nombre o descripción',
  `fk_a018_num_seg_useregistra` INT(11) NOT NULL COMMENT 'Id de la persona que registra',
  `fec_registro` DATETIME NOT NULL COMMENT 'Feha de registro',
  `fk_a018_num_seg_usermod` INT(11) NOT NULL COMMENT 'Id de la persona que actualiza',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Fecha última modificación',
  PRIMARY KEY (`pk_num_tipo_ente`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Tabla para el registro del tipo de ente';


-- -----------------------------------------------------
-- Table `SIACE`.`a038_categoria_ente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a038_categoria_ente` (
  `pk_num_categoria_ente` INT(1) NOT NULL AUTO_INCREMENT COMMENT 'Identificador de registro',
  `fk_a037_num_tipo_ente` INT(11) NOT NULL COMMENT 'Id del tipo de ente',
  `ind_categoria_ente` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Nombre o descripción',
  `fk_a018_num_seg_useregistra` INT(1) NOT NULL COMMENT 'Id de la persona que registra',
  `fec_registro` DATETIME NOT NULL COMMENT 'Feha de registro',
  `fk_a018_num_seg_usermod` INT(11) NOT NULL COMMENT 'Id de la persona que actualiza',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Fecha última modificación',
  PRIMARY KEY (`pk_num_categoria_ente`),
  INDEX `fk_a037_num_tipo_ente_idx` (`fk_a037_num_tipo_ente` ASC),
  CONSTRAINT `fk_a037_num_tipo_ente`
    FOREIGN KEY (`fk_a037_num_tipo_ente`)
    REFERENCES `SIACE`.`a037_tipo_ente` (`pk_num_tipo_ente`)
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Tabla para el registro de categorías de entes';


-- -----------------------------------------------------
-- Table `SIACE`.`a039_ente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a039_ente` (
  `pk_num_ente` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador de registro',
  `num_ente_padre` INT(11) NULL DEFAULT '0' COMMENT 'Id del ente padre',
  `fk_a037_num_tipo_ente` INT(11) NOT NULL COMMENT 'Id del tipo de ente',
  `fk_a038_num_categoria_ente` INT(11) NOT NULL COMMENT 'Id de la categoría de ente',
  `ind_nombre_ente` VARCHAR(256) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Nombre del ente, organización o dependencia',
  `ind_numero_registro` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Número de registro fiscal',
  `ind_tomo_registro` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Tomo',
  `fec_fundacion` DATE NULL DEFAULT NULL COMMENT 'Fecha de fundación',
  `fk_a006_num_miscdetalle_caractersocial` INT(11) NULL DEFAULT NULL COMMENT 'Id del miscelaneo: Caracter social',
  `num_sujeto_control` INT(1) NULL DEFAULT '0' COMMENT 'Implica si el ente es sujeto a control: Si: 1, No: 0',
  `ind_mision` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Misión del ente u organísmo',
  `ind_vision` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Visión del ente u organísmo',
  `ind_gaceta` VARCHAR(25) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Gaceta',
  `ind_resolucion` VARCHAR(25) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Resolución',
  `ind_otros` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Cualquier otro dato para con el ente u organísmo',
  `ind_pagina_web` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Portal web del ente u organísmo',
  `fk_a009_num_estado` INT(11) NULL DEFAULT '0' COMMENT 'Id de la entidad',
  `fk_a011_num_municipio` INT(11) NULL DEFAULT '0' COMMENT 'Id del municipio',
  `pf_a012_num_parroquia` INT(11) NULL DEFAULT '0' COMMENT 'Id de la parroquia',
  `fk_a010_num_ciudad` INT(11) NULL DEFAULT '0' COMMENT 'Id de la ciudad',
  `ind_direccion` VARCHAR(256) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Dirección',
  `ind_telefono` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Número teléfonicos formato (####) ###.##.##-tipo  (tipo: normal:N / fax:F) separados por coma(,)',
  `num_estatus` INT(1) NULL DEFAULT '1' COMMENT 'Estatus del ente u organísmo(Activo:1, Inactivo:0)',
  `fk_a018_num_seg_useregistra` INT(11) NOT NULL COMMENT 'Id de la persona que registra',
  `fec_registro` DATETIME NOT NULL COMMENT 'Feha de registro',
  `fk_a018_num_seg_usermod` INT(11) NOT NULL COMMENT 'Id de la persona que modificó el registro',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Fecha última modificación',
  PRIMARY KEY (`pk_num_ente`),
  INDEX `fk_a037_num_tipo_ente_idx` (`fk_a037_num_tipo_ente` ASC),
  INDEX `fk_a038_num_categoria_ente_idx` (`fk_a038_num_categoria_ente` ASC),
  INDEX `fk_num_miscdetalle_caractersocial_idx` (`fk_a006_num_miscdetalle_caractersocial` ASC),
  INDEX `fk_num_estado_idx` (`fk_a009_num_estado` ASC),
  INDEX `fk_num_municipio_idx` (`fk_a011_num_municipio` ASC),
  INDEX `pf_num_parroquia_idx` (`pf_a012_num_parroquia` ASC),
  INDEX `fk_num_ciudad_idx` (`fk_a010_num_ciudad` ASC),
  CONSTRAINT `fk_num_categoria_ente`
    FOREIGN KEY (`fk_a038_num_categoria_ente`)
    REFERENCES `SIACE`.`a038_categoria_ente` (`pk_num_categoria_ente`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_num_miscdetalle_caractersocial`
    FOREIGN KEY (`fk_a006_num_miscdetalle_caractersocial`)
    REFERENCES `SIACE`.`a006_miscelaneo_detalle` (`pk_num_miscelaneo_detalle`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_num_tipo_ente`
    FOREIGN KEY (`fk_a037_num_tipo_ente`)
    REFERENCES `SIACE`.`a037_tipo_ente` (`pk_num_tipo_ente`)
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Tabla para el registro de los entes externos';


-- -----------------------------------------------------
-- Table `SIACE`.`a040_cargo_personal_externo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a040_cargo_personal_externo` (
  `pk_num_cargo_personal_externo` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador de registro',
  `ind_cargo_personal_externo` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Nombre o descripción del cargo',
  `fk_a018_num_seg_useregistra` INT(11) NOT NULL COMMENT 'Id de la persona que registra',
  `fec_registro` DATETIME NOT NULL COMMENT 'Feha de registro',
  `fk_a018_num_seg_usermod` INT(1) NOT NULL COMMENT 'Id de la persona que actualiza',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Fecha última modificación',
  PRIMARY KEY (`pk_num_cargo_personal_externo`))
ENGINE = InnoDB
AUTO_INCREMENT = 28
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Tabla para el registro de cargos de personal externo';


-- -----------------------------------------------------
-- Table `SIACE`.`a041_persona_ente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`a041_persona_ente` (
  `pk_num_persona_ente` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador del registro',
  `fk_a003_num_persona_titular` INT(11) NOT NULL COMMENT 'Id de la persona representante o titular',
  `fk_a039_num_ente` INT(11) NOT NULL COMMENT 'Id del ente o dependencia',
  `fk_a040_num_cargo_personal_externo` INT(11) NOT NULL COMMENT 'Id del cargo del titular del ente o dependencia',
  `fk_a006_num_miscdetalle_situacion` INT(11) NOT NULL COMMENT 'Id del miscelanio detalle. Situación de la persona: Titular, Encargado, Empleado, Particular',
  `num_estatus_titular` INT(1) NOT NULL COMMENT 'Estatus de la persona para con el cargo y ente: Activo:1, Inactivo:0',
  `fk_a018_num_seg_useregistra` INT(11) NOT NULL COMMENT 'Id de la persona que registra',
  `fec_registro` DATETIME NOT NULL COMMENT 'Feha de registro',
  `fk_a018_num_seg_usermod` INT(11) NOT NULL COMMENT 'Id de la persona que actualiza',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Fecha última modificación',
  PRIMARY KEY (`pk_num_persona_ente`),
  INDEX `fk_a003_num_persona_titular_idx` (`fk_a003_num_persona_titular` ASC),
  INDEX `fk_a039_num_ente_idx` (`fk_a039_num_ente` ASC),
  INDEX `fk_a040_num_cargo_personal_externo_idx` (`fk_a040_num_cargo_personal_externo` ASC),
  INDEX `fk_a006_num_miscdetalle_situacion_idx` (`fk_a006_num_miscdetalle_situacion` ASC),
  CONSTRAINT `fk_a003_num_persona_titular`
    FOREIGN KEY (`fk_a003_num_persona_titular`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a006_num_miscdetalle_situacion`
    FOREIGN KEY (`fk_a006_num_miscdetalle_situacion`)
    REFERENCES `SIACE`.`a006_miscelaneo_detalle` (`pk_num_miscelaneo_detalle`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a039_num_ente`
    FOREIGN KEY (`fk_a039_num_ente`)
    REFERENCES `SIACE`.`a039_ente` (`pk_num_ente`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a040_num_cargo_personal_externo`
    FOREIGN KEY (`fk_a040_num_cargo_personal_externo`)
    REFERENCES `SIACE`.`a040_cargo_personal_externo` (`pk_num_cargo_personal_externo`)
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Tabla que relaciona la persona titular con el ente';


-- -----------------------------------------------------
-- Table `SIACE`.`rh_b001_empleado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_b001_empleado` (
  `pk_num_empleado` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a003_num_persona` INT(11) NOT NULL,
  `ind_estado_aprobacion` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_empleado`),
  INDEX `fk_a003_num_persona` (`fk_a003_num_persona` ASC),
  INDEX `fk_rh_b001_empleado_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a003_num_persona`
    FOREIGN KEY (`fk_a003_num_persona`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_b001_empleado_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 33
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`ad_c001_almacen`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`ad_c001_almacen` (
  `pk_num_almacen` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion_almacen` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Descripción del almacén',
  `ind_ubicacion_almacen` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Ubicación física del almacén',
  `fk_rhb001_num_empleado` INT(11) NOT NULL COMMENT 'Responsable del almacén',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Fecha de la última modificación',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Último usuario que efectuó cambios al registro de almacén',
  PRIMARY KEY (`pk_num_almacen`),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `ad_c001_almacen_ibfk_1`
    FOREIGN KEY (`fk_rhb001_num_empleado`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`pk_num_empleado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ad_c001_almacen_ibfk_2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Registro de Almacén';


-- -----------------------------------------------------
-- Table `SIACE`.`ad_c002_pasillo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`ad_c002_pasillo` (
  `pk_num_pasillo` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id autoincremental de la tabla pasillo',
  `ind_pasillo` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Nombre del pasillo',
  `ind_descripcion_pasillo` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Descripción del pasillo',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'fecha de la ultima modificación',
  `fk_adc001_num_almacen` INT(11) NOT NULL COMMENT 'Almacén al cual pertenece el pasillo',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Último usuario que realizó modificaciones',
  PRIMARY KEY (`pk_num_pasillo`),
  INDEX `fk_adc001_num_almacen` (`fk_adc001_num_almacen` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `ad_c002_pasillo_ibfk_1`
    FOREIGN KEY (`fk_adc001_num_almacen`)
    REFERENCES `SIACE`.`ad_c001_almacen` (`pk_num_almacen`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ad_c002_pasillo_ibfk_2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Registra los pasillos del almacén';


-- -----------------------------------------------------
-- Table `SIACE`.`ad_c003_estante`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`ad_c003_estante` (
  `pk_num_estante` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id autoincremental del estante',
  `ind_descripcion_estante` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Nombre descriptivo del estante',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Fecha de la última modificación al registro',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Usuario que realizó la última modificación',
  `fk_adc002_num_pasillo` INT(11) NOT NULL COMMENT 'Pasillo donde se encuentra en el almacén',
  PRIMARY KEY (`pk_num_estante`),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_adc002_num_pasillo` (`fk_adc002_num_pasillo` ASC),
  CONSTRAINT `ad_c003_estante_ibfk_1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ad_c003_estante_ibfk_2`
    FOREIGN KEY (`fk_adc002_num_pasillo`)
    REFERENCES `SIACE`.`ad_c002_pasillo` (`pk_num_pasillo`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Registra los estantes del almacén';


-- -----------------------------------------------------
-- Table `SIACE`.`ad_c005_tipo_documento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`ad_c005_tipo_documento` (
  `pk_num_documento` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id autoincremental de tipo de documento',
  `ind_descripcion_documento` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Nombre descriptivo del tipo de documento',
  `fec_ultima_modificacion` DATETIME NULL DEFAULT NULL COMMENT 'Fecha de la última modificación',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Último usuario que realizó modificaciones',
  PRIMARY KEY (`pk_num_documento`),
  INDEX `fk_ad_c005_tipo_documento_a018_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Registro de tipo de documentos';


-- -----------------------------------------------------
-- Table `SIACE`.`ad_c004_caja`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`ad_c004_caja` (
  `pk_num_caja` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id autoincremental de la tabla',
  `ind_descripcion_caja` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'nombre descriptivo de la caja',
  `fec_anio` INT(11) NOT NULL COMMENT 'Anio de la caja',
  `fk_a004_num_dependencia` INT(11) NOT NULL COMMENT 'Dependencia a la cual pertenece la caja',
  `fk_adc003_num_estante` INT(11) NOT NULL COMMENT 'Estante donde se encuentra la caja',
  `fk_adc005_num_documento` INT(11) NOT NULL COMMENT 'tipo de documento que contiene la caja',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Fecha de la última modificación',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Último usuario que efectuó cambios al registro',
  PRIMARY KEY (`pk_num_caja`),
  INDEX `fk_adc005_num_documento` (`fk_adc005_num_documento` ASC),
  INDEX `  fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_a004_num_dependencia` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_adc003_num_estante` (`fk_adc003_num_estante` ASC),
  CONSTRAINT `ad_c004_caja_ibfk_1`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ad_c004_caja_ibfk_2`
    FOREIGN KEY (`fk_adc003_num_estante`)
    REFERENCES `SIACE`.`ad_c003_estante` (`pk_num_estante`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ad_c004_caja_ibfk_3`
    FOREIGN KEY (`fk_adc005_num_documento`)
    REFERENCES `SIACE`.`ad_c005_tipo_documento` (`pk_num_documento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ad_c004_caja_ibfk_4`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Registro de la caja donde se guardan los documentos físicos';


-- -----------------------------------------------------
-- Table `SIACE`.`ad_b001_registro_documento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`ad_b001_registro_documento` (
  `pk_num_registro_documento` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave autoincremental primaria de la tabla',
  `num_registro` INT(11) NOT NULL COMMENT 'Número de registro del documento',
  `cod_memo` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Memorándum donde se solicita el registro del documento en el sistema',
  `ind_documento` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Código real del documento digitalizado',
  `fec_documento` DATE NOT NULL COMMENT 'Fecha del documento',
  `fec_registro` DATETIME NULL DEFAULT NULL COMMENT 'Fecha de registro del documento',
  `ind_descripcion` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Descripción del documento',
  `ind_resumen` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Indica el resumen del documento',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Fecha de la última modificación',
  `fk_adc004_num_caja` INT(11) NOT NULL COMMENT 'Id de la caja donde se guardó el documento',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Último usuario que efectuó cambios en el registro',
  `fk_rhb001_num_empleado` INT(11) NOT NULL COMMENT 'Funcionario remitente',
  `num_flag_archivo` INT(11) NOT NULL COMMENT 'Flag que indica 1 si el registro es un archivo y 0 si son imágenes',
  PRIMARY KEY (`pk_num_registro_documento`),
  INDEX `fk_adc004_num_caja` (`fk_adc004_num_caja` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `	fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  CONSTRAINT `ad_b001_registro_documento_ibfk_1`
    FOREIGN KEY (`fk_adc004_num_caja`)
    REFERENCES `SIACE`.`ad_c004_caja` (`pk_num_caja`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ad_b001_registro_documento_ibfk_2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ad_b001_registro_documento_ibfk_3`
    FOREIGN KEY (`fk_rhb001_num_empleado`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`pk_num_empleado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Se encarga de los datos de los documentos digitalizados';


-- -----------------------------------------------------
-- Table `SIACE`.`ad_b002_salida_documento_fisico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`ad_b002_salida_documento_fisico` (
  `pk_num_salida` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave autoincremental de la tabla que registra la salida de documentos',
  `fec_fecha_salida` DATE NOT NULL COMMENT 'Fecha de salida del documento',
  `ind_motivo` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Motivo por el cual se requiere sacar un documento del almacén',
  `cod_memo` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Código del memorándum con el que se autoriza la salida del documento',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Fecha de la última modificación',
  `fk_adb001_num_registro_documento` INT(11) NOT NULL COMMENT 'Id del documento saliente',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Último usuario que efectuó cambios al registro de salida',
  PRIMARY KEY (`pk_num_salida`),
  INDEX `fk_adb001_num_registro_documento` (`fk_adb001_num_registro_documento` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `ad_b002_salida_documento_fisico_ibfk_1`
    FOREIGN KEY (`fk_adb001_num_registro_documento`)
    REFERENCES `SIACE`.`ad_b001_registro_documento` (`pk_num_registro_documento`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ad_b002_salida_documento_fisico_ibfk_2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Se encarga del registro de salida de los documentos';


-- -----------------------------------------------------
-- Table `SIACE`.`ad_b003_entrada_documento_fisico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`ad_b003_entrada_documento_fisico` (
  `pk_num_entrada` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_entrada` DATE NOT NULL COMMENT 'Fecha de entrada del documento al almacén',
  `ind_observacion` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Observaciones en el estado del documento o alguna situación en particular',
  `cod_memo` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Memorándum que autoriza la entrada del documento',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Fecha de la última modificación',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Último usuario que efectuó cambios al registro de entrada',
  `fk_adb002_num_salida` INT(11) NOT NULL COMMENT 'Id de la salida del documento',
  PRIMARY KEY (`pk_num_entrada`),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_adb002_num_salida` (`fk_adb002_num_salida` ASC),
  CONSTRAINT `ad_b003_entrada_documento_fisico_ibfk_1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ad_b003_entrada_documento_fisico_ibfk_2`
    FOREIGN KEY (`fk_adb002_num_salida`)
    REFERENCES `SIACE`.`ad_b002_salida_documento_fisico` (`pk_num_salida`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Registro de entrada de documentos al almacén';


-- -----------------------------------------------------
-- Table `SIACE`.`ad_c006_operacion_entrada`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`ad_c006_operacion_entrada` (
  `pk_num_operacion` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id autoincremental de la operaciÃ³n',
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'PREPARADO: PR; APROBADO: AP; ANULADO: AN;',
  `fk_adb003_num_entrada` INT(11) NOT NULL COMMENT 'Id de la entrada del documento al almacén',
  `fk_rhb001_num_empleado` INT(11) NOT NULL COMMENT 'Funcionario que entrega el documento',
  `num_estatus` INT(1) NOT NULL COMMENT 'Estado de la entrada 1 activo y 0 inactivo',
  `fec_operacion` DATETIME NOT NULL COMMENT 'fecha de la operación',
  PRIMARY KEY (`pk_num_operacion`),
  INDEX `fk_adb003_num_entrada` (`fk_adb003_num_entrada` ASC),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  CONSTRAINT `ad_c006_operacion_entrada_ibfk_1`
    FOREIGN KEY (`fk_adb003_num_entrada`)
    REFERENCES `SIACE`.`ad_b003_entrada_documento_fisico` (`pk_num_entrada`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ad_c006_operacion_entrada_ibfk_2`
    FOREIGN KEY (`fk_rhb001_num_empleado`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`pk_num_empleado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Bitácora de entrada de documentos';


-- -----------------------------------------------------
-- Table `SIACE`.`ad_c007_operacion_salida`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`ad_c007_operacion_salida` (
  `pk_num_operacion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'PREPARADO: PR; APROBADO: AP; ANULADO: AN;',
  `fk_adb002_num_salida` INT(11) NOT NULL COMMENT 'Id del registro de salida del documento',
  `fk_rhb001_num_empleado` INT(11) NOT NULL COMMENT 'Funcionario que realiza la operación',
  `num_estatus` INT(1) NOT NULL COMMENT 'Estatus de la operación 1 para activo y 0 para inactivo',
  `fec_operacion` DATETIME NOT NULL COMMENT 'Fecha de la Operación',
  PRIMARY KEY (`pk_num_operacion`),
  INDEX `fk_adb002_num_salida` (`fk_adb002_num_salida` ASC),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  CONSTRAINT `ad_c007_operacion_salida_ibfk_1`
    FOREIGN KEY (`fk_adb002_num_salida`)
    REFERENCES `SIACE`.`ad_b002_salida_documento_fisico` (`pk_num_salida`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ad_c007_operacion_salida_ibfk_2`
    FOREIGN KEY (`fk_rhb001_num_empleado`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`pk_num_empleado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Bitácora de salida de documentos';


-- -----------------------------------------------------
-- Table `SIACE`.`ad_c008_archivo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`ad_c008_archivo` (
  `pk_num_archivo` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id autoincremental de archivos',
  `fk_adb001_num_registro_documento` INT(11) NOT NULL COMMENT 'Id del registro del documento',
  `ind_nombre_archivo` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  PRIMARY KEY (`pk_num_archivo`),
  INDEX `fk_adb001_num_registro_documento` (`fk_adb001_num_registro_documento` ASC),
  CONSTRAINT `ad_c008_archivo_ibfk_1`
    FOREIGN KEY (`fk_adb001_num_registro_documento`)
    REFERENCES `SIACE`.`ad_b001_registro_documento` (`pk_num_registro_documento`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Guarda el registro de los archivos cargados en el módulo';


-- -----------------------------------------------------
-- Table `SIACE`.`af_c007_situacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c007_situacion` (
  `pk_num_situacion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_nombre_situacion` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_depreciacion` INT(1) NOT NULL DEFAULT '0',
  `num_flag_revaluacion` INT(1) NOT NULL DEFAULT '0',
  `num_flag_sistema` INT(1) NOT NULL DEFAULT '0',
  `num_estatus` INT(1) NOT NULL DEFAULT '1',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_situacion`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario52`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cb_b005_contabilidades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cb_b005_contabilidades` (
  `pk_num_contabilidades` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_tipo_contabilidad` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL COMMENT '1: Activo - 0: Inactivo',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_contabilidades`),
  INDEX `fk_cb_b005_contabilidades_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_cb_b005_contabilidades_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c008_clasificacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c008_clasificacion` (
  `pk_num_clasificacion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_codigo` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_nombre_clasificacion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_cbb005_num_contabilidad` INT(11) NOT NULL,
  `num_nivel` INT(1) NOT NULL DEFAULT '0',
  `num_estatus` INT(1) NOT NULL DEFAULT '1',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_clasificacion`),
  UNIQUE INDEX `ind_codigo_UNIQUE` USING BTREE (`fk_cbb005_num_contabilidad` ASC, `ind_codigo` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_afc008_cbb005_num_contabilidad` (`fk_cbb005_num_contabilidad` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario53`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc008_cbb005_num_contabilidad`
    FOREIGN KEY (`fk_cbb005_num_contabilidad`)
    REFERENCES `SIACE`.`cb_b005_contabilidades` (`pk_num_contabilidades`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1376
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c005_ubicacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c005_ubicacion` (
  `pk_num_ubicacion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_nombre_ubicacion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_codigo` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_nivel` INT(1) NOT NULL DEFAULT '0',
  `num_estatus` INT(1) NOT NULL DEFAULT '1',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_ubicacion`),
  UNIQUE INDEX `ind_codigo__UNIQUE_idx` USING BTREE (`ind_codigo` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario51`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 52
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c016_tipo_seguro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c016_tipo_seguro` (
  `pk_num_tipo_seguro` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_nombre_tipo_seguro` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '1',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_tipo_seguro`),
  INDEX `fk_c016_tipo_seguro_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_c016_tipo_seguro_a018_num_seguridad_usuario_idx`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c017_tipo_vehiculo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c017_tipo_vehiculo` (
  `pk_num_tipo_vehiculo` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_nombre_tipo_vehiculo` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '1',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_tipo_vehiculo`),
  INDEX `fk_c017_tipo_vehiculo_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_c017_tipo_vehiculo_a018_num_seguridad_usuario_idx`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c018_poliza_seguro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c018_poliza_seguro` (
  `pk_num_poliza_seguro` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_nombre_poliza_seguro` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_empresa_aseguradora` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_agente_seguros` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_fecha_vencimiento` DATE NOT NULL,
  `num_monto_cobertura` DECIMAL(18,6) NOT NULL,
  `num_costo_poliza` DECIMAL(18,6) NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '1',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_poliza_seguro`),
  INDEX `fk_c018_poliza_seguro_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_c018_poliza_seguro_a018_num_seguridad_usuario_idx`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cb_b004_plan_cuenta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cb_b004_plan_cuenta` (
  `pk_num_cuenta` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_cuenta` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_grupo` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_subgrupo` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_rubro` VARCHAR(3) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_cuenta` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_sub_cuenta1` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_sub_cuenta2` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_sub_cuenta3` VARCHAR(3) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_nivel` INT(1) UNSIGNED NOT NULL,
  `num_flag_tipo` INT(1) NOT NULL COMMENT 'P:PRINCIPAL; A:AUXILIAR;',
  `fk_a006_num_miscelaneo_detalle` INT(11) NULL DEFAULT NULL,
  `ind_tipo_saldo` VARCHAR(1) CHARACTER SET 'utf8' NOT NULL COMMENT 'D:DEUDORA; A:ACREEDORA',
  `ind_naturaleza` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL COMMENT '1: Activo - 0: Inactivo',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `num_flag_tipo_cuenta` INT(1) NOT NULL,
  `num_flag_req_ccosto` INT(1) NOT NULL,
  `num_flag_req_activo` INT(1) NOT NULL,
  PRIMARY KEY (`pk_num_cuenta`),
  UNIQUE INDEX `CodCuenta_UNIQUE` USING BTREE (`cod_cuenta` ASC, `num_flag_tipo_cuenta` ASC),
  INDEX `fk_cb_b004_plan_cuenta_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cb_b004_plan_cuenta_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle` ASC),
  INDEX `cod_cuenta` (`cod_cuenta` ASC),
  CONSTRAINT `fk_cb_b004_plan_cuenta_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2612
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c019_categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c019_categoria` (
  `pk_num_categoria` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_codigo` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_nombre_categoria` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a006_num_tipo` INT(11) NOT NULL,
  `fk_a006_num_grupo` INT(11) NOT NULL,
  `fk_cbb004_num_cuenta_historica` INT(11) NOT NULL,
  `fk_cbb004_num_cuenta_historica_variacion` INT(11) NOT NULL,
  `fk_cbb004_num_cuenta_historica_revaluacion` INT(11) NOT NULL,
  `fk_cbb004_num_cuenta_depreciacion` INT(11) NOT NULL,
  `fk_cbb004_num_cuenta_depreciacion_variacion` INT(11) NOT NULL,
  `fk_cbb004_num_cuenta_depreciacion_revaluacion` INT(11) NOT NULL,
  `fk_cbb004_num_cuenta_gastos` INT(11) NOT NULL,
  `fk_cbb004_num_cuenta_gastos_revaluacion` INT(11) NOT NULL,
  `fk_cbb004_num_cuenta_neto` INT(11) NOT NULL,
  `fk_cbb004_num_cuenta_rei` INT(11) NOT NULL,
  `fk_cbb004_num_cuenta_resultado` INT(11) NOT NULL,
  `num_flag_inventariable` INT(1) NOT NULL DEFAULT '0',
  `num_estatus` INT(1) NOT NULL DEFAULT '1',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_categoria`),
  UNIQUE INDEX `ind_codigo_UNIQUE` USING BTREE (`ind_codigo` ASC),
  INDEX `fk_c019_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_afc019_a006_num_tipo_idx` (`fk_a006_num_tipo` ASC),
  INDEX `fk_afc019_a006_num_grupo_idx` (`fk_a006_num_grupo` ASC),
  INDEX `fk_c019_cbb004_num_cuenta_historica_idx` (`fk_cbb004_num_cuenta_historica` ASC),
  INDEX `fk_c019_cbb004_num_cuenta_historica_variacion_idx` (`fk_cbb004_num_cuenta_historica_variacion` ASC),
  INDEX `fk_c019_cbb004_num_cuenta_historica_revaluacion_idx` (`fk_cbb004_num_cuenta_historica_revaluacion` ASC),
  INDEX `fk_c019_cbb004_num_cuenta_depreciacion_idx` (`fk_cbb004_num_cuenta_depreciacion` ASC),
  INDEX `fk_c019_cbb004_num_cuenta_depreciacion_variacion_idx` (`fk_cbb004_num_cuenta_depreciacion_variacion` ASC),
  INDEX `fk_c019_cbb004_num_cuenta_depreciacion_revaluacion_idx` (`fk_cbb004_num_cuenta_depreciacion_revaluacion` ASC),
  INDEX `fk_c019_cbb004_num_cuenta_gastos_idx` (`fk_cbb004_num_cuenta_gastos` ASC),
  INDEX `fk_c019_cbb004_num_cuenta_gastos_revaluacion_idx` (`fk_cbb004_num_cuenta_gastos_revaluacion` ASC),
  INDEX `fk_c019_cbb004_num_cuenta_neto_idx` (`fk_cbb004_num_cuenta_neto` ASC),
  INDEX `fk_c019_cbb004_num_cuenta_rei_idx` (`fk_cbb004_num_cuenta_rei` ASC),
  INDEX `fk_c019_cbb004_num_cuenta_resultado_idx` (`fk_cbb004_num_cuenta_resultado` ASC),
  CONSTRAINT `fk_c019_a018_num_seguridad_usuario_idx`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_c019_cbb004_num_cuenta_depreciacion_idx`
    FOREIGN KEY (`fk_cbb004_num_cuenta_depreciacion`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_c019_cbb004_num_cuenta_depreciacion_revaluacion_idx`
    FOREIGN KEY (`fk_cbb004_num_cuenta_depreciacion_revaluacion`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_c019_cbb004_num_cuenta_depreciacion_variacion_idx`
    FOREIGN KEY (`fk_cbb004_num_cuenta_depreciacion_variacion`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_c019_cbb004_num_cuenta_gastos_idx`
    FOREIGN KEY (`fk_cbb004_num_cuenta_gastos`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_c019_cbb004_num_cuenta_gastos_revaluacion_idx`
    FOREIGN KEY (`fk_cbb004_num_cuenta_gastos_revaluacion`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_c019_cbb004_num_cuenta_historica_idx`
    FOREIGN KEY (`fk_cbb004_num_cuenta_historica`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_c019_cbb004_num_cuenta_historica_revaluacion_idx`
    FOREIGN KEY (`fk_cbb004_num_cuenta_historica_revaluacion`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_c019_cbb004_num_cuenta_historica_variacion_idx`
    FOREIGN KEY (`fk_cbb004_num_cuenta_historica_variacion`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_c019_cbb004_num_cuenta_neto_idx`
    FOREIGN KEY (`fk_cbb004_num_cuenta_neto`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_c019_cbb004_num_cuenta_rei_idx`
    FOREIGN KEY (`fk_cbb004_num_cuenta_rei`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_c019_cbb004_num_cuenta_resultado_idx`
    FOREIGN KEY (`fk_cbb004_num_cuenta_resultado`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c021_tipo_transaccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c021_tipo_transaccion` (
  `pk_num_tipo_transaccion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_codigo` VARCHAR(3) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_nombre_transaccion` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_cbc003_num_voucher` INT(11) NOT NULL,
  `ind_tipo` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'A:Alta/B:Baja',
  `num_flag_sistema` INT(1) NOT NULL DEFAULT '0',
  `num_estatus` INT(1) NOT NULL DEFAULT '1',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_tipo_transaccion`),
  INDEX `fk_c021_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_c021_cbc003_num_voucher_idx` (`fk_cbc003_num_voucher` ASC),
  CONSTRAINT `fk_c021_a018_num_seguridad_usuario_idx`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 22
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cb_b002_libro_contable`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cb_b002_libro_contable` (
  `pk_num_libro_contable` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_libro` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT '',
  `ind_descripcion` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT '',
  `num_estatus` INT(1) NOT NULL COMMENT '1: Activo - 0: Inactivo',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_libro_contable`),
  UNIQUE INDEX `CodLibroCont_UNIQUE` (`cod_libro` ASC),
  INDEX `fk_cb_b002_libro_contable_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_cb_b002_libro_contable_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cb_b003_libro_contabilidad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cb_b003_libro_contabilidad` (
  `pk_num_libro_contabilidad` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_tipo_contabilidad` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_cbb002_num_libro_contable` INT(11) NOT NULL,
  `fk_cbb005_num_contabilidades` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_libro_contabilidad`),
  INDEX `fk_cb_b003_libro_contabilidad_cb_b002_libro_contable2_idx` (`fk_cbb002_num_libro_contable` ASC),
  INDEX `fk_cb_b003_libro_contabilidad_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cb_b003_libro_contabilidad_cb_b005_contabilidades1_idx` (`fk_cbb005_num_contabilidades` ASC),
  CONSTRAINT `fk_cb_b003_libro_contabilidad_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cb_b003_libro_contabilidad_cb_b002_libro_contable`
    FOREIGN KEY (`fk_cbb002_num_libro_contable`)
    REFERENCES `SIACE`.`cb_b002_libro_contable` (`pk_num_libro_contable`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cb_b003_libro_contabilidad_cb_b005_contabilidades1`
    FOREIGN KEY (`fk_cbb005_num_contabilidades`)
    REFERENCES `SIACE`.`cb_b005_contabilidades` (`pk_num_contabilidades`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cb_c002_sistema_fuente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cb_c002_sistema_fuente` (
  `pk_num_sistema_fuente` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_sistema_fuente` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL COMMENT '1: Activo - 0: Inactivo',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_sistema_fuente`),
  INDEX `fk_cb_c002_sistema_fuente_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_cb_c002_sistema_fuente_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cb_b001_voucher`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cb_b001_voucher` (
  `pk_num_voucher_mast` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_anio` YEAR NOT NULL,
  `ind_voucher` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_nro_voucher` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_creditos` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_debitos` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `fk_a003_num_preparado_por` INT(11) NOT NULL,
  `fec_fecha_preparacion` DATETIME NOT NULL,
  `fk_a003_num_aprobado_por` INT(11) NOT NULL,
  `fec_fecha_aprobacion` DATETIME NOT NULL,
  `fk_a003_num_anulado_por` INT(11) NOT NULL,
  `fec_fecha_anulacion` DATETIME NOT NULL,
  `txt_motivo_anulacion` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_periodo_anulacion` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `txt_titulo_voucher` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_fecha_voucher` DATETIME NOT NULL,
  `num_flag_transferencia` INT(1) NOT NULL,
  `ind_estatus` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'AB' COMMENT 'AB:ABIERTO; AP:APROBADO; MA:MAYORIZADO; AN:ANULADO;RE:RECHAZADO;',
  `fk_num_orden_pago` INT(11) NOT NULL COMMENT 'clave primaria tabla orden de pago',
  `fk_cbc002_num_sistema_fuente` INT(11) NOT NULL,
  `fk_cbc003_num_voucher` INT(11) NOT NULL,
  `fk_cbb005_num_contabilidades` INT(11) NOT NULL,
  `fk_cbb003_num_libro_contabilidad` INT(11) NOT NULL,
  `fk_a001_num_organismo` INT(11) NOT NULL,
  `fk_a004_num_dependencia` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_voucher_mast`),
  UNIQUE INDEX `voucher_UNIQUE` (`ind_voucher` ASC, `ind_anio` ASC, `ind_mes` ASC, `fk_cbb005_num_contabilidades` ASC),
  INDEX `fk_cb_b001_voucher_cb_c002_sistema_fuente1_idx` (`fk_cbc002_num_sistema_fuente` ASC),
  INDEX `fk_cb_b001_voucher_a001_organismo1_idx` (`fk_a001_num_organismo` ASC),
  INDEX `fk_cb_b001_voucher_a004_dependencia1_idx` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_cb_b001_voucher_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cb_b001_voucher_cb_c003_tipo_voucher1_idx` (`fk_cbc003_num_voucher` ASC),
  INDEX `fk_cb_b001_voucher_cb_b003_libro_contabilidad1_idx` (`fk_cbb003_num_libro_contabilidad` ASC),
  CONSTRAINT `fk_cb_b001_voucher_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cb_b001_voucher_cb_b003_libro_contabilidad1`
    FOREIGN KEY (`fk_cbb003_num_libro_contabilidad`)
    REFERENCES `SIACE`.`cb_b003_libro_contabilidad` (`pk_num_libro_contabilidad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cbb001_voucher_a001_organismo1`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cbb001_voucher_a004_dependencia1`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cbb001_voucher_cb_c002_sistema_fuente1`
    FOREIGN KEY (`fk_cbc002_num_sistema_fuente`)
    REFERENCES `SIACE`.`cb_c002_sistema_fuente` (`pk_num_sistema_fuente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 147
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_b001_activo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_b001_activo` (
  `pk_num_activo` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_afc005_num_ubicacion` INT(11) NOT NULL,
  `fk_afc007_num_situacion` INT(11) NOT NULL,
  `fk_afc008_num_clasificacion` INT(11) NOT NULL,
  `fk_afc009_num_movimiento` INT(11) NOT NULL,
  `fk_afc019_num_categoria` INT(11) NOT NULL,
  `fk_a003_num_persona_responsable` INT(11) NOT NULL,
  `fk_a003_num_persona_usuario` INT(11) NOT NULL,
  `fk_a006_num_tipo_activo` INT(11) NOT NULL,
  `fk_a006_num_estado_conservacion` INT(11) NOT NULL,
  `fk_a006_num_tipo_mejora` INT(11) NOT NULL,
  `fk_a006_num_naturaleza` INT(11) NOT NULL,
  `fk_a006_num_origen` INT(11) NOT NULL,
  `fk_a023_num_centro_costo` INT(11) NOT NULL,
  `ind_modelo` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_serie` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_serie_motor` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_placa` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_motor` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_asientos` INT(3) NOT NULL DEFAULT '0',
  `num_flag_depreciacion` INT(1) NOT NULL DEFAULT '0',
  `num_monto` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_valor_mercado` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_referencial` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `fec_inventario` DATE NULL DEFAULT NULL,
  `ind_nro_control` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_fecha_obligacion` DATE NULL DEFAULT NULL,
  `ind_nro_factura` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_factura` DATE NULL DEFAULT NULL,
  `ind_nro_orden_compra` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_orden_compra` DATE NULL DEFAULT NULL,
  `ind_nro_guia_remision` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_guia_remision` DATE NULL DEFAULT NULL,
  `ind_nro_documento_almacen` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_documento_almacen` DATE NULL DEFAULT NULL,
  `ind_material` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_dimensiones` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_nro_parte` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_anio` YEAR NULL DEFAULT NULL,
  `fec_ingreso` DATE NULL DEFAULT NULL,
  `ind_periodo_inicio_depreciacion` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_periodo_inicio_revaluacion` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_unidades` INT(3) NOT NULL DEFAULT '0',
  `num_flag_activo_tecnologico` INT(1) NOT NULL DEFAULT '0',
  `num_flag_mantenimiento` INT(1) NOT NULL DEFAULT '0',
  `cod_codigo_barra` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_flag_voucher_ingreso` INT(1) NOT NULL DEFAULT '0',
  `fk_afc016_num_tipo_seguro` INT(11) NULL DEFAULT NULL,
  `fk_afc017_num_tipo_vehiculo` INT(11) NULL DEFAULT NULL,
  `fk_afc018_num_poliza_seguro` INT(11) NULL DEFAULT NULL,
  `fk_a003_num_proveedor` INT(11) NULL DEFAULT NULL,
  `fk_a006_num_marca` INT(11) NULL DEFAULT NULL,
  `fk_a006_num_unidad_medida` INT(11) NULL DEFAULT NULL,
  `fk_a006_num_color` INT(11) NULL DEFAULT NULL,
  `fk_a008_num_pais` INT(11) NULL DEFAULT NULL,
  `fk_afb001_num_activo` INT(11) NULL DEFAULT NULL,
  `fk_cbb001_num_voucher_baja` INT(11) NULL DEFAULT NULL,
  `fk_cbb001_num_voucher_ingreso` INT(11) NULL DEFAULT NULL,
  `fk_cpb002_num_tipo_documento` INT(11) NULL DEFAULT NULL,
  `fk_afc021_num_tipo_transaccion` INT(11) NULL DEFAULT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'PE',
  `ind_descripcion` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_observaciones` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `cod_codigo_interno` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_activo`),
  INDEX `fk_af_b001_activo_af_c007_situacion_idx` (`fk_afc007_num_situacion` ASC),
  INDEX `fk_af_b001_activo_af_c008_clasificacion_idx` (`fk_afc008_num_clasificacion` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_afb0001_afc005_num_ubicacion` (`fk_afc005_num_ubicacion` ASC),
  INDEX `fk_afb0001_afc009_num_movimiento` (`fk_afc009_num_movimiento` ASC),
  INDEX `fk_afb0001_afc019_num_categoria` (`fk_afc019_num_categoria` ASC),
  INDEX `fk_afb0001_a003_num_persona_responsable` (`fk_a003_num_persona_responsable` ASC),
  INDEX `fk_afb0001_a003_num_persona_usuario` (`fk_a003_num_persona_usuario` ASC),
  INDEX `fk_afb0001_a006_num_tipo_activo` (`fk_a006_num_tipo_activo` ASC),
  INDEX `fk_afb0001_a006_num_estado_conservacion` (`fk_a006_num_estado_conservacion` ASC),
  INDEX `fk_afb0001_a006_num_tipo_mejora` (`fk_a006_num_tipo_mejora` ASC),
  INDEX `fk_afb0001_a006_num_naturaleza` (`fk_a006_num_naturaleza` ASC),
  INDEX `fk_afb0001_a006_num_origen` (`fk_a006_num_origen` ASC),
  INDEX `fk_afb0001_a023_num_centro_costo` (`fk_a023_num_centro_costo` ASC),
  INDEX `fk_afb0001_afc016_num_tipo_seguro` (`fk_afc016_num_tipo_seguro` ASC),
  INDEX `fk_afb0001_afc017_num_tipo_vehiculo` (`fk_afc017_num_tipo_vehiculo` ASC),
  INDEX `fk_afb0001_afc018_num_poliza_seguro` (`fk_afc018_num_poliza_seguro` ASC),
  INDEX `fk_afb0001_afc021_num_tipo_transaccion` (`fk_afc021_num_tipo_transaccion` ASC),
  INDEX `fk_afb0001_a003_num_proveedor` (`fk_a003_num_proveedor` ASC),
  INDEX `fk_afb0001_a006_num_marca` (`fk_a006_num_marca` ASC),
  INDEX `fk_afb0001_a006_num_unidad_medida` (`fk_a006_num_unidad_medida` ASC),
  INDEX `fk_afb0001_a006_num_color` (`fk_a006_num_color` ASC),
  INDEX `fk_afb0001_a008_num_pais` (`fk_a008_num_pais` ASC),
  INDEX `fk_afb0001_afb001_num_activo` (`fk_afb001_num_activo` ASC),
  INDEX `fk_afb0001_cbb001_num_voucher_baja` (`fk_cbb001_num_voucher_baja` ASC),
  INDEX `fk_afb0001_cbb001_num_voucher_ingreso` (`fk_cbb001_num_voucher_ingreso` ASC),
  INDEX `fk_afb0001_cpb002_num_tipo_documento` (`fk_cpb002_num_tipo_documento` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario55`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_af_b001_activo_af_c007_situacion`
    FOREIGN KEY (`fk_afc007_num_situacion`)
    REFERENCES `SIACE`.`af_c007_situacion` (`pk_num_situacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_af_b001_activo_af_c008_clasificacion`
    FOREIGN KEY (`fk_afc008_num_clasificacion`)
    REFERENCES `SIACE`.`af_c008_clasificacion` (`pk_num_clasificacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afb0001_a003_num_persona_responsable`
    FOREIGN KEY (`fk_a003_num_persona_responsable`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afb0001_a003_num_persona_usuario`
    FOREIGN KEY (`fk_a003_num_persona_usuario`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afb0001_a003_num_proveedor`
    FOREIGN KEY (`fk_a003_num_proveedor`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afb0001_a008_num_pais`
    FOREIGN KEY (`fk_a008_num_pais`)
    REFERENCES `SIACE`.`a008_pais` (`pk_num_pais`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afb0001_a023_num_centro_costo`
    FOREIGN KEY (`fk_a023_num_centro_costo`)
    REFERENCES `SIACE`.`a023_centro_costo` (`pk_num_centro_costo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afb0001_afc005_num_ubicacion`
    FOREIGN KEY (`fk_afc005_num_ubicacion`)
    REFERENCES `SIACE`.`af_c005_ubicacion` (`pk_num_ubicacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afb0001_afc016_num_tipo_seguro`
    FOREIGN KEY (`fk_afc016_num_tipo_seguro`)
    REFERENCES `SIACE`.`af_c016_tipo_seguro` (`pk_num_tipo_seguro`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afb0001_afc017_num_tipo_vehiculo`
    FOREIGN KEY (`fk_afc017_num_tipo_vehiculo`)
    REFERENCES `SIACE`.`af_c017_tipo_vehiculo` (`pk_num_tipo_vehiculo`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afb0001_afc018_num_poliza_seguro`
    FOREIGN KEY (`fk_afc018_num_poliza_seguro`)
    REFERENCES `SIACE`.`af_c018_poliza_seguro` (`pk_num_poliza_seguro`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afb0001_afc019_num_categoria`
    FOREIGN KEY (`fk_afc019_num_categoria`)
    REFERENCES `SIACE`.`af_c019_categoria` (`pk_num_categoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afb0001_afc021_num_tipo_transaccion`
    FOREIGN KEY (`fk_afc021_num_tipo_transaccion`)
    REFERENCES `SIACE`.`af_c021_tipo_transaccion` (`pk_num_tipo_transaccion`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afb0001_cbb001_num_voucher_baja`
    FOREIGN KEY (`fk_cbb001_num_voucher_baja`)
    REFERENCES `SIACE`.`cb_b001_voucher` (`pk_num_voucher_mast`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afb0001_cbb001_num_voucher_ingreso`
    FOREIGN KEY (`fk_cbb001_num_voucher_ingreso`)
    REFERENCES `SIACE`.`cb_b001_voucher` (`pk_num_voucher_mast`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_b002_traslado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_b002_traslado` (
  `pk_num_traslado` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_responsable` INT(11) NOT NULL,
  `fk_afb001_num_activo` INT(11) NOT NULL,
  `fec_fecha` DATE NOT NULL,
  `fec_hora` TIME NOT NULL,
  `num_estado` INT(1) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_traslado`),
  INDEX `fk_af_c009_traslado_a003_empleado_idx` (`fk_rhb001_responsable` ASC),
  INDEX `fk_af_b002_traslado_af_b001_activo1` (`fk_afb001_num_activo` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario56`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c001_depreciacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c001_depreciacion` (
  `pk_num_depreciacion` INT(11) NOT NULL AUTO_INCREMENT,
  `num_inflacion` DECIMAL(18,6) NOT NULL,
  `fec_inflacion` DATE NULL DEFAULT NULL,
  `fk_afb001_num_activo` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_depreciacion`),
  INDEX `fk_af_c001_depreciacion_af_b001_activo` (`fk_afb001_num_activo` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario57`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c002_activo_asignado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c002_activo_asignado` (
  `pk_num_id` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_afc005_num_ubicacion` INT(11) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_a004_num_dependencia` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_id`),
  INDEX `fk_af_c002_activo_asignado_af_c005_ubicacion_idx` (`fk_afc005_num_ubicacion` ASC),
  INDEX `fk_af_c002_activo_asignado_rh_b001_empleado_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_af_c002_activo_asignado_a004_dependencia_idx` (`fk_a004_num_dependencia` ASC),
  CONSTRAINT `fk_af_c002_activo_asignado_a004_dependencia`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_af_c002_activo_asignado_af_c005_ubicacion`
    FOREIGN KEY (`fk_afc005_num_ubicacion`)
    REFERENCES `SIACE`.`af_c005_ubicacion` (`pk_num_ubicacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c003_disponible`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c003_disponible` (
  `pk_num_disponible` INT(11) NOT NULL AUTO_INCREMENT,
  `num_disponibilidad_mantenimiento` INT(11) NOT NULL,
  `num_disponibilidad_operacion` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_disponible`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c004_faltante`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c004_faltante` (
  `pk_num_faltante` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_afc001_activo` INT(11) NOT NULL,
  `num_estado` INT(11) NOT NULL,
  `fec_fecha` DATE NOT NULL,
  `ind_comentario` VARCHAR(75) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_faltante`),
  INDEX `fk_af_c004_faltante_af_c001_activo_idx` (`fk_afc001_activo` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario58`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c009_tipo_movimiento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c009_tipo_movimiento` (
  `pk_num_movimiento` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a006_num_miscelaneo_detalle_tipo_movimiento` INT(11) NOT NULL,
  `ind_codigo` VARCHAR(3) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_nombre_movimiento` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_movimiento` INT(1) NOT NULL DEFAULT '0',
  `num_incorporacion` INT(1) NOT NULL DEFAULT '0',
  `num_desincorporacion` INT(1) NOT NULL DEFAULT '0',
  `num_estatus` INT(1) NOT NULL DEFAULT '1',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_movimiento`),
  UNIQUE INDEX `ind_codigo_UNIQUE` USING BTREE (`ind_codigo` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_af_c009_tipo_movimiento_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle_tipo_movimiento` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario59`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 36
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c010_ingreso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c010_ingreso` (
  `pk_num_ingreso` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_procedencia` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_afb002_num_traslado` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_ingreso`),
  INDEX `fk_af_c010_ingreso_af_b002_traslado` (`fk_afb002_num_traslado` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario60`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_af_c010_ingreso_af_b002_traslado`
    FOREIGN KEY (`fk_afb002_num_traslado`)
    REFERENCES `SIACE`.`af_b002_traslado` (`pk_num_traslado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c011_egreso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c011_egreso` (
  `pk_num_egreso` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_motivo` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_destino` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_afb002_num_traslado` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_egreso`),
  INDEX `fk_af_c011_egreso_af_b002_traslado` (`fk_afb002_num_traslado` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario61`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_af_c011_egreso_af_b002_traslado`
    FOREIGN KEY (`fk_afb002_num_traslado`)
    REFERENCES `SIACE`.`af_b002_traslado` (`pk_num_traslado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c012_movimiento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c012_movimiento` (
  `pk_num_movimiento` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a001_num_organismo` INT(11) NOT NULL,
  `fk_a006_num_tipo_acta` INT(11) NOT NULL COMMENT 'AFTIPOACTA',
  `fec_fecha` DATE NOT NULL,
  `num_anio` YEAR NOT NULL,
  `ind_nro_acta` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_tipo` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'I' COMMENT 'I:INTERNO / E:EXTERNO',
  `fk_a006_num_motivo` INT(11) NOT NULL COMMENT 'AFTIPOMOV',
  `fk_a003_num_preparado_por` INT(11) NULL DEFAULT NULL,
  `fecha_preparado` DATE NULL DEFAULT NULL,
  `fk_a003_num_revisado_por` INT(11) NULL DEFAULT NULL,
  `fecha_revisado` DATE NULL DEFAULT NULL,
  `fk_a003_num_aprobado_por` INT(11) NULL DEFAULT NULL,
  `fecha_aprobado` DATE NULL DEFAULT NULL,
  `fk_a003_num_anulado_por` INT(11) NULL DEFAULT NULL,
  `fecha_anulado` DATE NULL DEFAULT NULL,
  `ind_comentarios` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'PR' COMMENT 'PR:EN PREPARACIÓN / RV:REVISADO / AP:APROBADO / AN:ANULADO',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_movimiento`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_afc012_a006_num_tipo_movimiento` (`fk_a006_num_motivo` ASC),
  INDEX `fk_afc012_a003_num_preparado_por` (`fk_a003_num_preparado_por` ASC),
  INDEX `fk_afc012_a003_num_revisado_por` (`fk_a003_num_revisado_por` ASC),
  INDEX `fk_afc012_a003_num_aprobado_por` (`fk_a003_num_aprobado_por` ASC),
  INDEX `fk_afc012_a006_num_tipo_acta` (`fk_a006_num_tipo_acta` ASC),
  INDEX `fk_afc012_a001_num_organismo` (`fk_a001_num_organismo` ASC),
  INDEX `fk_afc012_a003_num_anulado_por` (`fk_a003_num_anulado_por` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario62`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc012_a001_num_organismo`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc012_a003_num_anulado_por`
    FOREIGN KEY (`fk_a003_num_anulado_por`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc012_a003_num_aprobado_por`
    FOREIGN KEY (`fk_a003_num_aprobado_por`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc012_a003_num_preparado_por`
    FOREIGN KEY (`fk_a003_num_preparado_por`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc012_a003_num_revisado_por`
    FOREIGN KEY (`fk_a003_num_revisado_por`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c013_adquisicion_costo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c013_adquisicion_costo` (
  `pk_num_activo_adquisicion` INT(11) NOT NULL AUTO_INCREMENT,
  `num_costo` DECIMAL(18,6) NOT NULL,
  `num_orden_compra` INT(11) NULL DEFAULT NULL,
  `fec_fecha_compra` DATE NULL DEFAULT NULL,
  `ind_factura` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_activo_adquisicion`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario54`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c014_fabricante`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c014_fabricante` (
  `pk_num_fabricante` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_nombre_fabricante` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_fabricante`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario63`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c015_fabricante_activo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c015_fabricante_activo` (
  `pk_num_fabricante_activo` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_afc014_num_fabricante` INT(11) NOT NULL,
  `fk_a008_num_pais` INT(11) NOT NULL,
  `fec_anio_fabricacion` YEAR NOT NULL,
  `ind_modelo` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_numero_serie` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_afb001_num_activo` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_fabricante_activo`),
  INDEX `fk_af_015_fabricante_activo_af_c014_fabricante_idx` (`fk_afc014_num_fabricante` ASC),
  INDEX `fk_af_015_fabricante_activo_a008_pais_idx` (`fk_a008_num_pais` ASC),
  INDEX `fk_af_c015_fabricante_activo_af_b001_activo` (`fk_afb001_num_activo` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario64`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_af_c015_fabricante_activo_a008_pais`
    FOREIGN KEY (`fk_a008_num_pais`)
    REFERENCES `SIACE`.`a008_pais` (`pk_num_pais`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_af_c015_fabricante_activo_af_c014_fabricante`
    FOREIGN KEY (`fk_afc014_num_fabricante`)
    REFERENCES `SIACE`.`af_c014_fabricante` (`pk_num_fabricante`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c020_categoria_contabilidad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c020_categoria_contabilidad` (
  `pk_num_categoria_contabilidad` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_afc019_num_categoria` INT(11) NOT NULL,
  `fk_cbb005_num_contabilidades` INT(11) NOT NULL,
  `num_depreciacion` DECIMAL(18,6) NOT NULL,
  PRIMARY KEY (`pk_num_categoria_contabilidad`),
  UNIQUE INDEX `fk_num_categoria_num_contabilidades_UNIQUE` USING BTREE (`fk_afc019_num_categoria` ASC, `fk_cbb005_num_contabilidades` ASC),
  INDEX `fk_afc020_afc019_num_categoria_idx` (`fk_afc019_num_categoria` ASC),
  INDEX `fk_afc020_cbb005_num_contabilidades_idx` (`fk_cbb005_num_contabilidades` ASC),
  CONSTRAINT `fk_afc020_afc019_num_categoria_idx`
    FOREIGN KEY (`fk_afc019_num_categoria`)
    REFERENCES `SIACE`.`af_c019_categoria` (`pk_num_categoria`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_afc020_cbb005_num_contabilidades_idx`
    FOREIGN KEY (`fk_cbb005_num_contabilidades`)
    REFERENCES `SIACE`.`cb_b005_contabilidades` (`pk_num_contabilidades`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c022_tipo_transaccion_cuenta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c022_tipo_transaccion_cuenta` (
  `pk_num_tipo_transaccion_cuenta` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_afc021_num_tipo_transaccion` INT(11) NOT NULL,
  `fk_afc019_num_categoria` INT(11) NOT NULL,
  `fk_cbb004_num_cuenta` INT(11) NOT NULL,
  `fk_cbb005_num_contabilidades` INT(11) NOT NULL,
  `ind_descripcion` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_signo` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT '+/-',
  PRIMARY KEY (`pk_num_tipo_transaccion_cuenta`),
  INDEX `fk_afc022_afc021_num_tipo_transaccion_idx` (`fk_afc021_num_tipo_transaccion` ASC),
  INDEX `fk_afc022_afc019_num_categoria_idx` (`fk_afc019_num_categoria` ASC),
  INDEX `fk_afc022_cbb004_num_cuenta_idx` (`fk_cbb004_num_cuenta` ASC),
  INDEX `fk_afc022_cbb005_num_contabilidades_idx` (`fk_cbb005_num_contabilidades` ASC),
  CONSTRAINT `fk_afc022_afc019_num_categoria_idx`
    FOREIGN KEY (`fk_afc019_num_categoria`)
    REFERENCES `SIACE`.`af_c019_categoria` (`pk_num_categoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc022_afc021_num_tipo_transaccion_idx`
    FOREIGN KEY (`fk_afc021_num_tipo_transaccion`)
    REFERENCES `SIACE`.`af_c021_tipo_transaccion` (`pk_num_tipo_transaccion`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_afc022_cbb004_num_cuenta_idx`
    FOREIGN KEY (`fk_cbb004_num_cuenta`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc022_cbb005_num_contabilidades_idx`
    FOREIGN KEY (`fk_cbb005_num_contabilidades`)
    REFERENCES `SIACE`.`cb_b005_contabilidades` (`pk_num_contabilidades`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 63
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c023_movimiento_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c023_movimiento_detalle` (
  `pk_num_movimiento_detalle` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_afc012_num_movimiento` INT(11) NOT NULL,
  `fk_afb001_num_activo` INT(11) NOT NULL,
  `fk_a023_num_centro_costo` INT(11) NOT NULL,
  `fk_a023_num_centro_costo_ant` INT(11) NOT NULL,
  `fk_afc005_num_ubicacion` INT(11) NOT NULL,
  `fk_afc005_num_ubicacion_ant` INT(11) NOT NULL,
  `fk_a003_num_usuario` INT(11) NOT NULL,
  `fk_a003_num_usuario_ant` INT(11) NOT NULL,
  `fk_a003_num_responsable` INT(11) NOT NULL,
  `fk_a003_num_responsable_ant` INT(11) NOT NULL,
  `fk_afc009_num_movimiento_inc` INT(11) NOT NULL,
  `fk_afc009_num_movimiento_des` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_movimiento_detalle`),
  INDEX `fk_afc023_afc012_num_movimiento` (`fk_afc012_num_movimiento` ASC),
  INDEX `fk_afc023_afb001_num_activo` (`fk_afb001_num_activo` ASC),
  INDEX `fk_afc023_a023_num_centro_costo` (`fk_a023_num_centro_costo` ASC),
  INDEX `fk_afc023_a023_num_centro_costo_ant` (`fk_a023_num_centro_costo_ant` ASC),
  INDEX `fk_afc023_afc005_num_ubicacion` (`fk_afc005_num_ubicacion` ASC),
  INDEX `fk_afc023_afc005_num_ubicacion_ant` (`fk_afc005_num_ubicacion_ant` ASC),
  INDEX `fk_afc023_a003_num_usuario` (`fk_a003_num_usuario` ASC),
  INDEX `fk_afc023_a003_num_usuario_ant` (`fk_a003_num_usuario_ant` ASC),
  INDEX `fk_afc023_a003_num_responsable` (`fk_a003_num_responsable` ASC),
  INDEX `fk_afc023_a003_num_responsable_ant` (`fk_a003_num_responsable_ant` ASC),
  INDEX `fk_afc023_afc009_num_movimiento_inc` (`fk_afc009_num_movimiento_inc` ASC),
  INDEX `fk_afc023_afc009_num_movimiento_des` (`fk_afc009_num_movimiento_des` ASC),
  CONSTRAINT `fk_afc023_a003_num_responsable`
    FOREIGN KEY (`fk_a003_num_responsable`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc023_a003_num_responsable_ant`
    FOREIGN KEY (`fk_a003_num_responsable_ant`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc023_a003_num_usuario`
    FOREIGN KEY (`fk_a003_num_usuario`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc023_a003_num_usuario_ant`
    FOREIGN KEY (`fk_a003_num_usuario_ant`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc023_a023_num_centro_costo`
    FOREIGN KEY (`fk_a023_num_centro_costo`)
    REFERENCES `SIACE`.`a023_centro_costo` (`pk_num_centro_costo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc023_a023_num_centro_costo_ant`
    FOREIGN KEY (`fk_a023_num_centro_costo_ant`)
    REFERENCES `SIACE`.`a023_centro_costo` (`pk_num_centro_costo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc023_afb001_num_activo`
    FOREIGN KEY (`fk_afb001_num_activo`)
    REFERENCES `SIACE`.`af_b001_activo` (`pk_num_activo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc023_afc005_num_ubicacion`
    FOREIGN KEY (`fk_afc005_num_ubicacion`)
    REFERENCES `SIACE`.`af_c005_ubicacion` (`pk_num_ubicacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc023_afc005_num_ubicacion_ant`
    FOREIGN KEY (`fk_afc005_num_ubicacion_ant`)
    REFERENCES `SIACE`.`af_c005_ubicacion` (`pk_num_ubicacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc023_afc009_num_movimiento_des`
    FOREIGN KEY (`fk_afc009_num_movimiento_des`)
    REFERENCES `SIACE`.`af_c009_tipo_movimiento` (`pk_num_movimiento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc023_afc009_num_movimiento_inc`
    FOREIGN KEY (`fk_afc009_num_movimiento_inc`)
    REFERENCES `SIACE`.`af_c009_tipo_movimiento` (`pk_num_movimiento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc023_afc012_num_movimiento`
    FOREIGN KEY (`fk_afc012_num_movimiento`)
    REFERENCES `SIACE`.`af_c012_movimiento` (`pk_num_movimiento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c063_puestos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c063_puestos` (
  `pk_num_puestos` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhc006_num_cargo` INT(11) NOT NULL,
  `fk_rhc007_num_grado` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_paso` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_grupoocupacional` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_categoria` INT(11) NOT NULL,
  `fk_rhc009_num_nivel` INT(11) NOT NULL,
  `fk_rhc010_num_serie` INT(11) NOT NULL,
  `ind_descripcion_cargo` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `txt_descripcion_gen` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_sueldo_basico` DECIMAL(18,6) NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_puestos`),
  INDEX `fk_rh_c063_puestos_a006_miscelaneo_detalle_idx` USING BTREE (`fk_a006_num_miscelaneo_detalle_grupoocupacional` ASC),
  INDEX `fk_rh_c063_puestos_a006_num_miscelaneo_detalle_categoria` (`fk_a006_num_miscelaneo_detalle_categoria` ASC),
  INDEX `fk_rh_c063_puestos_rh_c006_tipo_cargo1_idx` (`fk_rhc006_num_cargo` ASC),
  INDEX `fk_rh_c063_puestos_rh_c007_grado_salarial1_idx1` (`fk_rhc007_num_grado` ASC),
  INDEX `fk_rh_c063_puestos_rh_c009_nivel1_idx1` (`fk_rhc009_num_nivel` ASC),
  INDEX `fk_rh_c063_puestos_rh_c010_serie1_idx1` (`fk_rhc010_num_serie` ASC),
  INDEX `fk_rh_c063_puestos_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 34
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c024_acta_responsabilidad_uso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c024_acta_responsabilidad_uso` (
  `pk_num_acta_responsabilidad_uso` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_afb001_num_activo` INT(11) NOT NULL,
  `fk_a003_num_responsable` INT(11) NOT NULL,
  `fk_rhc063_num_responsable_puesto` INT(11) NOT NULL,
  `fk_a003_num_usuario` INT(11) NOT NULL,
  `fk_rhc063_num_usuario_puesto` INT(11) NOT NULL,
  `fk_a004_num_dependencia` INT(11) NOT NULL,
  `fec_fecha` DATE NOT NULL,
  `num_anio` YEAR NOT NULL,
  `ind_nro_acta` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_acta_responsabilidad_uso`),
  INDEX `fk_afc024_afb001_num_activo` (`fk_afb001_num_activo` ASC),
  INDEX `fk_afc024_a003_num_responsable` (`fk_a003_num_responsable` ASC),
  INDEX `fk_afc024_rhc063_num_responsable_puesto` (`fk_rhc063_num_responsable_puesto` ASC),
  INDEX `fk_afc024_a003_num_usuario` (`fk_a003_num_usuario` ASC),
  INDEX `fk_afc024_rhc063_num_usuario_puesto` (`fk_rhc063_num_usuario_puesto` ASC),
  INDEX `fk_afc024_a004_num_dependencia` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_afc024_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_afc024_a003_num_responsable`
    FOREIGN KEY (`fk_a003_num_responsable`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc024_a003_num_usuario`
    FOREIGN KEY (`fk_a003_num_usuario`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc024_a004_num_dependencia`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc024_a018_num_seguridad_usuario`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc024_afb001_num_activo`
    FOREIGN KEY (`fk_afb001_num_activo`)
    REFERENCES `SIACE`.`af_b001_activo` (`pk_num_activo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc024_rhc063_num_responsable_puesto`
    FOREIGN KEY (`fk_rhc063_num_responsable_puesto`)
    REFERENCES `SIACE`.`rh_c063_puestos` (`pk_num_puestos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc024_rhc063_num_usuario_puesto`
    FOREIGN KEY (`fk_rhc063_num_usuario_puesto`)
    REFERENCES `SIACE`.`rh_c063_puestos` (`pk_num_puestos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 13
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c025_baja_activo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c025_baja_activo` (
  `pk_num_baja_activo` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_afb001_num_activo` INT(11) NOT NULL,
  `fk_afc021_num_tipo_transaccion` INT(11) NOT NULL,
  `fk_afc009_num_movimiento` INT(11) NOT NULL,
  `fk_a006_num_motivo` INT(11) NOT NULL,
  `fec_fecha` DATE NOT NULL,
  `fec_fecha_baja` DATE NOT NULL,
  `ind_comentarios` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_nro_resolucion` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_flag_acta_generada` INT(1) NOT NULL DEFAULT '0',
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'PE',
  `fk_a003_num_preparado_por` INT(11) NULL DEFAULT NULL,
  `fecha_preparado` DATE NULL DEFAULT NULL,
  `fk_a003_num_conformado_por` INT(11) NULL DEFAULT NULL,
  `fecha_conformado` DATE NULL DEFAULT NULL,
  `fk_a003_num_aprobado_por` INT(11) NULL DEFAULT NULL,
  `fecha_aprobado` DATE NULL DEFAULT NULL,
  `fk_a003_num_anulado_por` INT(11) NULL DEFAULT NULL,
  `fecha_anulado` DATE NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_baja_activo`),
  INDEX `fk_afc025_afb001_num_activo` (`fk_afb001_num_activo` ASC),
  INDEX `fk_afC025_afc021_num_tipo_transaccion` (`fk_afc021_num_tipo_transaccion` ASC),
  INDEX `fk_afC025_afc009_num_movimiento` (`fk_afc009_num_movimiento` ASC),
  INDEX `fk_afC025_a006_num_motivo` (`fk_a006_num_motivo` ASC),
  INDEX `fk_afc025_a003_num_preparado_por` (`fk_a003_num_preparado_por` ASC),
  INDEX `fk_afc025_a003_num_conformado_por` (`fk_a003_num_conformado_por` ASC),
  INDEX `fk_afc025_a003_num_aprobado_por` (`fk_a003_num_aprobado_por` ASC),
  INDEX `fk_afc025_a003_num_anulado_por` (`fk_a003_num_anulado_por` ASC),
  CONSTRAINT `fk_afC025_afc009_num_movimiento`
    FOREIGN KEY (`fk_afc009_num_movimiento`)
    REFERENCES `SIACE`.`af_c009_tipo_movimiento` (`pk_num_movimiento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afC025_afc021_num_tipo_transaccion`
    FOREIGN KEY (`fk_afc021_num_tipo_transaccion`)
    REFERENCES `SIACE`.`af_c021_tipo_transaccion` (`pk_num_tipo_transaccion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc024_a003_num_anulado_por`
    FOREIGN KEY (`fk_a003_num_anulado_por`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc024_a003_num_aprobado_por`
    FOREIGN KEY (`fk_a003_num_aprobado_por`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc024_a003_num_conformado_por`
    FOREIGN KEY (`fk_a003_num_conformado_por`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc024_a003_num_preparado_por`
    FOREIGN KEY (`fk_a003_num_preparado_por`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc025_afb001_num_activo`
    FOREIGN KEY (`fk_afb001_num_activo`)
    REFERENCES `SIACE`.`af_b001_activo` (`pk_num_activo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c026_baja_activo_cuenta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c026_baja_activo_cuenta` (
  `pk_num_baja_activo_cuenta` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_afc025_num_baja_activo` INT(11) NOT NULL,
  `fk_afc019_num_categoria` INT(11) NOT NULL,
  `fk_cbb004_num_cuenta` INT(11) NOT NULL,
  `fk_cbb005_num_contabilidades` INT(11) NOT NULL,
  `ind_descripcion` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_signo` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT '+/-',
  PRIMARY KEY (`pk_num_baja_activo_cuenta`),
  INDEX `fk_afc026_afc025_num_baja_activo` (`fk_afc025_num_baja_activo` ASC),
  INDEX `fk_afc026_afc019_num_categoria` (`fk_afc019_num_categoria` ASC),
  INDEX `fk_afc026_cbb004_num_cuenta` (`fk_cbb004_num_cuenta` ASC),
  INDEX `fk_afc026_cbb005_num_contabilidades` (`fk_cbb005_num_contabilidades` ASC),
  CONSTRAINT `fk_afc026_afc019_num_categoria`
    FOREIGN KEY (`fk_afc019_num_categoria`)
    REFERENCES `SIACE`.`af_c019_categoria` (`pk_num_categoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc026_afc025_num_baja_activo`
    FOREIGN KEY (`fk_afc025_num_baja_activo`)
    REFERENCES `SIACE`.`af_c025_baja_activo` (`pk_num_baja_activo`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_afc026_cbb004_num_cuenta`
    FOREIGN KEY (`fk_cbb004_num_cuenta`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc026_cbb005_num_contabilidades`
    FOREIGN KEY (`fk_cbb005_num_contabilidades`)
    REFERENCES `SIACE`.`cb_b005_contabilidades` (`pk_num_contabilidades`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 155
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c027_acta_incorporacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c027_acta_incorporacion` (
  `pk_num_acta_incorporacion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_afb001_num_activo` INT(11) NOT NULL,
  `fk_a003_num_responsable` INT(11) NOT NULL,
  `fk_rhc063_num_responsable_puesto` INT(11) NOT NULL,
  `fk_a003_num_aprobado_por` INT(11) NOT NULL,
  `fk_rhc063_num_aprobado_por_puesto` INT(11) NOT NULL,
  `fk_a003_num_conformado_por` INT(11) NOT NULL,
  `fk_rhc063_num_conformado_por_puesto` INT(11) NOT NULL,
  `fk_a004_num_dependencia` INT(11) NOT NULL,
  `fec_fecha` DATE NOT NULL,
  `num_anio` YEAR NOT NULL,
  `ind_nro_acta` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_acta_incorporacion`),
  INDEX `fk_afc027_afb001_num_activo` (`fk_afb001_num_activo` ASC),
  INDEX `fk_afc027_a003_num_responsable` (`fk_a003_num_responsable` ASC),
  INDEX `fk_afc027_rhc063_num_responsable_puesto` (`fk_rhc063_num_responsable_puesto` ASC),
  INDEX `fk_afc027_a003_num_aprobado_por` (`fk_a003_num_aprobado_por` ASC),
  INDEX `fk_afc027_rhc063_num_aprobado_por_puesto` (`fk_rhc063_num_aprobado_por_puesto` ASC),
  INDEX `fk_afc027_a003_num_conformado_por` (`fk_a003_num_conformado_por` ASC),
  INDEX `fk_afc027_rhc063_num_conformado_por_puesto` (`fk_rhc063_num_conformado_por_puesto` ASC),
  INDEX `fk_afc027_a004_num_dependencia` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_afc027_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_afc027_a003_num_aprobado_por`
    FOREIGN KEY (`fk_a003_num_aprobado_por`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc027_a003_num_conformado_por`
    FOREIGN KEY (`fk_a003_num_conformado_por`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc027_a003_num_responsable`
    FOREIGN KEY (`fk_a003_num_responsable`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc027_a004_num_dependencia`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc027_a018_num_seguridad_usuario`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc027_afb001_num_activo`
    FOREIGN KEY (`fk_afb001_num_activo`)
    REFERENCES `SIACE`.`af_b001_activo` (`pk_num_activo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc027_rhc063_num_aprobado_por_puesto`
    FOREIGN KEY (`fk_rhc063_num_aprobado_por_puesto`)
    REFERENCES `SIACE`.`rh_c063_puestos` (`pk_num_puestos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc027_rhc063_num_conformado_por_puesto`
    FOREIGN KEY (`fk_rhc063_num_conformado_por_puesto`)
    REFERENCES `SIACE`.`rh_c063_puestos` (`pk_num_puestos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc027_rhc063_num_responsable_puesto`
    FOREIGN KEY (`fk_rhc063_num_responsable_puesto`)
    REFERENCES `SIACE`.`rh_c063_puestos` (`pk_num_puestos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c028_acta_entrega`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c028_acta_entrega` (
  `pk_num_acta_entrega` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_afb001_num_activo` INT(11) NOT NULL,
  `fk_a003_num_aprobado_por` INT(11) NOT NULL,
  `fk_rhc063_num_aprobado_por_puesto` INT(11) NOT NULL,
  `fk_a003_num_conformado_por` INT(11) NOT NULL,
  `fk_rhc063_num_conformado_por_puesto` INT(11) NOT NULL,
  `fk_a004_num_dependencia` INT(11) NOT NULL,
  `fec_fecha` DATE NOT NULL,
  `num_anio` YEAR NOT NULL,
  `ind_nro_acta` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_acta_entrega`),
  INDEX `fk_afc028_afb001_num_activo` (`fk_afb001_num_activo` ASC),
  INDEX `fk_afc028_a003_num_aprobado_por` (`fk_a003_num_aprobado_por` ASC),
  INDEX `fk_afc028_rhc063_num_aprobado_por_puesto` (`fk_rhc063_num_aprobado_por_puesto` ASC),
  INDEX `fk_afc028_a003_num_conformado_por` (`fk_a003_num_conformado_por` ASC),
  INDEX `fk_afc028_rhc063_num_conformado_por_puesto` (`fk_rhc063_num_conformado_por_puesto` ASC),
  INDEX `fk_afc028_a004_num_dependencia` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_afc028_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_afc028_a003_num_aprobado_por`
    FOREIGN KEY (`fk_a003_num_aprobado_por`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc028_a003_num_conformado_por`
    FOREIGN KEY (`fk_a003_num_conformado_por`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc028_a004_num_dependencia`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc028_a018_num_seguridad_usuario`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc028_afb001_num_activo`
    FOREIGN KEY (`fk_afb001_num_activo`)
    REFERENCES `SIACE`.`af_b001_activo` (`pk_num_activo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc028_rhc063_num_aprobado_por_puesto`
    FOREIGN KEY (`fk_rhc063_num_aprobado_por_puesto`)
    REFERENCES `SIACE`.`rh_c063_puestos` (`pk_num_puestos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc028_rhc063_num_conformado_por_puesto`
    FOREIGN KEY (`fk_rhc063_num_conformado_por_puesto`)
    REFERENCES `SIACE`.`rh_c063_puestos` (`pk_num_puestos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 14
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c029_activo_historico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c029_activo_historico` (
  `pk_num_historico` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_afb001_num_activo` INT(11) NOT NULL,
  `fk_a023_num_centro_costo` INT(11) NOT NULL,
  `fk_afc007_num_situacion` INT(11) NOT NULL,
  `fk_afc009_num_movimiento` INT(11) NOT NULL,
  `fk_afc005_num_ubicacion` INT(11) NOT NULL,
  `fk_a006_num_motivo` INT(11) NOT NULL,
  `cod_codigo_interno` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_fecha_ingreso` DATE NOT NULL,
  `fec_fecha_transaccion` DATE NOT NULL,
  `ind_tipo` ENUM('I','E') CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_historico`),
  INDEX `fk_afc029_afb001_num_activo` (`fk_afb001_num_activo` ASC),
  INDEX `fk_afc029_a023_num_centro_costo` (`fk_a023_num_centro_costo` ASC),
  INDEX `fk_afc029_afc007_situacion` (`fk_afc007_num_situacion` ASC),
  INDEX `fk_afc029_afc009_num_movimiento` (`fk_afc009_num_movimiento` ASC),
  INDEX `fk_afc029_afc005_num_ubicacion` (`fk_afc005_num_ubicacion` ASC),
  INDEX `fk_afc029_a006_num_motivo` (`fk_a006_num_motivo` ASC),
  INDEX `fk_afc029_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_afc029_a018_num_seguridad_usuario`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc029_a023_num_centro_costo`
    FOREIGN KEY (`fk_a023_num_centro_costo`)
    REFERENCES `SIACE`.`a023_centro_costo` (`pk_num_centro_costo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc029_afb001_num_activo`
    FOREIGN KEY (`fk_afb001_num_activo`)
    REFERENCES `SIACE`.`af_b001_activo` (`pk_num_activo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc029_afc005_num_ubicacion`
    FOREIGN KEY (`fk_afc005_num_ubicacion`)
    REFERENCES `SIACE`.`af_c005_ubicacion` (`pk_num_ubicacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc029_afc007_situacion`
    FOREIGN KEY (`fk_afc007_num_situacion`)
    REFERENCES `SIACE`.`af_c007_situacion` (`pk_num_situacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc029_afc009_num_movimiento`
    FOREIGN KEY (`fk_afc009_num_movimiento`)
    REFERENCES `SIACE`.`af_c009_tipo_movimiento` (`pk_num_movimiento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 19
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c030_activos_faltantes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c030_activos_faltantes` (
  `pk_num_activo_faltante` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_afc009_num_movimiento` INT(11) NOT NULL,
  `fec_fecha` DATE NOT NULL,
  `num_anio` YEAR NOT NULL,
  `ind_codigo` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_comentarios` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a001_num_organismo_externo` INT(11) NULL DEFAULT NULL,
  `fec_fecha_informe` DATE NULL DEFAULT NULL,
  `ind_comentarios_informe` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a003_num_preparado_por` INT(11) NULL DEFAULT NULL,
  `fecha_preparado` DATE NULL DEFAULT NULL,
  `fk_a003_num_revisado_por` INT(11) NULL DEFAULT NULL,
  `fecha_revisado` DATE NULL DEFAULT NULL,
  `fk_a003_num_aprobado_por` INT(11) NULL DEFAULT NULL,
  `fecha_aprobado` DATE NULL DEFAULT NULL,
  `fk_a003_num_anulado_por` INT(11) NULL DEFAULT NULL,
  `fecha_anulado` DATE NULL DEFAULT NULL,
  `ind_situacion` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'PE',
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'PR',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_activo_faltante`),
  INDEX `fk_afC030_afc009_num_movimiento` (`fk_afc009_num_movimiento` ASC),
  INDEX `fk_afc030_a003_num_preparado_por` (`fk_a003_num_preparado_por` ASC),
  INDEX `fk_afc030_a003_num_revisado_por` (`fk_a003_num_revisado_por` ASC),
  INDEX `fk_afc030_a003_num_aprobado_por` (`fk_a003_num_aprobado_por` ASC),
  INDEX `fk_afc030_a003_num_anulado_por` (`fk_a003_num_anulado_por` ASC),
  INDEX `fk_afc030_a001_num_organismo_externo` (`fk_a001_num_organismo_externo` ASC),
  CONSTRAINT `fk_afC030_afc009_num_movimiento`
    FOREIGN KEY (`fk_afc009_num_movimiento`)
    REFERENCES `SIACE`.`af_c009_tipo_movimiento` (`pk_num_movimiento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc030_a001_num_organismo_externo`
    FOREIGN KEY (`fk_a001_num_organismo_externo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc030_a003_num_anulado_por`
    FOREIGN KEY (`fk_a003_num_anulado_por`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc030_a003_num_aprobado_por`
    FOREIGN KEY (`fk_a003_num_aprobado_por`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc030_a003_num_preparado_por`
    FOREIGN KEY (`fk_a003_num_preparado_por`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc030_a003_num_revisado_por`
    FOREIGN KEY (`fk_a003_num_revisado_por`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`af_c031_activos_faltantes_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`af_c031_activos_faltantes_detalle` (
  `pk_num_activo_faltante_detalle` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_afc030_num_activo_faltante` INT(11) NOT NULL,
  `fk_afb001_num_activo` INT(11) NOT NULL,
  `ind_comentarios` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  PRIMARY KEY (`pk_num_activo_faltante_detalle`),
  INDEX `fk_afc031_afc030_num_activo_faltante` (`fk_afc030_num_activo_faltante` ASC),
  INDEX `fk_afc023_afb001_num_activo` (`fk_afb001_num_activo` ASC),
  CONSTRAINT `fk_afc031_afb001_num_activo`
    FOREIGN KEY (`fk_afb001_num_activo`)
    REFERENCES `SIACE`.`af_b001_activo` (`pk_num_activo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_afc031_afc030_num_activo_faltante`
    FOREIGN KEY (`fk_afc030_num_activo_faltante`)
    REFERENCES `SIACE`.`af_c030_activos_faltantes` (`pk_num_activo_faltante`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cb_c001_voucher_det`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cb_c001_voucher_det` (
  `pk_num_voucher_det` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `num_linea` INT(3) NOT NULL,
  `num_debe` DECIMAL(18,6) NOT NULL,
  `num_haber` DECIMAL(18,6) NOT NULL,
  `fk_a003_num_persona` INT(11) NOT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_estatus` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a023_num_centro_costo` INT(11) NOT NULL,
  `fk_cbb001_num_voucher_mast` INT(11) NOT NULL,
  `fk_cbb004_num_cuenta` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fk_cbb005_num_contabilidades` INT(11) NOT NULL,
  `fk_cbb003_num_libro_contabilidad` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_voucher_det`, `fk_cbb005_num_contabilidades`, `fk_cbb003_num_libro_contabilidad`),
  UNIQUE INDEX ` voucher_det_UNIQUE` USING BTREE (`ind_mes` ASC, `fec_anio` ASC, `num_linea` ASC, `fk_cbb001_num_voucher_mast` ASC),
  INDEX `fk_cb_c001_voucher_det_a023_centro_costo2_idx` (`fk_a023_num_centro_costo` ASC),
  INDEX `fk_cb_c001_voucher_det_cb_b001_voucher1_idx1` (`fk_cbb001_num_voucher_mast` ASC),
  INDEX `fk_cb_c001_voucher_det_cb_b004_plan_cuenta1_idx` (`fk_cbb004_num_cuenta` ASC),
  INDEX `fk_cb_c001_voucher_det_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cb_c001_voucher_det_cb_b005_contabilidades1_idx` (`fk_cbb005_num_contabilidades` ASC),
  INDEX `fk_cb_c001_voucher_det_cb_b003_libro_contabilidad1_idx` (`fk_cbb003_num_libro_contabilidad` ASC),
  CONSTRAINT `fk_cb_c001_voucher_det_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cb_c001_voucher_det_cb_b001_voucher1`
    FOREIGN KEY (`fk_cbb001_num_voucher_mast`)
    REFERENCES `SIACE`.`cb_b001_voucher` (`pk_num_voucher_mast`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cb_c001_voucher_det_cb_b003_libro_contabilidad1`
    FOREIGN KEY (`fk_cbb003_num_libro_contabilidad`)
    REFERENCES `SIACE`.`cb_b003_libro_contabilidad` (`pk_num_libro_contabilidad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cb_c001_voucher_det_cb_b004_plan_cuenta1`
    FOREIGN KEY (`fk_cbb004_num_cuenta`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cb_c001_voucher_det_cb_b005_contabilidades1`
    FOREIGN KEY (`fk_cbb005_num_contabilidades`)
    REFERENCES `SIACE`.`cb_b005_contabilidades` (`pk_num_contabilidades`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 415
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cb_c003_tipo_voucher`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cb_c003_tipo_voucher` (
  `pk_num_voucher` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_voucher` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_manual` INT(1) NOT NULL,
  `num_estatus` INT(1) NOT NULL COMMENT '1: Activo - 0: Inactivo',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_voucher`),
  UNIQUE INDEX `cod_voucher_UNIQUE` (`cod_voucher` ASC),
  INDEX `fk_cb_c003_tipo_voucher_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_cb_c003_tipo_voucher_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 44
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cb_c004_voucher_balance`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cb_c004_voucher_balance` (
  `pk_cod_balance` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `num_saldo_inicial` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_saldo_balance` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `fk_cbb003_num_libro_contabilidad` INT(11) NOT NULL,
  `fk_cbb004_num_cuenta` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `num_saldo_final` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_saldo_balance_actual` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`pk_cod_balance`),
  UNIQUE INDEX `voucher_balance_UNIQUE` USING BTREE (`fec_anio` ASC, `ind_mes` ASC, `fk_cbb004_num_cuenta` ASC, `fk_cbb003_num_libro_contabilidad` ASC),
  INDEX `ac_mast_plan_cuenta` (`fk_cbb004_num_cuenta` ASC),
  INDEX `fk_cb_c004_voucher_balance_cb_b004_plan_cuenta1_idx` (`fk_cbb004_num_cuenta` ASC),
  INDEX `fk_cb_c004_voucher_balance_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cb_c004_voucher_balance_cb_b003_libro_contabilidad1_idx` (`fk_cbb003_num_libro_contabilidad` ASC),
  CONSTRAINT `fk_cb_c004_voucher_balance_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cb_c004_voucher_balance_cb_b003_libro_contabilidad1`
    FOREIGN KEY (`fk_cbb003_num_libro_contabilidad`)
    REFERENCES `SIACE`.`cb_b003_libro_contabilidad` (`pk_num_libro_contabilidad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cb_c004_voucher_balance_cb_b004_plan_cuenta1`
    FOREIGN KEY (`fk_cbb004_num_cuenta`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 793
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cb_c005_control_cierre_mensual`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cb_c005_control_cierre_mensual` (
  `pk_num_control_cierre` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_tipo_registro` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'AB:PERIODO ABIERTO; AC:PERIODO ACTUAL;',
  `ind_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_anio` YEAR NULL DEFAULT NULL,
  `ind_estatus` VARCHAR(1) CHARACTER SET 'utf8' NOT NULL DEFAULT 'A' COMMENT 'A:ABIERTO; C:CERRADO;',
  `fk_cbb002_cod_libro_contable` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_control_cierre`),
  UNIQUE INDEX `Periodo_UNIQUE` USING BTREE (`ind_tipo_registro` ASC, `fec_anio` ASC, `ind_mes` ASC),
  INDEX `FK_ac_controlcierremensual_1` (`pk_num_control_cierre` ASC),
  INDEX `fk_cb_c005_control_cierre_mensual_cb_b002_libro_contable1_idx` (`fk_cbb002_cod_libro_contable` ASC),
  INDEX `fk_cb_c005_control_cierre_mensual_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_cb_c005_control_cierre_mensual_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cb_c005_control_cierre_mensual_cb_b002_libro_contable`
    FOREIGN KEY (`fk_cbb002_cod_libro_contable`)
    REFERENCES `SIACE`.`cb_b002_libro_contable` (`pk_num_libro_contable`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cb_c006_voucher_det_his`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cb_c006_voucher_det_his` (
  `pk_num_voucher_det_his` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_cbb001_num_voucher_mast` INT(11) NOT NULL,
  `fk_cbc001_num_voucher_det` INT(11) NOT NULL,
  `fk_cb_b004_cod_cuenta` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_voucher_det_his`),
  INDEX `fk_cb_c006_voucher_det_his_cb_b001_voucher1_idx` (`fk_cbb001_num_voucher_mast` ASC),
  INDEX `fk_cb_c006_voucher_det_his_cb_c001_voucher_det1_idx` (`fk_cbc001_num_voucher_det` ASC),
  INDEX `fk_cb_c006_voucher_det_his_cb_b004_plan_cuenta1_idx1` (`fk_cb_b004_cod_cuenta` ASC),
  INDEX `fk_cb_c006_voucher_det_his_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_cb_c006_voucher_det_his_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cb_c006_voucher_det_his_cb_b001_voucher`
    FOREIGN KEY (`fk_cbb001_num_voucher_mast`)
    REFERENCES `SIACE`.`cb_b001_voucher` (`pk_num_voucher_mast`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cb_c006_voucher_det_his_cb_b004_plan_cuenta1`
    FOREIGN KEY (`fk_cb_b004_cod_cuenta`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`cod_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cb_c006_voucher_det_his_cb_c001_voucher_det1`
    FOREIGN KEY (`fk_cbc001_num_voucher_det`)
    REFERENCES `SIACE`.`cb_c001_voucher_det` (`pk_num_voucher_det`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cb_c007_balance_cuenta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cb_c007_balance_cuenta` (
  `pk_num_balance_cuenta` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_anio` YEAR NOT NULL,
  `fk_cbb005_num_contabilidades` INT(11) NOT NULL,
  `fk_cbb004_num_cuenta` INT(11) NOT NULL,
  `num_saldo_inicial` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_saldo_final` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_saldo_balance01` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_saldo_balance02` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_saldo_balance03` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_saldo_balance04` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_saldo_balance05` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_saldo_balance06` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_saldo_balance07` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_saldo_balance08` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_saldo_balance09` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_saldo_balance10` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_saldo_balance11` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_saldo_balance12` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fk_cbb003_num_libro_contabilidad` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_balance_cuenta`),
  UNIQUE INDEX `voucher_balance_cuenta_UNIQUE` (`fec_anio` ASC, `fk_cbb004_num_cuenta` ASC, `fk_cbb003_num_libro_contabilidad` ASC, `fk_cbb005_num_contabilidades` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 793
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cd_c003_tipo_correspondencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cd_c003_tipo_correspondencia` (
  `pk_num_tipo_documento` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id unico del tipo de correspondencia',
  `ind_descripcion` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'descripcion completa del tipo de correspondencia',
  `ind_descripcion_corta` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'descripcion corta o abreviatura del tipo de correspondencia',
  `ind_procedencia` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'I= Interna E= Externa, indica si el tipo de correspondencia es interna o externa',
  `num_estatus` INT(1) NOT NULL COMMENT '1= activo 0= inactivo, indica si el tipo de documento esta activo o inactivo',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Indica la fecha del ultimo registro modificado',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Indica el id que tiene el usuario que se ha logueado',
  PRIMARY KEY (`pk_num_tipo_documento`))
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cd_b001_documento_interno`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cd_b001_documento_interno` (
  `pk_num_documento` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id unico del documento interno',
  `ind_documento_completo` VARCHAR(25) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'numero del documento interno',
  `fec_registro` DATE NOT NULL COMMENT 'fecha de registro del documento en el sistema',
  `ind_dependencia_remitente` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica la dependencia que envia el documento, memorandum, otros',
  `ind_persona_remitente` VARCHAR(6) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica el representante de la dependencia que envia el documento, memorandum, otros',
  `ind_cargo_remitente` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica el cargo representante de la dependencia que envia el documento, memorandum, otros',
  `ind_flag_encargaduria` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_encargaduria_especial` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_asunto` VARCHAR(350) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'asunto del documento',
  `ind_descripcion` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica la descripcion del documento interno',
  `ind_plazo_atencion` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica los dias de plazo de atencion del documento',
  `fec_documento` DATE NOT NULL COMMENT 'fecha del documento interno',
  `txt_contenido` LONGTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'describe el contenido que va en el documento, memorandum, otros',
  `ind_media_firma` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica la media firma de la persona que redacta el documento',
  `ind_motivo_anulado` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica el motivo de la anulacion del documento',
  `fec_anulado` DATETIME NULL DEFAULT NULL COMMENT 'fecha de anulacion del documento',
  `ind_persona_anulado` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica la persona que anula el documento',
  `ind_anexo` TINYINT(1) NULL DEFAULT NULL COMMENT '1= si 0= no, flag que indica si el documento contiene anexo',
  `txt_descripcion_anexo` LONGTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica la descripcion en caso de tener anexos el documento',
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica el estatus del documento, PR: preparacion, PE: pendiente, PP: preparado, RE: recibido, AN: anulado',
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'mes actual',
  `fec_annio` YEAR NOT NULL COMMENT 'anio actual',
  `fk_cdc003_num_tipo_documento` INT(11) NOT NULL COMMENT 'fk relacionado con el id del tipo de documento',
  `fk_a001_num_organismo` INT(11) NOT NULL COMMENT 'fk relacionado con el id del organismo',
  `fk_a004_num_dependencia` INT(11) NOT NULL COMMENT 'fk relacionado con el id de la dependencia interna',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'fecha del ultimo registro modificado',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'fk relacionado con el id del usuario logueado',
  PRIMARY KEY (`pk_num_documento`),
  UNIQUE INDEX `cod_documento_completo_UNIQUE` (`ind_documento_completo` ASC),
  INDEX `Index_Cod_Documento` (`pk_num_documento` ASC),
  INDEX `Index_Cod_DocumentoCompleto` (`ind_documento_completo` ASC),
  INDEX `fk_cd_b001_documento_interno_a004_dependencia1_idx` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cd_b001_documento_interno_a001_organismo1_idx` USING BTREE (`fk_a001_num_organismo` ASC),
  INDEX `fk_cd_b001_documento_interno_cd_c003_tipo_correspondencia1_idx` USING BTREE (`fk_cdc003_num_tipo_documento` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario66`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_b001_documento_interno_a004_dependencia1`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_b001_documento_interno_cd_c003_tipo_correspondencia1`
    FOREIGN KEY (`fk_cdc003_num_tipo_documento`)
    REFERENCES `SIACE`.`cd_c003_tipo_correspondencia` (`pk_num_tipo_documento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cdb001_num_documento_a001_organismo1`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cd_c001_distribucion_interno`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cd_c001_distribucion_interno` (
  `pk_num_distribucion` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id unico del documento interno',
  `ind_dependencia_destinataria` VARCHAR(6) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica el id de la dependencia destinataria a quien se le envia el documento',
  `ind_persona_destinataria` VARCHAR(6) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica el id del empleado destinatario a quien se le envia el documento',
  `ind_cargo_destinatario` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica el cargo del responsable de la dependencia o empleado a quien se le envia el documento',
  `ind_con_copia` VARCHAR(6) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'con copias a otras dependencias o empleado en particular',
  `ind_plazo_atencion` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica los dias de plazo de atencion del documento',
  `fec_envio` DATETIME NULL DEFAULT NULL COMMENT 'fecha del envio del documento',
  `fec_acuse` DATETIME NULL DEFAULT NULL COMMENT 'fecha de acuse de recibo del documento',
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica el estatus del documento, PR: preparacion, PE: pendiente, PP: preparado, RE: recibido, AN: anulado',
  `fk_cdb001_num_documento` INT(11) NULL DEFAULT NULL COMMENT 'fk relacionado con el id del numero de documento',
  `fk_a003_num_persona` INT(11) NULL DEFAULT NULL COMMENT 'fk relacionado con el id de la persona responsable del envio del documento',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'fecha del ultimo registro modificado',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'fk relacionado con el id del usuario logueado',
  PRIMARY KEY (`pk_num_distribucion`),
  INDEX `Index_Cod_Documento` (`pk_num_distribucion` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cd_c001_distribucion_interno_cd_b001_documento_interno1_idx` USING BTREE (`fk_cdb001_num_documento` ASC),
  INDEX `fk_cd_c001_distribucion_interno_a003_persona_idx` USING BTREE (`fk_a003_num_persona` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario65`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_c001_distribucion_interno_a003_personal2`
    FOREIGN KEY (`fk_a003_num_persona`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_c001_distribucion_interno_cd_b001_documento_interno1`
    FOREIGN KEY (`fk_cdb001_num_documento`)
    REFERENCES `SIACE`.`cd_b001_documento_interno` (`pk_num_documento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cd_c004_documento_entrada`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cd_c004_documento_entrada` (
  `pk_num_documento` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id unico del documento externo',
  `fec_registro` DATE NOT NULL COMMENT 'fecha de registro del documento externo en el sistema',
  `num_documento` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'numero de oficio del documento externo',
  `fec_documento` DATE NOT NULL COMMENT 'indica la fecha de creacion del documento',
  `text_asunto` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'asunto del documento',
  `txt_descripcion_asunto` LONGTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica la descripcion del documento externo',
  `ind_persona_recibido` VARCHAR(35) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'persona a quien recibe el documento externo',
  `num_folio` INT(4) NULL DEFAULT NULL COMMENT 'indica si el documento contiene folio',
  `num_anexo` INT(4) NULL DEFAULT NULL COMMENT 'indica si el documento contiene anexo',
  `num_carpeta` INT(4) NULL DEFAULT NULL COMMENT 'indica si el documento contiene carpeta',
  `ind_descripcion_anexo` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica la descripcion en caso de tener anexos el documento',
  `ind_nombre_mensajero` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'persona encargada de entregar el documento en el ente',
  `ind_cedula_mensajero` VARCHAR(12) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'cedula de la persona que entrega el documento externo',
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica el estatus del documento, PR: preparacion, PE: pendiente, PP: preparado, RE: recibido, AN: anulado',
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'fecha mes actual',
  `fec_annio` YEAR NOT NULL COMMENT 'anio actual',
  `num_particular_rem` INT(5) NOT NULL COMMENT 'indica el particular que entrega el documento',
  `num_depend_ext` INT(5) NOT NULL COMMENT 'dependencia externa que envia el documento al ente',
  `fk_cdc003_num_tipo_documento` INT(11) NOT NULL COMMENT 'indica el tipo de documento',
  `num_org_ext` INT(5) NOT NULL COMMENT 'organismo externo que envia el documento',
  `num_empresa` INT(5) NULL DEFAULT NULL COMMENT 'empresa externa que envia el documento',
  `ind_representante` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica el nombre del representante',
  `ind_cargo` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica el cargo del representante',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'fecha del ultimo registro modificado',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'indica el id del usuario logueado',
  PRIMARY KEY (`pk_num_documento`),
  UNIQUE INDEX `num_documento_UNIQUE` USING BTREE (`num_documento` ASC),
  INDEX `fk_cd_c004_documento_entrada_cd_c003_tipo_correspondencia1_idx` (`fk_cdc003_num_tipo_documento` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario68`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_c004_documento_entrada_cd_c003_tipo_correspondencia1`
    FOREIGN KEY (`fk_cdc003_num_tipo_documento`)
    REFERENCES `SIACE`.`cd_c003_tipo_correspondencia` (`pk_num_tipo_documento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 30
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cd_c005_distribucion_entrada`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cd_c005_distribucion_entrada` (
  `pk_num_entrada` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id unico del documento externo',
  `ind_persona_destinataria` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica la persona a quien va dirigido el documento recibido',
  `ind_dependencia_destinataria` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica la dependencia quien va dirigido el documento recibido',
  `fec_envio` DATETIME NOT NULL COMMENT 'fecha de envio del documento',
  `fec_acuse` DATETIME NOT NULL COMMENT 'indica la fecha de acuse del docuemento',
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica el estatus del documento, PR: preparacion, PE: pendiente, PP: preparado, RE: recibido, AN: anulado',
  `fk_cdc004_num_documento` INT(11) NOT NULL COMMENT 'fk relacionado con el id de la tabla principal de los documentos',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'ultimo registro modificado',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'fk relacionado con el id del usuario logueado',
  PRIMARY KEY (`pk_num_entrada`),
  INDEX `fk_cd_c005_distribucion_entrada_cd_c004_documento_entrada1_idx` (`fk_cdc004_num_documento` ASC),
  CONSTRAINT `fk_cd_c005_distribucion_entrada_cd_c004_documento_entrada1`
    FOREIGN KEY (`fk_cdc004_num_documento`)
    REFERENCES `SIACE`.`cd_c004_documento_entrada` (`pk_num_documento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cd_c006_entrada_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cd_c006_entrada_detalle` (
  `pk_documento_detalle` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id unico del detalle de documento externo',
  `ind_informe_escrito` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'flag que indica si e documento requiere informe por escrito',
  `ind_hablar_conmigo` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'flag que indica si el documento requiere trato directo',
  `ind_coordinar_con` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'persona con quien desea coordinar',
  `ind_preparar_memorandum` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'persona a quien se le prepara memorandum del documento',
  `ind_investigar_informar` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'flag que indica si el documento requiere se investigue e informe verbalmente',
  `ind_tramitar_conclusion` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'flag que indica que el documento requiere ser  tramitado hasta su conclusion',
  `ind_distribuir` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'flag que indica si el documento requiere ser distribuido',
  `ind_conocimiento` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'flag que indica el conocimiento y afines  del documento',
  `ind_preparar_constentacion` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'flag que indica preparar contestacion para firma del documento',
  `ind_archivar` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'flag que indica si el documento requiere ser archivado',
  `ind_registro_de` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'flag que indica el registro',
  `ind_preparar_oficio` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica si se requiere preparar oficio a una determinada persona',
  `ind_conocer_opinion` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'flag que indica el documento requiere conocer su opinion',
  `ind_tramitar_caso` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'flag que indica tramitar en caso de proceder',
  `ind_acusar_recibo` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'flag que indica si el documento fue recibido',
  `ind_tramitar_en` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica la cantidad de dias para el documento sea tramitado',
  `ind_observacion` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica la observacion del documento, es obligatoria',
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica el estatus del documento, PR: preparacion, PE: pendiente, PP: preparado, RE: recibido, AN: anulado',
  `fk_cdc004_num_documento` INT(11) NOT NULL COMMENT 'fk relacionado con la tabla padre de los documentos',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'ultimo registro modificado',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'fk relacionado con el id del usuario logueado',
  PRIMARY KEY (`pk_documento_detalle`),
  INDEX `fk_cd_c006_entrada_detalle_cd_c004_documento_entrada1_idx` (`fk_cdc004_num_documento` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario69`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_c006_entrada_detalle_cd_c004_documento_entrada1`
    FOREIGN KEY (`fk_cdc004_num_documento`)
    REFERENCES `SIACE`.`cd_c004_documento_entrada` (`pk_num_documento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cd_c010_particular`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cd_c010_particular` (
  `pk_num_particular` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id unico del particular',
  `ind_cedula` VARCHAR(12) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Indica cedula del particular,empleado o representante legal',
  `ind_nombre` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'nombre del particular,empleado o representante legal',
  `ind_cargo` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'cargo del particular,empleado o representante legal',
  `ind_direccion` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'direccion del particular,empleado o representante legal',
  `ind_telefono` VARCHAR(25) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT '' COMMENT 'telefono del particular,empleado o representante legal',
  `ind_email` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'correo del particular,empleado o representante legal',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Indica la fecha del ultimo registro modificado',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Indica el id que tiene el usuario que se ha logueado',
  PRIMARY KEY (`pk_num_particular`),
  UNIQUE INDEX `cedula_UNIQUE` (`ind_cedula` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario67`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cd_c011_documento_salida`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cd_c011_documento_salida` (
  `pk_num_documento` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id unico del documento de salida',
  `ind_cod_completo` VARCHAR(35) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'numero del documento de salida',
  `ind_dependencia_remitente` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica la dependencia que envia el documento u oficio',
  `ind_persona_remitente` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica el representante de la dependencia que envia el documento u oficio',
  `ind_cargo_remitente` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica el cargo representante de la dependencia que envia el documento u oficio',
  `fec_registro` DATE NOT NULL COMMENT 'fecha de registro del documento en el sistema',
  `ind_asunto` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'asunto del documento',
  `txt_descripcion` LONGTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica la descripcion del documento de salida',
  `ind_anexo` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT '1= si 0= no, flag que indica si el documento contiene anexo',
  `txt_descripcion_anexo` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica la descripcion en caso de tener anexos el documento',
  `ind_plazo_atencion` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica los dias de plazo de atencion del documento',
  `fec_documento` DATE NOT NULL COMMENT 'fecha del documento de salida',
  `txt_contenido` LONGTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'describe el contenido que va en el documento u oficio',
  `ind_media_firma` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica la media firma de la persona que redacta el documento',
  `fec_anulado` DATE NULL DEFAULT NULL COMMENT 'fecha de anulacion del documento',
  `ind_motivo_anulado` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica el motivo de la anulacion del documento',
  `ind_persona_anulado` VARCHAR(6) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica la persona que anula el documento',
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica el estatus del documento, PR: preparacion, PE: pendiente, PP: preparado, RE: recibido, AN: anulado',
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'mes actual',
  `fec_annio` YEAR NOT NULL COMMENT 'anio atual',
  `fk_cdc003_num_tipo_documento` INT(11) NOT NULL COMMENT 'fk relacionado con el id del tipo de documento',
  `fk_a001_num_organismo` INT(11) NOT NULL COMMENT 'fk relacionado con el id del organismo',
  `fk_a004_num_dependencia` INT(11) NOT NULL COMMENT 'fk relacionado con el id de la dependencia interna',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'fecha del ultimo registro modificado',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'fk relacionado con el id del usuario logueado',
  PRIMARY KEY (`pk_num_documento`),
  UNIQUE INDEX `cod_documento_completo_UNIQUE` (`ind_cod_completo` ASC),
  INDEX `fk_cd_c011_documento_salida_cd_c003_tipo_correspondencia1_idx` (`fk_cdc003_num_tipo_documento` ASC),
  INDEX `fk_cd_c011_documento_salida_a001_organismo1_idx` (`fk_a001_num_organismo` ASC),
  INDEX `fk_cd_c011_documento_salida_a004_dependencia1_idx` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario70`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_c011_documento_salida_a001_organismo1`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_c011_documento_salida_a004_dependencia1`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_c011_documento_salida_cd_c003_tipo_correspondencia1`
    FOREIGN KEY (`fk_cdc003_num_tipo_documento`)
    REFERENCES `SIACE`.`cd_c003_tipo_correspondencia` (`pk_num_tipo_documento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cd_c012_distribucion_salida`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cd_c012_distribucion_salida` (
  `pk_num_distribucion` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id unico del documento de salida',
  `ind_organismo_externo` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica el id del organismo externo a quien se le envia el documento',
  `ind_dependencia_externa` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT '' COMMENT 'indica el id de la dependencia extern a quien se le envia el documento',
  `flag_r` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica el id del particular externo a quien se le envia el documento',
  `ind_representante_externo` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT '' COMMENT 'indica el representante del organismo, dependencia o particular',
  `ind_cargo_externo` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT '' COMMENT 'indica cargo del representante del organismo, dependencia o particular',
  `ind_con_atencion` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT '' COMMENT '1= si 0= no, flag que indica si existe dias de plazo de atencion del documento',
  `ind_plazo_atencion` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT '' COMMENT 'indica los dias de plazo de atencion del documento',
  `fec_envio` DATETIME NULL DEFAULT NULL COMMENT 'fecha del envio del documento',
  `fec_acuse` DATETIME NULL DEFAULT NULL COMMENT 'fecha de acuse de recibo del documento',
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'indica el estatus del documento, PR: preparacion, PE: pendiente, PP: preparado, RE: recibido, AN: anulado',
  `ind_motivo_devolucion` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica el motivo de devolucion del documento',
  `fec_devolucion` DATE NULL DEFAULT NULL COMMENT 'indica la fecha de devolucion del documento',
  `fk_cdc011_num_documento` INT(11) NOT NULL COMMENT 'fk relacionado con el id del numero de documento',
  `fk_a003_num_persona` INT(11) NULL DEFAULT NULL COMMENT 'fk relacionado con el id de la persona responsable del envio del documento',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'fecha del ultimo registro modificado',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'fk relacionado con el id del usuario logueado',
  PRIMARY KEY (`pk_num_distribucion`),
  INDEX `fk_cd_c012_distribucion_salida_cd_c011_documento_salida1_idx` (`fk_cdc011_num_documento` ASC),
  INDEX `fk_cd_c012_distribucion_salida_a003_persona_idx` (`fk_a003_num_persona` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario71`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_c012_distribucion_salida_a003_persona12`
    FOREIGN KEY (`fk_a003_num_persona`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_c012_distribucion_salida_cd_c011_documento_salida1`
    FOREIGN KEY (`fk_cdc011_num_documento`)
    REFERENCES `SIACE`.`cd_c011_documento_salida` (`pk_num_documento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 37
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cd_e001_historico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cd_e001_historico` (
  `pk_num_historico` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id unico del historico',
  `fec_historico` DATE NULL DEFAULT NULL COMMENT 'fecha del historico del documento',
  `ind_direccion_ip` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'ip de la PC que realizo el registro del documento',
  `ind_nombre_pc` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'nombre de la PC que realizo el registro del documento',
  `num_tipo` INT(11) NULL DEFAULT NULL,
  `txt_modulo` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica el modulo actual del registro',
  `fk_cdc001_num_distribucion` INT(11) NOT NULL COMMENT 'fk relacionado con el id de la distribucion de los documentos internos',
  `fk_cdb001_num_documento` INT(11) NOT NULL COMMENT 'fk relacionado con el id de los documentos internos',
  `fk_cdc011_num_documento` INT(11) NOT NULL COMMENT 'fk relacionado con el id de los documentos de salida',
  `fk_cdc012_num_distribucion` INT(11) NOT NULL COMMENT 'fk relacionado con el id de la distribucion de los documentos de salida',
  `fk_a003_num_persona` INT(11) NOT NULL COMMENT 'fk relacionado con el id de la persona responsable',
  `fk_cdc004_num_documento` INT(11) NOT NULL COMMENT 'fk relacionado con el id de los documentos de entrada externos',
  `fk_cdc005_num_entrada` INT(11) NOT NULL COMMENT 'fk relacionado con el id de la distribucion de los documentos de entrada externos',
  `fk_a004_num_dependencia` INT(11) NOT NULL COMMENT 'fk relacionado con el id de la dependencia',
  `fk_a001_num_organismo` INT(11) NOT NULL COMMENT 'fk relacionado con el id del organismo',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'fecha del ultimo registro modificado',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'fk relacionado con el id del usuario logueado',
  PRIMARY KEY (`pk_num_historico`),
  INDEX `fk_cd_e001_historico_cd_c001_distribucion_interno1_idx` (`fk_cdc001_num_distribucion` ASC),
  INDEX `fk_cd_e001_historico_cd_b001_documento_interno1_idx` (`fk_cdb001_num_documento` ASC),
  INDEX `fk_cd_e001_historico_cd_c011_documento_salida1_idx` (`fk_cdc011_num_documento` ASC),
  INDEX `fk_cd_e001_historico_cd_c012_distribucion_salida1_idx` (`fk_cdc012_num_distribucion` ASC),
  INDEX `fk_cd_e001_historico_a003_persona13_idx` (`fk_a003_num_persona` ASC),
  INDEX `fk_cd_e001_historico_cd_c004_documento_entrada1_idx` (`fk_cdc004_num_documento` ASC),
  INDEX `fk_cd_e001_historico_cd_c005_distribucion_entrada1_idx` (`fk_cdc005_num_entrada` ASC),
  INDEX `fk_cd_e001_historico_a004_dependencia1_idx` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_cd_e001_historico_a001_organismo1_idx` (`fk_a001_num_organismo` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario72`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_e001_historico_a001_organismo1`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_e001_historico_a003_persona33`
    FOREIGN KEY (`fk_a003_num_persona`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_e001_historico_a004_dependencia1`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cd_e002_historico_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cd_e002_historico_detalle` (
  `pk_num_tipo_historico` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id unico del detalle del historico',
  `txt_descripcion` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'indica la descripcion del documento',
  `fk_cde001_num_historico` INT(11) NOT NULL COMMENT 'fk relacionado con el id del numero del historico',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'fecha del ultimo registro modificado',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'fk relacionado con el id del usuario logueado',
  PRIMARY KEY (`pk_num_tipo_historico`),
  INDEX `fk_cd_e002_historico_detalle_cd_e001_historico1_idx` (`fk_cde001_num_historico` ASC),
  CONSTRAINT `fk_cd_e002_historico_detalle_cd_e001_historico1`
    FOREIGN KEY (`fk_cde001_num_historico`)
    REFERENCES `SIACE`.`cd_e001_historico` (`pk_num_historico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_b001_documento_clasificacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_b001_documento_clasificacion` (
  `pk_num_documento_clasificacion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_documento_clasificacion` VARCHAR(3) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_transaccion` INT(1) NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `num_flag_item` INT(1) NOT NULL,
  `num_flag_cliente` INT(1) NOT NULL,
  `num_flag_compromiso` INT(1) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_documento_clasificacion`),
  UNIQUE INDEX `documento_clasificacion_UNIQUE` (`ind_documento_clasificacion` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario73`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_b002_tipo_documento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_b002_tipo_documento` (
  `pk_num_tipo_documento` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_tipo_documento` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `cod_fiscal` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a006_num_miscelaneo_detalle_clasificacion` INT(11) NOT NULL COMMENT 'O:OBLIGACIONES; C:OTROS DE CTAS POR PAGAR; P:PRESTAMOS; E:OTROS EXTERNOS',
  `ind_clasificacion` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'O:OBLIGACIONES; C:OTROS DE CTAS POR PAGAR; P:PRESTAMOS; E:OTROS EXTERNOS',
  `fk_cbc003_num_tipo_voucher` INT(11) NULL DEFAULT NULL,
  `fk_cbc003_num_tipo_voucher_ordPago` INT(11) NULL DEFAULT NULL,
  `num_flag_provision` INT(1) NOT NULL DEFAULT '0',
  `fk_cbb004_num_plan_cuenta` INT(11) NULL DEFAULT NULL,
  `fk_cbb004_num_plan_cuenta_pub20` INT(11) NULL DEFAULT NULL,
  `num_flag_adelanto` INT(1) NOT NULL DEFAULT '0',
  `fk_cbb004_num_plan_cuenta_ade` INT(11) NULL DEFAULT NULL,
  `fk_cbb004_num_plan_cuenta_ade_pub20` INT(11) NULL DEFAULT NULL,
  `num_flag_fiscal` INT(1) NOT NULL DEFAULT '0',
  `num_flag_monto_negativo` INT(1) NOT NULL DEFAULT '0',
  `num_flag_auto_nomina` INT(1) NOT NULL DEFAULT '0',
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a006_num_miscelaneo_detalle_regimen_fiscal` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_tipo_documento`),
  UNIQUE INDEX `CodTipoDocumento_UNIQUE` (`cod_tipo_documento` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `FK_ap_tipodocumento_2_idx` USING BTREE (`fk_cbc003_num_tipo_voucher` ASC),
  INDEX `fk_cp_b002_tipo_documento_cbb004_plan_cuenta1_idx` (`fk_cbb004_num_plan_cuenta` ASC),
  INDEX `fk_cp_b002_tipo_documento_cbb004_plan_cuenta2_idx` (`fk_cbb004_num_plan_cuenta_pub20` ASC),
  INDEX `fk_cp_b002_tipo_documento_cbb004_plan_cuenta3_idx` (`fk_cbb004_num_plan_cuenta_ade` ASC),
  INDEX `fk_cp_b002_tipo_documento_cbb004_plan_cuenta4_idx` (`fk_cbb004_num_plan_cuenta_ade_pub20` ASC),
  INDEX `fk_cp_b002_tipo_documento_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle_clasificacion` ASC),
  INDEX `fk_cp_b002_tipo_documento_a006_miscelaneo_detalle2_idx` (`fk_a006_num_miscelaneo_detalle_regimen_fiscal` ASC),
  INDEX `fk_cp_b002_tipo_documento_cbb001_voucher` (`fk_cbc003_num_tipo_voucher_ordPago` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario75`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_b002_tipo_documento_cbb004_plan_cuenta1`
    FOREIGN KEY (`fk_cbb004_num_plan_cuenta`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_b002_tipo_documento_cbb004_plan_cuenta2`
    FOREIGN KEY (`fk_cbb004_num_plan_cuenta_pub20`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_b002_tipo_documento_cbb004_plan_cuenta3`
    FOREIGN KEY (`fk_cbb004_num_plan_cuenta_ade`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_b002_tipo_documento_cbb004_plan_cuenta4`
    FOREIGN KEY (`fk_cbb004_num_plan_cuenta_ade_pub20`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_b002_tipo_documento_cbc003_tipo_voucher1`
    FOREIGN KEY (`fk_cbc003_num_tipo_voucher`)
    REFERENCES `SIACE`.`cb_c003_tipo_voucher` (`pk_num_voucher`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_b002_tipo_documento_cbc003_tipo_voucher2`
    FOREIGN KEY (`fk_cbc003_num_tipo_voucher_ordPago`)
    REFERENCES `SIACE`.`cb_c003_tipo_voucher` (`pk_num_voucher`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 261
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_b004_clasificacion_gastos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_b004_clasificacion_gastos` (
  `pk_num_clasificacion_gastos` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_clasificacion` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a006_num_miscelaneo_detalle_aplicacion` INT(11) NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_clasificacion_gastos`),
  UNIQUE INDEX `CodClasificacion_UNIQUE` (`cod_clasificacion` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cp_b004_clasificacion_gastos_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle_aplicacion` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario76`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pr_b002_partida_presupuestaria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pr_b002_partida_presupuestaria` (
  `pk_num_partida_presupuestaria` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_prb001_num_tipo_cuenta` INT(11) NOT NULL,
  `cod_partida` VARCHAR(16) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_partida` VARCHAR(3) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_generica` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_especifica` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_subespecifica` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_denominacion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'A',
  `ind_tipo` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_nivel` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_cbb004_num_plan_cuenta_pub20` INT(11) NULL DEFAULT NULL,
  `fk_cbb004_num_plan_cuenta_onco` INT(11) NULL DEFAULT NULL,
  `fk_cbb004_num_plan_cuenta_gastopub20` INT(11) NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `ind_numeral` VARCHAR(3) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_partida_presupuestaria`),
  UNIQUE INDEX `cod_partida_UNIQUE` (`cod_partida` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario173`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3868
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_b005_concepto_gasto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_b005_concepto_gasto` (
  `pk_num_concepto_gasto` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_concepto_gasto` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_prb002_num_partida_presupuestaria` INT(11) NOT NULL,
  `fk_cbb004_num_plan_cuenta` INT(11) NOT NULL,
  `fk_cbb004_num_plan_cuenta_pub20` INT(11) NULL DEFAULT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_concepto_gasto`),
  UNIQUE INDEX `CodConceptoGasto_UNIQUE` (`cod_concepto_gasto` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cp_b005_concepto_gasto_prb002_partida_pres_idx` (`fk_prb002_num_partida_presupuestaria` ASC),
  INDEX `fk_cp_b005_concepto_gasto_cb_b004_plan_cta1_idx` (`fk_cbb004_num_plan_cuenta` ASC),
  INDEX `fk_cp_b005_concepto_gasto_a006_miscelaneo_detalle1` (`fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo` ASC),
  INDEX `fk_cbb004_num_plan_cuenta_pub20` (`fk_cbb004_num_plan_cuenta_pub20` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario78`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cbb004_num_plan_cuenta_pub20`
    FOREIGN KEY (`fk_cbb004_num_plan_cuenta_pub20`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_b005_concepto_gasto_cb_b004_plan_cuenta1`
    FOREIGN KEY (`fk_cbb004_num_plan_cuenta`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_b005_concepto_gasto_cb_b004_plan_cuenta2`
    FOREIGN KEY (`fk_cbb004_num_plan_cuenta_pub20`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_b005_concepto_gasto_prb002_partida_presupuestaria1`
    FOREIGN KEY (`fk_prb002_num_partida_presupuestaria`)
    REFERENCES `SIACE`.`pr_b002_partida_presupuestaria` (`pk_num_partida_presupuestaria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 42
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_b006_banco_tipo_transaccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_b006_banco_tipo_transaccion` (
  `pk_num_banco_tipo_transaccion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_cbb004_num_plan_cuenta` INT(11) NULL DEFAULT NULL,
  `fk_cbb004_num_plan_cuenta_pub20` INT(11) NULL DEFAULT NULL,
  `cod_tipo_transaccion` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a006_miscelaneo_detalle_tipo_transaccion` INT(11) NOT NULL,
  `num_flag_voucher` INT(1) NOT NULL DEFAULT '0',
  `num_flag_transaccion` INT(1) NOT NULL DEFAULT '0',
  `num_flag_transaccion_planilla` INT(1) NOT NULL DEFAULT '0',
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_cbc003_tipo_voucher` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_banco_tipo_transaccion`),
  UNIQUE INDEX `CodTipoTransaccion_UNIQUE` (`cod_tipo_transaccion` ASC),
  INDEX `CodCuenta` USING BTREE (`fk_cbb004_num_plan_cuenta` ASC),
  INDEX `fk_cp_b006_banco_tipo_transaccion_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cp_b006_banco_tipo_transaccion_a006_miscelaneo_detalle1_idx` (`fk_a006_miscelaneo_detalle_tipo_transaccion` ASC),
  INDEX `fk_cp_b006_banco_tipo_transaccion_cb_c003_tipo_voucher1_idx` (`fk_cbc003_tipo_voucher` ASC),
  INDEX `fk_cp_b006_banco_tipo_transaccion_cb_b004_plan_cuenta2` (`fk_cbb004_num_plan_cuenta_pub20` ASC),
  CONSTRAINT `cp_b006_banco_tipo_transaccion_ibfk_1`
    FOREIGN KEY (`fk_cbc003_tipo_voucher`)
    REFERENCES `SIACE`.`cb_c003_tipo_voucher` (`pk_num_voucher`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_b006_banco_tipo_transaccion_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_b006_banco_tipo_transaccion_cb_b004_plan_cuenta1`
    FOREIGN KEY (`fk_cbb004_num_plan_cuenta`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_b006_banco_tipo_transaccion_cb_b004_plan_cuenta2`
    FOREIGN KEY (`fk_cbb004_num_plan_cuenta_pub20`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 95
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_b007_cuenta_bancaria_defecto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_b007_cuenta_bancaria_defecto` (
  `pk_num_cuenta_bancaria_defecto` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_cpb014_num_cuenta_bancaria` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipo_pago` INT(11) NOT NULL,
  `int_numero_ultimo` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_cuenta_bancaria_defecto`),
  INDEX `fk_cpb014_pk_num_cuenta_bancaria` (`fk_cpb014_num_cuenta_bancaria` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_tipo_pago` (`fk_a006_num_miscelaneo_detalle_tipo_pago` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_b008_unidad_viatico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_b008_unidad_viatico` (
  `pk_num_unidad_viatico` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_num_resolucion` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_periodo` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_unidad_viatico` DECIMAL(18,6) NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_preparado` DATETIME NOT NULL,
  `fec_aprobado` DATETIME NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_preparado` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_aprobado` INT(11) NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_unidad_viatico`),
  INDEX `fk_rhb001_num_empleado_preparado` (`fk_rhb001_num_empleado_preparado` ASC),
  INDEX `fk_rhb001_num_empleado_aprobado` (`fk_rhb001_num_empleado_aprobado` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_b009_concepto_gasto_viatico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_b009_concepto_gasto_viatico` (
  `pk_num_concepto_gasto_viatico` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_concepto` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a006_num_miscelaneo_detalle_categoria` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_articulo` INT(11) NOT NULL,
  `fk_cbb004_num_plan_cuenta` INT(11) NOT NULL,
  `fk_cbb004_num_plan_cuenta_pub20` INT(11) NOT NULL,
  `fk_prb002_num_partida_presupuestaria` INT(11) NOT NULL,
  `num_numeral` INT(11) NOT NULL,
  `num_valor_ut` DECIMAL(18,6) NOT NULL,
  `num_flag_monto` INT(1) NOT NULL,
  `num_flag_cantidad` INT(1) NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_concepto_gasto_viatico`),
  INDEX `fk_cbb004_num_plan_cuenta` (`fk_cbb004_num_plan_cuenta` ASC),
  INDEX `fk_cbb004_num_plan_cuenta_pub20` (`fk_cbb004_num_plan_cuenta_pub20` ASC),
  INDEX `fk_prb002_num_partida_presupuestaria` (`fk_prb002_num_partida_presupuestaria` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_articulo` (`fk_a006_num_miscelaneo_detalle_articulo` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_categoria` (`fk_a006_num_miscelaneo_detalle_categoria` ASC),
  CONSTRAINT `cp_b009_concepto_gasto_viatico_a006_miscelaneo_detalle1`
    FOREIGN KEY (`fk_a006_num_miscelaneo_detalle_categoria`)
    REFERENCES `SIACE`.`a006_miscelaneo_detalle` (`pk_num_miscelaneo_detalle`),
  CONSTRAINT `cp_b009_concepto_gasto_viatico_a006_miscelaneo_detalle2`
    FOREIGN KEY (`fk_a006_num_miscelaneo_detalle_articulo`)
    REFERENCES `SIACE`.`a006_miscelaneo_detalle` (`pk_num_miscelaneo_detalle`),
  CONSTRAINT `cp_b009_concepto_gasto_viatico_a018_num_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`),
  CONSTRAINT `cp_b009_concepto_gasto_viatico_cbb004_plan_cuenta1`
    FOREIGN KEY (`fk_cbb004_num_plan_cuenta`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`),
  CONSTRAINT `cp_b009_concepto_gasto_viatico_cbb004_plan_cuenta2`
    FOREIGN KEY (`fk_cbb004_num_plan_cuenta_pub20`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`),
  CONSTRAINT `cp_b009_concepto_gasto_viatico_prb002_partida_presupuestaria`
    FOREIGN KEY (`fk_prb002_num_partida_presupuestaria`)
    REFERENCES `SIACE`.`pr_b002_partida_presupuestaria` (`pk_num_partida_presupuestaria`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`nm_b001_tipo_nomina`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`nm_b001_tipo_nomina` (
  `pk_num_tipo_nomina` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_tipo_nomina` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_nombre_nomina` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_pago_mensual` INT(1) NOT NULL,
  `ind_titulo_boleta` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_tipo_nomina`),
  UNIQUE INDEX `cod_tipo_nomina_UNIQUE` (`cod_tipo_nomina` ASC),
  INDEX `fk_nm_b001_tipo_nomina_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_nm_b001_tipo_nomina_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 15
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_b010_especificacion_viatico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_b010_especificacion_viatico` (
  `pk_num_especificacion_viatico` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_nmb001_num_tipo_nomina` INT(11) NOT NULL,
  `num_unidad_viatico` DECIMAL(18,6) NOT NULL,
  `ind_desg1` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_unidaddesg1` DECIMAL(18,6) NULL DEFAULT NULL,
  `ind_desg2` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_unidaddesg2` DECIMAL(18,6) NULL DEFAULT NULL,
  `ind_desg3` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_unidaddesg3` DECIMAL(18,6) NULL DEFAULT NULL,
  `num_tipo_pernocta` INT(1) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a006_num_miscelaneo_tipo_viatico` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_tipo_gasto` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_especificacion_viatico`),
  INDEX `fk_cp_b010_especificacion_viatico_nm_b001_tipo_nomina1_idx` (`fk_nmb001_num_tipo_nomina` ASC),
  INDEX `fk_cp_b010_especificacion_viatico_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cp_b010_especificacion_viatico_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_tipo_viatico` ASC),
  INDEX `fk_cp_b010_especificacion_viatico_a006_miscelaneo_detalle2_idx` (`fk_a006_num_miscelaneo_tipo_gasto` ASC),
  CONSTRAINT `fk_cp_b010_especificacion_viatico_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_b010_especificacion_viatico_nm_b001_tipo_nomina1`
    FOREIGN KEY (`fk_nmb001_num_tipo_nomina`)
    REFERENCES `SIACE`.`nm_b001_tipo_nomina` (`pk_num_tipo_nomina`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_b011_viaticos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_b011_viaticos` (
  `pk_num_viaticos` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_num_solicitud_viatico` INT(11) NOT NULL,
  `ind_destino` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_observacion_apro` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_observacion_rech` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_razon_rechazo` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_motivo` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_salida` DATE NOT NULL,
  `fec_regreso` DATE NOT NULL,
  `fec_preparado` DATETIME NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_solicita` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_beneficiario` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_procesa` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_revisa` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_aprueba` INT(11) NOT NULL,
  `fk_cpb010_num_especificacion_viatico` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_viaticos`),
  INDEX `fk_cp_b011_viaticos_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_solicita` ASC),
  INDEX `fk_cp_b011_viaticos_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_beneficiario` ASC),
  INDEX `fk_cp_b011_viaticos_rh_b001_empleado3_idx` (`fk_rhb001_num_empleado_procesa` ASC),
  INDEX `fk_cp_b011_viaticos_rh_b001_empleado4_idx` (`fk_rhb001_num_empleado_revisa` ASC),
  INDEX `fk_cp_b011_viaticos_rh_b001_empleado5_idx` (`fk_rhb001_num_empleado_aprueba` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cp_b011_viaticos_cp_b010_especificacion_viatico1_idx` (`fk_cpb010_num_especificacion_viatico` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario84`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_b011_viaticos_cp_b010_especificacion_viatico1`
    FOREIGN KEY (`fk_cpb010_num_especificacion_viatico`)
    REFERENCES `SIACE`.`cp_b010_especificacion_viatico` (`pk_num_especificacion_viatico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_b013_cajachica_autorizacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_b013_cajachica_autorizacion` (
  `pk_num_cajachica_autorizacion` INT(11) NOT NULL AUTO_INCREMENT,
  `num_monto_autorizado` DECIMAL(18,6) NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_cajachica_autorizacion`),
  INDEX `fk_cp_b013_cajachica_autorizacion_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario85`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_b014_cuenta_bancaria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_b014_cuenta_bancaria` (
  `pk_num_cuenta` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a006_num_miscelaneo_detalle_banco` INT(11) NOT NULL,
  `ind_num_cuenta` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipo_cuenta` VARCHAR(11) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a006_num_miscelaneo_det` VARCHAR(11) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_apertura` DATE NOT NULL,
  `fk_cbb004_num_plan_cuenta` INT(11) NULL DEFAULT NULL,
  `fk_cbb004_num_plan_cuenta_pub20` INT(11) NULL DEFAULT NULL,
  `ind_agencia` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_distrito` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_atencion` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_cargo` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_conciliacion_bancaria` INT(1) NOT NULL,
  `num_flag_debito_bancario` INT(1) NOT NULL DEFAULT '0',
  `num_flag_conciliacion_cp` INT(1) NOT NULL,
  `num_flag_default` INT(1) NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `num_saldo_inicial` DECIMAL(18,6) NULL DEFAULT NULL,
  `ind_comentarios` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_cuenta`),
  UNIQUE INDEX `NroCuenta_UNIQUE` (`ind_num_cuenta` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cp_b014_cuenta_bancaria_a006_miscelaneo_detalle2_idx` (`fk_a006_num_miscelaneo_detalle_tipo_cuenta` ASC),
  INDEX `fk_cp_b014_cuenta_bancaria_cb_b004_plan_cuenta1_idx` (`fk_cbb004_num_plan_cuenta` ASC),
  INDEX `fk_cp_b014_cuenta_bancaria_cb_b004_plan_cuenta2_idx` (`fk_cbb004_num_plan_cuenta_pub20` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_banco` USING BTREE (`fk_a006_num_miscelaneo_detalle_banco` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario86`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_b014_cuenta_bancaria_a006_miscelaneo_detalle2`
    FOREIGN KEY (`fk_cbb004_num_plan_cuenta`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_b014_cuenta_bancaria_cb_b004_plan_cuenta1`
    FOREIGN KEY (`fk_cbb004_num_plan_cuenta`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_b014_cuenta_bancaria_cb_b004_plan_cuenta2`
    FOREIGN KEY (`fk_cbb004_num_plan_cuenta_pub20`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`))
ENGINE = InnoDB
AUTO_INCREMENT = 16
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_b015_impuesto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_b015_impuesto` (
  `pk_num_impuesto` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_impuesto` VARCHAR(3) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `txt_descripcion` VARCHAR(25) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_signo` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_factor_porcentaje` DECIMAL(16,6) NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a006_num_miscelaneo_detalle_flag_provision` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_flag_imponible` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_flag_general` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipo_comprobante` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_regimen_fiscal` INT(11) NOT NULL,
  `fk_cbb004_num_plan_cuenta_pub20` INT(11) NULL DEFAULT NULL,
  `fk_cbb004_num_plan_cuenta` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_impuesto`),
  UNIQUE INDEX `cod_impuesto_UNIQUE` (`cod_impuesto` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cp_b015_impuesto_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle_flag_provision` ASC),
  INDEX `fk_cp_b015_impuesto_a006_miscelaneo_detalle2_idx` (`fk_a006_num_miscelaneo_detalle_flag_imponible` ASC),
  INDEX `fk_cp_b015_impuesto_a006_miscelaneo_detalle3_idx` (`fk_a006_num_miscelaneo_detalle_flag_general` ASC),
  INDEX `fk_cp_b015_impuesto_a006_miscelaneo_detalle4_idx` (`fk_a006_num_miscelaneo_detalle_tipo_comprobante` ASC),
  INDEX `fk_cp_b015_impuesto_cb_b004_plan_cuenta1_idx` (`fk_cbb004_num_plan_cuenta_pub20` ASC),
  INDEX `fk_cp_b015_impuesto_cb_b004_plan_cuenta2_idx` (`fk_cbb004_num_plan_cuenta` ASC),
  INDEX `fk_cp_b015_impuesto_a006_miscelaneo_detalle5_idx` (`fk_a006_num_miscelaneo_detalle_regimen_fiscal` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario196`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_b015_impuesto_cb_b004_plan_cuenta1`
    FOREIGN KEY (`fk_cbb004_num_plan_cuenta_pub20`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_b015_impuesto_cb_b004_plan_cuenta2`
    FOREIGN KEY (`fk_cbb004_num_plan_cuenta`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 17
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_b016_tipo_voucher`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_b016_tipo_voucher` (
  `pk_num_voucher_mast` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_voucher` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_manual` INT(1) NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_voucher_mast`),
  UNIQUE INDEX `cod_voucher_UNIQUE` (`cod_voucher` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_b017_tipo_servicio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_b017_tipo_servicio` (
  `pk_num_tipo_servico` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_tipo_servicio` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a006_num_miscelaneo_regimen_fiscal` INT(11) NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pk_num_tipo_servico`),
  INDEX `fk_a006_num_miscelaneo_regimen_fiscal1_idx` (`fk_a006_num_miscelaneo_regimen_fiscal` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 18
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_b018_unidad_tributaria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_b018_unidad_tributaria` (
  `pk_num_unidad_tributaria` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_anio` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_secuencia` INT(11) NOT NULL,
  `ind_valor` DECIMAL(18,6) NOT NULL,
  `fec` DATE NOT NULL,
  `txt_gaceta_oficial` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_providencia_num` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_unidad_tributaria`),
  INDEX `fk_cp_b018_unidad_tributaria_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_cp_b018_unidad_tributaria_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_b019_cuenta_bancaria_tipo_pago`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_b019_cuenta_bancaria_tipo_pago` (
  `pk_num_cuenta_bancaria_tipo_pago` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_cpb014_num_cuenta_bancaria` INT(11) NULL DEFAULT NULL,
  `fk_a006_num_miscelaneo_detalle_tipo_pago` INT(11) NULL DEFAULT NULL,
  `int_numero_ultimo_cheque` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_cuenta_bancaria_tipo_pago`),
  INDEX `fk_miscelaneo_detalle_tipo_pago` (`fk_a006_num_miscelaneo_detalle_tipo_pago` ASC),
  INDEX `fk_cuenta_banco` (`fk_cpb014_num_cuenta_bancaria` ASC),
  INDEX `fk_cp_b019_cuenta_bancaria_tipo_pago_a018_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_cp_b019_cuenta_bancaria_tipo_pago_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 25
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_b020_servicio_impuesto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_b020_servicio_impuesto` (
  `fk_cpb015_num_impuesto` INT(11) NOT NULL,
  `fk_cpb017_num_tipo_servico` INT(11) NOT NULL,
  INDEX `fk_cp_b020_servicio_impuesto_cp_b015_impuesto1_idx` (`fk_cpb015_num_impuesto` ASC),
  INDEX `fk_cp_b020_servicio_impuesto_cp_b017_tipo_servicio1_idx` (`fk_cpb017_num_tipo_servico` ASC),
  CONSTRAINT `fk_cp_b020_servicio_impuesto_cp_b015_impuesto1`
    FOREIGN KEY (`fk_cpb015_num_impuesto`)
    REFERENCES `SIACE`.`cp_b015_impuesto` (`pk_num_impuesto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_b020_servicio_impuesto_cp_b017_tipo_servicio1`
    FOREIGN KEY (`fk_cpb017_num_tipo_servico`)
    REFERENCES `SIACE`.`cp_b017_tipo_servicio` (`pk_num_tipo_servico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_c001_concepto_clasificacion_gasto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_c001_concepto_clasificacion_gasto` (
  `pk_num_concepto_clasificacion_gastos` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_cpb004_num_clasificacion_gastos` INT(11) NOT NULL,
  `fk_cpb005_num_concepto_gasto` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_concepto_clasificacion_gastos`),
  INDEX `fk_cp_c001_concepto_clasificacion_gasto_cp_b004_clasificaci_idx` (`fk_cpb004_num_clasificacion_gastos` ASC),
  INDEX `fk_cp_c001_concepto_clasificacion_gasto_cp_b005_concepto_ga_idx` (`fk_cpb005_num_concepto_gasto` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario87`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_c001_concepto_clasificacion_gasto_cp_b004_clasificacion1`
    FOREIGN KEY (`fk_cpb004_num_clasificacion_gastos`)
    REFERENCES `SIACE`.`cp_b004_clasificacion_gastos` (`pk_num_clasificacion_gastos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_c001_concepto_clasificacion_gasto_cp_b005_concepto_gasto1`
    FOREIGN KEY (`fk_cpb005_num_concepto_gasto`)
    REFERENCES `SIACE`.`cp_b005_concepto_gasto` (`pk_num_concepto_gasto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 41
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_c002_cajachica_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_c002_cajachica_detalle` (
  `pk_num_caja_chica_detalle` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_cbb004_num_plan_cuenta` INT(11) NOT NULL,
  `num_flag_caja_chica` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'C:Caja Chica; R:Reporte de Gasto',
  `fk_prb002_num_partida_presupuestaria` INT(11) NOT NULL,
  `fk_cpb017_num_tipo_servicio` INT(11) NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `num_secuencia` INT(4) NOT NULL,
  `fec_fecha` DATETIME NOT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_linea` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_proveedor` INT(20) NOT NULL,
  `fec_documento` DATE NOT NULL,
  `num_monto_afecto` DECIMAL(18,6) NOT NULL,
  `num_monto_no_afecto` DECIMAL(18,6) NOT NULL,
  `num_monto_impuesto` DECIMAL(18,6) NOT NULL,
  `num_monto_retencion` DECIMAL(18,6) NOT NULL,
  `num_monto_pagado` DECIMAL(18,6) NOT NULL,
  `fec_periodo_registro_compra` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_comentarios` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_num_recibo` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_no_afecto_IGV` INT(1) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_cpd004_num_caja_chica` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_regimen_fiscal` INT(11) NOT NULL,
  `fk_cpb002_num_tipo_documento` INT(11) NOT NULL,
  `num_documento` VARCHAR(20) CHARACTER SET 'utf8' NOT NULL,
  `fk_cpb005_num_concepto_gasto` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_caja_chica_detalle`),
  INDEX `fk_cp_c002_cajachica_detalle_cp_d004_caja_chica1_idx` (`fk_cpd004_num_caja_chica` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cp_c002_cajachica_detalle_prb002_partida_presupuestaria1_idx` (`fk_prb002_num_partida_presupuestaria` ASC),
  INDEX `fk_cp_c002_cajachica_detalle_cbb004_plan_cuenta1_idx` (`fk_cbb004_num_plan_cuenta` ASC),
  INDEX `fk_cp_c002_cajachica_detalle_cp_b017_tipo_servicio1_idx` (`fk_cpb017_num_tipo_servicio` ASC),
  INDEX `fk_cpb002_num_tipo_documento` (`fk_cpb002_num_tipo_documento` ASC),
  INDEX `fk_cpb005_num_concepto_gasto` (`fk_cpb005_num_concepto_gasto` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario89`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_c002_cajachica_detalle_cp_b017_tipo_servicio1`
    FOREIGN KEY (`fk_cpb017_num_tipo_servicio`)
    REFERENCES `SIACE`.`cp_b017_tipo_servicio` (`pk_num_tipo_servico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cpb005_num_concepto_gasto`
    FOREIGN KEY (`fk_cpb005_num_concepto_gasto`)
    REFERENCES `SIACE`.`cp_b005_concepto_gasto` (`pk_num_concepto_gasto`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_c003_viatico_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_c003_viatico_detalle` (
  `pk_num_viatico_detalle` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_detalle` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_gasto` VARCHAR(25) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_uv` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_dt` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_viaticos` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_cpb011_num_viatico` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_viatico_detalle`),
  INDEX `fk_cp_c003_viatico_detalle_cp_b011_viaticos1_idx` (`fk_cpb011_num_viatico` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario90`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_d001_obligacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_d001_obligacion` (
  `pk_num_obligacion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_cpb002_num_tipo_documento` INT(11) NOT NULL,
  `fk_cbb001_num_voucher_mast_anulacion` INT(11) NULL DEFAULT NULL,
  `fk_cpb017_num_tipo_servicio` INT(11) NOT NULL,
  `fk_a023_num_centro_de_costo` INT(4) NOT NULL,
  `fk_cbb001_num_voucher_mast` INT(11) NULL DEFAULT NULL,
  `fk_a006_num_miscelaneo_tipo_pago` INT(11) NOT NULL,
  `fk_cpb014_num_cuenta` INT(11) NOT NULL,
  `fk_a003_num_persona_proveedor` INT(11) NOT NULL,
  `num_flag_generar_pago` INT(1) NOT NULL DEFAULT '0',
  `num_monto_obligacion` DECIMAL(18,6) NOT NULL,
  `num_monto_impuesto_otros` DECIMAL(18,6) NOT NULL,
  `num_monto_no_afecto` DECIMAL(18,6) NOT NULL,
  `num_monto_afecto` DECIMAL(18,6) NOT NULL,
  `num_monto_adelanto` DECIMAL(18,6) NOT NULL,
  `num_monto_impuesto` DECIMAL(18,6) NOT NULL,
  `num_monto_pago_parcial` DECIMAL(18,6) NOT NULL,
  `num_monto_descuento` DECIMAL(18,6) NULL DEFAULT NULL,
  `num_flag_cont_pendiente_pub_20` INT(1) NOT NULL DEFAULT '0',
  `num_contabilizacion_pendiente` INT(1) NOT NULL DEFAULT '0',
  `num_flag_afecto_IGV` INT(1) NOT NULL DEFAULT '0',
  `num_flag_diferido` INT(1) NOT NULL DEFAULT '0',
  `num_flag_adelanto` INT(1) NOT NULL DEFAULT '0',
  `num_flag_pago_diferido` INT(1) NOT NULL DEFAULT '0',
  `num_flag_compromiso` INT(1) NOT NULL DEFAULT '0',
  `num_flag_presupuesto` INT(1) NOT NULL DEFAULT '0',
  `num_flag_obligacion_auto` INT(1) NOT NULL DEFAULT '0',
  `num_flag_obligacion_directa` INT(1) NOT NULL DEFAULT '0',
  `num_flag_caja_chica` INT(1) NOT NULL DEFAULT '0',
  `num_flag_pago_individual` INT(1) NOT NULL DEFAULT '0',
  `num_proceso_secuencia` INT(2) NOT NULL,
  `num_flag_verificado` INT(1) NOT NULL DEFAULT '0',
  `num_flag_distribucion_manual` INT(1) NULL DEFAULT '0',
  `ind_nro_pago` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_nro_proceso` VARCHAR(6) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_nro_control` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_nro_registro` VARCHAR(6) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_comentarios` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_comentarios_adicional` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_motivo_anulacion` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_periodo_anulacion` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_tipo_descuento` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_tipo_procedencia` VARCHAR(3) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_nro_factura` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_recepcion` DATE NOT NULL,
  `fec_vencimiento` DATE NOT NULL,
  `fec_registro` DATE NOT NULL,
  `fec_documento` DATE NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_preparacion` DATETIME NOT NULL,
  `fec_aprobado` DATETIME NULL DEFAULT NULL,
  `fec_revision` DATETIME NULL DEFAULT NULL,
  `fec_pago` DATETIME NULL DEFAULT NULL,
  `fec_programada` DATETIME NOT NULL,
  `fec_factura` DATE NOT NULL,
  `fec_anulacion` DATETIME NULL DEFAULT NULL,
  `fec_conformado` DATETIME NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a003_num_persona_proveedor_a_pagar` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_ingresa` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_conforma` INT(11) NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_revisa` INT(11) NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_aprueba` INT(11) NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_anula` INT(11) NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_obligacion`),
  UNIQUE INDEX `ind_nro_control` (`ind_nro_control` ASC),
  UNIQUE INDEX `ind_nro_factura` (`ind_nro_factura` ASC),
  INDEX `CodTipoServicio` (`fk_cpb017_num_tipo_servicio` ASC),
  INDEX `CodCentroCosto` (`fk_a023_num_centro_de_costo` ASC),
  INDEX `NroPago` (`ind_nro_pago` ASC),
  INDEX `fk_cp_d001_obligacion_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_ingresa` ASC),
  INDEX `fk_cp_d001_obligacion_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_conforma` ASC),
  INDEX `fk_cp_d001_obligacion_rh_b001_empleado3_idx` (`fk_rhb001_num_empleado_revisa` ASC),
  INDEX `fk_cp_d001_obligacion_rh_b001_empleado4_idx` (`fk_rhb001_num_empleado_aprueba` ASC),
  INDEX `fk_cp_d001_obligacion_rh_b001_empleado5_idx` (`fk_rhb001_num_empleado_anula` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cp_d001_obligacion_cpb002_tipo_documento1_idx` (`fk_cpb002_num_tipo_documento` ASC),
  INDEX `fk_cp_d001_obligacion_cbb001_voucher_idx` (`fk_cbb001_num_voucher_mast` ASC),
  INDEX `fk_cp_d001_obligacion_cbb001_voucher2_idx` (`fk_cbb001_num_voucher_mast_anulacion` ASC),
  INDEX `fk_cp_d001_obligacion_a003_persona1_idx` (`fk_a003_num_persona_proveedor` ASC),
  INDEX `fk_cp_d001_obligacion_a003_persona2_idx` (`fk_a003_num_persona_proveedor_a_pagar` ASC),
  INDEX `fk_cp_d001_obligacion_cp_b014_cuenta_bancaria1_idx` (`fk_cpb014_num_cuenta` ASC),
  INDEX `fk_cp_d001_obligacion_a006_misc_detalle_tipo_pago_idx` USING BTREE (`fk_a006_num_miscelaneo_tipo_pago` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario91`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d001_obligacion_a003_persona1`
    FOREIGN KEY (`fk_a003_num_persona_proveedor`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d001_obligacion_a003_persona2`
    FOREIGN KEY (`fk_a003_num_persona_proveedor_a_pagar`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d001_obligacion_a023_centro_de_costo1`
    FOREIGN KEY (`fk_a023_num_centro_de_costo`)
    REFERENCES `SIACE`.`a023_centro_costo` (`pk_num_centro_costo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d001_obligacion_cbb001_voucher1`
    FOREIGN KEY (`fk_cbb001_num_voucher_mast`)
    REFERENCES `SIACE`.`cb_b001_voucher` (`pk_num_voucher_mast`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d001_obligacion_cbb001_voucher2`
    FOREIGN KEY (`fk_cbb001_num_voucher_mast_anulacion`)
    REFERENCES `SIACE`.`cb_b001_voucher` (`pk_num_voucher_mast`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d001_obligacion_cpb017_tipo_servicio1`
    FOREIGN KEY (`fk_cpb017_num_tipo_servicio`)
    REFERENCES `SIACE`.`cp_b017_tipo_servicio` (`pk_num_tipo_servico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 88
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_b014_almacen`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_b014_almacen` (
  `pk_num_almacen` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_almacen` VARCHAR(6) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_tipo_almacen` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'P:PRINCIPAL; T:TRANSITO; ',
  `ind_direccion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_commodity` INT(1) NOT NULL DEFAULT '0',
  `fk_cbb004_num_cuenta_inventario` INT(11) NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_rhb001_num_empleado_responsable` INT(11) NOT NULL,
  `fk_a003_num_persona_dependencia_resp` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_almacen`),
  INDEX `fk_lg_b014_almacen_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_responsable` ASC),
  INDEX `fk_lg_b014_almacen_a003_persona1_idx` (`fk_a003_num_persona_dependencia_resp` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_lg_b014_almacen_cb_b004_cuenta_inventario_idx` (`fk_cbb004_num_cuenta_inventario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario106`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b014_almacen_a003_persona1`
    FOREIGN KEY (`fk_a003_num_persona_dependencia_resp`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b014_almacen_cb_b004_cuenta_inventario1`
    FOREIGN KEY (`fk_cbb004_num_cuenta_inventario`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_b017_clasificacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_b017_clasificacion` (
  `pk_num_clasificacion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a006_num_miscelaneos_tipo_requerimiento` INT(11) NOT NULL COMMENT 'miscelaneos->TIPOREQ',
  `num_flag_recepcion_almacen` INT(1) NOT NULL DEFAULT '0',
  `num_flag_revision` INT(1) NOT NULL DEFAULT '0',
  `num_flag_transaccion` INT(1) NOT NULL DEFAULT '0',
  `num_flag_caja_chica` INT(1) NOT NULL DEFAULT '0',
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `num_flag_activo_fijo` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_lgb014_num_almacen` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_clasificacion`),
  INDEX `fk_lg_b017_clasificacion_lg_b014_almacen1_idx` (`fk_lgb014_num_almacen` ASC),
  INDEX `fk_lg_b017_clasificacion_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneos_tipo_requerimiento` ASC),
  INDEX `fk_lg_b017_clasificacion_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_lg_b017_clasificacion_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b017_clasificacion_lg_b014_almacen1`
    FOREIGN KEY (`fk_lgb014_num_almacen`)
    REFERENCES `SIACE`.`lg_b014_almacen` (`pk_num_almacen`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_b019_orden`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_b019_orden` (
  `pk_num_orden` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `ind_orden` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a004_num_dependencia` INT(11) NOT NULL COMMENT 'mastdependencias->CodDependencia',
  `fec_prometida` DATE NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_creado_por` INT(11) NOT NULL COMMENT 'mastpersonas->CodPersona',
  `fec_creacion` DATETIME NOT NULL,
  `fk_rhb001_num_empleado_revisado_por` INT(11) NOT NULL COMMENT 'mastpersonas->CodPersona',
  `fec_revision` DATETIME NOT NULL,
  `fk_rhb001_num_empleado_aprobado_por` INT(11) NOT NULL COMMENT 'mastpersonas->CodPersona',
  `fec_aprobacion` DATETIME NOT NULL,
  `app_tipo_servicio` INT(11) NOT NULL COMMENT 'masttiposervicio->CodTipoServicio',
  `num_monto_bruto` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_igv` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_otros` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_total` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_pendiente` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_afecto` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_no_afecto` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_pagado` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `fk_a006_num_miscelaneos_forma_pago` INT(11) NOT NULL,
  `fk_lgb014_num_almacen` INT(11) NOT NULL COMMENT 'lg_almacenmast->CodAlmacen',
  `num_plazo_entrega` INT(11) NULL DEFAULT NULL,
  `ind_entregar_en` VARCHAR(75) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_direccion_entrega` VARCHAR(75) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_comentario` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_motivo_rechazo` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_flag_transferencia` INT(1) NOT NULL DEFAULT '0',
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'PR:En Preparacion; RV:Revisado; AP:Aprobado; AN:Anulado; RE:Rechazado; CE:Cerrado; CO:Completado;',
  `fk_cbb004_num_plan_cuenta_oncop` INT(11) NOT NULL COMMENT 'ac_mastplancuenta->CodCuenta',
  `fk_cbb004_num_plan_cuenta_pub_veinte` INT(11) NOT NULL COMMENT 'ac_mastplancuenta20->CodCuenta',
  `fk_prb002_num_partida_presupuestaria` INT(11) NOT NULL COMMENT 'pv_partida->cod_partida',
  `num_flag_verificado_presupuesto` INT(1) NOT NULL DEFAULT '0',
  `fk_rhb001_num_empleado_verificado_presupuesto` INT(11) NULL DEFAULT NULL,
  `fec_verificado_presupuesto` DATE NULL DEFAULT NULL,
  `num_flag_verificado_imputacion` INT(1) NOT NULL DEFAULT '0',
  `fk_rhb001_num_empleado_verificado_imputacion` INT(11) NULL DEFAULT NULL,
  `fec_verificado_imputacion` DATE NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_lgb013_num_adjudicacion` INT(11) NULL DEFAULT NULL,
  `fk_lgb022_num_proveedor` INT(11) NOT NULL,
  `fk_lgb017_num_clasificacion` INT(11) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_rhb001_num_empleado1` INT(11) NOT NULL,
  `fk_rhb001_num_empleado2` INT(11) NOT NULL,
  `fk_rhb001_num_empleado3` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_orden`),
  INDEX `Index_2` (`fk_a004_num_dependencia` ASC),
  INDEX `Index_3` (`fk_rhb001_num_empleado_creado_por` ASC),
  INDEX `Index_4` (`fk_rhb001_num_empleado_revisado_por` ASC),
  INDEX `Index_5` (`fk_rhb001_num_empleado_aprobado_por` ASC),
  INDEX `Index_7` (`fk_lgb014_num_almacen` ASC),
  INDEX `Index_8` (`fk_cbb004_num_plan_cuenta_oncop` ASC),
  INDEX `Index_9` (`fk_prb002_num_partida_presupuestaria` ASC),
  INDEX `FK_lg_ordencompra_3` (`app_tipo_servicio` ASC),
  INDEX `FK_lg_ordencompra_4` (`fk_a006_num_miscelaneos_forma_pago` ASC),
  INDEX `fk_lg_b019_orden_lg_b017_clasificacion1_idx` (`fk_lgb017_num_clasificacion` ASC),
  INDEX `fk_lg_b019_orden_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_lg_b019_orden_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado1` ASC),
  INDEX `fk_lg_b019_orden_rh_b001_empleado3_idx` (`fk_rhb001_num_empleado2` ASC),
  INDEX `fk_lg_b019_orden_rh_b001_empleado4_idx` (`fk_rhb001_num_empleado3` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_lg_b019_orden_cb_b004_plan_cuenta1_idx` (`fk_cbb004_num_plan_cuenta_oncop` ASC, `fk_cbb004_num_plan_cuenta_pub_veinte` ASC),
  INDEX `fk_lg_b019_orden_rh_b001_empleado8_idx` (`fk_rhb001_num_empleado_verificado_presupuesto` ASC),
  INDEX `fk_lg_b019_orden_rh_b001_empleado9_idx` (`fk_rhb001_num_empleado_verificado_imputacion` ASC),
  INDEX `fk_lg_b019_orden_lg_b013_adjudicacion1_idx` (`fk_lgb013_num_adjudicacion` ASC),
  INDEX `fk_lg_b019_orden_lg_b022_proveedor1_idx` (`fk_lgb022_num_proveedor` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario125`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b019_orden_lg_b017_clasificacion1`
    FOREIGN KEY (`fk_lgb017_num_clasificacion`)
    REFERENCES `SIACE`.`lg_b017_clasificacion` (`pk_num_clasificacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_d002_documento_obligacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_d002_documento_obligacion` (
  `pk_num_documento_obligacion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_cpd001_num_obligacion` INT(11) NOT NULL,
  `fk_lgb019_num_orden` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_documento_obligacion`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cpd001_num_obligacion` (`fk_cpd001_num_obligacion` ASC),
  INDEX `fk_lgb019_num_orden` (`fk_lgb019_num_orden` ASC),
  CONSTRAINT `compras`
    FOREIGN KEY (`fk_lgb019_num_orden`)
    REFERENCES `SIACE`.`lg_b019_orden` (`pk_num_orden`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a018_num_seguridad_usuario92`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `obligacion`
    FOREIGN KEY (`fk_cpd001_num_obligacion`)
    REFERENCES `SIACE`.`cp_d001_obligacion` (`pk_num_obligacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_d003_obligacion_impuesto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_d003_obligacion_impuesto` (
  `pk_num_obligaciones_impuesto` INT(11) NOT NULL AUTO_INCREMENT,
  `num_linea` INT(2) NOT NULL,
  `cod_impuesto` INT(3) NOT NULL COMMENT 'mastimpuestos->CodImpuesto',
  `ind_concepto` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'pr_concepto->CodConcepto',
  `num_monto_afecto` DECIMAL(18,6) NOT NULL,
  `num_flag_provision` INT(1) NOT NULL DEFAULT '0' COMMENT 'P:PAGO DEL DOCUMENTO; N:PROVISION DEL DOCUMENTO;',
  `fk_cbb004_num_plan_cuenta` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_cpd002_num_documento_obligacion` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_obligaciones_impuesto`),
  INDEX `fk_cp_d003_obligacion_impuesto_cp_d002_documento_obligacion_idx` (`fk_cpd002_num_documento_obligacion` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cp_d003_obligacion_impuesto_cbb004_plan_cuenta1_idx` (`fk_cbb004_num_plan_cuenta` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario93`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d003_obligacion_impuesto_cbb004_plan_cuenta1`
    FOREIGN KEY (`fk_cbb004_num_plan_cuenta`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d003_obligacion_impuesto_cp_d002_documento_obligacion1`
    FOREIGN KEY (`fk_cpd002_num_documento_obligacion`)
    REFERENCES `SIACE`.`cp_d002_documento_obligacion` (`pk_num_documento_obligacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_d004_caja_chica`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_d004_caja_chica` (
  `pk_num_caja_chica` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_num_voucher_mast` INT(11) NOT NULL,
  `fk_cpb004_num_clasificacion_gastos` INT(11) NOT NULL,
  `num_flag_caja_chica` INT(1) NOT NULL COMMENT 'C:Caja Chica; R:Reporte de Gasto',
  `fec_anio` YEAR NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'mastorganismos->CodOrganismo',
  `ind_num_caja_chica` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_preparacion` DATETIME NOT NULL,
  `fec_aprobacion` DATETIME NULL DEFAULT NULL,
  `fec_pago` DATE NOT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_razon_rechazo` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_monto_total` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_adelantos` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_neto` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_rhb001_num_empleado_persona_pagar` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_prepara` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_beneficiario` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_aprueba` INT(11) NULL DEFAULT NULL,
  `fk_a023_num_centro_costo` INT(11) NOT NULL,
  `fk_a004_num_dependencia` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipo_pago` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_caja_chica`),
  INDEX `fk_cp_d004_caja_chica_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_persona_pagar` ASC),
  INDEX `fk_cp_d004_caja_chica_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_prepara` ASC),
  INDEX `fk_cp_d004_caja_chica_rh_b001_empleado3_idx` (`fk_rhb001_num_empleado_beneficiario` ASC),
  INDEX `fk_cp_d004_caja_chica_rh_b001_empleado4_idx` (`fk_rhb001_num_empleado_aprueba` ASC),
  INDEX `fk_cp_d004_caja_chica_a023_centro_costo1_idx` (`fk_a023_num_centro_costo` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cp_d004_caja_chica_cp_b004_clasificacion_gastos1_idx` (`fk_cpb004_num_clasificacion_gastos` ASC),
  INDEX `fk_cp_d004_caja_chica_a004_dependencia1_idx` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_cp_d004_caja_chica_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle_tipo_pago` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario88`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`fk_a018_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d004_caja_chica_a004_dependencia1`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d004_caja_chica_a023_centro_costo1`
    FOREIGN KEY (`fk_a023_num_centro_costo`)
    REFERENCES `SIACE`.`a023_centro_costo` (`pk_num_centro_costo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d004_caja_chica_cp_b004_clasificacion_gastos1`
    FOREIGN KEY (`fk_cpb004_num_clasificacion_gastos`)
    REFERENCES `SIACE`.`cp_b004_clasificacion_gastos` (`pk_num_clasificacion_gastos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_d005_cajachica_autorizaciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_d005_cajachica_autorizaciones` (
  `pk_num_caja_autorizaciones` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_cpd004_num_caja_chica` INT(11) NOT NULL,
  `fk_cpb013_num_cajachica_autorizacion` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_caja_autorizaciones`),
  INDEX `fk_cp_d005_cajachica_autorizaciones_cp_d004_caja_chica1_idx` (`fk_cpd004_num_caja_chica` ASC),
  INDEX `fk_cp_d005_cajachica_autorizaciones_cp_b013_cajachica_autor_idx` (`fk_cpb013_num_cajachica_autorizacion` ASC),
  CONSTRAINT `fk_cp_d005_cajachica_autorizaciones_cp_b013_cajachica_autoriz1`
    FOREIGN KEY (`fk_cpb013_num_cajachica_autorizacion`)
    REFERENCES `SIACE`.`cp_b013_cajachica_autorizacion` (`pk_num_cajachica_autorizacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_d006_cuenta_bancaria_balance`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_d006_cuenta_bancaria_balance` (
  `pk_num_cuenta_balance` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_comentarios` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_transaccion` DATETIME NOT NULL,
  `num_monto_transaccion` DECIMAL(18,6) NOT NULL,
  `num_saldo_anterior` DECIMAL(18,6) NOT NULL,
  `num_saldo_actual` DECIMAL(18,6) NOT NULL,
  `num_saldo_inicial` DECIMAL(18,6) NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_cpb014_pk_num_cuenta_bancaria` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_cuenta_balance`),
  INDEX `fk_cp_d006_cuenta_bancaria_balance_cp_b014_cuenta_bancaria1_idx` (`fk_cpb014_pk_num_cuenta_bancaria` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario94`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_d007_diferencia_saldos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_d007_diferencia_saldos` (
  `pk_num_diferencia_saldo` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ind_tipo_banco_libro` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_anio` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estado_cuenta` DECIMAL(18,6) NOT NULL,
  `ind_descripcion` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_cpb014_num_cuenta_bancaria` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_diferencia_saldo`),
  UNIQUE INDEX `IdDiferenciaR` (`pk_num_diferencia_saldo` ASC),
  INDEX `fk_cp_d007_diferencia_saldos_cp_b014_cuenta_bancaria1_idx` (`fk_cpb014_num_cuenta_bancaria` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario95`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_d008_diferencia_conciliacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_d008_diferencia_conciliacion` (
  `pk_num_diferencia_saldo` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fec_anio` YEAR NOT NULL,
  `fec_fecha` DATE NOT NULL,
  `ind_carga_abono` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_num_orden` VARCHAR(11) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_num_proceso` VARCHAR(11) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_bolivares` DECIMAL(18,6) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_cpd007_num_diferencia_saldo` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`pk_num_diferencia_saldo`),
  UNIQUE INDEX `IdDiferencia` (`pk_num_diferencia_saldo` ASC),
  INDEX `fk_cp_d008_diferencia_conciliacion_cp_d007_diferencia_saldo_idx` (`fk_cpd007_num_diferencia_saldo` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario96`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d008_diferencia_conciliacion_cp_d007_diferencia_saldos1`
    FOREIGN KEY (`fk_cpd007_num_diferencia_saldo`)
    REFERENCES `SIACE`.`cp_d007_diferencia_saldos` (`pk_num_diferencia_saldo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_d009_orden_pago`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_d009_orden_pago` (
  `pk_num_orden_pago` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_cbb001_num_voucher_asiento` INT(11) NULL DEFAULT NULL,
  `fk_cbb001_num_voucher_asiento_anulacion` INT(11) NULL DEFAULT NULL,
  `fk_cpd001_num_obligacion` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_genera` INT(11) NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_transfiere` INT(11) NULL DEFAULT NULL,
  `fk_cpb002_num_tipo_documento` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_tipo_pago` INT(11) NOT NULL,
  `fk_cpb014_num_cuenta` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_anula` INT(11) NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_orden_pago` DATE NOT NULL,
  `fec_generado` DATETIME NULL DEFAULT NULL,
  `fec_transferencia` DATETIME NULL DEFAULT NULL,
  `fec_anulacion` DATETIME NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `ind_motivo_anulacion` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_num_orden` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' NOT NULL DEFAULT 'PE' COMMENT 'PE:PENDIENTE; GE:GENERADO; PA:PAGADO; AN:ANULADO;',
  `ind_nro_control` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_monto` DECIMAL(18,6) NOT NULL,
  `ind_comentarios` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  PRIMARY KEY (`pk_num_orden_pago`),
  UNIQUE INDEX `NroOrden_UNIQUE` (`ind_num_orden` ASC),
  INDEX `fk_cp_d009_orden_pago_cp_d001_obligacion1_idx` (`fk_cpd001_num_obligacion` ASC),
  INDEX `fk_cp_d009_orden_pago_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_transfiere` ASC),
  INDEX `fk_cp_d009_orden_pago_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_genera` ASC),
  INDEX `fk_cp_d009_orden_pago_rh_b001_empleado4_idx` (`fk_rhb001_num_empleado_anula` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cp_d009_orden_pago_cb_b001_voucher1_idx` (`fk_cbb001_num_voucher_asiento` ASC),
  INDEX `fk_cp_d009_orden_pago_cb_b001_voucher2_idx` (`fk_cbb001_num_voucher_asiento_anulacion` ASC),
  INDEX `orden_pago_cp_b002_tipo_documento` (`fk_cpb002_num_tipo_documento` ASC),
  INDEX `orden_pago_a006_miscelaneo_detalle` (`fk_a006_num_miscelaneo_tipo_pago` ASC),
  INDEX `orden_pago_cp_b014_cuenta_bancaria` (`fk_cpb014_num_cuenta` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario97`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d009_orden_pago_cb_b001_voucher1`
    FOREIGN KEY (`fk_cbb001_num_voucher_asiento`)
    REFERENCES `SIACE`.`cb_b001_voucher` (`pk_num_voucher_mast`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d009_orden_pago_cb_b001_voucher2`
    FOREIGN KEY (`fk_cbb001_num_voucher_asiento_anulacion`)
    REFERENCES `SIACE`.`cb_b001_voucher` (`pk_num_voucher_mast`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d009_orden_pago_cp_d001_obligacion1`
    FOREIGN KEY (`fk_cpd001_num_obligacion`)
    REFERENCES `SIACE`.`cp_d001_obligacion` (`pk_num_obligacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `orden_pago_a006_miscelaneo_detalle`
    FOREIGN KEY (`fk_a006_num_miscelaneo_tipo_pago`)
    REFERENCES `SIACE`.`a006_miscelaneo_detalle` (`pk_num_miscelaneo_detalle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `orden_pago_cp_b002_tipo_documento`
    FOREIGN KEY (`fk_cpb002_num_tipo_documento`)
    REFERENCES `SIACE`.`cp_b002_tipo_documento` (`pk_num_tipo_documento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `orden_pago_cp_b014_cuenta_bancaria`
    FOREIGN KEY (`fk_cpb014_num_cuenta`)
    REFERENCES `SIACE`.`cp_b014_cuenta_bancaria` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 58
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_d010_pago`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_d010_pago` (
  `pk_num_pago` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_cbb001_num_voucher_pago` INT(11) NULL DEFAULT NULL,
  `fk_cbb001_num_voucher_anulacion` INT(11) NULL DEFAULT NULL,
  `fk_cpd009_num_orden_pago` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_crea` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_anula` INT(11) NULL DEFAULT NULL,
  `ind_num_pago` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_num_cheque` VARCHAR(6) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `ind_motivo_anulacion` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'GE' COMMENT 'GE:GENERADO; IM:IMPRESO; AN:ANULADO;',
  `ind_estado_entrega` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'C' COMMENT 'C:CUSTODIA; E:ENTREGADO;',
  `fec_anio` YEAR NOT NULL COMMENT 'ap_ordenpago',
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_pago` DATETIME NOT NULL,
  `fec_entregado` DATETIME NULL DEFAULT NULL,
  `fec_cobrado` DATETIME NULL DEFAULT NULL,
  `fec_devuelto` DATETIME NULL DEFAULT NULL,
  `fec_anulacion` DATETIME NULL DEFAULT NULL,
  `fec_diferido` DATETIME NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `num_monto_pago` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_retenido` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_flag_cobrado` TINYINT(1) NOT NULL,
  `num_flag_negociable` TINYINT(1) NOT NULL,
  PRIMARY KEY (`pk_num_pago`),
  INDEX `fk_cp_d010_pago_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_crea` ASC),
  INDEX `fk_cp_d010_pago_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_anula` ASC),
  INDEX `fk_cp_d010_pago_cp_d009_orden_pago1_idx` (`fk_cpd009_num_orden_pago` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cp_d010_pago_cbb001_num_voucher1_idx` (`fk_cbb001_num_voucher_pago` ASC),
  INDEX `fk_cp_d010_pago_cbb001_num_voucher2_idx` (`fk_cbb001_num_voucher_anulacion` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario98`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d010_pago_cbb001_num_voucher1`
    FOREIGN KEY (`fk_cbb001_num_voucher_pago`)
    REFERENCES `SIACE`.`cb_b001_voucher` (`pk_num_voucher_mast`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d010_pago_cbb001_num_voucher2`
    FOREIGN KEY (`fk_cbb001_num_voucher_anulacion`)
    REFERENCES `SIACE`.`cb_b001_voucher` (`pk_num_voucher_mast`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d010_pago_cp_d009_orden_pago1`
    FOREIGN KEY (`fk_cpd009_num_orden_pago`)
    REFERENCES `SIACE`.`cp_d009_orden_pago` (`pk_num_orden_pago`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 76
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_d011_banco_transaccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_d011_banco_transaccion` (
  `pk_num_banco_transaccion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_cpb006_num_banco_tipo_transaccion` INT(11) NOT NULL,
  `fk_cpb002_num_tipo_documento` INT(11) NOT NULL,
  `fk_a023_num_centro_costo` INT(11) NOT NULL,
  `fk_cpd010_num_pago` INT(11) NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_crea` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_revisa` INT(11) NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_aprueba` INT(11) NULL DEFAULT NULL,
  `ind_num_transaccion` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_periodo_contable` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'PR:PENDIENTE; AP:ACTUALIZADO; CO:CONTABILIZADO',
  `fec_anio` YEAR NOT NULL,
  `fec_revisado` DATETIME NULL DEFAULT NULL,
  `fec_aprobado` DATETIME NULL DEFAULT NULL,
  `fec_transaccion` DATETIME NULL DEFAULT NULL,
  `fec_conciliacion` DATE NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `num_monto` DECIMAL(18,6) NULL DEFAULT NULL,
  `num_secuencia` INT(1) NOT NULL,
  `num_flag_genera_voucher` INT(1) NOT NULL DEFAULT '0',
  `num_flag_conciliacion` INT(1) NOT NULL DEFAULT '0',
  `num_flag_presupuesto` INT(1) NOT NULL DEFAULT '0',
  `txt_comentarios` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_mes` VARCHAR(2) CHARACTER SET 'utf8' NOT NULL COMMENT 'PR:PENDIENTE; AP:ACTUALIZADO; CO:CONTABILIZADO',
  `fec_preparacion` DATETIME NOT NULL,
  `fk_a003_num_persona_proveedor` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `txt_comentario_anulacion` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_cpb014_num_cuenta_bancaria` INT(11) NULL DEFAULT NULL,
  `fk_prb002_num_partida_presupuestaria` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_banco_transaccion`),
  INDEX `fk_cp_d011_banco_transaccion_cp_b002_tipo_documento1_idx` (`fk_cpb002_num_tipo_documento` ASC),
  INDEX `fk_cp_d011_banco_transaccion_cp_b006_banco_tipo_transaccion_idx` (`fk_cpb006_num_banco_tipo_transaccion` ASC),
  INDEX `fk_cp_d011_banco_transaccion_cp_d010_pago1_idx` (`fk_cpd010_num_pago` ASC),
  INDEX `fk_cp_d011_banco_transaccion_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_crea` ASC),
  INDEX `fk_cp_d011_banco_transaccion_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_revisa` ASC),
  INDEX `fk_cp_d011_banco_transaccion_rh_b001_empleado3_idx` (`fk_rhb001_num_empleado_aprueba` ASC),
  INDEX `fk_cp_d011_banco_transaccion_a023_centro_costo1_idx` (`fk_a023_num_centro_costo` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cp_d011_banco_transaccion_a003_persona1_idx` (`fk_a003_num_persona_proveedor` ASC),
  INDEX `fk_cp_d011_banco_transaccion_cp_b014_cuenta_bancaria1_idx` (`fk_cpb014_num_cuenta_bancaria` ASC),
  INDEX `fk_cp_d011_banco_transaccion_pr_b002_partida_presupuestaria` (`fk_prb002_num_partida_presupuestaria` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario99`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d011_banco_transaccion_a003_persona1`
    FOREIGN KEY (`fk_a003_num_persona_proveedor`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d011_banco_transaccion_a023_centro_costo1`
    FOREIGN KEY (`fk_a023_num_centro_costo`)
    REFERENCES `SIACE`.`a023_centro_costo` (`pk_num_centro_costo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d011_banco_transaccion_cp_b006_banco_tipo_transaccion1`
    FOREIGN KEY (`fk_cpb006_num_banco_tipo_transaccion`)
    REFERENCES `SIACE`.`cp_b006_banco_tipo_transaccion` (`pk_num_banco_tipo_transaccion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d011_banco_transaccion_cp_b014_cuenta_bancaria1`
    FOREIGN KEY (`fk_cpb014_num_cuenta_bancaria`)
    REFERENCES `SIACE`.`cp_b014_cuenta_bancaria` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d011_banco_transaccion_cp_d010_pago1`
    FOREIGN KEY (`fk_cpd010_num_pago`)
    REFERENCES `SIACE`.`cp_d010_pago` (`pk_num_pago`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d011_banco_transaccion_pr_b002_partida_presupuestaria`
    FOREIGN KEY (`fk_prb002_num_partida_presupuestaria`)
    REFERENCES `SIACE`.`pr_b002_partida_presupuestaria` (`pk_num_partida_presupuestaria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 165
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`nm_b002_concepto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`nm_b002_concepto` (
  `pk_num_concepto` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a006_num_tipo_concepto` INT(11) NOT NULL,
  `cod_concepto` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_impresion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_orden_planilla` INT(2) NOT NULL DEFAULT '0',
  `ind_formula` MEDIUMTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fec_ultima_moficacion` DATETIME NOT NULL,
  `ind_abrebiatura` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_automatico` INT(1) NULL DEFAULT NULL,
  `num_flag_bono` INT(1) NULL DEFAULT NULL,
  `num_flag_incidencia` INT(1) NULL DEFAULT NULL,
  `num_flag_jubilacion` INT(1) NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_concepto`),
  UNIQUE INDEX `UNICO_cod_concepto` (`cod_concepto` ASC),
  UNIQUE INDEX `ind_abrebiatura_UNIQUE` (`ind_abrebiatura` ASC),
  INDEX `fk_nm_b001_concepto_miscelaneos1_idx` (`fk_a006_num_tipo_concepto` ASC),
  INDEX `fk_nm_b002_concepto_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_nm_b002_concepto_a018_seguridad_usuario`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 42
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_d012_obligacion_impuesto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_d012_obligacion_impuesto` (
  `pk_num_obligacion_impuesto` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_cpd001_num_obligacion` INT(11) NOT NULL,
  `fk_a003_num_persona_proveedor` INT(11) NOT NULL,
  `ind_secuencia` INT(2) NOT NULL,
  `fk_cpb015_num_impuesto` INT(11) NULL DEFAULT NULL,
  `fk_nmb002_num_concepto` INT(11) NULL DEFAULT NULL,
  `num_monto_afecto` DECIMAL(18,6) NOT NULL,
  `num_monto_impuesto` DECIMAL(18,6) NULL DEFAULT NULL,
  `fk_cbb004_num_cuenta` INT(11) NULL DEFAULT NULL,
  `fk_cbb004_num_cuenta_pub20` INT(11) NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_obligacion_impuesto`),
  INDEX `fk_cp_d012_obligacion_impuesto_cp_d001_obligacion1_idx` (`fk_cpd001_num_obligacion` ASC),
  INDEX `fk_cp_d012_obligacion_impuesto_a003_persona1_idx` (`fk_a003_num_persona_proveedor` ASC),
  INDEX `fk_cp_d012_obligacion_impuesto_cp_b015_impuesto1_idx` (`fk_cpb015_num_impuesto` ASC),
  INDEX `fk_cp_d012_obligacion_impuesto_nm_b002_concepto1_idx` (`fk_nmb002_num_concepto` ASC),
  INDEX `fk_cp_d012_obligacion_impuesto_cb_b004_plan_cuenta1_idx` (`fk_cbb004_num_cuenta` ASC),
  INDEX `fk_cp_d012_obligacion_impuesto_cb_b004_plan_cuenta2_idx` (`fk_cbb004_num_cuenta_pub20` ASC),
  INDEX `fk_cp_d012_obligacion_impuesto_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_cp_d012_obligacion_impuesto_a003_persona1`
    FOREIGN KEY (`fk_a003_num_persona_proveedor`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d012_obligacion_impuesto_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d012_obligacion_impuesto_cb_b004_plan_cuenta1`
    FOREIGN KEY (`fk_cbb004_num_cuenta`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d012_obligacion_impuesto_cb_b004_plan_cuenta2`
    FOREIGN KEY (`fk_cbb004_num_cuenta_pub20`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d012_obligacion_impuesto_cp_b015_impuesto1`
    FOREIGN KEY (`fk_cpb015_num_impuesto`)
    REFERENCES `SIACE`.`cp_b015_impuesto` (`pk_num_impuesto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d012_obligacion_impuesto_cp_d001_obligacion1`
    FOREIGN KEY (`fk_cpd001_num_obligacion`)
    REFERENCES `SIACE`.`cp_d001_obligacion` (`pk_num_obligacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d012_obligacion_impuesto_nm_b002_concepto1`
    FOREIGN KEY (`fk_nmb002_num_concepto`)
    REFERENCES `SIACE`.`nm_b002_concepto` (`pk_num_concepto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 66
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_d013_obligacion_cuentas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_d013_obligacion_cuentas` (
  `pk_num_obligacion_cuentas` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_cpd001_num_obligacion` INT(11) NOT NULL,
  `fk_a003_num_persona_proveedor` INT(11) NOT NULL,
  `num_secuencia` INT(2) NOT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_monto` DECIMAL(18,6) NOT NULL,
  `num_flag_no_afecto` INT(1) NOT NULL,
  `fk_a023_num_centro_costo` INT(11) NOT NULL,
  `fk_cbb004_num_cuenta` INT(11) NULL DEFAULT NULL,
  `fk_cbb004_num_cuenta_pub20` INT(11) NULL DEFAULT NULL,
  `fk_prb002_num_partida_presupuestaria` INT(11) NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_obligacion_cuentas`),
  INDEX `fk_cp_d013_obligacion_cuentas_cp_d001_obligacion1_idx` (`fk_cpd001_num_obligacion` ASC),
  INDEX `fk_cp_d013_obligacion_cuentas_a003_persona1_idx` (`fk_a003_num_persona_proveedor` ASC),
  INDEX `fk_cp_d013_obligacion_cuentas_pr_b002_partida_presupuestari_idx` (`fk_prb002_num_partida_presupuestaria` ASC),
  INDEX `fk_cp_d013_obligacion_cuentas_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cp_d013_obligacion_cuentas_a023_centro_costo1_idx` (`fk_a023_num_centro_costo` ASC),
  INDEX `fk_cp_d013_obligacion_cuentas_cb_b004_plan_cuenta1_idx` (`fk_cbb004_num_cuenta` ASC),
  INDEX `fk_cp_d013_obligacion_cuentas_cb_b004_plan_cuenta2_idx` (`fk_cbb004_num_cuenta_pub20` ASC),
  CONSTRAINT `fk_cp_d013_obligacion_cuentas_a003_persona1`
    FOREIGN KEY (`fk_a003_num_persona_proveedor`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d013_obligacion_cuentas_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d013_obligacion_cuentas_a023_centro_costo1`
    FOREIGN KEY (`fk_a023_num_centro_costo`)
    REFERENCES `SIACE`.`a023_centro_costo` (`pk_num_centro_costo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d013_obligacion_cuentas_cb_b004_plan_cuenta1`
    FOREIGN KEY (`fk_cbb004_num_cuenta`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d013_obligacion_cuentas_cb_b004_plan_cuenta2`
    FOREIGN KEY (`fk_cbb004_num_cuenta_pub20`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d013_obligacion_cuentas_cp_d001_obligacion1`
    FOREIGN KEY (`fk_cpd001_num_obligacion`)
    REFERENCES `SIACE`.`cp_d001_obligacion` (`pk_num_obligacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d013_obligacion_cuentas_pr_b002_partida_presupuestaria1`
    FOREIGN KEY (`fk_prb002_num_partida_presupuestaria`)
    REFERENCES `SIACE`.`pr_b002_partida_presupuestaria` (`pk_num_partida_presupuestaria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 289
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_d014_orden_pago_contabilidad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_d014_orden_pago_contabilidad` (
  `pk_num_orden_pago_contabilidad` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_anio` YEAR NOT NULL,
  `fk_cpd009_num_orden_pago` INT(11) NOT NULL,
  `num_secuencia` INT(3) NOT NULL,
  `fk_cbb004_num_cuenta` INT(11) NULL DEFAULT NULL,
  `fk_cbb004_num_cuenta_pub20` INT(11) NULL DEFAULT NULL,
  `num_monto` DECIMAL(18,6) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_orden_pago_contabilidad`),
  INDEX `fk_cp_d014_orden_pago_contabilidad_cp_d009_orden_pago1_idx` (`fk_cpd009_num_orden_pago` ASC),
  INDEX `fk_cp_d014_orden_pago_contabilidad_cb_b004_plan_cuenta1_idx` (`fk_cbb004_num_cuenta` ASC),
  INDEX `fk_cp_d014_orden_pago_contabilidad_cb_b004_plan_cuenta2_idx` (`fk_cbb004_num_cuenta_pub20` ASC),
  INDEX `fk_cp_d014_orden_pago_contabilidad_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_cp_d014_orden_pago_contabilidad_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d014_orden_pago_contabilidad_cb_b004_plan_cuenta1`
    FOREIGN KEY (`fk_cbb004_num_cuenta`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d014_orden_pago_contabilidad_cb_b004_plan_cuenta2`
    FOREIGN KEY (`fk_cbb004_num_cuenta_pub20`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_d014_orden_pago_contabilidad_cp_d009_orden_pago1`
    FOREIGN KEY (`fk_cpd009_num_orden_pago`)
    REFERENCES `SIACE`.`cp_d009_orden_pago` (`pk_num_orden_pago`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 644
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cp_e001_retenciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cp_e001_retenciones` (
  `pk_num_retenciones` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_b015_num_impuesto` INT(11) NOT NULL COMMENT 'mastimpuestos->CodImpuesto',
  `fec_anio` YEAR NOT NULL,
  `ind_num_comprobante` VARCHAR(8) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_num_control` VARCHAR(25) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_tipo_comprobante` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_periodo_fiscal` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_comprobante` DATETIME NOT NULL,
  `fec_factura` DATETIME NOT NULL,
  `num_monto_afecto` DECIMAL(18,6) NOT NULL,
  `num_monto_no_afecto` DECIMAL(18,6) NOT NULL,
  `num_monto_impuesto` DECIMAL(18,6) NOT NULL,
  `num_monto_factura` DECIMAL(18,6) NOT NULL,
  `num_monto_retenido` DECIMAL(18,6) NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'PA' COMMENT 'PA:PAGADO; AN:ANULADO',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_cpd010_num_orden_pago` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_retenciones`),
  INDEX `fk_cp_e001_retenciones_cp_d009_orden_pago1_idx` (`fk_cpd010_num_orden_pago` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cp_e001_retenciones_cb015_impuesto1_idx` (`fk_b015_num_impuesto` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario100`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_e001_retenciones_cb015_impuesto1`
    FOREIGN KEY (`fk_b015_num_impuesto`)
    REFERENCES `SIACE`.`cp_b015_impuesto` (`pk_num_impuesto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cp_e001_retenciones_cp_d009_orden_pago1`
    FOREIGN KEY (`fk_cpd010_num_orden_pago`)
    REFERENCES `SIACE`.`cp_d009_orden_pago` (`pk_num_orden_pago`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 65
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`cr_b001_equipo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cr_b001_equipo` (
  `pk_num_equipo` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador autoincremental de equipos',
  `fk_a004_num_dependencia` INT(11) NOT NULL COMMENT 'Dependencia donde se encuentra el equipo',
  `ind_nombre_equipo` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Nombre del equipo',
  `ind_direccion_fisica` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Dirección asignada por el Mysar',
  `ind_ip` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'ip del equipo',
  `num_estado` INT(1) NULL DEFAULT NULL,
  `fec_fecha_modificacion` DATETIME NOT NULL COMMENT 'Última fecha de modificación',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Usuario que realiza la modificación',
  PRIMARY KEY (`pk_num_equipo`),
  INDEX `fk_cr_b001_equipo_a001_dependencia_idx` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario101`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cr_b001_equipo_a004_dependencia`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Registro de equipos y usuarios';


-- -----------------------------------------------------
-- Table `SIACE`.`cr_c002_usuario_equipo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cr_c002_usuario_equipo` (
  `pk_num_equipo` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id del equipo',
  `fk_rhb001_num_empleado` INT(11) NOT NULL COMMENT 'Empleado responsable del equipo',
  PRIMARY KEY (`pk_num_equipo`),
  INDEX `fk_cr_c002_usuario_equipo_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  CONSTRAINT `fk_cr_c002_usuario_equipo_cr_b001_equipo`
    FOREIGN KEY (`pk_num_equipo`)
    REFERENCES `SIACE`.`cr_b001_equipo` (`pk_num_equipo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Se encarga de unir el usuario con el equipo asignado';


-- -----------------------------------------------------
-- Table `SIACE`.`cr_c006_reporte`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cr_c006_reporte` (
  `pk_num_reporte` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id autoincremental del reporte',
  `fec_fecha_inicio` DATE NOT NULL COMMENT 'fecha de inicio de la consulta',
  `fec_fecha_fin` DATE NOT NULL COMMENT 'Fecha fin de la consulta de consumo de red',
  `fec_fecha_reporte` DATE NOT NULL COMMENT 'Fecha en la que se guardó el reporte',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Usuario que generó el reporte',
  PRIMARY KEY (`pk_num_reporte`),
  INDEX `fk_cr_c006_reporte_a018_seguridad_usuario1` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Registra los datos del reporte';


-- -----------------------------------------------------
-- Table `SIACE`.`cr_c003_descarga_equipo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cr_c003_descarga_equipo` (
  `pk_num_descarga` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id de descarga',
  `fk_crc006_num_reporte` INT(11) NOT NULL COMMENT 'Reporte al que pertenece la descarga',
  `fk_crb001_num_equipo` INT(11) NOT NULL COMMENT 'Equipo donde se realizó la descarga',
  `num_descarga_total` INT(11) NOT NULL COMMENT 'Descarga total del equipo',
  PRIMARY KEY (`pk_num_descarga`),
  INDEX `fk_cr_c003_descarga_equipo_cr_c006_reporte_idx` (`fk_crc006_num_reporte` ASC),
  INDEX `fk_cr_c003_descarga_equipo_cr_c001_equipo_idx` (`fk_crb001_num_equipo` ASC),
  CONSTRAINT `fk_cr_c003_descarga_equipo_cr_c006_reporte`
    FOREIGN KEY (`fk_crc006_num_reporte`)
    REFERENCES `SIACE`.`cr_c006_reporte` (`pk_num_reporte`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Registra la descarga por equipos ';


-- -----------------------------------------------------
-- Table `SIACE`.`cr_c004_descarga_sitio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cr_c004_descarga_sitio` (
  `pk_num_descarga_sitio` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id de descarga de sitios',
  `fk_crc003_num_descarga` INT(11) NOT NULL COMMENT 'Enlaza la descarga total con los sitios',
  `fk_crc005_num_sitio` INT(11) NOT NULL COMMENT 'Id del sitio web',
  PRIMARY KEY (`pk_num_descarga_sitio`),
  INDEX `fk_cr_c004_descarga_sitio_cr_c003_descarga_equipo` (`fk_crc003_num_descarga` ASC),
  INDEX `fk_cr_c004_descarga_sitio_cr_c005_sitio` (`fk_crc005_num_sitio` ASC),
  CONSTRAINT `fk_cr_c004_descarga_sitio_cr_c003_descarga_equipo`
    FOREIGN KEY (`fk_crc003_num_descarga`)
    REFERENCES `SIACE`.`cr_c003_descarga_equipo` (`pk_num_descarga`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Registra los sitios descargados por equipo';


-- -----------------------------------------------------
-- Table `SIACE`.`cr_c005_sitio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cr_c005_sitio` (
  `pk_num_sitio` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id autoincremental de los sitios',
  `ind_nombre_sitio` VARCHAR(45) CHARACTER SET 'ucs2' COLLATE 'ucs2_spanish2_ci' NULL DEFAULT NULL COMMENT 'Nombre del sitio',
  PRIMARY KEY (`pk_num_sitio`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Registra sitios web';


-- -----------------------------------------------------
-- Table `SIACE`.`cv_b001_registro_visita`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`cv_b001_registro_visita` (
  `pk_num_registro_visita` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a006_num_miscelaneo_detalle_motivo_visita` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_destino` INT(11) NOT NULL,
  `fk_a001_num_organismo` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fk_a003_num_persona` INT(11) NOT NULL,
  `fec_fecha_entrada` DATETIME NOT NULL,
  `fec_fecha_salida` DATETIME NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_registro_visita`),
  INDEX `fk_cv_b001_registro_visita_a001_organismo1_idx` (`fk_a001_num_organismo` ASC),
  INDEX `fk_cv_b001_registro_visita_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_cv_b001_registro_visita_a003_persona1_idx` (`fk_a003_num_persona` ASC),
  INDEX `fk_cv_b001_registro_visita_a006_misc_detalle_destino_idx` USING BTREE (`fk_a006_num_miscelaneo_detalle_destino` ASC),
  INDEX `fk_cv_b001_registro_visita_a006_miscelaneo_detalle_idx` USING BTREE (`fk_a006_num_miscelaneo_detalle_motivo_visita` ASC),
  CONSTRAINT `fk_cv_b001_registro_visita_a001_organismo1`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cv_b001_registro_visita_a003_persona1`
    FOREIGN KEY (`fk_a003_num_persona`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cv_b001_registro_visita_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 17
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`dn_b001_denuncia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`dn_b001_denuncia` (
  `pk_num_denuncia` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_solicitud` VARCHAR(9) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_correlativo` VARCHAR(12) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_num_denuncia` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_anio` YEAR NOT NULL,
  `fk_a023_num_centro_costo` INT(11) NOT NULL,
  `ind_propuesta` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_descripcion_denuncia` LONGTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_motivo_origen` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_estatus` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_duracion` INT(11) NULL DEFAULT NULL,
  `fec_inicio` DATE NULL DEFAULT NULL,
  `fec_termino_real` DATE NULL DEFAULT NULL,
  `fec_elaborado` DATETIME NOT NULL,
  `fec_revisado` DATE NULL DEFAULT NULL,
  `fec_aprobado` DATE NULL DEFAULT NULL,
  `fec_anulado` DATE NULL DEFAULT NULL,
  `fec_recepcion` DATE NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a039_num_ente` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_elabora` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_recepcion` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_revisa` INT(11) NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_aprueba` INT(11) NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_anula` INT(11) NULL DEFAULT NULL,
  `ind_especificacion_tipo_denuncia` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a006_pk_num_miscelaneo_detalle_tipo_denuncia` INT(11) NOT NULL,
  `ind_especificacion_origen_denuncia` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a006_pk_num_miscelaneo_detalle_origen` INT(11) NOT NULL,
  `fk_a006_pk_num_miscelaneo_detalle_tipo_actuacion` INT(11) NOT NULL,
  `fk_a039_num_ente_recursos` INT(11) NULL DEFAULT NULL,
  `fk_a013_num_sector` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_denuncia`),
  INDEX `fk_dn_b006_denuncia_a001_organismo1_idx` (`fk_a039_num_ente` ASC),
  INDEX `fk_dn_b006_denuncia_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_elabora` ASC),
  INDEX `fk_dn_b006_denuncia_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_revisa` ASC),
  INDEX `fk_dn_b006_denuncia_rh_b001_empleado3_idx` (`fk_rhb001_num_empleado_aprueba` ASC),
  INDEX `fk_dn_b006_denuncia_rh_b001_empleado4_idx` (`fk_rhb001_num_empleado_anula` ASC),
  INDEX `fk_dn_b006_denuncia_dn_b004_tipo_denuncia1_idx` (`fk_a006_pk_num_miscelaneo_detalle_tipo_denuncia` ASC),
  INDEX `fk_dn_b006_denuncia_dn_b003_origen1_idx` (`fk_a006_pk_num_miscelaneo_detalle_origen` ASC),
  INDEX `fk_dn_b006_denuncia_a013_sector1_idx` (`fk_a013_num_sector` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_rhb001_num_empleado_recepcion` (`fk_rhb001_num_empleado_recepcion` ASC),
  CONSTRAINT `dn_b001_denuncia_ibfk_1`
    FOREIGN KEY (`fk_rhb001_num_empleado_recepcion`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`pk_num_empleado`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
ROW_FORMAT = COMPACT;


-- -----------------------------------------------------
-- Table `SIACE`.`dn_b002_prorroga`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`dn_b002_prorroga` (
  `pk_num_prorroga_detalle` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_anio` YEAR NOT NULL,
  `ind_correlativo` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_secuencia` INT(4) NOT NULL,
  `num_prorroga` INT(4) NOT NULL,
  `ind_motivo` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_preparacion` DATETIME NOT NULL,
  `fec_registro` DATETIME NOT NULL,
  `fec_revision` DATETIME NOT NULL,
  `fec_aprobacion` DATETIME NOT NULL,
  `fec_anulado` DATETIME NULL DEFAULT NULL,
  `ind_estatus` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'PR' COMMENT 'PR:En Preparación, RV:Revisado, AP:Aprobado, AN:Anulado',
  `fk_dnb001_num_denuncia` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_elabora` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_revisa` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_aprueba` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_anula` INT(11) NOT NULL,
  `fk_dnd001_num_detalle_dc` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_prorroga_detalle`),
  INDEX `fk_dn_b005_prorroga_dn_b006_denuncia1_idx` (`fk_dnb001_num_denuncia` ASC),
  INDEX `fk_dn_b005_prorroga_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_elabora` ASC),
  INDEX `fk_dn_b005_prorroga_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_revisa` ASC),
  INDEX `fk_dn_b005_prorroga_rh_b001_empleado3_idx` (`fk_rhb001_num_empleado_aprueba` ASC),
  INDEX `fk_dn_b005_prorroga_rh_b001_empleado4_idx` (`fk_rhb001_num_empleado_anula` ASC),
  INDEX `fk_dn_b005_prorroga_dn_d001_actividades1_idx` (`fk_dnd001_num_detalle_dc` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`dn_c001_fase`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`dn_c001_fase` (
  `pk_num_fase` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_fase` VARCHAR(3) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipo_actuacion` INT(11) NOT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_fase`),
  UNIQUE INDEX `cod_fase` USING BTREE (`cod_fase` ASC, `fk_a006_num_miscelaneo_detalle_tipo_actuacion` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 13
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
ROW_FORMAT = COMPACT;


-- -----------------------------------------------------
-- Table `SIACE`.`dn_c002_actividad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`dn_c002_actividad` (
  `pk_num_actividad` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_actividad` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_tipo_actuacion` INT(11) NOT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_comentarios` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_duracion` INT(4) NOT NULL,
  `num_flag_auto_archivo` INT(1) NOT NULL DEFAULT '0',
  `num_flag_no_afecto_plan` INT(1) NOT NULL DEFAULT '0',
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_dnc001_num_fase` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_actividad`),
  INDEX `fk_dn_a003_actividad_dn_a002_fase1_idx` (`fk_dnc001_num_fase` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 14
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
ROW_FORMAT = COMPACT;


-- -----------------------------------------------------
-- Table `SIACE`.`dn_c003_tipo_resultado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`dn_c003_tipo_resultado` (
  `pk_num_tipo_resultado` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_resultado` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipo_actuacion` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_tipo_resultado`),
  UNIQUE INDEX `resultado_UNIQUE` (`ind_resultado` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
ROW_FORMAT = COMPACT;


-- -----------------------------------------------------
-- Table `SIACE`.`dn_d001_actividades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`dn_d001_actividades` (
  `pk_num_detalle_dc` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a006_num_miscelaneo_detalle_tipo_actuacion` INT(11) NOT NULL,
  `num_secuencia` INT(3) NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fec_inicio` DATE NOT NULL,
  `fec_inicio_real` DATE NOT NULL,
  `fec_termino` DATE NOT NULL,
  `fec_termino_real` DATE NOT NULL,
  `fec_cierre` DATE NOT NULL,
  `fec_termino_cierre` DATE NOT NULL,
  `num_dias_cierre` INT(4) NOT NULL,
  `num_prorroga` INT(4) NOT NULL,
  `num_duracion` INT(4) NOT NULL,
  `ind_observaciones` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_estatus` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'PR' COMMENT 'PR:En Preparación, RV:Revisado, EJ:En Ejecucion, AN:Anulado, CO:Completado',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_dnc002_num_actividad` INT(11) NOT NULL,
  `fk_dnb001_num_denuncia` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_detalle_dc`),
  INDEX `fk_dn_d001_actividades_dn_a003_actividad1_idx` (`fk_dnc002_num_actividad` ASC),
  INDEX `fk_dn_d001_actividades_dn_b006_denuncia1_idx` (`fk_dnb001_num_denuncia` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`dn_d002_prorroga_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`dn_d002_prorroga_detalle` (
  `pk_num_prorroga_detalle` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fec_inicio_real` DATE NOT NULL,
  `fec_termino_real` DATE NOT NULL,
  `fk_dnd001_num_detalle_dc` INT(11) NOT NULL,
  `fk_dnb002_num_prorroga_detalle` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_prorroga_detalle`),
  INDEX `fk_dn_c002_prorroga_detalle_dn_d001_actividades1_idx` (`fk_dnd001_num_detalle_dc` ASC),
  INDEX `fk_dn_c002_prorroga_detalle_dn_b005_prorroga1_idx` (`fk_dnb002_num_prorroga_detalle` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
ROW_FORMAT = COMPACT;


-- -----------------------------------------------------
-- Table `SIACE`.`dn_e001_denunciantes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`dn_e001_denunciantes` (
  `pk_num_actuacion_persona` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_dnb001_num_denuncia` INT(11) NOT NULL,
  `fk_a003_num_persona` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_actuacion_persona`),
  INDEX `fk_dn_d003_actuacion_persona_dn_b006_denuncia1_idx` (`fk_dnb001_num_denuncia` ASC),
  INDEX `fk_dn_d003_actuacion_persona_a003_persona1_idx` (`fk_a003_num_persona` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
ROW_FORMAT = COMPACT;


-- -----------------------------------------------------
-- Table `SIACE`.`dn_e002_prueba`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`dn_e002_prueba` (
  `pk_num_pruebas` INT(11) NOT NULL AUTO_INCREMENT,
  `num_cantidad` INT(11) NOT NULL,
  `ind_numero_documento` VARCHAR(25) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_descripcion` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_dnb001_num_denuncia` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_pruebas`),
  INDEX `dc_denuncia` (`fk_dnb001_num_denuncia` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
ROW_FORMAT = COMPACT;


-- -----------------------------------------------------
-- Table `SIACE`.`dn_e003_designacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`dn_e003_designacion` (
  `pk_num_designacion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_estatus` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_designacion` DATE NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_dnb001_num_denuncia` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_designacion`),
  INDEX `fk_dn_c003_designacion_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_dn_c003_designacion_dn_b006_denuncia1_idx` (`fk_dnb001_num_denuncia` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
ROW_FORMAT = COMPACT;


-- -----------------------------------------------------
-- Table `SIACE`.`dn_e004_valoracion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`dn_e004_valoracion` (
  `pk_num_valoracion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_antecedente_expediente` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_analisis_hechos_expediente` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_valoracion_expediente` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_conclusion_valoracion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_manifiesto` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_recomendaciones` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_valoracion` DATE NOT NULL,
  `fk_dnc004_num_tipo_resultado` INT(11) NOT NULL,
  `fk_a004_num_dependencia_control` INT(11) NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_valoracion`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
ROW_FORMAT = COMPACT;


-- -----------------------------------------------------
-- Table `SIACE`.`dn_e005_resultado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`dn_e005_resultado` (
  `pk_num_resultado` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_observacion` VARCHAR(400) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  ` fk_rhb001_num_empleado_elabora` INT(11) NOT NULL,
  `fec_elabora` DATETIME NOT NULL,
  `fk_dnb001_num_denuncia` INT(11) NOT NULL,
  `fk_dnc003_num_tipo_resultado` INT(11) NOT NULL,
  `fk_a004_num_dependencia` INT(11) NULL DEFAULT NULL,
  `ind_estatus` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'PR',
  `fk_rhb001_num_empleado_aprueba` INT(11) NOT NULL,
  `fec_aprobado` DATE NOT NULL,
  PRIMARY KEY (`pk_num_resultado`),
  INDEX `fk_dn_c001_resultado_dn_b006_denuncia1_idx` (`fk_dnb001_num_denuncia` ASC),
  INDEX `fk_dn_c001_resultado_dn_b001_tipo_resultado1_idx` (`fk_dnc003_num_tipo_resultado` ASC),
  INDEX `fk_dn_c001_resultado_dn_b002_asignacion_actuacion1_idx` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (` fk_rhb001_num_empleado_elabora` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
ROW_FORMAT = COMPACT;


-- -----------------------------------------------------
-- Table `SIACE`.`ev_c003_lugar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`ev_c003_lugar` (
  `pk_num_lugar_evento` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador del lugar',
  `ind_lugar_evento` VARCHAR(600) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Lugar donde se realiza el evento',
  `num_estatus` INT(11) NOT NULL COMMENT 'Estatus del lugar 1 para activo y 0 para inactivo',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Último usuario que realizó modificaciones al registro',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Fecha de la última modificación',
  PRIMARY KEY (`pk_num_lugar_evento`),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `ev_c003_lugar_ibfk_1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Registra el lugar donde se realiza un evento';


-- -----------------------------------------------------
-- Table `SIACE`.`ev_b001_evento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`ev_b001_evento` (
  `pk_num_evento` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador del evento',
  `ind_nombre_evento` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Nombre del evento',
  `ind_descripcion` VARCHAR(600) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Descripción del evento',
  `fec_registro` DATE NOT NULL COMMENT 'Fecha de registro del evento',
  `fec_inicio` DATE NOT NULL COMMENT 'Fecha de inicio del evento',
  `fec_fin` DATE NOT NULL COMMENT 'Fecha final del evento',
  `fec_horas_total` TIME NOT NULL COMMENT 'Cantidad de horas totales del evento',
  `fec_hora_entrada` TIME NOT NULL COMMENT 'Hora de entrada al evento',
  `fec_hora_salida` TIME NOT NULL COMMENT 'Hora de salida del evento',
  `ind_certificado` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Nombre del certificado',
  `num_flag_certificado_participante` INT(11) NOT NULL COMMENT 'Flag que indica si los participantes recibirán certificado; 1 para recibir y 0 en caso contrario',
  `num_flag_certificado_ponente` INT(11) NOT NULL COMMENT 'Flag que indica si los ponentes recibirán certificado; 1 para recibir y 0 en caso contrario',
  `fk_a006_num_tipo_evento` INT(11) NOT NULL COMMENT 'Indica el tipo de evento',
  `fk_evc003_num_lugar` INT(11) NOT NULL COMMENT 'Indica el lugar del evento',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Usuario que realizó la última modificación',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Última modificación realizada al registro',
  PRIMARY KEY (`pk_num_evento`),
  INDEX `fk_a006_num_tipo_evento` (`fk_a006_num_tipo_evento` ASC),
  INDEX `fk_evc003_num_lugar` (`fk_evc003_num_lugar` ASC),
  INDEX `	fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `ev_b001_evento_ibfk_2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ev_b001_evento_ibfk_3`
    FOREIGN KEY (`fk_evc003_num_lugar`)
    REFERENCES `SIACE`.`ev_c003_lugar` (`pk_num_lugar_evento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Registro del evento';


-- -----------------------------------------------------
-- Table `SIACE`.`ev_c001_persona_evento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`ev_c001_persona_evento` (
  `pk_num_persona_evento` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador de ponentes y participantes al evento',
  `fk_a003_num_persona` INT(11) NOT NULL COMMENT 'Asistente al evento',
  `num_flag_culmino_evento` INT(11) NULL DEFAULT NULL COMMENT 'Flag que indica si culminó el evento',
  `num_flag_recibio_certificado` INT(11) NOT NULL COMMENT 'Flag que indica si recibió certificado',
  `fk_evb001_num_evento` INT(11) NOT NULL COMMENT 'Id del evento ',
  `fk_a006_num_persona_capacitacion` INT(11) NOT NULL COMMENT 'Tipo de persona, puede ser ponente o participante',
  `fk_a006_num_tipo_instruccion` INT(11) NOT NULL COMMENT 'Tipo de instrucción en caso de ser ponente',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Último usuario que modificó el registro',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Última modificación',
  PRIMARY KEY (`pk_num_persona_evento`),
  INDEX `fk_a003_num_persona` (`fk_a003_num_persona` ASC),
  INDEX `fk_evb001_num_evento` (`fk_evb001_num_evento` ASC),
  INDEX `fk_a006_num_persona_capacitacion` (`fk_a006_num_persona_capacitacion` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_a006_num_tipo_instruccion` (`fk_a006_num_tipo_instruccion` ASC),
  CONSTRAINT `ev_c001_persona_evento_ibfk_2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ev_c001_persona_evento_ibfk_4`
    FOREIGN KEY (`fk_evb001_num_evento`)
    REFERENCES `SIACE`.`ev_b001_evento` (`pk_num_evento`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 81
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Registra los ponentes y participantes asistentes al evento';


-- -----------------------------------------------------
-- Table `SIACE`.`ev_c002_tema`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`ev_c002_tema` (
  `pk_num_tema_evento` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador del tema del evento',
  `ind_tema` VARCHAR(600) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Nombre del tema',
  `num_estatus` INT(11) NOT NULL COMMENT 'Estatus del tema 1 para activo y 0 para inactivo',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Último usuario que realizó modificaciones',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Última modificación realizada al registro',
  PRIMARY KEY (`pk_num_tema_evento`),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `ev_c002_tema_ibfk_1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Registra los temas de los eventos';


-- -----------------------------------------------------
-- Table `SIACE`.`ev_c004_tema_evento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`ev_c004_tema_evento` (
  `pk_num_tema_evento` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador del tema del evento',
  `fk_evb001_num_evento` INT(11) NOT NULL COMMENT 'Identificador del evento',
  `fk_evc002_num_tema` INT(11) NOT NULL COMMENT 'Tema del evento',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Usuario que realizó la última modificación',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Fecha de la última modificación',
  PRIMARY KEY (`pk_num_tema_evento`),
  INDEX `fk_evb001_num_evento` (`fk_evb001_num_evento` ASC),
  INDEX `fk_evc002_num_tema` (`fk_evc002_num_tema` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `ev_c004_tema_evento_ibfk_1`
    FOREIGN KEY (`fk_evb001_num_evento`)
    REFERENCES `SIACE`.`ev_b001_evento` (`pk_num_evento`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ev_c004_tema_evento_ibfk_2`
    FOREIGN KEY (`fk_evc002_num_tema`)
    REFERENCES `SIACE`.`ev_c002_tema` (`pk_num_tema_evento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ev_c004_tema_evento_ibfk_3`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 30
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Une los temas a un evento en particular';


-- -----------------------------------------------------
-- Table `SIACE`.`ev_c005_certificado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`ev_c005_certificado` (
  `pk_num_certificado` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador de certificado',
  `ind_imagen` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Nombre del certificado',
  `num_estatus` INT(11) NOT NULL COMMENT 'Estatus del certificado. 1 para activo y 0 para inactivo',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Usuario que realizó la última modificación',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Última modificación realizada al registro',
  PRIMARY KEY (`pk_num_certificado`),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `ev_c005_certificado_ibfk_1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Registra los fondos de certificados';


-- -----------------------------------------------------
-- Table `SIACE`.`ev_c006_persona_certificado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`ev_c006_persona_certificado` (
  `pk_num_persona_certificado` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id del registro',
  `num_certificado` INT(11) NOT NULL COMMENT 'Número de certificado',
  `fk_a003_num_persona` INT(11) NOT NULL COMMENT 'Persona a la cual pertenece el certificado',
  `fk_evb001_num_evento` INT(11) NOT NULL COMMENT 'Identificador del evento',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Usuario que realizó la última modificación',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Fecha de la última modificación',
  PRIMARY KEY (`pk_num_persona_certificado`),
  INDEX `fk_a003_num_persona` (`fk_a003_num_persona` ASC),
  INDEX `fk_evb001_num_evento` (`fk_evb001_num_evento` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `ev_c006_persona_certificado_ibfk_1`
    FOREIGN KEY (`fk_a003_num_persona`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ev_c006_persona_certificado_ibfk_2`
    FOREIGN KEY (`fk_evb001_num_evento`)
    REFERENCES `SIACE`.`ev_b001_evento` (`pk_num_evento`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ev_c006_persona_certificado_ibfk_3`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Asignación de certificados a participantes y ponentes';


-- -----------------------------------------------------
-- Table `SIACE`.`gc_c003_tipo_contrato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`gc_c003_tipo_contrato` (
  `pk_num_tipo_contrato` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a004_dependencia` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_tipo_contrato`),
  INDEX `fk_gc_c003_tipo_contrato_gc_c002_dependencia_contrato2_idx` (`fk_a004_dependencia` ASC),
  INDEX `fk_a004_dependencia` (`fk_a004_dependencia` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `gc_c003_tipo_contrato_ibfk_1`
    FOREIGN KEY (`fk_a004_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `gc_c003_tipo_contrato_ibfk_2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`gc_c004_aplicable_contrato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`gc_c004_aplicable_contrato` (
  `pk_num_aplicable_contrato` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_gcc003_num_tipo_contrato` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_aplicable_contrato`),
  INDEX `fk_gc_c004_aplicable_contrato_gc_c001_registro_contrato1_idx` (`fk_gcc003_num_tipo_contrato` ASC),
  INDEX `fk_gcb003_num_tipo_contrato` (`fk_gcc003_num_tipo_contrato` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `gc_c004_aplicable_contrato_ibfk_1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `gc_c004_aplicable_contrato_ibfk_2`
    FOREIGN KEY (`fk_gcc003_num_tipo_contrato`)
    REFERENCES `SIACE`.`gc_c003_tipo_contrato` (`pk_num_tipo_contrato`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 13
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_b016_tipo_documento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_b016_tipo_documento` (
  `pk_num_tipo_documento` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_tipo_documento` VARCHAR(3) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_documento_fiscal` INT(1) NOT NULL DEFAULT '0',
  `num_flag_transaccion_sistema` INT(1) NOT NULL DEFAULT '0',
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_tipo_documento`),
  UNIQUE INDEX `cod_tipo_documento_UNIQUE` (`cod_tipo_documento` ASC),
  UNIQUE INDEX `descripcion_UNIQUE` (`ind_descripcion` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario123`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_b022_proveedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_b022_proveedor` (
  `pk_num_proveedor` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_tipo_pago` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'masttipopago->CodTipoPago',
  `cod_forma_pago` VARCHAR(3) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'mastformapago->CodFormaPago',
  `cod_tipo_servicio` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'masttiposervicio->CodTipoServicio',
  `num_dias_pago` INT(4) NULL DEFAULT NULL,
  `ind_registro_publico` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_licencia_municipal` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_constitucion` DATE NOT NULL,
  `ind_snc` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_num_inscripcion_snc` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_emision_snc` DATE NULL DEFAULT NULL,
  `fec_validacion_snc` DATE NULL DEFAULT NULL,
  `ind_nacionalidad` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'N:NACIONAL; E:EXTRANJERO;',
  `ind_condicion_rcn` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `ind_calificacion` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_nivel` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_capacidad_financiera` DECIMAL(18,6) NULL DEFAULT NULL,
  `fk_lgb016_num_tipo_documento` INT(11) NOT NULL,
  `fk_a003_num_persona_representante` INT(11) NOT NULL,
  `fk_a003_num_persona_vendedor` INT(11) NOT NULL,
  `fk_a003_num_persona_proveedor` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_proveedor`),
  INDEX `fk_lg_b022_proveedor_lg_b016_tipo_documento1_idx` (`fk_lgb016_num_tipo_documento` ASC),
  INDEX `fk_lg_b022_proveedor_a003_persona2_idx` (`fk_a003_num_persona_representante` ASC),
  INDEX `fk_lg_b022_proveedor_a003_persona3_idx` (`fk_a003_num_persona_vendedor` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_a003_num_persona_proveedor_idx` (`fk_a003_num_persona_proveedor` ASC),
  CONSTRAINT `fk_a003_num_persona_proveedor`
    FOREIGN KEY (`fk_a003_num_persona_proveedor`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a018_num_seguridad_usuario187`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b022_proveedor_a003_persona2`
    FOREIGN KEY (`fk_a003_num_persona_representante`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b022_proveedor_a003_persona3`
    FOREIGN KEY (`fk_a003_num_persona_vendedor`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b022_proveedor_lg_b016_tipo_documento1`
    FOREIGN KEY (`fk_lgb016_num_tipo_documento`)
    REFERENCES `SIACE`.`lg_b016_tipo_documento` (`pk_num_tipo_documento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 16
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`gc_b001_registro_contrato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`gc_b001_registro_contrato` (
  `pk_num_registro_contrato` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_correlativo` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_objeto_contrato` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_monto_contrato` DECIMAL(18,6) NOT NULL,
  `fk_gcc004_num_aplicable_contrato` INT(11) NOT NULL,
  `fk_a006_num_estado_contrato` INT(11) NOT NULL DEFAULT '1',
  `fk_gcc006_num_modelo_contrato` INT(11) NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fk_a004_dependencia` INT(11) NOT NULL,
  `fk_gcc003_num_tipo_contrato` INT(11) NOT NULL,
  `fk_num_forma_pago` INT(11) NOT NULL,
  `fk_lgb022_proveedor` INT(11) NOT NULL,
  `fk_a003_representante` INT(11) NOT NULL,
  `ind_cuerpo` VARCHAR(20000) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT 'En la ',
  `ind_correlativo_contrato` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_anio` YEAR NOT NULL,
  PRIMARY KEY (`pk_num_registro_contrato`),
  INDEX `fk_gc_c001_registro_contrato_gc_c004_aplicable_contrato2_idx` (`fk_gcc004_num_aplicable_contrato` ASC),
  INDEX `fk_fk_gc_c001_registro_contrato_gc_c006_modelo_contrato1_idx` (`fk_gcc006_num_modelo_contrato` ASC),
  INDEX `fk_a004_dependencia` (`fk_a004_dependencia` ASC),
  INDEX `fk_gcc004_num_aplicable_contrato` (`fk_gcc004_num_aplicable_contrato` ASC),
  INDEX `fk_gcc006_num_modelo_contrato` (`fk_gcc006_num_modelo_contrato` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_a004_dependencia_2` (`fk_a004_dependencia` ASC),
  INDEX `fk_gcc003_num_tipo_contrato` (`fk_gcc003_num_tipo_contrato` ASC),
  INDEX `fk_num_forma_pago` (`fk_num_forma_pago` ASC),
  INDEX `fk_lgb022_proveedor` (`fk_lgb022_proveedor` ASC),
  INDEX `fk_a003_representante` (`fk_a003_representante` ASC),
  INDEX `fk_a006_num_estado_contrato` (`fk_a006_num_estado_contrato` ASC),
  CONSTRAINT `gc_b001_registro_contrato_ibfk_1`
    FOREIGN KEY (`fk_gcc004_num_aplicable_contrato`)
    REFERENCES `SIACE`.`gc_c004_aplicable_contrato` (`pk_num_aplicable_contrato`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `gc_b001_registro_contrato_ibfk_3`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`fk_a018_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `gc_b001_registro_contrato_ibfk_4`
    FOREIGN KEY (`fk_a004_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `gc_b001_registro_contrato_ibfk_7`
    FOREIGN KEY (`fk_lgb022_proveedor`)
    REFERENCES `SIACE`.`lg_b022_proveedor` (`pk_num_proveedor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `gc_b001_registro_contrato_ibfk_8`
    FOREIGN KEY (`fk_a003_representante`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`gc_b002_registro_contrato_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`gc_b002_registro_contrato_detalle` (
  ` 	pk_num_registro_contrato_detalle` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_gcb001_num_registro_contrato` INT(11) NOT NULL,
  `fec_desde` DATETIME NOT NULL,
  `fec_hasta` DATETIME NOT NULL,
  `fec_entrada` DATETIME NOT NULL,
  `ind_obligaciones_contratante` TEXT NULL DEFAULT NULL,
  `ind_obligaciones_contratado` TEXT NULL DEFAULT NULL,
  `ind_observacion` TEXT NULL DEFAULT NULL,
  `ind_lugar_pago` TEXT NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (` 	pk_num_registro_contrato_detalle`),
  INDEX `fk_gcb001_num_registro_contrato` (`fk_gcb001_num_registro_contrato` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `gc_b002_registro_contrato_detalle_ibfk_1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `gc_b002_registro_contrato_detalle_ibfk_2`
    FOREIGN KEY (`fk_gcb001_num_registro_contrato`)
    REFERENCES `SIACE`.`gc_b001_registro_contrato` (`pk_num_registro_contrato`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `SIACE`.`gc_b003_predeterminado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`gc_b003_predeterminado` (
  `pk_num_predeterminado` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ind_encabezado` VARCHAR(1500) NOT NULL,
  `fk_c003_tipo_contrato` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_predeterminado`),
  INDEX `fk_c003_tipo_contrato` (`fk_c003_tipo_contrato` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `gc_b003_predeterminado_ibfk_1`
    FOREIGN KEY (`fk_c003_tipo_contrato`)
    REFERENCES `SIACE`.`gc_c003_tipo_contrato` (`pk_num_tipo_contrato`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `gc_b003_predeterminado_ibfk_2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `SIACE`.`gc_b004_partidas_contrato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`gc_b004_partidas_contrato` (
  `pk_num_partidas` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_partida` VARCHAR(20) NOT NULL,
  `num_monto` DECIMAL(16,8) NOT NULL,
  `fk_gcb001_registro_contacto` INT(11) NOT NULL,
  `fec_anio` YEAR NOT NULL,
  PRIMARY KEY (`pk_num_partidas`),
  INDEX `fk_gcb001_registro_contacto` (`fk_gcb001_registro_contacto` ASC),
  CONSTRAINT `gc_b004_partidas_contrato_ibfk_1`
    FOREIGN KEY (`fk_gcb001_registro_contacto`)
    REFERENCES `SIACE`.`gc_b001_registro_contrato` (`pk_num_registro_contrato`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `SIACE`.`gc_c001_relacion_obligacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`gc_c001_relacion_obligacion` (
  `pk_num_relacion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_gcb001_registro_contrato` INT(11) NOT NULL,
  `fk_cpd001_obligacion` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_relacion`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `SIACE`.`gc_c005_bitacora`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`gc_c005_bitacora` (
  `pk_num_bitacora` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a006_miscelaneo` INT(11) NULL DEFAULT NULL,
  `fk_gcb001_num_registro_contrato` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_bitacora`),
  INDEX `fk_gc_c001_registro_contrato1_idx` (`fk_gcb001_num_registro_contrato` ASC),
  INDEX `fk_a006_miscelaneo` (`fk_a006_miscelaneo` ASC),
  INDEX `fk_gcb001_num_registro_contrato` (`fk_gcb001_num_registro_contrato` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `gc_c005_bitacora_ibfk_2`
    FOREIGN KEY (`fk_gcb001_num_registro_contrato`)
    REFERENCES `SIACE`.`gc_b001_registro_contrato` (`pk_num_registro_contrato`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `gc_c005_bitacora_ibfk_3`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`gc_c006_modelo_contrato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`gc_c006_modelo_contrato` (
  `pk_cod_modelo_contrato` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_cod_modelo_contrato`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`gc_c007_fecha_contrato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`gc_c007_fecha_contrato` (
  `pk_num_fecha` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_desde` DATE NULL DEFAULT NULL,
  `fec_hasta` DATE NULL DEFAULT NULL,
  `fec_firma` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_gcb001_num_registro_contrato` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_fecha`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`in_c001_noticias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`in_c001_noticias` (
  `pk_num_noticia` INT(10) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria de la tabla',
  `ind_titulo` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Título de la noticia',
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Subtitulo descriptivo de la Noticia (opcional)',
  `txt_cuerpo` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Cuerpo de la noticia',
  `fk_a001_num_organismo` INT(11) NOT NULL COMMENT 'Numero del organismo que es la fuente de la Noticia',
  `fec_registro` DATE NOT NULL COMMENT 'Fecha de registro de la Noticia',
  `hora_regisro` TIME NOT NULL COMMENT 'Hora de registro de la Noticia',
  `ind_ruta_img` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Ruta de la imagen de la Noticia',
  `ind_status` CHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Estado de publicada (m) o no publicada (n) la Noticia',
  `fec_ultima_modificacion` DATETIME NULL DEFAULT NULL COMMENT 'Fecha de la última modificación del registro',
  PRIMARY KEY (`pk_num_noticia`),
  INDEX `fk_a001_num_organismo` (`fk_a001_num_organismo` ASC),
  CONSTRAINT `in_c001_noticias_ibfk_1`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`in_c002_eventos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`in_c002_eventos` (
  `pk_num_evento` INT(10) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria de la tabla',
  `fec_registro` DATE NOT NULL COMMENT 'Fecha de registro del evento',
  `hora_inicio` TIME NOT NULL COMMENT 'Hora que inicia el evento',
  `hora_final` TIME NOT NULL COMMENT 'Hora en que finaliza el evento',
  `horario_inicio` CHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Post merídiem (pm) o Ante merídiem (am) de la hora de inicio del evento',
  `horario_final` CHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Post merídiem (pm) o Ante merídiem (am) de la hora de culminación del evento',
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Descripción del evento',
  `ind_anual` CHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Estado que indica si el evento es anual (Y) o no',
  `ind_lugar` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Lugar del evento',
  `ind_estado` INT(10) NOT NULL COMMENT 'Entero correspondiente al estado del lugar del evento (opcional)',
  `ind_municipio` INT(10) NOT NULL COMMENT 'Entero correspondiente al Municipio del lugar del evento (opcional)',
  `ind_parroquia` INT(10) NOT NULL COMMENT 'Entero correspondiente a la Parroquia del lugar del evento (opcional)',
  `ind_ruta_img` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL COMMENT 'Ruta de la imagen del evento',
  `fec_ultima_modificacion` DATETIME NULL DEFAULT NULL COMMENT 'Fecha de la última modificación del registro',
  PRIMARY KEY (`pk_num_evento`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`in_c003_chat`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`in_c003_chat` (
  `pk_num_chat` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria de la tabla',
  `ind_mensaje` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Descripción del mensaje',
  `fec_registro` DATETIME NOT NULL COMMENT 'Fecha y hora del mensaje',
  `fk_b001_num_empleado` INT(11) NOT NULL COMMENT 'Entero correspondiente al empleado autor del mensaje',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Fecha de la última modificación del registro',
  PRIMARY KEY (`pk_num_chat`),
  INDEX `fk_b001_num_empleado` (`fk_b001_num_empleado` ASC),
  CONSTRAINT `in_c003_chat_ibfk_1`
    FOREIGN KEY (`fk_b001_num_empleado`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`pk_num_empleado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`in_c004_normativa_legal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`in_c004_normativa_legal` (
  `pk_num_normativa_legal` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria de la tabla',
  `ind_tipo_normativa` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Tipo de Normativa Legal',
  `ind_gaceta_circular` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Número de la gaceta del circular',
  `fec_gaceta_circular` DATE NULL DEFAULT NULL COMMENT 'Fecha de la gaceta del circular',
  `ind_descripcion` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Breve descripción de la Normativa Legal',
  `ind_tipo_ley_circular` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Tipo de la ley o el circular (Nacional o no)',
  `fk_a004_num_dependencia` INT(11) NULL DEFAULT NULL COMMENT 'Entero correspondiente a la dependencia que emite la normativa',
  `ind_resolucion` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Número de resolución de la Normativa',
  `fec_resolucion` DATE NULL DEFAULT NULL COMMENT 'Fecha de la resolución de la Normativa',
  `ind_ruta_archivo` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Ruta del Archivo en PDF correspondiente a la Normativa',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Entero correspondiente al número de seguridad del funcionario que modifica el registro',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Fecha de la última modificación del registro',
  PRIMARY KEY (`pk_num_normativa_legal`),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `in_c004_normativa_legal_ibfk_1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`in_c005_interpretacion_ley`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`in_c005_interpretacion_ley` (
  `pk_num_interpretacion` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria de la tabla',
  `fk_inc004_num_normativa_legal` INT(11) NOT NULL COMMENT 'Entero correspondiente a la Ley a la cual pertenece la Interpretación de Ley',
  `ind_descripcion` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Breve descripción de la Interpretación de la Ley',
  `ind_ruta_archivo` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Ruta del archivo PDF correspondiente a la Interpretación de Ley',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Entero correspondiente al funcionario que realizó la última modificación',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Fecha de la última modificación del registro',
  PRIMARY KEY (`pk_num_interpretacion`),
  INDEX `FORANEA` (`fk_inc004_num_normativa_legal` ASC),
  CONSTRAINT `in_c005_interpretacion_ley_ibfk_1`
    FOREIGN KEY (`fk_inc004_num_normativa_legal`)
    REFERENCES `SIACE`.`in_c004_normativa_legal` (`pk_num_normativa_legal`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`in_c006_carpetas_documentos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`in_c006_carpetas_documentos` (
  `pk_num_carpetas_documentos` INT(10) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria de la tabla',
  `fec_creacion` DATE NOT NULL COMMENT 'Fecha de la creación de la carpeta de documentos',
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Nombre de la carpeta',
  `fk_a004_dependencia` INT(11) NOT NULL COMMENT 'Entero correspondiente a la Dependencia a la cual pertenece la carpeta',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Número de seguridad correspondiente al funcionario que realizó la última modificación',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Fecha de la última modificación del registro',
  PRIMARY KEY (`pk_num_carpetas_documentos`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`in_c007_documentos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`in_c007_documentos` (
  `pk_num_documentos` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria de la tabla',
  `fec_creacion` DATE NOT NULL COMMENT 'Fecha de creación del documento',
  `ind_peso` VARCHAR(11) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Peso en Kb/Mb del documento',
  `ind_formato` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Tipo de formato del documento',
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Nombre del documento',
  `fk_c006_carpetas_documentos` INT(11) NOT NULL COMMENT 'Entero correspondiente a la carpeta a la cual pertenece los documentos',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Número de seguridad del usuario que realizó la última modificación',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Fecha de la última modificación del registro',
  PRIMARY KEY (`pk_num_documentos`),
  INDEX `fk_c006_carpetas_documentos` (`fk_c006_carpetas_documentos` ASC),
  CONSTRAINT `in_c007_documentos_ibfk_1`
    FOREIGN KEY (`fk_c006_carpetas_documentos`)
    REFERENCES `SIACE`.`in_c006_carpetas_documentos` (`pk_num_carpetas_documentos`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_b001_requerimiento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_b001_requerimiento` (
  `pk_num_requerimiento` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_requerimiento` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a004_num_dependencia` INT(11) NOT NULL COMMENT 'mastdependencias->CodDependencia',
  `fk_a023_num_centro_costo` INT(11) NOT NULL COMMENT 'ac_mastcentrocosto->CodCentroCosto',
  `fk_a006_num_miscelaneos_almacen` INT(11) NOT NULL COMMENT 'lg_almacenmast->CodAlmacen',
  `fk_a006_num_miscelaneos_prioridad` INT(11) NOT NULL COMMENT 'N:NORMAL; U:URGENTE; M:MUY URGENTE',
  `fk_rhb001_num_empleado_preparado_por` INT(11) NOT NULL,
  `fec_preparacion` DATETIME NOT NULL,
  `fk_rhb001_num_empleado_revisado_por` INT(11) NULL DEFAULT NULL COMMENT 'mastpersonas-CodPersona',
  `fec_revision` DATETIME NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_conformado_por` INT(11) NULL DEFAULT NULL,
  `fec_conformacion` DATETIME NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_aprobado_por` INT(11) NULL DEFAULT NULL COMMENT 'mastpersonas-CodPersona',
  `fec_aprobacion` DATETIME NULL DEFAULT NULL,
  `fec_requerida` DATE NOT NULL,
  `ind_comentarios` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_razon_rechazo` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_caja_chica` INT(1) NOT NULL DEFAULT '0',
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'PR' COMMENT 'PR:En Preparacion; RV:Revisado; CN:Conformado; AP:Aprobado; AN:Anulado; RE:Rechazado; CE:Cerrado; CO:Completado;',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `ind_verificacion_caja_chica` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT '0',
  `fec_verificacion_caja_chica` DATE NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_verif_caja_chica` INT(11) NULL DEFAULT NULL,
  `ind_verificacion_presupuestaria` VARCHAR(11) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT '0',
  `fec_verificacion_presupuestaria` DATE NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_verif_presupuestaria` INT(11) NULL DEFAULT NULL,
  `num_flag_aprobacion_despacho` INT(1) NOT NULL DEFAULT '0',
  `fec_aprobacion_despacho` DATETIME NULL DEFAULT NULL,
  `fec_anio` YEAR NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_lgb022_num_proveedor_sugerido` INT(11) NULL DEFAULT NULL,
  `fk_lgb017_num_clasificacion` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_aprueba_despacho` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_requerimiento`),
  INDEX `fk_lg_b001_requerimiento_lg_b022_proveedor1_idx` (`fk_lgb022_num_proveedor_sugerido` ASC),
  INDEX `fk_lg_b001_requerimiento_lg_b017_clasificacion1_idx` (`fk_lgb017_num_clasificacion` ASC),
  INDEX `fk_lg_b001_requerimiento_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_aprueba_despacho` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_lg_b001_requerimiento_a004_dependencia1_idx` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_lg_b001_requerimiento_a023_centro_costo1_idx` (`fk_a023_num_centro_costo` ASC),
  INDEX `fk_lg_b001_requerimiento_a006_miscelaneos_detalle1_idx` (`fk_a006_num_miscelaneos_almacen` ASC),
  INDEX `fk_lg_b001_requerimiento_a006_miscelaneos_detalle2_idx` (`fk_a006_num_miscelaneos_prioridad` ASC),
  INDEX `fk_lg_b001_requerimiento_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_preparado_por` ASC),
  INDEX `fk_lg_b001_requerimiento_rh_b001_empleado3_idx` (`fk_rhb001_num_empleado_revisado_por` ASC),
  INDEX `fk_lg_b001_requerimiento_rh_b001_empleado4_idx` (`fk_rhb001_num_empleado_conformado_por` ASC),
  INDEX `fk_lg_b001_requerimiento_rh_b001_empleado5_idx` (`fk_rhb001_num_empleado_aprobado_por` ASC),
  INDEX `fk_lg_b001_requerimiento_rh_b001_empleado6_idx` (`fk_rhb001_num_empleado_verif_caja_chica` ASC),
  INDEX `fk_lg_b001_requerimiento_rh_b001_empleado7_idx` (`fk_rhb001_num_empleado_verif_presupuestaria` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario107`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b001_requerimiento_a004_dependencia1`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b001_requerimiento_a023_centro_costo1`
    FOREIGN KEY (`fk_a023_num_centro_costo`)
    REFERENCES `SIACE`.`a023_centro_costo` (`pk_num_centro_costo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b001_requerimiento_lg_b017_clasificacion1`
    FOREIGN KEY (`fk_lgb017_num_clasificacion`)
    REFERENCES `SIACE`.`lg_b017_clasificacion` (`pk_num_clasificacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b001_requerimiento_lg_b022_proveedor1`
    FOREIGN KEY (`fk_lgb022_num_proveedor_sugerido`)
    REFERENCES `SIACE`.`lg_b022_proveedor` (`pk_num_proveedor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 62
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_b002_item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_b002_item` (
  `pk_num_item` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_lotes` INT(1) NOT NULL DEFAULT '0',
  `num_flag_kit` INT(1) NOT NULL DEFAULT '0',
  `num_flag_impuesto_venta` INT(1) NOT NULL DEFAULT '0',
  `num_flag_auto` INT(1) NOT NULL DEFAULT '0',
  `num_flag_disponible` INT(1) NOT NULL DEFAULT '0',
  `num_flag_verificado_presupuesto` INT(1) NOT NULL DEFAULT '0',
  `fec_verificado_presupuesto` DATE NULL DEFAULT NULL,
  `ind_imagen` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_stock_minimo` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_stock_maximo` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_punto_reorden` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `fk_cbb004_num_plan_cuenta_inventario_oncop` INT(11) NULL DEFAULT NULL COMMENT 'ac_mastplancuenta->CodCuenta',
  `fk_cbb004_num_plan_cuenta_gasto_oncop` INT(11) NULL DEFAULT NULL COMMENT 'ac_mastplancuenta->CodCuenta',
  `fk_cbb004_num_plan_cuenta_inventario_pub_veinte` INT(11) NULL DEFAULT NULL COMMENT 'ac_mastplancuenta20->CodCuenta',
  `fk_cbb004_num_plan_cuenta_gasto_pub_veinte` INT(11) NULL DEFAULT NULL COMMENT 'ac_mastplancuenta20->CodCuenta',
  `fk_prb002_num_partida_presupuestaria` INT(11) NULL DEFAULT NULL COMMENT 'pv_partida->cod_partida',
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_lgb007_num_clase_subfamilia` INT(11) NOT NULL,
  `fk_lgc002_num_unidad_conversion` INT(11) NOT NULL,
  `fk_lgc007_num_item_stock` INT(11) NOT NULL,
  `fk_lgb004_num_unidad_compra` INT(11) NOT NULL,
  `fk_lgb004_num_unidad_despacho` INT(11) NOT NULL,
  `fk_lgb014_num_almacen` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_verifica_presupuesto` INT(11) NULL DEFAULT NULL,
  `fk_a006_num_miscelaneo_tipo_item` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_item`),
  INDEX `fk_lg_b002_item_lg_c002_unidades_conversion1_idx` (`fk_lgc002_num_unidad_conversion` ASC),
  INDEX `fk_lg_b002_item_lg_c007_item_stock1_idx` (`fk_lgc007_num_item_stock` ASC),
  INDEX `fk_lg_b002_item_lg_b004_unidades1_idx` (`fk_lgb004_num_unidad_compra` ASC),
  INDEX `fk_lg_b002_item_lg_b004_unidades2_idx` (`fk_lgb004_num_unidad_despacho` ASC),
  INDEX `fk_lg_b002_item_lg_b014_almacen1_idx` (`fk_lgb014_num_almacen` ASC),
  INDEX `fk_lg_b002_item_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_verifica_presupuesto` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_lg_b002_item_cb_b004_plan_cuenta1_idx` (`fk_cbb004_num_plan_cuenta_inventario_oncop` ASC),
  INDEX `fk_lg_b002_item_cb_b004_plan_cuenta2_idx` (`fk_cbb004_num_plan_cuenta_gasto_oncop` ASC),
  INDEX `fk_lg_b002_item_cb_b004_plan_cuenta3_idx` (`fk_cbb004_num_plan_cuenta_inventario_pub_veinte` ASC),
  INDEX `fk_lg_b002_item_cb_b004_plan_cuenta4_idx` (`fk_cbb004_num_plan_cuenta_gasto_pub_veinte` ASC),
  INDEX `fk_lg_b002_item_pr_b002_partida_presupuestaria1_idx` (`fk_prb002_num_partida_presupuestaria` ASC),
  INDEX `fk_lg_b002_item_lg_b007_clase_subfamilia1_idx` (`fk_lgb007_num_clase_subfamilia` ASC),
  INDEX `fk_lg_b002_item_fk_a006_num_miscelaneo_tipo_item1_idx` (`fk_a006_num_miscelaneo_tipo_item` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 364
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_b004_unidades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_b004_unidades` (
  `pk_num_unidad` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a006_num_miscelaneos_tipo_medida` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pk_num_unidad`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_lg_b004_unidades_a006_miscelaneos_detalle1_idx` (`fk_a006_num_miscelaneos_tipo_medida` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario108`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 52
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_b003_commodity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_b003_commodity` (
  `pk_num_commodity` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_prb002_num_partida_presupuestaria` INT(11) NULL DEFAULT NULL COMMENT 'pv_partida->cod_partida',
  `fk_cbb004_num_plan_cuenta` INT(11) NULL DEFAULT NULL COMMENT 'ac_mastplancuenta->CodCuenta',
  `fk_cbb004_num_plan_cuenta_pub_veinte` INT(11) NULL DEFAULT NULL COMMENT 'ac_mastplancuenta20->CodCuenta',
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `num_flag_vericado_presupuesto` INT(1) NOT NULL DEFAULT '0',
  `fec_verificado_presupuesto` DATE NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_rhb001_num_empleado_verifica_presupuesto` INT(11) NULL DEFAULT NULL,
  `fk_lgb004_num_unidad` INT(11) NOT NULL,
  `fk_lgb014_num_almacen` INT(11) NOT NULL,
  `fk_lgb017_num_clasificacion` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_commodity`),
  INDEX `fk_lg_b003_commodity_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_verifica_presupuesto` ASC),
  INDEX `fk_lg_b003_commodity_lg_b004_unidades1_idx` (`fk_lgb004_num_unidad` ASC),
  INDEX `fk_lg_b003_commodity_lg_b014_almacen1_idx` (`fk_lgb014_num_almacen` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_lgb017_num_clasificacion` (`fk_lgb017_num_clasificacion` ASC),
  INDEX `fk_lg_b003_commodity_cb_b004_plan_cuenta1` (`fk_cbb004_num_plan_cuenta_pub_veinte` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario113`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b003_commodity_cb_b004_plan_cuenta1`
    FOREIGN KEY (`fk_cbb004_num_plan_cuenta_pub_veinte`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b003_commodity_lg_b004_unidades1`
    FOREIGN KEY (`fk_lgb004_num_unidad`)
    REFERENCES `SIACE`.`lg_b004_unidades` (`pk_num_unidad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b003_commodity_lg_b014_almacen1`
    FOREIGN KEY (`fk_lgb014_num_almacen`)
    REFERENCES `SIACE`.`lg_b014_almacen` (`pk_num_almacen`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `lg_b003_commodity_ibfk_1`
    FOREIGN KEY (`fk_lgb017_num_clasificacion`)
    REFERENCES `SIACE`.`lg_b017_clasificacion` (`pk_num_clasificacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2280
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_b006_clase_familia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_b006_clase_familia` (
  `pk_num_clase_familia` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_cb_b004_cuenta_inventario` INT(11) NULL DEFAULT NULL,
  `fk_cb_b004_cuenta_gasto` INT(11) NULL DEFAULT NULL,
  `fk_pr_b002_partida_presupuestaria` INT(11) NULL DEFAULT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a006_num_miscelaneo_clase_linea` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_clase_familia`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_lg_b006_clase_familia_pr_b002_partida_presupuestaria1_idx` (`fk_pr_b002_partida_presupuestaria` ASC),
  INDEX `fk_lg_b006_clase_familia_cb_b004_plan_cuenta1_idx` (`fk_cb_b004_cuenta_inventario` ASC),
  INDEX `fk_lg_b006_clase_familia_cb_b004_plan_cuenta2_idx` (`fk_cb_b004_cuenta_gasto` ASC),
  INDEX `fk_lg_b006_clase_familia_a006_num_miscelaneo_clase_linea1_idx` (`fk_a006_num_miscelaneo_clase_linea` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario115`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b006_clase_familia_cb_b004_plan_cuenta1`
    FOREIGN KEY (`fk_cb_b004_cuenta_inventario`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b006_clase_familia_cb_b004_plan_cuenta2`
    FOREIGN KEY (`fk_cb_b004_cuenta_gasto`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b006_clase_familia_pr_b002_partida_presupuestaria`
    FOREIGN KEY (`fk_pr_b002_partida_presupuestaria`)
    REFERENCES `SIACE`.`pr_b002_partida_presupuestaria` (`pk_num_partida_presupuestaria`))
ENGINE = InnoDB
AUTO_INCREMENT = 49
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_b007_clase_subfamilia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_b007_clase_subfamilia` (
  `pk_num_subfamilia` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_lgb006_num_clase_familia` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_subfamilia`),
  INDEX `fk_lg_clase_subfamilia_lg_clase_familia1_idx` (`fk_lgb006_num_clase_familia` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario116`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_clase_subfamilia_lg_clase_familia1`
    FOREIGN KEY (`fk_lgb006_num_clase_familia`)
    REFERENCES `SIACE`.`lg_b006_clase_familia` (`pk_num_clase_familia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 84
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_b009_acta_inicio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_b009_acta_inicio` (
  `pk_num_acta_inicio` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rh_b001_empleado_asistente_a` INT(11) NOT NULL,
  `fk_rh_b001_empleado_asistente_b` INT(11) NOT NULL,
  `fk_rh_b001_empleado_asistente_c` INT(11) NULL DEFAULT NULL,
  `fec_registro` DATE NOT NULL,
  `num_procedimiento` INT(20) NOT NULL,
  `fk_rh_b001_empleado_anulado_termino` INT(11) NULL DEFAULT NULL,
  `fec_anulado_termino` DATE NULL DEFAULT NULL,
  `ind_motivo_anulado_termino` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_reunion` DATE NOT NULL,
  `fec_hora_reunion` TIME NOT NULL,
  `num_presupuesto_base` DECIMAL(18,6) NOT NULL,
  `fec_inicio` DATE NOT NULL,
  `fec_fin` DATE NOT NULL,
  `fk_a006_num_miscelaneos_modalidad` INT(11) NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'PR' COMMENT 'PR: PREPARACION, AD: ADJUDICADO, DS: DESIERTO, RV: REVISADO, AP: APROBADO',
  `num_anio` YEAR NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_lgb001_num_requerimiento` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_acta_inicio`),
  INDEX `fk_lg_b009_acta_inicio_lg_b001_requerimiento1_idx` (`fk_lgb001_num_requerimiento` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_lg_b009_acta_inicio_rh_b001_empleado1_idx` (`fk_rh_b001_empleado_asistente_a` ASC),
  INDEX `fk_lg_b009_acta_inicio_rh_b001_empleado2_idx` (`fk_rh_b001_empleado_asistente_b` ASC),
  INDEX `fk_lg_b009_acta_inicio_rh_b001_empleado3_idx` (`fk_rh_b001_empleado_asistente_c` ASC),
  INDEX `fk_lg_b009_acta_inicio_rh_b001_empleado4_idx` (`fk_rh_b001_empleado_anulado_termino` ASC),
  INDEX `fk_lg_b009_acta_inicio_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneos_modalidad` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario117`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b009_acta_inicio_lg_b001_requerimiento1`
    FOREIGN KEY (`fk_lgb001_num_requerimiento`)
    REFERENCES `SIACE`.`lg_b001_requerimiento` (`pk_num_requerimiento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 44
AVG_ROW_LENGTH = 44
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_b010_invitacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_b010_invitacion` (
  `pk_num_invitacion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_num_invitacion` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_invitacion` DATE NOT NULL,
  `fec_apertura` DATE NOT NULL,
  `fec_entrega` DATE NOT NULL,
  `num_validez_oferta` INT(10) NOT NULL DEFAULT '0',
  `num_dias_entrega` INT(10) NOT NULL,
  `fec_limite` DATE NOT NULL,
  `ind_condiciones` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_observaciones` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fk_lgb009_num_acta_inicio` INT(11) NOT NULL,
  `fk_lgb022_num_proveedor` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_invitacion`),
  INDEX `fk_lg_b010_invitacion_lg_b022_proveedor1_idx` (`fk_lgb022_num_proveedor` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_lg_b010_invitacion_lg_b009_acta_inicio1` (`fk_lgb009_num_acta_inicio` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario118`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b010_invitacion_1_lg_b022_proveedor1`
    FOREIGN KEY (`fk_lgb022_num_proveedor`)
    REFERENCES `SIACE`.`lg_b022_proveedor` (`pk_num_proveedor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b010_invitacion_lg_b009_acta_inicio1`
    FOREIGN KEY (`fk_lgb009_num_acta_inicio`)
    REFERENCES `SIACE`.`lg_b009_acta_inicio` (`pk_num_acta_inicio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 93
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_b011_evaluacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_b011_evaluacion` (
  `pk_num_evaluacion` INT(11) NOT NULL AUTO_INCREMENT,
  `num_evaluacion` INT(11) NOT NULL,
  `fec_evaluacion` DATE NOT NULL,
  `ind_objeto_evaluacion` VARCHAR(255) CHARACTER SET 'latin1' NOT NULL,
  `ind_conclusion` VARCHAR(255) CHARACTER SET 'latin1' NOT NULL,
  `ind_recomendacion` VARCHAR(255) CHARACTER SET 'latin1' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fk_lgb009_num_acta_inicio` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_asistente_a` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_asistente_b` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_asistente_c` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_evaluacion`),
  INDEX `fk_lg_b011_evaluacion_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_asistente_a` ASC),
  INDEX `fk_lg_b011_evaluacion_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_asistente_b` ASC),
  INDEX `fk_lg_b011_evaluacion_rh_b001_empleado3_idx` (`fk_rhb001_num_empleado_asistente_c` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_lg_b011_evaluacion_lg_b009_acta_inicio1_idx` (`fk_lgb009_num_acta_inicio` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario119`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b011_evaluacion_lg_b009_acta_inicio1`
    FOREIGN KEY (`fk_lgb009_num_acta_inicio`)
    REFERENCES `SIACE`.`lg_b009_acta_inicio` (`pk_num_acta_inicio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_b012_informe_recomendacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_b012_informe_recomendacion` (
  `pk_num_informe_recomendacion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_conclusiones` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_recomendacion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_rhb001_num_empleado_asistente_a` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_asistente_b` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_asistente_c` INT(11) NULL DEFAULT NULL,
  `ind_objeto_consulta` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_asunto` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_recomendacion` INT(20) NOT NULL,
  `fk_rhb001_num_empleado_creado_por` INT(11) NOT NULL,
  `fec_creacion` DATETIME NOT NULL,
  `fk_rhb001_num_empleado_revisado_por` INT(11) NULL DEFAULT NULL,
  `fec_revision` DATETIME NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_aprobado_por` INT(11) NULL DEFAULT NULL,
  `fec_aprobacion` DATETIME NULL DEFAULT NULL,
  `ind_tipo_adjudicacion` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'TT' COMMENT 'TT: Total, PR: Parcial, DT:Directa, DE: Desierto',
  `ind_articulo_numeral` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'PR' COMMENT 'PR: PREPARACION, AD: ADJUDICADO, AN: ANULADO, RV: REVISADO, AP: APROBADO',
  `fec_anio` YEAR NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_lgb011_num_evaluacion` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_informe_recomendacion`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_lg_b012_informe_recomendacion_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_asistente_a` ASC),
  INDEX `fk_lg_b012_informe_recomendacion_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_asistente_b` ASC),
  INDEX `fk_lg_b012_informe_recomendacion_rh_b001_empleado3_idx` (`fk_rhb001_num_empleado_asistente_c` ASC),
  INDEX `fk_lg_b012_informe_recomendacion_rh_b001_empleado4_idx` (`fk_rhb001_num_empleado_creado_por` ASC),
  INDEX `fk_lg_b012_informe_recomendacion_rh_b001_empleado5_idx` (`fk_rhb001_num_empleado_revisado_por` ASC),
  INDEX `fk_lg_b012_informe_recomendacion_rh_b001_empleado6_idx` (`fk_rhb001_num_empleado_aprobado_por` ASC),
  INDEX `fk_lg_b012_informe_recomendacion_lg_b011_evaluacion1_idx` (`fk_lgb011_num_evaluacion` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario120`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b012_informe_recomendacion_lg_b011_evaluacion1`
    FOREIGN KEY (`fk_lgb011_num_evaluacion`)
    REFERENCES `SIACE`.`lg_b011_evaluacion` (`pk_num_evaluacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_b013_adjudicacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_b013_adjudicacion` (
  `pk_num_adjudicacion` INT(11) NOT NULL AUTO_INCREMENT,
  `num_adjudicacion` INT(20) NOT NULL,
  `ind_tipo_adjudicacion` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'TT: TOTAL, PC: PARCIAL, DT:DIRECTA, DE: Desierto',
  `fec_creacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'PR' COMMENT 'PR: PREPARACION, AD: ADJUDICADO, AN: ANULADO, RV: REVISADO, AP: APROBADO',
  `fec_anio` YEAR NOT NULL,
  `fk_lgb012_num_informe_recomendacion` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_adjudicacion`),
  INDEX `fk_lg_adjudicacion_lg_informe_recomendacion1_idx` (`fk_lgb012_num_informe_recomendacion` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario121`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b013_adjudicacion_lg_b012_informe_recomendacion1`
    FOREIGN KEY (`fk_lgb012_num_informe_recomendacion`)
    REFERENCES `SIACE`.`lg_b012_informe_recomendacion` (`pk_num_informe_recomendacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
AVG_ROW_LENGTH = 48
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_b015_tipo_transaccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_b015_tipo_transaccion` (
  `pk_num_tipo_transaccion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipo_documento` INT(11) NOT NULL COMMENT 'I:INGRESO; E:EGRESO; T:TRANSFERENCIA',
  `fk_lgb016_num_tipo_documento_generado` INT(11) NOT NULL COMMENT 'lg_tipodocumento->CodDocumento(NI:Nota de Ingreso; NE:Nota de Salida; NT:Nota de Transferencia)',
  `fk_lgb016_num_tipo_documento_transaccion` INT(11) NOT NULL COMMENT 'lg_tipodocumento->CodDocumento',
  `num_flag_voucher_consumo` INT(1) NOT NULL,
  `num_flag_voucher_ajuste` INT(1) NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_tipo_transaccion`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_lg_b015_tipo_transaccion_af_b009_tipo_moviemiento1_idx` (`fk_a006_num_miscelaneo_detalle_tipo_documento` ASC),
  INDEX `fk_lg_b015_tipo_transaccion_cp_b002_tipo_documento1_idx` (`fk_lgb016_num_tipo_documento_generado` ASC),
  INDEX `fk_lg_b015_tipo_transaccion_cp_b002_tipo_documento2_idx` (`fk_lgb016_num_tipo_documento_transaccion` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario122`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 57
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_b018_control_periodo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_b018_control_periodo` (
  `pk_num_periodo` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `num_flag_transaccion` INT(1) NOT NULL DEFAULT '0',
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_periodo`),
  UNIQUE INDEX `mes_UNIQUE` (`fec_mes` ASC, `fec_anio` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario124`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a017_seguridad_perfil` (`pk_num_seguridad_perfil`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_b020_control_perceptivo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_b020_control_perceptivo` (
  `pk_num_control_perceptivo` INT(11) NOT NULL AUTO_INCREMENT,
  `num_control` INT(4) NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fec_registro` DATETIME NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_lgb019_num_orden` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_conforme_a` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_conforme_b` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_conforme_c` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_conforme_d` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_control_perceptivo`),
  INDEX `fk_lg_b020_control_perceptivo_lg_b019_orden1_idx` (`fk_lgb019_num_orden` ASC),
  INDEX `fk_lg_b020_control_perceptivo_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_conforme_a` ASC),
  INDEX `fk_lg_b020_control_perceptivo_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_conforme_b` ASC),
  INDEX `fk_lg_b020_control_perceptivo_rh_b001_empleado3_idx` (`fk_rhb001_num_empleado_conforme_c` ASC),
  INDEX `fk_lg_b020_control_perceptivo_rh_b001_empleado4_idx` (`fk_rhb001_num_empleado_conforme_d` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario126`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b020_control_perceptivo_lg_b019_orden1`
    FOREIGN KEY (`fk_lgb019_num_orden`)
    REFERENCES `SIACE`.`lg_b019_orden` (`pk_num_orden`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_b021_confirmacion_servicio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_b021_confirmacion_servicio` (
  `pk_num_confirmacion` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_anio` YEAR NOT NULL COMMENT 'lg_ordenserviciodetalle->Anio',
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_numero_confirmacion` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'lg_ordenserviciodetalle->NroOrden',
  `num_secuencia` INT(4) NOT NULL COMMENT 'lg_ordenserviciodetalle->Secuencia',
  `num_cantidad` DECIMAL(18,6) NOT NULL,
  `fec_confirmacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_lgb019_num_orden` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_conforma` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_confirmacion`),
  INDEX `fk_lg_b021_confirmacion_servicio_lg_b019_orden1_idx` (`fk_lgb019_num_orden` ASC),
  INDEX `fk_lg_b021_confirmacion_servicio_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_conforma` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario127`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_b021_confirmacion_servicio_lg_b019_orden1`
    FOREIGN KEY (`fk_lgb019_num_orden`)
    REFERENCES `SIACE`.`lg_b019_orden` (`pk_num_orden`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_c003_cotizacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_c003_cotizacion` (
  `pk_num_cotizacion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_num_cotizacion` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_documento` DATE NOT NULL,
  `fec_apertura` DATE NOT NULL,
  `fec_recepcion` DATE NOT NULL,
  `fec_entrega` DATE NOT NULL,
  `num_precio_unitario` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_precio_unitario_iva` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_cantidad` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_total` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_validez_oferta` INT(10) NOT NULL DEFAULT '0',
  `num_dias_entrega` INT(10) NOT NULL,
  `ind_num_cotizacion_proveedor` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_cotizacion_proveedor` DATETIME NULL DEFAULT NULL,
  `num_flag_asignado` INT(1) NOT NULL DEFAULT '0',
  `num_flag_exonerado` INT(1) NOT NULL DEFAULT '0',
  `num_descuento_fijo` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_descuento_porcentaje` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `ind_estatus` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fk_lgc001_num_requerimiento_detalle` INT(11) NOT NULL,
  `fk_lgb010_num_invitacion` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_crea` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_cotizacion`),
  INDEX `fk_lg_c003_cotizacion_lg_c001_requerimiento_detalle1_idx` (`fk_lgc001_num_requerimiento_detalle` ASC),
  INDEX `fk_lg_c003_cotizacion_lg_b010_invitacion1_idx` (`fk_lgb010_num_invitacion` ASC),
  INDEX `fk_lg_c003_cotizacion_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_crea` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario131`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c003_cotizacion_lg_b010_invitacion1`
    FOREIGN KEY (`fk_lgb010_num_invitacion`)
    REFERENCES `SIACE`.`lg_b010_invitacion` (`pk_num_invitacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c003_cotizacion_lg_c001_requeimiento_detalle`
    FOREIGN KEY (`fk_lgc001_num_requerimiento_detalle`)
    REFERENCES `SIACE`.`lg_c001_requerimiento_detalle` (`pk_num_requerimiento_detalle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 78
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_c001_requerimiento_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_c001_requerimiento_detalle` (
  `pk_num_requerimiento_detalle` INT(11) NOT NULL AUTO_INCREMENT,
  `num_secuencia` INT(4) NOT NULL,
  `num_cantidad_pedida` DECIMAL(18,6) NOT NULL,
  `num_flag_exonerado` INT(1) NOT NULL,
  `ind_comentarios` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'PR' COMMENT 'PR:En Preparacion; PE:Pendiente; AN:Anulado; RE:Rechazado; CE:Cerrado; CO:Completado',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_lgb002_num_item` INT(11) NULL DEFAULT NULL,
  `fk_lgb001_num_requerimiento` INT(11) NOT NULL,
  `fk_lgb003_num_commodity` INT(11) NULL DEFAULT NULL,
  `fk_a023_num_centro_costo` INT(11) NOT NULL,
  `fk_lgc003_num_cotizacion` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_requerimiento_detalle`),
  UNIQUE INDEX `fk_lgb003_num_commodity` (`fk_lgb003_num_commodity` ASC, `fk_lgb001_num_requerimiento` ASC),
  INDEX `fk_lg_c001_requerimiento_detalle_lg_b002_item1_idx` (`fk_lgb002_num_item` ASC),
  INDEX `fk_lg_c001_requerimiento_detalle_lg_b001_requerimiento1_idx` (`fk_lgb001_num_requerimiento` ASC),
  INDEX `fk_lg_c001_requerimiento_detalle_lg_b003_commodity1_idx` (`fk_lgb003_num_commodity` ASC),
  INDEX `fk_lg_c001_requerimiento_detalle_a023_centro_costo1_idx` (`fk_a023_num_centro_costo` ASC),
  INDEX `fk_lg_c001_requerimiento_detalle_lg_c003_cotizacion1_idx` (`fk_lgc003_num_cotizacion` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario130`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c001_requerimiento_detalle_a023_centro_costo1`
    FOREIGN KEY (`fk_a023_num_centro_costo`)
    REFERENCES `SIACE`.`a023_centro_costo` (`pk_num_centro_costo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c001_requerimiento_detalle_lg_b001_requerimiento1`
    FOREIGN KEY (`fk_lgb001_num_requerimiento`)
    REFERENCES `SIACE`.`lg_b001_requerimiento` (`pk_num_requerimiento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c001_requerimiento_detalle_lg_b002_item1`
    FOREIGN KEY (`fk_lgb002_num_item`)
    REFERENCES `SIACE`.`lg_b002_item` (`pk_num_item`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c001_requerimiento_detalle_lg_b003_commodity1`
    FOREIGN KEY (`fk_lgb003_num_commodity`)
    REFERENCES `SIACE`.`lg_b003_commodity` (`pk_num_commodity`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c001_requerimiento_detalle_lg_c003_cotizacion1`
    FOREIGN KEY (`fk_lgc003_num_cotizacion`)
    REFERENCES `SIACE`.`lg_c003_cotizacion` (`pk_num_cotizacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 100
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_c002_unidades_conversion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_c002_unidades_conversion` (
  `pk_num_unidad_conversion` INT(11) NOT NULL AUTO_INCREMENT,
  `num_cantidad` DECIMAL(18,6) NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_lgb004_num_unidad` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_unidad_conversion`),
  INDEX `fk_lg_c002_unidades_conversion_lg_b004_unidades1_idx` (`fk_lgb004_num_unidad` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario109`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c002_unidades_conversion_lg_b004_unidades1`
    FOREIGN KEY (`fk_lgb004_num_unidad`)
    REFERENCES `SIACE`.`lg_b004_unidades` (`pk_num_unidad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 184
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_c004_evaluacion_cualicuantitativa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_c004_evaluacion_cualicuantitativa` (
  `pk_num_cualicuantitativa` INT(11) NOT NULL AUTO_INCREMENT,
  `num_puntaje_renglon_ofertado` DECIMAL(18,6) NOT NULL,
  `num_puntaje_requerimiento_tecnico` DECIMAL(18,6) NOT NULL,
  `num_puntaje_tiempo_entrega` DECIMAL(18,6) NOT NULL,
  `num_puntaje_condicion_pago` DECIMAL(18,6) NOT NULL,
  `num_total_puntaje_cualicuantitativo` DECIMAL(18,6) NOT NULL,
  `num_pmo_poe` DECIMAL(18,6) NOT NULL,
  `num_pp` DECIMAL(18,6) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fk_lgc003_num_cotizacion` INT(11) NOT NULL,
  `fk_lgb022_num_proveedor_recomendado` INT(11) NOT NULL,
  `fk_lgb011_num_evaluacion` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_cualicuantitativa`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_lg_c004_evaluacion_cualicuantitativa_lg_c003_cotizacion1_idx` (`fk_lgc003_num_cotizacion` ASC),
  INDEX `fk_lg_c004_evaluacion_cualicuantitativa_lg_b022_proveedor1_idx` (`fk_lgb022_num_proveedor_recomendado` ASC),
  INDEX `fk_lg_c004_evaluacion_cualicuantitativa_lg_b011_evaluacion1_idx` (`fk_lgb011_num_evaluacion` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario132`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c004_evaluacion_cualicuantitativa_lg_b011_evaluacion1`
    FOREIGN KEY (`fk_lgb011_num_evaluacion`)
    REFERENCES `SIACE`.`lg_b011_evaluacion` (`pk_num_evaluacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c004_evaluacion_cualicuantitativa_lg_b022_proveedor1`
    FOREIGN KEY (`fk_lgb022_num_proveedor_recomendado`)
    REFERENCES `SIACE`.`lg_b022_proveedor` (`pk_num_proveedor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c004_evaluacion_cualicuantitativa_lg_c003_cotizacion1`
    FOREIGN KEY (`fk_lgc003_num_cotizacion`)
    REFERENCES `SIACE`.`lg_c003_cotizacion` (`pk_num_cotizacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 19
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_c005_adjudicacion_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_c005_adjudicacion_detalle` (
  `pk_num_adjudicacion_detalle` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_lgc001_num_requerimiento_detalle` INT(11) NOT NULL,
  `fk_lgb013_num_adjudicacion` INT(11) NOT NULL,
  `fk_lgb022_num_proveedor_adjudicado` INT(11) NOT NULL,
  `lg_c003_num_cotizacion` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_adjudicacion_detalle`),
  UNIQUE INDEX `fk_lgc001_num_requerimiento_detalle` (`fk_lgc001_num_requerimiento_detalle` ASC, `fk_lgb013_num_adjudicacion` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_lg_c005_adjudicacion_detalle_lg_b022_proveedor1_idx` (`fk_lgb022_num_proveedor_adjudicado` ASC),
  INDEX `fk_lg_c005_adjudicacion_detalle_lg_b013_adjudicacion1_idx` (`fk_lgb013_num_adjudicacion` ASC),
  INDEX `fk_lg_c005_adjudicacion_detalle_lg_c001_requerimiento_detal_idx` (`fk_lgc001_num_requerimiento_detalle` ASC),
  INDEX `lg_c003_num_cotizacion` (`lg_c003_num_cotizacion` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario133`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c005_adjudicacion_detalle_lg_b013_adjudicacion1`
    FOREIGN KEY (`fk_lgb013_num_adjudicacion`)
    REFERENCES `SIACE`.`lg_b013_adjudicacion` (`pk_num_adjudicacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c005_adjudicacion_detalle_lg_b022_proveedor1`
    FOREIGN KEY (`fk_lgb022_num_proveedor_adjudicado`)
    REFERENCES `SIACE`.`lg_b022_proveedor` (`pk_num_proveedor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c005_adjudicacion_detalle_lg_c001_requerimiento_detalle1`
    FOREIGN KEY (`fk_lgc001_num_requerimiento_detalle`)
    REFERENCES `SIACE`.`lg_c001_requerimiento_detalle` (`pk_num_requerimiento_detalle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_c006_proveedor_recomendado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_c006_proveedor_recomendado` (
  `pk_num_proveedor_recomendado` INT(11) NOT NULL AUTO_INCREMENT,
  `num_secuencia` INT(10) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_lgb012_num_informe_recomendacion` INT(11) NOT NULL,
  `fk_lgb022_num_proveedor` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_proveedor_recomendado`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_lg_c006_proveedor_recomendado_lg_b012_informe_recomendac_idx` (`fk_lgb012_num_informe_recomendacion` ASC),
  INDEX `fk_lg_c006_proveedor_recomendado_lg_b022_proveedor1_idx` (`fk_lgb022_num_proveedor` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario134`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c006_proveedor_recomendado_lg_b012_informe_recomendacion1`
    FOREIGN KEY (`fk_lgb012_num_informe_recomendacion`)
    REFERENCES `SIACE`.`lg_b012_informe_recomendacion` (`pk_num_informe_recomendacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c006_proveedor_recomendado_lg_b022_proveedor1`
    FOREIGN KEY (`fk_lgb022_num_proveedor`)
    REFERENCES `SIACE`.`lg_b022_proveedor` (`pk_num_proveedor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_c007_item_stock`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_c007_item_stock` (
  `pk_num_item_stock` INT(11) NOT NULL AUTO_INCREMENT,
  `num_stock_inicial` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_stock_actual` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_stock_comprometido` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_precio_unitario` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_lgc002_num_unidad_conversion` INT(11) NOT NULL,
  `fk_lgb014_num_almacen` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_ingresadopor` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_item_stock`),
  INDEX `fk_lg_c007_item_stock_lg_c002_unidades_conversion1_idx` (`fk_lgc002_num_unidad_conversion` ASC),
  INDEX `fk_lg_c007_item_stock_lg_b014_almacen1_idx` (`fk_lgb014_num_almacen` ASC),
  INDEX `fk_lg_c007_item_stock_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_ingresadopor` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario110`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c007_item_stock_lg_b014_almacen1`
    FOREIGN KEY (`fk_lgb014_num_almacen`)
    REFERENCES `SIACE`.`lg_b014_almacen` (`pk_num_almacen`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c007_item_stock_lg_c002_unidades_conversion1`
    FOREIGN KEY (`fk_lgc002_num_unidad_conversion`)
    REFERENCES `SIACE`.`lg_c002_unidades_conversion` (`pk_num_unidad_conversion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 364
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_c008_commodity_stock`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_c008_commodity_stock` (
  `pk_num_commodity_stock` INT(11) NOT NULL AUTO_INCREMENT,
  `num_stock_inicial` DECIMAL(18,6) NOT NULL,
  `num_cantidad` DECIMAL(18,6) NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `num_precio_unitario` DECIMAL(18,6) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_lgb014_num_almacen` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_ingresadopor` INT(11) NOT NULL,
  `fk_lgb003_num_commodity` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_commodity_stock`),
  INDEX `fk_lg_c008_commodity_stock_lg_b014_almacen1_idx` (`fk_lgb014_num_almacen` ASC),
  INDEX `fk_lg_c008_commodity_stock_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_ingresadopor` ASC),
  INDEX `fk_lg_c008_commodity_stock_lg_b003_commodity1_idx` (`fk_lgb003_num_commodity` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario135`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c008_commodity_stock_lg_b003_commodity1`
    FOREIGN KEY (`fk_lgb003_num_commodity`)
    REFERENCES `SIACE`.`lg_b003_commodity` (`pk_num_commodity`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c008_commodity_stock_lg_b014_almacen1`
    FOREIGN KEY (`fk_lgb014_num_almacen`)
    REFERENCES `SIACE`.`lg_b014_almacen` (`pk_num_almacen`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2280
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_c009_orden_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_c009_orden_detalle` (
  `pk_num_orden_detalle` INT(11) NOT NULL AUTO_INCREMENT,
  `num_secuencia` INT(10) NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_cantidad` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_precio_unitario` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_precio_unitario_iva` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_descuento_porcentaje` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_descuento_fijo` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_base` DECIMAL(18,6) NOT NULL,
  `num_total` DECIMAL(18,6) NOT NULL,
  `num_flag_exonerado` INT(1) NOT NULL DEFAULT '0',
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'PR' COMMENT 'PR:En Preparacion; PE:Pendiente; AN:Anulado; RE:Rechazado; CE:Cerrado; CO:Completado',
  `fk_cvb004_plan_cuenta_oncop` INT(11) NOT NULL COMMENT 'ac_mastplancuenta->CodCuenta',
  `fk_cvb004_plan_cuenta_pub_veinte` INT(11) NOT NULL COMMENT 'ac_mastplancuenta20->CodCuenta',
  `fk_prb002_partida_presupuestaria` INT(11) NOT NULL COMMENT 'pv_partida->cod_partida',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_lgb004_num_unidad` INT(11) NOT NULL,
  `fk_lgb019_num_orden` INT(11) NOT NULL,
  `fk_a023_num_centro_costo` INT(11) NOT NULL,
  `fk_lgb002_num_item` INT(11) NULL DEFAULT NULL,
  `fk_lgb003_num_commodity` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_orden_detalle`),
  INDEX `FK_lg_ordencompradetalle_5` (`fk_prb002_partida_presupuestaria` ASC),
  INDEX `fk_lg_c009_orden_detalle_lg_b004_unidades1_idx` (`fk_lgb004_num_unidad` ASC),
  INDEX `fk_lg_c009_orden_detalle_lg_b019_orden1_idx` (`fk_lgb019_num_orden` ASC),
  INDEX `fk_lg_c009_orden_detalle_a023_centro_costo1_idx` (`fk_a023_num_centro_costo` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_lg_c009_orden_detalle_cv_b004_plan_cuenta1_idx` (`fk_cvb004_plan_cuenta_oncop` ASC),
  INDEX `fk_lg_c009_orden_detalle_cv_b004_plan_cuenta2_idx` (`fk_cvb004_plan_cuenta_pub_veinte` ASC),
  INDEX `fk_lgb002_num_item` (`fk_lgb002_num_item` ASC),
  INDEX `fk_lgb003_num_commodity` (`fk_lgb003_num_commodity` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario136`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c009_orden_detalle_a023_centro_costo1`
    FOREIGN KEY (`fk_a023_num_centro_costo`)
    REFERENCES `SIACE`.`a023_centro_costo` (`pk_num_centro_costo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c009_orden_detalle_cv_b004_plan_cuenta1`
    FOREIGN KEY (`fk_cvb004_plan_cuenta_oncop`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c009_orden_detalle_cv_b004_plan_cuenta2`
    FOREIGN KEY (`fk_cvb004_plan_cuenta_pub_veinte`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c009_orden_detalle_lg_b004_unidades1`
    FOREIGN KEY (`fk_lgb004_num_unidad`)
    REFERENCES `SIACE`.`lg_b004_unidades` (`pk_num_unidad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c009_orden_detalle_lg_b019_orden1`
    FOREIGN KEY (`fk_lgb019_num_orden`)
    REFERENCES `SIACE`.`lg_b019_orden` (`pk_num_orden`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c009_orden_detalle_pr_b002_partida_presupuestaria1`
    FOREIGN KEY (`fk_prb002_partida_presupuestaria`)
    REFERENCES `SIACE`.`pr_b002_partida_presupuestaria` (`pk_num_partida_presupuestaria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_c010_control_perceptivo_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_c010_control_perceptivo_detalle` (
  `pk_num_control_perceptivo_detalle` INT(11) NOT NULL AUTO_INCREMENT,
  `num_secuencia` INT(10) NOT NULL,
  `ind_observacion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_cantidad` DECIMAL(18,6) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_lgb020_num_control_perceptivo` INT(11) NOT NULL,
  `num_flag_recibido` INT(1) NOT NULL DEFAULT '0',
  `fk_lgc009_num_orden_detalle` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_control_perceptivo_detalle`),
  INDEX `fk_lg_c010_control_perceptivo_detalle_lg_b020_control_perce_idx` (`fk_lgb020_num_control_perceptivo` ASC),
  INDEX `fk_lg_c010_control_perceptivo_detalle_lg_c009_orden_detalle_idx` (`fk_lgc009_num_orden_detalle` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario137`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c010_control_perceptivo_detalle_lg_b020_control_percept1`
    FOREIGN KEY (`fk_lgb020_num_control_perceptivo`)
    REFERENCES `SIACE`.`lg_b020_control_perceptivo` (`pk_num_control_perceptivo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c010_control_perceptivo_detalle_lg_c009_orden_detalle1`
    FOREIGN KEY (`fk_lgc009_num_orden_detalle`)
    REFERENCES `SIACE`.`lg_c009_orden_detalle` (`pk_num_orden_detalle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_c011_acta_requerimiento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_c011_acta_requerimiento` (
  `pk_num_acta_requerimiento` INT(11) NOT NULL AUTO_INCREMENT,
  `num_secuencia` INT(10) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_lgb009_num_acta_inicio` INT(11) NOT NULL,
  `fk_lgb001_num_requerimiento` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_acta_requerimiento`),
  INDEX `fk_lg_c011_acta_requerimiento_lg_b009_acta_inicio1_idx` (`fk_lgb009_num_acta_inicio` ASC),
  INDEX `fk_lg_c011_acta_requerimiento_lg_b001_requerimiento1_idx` (`fk_lgb001_num_requerimiento` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario129`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c011_acta_requerimiento_lg_b001_requerimiento1`
    FOREIGN KEY (`fk_lgb001_num_requerimiento`)
    REFERENCES `SIACE`.`lg_b001_requerimiento` (`pk_num_requerimiento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_c011_acta_requerimiento_lg_b009_acta_inicio1`
    FOREIGN KEY (`fk_lgb009_num_acta_inicio`)
    REFERENCES `SIACE`.`lg_b009_acta_inicio` (`pk_num_acta_inicio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_d001_transaccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_d001_transaccion` (
  `pk_num_transaccion` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'lg_tipodocumento->CodDocumento',
  `num_transaccion` VARCHAR(6) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_documento` DATE NOT NULL,
  `fk_a006_num_centro_costo` INT(11) NOT NULL COMMENT 'ac_mastcentrocosto->CodCentroCosto',
  `ind_num_documento_referencia` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_comentario` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_manual` INT(1) NOT NULL DEFAULT '0',
  `num_flag_pendiente` INT(1) NOT NULL DEFAULT '0',
  `ind_nota_entrega_factura` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_ejecutado_por` INT(11) NOT NULL,
  `fec_ejecucion` DATETIME NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fk_lgb015_num_tipo_transaccion` INT(11) NOT NULL,
  `fk_lgb014_num_almacen` INT(11) NOT NULL,
  `fk_lgb016_num_tipo_documento` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_transaccion`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_lg_d001_transaccion_a006_centro_costo1_idx` (`fk_a006_num_centro_costo` ASC),
  INDEX `fk_lg_d001_transaccion_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_ejecutado_por` ASC),
  INDEX `fk_lg_d001_transaccion_lg_b015_tipo_transaccion1_idx` (`fk_lgb015_num_tipo_transaccion` ASC),
  INDEX `fk_lg_d001_transaccion_lg_b014_almacen1_idx` (`fk_lgb014_num_almacen` ASC),
  INDEX `fk_lg_d001_transaccion_lg_b016_tipo_documento1_idx` (`fk_lgb016_num_tipo_documento` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario138`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_d001_transaccion_lg_b014_almacen1`
    FOREIGN KEY (`fk_lgb014_num_almacen`)
    REFERENCES `SIACE`.`lg_b014_almacen` (`pk_num_almacen`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_d001_transaccion_lg_b015_tipo_transaccion1`
    FOREIGN KEY (`fk_lgb015_num_tipo_transaccion`)
    REFERENCES `SIACE`.`lg_b015_tipo_transaccion` (`pk_num_tipo_transaccion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_d001_transaccion_lg_b016_tipo_documento1`
    FOREIGN KEY (`fk_lgb016_num_tipo_documento`)
    REFERENCES `SIACE`.`lg_b016_tipo_documento` (`pk_num_tipo_documento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_d002_transaccion_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_d002_transaccion_detalle` (
  `pk_num_transaccion_detalle` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'lg_tipodocumento->CodDocumento',
  `num_secuencia` INT(10) NOT NULL,
  `num_stock_anterior` DECIMAL(18,6) NOT NULL,
  `num_cantidad_transaccion` DECIMAL(18,6) NOT NULL,
  `num_precio_unitario` DECIMAL(18,6) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_lgb003_num_commodity` INT(11) NULL DEFAULT NULL,
  `fk_lgb002_num_item` INT(11) NULL DEFAULT NULL,
  `fk_lgb004_num_unidad` INT(11) NULL DEFAULT NULL,
  `fk_lgd001_num_transaccion` INT(11) NOT NULL,
  `fk_lgc001_num_requerimiento_detalle` INT(11) NULL DEFAULT NULL,
  `fk_lgc009_num_orden_detalle` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_transaccion_detalle`),
  INDEX `fk_lg_d002_transaccion_detalle_lg_b002_item1_idx` (`fk_lgb002_num_item` ASC),
  INDEX `fk_lg_d002_transaccion_detalle_lg_b004_unidades1_idx` (`fk_lgb004_num_unidad` ASC),
  INDEX `fk_lg_d002_transaccion_detalle_lg_d001_transaccion1_idx` (`fk_lgd001_num_transaccion` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_lgc001_num_requerimiento_detalle` (`fk_lgc001_num_requerimiento_detalle` ASC),
  INDEX `fk_lgc009_num_orden_detalle` (`fk_lgc009_num_orden_detalle` ASC),
  INDEX `fk_lgb003_num_commodity` (`fk_lgb003_num_commodity` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario139`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_d002_transaccion_detalle_lg_b002_item1`
    FOREIGN KEY (`fk_lgb002_num_item`)
    REFERENCES `SIACE`.`lg_b002_item` (`pk_num_item`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_d002_transaccion_detalle_lg_b004_unidades1`
    FOREIGN KEY (`fk_lgb004_num_unidad`)
    REFERENCES `SIACE`.`lg_b004_unidades` (`pk_num_unidad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_d002_transaccion_detalle_lg_d001_transaccion1`
    FOREIGN KEY (`fk_lgd001_num_transaccion`)
    REFERENCES `SIACE`.`lg_d001_transaccion` (`pk_num_transaccion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `lg_d002_transaccion_detalle_ibfk_1`
    FOREIGN KEY (`fk_lgb003_num_commodity`)
    REFERENCES `SIACE`.`lg_b003_commodity` (`pk_num_commodity`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_d003_activo_fijo_transito`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_d003_activo_fijo_transito` (
  `pk_num_activo_fijo_transito` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'PE:PENDIENTE; TR:TRANSFERIDO;',
  `fec_anio` YEAR NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_lgd002_num_transaccion_detalle` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_activo_fijo_transito`),
  INDEX `fk_lg_activo_fijo_transito_lg_transaccion_detalle1_idx` (`fk_lgd002_num_transaccion_detalle` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario140`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_activo_fijo_transito_lg_transaccion_detalle1`
    FOREIGN KEY (`fk_lgd002_num_transaccion_detalle`)
    REFERENCES `SIACE`.`lg_d002_transaccion_detalle` (`pk_num_transaccion_detalle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_d004_cierre_mensual`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_d004_cierre_mensual` (
  `pk_num_cierre` INT(11) NOT NULL AUTO_INCREMENT,
  `num_ingreso` DECIMAL(18,6) NOT NULL,
  `num_salida` DECIMAL(18,6) NOT NULL,
  `num_stock_final` DECIMAL(18,6) NOT NULL,
  `num_precio_unitario` DECIMAL(18,6) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fk_lgc007_num_item_stock` INT(11) NOT NULL,
  `fk_lgb018_num_periodo` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_cierre`),
  INDEX `fk_lg_d004_cierre_mensual_lg_c007_item_stock1_idx` (`fk_lgc007_num_item_stock` ASC),
  INDEX `fk_lg_d004_cierre_mensual_lg_b018_control_periodo1_idx` (`fk_lgb018_num_periodo` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario141`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_d004_cierre_mensual_lg_b018_control_periodo1`
    FOREIGN KEY (`fk_lgb018_num_periodo`)
    REFERENCES `SIACE`.`lg_b018_control_periodo` (`pk_num_periodo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_d004_cierre_mensual_lg_c007_item_stock1`
    FOREIGN KEY (`fk_lgc007_num_item_stock`)
    REFERENCES `SIACE`.`lg_c007_item_stock` (`pk_num_item_stock`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_d005_orden_requerimiento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_d005_orden_requerimiento` (
  `pk_num_orden_vs_requerimiento` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_lgb019_num_orden` INT(11) NOT NULL,
  `fk_lgb001_num_requerimiento` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_orden_vs_requerimiento`),
  INDEX `fk_lg_d005_orden_requerimiento_lg_c009_orden_detalle1_idx` (`fk_lgb019_num_orden` ASC),
  INDEX `fk_lg_d005_orden_requerimiento_lg_b001_requerimiento1_idx` (`fk_lgb001_num_requerimiento` ASC),
  CONSTRAINT `fk_lg_d005_orden_requerimiento_lg_b001_requerimiento1`
    FOREIGN KEY (`fk_lgb001_num_requerimiento`)
    REFERENCES `SIACE`.`lg_b001_requerimiento` (`pk_num_requerimiento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_d005_orden_requerimiento_lg_c009_orden_detalle1`
    FOREIGN KEY (`fk_lgb019_num_orden`)
    REFERENCES `SIACE`.`lg_c009_orden_detalle` (`pk_num_orden_detalle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_d006_orden_transaccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_d006_orden_transaccion` (
  `pk_num_orden_transaccion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_lgd001_num_transaccion` INT(11) NOT NULL,
  `fk_lgb020_num_control_perceptivo` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_orden_transaccion`),
  INDEX `fk_lg_d006_orden_transaccion_lg_d001_transaccion1_idx` (`fk_lgd001_num_transaccion` ASC),
  INDEX `fk_lg_d006_orden_transaccion_lg_b020_control_perceptivo1_idx` (`fk_lgb020_num_control_perceptivo` ASC),
  CONSTRAINT `fk_lg_d006_orden_transaccion_lg_b020_control_perceptivo1`
    FOREIGN KEY (`fk_lgb020_num_control_perceptivo`)
    REFERENCES `SIACE`.`lg_b020_control_perceptivo` (`pk_num_control_perceptivo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_d006_orden_transaccion_lg_d001_transaccion1`
    FOREIGN KEY (`fk_lgd001_num_transaccion`)
    REFERENCES `SIACE`.`lg_d001_transaccion` (`pk_num_transaccion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_d007_distribucion_orden`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_d007_distribucion_orden` (
  `pk_num_distribucion_orden` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_anio` YEAR NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_secuencia` INT(10) NOT NULL,
  `fk_prb002_num_partida_presupuestaria` INT(11) NOT NULL COMMENT 'pv_partida->cod_partida',
  `fk_cbb004_num_plan_cuenta_oncop` INT(11) NOT NULL COMMENT 'ac_mastplancuenta->CodCuenta',
  `fk_cvb004_num_plan_cuenta_pub_veinte` INT(11) NOT NULL COMMENT 'ac_mastplancuenta20->CodCuenta',
  `num_monto` DECIMAL(18,6) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_lgb019_num_orden` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_distribucion_orden`),
  INDEX `FK_lg_distribucionoc_2` (`fk_prb002_num_partida_presupuestaria` ASC),
  INDEX `fk_lg_d007_distribucion_orden_lg_b019_orden1_idx` (`fk_lgb019_num_orden` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_lg_d007_distribucion_orden_cb_b004_plan_cuenta1_idx` (`fk_cbb004_num_plan_cuenta_oncop` ASC),
  INDEX `fk_lg_d007_distribucion_orden_cb_b004_plan_cuenta2_idx` (`fk_cvb004_num_plan_cuenta_pub_veinte` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario142`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_d007_distribucion_orden_cb_b004_plan_cuenta1`
    FOREIGN KEY (`fk_cbb004_num_plan_cuenta_oncop`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_d007_distribucion_orden_cb_b004_plan_cuenta2`
    FOREIGN KEY (`fk_cvb004_num_plan_cuenta_pub_veinte`)
    REFERENCES `SIACE`.`cb_b004_plan_cuenta` (`pk_num_cuenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_d007_distribucion_orden_lg_b019_orden1`
    FOREIGN KEY (`fk_lgb019_num_orden`)
    REFERENCES `SIACE`.`lg_b019_orden` (`pk_num_orden`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_d007_distribucion_orden_pr_b002_partida_presupuestaria1`
    FOREIGN KEY (`fk_prb002_num_partida_presupuestaria`)
    REFERENCES `SIACE`.`pr_b002_partida_presupuestaria` (`pk_num_partida_presupuestaria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_e001_inventario_fisico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_e001_inventario_fisico` (
  `pk_num_registro` INT(11) NOT NULL AUTO_INCREMENT,
  `num_proceso` INT(4) NOT NULL,
  `fec_registro` DATE NOT NULL,
  `num_stock_logico` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_stock_fisico` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fk_lgb002_num_item` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_realiza` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_registro`),
  INDEX `fk_lg_e001_inventario_fisico_lg_b002_item1_idx` (`fk_lgb002_num_item` ASC),
  INDEX `fk_lg_e001_inventario_fisico_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_realiza` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario143`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lg_e001_inventario_fisico_lg_b002_item1`
    FOREIGN KEY (`fk_lgb002_num_item`)
    REFERENCES `SIACE`.`lg_b002_item` (`pk_num_item`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_e002_proveedor_documento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_e002_proveedor_documento` (
  `fk_cpb002_num_tipo_documento` INT(11) NOT NULL,
  `fk_lgb022_num_proveedor` INT(11) NOT NULL,
  UNIQUE INDEX `fk_cpb002_num_tipo_documento` (`fk_cpb002_num_tipo_documento` ASC, `fk_lgb022_num_proveedor` ASC),
  INDEX `lg_e002_proveedor_documento_lg_b022_proveedor1` (`fk_lgb022_num_proveedor` ASC),
  CONSTRAINT `lg_e002_proveedor_documento_lg_b022_proveedor1`
    FOREIGN KEY (`fk_lgb022_num_proveedor`)
    REFERENCES `SIACE`.`lg_b022_proveedor` (`pk_num_proveedor`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_e003_proveedor_servicio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_e003_proveedor_servicio` (
  `fk_cpb017_num_tipo_servicio` INT(11) NOT NULL,
  `fk_lgb022_num_proveedor` INT(11) NOT NULL,
  UNIQUE INDEX `fk_cpb017_num_tipo_servicio` (`fk_cpb017_num_tipo_servicio` ASC, `fk_lgb022_num_proveedor` ASC),
  INDEX `lg_e003_proveedor_servicio_lg_b022_proveedor1` (`fk_lgb022_num_proveedor` ASC),
  CONSTRAINT `lg_e003_proveedor_servicio_cp_b017_tipo_servicio1`
    FOREIGN KEY (`fk_cpb017_num_tipo_servicio`)
    REFERENCES `SIACE`.`cp_b017_tipo_servicio` (`pk_num_tipo_servico`),
  CONSTRAINT `lg_e003_proveedor_servicio_lg_b022_proveedor1`
    FOREIGN KEY (`fk_lgb022_num_proveedor`)
    REFERENCES `SIACE`.`lg_b022_proveedor` (`pk_num_proveedor`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_e004_activo_fijo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_e004_activo_fijo` (
  `pk_num_activo_fijo` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_lgd002_num_transaccion_detalle` INT(11) NOT NULL,
  `fk_lgc009_num_orden_detalle` INT(11) NOT NULL,
  `fk_afc005_num_ubicacion` INT(11) NOT NULL,
  `fk_a023_num_centro_costo` INT(11) NOT NULL,
  `fk_a006_num_marca` INT(11) NULL DEFAULT NULL,
  `fk_a006_num_color` INT(11) NULL DEFAULT NULL,
  `fk_cpd001_num_obligacion` INT(11) NULL DEFAULT NULL,
  `fk_afb001_num_activo` INT(11) NULL DEFAULT NULL,
  `ind_nro_serie` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_modelo` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_cod_barra` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_nro_placa` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ingreso` DATE NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'PE:PENDIENTE / TR:TRANSFERIDO; / AN:ANULADO',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATE NOT NULL,
  PRIMARY KEY (`pk_num_activo_fijo`),
  INDEX `fk_lge004_lgd002_num_transaccion_detalle` (`fk_lgd002_num_transaccion_detalle` ASC),
  INDEX `fk_lgb004_afc005_num_ubicacion` (`fk_afc005_num_ubicacion` ASC),
  INDEX `fk_lgb004_a023_num_centro_costo` (`fk_a023_num_centro_costo` ASC),
  INDEX `fk_lgb004_cpd001_num_obligacion` (`fk_cpd001_num_obligacion` ASC),
  INDEX `fk_lgb004_afb001_num_activo` (`fk_afb001_num_activo` ASC),
  INDEX `fk_lgb004_lgc009_num_orden_detalle` USING BTREE (`fk_lgc009_num_orden_detalle` ASC),
  INDEX `fk_lge004_a006_num_color` (`fk_a006_num_color` ASC),
  INDEX `fk_lge004_a006_num_marca` (`fk_a006_num_marca` ASC),
  INDEX `fk_lge004_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_lge004_a006_num_color`
    FOREIGN KEY (`fk_a006_num_color`)
    REFERENCES `SIACE`.`a006_miscelaneo_detalle` (`pk_num_miscelaneo_detalle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lge004_a006_num_marca`
    FOREIGN KEY (`fk_a006_num_marca`)
    REFERENCES `SIACE`.`a006_miscelaneo_detalle` (`pk_num_miscelaneo_detalle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lge004_a018_num_seguridad_usuario`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lge004_a023_num_centro_costo`
    FOREIGN KEY (`fk_a023_num_centro_costo`)
    REFERENCES `SIACE`.`a023_centro_costo` (`pk_num_centro_costo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lge004_afb001_num_activo`
    FOREIGN KEY (`fk_afb001_num_activo`)
    REFERENCES `SIACE`.`af_b001_activo` (`pk_num_activo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lge004_afc005_num_ubicacion`
    FOREIGN KEY (`fk_afc005_num_ubicacion`)
    REFERENCES `SIACE`.`af_c005_ubicacion` (`pk_num_ubicacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lge004_cpd001_num_obligacion`
    FOREIGN KEY (`fk_cpd001_num_obligacion`)
    REFERENCES `SIACE`.`cp_d001_obligacion` (`pk_num_obligacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lge004_lgc009_num_orden`
    FOREIGN KEY (`fk_lgc009_num_orden_detalle`)
    REFERENCES `SIACE`.`lg_c009_orden_detalle` (`pk_num_orden_detalle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lge004_lgd002_num_transaccion_detalle`
    FOREIGN KEY (`fk_lgd002_num_transaccion_detalle`)
    REFERENCES `SIACE`.`lg_d002_transaccion_detalle` (`pk_num_transaccion_detalle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_e005_documento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_e005_documento` (
  `pk_num_documento` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_cpb001_num_documento_clasificacion` INT(11) NOT NULL,
  `fk_lgb019_num_orden` INT(11) NOT NULL,
  `fec_documento` DATE NOT NULL,
  `num_anio` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_tipo_documento` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_monto_afecto` DECIMAL(18,6) NOT NULL,
  `num_monto_no_afecto` DECIMAL(18,6) NOT NULL,
  `num_monto_impuesto` DECIMAL(18,6) NOT NULL,
  `num_monto_total` DECIMAL(18,6) NOT NULL,
  `num_monto_pendiente` DECIMAL(18,6) NOT NULL,
  `num_monto_pagado` DECIMAL(18,6) NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_lgd001_num_transaccion` INT(11) NULL DEFAULT NULL,
  `ind_comentarios` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_obligacion_tipo_documento` INT(11) NULL DEFAULT NULL,
  `fk_cpd001_num_obligacion` INT(11) NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATE NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_documento`),
  INDEX `fk_cpb001_num_documento_clasificacion` (`fk_cpb001_num_documento_clasificacion` ASC),
  INDEX `fk_lgb019_num_orden` (`fk_lgb019_num_orden` ASC),
  INDEX `fk_lgd001_num_transaccion` (`fk_lgd001_num_transaccion` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `lg_e005_documento_ibfk_1`
    FOREIGN KEY (`fk_cpb001_num_documento_clasificacion`)
    REFERENCES `SIACE`.`cp_b001_documento_clasificacion` (`pk_num_documento_clasificacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `lg_e005_documento_ibfk_2`
    FOREIGN KEY (`fk_lgb019_num_orden`)
    REFERENCES `SIACE`.`lg_b019_orden` (`pk_num_orden`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `lg_e005_documento_ibfk_3`
    FOREIGN KEY (`fk_lgd001_num_transaccion`)
    REFERENCES `SIACE`.`lg_d001_transaccion` (`pk_num_transaccion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `lg_e005_documento_ibfk_4`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`lg_e006_documento_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`lg_e006_documento_detalle` (
  `pk_num_documento_detalle` INT(11) NOT NULL AUTO_INCREMENT,
  `num_secuencia` INT(11) NOT NULL,
  `fk_lge005_num_documento` INT(11) NOT NULL,
  `fk_lgd002_num_transaccion_detalle` INT(11) NULL DEFAULT NULL,
  `fk_lgb021_num_confirmacion_servicio` INT(11) NULL DEFAULT NULL,
  `num_cantidad` DECIMAL(18,6) NOT NULL,
  `num_precio_unit` DECIMAL(18,6) NOT NULL,
  `num_precio_cantidad` DECIMAL(18,6) NOT NULL,
  `num_total` DECIMAL(18,6) NOT NULL,
  `fec_ultima_modificacion` DATE NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_documento_detalle`),
  INDEX `fk_lge005_num_documento` (`fk_lge005_num_documento` ASC),
  INDEX `fk_lgd002_num_transaccion_detalle` (`fk_lgd002_num_transaccion_detalle` ASC),
  INDEX `fk_lgb021_num_confirmacion_servicio` (`fk_lgb021_num_confirmacion_servicio` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `lg_e006_documento_detalle_ibfk_1`
    FOREIGN KEY (`fk_lge005_num_documento`)
    REFERENCES `SIACE`.`lg_e005_documento` (`pk_num_documento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `lg_e006_documento_detalle_ibfk_2`
    FOREIGN KEY (`fk_lgd002_num_transaccion_detalle`)
    REFERENCES `SIACE`.`lg_d002_transaccion_detalle` (`pk_num_transaccion_detalle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `lg_e006_documento_detalle_ibfk_3`
    FOREIGN KEY (`fk_lgb021_num_confirmacion_servicio`)
    REFERENCES `SIACE`.`lg_b021_confirmacion_servicio` (`pk_num_confirmacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `lg_e006_documento_detalle_ibfk_4`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`nm_b003_tipo_proceso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`nm_b003_tipo_proceso` (
  `pk_num_tipo_proceso` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_proceso` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_nombre_proceso` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_adelanto` INT(1) NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_tipo_proceso`),
  UNIQUE INDEX `cod_porceso_UNIQUE` (`cod_proceso` ASC),
  INDEX `fk_nm_b003_tipo_proceso_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_nm_b003_tipo_proceso_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 21
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`nm_b004_ajuste_salarial`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`nm_b004_ajuste_salarial` (
  `pk_num_ajust_salarial` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_anio` YEAR NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_numero_resolucion` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_numero_gaceta` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_preparado` DATETIME NOT NULL,
  `fec_conformado` DATETIME NULL DEFAULT NULL,
  `fec_aprobado` DATETIME NULL DEFAULT NULL,
  `fec_anulado` DATETIME NULL DEFAULT NULL,
  `ind_motivo_anulacion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'PR',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_rhb001_num_empleado_crea` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_conforma` INT(11) NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_aprueba` INT(11) NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_anula` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_ajust_salarial`),
  INDEX `ANIO` (`fec_anio` ASC),
  INDEX `fk_nm_b004_ajuste_salarial_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_crea` ASC),
  INDEX `fk_nm_b004_ajuste_salarial_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_aprueba` ASC),
  INDEX `fk_nm_b004_ajuste_salarial_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_nm_b004_ajuste_salarial_rh_b001_empleado3_idx` (`fk_rhb001_num_empleado_conforma` ASC),
  INDEX `fk_nm_b004_ajuste_salarial_rh_b001_empleado4_idx` (`fk_rhb001_num_empleado_anula` ASC),
  CONSTRAINT `fk_nm_b004_ajuste_salarial_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`nm_b005_interfaz_cxp`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`nm_b005_interfaz_cxp` (
  `pk_num_interfaz_cxp` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a003_num_persona_proveedor` INT(11) NOT NULL,
  `fk_a003_num_persona_pagar` INT(11) NOT NULL,
  `fk_cpb002_num_tipo_documento` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_det_tipo_pago` INT(11) NOT NULL,
  `fk_000_tipo_servicio` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_det_tipo_obligacion` INT(11) NOT NULL,
  `ind_titulo` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_comentario` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_num_cuenta` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_num_factura` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_monto` DECIMAL(18,6) NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridadusuario` INT(11) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NULL DEFAULT NULL,
  `num_flag_prq` INT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pk_num_interfaz_cxp`),
  INDEX `fk_nm_b005_interfaz_cxp_a000_persona1_idx` (`fk_a003_num_persona_proveedor` ASC),
  INDEX `fk_nm_b005_interfaz_cxp_a000_persona2_idx` (`fk_a003_num_persona_pagar` ASC),
  INDEX `fk_nm_b005_interfaz_cxp_a000_miscelaneo_det1_idx` (`fk_a006_num_miscelaneo_det_tipo_pago` ASC),
  INDEX `fk_nm_b005_interfaz_cxp_a000_miscelaneo_det2_idx` (`fk_a006_num_miscelaneo_det_tipo_obligacion` ASC),
  INDEX `fk_nm_b005_interfaz_cxp_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridadusuario` ASC),
  INDEX `fk_nm_b005_interfaz_cxp_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  CONSTRAINT `fk_nm_b005_interfaz_cxp_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridadusuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`nm_b006_sueldo_minimo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`nm_b006_sueldo_minimo` (
  `pk_num_sueldo_minimo` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_periodo` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_preparado` DATETIME NOT NULL,
  `fec_aprobado` DATETIME NULL DEFAULT NULL,
  `fec_anulado` DATETIME NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `ind_descripcion` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_num_resolucion` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_num_gaceta` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_motivo_anulado` LONGTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_preparado` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_aprobado` INT(11) NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_anulado` INT(11) NULL DEFAULT NULL,
  `num_sueldo_minimo` DECIMAL(18,6) NOT NULL,
  PRIMARY KEY (`pk_num_sueldo_minimo`),
  UNIQUE INDEX `unico` (`ind_num_resolucion` ASC, `ind_num_gaceta` ASC),
  INDEX `fk_nm_b006_sueldo_minimo_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_nm_b006_sueldo_minimo_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_preparado` ASC),
  INDEX `fk_nm_b006_sueldo_minimo_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_aprobado` ASC),
  INDEX `fk_nm_b006_sueldo_minimo_rh_b001_empleado3_idx` (`fk_rhb001_num_empleado_anulado` ASC),
  CONSTRAINT `fk_nm_b006_sueldo_minimo_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`nm_c001_tipo_nomina_periodo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`nm_c001_tipo_nomina_periodo` (
  `pk_num_tipo_nomina_periodo` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_nmb001_num_tipo_nomina` INT(11) NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  PRIMARY KEY (`pk_num_tipo_nomina_periodo`),
  UNIQUE INDEX `unico` (`fec_mes` ASC, `fec_anio` ASC, `fk_nmb001_num_tipo_nomina` ASC),
  INDEX `fk_pr_tipo_nomina_periodo_pr_tipo_nomina1_idx` (`fk_nmb001_num_tipo_nomina` ASC),
  CONSTRAINT `fk_pr_tipo_nomina_periodo_pr_tipo_nomina1`
    FOREIGN KEY (`fk_nmb001_num_tipo_nomina`)
    REFERENCES `SIACE`.`nm_b001_tipo_nomina` (`pk_num_tipo_nomina`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 24
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`nm_c002_proceso_periodo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`nm_c002_proceso_periodo` (
  `pk_num_proceso_periodo` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_nmc001_num_tipo_nomina_periodo` INT(11) NOT NULL,
  `fk_nmb003_num_tipo_proceso` INT(11) NOT NULL,
  `fec_desde` DATE NOT NULL,
  `fec_hasta` DATE NOT NULL,
  `fec_creado` DATETIME NOT NULL,
  `fec_aprobado` DATETIME NULL DEFAULT NULL,
  `fec_procesado` DATETIME NULL DEFAULT NULL,
  `fec_cerrado` DATETIME NULL DEFAULT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_flag_aprobado` INT(1) NULL DEFAULT NULL,
  `num_flag_procesado` INT(1) NULL DEFAULT NULL,
  `num_flag_cerrado` INT(1) NULL DEFAULT NULL,
  `num_flag_pagado` INT(1) NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_rhb001_num_empleado_crea` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_aprueba` INT(11) NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_procesa` INT(11) NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_cierra` INT(11) NULL DEFAULT NULL,
  `fk_a018_num_seguridadusuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_proceso_periodo`),
  UNIQUE INDEX `fk_nmc001_num_tipo_nomina_periodo` (`fk_nmc001_num_tipo_nomina_periodo` ASC, `fk_nmb003_num_tipo_proceso` ASC),
  INDEX `fk_pr_proceso_periodo_pr_tipo_proceso1_idx` (`fk_nmb003_num_tipo_proceso` ASC),
  INDEX `fk_pr_proceso_periodo_pr_tipo_nomina_periodo1_idx` (`fk_nmc001_num_tipo_nomina_periodo` ASC),
  INDEX `fk_nm_c002_proceso_periodo_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_crea` ASC),
  INDEX `fk_nm_c002_proceso_periodo_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_aprueba` ASC),
  INDEX `fk_nm_c002_proceso_periodo_rh_b001_empleado3_idx` (`fk_rhb001_num_empleado_procesa` ASC),
  INDEX `fk_nm_c002_proceso_periodo_rh_b001_empleado4_idx` (`fk_rhb001_num_empleado_cierra` ASC),
  INDEX `fk_nm_c002_proceso_periodo_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridadusuario` ASC),
  CONSTRAINT `fk_nm_c002_proceso_periodo_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridadusuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_proceso_periodo_pr_tipo_nomina_periodo1`
    FOREIGN KEY (`fk_nmc001_num_tipo_nomina_periodo`)
    REFERENCES `SIACE`.`nm_c001_tipo_nomina_periodo` (`pk_num_tipo_nomina_periodo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_proceso_periodo_pr_tipo_proceso1`
    FOREIGN KEY (`fk_nmb003_num_tipo_proceso`)
    REFERENCES `SIACE`.`nm_b003_tipo_proceso` (`pk_num_tipo_proceso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 43
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c007_grado_salarial`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c007_grado_salarial` (
  `pk_num_grado` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a006_num_miscelaneo_detalle` INT(11) NOT NULL,
  `ind_grado` VARCHAR(3) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_nombre_grado` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `flag_pasos` INT(11) NOT NULL,
  `num_sueldo_minimo` DECIMAL(18,6) NOT NULL,
  `num_sueldo_maximo` DECIMAL(18,6) NOT NULL,
  `num_sueldo_promedio` DECIMAL(18,6) NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_grado`),
  INDEX `fk_rh_c007_grado_salarial_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle` ASC),
  INDEX `fk_rh_c007_grado_salarial_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c007_grado_salarial_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 26
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`nm_c003_ajust_salarial_det`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`nm_c003_ajust_salarial_det` (
  `pk_num_ajuste_salarial_det` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_nmb004_num_ajuste_salarial` INT(11) NOT NULL,
  `fk_rhc007_pk_num_grado` INT(11) NOT NULL,
  `num_sueldo_anterior` DECIMAL(18,6) NOT NULL,
  `num_porcentaje` INT(4) NULL DEFAULT NULL,
  `num_monto` DECIMAL(18,6) NULL DEFAULT NULL,
  `num_sueldo_promedio` DECIMAL(18,6) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_ajuste_salarial_det`),
  INDEX `RELACION_AJUSTESALARIAL` (`fk_nmb004_num_ajuste_salarial` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_nm_c003_ajust_salarial_det_rh_c007_grado_salarial1_idx` (`fk_rhc007_pk_num_grado` ASC),
  CONSTRAINT `RELACION_AJUSTESALARIAL`
    FOREIGN KEY (`fk_nmb004_num_ajuste_salarial`)
    REFERENCES `SIACE`.`nm_b004_ajuste_salarial` (`pk_num_ajust_salarial`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_a018_num_seguridad_usuario144`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_nm_c003_ajust_salarial_det_rh_c007_grado_salarial1_idx`
    FOREIGN KEY (`fk_rhc007_pk_num_grado`)
    REFERENCES `SIACE`.`rh_c007_grado_salarial` (`pk_num_grado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 23
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`nm_c004_interfas_cxp_det`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`nm_c004_interfas_cxp_det` (
  `pk_num_interfaz_cxp_det` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_nmb005_num_interfaz_cxp` INT(11) NOT NULL,
  `fk_prb002_num_partida_presupuestaria` INT(11) NULL DEFAULT NULL,
  `fk_000_num_cuenta_contable` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_interfaz_cxp_det`),
  INDEX `fk_pr_Interfas_Cuentas_x_Pagar_Det_pr_interfas_cuenta_x_pag_idx` (`fk_nmb005_num_interfaz_cxp` ASC),
  CONSTRAINT `fk_pr_Interfas_Cuentas_x_Pagar_Det_pr_interfas_cuenta_x_pagar1`
    FOREIGN KEY (`fk_nmb005_num_interfaz_cxp`)
    REFERENCES `SIACE`.`nm_b005_interfaz_cxp` (`pk_num_interfaz_cxp`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c027_grado_salarial_pasos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c027_grado_salarial_pasos` (
  `pk_num_grados_pasos` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhc007_num_grado` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_paso` INT(11) NOT NULL,
  `num_sueldo_minimo` DECIMAL(18,6) NOT NULL,
  `num_sueldo_maximo` DECIMAL(18,6) NOT NULL,
  `num_sueldo_promedio` DECIMAL(18,6) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_grados_pasos`),
  INDEX `fk_rhc007_num_grado` (`fk_rhc007_num_grado` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_paso` (`fk_a006_num_miscelaneo_detalle_paso` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rhc007_num_grado`
    FOREIGN KEY (`fk_rhc007_num_grado`)
    REFERENCES `SIACE`.`rh_c007_grado_salarial` (`pk_num_grado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c027_grado_salarial_pasos_ibfk_2`
    FOREIGN KEY (`fk_a006_num_miscelaneo_detalle_paso`)
    REFERENCES `SIACE`.`a006_miscelaneo_detalle` (`pk_num_miscelaneo_detalle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c027_grado_salarial_pasos_ibfk_3`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 19
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`nm_c005_ajust_salarial_det_paso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`nm_c005_ajust_salarial_det_paso` (
  `pk_num_ajuste_salarial_det_paso` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_nmb004_num_ajuste_salarial` INT(11) NOT NULL,
  `fk_rhc007_num_grado` INT(11) NOT NULL,
  `fk_rhc027_num_grados_pasos` INT(11) NOT NULL,
  `num_sueldo_anterior` DECIMAL(18,6) NOT NULL,
  `num_porcentaje` INT(4) NULL DEFAULT NULL,
  `num_monto` DECIMAL(18,6) NULL DEFAULT NULL,
  `num_sueldo_promedio` DECIMAL(18,6) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_ajuste_salarial_det_paso`),
  INDEX `fk_nmb004_num_ajuste_salarial` (`fk_nmb004_num_ajuste_salarial` ASC),
  INDEX `fk_rhc007_num_grado` (`fk_rhc007_num_grado` ASC),
  INDEX `fk_rhc027_num_grados_pasos` (`fk_rhc027_num_grados_pasos` ASC),
  INDEX `seguridad_Usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `relacion_ajuste_salarial`
    FOREIGN KEY (`fk_nmb004_num_ajuste_salarial`)
    REFERENCES `SIACE`.`nm_b004_ajuste_salarial` (`pk_num_ajust_salarial`),
  CONSTRAINT `relacion_grado_salarial`
    FOREIGN KEY (`fk_rhc007_num_grado`)
    REFERENCES `SIACE`.`rh_c007_grado_salarial` (`pk_num_grado`),
  CONSTRAINT `relacion_grado_salarial_paso`
    FOREIGN KEY (`fk_rhc027_num_grados_pasos`)
    REFERENCES `SIACE`.`rh_c027_grado_salarial_pasos` (`pk_num_grados_pasos`),
  CONSTRAINT `seguridad_Usuario`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`))
ENGINE = InnoDB
AUTO_INCREMENT = 25
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`nm_d001_concepto_perfil_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`nm_d001_concepto_perfil_detalle` (
  `fk_nmb001_num_tipo_nomina` INT(11) NOT NULL,
  `fk_nmb002_num_concepto` INT(11) NOT NULL,
  `fk_nmb003_num_tipo_proceso` INT(11) NOT NULL,
  `ind_cod_partida` VARCHAR(75) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_cod_cuenta_contable_debe` VARCHAR(75) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_cod_cuenta_contable_haber` VARCHAR(75) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_cod_cuenta_contable_debe_pub20` VARCHAR(75) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_cod_cuenta_contable_haber_pub20` VARCHAR(75) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`fk_nmb001_num_tipo_nomina`, `fk_nmb002_num_concepto`, `fk_nmb003_num_tipo_proceso`),
  INDEX `fk_pr_concepto_perfil_detalle_pr_tipo_nomina1_idx` (`fk_nmb001_num_tipo_nomina` ASC),
  INDEX `fk_pr_concepto_perfil_detalle_pr_concepto1_idx` (`fk_nmb002_num_concepto` ASC),
  INDEX `fk_pr_concepto_perfil_detalle_pr_tipo_proceso1_idx` (`fk_nmb003_num_tipo_proceso` ASC),
  INDEX `fk_nm_d001_concepto_perfil_detalle_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_nm_d001_concepto_perfil_detalle_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_concepto_perfil_detalle_pr_concepto1`
    FOREIGN KEY (`fk_nmb002_num_concepto`)
    REFERENCES `SIACE`.`nm_b002_concepto` (`pk_num_concepto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_concepto_perfil_detalle_pr_tipo_nomina1`
    FOREIGN KEY (`fk_nmb001_num_tipo_nomina`)
    REFERENCES `SIACE`.`nm_b001_tipo_nomina` (`pk_num_tipo_nomina`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_concepto_perfil_detalle_pr_tipo_proceso1`
    FOREIGN KEY (`fk_nmb003_num_tipo_proceso`)
    REFERENCES `SIACE`.`nm_b003_tipo_proceso` (`pk_num_tipo_proceso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`nm_d002_empleado_concepto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`nm_d002_empleado_concepto` (
  `pk_empleado_concepto` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_nmb002_num_concepto` INT(11) NOT NULL,
  `fec_periodo_desde` VARCHAR(8) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_periodo_hasta` VARCHAR(8) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_monto` DECIMAL(18,6) NOT NULL,
  `num_cantidad` INT(3) NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_empleado_concepto`),
  INDEX `fk_pr_empleadoconcepto_pr_concepto1_idx` (`fk_nmb002_num_concepto` ASC),
  INDEX `fk_nm_d002_empleado_concepto_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_nm_d002_empleado_concepto_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_nm_d002_empleado_concepto_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_empleadoconcepto_pr_concepto1`
    FOREIGN KEY (`fk_nmb002_num_concepto`)
    REFERENCES `SIACE`.`nm_b002_concepto` (`pk_num_concepto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`nm_d003_proceso_empleado_concepto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`nm_d003_proceso_empleado_concepto` (
  `fk_nmd002_num_empleado_concepto` INT(11) NOT NULL,
  `fk_nmb005_num_tipo_proceso` INT(11) NOT NULL,
  PRIMARY KEY (`fk_nmd002_num_empleado_concepto`, `fk_nmb005_num_tipo_proceso`),
  INDEX `fk_pr_empleado_concepto_has_pr_tipo_proceso_pr_tipo_proceso_idx` (`fk_nmb005_num_tipo_proceso` ASC),
  INDEX `fk_pr_empleado_concepto_has_pr_tipo_proceso_pr_empleado_con_idx` (`fk_nmd002_num_empleado_concepto` ASC),
  CONSTRAINT `fk_pr_empleado_concepto_has_pr_tipo_proceso_pr_empleado_conce1`
    FOREIGN KEY (`fk_nmd002_num_empleado_concepto`)
    REFERENCES `SIACE`.`nm_d002_empleado_concepto` (`pk_empleado_concepto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_empleado_concepto_has_pr_tipo_proceso_pr_tipo_proceso1`
    FOREIGN KEY (`fk_nmb005_num_tipo_proceso`)
    REFERENCES `SIACE`.`nm_b003_tipo_proceso` (`pk_num_tipo_proceso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`nm_d004_proceso_tipo_nomina`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`nm_d004_proceso_tipo_nomina` (
  `fk_nmb003_num_tipo_proceso` INT(11) NOT NULL,
  `fk_nmb001_num_tipo_nomina` INT(11) NOT NULL,
  `fk_000_num_tipo_documento` INT(11) NOT NULL,
  PRIMARY KEY (`fk_nmb003_num_tipo_proceso`, `fk_nmb001_num_tipo_nomina`, `fk_000_num_tipo_documento`),
  INDEX `fk_pr_tipo_proceso_has_pr_tipo_nomina_pr_tipo_nomina1_idx` (`fk_nmb001_num_tipo_nomina` ASC),
  INDEX `fk_pr_tipo_proceso_has_pr_tipo_nomina_pr_tipo_proceso1_idx` (`fk_nmb003_num_tipo_proceso` ASC),
  CONSTRAINT `fk_pr_tipo_proceso_has_pr_tipo_nomina_pr_tipo_nomina1`
    FOREIGN KEY (`fk_nmb001_num_tipo_nomina`)
    REFERENCES `SIACE`.`nm_b001_tipo_nomina` (`pk_num_tipo_nomina`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_tipo_proceso_has_pr_tipo_nomina_pr_tipo_proceso1`
    FOREIGN KEY (`fk_nmb003_num_tipo_proceso`)
    REFERENCES `SIACE`.`nm_b003_tipo_proceso` (`pk_num_tipo_proceso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`nm_d005_tipo_nomina_empleado_concepto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`nm_d005_tipo_nomina_empleado_concepto` (
  `pk_num_tiponomina_empleadoconcepto` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_nmc002_num_proceso_periodo` INT(11) NOT NULL,
  `fk_nmb002_num_concepto` INT(11) NOT NULL,
  `num_monto` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_cantidad` INT(3) NOT NULL DEFAULT '0',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_tiponomina_empleadoconcepto`),
  INDEX `fk_pr_tipo_nomina_empleado_concepto_pr_proceso_periodo1_idx` (`fk_nmc002_num_proceso_periodo` ASC),
  INDEX `fk_pr_tipo_nomina_empleado_concepto_pr_concepto1_idx` (`fk_nmb002_num_concepto` ASC),
  INDEX `fk_nm_d001_tipo_nomina_empleado_concepto_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_nm_d001_tipo_nomina_empleado_concepto_a018_seguridad_usu_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_nm_d001_tipo_nomina_empleado_concepto_a018_seguridad_usuar1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_tipo_nomina_empleado_concepto_pr_concepto1`
    FOREIGN KEY (`fk_nmb002_num_concepto`)
    REFERENCES `SIACE`.`nm_b002_concepto` (`pk_num_concepto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_tipo_nomina_empleado_concepto_pr_proceso_periodo1`
    FOREIGN KEY (`fk_nmc002_num_proceso_periodo`)
    REFERENCES `SIACE`.`nm_c002_proceso_periodo` (`pk_num_proceso_periodo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3406
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`nm_e001_prestaciones_sociales_calculo_retroactivo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`nm_e001_prestaciones_sociales_calculo_retroactivo` (
  `pk_num_prestaciones_sociales_calculo_retroactivo` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_nmb001_num_tipo_nomina` INT(11) NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_sueldo_base` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_total_asignaciones` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_sueldo_normal` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_alicuota_vacacional` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_alicuota_fin_anio` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_bono_especial` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_bono_fiscal` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_dias_trimestre` INT(2) NOT NULL DEFAULT '0',
  `num_dias_anual` INT(2) NULL DEFAULT NULL,
  `num_remuneracion_mensual` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_remuneracion_diaria` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_trimestral` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_anual` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `num_monto_acumulado` DECIMAL(18,6) NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridadusuario` INT(11) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_prestaciones_sociales_calculo_retroactivo`),
  UNIQUE INDEX `UNICO` (`fec_anio` ASC, `fec_mes` ASC),
  INDEX `fk_pr_prestaciones_sociales_calculo_pr_tipo_nomina1_idx` (`fk_nmb001_num_tipo_nomina` ASC),
  INDEX `fk_nm_e001_prestaciones_sociales_calculo_retroactivo_a018_s_idx` (`fk_a018_num_seguridadusuario` ASC),
  INDEX `fk_nm_e001_prestaciones_sociales_calculo_retroactivo_rh_b00_idx1` (`fk_rhb001_num_empleado` ASC),
  CONSTRAINT `fk_nm_e001_prestaciones_sociales_calculo_retroactivo_a018_seg1`
    FOREIGN KEY (`fk_a018_num_seguridadusuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_prestaciones_sociales_calculo_pr_tipo_nomina10`
    FOREIGN KEY (`fk_nmb001_num_tipo_nomina`)
    REFERENCES `SIACE`.`nm_b001_tipo_nomina` (`pk_num_tipo_nomina`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`nm_e002_prestaciones_sociales_calculo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`nm_e002_prestaciones_sociales_calculo` (
  `pk_num_prestaciones_sociales_calculo` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_nmb001_num_tipo_nomina` INT(11) NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_sueldo_base` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_total_asignaciones` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_sueldo_normal` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_alicuota_vacacional` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_alicuota_fin_anio` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_bono_especial` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_bono_fiscal` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_dias_trimestre` INT(2) NOT NULL DEFAULT '0',
  `num_dias_anual` INT(2) NULL DEFAULT NULL,
  `num_remuneracion_mensual` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_remuneracion_diaria` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_trimestral` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_anual` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `num_monto_acumulado` DECIMAL(18,6) NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_prestaciones_sociales_calculo`),
  INDEX `fk_pr_prestaciones_sociales_calculo_pr_tipo_nomina1_idx` (`fk_nmb001_num_tipo_nomina` ASC),
  INDEX `fk_nm_e002_prestaciones_sociales_calculo_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_nm_e002_prestaciones_sociales_calculo_a018_seguridad_usu_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_nm_e002_prestaciones_sociales_calculo_a018_seguridad_usuar1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_prestaciones_sociales_calculo_pr_tipo_nomina1`
    FOREIGN KEY (`fk_nmb001_num_tipo_nomina`)
    REFERENCES `SIACE`.`nm_b001_tipo_nomina` (`pk_num_tipo_nomina`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 53
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`nm_e003_prestaciones_sociales_anticipo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`nm_e003_prestaciones_sociales_anticipo` (
  `pk_num_prestaciones_sociales_anticipo` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_justificativo` INT(11) NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_anticipo` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `ind_porcentaje` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_anticipo` DATE NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `num_estatus` INT(1) NOT NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_prestaciones_sociales_anticipo`),
  UNIQUE INDEX `ind_periodo` (`fec_mes` ASC, `fec_anio` ASC, `fk_rhb001_num_empleado` ASC),
  INDEX `fk_nm_e003_prestaciones_sociales_anticipo_a018_seguridad_us_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_nm_e003_prestaciones_sociales_anticipo_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_nm_e003_prestaciones_sociales_anticipo_a006_miscelaneo_d_idx` (`fk_a006_num_miscelaneo_detalle_justificativo` ASC),
  CONSTRAINT `fk_nm_e003_prestaciones_sociales_anticipo_a018_seguridad_usua1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_b009_tipo_vehiculo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_b009_tipo_vehiculo` (
  `pk_num_tipo_vehiculo` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `fk_a006_num_clase_vehiculo` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_tipo_vehiculo`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `	fk_a006_num_clase_vehiculo` (`fk_a006_num_clase_vehiculo` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario147`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `pa_b009_tipo_vehiculo_ibfk_1`
    FOREIGN KEY (`fk_a006_num_clase_vehiculo`)
    REFERENCES `SIACE`.`a006_miscelaneo_detalle` (`pk_num_miscelaneo_detalle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_b001_vehiculo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_b001_vehiculo` (
  `pk_num_vehiculo` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_annio` YEAR NOT NULL,
  `ind_placa` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ind_serial_motor` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ind_serial_carroceria` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ind_url_foto_perfil` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `ind_url_foto_lateral_derecha` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ind_url_foto_lateral_izquierda` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ind_url_foto_frontal` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ind_url_foto_trasera` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ind_url_foto_superior` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ind_url_foto_inferior` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ind_estatus_salida_entrada` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT '0',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `ind_color` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_modelo` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fk_a006_num_marca_vehiculo` INT(11) NOT NULL,
  `fk_a006_num_clase_vehiculo` INT(11) NOT NULL,
  `fk_pab009_num_tipo_vehiculo` INT(11) NOT NULL,
  `fk_a006_num_activo` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_vehiculo`),
  INDEX `fk_pa_vehiculo_pa_marca_vehiculo1` (`fk_a006_num_marca_vehiculo` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_pa_b001_vehiculo_pa_b010_clase_vehiculo1_idx` (`fk_a006_num_clase_vehiculo` ASC),
  INDEX `fk_pa_b001_vehiculo_pa_b009_tipo_vehiculo1_idx` (`fk_pab009_num_tipo_vehiculo` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_pab009_num_tipo_vehiculo` (`fk_pab009_num_tipo_vehiculo` ASC),
  INDEX `fk_pab009_num_tipo_vehiculo_2` (`fk_pab009_num_tipo_vehiculo` ASC),
  INDEX `fk_afb001_num_activo` (`fk_a006_num_activo` ASC),
  INDEX `	fk_a006_num_marca_vehiculo` (`fk_a006_num_marca_vehiculo` ASC),
  INDEX `fk_a006_num_clase_vehiculo` (`fk_a006_num_clase_vehiculo` ASC),
  CONSTRAINT `pa_b001_vehiculo_ibfk_1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `pa_b001_vehiculo_ibfk_7`
    FOREIGN KEY (`fk_pab009_num_tipo_vehiculo`)
    REFERENCES `SIACE`.`pa_b009_tipo_vehiculo` (`pk_num_tipo_vehiculo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_b002_marca_vehiculo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_b002_marca_vehiculo` (
  `pk_num_marca` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_marca` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_marca`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario145`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_b003_tipo_mantenimiento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_b003_tipo_mantenimiento` (
  `pk_num_tipo_mantenimiento` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_tipo_mantenimiento`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario149`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_b007_pieza_vehiculo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_b007_pieza_vehiculo` (
  `pk_num_piezas` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_pab008_num_ubicacion_pieza` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_piezas`),
  INDEX `fk_pa_pieza_vehiculo_pa_ubicacion_pieza1` (`fk_pab008_num_ubicacion_pieza` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_pab008_num_ubicacion_pieza` (`fk_pab008_num_ubicacion_pieza` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario151`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_b008_ubicacion_pieza`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_b008_ubicacion_pieza` (
  `pk_num_ubicacion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_pab009_num_tipo_vehiculo` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_ubicacion`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario150`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_b010_clase_vehiculo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_b010_clase_vehiculo` (
  `pk_num_clase_vehiculo` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_clase_vehiculo`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario146`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_b011_accesorio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_b011_accesorio` (
  `pk_num_accesorio` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion_accesorio` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `ind_estatus` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  PRIMARY KEY (`pk_num_accesorio`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario152`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_b012_chofer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_b012_chofer` (
  `pk_num_chofer` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_observacion` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_chofer`),
  INDEX `fk_pa_chofer_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario153`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_b013_motivo_salida`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_b013_motivo_salida` (
  `pk_num_motivo_salida` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_num_motivo` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ind_motivo` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  PRIMARY KEY (`pk_num_motivo_salida`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario154`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_b014_tipo_salida`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_b014_tipo_salida` (
  `pk_num_tipo_sailda` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_tipo_salida` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_tipo_sailda`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario155`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_b015_estatus`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_b015_estatus` (
  `pk_num_estatus` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_estatus`),
  INDEX `fk_pa_b015_estatus_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_pa_b015_estatus_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_c001_poliza`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_c001_poliza` (
  `pk_num_poliza` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_poliza` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_recibo` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_desde` DATE NOT NULL,
  `fec_hasta` DATE NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `num_monto_pagado` DECIMAL(18,6) NOT NULL,
  `num_monto_cobertura` DECIMAL(18,6) NOT NULL,
  `fk_pab001_num_vehiculo` INT(11) NOT NULL,
  `fk_lgb022_proveedor` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_poliza`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario156`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_e001_salida_vehiculo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_e001_salida_vehiculo` (
  `pk_num_salida_vehiculo` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_hora_salida` DATETIME NOT NULL,
  `fec_hora_hasta` DATETIME NOT NULL,
  `ind_kilometraje` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ind_observacion` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_pad001_num_solicitud_vehiculo` INT(11) NOT NULL,
  `fk_pab012_num_chofer` INT(11) NOT NULL,
  `fk_pab001_num_vehiculo` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_salida_vehiculo`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario158`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_c002_accesorio_salida`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_c002_accesorio_salida` (
  `pk_num_accesorio_salida` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_pab011_num_accesorio` INT(11) NOT NULL,
  `fk_pae001_num_salida_vehiculo` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_accesorio_salida`),
  INDEX `fk_pa_accesorio_salida_pa_accesorio1` (`fk_pab011_num_accesorio` ASC),
  INDEX `fk_pa_accesorio_salida_pa_salida_vehiculo1` (`fk_pae001_num_salida_vehiculo` ASC),
  CONSTRAINT `fk_pa_accesorio_salida_pa_accesorio1`
    FOREIGN KEY (`fk_pab011_num_accesorio`)
    REFERENCES `SIACE`.`pa_b011_accesorio` (`pk_num_accesorio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pa_accesorio_salida_pa_salida_vehiculo1`
    FOREIGN KEY (`fk_pae001_num_salida_vehiculo`)
    REFERENCES `SIACE`.`pa_e001_salida_vehiculo` (`pk_num_salida_vehiculo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_c003_solucion_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_c003_solucion_detalle` (
  `pk_num_solucion_detalle` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_pae008_num_solucion` INT(11) NOT NULL,
  `fk_pae004_num_averia_vehiculo` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_solucion_detalle`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_d001_solicitud_vehiculo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_d001_solicitud_vehiculo` (
  `pk_num_solicitud` INT(11) NOT NULL AUTO_INCREMENT,
  `num_solicitud` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_requerida` DATETIME NOT NULL,
  `fec_hasta` DATETIME NOT NULL,
  `fk_rhb001_empleado_solicitante` INT(11) NOT NULL,
  `fec_preparacion` DATE NOT NULL,
  `fec_conformado` DATE NULL DEFAULT NULL,
  `fk_rhb001_empleado_conformado` INT(11) NULL DEFAULT NULL,
  `fk_rhb001_empleado_aprobado` INT(11) NULL DEFAULT NULL,
  `fec_aprobado` DATETIME NULL DEFAULT NULL,
  `fk_a006_num_estado_solicitud` INT(11) NOT NULL,
  `ind_observacion` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_motivo_rechazo` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a006_num_motivo_salida` INT(11) NOT NULL,
  `fk_a006_num_tipo_salida` INT(11) NOT NULL,
  `fk_pab001_num_vehiculo` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_solicitud`),
  INDEX `fk_pa_solicitud_vehiculo_pa_motivo_salida` (`fk_a006_num_motivo_salida` ASC),
  INDEX `fk_pa_solicitud_vehiculo_pa_tipo_salida1` (`fk_a006_num_tipo_salida` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_pa_d001_solicitud_vehiculo_pa_b001_vehiculo1_idx` (`fk_pab001_num_vehiculo` ASC),
  INDEX `fk_a006_num_estado_solicitud` (`fk_a006_num_estado_solicitud` ASC),
  INDEX `fk_a006_num_motivo_salida` (`fk_a006_num_motivo_salida` ASC),
  INDEX `fk_a006_num_tipo_salida` (`fk_a006_num_tipo_salida` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario157`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `pa_d001_solicitud_vehiculo_ibfk_4`
    FOREIGN KEY (`fk_pab001_num_vehiculo`)
    REFERENCES `SIACE`.`pa_b001_vehiculo` (`pk_num_vehiculo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_e002_mantenimiento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_e002_mantenimiento` (
  `pk_num_mantenimiento` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_en_ocacion` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `fec_mant` DATE NOT NULL,
  `fec_proximo_mantenimiento` DATE NOT NULL,
  `ind_num_siguiente_kilometraje` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ind_observacion` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `num_monto` DECIMAL(18,6) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a006_num_tipo_mantenimiento` INT(11) NOT NULL,
  `fk_pae004_num_averia_vehiculo` INT(11) NOT NULL,
  `fk_pab001_num_vehiculo` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_mantenimiento`),
  INDEX `fk_pab001_num_vehiculo` (`fk_pab001_num_vehiculo` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_a006_num_tipo_mantenimiento` (`fk_a006_num_tipo_mantenimiento` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario160`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pab001_num_vehiculo`
    FOREIGN KEY (`fk_pab001_num_vehiculo`)
    REFERENCES `SIACE`.`pa_b001_vehiculo` (`pk_num_vehiculo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_e003_entrada_vehiculo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_e003_entrada_vehiculo` (
  `pk_num_entrada` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_entrada` INT(6) NOT NULL,
  `fec_hora_entrada` DATE NOT NULL,
  `ind_kilometraje_entrada` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ind_observacion_entrada` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_pae001_num_salida_vehiculo` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_entrada`),
  INDEX `fk_pa_entrada_vehiculo_pa_salida_vehiculo1` (`fk_pae001_num_salida_vehiculo` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario161`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pa_entrada_vehiculo_pa_salida_vehiculo1`
    FOREIGN KEY (`fk_pae001_num_salida_vehiculo`)
    REFERENCES `SIACE`.`pa_e001_salida_vehiculo` (`pk_num_salida_vehiculo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_e004_averia_vehiculo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_e004_averia_vehiculo` (
  `pk_num_averia_vehiculo` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_observacion_averia` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `fec_registro` DATETIME NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `ind_estado` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ind_url` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fk_pab001_num_vehiculo` INT(11) NOT NULL,
  `fk_pab007_num_piezas` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_averia_vehiculo`),
  INDEX `fk_pa_dannio_vehiculo_pa_vehiculo1` (`fk_pab001_num_vehiculo` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_pa_e004_dannio_vehiculo_pa_b007_pieza_vehiculo1_idx` (`fk_pab007_num_piezas` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario_22`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pa_dannio_vehiculo_pa_vehiculo1`
    FOREIGN KEY (`fk_pab001_num_vehiculo`)
    REFERENCES `SIACE`.`pa_b001_vehiculo` (`pk_num_vehiculo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pa_e004_dannio_vehiculo_pa_b007_pieza_vehiculo1`
    FOREIGN KEY (`fk_pab007_num_piezas`)
    REFERENCES `SIACE`.`pa_b007_pieza_vehiculo` (`pk_num_piezas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_e006_entrada_taller`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_e006_entrada_taller` (
  `pk_num_entrada_taller` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_hora` DATETIME NOT NULL,
  `ind_motivo` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ind_kilometraje` VARCHAR(25) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ind_cobertura` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT '0' COMMENT 'Cobertura: Contraloria 1, seguro 0',
  `ind_observacion` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_pab001_num_vehiculo` INT(11) NOT NULL,
  `fk_pab012_num_chofer` INT(11) NOT NULL,
  `fk_lgb022_num_proveedor` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_entrada_taller`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario162`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_e007_salida_taller`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_e007_salida_taller` (
  `pk_num_salida_taller` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_hora` DATETIME NOT NULL,
  `ind_motivo` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ind_kilometraje` VARCHAR(25) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ind_condiciones` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ind_observacion` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_pae006_num_entrada_taller` INT(11) NOT NULL,
  `num_monto` DECIMAL(18,6) NOT NULL,
  PRIMARY KEY (`pk_num_salida_taller`),
  INDEX `fk_pa_salida_taller_pa_entrada_taller1` (`fk_pae006_num_entrada_taller` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario163`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pa_salida_taller_pa_entrada_taller1`
    FOREIGN KEY (`fk_pae006_num_entrada_taller`)
    REFERENCES `SIACE`.`pa_e006_entrada_taller` (`pk_num_entrada_taller`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pa_e008_solucion_averia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pa_e008_solucion_averia` (
  `pk_num_solucion` INT(11) NOT NULL AUTO_INCREMENT,
  `num_monto` DECIMAL(16,8) NOT NULL,
  `fec_solucion` DATETIME NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `ind_observacion` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_pae004_num_averia_vehiculo` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_solucion`),
  INDEX `fk_pa_e008_solucion_dannio_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_pa_e008_solucion_averia_pa_e004_averia_vehiculo1_idx` (`fk_pae004_num_averia_vehiculo` ASC),
  CONSTRAINT `fk_pa_e008_solucion_averia_pa_e004_averia_vehiculo1`
    FOREIGN KEY (`fk_pae004_num_averia_vehiculo`)
    REFERENCES `SIACE`.`pa_e004_averia_vehiculo` (`pk_num_averia_vehiculo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pa_e008_solucion_dannio_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pf_b002_proceso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pf_b002_proceso` (
  `pk_num_proceso` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id identificador del registro',
  `cod_proceso` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Código de proceso',
  `txt_descripcion_proceso` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Nombre ó descripción del proceso',
  `num_estatus` INT(1) NOT NULL COMMENT 'Estatus con respecto al tipo de proceso: Activo(1), Inactivo(0)',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Última modificación del registro',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Ultimo usuario que modifica el registro',
  PRIMARY KEY (`pk_num_proceso`),
  UNIQUE INDEX `cod_proceso_UNIQUE` (`cod_proceso` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_pers_registra`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pf_b001_planificacion_fiscal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pf_b001_planificacion_fiscal` (
  `pk_num_planificacion` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id que identifica al registro',
  `num_planificacion_padre` INT(11) NULL DEFAULT '0' COMMENT 'Id padre',
  `fk_b002_num_proceso` INT(11) NOT NULL COMMENT 'Id del Proceso: Actuación fiscal, Valoración jurídica, Potestad investigativa o Determinación de responsabilidades',
  `cod_planificacion` VARCHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Código de las planificaciones: Siglas de la Dirección + siglas dependencia que procesa + correlativo por dependencia que procesa +año fiscal ',
  `fk_a001_num_organismo` INT(11) NOT NULL COMMENT 'Id del organismo que ejecuta la planificación',
  `fk_a004_num_dependencia` INT(11) NOT NULL COMMENT 'id de la dependencia que prepara y ejecuta la planificación',
  `fk_a041_num_persona_ente` INT(11) NULL DEFAULT NULL COMMENT 'Id de la relación persona representante - ente a objeto de la actuación',
  `fk_a006_num_miscdet_tipoactuacion` INT(11) NOT NULL COMMENT 'Id del tipo de actuación viene de miscelanio detalle',
  `fk_a006_num_miscdet_origenactuacion` INT(11) NOT NULL COMMENT 'Id del origen de la actuación viene de miscelanio detalle',
  `fk_a023_num_centro_costo` INT(11) NOT NULL COMMENT 'id de la dependencia de centro de costo',
  `ind_objetivo` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Indica el propósito o finalidad de la actuación',
  `ind_alcance` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Indica la delimitación de responsabilidad del auditor y del órgano en cuanto al alcance según el propósito de la actuación',
  `ind_observacion` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Para ingresar ulguna observación con respecto a la planificación',
  `fk_rhb001_num_empleado_preparado` INT(11) NOT NULL COMMENT 'id de la persona que preparó la planificación',
  `fec_preparacion` DATETIME NOT NULL COMMENT 'Fecha de preparación',
  `fk_rhb001_num_empleado_revisado` INT(11) NULL DEFAULT NULL COMMENT 'id de la persona que revisó la planificación',
  `fec_revision` DATETIME NULL DEFAULT NULL COMMENT 'Fecha de revisión',
  `fk_rhb001_num_empleado_aprobado` INT(11) NULL DEFAULT NULL COMMENT 'id de la persona que aprobó la planificación',
  `fec_aprobado` DATETIME NULL DEFAULT NULL COMMENT 'Fecha de aprobación',
  `ind_estado_planificacion` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'PR' COMMENT 'Indica los diferentes estados de la planificación en curso (preparado:PR, revisado:RV, aprobado:AP, completado:CO, Cerrado:CE)',
  `fec_inicio` DATE NOT NULL COMMENT 'Fecha de inicio de la planificación',
  `num_dias_duracion` INT(4) NULL DEFAULT '0' COMMENT 'Cantidad en días para la culminación de la planificación',
  `fec_completada` DATE NULL DEFAULT NULL COMMENT 'Fecha en la cual se completa una planificación (Actividades terminadas)',
  `fec_termino` DATE NULL DEFAULT NULL COMMENT 'Fecha de término ó culminación',
  `fec_termino_real` DATE NULL DEFAULT NULL COMMENT 'Fecha de término ó culminación real',
  `num_dias_adelanto` INT(4) NULL DEFAULT '0' COMMENT 'Cantidad general de días de adelantado en el término de la planificación',
  `num_dias_prorroga` INT(4) NULL DEFAULT '0' COMMENT 'Cantidad en días de prorroga para la culminación de la planificación',
  `ind_motivo_anulacion` TEXT CHARACTER SET 'dec8' NULL DEFAULT NULL COMMENT 'Nota de justificación de la anulación de un proceso de planificación',
  `fk_a018_num_seg_usuario_reg` INT(11) NOT NULL COMMENT 'Id de la persona que ingresa',
  `fec_registro_planificacion` DATETIME NOT NULL COMMENT 'Fecha de creación del registro de la planificación',
  `fk_a018_num_seg_usuario_mod` INT(11) NOT NULL COMMENT 'Id de la última persona que modifica el registro',
  `fec_ultima_modific_planificacion` DATETIME NOT NULL COMMENT 'Fecha de la última actualización',
  PRIMARY KEY (`pk_num_planificacion`),
  INDEX `fk_b005_num_proceso` (`fk_b002_num_proceso` ASC),
  INDEX `fk_a001_num_organismo` (`fk_a001_num_organismo` ASC),
  INDEX `fk_a004_num_dependencia` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_a006_num_miscdet_tipoactuacion` (`fk_a006_num_miscdet_tipoactuacion` ASC),
  INDEX `fk_a006_num_miscdet_origenactuacion` (`fk_a006_num_miscdet_origenactuacion` ASC),
  INDEX `fk_a023_num_centro_costo` (`fk_a023_num_centro_costo` ASC),
  INDEX `fk_rhb001_num_empleado_preparado` (`fk_rhb001_num_empleado_preparado` ASC),
  INDEX `fk_rhb001_num_empleado_revisado` (`fk_rhb001_num_empleado_revisado` ASC),
  INDEX `fk_rhb001_num_empleado_aprobado` (`fk_rhb001_num_empleado_aprobado` ASC),
  INDEX `fk_a018_num_seg_usuario_reg` (`fk_a018_num_seg_usuario_reg` ASC),
  INDEX `fk_a018_num_seg_usuario_mod` (`fk_a018_num_seg_usuario_mod` ASC),
  INDEX `fk_a041_num_persona_ente` (`fk_a041_num_persona_ente` ASC),
  CONSTRAINT `fk_a001_num_organismo`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a004_num_dependencia`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a006_num_miscdet_origenactuacion`
    FOREIGN KEY (`fk_a006_num_miscdet_origenactuacion`)
    REFERENCES `SIACE`.`a006_miscelaneo_detalle` (`pk_num_miscelaneo_detalle`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a006_num_miscdet_tipoactuacion`
    FOREIGN KEY (`fk_a006_num_miscdet_tipoactuacion`)
    REFERENCES `SIACE`.`a006_miscelaneo_detalle` (`pk_num_miscelaneo_detalle`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a018_num_seg_usuario_mod`
    FOREIGN KEY (`fk_a018_num_seg_usuario_mod`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a018_num_seg_usuario_reg`
    FOREIGN KEY (`fk_a018_num_seg_usuario_reg`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a023_num_centro_costo`
    FOREIGN KEY (`fk_a023_num_centro_costo`)
    REFERENCES `SIACE`.`a023_centro_costo` (`pk_num_centro_costo`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a041_num_persona_ente`
    FOREIGN KEY (`fk_a041_num_persona_ente`)
    REFERENCES `SIACE`.`a041_persona_ente` (`pk_num_persona_ente`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_b005_num_proceso`
    FOREIGN KEY (`fk_b002_num_proceso`)
    REFERENCES `SIACE`.`pf_b002_proceso` (`pk_num_proceso`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rhb001_num_empleado_aprobado`
    FOREIGN KEY (`fk_rhb001_num_empleado_aprobado`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`pk_num_empleado`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rhb001_num_empleado_preparado`
    FOREIGN KEY (`fk_rhb001_num_empleado_preparado`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`pk_num_empleado`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rhb001_num_empleado_revisado`
    FOREIGN KEY (`fk_rhb001_num_empleado_revisado`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`pk_num_empleado`)
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Tabla para registrar los procesos de Planificación de: Actua /* comment truncated */ /*ción fiscal, Valoración jurídica, Potestad investigativa y Determinación de responsabilidades*/';


-- -----------------------------------------------------
-- Table `SIACE`.`pf_b003_fase`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pf_b003_fase` (
  `pk_num_fase` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id del registro',
  `fk_pfb002_num_proceso` INT(11) NOT NULL COMMENT 'Id del proceso fiscal',
  `cod_fase` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Código de la Fase: código del maestro de proceso y número secuencial de fase por tipo de proceso',
  `txt_descripcion_fase` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Nombre ó descripción de la fase',
  `num_estatus` INT(1) NOT NULL COMMENT 'Estatus de la fase: Activo:1, Inactivo:0\n',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Ultima fecha de modificación',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'id del último usuario que modifica ',
  PRIMARY KEY (`pk_num_fase`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_num_proceso_idx` (`fk_pfb002_num_proceso` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 28
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Tabla para las diferentes fases fiscales';


-- -----------------------------------------------------
-- Table `SIACE`.`pf_b004_actividad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pf_b004_actividad` (
  `pk_num_actividad` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id identificador de registro',
  `fk_pfb003_num_fase` INT(11) NOT NULL COMMENT 'Id de la fase',
  `cod_actividad` VARCHAR(6) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Código de la actividad: código de la fase y número de secuencia de la actividad por fase',
  `txt_descripcion_actividad` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Nombre o descripción de la actividad',
  `txt_comentarios_actividad` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Algún comentario respecto a la actividad',
  `num_duracion_actividad` INT(4) NOT NULL COMMENT 'Cantidad de días por defecto que tendrá la actividad para su ejecución',
  `ind_auto_archivo` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'N' COMMENT 'Indica si el proceso de valoración en adelante tendrá o nó auto de archivo',
  `ind_afecto_plan` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'N' COMMENT 'Indica si la activida afecta o nó el lapso de preceso de la planificación',
  `num_estatus` INT(1) NOT NULL COMMENT 'Estatus con respecto a la actividad Activo(1), Inactivo(0)',
  `fec_ultima_modificacion` DATETIME NOT NULL COMMENT 'Fecha de la última afectación del registro',
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL COMMENT 'Indica al usuario que afectó el regsitro',
  PRIMARY KEY (`pk_num_actividad`),
  INDEX `fk_pf_b002_actividad_pf_b004_fase1_idx` (`fk_pfb003_num_fase` ASC),
  INDEX `fk_usuario_regmod_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_num_fase`
    FOREIGN KEY (`fk_pfb003_num_fase`)
    REFERENCES `SIACE`.`pf_b003_fase` (`pk_num_fase`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_reg`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 109
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Tabla para las diferentes actividades por fase fiscal';


-- -----------------------------------------------------
-- Table `SIACE`.`pf_c001_actividad_planific`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pf_c001_actividad_planific` (
  `pk_num_actividad_planific` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id identificador de registro',
  `fk_pfb001_num_planificacion` INT(11) NOT NULL COMMENT 'Id de la planificación fiscal',
  `fk_pfb004_num_actividad` INT(11) NOT NULL COMMENT 'Id de la actividad que se agrega a la planificación',
  `num_secuencia_actividad` INT(4) NOT NULL COMMENT 'indica la secuencia de ejecución de actividades por planificación',
  `fec_inicio_actividad` DATE NOT NULL COMMENT 'Fecha de inicio de la actividad',
  `fec_inicio_real_actividad` DATE NOT NULL COMMENT 'Fecha de inicio real de la actividad',
  `fec_culmina_actividad` DATE NOT NULL COMMENT 'Fecha de culminación de la actividad',
  `fec_culmina_real_actividad` DATE NOT NULL COMMENT 'Fecha de culminación real de la actividad',
  `num_dias_duracion_actividad` INT(4) NOT NULL COMMENT 'Cantidad de dias de duración para la ejecución de la actividad',
  `num_dias_prorroga_actividad` INT(4) NULL DEFAULT NULL COMMENT 'Cantidad de dias de prorroga para la ejecución de la actividad',
  `num_dias_adelanto_actividad` INT(4) NULL DEFAULT NULL COMMENT 'Cantidad de dias en la cual se adelantó la terminación de la actividad',
  `fec_termino_cierre_actividad` DATE NULL DEFAULT NULL COMMENT 'Fecha en la cual se cierra la actividad por cualquier circunstancia',
  `fec_registro_cierre_actividad` DATE NULL DEFAULT NULL COMMENT 'Fecha de registro de cierre de la actividad por cualquier sircunstancia',
  `num_dias_cierre_actividad` INT(4) NULL DEFAULT NULL COMMENT 'Cantidad de dias al cierre de la actividad',
  `ind_estado_actividad` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'PR' COMMENT 'Indica el estado en la cual se encuentra la actividad: PR, EJ, PE, TE, CE, AN',
  `ind_observacion_actividad` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Indica alguna observación con respecto a la ejecución de la actividad',
  `fk_a018_num_seg_usuario_rap` INT(11) NOT NULL COMMENT 'Id de la presona que agrega la actividad a la planificación',
  `fec_registro_ap` DATETIME NOT NULL COMMENT 'Fecha de ingreso de la actividad a la planificación',
  `fk_a018_num_seg_usuaro_map` INT(11) NOT NULL COMMENT 'Id de la última presona que modifica el registro',
  `fec_modifica_ap` DATETIME NOT NULL COMMENT 'Fecha última modificación del registro',
  PRIMARY KEY (`pk_num_actividad_planific`),
  INDEX `fk_pfb001_num_planificacion` (`fk_pfb001_num_planificacion` ASC),
  INDEX `fk_pfb004_num_actividad` (`fk_pfb004_num_actividad` ASC),
  INDEX `fk_a018_num_seg_usuario_rap` (`fk_a018_num_seg_usuario_rap` ASC),
  INDEX `pk_num_atividad_idx` (`fk_pfb004_num_actividad` ASC),
  INDEX `pk_num_planificacion_idx` (`fk_pfb001_num_planificacion` ASC),
  CONSTRAINT `pk_num_atividad`
    FOREIGN KEY (`fk_pfb004_num_actividad`)
    REFERENCES `SIACE`.`pf_b004_actividad` (`pk_num_actividad`)
    ON UPDATE NO ACTION,
  CONSTRAINT `pk_num_planificacion`
    FOREIGN KEY (`fk_pfb001_num_planificacion`)
    REFERENCES `SIACE`.`pf_b001_planificacion_fiscal` (`pk_num_planificacion`)
    ON UPDATE NO ACTION,
  CONSTRAINT `pk_num_seguridad_usuario`
    FOREIGN KEY (`fk_a018_num_seg_usuario_rap`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 21
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Tabla para agregar las actividades correspondientes a una Pl /* comment truncated */ /*anificación de: Actuación fiscal, Valoración jurídica, Potestad investigativa ó Determinación de responsabilidades*/';


-- -----------------------------------------------------
-- Table `SIACE`.`pf_c002_auditor_planificacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pf_c002_auditor_planificacion` (
  `pk_num_auditor_planificacion` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id del registro',
  `fk_pfb001_num_planificacion` INT(11) NOT NULL COMMENT 'Id de la planificación',
  `fk_rhb001_num_empleado` INT(11) NOT NULL COMMENT 'Id del funcionario auditor responsable',
  `num_flag_coordinador` INT(1) NOT NULL COMMENT 'Bandera que indica si el responsable es el coordinador de la planificación Si:1 ó No:0',
  `fk_a018_num_seg_user_regauditor` INT(11) NOT NULL COMMENT 'Id num_seguridad_usuario de la persona que registra',
  `fec_registro_auditor` DATETIME NOT NULL COMMENT 'Fecha de registro',
  PRIMARY KEY (`pk_num_auditor_planificacion`),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a018_num_seg_user_regauditor_idx` (`fk_a018_num_seg_user_regauditor` ASC),
  INDEX `fk_pfb001_num_planificacion_idx` (`fk_pfb001_num_planificacion` ASC),
  CONSTRAINT `fk_a018_num_seg_user_regauditor`
    FOREIGN KEY (`fk_a018_num_seg_user_regauditor`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pfb001_num_planificacion`
    FOREIGN KEY (`fk_pfb001_num_planificacion`)
    REFERENCES `SIACE`.`pf_b001_planificacion_fiscal` (`pk_num_planificacion`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rhb001_num_empleado`
    FOREIGN KEY (`fk_rhb001_num_empleado`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`pk_num_empleado`)
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Contendrá a los funcionarios internos responsables de la eje /* comment truncated */ /*cución de las planificaciones*/';


-- -----------------------------------------------------
-- Table `SIACE`.`pf_c003_prorroga_actividad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pf_c003_prorroga_actividad` (
  `pk_num_prorroga_actividad` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id de la prÃ³rroga',
  `fk_pfc001_num_actividad_planific` INT(11) NOT NULL COMMENT 'Id del registro relación actividad – aplicacion',
  `num_dias_prorroga` INT(4) NOT NULL COMMENT 'Cantidad de días de prórroga',
  `ind_motivo` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL COMMENT 'Motivo por cual se establece la prórroga',
  `fk_rhb001_num_empleado_prepapor` INT(11) NOT NULL COMMENT 'id de la persona que prepara la prórroga',
  `fec_prorroga_preparado` DATETIME NOT NULL COMMENT 'Fecha de preparación',
  `fk_rhb001_num_empleado_revispor` INT(11) NULL DEFAULT NULL COMMENT 'Id de la persona que revisa la prórroga',
  `fec_prorroga_revisado` DATETIME NULL DEFAULT NULL COMMENT 'fec_prorroga_revisado',
  `fk_rhb001_num_empleado_aprobpor` INT(11) NULL DEFAULT NULL COMMENT 'Id de la persona que apueba la prórroga',
  `fec_prorroga_aprobado` DATETIME NULL DEFAULT NULL COMMENT 'Fecha en la cual se aprueba la prórroga',
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT 'PR' COMMENT 'Indica el estado de la prórroga',
  `fk_a018_num_seg_user_regpro` INT(11) NOT NULL COMMENT 'Id de la persona que ingresó',
  `fec_registro_prorroga` DATETIME NULL DEFAULT NULL COMMENT 'Fecha de registro',
  `fk_a018_num_seg_user_actpro` INT(11) NOT NULL COMMENT 'Id de la persona que actualiza la prórroga',
  `fec_actualiza_prorroga` DATETIME NOT NULL COMMENT 'Fecha de actualización',
  PRIMARY KEY (`pk_num_prorroga_actividad`),
  INDEX `fk_pfc001_num_actividad_planificac_idx` (`fk_pfc001_num_actividad_planific` ASC),
  INDEX `fk_rhb001_num_empleado_preparado_idx` (`fk_rhb001_num_empleado_prepapor` ASC),
  INDEX `fk_revisado_por_idx` (`fk_rhb001_num_empleado_revispor` ASC),
  INDEX `fk_aprobado_por_idx` (`fk_rhb001_num_empleado_aprobpor` ASC),
  INDEX `fk_registrado_por_idx` (`fk_a018_num_seg_user_regpro` ASC),
  INDEX `fk_actualizado_por_idx` (`fk_a018_num_seg_user_actpro` ASC),
  CONSTRAINT `fk_actualizado_por`
    FOREIGN KEY (`fk_a018_num_seg_user_actpro`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_aprobado_por`
    FOREIGN KEY (`fk_rhb001_num_empleado_aprobpor`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`pk_num_empleado`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_actividad_planific`
    FOREIGN KEY (`fk_pfc001_num_actividad_planific`)
    REFERENCES `SIACE`.`pf_c001_actividad_planific` (`pk_num_actividad_planific`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_preparador`
    FOREIGN KEY (`fk_rhb001_num_empleado_prepapor`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`pk_num_empleado`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_registrado_por`
    FOREIGN KEY (`fk_a018_num_seg_user_regpro`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_revisado_por`
    FOREIGN KEY (`fk_rhb001_num_empleado_revispor`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`pk_num_empleado`)
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci
COMMENT = 'Tabla para el registro de prorrogas de actividades en proces /* comment truncated */ /*o*/';


-- -----------------------------------------------------
-- Table `SIACE`.`pf_c004_documento_actividad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pf_c004_documento_actividad` (
  `pk_num_documento_actividad` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id identificador del registro',
  `fk_pfc001_num_actividad_planific` INT(11) NOT NULL COMMENT 'Id del registro relación actividad–planificación',
  `fec_documento` DATE NOT NULL COMMENT 'Fecha del documento',
  `ind_nro_documento` VARCHAR(16) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Número de identificación del documento',
  `ind_desc_documento` VARCHAR(128) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'Nombre o descripción del documento',
  `ind_ruta_documento` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL COMMENT 'Ruta de la carpeta donde se aloja el documento pdf (opcional)',
  `fk_a018_num_seg_user_regdoc` INT(11) NOT NULL COMMENT 'Id num_seguridad_usuario de la persona que registra',
  `fec_registro_documento` DATETIME NOT NULL COMMENT 'Fecha de registro',
  `fk_a018_num_seg_user_actdoc` INT(11) NOT NULL COMMENT 'Id num_seguridad_usuario de la persona que actualiza el registro',
  `fec_actualiza_regdoc` DATETIME NOT NULL COMMENT 'Fecha de la última actualización del resgistro',
  PRIMARY KEY (`pk_num_documento_actividad`),
  INDEX `pk_num_actividad_planificac_idx` (`fk_pfc001_num_actividad_planific` ASC),
  INDEX `fk_a018_num_seg_user_regdoc_idx` (`fk_a018_num_seg_user_regdoc` ASC),
  INDEX `fk_a018_num_seg_user_actdoc_idx` (`fk_a018_num_seg_user_actdoc` ASC),
  CONSTRAINT `fk_a018_num_seg_user_actdoc`
    FOREIGN KEY (`fk_a018_num_seg_user_actdoc`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_a018_num_seg_user_regdoc`
    FOREIGN KEY (`fk_a018_num_seg_user_regdoc`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_num_actividad_planificacion`
    FOREIGN KEY (`fk_pfc001_num_actividad_planific`)
    REFERENCES `SIACE`.`pf_c001_actividad_planific` (`pk_num_actividad_planific`)
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Tabla para registrar los documentos correspondientes a una a /* comment truncated */ /*ctividad en proceso*/';


-- -----------------------------------------------------
-- Table `SIACE`.`pf_c005_actividad_tipoactuac`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pf_c005_actividad_tipoactuac` (
  `pk_num_actividad_tipoactuac` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id del registro',
  `fk_pfb002_num_proceso_tipo` INT(11) NOT NULL COMMENT 'Id del tipo de proceso de planificación',
  `fk_a006_num_miscelaneodet` INT(11) NOT NULL COMMENT 'id del tipo de actuación de detalle de misceláneos',
  `fk_pfb004_num_actividad` INT(11) NOT NULL COMMENT 'Id de la actividad',
  `fk_a018_num_seg_user_reg` INT(11) NOT NULL COMMENT 'Id de la persona que registra',
  `fec_registro` DATETIME NOT NULL COMMENT 'Fecha de la persona que registra',
  PRIMARY KEY (`pk_num_actividad_tipoactuac`),
  INDEX `fk_a018_num_seg_user_regtipo_idx` (`fk_a018_num_seg_user_reg` ASC),
  INDEX `fk_a006_num_miscelaneodet_idx` (`fk_a006_num_miscelaneodet` ASC),
  INDEX `fk_pfb002_num_actividad_idx` (`fk_pfb004_num_actividad` ASC),
  CONSTRAINT `fk_detalle_miscelaneo`
    FOREIGN KEY (`fk_a006_num_miscelaneodet`)
    REFERENCES `SIACE`.`a006_miscelaneo_detalle` (`pk_num_miscelaneo_detalle`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_actividad`
    FOREIGN KEY (`fk_pfb004_num_actividad`)
    REFERENCES `SIACE`.`pf_b004_actividad` (`pk_num_actividad`)
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 101
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'Tabla para agregar actividades a un tipo de actuación fiscal';


-- -----------------------------------------------------
-- Table `SIACE`.`pr_b001_tipo_cuenta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pr_b001_tipo_cuenta` (
  `pk_num_tipo_cuenta` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `cod_tipo_cuenta` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_tipo_cuenta`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario172`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pr_b003_antepresupuesto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pr_b003_antepresupuesto` (
  `pk_num_antepresupuesto` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado_creado_por` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_aprobado_por` INT(11) NULL DEFAULT NULL,
  `cod_antepresupuesto` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fec_antepresupuesto` DATE NOT NULL,
  `fec_inicio` DATE NOT NULL,
  `dec_fin` DATE NOT NULL,
  `ind_numero_gaceta` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_gaceta` DATE NOT NULL,
  `ind_numero_decreto` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_decreto` DATE NOT NULL,
  `fec_creado` DATE NOT NULL,
  `fec_aprobado` DATETIME NULL DEFAULT NULL,
  `num_monto_generado` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_presupuestado` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `fec_generado` DATETIME NULL DEFAULT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `ind_tipo_presupuesto` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_antepresupuesto`),
  UNIQUE INDEX `cod_ante_presupuesto_UNIQUE` (`cod_antepresupuesto` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario174`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 20
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pr_b004_presupuesto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pr_b004_presupuesto` (
  `pk_num_presupuesto` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_empleado_creado` INT(11) NOT NULL,
  `fk_rhb001_empleado_aprobado` INT(11) NOT NULL,
  `fk_a018_seguridad_usuario` INT(11) NOT NULL,
  `ind_cod_presupuesto` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fec_presupuesto` DATE NOT NULL,
  `fec_inicio` DATE NOT NULL,
  `fec_fin` DATE NOT NULL,
  `ind_numero_gaceta` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_gaceta` DATE NOT NULL,
  `ind_numero_decreto` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fecha_decreto` DATE NOT NULL,
  `num_monto_aprobado` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_ajustado` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'PR',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_prb005_num_proyecto` INT(11) NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `ind_tipo_presupuesto` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_presupuesto`),
  UNIQUE INDEX `cod_presupuesto_UNIQUE` (`ind_cod_presupuesto` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_pr_b005_proyecto_proyecto` (`fk_prb005_num_proyecto` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario176`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 18
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pr_b005_proyecto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pr_b005_proyecto` (
  `pk_num_proyecto` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_cod_proyecto` VARCHAR(3) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_proyecto`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario175`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pr_b006_aespecifica`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pr_b006_aespecifica` (
  `pk_num_aespecifica` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_cod_aespecifica` VARCHAR(3) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_aespecifica`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario177`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pr_c001_antepresupuesto_det`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pr_c001_antepresupuesto_det` (
  `pk_num_antepresupuesto_det` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_prb003_num_antepresupuesto` INT(11) NOT NULL,
  `fk_prb002_num_partida_presupuestaria` INT(11) NOT NULL,
  `num_monto_presupuestado` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_aprobado` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fk_pr_b006_aespecifica` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_antepresupuesto_det`),
  INDEX `fk_pv_ante_presupuesto_det_pv_ante_presupuesto1_idx` (`fk_prb003_num_antepresupuesto` ASC),
  INDEX `fk_pv_ante_presupuesto_det_pv_partida1_idx` (`fk_prb002_num_partida_presupuestaria` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario178`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pv_ante_presupuesto_det_pv_ante_presupuesto1`
    FOREIGN KEY (`fk_prb003_num_antepresupuesto`)
    REFERENCES `SIACE`.`pr_b003_antepresupuesto` (`pk_num_antepresupuesto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pv_ante_presupuesto_det_pv_partida1`
    FOREIGN KEY (`fk_prb002_num_partida_presupuestaria`)
    REFERENCES `SIACE`.`pr_b002_partida_presupuestaria` (`pk_num_partida_presupuestaria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 522
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pr_c002_presupuesto_det`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pr_c002_presupuesto_det` (
  `pk_num_presupuesto_det` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_prb004_num_presupuesto` INT(11) NOT NULL,
  `fk_prb002_num_partida_presupuestaria` INT(11) NOT NULL,
  `num_flags_anexa` INT(1) NULL DEFAULT NULL,
  `num_flags_reformulacion` INT(1) NULL DEFAULT NULL,
  `num_monto_aprobado` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_ajustado` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_compromiso` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_causado` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_pagado` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_incremento` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_disminucion` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_credito` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `fk_pr006_num_aespecifica` INT(11) NULL DEFAULT NULL,
  `ind_numeral` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_presupuesto_det`),
  INDEX `fk_pv_presupuesto_det_pv_presupuesto1_idx` (`fk_prb004_num_presupuesto` ASC),
  INDEX `fk_pv_presupuesto_det_pv_partida1_idx` (`fk_prb002_num_partida_presupuestaria` ASC),
  INDEX `fk_pr_b006_aespecifica_idx` (`fk_pr006_num_aespecifica` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_pr006_num_aespecifica` (`fk_pr006_num_aespecifica` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario179`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pv_presupuesto_det_pv_partida1`
    FOREIGN KEY (`fk_prb002_num_partida_presupuestaria`)
    REFERENCES `SIACE`.`pr_b002_partida_presupuestaria` (`pk_num_partida_presupuestaria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pv_presupuesto_det_pv_presupuesto1`
    FOREIGN KEY (`fk_prb004_num_presupuesto`)
    REFERENCES `SIACE`.`pr_b004_presupuesto` (`pk_num_presupuesto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 124
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pr_d001_ejecucion_presupuestaria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pr_d001_ejecucion_presupuestaria` (
  `pk_num_ejecucion_presupuestaria` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_prc002_num_presupuesto_det` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_disponibilidad_presupuestaria_inicial` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_disponibilidad_financiera_inicial` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_incremento` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_disminucion` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_credito` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_presupuesto_ajustado` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_compromiso` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_causado` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_pagado` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_disponibilidad_presupuestaria` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_disponibilidad_financiera` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_variacion` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_ejecucion_presupuestaria`),
  INDEX `fk_pv_ejecucion_presupuestaria_pv_presupuesto_det1_idx` (`fk_prc002_num_presupuesto_det` ASC),
  CONSTRAINT `fk_pv_ejecucion_presupuestaria_pv_presupuesto_det1`
    FOREIGN KEY (`fk_prc002_num_presupuesto_det`)
    REFERENCES `SIACE`.`pr_c002_presupuesto_det` (`pk_num_presupuesto_det`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 128
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pr_d002_ajuste_presupuestario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pr_d002_ajuste_presupuestario` (
  `pk_num_ajuste_presupuestario` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a006_num_miscelaneo_det_tipo_ajuste` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_creado` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_aprobado` INT(11) NULL DEFAULT NULL,
  `ind_cod_ajuste_presupuestario` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ajuste` DATE NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_numero_gaceta` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_gaceta` DATETIME NOT NULL,
  `ind_numero_resolucion` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_resolucion` DATE NOT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_total_ajuste` DECIMAL(18,6) NOT NULL,
  `fec_creado` DATETIME NOT NULL,
  `fec_aprobado` DATETIME NULL DEFAULT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_prb004_num_presupuesto` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_ajuste_presupuestario`),
  INDEX `fk_pr_d002_ajuste_presupuestario_pr_b004_presupuesto1_idx` (`fk_prb004_num_presupuesto` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario180`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_d002_ajuste_presupuestario_pr_b004_presupuesto1`
    FOREIGN KEY (`fk_prb004_num_presupuesto`)
    REFERENCES `SIACE`.`pr_b004_presupuesto` (`pk_num_presupuesto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pr_d003_ajuste_presupuestario_det`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pr_d003_ajuste_presupuestario_det` (
  `pk_num_ajuste_presupuestario_det` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_prd002_num_ajuste_presupuestario` INT(11) NOT NULL,
  `num_monto_disponible` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_ajustado` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `fk_prc002_num_presupuesto_det` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_prb002_num_partida_presupuestaria` INT(11) NULL DEFAULT NULL,
  `fk_pr_b006_aespecifica` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_ajuste_presupuestario_det`),
  INDEX `fk_pv_ajuste_presupuestario_det_pv_ajuste_presupuestario1_idx` (`fk_prd002_num_ajuste_presupuestario` ASC),
  INDEX `fk_pr_d003_ajuste_presupuestario_det_pr_c002_presupuesto_de_idx` (`fk_prc002_num_presupuesto_det` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_prb002_num_partida_presupuestaria01` (`fk_prb002_num_partida_presupuestaria` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario181`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_d003_ajuste_presupuestario_det_pr_c002_presupuesto_det1`
    FOREIGN KEY (`fk_prc002_num_presupuesto_det`)
    REFERENCES `SIACE`.`pr_c002_presupuesto_det` (`pk_num_presupuesto_det`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prb002_num_partida_presupuestaria01`
    FOREIGN KEY (`fk_prb002_num_partida_presupuestaria`)
    REFERENCES `SIACE`.`pr_b002_partida_presupuestaria` (`pk_num_partida_presupuestaria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pv_ajuste_presupuestario_det_pv_ajuste_presupuestario1`
    FOREIGN KEY (`fk_prd002_num_ajuste_presupuestario`)
    REFERENCES `SIACE`.`pr_d002_ajuste_presupuestario` (`pk_num_ajuste_presupuestario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 747
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pr_d004_credito_adicional`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pr_d004_credito_adicional` (
  `pk_num_credito_adicional` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado_creado` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_revisado` INT(11) NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_comformado` INT(11) NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_aprobado` INT(11) NULL DEFAULT NULL,
  `ind_numero_oficio` VARCHAR(15) CHARACTER SET 'utf8' NOT NULL,
  `fec_oficio` DATE NOT NULL,
  `ind_numero_decreto` VARCHAR(15) CHARACTER SET 'utf8' NOT NULL,
  `fec_decreto` DATE NOT NULL,
  `ind_motivo` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fec_creado` DATETIME NOT NULL,
  `fec_revisado` DATETIME NULL DEFAULT NULL,
  `fec_conformado` DATETIME NULL DEFAULT NULL,
  `fec_aprobado` DATETIME NULL DEFAULT NULL,
  `num_monto_total` DECIMAL(18,6) NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' NOT NULL DEFAULT 'PR',
  `ind_cod_credito_adicional` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_credito_adicional` DATE NOT NULL,
  `fk_prb004_num_presupuesto` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_credito_adicional`),
  INDEX `fk_pr_d004_credito_adicional_pr_b004_presupuesto1_idx` (`fk_prb004_num_presupuesto` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario182`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_d004_credito_adicional_pr_b004_presupuesto1`
    FOREIGN KEY (`fk_prb004_num_presupuesto`)
    REFERENCES `SIACE`.`pr_b004_presupuesto` (`pk_num_presupuesto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pr_d005_credito_adicional_det`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pr_d005_credito_adicional_det` (
  `pk_num_credito_adicional_det` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_prd004_num_credito_adicional` INT(11) NOT NULL,
  `fk_prc002_num_presupuesto_det` INT(11) NOT NULL,
  `num_monto_credito` DECIMAL(18,6) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_credito_adicional_det`),
  INDEX `fk_pv_credito_adicional_det_pv_credito_adicional1_idx` (`fk_prd004_num_credito_adicional` ASC),
  INDEX `fk_pv_credito_adicional_det_pv_presupuesto_det1_idx` (`fk_prc002_num_presupuesto_det` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario183`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pv_credito_adicional_det_pv_credito_adicional1`
    FOREIGN KEY (`fk_prd004_num_credito_adicional`)
    REFERENCES `SIACE`.`pr_d004_credito_adicional` (`pk_num_credito_adicional`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pv_credito_adicional_det_pv_presupuesto_det1`
    FOREIGN KEY (`fk_prc002_num_presupuesto_det`)
    REFERENCES `SIACE`.`pr_c002_presupuesto_det` (`pk_num_presupuesto_det`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 84
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pr_d006_reformulacion_presupuesto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pr_d006_reformulacion_presupuesto` (
  `pk_num_reformulacion_presupuesto` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_prb004_num_presupuesto` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_creado` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_aprobado` INT(11) NOT NULL,
  `cod_reformulacion` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_numero_gaceta` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_gaceta` DATE NOT NULL,
  `ind_numero_resolucion` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_resolucion` DATE NOT NULL,
  `num_monto_reformulacion` DECIMAL(18,6) NOT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_creado` DATE NOT NULL,
  `fec_aprobado` DATE NOT NULL,
  `fec_reformulacion` DATE NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fk_pr_b006_aespecifica` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_reformulacion_presupuesto`),
  UNIQUE INDEX `cod_reformulacion_UNIQUE` (`cod_reformulacion` ASC),
  INDEX `fk_pv_reformulacion_presupuesto_pv_presupuesto1_idx` (`fk_prb004_num_presupuesto` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario185`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pv_reformulacion_presupuesto_pv_presupuesto1`
    FOREIGN KEY (`fk_prb004_num_presupuesto`)
    REFERENCES `SIACE`.`pr_b004_presupuesto` (`pk_num_presupuesto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pr_d007_reformulacion_presupuesto_det`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pr_d007_reformulacion_presupuesto_det` (
  `pk_num_reformulacion_presupuesto_det` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_prd006_num_reformulacion_presupuesto` INT(11) NOT NULL,
  `fk_prc002_num_presupuesto_det` INT(11) NULL DEFAULT NULL,
  `num_monto` DECIMAL(18,6) NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fk_prb002_num_partida_presupuestaria` INT(11) NOT NULL,
  `fk_pr_b006_aespecifica` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_reformulacion_presupuesto_det`),
  INDEX `fk_pv_reformulacion_presupuesto_det_pv_reformulacion_presup_idx` (`fk_prd006_num_reformulacion_presupuesto` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_pr_d007_reformulacion_presupuesto_det_pr_b002_partida_pr_idx` (`fk_prb002_num_partida_presupuestaria` ASC),
  INDEX `fk_pr_d007_reformulacion_presupuesto_det_pr_c002_presupuesto_1` (`fk_prc002_num_presupuesto_det` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario186`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_d007_reformulacion_presupuesto_det_pr_b002_partida_pres1`
    FOREIGN KEY (`fk_prb002_num_partida_presupuestaria`)
    REFERENCES `SIACE`.`pr_b002_partida_presupuestaria` (`pk_num_partida_presupuestaria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pv_reformulacion_presupuesto_det_pv_reformulacion_presupue1`
    FOREIGN KEY (`fk_prd006_num_reformulacion_presupuesto`)
    REFERENCES `SIACE`.`pr_d006_reformulacion_presupuesto` (`pk_num_reformulacion_presupuesto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pr_d008_estado_distribucion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pr_d008_estado_distribucion` (
  `pk_num_estado_distribucion` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_periodo` DATE NOT NULL,
  `num_monto` DECIMAL(18,6) NOT NULL,
  `ind_tipo_distribucion` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'CO' COMMENT 'CO/CA/PA',
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'AC' COMMENT 'AC/AN',
  `fk_prc002_num_presupuesto_det` INT(11) NOT NULL,
  `fk_prd008_num_estado_distribucion` INT(11) NULL DEFAULT NULL,
  `fk_cpd001_num_obligacion` INT(11) NULL DEFAULT NULL,
  `fk_lgb019_num_orden` INT(11) NULL DEFAULT NULL,
  `fk_cpd011_num_banco_transaccion` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_estado_distribucion`),
  INDEX `fk_pr_d008_estado_distribucion_pr_d008_estado_distribucion1_idx` (`fk_prd008_num_estado_distribucion` ASC),
  INDEX `fk_pr_d008_estado_distribucion_pr_c002_presupuesto_det1_idx` (`fk_prc002_num_presupuesto_det` ASC),
  INDEX `fk_pr_d008_estado_distribucion_cp_d001_obligacion1_idx` (`fk_cpd001_num_obligacion` ASC),
  INDEX `fk_lgb019_num_orden` (`fk_lgb019_num_orden` ASC),
  INDEX `fk_cpd011_num_banco_transaccion` (`fk_cpd011_num_banco_transaccion` ASC),
  CONSTRAINT `fk_lgb019_num_orden`
    FOREIGN KEY (`fk_lgb019_num_orden`)
    REFERENCES `SIACE`.`lg_b019_orden` (`pk_num_orden`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_d008_estado_distribucion_cp_d001_obligacion1`
    FOREIGN KEY (`fk_cpd001_num_obligacion`)
    REFERENCES `SIACE`.`cp_d001_obligacion` (`pk_num_obligacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_d008_estado_distribucion_cp_d011_banco_transaccion`
    FOREIGN KEY (`fk_cpd011_num_banco_transaccion`)
    REFERENCES `SIACE`.`cp_d011_banco_transaccion` (`pk_num_banco_transaccion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_d008_estado_distribucion_pr_c002_presupuesto_det1`
    FOREIGN KEY (`fk_prc002_num_presupuesto_det`)
    REFERENCES `SIACE`.`pr_c002_presupuesto_det` (`pk_num_presupuesto_det`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_d008_estado_distribucion_pr_d008_estado_distribucion1`
    FOREIGN KEY (`fk_prd008_num_estado_distribucion`)
    REFERENCES `SIACE`.`pr_d008_estado_distribucion` (`pk_num_estado_distribucion`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 1456
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`pv_partida`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`pv_partida` (
  `cod_partida` VARCHAR(17) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `partida1` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `generica` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `especifica` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `subespecifica` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `denominacion` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `cod_tipocuenta` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `Estado` CHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `UltimoUsuario` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `UltimaFecha` DATETIME NULL DEFAULT NULL,
  `tipo` CHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `nivel` CHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `CodCuenta` VARCHAR(13) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `CodCuentaPub20` VARCHAR(13) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `CtaOrdPagoPub20` VARCHAR(13) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  PRIMARY KEY (`cod_partida`),
  INDEX `cod_tipocuenta` (`cod_tipocuenta` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`py_b001_proyeccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`py_b001_proyeccion` (
  `pk_num_proyeccion` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_anio` YEAR NOT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_desde` DATE NOT NULL,
  `fec_hasta` DATE NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_proyeccion`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario188`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`py_c001_concepto_porcentaje`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`py_c001_concepto_porcentaje` (
  `pk_num_concepto_porcentaje` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_pyb001_num_proyeccion` INT(11) NOT NULL,
  `fk_nmb002_num_concepto` INT(11) NOT NULL,
  `num_numero` INT(10) NOT NULL,
  `num_flag_porcentaje` INT(1) NOT NULL,
  `fec_desde` DATE NOT NULL,
  `fec_hasta` DATE NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_concepto_porcentaje`),
  INDEX `fk_py_concepto_porcentaje_py_proyeccion1_idx` (`fk_pyb001_num_proyeccion` ASC),
  INDEX `fk_py_concepto_porcentaje_pr_concepto1_idx` (`fk_nmb002_num_concepto` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario189`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_py_concepto_porcentaje_pr_concepto1`
    FOREIGN KEY (`fk_nmb002_num_concepto`)
    REFERENCES `SIACE`.`nm_b002_concepto` (`pk_num_concepto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_py_concepto_porcentaje_py_proyeccion1`
    FOREIGN KEY (`fk_pyb001_num_proyeccion`)
    REFERENCES `SIACE`.`py_b001_proyeccion` (`pk_num_proyeccion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`py_c002_proceso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`py_c002_proceso` (
  `pk_num_proceso` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_nmb003_num_tipo_proceso` INT(11) NOT NULL,
  `fk_pyb001_num_proyeccion` INT(11) NOT NULL,
  `fk_nmb001_num_tipo_nomina` INT(11) NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_proceso`),
  INDEX `fk_py_proceso_py_proyeccion1_idx` (`fk_pyb001_num_proyeccion` ASC),
  INDEX `fk_py_proceso_pr_tipo_proceso1_idx` (`fk_nmb003_num_tipo_proceso` ASC),
  INDEX `fk_py_proceso_pr_tipo_nomina1_idx` (`fk_nmb001_num_tipo_nomina` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario190`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_py_proceso_pr_tipo_nomina1`
    FOREIGN KEY (`fk_nmb001_num_tipo_nomina`)
    REFERENCES `SIACE`.`nm_b001_tipo_nomina` (`pk_num_tipo_nomina`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_py_proceso_pr_tipo_proceso1`
    FOREIGN KEY (`fk_nmb003_num_tipo_proceso`)
    REFERENCES `SIACE`.`nm_b003_tipo_proceso` (`pk_num_tipo_proceso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_py_proceso_py_proyeccion1`
    FOREIGN KEY (`fk_pyb001_num_proyeccion`)
    REFERENCES `SIACE`.`py_b001_proyeccion` (`pk_num_proyeccion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`py_d001_prestaciones_sociales_calculo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`py_d001_prestaciones_sociales_calculo` (
  `pk_num_prestaciones_socialescalculo` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_nmb001_num_tipo_nomina` INT(11) NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fec_mes` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_sueldo_base` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_total_asignaciones` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_sueldo_normal` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_alicuota_vacacional` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_alicuota_fin_anio` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_bono_especial` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_bono_fiscal` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_dias_trimestre` INT(2) NOT NULL DEFAULT '0',
  `num_dias_anual` INT(2) NOT NULL DEFAULT '0',
  `num_total_remuneracion_mensual` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_total_remuneracion_diaria` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_trimestral` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_anual` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_prestaciones_socialescalculo`),
  UNIQUE INDEX `unico` (`fec_anio` ASC, `fec_mes` ASC),
  INDEX `fk_pr_prestaciones_sociales_calculo_pr_tipo_nomina1_idx` (`fk_nmb001_num_tipo_nomina` ASC),
  INDEX `fk_py_d001_prestaciones_sociales_calculo_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario191`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prprestaciones_sociales_calculo_pr_tipo_nomina`
    FOREIGN KEY (`fk_nmb001_num_tipo_nomina`)
    REFERENCES `SIACE`.`nm_b001_tipo_nomina` (`pk_num_tipo_nomina`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`py_d002_empleado_proceso_concepto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`py_d002_empleado_proceso_concepto` (
  `pk_num_empleado_procesoconcepto` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_pyc002_num_proceso` INT(11) NOT NULL,
  `fk_nmb002_num_concepto` INT(11) NOT NULL,
  `num_monto` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_aumento` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_antidad` INT(2) NOT NULL DEFAULT '1',
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_empleado_procesoconcepto`),
  INDEX `fk_py_empleado_proceso_concepto_py_proceso1_idx` (`fk_pyc002_num_proceso` ASC),
  INDEX `fk_py_empleado_proceso_concepto_pr_concepto1_idx` (`fk_nmb002_num_concepto` ASC),
  INDEX `fk_py_d002_empleado_proceso_concepto_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_a018_num_seguridad_usuario192`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_py_empleado_proceso_concepto_pr_concepto1`
    FOREIGN KEY (`fk_nmb002_num_concepto`)
    REFERENCES `SIACE`.`nm_b002_concepto` (`pk_num_concepto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_py_empleado_proceso_concepto_py_proceso1`
    FOREIGN KEY (`fk_pyc002_num_proceso`)
    REFERENCES `SIACE`.`py_c002_proceso` (`pk_num_proceso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_b002_solicitud`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_b002_solicitud` (
  `pk_num_solicitud` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a006_num_miscelaneo_det_tipo_solicitud` INT(11) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fec_fecha_solicitud` DATE NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_solicitud`),
  INDEX `fk_rh_b002_solicitud_rh_b001_empleado_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_b002_solicitud_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_rh_b002_a006_num_misc_det_tipo_solicitud_idx` USING BTREE (`fk_a006_num_miscelaneo_det_tipo_solicitud` ASC),
  CONSTRAINT `fk_rh_b002_solicitud_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_b003_pension`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_b003_pension` (
  `pk_num_proceso_pension` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_a001_num_organismo` INT(11) NOT NULL,
  `ind_organismo` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a004_num_dependencia` INT(11) NOT NULL,
  `ind_dependencia` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_rhc063_num_puestos` INT(11) NOT NULL,
  `ind_cargo` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_ultimo_sueldo` DECIMAL(18,6) NOT NULL,
  `num_anio_servicio` INT(11) NOT NULL,
  `num_edad` INT(11) NOT NULL,
  `fec_fecha` DATE NOT NULL,
  `fec_fecha_ingreso` DATE NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipopension` INT(11) NOT NULL,
  `ind_detalle_tipopension` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a006_num_miscelaneo_detalle_motivopension` INT(11) NOT NULL,
  `ind_detalle_motivopension` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_proceso_pension`),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a001_num_organismo` (`fk_a001_num_organismo` ASC),
  INDEX `fk_a004_num_dependencia` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_rhc063_num_puestos` (`fk_rhc063_num_puestos` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_tipopension` (`fk_a006_num_miscelaneo_detalle_tipopension` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_motivopension` (`fk_a006_num_miscelaneo_detalle_motivopension` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_b003_pension_ibfk_10`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_b003_pension_ibfk_2`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_b003_pension_ibfk_3`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_b003_pension_ibfk_4`
    FOREIGN KEY (`fk_rhc063_num_puestos`)
    REFERENCES `SIACE`.`rh_c063_puestos` (`pk_num_puestos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_b004_beneficio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_b004_beneficio` (
  `pk_num_beneficio` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_fecha_beneficio` DATE NOT NULL,
  `ind_nombre_beneficio` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fec_ultima-modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_beneficio`),
  INDEX `fk_rh_b004_beneficio_rh_b001_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_b004_beneficio_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_b004_beneficio_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_b005_estudios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_b005_estudios` (
  `pk_num_estudio` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `ind_nombre_estudio` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_fecha_estudio` DATE NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_estudio`),
  INDEX `fk_rh_c020_estudios_rh_b001_empleado_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_b005_estudios_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_b005_estudios_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_b007_horario_laboral`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_b007_horario_laboral` (
  `pk_num_horario` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_corrido` INT(1) NOT NULL,
  `ind_resolucion` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_resolucion` DATE NULL DEFAULT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_horario`),
  INDEX `fk_rh_b007_horario_laboral_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_b007_horario_laboral_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_b008_ayuda_global`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_b008_ayuda_global` (
  `pk_num_ayuda_global` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_limite` DECIMAL(18,6) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_ayuda_global`),
  INDEX `fk_rh_b008_ayuda_global_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_b008_ayuda_global_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_b009_vacacion_solicitud`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_b009_vacacion_solicitud` (
  `pk_num_solicitud_vacacion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_a004_num_dependencia` INT(11) NOT NULL,
  `fk_rhc063_num_puesto` INT(11) NOT NULL,
  `fk_a006_tipo_vacacion` INT(11) NOT NULL,
  `num_dias_solicitados` INT(11) NOT NULL,
  `fec_solicitud` DATE NOT NULL,
  `fec_salida` DATE NOT NULL,
  `fec_termino` DATE NOT NULL,
  `fec_incorporacion` DATE NOT NULL,
  `ind_motivo` VARCHAR(600) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_otorgamiento` INT(11) NULL DEFAULT NULL,
  `ind_observacion` VARCHAR(700) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_solicitud_vacacion`),
  INDEX `	fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a006_tipo_vacacion` (`fk_a006_tipo_vacacion` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_rhc063_num_puesto` (`fk_rhc063_num_puesto` ASC),
  INDEX `fk_a004_num_dependencia` (`fk_a004_num_dependencia` ASC),
  CONSTRAINT `rh_b009_vacacion_solicitud_ibfk_3`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_b009_vacacion_solicitud_ibfk_4`
    FOREIGN KEY (`fk_rhc063_num_puesto`)
    REFERENCES `SIACE`.`rh_c063_puestos` (`pk_num_puestos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_b009_vacacion_solicitud_ibfk_6`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c032_motivo_cese`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c032_motivo_cese` (
  `pk_num_motivo_cese` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_nombre_cese` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_falta_grave` INT(1) NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_motivo_cese`),
  INDEX `fk_rh_c032_motivo_cese_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c032_motivo_cese_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_b010_jubilacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_b010_jubilacion` (
  `pk_num_jubilacion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_nmb001_num_tipo_nomina` INT(11) NOT NULL,
  `fk_a006_num_tipo_trabajador` INT(11) NOT NULL,
  `fk_rhc032_num_motivo_cese` INT(11) NOT NULL,
  `fk_a001_num_organismo` INT(11) NOT NULL,
  `fk_a004_num_dependencia` INT(11) NOT NULL,
  `fk_rhc063_num_puesto` INT(11) NOT NULL,
  `fec_ingreso` DATE NOT NULL,
  `fec_egreso` DATE NOT NULL,
  `ind_periodo` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `num_edad` INT(11) NOT NULL,
  `num_anio_servicio` INT(11) NOT NULL,
  `num_anio_experiencia` INT(11) NOT NULL,
  `num_anio_total` INT(11) NOT NULL,
  `ind_observacion_cese` VARCHAR(600) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_jubilacion`),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a001_num_organismo` (`fk_a001_num_organismo` ASC),
  INDEX `fk_a004_num_dependencia` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_rhc063_num_puesto` (`fk_rhc063_num_puesto` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_nmb001_num_tipo_nomina` (`fk_nmb001_num_tipo_nomina` ASC),
  INDEX `fk_a006_num_tipo_trabajador` (`fk_a006_num_tipo_trabajador` ASC),
  INDEX `fk_rhc032_num_motivo_cese` (`fk_rhc032_num_motivo_cese` ASC),
  CONSTRAINT `rh_b010_jubilacion_ibfk_11`
    FOREIGN KEY (`fk_rhc032_num_motivo_cese`)
    REFERENCES `SIACE`.`rh_c032_motivo_cese` (`pk_num_motivo_cese`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_b010_jubilacion_ibfk_5`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_b010_jubilacion_ibfk_6`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_b010_jubilacion_ibfk_7`
    FOREIGN KEY (`fk_rhc063_num_puesto`)
    REFERENCES `SIACE`.`rh_c063_puestos` (`pk_num_puestos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_b010_jubilacion_ibfk_8`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_b010_jubilacion_ibfk_9`
    FOREIGN KEY (`fk_nmb001_num_tipo_nomina`)
    REFERENCES `SIACE`.`nm_b001_tipo_nomina` (`pk_num_tipo_nomina`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c001_empleado_domicilio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c001_empleado_domicilio` (
  `pk_num_empleado_domicilio` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a010_num_ciudad` INT(11) NOT NULL,
  `ind_direccion_fiscal` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_direccion_residencia` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a006_num_miscelaneo_detalle_domicilio` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_empleado_domicilio`),
  INDEX `fk_rh_c001_empleado_domicilio_a010_ciudad1_idx` (`fk_a010_num_ciudad` ASC),
  INDEX `fk_rh_c001_empleado_domicilio_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle_domicilio` ASC),
  INDEX `fk_rh_c001_empleado_domicilio_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c001_empleado_domicilio_a010_ciudad1`
    FOREIGN KEY (`fk_a010_num_ciudad`)
    REFERENCES `SIACE`.`a010_ciudad` (`pk_num_ciudad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c001_empleado_domicilio_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c002_empleado_emergencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c002_empleado_emergencia` (
  `pk_num_empleado_emergencia` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_parentesco` INT(11) NOT NULL,
  `fk_a003_num_persona` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_empleado_emergencia`),
  INDEX `fk_rh_c002_empleado_emergencia_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c002_empleado_emergencia_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle_parentesco` ASC),
  INDEX `fk_rh_c002_empleado_emergencia_a003_persona1_idx` (`fk_a003_num_persona` ASC),
  INDEX `fk_rh_c002_empleado_emergencia_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c002_empleado_emergencia_a003_persona1`
    FOREIGN KEY (`fk_a003_num_persona`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c002_empleado_emergencia_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c003_operaciones_empleado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c003_operaciones_empleado` (
  `pk_num_operaciones_empleado` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'PR: PREPARADO, RV:REVISADO, AP: APROBADO',
  `fk_rhb001_num_empleado_registro` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_operacion` INT(11) NOT NULL,
  `num_flag_operacion` INT(1) NOT NULL,
  `txt_observaciones` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_operacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_operaciones_empleado`),
  INDEX `fk_rh_c003_operaciones_empleado_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_registro` ASC),
  INDEX `fk_rh_c003_operaciones_empleado_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_operacion` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 87
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c045_viatico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c045_viatico` (
  `pk_num_viatico` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_salida` DATE NOT NULL,
  `fec_ingreso` DATE NOT NULL,
  `num_total_dias` INT(10) NOT NULL,
  `ind_destino` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_razon_rechazo` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_observacion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_registro_viatico` DATE NOT NULL,
  `ind_motivo` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `cod_interno` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_aprobacion` DATE NOT NULL,
  `fk_rhb001_num_empleado_solicita` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_preparado_por` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_aprobado_por` INT(11) NOT NULL,
  `fk_a001_num_organismo` INT(11) NOT NULL,
  `fk_a004_num_dependencia` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_viatico`),
  INDEX `fk_rh_c045_viatico_a018_seguridad_usuario1_idx1` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_rh_c045_viatico_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_solicita` ASC),
  INDEX `fk_rh_c045_viatico_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_preparado_por` ASC),
  INDEX `fk_rh_c045_viatico_rh_b001_empleado3_idx` (`fk_rhb001_num_empleado_aprobado_por` ASC),
  INDEX `fk_rh_c045_viatico_a001_organismo1_idx` (`fk_a001_num_organismo` ASC),
  INDEX `fk_rh_c045_viatico_a004_dependencia1_idx` (`fk_a004_num_dependencia` ASC),
  CONSTRAINT `fk_rh_c045_viatico_a001_organismo1`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c045_viatico_a004_dependencia1`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c045_viatico_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c004_detalle_viatico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c004_detalle_viatico` (
  `pk_num_detalle_viatico` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhc045_num_viatico` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_beneficiario` INT(11) NOT NULL,
  `fec_salida_detalle` DATE NOT NULL,
  `fec_ingreso_detalle` DATE NOT NULL,
  `num_total_dias_detalle` INT(3) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_detalle_viatico`),
  INDEX `fk_rh_c004_detalle_viatico_rh_c045_viatico1_idx` (`fk_rhc045_num_viatico` ASC),
  INDEX `fk_rh_c004_detalle_viatico_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_beneficiario` ASC),
  INDEX `fk_rh_c004_detalle_viatico_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c004_detalle_viatico_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c004_detalle_viatico_rh_c045_viatico1`
    FOREIGN KEY (`fk_rhc045_num_viatico`)
    REFERENCES `SIACE`.`rh_c045_viatico` (`pk_num_viatico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c005_empleado_laboral`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c005_empleado_laboral` (
  `pk_num_empleado_laboral` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fec_ingreso` DATE NOT NULL,
  `fec_ingreso_anterior` DATE NULL DEFAULT NULL,
  `ind_resolucion_ingreso` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_egreso` DATE NULL DEFAULT NULL,
  `ind_resolucion_egreso` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_sueldo_ant` DECIMAL(18,6) NULL DEFAULT NULL,
  `fk_rhc063_num_puestos_cargo` INT(11) NOT NULL,
  `fk_rhc063_num_puestos_cargotmp` INT(11) NULL DEFAULT NULL,
  `fk_a006_num_miscelaneo_detalle_paso` INT(11) NOT NULL,
  `num_sueldo` DECIMAL(18,6) NOT NULL,
  `fk_rhb007_num_horario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_empleado_laboral`),
  INDEX `fk_rh_c005_empleado_laboral_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c005_empleado_laboral_rh_c063_puestos1_idx` (`fk_rhc063_num_puestos_cargo` ASC),
  INDEX `fk_rh_c005_empleado_laboral_rh_c063_puestos2_idx` (`fk_rhc063_num_puestos_cargotmp` ASC),
  INDEX `fk_rh_c005_empleado_laboral_rh_b007_horario_laboral1_idx` (`fk_rhb007_num_horario` ASC),
  INDEX `fk_rh_c005_empleado_laboral_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c005_empleado_laboral_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c005_empleado_laboral_rh_b007_horario_laboral1`
    FOREIGN KEY (`fk_rhb007_num_horario`)
    REFERENCES `SIACE`.`rh_b007_horario_laboral` (`pk_num_horario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c005_empleado_laboral_rh_c063_puestos1`
    FOREIGN KEY (`fk_rhc063_num_puestos_cargo`)
    REFERENCES `SIACE`.`rh_c063_puestos` (`pk_num_puestos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c005_empleado_laboral_rh_c063_puestos2`
    FOREIGN KEY (`fk_rhc063_num_puestos_cargotmp`)
    REFERENCES `SIACE`.`rh_c063_puestos` (`pk_num_puestos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 33
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c006_tipo_cargo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c006_tipo_cargo` (
  `pk_num_cargo` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_nombre_cargo` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `txt_descripcion` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `txt_funcion` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_cargo`),
  INDEX `fk_rh_c006_tipo_cargo_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c006_tipo_cargo_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 67
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c008_meritos_demeritos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c008_meritos_demeritos` (
  `pk_num_meritos_demeritos` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_meritodemerito` INT(11) NOT NULL,
  `ind_tipo` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_documento` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_documento` DATE NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipo` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_responsable` INT(11) NULL DEFAULT NULL,
  `ind_persona_externa` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_flag_externo` INT(1) NULL DEFAULT NULL,
  `txt_observaciones` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_meritos_demeritos`),
  INDEX `fk_rh_c008_meritos_demeritos_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c008_meritos_demeritos_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle_meritodemerito` ASC),
  INDEX `fk_rh_c008_meritos_demeritos_a006_miscelaneo_detalle2_idx` (`fk_a006_num_miscelaneo_detalle_tipo` ASC),
  INDEX `fk_rh_c008_meritos_demeritos_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_responsable` ASC),
  INDEX `fk_rh_c008_meritos_demeritos_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c008_meritos_demeritos_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c009_nivel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c009_nivel` (
  `pk_num_nivel` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhc006_num_cargo` INT(11) NOT NULL,
  `num_nivel` INT(2) NOT NULL,
  `ind_nombre_nivel` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_nivel`),
  INDEX `fk_rh_c009_nivel_rh_c006_tipo_cargo1_idx` (`fk_rhc006_num_cargo` ASC),
  INDEX `fk_rh_c009_nivel_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c009_nivel_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c009_nivel_rh_c006_tipo_cargo1`
    FOREIGN KEY (`fk_rhc006_num_cargo`)
    REFERENCES `SIACE`.`rh_c006_tipo_cargo` (`pk_num_cargo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 252
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c010_serie`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c010_serie` (
  `pk_num_serie` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_nombre_serie` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a006_num_miscelaneo_detalle_grupoocupacional` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_serie`),
  INDEX `fk_rh_c10_serie_a006_miscelaneo_detalle_idx` USING BTREE (`fk_a006_num_miscelaneo_detalle_grupoocupacional` ASC),
  INDEX `fk_rh_c010_serie_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c010_serie_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 205
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c011_experiencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c011_experiencia` (
  `pk_num_experiencia` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `ind_cargo` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_empresa` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a006_num_miscelaneo_detalle_areaexp` INT(11) NOT NULL,
  `ind_area` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ingreso` DATE NOT NULL,
  `fec_egreso` DATE NULL DEFAULT NULL,
  `fk_a006_num_miscelaneo_detalle_tipo_empresa` INT(11) NOT NULL,
  `fk_rhc032_num_motivo_cese` INT(11) NOT NULL,
  `num_sueldo_mensual` DECIMAL(18,6) NOT NULL,
  `num_flag_antvacacion` INT(1) NOT NULL,
  `txt_funciones` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_experiencia`),
  INDEX `fk_rh_c011_experiencia_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c011_experiencia_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle_tipo_empresa` ASC),
  INDEX `fk_rh_c011_experiencia_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_rh_c011_experiencia_a006_miscelaneo_detalle2_idx` (`fk_a006_num_miscelaneo_detalle_areaexp` ASC),
  INDEX `fk_rh_c011_experiencia_rh_c032_motivo_cese1_idx` (`fk_rhc032_num_motivo_cese` ASC),
  CONSTRAINT `fk_rh_c011_experiencia_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c011_experiencia_rh_c032_motivo_cese1`
    FOREIGN KEY (`fk_rhc032_num_motivo_cese`)
    REFERENCES `SIACE`.`rh_c032_motivo_cese` (`pk_num_motivo_cese`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c012_empleado_banco`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c012_empleado_banco` (
  `pk_num_empleado_banco` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_banco` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipocta` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipoapo` INT(11) NOT NULL,
  `ind_cuenta` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_sueldo_mensual` DECIMAL(18,6) NULL DEFAULT NULL,
  `num_flag_principal` INT(1) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_empleado_banco`),
  INDEX `fk_rh_c012_empleado_banco_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c012_empleado_banco_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle_banco` ASC),
  INDEX `fk_rh_c012_empleado_banco_a006_miscelaneo_detalle2_idx` (`fk_a006_num_miscelaneo_detalle_tipocta` ASC),
  INDEX `fk_rh_c012_empleado_banco_a006_miscelaneo_detalle3_idx` (`fk_a006_num_miscelaneo_detalle_tipoapo` ASC),
  INDEX `fk_rh_c012_empleado_banco_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c012_empleado_banco_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c013_idioma`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c013_idioma` (
  `pk_num_idioma` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_nombre_idioma` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_nombre_ext` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_idioma`),
  INDEX `fk_rh_c013_idioma_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c013_idioma_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c014_asistencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c014_asistencia` (
  `pk_num_asistencia` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fec_fecha` DATE NOT NULL,
  `fec_hora_entrada` TIME NOT NULL,
  `fec_hora_salida` TIME NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_asistencia`),
  INDEX `fk_rh_c014_asistencia_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c014_asistencia_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c014_asistencia_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c040_institucion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c040_institucion` (
  `pk_num_institucion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_nombre_institucion` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_ubicacion` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_estudio` INT(1) NOT NULL,
  `num_flag_curso` INT(1) NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_institucion`),
  INDEX `fk_rh_c040_institucion_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c040_institucion_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 314
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c064_nivel_grado_instruccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c064_nivel_grado_instruccion` (
  `pk_num_nivel_grado` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a006_num_miscelaneo_detalle_gradoinst` INT(11) NOT NULL,
  `ind_nombre_nivel_grado` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `num_flag_utiles` INT(1) NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_nivel_grado`),
  INDEX `rh_c064_nivel_grado_instru_a006_miscelaneo_detalle_idx` USING BTREE (`fk_a006_num_miscelaneo_detalle_gradoinst` ASC),
  INDEX `fk_rh_c064_nivel_grado_instruccion_a018_seguridad_usuario1_idx1` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c064_nivel_grado_instruccion_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 19
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c015_carga_familiar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c015_carga_familiar` (
  `pk_num_carga` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a006_num_miscelaneo_detalle_parentezco` INT(11) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_a003_num_persona` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_gradoinst` INT(11) NULL DEFAULT NULL,
  `fk_a006_num_miscelaneo_detalle_tipoedu` INT(11) NULL DEFAULT NULL,
  `fk_rhc040_num_institucion` INT(11) NULL DEFAULT NULL,
  `ind_empresa` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `txt_direccion_empresa` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_sueldo_mensual` DECIMAL(18,6) NULL DEFAULT NULL,
  `num_flag_discapacidad` INT(1) NULL DEFAULT NULL,
  `num_flag_estudia` INT(1) NULL DEFAULT NULL,
  `num_flag_trabaja` INT(1) NULL DEFAULT NULL,
  `fk_rhc064_nivel_grado_instruccion` INT(11) NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_carga`),
  INDEX `fk_rh_c015_carga_familiar_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle_parentezco` ASC),
  INDEX `fk_rh_c015_carga_familiar_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c015_carga_familiar_a003_persona1_idx` (`fk_a003_num_persona` ASC),
  INDEX `fk_rh_c015_carga_familiar_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_rh_c015_carga_familiar_a006_miscelaneo_detalle2_idx` (`fk_a006_num_miscelaneo_detalle_gradoinst` ASC),
  INDEX `fk_rh_c015_carga_familiar_a006_miscelaneo_detalle3_idx` (`fk_a006_num_miscelaneo_detalle_tipoedu` ASC),
  INDEX `fk_rh_c015_carga_familiar_rh_c040_institucion1_idx` (`fk_rhc040_num_institucion` ASC),
  INDEX `fk_rh_c015_carga_familiar_rh_c064_nivel_grado_instruccion1_idx` (`fk_rhc064_nivel_grado_instruccion` ASC),
  CONSTRAINT `fk_rh_c015_carga_familiar_a003_persona1`
    FOREIGN KEY (`fk_a003_num_persona`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c015_carga_familiar_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c015_carga_familiar_rh_c040_institucion1`
    FOREIGN KEY (`fk_rhc040_num_institucion`)
    REFERENCES `SIACE`.`rh_c040_institucion` (`pk_num_institucion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c015_carga_familiar_rh_c064_nivel_grado_instruccion1`
    FOREIGN KEY (`fk_rhc064_nivel_grado_instruccion`)
    REFERENCES `SIACE`.`rh_c064_nivel_grado_instruccion` (`pk_num_nivel_grado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 17
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c016_retencion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c016_retencion` (
  `pk_num_retencion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a006_num_miscelaneo_detalle_tipo_retencion` INT(11) NOT NULL,
  `fec_fecha_resolucion` DATE NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `ind_expediente` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_juzgado` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `txt_demandante` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_tipo_pago` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_concepto_retencion` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_concepto` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_comentario` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_retencion`),
  INDEX `fk_rh_c016_retencion_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c016_retencion_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_rh_c016_retencion_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle_tipo_retencion` ASC),
  CONSTRAINT `fk_rh_c016_retencion_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c017_empleado_documentos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c017_empleado_documentos` (
  `pk_num_empleado_documento` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_documento` INT(11) NOT NULL,
  `num_flag_entregado` INT(1) NOT NULL,
  `fec_entrega` DATE NOT NULL,
  `fec_vencimiento` DATE NOT NULL,
  `num_flag_carga` INT(1) NOT NULL,
  `fk_c015_num_carga_familiar` INT(11) NULL DEFAULT NULL,
  `txt_observaciones` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_documento` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_empleado_documento`),
  INDEX `fk_rh_c017_empleado_documentos_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c017_empleado_documentos_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle_documento` ASC),
  INDEX `fk_rh_c017_empleado_documentos_rh_c015_carga_familiar1_idx` (`fk_c015_num_carga_familiar` ASC),
  INDEX `fk_rh_c017_empleado_documentos_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c017_empleado_documentos_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c017_empleado_documentos_rh_c015_carga_familiar1`
    FOREIGN KEY (`fk_c015_num_carga_familiar`)
    REFERENCES `SIACE`.`rh_c015_carga_familiar` (`pk_num_carga`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c057_profesiones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c057_profesiones` (
  `pk_num_profesion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a006_num_miscelaneo_detalle_areaformacion` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_gradoinst` INT(11) NOT NULL,
  `ind_nombre_profesion` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `ind_ultimo_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_profesion`),
  INDEX `fk_a018_num_seguridad_usuario` (`ind_ultimo_usuario` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_areaformacion_idx` USING BTREE (`fk_a006_num_miscelaneo_detalle_areaformacion` ASC),
  INDEX `fk_rh_057_profesiones_a006_miscelaneo_detalle_gradoinst` USING BTREE (`fk_a006_num_miscelaneo_detalle_gradoinst` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c018_empleado_instruccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c018_empleado_instruccion` (
  `pk_num_empleado_instruccion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_gradoinst` INT(11) NOT NULL,
  `fk_rhc064_num_nivel_grado` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_area` INT(11) NOT NULL,
  `fk_rhc057_num_profesion` INT(11) NOT NULL,
  `fk_rhc040_num_institucion` INT(11) NOT NULL,
  `fec_graduacion` DATE NOT NULL,
  `fec_ingreso` DATE NOT NULL,
  `fec_egreso` DATE NOT NULL,
  `fk_a006_num_miscelaneo_detalle_colegiatura` INT(11) NULL DEFAULT NULL,
  `ind_colegiatura` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `txt_observaciones` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_empleado_instruccion`),
  INDEX `fk_rh_c018_empleado_instruccion_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c018_empleado_instruccion_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle_gradoinst` ASC),
  INDEX `fk_rh_c018_empleado_instruccion_rh_c064_nivel_grado_instruc_idx` (`fk_rhc064_num_nivel_grado` ASC),
  INDEX `fk_rh_c018_empleado_instruccion_a006_miscelaneo_detalle2_idx` (`fk_a006_num_miscelaneo_detalle_area` ASC),
  INDEX `fk_rh_c018_empleado_instruccion_rh_c057_profesiones1_idx` (`fk_rhc057_num_profesion` ASC),
  INDEX `fk_rh_c018_empleado_instruccion_rh_c040_institucion1_idx` (`fk_rhc040_num_institucion` ASC),
  INDEX `fk_rh_c018_empleado_instruccion_a006_miscelaneo_detalle3_idx` (`fk_a006_num_miscelaneo_detalle_colegiatura` ASC),
  INDEX `fk_rh_c018_empleado_instruccion_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c018_empleado_instruccion_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c018_empleado_instruccion_rh_c040_institucion1`
    FOREIGN KEY (`fk_rhc040_num_institucion`)
    REFERENCES `SIACE`.`rh_c040_institucion` (`pk_num_institucion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c018_empleado_instruccion_rh_c057_profesiones1`
    FOREIGN KEY (`fk_rhc057_num_profesion`)
    REFERENCES `SIACE`.`rh_c057_profesiones` (`pk_num_profesion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c018_empleado_instruccion_rh_c064_nivel_grado_instrucci1`
    FOREIGN KEY (`fk_rhc064_num_nivel_grado`)
    REFERENCES `SIACE`.`rh_c064_nivel_grado_instruccion` (`pk_num_nivel_grado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c019_beneficio_alimentacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c019_beneficio_alimentacion` (
  `pk_num_beneficio` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a001_num_organismo` INT(11) NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `ind_periodo` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_nmb001_num_tipo_nomina` INT(11) NOT NULL,
  `fec_inicio_periodo` DATE NOT NULL,
  `fec_fin_periodo` DATE NOT NULL,
  `num_dias_periodo` INT(11) NOT NULL DEFAULT '0',
  `num_dias_pago` INT(11) NOT NULL DEFAULT '0',
  `num_total_feriados` INT(11) NOT NULL DEFAULT '0',
  `num_valor_diario` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `fec_horas_diarias` INT(3) NOT NULL,
  `fec_horas_semanales` INT(3) NOT NULL,
  `num_valor_semanal` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_valor_mes` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `ind_estado` INT(11) NOT NULL DEFAULT '1' COMMENT '1:ABIERTO ; 0:CERRADO',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_beneficio`),
  INDEX `fk_rh_c019_beneficio_alimentacion_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_a001_num_organismo` (`fk_a001_num_organismo` ASC),
  INDEX `fk_nmb001_num_tipo_nomina` (`fk_nmb001_num_tipo_nomina` ASC),
  CONSTRAINT `fk_rh_c019_beneficio_alimentacion_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c019_beneficio_alimentacion_ibfk_1`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c019_beneficio_alimentacion_ibfk_2`
    FOREIGN KEY (`fk_nmb001_num_tipo_nomina`)
    REFERENCES `SIACE`.`nm_b001_tipo_nomina` (`pk_num_tipo_nomina`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c020_patrimonio_inmueble`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c020_patrimonio_inmueble` (
  `pk_num_patrimonio_inmueble` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `ind_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_ubicacion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_uso` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_valor` DECIMAL(18,6) NOT NULL,
  `num_flag_hipotecado` INT(1) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_patrimonio_inmueble`),
  INDEX `fk_rh_c020_patrimonio_inmueble_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c020_patrimonio_inmueble_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c020_patrimonio_inmueble_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c021_patrimonio_vehiculo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c021_patrimonio_vehiculo` (
  `pk_num_patrimonio_vehiculo` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_marca` INT(11) NOT NULL,
  `ind_modelo` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a006_num_miscelaneo_detalle_color` INT(11) NOT NULL,
  `ind_placa` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `num_valor` DECIMAL(18,6) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_patrimonio_vehiculo`),
  INDEX `fk_rh_c021_patrimonio_vehiculo_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c021_patrimonio_vehiculo_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle_marca` ASC),
  INDEX `fk_rh_c021_patrimonio_vehiculo_a006_miscelaneo_detalle2_idx` (`fk_a006_num_miscelaneo_detalle_color` ASC),
  INDEX `fk_rh_c021_patrimonio_vehiculo_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c021_patrimonio_vehiculo_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c022_requerimiento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c022_requerimiento` (
  `pk_num_requerimiento` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_rhb002_num_solicitud` INT(11) NOT NULL,
  `fec_vigencia_inicio` DATE NOT NULL,
  `fec_vigencia_fin` DATE NOT NULL,
  `ind_vacante` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_termino` DATE NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_requerimiento`),
  INDEX `fk_rh_c022_requerimiento_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c022_requerimiento_rh_b002_solicitud1_idx` (`fk_rhb002_num_solicitud` ASC),
  INDEX `fk_rh_c022_requerimiento_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c022_requerimiento_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c022_requerimiento_rh_b002_solicitud1`
    FOREIGN KEY (`fk_rhb002_num_solicitud`)
    REFERENCES `SIACE`.`rh_b002_solicitud` (`pk_num_solicitud`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c023_patrimonio_inversion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c023_patrimonio_inversion` (
  `pk_num_patrimonio_inversion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `ind_titular` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_remitente` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_certificado` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_cant` INT(11) NULL DEFAULT NULL,
  `num_valor_nominal` DECIMAL(18,6) NULL DEFAULT NULL,
  `num_valor` DECIMAL(18,6) NULL DEFAULT NULL,
  `num_flag_garantia` INT(1) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_patrimonio_inversion`),
  INDEX `fk_rh_c023_patrimonio_inversion_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c023_patrimonio_inversion_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c023_patrimonio_inversion_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c024_tabla_vacacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c024_tabla_vacacion` (
  `pk_num_tabla_vacacion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_nmb001_num_nomina` INT(11) NOT NULL,
  `num_anios` INT(11) NOT NULL,
  `num_dias_disfrute` INT(11) NOT NULL,
  `num_dias_adicionales` INT(11) NOT NULL,
  `num_total_disfrutar` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_tabla_vacacion`),
  INDEX `	fk_nmb001_num_nomina` (`fk_nmb001_num_nomina` ASC),
  INDEX `	fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c024_tabla_vacacion_ibfk_1`
    FOREIGN KEY (`fk_nmb001_num_nomina`)
    REFERENCES `SIACE`.`nm_b001_tipo_nomina` (`pk_num_tipo_nomina`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c024_tabla_vacacion_ibfk_2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 86
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c025_patrimonio_cuenta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c025_patrimonio_cuenta` (
  `pk_num_patrimonio_cuenta` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipocta` INT(11) NOT NULL,
  `ind_banco` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_cuenta` VARCHAR(25) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_monto` DECIMAL(18,6) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_patrimonio_cuenta`),
  INDEX `fk_rh_c025_patrimonio_cuenta_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c025_patrimonio_cuenta_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle_tipocta` ASC),
  INDEX `fk_rh_c025_patrimonio_cuenta_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c025_patrimonio_cuenta_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c026_patrimonio_otros`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c026_patrimonio_otros` (
  `pk_num_patrimonio_otros` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `ind_descripcion` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_valor_compra` DECIMAL(18,6) NOT NULL,
  `num_valor_actual` DECIMAL(18,6) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_patrimonio_otros`),
  INDEX `fk_rh_c026_patrimonio_otros_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c026_patrimonio_otros_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c026_patrimonio_otros_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c028_empleado_referencias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c028_empleado_referencias` (
  `pk_num_empleado_referencia` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tiporef` INT(11) NOT NULL,
  `ind_nombre` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_empresa` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_direccion` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_telefono` VARCHAR(14) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_cargo` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_empleado_referencia`),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_tiporef` (`fk_a006_num_miscelaneo_detalle_tiporef` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c028_empleado_referencias_ibfk_3`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c029_empleado_idioma`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c029_empleado_idioma` (
  `pk_num_empleado_idioma` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_rhc013_num_idioma` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_lectura` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_oral` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_escritura` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_general` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_empleado_idioma`),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rhc013_num_idioma` (`fk_rhc013_num_idioma` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_lectura` (`fk_a006_num_miscelaneo_detalle_lectura` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_oral` (`fk_a006_num_miscelaneo_detalle_oral` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_escritura` (`fk_a006_num_miscelaneo_detalle_escritura` ASC),
  INDEX ` fk_a006_num_miscelaneo_detalle_general` (`fk_a006_num_miscelaneo_detalle_general` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c029_empleado_idioma_ibfk_2`
    FOREIGN KEY (`fk_rhc013_num_idioma`)
    REFERENCES `SIACE`.`rh_c013_idioma` (`pk_num_idioma`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c029_empleado_idioma_ibfk_7`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c033_empleado_islr`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c033_empleado_islr` (
  `pk_num_empleado_islr` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `ind_desde` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_hasta` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_porcentaje` DECIMAL(11,2) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_empleado_islr`),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c033_empleado_islr_ibfk_2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c034_invalidez`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c034_invalidez` (
  `pk_num_invalidez` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb003_num_proceso_pension` INT(11) NOT NULL,
  `num_monto_pension` DECIMAL(18,6) NOT NULL,
  `ind_periodo` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_nmb001_num_tipo_nomina` INT(11) NULL DEFAULT NULL,
  `fk_a006_num_miscelaneo_detalle_tipotrab` INT(11) NULL DEFAULT NULL,
  `num_situacion_trabajo` INT(11) NULL DEFAULT NULL,
  `fk_rhc032_num_motivo_cese` INT(11) NULL DEFAULT NULL,
  `fec_fecha_egreso` DATE NULL DEFAULT NULL,
  `txt_observacion_egreso` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_resolucion` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_invalidez`),
  INDEX `fk_rh_c034_invalidez_rh_b003_pension1_idx` (`fk_rhb003_num_proceso_pension` ASC),
  INDEX `fk_rh_c034_invalidez_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_nmb001_num_tipo_nomina` (`fk_nmb001_num_tipo_nomina` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_tipotrab` (`fk_a006_num_miscelaneo_detalle_tipotrab` ASC),
  INDEX `fk_rhc032_num_motivo_cese` (`fk_rhc032_num_motivo_cese` ASC),
  CONSTRAINT `fk_rh_c034_invalidez_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c034_invalidez_ibfk_1`
    FOREIGN KEY (`fk_rhb003_num_proceso_pension`)
    REFERENCES `SIACE`.`rh_b003_pension` (`pk_num_proceso_pension`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c034_invalidez_ibfk_2`
    FOREIGN KEY (`fk_nmb001_num_tipo_nomina`)
    REFERENCES `SIACE`.`nm_b001_tipo_nomina` (`pk_num_tipo_nomina`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c034_invalidez_ibfk_4`
    FOREIGN KEY (`fk_rhc032_num_motivo_cese`)
    REFERENCES `SIACE`.`rh_c032_motivo_cese` (`pk_num_motivo_cese`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c035_sobreviviente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c035_sobreviviente` (
  `pk_num_sobreviviente` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb003_num_proceso_pension` INT(11) NOT NULL,
  `num_monto_pension` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_monto_jubilacion` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_coeficiente` DECIMAL(11,2) NOT NULL DEFAULT '0.00',
  `num_total_sueldo` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_anios_servicio_exceso` INT(11) NOT NULL,
  `ind_periodo` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_nmb001_num_tipo_nomina` INT(11) NULL DEFAULT NULL,
  `fk_a006_num_miscelaneo_detalle_tipotrab` INT(11) NULL DEFAULT NULL,
  `num_situacion_trabajo` INT(11) NOT NULL,
  `fk_rhc032_num_motivo_cese` INT(11) NULL DEFAULT NULL,
  `fec_fecha_egreso` DATE NULL DEFAULT NULL,
  `txt_observacion_egreso` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_resolucion` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_sobreviviente`),
  INDEX `fk_rh_c035_sobreviviente_a018_seguridad_usuario1_idx1` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_rhb003_num_proceso_pension` (`fk_rhb003_num_proceso_pension` ASC),
  INDEX `fk_nmb001_num_tipo_nomina` (`fk_nmb001_num_tipo_nomina` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_tipotrab` (`fk_a006_num_miscelaneo_detalle_tipotrab` ASC),
  INDEX `fk_rhc032_num_motivo_cese` (`fk_rhc032_num_motivo_cese` ASC),
  CONSTRAINT `fk_rh_c035_sobreviviente_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c035_sobreviviente_ibfk_1`
    FOREIGN KEY (`fk_rhb003_num_proceso_pension`)
    REFERENCES `SIACE`.`rh_b003_pension` (`pk_num_proceso_pension`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c035_sobreviviente_ibfk_2`
    FOREIGN KEY (`fk_nmb001_num_tipo_nomina`)
    REFERENCES `SIACE`.`nm_b001_tipo_nomina` (`pk_num_tipo_nomina`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c035_sobreviviente_ibfk_4`
    FOREIGN KEY (`fk_rhc032_num_motivo_cese`)
    REFERENCES `SIACE`.`rh_c032_motivo_cese` (`pk_num_motivo_cese`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c036_permiso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c036_permiso` (
  `pk_num_permiso` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a006_num_miscelaneo_motivo_ausencia` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_tipo_ausencia` INT(11) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_rhb007_num_horario` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_aprueba` INT(11) NOT NULL,
  `fk_a004_num_dependencia` INT(11) NOT NULL,
  `fk_rhc063_num_puesto` INT(11) NOT NULL,
  `ind_periodo_contable` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_salida` DATE NOT NULL,
  `fec_entrada` DATE NOT NULL,
  `fec_hora_salida` TIME NOT NULL,
  `fec_hora_entrada` TIME NOT NULL,
  `num_total_dia` INT(11) NOT NULL,
  `num_total_hora` INT(11) NOT NULL,
  `num_total_minuto` INT(11) NOT NULL,
  `ind_motivo` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_remuneracion` INT(11) NULL DEFAULT NULL,
  `num_justificativo` INT(11) NULL DEFAULT NULL,
  `num_exento` INT(11) NULL DEFAULT NULL,
  `fec_ingreso` DATE NOT NULL,
  `ind_observacion_aprobacion` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_observacion_verificacion` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_permiso`),
  INDEX ` fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_rhb007_num_horario` (`fk_rhb007_num_horario` ASC),
  INDEX `fk_a006_num_miscelaneo_motivo_ausencia` (`fk_a006_num_miscelaneo_motivo_ausencia` ASC),
  INDEX `fk_a006_num_miscelaneo_tipo_ausencia` (`fk_a006_num_miscelaneo_tipo_ausencia` ASC),
  INDEX `fk_rhb001_num_empleado_aprueba` (`fk_rhb001_num_empleado_aprueba` ASC),
  INDEX ` fk_a004_num_dependencia` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_rhc063_num_puesto` (`fk_rhc063_num_puesto` ASC),
  CONSTRAINT `rh_c036_permiso_ibfk_10`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c036_permiso_ibfk_11`
    FOREIGN KEY (`fk_rhc063_num_puesto`)
    REFERENCES `SIACE`.`rh_c063_puestos` (`pk_num_puestos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c036_permiso_ibfk_4`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c036_permiso_ibfk_5`
    FOREIGN KEY (`fk_rhb007_num_horario`)
    REFERENCES `SIACE`.`rh_b007_horario_laboral` (`pk_num_horario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 19
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c037_operacion_permiso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c037_operacion_permiso` (
  `pk_num_operacion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'PREPARADO: PR; REVISADO: RE; APROBADO: AP; CONFORMADO: CO; ANULADO: AN;',
  `fk_rhc036_num_permiso` INT(11) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fec_operacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_operacion`),
  INDEX `fk_rhc036_num_permiso` (`fk_rhc036_num_permiso` ASC),
  INDEX ` fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  CONSTRAINT `rh_c037_operacion_permiso_ibfk_1`
    FOREIGN KEY (`fk_rhc036_num_permiso`)
    REFERENCES `SIACE`.`rh_c036_permiso` (`pk_num_permiso`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 38
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c038_hcm`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c038_hcm` (
  `pk_num_orden` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a006_num_miscelaneo_det_tipo_hcm` INT(11) NOT NULL,
  `fk_rhc040_num_institucion` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_det_rama` INT(11) NOT NULL,
  `fk_rhb002_num_solicitud` INT(11) NOT NULL,
  `fk_rhc015_num_carga` INT(11) NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `ind_servicio` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_medico` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_factura` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_reporte` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_planilla` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_otro` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_orden`),
  INDEX `fk_rh_c038_hcm_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_det_tipo_hcm` ASC),
  INDEX `fk_rh_c038_hcm_rh_c040_institucion1_idx` (`fk_rhc040_num_institucion` ASC),
  INDEX `fk_rh_c038_hcm_a006_miscelaneo_detalle2_idx` (`fk_a006_num_miscelaneo_det_rama` ASC),
  INDEX `fk_rh_c038_hcm_rh_b002_solicitud1_idx` (`fk_rhb002_num_solicitud` ASC),
  INDEX `fk_rh_c038_hcm_rh_c015_carga_familiar1_idx` (`fk_rhc015_num_carga` ASC),
  INDEX `fk_rh_c038_hcm_a018_seguridad_usuario1_idx1` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c038_hcm_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c038_hcm_rh_b002_solicitud1`
    FOREIGN KEY (`fk_rhb002_num_solicitud`)
    REFERENCES `SIACE`.`rh_b002_solicitud` (`pk_num_solicitud`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c038_hcm_rh_c015_carga_familiar1`
    FOREIGN KEY (`fk_rhc015_num_carga`)
    REFERENCES `SIACE`.`rh_c015_carga_familiar` (`pk_num_carga`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c038_hcm_rh_c040_institucion1`
    FOREIGN KEY (`fk_rhc040_num_institucion`)
    REFERENCES `SIACE`.`rh_c040_institucion` (`pk_num_institucion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c039_beneficio_alimentacion_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c039_beneficio_alimentacion_detalle` (
  `pk_num_beneficio_alimentacion_detalle` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a001_num_organismo` INT(11) NOT NULL,
  `fk_rhc019_num_beneficio` INT(3) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `num_dia1` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia2` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia3` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia4` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia5` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia6` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia7` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia8` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia9` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia10` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia11` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia12` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia13` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia14` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia15` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia16` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia17` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia18` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia19` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia20` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia21` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia22` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia23` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia24` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia25` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia26` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia27` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia28` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT 'X',
  `num_dia29` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT 'X',
  `num_dia30` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT 'X',
  `num_dia31` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT 'X',
  `num_dias_periodo` INT(3) NOT NULL DEFAULT '0',
  `num_dias_pago` INT(3) NOT NULL DEFAULT '0',
  `num_dias_feriados` INT(3) NOT NULL DEFAULT '0',
  `num_dias_inactivos` INT(3) NOT NULL DEFAULT '0',
  `num_dias_descuento` INT(3) NOT NULL DEFAULT '0',
  `num_valor_pago` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_valor_descuento` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `num_valor_total` DECIMAL(18,6) NOT NULL DEFAULT '0.000000',
  `ind_estado` INT(1) NOT NULL DEFAULT '1' COMMENT '1:ABIERTO ; 0:CERRADO',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_beneficio_alimentacion_detalle`),
  INDEX `fk_a001_num_organismo` (`fk_a001_num_organismo` ASC),
  INDEX `fk_rhc019_num_beneficio` (`fk_rhc019_num_beneficio` ASC),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c039_beneficio_alimentacion_detalle_ibfk_1`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c039_beneficio_alimentacion_detalle_ibfk_2`
    FOREIGN KEY (`fk_rhc019_num_beneficio`)
    REFERENCES `SIACE`.`rh_c019_beneficio_alimentacion` (`pk_num_beneficio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c039_beneficio_alimentacion_detalle_ibfk_4`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 86
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c041_beneficio_alimentacion_eventos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c041_beneficio_alimentacion_eventos` (
  `pk_num_beneficio_alimentacion_eventos` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a001_num_organismo` INT(11) NOT NULL,
  `fk_rhc019_num_beneficio` INT(11) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fec_anio` YEAR NOT NULL,
  `fec_fecha` DATE NOT NULL,
  `fec_hora_salida` TIME NOT NULL,
  `fec_hora_entrada` TIME NOT NULL,
  `fec_total_horas` TIME NOT NULL,
  `fk_a006_num_miscelaneo_tipo_ausencia` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_motivo_ausencia` INT(11) NOT NULL,
  `txt_observaciones` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `num_flag_procesado` INT(1) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_beneficio_alimentacion_eventos`),
  INDEX `fk_a001_num_organismo` (`fk_a001_num_organismo` ASC),
  INDEX `fk_rhc019_num_beneficio` (`fk_rhc019_num_beneficio` ASC),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a006_num_miscelaneo_tipo_ausencia` (`fk_a006_num_miscelaneo_tipo_ausencia` ASC),
  INDEX ` fk_a006_num_miscelaneo_motivo_ausencia` (`fk_a006_num_miscelaneo_motivo_ausencia` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c041_beneficio_alimentacion_eventos_ibfk_1`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c041_beneficio_alimentacion_eventos_ibfk_2`
    FOREIGN KEY (`fk_rhc019_num_beneficio`)
    REFERENCES `SIACE`.`rh_c019_beneficio_alimentacion` (`pk_num_beneficio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c041_beneficio_alimentacion_eventos_ibfk_6`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c042_institucion_hcm`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c042_institucion_hcm` (
  `pk_num_institucion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_nombre_institucion` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_ubicacion` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_telefono1` VARCHAR(11) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_telefono2` VARCHAR(11) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a006_clase_centro` INT(11) NOT NULL,
  `fk_a006_tipo_centro` INT(11) NOT NULL,
  `flag_convenio_ces` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_institucion`),
  INDEX `fk_rh_c040_institucion_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_a006_clase_centro` (`fk_a006_clase_centro` ASC),
  INDEX `fk_a006_tipo_centro` (`fk_a006_tipo_centro` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c042_institucion_hcm_ibfk_2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c043_medico_hcm`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c043_medico_hcm` (
  `pk_num_medico_hcm` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a003_persona` INT(11) NOT NULL,
  `flag_convenio_ces` INT(11) NOT NULL,
  `flag_interno_ces` INT(11) NOT NULL,
  `fk_rhc040_institucion` INT(11) NULL DEFAULT NULL,
  `ind_telefono` VARCHAR(11) NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_medico_hcm`),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c043_medico_hcm_ibfk_1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c068_ayuda_especifica`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c068_ayuda_especifica` (
  `pk_num_ayuda_especifica` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb008_num_ayuda_global` INT(11) NOT NULL,
  `ind_descripcion_especifica` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_limite_esp` DECIMAL(18,6) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `flag_extensible` INT(1) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_ayuda_especifica`),
  INDEX `fk_rh_c068_ayuda_especifica_rh_b008_ayuda_global1_idx1` (`fk_rhb008_num_ayuda_global` ASC),
  INDEX `fk_rh_c068_ayuda_especifica_rh_b001_empleado1_idx1` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c068_ayuda_especifica_a018_seguridad_usuario1_idx1` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c068_ayuda_especifica_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c068_ayuda_especifica_rh_b008_ayuda_global1`
    FOREIGN KEY (`fk_rhb008_num_ayuda_global`)
    REFERENCES `SIACE`.`rh_b008_ayuda_global` (`pk_num_ayuda_global`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c044_extension_beneficio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c044_extension_beneficio` (
  `pk_num_extension` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhc068_ayuda_especifica` INT(11) NOT NULL,
  `fk_rhb008_ayuda_global` INT(11) NOT NULL,
  `fk_rhb001_empleado` INT(11) NOT NULL,
  `num_monto` DECIMAL(16,4) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_extension`),
  INDEX `fk_rhc068_ayuda_especifica` (`fk_rhc068_ayuda_especifica` ASC),
  INDEX `fk_rhb008_ayuda_global` (`fk_rhb008_ayuda_global` ASC),
  INDEX `fk_rhb001_empleado` (`fk_rhb001_empleado` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c044_extension_beneficio_ibfk_1`
    FOREIGN KEY (`fk_rhc068_ayuda_especifica`)
    REFERENCES `SIACE`.`rh_c068_ayuda_especifica` (`pk_num_ayuda_especifica`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c044_extension_beneficio_ibfk_2`
    FOREIGN KEY (`fk_rhb008_ayuda_global`)
    REFERENCES `SIACE`.`rh_b008_ayuda_global` (`pk_num_ayuda_global`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c044_extension_beneficio_ibfk_4`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c046_beneficio_medico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c046_beneficio_medico` (
  `pk_beneficio_medico` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a001_organismo` INT(11) NOT NULL,
  `fk_a006_tipo_solicitud` INT(11) NOT NULL,
  `fk_rhb001_empleado` INT(11) NOT NULL,
  `fk_a003_persona` INT(11) NOT NULL,
  `fk_rhb008_ayuda_global` INT(11) NOT NULL,
  `fk_rhc068_ayuda_especifica` INT(11) NOT NULL,
  `fk_a006_rama` INT(11) NOT NULL,
  `fk_rhc040_institucion` INT(11) NOT NULL,
  `fk_rhc047_medico_hcm` INT(11) NOT NULL,
  `fec_creacion` DATETIME NOT NULL,
  `flag_planilla_solicitud` INT(1) NOT NULL,
  `flag_informe_medico` INT(1) NOT NULL,
  `flag_factura` INT(1) NOT NULL,
  `flag_recipe_medico` INT(1) NOT NULL,
  `flag_otro` INT(1) NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT '1: PREPARADO, 2: REVISADO, 3: APROBADO, 4: ANULADO, 5: MODIFICADO',
  `num_monto` INT(1) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_beneficio_medico`),
  INDEX `fk_rh_c046_medico_beneficio_a018_seguridad_usuario1_idx1` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_a001_organismo` (`fk_a001_organismo` ASC),
  INDEX `fk_a006_tipo_solicitud` (`fk_a006_tipo_solicitud` ASC),
  INDEX `fk_rhb001_empleado` (`fk_rhb001_empleado` ASC),
  INDEX `fk_a003_persona` (`fk_a003_persona` ASC),
  INDEX `fk_rhb008_ayuda_global` (`fk_rhb008_ayuda_global` ASC),
  INDEX `fk_rhc068_ayuda_especifica` (`fk_rhc068_ayuda_especifica` ASC),
  INDEX `fk_a006_rama` (`fk_a006_rama` ASC),
  INDEX `fk_rhc040_institucion` (`fk_rhc040_institucion` ASC),
  INDEX `fk_rhc047_medico_hcm` (`fk_rhc047_medico_hcm` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c046_beneficio_medico_ibfk_1`
    FOREIGN KEY (`fk_a001_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c046_beneficio_medico_ibfk_4`
    FOREIGN KEY (`fk_rhb008_ayuda_global`)
    REFERENCES `SIACE`.`rh_b008_ayuda_global` (`pk_num_ayuda_global`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c046_beneficio_medico_ibfk_5`
    FOREIGN KEY (`fk_rhc068_ayuda_especifica`)
    REFERENCES `SIACE`.`rh_c068_ayuda_especifica` (`pk_num_ayuda_especifica`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c046_beneficio_medico_ibfk_7`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c046_beneficio_medico_ibfk_8`
    FOREIGN KEY (`fk_rhc040_institucion`)
    REFERENCES `SIACE`.`rh_c040_institucion` (`pk_num_institucion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c048_curso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c048_curso` (
  `pk_num_curso` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a006_num_miscelaneo_detalle_areaformacion` INT(11) NOT NULL,
  `ind_nombre_curso` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_duracion` TIME NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_curso`),
  INDEX `fk_rh_c048_curso_a006_miscelaneo_detalle_idx` USING BTREE (`fk_a006_num_miscelaneo_detalle_areaformacion` ASC),
  INDEX `fk_rh_c048_curso_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c048_curso_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c047_empleado_curso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c047_empleado_curso` (
  `pk_num_empleado_curso` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_rhc048_num_curso` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipocurso` INT(11) NOT NULL,
  `fk_rhc040_num_institucion` INT(11) NOT NULL,
  `num_horas` INT(4) NOT NULL,
  `fec_desde` DATE NOT NULL,
  `fec_hasta` DATE NOT NULL,
  `ind_periodo` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `txt_observaciones` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_empleado_curso`),
  INDEX `fk_rh_c047_empleado_curso_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c047_empleado_curso_rh_c048_curso1_idx` (`fk_rhc048_num_curso` ASC),
  INDEX `fk_rh_c047_empleado_curso_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle_tipocurso` ASC),
  INDEX `fk_rh_c047_empleado_curso_rh_c040_institucion1_idx` (`fk_rhc040_num_institucion` ASC),
  INDEX `fk_rh_c047_empleado_curso_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c047_empleado_curso_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c047_empleado_curso_rh_c040_institucion1`
    FOREIGN KEY (`fk_rhc040_num_institucion`)
    REFERENCES `SIACE`.`rh_c040_institucion` (`pk_num_institucion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c047_empleado_curso_rh_c048_curso1`
    FOREIGN KEY (`fk_rhc048_num_curso`)
    REFERENCES `SIACE`.`rh_c048_curso` (`pk_num_curso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c077_empleado_cese_reingreso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c077_empleado_cese_reingreso` (
  `pk_num_empleado_cese_reingreso` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipocr` INT(11) NOT NULL,
  `fk_a001_num_organismo` INT(11) NOT NULL,
  `fk_a004_num_dependencia` INT(11) NOT NULL,
  `ind_dependencia` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_rhc063_num_puestos` INT(11) NOT NULL,
  `ind_cargo` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_sueldo_actual` DECIMAL(18,6) NOT NULL,
  `ind_periodo` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_fecha` DATE NOT NULL,
  `num_anio_servicio` INT(2) NOT NULL,
  `num_edad` INT(3) NOT NULL,
  `fec_fecha_ingreso` DATE NOT NULL,
  `fk_nmb001_num_tipo_nomina` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipotrab` INT(11) NOT NULL,
  `fk_rhc032_num_motivo_cese` INT(11) NULL DEFAULT NULL,
  `fec_fecha_egreso` DATE NULL DEFAULT NULL,
  `txt_observacion_egreso` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_situacion_trabajo` INT(11) NOT NULL COMMENT '1: activo 0:inactivo',
  `ind_resolucion` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_empleado_cese_reingreso`),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `	fk_a006_num_miscelaneo_detalle_tipocr` (`fk_a006_num_miscelaneo_detalle_tipocr` ASC),
  INDEX `fk_a001_num_organismo` (`fk_a001_num_organismo` ASC),
  INDEX `fk_a004_num_dependencia` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_rhc063_num_puestos` (`fk_rhc063_num_puestos` ASC),
  INDEX `fk_nmb001_num_tipo_nomina` (`fk_nmb001_num_tipo_nomina` ASC),
  INDEX `	fk_a006_num_miscelaneo_detalle_tipotrab` (`fk_a006_num_miscelaneo_detalle_tipotrab` ASC),
  INDEX `fk_rhc032_num_motivo_cese` (`fk_rhc032_num_motivo_cese` ASC),
  INDEX `	fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c077_empleado_cese_reingreso_ibfk_3`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c077_empleado_cese_reingreso_ibfk_4`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c077_empleado_cese_reingreso_ibfk_5`
    FOREIGN KEY (`fk_rhc063_num_puestos`)
    REFERENCES `SIACE`.`rh_c063_puestos` (`pk_num_puestos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c077_empleado_cese_reingreso_ibfk_6`
    FOREIGN KEY (`fk_nmb001_num_tipo_nomina`)
    REFERENCES `SIACE`.`nm_b001_tipo_nomina` (`pk_num_tipo_nomina`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c077_empleado_cese_reingreso_ibfk_8`
    FOREIGN KEY (`fk_rhc032_num_motivo_cese`)
    REFERENCES `SIACE`.`rh_c032_motivo_cese` (`pk_num_motivo_cese`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c077_empleado_cese_reingreso_ibfk_9`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c049_operaciones_cese_reingreso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c049_operaciones_cese_reingreso` (
  `pk_num_operaciones_cr` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'PR: PREPARADO, CO: CONFORMADO, AP: APROBADO',
  `fk_rhc077_num_empleado_cese_reingreso` INT(11) NOT NULL,
  `fk_rhb0001_num_empleado` INT(11) NOT NULL,
  `num_flag_estatus` INT(11) NOT NULL,
  `fec_operacion` DATE NOT NULL,
  `txt_observaciones` MEDIUMTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_operaciones_cr`),
  INDEX `rh_c049_operaciones_cese_reingreso_ibfk_3` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `rh_c049_operaciones_cese_reingreso_ibfk_1` (`fk_rhc077_num_empleado_cese_reingreso` ASC),
  INDEX `rh_c049_operaciones_cese_reingreso_ibfk_2` (`fk_rhb0001_num_empleado` ASC),
  CONSTRAINT `rh_c049_operaciones_cese_reingreso_ibfk_1`
    FOREIGN KEY (`fk_rhc077_num_empleado_cese_reingreso`)
    REFERENCES `SIACE`.`rh_c077_empleado_cese_reingreso` (`pk_num_empleado_cese_reingreso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c049_operaciones_cese_reingreso_ibfk_3`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c050_encuesta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c050_encuesta` (
  `pk_num_encuesta` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a001_num_organismo` INT(11) NOT NULL,
  `ind_titulo_encuesta` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_fecha` DATE NOT NULL,
  `ind_observacion` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_plantilla` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_respuesta` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_encuesta`),
  INDEX `fk_rh_c050_encuesta_a001_organismo1_idx` (`fk_a001_num_organismo` ASC),
  INDEX `fk_rh_c050_encuesta_a018_seguridad_usuario1_idx1` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c050_encuesta_a001_organismo1`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c050_encuesta_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c051_pregunta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c051_pregunta` (
  `pk_num_pregunta` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a006_num_miscelaneo_detalle_tipo_contenido` INT(11) NOT NULL,
  `fk_rhc050_encuesta_num_encuesta` INT(11) NOT NULL,
  `ind_pregunta` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_tipo_cotenido` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_pregunta`),
  INDEX `fk_rh_c051_pregunta_rh_c050_encuesta1_idx` (`fk_rhc050_encuesta_num_encuesta` ASC),
  INDEX `fk_rh_c051_pregunta_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle_tipo_contenido` ASC),
  INDEX `fk_rh_c051_pregunta_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c051_pregunta_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c051_pregunta_rh_c050_encuesta1`
    FOREIGN KEY (`fk_rhc050_encuesta_num_encuesta`)
    REFERENCES `SIACE`.`rh_c050_encuesta` (`pk_num_encuesta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c053_evaluacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c053_evaluacion` (
  `pk_num_evaluacion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhc051_num_pregunta` INT(11) NOT NULL,
  `fk_rhc005_num_empleado_laboral` INT(11) NOT NULL,
  `fec_inicio` DATE NOT NULL,
  `fec_fin` DATE NOT NULL,
  `fec_cierre` DATE NOT NULL,
  `ind_estado` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_evaluacion`),
  INDEX `fk_rh_c053_evaluacion_rh_c051_pregunta1_idx` (`fk_rhc051_num_pregunta` ASC),
  INDEX `fk_rh_c053_evaluacion_rh_c005_empleado_laboral1_idx` (`fk_rhc005_num_empleado_laboral` ASC),
  INDEX `fk_rh_c053_evaluacion_a018_seguridad_usuario1_idx1` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c053_evaluacion_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c053_evaluacion_rh_c005_empleado_laboral1`
    FOREIGN KEY (`fk_rhc005_num_empleado_laboral`)
    REFERENCES `SIACE`.`rh_c005_empleado_laboral` (`pk_num_empleado_laboral`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c053_evaluacion_rh_c051_pregunta1`
    FOREIGN KEY (`fk_rhc051_num_pregunta`)
    REFERENCES `SIACE`.`rh_c051_pregunta` (`pk_num_pregunta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c054_hcm_bitacora`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c054_hcm_bitacora` (
  `pk_num_hcm_bitacora` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a006_miscelaneo` INT(11) NOT NULL,
  `fk_rhc046_beneficio_medico` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATE NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_hcm_bitacora`),
  INDEX `pk_num_hcm_bitacora` (`pk_num_hcm_bitacora` ASC),
  INDEX `fk_a006_miscelaneo` (`fk_a006_miscelaneo` ASC),
  INDEX `fk_rhc046_beneficio_medico` (`fk_rhc046_beneficio_medico` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c054_hcm_bitacora_ibfk_2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c054_hcm_bitacora_ibfk_3`
    FOREIGN KEY (`fk_rhc046_beneficio_medico`)
    REFERENCES `SIACE`.`rh_c046_beneficio_medico` (`pk_beneficio_medico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c055_empleado_cuenta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c055_empleado_cuenta` (
  `pk_num_cuenta` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_a026_num_banco` INT(11) NOT NULL,
  `a006_num_miscelaneo_detalle_tipo_cuenta` INT(11) NOT NULL,
  `ind_num_cuenta` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_aportes` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_cuenta`),
  INDEX `fk_rh_c055_empleado_cuenta_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c055_empleado_cuenta_a026_banco1_idx` (`fk_a026_num_banco` ASC),
  INDEX `fk_rh_c055_empleado_cuenta_a006_miscelaneo_detalle1_idx` (`a006_num_miscelaneo_detalle_tipo_cuenta` ASC),
  INDEX `fk_rh_c055_empleado_cuenta_a018_seguridad_usuario1_idx1` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c055_empleado_cuenta_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c055_empleado_cuenta_a026_banco1`
    FOREIGN KEY (`fk_a026_num_banco`)
    REFERENCES `SIACE`.`a026_banco` (`pk_num_banco`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c056_beneficio_utiles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c056_beneficio_utiles` (
  `pk_num_beneficio_utiles` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion_beneficio` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_monto_asignado` DECIMAL(18,6) NOT NULL,
  `ind_periodo_ini` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_periodo_fin` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_beneficio_utiles`),
  INDEX `fk_rh_c056_beneficio_utiles_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c056_beneficio_utiles_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c058_factura_hcm`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c058_factura_hcm` (
  `pk_num_factura_hcm` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhc046_beneficio_medico` INT(11) NOT NULL,
  `num_factura` INT(11) NOT NULL,
  `ind_descripcion` VARCHAR(300) NOT NULL,
  `num_monto` DECIMAL(18,6) NOT NULL,
  PRIMARY KEY (`pk_num_factura_hcm`),
  INDEX `fk_rhc046_beneficio_medico` (`fk_rhc046_beneficio_medico` ASC),
  INDEX `fk_rhc046_beneficio_medico_2` (`fk_rhc046_beneficio_medico` ASC),
  CONSTRAINT `rh_c058_factura_hcm_ibfk_1`
    FOREIGN KEY (`fk_rhc046_beneficio_medico`)
    REFERENCES `SIACE`.`rh_c046_beneficio_medico` (`pk_beneficio_medico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c059_empleado_nivelacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c059_empleado_nivelacion` (
  `pk_num_empleado_nivelacion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fec_fecha_registro` DATE NOT NULL,
  `fk_a001_num_organismo` INT(11) NOT NULL,
  `fk_a004_num_dependencia` INT(11) NOT NULL,
  `fk_a023_num_centro_costo` INT(11) NOT NULL,
  `fk_rhc063_num_puestos` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_paso` INT(11) NOT NULL,
  `fk_nmb001_num_tipo_nomina` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipoaccion` INT(11) NULL DEFAULT NULL,
  `ind_motivo` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_documento` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_responsable` INT(11) NULL DEFAULT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fec_fecha_hasta` DATE NULL DEFAULT NULL,
  `txt_observaciones` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_empleado_nivelacion`),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rhb001_num_empleado_responsable` (`fk_rhb001_num_empleado_responsable` ASC),
  INDEX ` fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c059_empleado_nivelacion_ibfk_3`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 62
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c060_empleado_nivelacion_historial`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c060_empleado_nivelacion_historial` (
  `pk_num_empleado_nivelacion_historial` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhc059_num_empleado_nivelacion` INT(11) NOT NULL,
  `fec_fecha_registro` DATE NOT NULL,
  `ind_organismo` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_dependencia` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_cargo` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_nivel_salarial` DECIMAL(18,6) NOT NULL,
  `ind_categoria_cargo` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_tipo_nomina` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_centro_costo` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_tipo_accion` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_estatus` INT(1) NOT NULL,
  `ind_responsable` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_empleado_nivelacion_historial`),
  INDEX `fk_rhc059_num_empleado_nivelacion` (`fk_rhc059_num_empleado_nivelacion` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c060_empleado_nivelacion_historial_ibfk_1`
    FOREIGN KEY (`fk_rhc059_num_empleado_nivelacion`)
    REFERENCES `SIACE`.`rh_c059_empleado_nivelacion` (`pk_num_empleado_nivelacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c060_empleado_nivelacion_historial_ibfk_2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 62
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c061_empleado_historial`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c061_empleado_historial` (
  `pk_num_empleado_historial` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `ind_periodo` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ingreso` DATE NOT NULL,
  `ind_organismo` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_dependencia` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_centro_costo` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_cargo` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_nivel_salarial` DECIMAL(18,6) NOT NULL,
  `ind_categoria_cargo` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_tipo_nomina` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_tipo_pago` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `ind_motivo_cese` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_egreso` DATE NULL DEFAULT NULL,
  `txt_obs_cese` TINYTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_tipo_trabajador` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_empleado_historial`),
  INDEX `fk_rh_c061_empleado_historial_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c061_empleado_historial_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c061_empleado_historial_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 114
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c066_tipo_contrato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c066_tipo_contrato` (
  `pk_num_tipo_contrato` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_tipo_contrato` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_nomina` INT(1) NOT NULL,
  `num_flag_vencimiento` INT(1) NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_tipo_contrato`),
  INDEX `fk_rh_c066_tipo_contrato_a018_seguridad_usuario1_idx1` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c066_tipo_contrato_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c067_horario_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c067_horario_detalle` (
  `pk_num_horario_detalle` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb007_num_horario` INT(11) NOT NULL,
  `num_dia` INT(2) NOT NULL,
  `num_flag_laborable` INT(1) NOT NULL,
  `fec_entrada1` TIME NOT NULL,
  `fec_salida1` TIME NOT NULL,
  `fec_entrada2` TIME NULL DEFAULT NULL,
  `fec_salida2` TIME NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_horario_detalle`),
  INDEX `fk_rh_c067_horario_detalle_rh_b007_horario_laboral1_idx1` (`fk_rhb007_num_horario` ASC),
  INDEX `fk_rh_c067_horario_detalle_a018_seguridad_usuario1_idx1` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c067_horario_detalle_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c067_horario_detalle_rh_b007_horario_laboral1`
    FOREIGN KEY (`fk_rhb007_num_horario`)
    REFERENCES `SIACE`.`rh_b007_horario_laboral` (`pk_num_horario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c069_feriados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c069_feriados` (
  `pk_num_feriado` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_anio` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_dia` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion_feriado` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_variable` INT(1) NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_feriado`),
  INDEX `fk_rh_c069_feriados_a018_seguridad_usuario1_idx1` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c069_feriados_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c072_sueldos_minimos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c072_sueldos_minimos` (
  `pk_num_sueldo_minimo` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_periodo_sueldo` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_monto_sueldo` DECIMAL(18,6) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_sueldo_minimo`),
  INDEX `fk_rh_c072_sueldos_minimos_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c072_sueldos_minimos_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c073_plantillas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c073_plantillas` (
  `pk_num_plantillas` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion_plantilla` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_plantillas`),
  INDEX `fk_rh_c073_plantillas_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `fk_rh_c073_plantillas_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c074_preguntas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c074_preguntas` (
  `pk_num_preguntas` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_descripcion_preguntas` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a006_num_miscelaneo_detalle_area` INT(11) NOT NULL,
  `num_valor_minimo` INT(4) NOT NULL,
  `num_valor_maximo` INT(4) NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_preguntas`),
  INDEX `fk_rh_c074_preguntas_a018_seguridad_usuario1_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c075_plantilla_preguntas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c075_plantilla_preguntas` (
  `fk_rhc073_num_plantillas` INT(11) NOT NULL,
  `fk_rhc074_num_preguntas` INT(11) NOT NULL,
  PRIMARY KEY (`fk_rhc073_num_plantillas`, `fk_rhc074_num_preguntas`),
  INDEX `fk_rh_c075_plantilla_preguntas_rh_c073_plantillas1_idx` (`fk_rhc073_num_plantillas` ASC),
  INDEX `fk_rh_c075_plantilla_preguntas_rh_c074_preguntas1_idx` (`fk_rhc074_num_preguntas` ASC),
  CONSTRAINT `fk_rh_c075_plantilla_preguntas_rh_c073_plantillas1`
    FOREIGN KEY (`fk_rhc073_num_plantillas`)
    REFERENCES `SIACE`.`rh_c073_plantillas` (`pk_num_plantillas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c075_plantilla_preguntas_rh_c074_preguntas1`
    FOREIGN KEY (`fk_rhc074_num_preguntas`)
    REFERENCES `SIACE`.`rh_c074_preguntas` (`pk_num_preguntas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c076_empleado_organizacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c076_empleado_organizacion` (
  `pk_num_empleado_organizacion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_a001_num_organismo` INT(11) NOT NULL,
  `fk_a004_num_dependencia` INT(11) NOT NULL,
  `fk_a004_num_dependencia_tmp` INT(11) NULL DEFAULT NULL,
  `fk_a023_num_centro_costo` INT(11) NOT NULL,
  `fk_nmb001_num_tipo_nomina` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipopago` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipotrabajador` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_empleado_organizacion`),
  INDEX `fk_rh_c076_empleado_organizacion_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rh_c076_empleado_organizacion_a001_organismo1_idx` (`fk_a001_num_organismo` ASC),
  INDEX `fk_rh_c076_empleado_organizacion_a004_dependencia1_idx` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_rh_c076_empleado_organizacion_a004_dependencia2_idx` (`fk_a004_num_dependencia_tmp` ASC),
  INDEX `fk_rh_c076_empleado_organizacion_a023_centro_costo1_idx` (`fk_a023_num_centro_costo` ASC),
  INDEX `fk_rh_c076_empleado_organizacion_a006_miscelaneo_detalle1_idx` (`fk_a006_num_miscelaneo_detalle_tipopago` ASC),
  INDEX `fk_rh_c076_empleado_organizacion_a006_miscelaneo_detalle2_idx` (`fk_a006_num_miscelaneo_detalle_tipotrabajador` ASC),
  INDEX `fk_rh_c076_empleado_organizacion_a018_seguridad_usuario1_idx1` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_rh_c076_empleado_organizacion_nm_b001_tipo_nomina1_idx` (`fk_nmb001_num_tipo_nomina` ASC),
  CONSTRAINT `fk_rh_c076_empleado_organizacion_a001_organismo1`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c076_empleado_organizacion_a004_dependencia1`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c076_empleado_organizacion_a004_dependencia2`
    FOREIGN KEY (`fk_a004_num_dependencia_tmp`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c076_empleado_organizacion_a018_seguridad_usuario1`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c076_empleado_organizacion_a023_centro_costo1`
    FOREIGN KEY (`fk_a023_num_centro_costo`)
    REFERENCES `SIACE`.`a023_centro_costo` (`pk_num_centro_costo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rh_c076_empleado_organizacion_nm_b001_tipo_nomina1`
    FOREIGN KEY (`fk_nmb001_num_tipo_nomina`)
    REFERENCES `SIACE`.`nm_b001_tipo_nomina` (`pk_num_tipo_nomina`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 35
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c079_vacacion_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c079_vacacion_detalle` (
  `pk_num_vacacion_detalle` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb009_num_solicitud_vacacion` INT(11) NOT NULL,
  `fk_rhc081_num_periodo_vacacion` INT(11) NOT NULL,
  `num_dias_derecho` INT(11) NOT NULL,
  `num_dias_pendientes` INT(11) NOT NULL,
  `num_dias_usados` INT(11) NOT NULL,
  `num_dias_detalle` INT(11) NOT NULL,
  `fec_inicial` DATE NOT NULL,
  `fec_final` DATE NOT NULL,
  `fec_incorporacion` DATE NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_vacacion_detalle`),
  INDEX `fk_rhb009_num_solicitud_vacacion` (`fk_rhb009_num_solicitud_vacacion` ASC),
  INDEX `	fk_rhc081_num_periodo_vacacion` (`fk_rhc081_num_periodo_vacacion` ASC),
  CONSTRAINT `rh_c079_vacacion_detalle_ibfk_1`
    FOREIGN KEY (`fk_rhb009_num_solicitud_vacacion`)
    REFERENCES `SIACE`.`rh_b009_vacacion_solicitud` (`pk_num_solicitud_vacacion`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c080_operacion_vacacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c080_operacion_vacacion` (
  `pk_num_operacion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'PREPARADO: PR; REVISADO: RE; APROBADO: AP; CONFORMADO: CO; ANULADO: AN;',
  `fk_rhb009_num_solicitud_vacacion` INT(11) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_rhc063_num_puesto` INT(11) NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fec_operacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_operacion`),
  INDEX `fk_rhb009_num_solicitud_vacacion` (`fk_rhb009_num_solicitud_vacacion` ASC),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  CONSTRAINT `rh_c080_operacion_vacacion_ibfk_1`
    FOREIGN KEY (`fk_rhb009_num_solicitud_vacacion`)
    REFERENCES `SIACE`.`rh_b009_vacacion_solicitud` (`pk_num_solicitud_vacacion`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c081_periodo_vacacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c081_periodo_vacacion` (
  `pk_num_periodo` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `num_anio` INT(11) NOT NULL,
  `num_mes` INT(11) NOT NULL,
  `num_dias_derecho` INT(11) NOT NULL,
  `num_dias_gozados` INT(11) NOT NULL,
  `num_dias_interrumpidos` INT(11) NOT NULL,
  `num_total_utilizados` INT(11) NOT NULL,
  `num_pendientes` INT(11) NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_periodo`),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  CONSTRAINT `rh_c081_periodo_vacacion_ibfk_2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 228
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c082_vacacion_utilizacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c082_vacacion_utilizacion` (
  `pk_num_vacacion_utilizacion` INT(11) NOT NULL AUTO_INCREMENT,
  `num_dias_utiles` INT(11) NOT NULL,
  `fec_inicio` DATE NOT NULL,
  `fec_fin` DATE NOT NULL,
  `fk_a006_tipo_vacacion` INT(11) NOT NULL,
  `fk_rhb009_num_solicitud_vacacion` INT(11) NULL DEFAULT NULL,
  `fk_rhc081_num_periodo` INT(11) NOT NULL,
  `num_flag_solicitud` INT(11) NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  PRIMARY KEY (`pk_num_vacacion_utilizacion`),
  INDEX `fk_a006_tipo_vacacion` (`fk_a006_tipo_vacacion` ASC),
  INDEX `fk_rhb009_num_solicitud_vacacion` (`fk_rhb009_num_solicitud_vacacion` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c082_vacacion_utilizacion_ibfk_3`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 117
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c083_montos_jubilacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c083_montos_jubilacion` (
  `pk_num_monto_jubilacion` INT(11) NOT NULL AUTO_INCREMENT,
  `num_ultimo_sueldo` INT(11) NOT NULL,
  `num_total_sueldo` FLOAT NOT NULL,
  `num_total_primas` FLOAT NOT NULL,
  `num_sueldo_base` FLOAT NOT NULL,
  `num_porcentaje` FLOAT NOT NULL,
  `num_coeficiente` FLOAT NOT NULL,
  `num_monto_jubilacion` FLOAT NOT NULL,
  `fk_rhb010_num_jubilacion` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_monto_jubilacion`),
  INDEX `fk_rhb010_num_jubilacion` (`fk_rhb010_num_jubilacion` ASC),
  CONSTRAINT `rh_c083_montos_jubilacion_ibfk_1`
    FOREIGN KEY (`fk_rhb010_num_jubilacion`)
    REFERENCES `SIACE`.`rh_b010_jubilacion` (`pk_num_jubilacion`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c084_relacion_sueldo_jubilacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c084_relacion_sueldo_jubilacion` (
  `pk_num_relacion_sueldo_jubilacion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb010_num_jubilacion` INT(11) NOT NULL,
  `fk_rhb003_num_proceso_pension` INT(11) NULL DEFAULT NULL,
  `num_anio_periodo` INT(11) NOT NULL,
  `ind_mes_periodo` INT(11) NOT NULL,
  `num_monto` INT(11) NOT NULL,
  `fk_nmb002_num_concepto` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_relacion_sueldo_jubilacion`),
  INDEX `fk_rhb010_num_jubilacion` (`fk_rhb010_num_jubilacion` ASC),
  INDEX `fk_rhb003_num_proceso_pension` (`fk_rhb003_num_proceso_pension` ASC),
  INDEX `fk_nmb002_num_concepto` (`fk_nmb002_num_concepto` ASC),
  CONSTRAINT `rh_c084_relacion_sueldo_jubilacion_ibfk_1`
    FOREIGN KEY (`fk_rhb010_num_jubilacion`)
    REFERENCES `SIACE`.`rh_b010_jubilacion` (`pk_num_jubilacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c084_relacion_sueldo_jubilacion_ibfk_2`
    FOREIGN KEY (`fk_rhb003_num_proceso_pension`)
    REFERENCES `SIACE`.`rh_b003_pension` (`pk_num_proceso_pension`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c084_relacion_sueldo_jubilacion_ibfk_3`
    FOREIGN KEY (`fk_nmb002_num_concepto`)
    REFERENCES `SIACE`.`nm_b002_concepto` (`pk_num_concepto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c085_operacion_jubilacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c085_operacion_jubilacion` (
  `pk_num_operacion_jubilacion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb010_num_jubilacion` INT(11) NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'PREPARADO: PR; REVISADO: RE; APROBADO: AP; CONFORMADO: CO; ANULADO: AN;',
  `ind_observacion` VARCHAR(600) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_rhc063_num_puesto` INT(11) NOT NULL,
  `fec_operacion` DATETIME NOT NULL,
  `num_estatus` INT(1) NOT NULL,
  PRIMARY KEY (`pk_num_operacion_jubilacion`),
  INDEX `fk_rhb010_num_jubilacion` (`fk_rhb010_num_jubilacion` ASC),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rhc063_num_puesto` (`fk_rhc063_num_puesto` ASC),
  CONSTRAINT `rh_c085_operacion_jubilacion_ibfk_1`
    FOREIGN KEY (`fk_rhb010_num_jubilacion`)
    REFERENCES `SIACE`.`rh_b010_jubilacion` (`pk_num_jubilacion`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `rh_c085_operacion_jubilacion_ibfk_3`
    FOREIGN KEY (`fk_rhc063_num_puesto`)
    REFERENCES `SIACE`.`rh_c063_puestos` (`pk_num_puestos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c086_operaciones_pension`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c086_operaciones_pension` (
  `pk_num_operaciones_pension` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'PR: PREPARADO, CO: CONFORMADO, AP: APROBADO',
  `fk_rhb003_num_pension` INT(11) NOT NULL,
  `fk_rhb0001_num_empleado` INT(11) NOT NULL,
  `num_flag_estatus` INT(11) NOT NULL,
  `fec_operacion` DATE NOT NULL,
  `txt_observaciones` MEDIUMTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_operaciones_pension`),
  INDEX `fk_rhb003_num_pension` (`fk_rhb003_num_pension` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c086_operaciones_pension_ibfk_1`
    FOREIGN KEY (`fk_rhb003_num_pension`)
    REFERENCES `SIACE`.`rh_b003_pension` (`pk_num_proceso_pension`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c086_operaciones_pension_ibfk_2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c087_empleado_antecedentes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c087_empleado_antecedentes` (
  `pk_num_empleado_antecedentes` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_rhb003_num_proceso_pension` INT(11) NOT NULL,
  `ind_organismo` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_fecha_ingreso` DATE NOT NULL,
  `fec_fecha_egreso` DATE NOT NULL,
  `ind_proceso` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'J: Jubilacion P:Pensionado',
  `num_anio` INT(11) NOT NULL,
  `num_meses` INT(11) NOT NULL,
  `num_dias` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_empleado_antecedentes`),
  INDEX `	fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_rh_c087_empleado_antecedentes_rh_b003_pension1_idx` (`fk_rhb003_num_proceso_pension` ASC),
  CONSTRAINT `fk_rh_c087_empleado_antecedentes_rh_b003_pension1`
    FOREIGN KEY (`fk_rhb003_num_proceso_pension`)
    REFERENCES `SIACE`.`rh_b003_pension` (`pk_num_proceso_pension`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c087_empleado_antecedentes_ibfk_2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c088_beneficiarios_pension`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c088_beneficiarios_pension` (
  `pk_num_beneficiario_pension` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhb003_num_proceso_pension` INT(11) NOT NULL,
  `fk_a003_num_persona` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_parentesco` INT(11) NOT NULL,
  `ind_parentesco` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_nacimiento` DATE NOT NULL,
  `fk_a006_num_miscelaneo_detalle_sexo` INT(11) NOT NULL,
  `ind_sexo` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_flag_discapacidad` INT(11) NULL DEFAULT NULL,
  `num_flag_estudia` INT(11) NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_beneficiario_pension`),
  INDEX `fk_rhb003_num_proceso_pension` (`fk_rhb003_num_proceso_pension` ASC),
  INDEX `fk_a003_num_persona` (`fk_a003_num_persona` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_parentesco` (`fk_a006_num_miscelaneo_detalle_parentesco` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_sexo` (`fk_a006_num_miscelaneo_detalle_sexo` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c088_beneficiarios_pension_ibfk_1`
    FOREIGN KEY (`fk_rhb003_num_proceso_pension`)
    REFERENCES `SIACE`.`rh_b003_pension` (`pk_num_proceso_pension`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c088_beneficiarios_pension_ibfk_2`
    FOREIGN KEY (`fk_a003_num_persona`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c088_beneficiarios_pension_ibfk_5`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c089_beneficio_utiles_requisito`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c089_beneficio_utiles_requisito` (
  `pk_num_beneficio_utiles_requisito` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhc056_num_beneficio_utiles` INT(11) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fec_fecha` DATE NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_beneficio_utiles_requisito`),
  INDEX `fk_rhc056_num_beneficio_utiles` (`fk_rhc056_num_beneficio_utiles` ASC),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c090_beneficio_utiles_requisito_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c090_beneficio_utiles_requisito_detalle` (
  `pk_num_requisito_detalle` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhc089_beneficio_utiles_requisitos` INT(11) NOT NULL,
  `fk_rhc015_num_carga_familiar` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_requisito` INT(11) NOT NULL,
  `num_flag_entregado` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_requisito_detalle`),
  INDEX `fk_rhc089_beneficio_utiles_requisitos` (`fk_rhc089_beneficio_utiles_requisitos` ASC),
  INDEX `	fk_rhc015_num_carga_familiar` (`fk_rhc015_num_carga_familiar` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_requisito` (`fk_a006_num_miscelaneo_detalle_requisito` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c091_utiles_beneficio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c091_utiles_beneficio` (
  `pk_num_utiles_beneficio` INT(11) NOT NULL AUTO_INCREMENT,
  `num_orden` VARCHAR(6) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_rhc056_num_beneficio_utiles` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_asignacion` INT(11) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fec_fecha` DATE NOT NULL,
  `lg_b022_num_proveedor` INT(11) NULL DEFAULT NULL,
  `num_monto` DECIMAL(18,6) NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_utiles_beneficio`),
  INDEX `fk_rhc056_num_beneficio_utiles` (`fk_rhc056_num_beneficio_utiles` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_asignacion` (`fk_a006_num_miscelaneo_detalle_asignacion` ASC),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `lg_b022_num_proveedor` (`lg_b022_num_proveedor` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c092_utiles_beneficarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c092_utiles_beneficarios` (
  `pk_num_utiles_beneficiarios` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhc091_utiles_beneficio` INT(11) NOT NULL,
  `fk_c015_num_carga_familiar` INT(11) NOT NULL,
  `num_monto_asignado` DECIMAL(18,6) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_utiles_beneficiarios`),
  INDEX `fk_rhc091_utiles_beneficio` (`fk_rhc091_utiles_beneficio` ASC),
  INDEX `fk_c015_num_carga_familiar` (`fk_c015_num_carga_familiar` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c093_utiles_asignacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c093_utiles_asignacion` (
  `pk_num_utiles_asignacion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rh091_utiles_beneficio` INT(11) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_rhc015_num_carga_familiar` INT(11) NOT NULL,
  `fk_rhc064_nivel_grado_instruccion` INT(11) NOT NULL,
  `fk_rhc056_num_beneficio_utiles` INT(11) NOT NULL,
  `num_anio` YEAR NOT NULL,
  `num_factura` INT(11) NOT NULL,
  `fec_fecha_factura` DATE NOT NULL,
  `num_excento` DECIMAL(18,6) NOT NULL,
  `num_base_imponible` DECIMAL(18,6) NOT NULL,
  `num_iva` DECIMAL(18,6) NOT NULL,
  `num_monto` DECIMAL(18,6) NOT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_utiles_asignacion`),
  INDEX `fk_rh091_utiles_beneficio` (`fk_rh091_utiles_beneficio` ASC),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_rhc015_num_carga_familiar` (`fk_rhc015_num_carga_familiar` ASC),
  INDEX `fk_rhc064_nivel_grado_instruccion` (`fk_rhc064_nivel_grado_instruccion` ASC),
  INDEX `fk_rhc056_num_beneficio_utiles` (`fk_rhc056_num_beneficio_utiles` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c094_utiles_asignacion_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c094_utiles_asignacion_detalle` (
  `pk_num_utiles_asignacion_detalle` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhc093_utiles_asignacion` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_utiles` INT(11) NOT NULL,
  `ind_descripcion_utiles` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_cantidad` INT(11) NOT NULL,
  `ind_exento` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_descuento` DECIMAL(18,6) NOT NULL,
  `nom_monto` DECIMAL(18,6) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_utiles_asignacion_detalle`),
  INDEX `fk_rhc093_utiles_asignacion` (`fk_rhc093_utiles_asignacion` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_utiles` (`fk_a006_num_miscelaneo_detalle_utiles` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c095_operaciones_beneficios_utiles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c095_operaciones_beneficios_utiles` (
  `pk_num_operaciones_beneficios_utiles` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'PR: PREPARADO, RV: REVISADO, AP: APROBADO',
  `fk_rhc091_num_utiles_beneficio` INT(11) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `num_flag_estatus` INT(11) NOT NULL,
  `fec_operacion` DATE NOT NULL,
  `txt_observaciones` MEDIUMTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_operaciones_beneficios_utiles`),
  INDEX `fk_rhc091_num_utiles_beneficio` (`fk_rhc091_num_utiles_beneficio` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  CONSTRAINT `rh_c095_operaciones_beneficios_utiles_ibfk_1`
    FOREIGN KEY (`fk_rhc091_num_utiles_beneficio`)
    REFERENCES `SIACE`.`rh_c091_utiles_beneficio` (`pk_num_utiles_beneficio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c095_operaciones_beneficios_utiles_ibfk_2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c095_operaciones_beneficios_utiles_ibfk_3`
    FOREIGN KEY (`fk_rhb001_num_empleado`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`pk_num_empleado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c096_operaciones_asignaciones_utiles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c096_operaciones_asignaciones_utiles` (
  `pk_num_operaciones_asignaciones_utiles` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'PR: PREPARADO, RV: REVISADO , AP: APROBADO',
  `fk_rhc093_utiles_asignacion` INT(11) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `num_flag_estatus` INT(11) NOT NULL,
  `fec_operacion` DATE NOT NULL,
  `txt_observaciones` MEDIUMTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_operaciones_asignaciones_utiles`),
  INDEX `fk_rhc093_utiles_asignacion` (`fk_rhc093_utiles_asignacion` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  CONSTRAINT `rh_c096_operaciones_asignaciones_utiles_ibfk_1`
    FOREIGN KEY (`fk_rhc093_utiles_asignacion`)
    REFERENCES `SIACE`.`rh_c093_utiles_asignacion` (`pk_num_utiles_asignacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c096_operaciones_asignaciones_utiles_ibfk_2`
    FOREIGN KEY (`fk_rhb001_num_empleado`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`pk_num_empleado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c096_operaciones_asignaciones_utiles_ibfk_3`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c097_capacitacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c097_capacitacion` (
  `pk_num_capacitacion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a001_num_organismo` INT(11) NOT NULL,
  `fec_anio` YEAR NULL DEFAULT NULL,
  `fk_rhb001_empleado_solicitante` INT(11) NOT NULL,
  `fk_rhc048_num_curso` INT(11) NOT NULL,
  `fk_rhc040_centro_estudio` INT(11) NOT NULL,
  `fk_a010_num_ciudad` INT(11) NOT NULL,
  `fk_a006_num_miscelnaeo_detalle_tipocap` INT(11) NOT NULL,
  `fk_a006_num_miscelnaeo_detalle_modalidad` INT(11) NOT NULL,
  `fk_a006_num_miscelnaeo_detalle_origen` INT(11) NOT NULL,
  `ind_periodo` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL DEFAULT '0000-00',
  `num_vacantes` INT(11) NOT NULL,
  `num_participantes` INT(11) NULL DEFAULT NULL,
  `fec_fecha_inicio` DATE NOT NULL,
  `fec_fecha_termino` DATE NOT NULL,
  `ind_expositor` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_telefonos` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `flag_costos` INT(11) NOT NULL,
  `txt_observaciones` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_monto_estimado` DECIMAL(18,6) NULL DEFAULT NULL,
  `num_monto_asumido` DECIMAL(18,6) NULL DEFAULT NULL,
  `num_total_dias` INT(11) NULL DEFAULT NULL,
  `ind_total_horas` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_estado` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_fecha_creado` DATE NOT NULL,
  `fk_rhb001_num_empleado_creado` INT(11) NOT NULL,
  `fec_fecha_aprobado` DATE NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_aprobado` INT(11) NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_capacitacion`),
  INDEX `fk_a001_num_organismo` (`fk_a001_num_organismo` ASC),
  INDEX `fk_rhb001_empleado_solicitante` (`fk_rhb001_empleado_solicitante` ASC),
  INDEX `fk_rhc048_num_curso` (`fk_rhc048_num_curso` ASC),
  INDEX `fk_rhc040_centro_estudio` (`fk_rhc040_centro_estudio` ASC),
  INDEX `fk_a010_num_ciudad` (`fk_a010_num_ciudad` ASC),
  INDEX `fk_a006_num_miscelnaeo_detalle_tipocap` (`fk_a006_num_miscelnaeo_detalle_tipocap` ASC),
  INDEX `fk_a006_num_miscelnaeo_detalle_modalidad` (`fk_a006_num_miscelnaeo_detalle_modalidad` ASC),
  INDEX `fk_a006_num_miscelnaeo_detalle_origen` (`fk_a006_num_miscelnaeo_detalle_origen` ASC),
  INDEX `fk_rhb001_num_empleado_creado` (`fk_rhb001_num_empleado_creado` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c097_capacitacion_ibfk_1`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c097_capacitacion_ibfk_10`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c097_capacitacion_ibfk_2`
    FOREIGN KEY (`fk_rhb001_empleado_solicitante`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`pk_num_empleado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c097_capacitacion_ibfk_3`
    FOREIGN KEY (`fk_rhc048_num_curso`)
    REFERENCES `SIACE`.`rh_c048_curso` (`pk_num_curso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c097_capacitacion_ibfk_4`
    FOREIGN KEY (`fk_rhc040_centro_estudio`)
    REFERENCES `SIACE`.`rh_c040_institucion` (`pk_num_institucion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c097_capacitacion_ibfk_5`
    FOREIGN KEY (`fk_a010_num_ciudad`)
    REFERENCES `SIACE`.`a010_ciudad` (`pk_num_ciudad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c097_capacitacion_ibfk_9`
    FOREIGN KEY (`fk_rhb001_num_empleado_creado`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`pk_num_empleado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c098_capacitacion_fundamentos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c098_capacitacion_fundamentos` (
  `pk_num_capacitacion_fundamentos` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhc097_num_capacitacion` INT(11) NOT NULL,
  `txt_fundamentacion_1` MEDIUMTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `txt_fundamentacion_2` MEDIUMTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `txt_fundamentacion_3` MEDIUMTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `txt_fundamentacion_4` MEDIUMTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `txt_fundamentacion_5` MEDIUMTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `txt_fundamentacion_6` MEDIUMTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `txt_fundamentacion_7` MEDIUMTEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_capacitacion_fundamentos`),
  INDEX `fk_rhc097_num_capacitacion` (`fk_rhc097_num_capacitacion` ASC),
  CONSTRAINT `rh_c098_capacitacion_fundamentos_ibfk_1`
    FOREIGN KEY (`fk_rhc097_num_capacitacion`)
    REFERENCES `SIACE`.`rh_c097_capacitacion` (`pk_num_capacitacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c099_capacitacion_empleados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c099_capacitacion_empleados` (
  `pk_num_capacitacion_empleado` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhc097_num_capacitacion` INT(11) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NULL DEFAULT NULL,
  `fk_a004_num_dependencia` INT(11) NULL DEFAULT NULL,
  `num_asistencias` INT(11) NULL DEFAULT NULL,
  `num_dias_asistidos` INT(11) NULL DEFAULT NULL,
  `ind_horas_asistencia` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_calificacion` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_costo_individual` DECIMAL(18,6) NULL DEFAULT NULL,
  `num_importe_gastos` DECIMAL(18,6) NULL DEFAULT NULL,
  `flag_aprobado` INT(11) NULL DEFAULT NULL COMMENT '1: APROBADO  0:REPROBADO',
  `fec_ultima_modificacion` DATETIME NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_capacitacion_empleado`),
  INDEX `fk_rhc097_num_capacitacion` (`fk_rhc097_num_capacitacion` ASC),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a004_num_dependencia` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c099_capacitacion_empleados_ibfk_1`
    FOREIGN KEY (`fk_rhc097_num_capacitacion`)
    REFERENCES `SIACE`.`rh_c097_capacitacion` (`pk_num_capacitacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c099_capacitacion_empleados_ibfk_2`
    FOREIGN KEY (`fk_rhb001_num_empleado`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`pk_num_empleado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c099_capacitacion_empleados_ibfk_3`
    FOREIGN KEY (`fk_a004_num_dependencia`)
    REFERENCES `SIACE`.`a004_dependencia` (`pk_num_dependencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c099_capacitacion_empleados_ibfk_4`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c100_capacitacion_gastos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c100_capacitacion_gastos` (
  `pk_num_capacitacion_gastos` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhc097_num_capacitacion` INT(11) NOT NULL,
  `ind_detalle` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_fecha` DATE NULL DEFAULT NULL,
  `num_sub_total` DECIMAL(18,6) NULL DEFAULT NULL,
  `num_impuestos` DECIMAL(18,6) NULL DEFAULT NULL,
  `num_total` DECIMAL(18,6) NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_capacitacion_gastos`),
  INDEX `fk_rhc097_num_capacitacion` (`fk_rhc097_num_capacitacion` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c100_capacitacion_gastos_ibfk_1`
    FOREIGN KEY (`fk_rhc097_num_capacitacion`)
    REFERENCES `SIACE`.`rh_c097_capacitacion` (`pk_num_capacitacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c100_capacitacion_gastos_ibfk_2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c101_capacitacion_horario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c101_capacitacion_horario` (
  `pk_num_capacitacion_horario` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhc097_num_capacitacion` INT(11) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NULL DEFAULT NULL,
  `fec_fecha` DATE NULL DEFAULT NULL,
  `fec_hora_ini` TIME NULL DEFAULT NULL,
  `fec_hora_fin` TIME NULL DEFAULT NULL,
  `ind_total_horas` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NULL DEFAULT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_capacitacion_horario`),
  INDEX `fk_rhc097_num_capacitacion` (`fk_rhc097_num_capacitacion` ASC),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c101_capacitacion_horario_ibfk_1`
    FOREIGN KEY (`fk_rhc097_num_capacitacion`)
    REFERENCES `SIACE`.`rh_c097_capacitacion` (`pk_num_capacitacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c101_capacitacion_horario_ibfk_2`
    FOREIGN KEY (`fk_rhb001_num_empleado`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`pk_num_empleado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c101_capacitacion_horario_ibfk_3`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c102_retencion_judicial`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c102_retencion_judicial` (
  `pk_num_retencion` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_a001_num_organismo` INT(11) NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fk_a003_num_persona_demandante` INT(11) NOT NULL,
  `ind_expediente` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_fecha_resolucion` DATE NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tiporetencion` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipopago` INT(11) NOT NULL,
  `ind_juzgado` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `txt_observaciones` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_estatus` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_retencion`),
  INDEX `fk_a001_num_organismo` (`fk_a001_num_organismo` ASC),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a003_num_persona_demandante` (`fk_a003_num_persona_demandante` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_tiporetencion` (`fk_a006_num_miscelaneo_detalle_tiporetencion` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_tipopago` (`fk_a006_num_miscelaneo_detalle_tipopago` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c102_retencion_judicial_ibfk_1`
    FOREIGN KEY (`fk_a001_num_organismo`)
    REFERENCES `SIACE`.`a001_organismo` (`pk_num_organismo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c102_retencion_judicial_ibfk_2`
    FOREIGN KEY (`fk_rhb001_num_empleado`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`pk_num_empleado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c102_retencion_judicial_ibfk_3`
    FOREIGN KEY (`fk_a003_num_persona_demandante`)
    REFERENCES `SIACE`.`a003_persona` (`pk_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c102_retencion_judicial_ibfk_4`
    FOREIGN KEY (`fk_a006_num_miscelaneo_detalle_tiporetencion`)
    REFERENCES `SIACE`.`a006_miscelaneo_detalle` (`pk_num_miscelaneo_detalle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c102_retencion_judicial_ibfk_5`
    FOREIGN KEY (`fk_a006_num_miscelaneo_detalle_tipopago`)
    REFERENCES `SIACE`.`a006_miscelaneo_detalle` (`pk_num_miscelaneo_detalle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c102_retencion_judicial_ibfk_6`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c103_retencion_judicial_conceptos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c103_retencion_judicial_conceptos` (
  `pk_num_retencion_conceptos` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_rhc102_num_retencion` INT(11) NOT NULL,
  `fk_a006_num_miscelaneo_detalle_tipodescuento` INT(11) NOT NULL,
  `ind_tipo_descuento` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_nmb002_num_concepto` INT(11) NOT NULL,
  `ind_concepto` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `num_descuento` DECIMAL(18,6) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_retencion_conceptos`),
  INDEX `fk_rhc102_num_retencion` (`fk_rhc102_num_retencion` ASC),
  INDEX `fk_a006_num_miscelaneo_detalle_tipodescuento` (`fk_a006_num_miscelaneo_detalle_tipodescuento` ASC),
  INDEX `fk_nmb002_num_concepto` (`fk_nmb002_num_concepto` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c103_retencion_judicial_conceptos_ibfk_1`
    FOREIGN KEY (`fk_rhc102_num_retencion`)
    REFERENCES `SIACE`.`rh_c102_retencion_judicial` (`pk_num_retencion`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c103_retencion_judicial_conceptos_ibfk_2`
    FOREIGN KEY (`fk_a006_num_miscelaneo_detalle_tipodescuento`)
    REFERENCES `SIACE`.`a006_miscelaneo_detalle` (`pk_num_miscelaneo_detalle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c103_retencion_judicial_conceptos_ibfk_3`
    FOREIGN KEY (`fk_nmb002_num_concepto`)
    REFERENCES `SIACE`.`nm_b002_concepto` (`pk_num_concepto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c103_retencion_judicial_conceptos_ibfk_4`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c104_transferencia_eventos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c104_transferencia_eventos` (
  `pk_num_transferencia` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'clave primaria',
  `fec_fecha` DATE NOT NULL COMMENT 'fecha del evento',
  `fec_hora` TIME NOT NULL COMMENT 'hora del evento',
  `ind_evento` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'descripcion del evento',
  `ind_cedula_empleado` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'cedula del empleado',
  PRIMARY KEY (`pk_num_transferencia`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`rh_c105_controlasistencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`rh_c105_controlasistencia` (
  `pk_num_controlasistencia` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'clave primaria',
  `fk_rhb001_num_empleado` INT(11) NOT NULL COMMENT 'id del empleado',
  `ind_cedula_empleado` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'cedula del empleado',
  `fec_fecha` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'fecha del evento texto',
  `fec_hora` VARCHAR(12) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'hora del evento texto',
  `fec_fecha_format` DATE NOT NULL COMMENT 'fecha del evento formato',
  `fec_hora_format` TIME NOT NULL COMMENT 'hora del evento formato',
  `ind_evento` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'descripcion del evento',
  `ind_estado` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL COMMENT 'S: SIN PROCESAR  P: PROCESADO',
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_controlasistencia`),
  INDEX `fk_rhb001_num_empleado` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a018_num_seguridad_usuario` (`fk_a018_num_seguridad_usuario` ASC),
  CONSTRAINT `rh_c105_controlasistencia_ibfk_1`
    FOREIGN KEY (`fk_rhb001_num_empleado`)
    REFERENCES `SIACE`.`rh_b001_empleado` (`fk_a003_num_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rh_c105_controlasistencia_ibfk_2`
    FOREIGN KEY (`fk_a018_num_seguridad_usuario`)
    REFERENCES `SIACE`.`a018_seguridad_usuario` (`pk_num_seguridad_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`st_a001_equipo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`st_a001_equipo` (
  `pk_equipo_tecnologico` INT(10) NOT NULL AUTO_INCREMENT,
  `fk_num_activo` INT(10) NULL DEFAULT NULL,
  `ind_estatus` INT(1) NULL DEFAULT NULL,
  `fk_num_dependencia` INT(2) NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_equipo_tecnologico`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 42
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`st_b001_ubicacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`st_b001_ubicacion` (
  `pk_num_ubicacion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_ubicacion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_ubicacion`),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`st_b002_tipo_soporte`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`st_b002_tipo_soporte` (
  `pk_num_tipo_soporte` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_soporte_t` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_tipo_soporte`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`st_c001_ingreso_laboratorio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`st_c001_ingreso_laboratorio` (
  `pk_num_ingreso` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_ingreso` DATETIME NOT NULL,
  `fec_salida` DATETIME NOT NULL,
  `ind_motivo` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_resultados` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `stat` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `fk_stb001_num_ubicacion` INT(11) NOT NULL,
  `fk_std001_num_solicitud` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_ingreso`),
  INDEX `fk_st_c001_ingreso_laboratorio_st_b001_ubicacion1_idx` (`fk_stb001_num_ubicacion` ASC),
  INDEX `fk_st_c001_ingreso_laboratorio_st_d001_solicitud1_idx` (`fk_std001_num_solicitud` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`st_c002_actividad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`st_c002_actividad` (
  `pk_num_actividad` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_nombre_actividad` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_descripcion_actividad` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_inicio` DATE NOT NULL,
  `fec_fin` DATE NOT NULL,
  `fk_rhb001_num_empleado` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_actividad`),
  INDEX `fk_st_c002_actividad_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`st_c003_planificacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`st_c003_planificacion` (
  `pk_num_planificacion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_actividad` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_inicio` DATE NOT NULL,
  `fec_fin` DATE NOT NULL,
  `fk_std001_num_solicitud` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_planificacion`),
  INDEX `fk_st_c003_planificacion_st_d001_solicitud1_idx` (`fk_std001_num_solicitud` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`st_d001_evaluacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`st_d001_evaluacion` (
  `pk_num_evaluacion` INT(11) NOT NULL AUTO_INCREMENT,
  `ind_evaluacion` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  PRIMARY KEY (`pk_num_evaluacion`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`st_d001_grupo_tecnica`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`st_d001_grupo_tecnica` (
  `pk_grupo_tecnica` INT(10) NOT NULL AUTO_INCREMENT,
  `fk_a004_num_dependencia` INT(10) NOT NULL,
  `grupo` VARCHAR(254) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fec_ultima_modificacion` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `fk_a018_num_seguridad_usuario` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  PRIMARY KEY (`pk_grupo_tecnica`, `fk_a004_num_dependencia`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`st_d001_modalidad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`st_d001_modalidad` (
  `pk_num_modalidad` INT(10) NOT NULL AUTO_INCREMENT,
  `ind_modalidad` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  PRIMARY KEY (`pk_num_modalidad`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`st_d001_solicitud`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`st_d001_solicitud` (
  `pk_num_solicitud` INT(11) NOT NULL AUTO_INCREMENT,
  `fecha_solicitud` DATETIME NOT NULL,
  `fk_num_modalidad` INT(20) NULL DEFAULT NULL,
  `ind_equipo` INT(20) NULL DEFAULT NULL,
  `ind_detalles` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NOT NULL,
  `ind_detalles_analista` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_detalles_supervisor` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `num_estatus` INT(1) NOT NULL,
  `ind_estatus` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_num_evaluacion` INT(20) NULL DEFAULT NULL,
  `ind_aprobado_por` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_ip` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_asignado` INT(11) NULL DEFAULT NULL,
  `fk_rhb001_num_empleado_supervisor` INT(11) NULL DEFAULT NULL,
  `ind_detalles_analista_final` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `fk_a004_num_dependencia` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_solicitante` INT(10) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  `ind_fecha_inicio` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  `ind_fecha_fin` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_solicitud`),
  INDEX `fk_st_d001_solicitud_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_asignado` ASC),
  INDEX `fk_st_d001_solicitud_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_supervisor` ASC),
  INDEX `fk_st_d001_solicitud_a004_dependencia1_idx` (`fk_a004_num_dependencia` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`st_d002_dato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`st_d002_dato` (
  `pk_num_datos_soporte` INT(100) NOT NULL AUTO_INCREMENT,
  `ind_observaciones` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `fec_ejecucion` DATE NOT NULL,
  `fec_hora_i` TIME NOT NULL,
  `fec_hora_c` TIME NOT NULL,
  `num_total` INT(100) NOT NULL,
  `ind_modalidad` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ind_evaluacion` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `num_estatus` INT(2) NOT NULL,
  `fk_std001_num_solicitud` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_asesor` INT(11) NOT NULL,
  `fk_rhb001_num_empleado_receptor` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_datos_soporte`),
  INDEX `fk_st_d002_dato_st_d001_solicitud1_idx` (`fk_std001_num_solicitud` ASC),
  INDEX `fk_st_d002_dato_rh_b001_empleado1_idx` (`fk_rhb001_num_empleado_asesor` ASC),
  INDEX `fk_st_d002_dato_rh_b001_empleado2_idx` (`fk_rhb001_num_empleado_receptor` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`st_d002_registro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`st_d002_registro` (
  `pk_num_registro` INT(11) NOT NULL AUTO_INCREMENT,
  `fec_creado` DATETIME NULL DEFAULT NULL,
  `fec_aprobado` DATETIME NULL DEFAULT NULL,
  `fec_revisado` DATETIME NULL DEFAULT NULL,
  `fec_asignado` DATETIME NULL DEFAULT NULL,
  `fec_ejecutado` DATETIME NULL DEFAULT NULL,
  `fec_culminado` DATETIME NULL DEFAULT NULL,
  `fec_evaluado` DATETIME NULL DEFAULT NULL,
  `fec_cerrado` DATETIME NULL DEFAULT NULL,
  `fk_std001_num_relacion` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_spanish2_ci' NULL DEFAULT NULL,
  PRIMARY KEY (`pk_num_registro`),
  INDEX `fk_st_d002_registro_st_d001_solicitud1_idx` (`fk_std001_num_relacion` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 13
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;


-- -----------------------------------------------------
-- Table `SIACE`.`st_e001_historial`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`st_e001_historial` (
  `pk_num_historial` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_num_solicitud` INT(11) NOT NULL,
  `fec_creado` DATETIME NOT NULL,
  `fec_aprobado` DATETIME NOT NULL,
  `fec_revisado` DATETIME NOT NULL,
  `fec_asignado` DATETIME NOT NULL,
  `fec_ejecutado` DATETIME NOT NULL,
  `fec_completado` DATETIME NOT NULL,
  `fec_cerrado` DATETIME NOT NULL,
  `fec_evaluado` DATETIME NOT NULL,
  `fec_eliminado` DATETIME NOT NULL,
  `fk_std001_num_solicitud` INT(11) NOT NULL,
  `fec_ultima_modificacion` DATETIME NOT NULL,
  `fk_a018_num_seguridad_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`pk_num_historial`),
  INDEX `fk_st_e001_historial_st_d001_solicitud1_idx` (`fk_std001_num_solicitud` ASC),
  INDEX `fk_a018_num_seguridad_usuario_idx` (`fk_a018_num_seguridad_usuario` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci;

USE `SIACE` ;

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_capacitaciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_capacitaciones` (`pk_num_capacitacion` INT, `fk_a001_num_organismo` INT, `ind_descripcion_empresa` INT, `fec_anio` INT, `fk_rhb001_empleado_solicitante` INT, `fk_persona_solicitante` INT, `nombre_persona_solicitante` INT, `fk_rhc048_num_curso` INT, `ind_nombre_curso` INT, `fk_rhc040_centro_estudio` INT, `ind_nombre_institucion` INT, `fk_a010_num_ciudad` INT, `ind_ciudad` INT, `pk_num_municipio` INT, `ind_municipio` INT, `pk_num_estado` INT, `ind_estado` INT, `pk_num_pais` INT, `ind_pais` INT, `fk_a006_num_miscelnaeo_detalle_tipocap` INT, `nombre_tipo_capacitacion` INT, `fk_a006_num_miscelnaeo_detalle_modalidad` INT, `nombre_modalidad` INT, `fk_a006_num_miscelnaeo_detalle_origen` INT, `nombre_origen` INT, `ind_periodo` INT, `num_vacantes` INT, `num_participantes` INT, `fec_fecha_inicio` INT, `fec_fecha_termino` INT, `ind_expositor` INT, `ind_telefonos` INT, `flag_costos` INT, `txt_observaciones` INT, `num_monto_estimado` INT, `num_monto_asumido` INT, `fec_fecha_creado` INT, `fk_rhb001_num_empleado_creado` INT, `fk_persona_creacion` INT, `nombre_persona_creacion` INT, `fec_fecha_aprobado` INT, `fk_rhb001_num_empleado_aprobado` INT, `fk_persona_aprobado` INT, `nombre_persona_aprobacion` INT, `fec_ultima_modificacion` INT, `fk_a018_num_seguridad_usuario` INT, `estado_capacitacion` INT, `num_total_dias` INT, `ind_total_horas` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_capacitaciones_empleados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_capacitaciones_empleados` (`pk_num_capacitacion_empleado` INT, `fk_rhc097_num_capacitacion` INT, `fk_rhb001_num_empleado` INT, `ind_cedula_documento` INT, `ind_nombre1` INT, `ind_nombre2` INT, `ind_apellido1` INT, `ind_apellido2` INT, `fk_a004_num_dependencia` INT, `ind_dependencia` INT, `num_asistencias` INT, `num_dias_asistidos` INT, `ind_horas_asistencia` INT, `ind_calificacion` INT, `num_costo_individual` INT, `num_importe_gastos` INT, `flag_aprobado` INT, `fec_ultima_modificacion` INT, `fk_a018_num_seguridad_usuario` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_capacitaciones_fundamentos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_capacitaciones_fundamentos` (`pk_num_capacitacion_fundamentos` INT, `fk_rhc097_num_capacitacion` INT, `txt_fundamentacion_1` INT, `txt_fundamentacion_2` INT, `txt_fundamentacion_3` INT, `txt_fundamentacion_4` INT, `txt_fundamentacion_5` INT, `txt_fundamentacion_6` INT, `txt_fundamentacion_7` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_capacitaciones_horarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_capacitaciones_horarios` (`pk_num_capacitacion_horario` INT, `fk_rhc097_num_capacitacion` INT, `fk_rhb001_num_empleado` INT, `ind_cedula_documento` INT, `ind_nombre1` INT, `ind_nombre2` INT, `ind_apellido1` INT, `ind_apellido2` INT, `fec_fecha` INT, `fec_hora_ini` INT, `fec_hora_fin` INT, `ind_total_horas` INT, `fec_ultima_modificacion` INT, `fk_a018_num_seguridad_usuario` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_capacitaciones_ubicacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_capacitaciones_ubicacion` (`pk_num_capacitacion` INT, `fk_a001_num_organismo` INT, `fk_a010_num_ciudad` INT, `ind_ciudad` INT, `pk_num_municipio` INT, `ind_municipio` INT, `pk_num_estado` INT, `ind_estado` INT, `pk_num_pais` INT, `ind_pais` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_empleado_carga_familiar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_empleado_carga_familiar` (`pk_num_carga` INT, `pk_num_empleado` INT, `pk_num_persona` INT, `fk_a003_num_persona` INT, `ind_cedula_documento` INT, `nombre` INT, `ind_nombre1` INT, `ind_nombre2` INT, `ind_apellido1` INT, `ind_apellido2` INT, `fk_a006_num_miscelaneo_detalle_sexo` INT, `detalle_parentesco` INT, `fk_a006_num_miscelaneo_detalle_nacionalidad` INT, `fec_nacimiento` INT, `fk_a006_num_miscelaneo_detalle_edocivil` INT, `ind_estatus` INT, `ind_estatus_persona` INT, `fk_a006_num_miscelaneo_detalle_parentezco` INT, `fk_a006_num_miscelaneo_detalle_gradoinst` INT, `fk_a006_num_miscelaneo_detalle_tipoedu` INT, `fk_rhc040_num_institucion` INT, `ind_empresa` INT, `txt_direccion_empresa` INT, `num_sueldo_mensual` INT, `fk_a006_num_miscelaneo_det_gruposangre` INT, `ind_foto` INT, `num_flag_discapacidad` INT, `num_flag_estudia` INT, `num_flag_trabaja` INT, `detalle_sexo` INT, `sexo` INT, `parentesco` INT, `fk_rhc064_nivel_grado_instruccion` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_empleado_carga_familiar_direccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_empleado_carga_familiar_direccion` (`pk_num_carga` INT, `pk_num_empleado` INT, `pk_num_persona` INT, `pk_num_direccion_persona` INT, `fk_a006_num_miscelaneo_detalle_tipodir` INT, `fk_a006_num_miscelaneo_detalle_domicilio` INT, `ind_direccion` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_empleado_carga_familiar_telefono`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_empleado_carga_familiar_telefono` (`pk_num_carga` INT, `pk_num_empleado` INT, `pk_num_persona` INT, `pk_num_telefono` INT, `ind_telefono` INT, `fk_a006_num_miscelaneo_detalle_tipo_telefono` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_empleado_experiencia_laboral`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_empleado_experiencia_laboral` (`pk_num_experiencia` INT, `pk_num_empleado` INT, `ind_cargo` INT, `ind_empresa` INT, `fk_a006_num_miscelaneo_detalle_areaexp` INT, `areaexp` INT, `fec_ingreso` INT, `fec_egreso` INT, `fk_a006_num_miscelaneo_detalle_tipo_empresa` INT, `tipoente` INT, `fk_rhc032_num_motivo_cese` INT, `ind_nombre_cese` INT, `num_sueldo_mensual` INT, `num_flag_antvacacion` INT, `txt_funciones` INT, `fec_ultima_modificacion` INT, `fk_a018_num_seguridad_usuario` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_empleado_informacion_bancaria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_empleado_informacion_bancaria` (`pk_num_empleado_banco` INT, `pk_num_empleado` INT, `fk_a003_num_persona` INT, `fk_a006_num_miscelaneo_detalle_banco` INT, `banco` INT, `fk_a006_num_miscelaneo_detalle_tipocta` INT, `tipocta` INT, `fk_a006_num_miscelaneo_detalle_tipoapo` INT, `tipoapo` INT, `ind_cuenta` INT, `num_sueldo_mensual` INT, `num_flag_principal` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_empleado_jubilacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_empleado_jubilacion` (`pk_num_jubilacion` INT, `fk_rhb001_num_empleado` INT, `fk_nmb001_num_tipo_nomina` INT, `fk_a006_num_tipo_trabajador` INT, `fk_rhc032_num_motivo_cese` INT, `fk_a001_num_organismo` INT, `fk_a004_num_dependencia` INT, `fk_rhc063_num_puesto` INT, `fec_ingreso` INT, `fec_egreso` INT, `ind_periodo` INT, `num_edad` INT, `num_anio_servicio` INT, `num_anio_experiencia` INT, `num_anio_total` INT, `ind_observacion_cese` INT, `fk_a018_num_seguridad_usuario` INT, `fec_ultima_modificacion` INT, `num_ultimo_sueldo` INT, `num_total_sueldo` INT, `num_total_primas` INT, `num_sueldo_base` INT, `num_porcentaje` INT, `num_coeficiente` INT, `num_monto_jubilacion` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_info_registro_empleado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_info_registro_empleado` (`pk_num_operaciones_empleado` INT, `txt_estatus` INT, `empleado_registro` INT, `persona_registro` INT, `nombre1_empleado` INT, `nombre2_empleado` INT, `apellido1_empleado` INT, `apellido2_empleado` INT, `empleado_operacion` INT, `persona_operacion` INT, `nombre1_operacion` INT, `nombre2_operacion` INT, `apellido1_operacion` INT, `apellido2_operacion` INT, `fec_operacion` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_pension_invalidez`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_pension_invalidez` (`pk_num_proceso_pension` INT, `pk_num_invalidez` INT, `fk_rhb001_num_empleado` INT, `pk_num_persona` INT, `ind_cedula_documento` INT, `nombre_completo` INT, `fk_a001_num_organismo` INT, `ind_organismo` INT, `fk_a004_num_dependencia` INT, `ind_dependencia` INT, `fk_rhc063_num_puestos` INT, `ind_cargo` INT, `num_ultimo_sueldo` INT, `num_anio_servicio` INT, `num_edad` INT, `fec_fecha` INT, `fec_fecha_ingreso` INT, `fk_a006_num_miscelaneo_detalle_tipopension` INT, `ind_detalle_tipopension` INT, `tipo_pension` INT, `fk_a006_num_miscelaneo_detalle_motivopension` INT, `ind_detalle_motivopension` INT, `motivo_pension` INT, `num_monto_pension` INT, `ind_periodo` INT, `fk_nmb001_num_tipo_nomina` INT, `ind_nombre_nomina` INT, `fk_a006_num_miscelaneo_detalle_tipotrab` INT, `tipo_trabajador` INT, `num_situacion_trabajo` INT, `fk_rhc032_num_motivo_cese` INT, `ind_nombre_cese` INT, `fec_fecha_egreso` INT, `txt_observacion_egreso` INT, `ind_resolucion` INT, `fec_ultima_modificacion` INT, `fk_a018_num_seguridad_usuario` INT, `txt_estatus` INT, `num_flag_estatus` INT, `fec_operacion` INT, `fk_a006_num_miscelaneo_detalle_sexo` INT, `sexo` INT, `fec_nacimiento` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_pension_sobreviviente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_pension_sobreviviente` (`pk_num_proceso_pension` INT, `pk_num_sobreviviente` INT, `fk_rhb001_num_empleado` INT, `pk_num_persona` INT, `ind_cedula_documento` INT, `nombre_completo` INT, `fk_a001_num_organismo` INT, `ind_organismo` INT, `fk_a004_num_dependencia` INT, `ind_dependencia` INT, `fk_rhc063_num_puestos` INT, `ind_cargo` INT, `num_ultimo_sueldo` INT, `num_anio_servicio` INT, `num_edad` INT, `fec_fecha` INT, `fec_fecha_ingreso` INT, `fk_a006_num_miscelaneo_detalle_tipopension` INT, `ind_detalle_tipopension` INT, `tipo_pension` INT, `fk_a006_num_miscelaneo_detalle_motivopension` INT, `ind_detalle_motivopension` INT, `motivo_pension` INT, `num_monto_pension` INT, `num_monto_jubilacion` INT, `num_coeficiente` INT, `num_total_sueldo` INT, `num_anios_servicio_exceso` INT, `ind_periodo` INT, `fk_nmb001_num_tipo_nomina` INT, `ind_nombre_nomina` INT, `fk_a006_num_miscelaneo_detalle_tipotrab` INT, `tipo_trabajador` INT, `num_situacion_trabajo` INT, `fk_rhc032_num_motivo_cese` INT, `ind_nombre_cese` INT, `fec_fecha_egreso` INT, `txt_observacion_egreso` INT, `ind_resolucion` INT, `fec_ultima_modificacion` INT, `fk_a018_num_seguridad_usuario` INT, `txt_estatus` INT, `num_flag_estatus` INT, `fec_operacion` INT, `fk_a006_num_miscelaneo_detalle_sexo` INT, `sexo` INT, `fec_nacimiento` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_persona_empleado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_persona_empleado` (`pk_num_persona` INT, `pk_num_empleado` INT, `ind_cedula_documento` INT, `ind_documento_fiscal` INT, `ind_nombre1` INT, `ind_nombre2` INT, `ind_apellido1` INT, `ind_apellido2` INT, `fk_a006_num_miscelaneo_detalle_sexo` INT, `fk_a006_num_miscelaneo_detalle_nacionalidad` INT, `fec_nacimiento` INT, `ind_lugar_nacimiento` INT, `fk_a006_num_miscelaneo_detalle_edocivil` INT, `fec_estado_civil` INT, `ind_email` INT, `ind_foto` INT, `ind_tipo_persona` INT, `ind_estatus_persona` INT, `fk_a006_num_miscelaneo_det_gruposangre` INT, `fk_a006_num_miscelaneo_det_tipopersona` INT, `fk_a010_num_ciudad` INT, `fk_a013_num_sector` INT, `ind_estatus_empleado` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_persona_empleado_datos_laborales`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_persona_empleado_datos_laborales` (`pk_num_persona` INT, `pk_num_empleado` INT, `ind_cedula_documento` INT, `ind_documento_fiscal` INT, `ind_nombre1` INT, `ind_nombre2` INT, `ind_apellido1` INT, `ind_apellido2` INT, `fec_ingreso` INT, `ind_resolucion_ingreso` INT, `fk_rhc063_num_puestos_cargo` INT, `fk_rhc063_num_puestos_cargotmp` INT, `fk_rhb007_num_horario` INT, `fk_a006_num_miscelaneo_detalle_paso` INT, `fk_a001_num_organismo` INT, `fk_a004_num_dependencia` INT, `fk_a004_num_dependencia_tmp` INT, `fk_a023_num_centro_costo` INT, `fk_nmb001_num_tipo_nomina` INT, `fk_a006_num_miscelaneo_detalle_tipopago` INT, `fk_a006_num_miscelaneo_detalle_tipotrabajador` INT, `fk_a006_num_miscelaneo_detalle_grupoocupacional` INT, `fk_rhc010_num_serie` INT, `fk_a006_num_miscelaneo_detalle_categoria` INT, `num_sueldo_basico` INT, `categoria_cargo` INT, `ind_estatus` INT, `fec_ingreso_anterior` INT, `fec_egreso` INT, `ind_resolucion_egreso` INT, `num_sueldo_ant` INT, `num_sueldo` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_persona_empleado_direccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_persona_empleado_direccion` (`pk_num_persona` INT, `ind_cedula_documento` INT, `pk_num_empleado` INT, `pk_num_direccion_persona` INT, `fk_a006_num_miscelaneo_detalle_tipodir` INT, `fk_a006_num_miscelaneo_detalle_domicilio` INT, `ind_direccion` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_persona_empleado_domicilio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_persona_empleado_domicilio` (`pk_num_persona` INT, `ind_cedula_documento` INT, `pk_num_empleado` INT, `estado_domicilio` INT, `municipio_domicilio` INT, `pais_domicilio` INT, `parroquia_domicilio` INT, `sector_domicilio` INT, `ciudad_domicilio` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_persona_empleado_emergencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_persona_empleado_emergencia` (`pk_num_empleado` INT, `pk_num_persona` INT, `ind_cedula_documento` INT, `ind_nombre1` INT, `ind_nombre2` INT, `ind_apellido1` INT, `ind_apellido2` INT, `pk_num_empleado_emergencia` INT, `persona_emergencia` INT, `fk_a006_num_miscelaneo_detalle_tipodir` INT, `ind_direccion` INT, `fk_a006_num_miscelaneo_detalle_parentesco` INT, `pk_num_direccion_persona` INT, `fk_a006_num_miscelaneo_detalle_domicilio` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_persona_empleado_emergencia_telefono`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_persona_empleado_emergencia_telefono` (`pk_num_telefono` INT, `pk_num_empleado` INT, `pk_num_persona` INT, `ind_cedula_documento` INT, `pk_num_empleado_emergencia` INT, `persona_emergencia` INT, `fk_a006_num_miscelaneo_detalle_tipo_telefono` INT, `ind_telefono` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_persona_empleado_licencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_persona_empleado_licencia` (`pk_num_persona` INT, `pk_num_empleado` INT, `ind_cedula_documento` INT, `ind_nombre1` INT, `ind_nombre2` INT, `ind_apellido1` INT, `ind_apellido2` INT, `pk_num_licencia` INT, `fk_a006_num_miscelaneo_detalle_tipolic` INT, `fk_a006_num_miscelaneo_detalle_claselic` INT, `fec_expiracion_licencia` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_persona_empleado_nacimiento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_persona_empleado_nacimiento` (`pk_num_persona` INT, `ind_cedula_documento` INT, `pk_num_empleado` INT, `fec_nacimiento` INT, `ind_lugar_nacimiento` INT, `ciudad_nacimiento` INT, `municipio_nacimiento` INT, `estado_nacimiento` INT, `pais_nacimiento` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_persona_empleado_telefono`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_persona_empleado_telefono` (`pk_num_telefono` INT, `pk_num_persona` INT, `ind_cedula_documento` INT, `pk_num_empleado` INT, `fk_a006_num_miscelaneo_detalle_tipo_telefono` INT, `ind_telefono` INT);

-- -----------------------------------------------------
-- Placeholder table for view `SIACE`.`vl_rh_usuario_empleado_dependencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SIACE`.`vl_rh_usuario_empleado_dependencia` (`pk_num_seguridad_usuario` INT, `pk_num_empleado` INT, `pk_num_persona` INT, `pk_num_empleado_organizacion` INT, `fk_a004_num_dependencia` INT, `fk_a004_num_dependencia_tmp` INT);

-- -----------------------------------------------------
-- function generar_cierremensual
-- -----------------------------------------------------

DELIMITER $$
USE `SIACE`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `generar_cierremensual`(`fec_anio` VARCHAR(4), `fec_mes` VARCHAR(4), `presupuesto` INT, `usuario` INT) RETURNS int(11)
BEGIN
SET @anio=fec_anio;
 SET @mes=fec_mes;
 SET @presupuesto=presupuesto;
 SET @usuario=usuario;


 IF (@mes="01") THEN
	INSERT INTO pr_d001_ejecucion_presupuestaria  (fk_prc002_num_presupuesto_det, fec_anio,fec_mes,
					num_disponibilidad_presupuestaria_inicial,num_disponibilidad_financiera_inicial,num_incremento,num_disminucion,num_credito,num_presupuesto_ajustado,
          num_compromiso, num_causado, num_pagado, num_disponibilidad_presupuestaria,num_disponibilidad_financiera,num_variacion,fec_ultima_modificacion,
          fk_a018_num_seguridad_usuario)
			SELECT pk_num_presupuesto_det, @anio, @mes,
					num_monto_aprobado, num_monto_aprobado,
          num_monto_incremento,num_monto_disminucion,num_monto_credito,num_monto_ajustado,num_monto_compromiso,
					num_monto_causado, num_monto_pagado, num_monto_ajustado-num_monto_compromiso, num_monto_ajustado-num_monto_pagado,0,NOW(),
          @usuario
			FROM pr_c002_presupuesto_det WHERE fk_prb004_num_presupuesto=@presupuesto;
  ELSE

INSERT INTO pr_d001_ejecucion_presupuestaria  (fk_prc002_num_presupuesto_det, fec_anio,fec_mes,
					num_disponibilidad_presupuestaria_inicial,num_disponibilidad_financiera_inicial,num_incremento,num_disminucion,num_credito,num_presupuesto_ajustado,
          num_compromiso, num_causado, num_pagado, num_disponibilidad_presupuestaria,num_disponibilidad_financiera,num_variacion,fec_ultima_modificacion,
          fk_a018_num_seguridad_usuario)
			SELECT pk_num_presupuesto_det, @anio, @mes,
					(SELECT num_disponibilidad_presupuestaria FROM pr_d001_ejecucion_presupuestaria  WHERE fec_anio=@anio and CAST(fec_mes AS UNSIGNED)=@mes-1  and fk_prc002_num_presupuesto_det=pk_num_presupuesto_det),
          (SELECT num_disponibilidad_financiera FROM pr_d001_ejecucion_presupuestaria  WHERE fec_anio=@anio and CAST(fec_mes AS UNSIGNED)=@mes-1  and fk_prc002_num_presupuesto_det=pk_num_presupuesto_det),
           num_monto_incremento,num_monto_disminucion,num_monto_credito,num_monto_ajustado,num_monto_compromiso,
					 num_monto_causado, num_monto_pagado, num_monto_ajustado-num_monto_compromiso, num_monto_ajustado-num_monto_pagado,0,NOW(),
           @usuario
			FROM pr_c002_presupuesto_det WHERE fk_prb004_num_presupuesto=@presupuesto;
 END IF;

RETURN 1;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- function generar_presupuesto
-- -----------------------------------------------------

DELIMITER $$
USE `SIACE`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `generar_presupuesto`(`idAntepresupuesto` INT, `codPresupuesto` VARCHAR(4), `usuario` INT) RETURNS int(11)
BEGIN
SET @codPresu=codPresupuesto;

insert into pr_b004_presupuesto (fk_rhb001_empleado_creado,fk_rhb001_empleado_aprobado,ind_cod_presupuesto, fec_anio,fec_presupuesto,fec_inicio,fec_fin,
ind_numero_gaceta,fec_gaceta,ind_numero_decreto, fecha_decreto,num_monto_aprobado,num_monto_ajustado,ind_estado,fec_ultima_modificacion,
fk_prb005_num_proyecto,fk_a018_num_seguridad_usuario,fk_a018_seguridad_usuario, ind_tipo_presupuesto)

 select fk_rhb001_num_empleado_creado_por,usuario, @codPresu,fec_anio,CURDATE(),fec_inicio,dec_fin, 
ind_numero_gaceta, fec_gaceta ,ind_numero_decreto, fec_decreto, num_monto_generado,num_monto_generado,'AP',NOW(),1,usuario,usuario,ind_tipo_presupuesto
 from pr_b003_antepresupuesto where pk_num_antepresupuesto = idAntepresupuesto;

SET @presupuestoid = LAST_INSERT_ID();
insert into pr_c002_presupuesto_det (fk_prb004_num_presupuesto, fk_prb002_num_partida_presupuestaria, num_flags_anexa,num_flags_reformulacion,
num_monto_aprobado,num_monto_ajustado, num_monto_compromiso, num_monto_causado, num_monto_pagado,num_monto_incremento,num_monto_disminucion,
num_monto_credito,fk_pr006_num_aespecifica,fk_a018_num_seguridad_usuario,fec_ultima_modificacion)

select @presupuestoid, fk_prb002_num_partida_presupuestaria,if(num_monto_presupuestado<1,1,0),0,num_monto_aprobado,num_monto_aprobado,0,0,0,0,0,0,fk_pr_b006_aespecifica,usuario,NOW()
from pr_c001_antepresupuesto_det where fk_prb003_num_antepresupuesto = idAntepresupuesto;
RETURN 1;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- function u_rh_ultimoRegistroAsignacionUtiles
-- -----------------------------------------------------

DELIMITER $$
USE `SIACE`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `u_rh_ultimoRegistroAsignacionUtiles`(`id_asignacion_utiles` INT) RETURNS int(11)
BEGIN
	 SET @registro = (SELECT 	pk_num_operaciones_asignaciones_utiles 
		FROM rh_c096_operaciones_asignaciones_utiles
		WHERE fk_rhc093_utiles_asignacion=id_asignacion_utiles
		AND num_flag_estatus=1
    ORDER BY pk_num_operaciones_asignaciones_utiles   DESC);

	RETURN @registro;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- function u_rh_ultimoRegistroBeneficioUtiles
-- -----------------------------------------------------

DELIMITER $$
USE `SIACE`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `u_rh_ultimoRegistroBeneficioUtiles`(`id_beneficio_utiles` INT) RETURNS int(11)
BEGIN
	 SET @registro = (SELECT 	pk_num_operaciones_beneficios_utiles 
		FROM rh_c095_operaciones_beneficios_utiles
		WHERE fk_rhc091_num_utiles_beneficio=id_beneficio_utiles
		AND num_flag_estatus=1
    ORDER BY pk_num_operaciones_beneficios_utiles  DESC);

	RETURN @registro;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- function u_rh_ultimoRegistroCeseReingreso
-- -----------------------------------------------------

DELIMITER $$
USE `SIACE`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `u_rh_ultimoRegistroCeseReingreso`(`id_empleado_cese_reingreso` INT) RETURNS int(11)
BEGIN
 SET @registro = (SELECT pk_num_operaciones_cr   
		FROM rh_c049_operaciones_cese_reingreso 
		WHERE fk_rhc077_num_empleado_cese_reingreso=id_empleado_cese_reingreso
		AND num_flag_estatus=1
    ORDER BY pk_num_operaciones_cr DESC);

	RETURN @registro;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- function u_rh_ultimoRegistroProcesoPension
-- -----------------------------------------------------

DELIMITER $$
USE `SIACE`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `u_rh_ultimoRegistroProcesoPension`(`id_proceso_pension` INT) RETURNS int(11)
BEGIN
 SET @registro = (SELECT pk_num_operaciones_pension  
		FROM rh_c086_operaciones_pension
		WHERE fk_rhb003_num_pension=id_proceso_pension
		AND num_flag_estatus=1
    ORDER BY pk_num_operaciones_pension  DESC);

	RETURN @registro;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure w_rh_operacionesAsignacionUtiles
-- -----------------------------------------------------

DELIMITER $$
USE `SIACE`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `w_rh_operacionesAsignacionUtiles`(IN `txt_estatus` VARCHAR(2), IN `id_asignacion_utiles` INT, IN `id_empleado` INT, IN `flag_estatus` INT, IN `fec_operacion` DATE, IN `txt_observaciones` MEDIUMTEXT, IN `usuario` INT)
BEGIN
	
  DECLARE N INT;

	IF NOT EXISTS ( SELECT t.fk_rhc093_utiles_asignacion FROM rh_c096_operaciones_asignaciones_utiles AS t WHERE t.fk_rhc093_utiles_asignacion = id_asignacion_utiles) THEN
		
		INSERT INTO rh_c096_operaciones_asignaciones_utiles(ind_estado, fk_rhc093_utiles_asignacion , fk_rhb001_num_empleado, num_flag_estatus, fec_operacion,txt_observaciones,fk_a018_num_seguridad_usuario)
		VALUES ( txt_estatus,id_asignacion_utiles,id_empleado,flag_estatus,fec_operacion,txt_observaciones,usuario);
  ELSE
		
		SET N = (SELECT u_rh_ultimoRegistroAsignacionUtiles(id_asignacion_utiles));
		
		update rh_c096_operaciones_asignaciones_utiles SET num_flag_estatus=0 WHERE pk_num_operaciones_asignaciones_utiles=N; 	
		
		INSERT INTO rh_c096_operaciones_asignaciones_utiles(ind_estado, fk_rhc093_utiles_asignacion, fk_rhb001_num_empleado, num_flag_estatus, fec_operacion,txt_observaciones,fk_a018_num_seguridad_usuario)
		VALUES ( txt_estatus,id_asignacion_utiles,id_empleado,flag_estatus,fec_operacion,txt_observaciones,usuario);

  END IF;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure w_rh_operacionesBeneficioUtiles
-- -----------------------------------------------------

DELIMITER $$
USE `SIACE`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `w_rh_operacionesBeneficioUtiles`(IN `txt_estatus` VARCHAR(2), IN `id_beneficio_pension` INT, IN `id_empleado` INT, IN `flag_estatus` INT, IN `fec_operacion` DATE, IN `txt_observaciones` MEDIUMTEXT, IN `usuario` INT)
BEGIN
	
  DECLARE N INT;

	IF NOT EXISTS ( SELECT t.fk_rhc091_num_utiles_beneficio FROM rh_c095_operaciones_beneficios_utiles AS t WHERE t.fk_rhc091_num_utiles_beneficio = id_beneficio_pension) THEN
		
		INSERT INTO rh_c095_operaciones_beneficios_utiles(ind_estado, fk_rhc091_num_utiles_beneficio, fk_rhb001_num_empleado, num_flag_estatus, fec_operacion,txt_observaciones,fk_a018_num_seguridad_usuario)
		VALUES ( txt_estatus,id_beneficio_pension,id_empleado,flag_estatus,fec_operacion,txt_observaciones,usuario);
  ELSE
		
		SET N = (SELECT u_rh_ultimoRegistroBeneficioUtiles(id_beneficio_pension));
		
		update rh_c095_operaciones_beneficios_utiles SET num_flag_estatus=0 WHERE pk_num_operaciones_beneficios_utiles=N; 	
		
		INSERT INTO rh_c095_operaciones_beneficios_utiles(ind_estado, fk_rhc091_num_utiles_beneficio, fk_rhb001_num_empleado, num_flag_estatus, fec_operacion,txt_observaciones,fk_a018_num_seguridad_usuario)
		VALUES ( txt_estatus,id_beneficio_pension,id_empleado,flag_estatus,fec_operacion,txt_observaciones,usuario);

  END IF;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure w_rh_operacionesCeseReingreso
-- -----------------------------------------------------

DELIMITER $$
USE `SIACE`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `w_rh_operacionesCeseReingreso`(IN `txt_estatus` VARCHAR(2), IN `id_cese_reingreso` INT, IN `id_empleado` INT, IN `flag_estatus` INT, IN `fec_operacion` DATE, IN `txt_observaciones` MEDIUMTEXT, IN `usuario` INT)
BEGIN
	
  DECLARE N INT;

	IF NOT EXISTS ( SELECT t.fk_rhc077_num_empleado_cese_reingreso FROM rh_c049_operaciones_cese_reingreso AS t WHERE t.fk_rhc077_num_empleado_cese_reingreso = id_cese_reingreso) THEN

		
		INSERT INTO rh_c049_operaciones_cese_reingreso(ind_estado, fk_rhc077_num_empleado_cese_reingreso, fk_rhb0001_num_empleado, num_flag_estatus, fec_operacion,txt_observaciones,fk_a018_num_seguridad_usuario)
		VALUES ( txt_estatus,id_cese_reingreso,id_empleado,flag_estatus,fec_operacion,txt_observaciones,usuario);


  ELSE

		
		SET N = (SELECT u_rh_ultimoRegistroCeseReingreso(id_cese_reingreso));

		

		update rh_c049_operaciones_cese_reingreso SET num_flag_estatus=0 WHERE pk_num_operaciones_cr=N; 	

		
		INSERT INTO rh_c049_operaciones_cese_reingreso(ind_estado, fk_rhc077_num_empleado_cese_reingreso, fk_rhb0001_num_empleado, num_flag_estatus, fec_operacion,txt_observaciones,fk_a018_num_seguridad_usuario)
		VALUES ( txt_estatus,id_cese_reingreso,id_empleado,flag_estatus,fec_operacion,txt_observaciones,usuario);


  
  END IF;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure w_rh_operacionesProcesoPension
-- -----------------------------------------------------

DELIMITER $$
USE `SIACE`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `w_rh_operacionesProcesoPension`(IN `txt_estatus` VARCHAR(2), IN `id_proceso_pension` INT, IN `id_empleado` INT, IN `flag_estatus` INT, IN `fec_operacion` DATE, IN `txt_observaciones` MEDIUMTEXT, IN `usuario` INT)
BEGIN
	
  DECLARE N INT;

	IF NOT EXISTS ( SELECT t.fk_rhb003_num_pension FROM rh_c086_operaciones_pension AS t WHERE t.fk_rhb003_num_pension = id_proceso_pension) THEN

		
		INSERT INTO rh_c086_operaciones_pension(ind_estado, fk_rhb003_num_pension, fk_rhb0001_num_empleado, num_flag_estatus, fec_operacion,txt_observaciones,fk_a018_num_seguridad_usuario)
		VALUES ( txt_estatus,id_proceso_pension,id_empleado,flag_estatus,fec_operacion,txt_observaciones,usuario);


  ELSE

		
		SET N = (SELECT u_rh_ultimoRegistroProcesoPension(id_proceso_pension));

		
		update rh_c086_operaciones_pension SET num_flag_estatus=0 WHERE pk_num_operaciones_pension=N; 	

		
		INSERT INTO rh_c086_operaciones_pension(ind_estado, fk_rhb003_num_pension, fk_rhb0001_num_empleado, num_flag_estatus, fec_operacion,txt_observaciones,fk_a018_num_seguridad_usuario)
		VALUES ( txt_estatus,id_proceso_pension,id_empleado,flag_estatus,fec_operacion,txt_observaciones,usuario);


  
  END IF;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_capacitaciones`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_capacitaciones`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_capacitaciones` AS select `rh_c097_capacitacion`.`pk_num_capacitacion` AS `pk_num_capacitacion`,`rh_c097_capacitacion`.`fk_a001_num_organismo` AS `fk_a001_num_organismo`,`a001_organismo`.`ind_descripcion_empresa` AS `ind_descripcion_empresa`,`rh_c097_capacitacion`.`fec_anio` AS `fec_anio`,`rh_c097_capacitacion`.`fk_rhb001_empleado_solicitante` AS `fk_rhb001_empleado_solicitante`,`empleado_solicitante`.`fk_a003_num_persona` AS `fk_persona_solicitante`,concat(`persona_solicitante`.`ind_nombre1`,' ',`persona_solicitante`.`ind_nombre2`,' ',`persona_solicitante`.`ind_apellido1`,' ',`persona_solicitante`.`ind_apellido2`) AS `nombre_persona_solicitante`,`rh_c097_capacitacion`.`fk_rhc048_num_curso` AS `fk_rhc048_num_curso`,`rh_c048_curso`.`ind_nombre_curso` AS `ind_nombre_curso`,`rh_c097_capacitacion`.`fk_rhc040_centro_estudio` AS `fk_rhc040_centro_estudio`,`rh_c040_institucion`.`ind_nombre_institucion` AS `ind_nombre_institucion`,`rh_c097_capacitacion`.`fk_a010_num_ciudad` AS `fk_a010_num_ciudad`,`a010_ciudad`.`ind_ciudad` AS `ind_ciudad`,`a011_municipio`.`pk_num_municipio` AS `pk_num_municipio`,`a011_municipio`.`ind_municipio` AS `ind_municipio`,`a009_estado`.`pk_num_estado` AS `pk_num_estado`,`a009_estado`.`ind_estado` AS `ind_estado`,`a008_pais`.`pk_num_pais` AS `pk_num_pais`,`a008_pais`.`ind_pais` AS `ind_pais`,`rh_c097_capacitacion`.`fk_a006_num_miscelnaeo_detalle_tipocap` AS `fk_a006_num_miscelnaeo_detalle_tipocap`,`tipo_capacitacion`.`ind_nombre_detalle` AS `nombre_tipo_capacitacion`,`rh_c097_capacitacion`.`fk_a006_num_miscelnaeo_detalle_modalidad` AS `fk_a006_num_miscelnaeo_detalle_modalidad`,`modalidad`.`ind_nombre_detalle` AS `nombre_modalidad`,`rh_c097_capacitacion`.`fk_a006_num_miscelnaeo_detalle_origen` AS `fk_a006_num_miscelnaeo_detalle_origen`,`origen`.`ind_nombre_detalle` AS `nombre_origen`,`rh_c097_capacitacion`.`ind_periodo` AS `ind_periodo`,`rh_c097_capacitacion`.`num_vacantes` AS `num_vacantes`,`rh_c097_capacitacion`.`num_participantes` AS `num_participantes`,`rh_c097_capacitacion`.`fec_fecha_inicio` AS `fec_fecha_inicio`,`rh_c097_capacitacion`.`fec_fecha_termino` AS `fec_fecha_termino`,`rh_c097_capacitacion`.`ind_expositor` AS `ind_expositor`,`rh_c097_capacitacion`.`ind_telefonos` AS `ind_telefonos`,`rh_c097_capacitacion`.`flag_costos` AS `flag_costos`,`rh_c097_capacitacion`.`txt_observaciones` AS `txt_observaciones`,`rh_c097_capacitacion`.`num_monto_estimado` AS `num_monto_estimado`,`rh_c097_capacitacion`.`num_monto_asumido` AS `num_monto_asumido`,`rh_c097_capacitacion`.`fec_fecha_creado` AS `fec_fecha_creado`,`rh_c097_capacitacion`.`fk_rhb001_num_empleado_creado` AS `fk_rhb001_num_empleado_creado`,`empleado_creacion`.`fk_a003_num_persona` AS `fk_persona_creacion`,concat(`persona_creacion`.`ind_nombre1`,' ',`persona_creacion`.`ind_nombre2`,' ',`persona_creacion`.`ind_apellido1`,' ',`persona_creacion`.`ind_apellido2`) AS `nombre_persona_creacion`,`rh_c097_capacitacion`.`fec_fecha_aprobado` AS `fec_fecha_aprobado`,`rh_c097_capacitacion`.`fk_rhb001_num_empleado_aprobado` AS `fk_rhb001_num_empleado_aprobado`,`empleado_aprobado`.`fk_a003_num_persona` AS `fk_persona_aprobado`,concat(`persona_aprobacion`.`ind_nombre1`,' ',`persona_aprobacion`.`ind_nombre2`,' ',`persona_aprobacion`.`ind_apellido1`,' ',`persona_aprobacion`.`ind_apellido2`) AS `nombre_persona_aprobacion`,`rh_c097_capacitacion`.`fec_ultima_modificacion` AS `fec_ultima_modificacion`,`rh_c097_capacitacion`.`fk_a018_num_seguridad_usuario` AS `fk_a018_num_seguridad_usuario`,`rh_c097_capacitacion`.`ind_estado` AS `estado_capacitacion`,`rh_c097_capacitacion`.`num_total_dias` AS `num_total_dias`,`rh_c097_capacitacion`.`ind_total_horas` AS `ind_total_horas` from (((((((((((((((((`rh_c097_capacitacion` join `a001_organismo` on((`rh_c097_capacitacion`.`fk_a001_num_organismo` = `a001_organismo`.`pk_num_organismo`))) join `rh_b001_empleado` `empleado_solicitante` on((`rh_c097_capacitacion`.`fk_rhb001_empleado_solicitante` = `empleado_solicitante`.`pk_num_empleado`))) join `a003_persona` `persona_solicitante` on((`empleado_solicitante`.`fk_a003_num_persona` = `persona_solicitante`.`pk_num_persona`))) join `rh_c048_curso` on((`rh_c097_capacitacion`.`fk_rhc048_num_curso` = `rh_c048_curso`.`pk_num_curso`))) join `rh_c040_institucion` on((`rh_c097_capacitacion`.`fk_rhc040_centro_estudio` = `rh_c040_institucion`.`pk_num_institucion`))) join `a010_ciudad` on((`rh_c097_capacitacion`.`fk_a010_num_ciudad` = `a010_ciudad`.`pk_num_ciudad`))) join `a011_municipio`) join `a009_estado` on((`a011_municipio`.`fk_a009_num_estado` = `a009_estado`.`pk_num_estado`))) join `a008_pais` on((`a009_estado`.`fk_a008_num_pais` = `a008_pais`.`pk_num_pais`))) join `a006_miscelaneo_detalle` `tipo_capacitacion` on((`rh_c097_capacitacion`.`fk_a006_num_miscelnaeo_detalle_tipocap` = `tipo_capacitacion`.`pk_num_miscelaneo_detalle`))) join `a006_miscelaneo_detalle` `modalidad` on((`rh_c097_capacitacion`.`fk_a006_num_miscelnaeo_detalle_modalidad` = `modalidad`.`pk_num_miscelaneo_detalle`))) join `a006_miscelaneo_detalle` `origen` on((`rh_c097_capacitacion`.`fk_a006_num_miscelnaeo_detalle_origen` = `origen`.`pk_num_miscelaneo_detalle`))) join `rh_b001_empleado` `empleado_creacion` on((`rh_c097_capacitacion`.`fk_rhb001_num_empleado_creado` = `empleado_creacion`.`pk_num_empleado`))) join `a003_persona` `persona_creacion` on((`empleado_creacion`.`fk_a003_num_persona` = `persona_creacion`.`pk_num_persona`))) left join `rh_b001_empleado` `empleado_aprobado` on((`rh_c097_capacitacion`.`fk_rhb001_num_empleado_aprobado` = `empleado_aprobado`.`pk_num_empleado`))) left join `a003_persona` `persona_aprobacion` on((`empleado_aprobado`.`fk_a003_num_persona` = `persona_aprobacion`.`pk_num_persona`))) join `a014_ciudad_municipio` on(((`a014_ciudad_municipio`.`fk_a011_num_municipio` = `a011_municipio`.`pk_num_municipio`) and (`a014_ciudad_municipio`.`fk_a010_num_ciudad` = `a010_ciudad`.`pk_num_ciudad`)))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_capacitaciones_empleados`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_capacitaciones_empleados`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`dmunoz`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_capacitaciones_empleados` AS select `rh_c099_capacitacion_empleados`.`pk_num_capacitacion_empleado` AS `pk_num_capacitacion_empleado`,`rh_c099_capacitacion_empleados`.`fk_rhc097_num_capacitacion` AS `fk_rhc097_num_capacitacion`,`rh_c099_capacitacion_empleados`.`fk_rhb001_num_empleado` AS `fk_rhb001_num_empleado`,`a003_persona`.`ind_cedula_documento` AS `ind_cedula_documento`,`a003_persona`.`ind_nombre1` AS `ind_nombre1`,`a003_persona`.`ind_nombre2` AS `ind_nombre2`,`a003_persona`.`ind_apellido1` AS `ind_apellido1`,`a003_persona`.`ind_apellido2` AS `ind_apellido2`,`rh_c099_capacitacion_empleados`.`fk_a004_num_dependencia` AS `fk_a004_num_dependencia`,`a004_dependencia`.`ind_dependencia` AS `ind_dependencia`,`rh_c099_capacitacion_empleados`.`num_asistencias` AS `num_asistencias`,`rh_c099_capacitacion_empleados`.`num_dias_asistidos` AS `num_dias_asistidos`,`rh_c099_capacitacion_empleados`.`ind_horas_asistencia` AS `ind_horas_asistencia`,`rh_c099_capacitacion_empleados`.`ind_calificacion` AS `ind_calificacion`,`rh_c099_capacitacion_empleados`.`num_costo_individual` AS `num_costo_individual`,`rh_c099_capacitacion_empleados`.`num_importe_gastos` AS `num_importe_gastos`,`rh_c099_capacitacion_empleados`.`flag_aprobado` AS `flag_aprobado`,`rh_c099_capacitacion_empleados`.`fec_ultima_modificacion` AS `fec_ultima_modificacion`,`rh_c099_capacitacion_empleados`.`fk_a018_num_seguridad_usuario` AS `fk_a018_num_seguridad_usuario` from ((((`rh_c099_capacitacion_empleados` join `rh_c097_capacitacion` on((`rh_c099_capacitacion_empleados`.`fk_rhc097_num_capacitacion` = `rh_c097_capacitacion`.`pk_num_capacitacion`))) join `rh_b001_empleado` on((`rh_c099_capacitacion_empleados`.`fk_rhb001_num_empleado` = `rh_b001_empleado`.`pk_num_empleado`))) join `a003_persona` on((`rh_b001_empleado`.`fk_a003_num_persona` = `a003_persona`.`pk_num_persona`))) join `a004_dependencia` on((`rh_c099_capacitacion_empleados`.`fk_a004_num_dependencia` = `a004_dependencia`.`pk_num_dependencia`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_capacitaciones_fundamentos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_capacitaciones_fundamentos`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`dmunoz`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_capacitaciones_fundamentos` AS select `rh_c098_capacitacion_fundamentos`.`pk_num_capacitacion_fundamentos` AS `pk_num_capacitacion_fundamentos`,`rh_c098_capacitacion_fundamentos`.`fk_rhc097_num_capacitacion` AS `fk_rhc097_num_capacitacion`,`rh_c098_capacitacion_fundamentos`.`txt_fundamentacion_1` AS `txt_fundamentacion_1`,`rh_c098_capacitacion_fundamentos`.`txt_fundamentacion_2` AS `txt_fundamentacion_2`,`rh_c098_capacitacion_fundamentos`.`txt_fundamentacion_3` AS `txt_fundamentacion_3`,`rh_c098_capacitacion_fundamentos`.`txt_fundamentacion_4` AS `txt_fundamentacion_4`,`rh_c098_capacitacion_fundamentos`.`txt_fundamentacion_5` AS `txt_fundamentacion_5`,`rh_c098_capacitacion_fundamentos`.`txt_fundamentacion_6` AS `txt_fundamentacion_6`,`rh_c098_capacitacion_fundamentos`.`txt_fundamentacion_7` AS `txt_fundamentacion_7` from (`rh_c098_capacitacion_fundamentos` join `rh_c097_capacitacion` on((`rh_c098_capacitacion_fundamentos`.`fk_rhc097_num_capacitacion` = `rh_c097_capacitacion`.`pk_num_capacitacion`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_capacitaciones_horarios`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_capacitaciones_horarios`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`dmunoz`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_capacitaciones_horarios` AS select `rh_c101_capacitacion_horario`.`pk_num_capacitacion_horario` AS `pk_num_capacitacion_horario`,`rh_c101_capacitacion_horario`.`fk_rhc097_num_capacitacion` AS `fk_rhc097_num_capacitacion`,`rh_c101_capacitacion_horario`.`fk_rhb001_num_empleado` AS `fk_rhb001_num_empleado`,`a003_persona`.`ind_cedula_documento` AS `ind_cedula_documento`,`a003_persona`.`ind_nombre1` AS `ind_nombre1`,`a003_persona`.`ind_nombre2` AS `ind_nombre2`,`a003_persona`.`ind_apellido1` AS `ind_apellido1`,`a003_persona`.`ind_apellido2` AS `ind_apellido2`,`rh_c101_capacitacion_horario`.`fec_fecha` AS `fec_fecha`,`rh_c101_capacitacion_horario`.`fec_hora_ini` AS `fec_hora_ini`,`rh_c101_capacitacion_horario`.`fec_hora_fin` AS `fec_hora_fin`,`rh_c101_capacitacion_horario`.`ind_total_horas` AS `ind_total_horas`,`rh_c101_capacitacion_horario`.`fec_ultima_modificacion` AS `fec_ultima_modificacion`,`rh_c101_capacitacion_horario`.`fk_a018_num_seguridad_usuario` AS `fk_a018_num_seguridad_usuario` from (((`rh_c101_capacitacion_horario` join `rh_c097_capacitacion` on((`rh_c101_capacitacion_horario`.`fk_rhc097_num_capacitacion` = `rh_c097_capacitacion`.`pk_num_capacitacion`))) join `rh_b001_empleado` on((`rh_c101_capacitacion_horario`.`fk_rhb001_num_empleado` = `rh_b001_empleado`.`pk_num_empleado`))) join `a003_persona` on((`rh_b001_empleado`.`fk_a003_num_persona` = `a003_persona`.`pk_num_persona`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_capacitaciones_ubicacion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_capacitaciones_ubicacion`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_capacitaciones_ubicacion` AS select `rh_c097_capacitacion`.`pk_num_capacitacion` AS `pk_num_capacitacion`,`rh_c097_capacitacion`.`fk_a001_num_organismo` AS `fk_a001_num_organismo`,`rh_c097_capacitacion`.`fk_a010_num_ciudad` AS `fk_a010_num_ciudad`,`a010_ciudad`.`ind_ciudad` AS `ind_ciudad`,`a011_municipio`.`pk_num_municipio` AS `pk_num_municipio`,`a011_municipio`.`ind_municipio` AS `ind_municipio`,`a009_estado`.`pk_num_estado` AS `pk_num_estado`,`a009_estado`.`ind_estado` AS `ind_estado`,`a008_pais`.`pk_num_pais` AS `pk_num_pais`,`a008_pais`.`ind_pais` AS `ind_pais` from (((((`rh_c097_capacitacion` join `a010_ciudad` on((`a010_ciudad`.`pk_num_ciudad` = `rh_c097_capacitacion`.`fk_a010_num_ciudad`))) join `a014_ciudad_municipio` on((`a014_ciudad_municipio`.`fk_a010_num_ciudad` = `a010_ciudad`.`pk_num_ciudad`))) join `a011_municipio` on((`a014_ciudad_municipio`.`fk_a011_num_municipio` = `a011_municipio`.`pk_num_municipio`))) join `a009_estado` on((`a011_municipio`.`fk_a009_num_estado` = `a009_estado`.`pk_num_estado`))) join `a008_pais` on((`a009_estado`.`fk_a008_num_pais` = `a008_pais`.`pk_num_pais`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_empleado_carga_familiar`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_empleado_carga_familiar`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_empleado_carga_familiar` AS select `rh_c015_carga_familiar`.`pk_num_carga` AS `pk_num_carga`,`rh_b001_empleado`.`pk_num_empleado` AS `pk_num_empleado`,`a003_persona`.`pk_num_persona` AS `pk_num_persona`,`rh_b001_empleado`.`fk_a003_num_persona` AS `fk_a003_num_persona`,`a003_persona`.`ind_cedula_documento` AS `ind_cedula_documento`,concat(`a003_persona`.`ind_nombre1`,' ',`a003_persona`.`ind_nombre2`,' ',`a003_persona`.`ind_apellido1`,' ',`a003_persona`.`ind_apellido2`) AS `nombre`,`a003_persona`.`ind_nombre1` AS `ind_nombre1`,`a003_persona`.`ind_nombre2` AS `ind_nombre2`,`a003_persona`.`ind_apellido1` AS `ind_apellido1`,`a003_persona`.`ind_apellido2` AS `ind_apellido2`,`a003_persona`.`fk_a006_num_miscelaneo_detalle_sexo` AS `fk_a006_num_miscelaneo_detalle_sexo`,`parentesco`.`cod_detalle` AS `detalle_parentesco`,`a003_persona`.`fk_a006_num_miscelaneo_detalle_nacionalidad` AS `fk_a006_num_miscelaneo_detalle_nacionalidad`,`a003_persona`.`fec_nacimiento` AS `fec_nacimiento`,`a003_persona`.`fk_a006_num_miscelaneo_detalle_edocivil` AS `fk_a006_num_miscelaneo_detalle_edocivil`,`rh_b001_empleado`.`num_estatus` AS `ind_estatus`,`a003_persona`.`num_estatus` AS `ind_estatus_persona`,`rh_c015_carga_familiar`.`fk_a006_num_miscelaneo_detalle_parentezco` AS `fk_a006_num_miscelaneo_detalle_parentezco`,`rh_c015_carga_familiar`.`fk_a006_num_miscelaneo_detalle_gradoinst` AS `fk_a006_num_miscelaneo_detalle_gradoinst`,`rh_c015_carga_familiar`.`fk_a006_num_miscelaneo_detalle_tipoedu` AS `fk_a006_num_miscelaneo_detalle_tipoedu`,`rh_c015_carga_familiar`.`fk_rhc040_num_institucion` AS `fk_rhc040_num_institucion`,`rh_c015_carga_familiar`.`ind_empresa` AS `ind_empresa`,`rh_c015_carga_familiar`.`txt_direccion_empresa` AS `txt_direccion_empresa`,`rh_c015_carga_familiar`.`num_sueldo_mensual` AS `num_sueldo_mensual`,`a003_persona`.`fk_a006_num_miscelaneo_det_gruposangre` AS `fk_a006_num_miscelaneo_det_gruposangre`,`a003_persona`.`ind_foto` AS `ind_foto`,`rh_c015_carga_familiar`.`num_flag_discapacidad` AS `num_flag_discapacidad`,`rh_c015_carga_familiar`.`num_flag_estudia` AS `num_flag_estudia`,`rh_c015_carga_familiar`.`num_flag_trabaja` AS `num_flag_trabaja`,`sexo`.`cod_detalle` AS `detalle_sexo`,`sexo`.`ind_nombre_detalle` AS `sexo`,`parentesco`.`ind_nombre_detalle` AS `parentesco`,`rh_c015_carga_familiar`.`fk_rhc064_nivel_grado_instruccion` AS `fk_rhc064_nivel_grado_instruccion` from ((((`rh_c015_carga_familiar` join `rh_b001_empleado` on((`rh_c015_carga_familiar`.`fk_rhb001_num_empleado` = `rh_b001_empleado`.`pk_num_empleado`))) join `a003_persona` on((`rh_c015_carga_familiar`.`fk_a003_num_persona` = `a003_persona`.`pk_num_persona`))) join `a006_miscelaneo_detalle` `parentesco` on((`parentesco`.`pk_num_miscelaneo_detalle` = `rh_c015_carga_familiar`.`fk_a006_num_miscelaneo_detalle_parentezco`))) join `a006_miscelaneo_detalle` `sexo` on((`sexo`.`pk_num_miscelaneo_detalle` = `a003_persona`.`fk_a006_num_miscelaneo_detalle_sexo`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_empleado_carga_familiar_direccion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_empleado_carga_familiar_direccion`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`dmunoz`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_empleado_carga_familiar_direccion` AS select `rh_c015_carga_familiar`.`pk_num_carga` AS `pk_num_carga`,`rh_b001_empleado`.`pk_num_empleado` AS `pk_num_empleado`,`a003_persona`.`pk_num_persona` AS `pk_num_persona`,`a036_persona_direccion`.`pk_num_direccion_persona` AS `pk_num_direccion_persona`,`a036_persona_direccion`.`fk_a006_num_miscelaneo_detalle_tipodir` AS `fk_a006_num_miscelaneo_detalle_tipodir`,`a036_persona_direccion`.`fk_a006_num_miscelaneo_detalle_domicilio` AS `fk_a006_num_miscelaneo_detalle_domicilio`,`a036_persona_direccion`.`ind_direccion` AS `ind_direccion` from (((`rh_c015_carga_familiar` join `rh_b001_empleado` on((`rh_c015_carga_familiar`.`fk_rhb001_num_empleado` = `rh_b001_empleado`.`pk_num_empleado`))) join `a003_persona` on((`rh_c015_carga_familiar`.`fk_a003_num_persona` = `a003_persona`.`pk_num_persona`))) join `a036_persona_direccion` on((`a003_persona`.`pk_num_persona` = `a036_persona_direccion`.`fk_a003_num_persona`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_empleado_carga_familiar_telefono`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_empleado_carga_familiar_telefono`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_empleado_carga_familiar_telefono` AS select `rh_c015_carga_familiar`.`pk_num_carga` AS `pk_num_carga`,`rh_b001_empleado`.`pk_num_empleado` AS `pk_num_empleado`,`a003_persona`.`pk_num_persona` AS `pk_num_persona`,`a007_persona_telefono`.`pk_num_telefono` AS `pk_num_telefono`,`a007_persona_telefono`.`ind_telefono` AS `ind_telefono`,`a007_persona_telefono`.`fk_a006_num_miscelaneo_detalle_tipo_telefono` AS `fk_a006_num_miscelaneo_detalle_tipo_telefono` from (((`rh_c015_carga_familiar` join `rh_b001_empleado` on((`rh_c015_carga_familiar`.`fk_rhb001_num_empleado` = `rh_b001_empleado`.`pk_num_empleado`))) join `a003_persona` on((`rh_c015_carga_familiar`.`fk_a003_num_persona` = `a003_persona`.`pk_num_persona`))) join `a007_persona_telefono` on((`a007_persona_telefono`.`fk_a003_num_persona` = `a003_persona`.`pk_num_persona`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_empleado_experiencia_laboral`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_empleado_experiencia_laboral`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_empleado_experiencia_laboral` AS select `rh_c011_experiencia`.`pk_num_experiencia` AS `pk_num_experiencia`,`rh_b001_empleado`.`pk_num_empleado` AS `pk_num_empleado`,`rh_c011_experiencia`.`ind_cargo` AS `ind_cargo`,`rh_c011_experiencia`.`ind_empresa` AS `ind_empresa`,`rh_c011_experiencia`.`fk_a006_num_miscelaneo_detalle_areaexp` AS `fk_a006_num_miscelaneo_detalle_areaexp`,`areaexp`.`ind_nombre_detalle` AS `areaexp`,`rh_c011_experiencia`.`fec_ingreso` AS `fec_ingreso`,`rh_c011_experiencia`.`fec_egreso` AS `fec_egreso`,`rh_c011_experiencia`.`fk_a006_num_miscelaneo_detalle_tipo_empresa` AS `fk_a006_num_miscelaneo_detalle_tipo_empresa`,`tipoente`.`ind_nombre_detalle` AS `tipoente`,`rh_c011_experiencia`.`fk_rhc032_num_motivo_cese` AS `fk_rhc032_num_motivo_cese`,`rh_c032_motivo_cese`.`ind_nombre_cese` AS `ind_nombre_cese`,`rh_c011_experiencia`.`num_sueldo_mensual` AS `num_sueldo_mensual`,`rh_c011_experiencia`.`num_flag_antvacacion` AS `num_flag_antvacacion`,`rh_c011_experiencia`.`txt_funciones` AS `txt_funciones`,`rh_c011_experiencia`.`fec_ultima_modificacion` AS `fec_ultima_modificacion`,`rh_c011_experiencia`.`fk_a018_num_seguridad_usuario` AS `fk_a018_num_seguridad_usuario` from ((((`rh_c011_experiencia` join `rh_b001_empleado` on((`rh_c011_experiencia`.`fk_rhb001_num_empleado` = `rh_b001_empleado`.`pk_num_empleado`))) join `a006_miscelaneo_detalle` `areaexp` on((`areaexp`.`pk_num_miscelaneo_detalle` = `rh_c011_experiencia`.`fk_a006_num_miscelaneo_detalle_areaexp`))) join `a006_miscelaneo_detalle` `tipoente` on((`tipoente`.`pk_num_miscelaneo_detalle` = `rh_c011_experiencia`.`fk_a006_num_miscelaneo_detalle_tipo_empresa`))) join `rh_c032_motivo_cese` on((`rh_c032_motivo_cese`.`pk_num_motivo_cese` = `rh_c011_experiencia`.`fk_rhc032_num_motivo_cese`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_empleado_informacion_bancaria`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_empleado_informacion_bancaria`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_empleado_informacion_bancaria` AS select `rh_c012_empleado_banco`.`pk_num_empleado_banco` AS `pk_num_empleado_banco`,`rh_b001_empleado`.`pk_num_empleado` AS `pk_num_empleado`,`rh_b001_empleado`.`fk_a003_num_persona` AS `fk_a003_num_persona`,`rh_c012_empleado_banco`.`fk_a006_num_miscelaneo_detalle_banco` AS `fk_a006_num_miscelaneo_detalle_banco`,`banco`.`ind_nombre_detalle` AS `banco`,`rh_c012_empleado_banco`.`fk_a006_num_miscelaneo_detalle_tipocta` AS `fk_a006_num_miscelaneo_detalle_tipocta`,`tipocta`.`ind_nombre_detalle` AS `tipocta`,`rh_c012_empleado_banco`.`fk_a006_num_miscelaneo_detalle_tipoapo` AS `fk_a006_num_miscelaneo_detalle_tipoapo`,`tipoapo`.`ind_nombre_detalle` AS `tipoapo`,`rh_c012_empleado_banco`.`ind_cuenta` AS `ind_cuenta`,`rh_c012_empleado_banco`.`num_sueldo_mensual` AS `num_sueldo_mensual`,`rh_c012_empleado_banco`.`num_flag_principal` AS `num_flag_principal` from ((((`rh_c012_empleado_banco` join `rh_b001_empleado` on((`rh_c012_empleado_banco`.`fk_rhb001_num_empleado` = `rh_b001_empleado`.`pk_num_empleado`))) join `a006_miscelaneo_detalle` `banco` on((`rh_c012_empleado_banco`.`fk_a006_num_miscelaneo_detalle_banco` = `banco`.`pk_num_miscelaneo_detalle`))) join `a006_miscelaneo_detalle` `tipocta` on((`rh_c012_empleado_banco`.`fk_a006_num_miscelaneo_detalle_tipocta` = `tipocta`.`pk_num_miscelaneo_detalle`))) join `a006_miscelaneo_detalle` `tipoapo` on((`rh_c012_empleado_banco`.`fk_a006_num_miscelaneo_detalle_tipoapo` = `tipoapo`.`pk_num_miscelaneo_detalle`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_empleado_jubilacion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_empleado_jubilacion`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_empleado_jubilacion` AS select `rh_b010_jubilacion`.`pk_num_jubilacion` AS `pk_num_jubilacion`,`rh_b010_jubilacion`.`fk_rhb001_num_empleado` AS `fk_rhb001_num_empleado`,`rh_b010_jubilacion`.`fk_nmb001_num_tipo_nomina` AS `fk_nmb001_num_tipo_nomina`,`rh_b010_jubilacion`.`fk_a006_num_tipo_trabajador` AS `fk_a006_num_tipo_trabajador`,`rh_b010_jubilacion`.`fk_rhc032_num_motivo_cese` AS `fk_rhc032_num_motivo_cese`,`rh_b010_jubilacion`.`fk_a001_num_organismo` AS `fk_a001_num_organismo`,`rh_b010_jubilacion`.`fk_a004_num_dependencia` AS `fk_a004_num_dependencia`,`rh_b010_jubilacion`.`fk_rhc063_num_puesto` AS `fk_rhc063_num_puesto`,`rh_b010_jubilacion`.`fec_ingreso` AS `fec_ingreso`,`rh_b010_jubilacion`.`fec_egreso` AS `fec_egreso`,`rh_b010_jubilacion`.`ind_periodo` AS `ind_periodo`,`rh_b010_jubilacion`.`num_edad` AS `num_edad`,`rh_b010_jubilacion`.`num_anio_servicio` AS `num_anio_servicio`,`rh_b010_jubilacion`.`num_anio_experiencia` AS `num_anio_experiencia`,`rh_b010_jubilacion`.`num_anio_total` AS `num_anio_total`,`rh_b010_jubilacion`.`ind_observacion_cese` AS `ind_observacion_cese`,`rh_b010_jubilacion`.`fk_a018_num_seguridad_usuario` AS `fk_a018_num_seguridad_usuario`,`rh_b010_jubilacion`.`fec_ultima_modificacion` AS `fec_ultima_modificacion`,`rh_c083_montos_jubilacion`.`num_ultimo_sueldo` AS `num_ultimo_sueldo`,`rh_c083_montos_jubilacion`.`num_total_sueldo` AS `num_total_sueldo`,`rh_c083_montos_jubilacion`.`num_total_primas` AS `num_total_primas`,`rh_c083_montos_jubilacion`.`num_sueldo_base` AS `num_sueldo_base`,`rh_c083_montos_jubilacion`.`num_porcentaje` AS `num_porcentaje`,`rh_c083_montos_jubilacion`.`num_coeficiente` AS `num_coeficiente`,`rh_c083_montos_jubilacion`.`num_monto_jubilacion` AS `num_monto_jubilacion` from (`rh_b010_jubilacion` join `rh_c083_montos_jubilacion` on((`rh_c083_montos_jubilacion`.`fk_rhb010_num_jubilacion` = `rh_b010_jubilacion`.`pk_num_jubilacion`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_info_registro_empleado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_info_registro_empleado`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_info_registro_empleado` AS select `rh_c003_operaciones_empleado`.`pk_num_operaciones_empleado` AS `pk_num_operaciones_empleado`,`rh_c003_operaciones_empleado`.`ind_estado` AS `txt_estatus`,`e2`.`pk_num_empleado` AS `empleado_registro`,`p2`.`pk_num_persona` AS `persona_registro`,`p2`.`ind_nombre1` AS `nombre1_empleado`,`p2`.`ind_nombre2` AS `nombre2_empleado`,`p2`.`ind_apellido1` AS `apellido1_empleado`,`p2`.`ind_apellido2` AS `apellido2_empleado`,`e1`.`pk_num_empleado` AS `empleado_operacion`,`p1`.`pk_num_persona` AS `persona_operacion`,`p1`.`ind_nombre1` AS `nombre1_operacion`,`p1`.`ind_nombre2` AS `nombre2_operacion`,`p1`.`ind_apellido1` AS `apellido1_operacion`,`p1`.`ind_apellido2` AS `apellido2_operacion`,`rh_c003_operaciones_empleado`.`fec_operacion` AS `fec_operacion` from ((((`rh_c003_operaciones_empleado` join `rh_b001_empleado` `e1` on((`e1`.`pk_num_empleado` = `rh_c003_operaciones_empleado`.`fk_rhb001_num_empleado_operacion`))) join `a003_persona` `p1` on((`e1`.`fk_a003_num_persona` = `p1`.`pk_num_persona`))) join `rh_b001_empleado` `e2` on((`e2`.`pk_num_empleado` = `rh_c003_operaciones_empleado`.`fk_rhb001_num_empleado_registro`))) join `a003_persona` `p2` on((`e2`.`fk_a003_num_persona` = `p2`.`pk_num_persona`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_pension_invalidez`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_pension_invalidez`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_pension_invalidez` AS select `rh_b003_pension`.`pk_num_proceso_pension` AS `pk_num_proceso_pension`,`rh_c034_invalidez`.`pk_num_invalidez` AS `pk_num_invalidez`,`rh_b003_pension`.`fk_rhb001_num_empleado` AS `fk_rhb001_num_empleado`,`a003_persona`.`pk_num_persona` AS `pk_num_persona`,`a003_persona`.`ind_cedula_documento` AS `ind_cedula_documento`,concat(`a003_persona`.`ind_nombre1`,' ',`a003_persona`.`ind_nombre2`,' ',`a003_persona`.`ind_apellido1`,' ',`a003_persona`.`ind_apellido2`) AS `nombre_completo`,`rh_b003_pension`.`fk_a001_num_organismo` AS `fk_a001_num_organismo`,`rh_b003_pension`.`ind_organismo` AS `ind_organismo`,`rh_b003_pension`.`fk_a004_num_dependencia` AS `fk_a004_num_dependencia`,`rh_b003_pension`.`ind_dependencia` AS `ind_dependencia`,`rh_b003_pension`.`fk_rhc063_num_puestos` AS `fk_rhc063_num_puestos`,`rh_b003_pension`.`ind_cargo` AS `ind_cargo`,`rh_b003_pension`.`num_ultimo_sueldo` AS `num_ultimo_sueldo`,`rh_b003_pension`.`num_anio_servicio` AS `num_anio_servicio`,`rh_b003_pension`.`num_edad` AS `num_edad`,`rh_b003_pension`.`fec_fecha` AS `fec_fecha`,`rh_b003_pension`.`fec_fecha_ingreso` AS `fec_fecha_ingreso`,`rh_b003_pension`.`fk_a006_num_miscelaneo_detalle_tipopension` AS `fk_a006_num_miscelaneo_detalle_tipopension`,`rh_b003_pension`.`ind_detalle_tipopension` AS `ind_detalle_tipopension`,`tipopension`.`ind_nombre_detalle` AS `tipo_pension`,`rh_b003_pension`.`fk_a006_num_miscelaneo_detalle_motivopension` AS `fk_a006_num_miscelaneo_detalle_motivopension`,`rh_b003_pension`.`ind_detalle_motivopension` AS `ind_detalle_motivopension`,`motivopension`.`ind_nombre_detalle` AS `motivo_pension`,`rh_c034_invalidez`.`num_monto_pension` AS `num_monto_pension`,`rh_c034_invalidez`.`ind_periodo` AS `ind_periodo`,`rh_c034_invalidez`.`fk_nmb001_num_tipo_nomina` AS `fk_nmb001_num_tipo_nomina`,`nm_b001_tipo_nomina`.`ind_nombre_nomina` AS `ind_nombre_nomina`,`rh_c034_invalidez`.`fk_a006_num_miscelaneo_detalle_tipotrab` AS `fk_a006_num_miscelaneo_detalle_tipotrab`,`a006_miscelaneo_detalle`.`ind_nombre_detalle` AS `tipo_trabajador`,`rh_c034_invalidez`.`num_situacion_trabajo` AS `num_situacion_trabajo`,`rh_c034_invalidez`.`fk_rhc032_num_motivo_cese` AS `fk_rhc032_num_motivo_cese`,`rh_c032_motivo_cese`.`ind_nombre_cese` AS `ind_nombre_cese`,`rh_c034_invalidez`.`fec_fecha_egreso` AS `fec_fecha_egreso`,`rh_c034_invalidez`.`txt_observacion_egreso` AS `txt_observacion_egreso`,`rh_c034_invalidez`.`ind_resolucion` AS `ind_resolucion`,`rh_b003_pension`.`fec_ultima_modificacion` AS `fec_ultima_modificacion`,`rh_b003_pension`.`fk_a018_num_seguridad_usuario` AS `fk_a018_num_seguridad_usuario`,`rh_c086_operaciones_pension`.`ind_estado` AS `txt_estatus`,`rh_c086_operaciones_pension`.`num_flag_estatus` AS `num_flag_estatus`,`rh_c086_operaciones_pension`.`fec_operacion` AS `fec_operacion`,`a003_persona`.`fk_a006_num_miscelaneo_detalle_sexo` AS `fk_a006_num_miscelaneo_detalle_sexo`,`sexo_persona`.`ind_nombre_detalle` AS `sexo`,`a003_persona`.`fec_nacimiento` AS `fec_nacimiento` from ((((((((((`rh_b003_pension` join `rh_b001_empleado` on((`rh_b003_pension`.`fk_rhb001_num_empleado` = `rh_b001_empleado`.`pk_num_empleado`))) join `a003_persona` on((`rh_b001_empleado`.`fk_a003_num_persona` = `a003_persona`.`pk_num_persona`))) join `a006_miscelaneo_detalle` `tipopension` on((`rh_b003_pension`.`fk_a006_num_miscelaneo_detalle_tipopension` = `tipopension`.`pk_num_miscelaneo_detalle`))) join `a006_miscelaneo_detalle` `motivopension` on((`rh_b003_pension`.`fk_a006_num_miscelaneo_detalle_motivopension` = `motivopension`.`pk_num_miscelaneo_detalle`))) join `rh_c034_invalidez` on((`rh_b003_pension`.`pk_num_proceso_pension` = `rh_c034_invalidez`.`fk_rhb003_num_proceso_pension`))) left join `nm_b001_tipo_nomina` on((`nm_b001_tipo_nomina`.`pk_num_tipo_nomina` = `rh_c034_invalidez`.`fk_nmb001_num_tipo_nomina`))) left join `a006_miscelaneo_detalle` on((`a006_miscelaneo_detalle`.`pk_num_miscelaneo_detalle` = `rh_c034_invalidez`.`fk_a006_num_miscelaneo_detalle_tipotrab`))) left join `rh_c032_motivo_cese` on((`rh_c032_motivo_cese`.`pk_num_motivo_cese` = `rh_c034_invalidez`.`fk_rhc032_num_motivo_cese`))) join `rh_c086_operaciones_pension` on((`rh_b003_pension`.`pk_num_proceso_pension` = `rh_c086_operaciones_pension`.`fk_rhb003_num_pension`))) join `a006_miscelaneo_detalle` `sexo_persona` on((`a003_persona`.`fk_a006_num_miscelaneo_detalle_sexo` = `sexo_persona`.`pk_num_miscelaneo_detalle`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_pension_sobreviviente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_pension_sobreviviente`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_pension_sobreviviente` AS select `rh_b003_pension`.`pk_num_proceso_pension` AS `pk_num_proceso_pension`,`rh_c035_sobreviviente`.`pk_num_sobreviviente` AS `pk_num_sobreviviente`,`rh_b003_pension`.`fk_rhb001_num_empleado` AS `fk_rhb001_num_empleado`,`a003_persona`.`pk_num_persona` AS `pk_num_persona`,`a003_persona`.`ind_cedula_documento` AS `ind_cedula_documento`,concat(`a003_persona`.`ind_nombre1`,' ',`a003_persona`.`ind_nombre2`,' ',`a003_persona`.`ind_apellido1`,' ',`a003_persona`.`ind_apellido2`) AS `nombre_completo`,`rh_b003_pension`.`fk_a001_num_organismo` AS `fk_a001_num_organismo`,`rh_b003_pension`.`ind_organismo` AS `ind_organismo`,`rh_b003_pension`.`fk_a004_num_dependencia` AS `fk_a004_num_dependencia`,`rh_b003_pension`.`ind_dependencia` AS `ind_dependencia`,`rh_b003_pension`.`fk_rhc063_num_puestos` AS `fk_rhc063_num_puestos`,`rh_b003_pension`.`ind_cargo` AS `ind_cargo`,`rh_b003_pension`.`num_ultimo_sueldo` AS `num_ultimo_sueldo`,`rh_b003_pension`.`num_anio_servicio` AS `num_anio_servicio`,`rh_b003_pension`.`num_edad` AS `num_edad`,`rh_b003_pension`.`fec_fecha` AS `fec_fecha`,`rh_b003_pension`.`fec_fecha_ingreso` AS `fec_fecha_ingreso`,`rh_b003_pension`.`fk_a006_num_miscelaneo_detalle_tipopension` AS `fk_a006_num_miscelaneo_detalle_tipopension`,`rh_b003_pension`.`ind_detalle_tipopension` AS `ind_detalle_tipopension`,`tipopension`.`ind_nombre_detalle` AS `tipo_pension`,`rh_b003_pension`.`fk_a006_num_miscelaneo_detalle_motivopension` AS `fk_a006_num_miscelaneo_detalle_motivopension`,`rh_b003_pension`.`ind_detalle_motivopension` AS `ind_detalle_motivopension`,`motivopension`.`ind_nombre_detalle` AS `motivo_pension`,`rh_c035_sobreviviente`.`num_monto_pension` AS `num_monto_pension`,`rh_c035_sobreviviente`.`num_monto_jubilacion` AS `num_monto_jubilacion`,`rh_c035_sobreviviente`.`num_coeficiente` AS `num_coeficiente`,`rh_c035_sobreviviente`.`num_total_sueldo` AS `num_total_sueldo`,`rh_c035_sobreviviente`.`num_anios_servicio_exceso` AS `num_anios_servicio_exceso`,`rh_c035_sobreviviente`.`ind_periodo` AS `ind_periodo`,`rh_c035_sobreviviente`.`fk_nmb001_num_tipo_nomina` AS `fk_nmb001_num_tipo_nomina`,`nm_b001_tipo_nomina`.`ind_nombre_nomina` AS `ind_nombre_nomina`,`rh_c035_sobreviviente`.`fk_a006_num_miscelaneo_detalle_tipotrab` AS `fk_a006_num_miscelaneo_detalle_tipotrab`,`a006_miscelaneo_detalle`.`ind_nombre_detalle` AS `tipo_trabajador`,`rh_c035_sobreviviente`.`num_situacion_trabajo` AS `num_situacion_trabajo`,`rh_c035_sobreviviente`.`fk_rhc032_num_motivo_cese` AS `fk_rhc032_num_motivo_cese`,`rh_c032_motivo_cese`.`ind_nombre_cese` AS `ind_nombre_cese`,`rh_c035_sobreviviente`.`fec_fecha_egreso` AS `fec_fecha_egreso`,`rh_c035_sobreviviente`.`txt_observacion_egreso` AS `txt_observacion_egreso`,`rh_c035_sobreviviente`.`ind_resolucion` AS `ind_resolucion`,`rh_b003_pension`.`fec_ultima_modificacion` AS `fec_ultima_modificacion`,`rh_b003_pension`.`fk_a018_num_seguridad_usuario` AS `fk_a018_num_seguridad_usuario`,`rh_c086_operaciones_pension`.`ind_estado` AS `txt_estatus`,`rh_c086_operaciones_pension`.`num_flag_estatus` AS `num_flag_estatus`,`rh_c086_operaciones_pension`.`fec_operacion` AS `fec_operacion`,`a003_persona`.`fk_a006_num_miscelaneo_detalle_sexo` AS `fk_a006_num_miscelaneo_detalle_sexo`,`sexo_persona`.`ind_nombre_detalle` AS `sexo`,`a003_persona`.`fec_nacimiento` AS `fec_nacimiento` from ((((((((((`rh_b003_pension` join `rh_b001_empleado` on((`rh_b003_pension`.`fk_rhb001_num_empleado` = `rh_b001_empleado`.`pk_num_empleado`))) join `a003_persona` on((`rh_b001_empleado`.`fk_a003_num_persona` = `a003_persona`.`pk_num_persona`))) join `a006_miscelaneo_detalle` `tipopension` on((`rh_b003_pension`.`fk_a006_num_miscelaneo_detalle_tipopension` = `tipopension`.`pk_num_miscelaneo_detalle`))) join `a006_miscelaneo_detalle` `motivopension` on((`rh_b003_pension`.`fk_a006_num_miscelaneo_detalle_motivopension` = `motivopension`.`pk_num_miscelaneo_detalle`))) join `rh_c035_sobreviviente` on((`rh_b003_pension`.`pk_num_proceso_pension` = `rh_c035_sobreviviente`.`fk_rhb003_num_proceso_pension`))) left join `nm_b001_tipo_nomina` on((`nm_b001_tipo_nomina`.`pk_num_tipo_nomina` = `rh_c035_sobreviviente`.`fk_nmb001_num_tipo_nomina`))) left join `a006_miscelaneo_detalle` on((`a006_miscelaneo_detalle`.`pk_num_miscelaneo_detalle` = `rh_c035_sobreviviente`.`fk_a006_num_miscelaneo_detalle_tipotrab`))) left join `rh_c032_motivo_cese` on((`rh_c032_motivo_cese`.`pk_num_motivo_cese` = `rh_c035_sobreviviente`.`fk_rhc032_num_motivo_cese`))) join `rh_c086_operaciones_pension` on((`rh_b003_pension`.`pk_num_proceso_pension` = `rh_c086_operaciones_pension`.`fk_rhb003_num_pension`))) join `a006_miscelaneo_detalle` `sexo_persona` on((`a003_persona`.`fk_a006_num_miscelaneo_detalle_sexo` = `sexo_persona`.`pk_num_miscelaneo_detalle`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_persona_empleado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_persona_empleado`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_persona_empleado` AS select `p`.`pk_num_persona` AS `pk_num_persona`,`rh_b001_empleado`.`pk_num_empleado` AS `pk_num_empleado`,`p`.`ind_cedula_documento` AS `ind_cedula_documento`,`p`.`ind_documento_fiscal` AS `ind_documento_fiscal`,`p`.`ind_nombre1` AS `ind_nombre1`,`p`.`ind_nombre2` AS `ind_nombre2`,`p`.`ind_apellido1` AS `ind_apellido1`,`p`.`ind_apellido2` AS `ind_apellido2`,`p`.`fk_a006_num_miscelaneo_detalle_sexo` AS `fk_a006_num_miscelaneo_detalle_sexo`,`p`.`fk_a006_num_miscelaneo_detalle_nacionalidad` AS `fk_a006_num_miscelaneo_detalle_nacionalidad`,`p`.`fec_nacimiento` AS `fec_nacimiento`,`p`.`ind_lugar_nacimiento` AS `ind_lugar_nacimiento`,`p`.`fk_a006_num_miscelaneo_detalle_edocivil` AS `fk_a006_num_miscelaneo_detalle_edocivil`,`p`.`fec_estado_civil` AS `fec_estado_civil`,`p`.`ind_email` AS `ind_email`,`p`.`ind_foto` AS `ind_foto`,`p`.`ind_tipo_persona` AS `ind_tipo_persona`,`p`.`num_estatus` AS `ind_estatus_persona`,`p`.`fk_a006_num_miscelaneo_det_gruposangre` AS `fk_a006_num_miscelaneo_det_gruposangre`,`p`.`fk_a006_num_miscelaneo_det_tipopersona` AS `fk_a006_num_miscelaneo_det_tipopersona`,`p`.`fk_a010_num_ciudad` AS `fk_a010_num_ciudad`,`p`.`fk_a013_num_sector` AS `fk_a013_num_sector`,`rh_b001_empleado`.`num_estatus` AS `ind_estatus_empleado` from ((`a003_persona` `p` left join `rh_b001_empleado` on((`rh_b001_empleado`.`fk_a003_num_persona` = `p`.`pk_num_persona`))) left join `a006_miscelaneo_detalle` on(((`p`.`fk_a006_num_miscelaneo_det_gruposangre` = `a006_miscelaneo_detalle`.`pk_num_miscelaneo_detalle`) and (`a006_miscelaneo_detalle`.`pk_num_miscelaneo_detalle` = `p`.`fk_a006_num_miscelaneo_detalle_edocivil`) and (`a006_miscelaneo_detalle`.`pk_num_miscelaneo_detalle` = `p`.`fk_a006_num_miscelaneo_detalle_nacionalidad`) and (`a006_miscelaneo_detalle`.`pk_num_miscelaneo_detalle` = `p`.`fk_a006_num_miscelaneo_detalle_sexo`)))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_persona_empleado_datos_laborales`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_persona_empleado_datos_laborales`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_persona_empleado_datos_laborales` AS select `p`.`pk_num_persona` AS `pk_num_persona`,`rh_b001_empleado`.`pk_num_empleado` AS `pk_num_empleado`,`p`.`ind_cedula_documento` AS `ind_cedula_documento`,`p`.`ind_documento_fiscal` AS `ind_documento_fiscal`,`p`.`ind_nombre1` AS `ind_nombre1`,`p`.`ind_nombre2` AS `ind_nombre2`,`p`.`ind_apellido1` AS `ind_apellido1`,`p`.`ind_apellido2` AS `ind_apellido2`,`rh_c005_empleado_laboral`.`fec_ingreso` AS `fec_ingreso`,`rh_c005_empleado_laboral`.`ind_resolucion_ingreso` AS `ind_resolucion_ingreso`,`rh_c005_empleado_laboral`.`fk_rhc063_num_puestos_cargo` AS `fk_rhc063_num_puestos_cargo`,`rh_c005_empleado_laboral`.`fk_rhc063_num_puestos_cargotmp` AS `fk_rhc063_num_puestos_cargotmp`,`rh_c005_empleado_laboral`.`fk_rhb007_num_horario` AS `fk_rhb007_num_horario`,`rh_c005_empleado_laboral`.`fk_a006_num_miscelaneo_detalle_paso` AS `fk_a006_num_miscelaneo_detalle_paso`,`rh_c076_empleado_organizacion`.`fk_a001_num_organismo` AS `fk_a001_num_organismo`,`rh_c076_empleado_organizacion`.`fk_a004_num_dependencia` AS `fk_a004_num_dependencia`,`rh_c076_empleado_organizacion`.`fk_a004_num_dependencia_tmp` AS `fk_a004_num_dependencia_tmp`,`rh_c076_empleado_organizacion`.`fk_a023_num_centro_costo` AS `fk_a023_num_centro_costo`,`rh_c076_empleado_organizacion`.`fk_nmb001_num_tipo_nomina` AS `fk_nmb001_num_tipo_nomina`,`rh_c076_empleado_organizacion`.`fk_a006_num_miscelaneo_detalle_tipopago` AS `fk_a006_num_miscelaneo_detalle_tipopago`,`rh_c076_empleado_organizacion`.`fk_a006_num_miscelaneo_detalle_tipotrabajador` AS `fk_a006_num_miscelaneo_detalle_tipotrabajador`,`rh_c063_puestos`.`fk_a006_num_miscelaneo_detalle_grupoocupacional` AS `fk_a006_num_miscelaneo_detalle_grupoocupacional`,`rh_c063_puestos`.`fk_rhc010_num_serie` AS `fk_rhc010_num_serie`,`rh_c063_puestos`.`fk_a006_num_miscelaneo_detalle_categoria` AS `fk_a006_num_miscelaneo_detalle_categoria`,`rh_c063_puestos`.`num_sueldo_basico` AS `num_sueldo_basico`,`cat`.`ind_nombre_detalle` AS `categoria_cargo`,`rh_b001_empleado`.`num_estatus` AS `ind_estatus`,`rh_c005_empleado_laboral`.`fec_ingreso_anterior` AS `fec_ingreso_anterior`,`rh_c005_empleado_laboral`.`fec_egreso` AS `fec_egreso`,`rh_c005_empleado_laboral`.`ind_resolucion_egreso` AS `ind_resolucion_egreso`,`rh_c005_empleado_laboral`.`num_sueldo_ant` AS `num_sueldo_ant`,`rh_c005_empleado_laboral`.`num_sueldo` AS `num_sueldo` from ((((((`a003_persona` `p` join `rh_b001_empleado` on((`rh_b001_empleado`.`fk_a003_num_persona` = `p`.`pk_num_persona`))) join `rh_c005_empleado_laboral` on((`rh_c005_empleado_laboral`.`fk_rhb001_num_empleado` = `rh_b001_empleado`.`pk_num_empleado`))) join `rh_c076_empleado_organizacion` on((`rh_b001_empleado`.`pk_num_empleado` = `rh_c076_empleado_organizacion`.`fk_rhb001_num_empleado`))) left join `rh_c063_puestos` on((`rh_c005_empleado_laboral`.`fk_rhc063_num_puestos_cargo` = `rh_c063_puestos`.`pk_num_puestos`))) left join `a006_miscelaneo_detalle` on(((`rh_c063_puestos`.`fk_a006_num_miscelaneo_detalle_grupoocupacional` = `a006_miscelaneo_detalle`.`pk_num_miscelaneo_detalle`) and (`rh_c063_puestos`.`fk_a006_num_miscelaneo_detalle_categoria` = `a006_miscelaneo_detalle`.`pk_num_miscelaneo_detalle`)))) left join `a006_miscelaneo_detalle` `cat` on((`rh_c063_puestos`.`fk_a006_num_miscelaneo_detalle_categoria` = `cat`.`pk_num_miscelaneo_detalle`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_persona_empleado_direccion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_persona_empleado_direccion`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_persona_empleado_direccion` AS select `a003_persona`.`pk_num_persona` AS `pk_num_persona`,`a003_persona`.`ind_cedula_documento` AS `ind_cedula_documento`,`rh_b001_empleado`.`pk_num_empleado` AS `pk_num_empleado`,`a036_persona_direccion`.`pk_num_direccion_persona` AS `pk_num_direccion_persona`,`a036_persona_direccion`.`fk_a006_num_miscelaneo_detalle_tipodir` AS `fk_a006_num_miscelaneo_detalle_tipodir`,`a036_persona_direccion`.`fk_a006_num_miscelaneo_detalle_domicilio` AS `fk_a006_num_miscelaneo_detalle_domicilio`,`a036_persona_direccion`.`ind_direccion` AS `ind_direccion` from ((`a003_persona` join `rh_b001_empleado` on((`rh_b001_empleado`.`fk_a003_num_persona` = `a003_persona`.`pk_num_persona`))) join `a036_persona_direccion` on((`a003_persona`.`pk_num_persona` = `a036_persona_direccion`.`fk_a003_num_persona`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_persona_empleado_domicilio`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_persona_empleado_domicilio`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_persona_empleado_domicilio` AS select `a003_persona`.`pk_num_persona` AS `pk_num_persona`,`a003_persona`.`ind_cedula_documento` AS `ind_cedula_documento`,`rh_b001_empleado`.`pk_num_empleado` AS `pk_num_empleado`,`ed`.`pk_num_estado` AS `estado_domicilio`,`md`.`pk_num_municipio` AS `municipio_domicilio`,`pd`.`pk_num_pais` AS `pais_domicilio`,`pr`.`pk_num_parroquia` AS `parroquia_domicilio`,`sd`.`pk_num_sector` AS `sector_domicilio`,`cd`.`pk_num_ciudad` AS `ciudad_domicilio` from (((((((`a003_persona` join `rh_b001_empleado` on((`rh_b001_empleado`.`fk_a003_num_persona` = `a003_persona`.`pk_num_persona`))) join `a013_sector` `sd` on((`sd`.`pk_num_sector` = `a003_persona`.`fk_a013_num_sector`))) join `a012_parroquia` `pr` on((`pr`.`pk_num_parroquia` = `sd`.`fk_a012_num_parroquia`))) join `a011_municipio` `md` on((`md`.`pk_num_municipio` = `pr`.`fk_a011_num_municipio`))) join `a009_estado` `ed` on((`ed`.`pk_num_estado` = `md`.`fk_a009_num_estado`))) join `a008_pais` `pd` on((`pd`.`pk_num_pais` = `ed`.`fk_a008_num_pais`))) join `a010_ciudad` `cd` on((`ed`.`pk_num_estado` = `cd`.`fk_a009_num_estado`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_persona_empleado_emergencia`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_persona_empleado_emergencia`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_persona_empleado_emergencia` AS select `rh_c002_empleado_emergencia`.`fk_rhb001_num_empleado` AS `pk_num_empleado`,`p`.`pk_num_persona` AS `pk_num_persona`,`p`.`ind_cedula_documento` AS `ind_cedula_documento`,`p`.`ind_nombre1` AS `ind_nombre1`,`p`.`ind_nombre2` AS `ind_nombre2`,`p`.`ind_apellido1` AS `ind_apellido1`,`p`.`ind_apellido2` AS `ind_apellido2`,`rh_c002_empleado_emergencia`.`pk_num_empleado_emergencia` AS `pk_num_empleado_emergencia`,`rh_c002_empleado_emergencia`.`fk_a003_num_persona` AS `persona_emergencia`,`a036_persona_direccion`.`fk_a006_num_miscelaneo_detalle_tipodir` AS `fk_a006_num_miscelaneo_detalle_tipodir`,`a036_persona_direccion`.`ind_direccion` AS `ind_direccion`,`rh_c002_empleado_emergencia`.`fk_a006_num_miscelaneo_detalle_parentesco` AS `fk_a006_num_miscelaneo_detalle_parentesco`,`a036_persona_direccion`.`pk_num_direccion_persona` AS `pk_num_direccion_persona`,`a036_persona_direccion`.`fk_a006_num_miscelaneo_detalle_domicilio` AS `fk_a006_num_miscelaneo_detalle_domicilio` from ((`a003_persona` `p` join `rh_c002_empleado_emergencia` on((`rh_c002_empleado_emergencia`.`fk_a003_num_persona` = `p`.`pk_num_persona`))) left join `a036_persona_direccion` on((`a036_persona_direccion`.`fk_a003_num_persona` = `p`.`pk_num_persona`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_persona_empleado_emergencia_telefono`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_persona_empleado_emergencia_telefono`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_persona_empleado_emergencia_telefono` AS select `a007_persona_telefono`.`pk_num_telefono` AS `pk_num_telefono`,`rh_c002_empleado_emergencia`.`fk_rhb001_num_empleado` AS `pk_num_empleado`,`p`.`pk_num_persona` AS `pk_num_persona`,`p`.`ind_cedula_documento` AS `ind_cedula_documento`,`rh_c002_empleado_emergencia`.`pk_num_empleado_emergencia` AS `pk_num_empleado_emergencia`,`rh_c002_empleado_emergencia`.`fk_a003_num_persona` AS `persona_emergencia`,`a007_persona_telefono`.`fk_a006_num_miscelaneo_detalle_tipo_telefono` AS `fk_a006_num_miscelaneo_detalle_tipo_telefono`,`a007_persona_telefono`.`ind_telefono` AS `ind_telefono` from ((`a003_persona` `p` join `rh_c002_empleado_emergencia` on((`rh_c002_empleado_emergencia`.`fk_a003_num_persona` = `p`.`pk_num_persona`))) join `a007_persona_telefono` on((`a007_persona_telefono`.`fk_a003_num_persona` = `p`.`pk_num_persona`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_persona_empleado_licencia`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_persona_empleado_licencia`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_persona_empleado_licencia` AS select `a003_persona`.`pk_num_persona` AS `pk_num_persona`,`rh_b001_empleado`.`pk_num_empleado` AS `pk_num_empleado`,`a003_persona`.`ind_cedula_documento` AS `ind_cedula_documento`,`a003_persona`.`ind_nombre1` AS `ind_nombre1`,`a003_persona`.`ind_nombre2` AS `ind_nombre2`,`a003_persona`.`ind_apellido1` AS `ind_apellido1`,`a003_persona`.`ind_apellido2` AS `ind_apellido2`,`a032_persona_licencia`.`pk_num_licencia` AS `pk_num_licencia`,`a032_persona_licencia`.`fk_a006_num_miscelaneo_detalle_tipolic` AS `fk_a006_num_miscelaneo_detalle_tipolic`,`a032_persona_licencia`.`fk_a006_num_miscelaneo_detalle_claselic` AS `fk_a006_num_miscelaneo_detalle_claselic`,`a032_persona_licencia`.`fec_expiracion_licencia` AS `fec_expiracion_licencia` from ((`a003_persona` join `rh_b001_empleado` on((`rh_b001_empleado`.`fk_a003_num_persona` = `a003_persona`.`pk_num_persona`))) join `a032_persona_licencia` on((`a032_persona_licencia`.`fk_a003_num_persona` = `a003_persona`.`pk_num_persona`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_persona_empleado_nacimiento`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_persona_empleado_nacimiento`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_persona_empleado_nacimiento` AS select `a003_persona`.`pk_num_persona` AS `pk_num_persona`,`a003_persona`.`ind_cedula_documento` AS `ind_cedula_documento`,`rh_b001_empleado`.`pk_num_empleado` AS `pk_num_empleado`,`a003_persona`.`fec_nacimiento` AS `fec_nacimiento`,`a003_persona`.`ind_lugar_nacimiento` AS `ind_lugar_nacimiento`,`cn`.`pk_num_ciudad` AS `ciudad_nacimiento`,`mn`.`fk_a011_num_municipio` AS `municipio_nacimiento`,`en`.`pk_num_estado` AS `estado_nacimiento`,`pn`.`pk_num_pais` AS `pais_nacimiento` from (((((`a003_persona` join `rh_b001_empleado` on((`rh_b001_empleado`.`fk_a003_num_persona` = `a003_persona`.`pk_num_persona`))) join `a010_ciudad` `cn` on((`a003_persona`.`fk_a010_num_ciudad` = `cn`.`pk_num_ciudad`))) join `a009_estado` `en` on((`cn`.`fk_a009_num_estado` = `en`.`pk_num_estado`))) join `a008_pais` `pn` on((`en`.`fk_a008_num_pais` = `pn`.`pk_num_pais`))) join `a014_ciudad_municipio` `mn` on((`mn`.`fk_a010_num_ciudad` = `cn`.`pk_num_ciudad`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_persona_empleado_telefono`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_persona_empleado_telefono`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_persona_empleado_telefono` AS select `a007_persona_telefono`.`pk_num_telefono` AS `pk_num_telefono`,`a003_persona`.`pk_num_persona` AS `pk_num_persona`,`a003_persona`.`ind_cedula_documento` AS `ind_cedula_documento`,`rh_b001_empleado`.`pk_num_empleado` AS `pk_num_empleado`,`a007_persona_telefono`.`fk_a006_num_miscelaneo_detalle_tipo_telefono` AS `fk_a006_num_miscelaneo_detalle_tipo_telefono`,`a007_persona_telefono`.`ind_telefono` AS `ind_telefono` from ((`a003_persona` join `rh_b001_empleado` on((`rh_b001_empleado`.`fk_a003_num_persona` = `a003_persona`.`pk_num_persona`))) join `a007_persona_telefono` on((`a007_persona_telefono`.`fk_a003_num_persona` = `a003_persona`.`pk_num_persona`))) */;

-- -----------------------------------------------------
-- View `SIACE`.`vl_rh_usuario_empleado_dependencia`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SIACE`.`vl_rh_usuario_empleado_dependencia`;
USE `SIACE`;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vl_rh_usuario_empleado_dependencia` AS select `a018_seguridad_usuario`.`pk_num_seguridad_usuario` AS `pk_num_seguridad_usuario`,`rh_b001_empleado`.`pk_num_empleado` AS `pk_num_empleado`,`a003_persona`.`pk_num_persona` AS `pk_num_persona`,`rh_c076_empleado_organizacion`.`pk_num_empleado_organizacion` AS `pk_num_empleado_organizacion`,`rh_c076_empleado_organizacion`.`fk_a004_num_dependencia` AS `fk_a004_num_dependencia`,`rh_c076_empleado_organizacion`.`fk_a004_num_dependencia_tmp` AS `fk_a004_num_dependencia_tmp` from (((`a018_seguridad_usuario` join `rh_b001_empleado` on((`rh_b001_empleado`.`pk_num_empleado` = `a018_seguridad_usuario`.`fk_rhb001_num_empleado`))) join `a003_persona` on((`rh_b001_empleado`.`fk_a003_num_persona` = `a003_persona`.`pk_num_persona`))) join `rh_c076_empleado_organizacion` on((`rh_b001_empleado`.`pk_num_empleado` = `rh_c076_empleado_organizacion`.`fk_rhb001_num_empleado`))) */;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
USE `SIACE`;

DELIMITER $$
USE `SIACE`$$
/*!50003 CREATE*/ /*!50017 DEFINER=`dmunoz`@`%`*/ /*!50003 TRIGGER `t_cb_c001_voucher_det_insertar` AFTER INSERT ON `cb_c001_voucher_det`
FOR EACH ROW BEGIN

SET @Sum_debe = (SELECT SUM(num_debe)

				FROM cb_c001_voucher_det 

				WHERE

					ind_mes = NEW.ind_mes AND 

					fec_anio = NEW.fec_anio AND

					fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND 

					fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad AND 

					ind_estatus = 'MA');

					

     SET @Sum_haber = (SELECT SUM(num_haber)

				    FROM cb_c001_voucher_det

				   WHERE

					    ind_mes = NEW.ind_mes AND 

						fec_anio = NEW.fec_anio AND

						fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND 

						fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad AND 

						ind_estatus = 'MA');

					

	SET @SaldoBalance_debe = COALESCE(@Sum_debe, 0);

	SET @SaldoBalance_haber= COALESCE(@Sum_haber, 0);

	

	SET @SaldoBalActual = COALESCE((SELECT num_saldo_balance_actual

							   FROM cb_c004_voucher_balance

							  WHERE

							  	   ind_mes < NEW.ind_mes AND

								   fec_anio < NEW.fec_anio AND

								   fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND

								   fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad

							ORDER BY fec_anio DESC, ind_mes DESC

							    LIMIT 0, 1), 0) + @SaldoBalance_debe - @SaldoBalance_haber;

							    

	SET @mes= NEW.ind_mes;

	SET @Anio= NEW.fec_anio;

	SET @SaldoBalance= @SaldoBalance_debe - @SaldoBalance_haber;



	INSERT INTO cb_c004_voucher_balance

	        SET

			 ind_mes =  NEW.ind_mes,

			 fec_anio = NEW.fec_anio,

			 fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta,

			 fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad,

			 num_saldo_balance = @SaldoBalance,

			 num_saldo_balance_actual = @SaldoBalActual,

			 fk_a018_num_seguridad_usuario = NEW.fk_a018_num_seguridad_usuario,

			 fec_ultima_modificacion = NOW()

	ON DUPLICATE KEY UPDATE

		num_saldo_balance = @SaldoBalance,

		num_saldo_balance_actual = @SaldoBalActual,

		fk_a018_num_seguridad_usuario = NEW.fk_a018_num_seguridad_usuario,

		fec_ultima_modificacion = NOW();



	INSERT INTO cb_c007_balance_cuenta

		   SET

			  fec_anio = NEW.fec_anio,

			  fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta,

              fk_cbb005_num_contabilidades = NEW.fk_cbb005_num_contabilidades, 

			  fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad,

			  fk_a018_num_seguridad_usuario = NEW.fk_a018_num_seguridad_usuario,

			  fec_ultima_modificacion = NOW()

	ON DUPLICATE KEY UPDATE

		fk_a018_num_seguridad_usuario = NEW.fk_a018_num_seguridad_usuario,

		fec_ultima_modificacion = NOW();



	IF (@Mes = '01') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance01 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '02') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance02 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '03') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance03 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '04') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance04 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '05') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance05 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND

		     fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '06') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance06 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '07') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance07 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;


	END IF;



	IF (@Mes = '08') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance08 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '09') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance09 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '10') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance10= @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '11') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance11 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '12') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance12 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;

	END IF;

END */$$

USE `SIACE`$$
/*!50003 CREATE*/ /*!50017 DEFINER=`dmunoz`@`%`*/ /*!50003 TRIGGER `t_cb_c001_voucher_det_actualizar` AFTER UPDATE ON `cb_c001_voucher_det`
FOR EACH ROW BEGIN
SET @Sum_debe = (SELECT SUM(num_debe)
				FROM cb_c001_voucher_det 
				WHERE
					ind_mes = NEW.ind_mes AND 
					fec_anio = NEW.fec_anio AND
					fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND 
					fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad AND 
					ind_estatus = 'MA');

     SET @Sum_haber = (SELECT SUM(num_haber)
				    FROM cb_c001_voucher_det
				   WHERE
					    ind_mes = NEW.ind_mes AND 
					    fec_anio = NEW.fec_anio AND
					    fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND 
				   	    fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad AND 
					    ind_estatus = 'MA');

	SET @SaldoBalance_debe = COALESCE(@Sum_debe, 0);
	SET @SaldoBalance_haber= COALESCE(@Sum_haber, 0);

	SET @SaldoBalActual = COALESCE((SELECT num_saldo_balance_actual
							   FROM cb_c004_voucher_balance
							  WHERE
							  	   ind_mes < NEW.ind_mes AND
								   fec_anio < NEW.fec_anio AND
								   fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND
								   fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad
							ORDER BY fec_anio DESC, ind_mes DESC
							    LIMIT 0, 1), 0) + @SaldoBalance_debe - @SaldoBalance_haber;

	SET @mes= NEW.ind_mes;
	SET @Anio= NEW.fec_anio;
	SET @SaldoBalance= @SaldoBalance_debe - @SaldoBalance_haber;

	INSERT INTO cb_c004_voucher_balance
	        SET
			 ind_mes =  NEW.ind_mes,
			 fec_anio = NEW.fec_anio,
			 fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta,

			 fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad,
			 num_saldo_balance = @SaldoBalance,
			 num_saldo_balance_actual = @SaldoBalActual,
			 fk_a018_num_seguridad_usuario = NEW.fk_a018_num_seguridad_usuario,
			 fec_ultima_modificacion = NOW()
	ON DUPLICATE KEY UPDATE
		num_saldo_balance = @SaldoBalance,
		num_saldo_balance_actual = @SaldoBalActual,
		fk_a018_num_seguridad_usuario = NEW.fk_a018_num_seguridad_usuario,
		fec_ultima_modificacion = NOW();


	INSERT INTO cb_c007_balance_cuenta
		   SET
              fec_anio = NEW.fec_anio,
			  fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta,
			  fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad,
              fk_cbb005_num_contabilidades = NEW.fk_cbb005_num_contabilidades,
			  fk_a018_num_seguridad_usuario = NEW.fk_a018_num_seguridad_usuario,
			  fec_ultima_modificacion = NOW()
	       ON DUPLICATE KEY UPDATE
		      fk_a018_num_seguridad_usuario = NEW.fk_a018_num_seguridad_usuario,
		      fec_ultima_modificacion = NOW();

	IF (@Mes = '01') THEN
		UPDATE cb_c007_balance_cuenta
		SET num_saldo_balance01 = @SaldoBalance
		WHERE
			fec_anio = @Anio AND
			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND
			fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;
	END IF;

	IF (@Mes = '02') THEN
		UPDATE cb_c007_balance_cuenta
		SET num_saldo_balance02 = @SaldoBalance
		WHERE
			fec_anio = @Anio AND
			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND
			fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;
	END IF;

	IF (@Mes = '03') THEN
		UPDATE cb_c007_balance_cuenta
		SET num_saldo_balance03 = @SaldoBalance
		WHERE
			fec_anio = @Anio AND
			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND
			fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;
	END IF;

	IF (@Mes = '04') THEN
		UPDATE cb_c007_balance_cuenta
		SET num_saldo_balance04 = @SaldoBalance
		WHERE
			fec_anio = @Anio AND
			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND
			fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;
	END IF;

	IF (@Mes = '05') THEN
		UPDATE cb_c007_balance_cuenta
		SET num_saldo_balance05 = @SaldoBalance
		WHERE
			fec_anio = @Anio AND
			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND
			fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;
	END IF;

	IF (@Mes = '06') THEN
		UPDATE cb_c007_balance_cuenta
		SET num_saldo_balance06 = @SaldoBalance
		WHERE
			fec_anio = @Anio AND
			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND
			fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;
	END IF;

	IF (@Mes = '07') THEN
		UPDATE cb_c007_balance_cuenta
		SET num_saldo_balance07 = @SaldoBalance
		WHERE
			fec_anio = @Anio AND
			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND
			fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;
	END IF;



	IF (@Mes = '08') THEN
		UPDATE cb_c007_balance_cuenta
		SET num_saldo_balance08 = @SaldoBalance
		WHERE
			fec_anio = @Anio AND
			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND
			fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;
	END IF;



	IF (@Mes = '09') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance09 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND

		     fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '10') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance10= @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND

		     fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '11') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance11 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND

		     fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '12') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance12 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = NEW.fk_cbb004_num_cuenta AND

		     fk_cbb003_num_libro_contabilidad = NEW.fk_cbb003_num_libro_contabilidad;

	END IF;
END */$$

USE `SIACE`$$
/*!50003 CREATE*/ /*!50017 DEFINER=`dmunoz`@`%`*/ /*!50003 TRIGGER `t_cb_c001_voucher_det_eliminar` AFTER DELETE ON `cb_c001_voucher_det`
FOR EACH ROW BEGIN

SET @Sum_debe = (SELECT SUM(num_debe)

				FROM cb_c001_voucher_det 

  		        WHERE

			        ind_mes = OLD.ind_mes AND 

			        fec_anio = OLD.fec_anio AND

			        fk_cbb004_num_cuenta = OLD.fk_cbb004_num_cuenta AND 

		            fk_cbb003_num_libro_contabilidad = OLD.fk_cbb003_num_libro_contabilidad AND 

			        ind_estatus = 'MA');



     SET @Sum_haber = (SELECT SUM(num_haber)

				         FROM cb_c001_voucher_det

				        WHERE

					          ind_mes = OLD.ind_mes AND 

					          fec_anio = OLD.fec_anio AND

					          fk_cbb004_num_cuenta = OLD.fk_cbb004_num_cuenta AND 

				   	          fk_cbb003_num_libro_contabilidad = OLD.fk_cbb003_num_libro_contabilidad AND 

					          ind_estatus = 'MA');



	SET @SaldoBalance_debe = COALESCE(@Sum_debe, 0);

	SET @SaldoBalance_haber= COALESCE(@Sum_haber, 0);



	SET @SaldoBalActual = COALESCE((SELECT num_saldo_balance_actual

							          FROM cb_c004_voucher_balance

							         WHERE

							  	           ind_mes < OLD.ind_mes AND

								           fec_anio < OLD.fec_anio AND

								           fk_cbb004_num_cuenta = OLD.fk_cbb004_num_cuenta AND

								           fk_cbb003_num_libro_contabilidad = OLD.fk_cbb003_num_libro_contabilidad

							      ORDER BY fec_anio DESC, ind_mes DESC

							    LIMIT 0, 1), 0) + @SaldoBalance_debe - @SaldoBalance_haber;



							    

	SET @mes= OLD.ind_mes;

	SET @Anio= OLD.fec_anio;

	SET @SaldoBalance= @SaldoBalance_debe - @SaldoBalance_haber;



	INSERT INTO cb_c004_voucher_balance

	        SET

			  ind_mes =  OLD.ind_mes,

		      fec_anio = OLD.fec_anio,

			  fk_cbb004_num_cuenta = OLD.fk_cbb004_num_cuenta,

			  fk_cbb003_num_libro_contabilidad = OLD.fk_cbb003_num_libro_contabilidad,

			  num_saldo_balance = @SaldoBalance,

			  num_saldo_balance_actual = @SaldoBalActual,

			  fk_a018_num_seguridad_usuario = OLD.fk_a018_num_seguridad_usuario,

			  fec_ultima_modificacion = NOW()

	ON DUPLICATE KEY UPDATE

		      num_saldo_balance = @SaldoBalance,

		      num_saldo_balance_actual = @SaldoBalActual,

		      fk_a018_num_seguridad_usuario = OLD.fk_a018_num_seguridad_usuario,

		      fec_ultima_modificacion = NOW();



	INSERT INTO cb_c007_balance_cuenta

		   SET

			  fec_anio = OLD.fec_anio,

			  fk_cbb004_num_cuenta = OLD.fk_cbb004_num_cuenta,
              
              fk_cbb005_num_contabilidades = OLD.fk_cbb005_num_contabilidades,

  	          fk_cbb003_num_libro_contabilidad = OLD.fk_cbb003_num_libro_contabilidad,

			  fk_a018_num_seguridad_usuario = OLD.fk_a018_num_seguridad_usuario,

			  fec_ultima_modificacion = NOW()

	ON DUPLICATE KEY UPDATE

		fk_a018_num_seguridad_usuario = OLD.fk_a018_num_seguridad_usuario,

		fec_ultima_modificacion = NOW();



	IF (@Mes = '01') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance01 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = OLD.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = OLD.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '02') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance02 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = OLD.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = OLD.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '03') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance03 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = OLD.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = OLD.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '04') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance04 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = OLD.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = OLD.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '05') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance05 = @SaldoBalance


		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = OLD.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = OLD.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '06') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance06 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = OLD.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = OLD.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '07') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance07 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = OLD.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = OLD.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '08') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance08 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = OLD.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = OLD.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '09') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance09 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = OLD.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = OLD.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '10') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance10= @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = OLD.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = OLD.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '11') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance11 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = OLD.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = OLD.fk_cbb003_num_libro_contabilidad;

	END IF;



	IF (@Mes = '12') THEN

		UPDATE cb_c007_balance_cuenta

		SET num_saldo_balance12 = @SaldoBalance

		WHERE

			fec_anio = @Anio AND

			fk_cbb004_num_cuenta = OLD.fk_cbb004_num_cuenta AND

			fk_cbb003_num_libro_contabilidad = OLD.fk_cbb003_num_libro_contabilidad;

	END IF;

END */$$

USE `SIACE`$$
/*!50003 CREATE*/ /*!50017 DEFINER=`dmunoz`@`%`*/ /*!50003 TRIGGER `t_pr_d008_estado_distribucion_insertar` AFTER INSERT ON `pr_d008_estado_distribucion`
FOR EACH ROW BEGIN
                  SET  @id = NEW.fk_prc002_num_presupuesto_det;
                  SET  @estado=NEW.ind_tipo_distribucion;
   
	SET @Monto = (SELECT  SUM(num_monto) FROM pr_d008_estado_distribucion 
                                 WHERE ind_tipo_distribucion= @estado  and  ind_estado ='AC' and  fk_prc002_num_presupuesto_det= @id);
                   
          IF (@estado='CO') THEN
	UPDATE  pr_c002_presupuesto_det 
	SET num_monto_compromiso = @Monto
	WHERE pk_num_presupuesto_det= @id;
        END IF;

        IF (@estado='CA') THEN
                    UPDATE  pr_c002_presupuesto_det 
	SET num_monto_causado = @Monto
	WHERE pk_num_presupuesto_det= @id;       
       END IF;
     
             IF (@estado='PA') THEN
                    UPDATE  pr_c002_presupuesto_det 
	SET num_monto_pagado = @Monto
	WHERE pk_num_presupuesto_det= @id;       
       END IF;
		
END */$$

USE `SIACE`$$
/*!50003 CREATE*/ /*!50017 DEFINER=`dmunoz`@`%`*/ /*!50003 TRIGGER `t_pr_d008_estado_distribucion_actualizar` AFTER UPDATE ON `pr_d008_estado_distribucion`
FOR EACH ROW BEGIN
                  SET  @id = NEW.fk_prc002_num_presupuesto_det;
                  SET  @estado=NEW.ind_tipo_distribucion;
   
	SET @Monto = (SELECT  SUM(num_monto) FROM pr_d008_estado_distribucion 
                                 WHERE ind_tipo_distribucion= @estado  and  ind_estado ='AC' and  fk_prc002_num_presupuesto_det= @id);
                   
          IF (@estado='CO') THEN
	UPDATE  pr_c002_presupuesto_det 
	SET num_monto_compromiso = @Monto
	WHERE pk_num_presupuesto_det= @id;
        END IF;

        IF (@estado='CA') THEN
                    UPDATE  pr_c002_presupuesto_det 
	SET num_monto_causado = @Monto
	WHERE pk_num_presupuesto_det= @id;       
       END IF;
     
             IF (@estado='PA') THEN
                    UPDATE  pr_c002_presupuesto_det 
	SET num_monto_pagado = @Monto
	WHERE pk_num_presupuesto_det= @id;       
       END IF;
		
END */$$

USE `SIACE`$$
/*!50003 CREATE*/ /*!50017 DEFINER=`dmunoz`@`%`*/ /*!50003 TRIGGER `t_st_d001_solicitud_insertar` AFTER INSERT ON `st_d001_solicitud`
FOR EACH ROW INSERT INTO st_d002_registro
SET st_d002_registro.fec_creado=now() */$$

USE `SIACE`$$
/*!50003 CREATE*/ /*!50017 DEFINER=`dmunoz`@`%`*/ /*!50003 TRIGGER `t_st_d001_solicitud_actualizar` BEFORE UPDATE ON `st_d001_solicitud`
FOR EACH ROW BEGIN
         IF (NEW.num_estatus=0) THEN SET NEW.ind_estatus ='Preparado';
ELSEIF (NEW.num_estatus=1) THEN SET NEW.ind_estatus = 'Aprobado';
            ELSEIF (NEW.num_estatus=2) THEN SET NEW.ind_estatus = 'Revisado';
            ELSEIF (NEW.num_estatus=3) THEN SET NEW.ind_estatus = 'Asignado';
            ELSEIF (NEW.num_estatus=4) THEN SET  NEW.ind_estatus = 'Ejecutado';
            ELSEIF (NEW.num_estatus=5) THEN SET NEW.ind_estatus = 'Completado';
            ELSEIF (NEW.num_estatus=6) THEN SET NEW.ind_estatus = 'Evaluado';
           ELSEIF (NEW.num_estatus=7) THEN SET NEW.ind_estatus = 'Cerrado';
            END IF;
     END */$$


DELIMITER ;
