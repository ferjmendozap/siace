<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        08-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
#Define la Url Base del Sistema
define('BASE_URL', 'http://localhost/siace/');
#Define el Controlador por defecto
define('CONTROLADOR_DEFAULT', 'index');
#Define el Metodo por defecto
define('METODO_DEFAULT', 'index');
#Archivos de Configuracion de la Vista
#Define la vista principal por defecto
define('PLANTILLA_WEB_PREDEFINIDA', 'default');
#Define el Plantilla Web Por defecto
define('RUTA_PLANTILLA_WEB_PREDEFINIDA', 'TemaBasico');
#Define si el login se debe hacer mediante el siace ó a través del Ldap.
define('lOGINLDAP',false);# siace(false), Ldap(true).

#id de Concepto de Prestaciones Sociales
define('GPA',37);#Prestaciones Sociales Dias Adicionales
define('GPS',36);#Prestaciones Sociales
define('RPA',39);#Retroactivo Prestaciones Sociales Dias Adicionales
define('RPS',38);#Retroactivo Prestaciones Sociales
define('NOJ',4);#NOMINA JUBILADOS
define('NOP',5);#NOMINA PENSIONADOS

#Nombre Del Sistema
define('APP_NOMBRE', 'SIACE');
#Descripcion del Sistema
define('APP_DESCRIPCION', 'SISTEMA INTEGRAL ADMINISTRATIVO DE LA CONTRALORIA DEL ESTADO');
#Nombre De la Contraloria
define('APP_ORGANISMO', 'CONTRALORÍA DEL ESTADO SUCRE');
#rif De la Contraloria
define('APP_ORGANISMO_RIF', 'G-20001224-2');
#Direccion de la contraloria
define('APP_ORGANISMO_DIRECCION', 'Avenida Arismendi, Edificio Palacio Legislativo, Cumaná, estado Sucre');
#Codigo Unico Generado por MD5.
define('LLAVE_APLICACION', '4f6a6d832be79');
#Tiempo en el que la sesion va a permanecer inactiva antes de ser cerrada
define('SESSION_TIEMPO', 90960);

#Base de Datos

#Direccion del Servidor
define ('DB_HOST', 'localhost');
#Usuario de la Base de Datos
define ('DB_USER', 'root');
#Contraseña de la base de datos
define ('DB_PASS', 'm3nd0z4p');
#Nombre se la Base de Datos
define ('DB_NAME', 'siace0403');#cotejamiento de la base de datos
define ('DB_CHAR', 'utf8');

#Direccion del Servidor
define ('DB_HOST2', '');
#Usuario de la Base de Datos
define ('DB_USER2', '');
#Contraseña de la base de datos
define ('DB_PASS2', '');
#Nombre se la Base de Datos
define ('DB_NAME2', '');
#cotejamiento de la base de datos
define ('DB_CHAR2', 'utf8');

#Datos del Servidor LDAP
#dirección IP del servidor LDAP donde se realiza la consulta
define ('LDAP_HOST', 'nombreServidorLdap');
#puerto de red del servidor LDAP
define ('LDAP_PORT', 389);
#nombre distinguido a usar como base de búsqueda
define ('LDAP_BASE_DN', 'ou=Usuarios, dc=cmldc, dc=gob, dc=ve');

