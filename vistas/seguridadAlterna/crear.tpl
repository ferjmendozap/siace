<form action="{$_Parametros.url}seguridadAlterna/crear" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idUsuario}" name="idUsuario"/>
        <div class="row">
            <div class="col-md-12">
                <h3 class="pull-right">Usuario: {$usuario[0].ind_nombre1} {$usuario[0].ind_apellido1}</h3>
            </div>
            <div class="col-md-12">
                <div class="card tabs-left style-default-light">
                    <ul class="card-head nav nav-tabs" data-toggle="tabs">
                        {assign var="n" value="1"}
                        {foreach item=i from=$aplicaciones}
                            <li {if $n == 1}class="active"{/if}><a href="#aplicacion{$i.pk_num_seguridad_aplicacion}">{$n++} {$i.ind_nombre_modulo}</a></li>
                            <input type="hidden" name="form[int][pk_num_seguridad_aplicacion][]" value="{$i.pk_num_seguridad_aplicacion}">
                        {/foreach}
                    </ul>
                    <div class="card-body tab-content style-default-bright">
                        {assign var="n2" value="1"}
                        {foreach item=i from=$aplicaciones}
                            <div class="tab-pane {if $n2 == 1}active {$n2++}{/if}" id="aplicacion{$i.pk_num_seguridad_aplicacion}">
                                <h3>Modulo de {$i.ind_nombre_modulo}</h3>
                                <h4>Dependencias:</h4><hr>
                                {assign var="n3" value="0"}
                                {foreach item=ii from=$dependencias}
                                    {if $n3 == 0 OR $n3%2 == 0}<div class="col-md-12">{/if}
                                    <div class="col-md-6 {$n3++}">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" name="form[int][{$i.pk_num_seguridad_aplicacion}][pk_num_dependencia][]" value="{$ii.pk_num_dependencia}" {if isset($listado[$i.pk_num_seguridad_aplicacion])}{if isset($listado[$i.pk_num_seguridad_aplicacion][$ii.pk_num_dependencia])} checked {/if}{/if}>
                                                <span>{$ii.ind_dependencia} </span>
                                            </label>
                                        </div>
                                    </div>
                                    {if $n3%2 == 0}</div><div class="row"><br><br><hr></div>{/if}
                                    {if (count($dependencias) == $n3) AND ($n3%2 != 0)}</div>{/if}
                                {/foreach}
                                <hr>
                            </div>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    {if $ver==0}
        <div class="modal-footer">
            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
            <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="fa fa-exchange"></i>&nbsp;Asignar</button>
        </div>
    {/if}
</form>

<script type="text/javascript">
    $(document).ready(function() {
        var app = new  AppFunciones();
        $("#formAjax").submit(function(){
            return false;
        });
        $('#modalAncho').css("width","90%");
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='nuevo'){
                    app.metActualizarRegistroTablaJson('dataTablaJson','la seguridad fue agregada satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });
    });
</script>