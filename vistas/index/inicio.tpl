
<link type="text/css" rel="stylesheet" href="{$_Parametros.ruta_Css}Intranet.css" />

<style>
	@font-face {
		font-family: RifficFree-Bold;
		src: url('{$_Parametros.ruta_Img}modIN/RifficFree-Bold.ttf') format('truetype');
	}
	.fuente_cumpleanieros{
		font-family: RifficFree-Bold !important;
		font-weight: bolder;
		font-size: 19px;
		color: #A1162D;
		text-shadow: 2px 4px 3px rgba(0,0,0,0.3);


	}

	@font-face {
		font-family: Snake_in_the_Boot;
		src: url('{$_Parametros.ruta_Img}modIN/Snake_in_the_Boot.ttf') format('truetype');
	}

	.fuente_hoy{
		font-family: Snake_in_the_Boot;
		font-size: 11px;
		color: #232A32;
		text-shadow: 2px 4px 3px rgba(0,0,0,0.3);
	}

	.style-default-light-ces{
		background-color:  #e6e3e1; !important; /*e6e3e1*/
		background-image: url("{$_Parametros.ruta_Img}modIN/back-pattern.png");
		/* This is mostly intended for prototyping; please download the pattern and re-host for production environments. Thank you! */
	}

	.style-card-cumple{
		background-color: #2196f3 !important;
		color: white;
		border-radius: 0% !important;
	}

	.style-class-sombra-ces{
		box-shadow: 0px 0px 10px  #1a2229;
		-webkit-box-shadow: 0px 0px 10px #1a2229;
		-moz-box-shadow: 0px 0px 10px #1a2229;
	}

</style>

<section class="style-default-light-ces"><!--SECCION-->


<div class="section-body contain-lg"><!--CONTENIDO-->

	<div class="row  ">
		<div class="col-lg-12 ">
			<div class="card card-tiles style-default-light style-class-sombra-ces">
				<div class="row style-card-cumple ">
					<div class="col-sm-9">
						<div class="card-body style-default-dark" style="background: url( {$_Parametros.ruta_Img}modIN/wallpaper_2_1.jpg);">
							<img style="padding: 2px" class="img-responsive" src="{$_Parametros.ruta_Img}modIN/wallpaper_2_1b.png" alt="banner1" />
							{*<h2>SIACE</h2>
							<div class="text-default-light">Sistema Integral Adminstrativo de Contralorias de Estados<a href="#"></a></div>*}
						</div>
					</div>
					<div class="col-sm-3 style-default-light" >
						<div class="card-body style-card-cumple">
							<div style="position: relative" class="pasadorCumpleanieros">
								<!-- CUMPLEAÑOS -->
								{if $swCumple == 2 }{*mas de un cumpleañero*}
									<h3 class="text-light"><strong>{$diaCumple}</strong> {$nombre_mes}</h3>
									<a class="btn_lista_cumple_mes" id="btn_lista_cumple_mes"  titulo="Cumpleañeros de {$nombre_mes_titulo}" data-target="#formModal" data-toggle="modal" style="cursor: pointer;">Cumpleañeros de Hoy <i class="fa md-cake"></i></a>
									<div class="contenedorPasadorCumple"  >
										<div id="slider" style="position: relative">
											{foreach name=cumpleanieros  item=indice from=$listadoCumpleHoy}
												<div style="position: absolute; left: 0; top: 0;">
													<img class="img-circle posicion_avatar_cumple " width="50" height="50" src="{$_Parametros.ruta_Img}modRH/fotos/{$indice.ind_foto}" alt="" />
													<p class="text-sm " style="color: #ffffff !important;">{$indice.ind_nombre1} {$indice.ind_apellido1}</p>
												</div>
											{/foreach}
										</div>
									</div>
								{elseif $swCumple == 1 }{*un cumpleañero*}
									<h3 class="text-light"><strong>{$diaCumple}</strong> {$nombre_mes}</h3>
									<a class="btn_lista_cumple_mes" id="btn_lista_cumple_mes"  titulo="Cumpleañeros de {$nombre_mes_titulo}" data-target="#formModal" data-toggle="modal" style="cursor: pointer;" >Cumpleañero de Hoy <i class="fa md-cake"></i></a>
									<div class="contenedorPasadorCumple"  >
										<div style="position: relative">
											<div style="position: absolute; left: 0; top: 0;">
												<img class="img-circle posicion_avatar_cumple " width="50" height="50" src="{$_Parametros.ruta_Img}modRH/fotos/{$listadoCumpleHoy[0]['ind_foto']}" alt="" />
												<p class="text-sm " style="color: #ffffff !important; text-align: center">{$listadoCumpleHoy[0]['ind_nombre1']} {$listadoCumpleHoy[0]['ind_apellido1']}</p>
											</div>
										</div>
									</div>
								{elseif $swCumple == 3 } {*no hay cumpleañeros para hoy*}
									<h3 class="text-light"><strong>{$diaCumple}</strong> {$nombre_mes}</h3>
									<a class="btn_lista_cumple_mes" id="btn_lista_cumple_mes"  titulo="Cumpleañeros de {$nombre_mes_titulo}" data-target="#formModal" data-toggle="modal" style="cursor: pointer;" >Cumpleañeros del Mes <i class="fa md-cake"></i></a>
								{else}
									<h3 class="text-light"><strong>{$diaCumple}</strong>  {$nombre_mes}</h3>
									<a >No hay Cumpleañeros del Mes</a>
								{/if }
								<!-- -->
							</div>
							<div class="cumpleanierosSinPasador" >
								{if $swCumple == 0 }
								<h3 class="text-light"><strong>{$diaCumple}</strong> {$nombre_mes}</h3> <a href="#">No hay cumpleañeros</a>
								{else}
									<h3 class="text-light"><strong>{$diaCumple}</strong> {$nombre_mes}</h3> <a class="btn_lista_cumple_mes2" id="btn_lista_cumple_mes2"  titulo="Cumpleañeros de {$nombre_mes_titulo}" data-target="#formModal" data-toggle="modal" style="cursor: pointer;" >Cumpleañeros del Mes <i class="fa md-cake"></i></a>
								{/if}
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-9" >
						<article class="style-default-bright">
							<div >
								<div class="row bs-docs-carrusel" style="padding: 0px !important; margin: 0px !important;"><!--FILA 1 COLUMNA 8-->
									<div id="myCarousel" class="carousel slide" style="padding: 0px !important; margin-bottom: 8px !important;"><!--CARRUCEL-->

										<ol class="carousel-indicators">
											{foreach name=indicadores  item=indice from=$arrayNoticias}
												{if $smarty.foreach.indicadores.first }<!--Indica si es la primera iteracion-->
													<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
												{else}
													<li data-target="#myCarousel" data-slide-to="{($smarty.foreach.indicadores.iteration) - 1}"></li><!-- Iiteration ndica la iteracion actual -->
												{/if}
											{/foreach}
										</ol>
										<!-- Carousel items -->

										<div class="carousel-inner">

											{if $arrayNoticias != 0 }

												{foreach name=itemCarrusel item=indice from=$arrayNoticias}
													{if $smarty.foreach.itemCarrusel.first }
														<div class="active item style-default-dark" align="center" >

															<img class="img-responsive" style="cursor:pointer; height: 434px !important;"  src="{$_Parametros.ruta_Img}modIN/noticias/{$indice.ind_ruta_img}" alt="banner{$smarty.foreach.itemCarrusel.iteration}" />
															<a class="boton_caption" ind_titulo="{$indice.ind_titulo}"  pk_num_noticia="{$indice.pk_num_noticia}"  style="cursor:pointer"  data-target="#formModal" data-toggle="modal" >

																<div class="carousel-caption"  data-toggle="tooltip" title="Ver noticia"  >
																	<h4>{$indice.ind_titulo}</h4>
																	<p>{$indice.ind_descripcion}</p>
																</div>
															</a>

														</div>
													{else}
														<div class="item style-default-dark" align="center"  >
															<img style="height: 434px !important;" class="img-responsive" src="{$_Parametros.ruta_Img}modIN/noticias/{$indice.ind_ruta_img}" alt="banner{$smarty.foreach.itemCarrusel.iteration}" />
															<a class="boton_caption" ind_titulo="{$indice.ind_titulo}" pk_num_noticia="{$indice.pk_num_noticia}" style="cursor:pointer"  data-target="#formModal" data-toggle="modal" >
																<div  class="carousel-caption"  data-toggle="tooltip" title="Ver noticia"     >
																	<h4>{$indice.ind_titulo}</h4>
																	<p>{$indice.ind_descripcion}</p>
																</div>
															</a>
														</div>
													{/if}
												{/foreach}

											{else}
												<div class="active item style-default-dark" align="center" >

													<img class="img-responsive" src="{$_Parametros.ruta_Img}modIN/plantilla_img_noticia_SIACE.png" alt="banner1" />
													<a >
														<div class="carousel-caption"  data-toggle="tooltip"  >
															<h4>NO HAY NOTICIAS PUBLICADAS</h4>
															<p>Publique una noticia en el menú Intranet</p>
														</div>
													</a>

												</div>
											{/if}

										</div>

										<!-- Carousel nav -->
										<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
										<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
									</div><!--FIN CARRUCEL-->
								</div><!--FIN FILA 1 COLUMNA 8-->
							</div>
							<div class="card-body" style="margin: 0px !important; padding: 0px !important;">
								<!-- CALENSARIO -->
									 <!--FILA 2 COLUMNA 8-->
										<div class="col-lg-4" align="center">
											<div class="card style-default-bright " style="margin-left:8px; padding: 0px;">
												<div class="card-body" style="margin:0px; padding:0px;" >
													<div id="calendar" style="margin:0px; padding:0px; ">
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-8" style="padding-right: 8px; ">
											<div class="card card-bordered style-primary" >
												<div class="card-head"  >
													<header>Eventos</header>
													<div class="tools">
														<div class="btn-group">
															<a id="imprimeEvento" class="btn btn-icon-toggle btn-refresh">
																<i class="fa fa-print"></i>
															</a>
														</div>
													</div>
												</div>
												<div class="style-default-bright">
												<div class="card-body height-5 scroll style-default-bright" style="margin:10px; padding:0px;"  aling="center">
													<div id="myPrintAreaEvento">
														<div id="eventlist" style="height: 100% !important;">Haga clic en algun evento para mostrar</div>
													</div>
												</div>
												</div>
											</div>
										</div>

								<!-- -->
							</div><!--end .card-body -->
						</article>
					</div>
					<div class="col-md-3">
						<!-- TWITTER -->
						<div class="card-body" style="margin: 1px !important; padding:0px !important; ">
							<!--CARD-->
								<div class="card-body" style=" padding: 0px !important; margin: 0px !important;" >{* Para colocar otra cuenta cambiar el data-widget-id por el nuestro *}
									{literal}
										<a class="twitter-timeline" height="435" lang="ES" href="" data-widget-id="350318262493511680">Tweets por el @CESucre.</a>
										<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
									{/literal}
								</div>
							<!--FIN CARD-->
						</div><!--end .card-body -->
						<!-- -->
						<!-- PANEL DE OPCIONES -->
						<div class="card  card-bordered style-gray-dark" style="margin-right: 10px !important;"><!--CARD-->
							<div class="card-body">
								<div class="row">
									<div class="col-lg-3" align="center" >
										<button data-keyboard="false" data-backdrop="static" data-target="#formModal" data-toggle="modal" class="btn ink-reaction btn-icon-toggle btn-primary btn-lg active btn_constancia_trabajo" type="button" title="Constancia de trabajo" data-placement="top" >
											<i class="fa md-description" ></i>
										</button>
										<small class="text-xs"><a data-keyboard="false" data-backdrop="static" data-target="#formModal" data-toggle="modal" style="cursor: pointer;" class="btn_constancia_trabajo" >Constancia</a></small>
									</div>
									<div class="col-lg-3" align="center" >
										<button data-keyboard="false" data-backdrop="static" data-target="#formModal" data-toggle="modal" class="btn ink-reaction btn-icon-toggle btn-primary btn-lg active btn_certificados" type="button" title="Certificados" data-placement="top" >
											<i class="fa md-wallet-membership" ></i>
										</button>
										<small class="text-xs"><a data-keyboard="false" data-backdrop="static" data-target="#formModal" data-toggle="modal" style="cursor: pointer;" class="btn_certificados" >Certificados</a></small>
									</div>
									<div class="col-lg-3" align="center" >
										<button data-keyboard="false" data-backdrop="static" data-target="#formModal" data-toggle="modal" class="btn ink-reaction btn-icon-toggle btn-primary btn-lg active btn_doc_datos" type="button" title="Documentos/Datos internos" data-placement="top" >
											<i class="md md-assignment " ></i>
										</button>
										<small class="text-xs"><a data-keyboard="false" data-backdrop="static" data-target="#formModal" data-toggle="modal" style="cursor: pointer;" class="btn_doc_datos" >Doc/Datos</a></small>
									</div>
									<div class="col-lg-3" align="center" >
										<button data-keyboard="false" data-backdrop="static" data-target="#formModal" data-toggle="modal" class="btn ink-reaction btn-icon-toggle btn-primary btn-lg active btn_normativa" type="button" title="Normativa legal" data-placement="top" >
											<i class="md md-subject" ></i>
										</button>
										<small class="text-xs"><a data-keyboard="false" data-backdrop="static" data-target="#formModal" data-toggle="modal" style="cursor: pointer;" class="btn_normativa" >Normativa</a></small>
									</div>
								</div>
								<div class="row">&nbsp;</div>
								<div class="row">
									<div class="col-lg-3" align="center" >
										&nbsp;
									</div>
								</div>
								<div class="row">
									<div class="col-lg-3" align="center" >
										&nbsp;
									</div>
								</div>
								<div class="row">
									<div class="col-lg-3" align="center" >
										&nbsp;
									</div>
								</div>
								<div class="row" style="height: 46px;">
									&nbsp;
								</div>

							</div>
						</div>
						<!-- -->
					</div>
				</div>
		</div>
	</div>

</div><!--FIN section-body-->

</section><!--FIN SECCION-->
<script>

//**************************************************************************
//*    CARRUCEL DE IMAGENES
//**************************************************************************	

	$('.carousel').carousel({
			interval: 5000
	});
	
	 $('.boton_caption'). on('click', function () {
	 	 
		 var pk_num_noticia = $(this).attr('pk_num_noticia');
		 var ind_titulo = $(this).attr('ind_titulo');
	     
		 var url = '{$_Parametros.url}index/BuscarNoticia';
         
		 $('#modalAncho').css("width", "60%");
         $('#formModalLabel').html("");
         $('#ContenidoModal').html("");
         $.post(url, { pk_num_noticia: pk_num_noticia }, function(dato){
            $('#formModalLabel').html('<h2 class="text-primary">&nbsp;'+ind_titulo+'</h2>');
			$('#ContenidoModal').html(dato);
			
			
        });	
		
	 
	 });

//**************************************************************************
//*    CALENDARIO DE EVENTOS
//**************************************************************************	
var events = new Array({$arrayEventos});
var DIA_EVENTO = '';
var MES_EVENTO = '';
var ANIO_EVENTO = '';
var DIA_NUM = '';
var SEMANA_NUM = '';

$(document).ready(function() {

changedate('return');
showevents(DIA_EVENTO,MES_EVENTO,ANIO_EVENTO,DIA_NUM,SEMANA_NUM);

});

//**************************************
//var events = new Array({$arrayEventos});

//***********************************************
//*   FUNCION IMPRIMIR
//************************************************
 $("#imprimeEvento").click(function () {
            $("#myPrintAreaEvento").printThis();
 })

//**************************************************
//* VISOR DE CUMPLEAÑEROS
//*************************************************

$("#btn_lista_cumple_mes").click(function () {

	$('#modalAncho').css("width", "60%");
    var titulo_listado_cumple = $(this).attr('titulo');
	$('#formModalLabel').html(titulo_listado_cumple);
	var urlListado = '{$_Parametros.url}index/ListadoCumpleanios';
	$.post(urlListado, { titulo_listado_cumple : titulo_listado_cumple }, function(dato){
		$('#ContenidoModal').html(dato);
	});

});

$("#btn_lista_cumple_mes2").click(function () {

	$('#modalAncho').css("width", "60%");
	$('#formModalLabel').html('');
	var titulo_listado_cumple = $(this).attr('titulo');
	var urlListado = '{$_Parametros.url}index/ListadoCumpleanios';
	$.post(urlListado, { titulo_listado_cumple : titulo_listado_cumple }, function(dato){
		$('#ContenidoModal').html(dato);
	});

});
//-----------------------------------
$(function(){
	$('#slider div:gt(0)').hide();
	setInterval(function(){
		$('#slider div:first-child').fadeOut(1000)
				.next('div').fadeIn(1000)
				.end().appendTo('#slider');}, 4000);
});
//-----------------------------------

//*******************************************************
//*  PANEL DE OPCIONES - CONTSNACIA DE TRABAJO
//*******************************************************
$(document).on( 'click', '.btn_constancia_trabajo', function(){

	$('#modalAncho').css("width", "80%");
	$('#formModalLabel').html('');
	$('#formModalLabel').html('Descarga de la constancia de trabajo');
	$('#ContenidoModal').html('');
	var urlConstancia = '{$_Parametros.url}index/DescargarConstanciaTrabajo';
	$.post(urlConstancia, '', function(dato){
		$('#ContenidoModal').html(dato);
	});
});

//*******************************************************
//*  PANEL DE OPCIONES - CERTIFICADOS
//*******************************************************
$(document).on('click', '.btn_certificados', function(){

	$('#modalAncho').css("width", "80%");
	$('#formModalLabel').html('');
	$('#formModalLabel').html('Descarga de certificados de eventos');
	$('#ContenidoModal').html('');
	var urlRecibos = '{$_Parametros.url}index/DescargarCertificados';
	$.post(urlRecibos, '', function(dato){
		$('#ContenidoModal').html(dato);
	});

});
//*******************************************************
//*  PANEL DE OPCIONES - NORMATIVA LEGAL
//*******************************************************
$(document).on('click', '.btn_normativa', function(){

	$('#modalAncho').css("width", "80%");
	$('#formModalLabel').html('Listar normativa');
	$('#ContenidoModal').html('');
	var urlRecibos = '{$_Parametros.url}modIN/normativaCONTROL/ListarNormativaMET/';
	$.post(urlRecibos, { origenFormNorm: 'I'} , function(dato){
		$('#ContenidoModal').html(dato);
	});

});

//*******************************************************
//*  PANEL DE OPCIONES - DOCUMENTOS INTERNOS
//*******************************************************
$(document).on('click', '.btn_doc_datos', function(){

	$('#modalAncho').css("width", "90%");
	$('#formModalLabel').html('Listar documentos y datos internos');
	$('#ContenidoModal').html('');
	var urlRecibos = '{$_Parametros.url}modIN/documentosCONTROL/';
	$.post(urlRecibos, { origenFormDoc: 'I'}, function(dato){
		$('#ContenidoModal').html(dato);
	});

});

</script>




















