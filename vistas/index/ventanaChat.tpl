<style>
    .modalChatPub{
        margin-left:0 ;
        margin-right: 0;

    }
</style>
<div class="modalChatPub" >
<div id="offcanvas-chat" class="style-default-light width-10">
    <div class="offcanvas-head style-default-bright" style="width: 100% !important;">
        <form class="form">
            <div class="form-group floating-label">
                <textarea name="sidebarChatMessage" id="sidebarChatMessage" class="form-control autosize" rows="1"></textarea>
                <label for="sidebarChatMessage">Escribir mensaje...</label>
            </div>
        </form>
    </div>
    <div id="lista_chat" class="offcanvas-body" style="width: 100% !important; height: 400px; overflow-y: auto">

    </div>
</div>
<input type="hidden" id="msg_oculto" value="" />

</div>

<script>
$(document).ready(function() {

    //*********************************************
    //* ACTUALIZACION AUTOMATICA
    //**********************************************

    function actualizarChat() {

        $.ajax({
            url: '{$_Parametros.url}index/ListaChatPublico',
            cache: false,
            success: function (dato) {
                $('#lista_chat').html('');
                $('#lista_chat').html(dato);
            }
        });
    }

    tiempo = setInterval (actualizarChat, 500);//iniciamos la actualizacion del chat
    //***********************************************
    //* FUNCION AL PRESEIONAR ENTER CHAT
    //***********************************************
    $("#sidebarChatMessage").keyup(function (e) { //keyup para no tener conflicto con el keypres del demo.js
        if (e.which == 13) {
            var mensaje = $('#msg_oculto').val();

            tiempo = clearInterval(tiempo);//SE PARA LA ACTUALIZACIÓN DEL CHAT
            var url_chat = '{$_Parametros.url}index/InsertarMsgChatPublico';
            $.post(url_chat, { mensaje: mensaje}, function (dato) {  //SE INSERTA EL MENSAJE

                $('#lista_chat').html(dato);
                tiempo = setInterval(actualizarChat, 5000); // AL SER EXITOSA LA INSERCIÓN SE REAUNADA LA ACTUALIZACIÓN


            });

        }
    });


});
</script>