<section class="style-default-light">&nbsp;
    <div id="myPrintArea">
        <div class="row" align="center">

            <div class="col-lg-12" >
                <img class="img-responsive "  src="{$_Parametros.ruta_Img}modIN/noticias/{$datoNoticias.ind_ruta_img}" alt="Imagen"  />
            </div>

        </div>
        <div class="row" >

            <div class="col-lg-12" style="text-align: justify" >
                <div class="card card-underline">
                    <div class="card-head ">
                        <header>
                            <h6 class="text-bold">FUENTE: {if $datoNoticias.fk_a001_num_organismo == 1  } {$datoNoticias.ind_descripcion_empresa} {else} CGR {/if} <i class="md-keyboard-control"></i> {$datoNoticias.fec_registro}</h6>
                        </header>
                        <div class="tools">
                            <div class="btn-group">
                                <a id="imprime" class="btn btn-icon-toggle btn-refresh">
                                    <i class="fa fa-print"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body style-default-bright">
                        {$datoNoticias.txt_cuerpo}
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        $("#imprime").click(function () {
            $("#myPrintArea").printThis();
        })
    });
</script>