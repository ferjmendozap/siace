
<section class="style-default-light">&nbsp;
    <div >

        <div class="row" >

            <div class="col-lg-12" style="text-align: justify" >
                <div class="card card-underline">
                    <div class="card-body style-default-bright">
							<div class="row">
                                <div class="col-lg-12" >
                                    <table class="table no-margin table-hover">
                                        <thead>
                                        <tr>
                                            <th>&nbsp;</th>
                                            <th><i class=""></i></th>
                                            <th>Nombre</th>
                                            <th>Apellido</th>
                                            <th>N° Documento</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="info">
                                            <th>&nbsp;</th>
                                            <td ><img class="img-circle width-1" src="{$_Parametros.ruta_Img}modRH/fotos/{$datosUsuario['ind_foto']}" alt="" data-backdrop="static" ></td>
                                            <td>{$datosUsuario['ind_nombre1']}</td>
                                            <td>{$datosUsuario['ind_apellido1']}</td>
                                            <td>{$datosUsuario['ind_cedula_documento']}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row" >
                                &nbsp;
                            </div>
                            <div class="card card-outlined style-gray-light">
                                <div class="card-head" >

                                    <header><h3>Certificados Personales</h3></header>
                                </div>
                                <div class="card-body style-default-bright">
                                    <div class="row" >
                                    <div class="col-lg-12" >
                                        {if $swCertificado == 0 }
                                            <table id="datatable1" class="table table-striped table-hover" WIDTH="90%" >
                                                <thead>
                                                <tr  align="center">
                                                    <th ><i class=""></i>N°</th>
                                                    <th ><i class=""></i>N° Certificado</th>{* md-insert-photo*}
                                                    <th ><i class=""></i>N° Evento</th>
                                                    <th ><i class=""></i>Fecha</th>
                                                    <th ><i class=""></i>Descripción Evento</th>
                                                    <th ><i class=""></i>Horas</th>
                                                    <th ><i class=""></i>Rol</th>
                                                    <th align="center" ><i class=""></i>Certificado</th>
                                                </tr>
                                                </thead>
                                                <tbody >
                                                {foreach name=for_certificados item=indice from=$datosCertificados}
                                                    <tr >
                                                        <td>{$smarty.foreach.for_certificados.iteration}</td>
                                                        <td >{$indice.num_certificado}</td>
                                                        <td>{$indice.pk_num_evento}</td>
                                                        <td>{$indice.fec_inicio}</td>
                                                        <td>{$indice.ind_descripcion}</td>
                                                        <td>{$indice.fec_horas_total}</td>
                                                        {if $indice.fk_a006_num_persona_capacitacion == $codPonente}
                                                        <td>PONENTE</td>
                                                        {else}
                                                        <td>PARTICIPANTE</td>
                                                        {/if}
                                                        <td align="center"><button id=""  type="button" num_certificado="{$indice.num_certificado}"  pk_num_evento = "{$indice.pk_num_evento}" pk_num_persona = "{$indice.fk_a003_num_persona}"  class=" gsUsuario btn ink-reaction btn-raised btn-xs btn-primary btn_descargar_cert" data-target="#formModal2" data-toggle="modal" ><i class="md-file-download"></i></button></td>
                                                    </tr>
                                                {/foreach}
                                                </tbody>
                                            </table>
                                        {else}
                                            <p class="lead"><i class="fa fa-quote-left"></i>No tiene certificados registrados...<i class="fa fa-quote-right"></i></p>
                                        {/if}
                                    </div>
                                </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<script type="text/javascript">
$('#datatable1').on('click','.btn_descargar_cert',function() {

        $('#modalAncho2').css("width", "85%");
        $('#formModalLabel2').html(" ");
        $('#ContenidoModal2').html(" ");
        var pk_num_persona = $(this).attr('pk_num_persona');
        var pk_num_evento = $(this).attr('pk_num_evento');
        var num_certificado = $(this).attr('num_certificado');

        var urlReporte = '{$_Parametros.url}index/GenerarCertificado/?pk_num_persona=' + pk_num_persona+'&pk_num_evento='+pk_num_evento+'&num_certificado='+num_certificado;
        $('#ContenidoModal2').html('<iframe src="' + urlReporte + '" width="100%" height="950"></iframe>');

});
</script>