<form action="{$_Parametros.url}pais/crear" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idPais}" name="idPais"/>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group floating-label" id="ind_paisError">
                    <input type="text" class="form-control" value="{if isset($formDB.ind_pais)}{$formDB.ind_pais}{/if}" name="form[alphaNum][ind_pais]" id="ind_pais">
                    <label for="ind_pais"><i class="md md-map"></i> Pais</label>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="checkbox checkbox-styled">
                    <label>
                        <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked{/if} value="1" name="form[int][num_estatus]">
                        <span>Estatus</span>
                    </label>
                </div>
            </div>
        </div>

        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal"
                descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar
        </button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
    </div>
</form>

<script type="text/javascript">
$(document).ready(function() {
    var app = new  AppFunciones();
    $("#formAjax").submit(function(){
        return false;
    });
    $('#modalAncho').css( "width", "25%" );
    $('#accion').click(function(){
        swal({
            title: "¡Por favor espere!",
            text: "Se esta procesando su solicitud, puede demorar un poco.",
            timer: 50000000,
            showConfirmButton: false
        });
        $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
            var arrayCheck = ["num_estatus"];
            var arrayMostrarOrden = ['ind_pais','num_estatus'];
            if(dato['status']=='error'){
                app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
            }else if(dato['status']=='modificar'){
                app.metActualizarRegistroTablaJson('dataTablaJson','La aplicacion fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
            }else if(dato['status']=='nuevo'){
                app.metNuevoRegistroTablaJson('dataTablaJson','La aplicacion fue guardado satisfactoriamente.','cerrarModal','ContenidoModal');
            }
        },'json');
    });
});
</script>