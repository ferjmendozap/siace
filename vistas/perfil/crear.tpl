<form action="{$_Parametros.url}perfil/crear" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idPerfil}" name="idPerfil"/>
        <div class="col-sm-4">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="ind_nombre_perfilError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_nombre_perfil)}{$formDB.ind_nombre_perfil}{/if}" name="form[alphaNum][ind_nombre_perfil]" id="ind_nombre_perfil">
                        <label for="ind_nombre_perfil"><i class="md md-security"></i>Nombre del Perfil</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="ind_descripcionError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}" name="form[alphaNum][ind_descripcion]" id="ind_descripcion">
                        <label for="ind_descripcion"><i class="md md-insert-comment"></i> Descripcion del Perfil</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked{/if} value="1" name="form[int][num_estatus]">
                            <span>Estatus</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
                <select id="optgroup" class="multiselect" multiple="multiple" name="form[int][menu][]" style="height: 300px;">
                    {foreach item=menu from=$listaMenu}
                        {if in_array($menu.pk_num_seguridad_menu,$formDBMenu)}
                            <option selected="selected" value="{$menu.pk_num_seguridad_menu}">{$menu.cod_interno} - {$menu.ind_nombre}</option>
                            {else}
                            <option value="{$menu.pk_num_seguridad_menu}">{$menu.cod_interno} - {$menu.ind_nombre}</option>
                        {/if}
                    {/foreach}
                </select>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal"
                descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar
        </button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        $(function(){
            $.localise('ui.multiselect', { path: '{$_Parametros.ruta_Complementos}SelectMultipleTabla/js/locale/' });
            $("#optgroup").multiselect();
        });
        $("#formAjax").submit(function(){
            return false;
        });
        $('#modalAncho').css( "width", "75%" );
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    for (var item in dato) {
                        if(dato[item]=='error'){
                            $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                            $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                            $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                        }else{
                            $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                            $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                            $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                            $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                        }
                    }
                }else if(dato['status']=='modificacion'){
                    $(document.getElementById('idPerfil'+dato['idPerfil'])).html('<td>'+dato['ind_nombre_perfil']+'</td>' +
                            '<td>'+dato['ind_descripcion']+'</td>' +
                            '<td><i class="md md-check"></i></td>' +
                            '<td  align="center">' +
                            '{if in_array('AP-03-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idPerfil="'+dato['idPerfil']+'"' +
                            'descipcion="El Usuario a Modificado un perfil" titulo="Modificar Perfil">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('AP-03-03-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPerfil="'+dato['idPerfil']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un perfil" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar le perfil!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "El Menu fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }else if(dato['status']=='creacion'){
                    $(document.getElementById('datatable1')).append('<tr  id="idPerfil'+dato['idPerfil']+'">' +
                            '<td>'+dato['ind_nombre_perfil']+'</td>' +
                            '<td>'+dato['ind_descripcion']+'</td>' +
                            '<td><i class="md md-check"></i></td>' +
                            '<td  align="center">' +
                            '{if in_array('AP-03-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idPerfil="'+dato['idPerfil']+'"' +
                            'descipcion="El Usuario a Modificado un perfil" titulo="Modificar perfil">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('AP-03-03-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPerfil="'+dato['idPerfil']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un perfil" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Perfil!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');
                    swal("Registro Guardado!", "El Menu fue guardado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
    });
</script>