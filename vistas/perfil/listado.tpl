<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Perfil - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Descripcion</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=perfil from=$listado}
                                <tr id="idPerfil{$perfil.pk_num_seguridad_perfil}">
                                    <td><label>{$perfil.ind_nombre_perfil}</label></td>
                                    <td><label>{$perfil.ind_descripcion}</label></td>
                                    <td>
                                        <i class="{if $perfil.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                    </td>
                                    <td align="center">
                                        {if in_array('AP-03-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idPerfil="{$perfil.pk_num_seguridad_perfil}" title="Editar"
                                                    descipcion="El Usuario a Modificado un Perfil" titulo="Modificar Perfil">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        &nbsp;&nbsp;
                                        {if in_array('AP-03-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPerfil="{$perfil.pk_num_seguridad_perfil}"  boton="si, Eliminar" title="Eliminar"
                                                    descipcion="El usuario a eliminado un Perfil" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Perfil!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="4">
                                {if in_array('AP-03-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario a creado un Perfil"  titulo="Crear Perfil" id="nuevo" >
                                        <i class="md md-create"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nuevo Perfil
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}perfil/crear';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idPerfil:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPerfil: $(this).attr('idPerfil')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {

            var idPerfil=$(this).attr('idPerfil');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}perfil/Eliminar';
                $.post($url, { idPerfil: idPerfil },function(dato){
                    if(dato['status']=='OK'){
                        $(document.getElementById('idPerfil'+dato['idPerfil'])).html('');
                        swal("Eliminado!", "el perfil fue eliminado satisfactoriamente.", "success");
                    }
                },'json');
            });
        });
    });
</script>