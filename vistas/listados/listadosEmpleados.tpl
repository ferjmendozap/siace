<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Empleados - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Cod. Empleado</th>
                            <th>Nombre y Apellidos</th>
                            <th>Tipo Nomina</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$listado}
                            <tr idEmpleado="{$i.pk_num_empleado}" nombre="{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}" nombreNomina="{$i.ind_nombre_nomina}" fecIngreso="{$i.fec_ingreso}">
                                <td><label>{$i.pk_num_empleado}</label></td>
                                <td><label>{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}</label></td>
                                <td><label>{$i.ind_nombre_nomina} </label></td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<span class="clearfix"></span>
<script type="text/javascript">
    $(document).ready(function() {
        $('#modalAncho').css( "width", "60%" );
        $('#datatable1 tbody').on( 'click', 'tr', function () {
            $(document.getElementById('nomApe')).val($(this).attr('nombre'));
            $(document.getElementById('codEmpleado')).val($(this).attr('idEmpleado'));
            $(document.getElementById('tipoNomina')).val($(this).attr('nombreNomina'));
            $(document.getElementById('fecIngreso')).val($(this).attr('fecIngreso'));
            {if $parametro=='NMANT'}
            $.post('{$_Parametros.url}modNM/procesos/anticipoCONTROL/anticipoEmpleadoMET',{ idEmpleado: $(this).attr('idEmpleado')},function(dato){
                $(document.getElementById('listaConceptoProceso')).html(dato);
            });
            {elseif $parametro=='NMSL'}
                $.post('{$_Parametros.url}modNM/procesos/asignacionConceptoCONTROL/conceptosEmpleadoMET',{ idEmpleado: $(this).attr('idEmpleado')},function(dato){
                    $(document.getElementById('listaConceptoProceso')).html(dato);
                });
            {elseif $parametro=='NMACPR'}
                $.post('{$_Parametros.url}modNM/reportes/acumuladoPrestacionesSocialesCONTROL/listadoPrestacionesMET',{ idEmpleado: $(this).attr('idEmpleado')},function(dato){
                    $(document.getElementById('listaConceptoProceso')).html(dato);
                });
            {elseif $parametro=='CSV'}
                $.post('{$_Parametros.url}modNM/maestros/cargarCsvPrestacionesCONTROL/csvEmpleadoMET',{ idEmpleado: $(this).attr('idEmpleado')},function(dato){
                    $(document.getElementById('listaCsv')).html(dato);
                });
            {/if}
            $(document.getElementById('cerrarModal')).click();
            $(document.getElementById('ContenidoModal')).html('');
        });
    });
</script>