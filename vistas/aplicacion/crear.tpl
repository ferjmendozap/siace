<form action="{$_Parametros.url}aplicacion/crear" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idAplicacion}" name="idAplicacion"/>
        <div class="row">
            <div class="col-sm-6">
                <div class="col-sm-4">
                    <div class="form-group floating-label" id="cod_aplicacionError">
                        <input type="text" class="form-control" value="{if isset($formDB.cod_aplicacion)}{$formDB.cod_aplicacion}{/if}" name="form[alphaNum][cod_aplicacion]" id="cod_aplicacion">
                        <label for="cod_aplicacion"><i class="fa fa-code"></i> Codigo</label>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group floating-label" id="ind_nombre_moduloError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_nombre_modulo)}{$formDB.ind_nombre_modulo}{/if}" name="form[alphaNum][ind_nombre_modulo]" id="ind_nombre_modulo">
                        <label for="ind_nombre_modulo"><i class="md md-extension"></i> Modulo</label>
                    </div>
                </div>
                <div class="col-sm-7">
                    <div class="input-group floating-label date" id="fec_creacionError">
                        <div class="input-group-content text-center">
                            <input type="text" id="fec_creacion" class="form-control" placeholder="Fecha de Creacion" name="form[alphaNum][fec_creacion]" value="{if isset($formDB.fec_creacion)}{$formDB.fec_creacion}{/if}">
                        </div>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked{/if} value="1" name="form[int][num_estatus]">
                            <span>Estatus</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="ind_descripcionError">
                        <textarea name="form[alphaNum][ind_descripcion]" id="ind_descripcion" class="form-control" rows="3" placeholder="">{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}</textarea>
                        <label for="ind_descripcion"><i class="md md-insert-comment"></i> Descripcion del Modulo</label>
                    </div>
                </div>
            </div>
        </div>

        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal"
                descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar
        </button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        var app = new AppFunciones();
        $("#formAjax").submit(function(){
            return false;
        });

        $('#fec_creacion').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});
        $('#modalAncho').css( "width", "55%" );
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                var arrayCheck = ["num_estatus"];
                var arrayMostrarOrden = ['cod_aplicacion','ind_nombre_modulo','ind_descripcion','fec_creacion','num_estatus'];
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSql'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son unicos');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTabla(dato,dato['idAplicacion'],'idAplicacion',arrayCheck,arrayMostrarOrden,'La Apicación fue modificada satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTabla(dato,dato['idAplicacion'],'idAplicacion',arrayCheck,arrayMostrarOrden,'La Apicación fue guardada satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });
    });
</script>