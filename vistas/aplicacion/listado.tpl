<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Aplicaciones - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Cod Modulo</th>
                                <th>Modulo</th>
                                <th>Descripcion</th>
                                <th>Fecha Creacion</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=aplicacion from=$listado}
                                <tr id="idAplicacion{$aplicacion.pk_num_seguridad_aplicacion}">
                                    <td><label>{$aplicacion.cod_aplicacion}</label></td>
                                    <td><label>{$aplicacion.ind_nombre_modulo}</label></td>
                                    <td><label>{$aplicacion.ind_descripcion}</label></td>
                                    <td><label>{$aplicacion.fec_creacion}</label></td>
                                    <td>
                                        <i class="{if $aplicacion.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                    </td>
                                    <td align="center">
                                        {if in_array('AP-01-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idAplicacion="{$aplicacion.pk_num_seguridad_aplicacion}" title="Editar"
                                                    descipcion="El Usuario a Modificado una Aplicacion" titulo="Modificar Aplicacion">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="6">
                                {if in_array('AP-01-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario a creado una Aplicacion"  titulo="Crear Aplicacion" id="nuevo" >
                                        <i class="md md-create"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nueva Aplicacion
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var url='{$_Parametros.url}aplicacion/crear';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idAplicacion:0 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post(url,{ idAplicacion: $(this).attr('idAplicacion') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });

    });
</script>