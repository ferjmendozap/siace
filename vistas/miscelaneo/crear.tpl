<form action="{$_Parametros.url}miscelaneo/crear" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idMiscelaneo}" name="idMiscelaneo"/>

        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="fk_a015_num_seguridad_aplicacionError">
                        <select id="fk_a015_num_seguridad_aplicacion" name="form[int][fk_a015_num_seguridad_aplicacion]" class="form-control">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$aplicacion}
                                {if $formDB.fk_a015_num_seguridad_aplicacion==$app.pk_num_seguridad_aplicacion}
                                    <option selected value="{$app.pk_num_seguridad_aplicacion}">{$app.ind_nombre_modulo}</option>
                                    {else}
                                    <option value="{$app.pk_num_seguridad_aplicacion}">{$app.ind_nombre_modulo}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="fk_a015_num_seguridad_aplicacion"><i class="md md-apps"></i> Seleccione la aplicacion</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-7">
                    <div class="form-group floating-label" id="ind_nombre_maestroError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_nombre_maestro)}{$formDB.ind_nombre_maestro}{/if}" name="form[txt][ind_nombre_maestro]" id="ind_nombre_maestro">
                        <label for="ind_nombre_maestro"><i class="md md-border-color"></i> Nombre del Miscelaneo</label>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group floating-label" id="cod_maestroError">
                        <input maxlength="10" type="text" class="form-control" value="{if isset($formDB.cod_maestro)}{$formDB.cod_maestro}{/if}" {if isset($formDB.cod_maestro)} readonly {/if} name="form[txt][cod_maestro]" id="cod_maestro">
                        <label for="cod_maestro"><i class="fa fa-barcode"></i> Cod Maestro</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="ind_descripcionError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}" name="form[txt][ind_descripcion]" id="ind_descripcion">
                        <label for="ind_descripcion"><i class="md md-insert-comment"></i> Descripcion del Miscelaneo</label>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-12">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked{/if} value="1" name="form[int][num_estatus]">
                            <span>Estatus</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body no-padding">
                        <div class="table-responsive no-margin">
                            <table class="table table-striped no-margin" id="contenidoTabla">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>Codigo</th>
                                        <th>Nombre de las opciones</th>
                                        <th style="width: 20px;">Estatus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {if isset($formDBSelect)}
                                    {foreach item=opciones from=$formDBSelect}
                                        <tr>
                                            <input type="hidden" value="{$opciones.pk_num_miscelaneo_detalle}" name="form[int][pk_num_miscelaneo_detalle][{$n}]">
                                            <td class="text-right" style="vertical-align: middle;">
                                                {$numero++}
                                            </td>
                                            <td style="vertical-align: middle;">
                                                <div class="col-sm-12">
                                                    <div class="form-group floating-label" id="cod_detalle{$n}Error">
                                                        <input type="text" class="form-control" value="{$opciones.cod_detalle}" name="form[txt][cod_detalle][{$n}]" id="cod_detalle{$n}">
                                                        <label for="cod_detalle{$n}"><i class="md md-insert-comment"></i> Codigo</label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="vertical-align: middle;">
                                                <div class="col-sm-12">
                                                    <div class="form-group floating-label" id="ind_nombre_detalle{$n}Error">
                                                        <input type="text" class="form-control" value="{$opciones.ind_nombre_detalle}" name="form[txt][ind_nombre_detalle][{$n}]" id="ind_nombre_detalle{$n}">
                                                        <label for="ind_nombre_detalle{$n}"><i class="md md-insert-comment"></i> Nombre </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td  class="text-center" style="vertical-align: middle;">
                                                <div class="col-sm-12">
                                                    <div class="checkbox checkbox-styled">
                                                        <label>
                                                            <input type="checkbox" {if $opciones.num_estatus == 1}checked{/if} value="1" name="form[int][num_estatusDet][{$n++}]">
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    {/foreach}
                                {/if}
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3">
                                            <div class="col-sm-offset-3 col-sm-6">
                                                <button type="button" id="nuevoCampo" class="btn btn-info ink-reaction btn-raised">
                                                    <i class="md md-add"></i> Insertar Nuevo Campo
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal"
                descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar
        </button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        var app = new  AppFunciones();
        $('#contenidoTabla  tbody').on('keyup change', 'tr td .floating-label input' ,function (e) {
            var input = $(e.currentTarget);
            if ($.trim(input.val()) !== '') {
                input.addClass('dirty').removeClass('static');
            } else {
                input.removeClass('dirty').removeClass('static');
            }
        });
        $('#contenidoTabla').on('click', '.delete', function () {
            $(this).parent('td').parent('tr').remove();
        });
        $("#nuevoCampo").click(function() {
            var idtabla= $("#contenidoTabla");
            var tr= $("#contenidoTabla > tbody > tr");
            var numTr=tr.length - 1;
            var nuevoTr=numTr+1;

            var numero=nuevoTr+1;
            var nombreObciones='<div class="form-group floating-label" id="ind_nombre_detalle'+nuevoTr+'Error">'+
                                    '<input type="text" class="form-control" value="" name="form[txt][ind_nombre_detalle]['+nuevoTr+']" id="ind_nombre_detalle'+nuevoTr+'">'+
                                    '<label for="ind_nombre_detalle'+nuevoTr+'"><i class="md md-insert-comment"></i> Nombre '+numero+'</label>'+
                                '</div>';
            var codObciones='<div class="form-group floating-label" id="cod_detalle'+nuevoTr+'Error">'+
                                    '<input type="text" class="form-control" value="" name="form[txt][cod_detalle]['+nuevoTr+']" id="cod_detalle'+nuevoTr+'">'+
                                    '<label for="cod_detalle'+nuevoTr+'"><i class="md md-insert-comment"></i> Codigo '+numero+'</label>'+
                                '</div>';
            var checkOpciones='<div class="checkbox checkbox-styled">'+
                                '<label><input type="checkbox" value="1" name="form[int][num_estatusDet]['+nuevoTr+']" ><span></span></label>'+
                                '</div>';
            idtabla.append('<tr>'+
                                '<td class="text-right" style="vertical-align: middle;">'+numero+'</td>'+
                                '<td style="vertical-align: middle;"><div class="col-sm-12">'+codObciones+'</div></td>'+
                                '<td style="vertical-align: middle;"><div class="col-sm-12">'+nombreObciones+'</div></td>'+
                                '<td  class="text-center" style="vertical-align: middle;"><div class="col-sm-12">'+checkOpciones+'</div></td>'+
                                '<td  class="text-center" style="vertical-align: middle;"><button class="btn ink-reaction btn-raised btn-xs btn-danger delete"><i class="md md-delete"></i></button></td>'+
                            '</tr>');

        });
        $("#formAjax").submit(function(){
            return false;
        });
        $('#modalAncho').css( "width", "75%" );
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                var arrayCheck = ["num_estatus"];
                var arrayMostrarOrden = ['cod_maestro','ind_nombre_maestro','ind_descripcion','num_estatus'];
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTabla(dato,dato['idMiscelaneo'],'idMiscelaneo',arrayCheck,arrayMostrarOrden,'El Miscelaneo fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTabla(dato,dato['idMiscelaneo'],'idMiscelaneo',arrayCheck,arrayMostrarOrden,'El Miscelaneo fue Creado satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });
    });
</script>