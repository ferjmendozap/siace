<form action="{$_Parametros.url}ciudad/crearModificar" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idCiudad}" name="idCiudad"/>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group" id="fk_a008_num_paisError">
                    <label for="fk_a008_num_pais"><i class="md md-map"></i> Pais</label>
                    <select id="fk_a008_num_pais" name="form[int][fk_a008_num_pais]" class="form-control select2-list select2">
                        <option value="">Seleccione el Pais</option>
                        {foreach item=i from=$listadoPais}
                            {if isset($formDB.fk_a008_num_pais)}
                                {if $i.pk_num_pais==$formDB.fk_a008_num_pais}
                                    <option selected value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                {else}
                                    <option value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                {/if}
                            {else}
                                <option value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                            {/if}
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group" id="fk_a009_num_estadoError">
                    <label for="fk_a009_num_estado"><i class="md md-map"></i> Estado</label>
                    <select id="fk_a009_num_estado" name="form[int][fk_a009_num_estado]" class="form-control select2-list select2">
                        <option value="">Seleccione el Estado</option>
                    </select>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group" id="fk_a011_num_municipioError">
                    <input type="hidden" name="form[int][pk_num_ciudad_municipio]" value="{if isset($formDB.pk_num_ciudad_municipio)}{$formDB.pk_num_ciudad_municipio}{/if}">
                    <label for="fk_a011_num_municipio"><i class="md md-map"></i> Municipio</label>
                    <select id="fk_a011_num_municipio" name="form[int][fk_a011_num_municipio]" class="form-control select2-list select2">
                        <option value="">Seleccione el Municipio</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group floating-label" id="ind_ciudadError">
                    <input type="text" class="form-control" value="{if isset($formDB.ind_ciudad)}{$formDB.ind_ciudad}{/if}" name="form[alphaNum][ind_ciudad]" id="ind_ciudad">
                    <label for="ind_ciudad"><i class="md md-my-location"></i> Ciudad</label>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group floating-label" id="ind_cod_postalError">
                    <input type="text" class="form-control" value="{if isset($formDB.ind_cod_postal)}{$formDB.ind_cod_postal}{/if}" name="form[alphaNum][ind_cod_postal]" id="ind_cod_postal">
                    <label for="ind_cod_postal"><i class="md md-my-location"></i> Cod Postal</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="checkbox checkbox-styled">
                    <label>
                        <input type="checkbox" {if isset($formDB.num_flag_capital) and $formDB.num_flag_capital==1} checked{/if} value="1" name="form[int][num_flag_capital]">
                        <span>Flag Capital</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="checkbox checkbox-styled">
                    <label>
                        <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked{/if} value="1" name="form[int][num_estatus]">
                        <span>Estatus</span>
                    </label>
                </div>
            </div>
        </div>

        <span class="clearfix"></span>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal"
                descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar
        </button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
    </div>
</form>

<script type="text/javascript">
$(document).ready(function() {
    var app = new  AppFunciones();
    var idEstado = '{if isset($formDB.fk_a009_num_estado)}{$formDB.fk_a009_num_estado}{else}0{/if}';
    var idMunicipio = '{if isset($formDB.pk_num_municipio)}{$formDB.pk_num_municipio}{else}0{/if}';
    var idPais = '{if isset($formDB.fk_a008_num_pais)}{$formDB.fk_a008_num_pais}{/if}';
    app.metJsonEstado('{$_Parametros.url}estado/jsonEstado',idPais,idEstado);
    $("#formAjax").submit(function(){
        return false;
    });
    $('.select2').select2({ allowClear: true });
    $('#modalAncho').css( "width", "25%" );
    $('#accion').click(function(){
        swal({
            title: "¡Por favor espere!",
            text: "Se esta procesando su solicitud, puede demorar un poco.",
            timer: 50000000,
            showConfirmButton: false
        });
        $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
            var arrayCheck = ["num_estatus","num_flag_capital"];
            var arrayMostrarOrden = ['fk_a008_num_pais','fk_a009_num_estado','ind_ciudad',"num_flag_capital",'num_estatus'];
            if(dato['status']=='error'){
                app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
            }else if(dato['status']=='modificar'){
                app.metActualizarRegistroTablaJson('dataTablaJson',arrayCheck,arrayMostrarOrden,'El Estado fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
            }else if(dato['status']=='nuevo'){
                app.metNuevoRegistroTablaJson('dataTablaJson','El Estado fue guardado satisfactoriamente.','cerrarModal','ContenidoModal');
            }
        },'json');
    });
    $('#fk_a009_num_estado').ready(function(){
        $.post('{$_Parametros.url}ciudad/jsonMunicipioMET', { idEstado: idEstado },function(dato){
            $('#fk_a011_num_municipio').html('');
            $('#fk_a011_num_municipio').append('<option value="">Seleccione el Municipio</option>');
            $('#s2id_fk_a011_num_municipio .select2-chosen').html('Seleccione el Municipio');
            for (var i =0; i < dato.length; i=i+1) {
                if(dato[i]['pk_num_municipio']==idMunicipio){
                    $('#fk_a011_num_municipio').append('<option selected value="'+dato[i]['pk_num_municipio']+'">'+dato[i]['ind_municipio']+'</option>');
                    $('#s2id_fk_a011_num_municipio .select2-chosen').html(dato[i]['ind_municipio']);
                } else {
                    $('#fk_a011_num_municipio').append('<option value="'+dato[i]['pk_num_municipio']+'">'+dato[i]['ind_municipio']+'</option>');
                }
            }
        },'json');
    });
    $('#fk_a009_num_estado').change(function(){
        $.post('{$_Parametros.url}ciudad/jsonMunicipioMET', { idEstado: $(this).val() },function(dato){
            $('#fk_a011_num_municipio').html('');
            $('#fk_a011_num_municipio').append('<option value="">Seleccione el Municipio</option>');
            $('#s2id_fk_a011_num_municipio .select2-chosen').html('Seleccione el Municipio');
            for (var i =0; i < dato.length; i=i+1) {
                $('#fk_a011_num_municipio').append('<option value="'+dato[i]['pk_num_municipio']+'">'+dato[i]['ind_municipio']+'</option>');
            }
        },'json');
    });
});
</script>