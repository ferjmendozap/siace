<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Parametros - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Clave</th>
                                <th>Valor</th>
                                <th>Tipo</th>
                                <th>Descripcion</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=i from=$listado}
                                <tr id="idParametro{$i.pk_num_parametros}">
                                    <td><label>{$i.ind_parametro_clave}</label></td>
                                    <td><label>{$i.ind_valor_parametro}</label></td>
                                    <td><label>{$i.ind_nombre_detalle}</label></td>
                                    <td><label>{$i.ind_descripcion}</label></td>
                                    <td>
                                        <i class="{if $i.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                    </td>
                                    <td align="center">
                                        {if in_array('AP-07-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idParametro="{$i.pk_num_parametros}" title="Editar"
                                                    descipcion="El Usuario a Modificado un Parametro" titulo="Modificar Parametro">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="6">
                                {if in_array('AP-07-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario a creado un Parametro"  titulo="Crear Parametro" id="nuevo" >
                                        <i class="md md-create"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nuevo Parametro
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var url='{$_Parametros.url}parametros/crearModificar';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idParametro:0 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post(url,{ idParametro: $(this).attr('idParametro') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
    });
</script>