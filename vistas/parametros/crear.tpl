<form action="{$_Parametros.url}parametros/crearModificar" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idParametro}" name="idParametro"/>


        <div class="row">
            <div class="col-sm-12">
                <div class="form-group" id="fk_a015_num_seguridad_aplicacionError">
                    <label for="fk_a015_num_seguridad_aplicacion"><i class="md md-map"></i> Aplicacion</label>
                    <select id="fk_a015_num_seguridad_aplicacion" name="form[int][fk_a015_num_seguridad_aplicacion]" class="form-control select2-list select2">
                        <option value="">Seleccione la Aplicacion</option>
                        {foreach item=i from=$aplicaciones}
                            {if isset($formDB.fk_a015_num_seguridad_aplicacion)}
                                {if $i.pk_num_seguridad_aplicacion==$formDB.fk_a015_num_seguridad_aplicacion}
                                    <option selected value="{$i.pk_num_seguridad_aplicacion}">{$i.ind_nombre_modulo}</option>
                                {else}
                                    <option value="{$i.pk_num_seguridad_aplicacion}">{$i.ind_nombre_modulo}</option>
                                {/if}
                            {else}
                                <option value="{$i.pk_num_seguridad_aplicacion}">{$i.ind_nombre_modulo}</option>
                            {/if}
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group" id="fk_a006_num_miscelaneo_detalleError">
                    <label for="fk_a006_num_miscelaneo_detalle"><i class="md md-map"></i> Tipo de Parametro</label>
                    <select id="fk_a006_num_miscelaneo_detalle" name="form[int][fk_a006_num_miscelaneo_detalle]" class="form-control select2-list select2">
                        <option value="">Seleccione el Tipo de Parametro</option>
                        {foreach item=i from=$miscelaneo}
                            {if isset($formDB.fk_a006_num_miscelaneo_detalle)}
                                {if $i.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle}
                                    <option selected value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                {else}
                                    <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                {/if}
                            {else}
                                <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                            {/if}
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="ind_parametro_claveError">
                    <input type="text" class="form-control" value="{if isset($formDB.ind_parametro_clave)}{$formDB.ind_parametro_clave}{/if}" name="form[txt][ind_parametro_clave]" id="ind_parametro_clave">
                    <label for="ind_parametro_clave"><i class="md md-my-location"></i> Parametro</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="ind_valor_parametroError">
                    <input type="text" class="form-control" value="{if isset($formDB.ind_valor_parametro)}{$formDB.ind_valor_parametro}{/if}" name="form[txt][ind_valor_parametro]" id="ind_valor_parametro">
                    <label for="ind_valor_parametro"><i class="md md-my-location"></i> Valor</label>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group floating-label" id="ind_descripcionError">
                    <input type="text" class="form-control" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}" name="form[txt][ind_descripcion]" id="ind_descripcion">
                    <label for="ind_descripcion"><i class="md md-my-location"></i> Descripcion</label>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group floating-label" id="ind_explicacionError">
                    <textarea name="form[txt][ind_explicacion]" id="ind_explicacion" class="form-control" rows="3" placeholder="">{if isset($formDB.ind_explicacion)}{$formDB.ind_explicacion}{/if}</textarea>
                    <label for="ind_explicacion"> explicacion</label>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="checkbox checkbox-styled">
                    <label>
                        <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked{/if} value="1" name="form[int][num_estatus]">
                        <span>Estatus</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="cod_procesoError">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="cod_proceso"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal"
                descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar
        </button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        var app = new  AppFunciones();
        $("#formAjax").submit(function(){
            return false;
        });
        $('.select2').select2({ allowClear: true });
        $('#modalAncho').css( "width", "38%" );
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                var arrayCheck = ["num_estatus"];
                var arrayMostrarOrden = ['ind_parametro_clave','ind_valor_parametro','ind_tipo_valor','ind_descripcion','num_estatus'];
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTabla(dato,dato['idParametro'],'idParametro',arrayCheck,arrayMostrarOrden,'El Parametro fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTabla(dato,dato['idParametro'],'idParametro',arrayCheck,arrayMostrarOrden,'El Parametro fue Creado satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });
    });
</script>