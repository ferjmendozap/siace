<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Estados - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Pais</th>
                                <th>Estados</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th colspan="4">
                                {if in_array('AP-06-02-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario ha creado un Estado"  titulo="<i class='md md-map'></i> Crear Estado" id="nuevo" >
                                        <i class="md md-create"></i>&nbsp; Nuevo Estado
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}estado/jsonDataTabla",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "ind_pais" },
                    { "data": "ind_estado" },
                    { "orderable": false,"data": "num_estatus",'width':30 },
                    { "orderable": false,"data": "acciones",'width':30 }
                ]
        );

        var url='{$_Parametros.url}estado/crearModificar';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idEstado:0 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post(url,{ idEstado: $(this).attr('idEstado') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });

    });
</script>