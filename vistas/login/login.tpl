<!-- begin #page-loader -->
<div id="page-loader" class="fade in"><span class="spinner"></span></div>
<!-- end #page-loader -->

<div class="login-cover">
    <div class="login-cover-image"><img src="{$_Parametros.ruta_Img}login2/bg-5g.jpg" data-id="login-cover-image" alt="" /></div>
    <div class="login-cover-bg"></div>
</div>
<!-- begin #page-container -->
<div id="page-container" class="fade">
    <!-- begin login -->
    <div class="login login-v2" data-pageload-addclass="animated flipInX">
        <!-- begin brand -->
        <div class="login-header" style="margin-top: -30px;">
            <div class="brand text-center">
                <b>SIACE<span style="color:#009FC6 ">S</span></b>
                <small>
                    <strong>SISTEMA INTEGRAL ADMINISTRATIVO DE LA CONTRALORÍA DEL ESTADO <span style="color:#009FC6;">SUCRE</span></strong>
                </small>
            </div>
        </div>
        <!-- end brand -->
        <div class="login-content">
            <form action="{$_Parametros.url}login/loginCheck" accept-charset="utf-8" id="formAjax" method="POST" class="margin-bottom-0">
                <div class="form-group m-b-20">
                    <label>Usuario:</label>
                    <input type="text" class="form-control input-lg" name="login[usuario]" placeholder="Ingrese su Usuario aquí" required autofocus/>
                </div>
                <div class="form-group m-b-20">
                    <label>Contraseña:</label>
                    <input type="password" class="form-control input-lg" name="login[password]" placeholder="Ingrese su Contraseña aquí" required/>
                </div>

                <div class="login-buttons">
                    <button id="accion" type="button" class="btn btn-info btn-block btn-lg">Iniciar Sesión</button>
                </div>
            </form>
        </div>
    </div>
    <!-- end login -->
</div>

<script src="{$_Parametros.ruta_Complementos}jquery/jquery-1.9.1.min.js"></script>
<script src="{$_Parametros.ruta_Complementos}jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="{$_Parametros.ruta_Complementos}jquery-ui/jquery-ui.min.js"></script>
<script src="{$_Parametros.ruta_Complementos}bootstrap/bootstrap.js"></script>
<script src="{$_Parametros.ruta_Complementos}slimscroll/jquery.slimscroll.min.js"></script>
<script src="{$_Parametros.ruta_Complementos}jquery-cookie/jquery.cookie.js"></script>
<script src="{$_Parametros.ruta_Js}login2/login-v2.demo.min.js"></script>
<script src="{$_Parametros.ruta_Js}login2/apps.min.js"></script>
<script>
    $(document).ready(function() {
        $("#formAjax").submit(function(){
            return false;
        });
        function Enviar(){
            $.post($("#formAjax").attr("action"), $("#formAjax" ).serialize(),function($dato){
                if($dato['status']=='valido'){
                    window.location.href = "{$_Parametros.url}";
                }else{
                    alert($dato['status']);
                }
            },'json');
        }
        $('#accion').click(function(){
            Enviar()
        });
        $("#formAjax").keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                Enviar()
            }
        });
        App.init();
        LoginV2.init();
    });
</script>