<form action="{$_Parametros.url}usuario/crear" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idUsuario}" name="idUsuario"/>

        {if $idUsuario == 0}
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group" id="fk_rhb001_num_empleadoError">
                        <label for="fk_rhb001_num_empleado"><i class="md md-people"></i>Empleado</label>

                        <div class="form-group col-sm-12" id="fk_rhb001_num_empleadoError">
                            <input type="hidden" class="form-control" name="form[int][fk_rhb001_num_empleado]"
                                   id="idEmpleado">
                            <div class="form-group col-sm-11">
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="personaResp" value="{if isset($formDB.ind_nombre1) and isset($formDB.ind_apellido1)}{$formDB.ind_nombre1} {$formDB.ind_apellido1}{/if}" disabled>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="cedula" value="{if isset($formDB.ind_cedula_documento)}{$formDB.ind_cedula_documento}{/if}" disabled>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <button class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                        type="button"
                                        title="Buscar Empleado"
                                        data-toggle="modal" data-target="#formModal2"
                                        data-keyboard="false" data-backdrop="static"
                                        titulo="Buscar Empleado"
                                        url="{$_Parametros.url}modLG/maestros/almacenesCONTROL/empleadoMET/persona/">
                                    <i class="md md-search" style="color: #ffffff;"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        {/if}
        <div class="col-sm-4">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="ind_usuarioError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" name="form[alphaNum][ind_usuario]" id="ind_usuario">
                        <label for="ind_usuario"><i class="md md-person-add"></i> Usuario</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_password1Error">
                        <input type="password" class="form-control" value="{if isset($formDB.ind_password)}noModificar{/if}" name="form[alphaNum][ind_password1]" id="ind_password1">
                        <label for="ind_password1"><i class="md md-vpn-key"></i> Clave</label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_password2Error">
                        <input type="password" class="form-control" value="{if isset($formDB.ind_password)}noModificar{/if}" name="form[alphaNum][ind_password2]" id="ind_password2">
                        <label for="ind_password2"><i class="md md-vpn-key"></i> Repita la Clave</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="ind_templateError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_template)}{$formDB.ind_template}{/if}" name="form[alphaNum][ind_template]" id="ind_template">
                        <label for="ind_template"><i class="md md-style"></i>Tema del Sistema</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked{/if} value="1" name="form[int][num_estatus]">
                            <span>Estatus</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-offset-1"></div>
        <div class="col-sm-7">
            <div class="row">
                <div class="col-sm-5 col-sm-offset-1"><i class="fa fa-square-o"></i> POR SELECCIONAR</div>
                <div class="col-sm-4 col-sm-offset-2"><i class="fa fa-check-square-o"></i> SELECCIONADOS</div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                <select id="optgroup" multiple="multiple" name="form[int][perfil][]">
                    {foreach item=perfil from=$listaPerfil}
                        {if in_array($perfil.pk_num_seguridad_perfil,$formDBPerfiles)}
                            <option selected="selected" value="{$perfil.pk_num_seguridad_perfil}">Perfil: {$perfil.ind_nombre_perfil}</option>
                        {else}
                            <option value="{$perfil.pk_num_seguridad_perfil}">Perfil: {$perfil.ind_nombre_perfil}</option>
                        {/if}

                    {/foreach}
                </select>
                </div>
            </div>
        </div>

        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal"
                descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar
        </button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        $('#optgroup').multiSelect({ selectableOptgroup: true });
        $("#formAjax").submit(function(){
            return false;
        });
        $('.accionModal').click(function () {
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#modalAncho').css( "width", "75%" );
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    for (var item in dato) {
                        if(dato[item]=='error'){
                            $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                            $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                            $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                        }else{
                            $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                            $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                            $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                            $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                        }
                    }
                    swal('Error!','Los campos en Rojo son Incorrectos','error');
                }else if(dato['status']=='modificacion'){
                    $(document.getElementById('idUsuario'+dato['idUsuario'])).html('<td>'+dato['ind_usuario']+'</td>' +
                            '<td>espara por las tablas de recursos humanos</td>' +
                            '<td>'+dato['ind_template']+'</td>' +
                            '<td><i class="md md-check"></i></td>' +
                            '<td  align="center">' +
                            '{if in_array('AP-04-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idUsuario="'+dato['idUsuario']+'"' +
                            'descipcion="El Usuario a Modificado un Usuario del Sistema" titulo="Modificar Usuario">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('AP-04-03-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idUsuario="'+dato['idUsuario']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario ha eliminado un usuario del sistema" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar al Usuario!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "El Usuario fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }else if(dato['status']=='creacion'){
                    $(document.getElementById('datatable1')).append('<tr  id="idUsuario'+dato['idUsuario']+'">' +
                            '<td>'+dato['ind_usuario']+'</td>' +
                            '<td>espara por las tablas de recursos humanos</td>' +
                            '<td>'+dato['ind_template']+'</td>' +
                            '<td><i class="md md-check"></i></td>' +
                            '<td  align="center">' +
                            '{if in_array('AP-01-02-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idUsuario="'+dato['idUsuario']+'"' +
                            'descipcion="El Usuario a Modificado un Usuario del Sistema" titulo="Modificar Usuario">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('AP-01-02-03-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idUsuario="'+dato['idUsuario']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Usuario del Sistema" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Usuario!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');
                    swal("Registro Guardado!", "El Usuario fue guardado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
    });
</script>