<form action="{$_Parametros.url}usuario/cambiarPass" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idUsuario}" name="idUsuario"/>

        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="ind_passwordViejaError">
                        <input type="password" class="form-control" name="form[alphaNum][ind_passwordVieja]" id="ind_passwordVieja">
                        <label for="ind_passwordVieja"><i class="icm icm-key"></i> Contrase&ncaron;a Anterior</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="ind_passwordNueva1Error">
                        <input type="password" class="form-control" name="form[alphaNum][ind_passwordNueva1]" id="ind_passwordNueva1">
                        <label for="ind_passwordNueva1"><i class="icm icm-key"></i> Nueva Contrase&ncaron;a</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="ind_passwordNueva2Error">
                        <input type="password" class="form-control"  name="form[alphaNum][ind_passwordNueva2]" id="ind_passwordNueva2">
                        <label for="ind_passwordNueva2"><i class="icm icm-key"></i> Repita la Nueva Contrase&ncaron;a</label>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer text-center">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">Cambiar Contrase&ncaron;a</button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        $("#formAjax").submit(function(){
            return false;
        });
        $('#modalAncho').css( "width", "30%" );
        $('#accion').click(function(){
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    for (var item in dato) {
                        if(dato[item]=='error'){
                            $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                            $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                            $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                        }else{
                            $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                            $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                            $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                            $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                        }
                    }
                }else{
                    if(dato['status']=='modificado'){
                        swal("Modificacion exitosa!", "Su Contraseņa fue Modificada exitosamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                    }
                }
            },'json');
        });
    });
</script>
